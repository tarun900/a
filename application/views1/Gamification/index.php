<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
$favorites_user=array_filter(array_column($my_favorites,'module_id'));
$active_menu=array_filter(explode(",",$event_templates[0]['checkbox_values']));
?>

<div class="agenda_content">
  <div class="panel panel-white-new">

      <div class="header" style="display:table;margin:0 auto;height: 70px;">
        <img style="width: 50px;float: left;padding-top: 22px;" src="<?php echo base_url().'assets/images/gamification/Star.png';?>">
          <span style="float: left;margin-top: 0px;margin-right: 10px;margin-left: 10px;"><?php echo $get_leaderboard_data['header']; ?> </span>
        <img style="width: 50px;float: left;padding-top: 22px;" src="<?php echo base_url().'assets/images/gamification/Star-R.png';?>">
      </div>

      <?php 
        $width = 90;
        foreach($get_leaderboard_data['top_five'] as $key => $value): 
        switch ($value['Role_id'])
        {
          case '4':
            $role = 'Attendee';
            break;

          case '6':
            $role = 'Exhibitors';
            break;

          case '7':
            $role = 'Speakers';
            break;      
        }
        ?>
        <a href="<?php echo base_url().$role; ?>/<?php echo $acc_name."/".$Subdomain; ?>/View/<?php echo $value['id']; ?>" class="activity" id="full_directory_a<?php echo $value['id']; ?>">
      <div style="background: <?=$value['color'] ?>; color: #fff;position:relative; width: <?=$width?>%; height: 94px;   box-shadow:  0px 3px 3px #999; border-bottom-right-radius: 50px;border-top-right-radius: 50px;padding: 20px;margin-bottom: 2px;" class="bar">
        <?php 
        if(!empty($value['logo']))
        { ?>
          <img width="72" height="72" src="<?php echo base_url().'assets/user_files/'.$value['logo'];?>" alt="" style="float: left;border-radius: 100%;margin-top: -7px;margin-right: 12px;">
        <?php
        }
        else
        {
          ?>
          <span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display:block;color:#fff;float:left;margin:-7px 10px 0 0;background:<?php echo $event_templates[0]['Top_background_color'];?>"><?=substr($value['Sender_name'],0,1)?></span>
          <?php
        } ?>
          <span style="width: 70%;display: block;" class="detail"><?=ucfirst($value['Sender_name'])?></span>
          <span style="width: 70%;display: block;" class="detail"><?=$value['Company_name']?></span>
          <span style="width: 70%;display: block;" class="detail"><?=$value['Title']?></span>

          <div class="scrore" style="position:absolute;right:20px;top:32px;">
            <span style="font-weight: bold;font-size: 31px;display:block;line-height:26px;"><?=$value['total']?></span>
            <span>Points</span>
          </div>
      </div>
      </a>
    <?php 
    if($value['total'] != $get_leaderboard_data['top_five'][$key+1]['total'])
    {
      $width = $width - 10;
    } 
    endforeach;?>


      <div style="background: #EEE;width: 50%;margin: 20px auto;padding: 20px;height: 290px;" class="info">
        <h3 style="text-align: center;color:<?php echo $event_templates[0]['Top_background_color'];?>">Earn Points Now!</h3>
        <hr style="border: 1px solid #999; /">
        <div style="text-align: center;width: 100%;" class="points">
        <?php foreach($get_leaderboard_data['info'] as $key => $value):?>
          <span style="font-size: 20px;float: left;width: 60%;"><?=$value['point']?></span> <span style="float: right;"><?=$value['rank']?></span>
        <?php endforeach;?>
        </div>
      </div>


  </div>
</div>