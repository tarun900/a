<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                 <a style="top: 7px;" class="btn btn-primary list_page_btn schedule_btn" href="<?php echo base_url(); ?>Notification/addschedule/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Scheduled Notification</a>
                 <a style="top: 7px;display: none;" class="btn btn-primary list_page_btn sent_btn"  href="<?php echo base_url(); ?>Notification/addsentnoti/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i>Create Instant Notification <!--Add Sent Notification--></a>

                 <?php if ($this->session->flashdata('cms_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Push Notifications <?php echo $this->session->flashdata('cms_data'); ?> Successfully.
                    </div>
                <?php } ?>
                
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#schedule" data-toggle="tab">
                                Scheduled Notifications
                            </a>
                        </li>
                        <li>
                            <a href="#sent" data-toggle="tab">
                                Instant Notifications
                            </a>
                        </li>
                        <li>
                            <a href="#default_push" data-toggle="tab">
                                Default Push Notifications
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="schedule">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                  <tr>
                                       <th>Title</th>
                                       <th>Scheduled Time</th>
                                       <th>Short Description</th>
                                       <th>Action</th>
                                  </tr>
                             </thead>
                             <tbody>
                                  <?php foreach($arrSchedule as $key => $value) {?>
                                      
                                       <tr>
                                          <td><?php echo $value['title'] ?></td>
                                          <td><?php echo $value['datetime'] ?></td>
                                          <td><?php echo substr($value['content'],0, 80);?></td> 
                                          <td>
                                              <a href="<?php echo base_url() ?>Notification/editschedule/<?php echo $value['event_id']; ?>/<?php echo $value['Id'];?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                              <a href="javascript:;" onclick="delete_notification(<?php echo $value['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                          </td>
                                        </tr>

                                  <?php } ?>
                              
                         </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade" id="sent">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="item-table">
                                <thead>
                                  <tr>
                                       <th>Title</th>
                                       <th>Time Sent</th>
                                       <th>Short Description</th>
                                       <th>Action</th>
                                  </tr>
                             </thead>
                             <tbody>
                                  <?php $i=0; foreach($arrSent as $key => $value) {?>
                                      
                                       <tr>
                                          <td><?php echo $value['title'];?></td>
                                          <td><?php echo $value['created_at'];?></td>
                                          <td><?php echo substr($value['content'], 0, 80); ?></td> 
                                          <td>
                                              <a href="<?php echo base_url() ?>Notification/editsentnoti/<?php echo $value['event_id']; ?>/<?php echo $value['Id'];?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                              <a href="javascript:;" onclick="delete_notification(<?php echo $value['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                          </td>
                                        </tr>

                                  <?php } ?>
                              
                         </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default_push">
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th width="35%">Name</th>
                                        <th width="40%">Content</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($arrPushTemplate)){
                                        for($i=0;$i<count($arrPushTemplate);$i++) {?>
                                    <tr>
                                        <td width="35%"><?php echo stripslashes($arrPushTemplate[$i]['Slug']);?></td>
                                        <td width="40%"><?php echo stripslashes($arrPushTemplate[$i]['Content']);?></td>
                                        <td width="20%">
                                            <a href="<?php echo base_url() ?>Notification/pushedit/<?php echo $arrPushTemplate[$i]['event_id']; ?>/<?php echo $arrPushTemplate[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                        <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Feature Menu Title</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="img_view" id="img_view" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="Redirect_url" id="Redirect_url" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    jQuery(function(){
        jQuery('#item-table').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        jQuery("#myTab2 li a").click(function(){
          var strTab = jQuery(this).prop("href");
          var arrTab = strTab.split("#");
          
          if(arrTab[1]=='sent')
          {
              jQuery(".sent_btn").show();
              jQuery(".schedule_btn").hide();
          }
          else if(arrTab[1]=='schedule')
          {
              jQuery(".schedule_btn").show();
              jQuery(".sent_btn").hide();
          }
          else
          {
              jQuery(".schedule_btn").hide();
              jQuery(".sent_btn").hide();
          }
        });   
    });
    $(document).ready(function(){
        if(window.location.hash != "") {
            $('a[href="' + window.location.hash + '"]').click()
        }
      });
</script>    
<script type="text/javascript">
    function delete_notification(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            
            window.location.href ="<?php echo base_url(); ?>Notification/delete/"+<?php echo $test; ?>+"/"+id
        }
    }
    /*function up(id)
    {

       window.location.href ="<?php echo base_url(); ?>cms/up/"+id;
    }
    function down(id)
    {
      window.location.href ="<?php echo base_url(); ?>cms/down/"+id;
    }*/
</script>
