<?php $acc_name=$this->session->userdata('acc_name');
date_default_timezone_set("UTC");
$cdate=date('Y-m-d H:i:s');

if(!empty($event['Event_show_time_zone']))
{
    if(strpos($event['Event_show_time_zone'],"-")==true)
    { 
        $arr=explode("-",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
    }
    if(strpos($event['Event_show_time_zone'],"+")==true)
    {
        $arr=explode("+",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
    }
}

?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <?php if($this->uri->segment(3) == '1511'){ ?>
                        <li class="active">
                        <a id="analytics1" href="#analytics" data-toggle="tab">
                            Analytics
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
			<div class="panel-body">
				<?php if ($this->session->flashdata('notification_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Notification <?php echo $this->session->flashdata('notification_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="analytics"> 
                        <div class="row" style="margin-bottom: 25px;">
                            <a class="btn btn-primary list_page_btn schedule_btn" href="<?php echo base_url(); ?>Notification/index/<?php echo $this->uri->segment(3); ?>#analytics"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Status</th>
                                    <th>Sent Time</th>
                                    <th>Received Time</th>
                                    <th>Opened Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($noti_analytics as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>    
                                    <td><?php echo $value['Firstname'].' '.$value['Lastname'] ?></td>  
                                    <td><?php 
                                        switch ($value['status']) {
                                            case '1':
                                                echo '<label>Success</label>';
                                                break;
                                            case '0':
                                                echo '<label>Failed</label>';
                                                break;
                                        }
                                      ?></td> 
                                    <td><?php echo dateTimeFormat($value['sent_time'],$event); ?></td>   
                                    <td><?php echo dateTimeFormat($value['received_time'],$event); ?></td>    
                                    <td><?php echo dateTimeFormat($value['open_time'],$event); ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>    
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    if(window.location.hash != "") {
        $('a[href="' + window.location.hash + '"]').click()
    }
});
function delete_notification(id,is_geo=0)
{   
    <?php $test = $this->uri->segment(3); ?>
    if(confirm("Are you sure to delete this?"))
    {
        
        window.location.href ="<?php echo base_url(); ?>Notification/delete/"+<?php echo $test; ?>+"/"+id+"/"+is_geo
    }
}
function activeemail(id) {
    $.ajax({
        url:"<?php echo base_url().'Notification/active_mail_notify/'.$event_id.'/' ?>"+id,
        success:function(result) 
        {
            var shortCutFunction ='success';
            if($("#send_email_"+id).is(':checked'))
            {
              var title = 'Active Successfully';
              var msg = 'Email Notification Active Successfully.';
            }
            else
            {
              var title = 'Inactive Successfully';
              var msg = 'Email Notification Inactive Successfully'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
        }    
    });
}
</script>    		

<?php 
function dateTimeFormat($cdate,$event)
{
    if(!empty($event['Event_show_time_zone']) && strtotime($cdate))
    {
        if(strpos($event['Event_show_time_zone'],"-")==true)
        { 
            $arr=explode("-",$event['Event_show_time_zone']);
            $intoffset=$arr[1]*3600;
            $intNew = abs($intoffset);
            $cdate = date('d M Y, H:i:s',strtotime($cdate)-$intNew);
        }
        if(strpos($event['Event_show_time_zone'],"+")==true)
        {
            $arr=explode("+",$event['Event_show_time_zone']);
            $intoffset=$arr[1]*3600;
            $intNew = abs($intoffset);
            $cdate = date('d M Y, H:i:s',strtotime($cdate)+$intNew);
        }
    }
    else
    {
        $cdate = '-';
    }
    return $cdate;
}
?>