<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#add_notification" data-toggle="tab">
                                Add Notification
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="add_notification">
                    	<div class="col-sm-10">
                			<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data" onsubmit="return before_submit()">
                				<h4 style="color: #5381ce;">New Notification</h4>
                				<div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Title <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <input type="text" placeholder="Notification Title" id="name" name="title" class="form-control name_group required" value="<?=$notification['title']?>">
	                                </div>
	                            </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Message <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <textarea id="Description form-field-23" class="form-control required" name="Description" style="width:100%;height:100px;"><?=$notification['content']?></textarea>
	                                </div>
	                            </div>
                            	<div class="form-group" id="user_groups" style="display: none;">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Attendees Group<span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-10">
	                                    <select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_group" class="select2-container select2-container-multi form-control search-select menu-section-select" name="user_group[]">
										<?php
	                                    	$user_groups=explode(",",$notification['user_groups']);
	                                     foreach($user_group as $key => $value):?>
	                                    	<option value="usergroup_<?=$value['id']?>" <?php if(in_array($value['id'],$user_groups)){ ?> selected="selected" <?php } ?>><?=$value['group_name']?></option>
	                                    <?php endforeach;?>
	                                    </select>
                            		</div>
	                            </div>
	                            <div class="form-group" id="users">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Attendees <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                	<?php $user_ids=explode(",",$notification['user_ids']);?>
	                                		  
	                                	<select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_ids" class="select2-container select2-container-multi form-control search-select menu-section-select" name="user_ids[]">
	                                		<?php if($notification['send_to_all'] == '1'):?>
	                                    <option value="All" selected="selected">All</option>
	                                    <?php foreach ($attendee_data as $key => $value) { ?>
	                                    <option value="<?php echo $value['Id']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
	                                    <?php } ?>
	                                    </select>
	                                    <?php else: ?>
                                    	<option value="All">All</option>
	                                    <?php foreach ($attendee_data as $key => $value) { ?>
	                                    <option value="<?php echo $value['Id']; ?>" <?php if(in_array($value['Id'],$user_ids) && !in_array($value['Id'],$groups_user_ids)){ ?> selected="selected" <?php } ?>><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
	                                    <?php } ?>
	                                    </select>
	                                    <?php endif; ?>
                            		</div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Open button link <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <?php $arrkey=array(5,8,14,18,19,21,26,27,28,29); ?>
	                                    <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id">
	                                    <optgroup label="Select Survey">
	                                    <?php foreach($survey as $key => $value): ?>
	                                    	<option id="menu<?php echo $key; ?>" value="M_<?php echo $value['survey_id']; ?>" <?php if($value['survey_id']==$notification['moduleslink']){ ?> selected="selected" <?php } ?>><?=$value['survey_name']?></option>
	                                	<?php endforeach;?>
	                                    </optgroup>

	                                    <optgroup label="Select RSS post">
                                    	<?php foreach($rss as $key => $value): ?>
	                                    	<option id="menu<?php echo $key; ?>" value="C_<?php echo $value['id']; ?>" <?php if($value['id']==$notification['moduleslink']){ ?> selected="selected" <?php } ?>><?php echo $value['title']; ?></option>
	                                	<?php endforeach;?>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-md-2 pull-left">
	                                    <button class="btn btn-green btn-block" type="submit">
	                                        Send 
	                                    </button>
	                                </div>
	                            </div>       
                			</form>
                    	</div>
                    </div>
                </div>    
            </div>
        </div>        
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/geo/locationpicker.jquery.js?123"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	change();
});
function change()
{
	if($('#send_attendee_group').is(":checked"))
	{
		$('#user_groups').show();
		$('#user_group').addClass('required');
		$('#users').hide();
		$('#user_ids').removeClass('required');
	}
	else
	{
		$('#user_groups').hide();
		$('#user_group').removeClass('required');
		$('#users').show();	
		$('#user_ids').addClass('required');	
	}
}
function before_submit()
{
	if($('#send_attendee_group').is(":checked"))
		$('#user_ids').val('').change();
	else
		$('#user_group').val('').change();
	return true;
}
function noenter() {
  return !(window.event && window.event.keyCode == 13); }
$('#us3').locationpicker({
    location: {
        latitude: <?php echo $notification['lat'];?>,
        longitude: <?php echo $notification['long'];?>
    },
    radius: <?php echo $notification['radius'];?>,
    inputBinding: {
        latitudeInput: $('#us3-lat'),
        longitudeInput: $('#us3-lon'),
        radiusInput: $('#us3-radius'),
        locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
});
</script>