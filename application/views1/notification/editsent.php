<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
		
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#edit_notification" data-toggle="tab">
                                Edit Notification
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="edit_notification">

                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Title <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Notification Title" id="name" name="title" class="form-control name_group required" value="<?php echo $notification['title'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                               <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Send copy to Email 
                                </label>
                                <div class="col-sm-9">
                                   <input type="checkbox" name="chkEmailSend" value="1" <?php if($notification['send_email']=="1"): echo "checked"; endif;?> class="checkbox" /> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Content <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <textarea  id="Description" name="Description" class="" style="width:100%;height:250px;"><?php echo $notification['content'] ?></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Module<span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <?php $arr=explode(",",$notification['moduleslink']);
                            $string_menu_id = $advertising_by_id[0]['Menu_id'];
                            $Menu_id =  explode(',', $string_menu_id); 
                            $arrkey=array(5,12,13,18,19,20,22,23,24,25,26,27,28,29); ?>
                          <select multiple="multiple" style="margin-top:-8px;" class="select2-container select2-container-multi form-control menu-section-select search-select required" name="Menu_id[]">
                                <option value="Send All">Send All</option>
                                <?php foreach ($menu_toal_data as $key => $value) { 
                                    if(!in_array($value['id'], $arrkey)){
                                    ?>
                                <option <?php echo in_array($value['id'], $arr) ? 'selected="selected"' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
                                <?php } } ?>
                                
                            </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Update <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<script type="text/javascript">
    function closediv()
     {
        if($("#notification_type").val()=='1')
        {
            $("#notification_option").css('display','none');
            $(".btn-block").text('SEND NOW');
        }
        else
        {
            $("#notification_option").css('display','block');
            $(".btn-block").text('Update');
        }
      }
      closediv();
</script>


<!-- end: PAGE CONTENT-->