<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Notification</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
                    
			<div class="panel-body">
                            <?php if($this->session->flashdata('appnoti_data')){ ?>
                            <div class="errorHandler alert alert-success no-display" style="display: block;">
                                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('appnoti_data'); ?> Successfully.
                            </div>
                            <?php } ?>
                            <form role="form" method="post" class="form-horizontal" id="mine-form" action="<?php echo base_url();?>Notification/pushedit/<?php echo $arrTemplate[0]['event_id'];?>/<?php echo $arrTemplate[0]['Id'];?>" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                                Title : 
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" maxlength="20" class="form-control limited"  name="Subject" disabled="" value="<?php echo $arrTemplate[0]['Slug'];?>">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                            <label class="col-sm-2 control-label" for="form-field-1">
                                                    Content: <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-5">
                                                <textarea maxlength="40" name="Content" class="form-control limited" cols="10" rows="10"><?php echo $arrTemplate[0]['Content'];?></textarea>
                                            </div>
                                    </div>
                                    <?php if($arrTemplate[0]['send_email']=='1'){ ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                            Email Subject : 
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="email_subject" class="form-control" value="<?php echo $arrTemplate[0]['email_subject'];?>">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                            Email Content: 
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea name="email_content" class="ckeditor form-control" cols="10" rows="10"><?php echo $arrTemplate[0]['email_content'];?></textarea>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                        </label>
                                        <div class="col-md-4">
                                            <button class="btn btn-yellow btn-block" type="submit">
                                                Submit <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->