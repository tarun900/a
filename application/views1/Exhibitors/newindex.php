<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = ($this->session->userdata('acc_name'))?:$this->uri->segment(2);
$favorites_user=array_filter(array_column($my_favorites,'module_id'));
$active_menu=array_filter(explode(",",$event_templates[0]['checkbox_values']));
$exibitors_data[0]['exhibitor'] = $exibitors_list;
// j($exibitors_list);
$exhi_country_id = array_unique(array_column($exibitors_country,'country_id'));
$exhi_country = array_unique(array_column($exibitors_country,'country_name'));
$country = array_combine($exhi_country_id,$exhi_country);
asort($country);
?>
<style type="text/css">
.inner .margin-left-10{display: block;margin:0;}
.inner .agenda_content{padding:15px;width:100%;margin:0;}
.inner .form-group .form-control{width:100%;}
.inner .inner .gold-sponsor{padding:0;}

/*.inner .agenda_content select.form-control{border-radius:10px !important;border:2px solid #c1c1c1 !important;}*/
.inner .agenda_content #search_user_content{border-radius:10px !important;border:2px solid #c1c1c1;}

/*.inner .form-group select.form-control option {
    padding: 3px 4px;
    box-shadow: 1px 1px 1px #ddd !important;
    border: 1px solid #ddd;
    width: 99.5%;
    margin: 0 auto;
    text-align: left;
    padding: 10px;
    margin: 5px auto;
    background-color: #fff !important;
    border-radius: 2px !important; 
}*/
.inner ul li p.logo-img span{
    text-align: center;
    width: auto !important;
    height: auto !important;
    padding-top: 0;
    font-size: 28px !important;
    display: inline-block;
    color: #fff;
    max-width: 100%;
    padding:10px 15px   !important;
}
@media(max-width:1024px){
.inner .gold1 .logo-img > img{width:auto;height:50px;max-width:100%;margin:0;}
.inner .gold1 ul li p{margin-right:10px;width: 10%;}

.inner ul li p.logo-img span{font-size:20px !important;width: auto !important;
    height: auto !important;padding:10px 20px !important;}
}
@media(max-width:800px){
.inner .gold1 ul li h4{font-size:14px;margin:0;width:75%;display: inline-block;vertical-align: middle;float: none;}
.inner .gold1 .activities li a{padding:10px !important;}
.inner .agenda_content .activities li a i{font-size:14px;}
.inner .agenda_content .activities li a .time{top:15px;right: 10px;}
.inner .gold-sponsor {padding: 0}
}
@media(max-width:767px){
.inner .gold1 ul li p{margin-right:10px;width:15%;}
.inner .gold1 .logo-img > img{width:auto;height:auto;max-width:100%;margin:0;}
.inner ul li p.logo-img span{
    text-align: center;
    width: auto !important;
    height: auto !important;
    padding-top: 0;
    font-size: 16px !important;
    display: inline-block;
    color: #fff;
    background: #5266A0;
    max-width: 100%;
    padding:10px 15px   !important;
}
}
@media(max-width:400px){
.inner .gold1 ul li h4{font-size: 12px;}
}
</style>
<div class="row margin-left-10">
    <?php
        if(!empty($form_data)):
        foreach($form_data as $intKey=>$strval):
            if($strval['frm_posi']==2)
            {
                continue;
            }
        ?>
    <div class="col-sm-6 col-sm-offset-3">
        <?php
            $json_data = false;
            $formid="form";
            
            $temp_form_id=rand(1,9999);
            $json_data = isset($strval) ? $strval['json_data'] : FALSE;
            $loader = new formLoader($json_data, base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
            $loader->render_form();
            unset($loader);
            ?>
    </div>
    <?php include 'validation.php'; ?>
    <script>
        jQuery(document).ready(function() {
            FormValidator<?php echo $temp_form_id; ?>.init();
        });
    </script>
    <?php endforeach;  
        endif;
        ?>
</div>
<?php
    if(!empty($advertisement_images)) { ?>
<div class="main-ads">
    <?php 
        $string_cms_id = $advertisement_images->Cms_id;
        $Cms_id =  explode(',', $string_cms_id); 
        
        $string_menu_id = $advertisement_images['Menu_id'];
        $Menu_id =  explode(',', $string_menu_id); 
        
        if(in_array('3', $Menu_id)) 
        {
        
        $image_array = json_decode($advertisement_images['H_images']);
        $f_url = $advertisement_images['Footer_link'];
        
        if(!empty($image_array)) { ?>
    <div class="hdr-ads">
        <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id']; ?>");'> 
        <button type="button" class="close-custom close" data-dismiss="alert" 
            aria-hidden="true"> &times; </button>
        <?php
            $img_url = base_url().'assets/user_files/'.$image_array[0];
            $size = getimagesize($img_url);
            
            $originalWidth = $size[0];
            $originalHeight = $size[1];
            
             if($originalHeight > '118')
             {
                $first = $originalWidth * 118;
                $width = $first / $originalHeight;
            
                echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
             }
             elseif ($originalHeight < '118') 
             { 
                echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
             }
             else
             {
                 echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
             }
            ?>
        </a>
        <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>
        <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
            aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
        <?php } } }  ?>
    </div>
</div>
<?php } ?>
<?php if($user[0]->Rid=='6' && count($attendees_metting)>0){ ?>
<div class="col-sm-12"> <a id="check_metting_btn" href="<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>" class="btn btn-green user_btn">See My Meetings</a></div>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newsponsors.css?'.time(); ?>">
<?php if(count($exibitors_list) > 0){ ?>
<div class="agenda_content">
    <div>
        <div class="input-group search_box_user">
            <i class="fa fa-search search_user_icon"></i>    
            <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
    </div>
    <!-- <div class="different-product">
        <ul>
            <?php foreach ($exhibitor_category as $key => $value) { ?>
            <li>
                <a href="javascript:void(0);" onclick="getexibitor_list(<?php echo $value['id']; ?>,'0');">
                    <img src="<?php echo base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']; ?>" alt="img"/>
                    <p><?php echo ucfirst($value['category']); ?></p>
                </a>
            </li>
            <?php } ?>
        </ul>
    </div> -->
    <div class="form-group">
        <select class="js-example-basic-single" id='exhi_cat' multiple="multiple" style="width: 100%;border-radius:10px !important;" placeholder="Select Category">
            
            <?php foreach ($exhibitor_category as $key => $value) { ?>
                    <option value="<?php echo $value['id'];?>"><?php echo ucfirst($value['category']); ?></option>
            <?php } ?>
            <option value='0'>Clear Filter</option>
        </select>
    </div>
    <!-- <div class="different-product">
        <ul>
            <?php foreach ($country as $key => $value) { ?>
            <li>
                <a href="javascript:void(0);" onclick="getexibitor_list(<?php echo $key; ?>);">
                    <p><?php echo ucfirst($value); ?></p>
                </a>
            </li>
            <?php } ?>
        </ul>
    </div> -->
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
    $(".js-example-basic-single").select2();
});
</script>
    <div class="form-group">
    <!-- getexibitor_list('0',this.value) -->
      <select class="js-example-basic-single" multiple="multiple" onchange="" id='exhi_cntry' style="width: 100%;border-radius:10px !important;" placeholder="Select Country">
        <?php foreach ($country as $key => $value) { ?>
            <option value='<?php echo $key; ?>'><?php echo ucfirst($value); ?></option>            
        <?php } ?>
      </select>
    </div>
    <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 43px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.switch-box span {
    display: inline-block;
    line-height: 36px;
    padding-right: 10px;
    vertical-align: top;
}
.switch-box .switch {
    display: inline-block;
    float: none;
}
.gold-sponsor-nopadding{
	padding: 0;
}
@media (max-width: 767px){
	.venue-option .form-control{
		max-width: 280px;
	}	
}

</style>
    <?php if($event_templates[0]['Id'] == '585'): ?>
    <div class="form-group center row">
        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-7 venue-option">
          <select class="form-control" id="vanue" onchange="getexibitor_list_new()">
            <option value="0">Venue</option>
            <option value="1">Singapore Expo</option>
            <option value="2">Suntec Singapore</option>
          </select>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 pull-right text-right switch-box">
          <span>Halal</span>
          <label class="switch">
            <input type="checkbox" id="halal_check" onchange="getexibitor_list_new()">
            <span class="slider round"></span>
          </label>
        </div>
    </div>
    <?php endif; ?>
    <div class="form-group center">
        <input type="button" value="Apply" class="btn btn-green" onclick="getexibitor_list_new()">
        <input type="button" value="Clear" class="btn btn-green" onclick="getexibitor_list_new('1');">
        <input type="button" class="btn btn-green" onclick="history.back(-1)" value="Back">
    </div>
    <div id="main_exibitor_list">
        <?php 
        foreach ($exibitors_data as $key => $value) { ?>
        <div class="gold-sponsor gold-sponsor-nopadding">
            <?php if(count($value['exhibitor'])>0){ ?>
            <h3 style="background:<?php echo $value['type_color'] ?>;color:#fff;"><?php echo ucfirst($value['type_name']); ?></h3>
            <?php } ?>
            <div class="gold1">
                <ul class="activities columns activities-new">
                    <?php foreach ($value['exhibitor'] as $skey => $svalue){ ?>
                    <li>
                        <a data-url="<?php echo base_url(); ?>Exhibitors/<?php echo $acc_name.'/'.$Subdomain;?>/View/<?php echo $svalue['Id']; ?>" class="showmodel1" onclick="openmodel(<?php echo $svalue['Id']; ?>)" id="a<?php echo $svalue['Id']; ?>">
                            <div class="desc">
                                <p class="logo-img" style="width: auto;">
                                    <?php $clogo=json_decode($svalue['company_logo']); $pathexit; if($Subdomain=="gulfoodGulfood2017"){ 
                                        $pathexit=$clogo[0];
                                        }else{
                                          $pathexit=base_url().'assets/user_files/'.$clogo[0];
                                        } $ext = pathinfo($pathexit, PATHINFO_EXTENSION);?>
                                    <?php if(!empty($clogo[0]) && !empty($ext)){ ?>
                                    <img style="width: auto;max-width: 80px;height: auto;" src="<?php echo filter_var($clogo[0], FILTER_VALIDATE_URL) ? $clogo[0] : base_url().'assets/user_files/'.$clogo[0]; ?>"/>
                                    <?php }else{ $color = $event_templates[0]['Top_background_color']; ?>
                                    <span style="background:<?php echo $color; ?>"><?php echo ucfirst(substr($svalue['Heading'], 0, 1)); ?></span>
                                    <?php } ?>
                                </p>
                                <h4 class="user_container_name"><?php echo ucfirst($svalue['Heading']); ?></h4>
                                <h4 class="user_container_username" style="display:none;"><?php echo trim($svalue['Firstname']).' '.trim($svalue['Lastname']); ?></h4>
                                <div class="Exbitor_keyword" style="display: none;">
                                    <?php echo $svalue['Short_desc']; ?>
                                </div>
                                <div class="Exbitor_contry" style="display: none;">
                                    <?php echo $svalue['country_name']; ?>
                                </div>
                            </div>
                            <div class="time"><i class="fa fa-chevron-right"></i></div>
                        </a>
                        <?php if(!empty($user[0]->Id) && in_array('49',$active_menu) && $user[0]->Rid=='4'){ ?> 
                        <div class="myfavorites_content_div">
                            <a onclick='my_favorites("<?php echo $svalue['Id']; ?>");'>
                            <img width="5%" src="<?php echo base_url().'assets/images/favorites_not_selected.png'; ?>" class="pull-right" id="favorites_not_selected_<?php echo $svalue['Id']; ?>" <?php if(in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                            <img width="5%" src="<?php echo base_url().'assets/images/favorites_selected.png'; ?>" class="pull-right" id="favorites_selected_<?php echo $svalue['Id']; ?>" <?php if(!in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                            </a>
                            <a href="javascript:void(0);" style="display: none;">
                            <img width="5%" src="<?php echo base_url().'assets/images/ajax-loader.gif'; ?>" id="loder_icone_<?php echo $svalue['Id']; ?>">
                            </a>
                        </div>
                        <?php } ?>
                    </li>
                    <?php } ?>    
                    <?php if($event_templates[0]['Id'] == '585'){ ?>
                    <div class="col-md-12" style="text-align: center;">
                      <a href="javascript:;" id="load_more" class="btn btn-primary" >
                      Load More</a>
                    </div>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="gold-sponsor gold-sponsor-nopadding" id="categorie_exibitor_list" style="display: none;">
        <div class="gold1">
            <ul class="activities columns activities-new" id="categories_exibitorlist_ul">
            </ul>
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="tab-content">
    <span>No Exhibitor Available for this App.</span>
</div>
<?php } ?>
<style>
  .modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
}
.modal-dialog {
  position: fixed;
  margin: 0;
  width: 100%;
  height: 100%;
  padding: 0;
}
.modal-content {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  border-radius: 0;
  box-shadow: none;
}
.modal-body {
  /*position: absolute;*/
  top: 0px;
  bottom: 60px;
  width: 100%;
  font-weight: 300;
  overflow: auto;
  padding: 0;
}

</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="btn btn-green" data-dismiss="modal" aria-label="Close" style="z-index: 777;position: absolute;right: 50px;top: 40px;"><span aria-hidden="true">Back</span></button>
            <iframe src="" id="iframe" style="border: 0;width: 100%;"></iframe>
          </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>


$(document).ready(function(){
       $(".showmodel").click(function()
       {     
             $('#iframe').attr('height',$(window).height()+'px');
             document.getElementById("iframe").src = $(this).data('url');
             $('#exampleModal').modal('show');
       });

       
});
   function openmodel(id)
   {    
        var url = "<?php echo base_url(); ?>Exhibitors/<?php echo $acc_name.'/'.$Subdomain.'/View/';?>"+id;
        $('#iframe').attr('height',$(window).height()+'px');
         document.getElementById("iframe").src = url;
         $('#exampleModal').modal('show');
   }
    var halal_check = '0';
    var exhi_ids = <?php echo json_encode(array_unique(array_column($exibitors_country,'Id')));?>;
    function getexibitor_list(cid,cntry_id)
    { 
      if(cid == '0')
      {
        cid = $('#exhi_cat').val();
      }
      if(cntry_id == '0')
      {
        cntry_id = $('#exhi_cntry').val();
      }
      var parent_id = <?php echo $this->uri->segment(5);?>;
      $.ajax({
        url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/get_categorie_exibitorlist/' ?>"+cid+"/"+cntry_id+"/"+parent_id,
        error:function(error)
        {
          $('#main_exibitor_list').show();
          $('#categorie_exibitor_list').hide();
          $('#categories_exibitorlist_ul').html('');
        },
        success:function(response)
        {
          $('#main_exibitor_list').hide();
          $('#categorie_exibitor_list').show();
          $('#categories_exibitorlist_ul').html(response);
        }
      });
    }
    function getexibitor_list_with_load_more(cid,cntry_id)
    { 
      if(cid == '0')
      {
        cid = $('#exhi_cat').val();
      }
      if(cntry_id == '0')
      {
        cntry_id = $('#exhi_cntry').val();
      }
      var parent_id = <?php echo $this->uri->segment(5);?>;
      $.ajax({
        url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/get_categorie_exibitorlist/' ?>"+cid+"/"+cntry_id+"/"+parent_id,
        error:function(error)
        {
          $('#main_exibitor_list').show();
          $('#categorie_exibitor_list').hide();
          $('#categories_exibitorlist_ul').html('');
        },
        success:function(response)
        {
          $('#main_exibitor_list').hide();
          $('#categorie_exibitor_list').show();
          response += '<div class="col-md-12" style="text-align: center;"><a href="javascript:;" id="load_more" class="btn btn-primary" >Load More</a></div>';
          $('#categories_exibitorlist_ul').html(response);
        }
      });
    }
    function getexibitor_list_new(reset)
    { 
      if(reset == '1')
      {
        $('#exhi_cntry').select2('val','');
        $('#exhi_cat').select2('val','');
        $('#vanue option:eq(0)').prop('selected', true);
        $('#halal_check').prop('checked', false);
      }
      var venue = $('#vanue option:selected').val();
      var parent_id = <?php echo $this->uri->segment(5);?>;
      cntry_id = $('#exhi_cntry').select2('val');
      cat_id = $('#exhi_cat').select2('val');
      if($('#halal_check').is(":checked"))
      {
          halal_check = '1';
      }
      else
      {
          halal_check = '0';
      }
      if(halal_check == '0' && venue == '0' && cntry_id == '' && cat_id == '')
      { 
        if(reset == '1')
        {
          getexibitor_list_with_load_more('0','0');
        }
        else
        {
          getexibitor_list('0','0');
        }
      }
      else
      {
        $.ajax({
        	type:"POST",
          url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/get_categorie_exibitorlist_new/' ?>"+parent_id,
          data:{'country_ids':cntry_id,'category_ids':cat_id,halal_check:halal_check,exhi_ids:exhi_ids,venue:venue},
          error:function(error)
          {
            $('#main_exibitor_list').show();
            $('#categorie_exibitor_list').hide();
            $('#categories_exibitorlist_ul').html('');
          },
          success:function(response)
          {  
            $('#main_exibitor_list').hide();
            $('#categorie_exibitor_list').show();
            if(reset == '1')
                response += '<a href="javascript:;" id="load_more">Load More</a>';
            $('#categories_exibitorlist_ul').html(response);
          }
        });
      }
    }
         jQuery("#search_user_content").keyup(function()
         {    
              jQuery(".activities-new li").each(function( index ) 
              {
                  var str=jQuery(this).find('.user_container_name').html();
                  var strkey=jQuery(this).find('.Exbitor_keyword').html();
                  var strunm=jQuery('this').find('.user_container_username').html();
                  if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='' || jQuery.trim(strkey)!=undefined || jQuery.trim(strkey.toLowerCase())!='' || jQuery.trim(strunm)!=undefined || jQuery.trim(strunm.toLowerCase())!='')
                  {    
                       var str1=jQuery(this).find('.user_container_name').html();
                       var strkey1=jQuery(this).find('.Exbitor_keyword').html();
                       var strunm1=jQuery(this).find('.user_container_username').html();
                       if(str1!=undefined && strkey1!=undefined && strunm1!=undefined)
                       {
                             var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                             var strkey1=jQuery(this).find('.Exbitor_keyword').html().toLowerCase();
                             var strunm1=jQuery(this).find('.user_container_username').html().toLowerCase();
                             var content=jQuery("#search_user_content").val().toLowerCase();
                             if(content!=null)
                             {
                                  jQuery(this).parent().parent().parent().find(".date_separator").each(function(index) 
                                  {
                                    var content1=jQuery(this).text().toLowerCase();
                                    var n1 = str1.indexOf(content1);
                                    var nk1 = strkey1.indexOf(content1);
                                    var nk2 = strunm1.indexOf(content1);
                                  });
                                  var n = str1.indexOf(content);
                                  var nk = strkey1.indexOf(content);
                                  var nk1 = strunm1.indexOf(content);
                                  if(n=="-1" && nk=="-1" && nk1=="-1")
                                  {
                                    jQuery(this).css('display','none');
                                  }
                                  else
                                  {
                                    jQuery(this).css('display','block');
                                  }
                             }
                        }
                  }
              });
         }); 
    
    function my_favorites(uid)
    {
      $('#loder_icone_'+uid).parent().show();
      $('#favorites_not_selected_'+uid).parent().hide();
      $.ajax({
        url:"<?php echo base_url().'My_Favorites/'.$acc_name.'/'.$Subdomain.'/save_myfavorites'; ?>",
        type:'post',
        data:'module_id='+uid+"&module_type=3",
        success:function(result){
          var data=result.split('###');
          if($.trim(data[0])=="success")
          {
            var name=$('#a'+uid).find('.user_container_name').text();
            $('#loder_icone_'+uid).parent().hide();
            $('#favorites_not_selected_'+uid).parent().show();
            $('#favorites_not_selected_'+uid).parent().removeAttr('disabled');
            if(data[1]=='0')
            {
              $('#favorites_not_selected_'+uid).show();
              $('#favorites_selected_'+uid).hide();
              var msg = name+" Removed From My Favorites";
              var title = 'Removed From My Favorites';
            }
            else
            {
              $('#favorites_not_selected_'+uid).hide();
              $('#favorites_selected_'+uid).show();
              var msg = name+" Added Into Your My Favorites";
              var title = 'Added To My Favorites';
            }
          }
          else
          {
            $('#loder_icone_'+uid).parent().parent().remove();
            var msg = "Somthing Went To Wrong Please Reload the Page.";
            var title = 'Somthing Went To Wrong';
          }
          var shortCutFunction ='info';
          var $showDuration = 1000;
          var $hideDuration = 10000;
          var $timeOut = 10000;
          var $extendedTimeOut = 10000;
          var $showEasing = 'swing';
          var $hideEasing = 'linear';
          var $showMethod = 'fadeIn';
          var $hideMethod = 'fadeOut';
          toastr.options = {
            closeButton: true,
            debug: false,
            positionClass:'toast-bottom-right',
            onclick: null
          };
          toastr.options.showDuration = $showDuration;
          toastr.options.hideDuration = $hideDuration;
          toastr.options.timeOut = $timeOut;                        
          toastr.options.extendedTimeOut = $extendedTimeOut;
          toastr.options.showEasing = $showEasing;
          toastr.options.hideEasing = $hideEasing;
          toastr.options.showMethod = $showMethod;
          toastr.options.hideMethod = $hideMethod;
          toastr[shortCutFunction](msg, title);
        }
      });
    }    
</script>
<script>
  $(document).on('click',"#load_more",function(){
            $('#main_exibitor_list').find('#load_more').remove();
            $('#categories_exibitorlist_ul').find('#load_more').remove();
            var start=$('#main_exibitor_list .activities-new > li').length;
            $.ajax({
              url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/'.$intFormId.'/'.$parent_id .'/load_more'; ?>?start="+start,
              type:'get',
              success:function(result)
              {
                $('.activities-new').append(result);
              }
            });
       
    });
    
</script>