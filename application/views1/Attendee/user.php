<?php
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.min.css" />
<div id="request_meeting_popup" class="modal fade" role="dialog">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span>Request a meeting with <br/> <?php echo ucfirst($user_url[0]["Firstname"] . ' ' . $user_url[0]["Lastname"]); ?></span>
               </div>
               <?php  
               function getDatesFromRange($start, $end, $format = 'Y-m-d') 
               {
                    $array = array();
                    $interval = new DateInterval('P1D');

                    $realEnd = new DateTime($end);
                    $realEnd->add($interval);
                    
                    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
                    foreach($period as $date) 
                    { 
                         $array[] = $date->format($format); 
                    }
                    return $array;
               }
               if($event_templates[0]['Id']=='479')
	          {
	           	$dates = getDatesFromRange(date('Y-m-d',strtotime('2017-11-07')),date('Y-m-d',strtotime('2017-11-08')));
	          }
	          else
	          {
               	$dates = getDatesFromRange($event_templates[0]['Start_date'],$event_templates[0]['End_date']);
               } 
               ?>
               <div class="modal-body">
                    <form id="metting_user_validation_form" action="" method="post">
                         <div class="row">
                            <h5 id="request_meeting_error_msg_show" style="text-align: center;"></h5>
                         </div>
                         <div class="row" id="form_date_time_request_meeeting_form">
                              <div class="form-group"> 
                                   	<label class="control-label col-sm-2">Date:</label>
                                   	<div class="col-sm-10">  
                                        <select name="date" id="date_drop_down" <?php if($event_templates[0]['Id']=='479'){ ?> onchange="hidetimeslot(this);" <?php } ?> class="form-control">
                                             <?php foreach($dates as $key => $value) { ?>
                                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                             <?php } ?>
                                        </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-sm-2">Time:</label>
                                   <div class="col-sm-10">
                                        <select name="time" class="form-control" id="time_drop_down">
                                          <?php 
                                             if($event_templates[0]['Id']=='479')
							          {
                                                  $range=array(strtotime('10:40'),strtotime('10:55'),strtotime('12:55'),strtotime('13:10'),strtotime('13:25'),strtotime('13:40'),strtotime('15:40'),strtotime('15:55'),strtotime('17:10'),strtotime('17:25'),strtotime('17:40'),strtotime('17:55'),strtotime('18:10'),strtotime('18:25'),strtotime('18:40'),strtotime('18:55'));
							          }
							          else
							          {
                                        	    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                        	}
                                            foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                            <option value="<?php echo date("H:i",$time); ?>" <?php if(date("H:i",$time) > "15:55"){ ?> class="datetime_range_hide" <?php } ?>><?php echo date("H:i",$time); ?></option>
                                          <?php } } ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="form-group" <?=empty($meeting_locations)?'style=display:none;':''?>>
                                   <label class="control-label col-sm-2">Locations:</label>
                                   <div class="col-sm-10">
                                        <select class="form-control required" id="metting_location" name="metting_location">
                                             <option value="">Select Location</option>
                                             <?php foreach ($meeting_locations as $key => $value) { ?>
                                             <option value="<?php echo $value['location']; ?>"><?php echo $value['location']; ?></option>
                                             <?php } ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <div class="col-sm-12">
                                        <button type="button" id="request_meeting_btn_form_submitting" class="btn btn-green btn-block">Request Meeting</button>
                                   </div>
                              </div>    
                         </div>
                         <div class="row" id="form_date_time_request_meeeting_form_submittng_msg" style="display: none;">
                              <div class="row">
                                 <h5 id="msg_span_text_show" style="text-align: center;"></h5>
                              </div>
                              <div class="col-sm-12">
                                   <div class="col-sm-6">
                                     <button type="button" id="yes_meeting_btn_click" class="btn btn-green btn-block">Yes</button>
                                   </div>
                                   <div class="col-sm-6">
                                     <button type="button" id="no_metting_btn_click" class="btn btn-green btn-block">No</button>
                                   </div>
                              </div>
                         </div>
                     </form>
               </div>
          </div>
     </div>
</div>
<div class="agenda_content agenda-user-content">
     <div class="row row-box">  
          <div class="user_msg_profile clearfix box-effect" style="text-align: center;">
               <div class="desc">
                    <h4><?php echo ucfirst($user_url[0]["Firstname"] . ' ' . $user_url[0]["Lastname"]); ?></h4>
                    <?php if(in_array('52',array_filter(explode(',',$event_templates[0]['checkbox_values'])))) : ?>
                    <p>Gamification Points : <?php echo !empty($gamification_point[0]['gamification_points']) ? $gamification_point[0]['gamification_points'] : '0'; ?></p>
                    <?php endif; ?>
                    <?php if($user_url[0]['Title']!="" && $user_url[0]['Company_name']!=""){ 
                     echo ucfirst($user_url[0]['Title']).' at '.ucfirst($user_url[0]['Company_name']); } ?>
               </div>
               <div class="user_img">
               <?php 
               if ($user_url[0]['Logo'] !='' && !empty(pathinfo($user_url[0]['Logo'], PATHINFO_EXTENSION)))
               {
                    ?>
                    <img align="center" style="float: none;height:100px !important; width:100px !important;border-radius:50%;" src="<?php echo filter_var($user_url[0]['Logo'], FILTER_VALIDATE_URL) ? $user_url[0]['Logo'] : base_url().'assets/user_files/'.$user_url[0]['Logo']; ?>" alt="" />
                    <?php
               }
               else
               {
                    ?>
				<?php $color = sprintf("#%06x",rand(0,16777215)); ?>
				<span style="margin-bottom:8px;text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($user_url[0]['Firstname'], 0, 1)).''.ucfirst(substr($user_url[0]['Lastname'], 0, 1)); ?></span>
                <!--<img align="center" style="float: none;height:100px !important; width:100px !important;border-radius:50%;" src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
               <?php } ?>
               </div>
               <?php if($user[0]->Id==$this->uri->segment(5)){ ?>
               <div class="col-sm-12"> 
                    <a id="check_metting_btn" href="<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>" class="btn btn-green user_btn">See My Meetings</a>
               </div>
               <?php }else{ if($user[0]->Rid=='4' && $event_templates[0]['attendee_hide_request_meeting']=='1'){ ?>
               <button id="more_info_user" class="btn btn-green user_btn" data-toggle="modal" data-target="#request_meeting_popup" style="<?=($user_url[0]['message_permisson'] == '0')?'display: none' : '';?>">Request Meeting</button>
               <?php } } ?>  
               <?php if (!empty($social_url)) { ?>
               <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6" style="width: 100%;<?=($user_url[0]['message_permisson'] == '0')?'display: none' : '';?>">
                         <div class="social-icons">
                              <ul class="navbar-right" style="float:none !important;">
                                   <?php if (!empty($social_url[0]["Website_url"])) { ?>
                                   <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="top">
                                        <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                                             Visit my Website
                                        </a>
                                   </li>
                                   <?php } if (!empty($social_url[0]["Twitter_url"])) { ?>
                                   <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                                        <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                                             Follow me on Twitter
                                        </a>
                                   </li>
                                   <?php } if (!empty($social_url[0]["Facebook_url"])) { ?>
                                   <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                                        <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                                             Follow me on Facebook
                                        </a>
                                   </li>
                                   <?php } if (!empty($social_url[0]["Linkedin_url"])) { ?>
                                   <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                                        <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                                             Follow me on LinkedIn
                                        </a>
                                   </li> 
                                   <?php } ?>  
                              </ul>
                         </div>
                    </div>                                
               </div> 
               <?php } if(!empty($user[0]->Id)){ if($user[0]->Id==$this->uri->segment(5)){ ?>
               <?php if(count($pending_share_contact) > 0){ ?>
               <div class="row">
                    <div id="pending_share_contact_slider" class="owl-carousel owl-theme">
                    <?php foreach ($pending_share_contact as $key => $value) { ?>
                    <div class="item" style="text-align:center;">
                    <div class="col-sm-3" style="margin: 10px auto;float: none !important;">
                         <a href="javascript:void(0);" onclick='confirm_user_request(this,"<?php echo $value['from_id'] ?>","<?php echo $value['to_id'] ?>");' class="btn btn-green btn-block">Save to Device</a>
                    </div>
                    <div class="col-sm-3" style="margin: 10px auto;float: none !important;">
                         <a href="javascript:void(0);" onclick='reject_user_request(this,"<?php echo $value['from_id'] ?>","<?php echo $value['to_id'] ?>");'   class="btn btn-red btn-block">Reject</a>
                    </div>
                    <p><?php echo "Email: ".$value['Email']; ?></p>
                    <?php if(!empty($value['Mobile'])){ ?>
                    <p><?php echo "Phone No: ".$value['Mobile']; ?></p>
                    <?php } if(!empty($value['country_name'])){ ?>
                    <p><?php echo "Country: ".$value['country_name']; ?></p>
                    <?php } ?>
                    </div>
                    <?php } ?>
                    </div>
               </div> 
               <?php } }else{ if(count($show_share_contact)<=0 && $user[0]->Rid==4){ ?>
               <div class="col-sm-3" style="margin: 10px auto;float: none !important;">
                    <a href="<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/share_contact_info/'.$this->uri->segment(5); ?>" id="share_contact_info"   class="btn btn-green btn-block" style="<?=($user_url[0]['message_permisson'] == '0')?'display: none' : '';?>">Share Contact Information</a>
               </div>
               <?php }else{ if($user[0]->Rid==4 && $show_share_contact[0]['approval_status']=='0'){ ?>
               <div class="col-sm-3" style="margin: 10px auto;float: none !important;">
                    <?php if($this->uri->segment(5)==$show_share_contact[0]['from_id']){ ?>
                    <h4>Request Pending for Approval</h4>
                    <?php }else if($this->uri->segment(5)==$show_share_contact[0]['to_id']){  ?>
                    <button class="btn btn-block btn-success" disabled="disabled">Request Sent</button>
                    <?php } ?>
               </div>
               <?php } } } } ?>
          </div>

          <div class="view_biography user_msg_profile clearfix box-effect" id="view_biography" style="display: none;">
               <h4>Contact information</h4><br>
               <?php echo html_entity_decode($user_url[0]["Speaker_desc"]); ?>
               <?php if (!empty($social_url)) { ?>
                    <div class="row">
                         <div class="col-md-6 col-lg-6 col-sm-6">
                              <div class="social-icons">
                                   <ul class="navbar-right">
                                        <?php if (!empty($social_url[0]["Website_url"])) { ?>
                                        <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                                                  Visit my Website
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Twitter_url"])) { ?>
                                        <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                                                  Follow me on Twitter
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Facebook_url"])) { ?>
                                        <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                                                  Follow me on Facebook
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Linkedin_url"])) { ?>
                                        <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                                                  Follow me on LinkedIn
                                             </a>
                                        </li>  
                                        <?php } ?> 
                                   </ul>
                              </div>
                         </div>                                
                    </div> 
<?php } ?>  
          </div>
          <?php if($user !=''): ?>
            <div class="">
               <?php if($subdomain!="gulfoodGulfood2017" || $user[0]->Rid!='4'){ ?>
               <form id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Speakers/<?php echo $acc_name."/".$subdomain; ?>/upload_imag/<?php echo $sp_id; ?>' style="clear:both">
                    <div class="facebok-container" style="<?=($user_url[0]['message_permisson'] == '0')?'display: none' : '';?>">
                         <div class="facebok-main">
                              <div class="facebok-head">
                                   <ul>
                                        <li class="facebok-head-status">
                                             <span>Send a Message </span>
                                        </li>
                                        <li class="facebok-head-photo"> 
                                             <span> Add Photo </span>
                                             <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/></div>
                                             <div id='imageloadbutton'>
                                                  <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple />
                                             </div>
                                        </li>
                                   </ul>
                              </div>
                              <div class="facebok-middle">
                                   <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text" id="Message" placeholder="Type your message here..." name="Message"></textarea>
                              </div>
                              <div class="fb-upload-img-box">
                                   <ul id='preview'>
                                   </ul>
                                   <!--                                        <ul id="yourimage">
                                                                      </ul>-->
                                   <!--                                        <input type="file" capture="camera" accept="image/*" id="takePictureField">
                                   -->
                                   <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                              </div>
                              <div class="facebok-footer clearfix">
                                   <ul class="footer-left">
                                        <li class="camera-icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                                   </ul>
                                   <ul class="footer-right">
                                        <li class="public-box">
                                             <select name="ispublic">
                                                  <?php if($allow_contact[0]['allow_contact_me']=='1'){ ?>
                                                  <option value="0">Private</option>
                                                  <?php } ?>
                                                  <option value="1">Public</option>
                                             </select>
                                        </li>
                                        <li class="submit-button-box">
                                             <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                                  Send message <i class="fa fa-arrow-circle-right"></i>
                                             </button>
                                        </li>
                                   </ul>
                              </div>
                         </div>    
                    </div>
               </form>     
               <?php }
              
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               ?>
               <div class="col-md-12 col-lg-12 col-sm-12 clearfix" id="preview_msg">

                    <?php
                    if (!empty($user_url[0]["Firstname"]))
                    {
                         $username = $user_url[0]["Firstname"];
                    }
                    else
                    {
                         $username = "User";
                    }

                    echo '<div style="padding-top: 0px;">
             <div id="messages">';
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Messages to ' . $username . '<br/></h3>';
                    foreach ($view_chats1 as $key => $value)
                    {
                         echo "<div class='message_container'>";

                         if ($value['Sender_id'] == $user[0]->Id)
                         {
                              echo "<div class='msg_edit-view-box'>";
                              echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                              echo "</div>";
                              echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                              echo "Delete";
                              echo "</div>";
                              echo "</div>";
                         }

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "")
                         {
                              if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Senderlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                              }
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         if($value['SenderRole']=="Attendee")
                         {
                              $plink=base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['Sender_id'];
                              echo '<a href="'.$plink.'" class="tooltip_data">';
                         }
                         else
                         {
                              echo '<a href="#" class="tooltip_data">';
                         }
                         echo $value['Sendername'];
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              if($value['ReciverRole']=="Attendee")
                              {
                                   $plink=base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['Receiver_id'];
                                   echo '<a href="'.$plink.'" class="tooltip_data">';
                              }
                              else
                              {
                                   echo '<a href="#" class="tooltip_data">';
                              }
                              echo $value['Recivername'];
                              echo '</a>';
                              echo "</div>";
                         }
                         echo "</div>";
                         echo "<div class='msg_date'>";
                         echo timeAgo($value['Time'],$event_templates[0]['Event_show_time_zone']);
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";
                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                              echo "</a>";
                              echo "</div>";
                         }

                         echo "<div class='toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                              <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" .$acc_name."/".$subdomain . "/upload_commentimag/" . $Sid . "'>
                                   <div class='comment_message_img'>";
                         if ($user[0]->Logo!="")
                         {
                              if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $user[0]->Logo . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>
                         <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                   <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                   <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                   </ul>
                                   <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                   <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                                        
                              </form>
                              <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              $i = 0;
                              $flag = false;
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }

                                   echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                   if ($cval['Logo'] != "")
                                   {
                                        if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                        {
                                             echo "<img src='" .$cval['Logo'] . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/thumbnail/" . $cval['Logo'] . "'>";
                                        }
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                   }

//                                        echo "</div>
//                                        <div class='comment_text'>
//                                             " . $cval['comment'] . "
//                                        </div></div>";

                                   echo "</div>
                                   <div class='comment_wrapper'>        
                                   <div class='comment_username'>
                                        " . $cval['user_name'] . "
                                   </div>
                                   <div class='comment_text'>
                                        " . $cval['comment'] . "
                                   </div>
                                   <div class='comment_text' style='float:right;padding-left:13px;'>";
                                   ?>
                                   <?php timeAgo($cval['Time'],$event_templates[0]['Event_show_time_zone']); ?>
                                   <?php
                                   echo"</div>
                                   </div>";

                                   if ($cval['image'] != "")
                                   {
                                        $image_comment = json_decode($cval['image']);
                                        echo "<div class='msg_photo'>";
                                        echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                        echo "<img src='" . base_url() . "assets/user_files/" . $image_comment[0] . "'>";
                                        echo "</a>";
                                        echo "</div>";
                                   }
                                   if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                   {
                                        echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                   }
                                   echo "</div>";


                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }

                                   $i++;
                              }
                         }
                         echo "</div>
                         </div>";
                         echo "</div>";
                    }
                    echo "</div></div>";
                    ?>
               </div>
               <?php
                    if (count($view_chats1) > 4)
                    {
                         ?>
                         <div id="loadmore" class="load_btn">
                              <a href="javascript: void(0)" onclick="loadmore(10,0);" class="loadmore panel-green"> 
                                   Load More
                              </a>
                         </div>
                         <?php
                    }
               ?>
          </div>
          <?php else:?>
         <div class="errorHandler alert alert-info center" style="display: block;margin: 0 10%;">
                <i class="fa fa-remove-sign"></i> 
                 Login or Sign Up to proceed. To sign up or login tap the Sign Up button on the top right of the screen.
             </div>
         
          <?php endif;?>  
     </div>
</div>

<script type="text/javascript">
     $('#request_meeting_btn_form_submitting').click(function(){
          $('#request_meeting_btn_form_submitting').prop("disabled", true);
          $.ajax({
               url:"<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/request_meeting/'.$this->uri->segment(5); ?>",
               type:'post',
               data:"date="+$('#date_drop_down').val()+"&time="+$('#time_drop_down').val()+"&location="+$('#metting_location').val()+"&clash_check=1",
               success:function(data){
                    var result=data.split('###');
                    if($.trim(result[0])=="Info")
                    {
                      $('#msg_span_text_show').html(result[1]);
                      $('#form_date_time_request_meeeting_form').hide();
                      $('#form_date_time_request_meeeting_form_submittng_msg').show();
                    }
                    else if($.trim(result[0])=="Error")
                    {
                         $('#request_meeting_error_msg_show').html(result[1]);
                    }
                    else
                    {
                      $('#request_meeting_popup').modal('hide');
                    }
               }
          });
     });
     $('#yes_meeting_btn_click').click(function(){
          $('#yes_meeting_btn_click').prop("disabled", true);
          $('#no_metting_btn_click').prop("disabled", true);
          $.ajax({
               url:"<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/request_meeting/'.$this->uri->segment(5); ?>",
               type:'post',
               data:"date="+$('#date_drop_down').val()+"&time="+$('#time_drop_down').val()+"&location="+$('#metting_location').val()+"&clash_check=0",
               success:function(data){
                    var result=data.split('###');
                    if($.trim(result[0])=="Info")
                    {
                         $('#msg_span_text_show').html(result[1]);
                         $('#form_date_time_request_meeeting_form').hide();
                         $('#form_date_time_request_meeeting_form_submittng_msg').show();
                    }
                    else if($.trim(result[0])=="Error")
                    {
                         $('#request_meeting_error_msg_show').html(result[1]);
                         $('#form_date_time_request_meeeting_form').show();
                         $('#form_date_time_request_meeeting_form_submittng_msg').hide();
                    }
                    else
                    {
                         $('#request_meeting_popup').modal('hide');
                    }
               }
          });
     });
     $('#no_metting_btn_click').click(function(){
          $('#msg_span_text_show').html('');
          $('#form_date_time_request_meeeting_form').show();
          $('#form_date_time_request_meeeting_form_submittng_msg').hide();
          $('#request_meeting_btn_form_submitting').prop("disabled", false);
          $('#yes_meeting_btn_click').prop("disabled", false);
          $('#no_metting_btn_click').prop("disabled", false);
     });
     function sendmessage()
     {
         
          jQuery("#sendbtn").html('Submitting <i class="fa fa-refresh fa-spin"></i>');
          var str = jQuery("#imageform").serialize();
          var values = jQuery("input[name='unpublished_photo[]']")

          var flag=false;
          var flag1=false;
          
          if(jQuery("#Message").val() != "")
          {
               flag=true;
          }
          //alert(values.val());
          if(values.val() !=undefined)
          {
               console.log(values.val());
               flag1=true;
          }
          
          if ((flag==false && flag1==false))
          {
               alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               jQuery("#sendbtn").removeAttr('disabled');
               return false;
          }

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$subdomain; ?>/chatsviewdata/<?php echo $Sid; ?>",
               data: str,
               type: "POST",
               async: true,
               success: function(result)
               {
                    var loadfun = "loadmore(10,0);";
                    jQuery("#loadmore").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                    jQuery("#loadmore").css('display', 'block');
                    jQuery('#Message').val('');
                    jQuery("#preview_msg").html();
                    jQuery("#preview_msg").html(result);
                    jQuery("#preview").html('');
                    jQuery(".addmore_photo").css('display', 'none');

//                              jQuery(".comment_btn").on('click',function(){
//                                   var element = $(this);
//                                   var I = element.attr("id");
//
//                                   jQuery("#slidepanel" + I).slideToggle(300);
//                                   jQuery("#slidepanel" + I).css('overflow','hidden');
//                                   jQuery(this).toggleClass("active");
//
//                                   return false;
//                                });  

                    jQuery(".msg_edit-arrow").on('click', function() {
                         var id = jQuery(this).attr('data-id');
                         jQuery("#msg_edit-view" + id).slideToggle("slow");
                    });
                    jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               }

          });
     }
</script>
<?php
function timeAgo($time_ago,$timezone)
{

     date_default_timezone_set("UTC");
     if(!empty($timezone))
     {
        if(strpos($timezone,"-")==true)
        { 
          $arr=explode("-",$timezone);
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $time_ago = strtotime(date('Y-m-d H:i:s',strtotime($time_ago)-$intNew));
        }
        if(strpos($timezone,"+")==true)
        {
          $arr=explode("+",$timezone);
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $time_ago = strtotime(date('Y-m-d H:i:s',strtotime($time_ago)+$intNew));
        }
    }
     $cur_time = time();
     $time_elapsed = $cur_time - $time_ago;

     $seconds = $time_elapsed;
     $minutes = round($time_elapsed / 60);
     $hours = round($time_elapsed / 3600);
     $days = round($time_elapsed / 86400);
     $weeks = round($time_elapsed / 604800);
     $months = round($time_elapsed / 2600640);
     $years = round($time_elapsed / 31207680);
     // Seconds
     if ($seconds <= 60)
     {
          echo "$seconds seconds ago";
     }
     //Minutes
     else if ($minutes <= 60)
     {
          if ($minutes == 1)
          {
               echo "one minute ago";
          }
          else
          {
               echo "$minutes minutes ago";
          }
     }
     //Hours
     else if ($hours <= 24)
     {
          if ($hours == 1)
          {
               echo "an hour ago";
          }
          else
          {
               echo "$hours hours ago";
          }
     }
     //Days
     else if ($days <= 7)
     {
          if ($days == 1)
          {
               echo "yesterday";
          }
          else
          {
               echo "$days days ago";
          }
     }
     //Weeks
     else if ($weeks <= 4.3)
     {
          if ($weeks == 1)
          {
               echo "a week ago";
          }
          else
          {
               echo "$weeks weeks ago";
          }
     }
     //Months
     else if ($months <= 12)
     {
          if ($months == 1)
          {
               echo "a month ago";
          }
          else
          {
               echo "$months months ago";
          }
     }
     //Years
     else
     {
          if ($years == 1)
          {
               echo "one year ago";
          }
          else
          {
               echo "$years years ago";
          }
     }
}
?>

<script type="text/javascript">
     jQuery.noConflict();
     jQuery(document).ready(function() {

          jQuery(document).on('mouseover', '.msg_photo', function() {
               var my_class = $(this).find('a').attr('class');
               $("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
          });
          
          jQuery(document).on('mouseover', '.msg_fromname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'), maxHeight: '95%',width:"50%"});
          });
          
          jQuery(document).on('mouseover', '.msg_toname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'),maxHeight: '95%',width:"50%"});
          });

          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("#photoimg").removeAttr('multiple');
          }
          else
          {
               jQuery("#photoimg").attr('multiple');
          }

          jQuery("#btn_biography").click(function() {
               jQuery("#view_biography").toggle("slow");
          });


          jQuery(".msg_edit-arrow").on('click', function() {
               var id = jQuery(this).attr('data-id');
               jQuery("#msg_edit-view" + id).slideToggle("slow");
          });

     });
     var desiredWidth;
     jQuery(document).ready(function() {
          console.log('onReady');
          jQuery("#takePictureField").on("change", gotPic);
          jQuery("#preview").load(getSwatches);
          desiredWidth = window.innerWidth;
          if (!("url" in window) && ("webkitURL" in window)) {
               window.URL = window.webkitURL;
          }
     });
     function getSwatches() {
          var colorArr = createPalette(jQuery("#preview"), 5);
          for (var i = 0; i < Math.min(5, colorArr.length); i++) {
               jQuery("#swatch" + i).css("background-color", "rgb(" + colorArr[i][0] + "," + colorArr[i][1] + "," + colorArr[i][2] + ")");
               console.log(jQuery("#swatch" + i).css("background-color"));
          }
     }

     //Credit: https://www.youtube.com/watch?v=EPYnGFEcis4&feature=youtube_gdata_player
     function gotPic(event) {
          if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {
               jQuery("#preview").append("<li><div><input type='hidden' name='unpublished_photo[]' value='" + event.target.files[0] + "'><img src='" + URL.createObjectURL(event.target.files[0]) + "' class='imgList'><button class='photo_remove_btn' type='button' title='' id='" + event.target.files[0].name + "'>&nbsp;</button></div></li>");
               jQuery(".photo_remove_btn").click(function()
               {
                    jQuery(".addmore_photo").css("display", "none");
                    jQuery(this).parent().parent().remove();
                    jQuery("#preview li").each(function() {
                         jQuery(".addmore_photo").css("display", "block");
                    });
               });
               //jQuery("#preview").attr("src",URL.createObjectURL(event.target.files[0]));
          }
     }
     
     function loadmore(str, istype)
     {
          
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name.'/'.$subdomain; ?>/loadmore/" + (str - 5) + "/" + str + "/" + istype + "/"+ <?php echo $Sid; ?>,
               type: "POST",
               async: true,
               success: function(result)
               {
                    if (result != "")
                    {
                         jQuery('#Message').val('');

                         jQuery("#preview_msg").append(result);
                        
                         var a = parseInt(str) + 5;
                         var loadfun = "loadmore(" + a + "," + istype + ");";
                         jQuery("#loadmore").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                         jQuery("#preview").html('');
                         jQuery(".addmore_photo").css('display', 'none');

                         jQuery(".msg_edit-arrow").on('click', function()
                         {
                              var id = jQuery(this).attr('data-id');
                              jQuery("#msg_edit-view" + id).slideToggle("slow");
                         });
                    }
                    else
                    {
                         jQuery("#loadmore").css('display', 'none');
                    }
               }

          });
     }

     function addcomment(id)
     {
          var str = jQuery("#commentform" + id).serialize();
          var values = jQuery("input[name='cmphto[]']");
          
          if (jQuery.trim(jQuery("#comment_text" + id).val()) != "" || values.val() != "")
          {
               jQuery.ajax({
                    url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/commentadd/<?php echo $Sid; ?>",
                                        data: str,
                                        type: "POST",
                                        async: true,
                                        success: function(result)
                                        {
                                             jQuery("#comment_conten" + id).html(result);
                                             jQuery("#comment_text" + id).val('');
                                             jQuery("#cpreview" + id).html('');
                                             jQuery('#camera_icon_comment').show();
                                        }

                                   });
          }
          else
          {
               alert("Please write something or photo to add comment.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               return false;
          }
          return false;
     }

     function viewmore_comment(id)
     {
          jQuery("#comment_conten" + id + " div").each(function() {
               jQuery(this).removeClass('comment_msg_hide');
          });
          jQuery("#comment_viewmore" + id).remove();
     }

     function removemsg(id, e)
     {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/delete_message/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().parent().remove();
               }

          });
     }

     function removecomment(id, e)
     {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/delete_comment/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().remove();
               }

          });
     }

     function comment_photo(id)
     {
          jQuery("#commentform" + id).ajaxForm({target: '#cpreview' + id,
               beforeSubmit: function() 
               {

                    jQuery('#camera_icon_comment').hide();
                    jQuery("#imageloadstatus").show();
                    jQuery("#imageloadbutton").hide();
               },
               success: function() 
               {
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();
                    jQuery(".comment_section_btn").click(function()
                    {
                         jQuery(".addmore_photo").css("display", "none");
                         jQuery(this).parent().remove();
                         jQuery("#preview li").each(function() {
                              jQuery(".addmore_photo").css("display", "block");
                         });
                         jQuery('#camera_icon_comment').show();
                    });
               },
               error: function() 
               {
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();

               }}).submit();

          return false;

     }
     function confirm_user_request(e,from_id,to_id)
     {
          jQuery(e).html('Saving...');
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Attendee/<?php echo $acc_name."/".$Subdomain; ?>/confirm_user_request/<?php echo $this->uri->segment(5); ?>",
               data:"from_id="+from_id+"&to_id="+to_id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    var data=result.split('###');
                    if($.trim(data[0])=="success")
                    {
                         jQuery(e).parent().parent().remove();
                         if(parseInt($.trim(data[1]))==0)
                         {
                              jQuery("#pending_share_contact_slider").remove();
                         }
                         else
                         {
                              var owl=jQuery("#pending_share_contact_slider");
                              owl.owlCarousel();
                              owl.trigger('owl.next');
                              //owl.trigger('next.owl.carousel');
                         }
                    }
               }

          });
     }
     function reject_user_request(e,from_id,to_id)
     {
          jQuery(e).html('Rejecting...');
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Attendee/<?php echo $acc_name."/".$Subdomain; ?>/reject_user_request/<?php echo $this->uri->segment(5); ?>",
               data:"from_id="+from_id+"&to_id="+to_id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    var data=result.split('###');
                    if($.trim(data[0])=="success")
                    {
                         jQuery(e).parent().parent().remove();
                         if(parseInt($.trim(data[1]))==0)
                         {
                              jQuery("#pending_share_contact_slider").remove();
                         }
                         else
                         {
                              var owl=jQuery("#pending_share_contact_slider");
                              owl.owlCarousel();
                              owl.trigger('owl.next');
                         }
                    }
                    
               }

          });
     }
</script>
<script type="text/javascript">
 $(document).ready(function() {
  jQuery("#pending_share_contact_slider").owlCarousel({
     items : 1,
     autoplay: true,
     autoHeight:true,
     navigation : false,
     center: true,
     loop:true
  }); 
});
function hidetimeslot(elem)
{
     if($.trim($(elem).val())=="2017-11-08")
     {
          $('.datetime_range_hide').hide();
     }
     else 
     {
          $('.datetime_range_hide').show();
     }
}
</script>

