<?php $user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/checkin_view.css?<?php echo time(); ?>">
<div class="agenda_content speakers-content">
    <h2 style="text-align: center;">Check In Portal</h2>
    <div>
        <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
    </div>
	<div class="panel panel-white panel-white-new">
		<div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
			<div class="a-to-z-list pull-right">
				<ul class="timeline-scrubber inner-element timeline-scrubber-right">
					<?php
                    $keys = array_keys($attendees);
                    $a=range("A","Z");
                    foreach ($a as $char) 
                    { 
                        if(!empty($attendees))
                        {
                            echo '<li class="clearfix">'; ?>
                            <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'" href="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                            <?php   echo'</li>';
                        }
                    } ?>
				</ul>
			</div>
			<div class="speakers-left-bot user_container_data">
				<?php if(!empty($attendees)): ?>
				<?php foreach ($attendees as $key1 => $value1) { ?>
					<div class="date_separator panel-heading date_separator-new" id="<?php echo ucfirst($key1); ?>" data-appear-top-offset="-400">
                        <i class="fa fa-user"></i><?php echo ' '.$key1; ?>
                    </div>
                    <div class="ps-container ps-container-new" id="<?php echo ucfirst($key1); ?>">
                    	<ul class="activities columns columns-new">
                    	<?php foreach ($value1 as $key => $value) { ?>
                    		<li>
                    			<a class="activity" id="a<?php echo $value['Id']; ?>">
                                    <?php if(!empty($value['Logo'])) { ?>
                                    <img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value['Logo'];?>" alt="" />
                                    <?php } else { ?>
									<?php $color = sprintf("#%06x",rand(0,16777215)); ?>
									<span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($value['Firstname'], 0, 1)).''.ucfirst(substr($value['Lastname'], 0, 1)); ?></span>
                                    <!--<img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
                                    <?php } ?>
                                    <div class="desc">
                                        <h4 class="user_container_name"><?php echo ucwords($value['Firstname'].' '.$value['Lastname']);?></h4>
                                        <?php if($value['Title']!="" && $value['Company_name']!=""){ ?>
                                        <small><?php echo ucfirst($value['Title']).' at '.ucfirst($value['Company_name']); ?></small>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-4 pull-right" style="margin-top:15px;">
	                                    <div class="col-sm-6">
	                                    	<button onclick='user_checkin("<?php echo $value['Id'] ?>")' id="check_in_user_<?php echo $value['Id'] ?>" <?php if($value['check_in_attendee']=='1'){  ?> class="btn btn-success btn-block" <?php }else{ ?> class="btn btn-green btn-block" <?php } ?>><?php if($value['check_in_attendee']=='1'){  ?>Check Out<?php }else{ ?>Check In<?php } ?></button>
	                                    </div>
	                                    <div class="col-sm-6">
	                                    	<button onclick='get_user_information("<?php echo $value['Id']; ?>");' id="more_info_user_<?php echo $value['Id'] ?>" class="btn btn-yellow btn-block" data-toggle="modal" data-target="#attendee_infrormation_popup">More Info</button>
	                                    </div>
                                    </div>
                                </a>
                    		</li>
                    	<?php } ?>	
                    	</ul>
                    </div>
				<?php } else: ?>
					<div class="tab-content">
                        <span>No User available for this event.</span>
                    </div>
				<?php endif; ?>
			</div>
		</div>
		<?php foreach ($attendees as $key1 => $value1) { ?>
	    <?php foreach ($value1 as $key => $value) { ?>
	        <div class="speakers-content-bot" id="div<?php echo $value['Id']; ?>">
	        </div>
	    <?php } } ?>
	</div>
</div>
<div id="attendee_infrormation_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Attendee Information</h4>
            </div>
            <div class="modal-body">
                <form id="attendee_user_validation_form" onsubmit="return valid_attendee_form();" action="<?php echo base_url().'Checkinportal/'.$acc_name.'/'.$Subdomain.'/Save_Attendee_informaion'; ?>" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail">
                                    <img alt="" src="" id="text_user_logo">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail">
                                </div>
                                <div>
                                    <span class="btn btn-light-grey btn-file">
                                        <span class="fileupload-new">
                                            <i class="fa fa-picture-o"></i> Select image
                                        </span>
                                        <span class="fileupload-exists">
                                            <i class="fa fa-picture-o"></i> Change
                                        </span>
                                        <input type="file" name="attendee_profile_photos" id="attendee_profile_photos">
                                    </span>
                                    <a data-dismiss="fileupload" class="btn fileupload-exists btn-light-grey" href="#">
                                      <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label class="control-label">
                                First Name <span class="symbol required"></span>
                            </label>
                            <input type="text" placeholder="First Name" class="form-control required" id="text_Firstname" name="Firstname" value="">
                            <input type="hidden" id="user_id_text_in_popup" name="user_id" value="">
                            <span for="attendee_Firstname" class="help-block" id="attendee_Firstname_error" style="display:none;">This field is required.</span>
                        </div> 
                        <div class="col-sm-6">
                            <label class="control-label">Last Name <span class="symbol required"></span></label>
                            <input type="text" placeholder="Last Name" class="form-control required" id="text_Lastname" name="Lastname" value="">
                            <span for="attendee_Lastname" class="help-block" id="attendee_Lastname_error" style="display:none;">This field is required.</span>
                        </div>   
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Company Name</label>
                            <input type="text" placeholder="Company Name" class="form-control" id="text_company_name" name="attendee_company_name" value="">
                        </div>
                    </div>
                    <?php foreach ($keysdata as $key => $value) { ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label"><?php echo $value['column_name']; ?></label>
                            <input type="text" name="<?php echo $value['column_id']; ?>" id="text_<?php echo $value['column_id']; ?>" class="form-control" placeholder="">
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-green btn-block">Update <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>    
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(function () {
   $("li.clearfix:first-child").addClass("selected");
   $(".clearfix").click(function(){
       $("li.clearfix.selected").removeClass("selected");
       $(this).addClass("selected");
    });
});
</script>
<?php foreach ($attendees as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>
<script type="text/javascript">
$( "#a<?php echo $value['Id']; ?>" ).click(function() {
    $('#div<?php echo $value['Id']; ?>').css('display','block');
    $('#div<?php echo $value['Id']; ?>').css('background','white');
    $('#test').css('display','none');
});
</script>
<?php } } ?>
<script>
jQuery(document).ready(function(){
    jQuery(".user_container_data li").each(function(index) 
    {
        $(this).css('display','inline-block');
        $(this).parent().css('display','block');
    });
});
jQuery("#search_user_content").keyup(function()
{    
    jQuery(".user_container_data li").each(function( index ) 
    {
        var str=jQuery(this).find('.user_container_name').html();
        if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
        {    
            var str1=jQuery(this).find('.user_container_name').html();
            if(str1!=undefined)
            {
                var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                var content=jQuery("#search_user_content").val().toLowerCase();
             	if(content!=null)
            	{          
                    jQuery(this).parent().parent().parent().find(".date_separator").each(function(index){
                        var content1=jQuery(this).text().toLowerCase();
                        var n1 = str1.indexOf(content1);
                    });          
                    var n = str1.indexOf(content);          
                    if(n!="-1")
                    {
                        jQuery(this).css('display','inline-block');
                        //jQuery(this).parent().css('display','block');
                    }
                    else
                    {
                    	jQuery(this).css('display','none');
                    	//jQuery(this).parent().css('display','none');
                    }
                }
            }
        }
    });  
});
function user_checkin(uid)
{
    $.ajax({
        url:"<?php echo base_url().'Checkinportal/'.$acc_name.'/'.$Subdomain.'/user_check_in/'; ?>"+uid,
        success:function(result){
            var data=result.split('###');
            if($.trim(data[0])=="success")
            {
                $('#check_in_user_'+uid).attr('class',data[1]);
                $('#check_in_user_'+uid).html(data[2]);
            }
        }
    });
}
function get_user_information(uid)
{
    $.ajax({
        url:"<?php echo base_url().'Checkinportal/'.$acc_name.'/'.$Subdomain.'/get_attendee_user_info/'; ?>"+uid,
        success:function(result){
            var result= jQuery.parseJSON(result);
            $.each(result, function( index, value ) {
                if(index=="user_logo")
                {
                    $('#text_'+index).attr('src',value);
                }
                else
                {
                    $('#text_'+index).val(value);
                }
            });
            $('#user_id_text_in_popup').val(uid);
        }
    });
}
function valid_attendee_form()
{
    var fnm=$('#text_Firstname').val();
    var lnm=$('#text_Lastname').val();
    if(fnm=="" || lnm=="")
    {
        if(fnm=="")
        {
            $('#attendee_Firstname_error').parent().addClass('has-error');
            $('#attendee_Firstname_error').show();
        }
        else
        {
            $('#attendee_Firstname_error').parent().removeClass('has-error');
            $('#attendee_Firstname_error').hide();
        }
        if(lnm=="")
        {
            $('#attendee_Lastname_error').parent().addClass('has-error');
            $('#attendee_Lastname_error').show();
        }
        else
        {
            $('#attendee_Lastname_error').parent().removeClass('has-error');
            $('#attendee_Lastname_error').hide();
        }
        return false;
    }
    else
    {
        return true;
    }
    return false;
}
</script>
