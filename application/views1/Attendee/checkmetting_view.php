<?php $user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/checkin_view.css?<?php echo time(); ?>">
<div class="agenda_content speakers-content">
    <div>
        <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
    </div>
	<div class="panel panel-white panel-white-new">
		<div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
			<div class="a-to-z-list pull-right">
				<ul class="timeline-scrubber inner-element timeline-scrubber-right">
					<?php
                    $keys = array_keys($attendees_metting);
                    $a=range("A","Z");
                    foreach ($a as $char) 
                    { 
                        if(!empty($attendees_metting))
                        {
                            echo '<li class="clearfix">'; ?>
                            <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'" href="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                            <?php   echo'</li>';
                        }
                    } ?>
				</ul>
			</div>
			<div class="speakers-left-bot user_container_data">
				<?php if(!empty($attendees_metting)): ?>
				<?php foreach ($attendees_metting as $key1 => $value1) { ?>
					<div class="date_separator panel-heading date_separator-new" id="<?php echo ucfirst($key1); ?>" data-appear-top-offset="-400">
                        <i class="fa fa-user"></i><?php echo ' '.$key1; ?>
                    </div>
                    <div class="ps-container ps-container-new" id="<?php echo ucfirst($key1); ?>">
                    	<ul class="activities columns columns-new">
                    	<?php foreach ($value1 as $key => $value) { ?>
                    		<li>
                    			<a class="activity" id="a<?php echo $value['metting_id']; ?>">
                                    <?php if(!empty($value['Logo'])) { ?>
                                    <img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value['Logo'];?>" alt="" />
                                    <?php } else { ?>
									<?php $color = sprintf("#%06x",rand(0,16777215)); ?>
									<span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($value['Firstname'], 0, 1)).''.ucfirst(substr($value['Lastname'], 0, 1)); ?></span>
                                    <?php } ?>
                                    <div class="desc">
                                        <h4 class="user_container_name" id="user_container_name_<?php echo $value['metting_id']; ?>"><?php echo ucwords($value['name']);?></h4>
                                        <span><?php if($event_templates[0]['date_format']=='1')
                                                  {
                                                    echo date("H:i -  m/d/Y",strtotime($value['date'].' '.$value['time']));
                                                  }
                                                  else
                                                  {
                                                    echo date("H:i -  d/m/Y",strtotime($value['date'].' '.$value['time']));
                                                  }
                                        //echo date('H:i - m/d/Y',strtotime($value['date'].' '.$value['time'])); ?></span>
                                        <p id="show_comments_tag" <?php if($value['status']=='0'){ ?> style="display: none;" <?php } ?>><?php echo $value['location']; ?></p>
                                    </div>
                                    <?php if($value['status']=='0'){ if($value['recever_attendee_id']==$user[0]->Id){ ?>
                                    <div id="pending_status_btn_div_<?php echo $value['metting_id'] ?>" class="col-sm-4 pull-right" style="margin-top:15px;">
	                                    <div class="col-sm-6">
	                                    	<button class="btn btn-success btn-block" onclick='changemettingstatus("<?php echo $value['metting_id']; ?>","1");'>Accept</button>
	                                    </div>
	                                    <div class="col-sm-6">
	                                    	<button class="btn btn-red btn-block" onclick='changemettingstatus("<?php echo $value['metting_id']; ?>","2");'>Reject</button>
	                                    </div>
                                        <!-- <div class="col-sm-6">
                                            <button class="btn btn-green btn-block" onclick='suggest_new_time("<?php echo $value['metting_id']; ?>");' data-toggle="modal" data-target="#request_meeting_popup">Suggest New Time</button>
                                        </div> -->
                                    </div>
                                    <?php }else { ?>
                                    <div id="pending_status_btn_div_<?php echo $value['metting_id'] ?>" class="col-sm-4 pull-right" style="margin-top:15px;">
                                        <div class="col-sm-6">
                                            <button class="btn btn-info btn-block">Pending</button>
                                        </div>
                                    </div>    
                                    <?php } } ?>
                                    <div id="Accepted_btn_confirm_status_<?php echo $value['metting_id'] ?>" class="col-sm-4 pull-right" <?php if($value['status']=='1'){ ?> style="margin-top:15px;" <?php }else{ ?> style="margin-top:15px;display: none;" <?php } ?>>
                                        <div class="col-sm-6">
                                            <button class="btn btn-success btn-block" disabled="disabled">Accepted</button>
                                        </div>
                                    </div>
                                </a>
                    		</li>
                    	<?php } ?>	
                    	</ul>
                    </div>
				<?php } else: ?>
					<div class="tab-content">
                        <span>No Meetings have been booked.</span>
                    </div>
				<?php endif; ?>
			</div>
		</div>
		<?php foreach ($attendees as $key1 => $value1) { ?>
	    <?php foreach ($value1 as $key => $value) { ?>
	        <div class="speakers-content-bot" id="div<?php echo $value['metting_id']; ?>">
	        </div>
	    <?php } } ?>
	</div>
</div>
<div id="request_meeting_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span>Suggested Times</span>
            </div>
            <?php  
            function getDatesFromRange($start, $end, $format = 'Y-m-d') 
            {
                $array = array();
                $interval = new DateInterval('P1D');

                $realEnd = new DateTime($end);
                $realEnd->add($interval);
                
                $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
                foreach($period as $date) 
                { 
                    $array[] = $date->format($format); 
                }
                return $array;
            }
            if($event_templates[0]['Id']=='479')
            {
                $dates = getDatesFromRange(date('Y-m-d',strtotime('2017-11-07')),date('Y-m-d',strtotime('2017-11-08')));
            }
            else
            {
                $dates = getDatesFromRange($event_templates[0]['Start_date'],$event_templates[0]['End_date']); 
            }
            ?>
            <div class="modal-body">
                <form id="Suggested_user_validation_form" action="<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/suggest_new_datetime'; ?>" method="post">
                    <div class="row" id="form_date_time_Suggested_form">
                        <div class="form-group"> 
                            <input type="hidden" value="" name="meeting_id_textbox" id="meeting_id_textbox">
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down1" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down1">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {  
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date("H:i",$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down2" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down2">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {  
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date("H:i",$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down3" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down3">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {  
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date("H:i",$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" id="send_new_suggested_times" class="btn btn-green btn-block">Send new Suggested Times</button>
                            </div>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="reject_messages_popup" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="reject_msg_popup_heding">Meeting Reject Message</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="hidden" name="meeting_id" id="meeting_id" value="">
            <div class="form-group"> 
                <div class="col-sm-12">
                    <textarea style="width: 100%;height: 100px;" name="meeting_reject_reason" id="meeting_reject_reason"></textarea>
                </div>
            </div>
            <div class="form-group"> 
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <a href="javascript:void(0);" onclick="rejectmetting();" class="btn btn-green btn-block">Skip</a>
                    </div>
                    <div class="col-sm-6">
                        <a href="javascript:void(0);" onclick="rejectmetting();" class="btn btn-green btn-block">Send</a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(function () {
   $("li.clearfix:first-child").addClass("selected");
   $(".clearfix").click(function(){
       $("li.clearfix.selected").removeClass("selected");
       $(this).addClass("selected");
    });
});
</script>
<?php foreach ($attendees as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>
<script type="text/javascript">
$( "#a<?php echo $value['metting_id']; ?>" ).click(function() {
    $('#div<?php echo $value['metting_id']; ?>').css('display','block');
    $('#div<?php echo $value['metting_id']; ?>').css('background','white');
    $('#test').css('display','none');
});
</script>
<?php } } ?>
<script>
jQuery(document).ready(function(){
    jQuery(".user_container_data li").each(function(index) 
    {
        $(this).css('display','inline-block');
        $(this).parent().css('display','block');
    });
});
jQuery("#search_user_content").keyup(function()
{    
    jQuery(".user_container_data li").each(function( index ) 
    {
        var str=jQuery(this).find('.user_container_name').html();
        if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
        {    
            var str1=jQuery(this).find('.user_container_name').html();
            if(str1!=undefined)
            {
                var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                var content=jQuery("#search_user_content").val().toLowerCase();
             	if(content!=null)
            	{          
                    jQuery(this).parent().parent().parent().find(".date_separator").each(function(index){
                        var content1=jQuery(this).text().toLowerCase();
                        var n1 = str1.indexOf(content1);
                    });          
                    var n = str1.indexOf(content);          
                    if(n!="-1")
                    {
                        jQuery(this).css('display','inline-block');
                        jQuery(this).parent().css('display','block');
                    }
                    else
                    {
                    	jQuery(this).css('display','none');
                    	jQuery(this).parent().css('display','none');
                    }
                }
            }
        }
    });  
});
function changemettingstatus(mid,status)
{
    if(status=='2')
    {
        $('#meeting_id').val(mid);
        $('#meeting_status').val(status);
        $('#reject_msg_popup_heding').html("Send "+$('#user_container_name_'+mid).html()+" a rejection message");
        $('#reject_messages_popup').modal('show');
    }
    else
    {
        $.ajax({
            url:"<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/changemettingstatus/'; ?>"+mid,
            type:'post',
            data:'status='+status,
            success:function(data){
                var result=data.split('###');
                if($.trim(result[0])=="Success")
                {
                    $('#show_comments_tag').show();
                    if(status=='1'){
                        $('#pending_status_btn_div_'+mid).remove();
                        $('#Accepted_btn_confirm_status_'+mid).show();
                    }
                    else
                    {
                        $('#pending_status_btn_div_'+mid).parent().parent('li').remove();
                    }
                }
            },
        });
    }
}
function rejectmetting()
{
    var mid=$('#meeting_id').val();
    $.ajax({
        url:"<?php echo base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/changemettingstatus/'; ?>"+mid,
        type:'post',
        data:'status=2'+'&reject_msg='+$('#meeting_reject_reason').val(),
        success:function(data){
            var result=data.split('###');
            if($.trim(result[0])=="Success")
            {
                $('#show_comments_tag').show();
                $('#pending_status_btn_div_'+mid).parent().parent('li').remove();
                $('#meeting_reject_reason').val('');
                $('#reject_messages_popup').modal('hide');
            }
        },
    });
}
function suggest_new_time(mid)
{
    $('#meeting_id_textbox').val(mid);
}
</script>
