<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.question_sottable {
    list-style: none;
    padding: 0;
    display: inline-block;
    max-height: auto;
    overflow-y: auto;
    overflow-x: hidden;
    width: 100%;
    float: left;
    margin: 5px;
}
.question_sottable .placeholder {
    background: #007CBA;
    border: 1px dashed red;
    margin: 10px 0px;
}
.question_sottable .ui-sortable-handle {
    -ms-touch-action: none;
    touch-action: none;
    list-style: none;
    font-size: 15px;
    background: #eaeaea;
    border: 1px solid #d4d4d4;
    margin: 0 0 7px 0;
    padding: 7px 9px;
}
.question_sottable li {
    background: white;
    border: 1px solid gray;
    margin: 5px 0px;
}  
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <p>
                    <h4 class="panel-title">Stages</h4>
                </p>
                <div class="panel-body">
                    <form action="" method="post">
                        <ul id="sortable" class="question_sottable">
                            <?php foreach ($stages as $key => $value) { ?>
                            <li>
                                <input type="hidden" value="<?php echo $value['id']; ?>" name="stage_id[]">
                                <?php echo $value['stage_name']; ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="col-sm-3" style="padding: 0px;">
                            <button type="submit" class="btn btn-green btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
    $( "#sortable" ).sortable({
        axis: "y",
        containment: "parent",
        placeholder: "placeholder",
        forcePlaceholderSize: true,
        cursor: "move",
        items: "li",
        tolerance: "pointer",
    });
    $( "#sortable" ).disableSelection();
});
</script>

