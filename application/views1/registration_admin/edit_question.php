<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#surveylist" data-toggle="tab">
                        Edit Question
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="surveylist">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Question <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Question" id="Question" name="Question" class="form-control name_group required" value="<?=$question['question']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Stage 
                                </label>
                                <div class="col-sm-9">
                                    <select name="stage_id" class="name_group">
                                        <option value="">Select Stage</option>
                                        <?php foreach ($stages as $key => $value) : ?>
                                            <option value="<?=$value['id']?>" <?=($question['stage_id'] == $value['id']) ? 'selected' : ''?> ><?=$value['stage_name']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Question Type <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <select onchange="closediv()" id="Question_type" name="question_type" class="name_group required">
                                        <option value="">Select Question Type</option>
                                        <option value="1" <?=($question['type'] == '1') ? 'selected' : ''?> >Multiple Choice (One Answer)</option>
                                        <option value="2" <?=($question['type'] == '2') ? 'selected' : ''?> >Multiple Choice (More than one Answer)</option>
                                        <option value="3" <?=($question['type'] == '3') ? 'selected' : ''?> >Comment Box</option>
                                        <option value="4" <?=($question['type'] == '4') ? 'selected' : ''?> >Product Addition</option>
                                        <option value="5" <?=($question['type'] == '5') ? 'selected' : ''?> >Agenda Selection</option>
                                        <option value="6" <?=($question['type'] == '6') ? 'selected' : ''?> >Date Selection</option>
                                    </select>
                                    <br>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                 Required 
                                </label>
                                <div class="col-sm-9">
                                   <input type="checkbox" name="isrequired" style="margin: 10px;"<?=($question['isrequired'] == '1')? 'checked' : ''?>>
                                </div>
                            </div>
                            <?php if($event['Id'] == '259'): ?>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                 Update alert 
                                </label>
                                <div class="col-sm-9">
                                   <input type="checkbox" name="update_alert" style="margin: 10px;"<?=($question['update_alert'] == '1')? 'checked' : ''?>>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div id="ticket" class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                 Select Ticket 
                                </label>
                                <div class="col-sm-9">
                                    <select  id="t-id" name="t-id" class="name_group required">
                                    <option value="">Select Ticket</option>
                                <?php foreach ($tickets as $key => $value) : ?>
                                        <option value="<?=$value['id']?>" <?=($question['ticket_id'] == $value['id']) ? 'selected' : ''?> ><?=$value['ticket_name']?></option>
                                <?php endforeach;?>
                                </select>
                                </div>
                            </div>
                            <div id="Question_option">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Add More 
                                    </label>
                                    <div class="col-sm-9">
                                        <a class="btn btn-blue" href="javascript: void(0);" onclick="addmoreoption()" ><i class="fa fa-plus fa fa-white"></i></a>
                                    </div>
                                </div>
                                <div id="option_container">
                                    <?php $i=0; foreach($question['option'] as $value) :?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Option <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" style="margin-bottom:15px;" placeholder="Option" id="Option" name="Option[]" class="form-control required name_group" value="<?=$value?>">
                                            <input type="hidden" name="older_option[<?php echo $i; ?>]" value="<?php echo $value; ?>">
                                            <a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a>
                                        </div>
                                    </div>
                                    <?php $i++; endforeach;?>
                                </div>
                            </div>
                            <div id="date_selection" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Earliest Date <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" name="date[]" id="e_date" contenteditable="false" class="form-control" value="<?=($question['type'] == '6') ? $question['option'][0] : ''?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Latest Date <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" name="date[]" id="l_date" contenteditable="false" class="form-control" value="<?=($question['type'] == '6') ? $question['option'][1] : ''?>">
                                    </div>
                                </div>
                            </div>
                            <div id="product_info" style="<?=!empty($question['product_name']) ? '':'display: none'?>">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Prodcut Name <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="Prodcut Name" id="product_name" name="product_name" class="form-control name_group required" value="<?=$question['product_name']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Prodcut Price <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="Prodcut Price" id="product_price" name="product_price" class="form-control name_group required" value="<?=$question['product_price']?>">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   closediv();
      $('#e_date').datepicker({
        weekStart: 1,
        startDate: '-0d',
        autoclose: true,
        });
    $('#l_date').datepicker({
        weekStart: 1,
        startDate: '-0d',
        autoclose: true
        });
});
var count=parseInt("<?php echo count($question['option']); ?>");
function addmoreoption()
{
  count++;
  var id="Option"+count;
  var cnm=count-1;
  var name="Option["+cnm+"]";
  var html='<div class="form-group"><label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Option <span class="symbol required"></span></label><div class="col-sm-9"><input type="text" style="margin-bottom:15px;" placeholder="Option" id="'+id+'" name="'+name+'" class="form-control required name_group"><a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a></div></div>';
  jQuery("#option_container").append(html);
}
function removeoption(e)
{
  if(jQuery("#option_container div.form-group").length>1)
  {
    jQuery(e).parent().parent().remove();
  }
}
/*function closediv()
{ 
  if($("#Question_type").val()=='1' || $("#Question_type").val()=='2')  
  {
    $("#Question_option").show();
    $("#option_container #Option").addClass('required');
    $("#product_info").hide();
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
  }
  else if($("#Question_type").val() == '3')
  {
    $("#Question_option").hide();
    $("#option_container #Option").removeClass('required');
    $("#product_info").hide();
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
  }
  else if($("#Question_type").val() == '4')
  { 
    $("#Question_option").hide();
    $("#option_container #Option").removeClass('required');
    $("#product_info").show();
    $("#product_price").addClass('required');
    $("#product_name").addClass('required');
  }
  else
  {
    $("#Question_option").hide();
    $("#option_container #Option").removeClass('required');
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
    $("#product_info").hide();
  }
}*/
function closediv()
{
  if($("#Question_type").val()!='1' && $("#Question_type").val()!='2' && $("#Question_type").val()!='4' && $("#Question_type").val()!='5' && $("#Question_type").val()!='6')
  {
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $("#product_info").hide();
    $("#ticket").hide();
    $("#t-id").removeClass('required');
    $("#date_selection").hide();
    $("#e_date").removeClass('required');
    $("#l_date").removeClass('required');
  }
  else if($("#Question_type").val() == '4')
  { 
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $("#ticket").hide();
    $("#product_info").show();
    $("#product_price").addClass('required');
    $("#product_name").addClass('required');
    $("#t-id").removeClass('required');
    $("#date_selection").hide();
    $("#e_date").removeClass('required');
    $("#l_date").removeClass('required');

  }
  else if($("#Question_type").val() == '5')
  { 
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
    $("#product_info").hide();
    $("#t-id").addClass('required');
    $("#ticket").show();
    $("#date_selection").hide();
    $("#e_date").removeClass('required');
    $("#l_date").removeClass('required');
  }
  else if($("#Question_type").val() == '6')
  { 
    $("#date_selection").show();
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $("#ticket").hide();
    $("#t-id").removeClass('required');
    $("#product_info").hide();
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
    $("#e_date").addClass('required');
    $("#l_date").addClass('required');
  }
  else
  {
    $("#Question_option").show();
    $("#option_container #Option").addClass('required');
    $("#option_container #Option").addClass('name_group');
    $("#product_price").removeClass('required');
    $("#product_name").removeClass('required');
    $("#product_info").hide();
    $("#ticket").hide();
    $("#t-id").removeClass('required');
    $("#date_selection").hide();
    $("#e_date").removeClass('required');
    $("#l_date").removeClass('required');

  }
}
</script>