<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#screen_content').summernote({
        height:300,
    });
    closediv();
    send_email_data();
});
</script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#reg_page" data-toggle="tab">
                                Registration Page
                            </a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Stages_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                                <span id="Stages_textshow"> Stages & Questions </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Stages_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <li class="">
                                    <a href="#stages" data-toggle="tab">
                                        Stages
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#questions" data-toggle="tab">
                                        Questions
                                    </a>
                                </li>
                            </ul> 
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Payment_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                                <span id="Payment_textshow"> Payments & Discount </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Payment_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <li class="">
                                    <a href="#payment" data-toggle="tab">
                                        Payments & Currency
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#discount" data-toggle="tab">
                                        Discount Codes
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#badge" data-toggle="tab">
                                Badge Design
                            </a>
                        </li>
                        <li class="">
                            <a id="" href="#ticket" data-toggle="tab">
                                Ticket Types
                            </a>
                        </li>
                        <li class="">
                            <a id="" href="#thank_you_screen" data-toggle="tab">
                                Thank you Screen
                            </a>
                        </li> 
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                Preview
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="reg_page">
                        <div class="row padding-15">
                        <?php if ($this->session->flashdata('attendee_view_data')) { ?>
                            <div class="errorHandler alert alert-success no-display" style="display: block;">
                                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('attendee_view_data'); ?>
                            </div>
                        <?php } ?>
                            <h3 style="margin-top: -20px">Design your Registration Screen to appear how you would like using the tools below</h3>
                            <br>
                            <form action="" role="form" method="post" class="form-horizontal" id="form" novalidate enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Banner Images 
                                    </label>
                                    <div class="modal fade" id="companybannermodelscopetool" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Company Banner Image</h4>
                                            </div>
                                            <div class="modal-body">
                                                <section>
                                                    <div class="demo-wrap" style="display:none;">
                                                        <div class="container">
                                                            <div class="grid">
                                                                <div class="col-1-2">
                                                                    <div id="vanilla-demo"></div>
                                                                </div>
                                                                <div class="col-1-2">
                                                                    <strong>Vanilla Example</strong>
                                                                    <div class="actions">
                                                                        <button class="vanilla-result">Result</button>
                                                                        <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                                        <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="preview_company_banner_crop_tool"></div>
                                                        <div class="col-sm-3 crop-btn">
                                                            <button class="btn btn-green btn-block" id="company_banner_upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                                        </div>
                                                    </div>         
                                                </section>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                                            <div class="fileupload-new thumbnail" id="uploded_files_preview">
                                                <?php if (!empty($screen_data['banner_image'])) { ?>
                                                <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $screen_data['banner_image']; ?>" alt="">
                                                <?php } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div"></div>
                                            <?php if (!empty($screen_data['banner_image'])) { ?>
                                            <div class="user-edit-image-buttons">
                                                 <a href="javascript:void(0);" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_banner_image();">
                                                <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div> <?php } ?>
                                            <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                    <span class="fileupload-new">
                                                        <i class="fa fa-picture"></i> Select image
                                                    </span>
                                                    <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                    <input type="file" id="banner_Images" name="banner_Images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Background Images 
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" id="exisitionimagepreview">
                                                <?php if (!empty($screen_data['background_image'])) { ?>
                                                <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $screen_data['background_image']; ?>" alt="">
                                                <?php } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" id="backgroungimagepreview"></div>
                                            <?php if (!empty($screen_data['background_image'])) { ?>
                                            <div class="user-edit-image-buttons">
                                                <a href="javascript:void(0);" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_back_image();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div> <?php } ?>
                                            <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                    <span class="fileupload-new">
                                                        <i class="fa fa-picture"></i> Select image
                                                    </span>
                                                    <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                    <input type="file" name="background_image">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                        </div>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Description
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea id="screen_content" name="screen_content"><?php echo $screen_data['screen_content']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Theme Color
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?=$screen_data['theme_color']?>" placeholder="Footer background color" id="theme_color" name="theme_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Add QR Code for Badges
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" id="send_pdf" name="send_pdf" value="1" <?php if($screen_data['send_pdf']=='1'){ ?> checked="checked" <?php } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Send Qr code
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" id="send_qr_code" name="send_qr_code" value="1" <?php if($screen_data['send_qr_code']=='1'){ ?> checked="checked" <?php } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Send Notification Email when a new user registers
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="email_notify_admin" name="email_notify_admin" value="1" <?php if($screen_data['email_notify_admin']=='1'){ ?> checked="checked" <?php } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Show Menu in Frontend
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="show_in_front" name="show_in_front" value="1" <?php if($screen_data['show_in_front']=='1'){ ?> checked="checked" <?php } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Select atlest one agenda from each date
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="one_agenda_each_date" name="one_agenda_each_date" value="1" <?php if($screen_data['one_agenda_each_date']=='1'){ ?> checked="checked" <?php } ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-2">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                            Save <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="questions">
                        <div class="row padding-15">
                            <a style="top: 7px;margin-top: -10px;" class="btn btn-primary list_page_btn survey_btn" href="<?php echo base_url(); ?>Registration_admin/add_question/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add new Question</a>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">#</th>
                                            <th>Question</th>
                                            <th>Type</th>
                                            <th>Stage</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;foreach($questions as $key => $value) :?>
                                            <tr>
                                                <td><?=$i?></td>
                                                <td><a href="<?=base_url()?>Registration_admin/edit_question/<?=$value['event_id'].'/'.$value['id']?>" data-original-title="Edit"><?=$value['question']?></a></td>
                                                <td><?=$value['type']?></td>
                                                <td><?=$value['stage_name']?></td>
                                                <td>
                                                    <a href="<?=base_url()?>Registration_admin/edit_question/<?=$value['event_id'].'/'.$value['id']?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="javascript:;" onclick="delete_question(<?=$value['id']?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                                </td>
                                            </tr>
                                        <?php $i++;endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="stages">
                        <div class="row padding-15">
                            <a style="top: 7px;margin-top: -10px;" class="btn btn-primary list_page_btn survey_btn" data-toggle="modal" data-target="#add_stage"><i class="fa fa-plus"></i> Add new Stage</a>
                            <a style="top: 7px;margin-top: -10px;margin-right: 141px;" class="btn btn-primary list_page_btn survey_btn" href="<?php echo base_url(); ?>Registration_admin/reorder_stage/<?php echo $this->uri->segment(3); ?>"> Reorder Stage</a>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th width: 10px;>#</th>
                                            <th>Stage Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($stages as $key => $value) : ?>
                                        <tr>
                                            <td><?=$i?></td> 
                                            <td><a data-original-title="update" data-placement="top" href="<?=base_url().'Registration_admin/edit_stage/'.$this->uri->segment(3).'/'.$value['id']; ?>"><?=$value['stage_name']?></a></td>
                                            <td>
                                            <a href="<?=base_url().'Registration_admin/edit_stage/'.$this->uri->segment(3).'/'.$value['id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_stage("<?=$value['id']?>");' href="javascript:;"><i class="fa fa-times fa fa-white"></i></td></td>
                                        </tr>
                                    <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="payment">
                        <div class="row padding-15">
                            <form class="" id="payment_processor_form" name="payment_processor" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>Registration_admin/save_payment_processor/<?php echo $event_id; ?>">
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> You Want Payment Screen <span class="symbol required"></span> 
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" name="show_payment_screen" onchange="showpaymentoption()" id="show_payment_screen" value="1" <?=($screen_data['show_payment_screen'] == '1') ? 'checked' : ''?>>
                                        </div>
                                    </div> 
                                </div>
                                <div id="payment_screen_option">
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Payment Type <span class="symbol required"></span> </label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="payment_type" id="payment_type" onchange="closediv();">
                                                    <option value="">Select Payment Type</option>
                                                    <option value="0" <?=($screen_data['payment_type'] == '0') ? 'selected' : ''; ?>>Paypal</option>
                                                    <option value="1" <?=($screen_data['payment_type'] == '1') ? 'selected' : ''; ?>>Authorize.net</option>
                                                    <option value="2" <?=($screen_data['payment_type'] == '2') ? 'selected' : ''; ?>>Stripe</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                    <div id="paypal_feilds">
                                        <div class="col-sm-12" style="margin-bottom:10px;"> 
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Paypal API Username<span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="paypal_api_username" class="form-control required" value="<?=$screen_data['paypal_api_username']?>" id="paypal_api_username" placeholder="Please Enter Paypal API Username">
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom:10px;"> 
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Paypal API Password<span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="paypal_api_password" class="form-control required" value="<?=$screen_data['paypal_api_password']?>" id="paypal_api_password" placeholder="Please Enter Paypal API Password">
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom:10px;"> 
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Paypal API Signature<span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="paypal_api_signature" class="form-control required" value="<?=$screen_data['paypal_api_signature']?>" id="paypal_api_signature" placeholder="Please Enter Paypal API Signature">
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div id="authorise_feilds">
                                        <div class="col-sm-12" style="margin-bottom:10px;">   
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Transaction Api<span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="authorise_transaction_key" class="form-control required" value="<?=$screen_data['authorise_transaction_key']?>" id="authorise_transaction_key" placeholder="Please Enter Secret Key">
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom:10px;">   
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Api Name<span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="authorise_api_name" class="form-control required" value="<?=$screen_data['authorise_api_name']?>" id="authorise_api_name" placeholder="Please Enter Public Key">
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div id="strip_fields">
                                        <div class="col-sm-12" style="margin-bottom:10px;">   
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Secret Key <span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="stripe_secret_key" class="form-control required" value="<?=$screen_data['stripe_secret_key']?>" id="stripe_secret_key" placeholder="Please Enter Secret Key">
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom:10px;">   
                                            <div class="form-group">
                                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Public Key <span class="symbol required"></span> </label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="stripe_public_key" class="form-control required" value="<?=$screen_data['stripe_public_key']?>" id="stripe_public_key" placeholder="Please Enter Public Key">
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Payment Currency 
                                            </label>
                                            <div class="col-sm-10">
                                                <label class="radio">
                                                    <input type="radio" name="stripe_payment_currency" value="GBP" <?php if($screen_data['stripe_payment_currency']=='GBP'){ ?> checked="checked" <?php } ?>>
                                                    <span style="margin-left: 2%;"> <i class="fa fa-gbp"></i> GBP </span>
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="stripe_payment_currency" <?php if($screen_data['stripe_payment_currency']=='USD'){ ?> checked="checked" <?php } ?> value="USD">
                                                    <span style="margin-left: 2%;"> <i class="fa fa-usd"></i> USD </span>
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="stripe_payment_currency" <?php if($screen_data['stripe_payment_currency']=='AED'){ ?> checked="checked" <?php } ?> value="AED">
                                                    <span style="margin-left: 2%;"> <i class="fa fa-aed">د.إ</i> AED </span>
                                                </label>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Allow Invoice Payment <span class="symbol required"></span> </label>
                                            <div class="col-sm-8">
                                                <input type="checkbox" name="show_pay_by_invoice" <?=($screen_data['show_pay_by_invoice'] == '1') ? 'checked' : ''?>>
                                            </div>
                                        </div> 
                                    </div>
                                </div>    
                                <div class="col-sm-12">
                                    <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"></label>
                                    <div class="col-sm-3">
                                        <input  type="submit" class="btn btn-theme_green btn-block" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="discount">
                        <div class="row padding-15">
                         <form role="form" method="post" class="form-horizontal discount-form" id="discount-form" action="<?=base_url().'Registration_admin/add_discount/'.$this->uri->segment(3).'/'.$this->uri->segment(4)?>" enctype="multipart/form-data">
                            <div class="errorHandler alert alert-danger no-display" id="duplicate_error_msg_div" style="display: none;">
                                <i class="fa fa-remove-sign"></i> Coupan Code should be Unique
                            </div>
                            <div class="col-sm-12">
                                    <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-sm-3">
                                        
                                    </div>
                                    <div class="col-sm-1">
                                        
                                    </div>
                                    <div class="col-sm-3">
                                        Code
                                    </div>
                                    <div class="col-sm-3">
                                        New Price
                                    </div>
                                  </div>
                            <?php foreach($questions as $key => $value):?>
                            <?php if($value['type'] == '4'): $pr_ids[] = '#'.$value['id'].'__code';?>
                                <input type="hidden" id="product_ids" name="product_ids[]" value="<?=$value['id']?>">
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-sm-3">
                                        <?=$value['question']?>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="<?=$value['id']?>__status" <?=($value['status'] == '1') ? 'checked' : ''?> >
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" id="<?=$value['id']?>__code" name="<?=$value['id']?>__code" class="form-control codetext" value='<?=$value['code']?>' onkeyup="compare_code('<?='#'.$value['id']?>__code')">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="<?=$value['id']?>__price" class="form-control" value='<?=$value['price']?>' onkeypress="return isNumber(event)">
                                    </div>
                                </div>
                            <?php endif; 
                                  endforeach; $pr_ids[] = '#all__code';?>
                                <hr style="height: 5px; background-color: gray;">
                                  <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-sm-3">
                                        
                                    </div>
                                    <div class="col-sm-1">
                                        
                                    </div>
                                    <div class="col-sm-3">
                                        Code
                                    </div>
                                    <div class="col-sm-3">
                                        Precentage Discount
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-3">
                                        All
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="all__status" <?=($all_disc['status'] == 1) ? 'checked' : ''?>>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="all__code" class="form-control codetext" value="<?=$all_disc['code']?>" id="all__code" onkeyup="compare_code('#all__code')">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="all__percentage" class="form-control" value="<?=$all_disc['percentage']?>" onkeypress="return isNumber(event)">
                                    </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-sm-3">
                                          <button class="btn btn-theme_green btn-block" id="save_coupn">Submit</button>
                                          </form>
                                      </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="badge">
                        <div class="row padding-15">
                            <form class="" id="badges_form" name="badges_form" enctype="multipart/form-data" method="POST" action="<?=base_url()?>Registration_admin/save_badges_vales/<?=$this->uri->segment(3)?>">
                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-2" for="form-field-1">
                                            Badges Logo Images
                                        </label>
                                        <div class="col-sm-9">
                                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                            <div class="fileupload-preview fileupload-exists thumbnail" id="backgroungimageuploadpreview">
                                            </div>
                                                <?php if (!empty($badges_data['badges_logo_images'])) { ?>
                                                <div class="fileupload-new thumbnail" id="backgroungimageuploadpreview">
                                                <img src="<?php echo base_url(); ?>assets/badges_files/<?php echo $badges_data['badges_logo_images']; ?>" alt="">
                                                </div>
                                                <?php } ?>
                                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                    <input type="file" name="badges_logo_images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Send Email <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" name="send_email" <?=($badges_data['send_email'] == '1') ? 'checked': '' ?> id="send_email" onchange="send_email_data()">
                                        </div>
                                    </div> 
                                </div>
                                <div id="send_email_data">
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Sender Name <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sender_name" class="form-control required" value="<?=$badges_data['sender_name']?>" id="sender_name" placeholder="Please Enter Email Sender Name">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Subject <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="subject" class="form-control required" value="<?=$badges_data['subject']; ?>" id="subject" placeholder="Please Enter Email Subject">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Email Body <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <textarea type="text" name="email_body" class="form-control summernote" id="email_body" placeholder="Please Enter Email Content"><?=$badges_data['email_body']; ?></textarea>
                                        </div>
                                    </div> 
                                </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Send Code
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" id="code_type_0" <?php if($badges_data['code_type']!='1'){ ?> checked="checked" <?php } ?> name="code_type" value="0"> 
                                                <span>QR Code</span>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" <?php if($badges_data['code_type']=='1'){ ?> checked="checked" <?php } ?> id="code_type_1" name="code_type" value="1"> 
                                                <span>BarCode</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3" style="margin-top:15px;margin-left: 193px;">
                                        <input  type="submit" class="btn btn-theme_green btn-block" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="ticket">
                        <div class="row padding-15">
                            <a style="top: 7px;margin-top: -10px;" class="btn btn-primary list_page_btn survey_btn" data-toggle="modal" data-target="#add_ticket"><i class="fa fa-plus"></i> Add new Ticket</a>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th width: 10px;>#</th>
                                            <th>Ticket</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach ($tickets as $key => $value) : ?>
                                        <tr>
                                            <td><?=$i?></td> 
                                            <td><a data-original-title="update" data-placement="top" href="<?=base_url().'Registration_admin/edit_stage/'.$this->uri->segment(3).'/'.$value['id']; ?>"><?=$value['ticket_name']?></a></td>
                                            <td>
                                            <a data-toggle="modal" class="label btn-blue tooltips edit_ticket" data-placement="top" data-original-title="Edit" data-ticket_name="<?=$value['ticket_name']?>" data-id="<?=$value['id']?>"><i class="fa fa-edit"></i></a>
                                            <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_ticket("<?=$value['id']?>");' href="javascript:;"><i class="fa fa-times fa fa-white"></i></td></td>
                                        </tr>
                                    <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="thank_you_screen">
                        <div class="row padding-15">
                            <form class="" id="badges_form" name="badges_form" enctype="multipart/form-data" method="POST" action="<?=base_url()?>Registration_admin/save_thank_you_screen/<?=$this->uri->segment(3)?>">
                                <div class="form-group">
                                    <label class="col-sm-12" for="form-field-1"> 
                                        Screen Content 
                                    </label>
                                    <div class="col-sm-12">
                                        <textarea type="text" name="thank_you_content" class="form-control summernote" id="thank_you_content"><?=$screen_data['thank_you_content']; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-top: 3%;">
                                    <button style="width: 10%;margin: 0 auto;" class="btn btn-green btn-block">Save</button>
                                </div>
                            </form>
                        </div>    
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe1" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain'];?>/attendee_registration_screen"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain'];?>/attendee_registration_screen"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add_stage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
          <div class="modal-header bluebackgroupstyle">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Add Stage</h4>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Registration_admin/add_stage/<?php echo $this->uri->segment(3); ?>" id="form2">
              <div class="form-group">
                    <input type="text" name="stage_name" class="form-control required" placeholder="Enter Stage Name">
              </div>
                <button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Save</button>
              </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
          <div class="modal-header bluebackgroupstyle">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Add Ticket</h4>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Registration_admin/add_ticket/<?php echo $this->uri->segment(3); ?>" id="ticket_form">
              <div class="form-group">
                    <input type="text" name="ticket_name" class="form-control required" placeholder="Enter Ticket">
              </div>
                <button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Save</button>
              </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
          <div class="modal-header bluebackgroupstyle">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Update Ticket</h4>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Registration_admin/update_ticket/<?php echo $this->uri->segment(3); ?>" id="ticket_form">
              <div class="form-group">
                    <input type="hidden" name="t-id" id="t-id">
                    <input type="text" name="ticket_name" class="form-control required" placeholder="Enter Ticket" id='t-name'>
              </div>
                <button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Save</button>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function delete_banner_image()
{
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url().'Registration_admin/delete_register_banner_image/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'; ?>";
    }
}
function delete_back_image()
{
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url().'Registration_admin/delete_register_back_image/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'; ?>";
    }
}
function delete_stage(vid)
{
    <?php $event_id = $this->uri->segment(3); ?>
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url(); ?>Registration_admin/delete_stage/"+<?php echo $event_id; ?>+"/"+vid
    }
}
function delete_question(qid)
{
    <?php $event_id = $this->uri->segment(3); ?>
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url(); ?>Registration_admin/delete_question/"+<?php echo $event_id; ?>+"/"+qid
    }   
}
function delete_ticket(vid)
{
    <?php $event_id = $this->uri->segment(3); ?>
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url(); ?>Registration_admin/delete_ticket/"+<?php echo $event_id; ?>+"/"+vid
    }
}
function closediv()
{
    if($("#payment_type").val() == '0')
    {
        $("#authorise_feilds").hide();
        $("#strip_fields").hide();
        $("#stripe_secret_key").removeClass('required');
        $("#stripe_public_key").removeClass('required');
        $("#authorise_api_name").removeClass('required');
        $("#authorise_transaction_key").removeClass('required');
        $('#paypal_feilds').show();
        $('#paypal_api_username').addClass('required');
        $('#paypal_api_password').addClass('required');
        $('#paypal_api_signature').addClass('required');
    }
    else if($("#payment_type").val() == '1')
    { 
        $("#strip_fields").hide();
        $('#paypal_feilds').hide();
        $("#authorise_feilds").show();
        $("#stripe_secret_key").removeClass('required');
        $("#stripe_public_key").removeClass('required');
        $('#paypal_api_username').removeClass('required');
        $('#paypal_api_password').removeClass('required');
        $('#paypal_api_signature').removeClass('required');
        $("#authorise_api_name").addClass('required');
        $("#authorise_transaction_key").addClass('required');
    }
    else if($("#payment_type").val() =='2')
    {
        $('#paypal_feilds').hide();
        $("#authorise_feilds").hide();
        $("#strip_fields").show();
        $("#stripe_secret_key").addClass('required');
        $("#stripe_public_key").addClass('required');
        $('#paypal_api_username').removeClass('required');
        $('#paypal_api_password').removeClass('required');
        $('#paypal_api_signature').removeClass('required');
        $("#authorise_api_name").removeClass('required');
        $("#authorise_transaction_key").removeClass('required');
    }
}

function send_email_data()
{
    if($('#send_email').is(":checked"))   
        $("#send_email_data").show();
    else
        $("#send_email_data").hide();
}
function isNumber(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
$(document).ready(function() {
    showpaymentoption();
    $("#badges_form").validate({
        errorElement: "label",
        errorClass: 'help-block',  
        rules: 
        {
            badges_title:{
                required: true,
                minlength: 6,
            },
            badges_sub_title:{
                required: true,
                minlength: 3,
            },
            badges_date:{
                required: true,
                minlength: 4,
            },
            sender_name:{
                required:true,
            },
            subject:{
                required:true,
            }
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
    $('#payment_processor_form').validate({
        errorElement:"span",
        errorClass: 'help-block',
        rules:{
            stripe_secret_key:{
                required:true,
            },
            stripe_public_key:{
                required:true,
            },
            stripe_payment_price:{
                required:true,
                number:true
            },
            stripe_payment_currency:{
                required:true,
            },
            authorise_transaction_key:{
                required:true,
            },
            authorise_api_name:{
                required:true,
            }
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
    $('#ticket_form').validate({
        errorElement:"span",
        errorClass: 'help-block',
        rules:{
            ticket_name:{
                required:true,
            }
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
    $(".edit_ticket").click(function()
       {
            $("#t-name").val($(this).data('ticket_name'));
            $("#t-id").val($(this).data('id'));
            $('#edit-ticket').modal('show');
       });
}); 
function compare_code(id)
{   
    var tmp = <?php echo json_encode($pr_ids); ?>;
    var vals = {};
    $('#duplicate_error_msg_div').hide();
    $('#save_coupn').prop("disabled", false);
    /*var allvalue=[];
    tmp.forEach(function(value){   
        if($(value).val()!='' && value!=id)
        {
            allvalue.push($(value).val());
        }
    });
    if(jQuery.inArray($(id).val(),allvalue)!== -1)
    {
        $('#duplicate_error_msg_div').hide();
        $('#save_coupn').prop("disabled", false);
        $(id).parent().removeClass('has-error');
    }
    else
    {
        $('#duplicate_error_msg_div').show();
        $('#save_coupn').prop("disabled", true);
        $(id).parent().addClass('has-error');
    }*/
    tmp.forEach(function(value){        
        if($(value).val() != '')
        {
            var index = $(value).val().toLowerCase();
            if (vals[index])
            {
                vals[index]++
            }
            else
            { 
                vals[index] = 1;
            }
            var duplicates = $.map(vals, function(val, key){
                return (val > 1) ? key : null;
            });
            if (duplicates.length > 0) 
            {   
                if(!$(id).parent().hasClass('has-error'))
                {
                    $(id).parent().addClass('has-error');   
                }
                $('#duplicate_error_msg_div').show();
                $('#save_coupn').prop("disabled", true);
                return false;
            }
            else
            {
                $(id).parent().removeClass('has-error');      
            }
        }
    });
}
function findDuplicates() {
    var isDuplicate = false;
    jQuery("input[name^='access_keys']").each(function (i,el1) {
        var current_val = jQuery(el1).val();
        if (current_val != "") {
            jQuery("input[name^='access_keys']").each(function (i,el2) {
                if (jQuery(el2).val() == current_val && jQuery(el1).attr("name") != jQuery(el2).attr("name")) {
                    isDuplicate = true;
                    jQuery(el2).css("background-color", "yellow");
                    jQuery(el1).css("background-color", "yellow");
                    return;
                }
            });
        }
    });
    if (isDuplicate) {
        alert ("Duplicate values found.");
        return false;
    } else {
        return true;
    }
}
$('#Stages_dropdown_link_div li').click(function(){
    $('#Stages_textshow').html($(this).find('a').html());
});  
$('#Payment_dropdown_link_div li').click(function(){
    $('#Payment_textshow').html($(this).find('a').html());
});   
function showpaymentoption(){    
    if($('#show_payment_screen').prop("checked") == true)
    {
        $('#payment_screen_option').fadeIn('slow');
    }
    else
    {
        $('#payment_screen_option').fadeOut('slow');

        $("#stripe_secret_key").removeClass('required');
        $("#stripe_public_key").removeClass('required');
        $('#paypal_api_username').removeClass('required');
        $('#paypal_api_password').removeClass('required');
        $('#paypal_api_signature').removeClass('required');
        $("#authorise_api_name").removeClass('required');
        $("#authorise_transaction_key").removeClass('required');
    }
} 
</script>