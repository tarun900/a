<?php $acc_name=$this->session->userdata('acc_name');
$show_tem=array();
if(in_array(20,json_decode($eventmodule[0]['module_list'],TRUE)))
{
    $show_tem=array('Attendee_Email_Template_1','Attendee_Email_Template_2','Attendee_Email_Template_3','Attendee_Email_Template_4','Attendee_Email_Template_5');
}
else
{
   $show_tem=array('Attendee_Email_Template_1','Attendee_Email_Template_2','Attendee_Email_Template_3','Attendee_Email_Template_4','Attendee_Email_Template_5','You’re in the lead!','Thank you!','Auction Bid','outbid','Congratulations - you won the auction!');
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- start: EXPORT DATA TABLE PANEL  -->
        <div class="panel panel-white">
            <!--<div class="panel-heading">
                <h4 class="panel-title"><span class="text-bold">Email Templates</span></h4>
            </div>-->
            <div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('email_tmpl_data')){ ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('email_tmpl_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#email_templates" data-toggle="tab">
                                Email Templates
                            </a>
                        </li>

                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                        
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="email_templates">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample-table-2">
                                <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($email_template);$i++) {  
                                       if(!in_array($email_template[$i]['Slug'],$show_tem)){  ?>
                                    <tr>
                                        <td><?php echo $email_template[$i]['Subject'];?></td>
                                        <?php  $data = base64_encode($email_template[$i]['Slug']);
                                        $data = str_replace(array('+','/','='),array('-','_',''),$data);
                                        //echo $data."<br>";
                                         ?>
                                        <td><a data-original-title="Edit" data-placement="top" class="btn btn-xs btn-blue tooltips" href="<?php echo base_url();?>Setting/edit/<?php echo $email_template[$i]['event_id'];?>/<?php echo $email_template[$i]['Id'];?>"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: EXPORT DATA TABLE PANEL -->
    </div>
</div>