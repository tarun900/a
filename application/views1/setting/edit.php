<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
                    
			<div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('event_qty')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_qty'); ?> Successfully.
                </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#edit_event" data-toggle="tab">
                                Edit Email Settings
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
						
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="edit_event">

                        <form role="form" method="post" class="form-horizontal" id="mine-form" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                                Subject: <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="Subject" id="Subject" value="<?php echo $email_template_detail['Subject'];?>">
                                        </div>
                                </div>
                                <?php  //if($email_template_detail['Slug']!='Attendee_Email_Template_1' && $email_template_detail['Slug']!='Attendee_Email_Template_2' && $email_template_detail['Slug']!='Attendee_Email_Template_3' && $email_template_detail['Slug']!='Attendee_Email_Template_4' && $email_template_detail['Slug']!='Attendee_Email_Template_5'){ ?>
                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                                From: <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="From" id="From" value="<?php echo $email_template_detail['From'];?>">
                                        </div>
                                </div>
                                <?php //} ?>
                        
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                            Content: <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea name="Content" id="Content" class="summernote form-control" cols="10" rows="10"><?php echo $email_template_detail['Content'];?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                     <button data-toggle="modal" data-target="#add-slides" class="btn btn-yellow btn-block" type="button" onclick="preview();">
                                    Preview <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>  
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<div class='modal fade no-display' id='add-slides' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
            </div>
            <div class='test modal-body' style="padding:0px;" id="co">
            
            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .test
    {
        margin: 0px 15px 0px 15px;
        display: block;
        overflow: hidden;
    }
</style>
    <script type="text/javascript">

    function preview()
    {
         //$('.display_notification').click();
         var Content=$("#Content").code();
         var From=$("#From").val();
         var Subject=$("#Subject").val();
         var data="From: "+From+"<br>Subject: "+Subject+"<br>Content: "+Content;
          $("#co").html(data);
         /*$.ajax({
            url : '<?php echo base_url(); ?>Events/<?php echo $event['Subdomain'];?>/preview',
            data :'Background_color='+Background_color+'&Top_background_color='+Top_background_color+'&Top_text_color='+Top_text_color+'&Footer_background_color='+Footer_background_color+'&Description='+Description+'&Event_name='+Event_name+'&Start_date='+Start_date+'&End_date='+End_date+'&Organisor_id='+Organisor_id+'&Id='+Id+'&Images='+images,
            type: "POST",  
            async: false,
            success : function(data)
            {
                   $("#co").html(data);

            }
        });*/
     
    }
    </script>
<!-- end: PAGE CONTENT-->