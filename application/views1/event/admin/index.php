<div class="row">
    <div class="col-md-12">
        <!-- start: EXPORT DATA TABLE PANEL  -->
        <div class="panel panel-white">
            <!-- <div class="panel-heading">
                <h4 class="panel-title"><span class="text-bold">Events</span></h4>                
                <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>event/add"><i class="fa fa-plus"></i> Add Event</a>
                <div class="panel-tools"></div>
            </div>-->

           <!--  <div class="panel-body">
                <h2 style="text-align:center;">Events Section Comming Soon.</h2>
            </div> -->


            <div class="panel-body">
                <?php if ($this->session->flashdata('event_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_data'); ?> Successfully.
                    </div>
                <?php }
                ?>
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                
                <div class="col-md-7 col-lg-12">
                    <div class="panel panel-dark">
                        <div class="panel-heading">
                            <h4 class="panel-title">Events</h4>
                            <!-- <span class="label pull-right"><a class="panel-title btn btn-warning list_page_btn tooltips" style="padding:0px 12px" href="<?php echo base_url(); ?>event/add" data-placement="top" data-original-title="Add Event"><i class="fa fa-plus"></i> Add Event</a></span> -->

                        </div>
                        <div class="panel-body no-padding">
                            <div class="partition-green padding-15 text-center">
                            </div>
                            <?php foreach ($Event as $key => $eventname) { ?>
                                <div id="<?php echo $eventname['Id']; ?>" class="panel-group accordion accordion-white no-margin">
                                    <div class="panel no-radius">                                   
                                        <div class="panel-heading events">
                                            <h4 class="panel-title eventname">
                                                <a href="#collapseOne<?php echo $eventname['Id']; ?>" data-parent="#<?php echo $eventname['Id']; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15">
                                                    <i class="icon-arrow"></i>
                                                    <?php echo $eventname['Event_name']; ?>
                                                    <span class="label btn-red pull-right margin-left-10 tooltips" onclick="delete_event(<?php echo $eventname['Id']; ?>)" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
                                                    <!-- <span class="label btn-blue pull-right margin-left-10 tooltips" onclick="edit_event(<?php echo $eventname['Id']; ?>)" data-placement="top" data-original-title="Edit Event"><i class="fa fa-edit"></i></span> -->
                                                    <span class="label btn-green pull-right margin-left-10 tooltips" onclick="display_event(<?php echo $eventname['Id']; ?>)" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>

                                                    <!-- <a data-original-title="View Event" id="<?php echo $eventname['Id']; ?>" data-placement="top" class="btn btn-xs btn-green tooltips" href="<?php echo base_url(); ?>event/detail/<?php echo $eventname['Id']; ?>"><i class="fa fa-share"></i></a>
                                                    <a data-original-title="Edit" data-placement="top" class="btn btn-xs btn-blue tooltips" href="<?php echo base_url(); ?>event/edit/<?php echo $eventname['Id']; ?>"><i class="fa fa-edit"></i></a>
                                                    <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips" href="javascript:;" onclick="delete_event(<?php echo $eventname['Id']; ?>)"><i class="fa fa-times fa fa-white"></i></a> -->

                                                </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse collapse" id="collapseOne<?php echo $eventname['Id']; ?>">
                                            <?php foreach ($eventname['Month'] as $mkey => $mvalue) { ?>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title monthname">
                                                        <a href="#<?php echo "collapseOne" . $eventname['Id'] . $mkey; ?>" data-parent="#<?php echo $eventname['Id'] . $mkey; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15 months">
                                                            <i class="icon-arrow"></i>
                                                            <?php echo $mkey; ?> 
                                                        </a></h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="<?php echo "collapseOne" . $eventname['Id'] . $mkey; ?>">
                                                    <div class="panel-body no-padding partition-light-grey">
                                                        <?php foreach ($mvalue['Size'] as $skey => $svalue) { ?>
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title sizename">
                                                                    <a href="#<?php echo "collapseOne" . $eventname['Id'] . $mkey . $skey; ?>" data-parent="#<?php echo "collapseOne" . $eventname['Id'] . $mkey; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15 sizes">
                                                                        <i class="icon-arrow"></i>
                                                                        <?php echo $skey; ?> 
                                                                    </a></h4>
                                                            </div>
                                                            <div class="panel-collapse collapse innercontains" id="<?php echo "collapseOne" . $eventname['Id'] . $mkey . $skey; ?>">
                                                                <div class="panel-body no-padding partition-light-grey">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th class="center">#</th>
                                                                                    <th>Attendee Name</th>
                                                                                    <th class="center">Total Qty.</th>
                                                                                    <th class="center hidden-xs">Allocated Qty.</th>
                                                                                    <th>&nbsp;</th>
                                                                                </tr>
                                                                                <?php
                                                                                for ($i = 0; $i < count($svalue); $i++) {
                                                                                    if (isset($svalue[$i])) {
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td class="center"><?php echo $i + 1; ?></td>
                                                                                            <td><?php echo $svalue[$i]['AttendeeName']; ?></td>
                                                                                            <td class="center">
                                                                                                <?php
                                                                                                    $totalqty = $svalue[$i]['Allocatedqty'] + $svalue[$i]['Remainqty'];
                                                                                                    echo $totalqty;
                                                                                                ?>
                                                                                            </td>
                                                                                            <td class="center hidden-xs"><?php echo $svalue[$i]['Allocatedqty']; ?></td>
                                                                                            <td><i class="fa fa-caret-down <?php if (($totalqty - $svalue[$i]['Allocatedqty']) < 0) { ?> text-red<?php } else { ?> text-green <?php } ?>">&nbsp;<?php echo abs($totalqty - $svalue[$i]['Allocatedqty']); ?></i></td>
                                                                                        </tr>
                                                                                         <?php 
                                                                                    }
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>                                    
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php echo $pagination; ?>
                </div>                

            </div>
        </div>
        <!-- end: EXPORT DATA TABLE PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->

<script>
    function delete_event(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Event/delete/" + id;
        }
    }

    function edit_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/edit/" + id;
    }
    function display_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/detail/" + id;
    }
</script>