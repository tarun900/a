<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Event</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
                    
			<div class="panel-body">
                <?php if($this->session->flashdata('event_qty')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_qty'); ?> Successfully.
                </div>
                <?php } ?>
                <form role="form" method="post" class="form-horizontal" id="mine-form" action="" enctype="multipart/form-data">
                    <?php if($event['Event_name'] != NULL && $event['Event_name'] != '') { ?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
							Event name: <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
                            <?php echo $event['Event_name'];?>
						</div>
					</div>
                    <?php } if($event['Common_name'] != NULL && $event['Common_name'] != '') { ?>
                    <div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
							Common name: <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
                            <?php echo $event['Common_name'];?>
						</div>
					</div>
                    <?php } if($event['Description'] != NULL && $event['Description'] != '') { ?>
                    <div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
							Description: <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
                            <?php echo $event['Description'];?>
						</div>
					</div>
                    <?php } ?>
                    <!-- <div class="form-group months-qty">
						<label class="col-sm-2 control-label" for="form-field-1">&nbsp;</label>
						<?php for($i=date('m');$i<date('m')+5;$i++){ ?>
						<div class="col-sm-2"><?php echo date('M Y',mktime(0, 0, 0, $i, 1)); ?></div>
					   <?php } ?>
					</div> -->
                   <!--  <?php foreach($event['Size'] as $k=>$v){?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                                <b>Size</b> <?php echo $v['Size'];?>:
                        </label>
                        <?php for($i=date('m');$i<date('m')+5;$i++){ ?>
                        <div class="col-sm-2">
                        
                        <?php
                        $month_qty='';
                        foreach($event['Attendee'] as $k1=>$v1)
                        {
                        	if($v1['Id']==$v['Id'] && $v1['Month'] == date('M-Y',mktime(0, 0, 0, $i, 1)))
                        		$month_qty=$v1['Qty'];
                        }
                        ?>               	
                    		<input type="text" placeholder="qty" name="size[qty][<?php echo $k; ?>][]" class="add-qty col-sm-12" value="<?php echo $month_qty;?>">
    						<input type="hidden" name="size[month][<?php echo $k; ?>][]" value="<?php echo date('Y-m-01',mktime(0, 0, 0, $i, 1)); ?>" >
    						<input type="hidden" class="size_id" name="size[id][<?php echo $k; ?>][]" value="<?php echo $v['Id']; ?>">
    					</div>
						<?php } ?>
                    </div>
                        <?php } ?> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->