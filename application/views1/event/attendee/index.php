<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Events <span class="text-bold">List</span></h4>
                <div class="panel-tools"></div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('speaker_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_templates_data'); ?> Successfully.
                </div>
                <?php } ?>
                
        
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        for($i=0;$i<count($Event);$i++) { ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $Event[$i]['Event_name']; ?></td>
                            <td><?php echo $Event[$i]['Start_date']; ?></td>
                            <td><?php echo $Event[$i]['End_date']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->


<script>
    function delete_event(id)
    {
        if(confirm("Are you sure to delete this?"))
        {            
            window.location.href ="<?php echo base_url(); ?>Event/delete/"+id;
        }
    }
    function edit_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/edit/" + id;
    }
    function display_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/detail/" + id;
    }
    function check_edit_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            edit_event($('input[type=checkbox]:checked').val());
        }
    }
    function check_display_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            display_event($('input[type=checkbox]:checked').val());
        }
    }
    function check_delete_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else
        {
            delete_event($('input[type=checkbox]:checked').val());
        }
    }

</script>