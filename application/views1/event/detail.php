<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="col-md-12 col-lg-6 col-sm-12">
                    <div id="notes">
                        <div class="panel panel-note">										
                            <div class="item">
                                <div class="panel-heading">
                                    <h4 class="no-margin" style="text-align:center;">Event Notes</h4>
                                </div>

                                <div class="panel-body space10">
                                    <div class="note-short-content">
                                        <strong>Event Name</strong><br><?php echo $Event[0]['Event_name']; ?> 
                                    </div>
                                    <br>
                                    <div class="note-short-content">
                                        <strong>Description</strong><br>                                           
                                        <?php
                                            if(strlen($Event[0]['Description'])>220)
                                            {
                                                echo substr($Event[0]['Description'], 0, 220).'...';
                                            }
                                            else
                                            {
                                                echo $Event[0]['Description'];
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-5">
                    <div class="panel uploads">
                        <div class="panel-body panel-portfolio no-padding">
                            <div class="portfolio-grid portfolio-hover">
                                <div class="e-slider owl-carousel owl-theme portfolio-grid portfolio-hover" data-plugin-options='{"pagination": false, "stopOnHover": true}'>
                                    <?php
                                    $data=json_decode($Event[0]['Images']);
                                    for($i=0;$i<count($data);$i++)
                                    {
                                    ?>
                                    <div class="item">
                                        <img src="<?php echo base_url(); ?>assets/timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $data[$i]; ?>&h=312&w=312" alt="">                                        
                                    </div> 
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php $img_name = $Event[0]['Images'];
                                  $img_size =  explode(',',$img_name);
                            ?>
                            <?php if(sizeof($img_size) >= 2) { ?>
                                <div class="partition partition-white padding-15">
                                    <div class="navigator">
                                        <a href="#" class="circle-50 partition-white owl-prev"><i class="fa fa-chevron-left text-extra-large"></i></a>
                                        <a href="#" class="circle-50 partition-white owl-next"><i class="fa fa-chevron-right text-extra-large"></i></a>
                                    </div>
                                </div>
                            <?php } else { } ?>
                        </div>
                    </div>
                </div>
                <?php if($this->data['user']->Role_name == 'Administrator') { ?>
                    <!-- Add Accordien -->
                <div class="col-md-7 col-lg-12" style="display:none;">
                    <?php  foreach ($Event as $key=>$eventname) { ?>
                    <div class="panel panel-dark">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $eventname['Event_name']."/".$eventname['Common_name']; ?><span class="label label-danger pull-right"><?php echo $eventname['EventRemaintQty']; ?></span></h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div id="<?php echo $eventname['Id']; ?>" class="panel-group accordion accordion-white no-margin">
                                <div class="panel no-radius">                                   
                                    <?php
                                    foreach ($eventname['Month'] as $mkey=>$mvalue)
                                    {
                                        if($mvalue['MonthTotalRemaintQty']>0)
                                        {
                                    ?>
                                    <div class="panel-heading events">
                                        <h4 class="panel-title eventname">
                                            <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-parent="#<?php echo $eventname['Id']; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15">
                                                <i class="icon-arrow"></i>
                                                <?php echo $mkey; ?> <span class="label label-danger pull-right"><?php echo $mvalue['MonthTotalRemaintQty']; ?></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="<?php echo "collapseOne".$eventname['Id'].$mkey; ?>">
                                        <div class="panel-body no-padding partition-light-grey">
                                            <?php  foreach ($mvalue['Size'] as $skey=>$svalue) {  ?>
                                            <div class="panel-heading">
                                                <h4 class="panel-title sizename">
                                                    <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey.$skey; ?>" data-parent="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15 months">
                                                        <i class="icon-arrow"></i>
                                                        <?php echo $skey; ?> <!--<span class="label label-danger pull-right">3</span>-->
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse innercontains" id="<?php echo "collapseOne".$eventname['Id'].$mkey.$skey; ?>">
                                                <div class="panel-body no-padding partition-light-grey">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <th class="center">#</th>
                                                                <?php if($this->data['user']->Role_name == 'Administrator') {  ?>
                                                                <th>Attendee Name</th>
                                                                <?php  } ?>
                                                                <!-- <th class="center">Quantity</th> -->
                                                            </tr>
                                                            <?php
                                                            for($i=0;$i<count($svalue);$i++)
                                                            {
                                                                if(isset($svalue[$i]))
                                                                { 
                                                                    {
                                                            ?>
                                                            <tr>
                                                                <td class="center"><?php echo $i+1; ?></td>
                                                                <?php  if($this->data['user']->Role_name == 'Administrator')  { ?>
                                                                <td><?php echo $svalue[$i]['AttendeeName']; ?></td>
                                                                <?php  } ?>
                                                            </tr>
                                                            <?php  }  }  } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <?php  } }  ?>     
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>                
                        <!-- End Add Accordien -->
                <?php } else if($this->data['user']->Role_name == 'Attendee') {  ?>
                        <!-- Add Accordien -->
                <div class="col-md-7 col-lg-12" style="display:none;">
                    <?php foreach ($Event as $key=>$eventname) {  ?>
                    <div class="panel panel-dark">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $eventname['Event_name']."/".$eventname['Common_name']; ?><span class="label label-danger pull-right"><?php echo $eventname['EventRemaintQty']; ?></span></h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div id="<?php echo $eventname['Id']; ?>" class="panel-group accordion accordion-white no-margin">
                                <div class="panel no-radius">                                   
                                    <?php
                                        foreach ($eventname['Month'] as $mkey=>$mvalue)
                                        {
                                            if($mvalue['MonthTotalRemaintQty']>0)
                                            {
                                    ?>
                                    <div class="panel-heading events">
                                        <h4 class="panel-title eventname">
                                            <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-parent="#<?php echo $eventname['Id']; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15">
                                                <i class="icon-arrow"></i>
                                                <?php echo $mkey; ?> <span class="label label-danger pull-right"><?php echo $mvalue['MonthTotalRemaintQty']; ?></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="<?php echo "collapseOne".$eventname['Id'].$mkey; ?>">
                                        <div class="panel-body no-padding partition-light-grey">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th class="center">#</th>
                                                        <th>Size</th>
                                                        <th class="center">Quantity</th>
                                                    </tr>
                                                    <?php
                                                    $j=0;
                                                    foreach ($mvalue['Size'] as $skey=>$svalue)
                                                    {
                                                        for($i=0;$i<count($svalue);$i++)
                                                        {
                                                            if(isset($svalue[$i]))
                                                            { 
                                                                if($svalue[$i]['Remainqty']>0)
                                                                {
                                                        ?>
                                                        <tr>
                                                            <td class="center"><?php echo $j+1; ?></td>
                                                            <td><?php echo $skey; ?></td>
                                                            <td class="center"><?php echo $svalue[$i]['Remainqty']; ?></td>
                                                        </tr>
                                                     <?php   } } }
                                                        $j++;
                                                      }
                                                     ?>
                                                </tbody>
                                            </table>                                      
                                        </div>
                                    </div>
                                    <?php  } } ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  }  ?>
                </div>                
                <!-- End Add Accordien -->
                <?php } else { ?>
                <!-- Add Accordien -->
                <div class="col-md-7 col-lg-12" style="display:none;">
                    <?php  foreach ($Event as $key=>$eventname) {  ?>
                    <div class="panel panel-dark">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $eventname['Event_name']."/".$eventname['Common_name']; ?><span class="label label-danger pull-right"><?php echo $eventname['EventRemaintQty']; ?></span></h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div id="<?php echo $eventname['Id']; ?>" class="panel-group accordion accordion-white no-margin">
                                <div class="panel no-radius">                                   
                                    <?php
                                        foreach ($eventname['Month'] as $mkey=>$mvalue)
                                        {
                                            if($mvalue['MonthTotalRemaintQty']>0)
                                            {
                                    ?>
                                <div class="panel-heading events">
                                    <h4 class="panel-title eventname">
                                        <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-parent="#<?php echo $eventname['Id']; ?>" data-toggle="collapse" class="accordion-toggle collapsed padding-15">
                                            <i class="icon-arrow"></i>
                                            <span class="monthdata"><?php echo $mkey; ?></span><span class="label label-danger pull-right"><?php echo $mvalue['MonthTotalRemaintQty']; ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="<?php echo "collapseOne".$eventname['Id'].$mkey; ?>">
                                        <div class="panel-body no-padding partition-light-grey">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th class="center">#</th>
                                                        <th>Size</th>
                                                        <th>Quantity</th>
                                                        <th class="center">Place Order</th>
                                                    </tr>
                                                    <?php
                                                        $j=0;
                                                        foreach ($mvalue['Size'] as $skey=>$svalue)
                                                        {   
                                                            for($i=0;$i<count($svalue);$i++)
                                                            {
                                                                if(isset($svalue[$i]) && $svalue['TotalRemainQty']>0)
                                                                { 
                                                                    if($svalue[$i]['Remainqty']>0)
                                                                    {
                                                            ?>
                                                            <tr>
                                                                <td class="center"><?php echo $j+1; ?></td>
                                                                <td><?php echo $skey; ?></td>
                                                                <td><?php echo $svalue[$i]['Remainqty']; ?><input type="hidden" value="<?php echo $svalue['Plnt_size_id']; ?>"  class="size_id"></td>
                                                                <td class="center"><a href="javascript:;" class="btn btn-primary place_order">Place order</a></td>
                                                            </tr>
                                                    <?php } } }
                                                        $j++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>                               
                                        </div>
                                    </div>
                                    <?php  } } ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  } ?>
                </div>                
                <!-- End Add Accordien -->
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- end: PAGE CONTENT-->
