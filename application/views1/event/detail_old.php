<!-- start: PAGE CONTENT -->
<div class="row">
<div class="col-md-12">
    <div class="panel panel-white">
        <div class="panel-body">
            <div id="owl-demo" class="owl-carousel owl-theme">
                <?php
                    $data=json_decode($Event[0]['Images']);
                    for($i=0;$i<count($data);$i++)
                    {
                ?>
                    <div class="item"><img src="<?php echo base_url(); ?>assets/timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $data[$i]; ?>&h=360&w=960" alt=""></div>
                <?php
                    }
                ?>
            </div>
            <div class="col-md-6">
                <p class="desc_det">
                    <?php
                    echo $Event[0]['Description']; ?>
                </p>
            </div>
            <div class="col-md-6">
                <div class="panel-group accordion" id="accordion">
                    <div class="panel panel-white">
                            <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="font-size: 16px;">
                                            <i class="icon-arrow"></i> <?php echo "Total Quantity: ".$Event[0]['Attendee_Qty_Count']; ?>
                                    </a></h5>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <?php if($this->data['user']->Role_name == 'Administrator'){ ?><th>Attendee</th><?php } ?>
                                                    <th>Size</th>
                                                    <th>Quantity</th>
                                                    <th>Month-Year</th>
                                                    <?php if($this->data['user']->Role_name == 'Client'){ ?><th>Place Order</th><?php } ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if($this->data['user']->Role_name != 'Client')
                                                {                                                
                                                    for($i=0;$i<count($Event[0]['Attendee']);$i++)
                                                    {
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $i+1; ?></td>
                                                            <?php if($this->data['user']->Role_name == 'Administrator'){ ?><td><?php 
                                                            echo $sname=$Event[0]['Attendee'][$i]['Firstname']=="" ? $Event[0]['Attendee'][$i]['Firstname'] : $Event[0]['Attendee'][$i]['Company_name']; 
                                                            ?></td><?php } ?>
                                                            <td><?php echo $Event[0]['Attendee'][$i]['Size']; ?></td>
                                                            <td><?php echo $Event[0]['Attendee'][$i]['Qty']; ?></td>
                                                            <td><?php echo $Event[0]['Attendee'][$i]['Month']; ?></td>                                                            
                                                        </tr>
                                                    <?php
                                                    }
                                                }
                                                else
                                                {
                                                    $i=0;
                                                    foreach ($Event[0]['Attendee'] as $key=>$value)
                                                    {
                                                        $value = array_values($value);
                                                        foreach ($value as $keyd=>$data)
                                                        { 
                                                            ?>
                                                            <tr>
                                                            <td><?php if($keyd==0){echo $i+1;}else{ echo "&nbsp;";} ?></td>
                                                            <td><?php if($keyd==0){echo $key; }else{ echo "&nbsp;";}?><input type="hidden" value="<?php echo $data['plnt_size_id']; ?>"  class="size_id"></td>
                                                            <td><?php echo $data['Qty']; ?></td>
                                                            <td class="month"><?php echo $data['Month']; ?></td>
                                                            <td><a href="javascript:;" class="btn btn-primary place_order">Place order</a></td>
                                                             </tr>
                                                            <?php
                                                        }
                                                        $i++;
                                                    }
                                                   
                                                }
                                                ?>
                                            </tbody>
                                        </table>                                        
                                    </div>
                            </div>
                    </div>
		</div>
                
            </div>
        </div>
    </div>
</div>
</div>
<!-- end: PAGE CONTENT-->
