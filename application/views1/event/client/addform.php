<?php $acc_name=$this->session->userdata('acc_name');?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<div class="row" id="edit">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <div class="panel-body" style="padding: 0px;">
                <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div id="wizard" class="swMain">
                        <ul style="margin-top:15px;">
                            <li>
                                <a href="#step-1">
                                    <div class="stepNumber">
                                        1
                                    </div>
                                    <span class="stepDesc"> 
                                    <small>Define your app</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <div class="stepNumber">
                                        2
                                    </div>
                                    <span class="stepDesc">
                                    <small>Add Your Branding</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <div class="stepNumber">
                                        3
                                    </div>
                                    <span class="stepDesc">
                                    <small>Add Homescreen Text</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <div class="stepNumber">
                                        4
                                    </div>
                                    <span class="stepDesc">
                                    <small>Create your color scheme</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <div class="stepNumber">
                                        5
                                    </div>
                                    <span class="stepDesc"> 
                                    <small>Finish</small> </span>
                                </a>
                            </li>
                        </ul>
                        <div class="progress progress-xs transparent-black no-radius active">
                            <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                <!--<span class="sr-only"> 0% Complete (success) </span>-->
                            </div>
                        </div>
                        <div id="step-1">
                            <h3 class="StepTitle">Define your app</h3>
                            <span class="control-label">Laying the foundations of your app is the most important first step. Follow the instructions below to soft-launch your app so it is ready to be customized and launched!</span>
                            <div style="width:60%;">
                                <h3 class="StepTitle">Create your Unique URL</h3>
                                <span><p>This is the unique extension your app users will use to access your app. Once you have chosen your Unique URL you cannot change it without speaking with the Support Team - so choose a suitable extension you would like your users to see and you would like to share.</p>
                                </span>
                                <div class="form-group">
                                    <div class="col-sm-12" style="padding-right:0px;">
                                        <input type="text" placeholder="Unique URL" class="form-control" id="Subdomain" name="Subdomain" onblur="checksubdomain();">
                                    </div>
                                </div>
                                <h3 class="StepTitle">Give your App a Name</h3>
                                <p>Save your app with a suitable name. This name will appear when your app is shared across social media channels and underneath your logo in the app. For example: 25th Annual Event</p>
                                <div class="form-group">
                                    <div class="col-sm-12" style="padding-right:0px;">
                                        <input type="text" value="" placeholder="Enter Your app name" id="Event_name" name="Event_name" class="form-control name_group">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <button class="btn btn-theme_green next-step btn-block">
                                            Next Step >>>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                        <div id="step-2">
                            <div class="row">
                                <div class="col-sm-4">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Skip This Step
                                    </button>
                                </div>
                            </div>
                            <h3 class="StepTitle">Add your branding</h3>
                            <span class="control-label">Here you can your logo and app banner to customize your app with your own branding. Please use JPG, JPEG and PNG files to get the best results. These can be changed at any time!</span>
                            <div class="col-sm-6">
                                <h3 class="StepTitle">Add your logo</h3>
                                <span>We recommend adding a logo with a width of at least 500 pixels to ensure it appears nice and crisp.</span>
                                <div class="col-sm-9" style="margin-top:15px;">
                                    <?php $logo_images_array = json_decode($event['Logo_images']); ?>

                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail" id="logo_previews_image"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                <div id="sele">
                                                <span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span>
                                                </div><div id="chn">
                                                <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span></div>
                                                <input type="file" onchange="imagecrop();" name="logo_images" id="logo_images">
                                                </span>
                                                <div id="mybtn1">
                                                    <a href="#" id="mybtn" class="btn fileupload-exists btn-red mybtn" >
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                                </div>
                                           </div>
                                      </div>       
                                </div>
                                <div class="col-sm-3 custom-class">
                                    <input type="hidden" name="logo_image_tetbox" value="" id="logo_image_tetbox">
                                    <img alt="" src="<?php echo base_url(); ?>assets/images/logo_device.png">
                                </div>
                                <div class="banner" style="clear:both;">
                                    
                                    <h3 class="StepTitle">Add your banner</h3>
                                    <p>We recommend using an image with a larger width than 1500 pixels.</p>
                                    
                                    <div class="col-sm-9" style="margin-top:15px;">
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail" id="banner_previews_image"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file">
                                                    <div id="bannersele">
                                                    <span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span></div><div id="bannerchn">
                                                    <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span></div>
                                                    <input type="file" onchange="bannercrop();" name="images" id="images">
                                                    </span>
                                                    <div id="bannermybtn1">
                                                    <a href="#" id="bannerbtn" class="btn fileupload-exists btn-red">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                                    <button style="display:none;" id="cropbannerbtnshow" type="button" class="btn btn-info" data-toggle="modal" data-target="#bannerimage">Crop Banners</button>
                                                    </div>
                                               </div>
                                          </div>   
                                    </div>
                                    
                                    <div class="col-sm-3 custom-class">
                                    <input type="hidden" name="banner_image_tetbox" value="" id="banner_image_tetbox">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/banner_device.png">
                                </div>
                                    
                                </div>

                                <div class="banner" style="clear:both;">
                                <div class="col-sm-9">
                                    <h3 class="StepTitle">Add your background</h3>
                                    <p>Choose either a flat background color or upload a background image.</p>
                                    <div id="select_image_color" class="row">
                                        <div class="col-sm-6">
                                            <input value="Back-Ground Image" type="button" id="btn_select_image" onclick="show_div('select_image_div');" class="btn btn-theme_green btn-block">
                                        </div>
                                        <div class="col-sm-6">    
                                            <input value="Back-Ground Color" type="button" id="btn_select_color" onclick="show_div('background_color_div');" class="btn btn-theme_green btn-block"> 
                                        </div>
                                    </div>
                                    <div id="select_image_div" class="col-sm-9" style="display:none;margin-top:15px;">
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="background_img" id="background_img">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>  
                                    </div>
                                    </div>
                                    <div class="col-sm-3 custom-class">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/backgroud_device.png">
                                </div>
                                    
                                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                                    <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;display:none" id="background_color_div">
                                       <div class="col-sm-4" style="padding-left:0px;">
                                           <span style="color: #00b8c2;font-weight: bold;">Background Color</span>
                                        </div>
                                        <div class="col-sm-4" style="padding-right:0px;">
                                            <input type="text" value="" placeholder="Background color" id="Background_color" name="Background_color" class="color {hash:true} form-control name_group">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-top:5px;">
                                    <div class="col-sm-4">
                                        <button class="btn btn-green back-step btn-block">
                                            <<< Back
                                        </button>
                                    </div>

                                    <div class="col-sm-4">
                                        <button class="btn btn-theme_green next-step btn-block">
                                            Next Step >>>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step-3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Skip This Step
                                    </button>
                                </div>
                            </div>
                            <h3 class="StepTitle">Add homescreen text (optional)</h3>
                            <p>Add some text about your app. This will show just below your banner image. Try adding text that explains the app in more detail or maybe add text about your organization. The text box below allows for different text colors, images and weblinks so you can be creative! You can change this at any time.</p>
                            <div class="col-sm-9" style="padding-left:0px;margin-bottom: 2%;">
                                <textarea  id="Description" class="click2edit form-control" name="Description" style="width: 100%;height: 300px;"></textarea>
                            </div>
                            <div class="form-group" style="clear:both;">
                                <div class="col-sm-2">
                                    <button class="btn btn-green back-step btn-block">
                                        <<< Back
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <button id="three_step_btn" class="btn btn-theme_green next-step btn-block">
                                        Next Step >>>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="step-4">   
                            <h3 class="StepTitle">Create your color scheme</h3>
                            <p>Add your brand colors to your app to customize it further for your branding. Click on the box to select your colors from the color choose or paste in your color hex code into the box. You can change this at any time.</p>
                            <div class="col-sm-6">
                                 <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Top bar background Color </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="" placeholder="Top bar background color" id="Top_background_color" name="Top_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                    <div class="col-sm-2 custom-class-color">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/topbar_device.png">
                                </div>
                                  
                                </div>

                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Top bar text Color </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="" placeholder="Top bar text color" id="Top_text_color" name="Top_text_color" class="color {hash:true} form-control name_group">
                                    </div>
                                    <div class="col-sm-2 custom-class-color">
                                        <img alt="" src="<?php echo base_url(); ?>assets/images/topbar_texcolor_device.png">
                                    </div>
                                </div>

                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Footer background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="" placeholder="Footer background color" id="Footer_background_color" name="Footer_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" placeholder="Menu Background color" id="menu_background_color" name="menu_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Hover Background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" placeholder="Menu Hover Background color" id="menu_hover_background_color" name="menu_hover_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Text Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" placeholder="Menu Text color" id="menu_text_color" name="menu_text_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <button class="btn btn-green back-step btn-block">
                                            <<< Back
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button id="editform_submit" class="btn btn-theme_green next-step btn-block">
                                            Finish
                                        </button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div id="step-5" style="margin-bottom:15px;">
                            <div id="image-error-alert" style="display:none;" class="alert alert-danger"></div>
                            <div id="edit-success-alert" style="display:none;" class="alert alert-success"></div>
                            <h3 class="StepTitle">You have finished defining and setting up your app!</h3>
                            <p>Well done! You are now ready to add your content.</p>
                            <p>Start by opening the various modules you have access to in the left hand menu.</p>
                            <p>Below you can see our Guides Portal. Here you will find step by step instructions showing you how to customize each feature with video walkthroughs.</p>
                            <iframe id="guide_iframe" src="" style="width:90%;height:800px;position: relative;float: left;margin-bottom:10px;"></iframe>
                        </div>
                    </div>
                </form>   
            </div>   
        </div>
    </div>
</div>
<div class='modal fade no-display' id='bannerimage' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class="modal-header">
                    <button aria-hidden="true" aria-hidden="true" data-dismiss="modal" class="close" type="button">
                        ×
                    </button>
                </div>
                <div class='modal-body' id="show_banner_div">

                </div>
                <div class='modal-footer'>
                </div>
            </div>
        </div>
    </div>
<div class='modal fade no-display' id='displayimage' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header">
                <button aria-hidden="true" id="cancel"  data-dismiss="modal" class="close" type="button">
                    ×
                </button>
            </div>
            <div class='modal-body' id="show_image_div">

            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #map
    {
        margin-bottom: 20px;
        height: 250px;
        background-color: #fff !important;
    }
    .gm-style
    {
        left: 17.5% !important;
        width: 74% !important;
    }
    .fileupload-new.thumbnail
    {
      height:auto;
    }
</style>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript">
  $("#mybtn").click(function(){
    $('#logo_previews_image').hide();
    $('.fileupload-new').show();
    $("#chn").hide();
     $("#sele").show();
    $("#mybtn1").hide();
   
});
  $("#bannerbtn").click(function(){
    $('#banner_previews_image').hide();
    $('.fileupload-new').show();
    $("#bannerchn").hide();
     $("#bannersele").show();
    $("#bannermybtn1").hide();
});
   $('#three_step_btn').click(function(){
    setTimeout(function(){
        $('#step-5').hide();
    },1000);
  });
    $('#editform_submit').click(function(){
        $('#step-5').show();
        $('#step-4').hide();
        $('#guide_iframe').attr('src','https://www.allintheloop.com/guide-cms.html');
        var formData = new FormData($('form')[0]);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'Event/add/'; ?>",
            data: formData,
            processData: false,
            contentType: false,
            success: function(result){
                var values=result.split('###');
                $('#image-error-alert').html('');
                if(values[0]=="error")
                {
                    $('#image-error-alert').show();
                    $('#image-error-alert').html(values[1]);
                }
                else
                {
                   window.location.href=values[1];
                }
            }
        });
    });
    function bannercrop()
    {
        $('#cropbannerbtnshow').show();
        $('#cropbannerbtnshow').html('Uploading Banner');
        $('#banner_previews_image').show();
        $("#bannerchn").show();
        $("#bannersele").hide();
        $("#bannermybtn1").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url : "<?php echo base_url().'Event/bannerupload/'.$event['Id']; ?>",
            data:formData,
            type: "POST",  
            async: true,
            processData: false,
            contentType: false,
            success : function(data)
            {
                //$('#bannerimage').modal({show:'true'});
                $('#show_banner_div').html(data);
                $('#cropbannerbtnshow').html('Crop Banner'); 
            }
        });
    }
    function imagecrop()
    {
        $('#logo_previews_image').show();
        $("#chn").show();
        $("#sele").hide();
        $("#mybtn1").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url : "<?php echo base_url().'Event/imageupload/'.$event['Id']; ?>",
            data:formData,
            type: "POST",  
            async: true,
            processData: false,
            contentType: false,
            success : function(data)
            {
                $('#displayimage').modal({show:'true'});
                $('#show_image_div').html(data);        
            }
        });   
    }
    function closepoppu(){
        $('#displayimage').modal({show:'false'});
        $('#displayimage').data('modal', null);
    }
    function image_crop_save()
    {
        var coor ="x1="+$('#x1').val()+"&y1="+$('#y1').val()+"&x2="+$('#x2').val()+"&y2="+$('#y2').val()+"&w="+$('#w').val()+"&h="+$('#h').val()+"&organalimage="+$('#organalimage').val();
        if($('#x1').val()!="" && $('#y1').val()!="" && $('#x2').val()!="" && $('#y2').val()!="" && $('#w').val()!="" && $('#h').val()!=""){
        $.ajax({
            url : "<?php echo base_url().'Event/savecropimage/'.$event['Id']; ?>",
            data: coor,
            type: "POST",  
            async: true,
            success : function(data)
            {
                var ht='<img src="'+data+'" alert="no image"/>';
                if($.trim($('#logo_or_banner').val())==0)
                {
                    $('#logo_previews_image').html(ht);
                    $('#logo_image_tetbox').val($.trim($('#organalimage').val()));
                    $('#show_image_div').html('');
                }
                else
                {
                    $('#banner_previews_image').html(ht);
                    $('#banner_image_tetbox').val($.trim($('#organalimage').val()));
                    $('#show_banner_div').html('');
                }
                $('#displayimage').attr('class','modal fade');
                $('#displayimage').attr('style','display:none;');
                $('body').removeAttr('class');
                $('.modal-backdrop').removeClass('fade in');
                $('.modal-backdrop').hide();
                $('#displayimage').data('modal', null);
                $('#bannerimage').data('modal', null);
            }
        });
        }
        else
        {
            if($.trim($('#logo_or_banner').val())==0)
            {
                $('#logo_image_tetbox').val('');
            }
            else
            {
                $('#banner_image_tetbox').val('');    
            }
            $('#displayimage').attr('class','modal fade');
            $('#displayimage').attr('style','display:none;');
            $('body').removeAttr('class');
            $('.modal-backdrop').removeClass('fade in');
            $('.modal-backdrop').hide();
        }  
    }
  function show_div(divid)
  {
    if("select_image_div"==divid)
    {
        $('#'+divid).slideDown('slow');
        $('#background_color_div').slideUp('slow');
    }
    else
    {
        $('#'+divid).slideDown('slow'); 
        $('#select_image_div').slideUp('slow');
    }
  }
    function checksubdomain()
    {      
            var sendflag="";
            $.ajax({
            url : '<?php echo base_url(); ?>Event/checksubdomain',
            data :'Subdomain='+$("#Subdomain").val()+'&idval='+$('#idval').val(),
            type: "POST",  
            async: false,
            success : function(data)
            {
                var values=data.split('###');
                if(values[0]=="error")
                {   
                    $('#Subdomain').parent().removeClass('has-success').addClass('has-error');
                    $('#Subdomain').parent().find('.control-label span').removeClass('ok').addClass('required');
                    $('#Subdomain').parent().find('.help-block').removeClass('valid').html(values[1]);
                    sendflag=false;
                }
                else
                {
                    $('#Subdomain').parent().removeClass('has-error').addClass('has-success');
                    $('#Subdomain').parent().find('.control-label span').removeClass('required').addClass('ok');
                    $('#Subdomain').parent().find('.help-block').addClass('valid').html(''); 
                    sendflag=true;
                }
            }
        });
        return sendflag;
    }
</script>


<style>
  .cropit-image-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 5px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 250px;
    height: 250px;
    cursor: move;
  }
  .cropit-image-background {
    opacity: .2;
    cursor: auto;
  }
  .image-size-label {
    margin-top: 10px;
  }
  input {
    /* Use relative position to prevent from being covered by image background */
    position: relative;
    /*z-index: 10;*/
    display: block;
  }
  .export {
    margin-top: 10px;
  }
  #wizard h3
  {
    color: #00b8c2;
    font-weight: bold;
  }
  #wizard h3{
    font-size: 35px;
  }
  #wizard p,#wizard span{
    font-size: 15px;
  }
  #wizard #step-4 span
  {
    color: #00b8c2;
    font-weight: bold;
  }

  /*#wizard .progress-bar.partition-green.step-bar
  {
    background: #00b8c2;
  }*/

  #wizard .progress.progress-xs.transparent-black.no-radius.active.content
  {
    width: 95%;
    margin: 0 auto;
  }

  #wizard #step-1
  {
    padding:20px;
  }
  #wizard #step-2
  {
    padding:20px;
  }
  #wizard #step-3
  {
    padding:20px;
  }
  #wizard #step-4
  {
    padding:20px;
  }
  #wizard #step-5
  {
    padding:20px;
  }
  
</style>




<!-- end: PAGE CONTENT-->