<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/launchyourappviewpage.css?<?php echo time(); ?>">
<?php if(count($iseventfreetrial) > 0){ ?>
<form enctype="multipart/form-data" method="post" id="launch_your_app_form" name="launch_your_app_form" action="<?php echo base_url().'event/save_launch_your_app_data/'.$event_id; ?>">
<h4>Launch Your App</h4>
<div class="row" id="launch_your_app_popup_second_page" style="height: 500px;">
  <div class="col-sm-12 launch_your_app_form_messages_div">
    <h2>Congratulations! Your Payment is Successfull.</h2>
  </div>
  <div class="col-sm-12">
    <div class="col-sm-4">
      <div class="package_name_price">
        <h4 style="text-align: center;">Add Your Splash Screen</h4>
        <p>This shows when your App is opened.</p>
        <p>For best results use a JPEG image with the following dimensions: 2208x1242</p>
      </div>  
      <div class="col-sm-12 text-center">
        <div data-provides="fileupload" class="fileupload fileupload-new">
          <span class="btn btn-file btn-green">
            <span class="fileupload-new">Upload Splash Screen</span>
            <span class="fileupload-exists">Change</span>
            <input type="file" name="splash_screen_images" id="splash_screen_images">
          </span>
        </div>
        <input type="hidden" id="splash_screen_images_data_textbox" name="splash_screen_images_data_textbox" value="">
      </div>
    </div>
    <div class="col-sm-4">
      <div class="package_name_price">
        <h4 style="text-align: center;">Add Your Splash Screen</h4>
        <p>This will be seen on device Homescreens and in the Stores.</p>
        <p>For best results use a JPEG image with the following dimensions:1024x1024 – Do not round the corners</p>
      </div>
      <div class="col-sm-12 text-center">
        <div data-provides="fileupload" class="fileupload fileupload-new">
          <span class="btn btn-file btn-green"><span class="fileupload-new">Upload App Icon</span><span class="fileupload-exists">Change</span>
            <input type="file" name="app_icon_image" id="app_icon_image">
          </span>
        </div>
        <input type="hidden" id="app_icon_crope_images_text" name="app_icon_crope_images_text" value="">
      </div>
    </div>
    <div class="col-sm-4">
      <div class="package_name_price">
        <h4 style="text-align: center;">Add your App Description</h4>
        <p>This will be shown in the Stores.</p>
      </div>  
      <div class="col-sm-12 text-center">
        <a class="btn btn-green btn-block" data-toggle="modal" data-target="#edit_home_screen_text_popup" style="width: 190px; margin: 0 auto;" href="javascript:void(0);">Add Your App Description</a>
      </div>
    </div>
  </div>
  <div class="col-sm-12 text-center" style="display: none;">
    <button class="btn btn-green btn-block" type="button" style="width: 120px;margin:0 auto;" id="publish_your_app_btn">Publish</button>
  </div>
</div>
<div class="modal fade" id="splash_screen_images_crop_data_models" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Splash Screen Images</h4>
      </div>
      <div class="modal-body">
        <div class="row"  id="cropping_banner_div">
          <div id="preview_header_crop_tool">
            <img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
          </div>
          <div class="col-sm-12 text-center">
            <button type="button" style="width: 120px; margin: 0 auto;" class="btn btn-green btn-block" id="upload_result_btn_header_crop" data-dismiss="modal">Crop</button>   
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="appiconmodelscopetool" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">App Icon</h4>
        </div>
        <div class="modal-body" id="show_logoupload_div_data">
          <div class="row" id="cropping_logo_div">
            <div id="preview_logo_crop_tool">
              <img class="img-responsive" id="show_crop_img_model" src="" alt="Picture">
            </div>
            <div class="col-sm-12 text-center">
              <button class="btn btn-green btn-block" type="button" style="width: 120px;margin: 0 auto;" id="upload_result_btn_crop" data-dismiss="modal">Crop</button>   
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div id="edit_home_screen_text_popup" class="modal fade" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Add your App Description</h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">App Name for the App Stores <span class="symbol required"></span></label>
                <input style="clear:both;" type="text" placeholder="App Name for the App Stores" class="form-control name_group required" id="app_name_textbox" name="app_name_textbox" value="">
              </div>
            </div>
          </div>
          <div class="row">  
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Primary Language <span class="symbol required"></span></label>
                <input style="clear:both;" type="text" placeholder="Primary Language" class="form-control name_group required" id="primary_language" name="primary_language" value="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Description <span class="symbol required"></span></label>
                <textarea style="clear:both;" placeholder="Description" class="form-control name_group required" id="app_description" name="app_description"></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keywords for your App <span class="symbol required"></span></label>
                <textarea  style="clear:both;" placeholder="Keywords for your App (e.g. hotel conference, your event name)" class="form-control name_group required" id="app_keyword" name="app_keyword"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="row text-center" style="margin-top: 15px;">
          <button type="button" style="width: 150;margin: 0 auto;" name="submit_home_screen_text" id="submit_home_screen_text" class="btn btn-green btn-block">Save App Description</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="multi_event_app_name_models" class="modal fade" data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Multi-Event Name</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group">
            <h4 style="text-align: center;">What would you like to call your Multi-Event App?</h4>
            <div class="col-sm-12 text-center">
              <input type="text" name="multi_event_name_textbox" placeholder="Enater Multi-Events Name" id="multi_event_name_textbox" class="form-control name_group required" style="margin: 0 auto;">
              <span for="multi_event_name_textbox" class="help-block" id="multi_event_name_textbox_error_span" style="display: none;"></span>
            </div>
          </div>
        </div>
        <div class="row text-center" style="margin-top: 15px;">
          <button type="button" style="width: 150;margin: 0 auto;" name="submit_multi_event_name_textbox" id="submit_multi_event_name_textbox" class="btn btn-green btn-block">Publish</button>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $logoimage = $("#show_crop_img_model");
var $inputLogoImage = $('#app_icon_image'); 
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#splash_screen_images');
$inputLogoImage.change(function(){
  var uploadedImageURL;
  var URL = window.URL || window.webkitURL;
  var files = this.files;
  var file;
  if (files && files.length) {
    file = files[0];
    if (/^image\/\w+$/.test(file.type)) {
      if (uploadedImageURL) {
        URL.revokeObjectURL(uploadedImageURL);
      }
      uploadedImageURL = URL.createObjectURL(file);
      $logoimage.attr('src', uploadedImageURL);
      $('#appiconmodelscopetool').modal('show');
    } else {
      window.alert('Please choose an image file.');
    }
  }
});
$(document).on('shown.bs.modal','#appiconmodelscopetool', function () {
  $logoimage.cropper('destroy');
  var croppable = false;
  var $button = $('#upload_result_btn_crop');
  $logoimage.cropper({
    aspectRatio: 1,
    built: function () {
      croppable = true;
    }
  });
  $button.on('click', function () {
    var croppedCanvas;
    if (!croppable) {
      return;
    }
    croppedCanvas = $logoimage.cropper('getCroppedCanvas');
    $('#app_icon_crope_images_text').val(croppedCanvas.toDataURL());
    $("#app_icon_image").val('');
    if(valid_app_descript==1 && $('#splash_screen_images_data_textbox').val()!="" && $('#app_icon_crope_images_text').val()!="")
    {
      $('#publish_your_app_btn').parent().show();
    }
    else
    {
      $('#publish_your_app_btn').parent().hide();
    }
  });
}).on('hidden.bs.modal','#appiconmodelscopetool',function(){
  $logoimage.cropper('destroy');
});
$inputBannerImage.change(function(){
  var uploadedImageURL;
  var URL = window.URL || window.webkitURL;
  var files = this.files;
  var file;
  if (files && files.length) {
    file = files[0];
    if (/^image\/\w+$/.test(file.type)) {
      if (uploadedImageURL) {
        URL.revokeObjectURL(uploadedImageURL);
      }
      uploadedImageURL = URL.createObjectURL(file);
      $bannerimage.attr('src', uploadedImageURL);
      $('#splash_screen_images_crop_data_models').modal('toggle');
    } else {
      window.alert('Please choose an image file.');
    }
  }
});
$(document).on('shown.bs.modal','#splash_screen_images_crop_data_models' ,function () {
  $bannerimage.cropper('destroy');
    var croppable = false;
    var $button = $('#upload_result_btn_header_crop');
      $bannerimage.cropper({
        built: function () {
          croppable = true;
        }
      });
      $button.on('click', function () {
        var croppedCanvas;
        if (!croppable) {
          return;
        }
        croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
        $('#splash_screen_images_data_textbox').val(croppedCanvas.toDataURL());
        $("#splash_screen_images").val('');
        if(valid_app_descript==1 && $('#splash_screen_images_data_textbox').val()!="" && $('#app_icon_crope_images_text').val()!="")
        {
          $('#publish_your_app_btn').parent().show();
        }
        else
        {
          $('#publish_your_app_btn').parent().hide();
        }
      });
  }).on('hidden.bs.modal',function(){
       $bannerimage.cropper('destroy');
  }); 
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  Main.init();
  $('#launch_your_app_form').validate({
    errorElement: "span",
    errorClass: 'help-block',  
    rules: {
      app_name_textbox:{
        required:true,
        minlength:3,
      },
      primary_language:{
        required:true,
        minlength:4,
      },
      app_description:{
        required:true,
      },
      app_keyword:{
        required:true,
      }
    },
    highlight: function (element) 
    {
      $(element).closest('.help-block').removeClass('valid');
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
    }
  });
  $('#app_user_country').change(function(){
    $.ajax({
        url:"<?php echo base_url(); ?>Profile/getnewstate_by_name",    
        data: {name: $(this).val()},
        type: "POST",
        success: function(data)
        {
            $("#app_user_state").html(data);
        }
    });
  });
});
</script>
<script type="text/javascript">
var which_launch_your_app_btn_click="<?php echo $which_launch_your_app_btn_click; ?>";
var valid_app_descript=0;
$(window).load(function(){
  $('.summernote').summernote({
    height:'200px',
  });
});
$('#submit_home_screen_text').click(function(){

  if($("#launch_your_app_form").valid())
  {
    valid_app_descript=1;
  }
  if(valid_app_descript==1 && $('#splash_screen_images_data_textbox').val()!="" && $('#app_icon_crope_images_text').val()!="")
  {
    $('#publish_your_app_btn').parent().show();
  }
  else
  {
    $('#publish_your_app_btn').parent().hide();
  }
  if(valid_app_descript==1){
    $('#edit_home_screen_text_popup').modal('hide');
  }
});
$('#publish_your_app_btn').click(function(){
  if(which_launch_your_app_btn_click==2)
  {
    $('#multi_event_app_name_models').modal('show');
  }
  else
  {
    jQuery('#launch_your_app_form').submit();
  }
});
$('#submit_multi_event_name_textbox').click(function(){
  if($('#multi_event_name_textbox').val()=="")
  {
    $('#multi_event_name_textbox_error_span').html('This field is required.');
    $('#multi_event_name_textbox_error_span').parent().parent().addClass('has-error').removeClass('has-success');
    $('#multi_event_name_textbox_error_span').show();
  }
  else if($('#multi_event_name_textbox').val().length < 3)
  {
    $('#multi_event_name_textbox_error_span').html('Please enter at least 3 characters.');
    $('#multi_event_name_textbox_error_span').parent().parent().addClass('has-error').removeClass('has-success');
    $('#multi_event_name_textbox_error_span').show();
  }
  else
  {
    $('#multi_event_name_textbox_error_span').html('');
    $('#multi_event_name_textbox_error_span').parent().parent().addClass('has-success').removeClass('has-error');
    $('#multi_event_name_textbox_error_span').hide();
    jQuery('#launch_your_app_form').submit();
  }
});
</script>
<?php }else{ ?>
<script src="https://www.allintheloop.net/demo/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<div id="launch_your_app_form">
  <h4>Launch Your App</h4>
  <div class="row">
    <h2 style="text-align: center;"></h2>
  </div>
</div>
<?php } ?>
