<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="col-sm-12">
	<div class="panel panel-body">
		<h5>"Download the All In The Loop App to preview your App. Open your App within the All In The Loop App with the Secure Key <?php echo $event_templates[0]['secure_key']; ?>.</h5>
		<h5>The All In The Loop App can be downloaded from the iOS App Store and the Android Google Play Store."</h5>
		<div class="col-sm-12" style="padding:20px 0px;">
			<div class="col-sm-6"><img src="<?php echo base_url().'assets/images/preview.png'; ?>"></div>
			<div class="col-sm-6">
            	<div class="ios-android-img">
                <div class="ios-android-img2">
				<div class="col-sm-12">
					<a target="_blank" href="https://itunes.apple.com/us/app/all-in-the-loop/id1200784294?mt=8"><img width="100%" src="<?php echo base_url().'assets/images/ios icon.png'; ?>"></a>
				</div>
				<div class="col-sm-12">
					<a target="_blank" href="https://play.google.com/store/apps/details?id=com.allintheloop"><img width="100%" src="<?php echo base_url().'assets/images/android icon.png'; ?>"></a>
				</div>
                </div></div>
			</div>
		</div>
	</div>
</div>
