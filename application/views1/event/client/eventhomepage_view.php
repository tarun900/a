<?php $acc_name=$this->session->userdata('acc_name');
$active_menu=array_filter(explode(",",$event['checkbox_values']));
$addcountmodules=0;
$onmodules=0;
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/newcmseventpage.css?<?php echo time(); ?>">
<div class="col-sm-12">
	<div class="panel panel-body">
		<div class="col-sm-12">
			<h1 style="color:#5381ce;">App Summary</h1>
		</div>
		<div class="col-sm-6">
			<p>Use the table below to see which features still need content adding.  The Progress Bar at the top of the screen will help guide you through your App Build. Click on a circle for some more information on your progress.</p>
			<div class="row">
				<div class="col-sm-12" id="tabble_all_data_content_div">
					<div class="tabbable tabs-left">
						<ul id="visitor_event_second_screen_tab" class="nav nav-tabs">
							<li class="active">
								<a href="#app_content_data_section" data-toggle="tab">
									App Content
								</a>
							</li>
							<li class="">
								<a href="#interactive_content_data_section" data-toggle="tab">
									Interactive Features
								</a>
							</li>
							<li class="">
								<a href="#event_content_data_section" data-toggle="tab">
									Event Features
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="app_content_data_section">
								<div class="col-sm-12">
									<div class="row">
										<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor advertising icon.png'; ?>" />
											</div>
											<div class="content-info">
												<label class="control-label" id="label_5">Advertising</label>
												<p>Show advert banners within your App.</p>
												<?php if(in_array('5',$active_menu)){ $onmodules++; ?>
												<span><?php echo $advertising_count.' Adverts'; ?></span>
												<?php } ?>
											</div>
				                            <div class="edit-labeltxt">
				                            	<?php if($advertising_count > 0){ if(in_array('5',$active_menu)){ $addcountmodules++; }  ?>
			                            		<a href="<?php echo base_url().'Advertising_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_5" <?php if(!in_array('5',$active_menu)){ ?> style="display: none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Advertising_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_5" <?php if(!in_array('5',$active_menu)){ ?> style="display: none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_5" <?php if(in_array('5',$active_menu)){ ?> style="display: none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="advertising_active" value="5">
					                                <label class="onoffswitch-label" for="advertising_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
			                            </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor maps icon.png'; ?>" />
	                						</div>
	                                    	<div class="content-info">
												<label class="control-label" id="label_10">Maps</label>
												<p>Include Google Maps and Interactive Maps</p>
												<?php if(in_array('10',$active_menu)){ $onmodules++;  ?>
												<span><?php echo $maps_count.' Maps'; ?></span>
												<?php } ?>
											</div>
					                        <div class="edit-labeltxt">
				                            	<?php if($maps_count > 0){ if(in_array('10',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Map/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_10" <?php if(!in_array('10',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Map/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_10" <?php if(!in_array('10',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_10" <?php if(in_array('10',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="maps_active" value="10">
					                                <label class="onoffswitch-label" for="maps_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor social icon.png'; ?>" />
	                						</div>
	                                      	<div class="content-info">
												<label class="control-label" id="label_17">Social</label>
												<p>Include your social media links</p>
												<?php if(in_array('17',$active_menu)){ $onmodules++; ?>
												<span><?php echo $social_count.' Social Links'; ?></span>
												<?php } ?>
											</div>
				                            <div class="edit-labeltxt">
				                            	<?php if($social_count > 0){ if(in_array('17',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Social_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_17" <?php if(!in_array('17',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Social_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_17" <?php if(!in_array('17',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_17" <?php if(in_array('17',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="social_active" value="17">
					                                <label class="onoffswitch-label" for="social_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    	<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor presentations icon.png'; ?>" />
											</div>
	                                        <div class="content-info">
						                        <label class="control-label" id="label_9">Presentations</label>
		                                        <p>Add Powerpoint presentations</p>
		                                        <?php if(in_array('9',$active_menu)){ $onmodules++; ?>
		                                        <span><?php echo $presentation_count.' Presentations'; ?></
		                                        span>
		                                        <?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($presentation_count > 0){ if(in_array('9',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Presentation_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_9" <?php if(!in_array('9',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Presentation_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_9" <?php if(!in_array('9',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php }  ?> 
				                            	<div class="onoffswitch onoffswitch_div_9" <?php if(in_array('9',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="presentation_active" value="9">
					                                <label class="onoffswitch-label" for="presentation_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor documents icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
						                        <label class="control-label" id="label_16">Documents</label>
		                                        <p>Share multiple documents</p>
		                                        <?php if(in_array('16',$active_menu)){ $onmodules++; ?>
		                                        <span><?php echo $document_count.' Documents'; ?></span>
		                                        <?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($document_count > 0){ if(in_array('16',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Document/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_16" <?php if(!in_array('16',$active_menu)){ ?> style="display:none;" <?php  } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Document/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_16" <?php if(!in_array('16',$active_menu)){ ?> style="display:none;" <?php  } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_16" <?php if(in_array('16',$active_menu)){ ?> style="display:none;" <?php  } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="document_active" value="16">
					                                <label class="onoffswitch-label" for="document_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
								</div>	
							</div>
							<div class="tab-pane fade in" id="interactive_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    	<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor notes icon.png'; ?>" />
											</div>
	                                        <div class="content-info">
		                                        <label class="control-label" id="label_6">Notes</label>
		                                        <p>Allow Users to make notes.</p>
		                                        <?php if(in_array('6',$active_menu)){ $addcountmodules++; $onmodules++; ?>
		                                        <span><?php echo $notes_count.' Notes'; ?></span>
		                                        <?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
			                            		<a href="<?php echo base_url().'Notes_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_6" <?php if(!in_array('6',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			No Content Required <i class="fa fa-check"></i>
			                            		</a>
				                            	<div class="onoffswitch onoffswitch_div_6" <?php if(in_array('6',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="notes_active" value="6">
					                                <label class="onoffswitch-label" for="notes_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor private messaging icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_12">Private Messaging</label>
	                                        	<p>Allow Users to message privately.</p>
	                                        	<?php if(in_array('12',$active_menu)){ $addcountmodules++; $onmodules++; ?>
	                                        	<span><?php echo $private_messages_count.' Private Messaging'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
			                            		<a href="<?php echo base_url().'Message/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_12" <?php if(!in_array('12',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			No Content Required <i class="fa fa-check"></i>
			                            		</a>
				                            	<div class="onoffswitch onoffswitch_div_12" <?php if(in_array('12',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="private_messaging_active" value="12">
					                                <label class="onoffswitch-label" for="private_messaging_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor surveys icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_15">Surveys</label>
	                                        	<p>Include polls and surveys.</p>
	                                        	<?php if(in_array('15',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $surveys_count.' Surveys'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($surveys_count > 0){ if(in_array('15',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Survey/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_15" <?php if(!in_array('15',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Survey/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_15" <?php if(!in_array('15',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_15" <?php if(in_array('15',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="surveys_active" value="15">
					                                <label class="onoffswitch-label" for="surveys_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
			                            </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor twitter feed icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_44">Twitter Feed</label>
	                                        	<p>Include a Twitter Feed.</p>
	                                        	<?php if(in_array('44',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $hashtags_count.' Twitter Hashtags'; ?></span>	
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($hashtags_count > 0){ if(in_array('44',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Twitter_feed_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_44" <?php if(!in_array('44',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Twitter_feed_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_44" <?php if(!in_array('44',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_44" <?php if(in_array('44',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="twitter_active" value="44">
					                                <label class="onoffswitch-label" for="twitter_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor instagram icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_46">Instagram Feed</label>
	                                        	<p>Include an Instagram Feed.</p>
	                                        </div>
				                            <div class="edit-labeltxt"> 
				                            	<?php if(!empty($event['insta_access_token'])){ if(in_array('46',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Instagram_feed_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_46" <?php if(!in_array('46',$active_menu)){ ?> style="display: none;" <?php  } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Instagram_feed_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_46" <?php if(!in_array('46',$active_menu)){ ?> style="display: none;" <?php  } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_46" <?php if(in_array('46',$active_menu)){ $onmodules++; ?> style="display: none;" <?php  } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="instagram_active" value="46">
					                                <label class="onoffswitch-label" for="instagram_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    	<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor photos icon.png'; ?>" />
											</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_11">Photos</label>
	                                        	<p>Allow Users to share photos on a photo feed.</p>
	                                        	<?php if(in_array('11',$active_menu)){ $addcountmodules++; $onmodules++; ?>
	                                        	<span><?php echo $photos_count.' Photos'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
			                            		<a href="<?php echo base_url().'Photos_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_11" <?php if(!in_array('11',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			No Content Required <i class="fa fa-check"></i>
			                            		</a>
				                            	<div class="onoffswitch onoffswitch_div_11" <?php if(in_array('11',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="photos_active" value="11">
					                                <label class="onoffswitch-label" for="photos_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                                    </div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor public messaging icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_13">Public Messaging</label>
	                                        	<p>Allow Users share messages on a public message feed.</p>
	                                        	<?php if(in_array('13',$active_menu)){ $addcountmodules++; $onmodules++; ?>
	                                        	<span><?php echo $public_messages_count.' Public Messaging'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
			                            		<a href="<?php echo base_url().'Publicmessage/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_13" <?php if(!in_array('13',$active_menu)){ ?> style="display:none !important"<?php } ?>>
			                            			No Content Required <i class="fa fa-check"></i>
			                            		</a>
				                            	<div class="onoffswitch onoffswitch_div_13" <?php if(in_array('13',$active_menu)){ ?> style="display:none !important"<?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="public_message_active" value="13">
					                                <label class="onoffswitch-label" for="public_message_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor form builder icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_26">Form Builder</label>
	                                        	<p>Include contact forms to generate leads.</p>
	                                        	<?php if(in_array('26',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $formbuilder_count.' Forms'; ?></span>	
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($formbuilder_count > 0){ if(in_array('26',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Formbuilder/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_26" <?php if(!in_array('26',$active_menu)){ ?> style="display: none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Formbuilder/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_26" <?php if(!in_array('26',$active_menu)){ ?> style="display: none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_26" <?php if(in_array('26',$active_menu)){ ?> style="display: none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Forms_bulider_active" value="26">
					                                <label class="onoffswitch-label" for="Forms_bulider_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor activity icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">  
	                                        	<label class="control-label" id="label_45">Activity Feed</label>
	                                        	<p>Show a full activity feed of all Users.</p>
	                                        	<?php if(in_array('45',$active_menu)){ $addcountmodules++; $onmodules++; ?>
	                                        	<span><?php echo $activity_data.' Activity Feed'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
			                            		<a href="<?php echo base_url().'Activity_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_45" <?php if(!in_array('45',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			No Content Required <i class="fa fa-check"></i>
			                            		</a>
				                            	<div class="onoffswitch onoffswitch_div_45" <?php if(in_array('45',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="activity_active" value="45">
					                                <label class="onoffswitch-label" for="activity_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                                    </div>
								</div>
						
							<div class="tab-pane fade in" id="event_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    	<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor agenda icon.png'; ?>" />
											</div>
				                            <div class="content-info">
				                            	<label class="control-label" id="label_1">Agenda</label>
				                            	<p>Add multiple Agendas and sessions.</p>
				                            	<?php if(in_array('1',$active_menu)){ $onmodules++; ?>
				                            	<span><?php echo $session_count.' Sessions'; ?></span>
				                            	<?php } ?>
				                            </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($session_count > 0){ if(in_array('1',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Agenda_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_1" <?php if(!in_array('1',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Agenda_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_1" <?php if(!in_array('1',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_1" <?php if(in_array('1',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="agenda_active" value="1">
					                                <label class="onoffswitch-label" for="agenda_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor exhibitors icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_3">Exhibitors</label>
	                                        	<p>Manage and include an Exhibitor Directory.</p>
	                                        	<?php if(in_array('3',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $exhibitors_count.' Exhibitors'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($exhibitors_count > 0){ if(in_array('3',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Exhibitor/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_3" <?php if(!in_array('3',$active_menu)){ ?>  style="display: none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Exhibitor/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_3" <?php if(!in_array('3',$active_menu)){ ?>  style="display: none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_3" <?php if(in_array('3',$active_menu)){ ?>  style="display: none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="exhibitor_active" value="3">
					                                <label class="onoffswitch-label" for="exhibitor_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor sponsors icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_43">Sponsors</label>
	                                        	<p>Manage and include a Sponsor Directory.</p>
	                                        	<?php if(in_array('43',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $sponsors_count.' Sponsors'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($sponsors_count > 0){ if(in_array('43',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Sponsors/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_43" <?php if(!in_array('43',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Sponsors/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_43" <?php if(!in_array('43',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_43" <?php if(in_array('43',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Sponsors_active" value="43">
					                                <label class="onoffswitch-label" for="Sponsors_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    	<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor attendees icon.png'; ?>" />
											</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_2">Attendees</label>
	                                        	<p>Manage and include an Attendee Directory.</p>
	                                        	<?php if(in_array('2',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $attendee_count.' Attendees'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($attendee_count > 0){ if(in_array('2',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Attendee_admin/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_2" <?php if(!in_array('2',$active_menu)){ ?> style="display:none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Attendee_admin/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_2" <?php if(!in_array('2',$active_menu)){ ?> style="display:none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_2" <?php if(in_array('2',$active_menu)){ ?> style="display:none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="attendee_active" value="2">
					                                <label class="onoffswitch-label" for="attendee_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
	                						<div class="icon_img_modules">
	                							<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor speakers icon.png'; ?>" />
	                						</div>
	                                        <div class="content-info">
	                                        	<label class="control-label" id="label_7">Speakers</label>
	                                        	<p>Manage and include a Speaker Directory.</p>
	                                        	<?php if(in_array('7',$active_menu)){ $onmodules++; ?>
	                                        	<span><?php echo $speakers_count.' Speakers'; ?></span>
	                                        	<?php } ?>
	                                        </div>
				                            <div class="edit-labeltxt">
				                            	<?php if($speakers_count > 0){ if(in_array('7',$active_menu)){ $addcountmodules++; } ?>
			                            		<a href="<?php echo base_url().'Speaker/index/'.$event_id; ?>" class="btn btn-success btn-block add_content_btn_7" <?php if(!in_array('7',$active_menu)){  ?> style="display: none;" <?php } ?>>
			                            			Added <i class="fa fa-check"></i>
			                            		</a>
				                            	<?php }else{ ?>
				                            	<a href="<?php echo base_url().'Speaker/index/'.$event_id; ?>" class="btn btn-green btn-block add_content_btn_7" <?php if(!in_array('7',$active_menu)){  ?> style="display: none;" <?php } ?>>
				                            		<i class="fa fa-plus"></i> Add Content
				                            	</a>
				                            	<?php } ?> 
				                            	<div class="onoffswitch onoffswitch_div_7" <?php if(in_array('7',$active_menu)){  ?> style="display: none;" <?php } ?>>
					                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="speakers_active" value="7">
					                                <label class="onoffswitch-label" for="speakers_active">
					                                  <span class="onoffswitch-inner"></span>
					                                  <span class="onoffswitch-switch"></span>
					                                 </label>
				                            	</div>
				                            </div>
			                            </div>
                					</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div id="viewport" class="iphone">
                <iframe src="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>" id="result_iframe_first"></iframe>
            </div>
		</div>
	</div>
</div>
<div class="modal fade" id="design_point_models" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title text-center">Design</h4>
			</div>
			<div class="modal-body">
				<?php 
				$prosess=20; 
				if(!empty($event['Logo_images']) || $flage_data[0]['logo_complate']=='1'){ $prosess+=20; } 
				$banner12=json_decode($event['Images']);  
				if(count($banner12) > 0 || $flage_data[0]['banner_complate']=='1'){ $prosess+=20; }
				if($event['Top_background_color']!="#3b99d8" || $event['Top_text_color']!="#FFFFFF" || $event['menu_background_color']!="#2f80b6" || $event['menu_text_color']!="#FFFFFF" || $flage_data[0]['colors_complate']=='1'){ $prosess+=20; }
				if(!empty($event['Description']) || $flage_data[0]['homescreen_text_complate']=='1'){ $prosess+=20; }
				?>
				<div class="row">
					<div class="col-sm-12">
						<div class="progress" id="design_popup_progress_bar">
							<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $prosess ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $prosess ?>%;">
								<?php echo $prosess; ?>%
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-4">
							<h3>Logo Image</h3>
						</div>	
						<div class="col-sm-8">
							<div class="col-sm-6 added_logo_complate" <?php if(empty($event['Logo_images']) && $flage_data[0]['logo_complate']!='1'){ ?> style="display: none;" <?php } ?>>	
	                    		<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_images_section'; ?>" class="btn btn-success btn-block">
	                    			Added <i class="fa fa-check"></i>
	                    		</a>
                    		</div>
                        	<div class="col-sm-6 add_content_logo_complate" <?php if(!empty($event['Logo_images']) || $flage_data[0]['logo_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_images_section'; ?>" class="btn btn-green btn-block">
	                        		<i class="fa fa-plus"></i> Add Content
	                        	</a>
	                        </div>	
	                        <span id="or_span_logo_complate" <?php if(!empty($event['Logo_images']) || $flage_data[0]['logo_complate']=='1'){ ?> style="display: none;" <?php } ?>>or</span>
	                        <div class="col-sm-6" <?php if(!empty($event['Logo_images']) || $flage_data[0]['logo_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="javascript:void(0);" onclick="call_complate('logo_complate','1',this);" class="btn btn-green btn-block">
	                        		Mark as Complete
	                        	</a>
	                        </div>
						</div>	
					</div>
					<div class="col-sm-12">
						<div class="col-sm-4">
							<h3>Banner Image</h3>
						</div>	
						<div class="col-sm-8">
                    		<div class="col-sm-6 added_banner_complate" <?php $banner1=json_decode($event['Images']);  if(count($banner1) < 1 && $flage_data[0]['banner_complate']!='1'){ ?> style="display: none;" <?php } ?>>	
	                    		<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_images_section'; ?>" class="btn btn-success btn-block">
	                    			Added <i class="fa fa-check"></i>
	                    		</a>
                    		</div>
                        	<div class="col-sm-6 add_content_banner_complate" <?php $banner1=json_decode($event['Images']);  if(count($banner1) > 0 || $flage_data[0]['banner_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_images_section'; ?>" class="btn btn-green btn-block">
	                        		<i class="fa fa-plus"></i> Add Content
	                        	</a>
	                        </div>	
	                        <span id="or_span_banner_complate" <?php $banner1=json_decode($event['Images']);  if(count($banner1) > 0 || $flage_data[0]['banner_complate']=='1'){ ?> style="display: none;" <?php } ?>>or</span>
	                        <div class="col-sm-6" <?php $banner1=json_decode($event['Images']);  if(count($banner1) > 0 || $flage_data[0]['banner_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="javascript:void(0);" onclick="call_complate('banner_complate','1',this);" class="btn btn-green btn-block">
	                        		Mark as Complete
	                        	</a>
	                        </div>
						</div>	
					</div>	
					<div class="col-sm-12">
						<div class="col-sm-4">
							<h3>Colors</h3>
						</div>	
						<div class="col-sm-8">
                    		<div class="col-sm-6 added_colors_complate" <?php if($event['Top_background_color']=="#3b99d8" && $event['Top_text_color']=="#FFFFFF" && $event['menu_background_color']=="#2f80b6" && $event['menu_text_color']=="#FFFFFF" && $flage_data[0]['colors_complate']!='1'){ ?> style="display:none;" <?php } ?>>	
	                    		<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_colors_section'; ?>" class="btn btn-success btn-block">
	                    			Added <i class="fa fa-check"></i>
	                    		</a>
                    		</div>
                        	<div class="col-sm-6 add_content_colors_complate" <?php if($event['Top_background_color']!="#3b99d8" || $event['Top_text_color']!="#FFFFFF" || $event['menu_background_color']!="#2f80b6" || $event['menu_text_color']!="#FFFFFF" || $flage_data[0]['colors_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_colors_section'; ?>" class="btn btn-green btn-block">
	                        		<i class="fa fa-plus"></i> Add Content
	                        	</a>
	                        </div>	
	                        <span id="or_span_colors_complate" <?php if($event['Top_background_color']!="#3b99d8" || $event['Top_text_color']!="#FFFFFF" || $event['menu_background_color']!="#2f80b6" || $event['menu_text_color']!="#FFFFFF" || $flage_data[0]['colors_complate']=='1'){ ?> style="display: none;" <?php } ?>>or</span>
	                        <div class="col-sm-6" <?php if($event['Top_background_color']!="#3b99d8" || $event['Top_text_color']!="#FFFFFF" || $event['menu_background_color']!="#2f80b6" || $event['menu_text_color']!="#FFFFFF" || $flage_data[0]['colors_complate']=='1'){ ?> style="display: none;" <?php } ?>>
	                        	<a href="javascript:void(0);" onclick="call_complate('colors_complate','1',this);" class="btn btn-green btn-block">
	                        		Mark as Complete
	                        	</a>
	                        </div>
						</div>	
					</div>
					<div class="col-sm-12">
						<div class="col-sm-4">
							<h3>Icons</h3>
						</div>	
						<div class="col-sm-8">
                    		<div class="col-sm-6">	
	                    		<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_icons_section'; ?>" class="btn btn-success btn-block">
	                    			Added <i class="fa fa-check"></i>
	                    		</a>
                    		</div>
						</div>	
					</div>
					<div class="col-sm-12">
						<div class="col-sm-4">
							<h3>HomeScreen Text</h3>
						</div>	
						<div class="col-sm-8">
                    		<div class="col-sm-6 added_homescreen_text_complate" <?php if(empty(strip_tags($event['Description'])) && $flage_data[0]['homescreen_text_complate']!='1'){ ?> style="display:none;" <?php } ?>>	
	                    		<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_home_screen_text_section'; ?>" class="btn btn-success btn-block">
	                    			Added <i class="fa fa-check"></i>
	                    		</a>
                    		</div>
                        	<div class="col-sm-6 add_content_homescreen_text_complate" <?php if(!empty(strip_tags($event['Description'])) || $flage_data[0]['homescreen_text_complate']=='1'){ ?> style="display:none;" <?php } ?>>
	                        	<a href="<?php echo base_url().'Event/edit/'.$event_id.'#app_home_screen_text_section'; ?>" class="btn btn-green btn-block">
	                        		<i class="fa fa-plus"></i> Add Content
	                        	</a>
	                        </div>	
	                        <span id="or_span_homescreen_text_complate" <?php if(!empty(strip_tags($event['Description'])) || $flage_data[0]['homescreen_text_complate']=='1'){ ?> style="display:none;" <?php } ?>>or</span>
	                        <div class="col-sm-6" <?php if(!empty(strip_tags($event['Description'])) || $flage_data[0]['homescreen_text_complate']=='1'){ ?> style="display:none;" <?php } ?>>
	                        	<a href="javascript:void(0);" onclick="call_complate('homescreen_text_complate','1',this);" class="btn btn-green btn-block">
	                        		Mark as Complete
	                        	</a>
	                        </div>
						</div>	
					</div>
					<div class="col-sm-12">
                		<a  data-dismiss="modal" href="javascript:void(0);" class="btn btn-success btn-block" style="width: 120px;margin: 0 auto;">
                			Save 
                		</a>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="content_point_models" role="dialog">
    <div class="modal-dialog modal-md">
    	<div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title text-center">Content</h4>
	        </div>
	        <div class="modal-body">
	        	<div class="row">
	        		<?php $content_prosess=round($addcountmodules*100/$onmodules); ?>
					<div class="col-sm-12">
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $content_prosess ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $content_prosess ?>%;">
								<?php echo $content_prosess; ?>%
							</div>
						</div>
					</div>
					<div class="col-sm-12" id="pop_models_content_div">
						
					</div>
				</div>
	        </div>
      	</div>
    </div>
</div>
<div class="modal fade" id="check_point_models" role="dialog">
    <div class="modal-dialog modal-md">
    	<div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title text-center" id="check_mark_as_complet"><?php echo $flage_data[0]['check_complate']=='1' ? 'Checked' : 'Check' ; ?></h4>
	        </div>
	        <div class="modal-body">
	        	<div class="row text-center">
	        		<h4>
	        			Have you checked your App on your device yet? Download the All In The Loop Showcase App to view your App on your device. Enter your Secure Key "<?php echo $event_templates[0]['secure_key']; ?>" when you open the App to find yours and play around with it.
	        			<p style="font-weight: bold;">Login with your credentials to gain access to your App.</p> 
	        		</h4>
	        		<div class="col-sm-12">
	        			<div class="col-sm-6">
	        				<a href="https://itunes.apple.com/us/app/all-in-the-loop/id1200784294?ls=1&mt=8" target="_blank">
	        					<img width="100%" src="<?php echo base_url().'assets/images/ios icon.png'; ?>">
	        				</a>
	        			</div>
	        			<div class="col-sm-6">
	        				<a href="https://play.google.com/store/apps/details?id=com.allintheloop" target="_blank">
	        					<img width="100%" src="<?php echo base_url().'assets/images/android icon.png'; ?>">
	        				</a>
	        			</div>
	        		</div>
	        		<?php if($flage_data[0]['check_complate']!='1'){ ?>
	        		<a href="javascript:void(0);" onclick="call_complate('check_complate','1',this);" style="width: 150px;margin: 0 auto;" class="btn btn-green btn-block">
                		Mark as Checked
                	</a>
                	<?php } ?>
				</div>
	        </div>
      	</div>
    </div>
</div>
<div class="modal fade" id="submit_point_models" role="dialog">
    <div class="modal-dialog modal-md">
    	<div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title text-center"> Submit </h4>
	        </div>
	        <div class="modal-body">
	        	<div class="row">
	        		<div class="col-sm-12 text-center">
						<a href="javascript:void(0);" class="btn btn-green btn-block" style="width: 150px;margin: 0 auto;">LAUNCH YOUR APP</a>	        			
	        		</div>
	        	</div>
	        </div>
      	</div>
    </div>
</div>
<script src="https://www.allintheloop.net/demo/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	$('#pop_models_content_div').html($('#tabble_all_data_content_div').html());
	$('#pop_models_content_div div ul li').each(function(){
		var attr=$(this).find('a').attr('href')+'1';
		$(this).find('a').attr('href',attr);
	});
	$('#pop_models_content_div div div').each(function(){
		var attr=$(this).attr('id')+'1';
		$(this).attr('id',attr);
	});
	<?php if($prosess=="100" || $flage_data[0]['check_complate']=='1'){ ?>
	jQuery('#design_progress_link_inner_toolbar').addClass('done');
	<?php }else{ ?>
	jQuery('#design_progress_link_inner_toolbar').addClass('selected');	
	<?php } if(($content_prosess=="100" && $prosess=="100") || $flage_data[0]['check_complate']=='1'){ ?>
	jQuery('#content_progress_link_inner_toolbar').addClass('done');	
	<?php }else{ ?>
	jQuery('#content_progress_link_inner_toolbar').addClass('selected');	
	<?php } if($flage_data[0]['check_complate']=='1' || ($content_prosess=="100" && $prosess=="100")){ ?>
	jQuery('#check_progress_link_inner_toolbar').addClass('done');	
	<?php }else{ ?>
	jQuery('#check_progress_link_inner_toolbar').addClass('selected');	
	<?php } if(count($iseventfreetrial) > 0){ ?>
	jQuery('#submit_progress_link_inner_toolbar').addClass('selected');	
	jQuery('#submit_progress_link_inner_toolbar').attr('href',"<?php echo base_url().'Event/get_launch_your_app_function_content/'.$event['Id']; ?>");
	<?php }else{ ?>
		<?php  if($flage_data[0]['check_complate']=='1' || ($content_prosess=="100" && $prosess=="100")){ ?>
		jQuery('#submit_progress_link_inner_toolbar').addClass('done');	
		<?php }else{ ?>
		jQuery('#submit_progress_link_inner_toolbar').addClass('selected');	
		<?php } ?>
	<?php } ?>
});
function call_complate(key,value,e)
{
	$.ajax({
		url:"<?php echo base_url().'Event/save_as_complate/'.$event_id ?>",
		type:'post',
		data:key+'='+value,
		success:function(data)
		{
			e.remove();
			if($.trim(key)=="check_complate")
			{
				$('#check_mark_as_complet').html("Checked");
				jQuery('#design_progress_link_inner_toolbar').addClass('done');
				jQuery('#content_progress_link_inner_toolbar').addClass('done');	
				jQuery('#check_progress_link_inner_toolbar').addClass('done');	

			}
			else
			{
				$('#or_span_'+key).hide();
				$('.add_content_'+key).hide();
				$('.added_'+key).show();
				var proint=parseInt($('#design_popup_progress_bar').find('div').attr('aria-valuenow'))+20;
				$('#design_popup_progress_bar').find('div').attr('aria-valuenow',proint);
				$('#design_popup_progress_bar').find('div').attr('style','width:'+proint+'%');
				$('#design_popup_progress_bar').find('div').html(proint+"%");
				if(proint==100)
				{
					jQuery('#design_progress_link_inner_toolbar').addClass('done');
				}
			}
		}
	});
}
</script>
<script type="text/javascript">
$('input[name="active_modules_arr[]"]').change(function(e){
	var val=$(this).val();
	$.ajax({
		url:"<?php echo base_url().'Event/on_modules_in_home/'.$event_id ?>",
		type:'post',
		data:'mid='+$(this).val(),
		success:function(data)
		{
			if($.trim(data)=="success###")
			{
				$('.add_content_btn_'+val).show();
				$('.onoffswitch_div_'+val).hide();
			}
		}
	});
});
</script>