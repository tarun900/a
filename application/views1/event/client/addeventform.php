<?php 
$ip = $_SERVER['REMOTE_ADDR'];
$query = @unserialize(file_get_contents('https://ip-api.com/php/'.$ip));
if($query && $query['status'] == 'success') 
{
  if($query['countryCode'] == 'UK')
  {
    $currency="gbp";
  }
  else
  {
    $currency="usd";
  }
}
else
{
  $currency="usd";
}
?>
<?php $acc_name=$this->session->userdata('acc_name'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/newcmseventpage.css?<?php echo time(); ?>">
<div class="col-sm-12">
	<div class="panel panel-body">
		<form action="<?php echo base_url().'Event/add_event'; ?>" method="post" id="addeventform">
			<section id="app_name_section" class="show app_section">
				<h2 align="center">Name Your App</h2>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
			                <div class="col-sm-12">
			                    <input type="text" placeholder="Enter Your app name" id="Event_name" name="Event_name" class="form-control name_group">
			                </div>
			            </div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
			            <button id="save_app_name_btn" type="submit" class="btn btn-theme_green btn-block">
			                Save
			            </button>
			        </div>
		        </div>
	        </section>
	        <div class="modal fade" id="multi_event_point_models" role="dialog">
			    <div class="modal-dialog modal-md">
			    	<div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title text-center"> Launch Your App </h4>
				        </div>
				        <div class="modal-body">
				        	<div class="row" id="choose_launch_your_app_btn_div">
				        		<h3>Would you like to add this new App to a Multi-Event App?</h3>
				        		<div class="col-sm-12">
									<a href="javascript:void(0);" class="btn btn-light-grey btn-block" style="width: 200px;" id="choose_multi_app_btn_click"> Choose a Multi-Event App </a>	        			
				        		</div>
				        		<div class="col-sm-12" style="display: none;" id="dropdown_multievent_app">
				        			<select name="multievent_app_name" id="multievent_app_name">
				        				<option value="">Select Multi-Event App</option>
				        				<?php foreach ($multievent as $key => $value) { ?>
				        					<option value="<?php echo $value['Event_name']; ?>"><?php echo $value['Event_name']; ?></option>
				        				<?php } ?>
				        			</select>        			
				        		</div>
				        		<div class="col-sm-12">
									<a href="javascript:void(0);" class="btn btn-green btn-block" style="width: 200px;" id="no_thanks_app_separately_btn"> No Thanks, I want to launch this App separately </a>	        			
				        		</div>
				        	</div>
				        	<div class="row" id="multi_event_payment_screen" style="display: none;">
				        		<h3> Add this app to <span id="multi_event_name_span"></span> instantly</h3>
				        		<input type="hidden" name="stripeToken" id="stripeToken" value="">
				        		<input type="hidden" name="launch_your_app_type" id="launch_your_app_type" value="0">
				        		<span id="span_price"><i class="fa fa-<?php echo $currency; ?>"></i> 499</span>
				        		<div class="col-sm-12">
									<a href="javascript:void(0);" class="btn btn-green btn-block" style="width: 200px;" id="launch_your_stripe_payment_btn"> Click here to Launch </a>	        	
			        			</div>
				        	</div>
				        </div>
			      	</div>
			    </div>
			</div>
		</form>
	</div>
</div>
<script src="https://www.allintheloop.net/demo/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<?php  if(count($ismulitieventaccount)>0){ 
	//$key="pk_test_oxTGjphCde1zLtOwDSxcjjwr";
	$key=$admin_scret_key[0]['public_key']; ?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#multi_event_point_models').modal('show');
});
var handler = StripeCheckout.configure({
  key: '<?php echo $key; ?>',
  image: '<?php echo base_url(); ?>assets/images/stripe_logo.png',
  locale: 'auto',
  token: function(token) {
  	$('#launch_your_app_type').val('2');
    jQuery('#stripeToken').val(token.id);
  }
});
$('#choose_multi_app_btn_click').click(function(){
	$('#dropdown_multievent_app').show();
});
$('#multievent_app_name').change(function(){
	$('#multi_event_name_span').html($(this).val());
	$('#multi_event_payment_screen').show();
	$('#choose_launch_your_app_btn_div').hide();
});
$('#no_thanks_app_separately_btn').click(function(){
	$('#launch_your_app_type').val('1');
	$('#multi_event_point_models').modal('hide');
});
$('#launch_your_stripe_payment_btn').click(function(){
	$('#multi_event_point_models').modal('hide');
	handler.open({
	    name: 'Allintheloop.com',
	    description: 'All In The Loop Software',
	    amount: "<?php echo 499*100; ?>",
	    currency:'<?php echo $currency; ?>'
	});
	e.preventDefault();
});
</script>
<?php } ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$("#addeventform").validate({
	errorElement: "label",
	errorClass: 'help-block',  
	rules: {
	  Event_name:{
	  	required:true,
	  	minlength:3,
	  }
	},
	highlight: function (element) 
	{
	  $(element).closest('.help-block').removeClass('valid');
	  $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
	},
	unhighlight: function (element) {
	  $(element).closest('.form-group').removeClass('has-error');
	},
	submitHandler: function(form) {
		$('#save_app_name_btn').attr('disabled','disabled');
    	$('#addeventform').submit();
    }
});
</script>	