<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
                
                <?php if($this->session->flashdata('session_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Q&A Session <?php echo $this->session->flashdata('session_data'); ?> Successfully.
                </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#banner_list" data-toggle="tab">
                                Banner List
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="banner_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Banner Image</th>
                                        <th>URL</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $b_image = json_decode($event_templates[0]['Images'],true);?>
                                    <?php foreach ($b_image as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1; ?></td>
                                        <td><img width="100%" class="header_div_images_class" src="<?php echo base_url().'assets/user_files/'.$value; ?>" style="height: 100px;width: 150px;"></td>
                                        <td>
                                            <?=$old_banner_url[$value]?>
                                        </td>
                                        <td>
                                            <a class="btn btn-green showmodel" data-toggle="modal" data-image="<?=$value?>" data-url="<?=$old_banner_url[$value]?>">URL</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="banner_url" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Add URL to Banner Image</h4>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Event/save_banner_url/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="url" name="url">
                <input type="hidden" name="image" id="image">
              </div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   $(".showmodel").click(function()
   {
         $("#image").val($(this).data('image'));
         $("#url").val($(this).data('url'));
         $('#banner_url').modal('show');
   });
});
</script>

<!-- end: PAGE CONTENT-->