<?php $acc_name=$this->session->userdata('acc_name');
$iconset=array(
	'1'=>array('name'=>'Agenda','image'=>'1.png'),
	'2'=>array('name'=>'Attendees','image'=>'2.png'),
	'3'=>array('name'=>'Exhibitors','image'=>'3.png'),
	'6'=>array('name'=>'Notes','image'=>'6.png'),
	'7'=>array('name'=>'Speakers','image'=>'7.png'),
	'9'=>array('name'=>'Presentations','image'=>'9.png'),
	'10'=>array('name'=>'Maps','image'=>'10.png'),
	'11'=>array('name'=>'Photos','image'=>'11.png'),
	'12'=>array('name'=>'Private Messaging','image'=>'12.png'),
	'13'=>array('name'=>'Public Messaging','image'=>'13.png'),
	'15'=>array('name'=>'Surveys','image'=>'15.png'),
	'16'=>array('name'=>'Documents','image'=>'16.png'),
	'17'=>array('name'=>'Social','image'=>'17.png'),
	'43'=>array('name'=>'Sponsors','image'=>'43.png'),
	'44'=>array('name'=>'Twitter Feed','image'=>'44.png'),
	'45'=>array('name'=>'Activity','image'=>'45.png'),
	'46'=>array('name'=>'Instagram Feed','image'=>'46.png')
	/*'47'=>array('name'=>'Facebook Feed','image'=>'47.png'),
	'48'=>array('name'=>'Virtual Supermarket','image'=>'48.png'),
	'49'=>array('name'=>'My Favorites','image'=>'49.png'),
	'50'=>array('name'=>'Q&A','image'=>'50.png')*/
	);
	$activemenu=explode(",",$event['checkbox_values']);
	$iconset1=array_intersect_key($iconset, array_flip($activemenu));
	$homescreen_active=array_column($active_icon,'menu_id');
	$iconset1=array_intersect_key($iconset,array_flip($homescreen_active));
	$image=json_decode($event['Images'],true); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/newcmseventpage.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
<style type="text/css">
.menu ul li:hover{background-color: <?php echo $event['menu_hover_background_color']; ?> !important;}
#icon_crop_logo img
{
	max-width: 100%;
}
.col-sm-2.ui-draggable.ui-draggable-handle {
  z-index: 99;
}
.inner-wrap-lft {
  border-right: 1px solid #ccc;
  float: left;
  height: 100%;
  width: 85%;
}
.inner-wrap-rgt {
  background: #359CE0 none repeat scroll 0 0;
  float: right;
  height: auto;
  padding: 10px 5px;
  text-align: center;
  width: 15%;
}
</style>
<div class="col-sm-12">
	<div class="panel panel-body">
		<?php if($user->Role_name=='Client' || $user->Rid==103){ ?>
		<form action="<?php echo base_url().'Event/get_formdata/'.$event['Id']; ?>" method="post" enctype="multipart/form-data" id="eventedit_form">
		<section id="app_name_section" class="show app_section">
			<h2 align="center">Name Your App</h2>
			<div class="row">
	        	<div class="alert alert-success" id="success_msg_div" style="display: none;">
					App Name Save SuccessFully.
				</div>
				<div class="alert alert-danger" id="error_msg_div" style="display: none;">
					<span id="error_msg_span"></span>
				</div>
	        </div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
		                <div class="col-sm-12">
		                    <input type="text" value="<?php echo $event['Event_name']; ?>" placeholder="Enter Your app name" id="Event_name" name="Event_name" class="form-control name_group">
		                    <input type="hidden" name="id" id="Id" value="<?php echo $event['Id']; ?>">
		                    <span id="App_name_error_span" class="help-block" style="display:none;" for="App_name">This field is required.</span>
		                </div>
		            </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
		            <button id="save_app_name_btn" type="button" class="btn btn-theme_green btn-block">
		                Save
		            </button>
		        </div>
	        </div>
        </section>
        <section id="app_images_section" class="hide app_section">
        	<h2 align="center">App Images</h2>
        	<div class="row">
        		<a style="top:7px;" class="btn btn-primary list_page_btn ex_btn" href="<?=base_url()?>Event/add_url_to_banner/<?=$event['Id']?>"><i class="fa fa-plus"></i> Add URL to Banner</a>
	        	<div class="alert alert-success" id="success_msg_image_div" style="display: none;">
					Your new images are now active.
				</div>
				<div class="alert alert-danger" id="error_msg_image_div" style="display: none;">
					<span id="error_msg_image_span"></span>
				</div>
	        </div>
        	<div class="col-sm-9">
        		<div class="center_side_form_live_device_preview col-sm-12">
					<div class="iphone-screen2">
		            	<div class="inner-wrap">
		            	<div class="logo" id="logo_image_area_div">
		            		<?php if(!empty($event['Logo_images'])){$placeholder="style='display:none;'";}else{$logostyle="style='display:none;'";} ?>
		                	<div class="placeholder-text placeholder_text_div_class" <?php echo $placeholder; ?>>Click here to add your Logo</div>
		                   <img id="logo_preview_change" class="logo_preview_change_class" src="<?php echo base_url().'assets/user_files/'.$event['Logo_images']; ?>" <?php echo $logostyle; ?>>
		                </div>
		                <div class="logo-tagline">
		                   	<a href="javascript:void(0);" id="app_name_link_show" class="app_name_link"><?php echo $event['Event_name']; ?></a>
		                </div>
		                <div class="menu">
		                	<ul>
		                		<?php $notshow=array(8,21,26,27,28); foreach ($menu_list as $key => $value) { 
		                			if(!in_array($value['id'],$notshow)){
		                			?>
		                    	<li style="color:<?php echo $event['menu_text_color']; ?>;background-color: <?php echo $event['menu_background_color']; ?>;"><?php echo $value['menuname']; ?></li>
		                    	<?php } } ?>
		                    </ul>
		                </div>
		                </div>
		            </div>
		            <div class="iphone-screen1">
		            <div class="inner-wrap">
		            	<div class="header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
		            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
		                	<!-- <i class="fa fa-th-large" style="color:<?php echo $event['Top_text_color']; ?>;"></i> -->
		                    <span class="text" style="color:<?php echo $event['Top_text_color']; ?>;">Login</span>
		                </div>
		                <div class="banner" id="banner_image_area_div">
		                	<div id="banner_image_contener_div" class="banner_list_owl owl-carousel owl-theme loop">
		                    	<?php if(count($image)>0){ foreach ($image as $key => $value) { ?>
			                    	<div class="row" id="banner_item_div_<?php echo $key+1 ?>">
					                    <img width="100%" class="<?php echo $value ?> header_div_images_class" src="<?php echo base_url().'assets/user_files/'.$value; ?>">
					                    <input type="hidden" value="<?php echo $value; ?>" name="old_baaner_image_name[<?php echo $key+1 ?>]">
					                    <input type="hidden" value="" name="header_images_crope_text[<?php echo $key+1 ?>]" class="header_images_crope_textbox_<?php echo $key+1 ?>">
					                </div>
				                <?php } }else{ ?>
				                <div id="placeholderdivcontent" class="placeholder-text header_placeholder_div_class">Click here to add your banner</div>
				                <?php } ?>
		                    </div>
		                </div>
		                <div class="home-screen-text">
		                	<?php echo $event['Description']; ?>
		                </div>
		                <div class="icon-list">
		                	<ul class="iconset_one_icon">
		                		<?php foreach ($iconset1 as $key => $value) { 
		                			if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_two_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>	
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_thard_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                </div>
		            </div>
		            </div>
				</div>
        	</div>
        	<div class="col-sm-3" style="margin-left: -55px;">
        		<div class="col-sm-12" style="margin-bottom: 15px;">
        			<div class="modal fade" id="logomodelscopetool" role="dialog">
				    <div class="modal-dialog modal-md">
				    <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close close_logo_popup" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Logo</h4>
				        </div>
				        <div class="modal-body" id="show_logoupload_div_data">
				        	<div class="row">
				            	<div id="preview_logo_crop_tool">
				            		<img class="img-responsive" id="show_crop_img_model" src="" alt="Picture">
				            	</div>
				            	<div class="col-sm-12 crop-btn">
				            		<button class="btn btn-green btn-block" type="button" id="upload_result_btn_crop" data-dismiss="modal">Crop</button>   
				            	</div>
				            </div>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default close_logo_popup" data-dismiss="modal">Exit crop tool</button>
				        </div>
				    </div>
				    </div>
				</div>
					<div data-provides="fileupload" class="fileupload fileupload-new">
						<span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Edit Logo Image</span><span class="fileupload-exists">Change</span>
							<input type="file" name="logo_image" id="logo_image">
						</span>
						<div class="file_upload_div">
						<span class="fileupload-preview"></span>
						<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
							&times;
						</a>
						</div>
					</div>
					<input type="hidden" id="logo_crope_images_text" name="logo_crope_images_text" value="">
				</div>
        		<div class="col-sm-12">
        			<div class="modal fade" id="headermodelscopetool" role="dialog">
					    <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Banner Image</h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row" style="margin-bottom: 15px;margin-right: 10px;">
						        		<div class="header_crop_btn pull-right">
						            		<button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" data-dismiss="modal">Crop to Save</button>   
						            	</div>
						            </div>
					                <div class="row"  id="cropping_banner_div">
						            	<div id="preview_header_crop_tool">
						            		<img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
						            	</div>
						            </div>
						        </div>
						    </div>
					    </div>
					</div>
					<div id="header_images_contener">
						<?php if(count($image)>0){ foreach ($image as $key => $value) { ?>
						<div data-provides="fileupload" class="fileupload fileupload-exists">
							<span class="btn btn-file btn-light-grey">
								<i class="fa fa-folder-open-o"></i> 
								<span class="fileupload-new">Edit Header Image</span>
								<span class="fileupload-exists">Change</span>
								<input type="file" name="header_image['<?php echo $key+1; ?>']" onchange="geaderimageupload(this,'<?php echo $key+1; ?>');">
							</span>
							<div class="file_upload_div">
								<span class="fileupload-preview"><?php echo $value; ?></span>
								<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#" onclick="delete_banner_image(this,'<?php echo $key+1; ?>','<?php echo $value; ?>');">
									&times;
								</a>
							</div>
						</div>
						<?php } }else{ ?>
						<div data-provides="fileupload" class="fileupload fileupload-new">
							<span class="btn btn-file btn-light-grey">
								<i class="fa fa-folder-open-o"></i> 
								<span class="fileupload-new">Edit Header Image</span>
								<span class="fileupload-exists">Change</span>
								<input type="file" name="header_image[1]" onchange="geaderimageupload(this,'1');">
							</span>
							<div class="file_upload_div">
								<span class="fileupload-preview"></span>
								<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#" onclick="delete_banner_image(this,'1','');">
									&times;
								</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="row" <?php if(count($image)<=0){ ?> style="display: none;" <?php } ?> id="add_extra_image_button_div">
					<div class="col-sm-12" style="padding-left: 0px;">
			            <button type="button" id="add_extra_images_button" class="btn btn-theme_green">
			                Add Extra Header Image to create a Sliding Banner
			            </button>
			        </div>
				</div>
				<div class="row">
		        	<div class="col-sm-9" style="padding-left: 0px;">
			            <button type="submit" id="save_app_image_btn" class="btn btn-theme_green btn-block">
			                Save <i class="fa fa-arrow-circle-right"></i>
			            </button>
			        </div>
	        	</div>
        	</div>
        </section>
        <section id="app_colors_section" class="hide app_section">
        	<h2 align="center">Colors</h2>
        	<div class="row">
	        	<div class="alert alert-success" id="success_msg_color_div" style="display: none;">
					Your new color scheme is now active.
				</div>
				<div class="alert alert-danger" id="error_msg_color_div" style="display: none;">
					<span id="error_msg_color_span"></span>
				</div>
	        </div>
        	<div class="col-sm-12">
	        	<div class="col-sm-4">
	        		<h3>Appearance</h3>
	        		<div class="form-group color-pick">
						<label for="form-field-1" class="control-label col-sm-12">Pick a Color Scheme : </label>
						<div class="col-sm-12">
							<?php $color_arr=array('#e64d43_#bf3a31','#e47f31_#d05419','#fecc2f_#fda729','#efdeb6_#d4c297','#35495d_#2e3e4f','#2b2b2b_#262626','#995cb3_#8c48ab','#3c6f80_#366271','#3b99d8_#2f80b6','#38ca73_#30ad64','#2abb9b_#239f86','#ecf0f1_#bec3c7','#95a5a5_#7e8c8d','#365e42_#2f5038','#7460c2_#5b4ba0','#5e4535_#513b2e','#5d345d_#4e2c4e','#ed737c_#d7555c','#a6c446_#90af30','#f27ec2_#d25e9d','#78302c_#652724','#a18672_#8d725e','#b8caef_#9aabd3','#5266a0_#394d7f');
							foreach ($color_arr as $key => $value) { ?>
							<div class="col-sm-2">
			                    <div>
				                    <input type="radio" value="<?php echo $value; ?>" <?php if($key==8){ ?> checked="checked" <?php } ?>  name="custom_color_picker" id="radio<?php echo $key; ?>">
				                    <label class="control-label col-sm-12" for="radio<?php echo $key; ?>" style="background-color: <?php $arr=explode("_",$value); echo $arr[0]; ?>"></label>
			                    </div>
			                </div>
			                <?php } ?>
						</div>
					</div>
		            <h3>Edit Colors</h3>
					<div class="form-group">
						<label for="form-field-1" class="control-label col-sm-12">Header Color : </label>
						<div class="col-sm-12">
							<input type="text" name="header_color" id="header_color" value="<?php echo $event['Top_background_color']; ?>" class="color {hash:true} form-control name_group">
						</div>
					</div>
					<div class="form-group">
						<label for="form-field-1" class="control-label col-sm-12">Header Text Color : </label>
						<div class="col-sm-12">
							<input type="text" name="header_text_color" id="header_text_color" value="<?php echo $event['Top_text_color']; ?>" class="color {hash:true} form-control name_group">
						</div>
					</div>
					<div class="form-group">
						<label for="form-field-1" class="control-label col-sm-12">Left Hand Menu Color : </label>
						<div class="col-sm-12">
							<input type="text" name="left_hand_menu_color" value="<?php echo $event['menu_background_color']; ?>" id="left_hand_menu_color" class="color {hash:true} form-control name_group">
						</div>
					</div>
					<div class="form-group">
						<label for="form-field-1" class="control-label col-sm-12">Left Hand Menu Text Color : </label>
						<div class="col-sm-12">
							<input type="text" name="left_hand_menu_text_color" value="<?php echo $event['menu_text_color']; ?>" id="left_hand_menu_text_color" class="color {hash:true} form-control name_group">
						</div>
					</div>
					<div class="row">
			        	<div class="col-sm-12" style="padding-left: 0px;">
				            <button type="button" id="save_app_color_btn" class="btn btn-theme_green btn-block">
				                Save
				            </button>
				        </div>
			        </div>
		        </div>
	        	<div class="col-sm-8">
	        		<div class="center_side_form_live_device_preview col-sm-12">
					<div class="iphone-screen2">
		            	<div class="inner-wrap">
		            	<div class="logo">
		            		<?php if(!empty($event['Logo_images'])){$placeholder="style='display:none;'";}else{$logostyle="style='display:none;'";} ?>
		                	<div class="placeholder-text placeholder_text_div_class" <?php echo $placeholder; ?>>Click here to add your Logo</div>
		                   <img id="logo_preview_change" class="logo_preview_change_class" src="<?php echo base_url().'assets/user_files/'.$event['Logo_images']; ?>" <?php echo $logostyle; ?>>
		                </div>
		                <div class="logo-tagline">
		                   	<a href="javascript:void(0);" id="app_name_link_show" class="app_name_link"><?php echo $event['Event_name']; ?></a>
		                </div>
		                <div class="menu">
		                	<ul>
		                		<?php $notshow=array(8,21,26,27,28); foreach ($menu_list as $key => $value) { 
		                		if(!in_array($value['id'],$notshow)){
		                			?>
		                    	<li style="color:<?php echo $event['menu_text_color']; ?>;background-color: <?php echo $event['menu_background_color']; ?>;"><?php echo $value['menuname']; ?></li>
		                    	<?php } } ?>
		                    </ul>
		                </div>
		                </div>
		            </div>
		            <div class="iphone-screen1">
		            <div class="inner-wrap">
		            	<div class="header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
		            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
		                	<!-- <i class="fa fa-th-large" style="color:<?php echo $event['Top_text_color']; ?>;"></i> -->
		                    <span class="text" style="color:<?php echo $event['Top_text_color']; ?>;">Login</span>
		                </div>
		                <div class="banner">
		                	<div class="banner_list_owl owl-carousel owl-theme loop">
		                		<?php if(count($image)>0){ foreach ($image as $key => $value) { ?>
		                    	<div class="row">
				                    <img width="100%" class="header_div_images_class" src="<?php echo base_url().'assets/user_files/'.$value; ?>">
				                </div>
			                <?php } }else{ ?>
			                <div class="placeholder-text header_placeholder_div_class">Click here to add your banner</div>
			                <?php } ?>
		                    </div>
		                </div>
		                <div class="home-screen-text">
		                	<?php echo $event['Description']; ?>
		                    <!-- <p id="show_home_screen_text_p">Click here to add your home screen text</p> -->
		                </div>
		                <div class="icon-list">
		                	<ul class="iconset_one_icon">
		                		<?php foreach ($iconset1 as $key => $value) { 
		                			if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_two_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>	
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_thard_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                </div>
		            </div>
		            </div>
				</div>
	        	</div>
	        </div>	 
        </section>
        <section id="app_icons_section" class="hide app_section">
        	<h2 align="center">Icons</h2>
        	<div class="row">
	        	<div class="alert alert-success" id="success_msg_icon_div" style="display: none;">
					Your new icons are now active
				</div>
				<div class="alert alert-danger" id="error_msg_icon_div" style="display: none;">
					<span id="error_msg_icon_span"></span>
				</div>
	        </div>
        	<div class="col-sm-12">
	        	<div class="col-sm-6">
	        		<label for="form-field-1" class="control-label col-sm-12">Choose your icon Set : </label>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="col-sm-4" id="open_to_everyone_div">
			                    <div>
				                    <input type="radio" value="1" <?php if($event['icon_set_type']=='1'){ ?>checked="checked" <?php } ?>  name="icon_set_type" id="radio01">
				                    <label for="radio01">
				                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-1.jpg'; ?>">
				                    </label>
			                    </div>
			                </div>
			                <div class="col-sm-4" id="requires_login_div">
			                    <div>
				                    <input type="radio" value="2" <?php if($event['icon_set_type']=='2'){ ?> checked="checked" <?php } ?> name="icon_set_type" id="radio02">
				                    <label for="radio02">
				                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-2.jpg'; ?>">
				                    </label>
			                    </div>
			                </div>
			                <div class="col-sm-4" id="private_div">
			                    <div>
				                    <input type="radio" value="3" name="icon_set_type" <?php if($event['icon_set_type']=='3'){ ?>checked="checked" <?php } ?> id="radio03">
				                    <label for="radio03">
				                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-3.jpg'; ?>">
				                    </label>
			                    </div>
			                </div>
						</div>
					</div>
					<label for="form-field-1" class="control-label icon_back_color col-sm-12">Icon Background color : </label>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" name="icon_backgroung_color" value="<?php echo $event['Background_color']; ?>" id="icon_backgroung_color" class="color {hash:true} form-control name_group">
						</div>
					</div>
					<div class="col-sm-12">
						<h4> Or Upload custom icons manually by clicking on an icon you want to change.</h4>
					</div>
					<div class="row">
			        	<div class="col-sm-12">
				            <button type="button" id="save_app_icon_btn" class="btn btn-theme_green btn-block">
				                Save
				            </button>
				        </div>
			        </div>
	        	</div>
	        	<div class="col-sm-6">
	        		<div id="icone_name_popup" class="modal fade"  role="dialog">
					    <div class="modal-dialog modal-md">
						    <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close close_icon_data_popup" data-dismiss="modal">&times;</button>
						        </div>
						        <div class="modal-body">
						        	<div class="row">
						        		<div class="col-sm-12">
						        		<input type="hidden" id="icon_id_popup_text_box"  name="icon_id_popup_text_box">
						        		<input type="text" class="form-control name_group" id="icon_name_popup_text_box"  name="icon_name_popup_text_box"></div>
						        		<div class="col-sm-12">
						        		<label><input type="checkbox" name="create_home_screen_tab" id="create_home_screen_tab"> Create Home Screen Tabs</label></div>
						        		<div class="col-sm-12">
						        		<input type="text" value="" id="icon_backgroung_color_popup" class="color {hash:true} form-control name_group">
						        		</div>
						        	</div>
						        	<div class="row" id="icon_file_upload">
						        		<input type="file" name="icon_image" id="icon_image">
						        	</div>
						        	<div class="row" id="icon_crop_logo" style="display: none;">
						        		<img src="" id="icon_crop_logo_img">
						        		<div class="col-sm-12">
						        			<button id="icon_crop_button_in_models" type="button" class="btn btn-blue btn-block" data-dismiss="modal">Crop To Save</button>
						        		</div>
						        	</div>
						        	<div class="row" id="save_data_button_div">
						        		<div class="col-sm-12">
						        			<button id="icon_models_save_data" type="button" class="btn btn-blue btn-block" data-dismiss="modal">Save</button>
						        		</div>
						        	</div>
						        </div>
						    </div>
					    </div>
					</div>
	        		<div class="center_side_form_live_device_preview col-sm-12">
		            <div class="iphone-screen1">
		            <div class="inner-wrap">
		            	<div class="header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
		            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
		                	<!-- <i class="fa fa-th-large" style="color:<?php echo $event['Top_text_color']; ?>;"></i> -->
		                    <span class="text" style="color:<?php echo $event['Top_text_color']; ?>;">Login</span>
		                </div>
		                <div class="banner">
		                	<div class="banner_list_owl owl-carousel owl-theme loop">
		                    	<?php if(count($image)>0){ foreach ($image as $key => $value) { ?>
		                    	<div class="row">
				                    <img width="100%" class="header_div_images_class" src="<?php echo base_url().'assets/user_files/'.$value; ?>">
				                </div>
			                <?php } }else{ ?>
			                <div class="placeholder-text header_placeholder_div_class">Click here to add your banner</div>
			                <?php } ?>
		                    </div>
		                </div>
		                <div class="home-screen-text">
		                	<?php echo $event['Description']; ?>
		                </div>
		                <div class="icon-list">
		                	<ul class="iconset_one_icon">
		                		<?php foreach ($iconset1 as $key => $value) { 
		                			if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>");' style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<input type="hidden" value="<?php echo $value['name']; ?>" name="modules_name_<?php echo $key ?>" id="modules_name_<?php echo $key ?>">
		                    		<input type="hidden" value="0" name="create_home_screen_tab_<?php echo $key ?>" id="create_home_screen_tab_<?php echo $key ?>">
		                    		<input type="hidden" class="icon_background_color_class" value="#FFFFFF" name="home_screen_tab_back_color_<?php echo $key ?>" id="home_screen_tab_back_color_<?php echo $key ?>">
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>")' style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<input type="hidden" value="<?php echo $home_screen_tab[$key]['title']; ?>" name="modules_name_<?php echo $key ?>" id="modules_name_<?php echo $key ?>">
	                    			<input type="hidden" value="<?php echo $home_screen_tab[$key]['is_feture_product']; ?>" name="create_home_screen_tab_<?php echo $key ?>" id="create_home_screen_tab_<?php echo $key ?>">
	                    			<input type="hidden" class="icon_background_color_class" value="<?php echo $home_screen_tab[$key]['Background_color']; ?>" name="home_screen_tab_back_color_<?php echo $key ?>" id="home_screen_tab_back_color_<?php echo $key ?>">
	                    			<?php } ?>
	                    			<input type="hidden" value="" name="modules_crop_image_<?php echo $key ?>" id="modules_crop_image_<?php echo $key ?>">
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_two_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>")' style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>	
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>")' style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_thard_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>")' style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" onclick='show_modules_edit_popup("<?php echo $key ?>")' style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                </div>
		            </div>
		            </div>
	        	</div>
	        </div>	
        </section>
        <section id="app_home_screen_text_section" class="hide app_section">
        	<h2 align="center">Home Screen Text</h2>
        	<div class="row">
	        	<div class="alert alert-success" id="success_msg_home_content_div" style="display: none;">
					Your new home screen content is now active.
				</div>
				<div class="alert alert-danger" id="error_msg_home_content_div" style="display: none;">
					<span id="error_msg_home_content_span"></span>
				</div>
	        </div>
        	<div class="col-sm-12">
        		<div class="col-sm-6">
        			<textarea name="edit_home_screen_content" id="edit_home_screen_content" class="form-control"><?php echo $event['Description']; ?></textarea>
        			<div class="row" style="margin-top: 15px;">
				      	<div class="col-sm-4">
							<button type="button" name="submit_home_screen_text" id="submit_home_screen_text" class="btn btn-green btn-block">Preview</button>
						</div>
					</div>
					<div class="row" align="center" style="margin-top: 5px;">
			        	<div class="col-sm-4">
				            <!-- <button type="button" id="save_app_home_screen_text_btn" class="btn btn-theme_green btn-block">
				                Save
				            </button> -->
				            <button type="submit" class="btn btn-theme_green btn-block">
				                Save
				            </button>
				        </div>
			        </div>
        		</div>
        		<div class="col-sm-6">
        			<div class="center_side_form_live_device_preview col-sm-12">
		            <div class="iphone-screen1">
		            <div class="inner-wrap">
		            	<div class="header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
		            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
		                	<!-- <i class="fa fa-th-large" style="color:<?php echo $event['Top_text_color']; ?>;"></i> -->
		                    <span class="text" style="color:<?php echo $event['Top_text_color']; ?>;">Login</span>
		                </div>
		                <div class="banner">
		                	<div class="banner_list_owl owl-carousel owl-theme loop">
		                    	<?php if(count($image)>0){ foreach ($image as $key => $value) { ?>
		                    	<div class="row">
				                    <img width="100%" class="header_div_images_class" src="<?php echo base_url().'assets/user_files/'.$value; ?>">
				                </div>
				                <?php } }else{ ?>
				                <div class="placeholder-text header_placeholder_div_class">Click here to add your banner</div>
				                <?php } ?>
		                    </div>
		                </div>
		                <div class="home-screen-text">
		                	<?php echo $event['Description']; ?>
		                </div>
		                <div class="icon-list">
		                	<ul class="iconset_one_icon">
		                		<?php foreach ($iconset1 as $key => $value) { 
		                			if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_two_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>	
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                    <ul class="iconset_thard_icon" style="display: none;"> 
		                    	<?php foreach ($iconset1 as $key => $value) { 
		                    		if(!array_key_exists($key,$home_screen_tab)){
		                		?>
		                    		<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $event['Background_color']; ?>;">
		                    			<div class="event-img">
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
		                    			</div>
		                    		</li>
		                    		<?php }else{ ?>
	                    			<li class="home_tab_modules_li_<?php echo $key; ?>" style="background:<?php echo $home_screen_tab[$key]['Background_color']; ?>;">
	                    				<div class="event-img">
	                    					<?php if(empty($home_screen_tab[$key]['img'])){ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
		                    				<?php }else{ ?>
		                    				<img class="module_image_show_<?php echo $key; ?>" src="<?php echo base_url().'assets/user_files/'.$home_screen_tab[$key]['img']; ?>" alt="">
		                    				<?php } ?>
		                    			</div>	
		                    			<div class="event-tile">
		                    				<p class="module_title_<?php echo $key; ?>"><?php echo $home_screen_tab[$key]['title']; ?></p>
		                    			</div>
	                    			</li>
	                    			<?php } ?>		
		                    	<?php } ?>
		                    </ul>
		                </div>
		            </div>
		            </div>
        		</div>
        	</div>
        </section>
        </form>
        <section id="app_advanced_design_section" class="hide app_section">
        	<div class="tabbable">
        		<ul id="myTab6" class="nav nav-tabs">
        			<li class="active">
        				<a href="#tab_advanced_design" id="tab_advanced_design_a_tag" data-toggle="tab">
        					Advanced Design
        				</a>
        			</li>
        			<li>
        				<a href="#tab_lefthandmenu_advanced_design" id="tab_lefthandmenu_advanced_design_a_tag" data-toggle="tab">
        					Left Hand Menu
        				</a>
        			</li>
        		</ul>
        		<div class="tab-content">
        			<div class="tab-pane fade in active" id="tab_advanced_design">
        				<form action="<?php echo base_url().'Event/save_advance_design_image_content/'.$event['Id']; ?>" method="post" enctype="multipart/form-data" id="event_advanced_design_form">
				        <div class="row">
					        <div class="col-sm-12">
					        	<div class="col-sm-6">
					        		<h2>Advanced Design</h2>
					        		<p>Advanced Design allows you to personalize your home screen using custom images. You can upload images you want to appear on your home screen and then use your mouse to drag and drop areas you want to be clickable.</p>
					        		<p>Once you have dragged and dropped using your mouse you can choose which feature you would like to link the area to or link it to a webpage.</p>
					        		<div class="col-sm-5" style="margin-top:15px;padding-left: 0px;">
										<a href="javascript:void(0);" class="btn btn-green btn-block add_new_section">
											Add New Section <i class="fa fa-plus"></i> 
										</a>
									</div>
									<div class="col-sm-4" style="margin-top:15px;">
										<a href="javascript:void(0);" id="submit_advance_design_button" class="btn btn-yellow btn-block" data-toggle="modal" data-target="#confirm_dialog_div">
											Submit
										</a>
									</div>
					        		<div class="row" id="all_filesupload_contener_div">
					        			<?php if(count($banner_list) > 0) { foreach ($banner_list as $key => $value) { ?> 
					        			<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="image_upload_div_<?php echo $key ?>">
					        				<div class="file_upload_div images_upload_image_name_heding">
												<span class="fileupload-preview"><?php echo $value['Image']; ?></span>
											</div>
											<div class="col-sm-8">
								        		<div data-provides="fileupload" class="fileupload fileupload-exists">
													<span class="btn btn-file btn-light-grey">
														<i class="fa fa-folder-open-o"></i> 
														<span class="fileupload-new">Click Here To Upload An Image</span>
														<span class="fileupload-exists">Change</span>
														<input type="file" name="advaicon_images" onchange="croppingadvanceicon(this,'<?php echo $key ?>');">
													</span>
												</div>
												<div class="add_edit_text_button_div col-sm-6 pad-left-no">
													<a href="javascript:void(0);" onclick="get_add_text_btn(this,'<?php echo $key ?>');" class="btn btn-green btn-block">
														Add/Edit Text
													</a>
												</div>
												<div class="col-sm-6 pad-left-no">
													<a href="javascript:void(0);" onclick="remove_image_upload_div(this,'<?php echo $key ?>');" class="btn btn-red btn-block remove_section">
														Delete Section
													</a>
												</div>
											</div>
											<div class="col-sm-4">
												<img class="image_upload_div_preview_advance_image_demo" width="100%" src="<?php echo base_url().'assets/user_files/'.$value['Image']; ?>">	
											</div>
										</div>
					        			<?php } }else{ ?>
						        		<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="image_upload_div_0">
						        			<div class="file_upload_div images_upload_image_name_heding">
												<span class="fileupload-preview">...</span>
											</div>
											<div class="col-sm-8"> 
								        		<div data-provides="fileupload" class="fileupload fileupload-new">
													<span class="btn btn-file btn-light-grey">
														<i class="fa fa-folder-open-o"></i> 
														<span class="fileupload-new">Click Here To Upload An Image</span>
														<span class="fileupload-exists">Change</span>
														<input type="file" name="advaicon_images" onchange="croppingadvanceicon(this,'0');">
													</span>
												</div>
												<div class="add_edit_text_button_div col-sm-6 pad-left-no" style="display:none;">
													<a href="javascript:void(0);" onclick="get_add_text_btn(this,'0');" class="btn btn-green btn-block">
														Add/Edit Text
													</a>
												</div>
												<input type="hidden" name="advaicon_images_crope_text[0]" class="advaicon_images_crope_data" value="">
												<input type="hidden" name="advaicon_images_content_textbox[0]" class="advaicon_images_content_data" id="advaicon_images_content_textbox_0" value="">
												<div class="col-sm-6 pad-left-no">
													<a href="javascript:void(0);" onclick="remove_image_upload_div(this,'0');" class="btn btn-red btn-block remove_section">
														Delete Section
													</a>
												</div>
											</div>
											<div class="col-sm-4">
												<img class="image_upload_div_preview_advance_image_demo" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
											</div>	
										</div>
										<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="image_upload_div_1">
											<div class="file_upload_div images_upload_image_name_heding">
												<span class="fileupload-preview">...</span>
											</div>
											<div class="col-sm-8">
								        		<div data-provides="fileupload" class="fileupload fileupload-new">
													<span class="btn btn-file btn-light-grey">
														<i class="fa fa-folder-open-o"></i> 
														<span class="fileupload-new">Click Here To Upload An Image</span>
														<span class="fileupload-exists">Change</span>
														<input type="file" name="advaicon_images" onchange="croppingadvanceicon(this,'1');">
													</span>
												</div>
												<div class="add_edit_text_button_div col-sm-6 pad-left-no" style="display:none;">
													<a href="javascript:void(0);" onclick="get_add_text_btn(this,'1');" class="btn btn-green btn-block">
														Add/Edit Text
													</a>
												</div>
												<input type="hidden" name="advaicon_images_crope_text[1]" class="advaicon_images_crope_data" value="">
												<input type="hidden" name="advaicon_images_content_textbox[1]" class="advaicon_images_content_data" id="advaicon_images_content_textbox_1" value="">
												<div class="col-sm-6 pad-left-no">
													<a href="javascript:void(0);" onclick="remove_image_upload_div(this,'1');" class="btn btn-red btn-block remove_section">
														Delete Section
													</a>
												</div>
											</div>
											<div class="col-sm-4">
												<img class="image_upload_div_preview_advance_image_demo" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
											</div>	
										</div>
										<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="image_upload_div_2">
											<div class="file_upload_div images_upload_image_name_heding">
												<span class="fileupload-preview">...</span>
											</div>
											<div class="col-sm-8">
								        		<div data-provides="fileupload" class="fileupload fileupload-new">
													<span class="btn btn-file btn-light-grey">
														<i class="fa fa-folder-open-o"></i> 
														<span class="fileupload-new">Click Here To Upload An Image</span>
														<span class="fileupload-exists">Change</span>
														<input type="file" name="advaicon_images" onchange="croppingadvanceicon(this,'2');">
													</span>
												</div>
												<div class="add_edit_text_button_div col-sm-6 pad-left-no" style="display:none;">
													<a href="javascript:void(0);" onclick="get_add_text_btn(this,'2');" class="btn btn-green btn-block">
														Add/Edit Text
													</a>
												</div>
												<input type="hidden" name="advaicon_images_crope_text[2]" class="advaicon_images_crope_data" value="">
												<input type="hidden" name="advaicon_images_content_textbox[2]" class="advaicon_images_content_data" id="advaicon_images_content_textbox_2" value="">
												<div class="col-sm-6 pad-left-no">
													<a href="javascript:void(0);" onclick="remove_image_upload_div(this,'2');" class="btn btn-red btn-block remove_section">
														Delete Section
													</a>
												</div>
											</div>
											<div class="col-sm-4">
												<img width="100%" class="image_upload_div_preview_advance_image_demo" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
											</div>	
										</div>
										<?php } ?>
									</div>
					        	</div>
					        	<div class="col-sm-6">
				        			<div class="center_side_form_live_device_preview col-sm-12">
							            <div class="iphone-screen1">
								            <div class="inner-wrap">
								            	<div class="header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
								            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
								                	<!-- <i class="fa fa-th-large" style="color:<?php echo $event['Top_text_color']; ?>;"></i> -->
								                    <span class="text" style="color:<?php echo $event['Top_text_color']; ?>;">Login</span>
								                </div>
								                <div class="banner" id="preview_advance_images_div">
								                	<?php foreach ($banner_list as $key => $value) { ?>
								                	<div class="row image_contener_div">
									                	<img width="349" style="height: auto;" data-bannerid="<?php echo $value['id']; ?>" src="<?php echo base_url().'assets/user_files/'.$value['Image']; ?>" class="map1" usemap="#mape_<?php echo $value['id']; ?>">
									                	<map id="mape_<?php echo $value['id']; ?>" name="mape_<?php echo $value['id']; ?>">
									                		<?php foreach ($value['coords'] as $ckey => $cvalue) {
									                			echo '<area alt="Mapping" shape="rect" onclick="delete_area(this,'.$cvalue['id'].')" href="javascript:void(0);" coords="'.$cvalue['coords'].'">';
									                		} ?>
									                	</map>
									                	<div class="image_over_content" id="image_over_content_<?php echo $key; ?>">
									                		<?php echo $value['Content']; ?>	
									                	</div>
								                	</div>
								                	<?php } ?>
								                </div>
								                <div class="home-screen-text"></div>
								                <div class="icon-list" id="droppable_modules" style="min-height: 100px;height:auto;width: 100%;">
								                	<ul class="active_icone_list" id="active_icone_list">
								                	</ul>
								                </div>
								            </div>
								        </div>    
						            </div>
				        		</div>
					        </div>
				        </div>
				        <div class="col-sm-12">
				        	<h2>Icons</h2>
				        	<p>Drag and drop icons from here to the device or from the device to here to add and remove.</p>
				        	<div class="icon-list inactive_icon_list_csss">
					        	<ul class="row" id="sortable_modules">
					        		<?php  foreach ($all_homeicone as $key => $value) { ?>
					        		<li style="background:<?php echo $value['Background_color']; ?>">
					        			<input type="hidden" name="convert_<?php echo $value['id']; ?>" id="convert_<?php echo $value['id']; ?>">
					        			<input type="hidden" name="order_menu_id[]" value="<?php echo $value['id']; ?>">
					        			<a data-toggle="modal" data-target="#icon_assign_modules_link_popup" onclick='openiconmodifi("<?php echo $value['id']; ?>")'>
					        				<div class="event-img">
						        				<?php if(!empty($value['img']) && file_exists('./assets/user_files/'.$value['img'])){ ?>
						        				<img width="100%" src="<?php echo base_url().'assets/user_files/'.$value['img']; ?>"/>
						        				<?php }else{ $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
						        				if(in_array($value['menu_id'],$iconset)) { ?>
						        				<img width="100%" src="<?php echo base_url().'assets/css/images/icons/icon'.$event['icon_set_type'].'/'.$value['menu_id'].'.png'; ?>"/>
						        				<?php }else{ ?>
						        				<img width="100%" src="<?php echo base_url().'assets/images/EventApp_Default.jpg'; ?>"/>
						        				<?php } } ?>
						        			</div>
						        			<div class="event-tile">
						        				<p><?php echo $value['title']; ?></p>
						        			</div>
					        			</a>
					        		</li>	
					        		<?php } ?>
					        	</ul>
				        	</div>
				        </div>
				        </form>
				    </div>
				    <div class="tab-pane fade in" id="tab_lefthandmenu_advanced_design">
				    	<form action="" method="post" enctype="multipart/form-data" id="event_lefthandmenu_advanced_design_form">
					        <div class="row">
						        <div class="col-sm-12">
						        	<div class="col-sm-6">
						        		<h2>Advanced Design (Left Hand Menu)</h2>
						        		<p>Advanced Design allows you to personalize your left hand menu using custom images. You can upload images you want to appear on your left hand menu and then use your mouse to drag and drop areas you want to be clickable.</p>
						        		<p>Once you have dragged and dropped using your mouse you can choose which feature you would like to link the area to or link it to a webpage.</p>
						        		<div class="col-sm-5" style="margin-top:15px;padding-left: 0px;">
											<a href="javascript:void(0);" class="btn btn-green btn-block add_new_section_lefthandmenu">
												Add New Section <i class="fa fa-plus"></i> 
											</a>
										</div>
										 <div class="col-sm-4" style="margin-top:15px;">
											<a href="<?php echo base_url().'Event/edit/'.$event['Id']; ?>" id="submit_lefthandmenu_advance_design_button" class="btn btn-yellow btn-block">
												Submit
											</a>
										</div>
										<div class="row" id="all_filesupload_contener_div_for_lefthandmenu">
											<?php if(count($lefthandmenu_image_list) > 0){ 
												foreach ($lefthandmenu_image_list as $key => $value) { ?>
												<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="lefthandmenu_image_upload_div_<?php echo $key; ?>">
								        			<div class="file_upload_div images_upload_image_name_heding">
														<span class="fileupload-preview"><?php echo $value['Image']; ?></span>
													</div>
													<div class="col-sm-8"> 
										        		<div data-provides="fileupload" class="fileupload fileupload-exists">
															<span class="btn btn-file btn-light-grey">
																<i class="fa fa-folder-open-o"></i> 
																<span class="fileupload-new">Click Here To Upload An Image</span>
																<span class="fileupload-exists">Change</span>
																<input type="file" name="lefthandmenu_advaicon_images" onchange="croppinglefthandmenuadvanceicon(this,'<?php echo $key; ?>');">
															</span>
														</div>
														<div class="col-sm-6 pad-left-no">
															<a href="javascript:void(0);" onclick="lefthandmenu_remove_image_upload_div(this,'<?php echo $key; ?>');" class="btn btn-red btn-block remove_section">
																Delete Section
															</a>
														</div>
													</div>
													<div class="col-sm-4">
														<img class="imageuploaddiv_preview_leftmenuadvanceimage" width="100%" src="<?php echo base_url().'assets/user_files/'.$value['Image']; ?>">	
													</div>	
												</div>
												<?php }
											}else{ ?>
											<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="lefthandmenu_image_upload_div_0">
							        			<div class="file_upload_div images_upload_image_name_heding">
													<span class="fileupload-preview">...</span>
												</div>
												<div class="col-sm-8"> 
									        		<div data-provides="fileupload" class="fileupload fileupload-new">
														<span class="btn btn-file btn-light-grey">
															<i class="fa fa-folder-open-o"></i> 
															<span class="fileupload-new">Click Here To Upload An Image</span>
															<span class="fileupload-exists">Change</span>
															<input type="file" name="lefthandmenu_advaicon_images" onchange="croppinglefthandmenuadvanceicon(this,'0');">
														</span>
													</div>
													<div class="col-sm-6 pad-left-no">
														<a href="javascript:void(0);" onclick="lefthandmenu_remove_image_upload_div(this,'0');" class="btn btn-red btn-block remove_section">
															Delete Section
														</a>
													</div>
												</div>
												<div class="col-sm-4">
													<img class="imageuploaddiv_preview_leftmenuadvanceimage" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
												</div>	
											</div>
											<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="lefthandmenu_image_upload_div_1">
							        			<div class="file_upload_div images_upload_image_name_heding">
													<span class="fileupload-preview">...</span>
												</div>
												<div class="col-sm-8"> 
									        		<div data-provides="fileupload" class="fileupload fileupload-new">
														<span class="btn btn-file btn-light-grey">
															<i class="fa fa-folder-open-o"></i> 
															<span class="fileupload-new">Click Here To Upload An Image</span>
															<span class="fileupload-exists">Change</span>
															<input type="file" name="lefthandmenu_advaicon_images" onchange="croppinglefthandmenuadvanceicon(this,'1');">
														</span>
													</div>
													<div class="col-sm-6 pad-left-no">
														<a href="javascript:void(0);" onclick="lefthandmenu_remove_image_upload_div(this,'1');" class="btn btn-red btn-block remove_section">
															Delete Section
														</a>
													</div>
												</div>
												<div class="col-sm-4">
													<img class="imageuploaddiv_preview_leftmenuadvanceimage" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
												</div>	
											</div>
											<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="lefthandmenu_image_upload_div_2">
							        			<div class="file_upload_div images_upload_image_name_heding">
													<span class="fileupload-preview">...</span>
												</div>
												<div class="col-sm-8"> 
									        		<div data-provides="fileupload" class="fileupload fileupload-new">
														<span class="btn btn-file btn-light-grey">
															<i class="fa fa-folder-open-o"></i> 
															<span class="fileupload-new">Click Here To Upload An Image</span>
															<span class="fileupload-exists">Change</span>
															<input type="file" name="lefthandmenu_advaicon_images" onchange="croppinglefthandmenuadvanceicon(this,'2');">
														</span>
													</div>
													<div class="col-sm-6 pad-left-no">
														<a href="javascript:void(0);" onclick="lefthandmenu_remove_image_upload_div(this,'2');" class="btn btn-red btn-block remove_section">
															Delete Section
														</a>
													</div>
												</div>
												<div class="col-sm-4">
													<img class="imageuploaddiv_preview_leftmenuadvanceimage" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
												</div>	
											</div>
											<?php } ?>
										</div>
						        	</div>
						        	<div class="col-sm-6">
						        		<div class="center_side_form_live_device_preview col-sm-6">
											<div class="iphone-screen2">
								            	<div class="inner-wrap">
								            		<div class="inner-wrap-lft">
										                <div class="menu">
										                	<div id="lefthandmenu_main_contaner_div">
										                		<?php foreach ($lefthandmenu_image_list as $key => $value) { ?>
										                			<div class="row" id="lefthandmenu_image_contener_div_<?php echo $key; ?>">
										                				<img width='95%' style='height: auto;' data-bannerid="<?php echo $value['id']; ?>" class='lefthandmenu_map' usemap="#lefthandmenu_mape_<?php echo $value['id']; ?>" src="<?php echo base_url().'assets/user_files/'.$value['Image']; ?>">
										                				<map id="lefthandmenu_mape_<?php echo $value['id']; ?>" name="lefthandmenu_mape_<?php echo $value['id']; ?>">
										                					<?php foreach ($value['coords'] as $ckey => $cvalue) {
										                					echo '<area alt="Mapping" shape="rect" onclick="delete_area(this,'.$cvalue['id'].')" href="javascript:void(0);" coords="'.$cvalue['coords'].'">';
										                					} ?>
										                				</map>
										                			</div>
										                		<?php } ?>
										                    </div>
										                </div>
									                </div>
									                <div class="inner-wrap-rgt header" style="text-align: left;background:<?php echo $event['Top_background_color']; ?>;">
														<img src="https://www.allintheloop.net/demo/assets/images/menu-button.png" width="20" height="20">
									                </div>
									            </div>    
								            </div>
								        </div>        
						        	</div>
						        </div>
						    </div>
						</form>
				    </div>
				</div>
			</div>	        
        </section>
        <?php }else { ?>
            <div id="viewport" class="iphone">
                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
            </div>
            <img style="position:absolute;top:20%;left:40%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
            <div id="viewport_images" class="iphone-l" style="display:none;">
                   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
            </div>
        <?php } ?>
	</div>
</div>
<div class="modal fade" id="advanceiconemodelscopetool" role="dialog">
    <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close close_advance_icone_popup" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title" id="advanceiconemodal_heading">Icons Image</h4>
	        </div>
	        <div class="modal-body">
	        	<input type="hidden" name="fileuploadnumber" id="fileuploadnumber" value="">
                <div class="row"  id="cropping_advance_icone_div">
	            	<div id="preview_advance_icon_crop_tool">
	            		<img class="img-responsive" id="show_crop_advance_icone_model" src="" alt="Picture">
	            	</div>
	            	<div class="col-sm-12 header_crop_btn">
	            		<a href="javascript:void(0);" style="width:150px; margin: 0 auto;" class="btn btn-green btn-block" id="upload_result_btn_advance_icone_crop">Crop</a>
	            	</div>
	            </div>
	            <div class="row"  align="center" id="loding_icon_div_advance_images_crop" style="display:none;">
	            	<img src="<?php echo base_url(); ?>assets/images/loading.gif" alt="Loding..">
	            	<h3 style="text-align: center;">Loading...</h3>
	            </div>
	        </div>
	    </div>
    </div>
</div>
<div class="modal fade" id="lefthandmenu_advanceiconemodelscopetool" role="dialog">
    <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
	        	<button type="button" class="close close_lefthandmenu_advance_icone_popup" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title" id="lefthandmenu_advanceiconemodal_heading">Left Hand Menu Image</h4>
	        </div>
	        <div class="modal-body">
	        	<input type="hidden" value="" id="lefthandmenu_filesupload_number">
                <div class="row"  id="lefthandmenu_cropping_advance_icone_div">
	            	<div id="lefthandmenu_preview_advance_icon_crop_tool">
	            		<img class="img-responsive" id="show_crop_lefthandmenu_advance_icone_model" src="" alt="Picture">
	            	</div>
	            	<div class="col-sm-12 header_crop_btn">
	            		<a href="javascript:void(0);" style="width:150px; margin: 0 auto;" class="btn btn-green btn-block" id="upload_result_btn_lefthandmenu_advance_icone_crop">Crop</a>
	            	</div>
	            </div>
	            <div class="row"  align="center" id="lefthend_menu_loding_icon_div_advance_images_crop" style="display:none;">
	            	<img src="<?php echo base_url(); ?>assets/images/loading.gif" alt="Loding..">
	            	<h3 style="text-align: center;">Loading...</h3>
	            </div>
	        </div>
	    </div>
    </div>
</div>
<div id="edit_advance_design_image_content_popup" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Text HTML Text to show over your image</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="banner_id_edit_content_popup" id="banner_id_edit_content_popup" value="">
				<input type="hidden" name="imagedividtextbox" id="imagedividtextbox" value="">
				<textarea name="edit_advance_image_content" id="edit_advance_image_content" class="summernote form-control"></textarea>
				<div class="row" style="margin-top: 15px;">
					<div class="col-sm-4">
						<button type="button" name="Update_advance_image_content" id="Update_advance_image_content" class="btn btn-green btn-block">Update</button>
					</div>
					<div class="col-sm-4">
						<button type="button" name="Preview_advance_image_content" id="Preview_advance_image_content" class="btn btn-green btn-block">Preview</button>
					</div>
					<div class="col-sm-4">
						<button type="button" name="Cancel_advance_image_content" id="Cancel_advance_image_content" class="btn btn-grey btn-block">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="preview_image_with_content_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Preview Image with Content</h4>
			</div>
			<div class="modal-body">
				<div class="row"  id="prevew_modal_inner_div_contaner">
					<img src="" width="100%">
					<div class="image_over_contaner"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="icon_assign_modules_link_popup" class="modal fade" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Icons Modules</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="modules_id" name="modules_id" value="">
					<div class="col-sm-12">
						<label class="col-sm-4">Please select a module to link from this area : </label>
						<div class="col-sm-6">
							<select name="menu_id" id="menu_id" class="form-control name_group">
								<option value="">Select Modules</option>
								<optgroup label="Menu">
									<?php foreach ($active_menu as $key => $value) { ?>
									<option value="menuid_<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Custom modules">
									<?php foreach ($active_cms_menu as $key => $value) { ?>
									<option value="cmsid_<?php echo $value['Id']; ?>"><?php echo $value['Menuname']; ?></option>
									<?php } ?>
								</optgroup>
							</select>
							<span id="menu_id_error_span" class="help-block" style="display: none;" for="menu_id">This field is required.</span>
						</div>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="submit_icon_modules_select">Submit</button>
					</div>
					<div class="col-sm-12">
						<label>Or Enter a Web Link to link from this area : </label>
						<input type="text" name="modules_link" value="" id="modules_link" class="form-control name_group">
						<span id="modules_link_error_span" class="help-block" style="display: none;" for="modules_link"></span>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="submit_icon_modules_link">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="image_area_assign_modules_link_popup" class="modal fade" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Icons Modules</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" name="image_area_coords" id="image_area_coords" value="">
						<input type="hidden" name="image_banner_id" id="image_banner_id" value="">
						<label class="col-sm-4">Please select a module to link from this area : </label>
						<div class="col-sm-6">
							<select name="area_menu_id" id="area_menu_id" class="form-control name_group">
								<option value="">Select Modules</option>
								<optgroup label="Menu">
									<?php foreach ($menu_list as $key => $value) { ?>
									<option value="menuid_<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
									<?php } ?>
									<option value="menuid_56">My Meetings</option>
									<option value="menuid_27">Notification</option>
								</optgroup>
								<optgroup label="Custom modules">
									<?php foreach ($active_cms_menu1 as $key => $value) { ?>
									<option value="cmsid_<?php echo $value['Id']; ?>"><?php echo $value['Menuname']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Agenda Category">
									<?php foreach ($category_list as $key => $value) { ?>
									<option value="agendaid_<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Link My Agenda">
									<option value="myagenda">My Agenda</option>
								</optgroup>
								<optgroup label="Modules Group">
									<?php foreach ($modules_group as $key => $value) { ?>
									<option value="groupid_<?php echo $value['module_group_id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Exhibitors Category">
									<?php foreach ($exhibitor_category as $key => $value) { ?>
									<option value="exhiid_<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Exhibitors Sub-Category">
									<option value="allexhi_sub">All Products</option>
									<?php foreach ($exhibitor_child_category as $key => $value) { ?>
									<option value="exhisubid_<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>	
									<?php } ?>
								</optgroup>
					
								<optgroup label="CMS Super Group">
									<?php foreach ($cms_super_group as $key => $value) { ?>
									<option value="cmssupergroup_<?php echo $value['id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="CMS Sub-Group">
									<?php foreach ($cam_sub_group as $key => $value) { ?>
									<option value="cmsgroupid_<?php echo $value['module_group_id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
							</select>
							<span id="area_menu_id_error_span" class="help-block" style="display: none;" for="area_menu_id">This field is required.</span>
						</div>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="submit_image_arear_select">Submit</button>
					</div>
					<div class="col-sm-12">
						<label>Or Enter a Web Link to link from this area : </label>
						<input type="text" name="image_area_link" value="" id="image_area_link" class="form-control name_group">
						<span id="area_link_error_span" class="help-block" style="display: none;" for="image_area_link"></span>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="submit_image_area_link">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="lefthandmenu_image_area_assign_modules_link_popup" class="modal fade" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Icons Modules</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" name="lefthandmenu_image_area_coords" id="lefthandmenu_image_area_coords" value="">
						<input type="hidden" name="lefthandmenu_image_banner_id" id="lefthandmenu_image_banner_id" value="">
						<label class="col-sm-4">Please select a module to link from this area : </label>
						<div class="col-sm-6">
							<select name="lefthandmenu_area_menu_id" id="lefthandmenu_area_menu_id" class="form-control name_group">
								<option value="">Select Modules</option>
								<option value="menuid_19">Home</option>
								<optgroup label="Menu">
									<?php foreach ($menu_list as $key => $value) { if(!in_array($value['id'],array(5,19,18,26,27,28))){ ?>
									<option value="menuid_<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
									<?php } } ?>
									<option value="menuid_56">My Meetings</option>
									<option value="menuid_27">Notification</option>
								</optgroup>
								<optgroup label="Custom modules">
									<?php foreach ($frontcmsmenu as $key => $value) { ?>
									<option value="cmsid_<?php echo $value['Id']; ?>"><?php echo $value['Menu_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Agenda Category">
									<?php foreach ($category_list as $key => $value) { ?>
									<option value="agendaid_<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Link My Agenda">
									<option value="myagenda">My Agenda</option>
								</optgroup>
								<optgroup label="Modules Group">
									<?php foreach ($modules_group as $key => $value) { ?>
									<option value="groupid_<?php echo $value['module_group_id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="Exhibitors Category">
									<?php foreach ($exhibitor_category as $key => $value) { ?>
									<option value="exhiid_<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>	
									<?php } ?>
								</optgroup>
								<optgroup label="Exhibitors Sub-Category">
									<option value="allexhi_sub">All Products</option>
									<?php foreach ($exhibitor_child_category as $key => $value) { ?>
									<option value="exhisubid_<?php echo $value['id']; ?>"><?php echo $value['category']; ?></option>	
									<?php } ?>
								</optgroup>
								<optgroup label="CMS Super Group">
									<?php foreach ($cms_super_group as $key => $value) { ?>
									<option value="cmssupergroup_<?php echo $value['id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="CMS Sub-Group">
									<?php foreach ($cam_sub_group as $key => $value) { ?>
									<option value="groupid_<?php echo $value['module_group_id']; ?>"><?php echo $value['group_name']; ?></option>
									<?php } ?>
								</optgroup>
							</select>
							<span id="lefthandmenu_area_menu_id_error_span" class="help-block" style="display: none;" for="area_menu_id">This field is required.</span>
						</div>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="submit_lefthandmenu_image_arear_select">Submit</button>
					</div>
					<div class="col-sm-12">
						<label>Or Enter a Web Link to link from this area : </label>
						<input type="text" name="lefthandmenu_image_area_link" value="" id="lefthandmenu_image_area_link" class="form-control name_group">
						<span id="lefthandmenu_area_link_error_span" class="help-block" style="display: none;" for="lefthandmenu_image_area_link"></span>
					</div>
					<div class="col-sm-12" style="border-top: 1px solid #e5e5e5; padding-top: 10px;">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 10px;">Cancel</button>
						<button type="button" class="btn btn-default pull-right" id="lefthandmenu_submit_image_area_link">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="confirm_dialog_div" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="border:none;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirm Screen Design Change</h4>
			</div>
			<div class="modal-body">
				<strong>Are you sure you want to Save your new home screen? Any previous designs will be lost</strong>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="save_advance_values">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div style="display: none;">
<?php foreach ($banner_list as $key => $value) { ?>
<div class="row" id="bannner_image_div_<?php echo $value['id']; ?>">
	<map id="map_<?php echo $value['id']; ?>">
		<?php foreach ($value['coords'] as $ckey => $cvalue) {
			echo '<area alt="Mapping" id="area_'.$cvalue['id'].'" shape="rect" href="javascript:void(0);" coords="'.$cvalue['coords'].'">';
		} ?>
	</map>
</div>
<?php } ?>
<?php foreach ($lefthandmenu_image_list as $key => $value) { ?>
<div class="row" id="lefthandmenu_image_div_<?php echo $value['id']; ?>">
	<map id="lefthandmenumap_<?php echo $value['id']; ?>">
		<?php foreach ($value['coords'] as $ckey => $cvalue) {
			echo '<area alt="Mapping" id="area_'.$cvalue['id'].'" shape="rect" href="javascript:void(0);" coords="'.$cvalue['coords'].'">';
		} ?>
	</map>
</div>
<?php } ?>
</div>
<script id="uploadimages_template_div" type="text/x-jQuery-tmpl"> 
<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="image_upload_div_${divid}">
	<div class="file_upload_div images_upload_image_name_heding">
		<span class="fileupload-preview">...</span>
	</div>
	<div class="col-sm-8">
		<div data-provides="fileupload" class="fileupload fileupload-new">
			<span class="btn btn-file btn-light-grey">
				<i class="fa fa-folder-open-o"></i> 
				<span class="fileupload-new">Click Here To Upload An Image</span>
				<span class="fileupload-exists">Change</span>
				<input type="file" name="advaicon_images" onchange="croppingadvanceicon(this,'${divid}');">
			</span>
		</div>
		<div class="add_edit_text_button_div col-sm-6 pad-left-no" style="display:none;">
			<a href="javascript:void(0);" onclick="get_add_text_btn(this,'${divid}');" class="btn btn-green btn-block">
				Add/Edit Text
			</a>
		</div>
		<input type="hidden" name="advaicon_images_crope_text[${divid}]" class="advaicon_images_crope_data" value="">
		<input type="hidden" name="advaicon_images_content_textbox[${divid}]" class="advaicon_images_content_data" id="advaicon_images_content_textbox_${divid}" value="">
		<div class="col-sm-6 pad-left-no">
			<a href="javascript:void(0);" onclick="remove_image_upload_div(this,'${divid}');" class="btn btn-red btn-block remove_section">
				Delete Section
			</a>
		</div>
	</div>
	<div class="col-sm-4">
		<img class="image_upload_div_preview_advance_image_demo" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
	</div>	
</div>
</script>
<script id="lefthandmenu_uploadimages_template_div" type="text/x-jQuery-tmpl">
	<div class="col-sm-12 image_upload_div image_upload_div_backdesign" id="lefthandmenu_image_upload_div_${divid}">
		<div class="file_upload_div images_upload_image_name_heding">
			<span class="fileupload-preview">...</span>
		</div>
		<div class="col-sm-8"> 
			<div data-provides="fileupload" class="fileupload fileupload-new">
				<span class="btn btn-file btn-light-grey">
					<i class="fa fa-folder-open-o"></i> 
					<span class="fileupload-new">Click Here To Upload An Image</span>
					<span class="fileupload-exists">Change</span>
					<input type="file" name="lefthandmenu_advaicon_images" onchange="croppinglefthandmenuadvanceicon(this,'${divid}');">
				</span>
			</div>
			<div class="col-sm-6 pad-left-no">
				<a href="javascript:void(0);" onclick="lefthandmenu_remove_image_upload_div(this,'${divid}');" class="btn btn-red btn-block remove_section">
					Delete Section
				</a>
			</div>
		</div>
		<div class="col-sm-4">
			<img class="imageuploaddiv_preview_leftmenuadvanceimage" width="100%" src="<?php echo base_url().'assets/images/onimages.png' ?>">	
		</div>	
	</div> 
</script>
<script id="uploadheaderimages_template_div" type="text/x-jQuery-tmpl">
	<div data-provides="fileupload" class="fileupload fileupload-new" >
		<span class="btn btn-file btn-light-grey">
			<i class="fa fa-folder-open-o"></i> 
			<span class="fileupload-new">Edit Header Image</span>
			<span class="fileupload-exists">Change</span>
			<input type="file" name="header_image[${divid}]" onchange="geaderimageupload(this,'${divid}');">
		</span>
		<div class="file_upload_div">
			<span class="fileupload-preview"></span>
			<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#" onclick="delete_banner_image(this,'${divid}','');">
				&times;
			</a>
		</div>
	</div>
</script>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#submit_icon_modules_select').click(function(){
	if($.trim($('#menu_id').val())!="")
	{
		$('#menu_id_error_span').parent().removeClass('has-error').addClass('has-success');
		$('#menu_id_error_span').hide();
		$('#convert_'+$('#modules_id').val()).val($('#menu_id').val());
		$('#icon_assign_modules_link_popup').modal('hide');
	}
	else
	{
		$('#menu_id_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#menu_id_error_span').show();
	}
});
$('#menu_id').change(function(){
	if($.trim($('#menu_id').val())!="")
	{
		$('#menu_id_error_span').parent().removeClass('has-error').addClass('has-success');
		$('#menu_id_error_span').hide();
	}
	else
	{
		$('#menu_id_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#menu_id_error_span').show();
	}
});
$('#submit_icon_modules_link').click(function(){
	if($.trim($('#modules_link').val())!="")
	{
		if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#modules_link").val()))
		{
			$('#modules_link_error_span').parent().removeClass('has-error').addClass('has-success');
			$('#modules_link_error_span').hide();
			$('#convert_'+$('#modules_id').val()).val($('#modules_link').val());
			$('#icon_assign_modules_link_popup').modal('hide');
		}
		else
		{
			$('#modules_link_error_span').html("Please Enter valid Url.");
			$('#modules_link_error_span').parent().removeClass('has-success').addClass('has-error');
			$('#modules_link_error_span').show();
		}
	}
	else
	{
		$('#modules_link_error_span').html("This field is required.");
		$('#modules_link_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#modules_link_error_span').show();
	}
});
function openiconmodifi(id)
{
	$('#menu_id').val("");
	$('#modules_id').val(id);
}
var $gallery=$("#sortable_modules"),$trash=$("#droppable_modules");
$(function() {
	$('#active_icone_list').sortable({
		connectWith: '.sortable',
		revert: 0,
		forcePlaceholderSize: true,
		placeholder: 'ui-sortable-placeholder',
		tolerance: 'pointer'
	});
	$("li",$gallery).draggable({
		revert: "invalid",
	    cursor: "move",
	    containment: "document",
	    helper: "clone",
	});
	$("li",$trash).draggable({
		revert: "invalid",
	    cursor: "move",
	    containment: "document",
	    helper: "clone",
	});
	$trash.droppable({
		accept:"#sortable_modules > li",
		classes: {
        	"ui-droppable-active": "ui-state-highlight"
      	},
      	drop: function( event, ui ) {
      		ui.draggable.find("[name='order_menu_id[]']").attr("name","active_order_icon[]");
      		deleteul( ui.draggable );
      		$('#active_icone_list').sortable({
      			connectWith: '.sortable',
				revert: 0,
				forcePlaceholderSize: true,
				placeholder: 'ui-sortable-placeholder',
				tolerance: 'pointer'
			});
			$( "#active_icone_list" ).disableSelection();
      	}
	});
	$gallery.droppable({
      accept: "#droppable_modules li",
      classes: {
        "ui-droppable-active": "custom-state-active"
      },
      drop: function( event, ui ) {
      	ui.draggable.find("[name='active_order_icon[]']").attr("name","order_menu_id[]");
        recycleImage( ui.draggable );
      }
    });

});
function deleteul($item)
{
	$item.fadeOut(function() {
		$item.appendTo('#active_icone_list').fadeIn();
    });
}
function recycleImage($item) 
{
	$item.fadeOut(function() {
    	$item.appendTo($gallery).fadeIn();
  	});
}
jQuery(document).ready(function () {  
	if(location.hash){
		if($(location.hash).length){
			$('.app_section').removeClass('show').addClass('hide');
			$(location.hash).addClass('show');
		}
	} 
	$('#edit_home_screen_content').summernote({
		height: 300,                 
		minHeight: null,       
		maxHeight: null,
		onImageUpload: function(files, editor, welEditable) {
			sendFile(files[0],editor,welEditable);
		}
	});
});
function sendFile(file,editor,welEditable) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
      data: data,
      type: "POST",
      url: "<?php echo base_url().'Event/saveeditorfiles/'.$this->uri->segment(3); ?>",
      cache: false,
      contentType: false,
      processData: false,
      success: function(url) {
        editor.insertImage(welEditable, url);
      }
  });
}
function hideshowdiv_in_app(sid) 
{
	$('.app_section').removeClass('show').addClass('hide');
	$('#'+sid).removeClass('hide').addClass('show');
	$('.lefthandmenu_map').mapster({
	    	selected: true,
	  	});
  	$('.othermap1').mapster({
		selected: true,	
		});
	$('.map1').mapster({
    	selected: true,
  	});
}
$('#save_app_name_btn').click(function(){
	if($.trim($('#Event_name').val())!="")
	{
		$('#App_name_error_span').parent().parent().removeClass('has-error');
		$('#App_name_error_span').hide();
		var arr = {'Event_name':$.trim($('#Event_name').val())};
		$.ajax({
			url:"<?php echo base_url().'Event/save_app_name/'.$event['Id']; ?>",
			type:'post',
			data: arr,
	        dataType: 'json',
	        async: false,
			success:function(data){
				if($.trim(data)=="success")
				{
					$('#success_msg_div').show();
					setTimeout('$("#success_msg_div").hide()',2000);
				}
				else
				{
					$('#error_msg_span').html(data);
					$('#error_msg_div').show();
					setTimeout('$("#error_msg_div").hide()',2000);
				}
			}
		});
	}
	else
	{
		$('#App_name_error_span').parent().parent().addClass('has-error');
		$('#App_name_error_span').show();
	}
});
$('#Event_name').keydown(function(){
	if($.trim($('#Event_name').val())!="")
	{
		$('#App_name_error_span').parent().parent().removeClass('has-error');
		$('#App_name_error_span').hide();
		$('#event_name_left_hand_div').html($.trim($('#Event_name').val()));
		$('.app_name_link').html($.trim($('#Event_name').val()));
	}
});
$('#Event_name').blur(function(){
	if($.trim($('#Event_name').val())!="")
	{
		$('#App_name_error_span').parent().parent().removeClass('has-error');
		$('#App_name_error_span').hide();
		$('#event_name_left_hand_div').html($.trim($('#Event_name').val()));
		$('.app_name_link').html($.trim($('#Event_name').val()));
	}
});
/*$('#save_app_image_btn').click(function(){
	$('#save_app_image_btn').attr('disabled','disabled');
	$('#save_app_image_btn').find('i').attr('class','fa fa-spinner fa-spin');
	var formData = new FormData($('#eventedit_form')[0]);
	$.ajax({
		url:"<?php //echo base_url().'event/save_app_image_upload/'.$event['Id']; ?>",
		type:'post',
		data:formData,
		processData: false,
        contentType: false,
        error:function()
        {
        	$('#save_app_image_btn').removeAttr('disabled');
			$('#save_app_image_btn').find('i').attr('class','fa fa-arrow-circle-right');
        },
		success:function(data){
			var result=data.split('###');
			if($.trim(result[0])=="success")
			{
				$('#success_msg_image_div').show();
				setTimeout('$("#success_msg_image_div").hide()',2000);
			}
			else
			{
				$('#error_msg_image_span').html(result[1]);
				$('#error_msg_image_div').show();
				setTimeout('$("#error_msg_image_div").hide()',2000);
			}
			$('#save_app_image_btn').removeAttr('disabled');
			$('#save_app_image_btn').find('i').attr('class','fa fa-arrow-circle-right');
			/*window.location.href="<?php echo base_url().'event/edit/'.$event['Id'] ?>"+"#app_images_section";
			location.reload();*/
		/*}
	});
});*/
$('#save_app_color_btn').click(function(){
	$.ajax({
		url:"<?php echo base_url().'Event/save_app_color/'.$event['Id']; ?>",
		type:'post',
		data:'Top_background_color='+$('#header_color').val()+'&Top_text_color='+$('#header_text_color').val()+'&menu_background_color='+$('#left_hand_menu_color').val()+'&menu_text_color='+$('#left_hand_menu_text_color').val(),
		success:function(data){
			if($.trim(data)=="success")
			{
				$('#success_msg_color_div').show();
				setTimeout('$("#success_msg_color_div").hide()',2000);
			}
			else
			{
				$('#error_msg_color_span').html(data);
				$('#error_msg_color_div').show();
				setTimeout('$("#error_msg_color_div").hide()',2000);
			}
		}
	});
});
$('.close_logo_popup').click(function(){
	$('#logo_crope_images_text').val();
});
$('.close_banner_popup').click(function(){
    $('#header_crope_images_text').val();
});
$('.close_icon_data_popup').click(function(){
    $('#icon_id_popup_text_box').val('');
    $('#icon_backgroung_color_popup').val('');
    $('#icon_crop_logo_img').attr('src','');
	$("#icon_image").val('');   
    $('#icon_crop_logo').hide();
	$('#save_data_button_div').show();
});
$("input[name='custom_color_picker']").click(function(){
	var color_arr=$(this).val().split('_');
	$('#header_color').val(color_arr[0]);
	$('#header_color').css('background-color',color_arr[0]);
	$('#left_hand_menu_color').val(color_arr[1]);
	$('#left_hand_menu_color').css('background-color',color_arr[1]);
	$('.header').css('background-color',color_arr[0]);
	$('.menu').find('li').css('background-color',color_arr[1]);
	$('#icon_backgroung_color').val(color_arr[0]);
	$('#icon_backgroung_color').css('background-color',color_arr[0]);
	$('.icon-list').find('li').css('background-color',color_arr[0]);
});
$('#header_color').change(function(){
	$('.header').css('background-color',$('#header_color').val());
});	
$('#header_text_color').change(function(){
	$('.header').find('span').css('color',$('#header_text_color').val());
	$('.header').find('i').css('color',$('#header_text_color').val());
});
$('#left_hand_menu_color').change(function(){
	$('.menu').find('li').css('background-color',$('#left_hand_menu_color').val());
});	
$('#left_hand_menu_text_color').change(function(){
	$('.menu').find('li').css('color',$('#left_hand_menu_text_color').val());
});	
$('#icon_backgroung_color').change(function(){
	$('.icon-list').find('li').css('background-color',$('#icon_backgroung_color').val());
	$('.icon_background_color_class').val($('#icon_backgroung_color').val());
});
$('#submit_home_screen_text').click(function(){
	$('.home-screen-text').html($('#edit_home_screen_content').code());
});
$('#add_extra_images_button').click(function(){
	if($('#header_images_contener .fileupload').length <= 9)
	{
		var dividarr=[{"divid":Number(totalbannerupload)+1}];
		$("#header_images_contener").append($('#uploadheaderimages_template_div').tmpl(dividarr));
		totalbannerupload++;
	}
	else
	{
		alert('You can maximum 10 banner upload');
	}
});
$('#save_app_home_screen_text_btn').click(function(){
	$.ajax({
		url:"<?php echo base_url().'Event/save_app_homeScreen_content/'.$event['Id']; ?>",
		type:'post',
		data:'Description='+$('#edit_home_screen_content').code(),
		success:function(data){
			if($.trim(data)=="success")
			{
				$('#success_msg_home_content_div').show();
				setTimeout('$("#success_msg_home_content_div").hide()',2000);
			}
			else
			{
				$('#error_msg_home_content_span').html(data);
				$('#error_msg_home_content_div').show();
				setTimeout('$("#error_msg_home_content_div").hide()',2000);
			}
		}
	});
});
$("input[name='icon_set_type']").click(function(){
	if($(this).val()==1)
	{
		$('.iconset_one_icon').show();
		$('.iconset_two_icon').hide();
		$('.iconset_thard_icon').hide();
	}
	else if($(this).val()==2)
	{
		$('.iconset_one_icon').hide();
		$('.iconset_two_icon').show();
		$('.iconset_thard_icon').hide();
	}
	else
	{
		$('.iconset_one_icon').hide();
		$('.iconset_two_icon').hide();
		$('.iconset_thard_icon').show();
	}
});
$('#save_app_icon_btn').click(function(){
	var formData = new FormData($('#eventedit_form')[0]);
	//console.log($('#eventedit_form')[0]);
	$.ajax({
		url:"<?php echo base_url().'Event/save_app_icon_and_color/'.$event['Id']; ?>",
		type:'post',
		data:formData,
		processData: false,
        contentType: false,
		success:function(data){
			if($.trim(data)=="success")
			{
				$('#success_msg_icon_div').show();
				setTimeout('$("#success_msg_icon_div").hide()',2000);
			}
			else
			{
				$('#error_msg_icon_span').html(data);
				$('#error_msg_icon_div').show();
				setTimeout('$("#error_msg_icon_div").hide()',2000);
			}
		}
	});
});
function show_modules_edit_popup(id)
{
	$('#icon_name_popup_text_box').val($('#modules_name_'+id).val());
	$('#icon_id_popup_text_box').val(id);
	$('#icon_backgroung_color_popup').val($('#home_screen_tab_back_color_'+id).val());
	$('#icon_backgroung_color_popup').css('background-color',$('#home_screen_tab_back_color_'+id).val());
	if($('#create_home_screen_tab_'+id).val()==1)
	{
		$('#create_home_screen_tab').prop('checked', true);
	}
	else
	{
		$('#create_home_screen_tab').prop('checked', false);
	}
	$('#icone_name_popup').modal('show');
}
$('#logo_image_area_div').click(function(){
	$('#logo_image').trigger('click');
});
$('#icon_models_save_data').click(function(){
	var textid=$('#icon_id_popup_text_box').val();
    $('#modules_name_'+textid).val($('#icon_name_popup_text_box').val());
    $('.module_title_'+textid).html($('#icon_name_popup_text_box').val());
    if($('#create_home_screen_tab'). prop("checked") == true){
    	$('#create_home_screen_tab_'+textid).val('1');
    }
    else
    {
    	$('#create_home_screen_tab_'+textid).val('0');
    }
    $('#home_screen_tab_back_color_'+textid).val($('#icon_backgroung_color_popup').val());
    $('.home_tab_modules_li_'+textid).css('background-color',$('#icon_backgroung_color_popup').val());
    $('#icon_id_popup_text_box').val('');
    $('#icon_backgroung_color_popup').val('');
    $('#icone_name_popup').modal('hide');
});
</script>
<script src="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.js"></script>
<script type="text/javascript">
var owl;
var totalbannerupload="<?php echo count($image)==0 ? '1' : count($image); ?>";
$(document).ready(function() {
  owl = jQuery(".banner_list_owl").owlCarousel({
   items : 1,
   autoPlay: 5000,
   navigation : false,
   center: true,
   loop:true
  }); 
});
function delete_banner_image(elem,slideno,imgname)
{
	if(imgname!="")
	{
		$.ajax({
			url:"<?php echo base_url().'Event/delete_banner_image/'.$event['Id'] ?>",
			type:'post',
			data:'imagename='+imgname,
			success:function(result)
			{
				if($.trim(result)=="success")
				{
					$(elem).parent().parent().remove();
					$('#banner_item_div_'+slideno).parent().remove();
					owl.data('owlCarousel').reinit({items : 1});
				}
			}
		});
	}
	else
	{
		$(elem).parent().parent().remove();
		$('#banner_item_div_'+slideno).parent().remove();
		owl.data('owlCarousel').reinit({items : 1});
	}
}
</script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $logoimage = $("#show_crop_img_model");
var $inputLogoImage = $('#logo_image'); 
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image');
var $icon_logo_crop = $('#icon_crop_logo_img');
var $input_icon_logo = $("#icon_image");   
var $advanceiconeimage = $("#show_crop_advance_icone_model");
var $lefthandmenuadvanceiconeimage = $("#show_crop_lefthandmenu_advance_icone_model");
var fileuploadno;
<?php if(count($banner_list) > 0){ ?>
var howmanydivgaratedno=parseInt("<?php echo count($banner_list); ?>");
<?php }else{ ?>
var howmanydivgaratedno=3;
<?php } ?>
var banneritemno;
function geaderimageupload(elem,headerimagenumber)
{
	banneritemno=headerimagenumber;
	var uploadedImageURL;
	var URL = window.URL || window.webkitURL;
    var files = elem.files;
    var file;
	if (files && files.length) {
        file = files[0];
        if (/^image\/\w+$/.test(file.type)) {
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }
          uploadedImageURL = URL.createObjectURL(file);
          $bannerimage.attr('src', uploadedImageURL);
          $('#add_extra_image_button_div').show();
          $('#headermodelscopetool').modal('show');
        } else {
          window.alert('Please choose an image file.');
        }
    }
}
$(document).ready(function(){
	var icon_set_type_var="<?php echo $event['icon_set_type']; ?>";   
	if(icon_set_type_var==1)
	{
		$('.iconset_one_icon').show();
		$('.iconset_two_icon').hide();
		$('.iconset_thard_icon').hide();
	}
	else if(icon_set_type_var==2)
	{
		$('.iconset_one_icon').hide();
		$('.iconset_two_icon').show();
		$('.iconset_thard_icon').hide();
	}
	else
	{
		$('.iconset_one_icon').hide();
		$('.iconset_two_icon').hide();
		$('.iconset_thard_icon').show();
	}
	$inputLogoImage.change(function(){
	    var uploadedImageURL;
	    var URL = window.URL || window.webkitURL;
		var files = this.files;
		var file;
		if (files && files.length) {
		    file = files[0];
		    if (/^image\/\w+$/.test(file.type)) {
		      if (uploadedImageURL) {
		        URL.revokeObjectURL(uploadedImageURL);
		      }
		      
		      uploadedImageURL = URL.createObjectURL(file);
		      $logoimage.attr('src', uploadedImageURL);
		      $('.logo_preview_change_class').attr('src',uploadedImageURL);
		      $('.logo_preview_change_class').show();
		      $('.placeholder_text_div_class').hide();
		      $('#logomodelscopetool').modal('show');
		    } else {
		      window.alert('Please choose an image file.');
		    }
		}
	});
	$(document).on('shown.bs.modal','#logomodelscopetool', function () {
		$logoimage.cropper('destroy');
	    var croppable = false;
	    var $button = $('#upload_result_btn_crop');
	        $logoimage.cropper({
	            built: function () {
	              croppable = true;
	            }
	        });
	        $button.on('click', function () {
                var croppedCanvas;
                if (!croppable) {
                  return;
                }
                croppedCanvas = $logoimage.cropper('getCroppedCanvas');
                $('.logo_preview_change_class').attr('src',croppedCanvas.toDataURL());
                $('#logo_crope_images_text').val(croppedCanvas.toDataURL());
                $("#logo_image").val('');
	        });
	    }).on('hidden.bs.modal','#logomodelscopetool',function(){
	         $logoimage.cropper('destroy');
	});

	 
	$inputBannerImage.change(function(){
	    var uploadedImageURL;
	    var URL = window.URL || window.webkitURL;
   
    
	      var files = this.files;
	      var file;
	      if (files && files.length) {
        	file = files[0];

	        if (/^image\/\w+$/.test(file.type)) {
	          if (uploadedImageURL) {
	            URL.revokeObjectURL(uploadedImageURL);
	          }
	          
	          uploadedImageURL = URL.createObjectURL(file);
	          $bannerimage.attr('src', uploadedImageURL);
	          $('.header_div_images_class').attr('src',uploadedImageURL);
	          $('.header_placeholder_div_class').hide();
	          $('.header_div_images_class').show();
	          $('#headermodelscopetool').modal('toggle');
	        } else {
	          window.alert('Please choose an image file.');
	        }
      	}
	});
	$(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
		$bannerimage.cropper('destroy');
	    var croppable = false;
	    var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
        	/*aspectRatio: 16 / 8,*/
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            //croppedCanvas = $bannerimage.cropper("getCroppedCanvas",{ 'width': 1000, 'height': 500});
            croppedCanvas = $bannerimage.cropper("getCroppedCanvas");
            if($('#banner_item_div_'+banneritemno).length > 0)
            {
            	var textname="header_images_crope_text["+banneritemno+"]";
            	$('#banner_item_div_'+banneritemno).find('img').attr('src',croppedCanvas.toDataURL());
            	$('#banner_item_div_'+banneritemno).find('.header_images_crope_textbox_'+banneritemno).val(croppedCanvas.toDataURL());
            }
            else
            {
        		var divid="banner_item_div_"+banneritemno;
            	var textname="header_images_crope_text["+banneritemno+"]";
            	$('#banner_image_contener_div').find('.owl-wrapper').append('<div class="row" id="'+divid+'"><img width="100%" class="header_div_images_class" src="'+croppedCanvas.toDataURL()+'"/><input type="hidden" name="'+textname+'" value="'+croppedCanvas.toDataURL()+'"></div>');
            	owl.data('owlCarousel').reinit({items : 1});
            	if($('#placeholderdivcontent').length > 0){
            		$('#placeholderdivcontent').parent().remove();
            		owl.data('owlCarousel').reinit({items : 1});
            	}
            }
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    });  
    $input_icon_logo.change(function(){
		$icon_logo_crop.cropper('destroy');
	    var uploadedImageURL;
	    var URL = window.URL || window.webkitURL;
		var files = this.files;
		var file;
		if (files && files.length) {
		    file = files[0];
		    if (/^image\/\w+$/.test(file.type)) {
		      if (uploadedImageURL) {
		        URL.revokeObjectURL(uploadedImageURL);
		      }
		       uploadedImageURL = URL.createObjectURL(file);
		        $icon_logo_crop.attr('src', uploadedImageURL);
		        $('#save_data_button_div').hide();
		        $('#icon_crop_logo').show();
			    var croppable = false;
			    var $button = $('#icon_crop_button_in_models');
		        $icon_logo_crop.cropper({
		        	aspectRatio: 1,
		            built: function () {
		              croppable = true;
		            }
		        });
		        $button.on('click', function () {
		        	var textid=$('#icon_id_popup_text_box').val();
		            var croppedCanvas;
		            if (!croppable) {
		              return;
		            }
		            croppedCanvas = $icon_logo_crop.cropper('getCroppedCanvas');
		            $('#modules_name_'+textid).val($('#icon_name_popup_text_box').val());
		            $('.module_title_'+textid).html($('#icon_name_popup_text_box').val());
		            if($('#create_home_screen_tab'). prop("checked") == true){
		            	$('#create_home_screen_tab_'+textid).val('1');
		            }
		            else
		            {
		            	$('#create_home_screen_tab_'+textid).val('0');
		            }
		            $('#modules_crop_image_'+textid).val(croppedCanvas.toDataURL());
		            $('.module_image_show_'+textid).attr('src',croppedCanvas.toDataURL());
		            $('#home_screen_tab_back_color_'+textid).val($('#icon_backgroung_color_popup').val());
		            $('.home_tab_modules_li_'+textid).css('background-color',$('#icon_backgroung_color_popup').val());
		            $('#icon_id_popup_text_box').val('');
		            $('#icon_backgroung_color_popup').val('');
		            $('#icon_crop_logo').hide();
		            $('#save_data_button_div').show();
		            $('#icon_crop_logo_img').attr('src','');
		            $icon_logo_crop.cropper('destroy');
		            $input_icon_logo.val('');
		            $('#icone_name_popup').modal('hide');
		        });	
		    } else {
		      window.alert('Please choose an image file.');
		    }
		}
	}); 
	$('#advanceiconemodelscopetool').on('hidden.bs.modal',function(){
        $advanceiconeimage.cropper('destroy');
    });   
});
function remove_image_upload_div(elem,divno)
{
	if($('#image_over_content_'+divno).length > 0)
	{
		$.ajax({
			url:"<?php echo base_url().'Event/delete_advance_banner_image/'.$event['Id'].'/' ?>"+$('#image_over_content_'+divno).parent().find('img:last').attr('data-bannerid'),
			success:function(result)
			{
				if($.trim(result)=="success")
				{
					elem.parentElement.parentElement.remove();
					$('#image_upload_div_'+divno).remove();
					$('#image_over_content_'+divno).parent().remove();
				}
				else
				{
					alert('Something Went Wrong Please Refresh The Page And try again..');
				}
			}
		});
	}
	else
	{	
		elem.parentElement.parentElement.remove();
		$('#image_upload_div_'+divno).remove();
	}
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            return e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function croppingadvanceicon(elem,fiesuploadno)
{	
	fileuploadno=fiesuploadno;
	$('#fileuploadnumber').val(fileuploadno);
	$advanceiconeimage.cropper('destroy');
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
    var files = elem.files;
    var file;
    if (files && files.length) {
    	file = files[0];
    	var height,width;
    	var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
        	var image = new Image();
        	image.src = e.target.result;
        	image.onload = function () {
            	var height = this.height;
            	var width = this.width;
        	}
        }
        if (/^image\/\w+$/.test(file.type)) {
			if (uploadedImageURL) {
			URL.revokeObjectURL(uploadedImageURL);
			}
			if (height > 2000 || width > 1000) {
                alert("Your image is too large which may slow down your App. Please choose an image with a maximum width of 1000px and/or a maximum height of 2000px.");
                return false;
            }
            else
            {

            	if($.trim(file.type)=="image/gif")
            	{ 
            		var formData = new FormData();
		        	formData.append('bannerid',$('#image_over_content_'+fileuploadno).parent().find('img:last').attr('data-bannerid'));
		        	formData.append('gifimage',file);
		        	$('#advanceiconemodelscopetool').modal('toggle');
            		$('#upload_result_btn_advance_icone_crop').html('Croping <i class="fa fa-spinner fa-spin"></i>');
				    $('#upload_result_btn_advance_icone_crop').attr('disabled','disabled');
				    $('#advanceiconemodal_heading').html("Adding your homescreen GIF image. Please wait");
				    $('#loding_icon_div_advance_images_crop').show();
				    $('#cropping_advance_icone_div').hide();
				    $.ajax({
				    	url:"<?php echo base_url().'Event/save_banner_gif_image/'.$event['Id'] ?>",
				    	type:'post',
				    	data:formData,
				    	processData: false,
    					contentType: false,
				    	error:function(error)
				    	{
				    		$('#advanceiconemodal_heading').html("Icons Image");
				    		$('#edit_advance_design_image_content_popup').modal('show');
				            $('#loding_icon_div_advance_images_crop').hide();
				            $('#upload_result_btn_advance_icone_crop').html('Crop');
					        $('#upload_result_btn_advance_icone_crop').removeAttr('disabled');
				    	},
				    	success:function(result)
				    	{
				    		var data=$.parseJSON(result);
				    		if(data.status=="Success")
				    		{
					    		if($('#image_over_content_'+fileuploadno).length > 0)
						        {
						        	$('#image_over_content_'+fileuploadno).parent().find('img').attr('src',data.img_url);
									$('#edit_advance_image_content').code($('#image_over_content_'+fileuploadno).html());
						    	}
						    	else
						    	{
						    		$('#preview_advance_images_div').append('<div class="row image_contener_div"><img width="349" style="height: auto;" data-bannerid="'+data.banner_id+'" class="map1" usemap="#mape_'+data.banner_id+'" src="'+data.img_url+'"><map id="mape_'+data.banner_id+'" name="mape_'+data.banner_id+'"></map><div class="image_over_content" id="image_over_content_'+fileuploadno+'"></div></div>');
						    		$('#edit_advance_image_content').code('');
						    	}
						    	$('.map1').mapster({
					              selected: true
					            });
						    	$('#image_upload_div_'+fileuploadno).find('.image_upload_div_preview_advance_image_demo').attr('src',data.img_url);
						    	$('#image_upload_div_'+fileuploadno).find('.images_upload_image_name_heding').find('.fileupload-preview').html(data.img_name);
						    	$('#banner_id_edit_content_popup').val(data.banner_id);
						    	$('#imagedividtextbox').val('#image_over_content_'+fileuploadno);
						        $('#image_upload_div_'+fileuploadno).find('.advaicon_images_crope_data').val('');
						        $('#upload_result_btn_advance_icone_crop').html('Crop');
						        $('#upload_result_btn_advance_icone_crop').removeAttr('disabled');
						        $('#advanceiconemodelscopetool').modal('hide');
					            $('#edit_advance_design_image_content_popup').modal('show');
					            $('#advanceiconemodal_heading').html("Icons Image");
					            $('#loding_icon_div_advance_images_crop').hide();
					    		$('#cropping_advance_icone_div').show();
					    	}
					    	else
					    	{
					    		$('#advanceiconemodal_heading').html("Icons Image");
					    		$('#edit_advance_design_image_content_popup').modal('hide');
					            $('#loding_icon_div_advance_images_crop').hide();
					            $('#upload_result_btn_advance_icone_crop').html('Crop');
						        $('#upload_result_btn_advance_icone_crop').removeAttr('disabled');
						        $('#advanceiconemodelscopetool').modal('toggle');
					    	}
				    	}
				    });
            	}
            	else
            	{

					uploadedImageURL = URL.createObjectURL(file);
					$advanceiconeimage.attr('src', uploadedImageURL);
				    $('#image_upload_div_'+fileuploadno).find('.add_edit_text_button_div').show();
					$('#advanceiconemodelscopetool').modal('toggle');
		          	setTimeout(function(){
					    var croppable = false;
				        $advanceiconeimage.cropper({
				            built: function () {
				              croppable = true;
				            }
				        });
		    		}, 500);
		        }
	        }
        } 
        else 
        {
          window.alert('Please choose an image file.');
        }
  	}
}
$('#upload_result_btn_advance_icone_crop').click(function () {


    var croppedCanvas;
    $('#upload_result_btn_advance_icone_crop').html('Croping <i class="fa fa-spinner fa-spin"></i>');
    $('#upload_result_btn_advance_icone_crop').attr('disabled','disabled');
    $('#advanceiconemodal_heading').html("Adding your homescreen image. Please wait");
    $('#loding_icon_div_advance_images_crop').show();
    $('#cropping_advance_icone_div').hide();
    croppedCanvas = $advanceiconeimage.cropper('getCroppedCanvas');
    $.ajax({
    	url:"<?php echo base_url().'Event/save_banner_image/'.$event['Id'] ?>",
    	type:'post',
    	data:'image='+encodeURIComponent(croppedCanvas.toDataURL())+'&bannerid='+$('#image_over_content_'+fileuploadno).parent().find('img:last').attr('data-bannerid'),
    	error:function(error)
    	{
    		$('#advanceiconemodal_heading').html("Icons Image");
    		$('#edit_advance_design_image_content_popup').modal('show');
            $('#loding_icon_div_advance_images_crop').hide();
            $('#upload_result_btn_advance_icone_crop').html('Crop');
	        $('#upload_result_btn_advance_icone_crop').removeAttr('disabled');
    	},
    	success:function(result)
    	{
    		var data=$.parseJSON(result);
    		if($('#image_over_content_'+fileuploadno).length > 0)
	        {
	        	
	        	$('#image_over_content_'+fileuploadno).parent().find('img').attr('src',data.img_url);
				$('#edit_advance_image_content').code($('#image_over_content_'+fileuploadno).html());
	    	}
	    	else
	    	{  
	    		$('#preview_advance_images_div').append('<div class="row image_contener_div"><img width="349" style="height: auto;" data-bannerid="'+data.banner_id+'" class="map1" usemap="#mape_'+data.banner_id+'" src="'+data.img_url+'"><map id="mape_'+data.banner_id+'" name="mape_'+data.banner_id+'"></map><div class="image_over_content" id="image_over_content_'+fileuploadno+'"></div></div>');
	    		$('#edit_advance_image_content').code('');
	    	}
	    	$('.map1').mapster({
              selected: true
            });
	    	$('#image_upload_div_'+fileuploadno).find('.image_upload_div_preview_advance_image_demo').attr('src',data.img_url);
	    	$('#image_upload_div_'+fileuploadno).find('.images_upload_image_name_heding').find('.fileupload-preview').html(data.img_name);
	    	$('#banner_id_edit_content_popup').val(data.banner_id);
	    	$('#imagedividtextbox').val('#image_over_content_'+fileuploadno);
	        $('#image_upload_div_'+fileuploadno).find('.advaicon_images_crope_data').val(croppedCanvas.toDataURL());
	        $('#upload_result_btn_advance_icone_crop').html('Crop');
	        $('#upload_result_btn_advance_icone_crop').removeAttr('disabled');
	        $('#advanceiconemodelscopetool').modal('hide');
            $('#edit_advance_design_image_content_popup').modal('show');
            $('#advanceiconemodal_heading').html("Icons Image");
            $('#loding_icon_div_advance_images_crop').hide();
    		$('#cropping_advance_icone_div').show();
    	}
    });
});
$('.add_new_section').click(function(){
	var dividarr=[{"divid":howmanydivgaratedno}];
	$("#all_filesupload_contener_div").append($('#uploadimages_template_div').tmpl(dividarr));
	howmanydivgaratedno++;
});
$('#Update_advance_image_content').click(function(){
	$('#Update_advance_image_content').html('Updating <i class="fa fa-spinner fa-spin"></i>');
	$('#Update_advance_image_content').attr('disabled','disabled');
	$.ajax({
		url:"<?php echo base_url().'Event/save_advance_content/'.$event['Id']; ?>",
		type:'post',
		dataType : "json",
		data:{'content':$('#edit_advance_image_content').code(),"banner_id":$('#banner_id_edit_content_popup').val()},
		success:function(result){
			var noarr=$('#imagedividtextbox').val().split("_");
			$($('#imagedividtextbox').val()).html($('#edit_advance_image_content').code());
			$('#advaicon_images_content_textbox_'+noarr[3]).val($('#edit_advance_image_content').code());
			$('#Update_advance_image_content').html('Update');
			$('#Update_advance_image_content').removeAttr('disabled');
			$('#edit_advance_design_image_content_popup').modal('hide');
		}
	});
});
$('#Preview_advance_image_content').click(function(){
	$('#prevew_modal_inner_div_contaner').find('img').attr('src',$($('#imagedividtextbox').val()).parent().find('img').attr('src'));
	$('#prevew_modal_inner_div_contaner').find('div').html($('#edit_advance_image_content').code());
	$('#preview_image_with_content_modal').modal('show');
});
$('#Cancel_advance_image_content').click(function(){
	$('#edit_advance_design_image_content_popup').modal('hide');
});
function get_add_text_btn(btnelem,btnno)
{
	$('#imagedividtextbox').val('#image_over_content_'+btnno);
	$('#edit_advance_image_content').code($('#image_over_content_'+btnno).html());
	$('#banner_id_edit_content_popup').val($('#image_over_content_'+btnno).parent().find('img').attr('data-bannerid'));
	$('#edit_advance_design_image_content_popup').modal('show');
}
$('#save_advance_values').click(function(){
	$('#event_advanced_design_form').submit();
});
$(document).ready(function() {
	$('.othermap1').mapster({
  		selected: true,	
  	});
	$('.map1').mapster({
    	selected: true,
  	});
});
/*$(window).load(function(){
	setInterval(function(){
		$('.lefthandmenu_map').mapster({
	    	selected: true,
	  	});
	  	$('.othermap1').mapster({
  		selected: true,	
  		});
		$('.map1').mapster({
	    	selected: true,
	  	});
  	}, 1500);
});*/
function getpopup(img,sele)
{
	$('#image_area_coords').val(sele.x1 + ',' + sele.y1+','+sele.x2+','+sele.y2);
	$('#image_banner_id').val($(img).attr('data-bannerid'));
	$('#image_area_assign_modules_link_popup').modal('show');
}
$('#submit_image_arear_select').click(function(){
	if($.trim($('#area_menu_id').val())!="")
	{
		$('#area_menu_id_error_span').parent().removeClass('has-error').addClass('has-success');
		$('#area_menu_id_error_span').hide();
		$.ajax({
			url:"<?php echo base_url().'Event/save_banner_area_coords/'.$event['Id'] ?>",
			type:'post',
			data:'coords='+$('#image_area_coords').val()+'&banner_id='+$('#image_banner_id').val()+'&menu_id='+$('#area_menu_id').val(),
			success:function(result){
				$('#mape_'+$('#image_banner_id').val()).append(result);
				$('#map_'+$('#image_banner_id').val()).append(result);
				$('.map1').mapster({
	              selected: true
	            });
	            $('#image_area_assign_modules_link_popup').modal('hide');
			},
		});
	}
	else
	{
		$('#area_menu_id_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#area_menu_id_error_span').show();
	}
});
$('#area_menu_id').change(function(){
	if($.trim($('#area_menu_id').val())!="")
	{
		$('#area_menu_id_error_span').parent().removeClass('has-error').addClass('has-success');
		$('#area_menu_id_error_span').hide();
	}
	else
	{
		$('#area_menu_id_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#area_menu_id_error_span').show();
	}
});
$('#submit_image_area_link').click(function(){
	if($.trim($('#image_area_link').val())!="")
	{
		if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#image_area_link").val()))
		{
			$('#area_link_error_span').parent().removeClass('has-error').addClass('has-success');
			$('#area_link_error_span').hide();
			$.ajax({
				url:"<?php echo base_url().'Event/save_banner_area_coords/'.$event['Id'] ?>",
				type:'post',
				data:'coords='+$('#image_area_coords').val()+'&banner_id='+$('#image_banner_id').val()+'&area_link='+$('#image_area_link').val(),
				success:function(result){
					$('#mape_'+$('#image_banner_id').val()).append(result);
					$('#map_'+$('#image_banner_id').val()).append(result);
					$('.map1').mapster({
		              selected: true
		            });
		            $('#image_area_assign_modules_link_popup').modal('hide');
				},
			});
		}
		else
		{
			$('#area_link_error_span').html("Please Enter valid Url.");
			$('#area_link_error_span').parent().removeClass('has-success').addClass('has-error');
			$('#area_link_error_span').show();
		}
	}
	else
	{
		$('#area_link_error_span').html("This field is required.");
		$('#area_link_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#area_link_error_span').show();
	}
});
$(document).on('click', '.map1', function() {
	var htm='<div class="row"><img data-bannerid="'+$(this).find('img:last').attr('data-bannerid')+'" id="selection_area" usemap="#mape" src="'+$(this).find('img:last').attr('src')+'"><map name="mape" id="mape">'+$('#map_'+$(this).find('img:last').attr('data-bannerid')).html()+"</map></div>";
   	$.colorbox({html:htm,maxWidth: '100%'});
   	$('#selection_area').mapster({
    	selected: true,
  	});
    $('#selection_area').imgAreaSelect({
	    handles: true,
	    autoHide:true,
	    hide:true,
	    onSelectEnd: function(img,selection){
	    	$('#cboxClose').trigger('click');
	    	getpopup(img,selection);
	    }
	});
});
function delete_area(elem,areaid)
{
	if(confirm("Are You Sure Delete This Area?"))
	{
		$.ajax({
			url:"<?php echo base_url().'Event/delete_banner_area/'.$event['Id'].'/' ?>"+areaid,
			success:function(result){
				$(elem).remove();
				$('#area_'+areaid).remove();
				$('.map1').mapster({
			    	selected: true
			    });
			    $('.lefthandmenu_map').mapster({
    				selected: true,
  				});
			},
		});
	}
}
$('.add_new_section_lefthandmenu').click(function(){
	var dividarr=[{"divid":$('#all_filesupload_contener_div_for_lefthandmenu .image_upload_div_backdesign').length}];
	$("#all_filesupload_contener_div_for_lefthandmenu").append($('#lefthandmenu_uploadimages_template_div').tmpl(dividarr));
});
function croppinglefthandmenuadvanceicon(elem,imguploadnumber)
{
	$('#lefthandmenu_filesupload_number').val(imguploadnumber);
	$lefthandmenuadvanceiconeimage.cropper('destroy');
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
    var files = elem.files;
    var file;
    if (files && files.length) {
    	file = files[0];
        if (/^image\/\w+$/.test(file.type)) 
        {
			if (uploadedImageURL) 
			{
				URL.revokeObjectURL(uploadedImageURL);
			}
			uploadedImageURL = URL.createObjectURL(file);
			$lefthandmenuadvanceiconeimage.attr('src', uploadedImageURL);
			$('#lefthandmenu_advanceiconemodelscopetool').modal('toggle');
          	setTimeout(function(){
			    var croppable = false;
		        $lefthandmenuadvanceiconeimage.cropper({
		            built: function () {
		              croppable = true;
		            }
		        });
    		}, 500);
        } 
        else 
        {
          window.alert('Please choose an image file.');
        }
  	}
}
$('#upload_result_btn_lefthandmenu_advance_icone_crop').click(function(){
	var croppedCanvas;
	var fileupload_number=$('#lefthandmenu_filesupload_number').val();
	$('#upload_result_btn_lefthandmenu_advance_icone_crop').html('Croping <i class="fa fa-spinner fa-spin"></i>');
    $('#upload_result_btn_lefthandmenu_advance_icone_crop').attr('disabled','disabled');
    $('#lefthandmenu_advanceiconemodal_heading').html("Adding your Left Hand Menu image. Please wait");
	$('#lefthend_menu_loding_icon_div_advance_images_crop').show();
	$('#lefthandmenu_cropping_advance_icone_div').hide();
    croppedCanvas = $lefthandmenuadvanceiconeimage.cropper('getCroppedCanvas');
    $.ajax({
    	url:"<?php echo base_url().'Event/save_lefthandmenu_image/'.$event['Id'] ?>",
    	type:'post',
    	data:'image='+encodeURIComponent(croppedCanvas.toDataURL())+'&filesupload_number='+fileupload_number+'&bannerid='+$('#lefthandmenu_image_contener_div_'+fileupload_number).find('img:last').attr('data-bannerid'),
    	error:function(error)
    	{
    		$('#upload_result_btn_lefthandmenu_advance_icone_crop').html('Crop');
		    $('#upload_result_btn_lefthandmenu_advance_icone_crop').removeAttr('disabled');
		    $('#lefthandmenu_advanceiconemodal_heading').html("Left Hand Menu Image");
		    $('#lefthandmenu_cropping_advance_icone_div').show();
			$('#lefthend_menu_loding_icon_div_advance_images_crop').hide();
    	},
    	success:function(result)
    	{
    		var data=$.parseJSON(result);
    		$('#lefthandmenu_image_upload_div_'+fileupload_number).find('.imageuploaddiv_preview_leftmenuadvanceimage').attr('src',data.image_url);
    		$('#lefthandmenu_image_upload_div_'+fileupload_number).find('.images_upload_image_name_heding').find('.fileupload-preview').html(data.img_name);
    		if(data.html!="")
    		{
    			$('#lefthandmenu_main_contaner_div').append(data.html);
    		}
    		else
    		{
    			$('#lefthandmenu_image_contener_div_'+fileupload_number).find('img').attr('src',data.image_url);
    		}
    		$('.lefthandmenu_map').mapster({
    			selected: true,
  			});
    		$('#lefthandmenu_advanceiconemodelscopetool').modal('hide');
    		$('#upload_result_btn_lefthandmenu_advance_icone_crop').html('Crop');
		    $('#upload_result_btn_lefthandmenu_advance_icone_crop').removeAttr('disabled');
		    $('#lefthandmenu_advanceiconemodal_heading').html("Left Hend Menu Image");
		    $('#lefthandmenu_cropping_advance_icone_div').show();
			$('#lefthend_menu_loding_icon_div_advance_images_crop').hide();	
    	}
    });
});
$(document).on('click', '.lefthandmenu_map', function() {
	var htm='<div class="row"><img data-bannerid="'+$(this).find('img:last').attr('data-bannerid')+'" id="lefthandmenu_selection_area" usemap="#mape" src="'+$(this).find('img:last').attr('src')+'"><map name="mape" id="mape">'+$('#lefthandmenumap_'+$(this).find('img:last').attr('data-bannerid')).html()+"</map></div>";
   	$.colorbox({html:htm,maxWidth: '100%'});
   	$('#lefthandmenu_selection_area').mapster({
    	selected: true,
  	});
    $('#lefthandmenu_selection_area').imgAreaSelect({
	    handles: true,
	    autoHide:true,
	    hide:true,
	    onSelectEnd: function(img,selection){
	    	$('#cboxClose').trigger('click');
	    	getleftmenupopup(img,selection);
	    }
	});
});
function getleftmenupopup(img,sele)
{
	$('#lefthandmenu_image_area_coords').val(sele.x1 + ',' + sele.y1+','+sele.x2+','+sele.y2);
	$('#lefthandmenu_image_banner_id').val($(img).attr('data-bannerid'));
	$('#lefthandmenu_image_area_assign_modules_link_popup').modal('show');
}
$('#submit_lefthandmenu_image_arear_select').click(function(){
	if($.trim($('#lefthandmenu_area_menu_id').val())!="")
	{
		$('#lefthandmenu_area_menu_id_error_span').parent().removeClass('has-error').addClass('has-success');
		$('#lefthandmenu_area_menu_id_error_span').hide();
		$.ajax({
			url:"<?php echo base_url().'Event/save_banner_area_coords/'.$event['Id'] ?>",
			type:'post',
			data:'coords='+$('#lefthandmenu_image_area_coords').val()+'&banner_id='+$('#lefthandmenu_image_banner_id').val()+'&menu_id='+$('#lefthandmenu_area_menu_id').val(),
			success:function(result){
				$('#lefthandmenu_mape_'+$('#lefthandmenu_image_banner_id').val()).append(result);
				$('#lefthandmenumap_'+$('#lefthandmenu_image_banner_id').val()).append(result);
				$('.lefthandmenu_map').mapster({
	              selected: true
	            });
	            $('#lefthandmenu_image_area_assign_modules_link_popup').modal('hide');
			},
		});
	}
	else
	{
		$('#lefthandmenu_area_menu_id_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#lefthandmenu_area_menu_id_error_span').show();
	}
});
$('#lefthandmenu_submit_image_area_link').click(function(){
	if($.trim($('#lefthandmenu_image_area_link').val())!="")
	{
		if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#lefthandmenu_image_area_link").val()))
		{
			$('#lefthandmenu_area_link_error_span').parent().removeClass('has-error').addClass('has-success');
			$('#lefthandmenu_area_link_error_span').hide();
			$.ajax({
				url:"<?php echo base_url().'Event/save_banner_area_coords/'.$event['Id'] ?>",
				type:'post',
				data:'coords='+$('#lefthandmenu_image_area_coords').val()+'&banner_id='+$('#lefthandmenu_image_banner_id').val()+'&area_link='+$('#lefthandmenu_image_area_link').val(),
				success:function(result){
					$('#lefthandmenu_mape_'+$('#lefthandmenu_image_banner_id').val()).append(result);
					$('#lefthandmenumap_'+$('#lefthandmenu_image_banner_id').val()).append(result);
					$('.lefthandmenu_map').mapster({
		              selected: true
		            });
		            $('#lefthandmenu_image_area_assign_modules_link_popup').modal('hide');
				},
			});
		}
		else
		{
			$('#lefthandmenu_area_link_error_span').html("Please Enter valid Url.");
			$('#lefthandmenu_area_link_error_span').parent().removeClass('has-success').addClass('has-error');
			$('#lefthandmenu_area_link_error_span').show();
		}
	}
	else
	{
		$('#lefthandmenu_area_link_error_span').html("This field is required.");
		$('#lefthandmenu_area_link_error_span').parent().removeClass('has-success').addClass('has-error');
		$('#lefthandmenu_area_link_error_span').show();
	}
});
function lefthandmenu_remove_image_upload_div(elem,divno)
{
	if($('#lefthandmenu_image_contener_div_'+divno).length > 0)
	{
		$.ajax({
			url:"<?php echo base_url().'Event/delete_advance_banner_image/'.$event['Id'].'/' ?>"+$('#lefthandmenu_image_contener_div_'+divno).find('img:last').attr('data-bannerid'),
			success:function(result)
			{
				if($.trim(result)=="success")
				{
					$('#lefthandmenu_image_upload_div_'+divno).remove();
					$('#lefthandmenu_image_contener_div_'+divno).remove();
				}
				else
				{
					alert('Something Went Wrong Please Refresh The Page And try again..');
				}
			}
		});
	}
	else
	{	
		$('#lefthandmenu_image_upload_div_'+divno).remove();
	}
}
</script>  
<style type="text/css">
.map1
{
	height: auto !important;
}
#sortable_modules
{
	min-height: 60px;
}
canvas{
	height: 100%;
}
.banner_list_owl .owl-controls{
	display: none !important;
}
.mapster_el.othermap1, .othermap1{
	width: 100% !important;
	height: auto !important;
}
</style> 