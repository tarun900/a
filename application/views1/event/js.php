<script src="<?php echo base_url(); ?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pages-gallery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>

<script>
    jQuery(document).ready(function() {
//        var time = 7; // time in seconds 
//        var $progressBar,
//        $bar,
//        $elem,
//        isPause,
//        tick,
//        percentTime;
//
//        //Init the carousel
//        $("#owl-demo").owlCarousel({
//        slideSpeed : 500,
//        paginationSpeed : 500,
//        singleItem : true,
//        afterInit : progressBar,
//        afterMove : moved,
//        startDragging : pauseOnDragging
//        });
//
//        //Init progressBar where elem is $("#owl-demo")
//        function progressBar(elem){
//        $elem = elem;
//        //build progress bar elements
//        buildProgressBar();
//        //start counting
//        start();
//        }
//
//        //create div#progressBar and div#bar then prepend to $("#owl-demo")
//        function buildProgressBar(){
//        $progressBar = $("<div>",{
//        id:"progressBar"
//        });
//        $bar = $("<div>",{
//        id:"bar"
//        });
//        $progressBar.append($bar).prependTo($elem);
//        }
//
//        function start() {
//        //reset timer
//        percentTime = 0;
//        isPause = false;
//        //run interval every 0.01 second
//        tick = setInterval(interval, 10);
//        };
//
//        function interval() {
//        if(isPause === false){
//        percentTime += 1 / time;
//        $bar.css({
//        width: percentTime+"%"
//        });
//        //if percentTime is equal or greater than 100
//        if(percentTime >= 100){
//        //slide to next item
//        $elem.trigger('owl.next')
//        }
//        }
//        }
//
//        //pause while dragging
//        function pauseOnDragging(){
//        isPause = true;
//        }
//
//        //moved callback
//        function moved(){
//        //clear interval
//        clearTimeout(tick);
//        //start again
//        start();
//        }

        //uncomment this to make pause on mouseover
        // $elem.on('mouseover',function(){
        // isPause = true;
        // })
        // $elem.on('mouseout',function(){
        // isPause = false;
        // })
        
        $('.place_order').click(function (){
            var month = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.eventname a .monthdata').html();
            var event_id = "<?php echo $Event[0]['Id']; ?>"
            var size_id = $(this).parent().parent().find('.size_id').val();
            window.location.href = '<?php echo base_url(); ?>Order/place_order/?month='+month+'&event_id='+event_id+'&size_id='+size_id;
        });        
    });        
</script>

<script id="eventTemplate" type="text/x-jQuery-tmpl">
    <li class="col-md-3 col-sm-6 col-xs-12 mix ${Category} gallery-img">
        <div class="portfolio-item">
            {{if Image}}
                <a class="thumb-info" href="<?php echo base_url(); ?>assets/user_files/${Image}" data-lightbox="gallery" data-title="${Name}">
                        <img src="<?php echo base_url(); ?>assets/timthumb.php?src=<?php echo base_url(); ?>assets/user_files/${Image}&h=127&w=209" class="img-responsive" alt="">
                        <span class="thumb-info-title"> ${Name} </span>
                </a>
            {{else}}
                <a class="thumb-info" href="assets/images/event_default.jpg" data-lightbox="gallery" data-title="${Name}">
                        <img src="<?php echo base_url(); ?>assets/images/event_default.jpg" class="img-responsive" alt="">
                        <span class="thumb-info-title"> ${Name} </span>
                </a>
            {{/if}}
                    
                <div class="tools tools-bottom">
                    <a href="#">
                        <i class="fa fa-link"></i>
                    </a>
                    <a href="<?php echo base_url(); ?>Event/edit/${Event_id}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a href="<?php echo base_url(); ?>Event/delete/${Event_id}">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
        </div>
    </li>
</script>