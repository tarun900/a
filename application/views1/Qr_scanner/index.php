<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="qr_scanner">
            <div class="panel-body" style="padding: 0px;">
            </div>

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#menu_setting" data-toggle="tab">
                            Menu Setting
                        </a>
                    </li>
                    <li class="">
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View Events
                        </a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade active in" id="menu_setting">
                    <div class="row padding-15">
                        <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                            <div class="col-md-12 alert alert-info">
                                <h5 class="space15">Menu Title & Image</h5>
                                <div class="form-group">
                                    <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo $title; ?>">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                </div>
                                <div class="form-group">
                                   <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail"></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                        <div class="user-edit-image-buttons">
                                             <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                  <input type="file" name="Images[]">
                                             </span>
                                             <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                  <i class="fa fa-times"></i> Remove
                                             </a>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                        Create Home Tab
                                    </label>
                                    <?php if($img !='') { ?>
                                    <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                    <?php } ?>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                    <div class="col-sm-5">
                                        <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                    <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                    </div>

                    <!-- <div class="light intro">
                        <div class="row">
                            <section id="demo">
                                <figure id="devicePreview" class="phone">
                                    <iframe src="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>" scrolling="auto" frameborder="0"></iframe>
                                    <a class="laptopLink" target="_blank" href="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>">
                                        <img class="laptopImage hide" style="left: 145px;top: 46px;position: absolute;width: 67.3%;height: 383px;" src="<?php echo base_url(); ?>images/event_dummy.jpg">
                                    </a>
                                </figure>
                            </section>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end: PAGE CONTENT-->