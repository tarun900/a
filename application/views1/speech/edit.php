<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Speech</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="row">
                        <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Updated Successfully.
                        </div>
                        <?php } ?>

                            <div class="form-group">
                                <label class="col-sm-2">Speech</span><span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <textarea name="Speech_text" id="Speech_text" placeholder="Speech" class="summernote"><?php echo $speech_events[0]['Speech_text']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Documents</label>
                                <div class="col-sm-9">
                                    <?php $documents_array = json_decode($speech_events[0]['Documents']); ?>
                                    <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                        <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new documents">Select file</span><span class="fileupload-exists">Change</span>
                                                <input type="file" name="Documents[]" multiple>
                                        </span>
                                    </div>
                                    <?php for($i=0;$i<count($documents_array);$i++) { ?>
                                        <div class="col-sm-3 fileupload-new thumbnail center" style="float:left;margin-right:10px;width: auto;height: auto;border: none;">
                                            <?php 
                                            $fname = $documents_array[$i];
                                            $ext = pathinfo($fname, PATHINFO_EXTENSION);
                                            //echo $ext; exit; 
                                            if($ext == 'docx' || $ext == 'doc')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/speech_documents/<?php echo $fname; ?>"><img style="height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/docs.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'pdf')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/speech_documents/<?php echo $fname; ?>"><img style="height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/pdf.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'ppt')
                                            {
                                            
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/speech_documents/<?php echo $fname; ?>"><img style="height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/ppt.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'odg')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/speech_documents/<?php echo $fname; ?>"><img style="height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/ppt.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'txt')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/speech_documents/<?php echo $fname; ?>"><img style="height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/txt2.png"></a>
                                            <?php
                                            }
                                            else
                                            {
                                                //echo 555; exit;
                                            }
                                            ?>
                                            <a class="btn btn-red remove_image" href="javascript:;" style="padding: 3px;margin-top: 0px;"><i class="fa fa-times fa fa-white"></i></a>
                                            <input type="hidden" name="old_documents[]" value="<?php echo $documents_array[$i]; ?>">
                                        </div>
                                    <?php } ?>                                                                                                 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2">Events Name</span></label>
                                <div class="col-sm-5">
                                     <select id="form-field-select-4" class="form-control" name="Event_id">
                                        <?php 
                                            echo"<option>Select Evet Name</option>";
                                            foreach ($speaker_events as $key => $value) 
                                            { 
                                            ?>
                                                <option value="<?php echo $value['Event_id']; ?>" <?php if($speaker_events[0]['Event_id']==$value['Event_id']) { ?> selected <?php } ?>><?php echo $value["Event_name"]; ?></option>
                                            <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2">Agenda Name</span> </label>
                                <div class="col-sm-5">
                                    <select id="form-field-select-4" class="form-control" name="Agenda_id">
                                        <?php 
                                            echo"<option>Select Agenda Name</option>";
                                            foreach ($speaker_agenda as $key => $value) 
                                            { 
                                            ?>
                                                <option value="<?php echo $value['Id']; ?>" <?php if($speaker_agenda[0]['Heading']==$value['Heading']) { ?> selected <?php } ?>><?php echo $value["Heading"]; ?></option>
                                            <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                        <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>              
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .form-horizontal .form-group
    {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
</style>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<!-- end: PAGE CONTENT-->