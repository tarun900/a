<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Group</span></h4>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="group_name">
                        Group Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Group Name" class="form-control required" id="group_name" name="group_name" value="<?php echo $group_data['group_name']; ?>">
                        </div>
                    </div>
                    <div class="form-group" id="keyword">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Group Q&A Sessions <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <select style="height:auto;" multiple="multiple" class="select2-container select2-container-multi form-control search-select menu-section-select" name="group_maps[]">
                                <?php $selected = array_column($group_data['reation_data'],'module_id');
                                foreach ($session as $key => $value) { ?>
                                <option value="<?=$value['Id'];?>" <?=(in_array($value['Id'],$selected))?'selected=selected':''?> ><?=$value['Session_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                        Group Icon 
                        </label>
                        <div class="modal fade" id="groupiconcropetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Group Icon</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row"  id="cropping_banner_div">
                                            <div>
                                                <img class="img-responsive" id="show_crop_icons_model" src="" alt="Picture">
                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <button type="button" class="btn btn-green btn-block" id="upload_result_btn_icon_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <input type="hidden" value="" name="">
                                <div class="fileupload-preview fileupload-exists thumbnail" id="group_icons_preview_images">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                <input type="file" id="group_icons" name="group_icons">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_categorie_icons_data"><i class="fa fa-times"></i> Remove</a>
                            </div>
                            <input type="hidden" name="group_icons_crop_data" id="group_icons_crop_data">
                            <?php if(!empty($group_data['group_image'])){ ?>
                            <div class="col-sm-3 fileupload-new thumbnail center">
                                <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/group_icon/<?php echo $group_data['group_image']; ?>"><!-- <a class="btn btn-red remove_image" onClick="remove_icon(<?php echo $group_data['Id']; ?>);" href="javascript:void(0);" style=""><i class="fa fa-times fa fa-white"></i></a> -->
                                <label class="btn btn-red" style="float: right;margin: 10px;">
                                    <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form').submit();">
                                    <i class="fa fa-times"></i> Remove
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1"></label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
    function remove_icon(id)
    {
      if(confirm("Are You Sure Delete This??"))
      {
        window.location.href="<?php echo base_url().'Exhibitor/delete_categorie_images/'.$this->uri->segment(3).'/' ?>"+id;
      }
    }
    var $bannerimage = $("#show_crop_icons_model");
    var $inputBannerImage = $('#group_icons'); 
    $(document).ready(function() {
      $('#Short_desc').tagsInput({
        maxTags: 6,
        allowSpaces: false,
        removeWithBackspace : true
      });
      $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
          file = files[0];
          if (/^image\/\w+$/.test(file.type)) {
            if (uploadedImageURL) {
              URL.revokeObjectURL(uploadedImageURL);
            }
            uploadedImageURL = URL.createObjectURL(file);
            $bannerimage.attr('src', uploadedImageURL);
            $('#group_icons_crop_data').val('');
            $('#groupiconcropetool').modal('toggle');
          } else {
            window.alert('Please choose an image file.');
          }
        }
      });
      $(document).on('shown.bs.modal','#groupiconcropetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_icon_crop');
        $bannerimage.cropper({ 
          aspectRatio: 1, 
          built: function () {
            croppable = true;
          }
        });
        $button.on('click', function () {
          var croppedCanvas;
          if (!croppable) {
            return;
          }
          croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
          $('#group_icons_preview_images').html("<img src='"+croppedCanvas.toDataURL()+"'>");
          $('#group_icons_crop_data').val(croppedCanvas.toDataURL());
          $("#group_icons").val('');
        });
      }).on('hidden.bs.modal',function(){
        $bannerimage.cropper('destroy');
      }); 
    });
</script>