<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#session_list" data-toggle="tab">
                            Add Session List
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
                </ul>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="session_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Session Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <input type="text" placeholder="Session Name" id="Session_name" name="Session_name" class="form-control required name_group">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Session Description <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <textarea type="text" height="200px;" placeholder="Session Description" id="Session_description" name="Session_description" class="form-control required name_group"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Moderator/Speaker Assigned <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
									<select class="form-control required name_group" name="Moderator_speaker_id">
										<option value="">Select User</option>
                                        <?php foreach ($speaker_moderator as $key => $value) { ?>
                                        <option value="<?php echo $value['Id'] ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
                                        <?php } ?>
									</select>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Start Time <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div class="input-group input-append bootstrap-timepicker">
                                        <input type="text" id="start_time" name="start_time" class="form-control time-picker required name_group">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    End Time <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div class="input-group input-append bootstrap-timepicker">
                                        <input type="text" id="end_time" name="end_time" class="form-control time-picker required name_group">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Date <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                        <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                        <input type="text" name="session_date" id="session_date" contenteditable="false" class="form-control required name_group" value="<?php echo date('m/d/Y'); ?>">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Only show moderated questions to Attendees 
                                </label>
                                <div class="col-sm-2">
                                    <input type="checkbox" name="is_closed_qa">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-2">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
							<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							<a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#session_date').datepicker({
        weekStart: 1,
        startDate: '-0d',
        endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
        autoclose: true
    });
});
</script>