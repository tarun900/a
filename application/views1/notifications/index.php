<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: INBOX PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading" style="padding-bottom: 25px;">
                <h4 class="panel-title">Inbox</h4>
                <?php if($user->Role_name=='Administrator'){ ?>
                    <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Notifications/add"><i class="fa fa-plus"></i> Add Notifications</a>
                <?php } ?>
            </div>
            <div class="panel-body messages">
                <ul class="messages-list col-md-4 panel-scroll height-350 scroll-page">
                    <li class="messages-search">
                        <form action="#" class="form-inline" onsubmit="return false;">
                            <div class="input-group">
                                <input type="text" id="search_msg" name="search_msg" class="form-control" placeholder="Search messages...">
                                <div class="input-group-btn">
                                    <button class="btn btn-green" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </li>
                    <span id="msg_container">
                    <?php
                    $valuekey="";
                    foreach ($Notifications as $key=>$data)
                    {  
                        if($notify_id==$data['id'] &&  $notify_user_id==$data['Userid'])
                        {
                            $valuekey=$key;
                        }
                    ?>
                    <li class="messages-item messages-item <?php if($key==$valuekey && $valuekey!=""){ echo "active ".$valuekey;} ?> <?php if($data['Read_unread']=='unread' && $user->Role_name!='Administrator' ){ echo "unread_msg";}?> starred " id="<?php echo $data['id'].'_'.$data['Userid']; ?>" onclick="displaymailcontent(<?php echo $data['id']; ?>,<?php echo $data['Userid']; ?>,'<?php echo $data['id'].'_'.$data['Userid']; ?>')">
                        
                        <span title="<?php echo $data['Title']; ?>" class="messages-item-star"></span>
                        <?php if($data['Logo'] != '') {  ?>
                                <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $data['Logo']; ?>" class="messages-item-avatar">
                        <?php } else {?>
                                <img src="<?php echo base_url(); ?>assets/images/anonymous.jpg" alt=""  class="messages-item-avatar">
                        <?php } ?>                        
                        <span class="messages-item-from"><?php echo $data['Toname']; ?></span>
                        <div class="messages-item-time">
                            <span class="text">
                                <?php
                                    if(date('Y-m-d')==date('Y-m-d',strtotime($data['Created_date'])))
                                    {
                                        echo date('H:s A',strtotime($data['Created_date'])); 
                                    }
                                    else
                                    {
                                        echo date('d M y',strtotime($data['Created_date']));
                                    }
                                ?>
                            </span>                            
                        </div>
                        <span class="messages-item-subject">
                            <?php
                                if(strlen($data['Title'])>35)
                                {
                                    echo substr($data['Title'], 0, 35).'...';
                                }
                                else
                                {
                                    echo $data['Title'];
                                }
                            ?>
                        </span>
                        <span class="messages-item-preview">
                            <?php
                                if(strlen($data['Summary'])>73)
                                {
                                    echo substr($data['Summary'], 0, 73).'...';
                                }
                                else
                                {
                                    echo $data['Summary'];
                                }
                            ?>
                        </span>
                    </li>
                    <?php
                    }
                    ?>
                    </span>
                </ul>
                <?php 
                if($valuekey=="")
                {
                    $valuekey=0;
                }
                if(!empty($Notifications))
                {
                    $content = $Notifications[$valuekey]['content'];
                    
                    $slug = "NOTIFICATION";                
                    $em_template = $this->setting_model->back_email_template($slug);
                    $em_template = $em_template[0];
                    $msg = $em_template['Content'];
                    $patterns = array();
                    $patterns[0] = '/{{name}}/';
                    $patterns[1] = '/{{content}}/';
                    $replacements = array();
                    $replacements[0] = $Notifications[$valuekey]['Toname'];
                    $replacements[1] = $content;                    
                    $msg = preg_replace($patterns, $replacements, $msg );
                    
                ?>
                <div class="messages-content col-md-8" id="mesg_container">
                    <div class="message-header">
                        <div class="message-time">
                            <?php echo date('d M Y, h:i:s A',  strtotime($Notifications[$valuekey]['Created_date'])); ?>
                        </div>
                        <div class="message-from">
                            <?php echo $Notifications[$valuekey]['Fromname']; ?>&lt;<?php echo $Notifications[$valuekey]['Fromemail']; ?>&gt;
                        </div>
                        <div class="message-to">
                            To: <?php echo $Notifications[$valuekey]['Toemail']; ?>
                        </div>
                        <div class="message-subject">
                            <?php echo $Notifications[$valuekey]['Title']; ?>
                        </div>
                        
                        
                        <div class="message-actions">
                            <a title="Move to trash" href="javascript:void(0)" onclick="deletenotification(<?php echo $Notifications[$valuekey]['id']; ?>,<?php echo $Notifications[$valuekey]['Userid']; ?>)"><i class="fa fa-trash-o"></i></a>
                            <?php if($user->Role_name == "Administrator") { ?>
                                <a href="<?php echo base_url(); ?>Notifications/edit/<?php echo $Notifications[$valuekey]['id']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                        </div>
                        
                    </div>
                    <div class="message-content">
                        <p>
                            <?php echo $content; ?>
                        </p>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
        <!-- end: INBOX PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->