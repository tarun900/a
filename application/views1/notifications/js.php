<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
                
<script>
	jQuery(document).ready(function() {
		TableData.init();
	});
        
        function displaymailcontent(id,userid,currentid)
        {
           
           $.ajax({
                url : '<?php echo base_url(); ?>Notifications/displaycontent',
                data :'id='+id+'&userid='+userid,
                type: "POST",           
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
                        $('#forgotemail').parent().find('.control-label span').removeClass('valid');
                        $('#forgot_msg').html(values[1]);
                        $("#forgot_msg").removeClass('no-display');
                        $("#forgot_msg").removeClass('alert-success');
                        $("#forgot_msg").fadeIn();
                    }
                    else
                    {
                        var obj = jQuery.parseJSON(values[1]);                    
                        var data='<div class="message-header"><div class="message-time">'+ obj.Created_date+'</div><div class="message-from">'+obj.Fromname+'&lt;'+obj.Fromemail+'&gt;</div><div class="message-to">To: '+obj.Toemail+'</div><div class="message-subject">'+obj.Title+'</div><div class="message-actions">';
                        
                        
                        <?php if($user->Role_name=='Administrator'){ ?>
                            data+='<a title="Move to trash" href="#" onclick="deletenotification('+obj.id+','+obj.Userid+')"><i class="fa fa-trash-o"></i></a>';
                        <?php }else
                            {?>
                            data+='<a title="Move to trash" href="#" onclick="softdeletenotification('+obj.id+','+obj.Userid+')"><i class="fa fa-trash-o"></i></a>';
                            <?php
                            }?>
                        <?php if($user->Role_name=='Administrator'){ ?>
                        data+='<a href="<?php echo base_url(); ?>Notifications/edit/'+obj.id+'" title="Edit"><i class="fa fa-pencil"></i></a>';
                        <?php } ?>
                        data+='</div></div><div class="message-content panel-scroll height-350"><p>'+obj.content+'</p></div>';
                        $("#mesg_container").html('');
                        $("#mesg_container").html(data);
                        if($(".panel-scroll").length && $body.hasClass("isMobile") == false) {
                            $('.panel-scroll').perfectScrollbar({
                                    wheelSpeed: 50,
                                    minScrollbarLength: 20,
                                    suppressScrollX: true
                            });
                        }
                        $( ".messages-list li" ).each(function( index ) 
                        {
                            $(this).removeClass('active');
                        });
                        $("#"+currentid).addClass('active');
                        
                        <?php
                        if($this->data['user']->Role_name != 'Administrator')
                        {
                        ?>
                            $("#"+currentid).removeClass('unread_msg');
                        <?php
                        }
                        ?>
                    }
                }
            });            
        }
        
        function deletenotification(id,userid)
        {           
            $.ajax({
            url : '<?php echo base_url(); ?>Notifications/deletenotification',
            data :'id='+id+'&userid='+userid,
            type: "POST",           
            success : function(data)
               {
                   var values=data.split('###');
                   if(values[0]=="error")
                   {   
                       $("#userdata").html('');
                       $("#userdata").html(values[1]);
                   }
                   else
                   {
                      window.location.reload();
                   }
               }
           });
            
        }
        
        function softdeletenotification(id,userid)
        {           
            $.ajax({
            url : '<?php echo base_url(); ?>Notifications/softdeletenotification',
            data :'id='+id+'&userid='+userid,
            type: "POST",           
            success : function(data)
               {
                   var values=data.split('###');
                   if(values[0]=="error")
                   {   
                       $("#userdata").html('');
                       $("#userdata").html(values[1]);
                   }
                   else
                   {
                      window.location.reload();
                   }
               }
           });
            
        }
        
        var timeoutReference;
        $(document).ready(function() 
        {
            $('input#search_msg').keypress(function() 
            {
                var el = this; // copy of this object for further usage

                if (timeoutReference) clearTimeout(timeoutReference);
                timeoutReference = setTimeout(function() 
                {
                    doneTyping.call(el);
                }, 800);
            });
            
            $('input#search_msg').blur(function(){
                timeoutReference = 2;
                doneTyping.call(this);
            });
            
            if(!$('#msg_container li').hasClass('active'))
            { 
                $('#msg_container').find('li').first().addClass('active');
            }
        });

    function doneTyping(){
        // we only want to execute if a timer is pending
        if (!timeoutReference){
            return;
        }
        // reset the timeout then continue on with the code
        timeoutReference = null;

        //
        // Code to execute here
        //
        
        search_msg(this.value);        
    }        
    
    function search_msg(name)
    {
        $.ajax({
           type: 'POST',
           data: 'searchtext='+name,
           url: '<?php echo base_url(); ?>Notifications/searchmsg',
           dataType: "json",
           success: function(result)
           {               
                $("#msg_container").html('');
                $( result ).each(function( index, value ) 
                { 
                    $("#notifiyTemplate").tmpl(value).appendTo("#msg_container");
                });
                $('#msg_container').find('li').first().addClass('active'); 
                displaymailcontent(result[0]['id'],result[0]['Userid'],result[0]['id']+'_'+result[0]['Userid']);
           }
        });
    }
</script>
<script id="notifiyTemplate" type="text/x-jQuery-tmpl">
    ${( $data.unreadmsg = '' ),''}
    
    {{if Read_unread=="unread"}}
        <?php 
            if($user->Role_name!='Administrator')
            {
        ?>
                ${($data.unreadmsg = 'unread_msg' ),''}
        <?php 
            }
            else
            {
                ?>
                ${( $data.unreadmsg = '' ),''}
                <?php
            } 
        ?>
    {{/if}}
            
    <li class="messages-item messages-item ${unreadmsg} starred " id="${id}_${Userid}" onclick="displaymailcontent(${id},${Userid},'${id}_${Userid}')">
        <span title="${Title}" class="messages-item-star"></span>
        {{if Logo!='' }}
                <img src="<?php echo base_url(); ?>assets/user_files/${Logo}" class="messages-item-avatar">
        {{else}}
                <img src="<?php echo base_url(); ?>assets/images/anonymous.jpg" alt=""  class="messages-item-avatar">
        {{/if}}                       
        <span class="messages-item-from">${Toname}</span>
        <div class="messages-item-time">
            <span class="text">
                ${Created_date}
            </span>                            
        </div>
        <span class="messages-item-subject">
            ${Title}
        </span>
        <span class="messages-item-preview">
               ${Summary}
        </span>
    </li>
</script>