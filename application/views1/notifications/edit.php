<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Notification</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>                        
            <div class="panel-body">
                <form action="<?php echo base_url(); ?>Notifications/edit/<?php echo $Notifications['Id']; ?>" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    Title <span class="symbol required"></span>
                                </label>
                                <input type="text" placeholder="Title" class="form-control" id="Title" name="Title" value="<?php echo $Notifications['Title']; ?>">                                                                    
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Summary <span class="symbol required"></span>
                                </label>
                                <input type="text" placeholder="Summary" class="form-control" id="Summary" name="Summary" value="<?php echo $Notifications['Summary']; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Content <span class="symbol required"></span>
                                </label>
                                <textarea class="summernote" id="Content" name="Content" cols="10" rows="10"><?php echo $Notifications['Content']; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form-field-select-1">
                                            Notification Type <span class="symbol required"></span>
                                        </label>
                                        <select id="Notification_by" class="form-control" name="Notification_by">
                                            <option value="">Select...</option>                                                                                
                                            <option value="system" <?php if ($Notifications['Notification_by'] == "system") {
    echo "selected";
} ?>>System</option>                                                                                
                                            <option value="email" <?php if ($Notifications['Notification_by'] == "email") {
    echo "selected";
} ?>>Email</option>
                                        </select>
                                    </div>
                                </div>                                                                
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form-field-select-1">
                                            Type <span class="symbol required"></span>
                                        </label>
                                        <select id="User_type" class="form-control" name="User_type" onchange="displayuser(0);">
                                            <option value="">Select...</option>                                                                                
                                            <option value="Client" <?php if ($Notifications['User_type'] == "Client") {
    echo "selected";
} ?>>Client</option>                                                                                
                                            <option value="Attendee" <?php if ($Notifications['User_type'] == "Attendee") {
    echo "selected";
} ?>>Attendee</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="form-group" id="userdata">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-field-select-1">
                                    Schedule <span class="symbol required"></span>
                                </label>
                                <select id="Schedule" class="form-control" name="Schedule" onchange="showsheduledate();">
                                    <option value="">Select...</option>                                                                                
                                    <option value="1" <?php if ($Notifications['Schedule'] == "1") {
    echo "selected";
} ?>>Yes</option>                                                                                
                                    <option value="0" <?php if ($Notifications['Schedule'] == "0") {
    echo "selected";
} ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group" id="showdate" style="display: none;">
                                <label for="form-field-select-1">
                                    Schedule Date <span class="symbol required" id="showdate_remark"></span>
                                </label>
                                <div class="input-group">
                                    <input type="text" name="Schedual_date" id="Schedual_date" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control date-picker">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                </div>
                                
                            </div>
                            <div class="form-group" id="schedulefor" style="display: none;">
                                <label for="form-field-select-1">
                                    Schedule For <span class="symbol required"></span>
                                </label>
                                <select id="Schedule_for" class="form-control" name="Schedule_for">
                                    <option value="">Select...</option>
                                    <option value="0"  <?php if ($Notifications['Schedule_for'] == "0"){
    echo "selected";
} ?>>Once</option>
                                    <option value="1" <?php if ($Notifications['Schedule_for'] == "1"){
    echo "selected";
} ?>>Week</option>                                                                                
                                    <option value="2" <?php if ($Notifications['Schedule_for'] == "2"){
    echo "selected";
} ?>>Month</option>                                                                                
                                    <option value="3" <?php if ($Notifications['Schedule_for'] == "3"){
    echo "selected";
} ?>>Year</option>                                                                                
                                </select>
                            </div>
                        </div>							
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <button class="btn btn-green btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->