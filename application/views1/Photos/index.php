<?php
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
?>
<div class="row margin-left-10">
    <?php
         if(!empty($user)):   
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
          
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Photos/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('11', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">

    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>

<div class="agenda_content">
    <div class="panel panel-white">
        <div class="panel-body">
            <!-- GRID -->
            <ul id="Grid" class="list-unstyled" style="min-height: 0;">
                <?php for ($i = 0; $i < count($photo); $i++) { ?>
                <?php $image_array = json_decode($photo[$i]['Images']); ?>
                <?php foreach ($image_array as $key => $value) { ?>
                    <li style="display:block !important;" class="col-md-3 col-sm-6 col-xs-12 mix category_1 gallery-img" data-cat="1">
                        <div class="portfolio-item">
                            <a class="thumb-info" href="<?php echo base_url();?>assets/user_files/<?php echo $value; ?>" data-lightbox="gallery" data-title="Website">
                                <img style="width: 300px;height: 175px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value; ?>" class="img-responsive" alt="">
                                <span class="thumb-info-title"> <?php echo $photo[$i]['Event_name']; ?> </span>
                            </a>
                        </div>
                    </li>
                <?php  } } ?>
                <li class="gap"></li>
            </ul>
            
        </div>
    </div>
</div>
<script type="text/javascript">
  function add_advertise_hit()
  {
     
      $.ajax({
                url : '<?php echo base_url().Agenda."/".$Subdomain ?>/add_advertise_hit',
                type: "POST",  
                async: false,
                success : function(data1)
                {

                }
             });
  }
</script>