<?php
$Sid = $this->uri->segment(4);
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
?>

<div class="agenda_content agenda-user-content">
    <?php if(!empty($user)): ?>
     <div class="row row-box">
          <form class="navbar-search" id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Speakers/<?php echo $acc_name."/".$subdomain; ?>/upload_imag' style="clear:both">
               <div class="facebok-container">
                    <div class="facebok-main">
                         <div class="facebok-head">
                              <ul>
                                   <li class="facebok-head-photo"> 
                                        <span> Add Photo </span>
                                        <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/></div>
                                        <div id='imageloadbutton'>
                                             <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple />
                                        </div>
                                   </li>
                              </ul>
                         </div>
<!--                         <div class="facebok-middle">
                              <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text search-query span2" id="searchbox" placeholder="Type your message here..." id="Message" name="Message"></textarea>
                         </div>-->
                         <div class="fb-upload-img-box">
                              <ul id='preview'>
                              </ul>
                              <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                         </div>
                         <div class="facebok-footer clearfix">
                              <ul class="footer-left">
                                   <li class="camera-icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                              </ul>
                              <ul class="footer-right">
<!--                                   <li class="public-box">
                                        <select name="ispublic" id="ispublic">
                                             <option value="0">Private</option>
                                             <option value="1">Public</option>
                                        </select>
                                   </li>-->
                                   <input type="hidden" value="1" name="ispublic">
<!--                                   <li id="private_to">
                                        <select name="Receiver_id" id="Receiver_id">
                                             <option value="">All</option>
                                             <optgroup label="Speaker">
                                                  <?php
                                                  foreach ($speakers as $keysp => $values)
                                                  {
                                                       foreach ($values as $keyspeaker => $valuespeaker)
                                                       {
                                                            ?>
                                                            <option value="<?php echo $valuespeaker['Id']; ?>"><?php
                                                                 if ($valuespeaker['Firstname'] == "")
                                                                 {
                                                                      echo "User";
                                                                 }
                                                                 else
                                                                 {
                                                                      echo $valuespeaker['Firstname'] . ' ' . $valuespeaker['Lastname'];
                                                                 }
                                                                 ?></option>
                                                                 <?php
                                                       }
                                                  }
                                                  ?>
                                             </optgroup>
                                             <optgroup label="Attendee">
                                                  <?php
                                                  foreach ($attendees as $keyat => $valuea)
                                                  {
                                                       foreach ($valuea as $keyattendee => $valueattendee)
                                                       {
                                                            ?>
                                                            <option value="<?php echo $valueattendee['Id']; ?>"><?php
                                                  if ($valueattendee['Firstname'] == "")
                                                  {
                                                       echo "User";
                                                  }
                                                  else
                                                  {
                                                       echo $valueattendee['Firstname'] . ' ' . $valueattendee['Lastname'];
                                                  }
                                                            ?></option>
                                                                 <?php
                                                       }
                                                  }
                                                  ?>
                                             </optgroup>
                                        </select>
                                   </li>-->
                                   <li class="submit-button-box">
                                        <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                             Upload Photo <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                   </li>
                              </ul>
                         </div>
                    </div>    
               </div>
          </form> 
          <div class="tabbable msg_section">
               <div class="tab-content clearfix box-effect clearfix">
                    <div class="tab-pane fade in active clearfix" id="myTab6_example1">
                         <div class="">     


                              <?php
                              $Sid = $this->uri->segment(4);
                              $user = $this->session->userdata('current_user');
                              $lid = $user[0]->Id;
                              ?>
                              <div class="col-md-12 col-lg-12 col-sm-12" id="preview_msg_public">

                                   <?php
                                   echo '<div style="padding-top: 20px;">
                                   <div id="messages">';
                                   echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Feed<br/></h3>';
                                   foreach ($view_chats1 as $key => $value)
                                   {
                                      
                                        $arr=explode(',',$value['user_id']);
                                        
                                        $like=0;

                                        $like=count(array_filter($arr));

                                        echo "<div class='message_container'>";

                                        if ($value['Sender_id'] == $user[0]->Id)
                                        {
                                             echo "<div class='msg_edit-view-box'>";
                                             echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                                             echo "</div>";
                                             echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                                             echo "Delete";
                                             echo "</div>";
                                             echo "</div>";
                                        }

                                        echo "<div class='msg_main_body'>";
                                        echo "<div class='message_img'>";
                                        if ($value['Senderlogo'] != "")
                                        {
                                             echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                                        }
                                        else
                                        {
                                             echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                                        }
                                        echo "</div>";
                                        echo "<div class='msg_fromname'>";
                                        $t=time().$key;
                                        echo '<a href="#" class="tooltip_data">';
                                        echo $value['Sendername'];
                                        echo '<span>
                                                  <img class="callout" src="' . base_url() . 'assets/images/callout.gif" />';
                                                  echo '<div class="clearfix">
                                                  <div class="thread-image">';
                                                       if ($value['Senderlogo'] != "")
                                                       {
                                                            echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                                                       }
                                                       else
                                                       {
                                                            echo '<img src="' . base_url() . '/assets/images/anonymous.jpg">';
                                                       }
                                                  echo '</div>
                                                       <div class="thread-content">
                                                            <div class="author">'.$value['Sendername'].'</div>
                                                            <div class="companynamediv">'.$value['SenderCompnayname'].'</div>';
                                                  
                                                  if($value['SenderIsprofile']==1)
                                                  {
                                                  echo '<div class="companynamediv">'.$value['SenderStreet'].'</div>
                                                            <div class="companynamediv">'.$value['SenderSuburb'].'</div>
                                                            <div class="companynamediv">'.$value['SenderState'].'</div>
                                                            <div class="companynamediv">'.$value['SenderCountry'].'</div>
                                                            <div class="companynamediv">'.$value['SenderPostcode'].'</div>
                                                            <div class="companynamediv">'.$value['SenderMobile'].'</div>
                                                            <div class="companynamediv">'.$value['SenderPhone_business'].'</div>';
                                                  }
                                                  echo '</div>';
                                                  echo '<button type="button" class="usercontanct_'.$value['Sender_id'].'_'.$t.'" data-href="' . base_url() . 'MyContact/'. $subdomain .'/getuserdetails/' . $value['Sender_id'] . '">';
                                                  echo "Share Contact with ".$value['Sendername'];
                                                  echo '</button>';
                                                  echo '</div>';   
                                                  echo'</span></a>';
                                        
//                                        echo '<a class="usercontanct_'.$value['Sender_id'].'_'.$t.'" href="' . base_url() . 'MyContact/'. $subdomain .'/getuserdetails/' . $value['Sender_id'] . '">';
//                                        echo $value['Sendername'];
//                                        echo '</a>';
                                        echo "</div>";
                                        if (!empty($value['Receiver_id']))
                                        {
                                             echo "<div class='msg_with'>";
                                             echo "with";
                                             echo "</div>";
                                             echo "<div class='msg_toname'>";
                                             $t=time().$key;
                                             echo '<a href="#" class="tooltip_data">';
                                             echo $value['Recivername'];
                                             
                                             echo '<span>
                                                  <img class="callout" src="' . base_url() . 'assets/images/callout.gif" />';
                                             echo '<div class="clearfix">
                                             <div class="thread-image">';
                                                  if ($value['Reciverlogo'] != "")
                                                  {
                                                       echo '<img src="' . base_url() . '/assets/user_files/' . $value['Reciverlogo'] . '" >';
                                                  }
                                                  else
                                                  {
                                                       echo '<img src="' . base_url() . '/assets/images/anonymous.jpg">';
                                                  }
                                             echo '</div>
                                                  <div class="thread-content">
                                                       <div class="author">'.$value['Recivername'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverCompnayname'].'</div>';
                                             
                                             if($value['ReciverIsprofile']==1)
                                             {
                                             echo '<div class="companynamediv">'.$value['ReciverStreet'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverSuburb'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverState'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverCountry'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverPostcode'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverMobile'].'</div>
                                                       <div class="companynamediv">'.$value['ReciverPhone_business'].'</div>';
                                             }
                                             echo'</div>';
                                             echo '<button type="button" class="usercontanct_'.$value['Receiver_id'].'_'.$t.'" data-href="' . base_url() . 'MyContact/'. $subdomain .'/getuserdetails/' . $value['Receiver_id'] . '">';
                                             echo "Share Contact with ".$value['Recivername'];
                                             echo '</button>';
                                             echo '</div>';     
                                             echo'</span></a>';                                               
                                             
//                                             echo '<a class="usercontanct_' .$value['Receiver_id'] .'_'.$t. '" href="' . base_url() . 'MyContact/'. $subdomain .'/getuserdetails/' . $value['Receiver_id'] . '">';
//                                             echo $value['Recivername'];
//                                             echo "</a>";
                                             echo "</div>";
                                        }
                                        echo "</div>";

                                        echo "<div class='msg_date'>";
                                        echo timeAgo(strtotime($value['Time']));
                                        echo "</div>";
                                        //echo "<div class='msg_message'>";
                                        //echo $value['Message'];
                                        //echo "</div>";


                                        $img_data = json_decode($value['image']);
                                        foreach ($img_data as $kimg => $valimg)
                                        {
                                             echo "<div class='msg_photo photos_feed'>";
                                             echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                                             echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                                             echo "</a>";
                                             echo "</div>";
                                        }

                                             echo "<div style='clear:both'>";


                                             if(in_array($user[0]->Id, $arr))
                                             {
                                                  $str="dislike_".$value['Id'];
                                                  $post_id=$value['Id'];
                                                  ?>
                                                  <div class='like blue_like' id="<?php echo 'like_'.$value['Id'];?>" ><a href='#'  class="like_btn blue_btn" onclick="unlike('<?php echo $user[0]->Id;?>','<?php echo $post_id;?>','<?php echo $str;?>')" ><i class='fa fa-thumbs-up'></i>Like</a></div>
                                                  <?php 
                                             }
                                             else
                                             {      
                                                  $post_id=$value['Id'];
                                                  $str1="like_".$value['Id'];?>
                                                  <div class='like' id="<?php echo 'like_'.$value['Id'];?>"><a href='#' class='like_btn'  onclick="like('<?php echo $user[0]->Id;?>','<?php echo $post_id;?>','<?php echo $str1;?>')" ><i class='fa fa-thumbs-up'></i>Like</a></div>
                                                  <?php
                                             }

							          echo "</div>";
                                             $post_id=$value['Id'];
								     echo "<div id='".$post_id."'>";
                                             echo "&nbsp;comments   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$like." people like this";
                                             echo "<span style='float:left;padding:0px;'>".count($view_chats1[$key]['comment'])."  </span>";
                                             echo "</div>";
                                             echo "<div class='toggle_comment msg_toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                                             echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                                        
                                        if ($user[0]->Logo!="")
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                        }
                                        
                                        echo "</div>
                              <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />
                                             
                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                                        if (!empty($view_chats1[$key]['comment']))
                                        {
                                             $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                                             $i = 0;
                                             $flag = false;
                                             foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                                             {

                                                  /* echo '<pre>';
                                                    print_r($cval);
                                                    exit(); */
                                                  if ($i > 3)
                                                  {
                                                       $classadded = 'comment_msg_hide';
                                                  }
                                                  else
                                                  {
                                                       $classadded = '';
                                                  }

                                                  echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                                  if ($cval['Logo'] != "")
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                  }
                                                  else
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                  }

//                                        echo "</div>
//                                        <div class='comment_text'>
//                                             " . $cval['comment'] . "
//                                        </div></div>";

                                                  echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . $cval['user_name'] . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>
                                        <div class='comment_text' style='float:right;padding-left:13px;'>";
                                                  ?>
                                                  <?php timeAgo(strtotime($cval['Time'])); ?>
                                                  <?php
                                                  echo"</div>
                                        </div>";

                                                  if ($cval['image'] != "")
                                                  {
                                                       $image_comment = json_decode($cval['image']);
                                                       echo "<div class='msg_photo photos_feed'>";
                                                       echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                                       echo "<img src='" . base_url() . "/assets/user_files/thumbnail/" . $image_comment[0] . "'>";
                                                       echo "</a>";
                                                       echo "</div>";
                                                  }
                                                  if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                                  {
                                                       echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$post_id.",this)'>&nbsp;</button>";
                                                  }
                                                  echo "</div>";


                                                  if ($i > 3 && $flag == false)
                                                  {
                                                       $flag = true;
                                                       echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                                  }

                                                  $i++;
                                             }
                                        }
                                        echo "</div>
                              </div>";
                                        echo "</div>";
                                   }
                                   echo "</div></div>";
                                   ?>
                              </div>
                              <?php
                              if (count($view_chats1) > 4)
                              {
                                   ?>
                                   <div id="loadmore_1" class="load_btn">
                                        <a href="javascript: void(0)" onclick="loadmore(10, 1);" class="loadmore panel-green"> 
                                             Load More
                                        </a>
                                   </div>
                                   <?php
                              }
                              ?>
                              <?php if (!empty($social_url))
                              { ?>
                                   <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                             <div class="social-icons">
                                                  <ul class="navbar-right">
                                                       <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                                                                 Visit my Website
                                                            </a>
                                                       </li>
                                                       <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                                                                 Follow me on Twitter
                                                            </a>
                                                       </li>
                                                       <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                                                                 Follow me on Facebook
                                                            </a>
                                                       </li>
                                                       <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                                                                 Follow me on LinkedIn
                                                            </a>
                                                       </li>   
                                                  </ul>
                                             </div>
                                        </div>                                
                                   </div> 
<?php } ?>                                
                         </div>
                    </div>
               </div>
          </div>
     </div>
    <?php else:?>
    <div class="errorHandler alert alert-info center" style="display: block;margin: 0 10%;">
                <i class="fa fa-remove-sign"></i> 
                 Login or Sign Up to proceed. To sign up or login tap the Sign Up button on the top right of the screen.
             </div>
    <?php endif;?>
</div>

<script type="text/javascript">

     function loadmore(str, istype)
     {

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $subdomain; ?>/loadmore/" + (str - 5) + "/" + str + "/" + istype,
               type: "POST",
               async: true,
               success: function(result)
               {
                    if (result != "")
                    {
                         jQuery('#Message').val('');

                         if (istype == '1')
                         {
                              // jQuery("#preview_msg_public").html('');
                              jQuery("#preview_msg_public").append(result);
                         }
                         else
                         {
                              //jQuery("#preview_msg_private").html('');
                              jQuery("#preview_msg_private").append(result);
                         }
                         var a = parseInt(str) + 5;
                         var loadfun = "loadmore(" + a + "," + istype + ");";
                         jQuery("#loadmore_" + istype).html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                         jQuery("#preview").html('');
                         jQuery(".addmore_photo").css('display', 'none');

                         jQuery(".msg_edit-arrow").on('click', function()
                         {
                              var id = jQuery(this).attr('data-id');
                              jQuery("#msg_edit-view" + id).slideToggle("slow");
                         });
                    }
                    else
                    {
                         jQuery("#loadmore_" + istype).css('display', 'none');
                    }
               }

          });
     }


     function sendmessage()
     {
          jQuery("#sendbtn").html('Submitting <i class="fa fa-refresh fa-spin"></i>');
          jQuery("#sendbtn").attr('disabled','disabled');
          var str = jQuery("#imageform").serialize();
          var values = jQuery("input[name='unpublished_photo[]']");

          var flag=false;
          var flag1=false;
          
          if(jQuery("#Message").val() != "")
          {
               flag=true;
          }
          //alert(values.val());
          if(values.val() !=undefined)
          {
               console.log(values.val());
               flag1=true;
          }
          
          if ((flag==false && flag1==false))
          {
               alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               jQuery("#sendbtn").removeAttr('disabled');
               return false;
          }

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Photos/<?php echo $acc_name."/".$subdomain; ?>/chatspublic",
                              data: str,
                              type: "POST",
                              async: true,
                              success: function(result)
                              {
                                   jQuery("#loadmore_1").css('display', 'none');
                                   
                                   var loadfun = "loadmore(10,1);";
                                   jQuery("#loadmore_1").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                                   
                                   jQuery('#Message').val('');

                                   jQuery("#preview_msg_public").html('');
                                   jQuery("#preview_msg_public").html(result);

                                   jQuery("#preview").html('');
                                   jQuery(".addmore_photo").css('display', 'none');

                                   jQuery(".msg_edit-arrow").on('click', function()
                                   {
                                        var id = jQuery(this).attr('data-id');
                                        jQuery("#msg_edit-view" + id).slideToggle("slow");
                                   });
                                   
                                   jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
                                   jQuery("#sendbtn").removeAttr('disabled');
                              }

                         });
                    }
                    
function like(id,post_id)
{
    
     $.ajax({
               url  : "<?php echo base_url(); ?>Photos/<?php echo $acc_name."/".$subdomain; ?>/like",
               type : 'POST',
               data : 'user_id='+id+'&post_id='+post_id,
               success : function (data)
               {
                    var like = JSON.parse(data);
                    var tot_like=like.total_like;
                    var str=like.html;
                    var comm=$('#'+post_id+" span").html();

                    $('#'+post_id).html("<span style='float:left;padding:0px;'>"+ comm +"  </span>    &nbsp;comments  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+tot_like+' people like this');
                    
                    //alert(str);
                    $("#like_"+post_id).html(str);
                    $("#like_"+post_id).addClass("blue_like");
                    

                    
                 }

          });
}
function unlike(id,post_id)
{
    $.ajax({
               url : "<?php echo base_url(); ?>Photos/<?php echo $acc_name."/".$subdomain; ?>/dislike",
               type : 'POST',
               data :'user_id='+id+'&post_id='+post_id,
               success : function (data)
               {
                    var like = JSON.parse(data);
                    var tot_like=like.total_like;
                    var str=like.html;
                    //alert(str);
                    //$('#'+post_id).html(tot_like+' peoples like this');
                    var comm=$('#'+post_id+" span").html();

                    $('#'+post_id).html("<span style='float:left;padding:0px;'>"+ comm +"  </span> &nbsp;comments  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+tot_like+' people like this');
                    $('#like_'+post_id).html(str);
                   
               
                    $("#like_"+post_id)
                       .removeClass("blue_like");
                       /*.addClass("blue_like");*/
               
                 },

          });
}
</script>
<?php


function timeAgo($time_ago)
{
     $cur_time = time();
     $time_elapsed = $cur_time - $time_ago;

     $seconds = $time_elapsed;
     $minutes = round($time_elapsed / 60);
     $hours = round($time_elapsed / 3600);
     $days = round($time_elapsed / 86400);
     $weeks = round($time_elapsed / 604800);
     $months = round($time_elapsed / 2600640);
     $years = round($time_elapsed / 31207680);
     // Seconds
     if ($seconds <= 60)
     {
          echo "$seconds seconds ago";
     }
     //Minutes
     else if ($minutes <= 60)
     {
          if ($minutes == 1)
          {
               echo "one minute ago";
          }
          else
          {
               echo "$minutes minutes ago";
          }
     }
     //Hours
     else if ($hours <= 24)
     {
          if ($hours == 1)
          {
               echo "an hour ago";
          }
          else
          {
               echo "$hours hours ago";
          }
     }
     //Days
     else if ($days <= 7)
     {
          if ($days == 1)
          {
               echo "yesterday";
          }
          else
          {
               echo "$days days ago";
          }
     }
     //Weeks
     else if ($weeks <= 4.3)
     {
          if ($weeks == 1)
          {
               echo "a week ago";
          }
          else
          {
               echo "$weeks weeks ago";
          }
     }
     //Months
     else if ($months <= 12)
     {
          if ($months == 1)
          {
               echo "a month ago";
          }
          else
          {
               echo "$months months ago";
          }
     }
     //Years
     else
     {
          if ($years == 1)
          {
               echo "one year ago";
          }
          else
          {
               echo "$years years ago";
          }
     }
}
?>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
<script type="text/javascript">
     jQuery.noConflict();


     jQuery(document).ready(function() {

          jQuery(document).on('mouseover', '.msg_photo', function() {
               var my_class = $(this).find('a').attr('class');
               $("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
          });
          
          jQuery(document).on('mouseover', '.msg_fromname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'), maxHeight: '95%',width:"50%"});
          });
          
          jQuery(document).on('mouseover', '.msg_toname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'),maxHeight: '95%',width:"50%"});
          });

          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("#photoimg").removeAttr('multiple');
          }
          else
          {
               jQuery("#photoimg").attr('multiple');
          }

          jQuery(".msg_edit-arrow").on('click', function() {
               var id = jQuery(this).attr('data-id');
               jQuery("#msg_edit-view" + id).slideToggle("slow");
          });
     });

     jQuery(document).ready(function() {
          console.log('onReady');
          jQuery("#takePictureField").on("change", gotPic);
          jQuery("#preview").load(getSwatches);
          desiredWidth = window.innerWidth;

          if (!("url" in window) && ("webkitURL" in window)) {
               window.URL = window.webkitURL;
          }

     });
