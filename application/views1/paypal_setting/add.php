<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Paypal setting</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <?php if ($this->session->flashdata('paypal_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Paypal Settings <?php echo $this->session->flashdata('paypal_data'); ?> Successfully.
                    </div>
                <?php } ?>

            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                <!--<link rel="stylesheet" type="text/css" media="screen" href="https://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">-->
                   
                   

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            API Username <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="username" name="username" class="form-control name_group required" value="<?php echo $paypal_setting[0]['username'];?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            API Password <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="password" name="password" class="form-control name_group required" value="<?php echo $paypal_setting[0]['password'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            API Signature <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text"  id="signature" name="signature" class="form-control name_group required" value="<?php echo $paypal_setting[0]['signature'];?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-3">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<div id="full_popup">
    <div id="blanket" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a>
        <form role="form" method="post" class="ajax-form" id="form2">
        <div class="row">
            <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
            <div class="errorHandler alert alert-success no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> Updated Successfully.
            </div>
            <?php } ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">First Name </span>
                    </label>
                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name </span>
                    </label>
                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
                </div>
                <div class="form-group">
                    <label class="control-label">Email </span>
                    </label>
                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
                </div>
                <div class="form-group">
                        <label class="control-label">Password </label>
                        <input type="password" placeholder="Password" class="form-control required" id="password" name="password">
                </div>
                <div class="col-md-3">
                        <button id="comment" class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>                     
        </div>
        </form>
    </div> 
</div>

<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
.close_popup
{
    position: absolute;
    top: -10px;
    right: -10px;
}
#popUpDiv {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}
#popUpDiv .form-group label{color:#333;}
#popUpDiv .col-md-3{margin: 0; padding: 0;}
</style>

<script type="text/javascript">
$('#comment').click(function() {
    var form_data = {
        Firstname : $('#Firstname').val(),
        Lastname : $('#Lastname').val(),
        Email : $('#email').val(),
        Password : $('#password').val()
    };

    if($('#Firstname').val() != '' && $('#Lastname').val() && $('#email').val() != '' && $('#password').val() != '')
    {
        $.ajax({
            url: "<?php echo base_url().'Speaker/add_speakers/'.$this->uri->segment(3); ?>",
            type: 'POST',
            data: form_data,
            success: function(data){
                    $('#full_popup').css('display','none');
                    $('.old_speakers').html(data); 
                    $('#Firstname').val('');
                    $('#Lastname').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('.old_speakers').on('change',function(){
                        if($('.old_speakers').val()=='New')
                {
                    $("#full_popup").show();
                    popup('popUpDiv');
                    $("#blanket").show();
                    $("#popUpDiv").show();
                }else{
                    $("#full_popup").hide();
                }
                    });
               }
        });
    }
});

function openpop()
{
    if($(".old_speakers").val()=='New')
    {
    
       popup('popUpDiv');
    }
}
</script>

<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $ti = $("#Start_date").val();
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } ?>
<!-- end: PAGE CONTENT-->