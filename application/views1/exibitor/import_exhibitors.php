<!-- start: PAGE CONTENT -->   
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add Multiple <span class="text-bold">Exhibitors</span></h4>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('csv_flash_message')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('csv_flash_message'); ?> 
                </div>
                <?php } ?>
                <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Exhibitor/save_import_exhibitors/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
                    <h4>Add CSV or Json URL to import Exhibitors data</h4>
                    <!-- <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            <a target="_blank" href="<?php echo base_url(); ?>event/access_setting/<?php echo $this->uri->segment(3); ?>">Your Access Key </a><span class="symbol required"></span>
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Your Access Key" class="form-control required" id="access_key" name="access_key">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2">
                            File Type
                        </label>
                        <div class="col-sm-4">
                            <select class="form-control" id="file-type" name="file-type">
                                <option>CSV</option>
                                <option>JSON</option>
                            </select>
                        </div>
                    </div>
                    <div class="csv_sec">
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1">
                                Short instructions
                            </label>
                            <div class="col-sm-9">
                                <div class="col-sm-4" style="padding:0px;">
                                    <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Exhibitor/download_exhi_template_csv/<?php echo $this->uri->segment(3); ?>">Download CSV</a>
                                    <br/><br/><small>Download Example CSV of multiple insert Exhibitors</small>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1">
                                Your CSV File URL <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-9">
                                <div class="col-sm-8" style="padding:0px;">
                                    <input type="text" placeholder="Your CSV File URL" class="form-control required" id="csv_url" name="csv_url">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="json_sec" style="display: none;">
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1">
                                Short instructions
                            </label>
                            <div class="col-sm-9">
                                <div class="col-sm-4" style="padding:0px;">
                                    <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Exhibitor/download_import_exhi_json/<?php echo $this->uri->segment(3); ?>">Download JSON</a>
                                    <br/><br/><small>Download Example JSON of multiple insert Exhibitors</small>
                                </div>
                                <div class="col-sm-4" style="padding:0px;">
                                    <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" target="_blank" href="<?php echo base_url(); ?>Exhibitor/show_import_exhi_json/<?php echo $this->uri->segment(3); ?>">JSON Schema</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1">
                                Your JSON File URL <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-9">
                                <div class="col-sm-8" style="padding:0px;">
                                    <input type="text" placeholder="Your JSON File URL" class="form-control" id="json_url" name="json_url">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-3">
                            <button class="btn btn-theme_green btn-block" type="submit">
                                Add
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<script type="text/javascript">
    $('#file-type').change(function()
    {
        if($('#file-type').val() == 'CSV')
        {
            $('.csv_sec').show();            
            $('.json_sec').hide();
            $('#json_url').removeClass('required');
            $('#csv_url').addClass('required')            

        }
        else
        {
            $('.csv_sec').hide();            
            $('.json_sec').show();
            $('#csv_url').removeClass('required');            
            $('#json_url').addClass('required');
        }
    });
</script>