<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />    
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css"> 
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Edit <span class="text-bold">Parent Categorie</span></h4>
        <div class="panel-tools">
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="categorie_name">
              Categorie Name <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <input type="text" placeholder="Categorie Name" class="form-control required" id="categorie_name" name="categorie_name" value="<?php echo $categorie_data[0]['category']; ?>">
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Exhibitor Categories <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <select style="height:auto;" multiple="multiple" class="select2-container select2-container-multi form-control search-select menu-section-select" name="category_ids[]">
                <?php foreach ($exibitor_category as $key => $value) { ?>
                <option value="<?=$value['id'];?>" <?php if(in_array($value['id'],$categorie_data[0]['child_category'])){ ?> selected="selected" <?php } ?>><?=$value['category'];?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2" for="form-field-1">
              Category Icon 
            </label>
            <div class="modal fade" id="categorieiconcropetool" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Category Icon</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row"  id="cropping_banner_div">
                      <div>
                        <img class="img-responsive" id="show_crop_icons_model" src="" alt="Picture">
                      </div>
                      <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-green btn-block" id="upload_result_btn_icon_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-9">
              <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                <div class="fileupload-preview fileupload-exists thumbnail" id="categories_icons_preview_images">
                </div>
                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                  <input type="file" id="categories_icons" name="categories_icons">
                </span>
                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_categorie_icons_data"><i class="fa fa-times"></i> Remove</a>
              </div>
              <input type="hidden" name="categorie_icons_crop_data" id="categorie_icons_crop_data">
              <?php if(!empty($categorie_data[0]['categorie_icon'])){ ?>
              <div class="col-sm-3 fileupload-new thumbnail center">
                <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/exhibtior_category_icon/<?php echo $categorie_data[0]['categorie_icon']; ?>"><a class="btn btn-red remove_image" onclick="remove_icon(<?php echo $categorie_data[0]['id']; ?>);" href="javascript:void(0);" style=""><i class="fa fa-times fa fa-white"></i></a>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Submit <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
function remove_icon(id)
{
  if(confirm("Are You Sure Delete This??"))
  {
    window.location.href="<?php echo base_url().'Exhibitor/delete_categorie_images/'.$this->uri->segment(3).'/' ?>"+id;
  }
}
var $bannerimage = $("#show_crop_icons_model");
var $inputBannerImage = $('#categories_icons'); 
$(document).ready(function() {
  $('#Short_desc').tagsInput({
    maxTags: 6,
    allowSpaces: false,
    removeWithBackspace : true
  });
  $inputBannerImage.change(function(){
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
    var files = this.files;
    var file;
    if (files && files.length) {
      file = files[0];
      if (/^image\/\w+$/.test(file.type)) {
        if (uploadedImageURL) {
          URL.revokeObjectURL(uploadedImageURL);
        }
        uploadedImageURL = URL.createObjectURL(file);
        $bannerimage.attr('src', uploadedImageURL);
        $('#categorie_icons_crop_data').val('');
        $('#categorieiconcropetool').modal('toggle');
      } else {
        window.alert('Please choose an image file.');
      }
    }
  });
  $(document).on('shown.bs.modal','#categorieiconcropetool' ,function () {
    $bannerimage.cropper('destroy');
    var croppable = false;
    var $button = $('#upload_result_btn_icon_crop');
    $bannerimage.cropper({ 
      aspectRatio: 1, 
      built: function () {
        croppable = true;
      }
    });
    $button.on('click', function () {
      var croppedCanvas;
      if (!croppable) {
        return;
      }
      croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
      $('#categories_icons_preview_images').html("<img src='"+croppedCanvas.toDataURL()+"'>");
      $('#categorie_icons_crop_data').val(croppedCanvas.toDataURL());
      $("#categories_icons").val('');
    });
  }).on('hidden.bs.modal',function(){
    $bannerimage.cropper('destroy');
  }); 
});
</script>