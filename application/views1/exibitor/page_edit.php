<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Short_desc').tagsInput({
            maxTags: 6,
            allowSpaces: false,
            removeWithBackspace : true
        });
    });
</script>
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />  
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">  
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Exhibitor</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <?php $select_user_id=$exibitor_by_id[0]['user_id'];?>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Company Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Exhibitor Heading" id="Heading" value="<?php echo $exibitor_by_id[0]['Heading']; ?>" name="Heading" class="form-control name_group required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Exhibitor Type
                        </label>
                        <div class="col-sm-9">
                            <select name="exhibitor_type" id="sponsors_type" class="form-control">
                                <option value="">Select Exhibitor Type</option>
                                <?php foreach ($exhibitor_type as $key => $value) { ?>
                                    <option value="<?php echo $value['type_id']; ?>" <?php if($exibitor_by_id[0]['et_id']==$value['type_id']){ ?> selected="selected" <?php } ?>><?php echo $value['type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Exhibitor Group
                        </label>
                        <div class="col-sm-9">
                            <select name="exhibitor_group" id="exhibitor_group" class="form-control">
                                <option value="">Select Exhibitor Group</option>
                                <?php foreach ($exhibitor_group as $key => $value) { ?>
                                    <option value="<?php echo $value['group_id']; ?>" <?php if($exibitor_by_id[0]['eg_id']==$value['group_id']){ ?> selected="selected" <?php } ?>><?php echo $value['group_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Sponsored Category
                        </label>
                        <div class="col-sm-9">
                            <select name="sponsored_category[]" multiple="multiple" id="sponsored_category" class="select2-container select2-container-multi form-control search-select menu-section-select">
                                <?php foreach ($exhibitor_category as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],explode(',',$exibitor_by_id[0]['sponsored_category']))){ ?> selected="selected" <?php } ?>><?php echo $value['category']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Main Contact Name <!-- <span class="symbol required"></span> -->
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder=" " id="main_contact_name" name="main_contact_name" class="form-control name_group" value="<?php echo $exibitor_by_id[0]['main_contact_name']; ?>">
                        </div>
                    </div>       

                    <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                             Key Words 
                        </label>
                        <div class="col-sm-9" id="keyword">
                            <textarea id="Short_desc" class="form-control tags" name="Short_desc"><?php echo $exibitor_by_id[0]['Short_desc'] ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                             Description
                        </label>
                        <div class="col-sm-9">
                          <!--  <textarea maxlength="3000" id="Description" style="height:150px;" placeholder="Type your 
                          Description here" class="form-control limited" name="Description"><?php echo $exibitor_by_id[0]['Description'] ?></textarea> -->
                           <textarea id="Description" style="height:150px;" class="form-control summernote" name="Description"><?php echo $exibitor_by_id[0]['Description'] ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-select-4">
                            Link Attendees as Representatives
                        </label>
                        <div class="col-md-9">
                            <?php $auids=explode(",",$exibitor_by_id[0]['link_user']); ?>
                            <select id="attendee_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="attendee_id[]" multiple="true">
                                <?php 
                                    foreach ($attendee_user as $key => $value) 
                                    { ?>
                                        <option value="<?php echo $value['Id']; ?>" <?php if(in_array($value['Id'],$auids)){ ?> selected="selected" <?php } ?>><?php echo ucfirst($value["Firstname"]).' '.$value["Lastname"]; ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Website URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Website Url (e.g: https://www.yoursite.com)" id="website_url" name="website_url" class="form-control" value="<?php echo $exibitor_by_id[0]['website_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Facebook URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Facebook Url (e.g: https://www.yoursite.com)" id="facebook_url" name="facebook_url" class="form-control" value="<?php echo $exibitor_by_id[0]['facebook_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Twitter URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Twitter Url (e.g: https://www.yoursite.com)" id="twitter_url" name="twitter_url" class="form-control" value="<?php echo $exibitor_by_id[0]['twitter_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>LinkedIn URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="LinkedIn Url (e.g: https://www.yoursite.com)" id="linkedin_url" name="linkedin_url" class="form-control" value="<?php echo $exibitor_by_id[0]['linkedin_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Youtube URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Youtube Url (e.g: https://www.youtube.com)" id="youtube_url" name="youtube_url" class="form-control" value="<?php echo $exibitor_by_id[0]['youtube_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Instagram URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Instagram Url (e.g: https://www.instagram.com)" id="instagram_url" name="instagram_url" class="form-control" value="<?php echo $exibitor_by_id[0]['instagram_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Main Contact Email Address</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="" id="main_email_address" name="main_email_address" class="form-control" value="<?php echo $exibitor_by_id[0]['main_email_address'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Email Address</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Email Address" id="email_address" name="email_address" class="form-control" value="<?php echo $exibitor_by_id[0]['email_address'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Stand Number</em> <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="" id="stand_number" name="stand_number" class="form-control" value="<?php echo $exibitor_by_id[0]['stand_number'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Phone Number1</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Phone Number1" id="phone_number1" name="phone_number1" class="form-control" value="<?php echo $exibitor_by_id[0]['phone_number1'] ?>">
                        </div>
                    </div>

                      <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>HQ Phone Number</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="" id="HQ_phone_number" name="HQ_phone_number" class="form-control" value="<?php echo $exibitor_by_id[0]['HQ_phone_number'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Select Location 
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="Address_map">
                                            <?php 
                                                echo"<option value=''>Select Location</option>";
                                                foreach ($map_list as $key => $value) 
                                                { ?>
                                                <option value="<?php echo $value['Id']; ?>" <?php if($mapdata[0]['Id']==$value['Id']) { ?> selected="selected" <?php } ?>><?php echo $value["Map_title"]; ?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                 
                                <div class="form-group">
                                  <label class="col-sm-2" for="form-field-select-1">Country <span class="symbol required"></span></label>
                                   <div class="col-md-9">
                                  <select id="form-field-select-1" class="form-control" name="country_id">
                                      <?php foreach ($event_countries_dropdown as $key => $value) {  ?>
                                            <option <?php echo ($exibitor_by_id[0]['country_id'] == $value['id']) ? 'selected' : '' ?> value="<?php echo $value['id']; ?>"><?php echo $value['country_name']; ?></option>
                                      <?php } ?>
                                  </select>  
                                  </div>                                                              
                                </div>
                                <div class="form-group" id="areadiv">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Area
                                    </label>
                                  <div class="col-sm-9">
                                     <input id="area" name="area" value="<?php echo $mapdata[0]['area']; ?>" class="form-control" type="text" placeholder="Area"/>
                                  </div>
                               </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Company Logo</label>
                        <div class="modal fade" id="companylogomodelscopetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Company Logo</h4>
                                </div>
                                <div class="modal-body">
                                    <section>
                                        <div class="demo-wrap" style="display:none;">
                                            <div class="container">
                                                <div class="grid">
                                                    <div class="col-1-2">
                                                        <div id="vanilla-demo"></div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <strong>Vanilla Example</strong>
                                                        <div class="actions">
                                                            <button class="vanilla-result">Result</button>
                                                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="preview_company_logo_crop_tool"></div>
                                            <div class="col-sm-3 crop-btn">
                                                <button class="btn btn-green btn-block" id="company_logo_upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>         
                                    </section>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($exibitor_by_id[0]['company_logo']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                            <div class="fileupload-preview fileupload-exists thumbnail" id="company_logo_preview_div">
                            </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                    <input type="file" id="company_logo" name="company_logo">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_logo_data">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                            <input type="hidden" name="company_logo_crop_data_textbox" id="company_logo_crop_data_textbox">
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo filter_var($images_array[$i], FILTER_VALIDATE_URL) ? $images_array[$i] : base_url().'assets/user_files/'.$images_array[$i]; ?>">
                                    <input type="hidden" name="old_company_logo" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>                                                                                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Banner Images</label>
                        <div class="modal fade" id="headermodelscopetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Banner Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row"  id="cropping_banner_div">
                                            <div>
                                                <img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 imgbannerup">
                            <?php $images_array = json_decode($exibitor_by_id[0]['Images']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                            <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" id="header_image_sponder" name="banner_Images">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                            <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center ">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>"><a class="btn btn-red remove_image" href="javascript:;" style=""><i class="fa fa-times fa fa-white"></i></a>
                                    <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>                                                                                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<div id="messagess_show_models" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Messages</h4>
            </div>
            <div class="modal-body">
                <h3> Your Content Has Been <?php echo $this->session->flashdata('exibitor_data'); ?> Successfully. </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image_sponder'); 
$(document).ready(function(){
    $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              uploadedImageURL = URL.createObjectURL(file);
              $bannerimage.attr('src', uploadedImageURL);
              $('#company_banner_crop_data_textbox').val('');
              $('#headermodelscopetool').modal('toggle');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
            $('#banner_preview_image_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
            $('#company_banner_crop_data_textbox').val(croppedCanvas.toDataURL());
            $("#header_image_sponder").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    }); 
});
</script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js"></script>
<script>
    Demo.init();
</script>
<?php if($this->session->flashdata('exibitor_data')){ ?>
<script type="text/javascript">
$(window).load(function(){
    $('#messagess_show_models').modal('show');
});
</script>
<?php } ?>
<script type="text/javascript">
$('#remove_company_logo_data').click(function(){
    $('#company_logo_crop_data_textbox').val('');
});
$('#remove_company_banner_data').click(function(){
    $('#company_banner_crop_data_textbox').val('');
});
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>
<!-- end: PAGE CONTENT-->