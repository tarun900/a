<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Users</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
					<div class="row">
                        <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Updated Successfully.
                        </div>
                        <?php } ?>
                        <div class="col-md-12">
                            <div class="col-md-3" style="margin-bottom:20px;">
                                <label>Select Existing Users</label>
                            </div>
                            <div class="col-md-8" style="margin-bottom:20px;">
                                <div class="col-md-12">
                                    <select class="col-md-6" id="main_select" name="User_id">
                                        <option>Select User</option>
                                        <?php foreach ($user_list as $key => $value) { ?>
                                            <option value="<?php echo $value['uid']; ?>"><?php echo $value['Email']; ?></option>
                                        <?php }  ?>
                                    </select>
                                    <input type="hidden" name="Event_id" value="<?php echo $this->uri->segment(3); ?>">
                                </div>
                            </div>
                            <div id="details">
                              
                            </div>

    						<div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                            <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
    						</div>		
                        </div>					
					</div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<style type="text/css">
    .form-horizontal .form-group
    {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
</style>
<!-- end: PAGE CONTENT-->


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

<script type="text/javascript">  

    $('#main_select').change(function()
    {
        if($(this).val()!='')
        {
          $.ajax({
              url: "<?php echo base_url(); ?>profile/get_user_detail",
              type: "POST",
              data: {id: $(this).val(),eventid: <?php echo $Event_id; ?>,flag:1},
              success: function(data)
              {
                  $("#details").html(data);
              }
          });
        }
    });
</script>



<script type="text/javascript">  
    

        $("#country").change(function get_state(){
          
            $.ajax({
            url:"<?php echo base_url(); ?>profile/getnewstate",    
            data: {id: $(this).val()},
            type: "POST",
            success: function(data)
            {
                
                $("#state").html(data);
            }
            
            });
       
        });

            
</script>