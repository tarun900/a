<?php $user=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
        	<div class="panel-heading">
                <h4 class="panel-title">Edit Authorized <span class="text-bold">Emails</span></h4>
                <?php if($this->session->flashdata('authorized_error_data')) { ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('authorized_error_data'); ?>
                    </div>
                <?php } ?>
           </div>
           <div class="panel-body">
                <form action="" method="POST"  role="form" id="form" novalidate="novalidate">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label required">FirstName<span class="symbol required"></span></label>
                            <input type="text" placeholder="FirstName" class="form-control required" id="firstname" name="firstname" value="<?php echo $auth_user_data[0]['firstname']; ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">LastName<span class="symbol required"></span></label>
                            <input type="text" placeholder="LastName" class="form-control required" id="lastname" name="lastname" value="<?php echo $auth_user_data[0]['lastname']; ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Email<span class="symbol required"></span></label>
                            <input type="email" placeholder="Email" class="form-control required" id="Email" name="Email" value="<?php echo $auth_user_data[0]['Email']; ?>" onblur="check_exists_authuseremail();">
                            <span for="Email" class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Title</label>
                            <input type="text" placeholder="Title" class="form-control" id="title" name="title" value="<?php echo $auth_user_data[0]['title']; ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Company Name</label>
                            <input type="text" placeholder="Company Name" class="form-control" id="company_name" name="company_name" value="<?php echo $auth_user_data[0]['company_name']; ?>">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
function check_exists_authuseremail() {
    $.ajax({
        url:"<?php echo base_url().'Exhibitor/check_exists_authuseremail/'.$event_id.'/'.$auth_user_data[0]['authorized_id']; ?>",
        type:'post',
        data:"Email="+$.trim($('#Email').val()),
        success:function(result)
        {
            var values=result.split('###');
            if(values[0]=="error")
            {   
                $('#Email').parent().removeClass('has-success').addClass('has-error');
                $('#Email').parent().find('.control-label span').removeClass('ok').addClass('required');
                $('#Email').parent().find('.help-block').removeClass('valid').html(values[1]);
            }
            else
            {
                $('#Email').parent().removeClass('has-error').addClass('has-success');
                $('#Email').parent().find('.control-label span').removeClass('required').addClass('ok');
                $('#Email').parent().find('.help-block').addClass('valid').html(''); 
            }
        }
    });
}    
</script>