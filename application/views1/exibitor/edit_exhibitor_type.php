<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Edit <span class="text-bold">Exhibitor Type</span></h4>
        <div class="panel-tools">
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Exhibitor Type Name <span class="symbol required"></span>
            </label>
            <div class="col-sm-6">
              <input type="text" placeholder="Exhibitor Type Name" class="form-control required" value="<?php echo $type_data[0]['type_name']; ?>" id="Firstname" name="exhibitor_type_name">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Position in Directory <span class="symbol required"></span>
            </label>
            <div class="col-sm-6">
              <select name="position_in_directory" class="form-control required">
                <option value="">Select Position</option>
                <option value="1" <?php if($type_data[0]['type_position']==1){ ?>  selected="selected" <?php } ?>>1st</option>
                <option value="2" <?php if($type_data[0]['type_position']==2){ ?>  selected="selected" <?php } ?>>2nd</option>
                <option value="3" <?php if($type_data[0]['type_position']==3){ ?>  selected="selected" <?php } ?>>3rd</option>
                <option value="4" <?php if($type_data[0]['type_position']==4){ ?>  selected="selected" <?php } ?>>4th</option>
                <option value="5" <?php if($type_data[0]['type_position']==5){ ?>  selected="selected" <?php } ?>>5th</option>
                <option value="6" <?php if($type_data[0]['type_position']==6){ ?>  selected="selected" <?php } ?>>6th</option>
                <option value="7" <?php if($type_data[0]['type_position']==7){ ?>  selected="selected" <?php } ?>>7th</option>
                <option value="8" <?php if($type_data[0]['type_position']==8){ ?>  selected="selected" <?php } ?>>8th</option>
                <option value="9" <?php if($type_data[0]['type_position']==9){ ?>  selected="selected" <?php } ?>>9th</option>
                <option value="10" <?php if($type_data[0]['type_position']==10){ ?>  selected="selected" <?php } ?>>10th</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Color <span class="symbol required"></span>
            </label>
            <div class="col-sm-6 color-pick">
                <input type="text" class="color {hash:true} form-control required" value="<?php echo $type_data[0]['type_color']; ?>" name="custom_color_picker">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Update <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>