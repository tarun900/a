<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
$agendacategory=$menu_list['category_list'];
unset($menu_list['category_list']);
if(count($event_templates[0]['lefthandmenu_images_list']) > 0)
{
  foreach ($event_templates[0]['lefthandmenu_images_list'] as $key => $value) { ?>
  <div class="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
      <img width="100%" src="<?php echo base_url().'assets/user_files/'.$value['Image']; ?>" class="lefthandmenumap" usemap="#lefthandmenumape_<?php echo $value['id']; ?>">
      <map id="lefthandmenumape_<?php echo $value['id']; ?>" name="lefthandmenumape_<?php echo $value['id']; ?>">
        <?php foreach ($value['coords'] as $ckey => $cvalue) {
          $area_url=base_url()."App/".$acc_name."/".$Subdomain;
          $area_title="";

          if(!empty($cvalue['menuid']))
          {
            if(!in_array($cvalue['menuid'],array(22,23,24,25)))
            {
              if($cvalue['menuid']==12)
              {
                $area_url = base_url() .'Messages/'. $acc_name."/".$Subdomain.'/privatemsg';
                $area_title="Private Messages";
              }
              else if($cvalue['menuid']==13)
              {
                $area_url = base_url() .'Messages/'.$acc_name."/". $Subdomain.'/publicmsg';
                $area_title="Public Messages";
              }
              else if($cvalue['menuid']==42)
              {
                $area_url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/fundraising_donation';
                $area_title="Fundraising Donation";
              }
              else if($cvalue['menuid']==19)
              {
                $area_url = base_url().'App/'.$acc_name."/".$Subdomain;
                $area_title="Home";
              }
              else
              {
                $area_url = base_url() . ucfirst($cvalue['pagetitle']) . '/' . '' . $acc_name."/".$Subdomain;
                $area_title=$cvalue['menuname'];
              }
            }
            else
            {
              $area_url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/get_auction_pr';
              $var_target="";
              if($cvalue['menuid']==22)
              {
                $var_target='target="_blank"';
                $area_url=$area_url.'/1';
                $area_title="Silent Auction";
              }
              else if($cvalue['menuid']==23)
              {
                $var_target='target="_blank"';
                $area_url=$area_url.'/2';
                $area_title="Live Auction";
              }                                        
              else if($cvalue['menuid']==24)
              {
                $var_target='target="_blank"';
                $area_url=$area_url.'/3';
                $area_title="Buy Now";
              }
              else if($cvalue['menuid']==25)
              {
                $var_target='target="_blank"';
                $area_url=$area_url.'/4';
                $area_title="Pledge";
              }
            }
          }
          else if(!empty($cvalue['cmsid']))
          {
            $area_url=base_url().'Cms/'.$acc_name.'/'.$Subdomain.'/View/'.$cvalue['cmsid'];
            $area_title="Custom Modules";
          }
          else if(!empty($cvalue['agenda_id']))
          {
            $area_url=base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/view_category_session/'.$cvalue['agenda_id'];
            $area_title="Agenda Caegory";
          }
          else
          {
            $area_url=$cvalue['redirect_url'];
            $area_title="Other Url";
          }
          echo '<area title="'.$area_title.'" alt="'.$area_title.'" shape="rect" href="'.$area_url.'" coords="'.$cvalue['coords'].'">';
        } ?>
      </map>
    </div>
  </div>
  <?php }
}
else
{

if($event_templates[0]['show_attendee_menu']!='1')
{
  $key = array_search(2, array_column($menu_list, 'id'));
  if($key!="")
  {
    unset($menu_list[$key]);  
  }
}
if($event_templates[0]['Subdomain']=="youmatterday2016"){
  $ikey=8;
  $sort_key_val=array('1'=>"16",'2'=>"7",'3'=>"1",'4'=>"10",'5'=>"44",'6'=>"15",'7'=>"6");
  foreach ($menu_list as $key => $value) {
    $kkey=array_search($value['id'], $sort_key_val);
    if(!empty($kkey)){
      $menu_list[$key]['sort_key']=$kkey;
    }
    else
    {
      $menu_list[$key]['sort_key']=$ikey;
      $ikey++; 
    }
  }
  function aasort(&$array, $key) {
      $sorter=array();
      $ret=array();
      reset($array);
      foreach ($array as $ii => $va) {
          $sorter[$ii]=$va[$key];
      }
      asort($sorter);
      foreach ($sorter as $ii => $va) {
          $ret[$ii]=$array[$ii];
      }
      $array=$ret;
  }
  aasort($menu_list,'sort_key');
}
if($event_templates[0]['Subdomain']=="aeinstitute17")
{
  $ikey=11;
  $sort_key_val=array('1'=>"1",'2'=>"7",'3'=>"10",'4'=>"16",'5'=>"20",'6'=>"43",'7'=>"2",'8'=>"6",'9'=>"13",'10'=>"15",'11'=>"44",'12'=>"17");
  foreach ($menu_list as $key => $value) {
    $kkey=array_search($value['id'], $sort_key_val);
    if(!empty($kkey)){
      $menu_list[$key]['sort_key']=$kkey;
    }
    else
    {
      $menu_list[$key]['sort_key']=$ikey;
      $ikey++; 
    }
  }
  function aasort(&$array, $key) {
      $sorter=array();
      $ret=array();
      reset($array);
      foreach ($array as $ii => $va) {
          $sorter[$ii]=$va[$key];
      }
      asort($sorter);
      foreach ($sorter as $ii => $va) {
          $ret[$ii]=$array[$ii];
      }
      $array=$ret;
  }
  aasort($menu_list,'sort_key');
}
if($event_templates[0]['Subdomain']=="thegrayreveal"){
$ikey=10;
//$sort_key_val=array('1'=>"153",'2'=>"166",'3'=>"154",'4'=>"155",'5'=>"156",'6'=>"157",'7'=>"158",'8'=>"159",'9'=>"160");
$sort_key_val=array('1'=>"153",'2'=>"166",'3'=>"154",'4'=>"155",'5'=>"169",'6'=>"157",'7'=>"158",'8'=>"159",'9'=>"170",'10'=>"160");
$sort_key_val_eve=array('1'=>"20",'2'=>"24");
foreach ($cms_menu as $key => $value) {
  $kkey=array_search($value['Id'], $sort_key_val);
  if(!empty($kkey)){
    $cms_menu[$key]['sort_key']=$kkey;
  }
  else
  {
    $cms_menu[$key]['sort_key']=$ikey;
    $ikey++; 
  }
}
$iikey=3;
foreach ($menu_list as $key => $value) {
  $kkey=array_search($value['id'], $sort_key_val_eve);
  if(!empty($kkey)){
    $menu_list[$key]['sort_key']=$kkey;
  }
  else
  {
    $menu_list[$key]['sort_key']=$iikey;
    $iikey++; 
  }
}
function aasort(&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}
aasort($cms_menu,'sort_key');
aasort($menu_list,'sort_key');
}
$acc_name=$this->session->userdata('acc_name');?>
<div class="navbar-content">
  <div class="main-navigation left-wrapper transition-left">
	<div class="top_head_wrap">
    <?php if (!empty($cms_menu_details)) { ?>
      <a class="navbar-brand" href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>">
      <?php $logo_image_array = json_decode($cms_menu_details[0]['Logo_images']); ?>
      <?php if (!empty($logo_image_array)) { ?>
      <?php if ($cms_menu_details[0]['Img_view'] == '0') { ?>
      <?php
      $base_url = base_url();
      $img_url = $base_url."assets/user_files/".$logo_image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

      if($originalWidth > '258')
      {
                                            
        $first = $originalHeight * 258;
        $height = $first / $originalWidth;
        //echo "Wel-Come";
        echo'<img width="258" height="'.$height.'" alt="Logo" src="'.$img_url.'">';
      }
                                        
      elseif ($originalWidth < '258') 
      {  
        echo'<img width="258px" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">';
      }
      elseif($originalWidth == $originalHeight)
      {

       if($originalWidth > '258' || $originalHeight > '258') { ?>
       <?php echo'<img width="258px" height="258px" alt="Logo" src="'.$img_url.'">'; ?>
       <?php } else { ?>
       <?php echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">'; ?>
       <?php }
     }
     else
     {
           echo'<img alt="Logo" src="'.$img_url.'">';
     }
    }
    elseif ($cms_menu_details[0]['Img_view'] == '1') 
    {

        $base_url = base_url();

        $img_url = "$base_url/assets/user_files/$logo_image_array[0]";
        $size = getimagesize($img_url);

        $originalWidth = $size[0];
        $originalHeight = $size[1];

        if($originalWidth > '258')
        {
            
          $first = $originalHeight * 258;
          $height = $first / $originalWidth;
          echo'<img style="border-radius:50px;" width="258" height="'.$height.'" alt="Logo" src="'.$img_url.'">';
        }
                                        
        elseif ($originalWidth < '258') 
        {

          echo'<img style="border-radius:50px;" width="258px" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">';   
        }
        elseif($originalWidth == $originalHeight)
        {

         if($originalWidth > '258' || $originalHeight > '258') { ?>
         <?php echo'<img style="border-radius:50px;" width="258px" height="258px" alt="Logo" src="'.$img_url.'">'; ?>
         <?php } else { ?>
         <?php echo'<img style="border-radius:50px;" width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">'; ?>
         <?php }
        }
        else
        {
             echo'<img alt="Logo" src="'.$img_url.'">';
        }
        }
        } 
        ?>
      </a>
      <?php } else { for ($i = 0; $i < count($event_templates); $i++) { ?>
      <a class="navbar-brand" href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event_templates[$i]['Subdomain']; ?>">
      <?php $logo_image_array =$event_templates[$i]['Logo_images']; ?>
      <?php if (!empty($logo_image_array))
      { 
        if ($event_templates[$i]['Img_view'] != '1') 
        {
                                  
          $base_url = base_url();
          $img_url = $base_url."assets/user_files/".$logo_image_array;
          $size = getimagesize($img_url);

          $originalWidth = $size[0];
          $originalHeight = $size[1];

         if($originalWidth > '258')
         {
              
            $first = $originalHeight * 258;
            $height = $first / $originalWidth;

            echo'<img width="258" height="'.$height.'" alt="Logo" src="'.$img_url.'">';

         }
                                 
         elseif ($originalWidth < '258') 
         {

            echo'<img width="258px" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">'; 
         }
         elseif($originalWidth == $originalHeight)
         {

          if($originalWidth > '258' || $originalHeight > '258') { ?>
           <?php echo'<img width="258px" height="258px" alt="Logo" src="'.$img_url.'">'; ?>
           <?php } else { ?>
           <?php echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">'; ?>
           <?php }
         }
         else
         {
               echo'<img alt="Logo" src="'.$img_url.'">';
         }                          
        }
        if ($event_templates[$i]['Img_view'] == '1')
        {
                                        
          $base_url = base_url();
         $img_url = $base_url."assets/user_files/".$logo_image_array;
          $size = getimagesize($img_url);

          $originalWidth = $size[0];
          $originalHeight = $size[1];
          if($originalWidth > '258')
          {                          
            $first = $originalHeight * 258;
            $height = $first / $originalWidth;
            echo'<img style="border-radius:50px;" width="258" height="'.$height.'" alt="Logo" src="'.$img_url.'">';
          }
                                        
         elseif ($originalWidth < '258') 
         {
            echo'<img style="border-radius:50px;" width="258" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">';   
         }
         elseif($originalWidth == $originalHeight)
         {

           if($originalWidth > '258' || $originalHeight > '258') { ?>
             <?php echo'<img style="border-radius:50px;" width="258px" height="258px" alt="Logo" src="'.$img_url.'">'; ?>
             <?php } else { ?>
             <?php echo'<img style="border-radius:50px;" width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Logo" src="'.$img_url.'">'; ?>
             <?php }
        }
        else
        {
            echo'<img style="border-radius:50px;" alt="Logo" src="'.$img_url.'">';
        }
        }
        }
      ?>
      </a>
      <?php }  }  ?>
      <div class="logo-tagline">
         <?php for ($i = 0; $i < count($event_templates); $i++)  { 
          if(empty($event_templates[$i]['huburl'])){?>
          <a href="<?php echo base_url(); ?>App/<?php echo "$acc_name"."/".$event_templates[0]['Subdomain']; ?>"><?php echo $event_templates[$i]['Event_name']; ?></a>
          <?php }else{ ?>
            <a href="<?php echo $event_templates[$i]['huburl']; ?>"><?php echo $event_templates[$i]['Event_name']; ?></a>
          <?php }  ?>
         <?php }  ?>
      </div>
		</div>  
        <ul class="main-navigation-menu">
           <?php
             $user = $this->session->userdata('current_user');
            
             
             $subject1 = $_SERVER['REQUEST_URI'];
             $pattern1 = '/\bapp\b/i';
             $homeclass = "";
             if (preg_match($pattern1, $subject1))
             {
                  $homeclass = "class='active'";
             }
           ?>
           <li <?php echo $homeclass; ?>>
           <?php for ($i = 0; $i < count($event_templates); $i++)  { 
            if(empty($event_templates[$i]['huburl'])){ ?>
              <a href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event_templates[$i]['Subdomain']; ?>"> <span class="title">Home</span></a>
              <?php }else{   ?>
                  <li <?php if($this->uri->segment(1)=="hub"){ ?> class="active" <?php } ?>>
                  <a href="<?php echo $event_templates[$i]['huburl']; ?>"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> ">Home</span></a></li>
                  <?php if($event_templates[0]['attendees_active']=='1'){ ?>
                  <li <?php if($this->uri->segment(1)=="Attendee_hub"){ ?> class="active" <?php } ?>>
                  <a href="<?php echo base_url().'Attendee_hub/'.$acc_name; ?>"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> ">Attendee</span></a></li>
                  <?php } if($event_templates[0]['public_messages_active']=='1'){ ?>
                  <li <?php if($this->uri->segment(1)=="Public_messages_hub"){ ?> class="active" <?php } ?>>
                  <a href="<?php echo base_url().'Public_messages_hub/'.$acc_name; ?>"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> ">Public messages</span></a></li>
                  <?php } ?>
                  <li <?php if($this->uri->segment(1)=="Photos_hub"){ ?> class="active" <?php } ?>>
                  <a href="<?php echo base_url().'Photos_hub/'.$acc_name; ?>"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> ">photos</span></a></li>
                  <?php
                    foreach ($hub_cms_menu as $key => $value)
                    {
                      $class = "";
                      $subject = $_SERVER['REQUEST_URI'];
                      $pattern = '/\b' . $value['Id'] . '\b/i';
                      
                      if (preg_match($pattern, $subject))
                      {
                           $class = "class='active'";
                      }

                      $url = base_url() . 'Cms/'.$acc_name.'/'.$value['Subdomain'] .'/View/' . $value['Id'];
                      echo '<li ' . $class . '><a id="' . $value['Menu_name'] . '_' . $value['Id'] . '"  href="' . $url . '"><span class="title">' . $value['Menu_name'] . '</span></a></li>';
                     }
                   ?>
                <?php }  ?>
           <?php } ?>
           </li>
           <?php if($event_templates[0]['Subdomain']!="thegrayreveal"){ foreach ($categories as $key => $item): ?>
              <?php if(strpos($item->category,' ') > 0){ $ucategory=str_replace(" ","__",$item->category); } else { $ucategory=$item->category; }?>
            <li>
               <a href="<?php echo base_url().'fundraising/'.$acc_name."/".$event_templates[0]['Subdomain'].'/'.'category/'.$ucategory; ?>">
                <?php echo ucfirst($item->category); ?>
              </a>
            </li>
          <?php endforeach; } ?>
           <?php 
           if($event_templates[0]['Subdomain']=="thegrayreveal"){
           if($user[0]->Role_name=="Client" || $user[0]->Role_name=="Attendee" || $user[0]->Role_name=="Speaker")
           {
                $flag=1;
           }
           else if($event_templates[0]['Event_type']==3)
           {
               $flag=1;
           }
           
           if($flag==1)
           {
                
              foreach ($cms_menu as $key => $value)
              {
                  $class = "";
                  $subject = $_SERVER['REQUEST_URI'];
                  $pattern = '/\b' . $value['Id'] . '\b/i';
                  
                  if (preg_match($pattern, $subject))
                  {
                       $class = "class='active'";
                  }

                  $url = base_url() . 'Cms/'.$acc_name.'/'.$event_templates[0]['Subdomain'] .'/View/' . $value['Id'];
                  echo '<li ' . $class . '><a id="' . $value['Menu_name'] . '_' . $value['Id'] . '" href="' . $url . '"><span class="title">' . $value['Menu_name'] . '</span></a></li>';
               }
           }
          }
           $flag=0;
           $intFund = 0;
           foreach ($menu_list as $key => $value)
           {  
              
                for ($i = 0; $i < count($event_templates); $i++)
                {
                     if(!in_array($value['id'],array(5,19,21,18,51,57)))
                     {
                          if($value['pagetitle']=="CMS")
                          {
                                $flag=1;
                          }
                          else
                          {

                               if(!in_array($value['id'],array(22,23,24,25,42,54)))
                               {
                                    $subject = $_SERVER['REQUEST_URI'];
                                    $pattern = '/\b' . $value['pagetitle'] . '\b/i';

                                    if (preg_match($pattern, $subject))
                                    {
                                         $class = "class='active'";

                                    }
                                    

                                    if(in_array($value['id'],array(12)))
                                    {    
                                         $pattern = '/\b' .'privatemsg'. '\b/i';
                                         
                                         if (preg_match($pattern, $subject))
                                         {
                                              $classprivate = "class='active'";
                                         }
                                         
                                         $msgurlprivate = base_url() .'Messages/' . $acc_name.'/' . $event_templates[$i]['Subdomain'].'/privatemsg';
                                         
                                         $var_target="";
                                         echo '<li ' . $classprivate . '><a '.$var_target.' id="' . $value['pagetitle'] . 'private" href="' . $msgurlprivate . '"><span class="title">'.$value['menuname'].'</span></a></li>';
                                    }
                                    else if(in_array($value['id'],array(13)))
                                    {    
                                         $pattern1 = '/\b' .'publicmsg'. '\b/i';

                                         if (preg_match($pattern1, $subject))
                                         {
                                              $classpublic = "class='active'";
                                         }
                                         
                                         $msgurlpublic = base_url() .'Messages/' . $acc_name .'/'.$event_templates[$i]['Subdomain'].'/publicmsg';
                                         
                                         $var_target="";
                                         echo '<li ' . $classpublic . '><a '.$var_target.' id="' . $value['pagetitle'] . 'public" href="' . $msgurlpublic . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }
                                    else
                                    {
                                         if($value['id']==26 || $value['id']==27)
                                         {

                                         }
                                         else
                                         {
                                            if($value['id']==1 && count($agendacategory)!='0')
                                            {
                                              foreach ($agendacategory as $catkey => $catvalue) {
                                                $url = base_url().'Agenda/'.$acc_name.'/'. $event_templates[$i]['Subdomain'].'/view_category_session/'.$catvalue['Id'];
                                                echo '<li ' . $class . '><a '.$var_target.' id="' . $catvalue['categoryname'] . '" href="' . $url . '"><span class="title">'.$catvalue['categoryname'].'</span></a></li>';
                                              }
                                            }
                                            else if ($value['id'] == 55 )
                                            {   
                                                if($value['show_in_front'] == '1')
                                                {
                                                  $url = base_url().'App/'.$acc_name.'/'.$event_templates[$i]['Subdomain'].'/attendee_registration_screen';
                                                  echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">'.$value['menuname'] . '</span></a></li>';
                                                }
                                            }
                                            else
                                            {
                                              if($value['pagetitle'] == 'fundraising')
                                                $url = base_url() . ($value['pagetitle']) . '/' .$acc_name.'/'. $event_templates[$i]['Subdomain'];
                                              else
                                                $url = base_url() . ucfirst($value['pagetitle']) . '/' .$acc_name.'/'. $event_templates[$i]['Subdomain'];
                                              $var_target="";
                                              if($value['id']==20)
                                              {
                                                $intFund = 1;
                                                //$var_target='target="_blank"';
                                              }
                                              if($value['id']!=28 && $value['id']!=29 && $value['id']!=8 && $value['id']!=14 && $value['id']!=40){ 
                                                echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">'.$value['menuname'] . '</span></a></li>';
                                              }
                                            }
                                         }
                                    }
                               }
                               else
                               {  
                                    if($intFund==1)
                                    {
                                    $subject = $_SERVER['REQUEST_URI'];
                                    $pattern = '/\b' . $value['pagetitle'] . '\b/i';

                                    if (preg_match($pattern, $subject))
                                    {
                                         $class = "class='active'";
                                    }

                                    $url = base_url() . 'fundraising' . '/'.$acc_name.'/'.$event_templates[$i]['Subdomain'].'/home';
                                    $var_target="";
                                   
                                    if($value['id']==22)
                                    {
                                         $url=$url.'/get_auction_pr/1';
                                         echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }
                                    else if($value['id']==23)
                                    {
                                         $url=$url.'/get_auction_pr/2';
                                         echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }                                        
                                    else if($value['id']==24)
                                    {
                                         $url=$url.'/get_auction_pr/3';
                                         echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }
                                    else if($value['id']==25)
                                    {
                                         $url=$url.'/get_auction_pr/4';
                                         echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }
                                    else if($value['id']==42)
                                    {
                                      $url= $url = base_url() . 'fundraising/' . $acc_name."/". $event_templates[$i]['Subdomain'].'/home/fundraising_donation';
                                       echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">' . $value['menuname'] . '</span></a></li>';
                                    }
                                    }
                                   
                                    
                               }
                          }
                         $class = "";
                     }
              }
           }
         /*  $url = base_url() . 'fundraising'.'/'.$acc_name."/".$event_templates[0]['Subdomain'].'/home';
           echo '<li ' . $class . '><a '.$var_target.' id="' . $value['pagetitle'] . '" href="' . $url . '"><span class="title">Fundraising</span></a></li>';
             */ 
           ?><?php if($event_templates[0]['Subdomain']=="thegrayreveal"){ foreach ($categories as $key => $item): ?>
              <?php if(strpos($item->category,' ') > 0){ $ucategory=str_replace(" ","__",$item->category); } else { $ucategory=$item->category; }?>
            <li>
               <a href="<?php echo base_url().'fundraising/'.$acc_name."/".$event_templates[0]['Subdomain'].'/'.'category/'.$ucategory; ?>">
                <?php echo ucfirst($item->category); ?>
              </a>
            </li>
          <?php endforeach; } 
           if($user[0]->Role_name=="Client" || $user[0]->Role_name=="Attendee" || $user[0]->Role_name=="Speaker")
           {
                $flag=1;
           }
           else if($event_templates[0]['Event_type']==3)
           {
               $flag=1;
           }
           if($event_templates[0]['Subdomain']!="thegrayreveal"){
           if($flag==1)
           {
                
              foreach ($cms_menu as $key => $value)
              {
                  $class = "";
                  $subject = $_SERVER['REQUEST_URI'];
                  $pattern = '/\b' . $value['Id'] . '\b/i';
                  
                  if (preg_match($pattern, $subject))
                  {
                       $class = "class='active'";
                  }

                  $url = base_url() . 'Cms/'.$acc_name.'/'.$event_templates[0]['Subdomain'] .'/View/' . $value['Id'];
                  echo '<li ' . $class . '><a id="' . $value['Menu_name'] . '_' . $value['Id'] . '" href="' . $url . '"><span class="title">' . $value['Menu_name'] . '</span></a></li>';
               }
           }
          }
           ?>
           <?php if($event_templates[0]['hub_menu_show']=='1'){ ?>
            <li>
             <a href="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> "><?php echo $event_templates[0]['hub_menu_title']; ?></span></a>
             </li>
          <?php } if($event_templates[0]['signature_active']=='1'){ ?>
           <li>
            <a href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/signature_form"> <span class="title" style="color:<?php echo $event_templates[0]['menu_text_color']; ?> ">About This App</span></a>
            </li>
            <?php } ?>     
             </ul>
  </div>
</div>
<?php 
$bgcolor=$event_templates[0]['menu_background_color'];
$textcolor=$event_templates[0]['menu_text_color'];
$bhc=$event_templates[0]['menu_hover_background_color']; ?>
<style type="text/css">
/* css for sidemenu ios possion fixed */

.top_head_wrap{z-index: 9999;display: block !important;}
@media screen and (max-width: 959px) and (min-width: 768px){
  .top_head_wrap{z-index: 9999;display: block !important;}
}
@media screen and (max-width: 1024px){
  .top_head_wrap{z-index: 9999;display: block !important;}
  .main-navigation-menu{
    bottom: 0;
    left: 0;
    overflow: auto;
    position: absolute;
    right: 0;
    top: 0;
  }
}
ul.main-navigation-menu li a
{
  background-color: <?php echo $bgcolor; ?>;
  color: <?php echo $textcolor; ?>
}
ul.main-navigation-menu li a:hover,ul.main-navigation-menu li.active a
{
  background-color:<?php echo $bhc; ?>
}
</style>
<?php } ?>
<script type="text/javascript">
$( document ).ready(function() {
  var topheadheight = $(".top_head_wrap").height();
  if (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod') 
  {
    window.ontouchstart = function () 
    {
      $(".top_head_wrap").css("position", "absolute");
      $(".top_head_wrap").css("top", "0");
      $(".top_head_wrap").css("display", "block");
    }
  }
  $(".main-navigation-menu").css({ 'margin-top': topheadheight});
  $('.lefthandmenumap').mapster({
    selected: true,
    fill: false,
    strokeColor: false,
    fillColor: 'FFFFFF',
    fillOpacity: 0.6,
  });
});
</script>