<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Event Template <span class="text-bold">List</span></h4>
                <div class="panel-tools"></div>
			</div>
			<div class="panel-body">
                <?php if($this->session->flashdata('speaker_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_templates_data'); ?> Successfully.
                </div>
                <?php } ?> 
                <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Event Type</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php 
	                        for($i=0;$i<count($event_templates);$i++)
							{
							?>
							<tr>
								<td><?php echo $i+1; ?></td>
									<?php $surl = "https://".$event_templates[$i]['Subdomain'].'.eventapp.fmv.cc'; ?>
								<!--<td><a target="_blank" href="<?php echo $surl; ?> "><?php echo $event_templates[$i]['Event_name']; ?></a></td>-->						
								<td><a href="<?php echo base_url().'Events/'.$event_templates[$i]['Subdomain'] ?>"><?php echo $event_templates[$i]['Event_name']; ?></a></td>

								<td><?php echo $event_templates[$i]['Start_date']; ?></td>
								<td><?php echo $event_templates[$i]['End_date']; ?></td>
								<td>
									<?php 

										if($event_templates[$i]['Event_type'] =='1')
										{
											echo 'Public';
										}
										else
										{
											echo 'Private';
										}
									?>
								</td>
								<td>
									<?php 

										if($event_templates[$i]['Status'] =='1' && $event_templates[$i]['End_date'] >= date("Y-m-d"))
										{
											echo 'Active';
										}
										else
										{
											echo 'Inactive';
										}
									?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->