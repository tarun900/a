<!-- start: PAGE CONTENT -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/event_template.css" />
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<?php 
	  for($i=0;$i<count($event_templates);$i++)
		{
		?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
		<script type="text/javascript">
      jQuery( document ).ready(function()
      {
      
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom:15,  
        center: new google.maps.LatLng(<?php echo $event_templates[$i]['Lattitude']; ?>, <?php echo $event_templates[$i]['Longitude']; ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var infowindow = new google.maps.InfoWindow();
      var locations;
      var marker, i;
      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
        });

        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }

        })(marker, i));
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
          return function() {

            infowindow.close();
          }

        })(marker, i));
         google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
          infowindow.setContent(locations[i][0]);
             infowindow.open(map, marker);
          }

        })(marker, i));
      }
      });
    </script>
		<div class="panel panel-white">
      <div class="container">
        <div class="row" style="padding-left: 0px;background:<?php echo $event_templates[$i]['Background_color']; ?>">
          <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;margin-bottom: 44px;">
            <section class="static-page">
              <nav class="navigation">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <!-- <div class="hidden-phone"> 
                        <ul class="main-menu" style="padding:0 0 0 30px;">
                          <li> <a href="#" title="Home" class="title">Home</a> </li>
                          <li> <a href="#" title="About us" class="title">About us</a> </li>
                          <li> <a href="#" title="Store locator" class="title active">Events</a> </li>
                          <li> <a href="#" title="Blog" class="title">Blog</a> </li>
                          <li> <a href="#" title="Contact us" class="title">Contact us</a> </li>
                        </ul>
                      </div> -->
                    </div>                    
                  </div>
                </div>
              </nav>
              <div class="row-fluid">
                <div class="col-md-12">
                  <div class="content">
                    <h1 class="col-md-6"><?php echo $event_templates[$i]['Event_name']; ?></h1>
                    <p class="lead col-md-6" style="text-align:right;">Event Timing: 
                    <?php 
                      $timestamp1 = $event_templates[$i]['Start_date'];
                      $timestamp2 = $event_templates[$i]['End_date'];
                      $datetime1 = explode(" ",$timestamp1);
                      $datetime2 = explode(" ",$timestamp2);
                      $start_date = $datetime1[0];
                      $end_date = $datetime2[0];
                      echo $start_date.'&nbsp; from &nbsp;'.$end_date ?></p>
                    <?php if($event_templates[$i]['Description'] !='') { ?>
                    <p class="col-md-12" style="margin-left:0 !important;"><?php echo $event_templates[$i]['Description']; ?></p>
                    <?php } else { } ?>
                    <?php if($event_templates[$i]['Contact_us'] !='') { ?>
                     <hr />
                    <h3>Rsvp</h3>
                    <div class="row-fluid">
                      <div class="col-md-6">
                        <div class="map" style="height: 320px; width: 100%; background-color: #F0F0F0;" data-address="16 Clocktower Mews, Newmarket, CB8 8LL, UK" data-zoom="12"><div id="map" class="googlemapimage"></div></div>
                      </div>
                      <div class="col-md-6">
                        <h5><em class="icon-phone icon-larger"></em> Call us</h5>
                        <p><?php echo $event_templates[$i]['Contact_us']; ?></p>
                      </div>                      
                    </div>
                    <?php } else { } ?>
                    
                    <?php if($event_templates[$i]['Images'] != "NULL")    { ?>
                    <hr />
                    <div class="row-fluid">
                      <div class="col-md-12">
                        <h3>Photos</h3>
                          <ul style="width:100%; padding-left:0px;">
                            <?php $images_array = json_decode($event_templates[$i]['Images']); ?>
                              <?php for($i=0;$i<count($images_array);$i++) { ?>
                                <li style="display:inline-block;">
                                  <img alt="" src="<?php echo base_url(); ?>assets/timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>&h=220&w=230">
                                  <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                </li>
                              <?php } ?>
                          </ul>
                        </div>
                      </div>
                      <?php } else { } ?>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
		</div>
		<?php } ?>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->
