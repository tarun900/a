<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/coustom.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-f.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
<?php if($save_success){ ?> 
<div class="errorHandler alert alert-success no-display" style="display: block;">
    <i class="fa fa-remove-sign"></i> Scan Lead User Save SuccessFully... 
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
window.parent.location.href = '<?php echo base_url()."Lead_retrieval/".$acc_name."/".$Subdomain."#my_leads"; ?>';
window.parent.location.reload();
</script>
<?php }else{ if(!empty(@$attendee_user)){ ?>
<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery.datetimepicker.css"/>
<style type="text/css">
.smart-wizard input{
	margin-bottom: 15px;
}
.smart-wizard label{
	text-align: left !important;
}
</style>
<div class="surway-page">
	<div class="panel panel-white">
		<div class="panel-body">
			<form action="" role="form" class="smart-wizard" id="exibitorquestionforms" method="POST">
				<div id="exibitorquestionwizard" class="swMain">
					<ul style="display: none;">
						<li><a href="#step-0"></a></li>
			            <?php foreach(@$questions as $key => $val){ ?>
			            <li><a href="#step-<?php echo @$val['q_id']; ?>"></a></li>
			            <?php } ?>
		        	</ul>
		        	<div id="step-0">
		        		<input type="hidden" name="lead_user_id" id="lead_user_id" value="<?php echo @$attendee_user['Id']; ?>">
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="firstname">
		        				Firstname <span class="symbol required"></span>
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter Firstname" id="firstname" name="firstname" class="form-control name_group required" value="<?php echo @$attendee_user['Firstname']; ?>">
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="Lastname">
		        				Lastname <span class="symbol required"></span>
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter Lastname" id="lastname" name="lastname" class="form-control name_group required" value="<?php echo @$attendee_user['Lastname']; ?>">
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="Email">
		        				Email Address <span class="symbol required"></span>
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter Email Address" id="email" name="email" class="form-control name_group required" value="<?php echo @$attendee_user['Email']; ?>">
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="Title">
		        				Title <span class="symbol required"></span>
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter Title" id="title" name="title" class="form-control name_group required" value="<?php echo @$attendee_user['Title']; ?>">
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="Company_name">
		        				Company Name <span class="symbol required"></span>
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter Company Name" id="company_name" name="company_name" class="form-control name_group required" value="<?php echo @$attendee_user['Company_name']; ?>">
		        			</div>
		        		</div>
		        		<?php $custom_data=json_decode(@$attendee_user['extra_column'],true); foreach (@$custom_column as $key => $value) { ?>
		        		<div class="form-group">
		        			<label class="control-label col-sm-12" for="<?php echo 'text_'.@$value['column_id'] ?>">
		        				<?php echo ucfirst(@$value['column_name']); ?> 
		        			</label>
		        			<div class="col-sm-12">
		        				<input type="text" placeholder="Enter <?php echo ucfirst(@$value['column_name']); ?>" id="<?php echo 'text_'.@$value['column_id'] ?>" value="<?php echo @$custom_data[@$value['column_name']]; ?>" name="<?php echo @$value['column_id'] ?>" class="form-control">
		        			</div>
		        		</div>
		        		<?php } ?>
		        		<div class="form-group" style="width: 82%;">
			        		<?php if(!empty(@$questions)){ ?>
			        		<button class="btn btn-green next-step btn-block pull-right" data-redirectid="<?php echo $questions[0]['q_id']; ?>" data-thisstepid="0"> Next <i class="fa fa-arrow-circle-right"></i> </button>
			        		<?php }else{ ?>
			        		<button class="btn btn-primary finish-step btn-block pull-right"> Finish <i class="fa fa-arrow-circle-right"></i> </button>
			        		<?php } ?>
		        		</div>
      				</div>
		        	<?php foreach(@$questions as $key => $val){ ?>
      				<input type="hidden" name="question[]" value="<?php echo @$val['q_id']; ?>" />
      				<div id="step-<?php echo @$val['q_id']; ?>">
      					<div class="list-group">
							<div class="list-group-item active">
								<h4 class="list-group-item-heading"><?php echo $val['Question']; ?></h4>                
							</div>
							<div class="">
								<div class="col-md-12 col-lg-6 col-sm-12">
									<div class="form-group">
										<span class="hdr-span"> 
										<?php if($val['Question_type']==1){ echo 'Please select one answer.';  }else if($val['Question_type']==2){ echo 'Please select mulitple answer.'; } ?>
										</span>
										<div class="skins">
											<div class="skin skin-line">
												<dl class="clear">
													<dd class="selected">
														<div class="skin-section">
															<ul class="list">
																<?php switch ($val['Question_type']) { case 1: $Option=json_decode($val['Option']); $redirectids=json_decode($val['redirectids'],true); foreach($Option as $keyo=>$valuo){ ?>
																<li class="radiocheckboxli">
																	<input type="radio" data-redirectid="<?php echo empty($redirectids[str_ireplace(" ","_",$valuo)]) ? $val['redirectid'] :  $redirectids[str_ireplace(" ","_",$valuo)]; ?>" name="question_option<?php echo $val['q_id']; ?>" value="<?php echo $valuo; ?>" class="required radio-callback">
																	<label for="line-radio-<?php echo $val['q_id']; ?>"><?php echo $valuo; ?></label>
																</li>
																<?php } if($val['show_commentbox']=='1'){ if($val['commentbox_display_style']=='0'){ ?>
																<li class="radiocheckboxli" style="background-color:#2489c5;">
																	<input type="radio" data-redirectid="<?php echo $val['redirectid']; ?>" name="show_commentbox<?php echo $val['q_id']; ?>" class="radio-callback" value="<?php echo $val['commentbox_label_text']; ?>">
																	<div style="text-align: left;">
																	<?php echo $val['commentbox_label_text']; ?>
																	</div>
																	<textarea data-redirectid="<?php echo $val['redirectid']; ?>" style="margin: 0 auto;width: 90%;max-width: 90%;min-width: 90%;" name="answer_comment<?php echo $val['q_id']; ?>" class="texta form-control"></textarea>
																</li>
																<?php }else{ ?>  
																<li class="radiocheckboxli">
																	<div style="text-align: left;">
																	<?php echo $val['commentbox_label_text']; ?>
																	</div>
																	<textarea data-redirectid="<?php echo $val['redirectid']; ?>" name="answer_comment<?php echo $val['q_id']; ?>" class="texta form-control"></textarea>
																</li>
																<?php } } ?>
																<span for="question_option<?php echo $val['q_id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
																<?php break; case 2: $Option=json_decode($val['Option']); foreach($Option as $keyo=>$valuo){ ?>
																<li class="radiocheckboxli">
																	<input type="checkbox" data-redirectid="<?php echo $val['redirectid']; ?>" name="question_option<?php echo $val['q_id']; ?>[]" value="<?php echo $valuo; ?>" class="radio-callback required">
																	<label for="line-radio-<?php echo $val['q_id']; ?>[]"><?php echo $valuo; ?></label>
																</li>
																<?php } if($val['show_commentbox']=='1'){ if($val['commentbox_display_style']=='0'){ ?>
																<li class="radiocheckboxli" style="background-color:#2489c5;">
																	<input type="checkbox" data-redirectid="<?php echo $val['redirectid']; ?>" name="show_commentbox<?php echo $val['q_id']; ?>[]" class="radio-callback" value="<?php echo $val['commentbox_label_text']; ?>">
																	<div style="text-align: left;">
																	<?php echo $val['commentbox_label_text']; ?>
																	</div>
																	<textarea data-redirectid="<?php echo $val['redirectid']; ?>" style="margin: 0 auto;width: 90%;max-width: 90%;min-width: 90%;" name="answer_comment<?php echo $val['q_id']; ?>" class="texta form-control"></textarea>
																</li>
																<?php }else{ ?>  
																<li class="radiocheckboxli">
																	<div style="text-align: left;">
																	<?php echo $val['commentbox_label_text']; ?>
																	</div>
																	<textarea data-redirectid="<?php echo $val['redirectid']; ?>" name="answer_comment<?php echo $val['q_id']; ?>" class="texta form-control"></textarea>
																</li>
																<?php } } ?>
																<span for="question_option<?php echo $val['q_id']; ?>[]" class="help-block" style="display:none;">Please select an answer.</span>
																<?php  break; case 3: ?>
																<li>
																	<textarea data-redirectid="<?php echo $val['redirectid']; ?>" name="question_option<?php echo $val['q_id']; ?>" class="texta form-control required"></textarea>
																</li>
																<span for="question_option<?php echo$val['q_id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>  
																<?php break; case 4: ?>
																<li>
																	<div class="row star-center session-rating-star" style="text-align:center;">
																		<fieldset id='session_rating_field' class="rating" >
																			<input data-redirectid="<?php echo $val['redirectid']; ?>" class="stars" type="radio" id="star5" name="question_option<?php echo $val['q_id']; ?>" value="5" />
																			<label class = "full" for="star5" title="Awesome - 5 stars"></label>
																			<input data-redirectid="<?php echo $val['redirectid']; ?>" class="stars" type="radio" id="star4" name="question_option<?php echo $val['q_id']; ?>" value="4" />
																			<label class = "full" for="star4" title="Pretty good - 4 stars"></label>
																			<input data-redirectid="<?php echo $val['redirectid']; ?>" class="stars" type="radio" id="star3" name="question_option<?php echo $val['q_id']; ?>" value="3" />
																			<label class = "full" for="star3" title="Meh - 3 stars"></label>
																			<input data-redirectid="<?php echo $val['redirectid']; ?>" class="stars" type="radio" id="star2" name="question_option<?php echo $val['q_id']; ?>" value="2" />
																			<label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
																			<input data-redirectid="<?php echo $val['redirectid']; ?>" class="stars" type="radio" id="star1" name="question_option<?php echo $val['q_id']; ?>" value="1" />
																			<label class = "full" for="star1" title="Sucks big time - 1 star"></label>
																		</fieldset>
																	</div>
																</li>
																<span for="question_option<?php echo $key; ?>" class="help-block" style="display:none;">Please Gives an Rating.</span>  
																<?php break; case 5: $Option=json_decode($val['Option']); foreach($Option as $keyo=>$valuo){ ?>
																<li>
																	<div style="text-align: left;">
																		<?php echo $valuo; ?>
																	</div>
																	<input type="text" data-redirectid="<?php echo $val['redirectid']; ?>" class="form-control required" name="question_option<?php echo $val['q_id']; ?>[]">
																</li>
																<?php } ?>
																<span for="question_option<?php echo $val['q_id']; ?>[]" class="help-block" style="display:none;">Please select an answer.</span>
																<?php break; case 6: ?>
																<li>
																	<input data-redirectid="<?php echo $val['redirectid']; ?>" type="text" class="form-control required datetimepicker" name="question_option<?php echo $val['q_id']; ?>">
																</li>
																<span for="question_option<?php echo $val['q_id']; ?>" class="help-block" style="display:none;">Please select an DateTime.</span>
																<?php break; default: ?>
																<li> </li>
																<?php break;  } ?> 
															</ul>
														</div>
													</dd>
												</dl>
											</div>
										</div>          
									</div>
									<div class="form-group" style="clear: both;">
										<div class="row">
											<div class="col-sm-3 pull-left">
												<button class="btn btn-green back-step btn-block" data-redirectid="<?php echo $questions[$key-1]['q_id']; ?>" data-thisstepid="<?php echo $val['q_id']; ?>"><i class="fa fa-arrow-circle-left"></i> Back </button>
											</div>
											<div class="col-sm-3 pull-right">
												<?php if(!empty($val['redirectid']) || $val['skip_logic']=='1'){ ?>
												<button class="btn btn-green next-step btn-block" data-redirectid="<?php echo $val['redirectid']; ?>" data-thisstepid="<?php echo $val['q_id']; ?>"> Next <i class="fa fa-arrow-circle-right"></i> </button>
												<?php }else if(empty($val['redirectid']) && $val['skip_logic']!='1'){ ?>
												<button class="btn btn-primary finish-step btn-block"> Finish <i class="fa fa-arrow-circle-right"></i></button>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>  
						</div>
      				</div>
      				<?php } ?>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php  echo base_url();?>assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizard_exibitor_question.js?<?php echo time(); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	exibitorquestionFormWizard.init();	
	$('.radiocheckboxli input').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();
		label.remove();
		self.iCheck({
			checkboxClass: 'icheckbox_line-blue',
			radioClass: 'iradio_line-blue',
			insert: '<div class="icheck_line-icon"></div>' + label_text
		});
	});
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) 
	{
		jQuery("body").addClass("sidebar-close");
	}
	else
	{
		jQuery("body").removeClass("sidebar-close");
	}
});
$('.datetimepicker').datetimepicker({
  format:'Y-m-d H:i',
  step:5
});
</script>
<script src="https://www.allintheloop.net/demo/assets/plugins/iCheck/jquery.icheck.min.js"></script> 
<?php }else{ ?>
<div class="row">
	<h2 style="text-align: center;">Scan Lead Not Found In This App.</h2>
</div>
<?php } } ?>