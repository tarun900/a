<?php $k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name'); ?>
<div class="row margin-left-10">
  <?php
  if(!empty($user)):
    if(!empty($form_data)):
      foreach($form_data as $intKey=>$strval):
        if($strval['frm_posi']==2)
        {
          continue;
        } ?>
        <div class="col-sm-6 col-sm-offset-3">
        <?php $json_data = false; $formid="form"; $temp_form_id=rand(1,9999);
        $json_data = isset($strval) ? $strval['json_data'] : FALSE;
        $loader = new formLoader($json_data, base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
        $loader->render_form();
        unset($loader); ?>
        </div>
        <?php include 'validation.php'; ?>
        <script>
        jQuery(document).ready(function() {
          FormValidator<?php echo $temp_form_id; ?>.init();
        });
        </script>
      <?php endforeach;  
    endif;
  endif; ?>
</div>
<?php 
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
  <?php 
  $string_cms_id = $advertisement_images->Cms_id;
  $Cms_id =  explode(',', $string_cms_id); 
  $string_menu_id = $advertisement_images['Menu_id'];
  $Menu_id =  explode(',', $string_menu_id); 
  if(in_array('53', $Menu_id)) 
  {
    $image_array = json_decode($advertisement_images['H_images']);
    $f_url = $advertisement_images['Footer_link'];
    if(!empty($image_array)) { ?>
      <div class="hdr-ads">
        <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id'] ?>");'> 
          <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
          <?php $img_url = base_url().'assets/user_files/'.$image_array[0]; $size = getimagesize($img_url); 
          $originalWidth = $size[0];
          $originalHeight = $size[1];
          if($originalHeight > '118')
          {
            $first = $originalWidth * 118;
            $width = $first / $originalHeight;
            echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
          }
          elseif ($originalHeight < '118') 
          { 
            echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
          }
          else
          {
            echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
          } ?>
        </a>
        <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>
        <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
        aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
        <?php } } } ?>
      </div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('lead_success')) { ?>
  <div class="errorHandler alert alert-success no-display" style="display: block;">
      <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('lead_success'); ?> 
  </div>
<?php } ?>
<?php if ($this->session->flashdata('lead_error')) { ?>
  <div class="errorHandler alert alert-danger no-display" style="display: block;">
      <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('lead_error'); ?> 
  </div>
<?php } ?>
<div class="agenda_content attendee_content_div speakers-content">
  <div class="panel panel-white">
    <div class="tabbable">
      <div class="row">
        <div class="col-md-9 col-sm-9">
          <ul id="myTab2" class="nav nav-tabs">
            <li class="active">
              <a href="#scan_leads" id="scan_leads_a" data-toggle="tab">
                <h4 style="text-align: center;">Scan Leads</h4>
              </a>
            </li>
            <li>
              <a href="#my_leads" id="my_leads_a" data-toggle="tab">
                <h4 style="text-align: center;">My Leads</h4>
              </a>
            </li>
            <?php if($user[0]->Rid=='6'){ ?>
            <li>
              <a href="#setting" id="setting_a" data-toggle="tab">
                <h4 style="text-align: center;">Settings</h4>
              </a>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="tab-content">
      <div class="tab-pane active in" id="scan_leads">
        <div class="form-group col-sm-12">
          <label class="control-label col-sm-3"><h4>Enter badge number</h4></label>
          <div class="col-sm-4">
            <input type="text" id="badge_number" class="form-control" placeholder="Type the badge number here">
            <span class="help-block" id="badge_number_error" style="display: none;text-align: left;">This field is required.</span>
          </div>
          <div class="col-sm-3">
            <input type="button" class="btn btn-green" value="Scan Lead" onclick="openpopup();">
          </div>
        </div>
      </div>
      <div class="tab-pane" id="my_leads">
        <div class="input-group search_box_user">
          <i class="fa fa-search search_user_icon"></i>    
          <input type="text" class="form-control" id="my_leads_search_user" placeholder="Search My Leads...">
        </div>
       <!--  <?php if($event_templates[0]['allow_export_lead_data']=="1"){ ?>
        <div class="row">
          <div class="col-md-12 space20">
            <div class="btn-group pull-right">
              <button data-toggle="dropdown" class="btn btn-green dropdown-toggle">
                  Export Session Data <i class="fa fa-angle-down"></i>
              </button>
              <ul class="dropdown-menu dropdown-light pull-right">
                <li>
                  <a href="<?php echo base_url().'lead_retrieval/'.$acc_name.'/'.$Subdomain.'/export_my_lead'; ?>">
                    Download Directly
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url().'lead_retrieval/'.$acc_name.'/'.$Subdomain.'/email_my_lead'; ?>">
                    Email My Leads
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <?php } ?> -->
        <div class="ps-container ps-container-new">
          <ul class="activities columns columns-new" id="my_leads_ul">
          <?php foreach ($my_lead as $key => $value) { ?>
            <li>
              <a class="activity" style="border: 1px solid #eee;">
                <div class="desc">
                  <h4 class="user_container_name"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></h4>
                  <span class="user_profile" style="padding:0px;">
                    <?php echo ucfirst($value['Title']); ?>
                    <?php if(!empty($value['Title']) && !empty($value['Company_name'])){ ?>
                      At
                    <?php } ?>
                    <?php echo ucfirst($value['Company_name']); ?>
                  </span>
                </div>
              </a>
            </li>
          <?php } ?>
          </ul>
        </div>
      </div>
      <div class="tab-pane" id="setting">
        <div class="input-group search_box_user">
          <i class="fa fa-search search_user_icon"></i>    
          <input type="text" class="form-control" id="setting_search_user" placeholder="Search Representatives...">
        </div>
        <div class="row">
          <h2 class="col-sm-9">Linked Represenatatives</h2>
          <div class="col-sm-2 pull-right">
            <a class="btn btn-green btn-block" href="javascript:void(0);" data-toggle="modal" data-target="#add_resp_model" <?php if($total_reps_add >= $exibitor_premission['maximum_Reps'] && !empty($exibitor_premission['maximum_Reps']))
      { ?> disabled="disabled" <?php } ?>>
              <i class="fa fa-plus"></i> Add New Rep
            </a>
          </div>
        </div>
        <div class="ps-container ps-container-new">
          <ul class="activities columns columns-new" id="mu_representative_ul">
            <?php foreach ($my_representatives as $key => $value) { ?>
            <li>
              <a class="activity">
                <?php $logo= !empty($value['Logo']) ? $value['Logo'] : json_decode($value['company_logo'])[0]; if(!empty($logo)) { ?>
                <img style="border-radius: 50%;height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $logo;?>" alt="" />
                <?php } else { ?>
                <span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $event_templates[0]['Top_background_color']; ?>"><?php echo ucfirst(substr($value['name'], 0, 1)).''.ucfirst(substr($value['Lastname'], 0, 1)); ?></span>
                <?php } ?>
                <div class="desc">
                  <h4 class="user_container_name"><?php echo ucwords($value['Firstname'].' '.$value['Lastname']);?></h4>
                  <span class="user_profile" style="padding:0px;">
                    <?php echo ucfirst($value['Title']); ?>
                    <?php if(!empty($value['Title']) && !empty($value['Company_name'])){ ?>
                      At
                    <?php } ?>
                    <?php echo ucfirst($value['Company_name']); ?>
                  </span>
                </div>
                <div class="col-sm-3 pull-right">
                  <div class="col-sm-6">
                    <button class="btn btn-red btn-block" onclick='delete_representative("<?php echo $value['Id']; ?>");'>Delete</button>
                  </div> 
                </div>
              </a>
            </li>
            <?php } ?>
          </ul>
        </div>  
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.modal-scrollable{
  z-index: 9999 !important;
}
</style>
<div id="add_resp_model" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Representatives</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form method="post" action="<?php echo base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/add_new_resp' ?>">
            <div class="col-sm-12">
              <select id="add_resp_select" style="height: auto;" class="select2-container select2-container-multi form-control search-select menu-section-select" name="resp_ids[]" multiple="multiple">
                <?php foreach ($attendee_user as $key => $value) { ?>
                <option value="<?php echo $value['Id']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-12">
              <button type="submit" style="margin-top:15px;" class="btn btn-primary">Add</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
jQuery(document).ready(function(){   
  SVExamples.init();
  $('#add_resp_select').select2();
  if(location.hash){
    $('#myTab2 li').removeClass('active');
    $(location.hash+"_a").parent().addClass('active');
    $('.tab-pane').removeClass('active');
    $(location.hash).addClass('active');
  }
});
function openpopup()
{
  if($.trim($('#badge_number').val())!="")
  {
    if($.trim($('#badge_number').val()).indexOf('-'))
    {
      var badge=$.trim($('#badge_number').val()).split('-');
      if($.isNumeric(badge[0]) && $.isNumeric(badge[1]))
      {
        $('#badge_number_error').parent().parent().addClass('has-success').removeClass('has-error');
        $('#badge_number_error').hide();
        var userurl="<?php echo base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/scan_lead_user/' ?>"+badge[1];
        $.fancybox({
          href: userurl,
          'type' : 'iframe',
          openEffect  : 'none',
          closeEffect : 'none',
          mouseWheel : false,
        });
      }
      else
      {
        $('#badge_number_error').html('Please Enter Proper Badge Number.');
        $('#badge_number_error').parent().parent().addClass('has-error').removeClass('has-success');
        $('#badge_number_error').show();
        $('#badge_number').focus();
      }
    }
    else
    {
      $('#badge_number_error').html('Please Enter valid Format Badge Number.');
      $('#badge_number_error').parent().parent().addClass('has-error').removeClass('has-success');
      $('#badge_number_error').show();
      $('#badge_number').focus();
    }
  }
  else
  {
    $('#badge_number_error').html('This field is required.');
    $('#badge_number_error').parent().parent().addClass('has-error').removeClass('has-success');
    $('#badge_number_error').show();
    $('#badge_number').focus();
  }
}
function delete_representative(resp_uid)
{
  if(confirm("Are You Sure Delete This ?"))
  {
    window.location.href="<?php echo base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/delete_my_representative/'; ?>"+resp_uid;
  }
}
jQuery("#my_leads_search_user").keyup(function(){    
  jQuery("#my_leads_ul li").each(function( index ){
    var str_name=jQuery(this).find('.user_container_name').html();
    var str_profiles=jQuery(this).find('.user_profile').html();
    if(jQuery.trim(str_name)!=undefined || jQuery.trim(str_name.toLowerCase())!='' || jQuery.trim(str_profiles)!=undefined || jQuery.trim(str_profiles.toLowerCase())!='')
    {    
      var str_name1=jQuery(this).find('.user_container_name').html();
      var str_profiles1=jQuery(this).find('.user_profile').html();
      if(str_name1!=undefined || str_profiles1!=undefined)
      {
        var str_name2=jQuery(this).find('.user_container_name').html().toLowerCase();
        var str_profiles2=jQuery(this).find('.user_profile').html().toLowerCase();
        var content=jQuery("#my_leads_search_user").val().toLowerCase();
        if(content!=null)
        {                   
          var name = str_name2.indexOf(content);   
          var profile =str_profiles2.indexOf(content);                 
          if(name!="-1" || profile!="-1")
          {
           jQuery(this).css('display','inline-block');
          }
          else
          {
            jQuery(this).css('display','none');
          }
        }
      }
    }
  });
});
jQuery("#setting_search_user").keyup(function(){    
  jQuery("#mu_representative_ul li").each(function( index ){
    var str_name=jQuery(this).find('.user_container_name').html();
    var str_profiles=jQuery(this).find('.user_profile').html();
    if(jQuery.trim(str_name)!=undefined || jQuery.trim(str_name.toLowerCase())!='' || jQuery.trim(str_profiles)!=undefined || jQuery.trim(str_profiles.toLowerCase())!='')
    {    
      var str_name1=jQuery(this).find('.user_container_name').html();
      var str_profiles1=jQuery(this).find('.user_profile').html();
      if(str_name1!=undefined || str_profiles1!=undefined)
      {
        var str_name2=jQuery(this).find('.user_container_name').html().toLowerCase();
        var str_profiles2=jQuery(this).find('.user_profile').html().toLowerCase();
        var content=jQuery("#setting_search_user").val().toLowerCase();
        if(content!=null)
        {                   
          var name = str_name2.indexOf(content);   
          var profile =str_profiles2.indexOf(content);                 
          if(name!="-1" || profile!="-1")
          {
           jQuery(this).css('display','inline-block');
          }
          else
          {
            jQuery(this).css('display','none');
          }
        }
      }
    }
  });
});
</script>