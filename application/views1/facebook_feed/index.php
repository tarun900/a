<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
?>
<div class="row margin-left-10">
    <?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
         
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data,base_url().'Facebook_feed/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('47', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if($image_array!='') { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } }  } ?>
 </div>
</div>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/facebookfeedstyle.css?'.time(); ?>">
<div id="fb_feeds" class="row">
	<?php if(!empty($event_templates[0]['facebook_page_name'])){ ?>
	<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="event_maintitle">#<?php echo ucfirst($event_templates[0]['facebook_page_name']); ?>
		</div>
		<div class="panel panel-white">
			<?php foreach ($facebook_feeds as $key => $value) { ?>
			<div class="fb_feed">
				<div class="fb-feed-wrap">
					<div class="feed-title">
						<div class="col-md-12">
							<div class="profile-pic pull-left">
								<img src="https://graph.facebook.com/<?php echo $event_templates[0]['facebook_page_name']; ?>/picture?type=large" alt="" width="50">
							</div>
							<div class="profile-detail pull-left">
								<h3><a href=""><?php echo $value['from']['name']; ?></a></h3>
								<p><?php echo date('d F Y',strtotime($value['created_time'])) ." at ".date('H:i A',strtotime($value['created_time']));; ?></p>
							</div>
						</div>
					</div>
					<div class="feed-content">
						<div class="col-md-12">
							<p><?php echo $value['message']; ?></p>
							<div class="feed-image">
								<?php if(!empty($value['full_picture'])){ ?>
								<div class="fullwidth">
									<a class="colorbox_<?php echo $key; ?>" href="<?php echo $value['full_picture']; ?>">
										<img src="<?php echo $value['full_picture']; ?>" alt="" width="100%">
									</a>
								</div>
								<div class="clearfix"></div>
								<?php }
								if(array_key_exists('subattachments',$value['attachments']['data'][0]))
								{

									$medialoop=$value['attachments']['data'][0]['subattachments']['data'];
								}
								else
								{
									$medialoop=$value['attachments']['data'];
								} if(count($medialoop)>1){ ?>
								<div class="2-col">
									<?php $img=0; foreach ($medialoop as $akey => $avalue) { if($img<=1){ if($avalue['media']['image']['src']!=$value['full_picture']){ $img++; ?>
									<a class="colorbox_<?php echo $key; ?> <?php echo $img=='2' && (count($medialoop)-3)>0 ? 'last_images_show_count' : ''; ?>" href="<?php echo $avalue['media']['image']['src']; ?>">
										<img src="<?php echo $avalue['media']['image']['src']; ?>" alt="" width="50%">
										<?php if($img=='2' && (count($medialoop)-3)>0){ ?>
										<span class="more_images_count"><i class="fa fa-plus"></i> <?php echo count($medialoop)-3; ?></span>
										<?php } ?>
									</a>
									<?php } }else{ ?>
									<a class="colorbox_<?php echo $key; ?>" href="<?php echo $avalue['media']['image']['src']; ?>" style="display: none;">
										<img src="<?php echo $avalue['media']['image']['src']; ?>" alt="" width="50%">
									</a>
									<?php  } }?>
								</div>
								<?php } ?>
							</div>
							<?php if(!empty($value['name']) || !empty($value['description']) || !empty($value['caption'])){ ?>
							<div class="fedd-detail">
								<h3><a href="<?php echo $value['link'] ?>"><?php echo $value['name']; ?></a></h3>
								<p><?php echo $value['description']; ?></p>
								<span class="site-url">
								<a href="<?php echo $value['link'] ?>"><?php echo strtoupper($value['caption']); ?></a>
								</span>
							</div>
							<?php } ?>
							<div class="like-comment">
								<span class="like pull-left"><i class="fa fa-thumbs-up"></i><?php echo count($value['likes']['data']) ?> Likes</span>
								<span class="comment pull-right"><?php echo count($value['comments']['data']) ?> Comments</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php }else{?>
	<h2 style="text-align: center;">No Facebook Feed Found.</h2>
	<?php } ?>
</div>

<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function() {
	jQuery(document).on('mouseover', '.feed-image', function() {
		var my_class = $(this).find('a').attr('class');
		$("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%',photo:true});
	});
});
</script>
