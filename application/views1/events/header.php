<header class="topbar navbar navbar-inverse inner">
    <div class="container">
        <a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
            <i class="fa fa-th-large"></i> Menu
        </a>
    
        <div class="navigation-toggler hidden-sm hidden-xs col-md-1 col-lg-1 col-sm-1">
            <a href="#main-navbar" class="sb-toggle-left"><i class="fa fa-th-large"></i>Menu
            </a>
        </div>                            
        <div class="col-md-3 top-login-tab col-lg-3 col-sm-3">
            <a href="#"><i class="fa fa-user"></i>Login here</a>
        </div>
        <div class="top-search-tab">
            <form action="#" id="searchform" class="">
                <div class="input-group">
                    <input type="text" placeholder="Search" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-main-color btn-squared">
                            <i class="fa fa-search"></i>
                        </button> </span>
                </div>
            </form>
        </div>
    </div>
</header>      