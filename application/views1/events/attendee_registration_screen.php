<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/coustom.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-f.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newattendeeregistration.css?'.time(); ?>">
<div class="attend-register" <?php if(!empty($registration_screen[0]['background_image'])){ ?> style="background:transparent url('<?php echo base_url().'assets/user_files/'.$registration_screen[0]['background_image'] ?>')no-repeat scroll 0 0;" <?php } ?>>
	<div class="">
		<?php if(!empty($registration_screen[0]['banner_image'])){ ?>
		<div class="row banner-img">
            <img width="100%" src="<?php echo base_url().'assets/user_files/'.$registration_screen[0]['banner_image']; ?>" id="preview_model_image_tag">
        </div>
        <div class="row">
        	<h1 style="text-align: center;"><?php echo ucfirst($event_templates[0]['Event_name']); ?></h1>
        </div>
        <?php } if(!empty(trim(strip_tags($registration_screen[0]['screen_content'])))){ ?>
        <div class="row">
            <div id="preview_model_html_content">
                <?php echo $registration_screen[0]['screen_content']; ?>
            </div>
        </div>
        <?php } if($registration_screen[0]['show_app_link']=='1'){ ?>
        <div class="app-logo">
        	<?php if(!empty($registration_screen[0]['ios_link'])){ ?>
	        <div class="row ios-icn">
	            <a id="ios_link_in_models" target="_blank" href="<?php echo $registration_screen[0]['ios_link']; ?>">
	                <img src="<?php echo base_url().'assets/images/ios icon.png'; ?>">
	            </a>
	        </div>
	        <?php } if(!empty($registration_screen[0]['android_link'])){ ?>
	        <div class="row android-icn">
	            <a id="android_link_in_models" target="_blank" href="<?php echo $registration_screen[0]['android_link']; ?>">
	                <img src="<?php echo base_url().'assets/images/android icon.png'; ?>">
	            </a>
	        </div>
	        <?php } ?>
	        <div class="row web-icn">
	            <a target="_blank" href="<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain; ?>">
	                <img src="<?php echo base_url().'assets/images/web icon.png'; ?>">
	            </a>
	        </div>
        </div>
        <?php } ?>
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<div class="main-wrapper">
			<div class="main-container inner">
				<div class="main-content">
					<div class="container text-center">
						<?php if($this->session->flashdata('registration_screen_success')){ ?>
						<div class="errorHandler alert alert-success">
								<i class="fa fa-remove-sign"></i> <h2 style="text-align: center;"><?php echo $this->session->flashdata('registration_screen_success'); ?></h2>
							</div>
						<?php }else{ ?>
						<div class="col-sm-6 col-sm-offset-3 form-registration">
							<?php if($this->session->flashdata('registration_screen_error')){ ?>
							<div class="errorHandler alert alert-danger">
								<i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('registration_screen_error'); ?>
							</div>
							<?php } ?>
						<?php  if(!empty($registration_screen[0]['registration_forms'])){
							$customform=json_decode($registration_screen[0]['registration_forms'],true);
							$formid="form";
							$temp_form_id='_single';
							?>
							<form novalidate="novalidate" role="form" accept-charset="utf-8" method="post" id="<?php echo $formid.$temp_form_id ?>" class="form" action="<?php echo base_url().'Attendee_login/add_registration_screen_user/'.$acc_name.'/'.$Subdomain ?>">
								<input type="hidden" name="stripeToken" id="stripeToken" value="">
								<h2 style="text-align: center;"><?php echo $customform['title'];unset($customform['title']); ?></h2>
								<h3><?php echo $customform['description'];unset($customform['description']); ?></h3>
								<?php 
								$textboxname=array('first_name','last_name','email','company_name','title','attendee_type','choose_agenda');
								$staticfields=5;
								if(count($category_list) <= 1)
								{
									$staticfields=6;
								}

								foreach ($customform['fields'] as $key => $value) { 
									if($key <= $staticfields)
									{ 
										if($value['type']=='element-single-line-text'){ ?>
											<div class="form-group">
												<label class="control-label col-sm-12" for="<?php echo $textboxname[$key]; ?>">
													<?php echo $value['title'] ?> <span style="color: red">*</span>
												</label>
												<div class="col-sm-12">
													<input type="text" name="<?php echo $textboxname[$key]; ?>" id="<?php echo $textboxname[$key]; ?>" class="form-control required">
												</div>
											</div>
										<?php }else{ if($key=='5'){ if($registration_screen[0]['show_attendee_type']=='1'){ ?>
											<div class="form-group">
												<label class="control-label col-sm-12" for="<?php echo $textboxname[$key]; ?>">
													<?php echo $value['title'] ?> <span style="color: red">*</span>
												</label>
												<div class="radio col-sm-12">
													<label>
														<input type="radio" checked="checked" value="<?php echo $value['choices'][0]['title']; ?>" id="attendee_type_0" name="<?php echo $textboxname[$key]; ?>"><?php echo $value['choices'][0]['title']; ?>
													</label>
												</div>
												<div class="radio col-sm-12">
													<label>
														<input type="radio" value="<?php echo $value['choices'][1]['title']; ?>" id="attendee_type_1" name="<?php echo $textboxname[$key]; ?>"><?php echo $value['choices'][1]['title']; ?>
													</label>
												</div>
											</div>
										<?php } }else{ ?>
										<?php } } 	
										unset($customform['fields'][$key]);
									}
								}
								$json_data = json_encode($customform);
								$loader = new custom_formloader($json_data,'');
								$loader->render_form();
								unset($loader);
								?>
								<div class="form-group final-buuton">
									<div class="col-sm-4 pull-center">
                                        <button type="button" id="payment_btn" onclick="getstripepopup();" class="btn btn-green btn-block">
                                            Submit
                                        </button>
                                    </div>
								</div>
							</form>
						<?php } ?>
						</div>
						<?php } ?>
						<?php include 'validation.php'; ?>
						<script>
						    jQuery(document).ready(function() {
						        FormValidator<?php echo $temp_form_id; ?>.init();
						    });
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if($registration_screen[0]['show_footer']=='1'){ ?>
	<div class="attendee-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>This App was made on</p>
					<p><img src="<?php echo base_url(); ?>assets/images/aitl-logo1.png" alt="aitl-logo"/></p>
					<a href="https://www.allintheloop.com" target="_blank"><p>www.allintheloop.com</p></a>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<div class="modal fade" id="error_msg_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="display:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> 
        </div>
        <div class="modal-body" id="error_msg_body">

        </div>
        <div class="modal-footer">
          <button type="button" style="border:none;color:blue;" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if($registration_screen[0]['payment_type']=='2'){ ?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<?php $key=$registration_screen[0]['stripe_public_key']; } ?>
<script type="text/javascript">
<?php if($registration_screen[0]['payment_type']=='2'){ ?>
var handler = StripeCheckout.configure({
  key: '<?php echo $key; ?>',
  image: '<?php echo base_url(); ?>assets/images/favicon.png',
  locale: 'auto',
  token: function(token) {
    jQuery('#stripeToken').val(token.id);
    jQuery('#form_single').submit();
  }
});
function getstripepopup()
{
	if($('#form_single').valid())
	{
		var price="<?php echo $registration_screen[0]['stripe_payment_price']; ?>";
		handler.open({
		    name: 'Allintheloop.com',
		    description: 'All In The Loop Software',
		    amount: parseInt(price*100),
		    currency:'<?php echo $registration_screen[0]['stripe_payment_currency']; ?>',
		    billingAddress:"true"
		});
		e.preventDefault();
	}
}
<?php }else{ ?>
function getstripepopup()
{
	if($('#form_single').valid())
	{
		$('#form_single').submit();
	}
}	
<?php } ?>

$(document).ready(function(){
	$('body').addClass('sidebar-close');
});
</script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
.form-registration{
	float: none;
	display: inline-block;
	margin: 20px auto;
	text-align: left;
}
.form-registration input{
	margin-bottom: 15px;
}
.final-buuton button
{
	margin-bottom: 15px;
}
.main-container {
	background:transparent;
}
.main-container .col-sm-6.col-sm-offset-3{
	background: #FFF;
    font-size: 17px;
}
#parent_formtitle label{
	color: #000;
}
.attend-register .app-logo
{
	border: 0;
    box-shadow: none;
}
</style>