<?php 
if($acc_name == 'serve' && $Subdomain == 'serveKidsNow')
{
	$label_firstname = "Vorname";
	$label_lastname  = "Nachname";
	$label_email = "Emailadresse";
	$label_company = "Firma"; 
	$label_title = "Berufsbezeichnung";
	$label_next = "Nächster";
	$label_previous = "Bisherige";
	$label_submit = "Registrieren";
	$hide_reg_modal = '1';
}
elseif($acc_name == 'Grey' && $Subdomain == 'AmyBot5a14181891ce7')
{
	$label_firstname = "First Name";
	$label_lastname  = "Last Name";
	$label_email = "Email Address";
	$label_company = "Grey Office"; 
	$label_title = "Title";
	$label_next = "Save & Next";
	$label_previous = "Previous";
	$label_submit = "Submit";	
}
else
{
	$label_firstname = "First Name";
	$label_lastname  = "Last Name";
	$label_email = "Email Address";
	$label_company = "Company"; 
	$label_title = "Title";
	$label_next = "Save & Next";
	$label_previous = "Previous";
	$label_submit = "Submit";
}
if($acc_name == 'AmericasWarriorPartnership')
{
	$title_required = 'required'; 
	$comapany_required = 'required'; 
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Registration</title>
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/coustom.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-f.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
		<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newattendeeregistration.css?'.time(); ?>">
	</head>
	<body>
		<div class="attend-register" <?php if(!empty($registration_screen[0]['background_image'])){ ?> style="background:transparent url('<?php echo base_url().'assets/user_files/'.$registration_screen[0]['background_image'] ?>')no-repeat scroll 0 0;" <?php } ?>>
			<div class="">
				<?php if(!empty($registration_screen[0]['banner_image'])){ ?>
				<div class="row banner-img">
		            <img width="100%" src="<?php echo base_url().'assets/user_files/'.$registration_screen[0]['banner_image']; ?>" id="preview_model_image_tag">
		        </div>
		        <div class="row" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
		        	<h1 style="text-align: center;font-family: arial !important;"><?php echo ucfirst($event_templates[0]['Event_name']); ?></h1>
		        </div>
		        <?php } if(!empty(trim(strip_tags($registration_screen[0]['screen_content'])))){ ?>
		        <div class="row">
		            <div id="preview_model_html_content">
		                <?php echo html_entity_decode($registration_screen[0]['screen_content']); ?>
		            </div>
		        </div>
		        <?php } ?>
		        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
				<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
				<div class="main-wrapper">
					<div class="main-container inner">
						<div class="main-content">
							<div class="container text-center">
								<?php if($this->session->flashdata('registration_screen_success')){ ?>
								<div class="errorHandler alert alert-success">
									<i class="fa fa-remove-sign"></i> 
									<h2 style="text-align: center;">
										<?php echo $this->session->flashdata('registration_screen_success'); ?>
									</h2>
								</div>
								<?php }else{ ?>
								<div class="col-sm-6 col-sm-offset-3 form-registration">
									<?php if($this->session->flashdata('registration_screen_error')){ ?>
									<div class="errorHandler alert alert-danger">
										<i class="fa fa-remove-sign"></i> 
										<?php echo $this->session->flashdata('registration_screen_error'); ?>
									</div>
									<?php } ?>
									<div id="currency_symbol" style="display: none;">
										<?php if($registration_screen[0]['stripe_payment_currency']=='AED'){ ?>
										<i class="fa fa-aed">د.إ</i>
										<?php }else{ ?>
										<i class="fa fa-<?php echo strtolower($registration_screen[0]['stripe_payment_currency']); ?>"></i>
										<?php } ?>
									</div>
									<?php  $formid="form"; $temp_form_id='_single';?>
									<form novalidate="novalidate" role="form" accept-charset="utf-8" method="post" id="attendeeregistrationform" class="smart-wizard" action="<?php echo base_url().'Attendee_login/registraion_user_save_in_temp/'.$acc_name.'/'.$Subdomain ?>">
										<input type="hidden" name="update_user" value="<?=!empty($user_info)?$user_info['Id']:'0'?>" id="update_user">
										<input type="hidden" id="totalpricetopay" name="price" value="0">
										<input type="hidden" name="stripeToken" id="stripeToken" value="">
										<div id="attendeeregistrationwizard" class="swMain">
											<ul style="display: none;">
												<li>
			                                        <a class="wiozardnavigahref" href="#step-0">
			                                            <div class="stepNumber">
			                                                1
			                                            </div>
			                                            <span class="stepDesc"> 
			                                            	<small id="step_user_form">User Form</small>
			                                            </span>
			                                        </a>
			                                    </li>
												<?php foreach ($stages as $key => $stage) { ?>
			                                    <li>
			                                        <a class="wiozardnavigahref" href="#step-<?php echo $stage['id']; ?>">
			                                            <div class="stepNumber">
			                                                <?php echo $key+2; ?>
			                                            </div>
			                                            <span class="stepDesc"> 
			                                            	<small id="step_user_<?php echo $key+1; ?>"><?php echo ucfirst($stage['stage_name']); ?></small>
			                                            </span>
			                                        </a>
			                                    </li>
			                                    <?php } ?>
			                                    <li>
			                                        <a class="wiozardnavigahref" href="#step-111111">
			                                            <div class="stepNumber">
			                                                <?php echo count($stages)+2; ?>
			                                            </div>
			                                            <span class="stepDesc"> 
			                                            	<small id="step_111111">Payment</small>
			                                            </span>
			                                        </a>
			                                    </li>
			                                </ul>
			                                <div style="display: none;" class="progress progress-xs transparent-black no-radius active">
		                                    	<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar"></div>
		                                	</div> 
		                                	<div id="step-0"> 
		                                		<h2 style="text-align: center;">User Form</h2>
												<div class="form-group">
													<label class="control-label col-sm-12" for="First_Name">
														<?=$label_firstname?> <span style="color: red">*</span>
													</label>
													<div class="col-sm-12">
														<input type="text" name="First_Name" id="First_Name" class="form-control required" value="<?=$user_info['first_name']?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-12" for="Last_Name">
														<?=$label_lastname?> <span style="color: red">*</span>
													</label>
													<div class="col-sm-12">
														<input type="text" name="Last_Name" id="Last_Name" class="form-control required" value="<?=$user_info['last_name']?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-12" for="Email">
														<?=$label_email?> <span style="color: red">*</span>
													</label>
													<div class="col-sm-12">
														<input type="email" name="Email" id="Email" class="form-control required" value="<?=$user_info['email']?>" <?=!empty($user_info)?'readOnly=readOnly':''?>>
													</div>
												</div>
												<?php if($acc_name != 'MertelRGEventsGmbH') { ?>
												<div class="form-group">
													<label class="control-label col-sm-12" for="Company">
														<?=$label_company?>
														<?=empty($comapany_required)?'':'<span style="color: red">*</span>'?>
													</label>
													<div class="col-sm-12">
														<input type="text" name="Company" id="Company" class="form-control <?=$comapany_required?>" value="<?=$user_info['company_name']?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-12" for="Title">
														<?=$label_title?>
														<?=empty($title_required)?'':'<span style="color: red">*</span>'?>
													</label>
													<div class="col-sm-12">
														<input type="text" name="Title" id="Title" class="form-control <?=$title_required?>" value="<?=$user_info['title']?>">
													</div>
												</div>
												<?php } ?>
												<div class="form-group final-buuton">
													<div class="col-sm-4 pull-right">
					                                    <button type="button" data-thisstepid="<?php echo 0; ?>" data-redirectstepid="<?php echo $stages[0]['id']; ?>" data-skiplogicqid=""  class="btn btn-green next-step btn-block" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        <?=$label_next?>
					                                    </button>
					                                </div>
												</div>
											</div>
											<?php foreach ($stages as $key => $stage) { ?>
											<input type="hidden" name="stages_ids[]" value="<?php echo  $stage['id']; ?>">
											<script type="text/javascript">
												var date_session<?php echo "_".$stage['id']; ?> = new Array();
											</script>
											<?php 

											if($stage['skip_logic']=='1'){ 
											$redirectoption=json_decode($stage['redirectids'],true);
											foreach ($redirectoption as $key => $value) { ?>
											<input type="hidden" id="<?php echo preg_replace('/[^A-Za-z0-9\-]/','_',$key).'__'.$stage['skiplogic_questionid']; ?>" value="<?php echo $value; ?>">
											<?php } } ?>
											<div id="step-<?php echo  $stage['id']; ?>">
												<h2 style="text-align: center;"><?php echo ucfirst($stage['stage_name']); ?></h2>
												<div class="stage_description">
													<?php echo $stage['stage_html']; ?>
												</div>
												<div class="row">
													<?php foreach ($stage['stages_questions'] as $key => $question) { ?>
													<input type="hidden" name="question_ids[]" value="<?php echo $question['id']; ?>">
													<div class="" <?=($question['id'] == '508' || $question['id'] == '509')?'style=display:none;':''?>>
														<div class="list-group">
															<div class="list-group-item active">
																<h4 class="list-group-item-heading">
																	<?php echo $question['question']; ?>
																</h4>
															</div>
															<div class="list-group-item">
		            											<div class="form-group">
																	<div class="skins">
																		<div class="skin skin-line">
																			<dl class="clear">
																				<dd class="selected">
																					<div class="skin-section">
	<ul class="list">
		<?php 
		$requiredclass=$question['isrequired']=='1' ? 'required' : '';
		switch ($question['type']) {
			case 1: $option=json_decode($question['option']);
			foreach ($option as $keyo => $valuo) {?>
		<li class="radiocheckboxli">
			<input type="radio" id="question_option<?php echo $question['id'].preg_replace('/[^a-zA-Z0-9\']/', '_',$valuo); ?>"  name="question_option<?php echo $question['id']; ?>" value="<?php echo $valuo; ?>" class="<?php echo $requiredclass; ?> radio-callback" <?=($question['id'] == '508' || $question['id'] == '509')?'checked=checked':''?>>
				<label for="line-radio-<?php echo $question['id']; ?>">
				<?php echo $valuo; ?>
				</label>
				
		</li>
		<?php  } ?>
		<span for="question_option<?php echo $question['id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
		<?php break; case 2:
		$option=json_decode($question['option']);
		foreach ($option as $keyo => $valuo) {
		?>
		<li class="radiocheckboxli">
			<input type="checkbox" id="question_option<?php echo $question['id'].preg_replace('/[^a-zA-Z0-9\']/', '_',$valuo); ?>" name="question_option<?php echo $question['id']; ?>[]" value="<?php echo $valuo; ?>" class="<?php echo $requiredclass; ?> radio-callback">
				<label for="line-radio-<?php echo $question['id']; ?>">
				<?php echo $valuo; ?>
				</label>
				
		</li>
		<?php 
		} ?>
		<span for="question_option<?php echo $question['id']; ?>[]" class="help-block" style="display:none;">Please select an answer.</span>
		<?php break;
		case 3: 
		?>
		<li>
        	<input type="text" name="question_option<?php echo $question['id']; ?>" class="form-control <?php echo $requiredclass; ?>" value="<?php echo htmlspecialchars($user_info['user_ans'][$question['id']]); ?>">
        </li>
        <?php  unset($user_info['user_ans'][$question['id']]);?>
        <span for="question_option<?php echo $question['id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
        <?php 
		break;
		case 4: 
		?>
		<li class="radiocheckboxli">
			<input type="radio" id="question_option<?php echo $question['id'].preg_replace('/[^a-zA-Z0-9\']/', '_',$valuo); ?>" name="question_option<?php echo $question['id']; ?>" value="<?php echo $question['product_name']; ?>" data-productname="<?php echo ucfirst($question['product_name']); ?>" data-productprice="<?php echo $question['product_price']; ?>" data-discountcode="<?php echo $question['discountcode']; ?>" data-discountprice="<?php echo $question['discountprice']; ?>" class="<?php echo $requiredclass; ?> radio-callback question_product"
			onclick='<?php if ($user_info['Id']) { echo "return false;"; } else { echo "";}?>'>
				<label for="line-radio-<?php echo $question['id']; ?>">
				<?php echo ucfirst($question['product_name']); ?>
				</label>
				
		</li>
		<span for="question_option<?php echo $question['id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
		<?php break; case 5:  ?>
		<li>
			<input type="hidden" name="question_option<?php echo $question['id']; ?>" value="<?php echo $question['ticket_name']; ?>" class="question_ticket">
				<label style="font-size: 16px;line-height: 33px;color: #fff;background: #2489c5;width: 100%;">
					<?php echo ucfirst($question['ticket_name']); ?>
				</label>
		</li>
		<li>
			<?php $format="H:i";
			if($time_format[0]['format_time']=='0')
			{
			  $format="h:i A";
			}
			else
			{     
			  $format="H:i";
			}if(count($question['session_list']) > 0){ ?>
			
			<div class="panel-group accordion" id="accordion<?php echo $question['ticket_id']; ?>">
			<?php $couterkey=0; foreach ($question['session_list'] as $skey => $svalue) { 
				static $f1=0;if($f1==0) { $classname='';$f1++; } else { $classname='';$f1++;}
				static $f=0;if($f==0) { $classname1='in';$f++; } else { $classname1='in';$f++; }
				?>
			<div class="panel panel-white">
				<div class="panel-heading">
					<h5 class="panel-title agenda_title">
						<a class="accordion-toggle <?php echo $classname; ?>" data-toggle="collapse" data-parent="#accordion<?php echo $question['ticket_id']; ?>" href="#accordion<?php echo $couterkey.$question['ticket_id']; ?>"><i class="icon-arrow"></i>
						<?php echo date("m/d/Y",strtotime($skey)) ?>
						</a>
					</h5>
				</div>
				<div id="accordion<?php echo $couterkey.$question['ticket_id']; ?>" class="collapse <?php echo $classname1; ?>">
					<?php if($registration_screen[0]['one_agenda_each_date']):?>
					<script type="text/javascript">
                        // date_session.push("#accordion<?php echo $couterkey.$question['ticket_id']; ?>");
                        date_session<?php echo "_".$stage['id']; ?>.push("#accordion<?php echo $couterkey.$question['ticket_id']; ?>");
					</script>
					<?php endif; ?>
				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_<?php echo $couterkey.$question['ticket_id']; ?>">
				<thead>
					<tr>
						<th>Session Name</th><th>Time</th>
						<!-- <th>Speaker</th>
						<th>Places left</th> --><th>Save Session</th>
					</tr>
				</thead>
				<tbody>
					<?php $user_sesions = explode(',',$user_info['save_agenda_ids']); ?>
					<?php foreach ($svalue as $key => $session) { ?>
					<tr>
						<td><?php echo  ucfirst($session['Heading']); ?></td>
						<td><?php echo
						date($format,strtotime($session['Start_time']));?></td>
						<!-- .' - '.date($format,strtotime($session['End_time'])); <td><?php echo  ucfirst($session['custom_speaker_name']); ?></td>
						<td><?php if(!empty($session['Maximum_People'])){ echo $session['Place_left']<=0 ? 'NO Place Left' : $session['Place_left']; }else{
							echo "No Limit"; 
							}  ?></td> -->
						<td> 
						<label class="btn btn-<?=in_array($session['Id'],$user_sesions)?'success':'green';?> btn-block">
						<input type="checkbox" class='agenda_ticket_session' 
						value="<?php echo $session['Id']; ?>"
						name="savesession<?php echo $question['id']; ?>[]"
						onclick='<?php if ($user_info['Id'] && $event_templates[0]['Id'] != '1114') { echo "return false;"; } else {?> savesession(this,"<?php echo $question['id']; ?>","<?php echo !empty($session['Maximum_People']) ? $session['Place_left'] : 1; ?>");<?php } ?>' 
						style="display:none;" 
						<?=in_array($session['Id'],$user_sesions)?'checked=checked':'';?>><span>Save</span>
						</label>
						</td>
					</tr>
					<?php } ?>
				</tbody>
				</table>
				</div>
			</div>
			<?php $couterkey++; } $f1=0;$f=0; ?>
			</div>
			<?php }else{ ?>
			<h5 style="text-align:center;">No Session For This Ticket...</h5>
			<?php } ?>
		</li>
		<span id="question_option<?php echo $question['id']; ?>_error" for="question_option<?php echo $question['id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
		<?php break; 
		case 6: 
		?>
	<li class="">
		<input type="text" name="question_option<?php echo $question['id']; ?>" id="question_option<?php echo $question['id']; ?>" class="form-control <?php echo $requiredclass; ?>" value="<?=$user_info['user_ans'][$question['id']]?>">
		<label for="line-radio-<?php echo $question['id']; ?>">
		</label>
		<?php $opt_date=json_decode($question['option'],true);?>
		<script type="text/javascript">
			$(document).ready(function() {
			       $('#question_option<?php echo $question['id']; ?>').datepicker({
			            weekStart: 1,
			            startDate: '<?=$opt_date[0]?>',
			            endDate: '<?=$opt_date[1]?>',
			            autoclose: true,
			            });
			    });
		</script>
        <?php  unset($user_info['user_ans'][$question['id']]);?>

	</li>
	<span for="question_option<?php echo $question['id']; ?>" class="help-block" style="display:none;">Please select an answer.</span>
		<?php break;
		default: ?>
		<li></li>
		<?php
		break;  
	    } ?> 
	</ul>
																					</div>
																				</dd>
																			</dl>
																		</div>
																	</div>					
		            											</div>
		                									</div>
														</div>	
													</div>
													<?php } ?> 
												</div> 
												<div class="form-group final-buuton">
													<div class="col-sm-4 pull-left">
					                                    <button data-thisstepid="<?php echo $stage['id']; ?>" data-backstepredirectid="" type="button" class="btn btn-green back-step btn-block" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        <?=$label_previous?>
					                                    </button>
					                                </div>
					                                <div class="col-sm-4 pull-right">
					                                    <button data-thisstepid="<?php echo $stage['id']; ?>" <?php if($stage['skip_logic']!='1' && empty($stage['redirectid'])){ ?> data-redirectstepid="111111" <?php }else{ ?> data-redirectstepid="<?php echo $stage['redirectid']; ?>" <?php } ?> data-skiplogicqid="<?php echo $stage['skiplogic_questionid']; ?>"  type="button" class="btn btn-green next-step btn-block" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        <?=$label_next?>
					                                    </button>
					                                </div>
												</div>
											</div>
											<?php } ?>	
											<div id="step-111111"> 
												<?php if(!$user_info['Id']) {?>
												<?php if($registration_screen[0]['show_payment_screen']=='1'){ ?>
												<h2 style="text-align: center;">Payment Screen</h2>
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover table-full-width dataTable">
														<thead>
															<tr>
																<th>Item Name</th>
																<th>Item Price</th>
															</tr>
														</thead>
														<tbody id="payment_table_tbody">
															
														</tbody>
													</table>
												</div>
												<?php if($registration_screen[0]['show_pay_by_invoice']=='1'){ ?>
			                                	<div class="form-group">
			                                		<div class="col-sm-12">
					                                	<div class="checkbox">
															<label>
																<input type="checkbox" value="1" id="pay_by_invoice" name="pay_by_invoice"><strong>Pay By Invoice</strong>
															</label>
														</div>
													</div>
												</div>
			                                	<?php } }else{ ?>
			                                	<h2 style="text-align: center;">Form Submit</h2>
			                                	<?php } }?>
												<div class="form-group final-buuton">
													<div class="col-sm-4 pull-left">
					                                    <button data-thisstepid="111111" data-backstepredirectid="" type="button" class="btn btn-green back-step btn-block" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        <?=$label_previous?>
					                                    </button>
					                                </div>
					                                <?php if(!$user_info['Id']){ ?>
					                                <?php if($registration_screen[0]['show_payment_screen']=='1'){ ?>
					                                <div class="col-sm-4 pull-center">
					                                	<button type="button" data-toggle="modal" data-target="#add_coupon" id="add_coupon_code_btn" class="btn btn-green btn-block" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        Add Discount Code
					                                    </button>
					                                </div>
					                                <?php } }?>
													<div class="col-sm-4 pull-right">
					                                    <button data-thisstepid="111111" type="button" class="btn btn-green finish-step btn-block" onclick="processformsubmit();" style="background: <?php echo $registration_screen[0]['theme_color']; ?>">
					                                        <?=$label_submit?>
					                                    </button>
					                                </div>
												</div>
											</div>
										</div>	
									</form>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="add_coupon" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Discount Code</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="form-group">
								<label for="coupon_code" class="control-label col-sm-12"> Discount Code </label>
								<div class="col-sm-12">
									<input type="text" class="form-control" name="coupon_code" id="coupon_code">
									<span id="coupon_code_error" for="coupon_code" class="help-block"></span>
								</div>
							</div>
							<div class="col-sm-12" style="margin-top: 15px;">
								<button type="button" id="apply_couponcode_btn" class="btn btn-green btn-block">Apply Code</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="error_msg_model" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header" style="display:none;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4> 
		        </div>
		        <div class="modal-body" id="error_msg_body">

		        </div>
		        <div class="modal-footer" id="error-remove">
		          <button type="button" style="border:none;color:blue;" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<?php if($registration_screen[0]['payment_type']=='1'){ ?>
		<div class="modal fade" id="authorized_payment_box" data-backdrop="static" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header" style="display:none;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4> 
		        </div>
		        <div class="modal-body">
		        	<div class="row">
		        		<form method="post" action="https://test.authorize.net/payment/payment">
		        			<h2>Amount:<?php if($registration_screen[0]['stripe_payment_currency']=='AED'){ ?>
										<i class="fa fa-aed">د.إ</i>
										<?php }else{ ?>
										<i class="fa fa-<?php echo strtolower($registration_screen[0]['stripe_payment_currency']); ?>"></i>
										<?php } ?><?php echo $Price; ?></h2>
				        	<input type="hidden" name="token" value="<?php echo $Paymenttoken; ?>">
				        	<button type="submit" name="paymentbtn" class="btn btn-green">Continue To Pay</button>
				        </form>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<?php } ?>
	</body>
	<div id="reg-modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<center><h4 class="modal-title">Event Registration</h4></center>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<?php if($this->session->flashdata('reg_user_update_success')){ ?>
							<div class="errorHandler alert alert-success">
								<i class="fa fa-remove-sign"></i> 
								<h4 style="text-align: center;">
									<?php echo $this->session->flashdata('reg_user_update_success'); ?>
								</h4>
							</div>
						<?php } ?>
						<div class="row">
							<?php if($acc_name == 'Grey' && $Subdomain == 'AmyBot5a14181891ce7'):?>
							<div class="form-group">
								<center>
									<h2>Registration is now closed</h2>
								</center>
							
							<?php else:?>
							<div class="form-group">
								<?php if($acc_name == 'CandyBerman' && $Subdomain == 'CanBer5a2a5ca878165'): ?>
								<center>
									<button type="button" class="btn btn-green" id="con-reg">Register for this event</button>
								</center>
								<?php else: ?>
								<center>
									<button type="button" class="btn btn-green" id="new-reg">Register for this event</button>
									<button type="button" class="btn btn-green" id="con-reg">Continue with registration</button>
								</center>
								<?php endif; ?>
							</div>
							<?php endif;?>
						</div>
						<div class="row">
							<form action="<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain.'/attendee_registration_screen'?>" id="con-reg-form" style="display: none;" method="POST">
								<div class="form-group">
									<input type="text" name="user-email" placeholder="Enter your email" class="form-control">	
								</div>
								<div class="form-group">
									<input type="submit" name="submit" class="btn btn-green" value="Next">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js?<?php echo time(); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="<?php  echo base_url();?>assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard1.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/form-wizard_registration.js?<?php echo time(); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/css/datepicker.css">
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<?php if($registration_screen[0]['payment_type']=='2'){ ?>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<?php $key=$registration_screen[0]['stripe_public_key']; } ?>
	<script type="text/javascript">
		var acc_name =  "<?php echo $acc_name; ?>";
		var subdomain = "<?php echo $Subdomain; ?>";
		var p_step = "<?=$_SESSION['p_step']?>";
	$(window).on('load',function(){
    	<?php if($hide_reg_modal != '1'):?>
    	$('#reg-modal').modal({backdrop: 'static', keyboard: false});  
    	$('#reg-modal').modal('show');
    	<?php endif; ?>
	});
	$(document).ready(function(){
		$('#con-reg').click(function(e){    
	        $('#con-reg-form').fadeToggle('slow');
		});
		$('#new-reg').click(function(e){    
    	    $('#reg-modal').modal("hide");
		});
		$('.radiocheckboxli input').each(function(){
		    var self = $(this),
		    label = self.next(),
		    label_text = label.text();
		    label.remove();
		    self.iCheck({
				checkboxClass: 'icheckbox_line-blue',
				radioClass: 'iradio_line-blue',
				insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
	  	});

	  	<?php if(!empty($Paymenttoken)){ ?>
		    $('#authorized_payment_box').modal('show');
		<?php } ?>
		FormWizard.init();
		$('body').addClass('sidebar-close');
		//$('.wiozardnavigahref').unbind("click");
		$('#apply_couponcode_btn').click(function(){
			$('#apply_couponcode_btn').attr('disabled','disabled');
			var coupon_code=$.trim($('#coupon_code').val());
			if(coupon_code!='')
			{
				var allcoupon_code="<?php echo $discount_code['code']; ?>";
				$('#coupon_code_error').parent().parent().removeClass('has-error').addClass('has-success');
				$('#coupon_code_error').hide();
				if($('.'+coupon_code).length > 0)
				{
					var htm="";
                    if($('.question_product:checked').length > 0)
                    {
                        var discountprice=0;
                        $('.question_product:checked').each(function() {

                        	if(!$(this).data('discountprice'))
                        	{
                        		var tmp_discount = '0';;
                        	}
                        	else
                        	{
                        		
                        		var tmp_discount = $(this).data('discountprice');
                        	}
                            htm+="<tr class='"+$(this).data('discountcode')+"' data-discountcode='"+$(this).data('discountcode')+"' data-discountprice='"+tmp_discount+"'><td>"+$(this).data('productname')+"</td>";
                            htm+="<td>"+$('#currency_symbol').html()+" ";
                            if($(this).data('discountcode')==coupon_code)
                            {
                            	htm+="<strike>"+$(this).data('productprice')+"</strike>&nbsp;&nbsp;"+tmp_discount;
                            	discountprice=discountprice+parseInt(tmp_discount);
                            }
                            else
                            {
                            	htm+=$(this).data('productprice');
                            	discountprice=discountprice+parseInt($(this).data('productprice'));
                            }
                            htm+="</td></tr>";
                        });
                        if(!discountprice)
                        {
                        	discountprice = 0;
                        	htm+='<input type=hidden name=discount-per value=100>';
                        }
                   
                        htm+="<tr><td id='totaltexttd'>Total</td><td id='totalpricetd'>"+$('#currency_symbol').html()+" <strike>"+$('#totalpricetopay').val()+"</strike>&nbsp;&nbsp;"+discountprice+"</td></tr>";
                        console.log(htm);
                        $('#totalpricetopay').val(discountprice);
                    }
                    else
                    {
                        htm+="<tr><td colspan='2'>No Product Purchase</td></tr>";
                    }
                    $('#payment_table_tbody').html(htm);
					$('#add_coupon').modal('hide');
					$('#apply_couponcode_btn').removeAttr('disabled');
					$('#add_coupon_code_btn').hide();
				}
				else if(allcoupon_code==coupon_code)
				{
					var allcouponcode_per="<?php echo $discount_code['percentage']; ?>";
					if(allcouponcode_per!='' && allcouponcode_per!='0')
					{
						var totalprice=parseInt($('#totalpricetopay').val());
						var discountprice=(totalprice*parseInt(allcouponcode_per))/100;
						discountprice=totalprice-discountprice;
						$('#totalpricetd').html($('#currency_symbol').html()+" <strike>"+totalprice+"</strike>&nbsp;&nbsp;"+discountprice+"<input type=hidden name=discount-per value="+allcouponcode_per+">");
						$('#totalpricetopay').val(discountprice);
						$('#add_coupon').modal('hide');
						$('#apply_couponcode_btn').removeAttr('disabled');
						$('#add_coupon_code_btn').hide();
					}
				}
				else
				{
					$('#coupon_code_error').html('Discount Code is Not Valid.');
					$('#coupon_code_error').parent().parent().removeClass('has-success').addClass('has-error');
					$('#coupon_code_error').show();
					$('#apply_couponcode_btn').removeAttr('disabled');
				}
			}
			else
			{
				$('#coupon_code_error').html('This field is required.');
				$('#coupon_code_error').parent().parent().removeClass('has-success').addClass('has-error');
				$('#coupon_code_error').show();
				$('#apply_couponcode_btn').removeAttr('disabled');
			}
		});
	});
	function savesession(elem,qid,Placesleft)
	{	
		if($(elem).prop('checked')==true)
		{
			if(Placesleft > 0)
			{
	    		/*if($('#attendeeregistrationform').valid())
	    		{
	    			if($('input[name="question_option'+qid+'"]').prop('checked')==true)
	    			{*/
		    			var savesession=[];
		    			$('input[name="savesession'+qid+'[]"]:checked').each(function(){
		    				if($(this).val()!=$(elem).val())
		    				{
		    					savesession.push($(this).val());
		    				}
		    			});
		    			savesession.push($(elem).val());
		    			if(savesession.length > 0)
		    			{
			    			$.ajax({
				    			url:"<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain.'/save_tempusersession/' ?>"+$(elem).val(),
				    			data:'email='+$('#Email').val()+'&savesession='+savesession,
				    			type:'post',
				    			success:function(result)
				    			{
				    				var data=result.split('###');
				    				if($.trim(data[0])=="error")
				    				{
				    					$(elem).attr('checked',false);
				    					$('#error_msg_body').html('<b>'+data[1]+'</b>');
			    						$('#error_msg_model').modal('show');
				    				}
				    				else
				    				{
				    					$(elem).parent().attr('class','btn btn-success btn-block');
				    				}
				    			},
				    		});
		    			}
		    			else
		    			{
		    				$(elem).parent().attr('class','btn btn-success btn-block');
		    			}
	    			/*}
	    			else
	    			{
	    				$(elem).attr('checked',false);
	    				$('#question_option'+qid+'_error').show();
	    			}
	    		}
	    		else
		    	{
		    		$(elem).attr('checked',false);
		    		$('#error_msg_body').html('<b>Please Fill Up The Form.</b>');
	    			$('#error_msg_model').modal('show');
		    	}*/
			}
			else
	    	{
	    		$(elem).attr('checked',false);
	    		$('#error_msg_body').html('<b>This session is fully booked – Unfortunately you have not been booked onto this session</b>');
	    		$('#error_msg_model').modal('show');
	    	}
	    }
		else
		{
			$(elem).parent().attr('class','btn btn-green btn-block');
		}	
	}
	<?php if($registration_screen[0]['payment_type']=='2'){ ?>
	var handler = StripeCheckout.configure({
	  key: '<?php echo $key; ?>',
	  image: '<?php echo base_url(); ?>assets/images/favicon.png',
	  locale: 'auto',
	  token: function(token) {
	    jQuery('#stripeToken').val(token.id);
	    jQuery('#attendeeregistrationform').submit();
	  }
	});
	<?php } ?>
	function processformsubmit()
	{	
		
		/*var preformdata = $("#attendeeregistrationform :input");
		$.ajax({
          type: 'POST',
          url: "http://www.allintheloop.net/App/"+acc_name+"/"+subdomain+"/attendee_registration_screen?pre=1",
          data: preformdata,
          dataType: "json",
          success: function(resultData) { 
            // console.log(resultData);
           }
    	});*/
        
		if($('#attendeeregistrationform').valid())
	    {
	    	var paymentscreen="<?php echo $registration_screen[0]['show_payment_screen'] ?>";
	    	var update_user ="<?php echo $user_info['Id'] ?>";
	    	if(!update_user)
	    	{	    		
		    	if(paymentscreen=='1')
		    	{
			    	if($('#pay_by_invoice').prop('checked')==true)
			    	{
			    		$('#attendeeregistrationform').submit();
			    	}
			    	else
			    	{
			    		var payment_type="<?php echo $registration_screen[0]['payment_type']; ?>";
				    	if(payment_type=='2')
				    	{
							handler.open({
							    name: 'Allintheloop.com',
							    description: 'All In The Loop Software',
							    amount: parseInt($('#totalpricetopay').val())*100,
							    currency:'<?php echo $registration_screen[0]['stripe_payment_currency']; ?>',
							    billingAddress:"true"
							});
				    	}
				    	else
				    	{
				    		$('#attendeeregistrationform').submit();
				    	}
			    	}
			    }
			    else
			    {
			    	$('#attendeeregistrationform').submit();
			    }
		    }
		    else
		    {
		    	$('#attendeeregistrationform').submit();
		    }
	    }
	    else
    	{
    		$('#error_msg_body').html('<b>Please Fill Up The Form.</b>');
			$('#error_msg_model').modal('show');
    	}
	}
	<?php foreach ($user_info['user_ans'] as $key => $value)
		{ 
			if(is_array($value))
			{ 
				foreach($value as $v) {?>
				$('<?php echo '#question_option'.$key.preg_replace('/[^a-zA-Z0-9\']/', '_',$v); ?>').iCheck('check');	
			<?php }}else
			{

			?>
			$('<?php echo '#question_option'.$key.preg_replace('/[^a-zA-Z0-9\']/', '_',$value); ?>').iCheck('check');
	<?php }}
	foreach ($stages as $key => $stage) 
	{
		foreach ($stage['stages_questions'] as $key => $question)
		{
			if($question['type'] == '4')
			{
				if(array_key_exists($question['id'],$user_info['user_ans']))
				{?>
				 $("#question_option<?php echo $question['id'].preg_replace('/[^a-zA-Z0-9\']/', '_',$valuo); ?>").iCheck('check'); 
			 <?php }
			}
		}
	}
	?>
	
	</script>
	<style type="text/css">
		.form-registration{
			float: none;
			display: inline-block;
			margin: 20px auto;
			text-align: left;
		}
		.form-registration input{
			margin-bottom: 15px;
		}
		.final-buuton button
		{
			margin-bottom: 15px;
		}
		.main-container {
			background:transparent;
		}
		.main-container .col-sm-6.col-sm-offset-3{
			background: #FFF;
		    font-size: 17px;
		}
		#parent_formtitle label{
			color: #000;
		}
		.attend-register .app-logo
		{
			border: 0;
		    box-shadow: none;
		}
		.texta
		{
		  width: 100%;
		  min-width: 100%;
		  max-width: 100%;
		  height: 200px;
		  min-height: 200px;
		  max-height: 200px;
		}
		.skins .checked 
		{
		  background-color: #333;
		}
		.skins .icheckbox_line-blue:hover 
		{
		  background-color: #333;
		}
		.icheckbox_line-blue
		{
		  min-height: 45px;
		}
		.list-group-item
		{
		    clear: both;
		    border: none !important;
		}
		td{
			width: auto !important;
		}
		<?php if($acc_name == 'Grey' && $Subdomain == 'AmyBot5a14181891ce7'):; ?>
		.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
			background-color: #f5402a;
    		border-color: #f5402a;
		}
		<?php endif; ?>
	</style>
</html>