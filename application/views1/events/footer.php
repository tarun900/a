<footer class="">
    <div class="main-ads">
    <?php for ($i = 0; $i < count($advertisement_images); $i++) 
    { 
        $string_cms_id = $advertisement_images[0]['Cms_id'];
        $Cms_id =  explode(',', $string_cms_id); 

        $string_menu_id = $advertisement_images[0]['Menu_id'];
        $Menu_id =  explode(',', $string_menu_id); 

    if(in_array('17', $Menu_id)) {
    ?>
    <?php $image_array = json_decode($advertisement_images[$i]['F_images']);
    $h_url = $advertisement_images[0]['Header_link'];
    $f_url = $advertisement_images[0]['Footer_link'];
    ?>
    <?php foreach ($image_array as $key => $value) { ?>
        <div class="hdr-ads alert alert-success alert-dismissable">
           <button type="button" class="close-custom close" data-dismiss="alert" 
              aria-hidden="true">
              &times;
           </button>
           <a class="thumb-info" target="_blank" href="<?php echo $f_url; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery">
                <img style="height:75px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value; ?>" alt="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery">
            </a>
        </div>
    <?php } } } ?>
    </div>
</footer>      