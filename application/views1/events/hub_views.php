<?php $acc_name=$this->session->userdata('acc_name');?>
<div id="banner_content">
<div class="slider-banner">
     <div class="row">
          <?php for ($i = 0; $i < count($hubdesing); $i++) { ?>
          <?php $image_array = json_decode($hubdesing[$i]['hub_images']);  ?>
          <?php if (!empty($hubdesing)){ ?>
          <?php //if ($event_templates[$i]['Img_view'] == '0') { ?>
          <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
          <?php //} else { ?>
              <!--<img title="" style="border-radius:50px;" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">-->
          <?php //}
           } } ?>
     </div>
</div>
<?php $description= preg_replace('/[[:^print:]]/', '',trim(strip_tags($hubdesing[0]['hub_Description'])));
?>
<?php if($description!=""){ ?>
<div class="event-content panel-white" style="background:<?php for ($i = 0; $i < count($hubdesing); $i++) { echo $hubdesing[$i]['Background_color']; } ?>">
     <div class="event_description">
          <?php for ($i = 0; $i < count($hubdesing); $i++) { 
           ?>
          <p style="padding-left: 25px;">
            <?php
               $string = $hubdesing[$i]['hub_Description'];
               echo $string;
            ?>
          </p>
      <?php   }  ?>
     </div>
</div>
<?php }?> 
</div>
<div class="search_div">
  <div class="input-group search_box_user">
     <i class="fa fa-search search_user_icon"></i>
     <input type="text" class="form-control" id="search_user_content" placeholder="Search For Apps">
     <button id="cancel_search" class="close" type="button" style="display:none;">
        ×
      </button>
  </div>
</div>
<?php  if($hubdesing[0]['Background_img']!=""){ ?>
<div class="event-list hubdesinglistdiv" style="display:block;<?php for ($i = 0; $i < count($hubdesing); $i++) { $bgimg = json_decode($hubdesing[$i]['Background_img']); for($i=0;$i<count($bgimg);$i++) { ?>background-image:url('<?php echo base_url()."assets/user_files/".$bgimg[$i]; ?>') <?php } } ?>">
<?php } else{ ?>
<div class="event-list hubdesinglistdiv" style="background:<?php for ($i = 0; $i < count($hubdesing); $i++) { echo $hubdesing[$i]['Background_color']; } ?>">
<?php } ?>
     <div class="row">
        <div class="col-md-12">
               <?php
               if(count($hub_event_list)>0){
                foreach ($hub_event_list as $key => $value) {
                  $url=base_url().'App/'.$value['acc_name'].'/'.$value['Subdomain'];
                  $dimg = base_url()."assets/images/defult-menuimg.png";
                  $bimg=base_url().'assets/user_files/'.$value['hub_home_tab_image'];
                  if($value['img_view']=='1')
                  {
                    $classname="event-round-img";
                  }
                  else
                  {
                    $classname="event-img"; 
                  }
                ?>
                <div class="col-md-6 col-lg-4 col-sm-6 parentdivofsearch">
                  <a href="<?php echo $url; ?>"> 
                  <?php if($value['hub_home_tab_image']!=""){ ?>
                    <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="<?php echo $classname; ?>">
                         &nbsp;
                         <div class="event-tile">
                              <h3 class="user_container_name"><?php echo $value['title']; ?></h3>
                         </div>
                    </div>
                  <?php }else{ ?>
                    <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="<?php echo $classname; ?>">
                         &nbsp;
                         <div class="event-tile">
                              <h3 class="user_container_name"><?php echo $value['title']; ?></h3>
                         </div>
                    </div>
                  <?php } ?>
                  </a>
                  <div class="event-tile mobiletitle"> <h3 class="user_container_name"><?php echo $value['title']; ?></h3></div>
                </div>
                <?php } } ?>
      </div>
     </div>
</div>
<script>  
$(document).ready(function(){
  $('#search_user_content').focus(function(){
    $('#cancel_search').show();
    $('#banner_content').slideUp('slow');
  });
  $('#cancel_search').click(function(){
    $('#search_user_content').val('');
    $('#banner_content').slideDown('slow');
    $('#cancel_search').hide();
    jQuery(".parentdivofsearch a").each(function( index ) 
    {
     jQuery(this).show();
     jQuery(this).parent().show();
    });
  });
});
jQuery("#search_user_content").keyup(function()
{   
  jQuery(".parentdivofsearch a").each(function( index ) 
  {
    var str=jQuery(this).find('.user_container_name').html();
    if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
    {    

     var str1=jQuery(this).find('.user_container_name').html();
     if(str1!=undefined)
     {
       var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
       var content=jQuery("#search_user_content").val().toLowerCase();
       console.log(content);
       if(content!=null)
       { 
          var n = str1.indexOf(content);
          if(n!="-1")
          {
           jQuery(this).show();
           jQuery(this).parent().show();
          }
          else
          {
           jQuery(this).hide();
           jQuery(this).parent().hide();
          }
        }
      }
    }
  });
});
</script>
<style type="text/css">
   /* 14-6-2016 */
.search_div .search_box_user  { position:relative; }
.search_div .search_box_user button.close { position:absolute; top:16px; right:12px; z-index:999;}
.search_box_user #search_user_content { width:100%; padding-right:40px; }
/* 14-6-2016 end */
</style>