<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.min.css" />
<script type="text/javascript">
var owl;
$(document).ready(function() {
  owl = jQuery("#owlslider_banner_div").owlCarousel({
    items : 1,
    autoplay:true,
    autoHeight:true,
    navigation : false,
    center: true,
    loop:true
  }); 
});
</script>
<?php
if($event_templates[0]['Subdomain']=="youmatterday2016"){ $key = array_search(15, array_column($menu, 'id')); 
if(!empty($key)){
$surveyarray=$menu[$key]; 
unset($menu[$key]); } } if(!empty($advertisement_images)) { ?>
<div class="main-ads">
   <?php
   for ($i = 0; $i < count($advertisement_images); $i++)
   {
        $string_cms_id = $advertisement_images[0]['Cms_id'];
        $Cms_id = explode(',', $string_cms_id);

        $string_menu_id = $advertisement_images[0]['Menu_id'];
        $Menu_id = explode(',', $string_menu_id);

        if ($test = in_array('19', $Menu_id))
        {
             ?>
             <?php
             $image_array = json_decode($advertisement_images[$i]['H_images']);
             $h_url = $advertisement_images[0]['Header_link'];
             $f_url = $advertisement_images[0]['Footer_link'];
             ?>
        <?php foreach ($image_array as $key => $value)
        { ?>
                  <div class="hdr-ads alert alert-success alert-dismissable">
                       <button type="button" class="close-custom close" data-dismiss="alert" 
                               aria-hidden="true">
                            &times;
                       </button>
                       <a class="thumb-info" target="_blank" href="<?php echo $h_url; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'] . ' '); ?>Gallery">
                            <img style="height:75px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value; ?>" alt="<?php echo ucfirst($event_templates[0]['Event_name'] . ' '); ?>Gallery">
                       </a>
                  </div>
                  <?php }
             }
        } ?>
</div>
<?php } ?>
<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="slider-banner">
  <div class="row"> 
    <?php $image_array = json_decode($event_templates[0]['Images']);  ?>
    <div class="owl-carousel owl-theme loop" id="owlslider_banner_div">
      <?php if($event_templates[0]['Subdomain']!="gulfoodGulfood2017"){ ?> 
      <?php foreach ($image_array as $key => $value) { ?>
        <div>
        <img width="100%" height="500" title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>">
        </div>
      <?php } ?>
      <?php }else{  ?>
      <div>
        <img width="100%" src="<?php echo base_url().'assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height--FAT&OIL.JPG';?>">
      </div>
      <div>
        <img width="100%" src="<?php echo base_url().'assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BEVERAGES.JPG';?>">
      </div>
      <div>
        <img width="100%" src="<?php echo base_url().'assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BURGER.JPG';?>">
      </div>
      <div>
        <img width="100%" src="<?php echo base_url().'assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-DAIRY.JPG';?>">
      </div>
      <div>
        <img width="100%" src="<?php echo base_url().'assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-PULSES.JPG';?>">
      </div>
    <?php } ?>
    </div>
  </div>
</div>
<?php
if($event_templates[0]['Subdomain']=="MertelRGEventsGmbHErsteTalentBridge")
{
  $ikey=11;
  $sort_key_val=array('1'=>"10",'2'=>"11",'3'=>"7",'4'=>"12",'5'=>"16",'6'=>"2",'7'=>"15");
  foreach ($menu as $key => $value) {
    $kkey=array_search($value['id'], $sort_key_val);
    if(!empty($kkey)){
      $menu[$key]['sort_key']=$kkey;
    }
    else
    {
      $menu[$key]['sort_key']=$ikey;
      $ikey++; 
    }
  }
  function aasort(&$array, $key) {
      $sorter=array();
      $ret=array();
      reset($array);
      foreach ($array as $ii => $va) {
          $sorter[$ii]=$va[$key];
      }
      asort($sorter);
      foreach ($sorter as $ii => $va) {
          $ret[$ii]=$array[$ii];
      }
      $array=$ret;
  }
  aasort($menu,'sort_key');
  $ikey1=2;
  $sort_key_val1=array('1'=>"379",'2'=>"271");
  foreach ($cms_feture_menu as $key => $value) {
    $kkey=array_search($value->Id, $sort_key_val1);
    if(!empty($kkey)){
      $cms_feture_menu[$key]->sort_key=$kkey;
    }
    else
    {
      $cms_feture_menu[$key]->sort_key=$ikey1;
      $ikey1++; 
    }
  }
  function aasort1(&$array, $key) {
      $sorter=array();
      $ret=array();
      reset($array);
      foreach ($array as $ii => $va) {
          $sorter[$ii]=$va->$key;
      }
      asort($sorter);
      foreach ($sorter as $ii => $va) {
          $ret[$ii]=$array[$ii];
      }
      $array=$ret;
  }
  aasort1($cms_feture_menu,'sort_key');
}
if($event_templates[0]['Subdomain']=="thegrayreveal"){
  $description=$event_templates[0]['Description'];
 }else{
 $description= preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_templates[0]['Description'])));
  }
?>
<?php if($description!=""){

  $user = $this->session->userdata('current_user');
  if($user[0]->Role_id==4)
  {
    $extra=json_decode($custom[0]['extra_column'],true);
    foreach ($extra as $key => $value) {
      $keyword="{".str_replace(' ', '', $key)."}";
      if(stripos(strip_tags($event_templates[0]['Description'],$keyword)) !== false)
      {
        $event_templates[0]['Description']=str_ireplace($keyword, $value,$event_templates[0]['Description']);
      }
    }
  }
?>
<div class="event-content panel-white">
     <div class="event_description" <?php if($event_templates[0]['Subdomain']=="thegrayreveal"){ ?> style="padding: 0px !important;" <?php } ?>>
          <?php for ($i = 0; $i < count($event_templates); $i++) { 
           ?>
          <p <?php if($event_templates[0]['Subdomain']!="thegrayreveal"){ ?> style="padding-left: 25px;" <?php } ?>>
            <?php
               $string = $event_templates[0]['Description'];
               echo $string;
            ?>
          </p>
      <?php   }  ?>
     </div>
</div>
<?php } if(count($menu) > 0 || count($cms_feture_menu) > 0){ if($event_templates[0]['Background_img']!=""){ ?>
<div class="event-list" style="display:block;<?php for ($i = 0; $i < count($event_templates); $i++) { $bgimg = json_decode($event_templates[$i]['Background_img']); for($i=0;$i<count($bgimg);$i++) { ?>background-image:url('<?php echo base_url()."assets/user_files/".$bgimg[$i]; ?>') <?php } } ?>">
<?php } else{ ?>
<div class="event-list" <?php if($event_templates[0]['Id'] =="353") { ?> style="background-color: #357055;" <?php } else { ?> style="background-color: <?php echo $event_templates[$i]['Background_color']; ?>" <?php } ?> >
<?php } ?>

     <div class="row">
        <div class="col-md-12" <?php if($event_templates[0]['Id'] =="353") { ?> style="margin-top:40px;" <?php } ?>>
                    <?php
                    foreach ($menu as $key => $value)
                    {
                         
                         $img_view = $value['img_view'];
                         $img = $value['img'];
                         $url = base_url();
                         $bimg = "$url/assets/user_files/$img";
                         $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
                         if(in_array($value['id'],$iconset))
                         {
                            $dimg = $url."assets/css/images/icons/icon".$event_templates[0]['icon_set_type']."/".$value['id'].".png";
                         }
                         else
                         {
                            $dimg = $url."assets/images/EventApp_Default.jpg";
                         }
                         for ($i = 0; $i < count($event_templates); $i++)
                         {
                              if(!in_array($value['id'],array(22,23,24,25)))
                              {
                                   if($value['id']==12)
                                   {
                                        $url = base_url() .'Messages/'. $acc_name."/".$event_templates[$i]['Subdomain'].'/privatemsg';
                                   }
                                   else if($value['id']==13)
                                   {
                                       $url = base_url() .'Messages/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/publicmsg';
                                   }
                                   else if($value['id']==42)
                                   {
                                      $url = base_url() . 'fundraising' . '/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/home/fundraising_donation';
                                    }
                                   else
                                   {
                                        $url = base_url() . $value['pagetitle'] . '/' . '' . $acc_name."/".$event_templates[$i]['Subdomain'];
                                   }
                                   //echo $url;
                              }
                              else
                              {
                                $url = base_url() . 'fundraising' . '/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/home/get_auction_pr';
                               // $var_target="";
                                if($value['id']==22)
                                {
                                     //$var_target='target="_blank"';
                                     $url=$url.'/1';
                                }
                                else if($value['id']==23)
                                {
                                     //$var_target='target="_blank"';
                                     $url=$url.'/2';
                                }                                        
                                else if($value['id']==24)
                                {
                                     //$var_target='target="_blank"';
                                     $url=$url.'/3';
                                }
                                else if($value['id']==25)
                                {
                                     //$var_target='target="_blank"';
                                     $url=$url.'/4';
                                }
                              }
                           ?>
                              <?php if ($img_view == '0')
                              { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6">
                              <a href="<?php echo $url; ?>" style="background:<?php echo $value['Background_color']; ?>" <?php echo $var_target; ?>> 
               <?php if ($img != '')
               { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
               <?php }
               else
               { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
               <?php } ?>
                              </a>
                              <div class="event-tile mobiletitle"> <h3 <?php if($event_templates[0]['Id'] =="353") {?> style="color:#ffffff;" <?php } ?>><?php echo $value['menuname']; ?></h3></div>
                         </div>
          <?php }
          else
          { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6" style="">
                              <a href="<?php echo $url; ?>" style="background:<?php echo $value['Background_color']; ?>" <?php echo $var_target; ?>> 
                              <?php if ($img != '')
                              { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
                              <?php }
                              else
                              { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
                              <?php } ?>
                              </a>
                               <div class="event-tile mobiletitle"> <h3 <?php if($event_templates[0]['Id'] =="353") {?> style="color:#ffffff;" <?php } ?>><?php echo $value['menuname']; ?></h3></div>
                         </div>
                         <?php }
                    }
               } ?>
          
          
          
          <?php
                    if($event_templates[0]['Id']==353)
                    {
                      $default_backcolor="#357055";
                    }
                    else
                    {
                      $default_backcolor="#FFFFFF";
                    }

                    foreach ($cms_feture_menu as $key => $value)
                    {

                         $img_view = $value->img_view;
                         $img = $value->img;
                         $url = base_url();
                         $bimg = $url."assets/user_files/".$img;
                         $dimg = $url."assets/images/defult-menuimg.png";
                         for ($i = 0; $i < count($event_templates); $i++)
                         {
                              $url = base_url() . $value->menuurl;
                              ?>
                              <?php if ($img_view == '0')
                              { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6">
                              <a href="<?php echo $url; ?>" style="background:<?php echo !empty($value->Background_color) ? $value->Background_color : $default_backcolor; ?>"> 
               <?php if ($img != '')
               { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
               <?php }
               else
               { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 90%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
               <?php } ?>  
                              </a>
                              <div class="event-tile mobiletitle"> <h3 <?php if($event_templates[0]['Id'] =="353") {?> style="color:#ffffff;" <?php } ?>><?php echo $value->menuname;; ?></h3></div>
                         </div>

          <?php }
          else
          { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6" style="">
                              <a href="<?php echo $url; ?>" style="background:<?php echo !empty($value->Background_color) ? $value->Background_color : $default_backcolor; ?>"> 
                              <?php if ($img != '')
                              { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
                              <?php }
                              else
                              { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
                              <?php } ?>
                              </a>
                              <div class="event-tile mobiletitle"> <h3 <?php if($event_templates[0]['Id'] =="353") {?> style="color:#ffffff;" <?php } ?>><?php echo $value->menuname; ?></h3></div>
                         </div>

                         <?php }
                    }
               } if(count($surveyarray) > 0){ 
                $img_view = $surveyarray['img_view'];

               if($img_view == '0')
                  $classname="event-img";
               else
                $classname="event-round-img";

               $url = base_url();
               $linkurl=$url.$surveyarray['pagetitle'] . '/' . '' . $acc_name."/".$event_templates[0]['Subdomain'];
               if($surveyarray['img']=="")
                  $img = $url."assets/images/EventApp_Default.jpg";
                else
                 $img = "$url/assets/user_files/".$surveyarray['img'];
                   ?>
               <div class="col-md-6 col-lg-4 col-sm-6">
                  <a href="<?php echo $linkurl; ?>" style="background:<?php echo $event_templates[0]['Background_color']; ?>">
                    <div class="<?php echo $classname; ?>" style="background:url('<?php echo $img ?>') no-repeat center #ccc; background-size: 100%;">
                      <div class="event-tile">
                        <h3><?php echo $surveyarray['menuname']; ?></h3>
                      </div>
                    </div>
                  </a>
                  <div class="event-tile mobiletitle"> <h3 <?php if($event_templates[0]['Id'] =="353") {?> style="color:#ffffff;" <?php } ?>><?php echo $surveyarray['menuname']; ?></h3></div>
               </div>
               <?php }  ?>
      </div>    

     </div>
</div>
               <?php } if ($event_templates[0]['Event_time'] == '1')
               { ?>
     <div class="row" style="margin-left: 0px; padding-left: 0;">
          <?php if(!empty($event_templates[0]['Footer_background_color'])) { ?>
          <div class="event-timer" style="background:<?php echo $event_templates[0]['Footer_background_color']; ?>">
          <?php } else { ?>
          <div class="event-timer">
          <?php } ?>
               <div class="row">
                    <?php
                    for ($i = 0; $i < count($event_templates); $i++)
                    {
                         $event = $event_templates[$i]['Start_date'];
                         $end_event = $event_templates[$i]['End_date'];
                         $today = date("Y-m-d");

                         if ($event_templates[0]['Event_time_option'] == '1')
                         {?>
                              <?php if($event > date('Y-m-d H:i:s') && $end_event != date('Y-m-d')) { ?>

            <h2 id="auction_start_time_display" >Event Start in (<?php echo  $event; ?>) </h2>
            <?php } elseif($event < date('Y-m-d H:i:s') && $end_event != date('Y-m-d')) { ?>
                <h2 id="auction_start_time_display">Event end in </h2>
                        <?php } else{ echo "<h3>Event End</h3>";}


                          if($end_event > date('Y-m-d')) {
                        ?>
      
                        
                                  
                                  
                                    <ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>
                           <?php  } ?> 
                              <script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-2.0.3.js"></script>
                              <script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility-coming-soon.js"></script>
                              <?php if($event >= date('Y-m-d H:i:s')) { ?>

<script type="text/javascript">
//document.getElementById("bidnow").style.display="block";
        <?php
  //$auction = $details['product']["auctionStartDateTime"];
  $start_auction = $event;
  $start_auction_date = date('Y/m/d H:i:s', strtotime($start_auction));
  $end_auction = $end_event;
  $end_auction_date = date('Y/m/d H:i:s', strtotime($end_auction));
  ?>
  jQuery("#countdown").countdown({
            date : '<?php echo $start_auction_date; ?>',
            format: "on"
  },{
            date : '<?php echo $end_auction_date; ?>',
            format: "on"
  });                        
</script>

<?php } else { ?>

<script type="text/javascript">
  jQuery("#countdown").countdown({
  <?php
  $auction = $event;
  $end_auction = $end_event;
  $end_auction_date = date('Y/m/d H:i:s', strtotime($end_auction));
  ?>
  date : '<?php echo $end_auction_date; ?>',
         format: "on"
  });                     
</script>


<?php } 
          }
          elseif ($event_templates[0]['Event_time_option'] == '0')
          { 
            if($end_event > date('Y-m-d')) {
               echo '<h3>Event end in</h3>';
               echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
               ?>
                              <script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-2.0.3.js"></script>
                              <script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility-coming-soon.js"></script>
                              <script type="text/javascript">
                                           jQuery("#countdown").countdown({
               <?php
               $event = $event_templates[$i]['Start_date'];
               $end_event = $event_templates[$i]['End_date'];
               $end_event_date = date('Y/m/d H:i:s', strtotime($end_event));
               //$edate = date('Y/m/d H:i:s', strtotime($event));
               //$today = date('Y/m/d H:i:s');
               ?>

                                   date : '<?php echo $end_event_date; ?>',
                                           format: "on"
                                   });                        </script>
               <?php
             }
             else
             {
                echo "<h3>Event End</h3>";
             }

          }
        /*else
          {
               echo 'Event End';
               exit();
          }*/
     }
     ?>
               </div>
          </div>
     </div>
<?php }
else
{
   
} ?>
<?php if($event_templates[0]['Subdomain']=="thegrayreveal"){ ?>
<style type="text/css">
  @media (max-width: 960px) { .main-wrapper {     padding-top: 50px !important; } }
  .event_description > p{ margin:0;}
</style>
<?php } ?>
<style type="text/css">
.event-list a{
  display: inline-block;     
  height: 100%;     
  width: 100%;
}
</style>
<style type="text/css">
  .modal-dialog #auction_start_time_display
{
  text-align: center !important;
}

.modal-dialog #countdown
{
  text-align: center !important;
}
img[usemap] {
		border: none;
		height: auto;
		max-width: 100%;
	}
.event_description img { width:100%; max-width:100%; }
</style>