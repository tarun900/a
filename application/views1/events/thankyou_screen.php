<!DOCTYPE html>
<html>
	<head>
		<title>Thank You</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
	</head>
	<body>
		<div class="row">
			<?php echo !empty(strip_tags(trim($registration_screen[0]['thank_you_content']))) ? html_entity_decode($registration_screen[0]['thank_you_content']) : "<h2 style='text-align:center;'>Thank You For Registration...</h2>"; ?>
		</div>
	</body>
	<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('body').addClass('sidebar-close');
	});	
	</script>
</html>	