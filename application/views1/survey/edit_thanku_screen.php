<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <!--<div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Survey</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>-->

             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#survey_list" data-toggle="tab">
                            Update Thank you Screen
                        </a>
                    </li>
                    <!-- <li class="">
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View Event
                        </a>
                    </li> -->
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
              <div class="tab-content">
                <div class="tab-pane fade active in" id="survey_list">
                    <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    
                  <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Thank you content
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea name="thanku_data" id="content1" class="summernote form-control" placeholder=""><?php echo $survey_screens[0]['thanku_data'] ?></textarea>
                                    </div>
                                </div>
                  </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                <div id="viewport" class="iphone">
                        <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                </div>
                <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                <div id="viewport_images" class="iphone-l" style="display:none;">
                       <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

