<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
            <div class="panel-heading">
                <p>
                    <h4 class="panel-title">Survey <span class="text-bold">List</span></h4>
                </p>
                <span><?php $survey_answer[0]['Question']; ?></span>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Answer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($i=0; $i<count($survey_answer);$i++) { ?>
                            <tr>
                                <td><?php echo $i+1; ?></td>
                                <td><?php echo $survey_answer[$i]['Firstname'].' '.$survey_answer[$i]['Lastname']; ?></td>
                                <td width="70%;"><?php echo $survey_answer[$i]['panswer']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
