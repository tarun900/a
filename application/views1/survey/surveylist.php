<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
        	<div class="panel-body" style="padding: 0px;">
        		<a style="top: 7px;" class="btn btn-primary list_page_btn survey_btn" href="<?php echo base_url(); ?>Survey/question_index/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Survey</a>
                <a style="top: 7px;margin-right: 12%;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Survey/exprot_surveys/<?php echo $this->uri->segment(3); ?>"> Export all Surveys</a>
                <?php if ($this->session->flashdata('survey_success')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('survey_success'); ?>
                    </div>
                <?php } ?>
        		<div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#survey_list" data-toggle="tab">
                                Surveys
                            </a>
                        </li>
                        <li class="">
                            <a href="#hide_survey" data-toggle="tab">
                                Hide Survey
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin' && $is_news_event != '1'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>   
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
                    </ul>
                </div>
                <div class="tab-content">
                	<div class="tab-pane fade active in" id="survey_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            	<thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Surveys</th>
                                        <th>Number of Questions</th>
                                        <th>Hide Surveys</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach ($survey_category as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $i; $i++; ?></td>
                                        <td><a id="survey_a_tag_<?php echo $value['survey_id']; ?>" href="<?php echo base_url() ?>Survey/question_index/<?php echo $event_id.'/'.$value['survey_id']; ?>"><?php echo ucfirst($value['survey_name']); ?></a></td>
                                        <td><?php echo $value['total_question']; ?></td>
                                        <td>
                                            <input type="checkbox" <?php if($value['show_survey']=='0'){ ?> checked="checked" <?php } ?> onchange='hidesurveys("<?php echo $value['survey_id']; ?>");' name="show_survey" value="<?php echo $value['survey_id']; ?>" id="show_survey_<?php echo $value['survey_id'];?>" class="">
                                           <label for="show_survey_<?php echo $value['survey_id'];?>"></label>
                                        </td>
                                       <td>
                                            <a href="<?php echo base_url() ?>Survey/question_index/<?php echo $event_id.'/'.$value['survey_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_survey(<?php echo $value['survey_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <a href="javascript:;" onclick="" class="btn btn-xs btn-green tooltips showmodel" data-placement="top" data-original-title="Duplicate Survey" data-name="<?= $value['survey_name']; ?>" data-id="<?= $value['survey_id']; ?>"><i class="fa fa-files-o fa fa-white"></i></a>
                                            <a href="<?php echo base_url() ?>survey/downloadQR/<?php echo $event_id.'/'.$value['survey_id'].'/'.ucfirst($value['survey_name']); ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Download QR code"><i class="fa fa-download fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                    <div class="tab-pane fade" id="hide_survey">
				        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Survey/hide_survey/<?php echo $event_id; ?>" enctype="multipart/form-data">
                            	<div class="form-group">
	                                <div class="col-sm-12">
		                                <label style="margin-bottom: 15px;">
		                                	<input type="checkbox" value="1" name="hide_survey" <?php if($event['hide_survey']=="1") { ?> checked="checked" <?php } ?>>  <span>Hide Survey</span>
		                                </label>
									</div>		                                
		                            <div class="col-sm-12">
		                                <input style="width: 150px;" type="submit"  class="btn btn-primary col-sm-1" name="submit" value="Submit">

		                            </div>    
                                </div>
                            </form>
                        </div>
					</div>
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                       <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                    <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>                
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>    
                </div>
        	</div>
        </div>
    </div>
</div>
<div class="modal fade" id="copy-survey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="survey-name"></h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Survey/add_clone_survey/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="number" class="form-control" name="count" placeholder="Enter number of times to duplicate" min="1" max="50" required>
                <input type="hidden" name="survey_id" id="survey-id">
              </div>
                <button type="submit" class="btn btn-primary">Duplicate</button>
              </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">   
function delete_survey(id,Event_id)
{   
    <?php $test = $this->uri->segment(3); ?>
    if(confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url(); ?>Survey/delete_survey/"+<?php echo $test; ?>+"/"+id;
    }
}
</script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
function hidesurveys(sid)
{
    $.ajax({
        url:"<?php echo base_url(); ?>Survey/change_hide_surveys_status/<?php echo $this->uri->segment(3); ?>/"+sid,
        success:function(result)
        {
          if($.trim(result)>0)
          {
            var shortCutFunction ='success';
            if($("#show_survey_"+sid).is(':checked'))
            {
              var title = 'Hide Successfully';
              var msg = $('#survey_a_tag_'+sid).html()+' Hide Successfully.';
            }
            else
            {
              var title = 'Show Successfully';
              var msg = $('#survey_a_tag_'+sid).html()+' Show Successfully.'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
          }
        }
    });
}
</script>    
<script>
  $(document).ready(function(){
       $(".showmodel").click(function()
       {
            $("#survey-id").val($(this).data('id'));
            $("#survey-name").html($(this).data('name'));
            $('#copy-survey').modal('show');
       });
});
</script>