<?php  $user = $this->session->userdata('current_user');?>
<div class="toolbar row inner_toolbar">
<?php $links=$this->uri->segment(1).'/'.$this->uri->segment(2); if($links=="event/eventhomepage" && $user[0]->role_type=='0'){ ?>
<div class="row">
    <div class="col-sm-4">
        <h3>Progress Bar: Click on a Stage for an Overview of your progress</h3>
    </div>
    <div class="col-sm-8">
        <div id="wizard" class="swMain">
            <ul>
                <li>
                    <a id="design_progress_link_inner_toolbar" data-toggle="modal" data-target="#design_point_models" href="javascript:void(0);">
                        <div class="stepNumber">
                            1
                        </div>
                        <span class="stepDesc"> 
                            <small>Design</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a id="content_progress_link_inner_toolbar" data-toggle="modal" data-target="#content_point_models" href="javascript:void(0);">
                        <div class="stepNumber">
                            2
                        </div>
                        <span class="stepDesc">
                            <small>Content</small> 
                        </span>
                    </a>
                </li>
                <li>
                    <a id="check_progress_link_inner_toolbar" data-toggle="modal" data-target="#check_point_models" href="javascript:void(0);">
                        <div class="stepNumber">
                            3
                        </div>
                        <span class="stepDesc">
                            <small>Check</small> 
                        </span>
                    </a>
                </li>
                <li>
                    <a id="submit_progress_link_inner_toolbar" <?php if(count($iseventfreetrial) > 0){ ?> class="sidebar_lunch_your_app_btn" <?php } ?>>
                        <div class="stepNumber">
                            4
                        </div>
                        <span class="stepDesc">
                            <small>Submit</small> 
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div> 
</div> 
<?php }else{ ?>
    <div class="col-sm-12 hidden-xs">
        <div class="col-sm-7">
            <div class="page-header">
             <?php if($pagetitle=="Role Management") { ?><h3 style="font-size: 24px;line-height: 30px;"><?php echo $pagetitle; ?> <small><?php echo $smalltitle; ?></small></h3> <?php } else { ?>
                <h3 style="font-size: 24px;line-height: 30px;"><?php echo $pagetitle; ?> <small><?php echo $smalltitle; ?></small></h3>
                <?php } ?>
            </div>
        </div>
         <?php 
         $controller_url = base_url().''.$this->router->fetch_class().'/'.'index';
         $index_base_url = base_url().''.'Event/index';

         //echo $controller_url.'<br/>';
         //echo $index_base_url; exit();

         $action_url = $this->router->fetch_method();

         
         $edit_base_url = base_url().''.'Event/edit';

        if ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' || $this->uri->segment(2)=='edit_speaker' || $this->uri->segment(2) == 'email_templates' || $this->uri->segment(2) == 'fbloginsetting' || $this->uri->segment(2) == 'agenda_index' || $user[0]->role_type=='1') { 
            $nos=$this->uri->segment(1).'/'.$this->uri->segment(2);
        if($this->uri->segment(1)!='exhibitor_user' && $this->uri->segment(1)!='speaker_Messages' && $this->uri->segment(1)!='exhibitor_portal'){
            if($nos!="event/add" && $nos!="event/edit" && $nos!="twitter_feed_admin/edit" && $nos!='agenda/edit' && $is_news_event != '1') { ?>
		<div id="device_preview_section">
			<div class="col-sm-5 device_preview">
				<h3 style="font-size: 22px;line-height: 25px;">LIVE DEVICE PREVIEW</h3>
				<ul id="screen-options" style="float: right;">
					<li onClick="jQuery('#view_events1').click();" id="iphone" class="active">iPhone</li>
					<li onClick="jQuery('#view_events1').click();" id="iphone-l">iPhone landscape</li>
					<li onClick="jQuery('#view_events1').click();" id="ipad" class="">iPad</li>
					<li onClick="jQuery('#view_events1').click();" id="ipad-l" class="">iPad landscape</li>
					<li onClick="jQuery('#view_events1').click();" id="desktop" class="">Desktop</li>
				</ul>
			</div>
			<p class="click">Click on an icon to preview your app in device shown</p>
		</div>
        <?php } } } ?>
		
		<?php if($this->uri->segment(1) == 'role_management' && $this->uri->segment(2) == 'add'
				|| $this->uri->segment(1) == 'user' && $this->uri->segment(2) == 'add'
				){ ?>
		<style type="text/css">
		#device_preview_section { display:none; }
		</style>
		<?php } ?>
    </div>
    <div class="col-sm-6 col-xs-12">
        <a href="#" class="back-subviews">
            <i class="fa fa-chevron-left"></i> BACK
        </a>
        <a href="#" class="close-subviews">
            <i class="fa fa-times"></i> CLOSE
        </a>
    </div>
     <?php } ?>
</div>
<script>
    function searchtext()
    {
       $("#searchform").submit();
    }
    
    function clearsearch()
    {
        window.location.href = "<?php echo base_url(); ?>Event/clearsearch";
    }
</script>

