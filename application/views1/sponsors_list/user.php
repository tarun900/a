<?php
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
?>

<div class="agenda_content agenda-user-content">
     <div class="row row-box"> 
          <div class="user_msg_profile clearfix box-effect">
               <?php
               $logo_images_array = json_decode($sponsors_data[0]['company_logo']); 
               if (!empty($logo_images_array)) { ?>
               <img align="left" style="height:82px;width:82px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $logo_images_array[0]; ?>" alt="" />
               <?php } else { ?>
			   <?php $color = sprintf("#%06x",rand(0,16777215)); ?>
				<span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($sponsors_data[0]["Company_name"], 0, 1)); ?></span>
               <!--<img align="left" src="<?php echo base_url() ?>assets/images/EventApp_Default.jpg" alt="" />-->
               <?php } ?>
               <div class="desc">
                    <h4><?php echo ucfirst($sponsors_data[0]["Company_name"]); ?></h4><br>
               </div>
          </div>
            <div class="agenda_content">
            <div class="panel panel-white box-effect">
           <div class="demo1 col-md-12 col-lg-12 col-sm-12">
            <div class="tabbable-bot-inner">
                <?php foreach ($sponsors_data as $key => $value) { ?>
                  <?php } ?>
          <div class="row">
          <div class="col-md-12">
            <div class="page-title-heading">
              <h2><?php echo $sponsors_data[0]['Company_name']; ?></h2>
            </div>
            <div class="tabbable-bot-inner-banner">
              <div id="slider-box" class="carousel slide" data-ride="carousel">
                                <?php  $images_array = json_decode($value['Images']);  
                                        if(!empty($images_array)) { ?>
                <ol class="carousel-indicators">
                                    <li data-target="#slider-box" data-slide-to="0" class="active"></li>
                                    <?php for($j=1; $j<count($images_array); $j++) { ?>
                  <li data-target="#slider-box" data-slide-to="<?php echo $j; ?>"></li>
                                    <?php } ?>
                </ol>
                                <?php } ?>

                                    <?php  $images_array = json_decode($value['Images']);  
                                        if(!empty($images_array)) { ?>
                                       <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img style="height:500px;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>">
                                            </div>
                                            <?php for($i=1; $i<count($images_array); $i++) { ?>
                                                <div class="item">
                                                    <img style="height:500px;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                </div>
            <div class="address-tabbable-bot">
              <p></p>
            </div>
            <?php if(!empty($sponsors_data)) { ?>
              <div class="social-net-blocks">
                <ul>
                  <?php if (!empty($sponsors_data[0]['facebook_url'])){ ?>
                  <li><a href="<?php echo $sponsors_data[0]['facebook_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/fb-social-icon.png" alt="facebook"></a></li>
                  <?php } if (!empty($sponsors_data[0]['twitter_url'])){ ?>
                  <li><a href="<?php echo $sponsors_data[0]['twitter_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/twtr-social-icon.png" alt="twitter"></a></li>
                  <?php } if (!empty($sponsors_data[0]['linkedin_url'])){ ?>
                  <li><a href="<?php echo $sponsors_data[0]['linkedin_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/linkedin-social-icon.png" alt="linedin"></a></li>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>

            <div class="desc-blocks">
            <?php 
                $user = $this->session->userdata('current_user');
                if($user[0]->Role_id==4)
                {
                  $extra=json_decode($custom[0]['extra_column'],true);
                  foreach ($extra as $key => $value) {
                    $keyword="{".str_replace(' ', '', $key)."}";
                    if(stripos(strip_tags($sponsors_data[0]['Description']),$keyword) !== false)
                    {
                      $sponsors_data[0]['Description']=str_ireplace($keyword, $value,$sponsors_data[0]['Description']);
                    }
                  }
                }
              ?>
              <p><?php echo html_entity_decode($sponsors_data[0]['Description']); ?></p>
            </div>

            <div class="contact-detail">
              <?php if($sponsors_data[0]['website_url'] !='') { ?>
                <p>Website : <a target="_blank" href="<?php echo $sponsors_data[0]['website_url']; ?>"><?php echo $sponsors_data[0]['website_url']; ?></a></p>
              <?php } ?>
            </div>
          </div>
          </div>
                </div>
            </div>
        </div>
    </div>
</div>
