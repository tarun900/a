<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
$favorites_user=array_filter(array_column($my_favorites,'module_id'));
$active_menu=array_filter(explode(",",$event_templates[0]['checkbox_values']));
?>
<div class="row margin-left-10">
    <?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Sponsors_list/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('43', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id']; ?>")'> 
       <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } }  ?>
 </div>
</div>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newsponsors.css?'.time(); ?>">
<?php if(count($sponsors_list) > 0){ ?>
<div class="agenda_content">
    <div>
        <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
    </div>
    <?php foreach ($sponsors_data as $key => $value) { ?>
    <div class="gold-sponsor"> 
    <?php if(count($value['sponsors'])>0){ ?>
    <h3 style="background:<?php echo $value['type_color'] ?>;color:#fff;"><?php echo ucfirst($value['type_name']); ?></h3>
    <?php } ?>
        <div class="gold1"> 
            <ul class="activities columns activities-new">
            <?php foreach ($value['sponsors'] as $skey => $svalue) { ?>
                <li>
                    <a class="activity" href="<?php echo base_url(); ?>Sponsors_list/<?php echo $acc_name.'/'.$Subdomain;?>/View/<?php echo $svalue['Id']; ?>" class="activity" id="a<?php echo $svalue['Id']; ?>">
                        <div class="desc">
                            <p class="logo-img">
                                <?php $clogo=json_decode($svalue['company_logo']); if(!empty($clogo[0])){ ?>
                                <img src="<?php echo base_url().'assets/user_files/'.$clogo[0]; ?>"/>
                                <?php }else{ $color = sprintf("#%06x",rand(0,16777215)); ?>
                                <span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($svalue['Sponsors_name'], 0, 1)); ?></span>
                                <?php } ?>
                            </p>
                            <h4 class="user_container_name"><?php echo ucfirst($svalue['Sponsors_name']); ?></h4>
                        </div>
                        <div class="time"><i class="fa fa-chevron-right"></i></div>
                    </a>
                     <?php if(!empty($user[0]->Id) && in_array('49',$active_menu) && $user[0]->Rid=='4'){ ?> 
                    <div class="myfavorites_content_div">
                      <a onclick='my_favorites("<?php echo $svalue['Id']; ?>");'>
                        <img width="5%" src="<?php echo base_url().'assets/images/favorites_not_selected.png'; ?>" class="pull-right" id="favorites_not_selected_<?php echo $svalue['Id']; ?>" <?php if(in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                        <img width="5%" src="<?php echo base_url().'assets/images/favorites_selected.png'; ?>" class="pull-right" id="favorites_selected_<?php echo $svalue['Id']; ?>" <?php if(!in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                      </a>
                      <a href="javascript:void(0);" style="display: none;">
                        <img width="5%" src="<?php echo base_url().'assets/images/ajax-loader.gif'; ?>" id="loder_icone_<?php echo $svalue['Id']; ?>">
                      </a>
                    </div>
                    <?php } ?>
                </li>
            <?php } ?>    
            </ul>
        </div> 
    </div> 
    <?php } ?>
</div>
<?php }else{ ?>
<div class="tab-content">
  <span>No Sponsors Available for this App.</span>
</div>
<?php } ?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>
     jQuery("#search_user_content").keyup(function()
     {    
          jQuery(".activities-new li").each(function( index ) 
          {
              var str=jQuery(this).find('.user_container_name').html();
              var strkey=jQuery(this).find('.Exbitor_keyword').html();
              if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='' || jQuery.trim(strkey)!=undefined || jQuery.trim(strkey.toLowerCase())!='')
              {    
                   var str1=jQuery(this).find('.user_container_name').html();
                   if(str1!=undefined)
                   {
                         var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                         var content=jQuery("#search_user_content").val().toLowerCase();
                         console.log(content);
                         if(content!=null)
                         {
                              jQuery(this).parent().parent().parent().find(".date_separator").each(function(index) 
                              {
                                   var content1=jQuery(this).text().toLowerCase();
                                   var n1 = str1.indexOf(content1);
                              });
                              var n = str1.indexOf(content);
                              if(n=="-1")
                              {
                                jQuery(this).css('display','none');
                              }
                              else
                              {
                                jQuery(this).css('display','block');
                              }
                         }
                    }
              }
          });
     });
function my_favorites(uid)
{
  $('#loder_icone_'+uid).parent().show();
  $('#favorites_not_selected_'+uid).parent().hide();
  $.ajax({
    url:"<?php echo base_url().'My_Favorites/'.$acc_name.'/'.$Subdomain.'/save_myfavorites'; ?>",
    type:'post',
    data:'module_id='+uid+"&module_type=43",
    success:function(result){
      var data=result.split('###');
      if($.trim(data[0])=="success")
      {
        var name=$('#a'+uid).find('.user_container_name').text();
        $('#loder_icone_'+uid).parent().hide();
        $('#favorites_not_selected_'+uid).parent().show();
        $('#favorites_not_selected_'+uid).parent().removeAttr('disabled');
        if(data[1]=='0')
        {
          $('#favorites_not_selected_'+uid).show();
          $('#favorites_selected_'+uid).hide();
          var msg = name+" Removed From My Favorites";
          var title = 'Removed From My Favorites';
        }
        else
        {
          $('#favorites_not_selected_'+uid).hide();
          $('#favorites_selected_'+uid).show();
          var msg = name+" Added Into Your My Favorites";
          var title = 'Added To My Favorites';
        }
      }
      else
      {
        $('#loder_icone_'+uid).parent().parent().remove();
        var msg = "Somthing Went To Wrong Please Reload the Page.";
        var title = 'Somthing Went To Wrong';
      }
      var shortCutFunction ='info';
      var $showDuration = 1000;
      var $hideDuration = 10000;
      var $timeOut = 10000;
      var $extendedTimeOut = 10000;
      var $showEasing = 'swing';
      var $hideEasing = 'linear';
      var $showMethod = 'fadeIn';
      var $hideMethod = 'fadeOut';
      toastr.options = {
        closeButton: true,
        debug: false,
        positionClass:'toast-bottom-right',
        onclick: null
      };
      toastr.options.showDuration = $showDuration;
      toastr.options.hideDuration = $hideDuration;
      toastr.options.timeOut = $timeOut;                        
      toastr.options.extendedTimeOut = $extendedTimeOut;
      toastr.options.showEasing = $showEasing;
      toastr.options.hideEasing = $hideEasing;
      toastr.options.showMethod = $showMethod;
      toastr.options.hideMethod = $hideMethod;
      toastr[shortCutFunction](msg, title);
    }
  });
}                 
</script>