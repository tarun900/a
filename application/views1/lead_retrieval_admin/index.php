<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <?php if ($this->session->flashdata('setting_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('setting_data'); ?> 
                    </div>
                <?php } ?>
                 <?php if ($this->session->flashdata('upload_error_data')) { ?>
                     <div class="errorHandler alert alert-danger no-display" style="display: block;">
                         <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('upload_error_data'); ?> 
                     </div>
                 <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#setting_new" data-toggle="tab">
                                Settings
                            </a>
                        </li>
                        <li>
                            <a href="#users" data-toggle="tab">
                                Users
                            </a>
                        </li>
                        <li>
                            <a href="#survey_list" data-toggle="tab">
                                 Questions List
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name And Icon
                            </a>
                        </li>
                        <?php } ?>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- <div class="tab-pane fade active in" id="setting"> 
                        <h4>Switch on Lead Retrieval to allow Exhibitors to scan Attendee badges and collect their registration data. </h4>
                        <h4>When a lead is scanned a form will show up on the Exhibitor’s device which they can personalize to ask extra questions and collect more information from the Attendee.</h4>
                        <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>lead_retrieval_admin/save_lead_setting/<?php echo $event_id; ?>">
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="1" class="square-orange" name="allow_export_lead_data" <?php if($event['allow_export_lead_data']=='1') { ?> checked="checked" <?php } ?>>
                                    <span style="font-weight: bold;"> Allow Exhibitors to export lead data </span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="1" class="square-orange" name="allow_custom_survey_lead" <?php if($event['allow_custom_survey_lead']=='1') { ?> checked="checked" <?php } ?>>
                                    <span style="font-weight: bold;"> Allow Exhibitors to customize add a survey for leads </span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                Save Settings
                            </button>
                        </form>
                    </div> -->
                    <div class="tab-pane fade active in" id="setting_new">
                        <h4>
                            Create roles to define permissions for Users who can use the Lead Retrieval feature.
                        </h4>
                        <a style="top: -57px;" class="btn btn-primary list_page_btn add_role"><i class="fa fa-plus"></i> Add Role</a>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Role</th>
                                        <th>Ask Survey Questions</th>
                                        <th>Add extra Survey Questions</th>
                                        <th>Delete</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lead_role as $key => $value){ ?>
                                        <tr>
                                            <td><?=++$i;?></td>
                                            <td><a class="showmodel" data-toggle="modal" data-id="<?= $value['Id']; ?>" data-rname="<?= $value['Name']; ?>" data-addsurvey="<?=$value['add_survey']?>"  data-asksurvey="<?=$value['ask_survey']?>" id="announce" ><?=$value['Name']?></a></td>
                                            <td>
                                            <span class="badge" style="background-color:<?=($value['ask_survey'])? '#468847' : '#b94a48'?>"><?=($value['ask_survey'])? 'ON' : 'OFF'?></span>
                                            </td>
                                            <td>
                                            <span class="badge" style="background-color:<?=($value['add_survey'])? '#468847' : '#b94a48'?>"><?=($value['add_survey'])? 'ON' : 'OFF'?></span>
                                            </td>
                                           <td>
                                                <a href="javascript:;" onclick="delete_role(<?=$value['Id']?>,<?=$value['event_id']?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove">
                                                <i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                            <td>
                                            <a class="btn btn-green showmodel" data-toggle="modal" data-id="<?= $value['Id']; ?>" data-rname="<?= $value['Name']; ?>" data-addsurvey="<?=$value['add_survey']?>"  data-asksurvey="<?=$value['ask_survey']?>" id="announce" >Edit</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="users">
                       <div class="form-group">
                        
                            <div class="form-group col-sm-12">
                                <div class="col-sm-6" style="margin-left: -46px;">
                                    <div class="col-sm-6">
                                        <label>Short instructions</label>
                                        <small>Download Template Example of Add multiple Lead Users.</small>
                                    </div>
                                    <div class="col-sm-6" style="padding:0px;">
                                        <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Lead_retrieval_admin/download_template_csv/<?php echo $this->uri->segment(3); ?>">Download Template</a>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Lead_retrieval_admin/uploadUsers/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
                                    <div class="col-sm-6" style="padding:0px;">
                                    <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                        <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" name="csv_files" id="csv_files" class="required">
                                          </span><br/><small>Browse CSV file only</small>
                                      </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn btn-theme_green btn-block" type="submit">
                                        Add
                                        </button>
                                    </div>
                                    </form>
                                    <div class="col-sm-6" style="margin-top: 2px;">
                                        <button class="btn btn-theme_green btn-block add-lead-user" data-toggle="modal">
                                        Add Manually
                                        </button>
                                    </div>
                                </div>
                            </div>
                       </div>
                       <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        <th>Role</th>
                                        <th>Max. Rep</th>
                                        <th>Change Password Link</th>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lead_users as $key => $value){ ?>
                                        <tr>
                                            <td><?=++$j;?></td>
                                            <td><?=$value['Firstname'].' '.$value['Lastname']?></td>
                                            <td><?=$value['Email']?></td>
                                            <td><?=$value['Title']?></td>
                                            <td><?=$value['Company_name']?></td>
                                            <td><?=$value['Name']?></td>
                                            <td><?=$value['no_of_reps']?></td>
                                            <td><a class="btn btn-green tooltips" onclick="copyToClipboard('<?=$value['lead_user_password_link']?>')" data-placement="top" data-original-title="Click to Copy URL"><i class="fa fa-copy"></i></a></td>
                                            <td>
                                                <a href="javascript:;" onclick="delete_lead_user(<?=$value['User_id']?>,<?=$value['Event_id']?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove">
                                                <i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                            <td>
                                            <a class="btn btn-green edit-lead-user" data-toggle="modal"
                                            data-uid="<?= $value['User_id']; ?>"
                                            data-fname="<?= $value['Firstname']; ?>"
                                            data-lname="<?= $value['Lastname']; ?>"
                                            data-cname="<?= $value['Company_name']; ?>"
                                            data-tname="<?= $value['Title']; ?>"
                                            data-max_rep="<?= $value['no_of_reps']; ?>"
                                            data-rid="<?= $value['Id']; ?>">Edit</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="survey_list">
                        <div class="row">
                            <a style="margin-top: 0.5%;margin-bottom:0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Exibitor_survey/add/<?php echo @$event_id; ?>"><i class="fa fa-plus"></i> Add new Question</a>
                            <a style="margin-top: 0.5%;margin-right: 5px;margin-bottom:0.5%;<?php echo ($user->Role_id == '3') ? 'display: none' : ''; ?>" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Exibitor_survey/reorder_question/<?php echo @$event_id; ?>"> Reorder Questions</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Question</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($question_list as $key => $question) { ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Exibitor_survey/edit/<?php echo $event_id.'/'.$question['q_id']; ?>"><?php echo $question['Question']; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Exibitor_survey/edit/<?php echo $event_id.'/'.$question['q_id']; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" style="margin-top: 0;" onclick="delete_survey(<?php echo $question['q_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index/<?php echo $event_id ?>" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                   <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-role" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Update Lead Role</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_retrieval_admin/update/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="role-name" name="role" placeholder="Enter Role Name">
                <input type="hidden" name="id" id="role-id">
              </div>
              <div class="form-group">
                <label>Allow Users to ask the Standard Survey Questions set by the CMS Administrator</label>
                <input type="checkbox" name="ask_survey" id="ask_survey">
              </div>
              <div class="form-group">
                <label>Allow Users to add extra Survey Questions</label>
                <input type="checkbox" name="add_survey" id="add_survey">
              </div>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-role" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Lead Role</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_retrieval_admin/store/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="role-name-new" name="role" placeholder="Enter Role Name">
              </div>
              <div class="form-group">
                <label>Allow Users to ask the Standard Survey Questions set by the CMS Administrator</label>
                <input type="checkbox" name="ask_survey">
              </div>
              <div class="form-group">
                <label>Allow Users to add extra Survey Questions</label>
                <input type="checkbox" name="add_survey">
              </div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-lead-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Update Lead User</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_retrieval_admin/update_lead_user/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="first-name" name="first-name" placeholder="Enter First Name">
                <input type="hidden" name="rid" id="old-role-id">
                <input type="hidden" name="uid" id="user-id">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="last-name" name="last-name" placeholder="Enter Last Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="title-name" name="title" placeholder="Enter Title">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="company-name" name="company-name" placeholder="Enter Company Name">
              </div>
             <!--  <div class="form-group">
                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter New Password if you want to change password" value="">
              </div> -->
              <div class="form-group">
                <input type="text" class="form-control" id="max_rep" name="max_rep" placeholder="Enter no of Max Rep." onkeypress="return isNumber(event)">
              </div>
              <div class="form-group">
                  <select name="new-role-id" class="form-control" id="new-role-id">
                        <?php foreach ($lead_role as $key => $value): ?>
                            <option value="<?=$value['Id']?>"><?=$value['Name']?></option>
                        <?php endforeach;?>
                  </select>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
              </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-lead-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Add Lead User</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_retrieval_admin/add_lead_user/<?php echo $this->uri->segment(3); ?>" id="form2">
              <div class="form-group">
                <input type="text" class="form-control" name="firstname" placeholder="Enter First Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="lastname" placeholder="Enter Last Name">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Enter Email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="title" placeholder="Enter Title">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="company_name" placeholder="Enter Company Name">
              </div>
              <!-- <div class="form-group">
                <input type="password" class="form-control" name="update_password" placeholder="Enter Password" value="">
              </div> -->
              <div class="form-group">
                <input type="text" class="form-control" name="no_of_reps" placeholder="Enter no of Max Rep." onkeypress="return isNumber(event)">
              </div>
              <div class="form-group">
                  <select name="role_name" class="form-control">
                        <?php foreach ($lead_role as $key => $value): ?>
                            <option value="<?=$value['Name']?>"><?=$value['Name']?></option>
                        <?php endforeach;?>
                  </select>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Add</button>
              </div>
              </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-overflow in" id="show_adn_copy_app_link_model1" role="dialog" tabindex="-1" style="display: none; margin-top: 0px;" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Link Copied</h4>
      </div>
      <div class="modal-body">
        <p id="show_app_link_p_tag" style="word-wrap: break-word;text-align: center;">The password link for this user has been copied to your clipboard, please send this to your Lead Retrieval User so they can set or reset their password securely and gain access to the platform</p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>

<script>
    $(document).ready(function(){
       $(".showmodel").click(function()
       {    
            $("#role-name").val($(this).data('rname'));
            $("#role-id").val($(this).data('id'));
            $('#edit-role').modal('show');
           if($(this).data('addsurvey') == '1')
           {
                $("#add_survey"). prop("checked", true);
           }
           if($(this).data('asksurvey') == '1')
           {
                $("#ask_survey"). prop("checked", true);
           }
       });
       $(".edit-lead-user").click(function()
       {    
            $("#first-name").val($(this).data('fname'));
            $("#last-name").val($(this).data('lname'));
            $("#company-name").val($(this).data('cname'));
            $("#title-name").val($(this).data('tname'));
            $("#max_rep").val($(this).data('max_rep'));
            $("#old-role-id").val($(this).data('rid'));
            $('#new-role-id option[value='+$(this).data('rid')+']').attr('selected','selected');
            $("#user-id").val($(this).data('uid'));
            $('#edit-lead-user').modal('show');

       });
       $(".add-lead-user").click(function()
       {    
            $('#add-lead-user').modal('show');
       });
       $(".add_role").click(function()
       {
            $('#add-role').modal('show');
       });
    });
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
    }
  function delete_role(id,event_id)
    {   
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Lead_retrieval_admin/delete/"+event_id+"/"+id;
        }
    }
  function delete_lead_user(id,event_id)
    {   
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Lead_retrieval_admin/delete_lead_user/"+event_id+"/"+id;
        }
    }
function delete_survey(id)
{   
    if(confirm("Are you sure to delete this?"))
    {
        window.location.href="<?php echo base_url().'Exibitor_survey/delete_question/'.$event_id.'/'; ?>"+id;
    }
}
function copyToClipboard(text) {

  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value",text);

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);
  $('#show_adn_copy_app_link_model1').modal('show');
}
</script>
<!-- end: PAGE CONTENT-->