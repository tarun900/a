<?php $user = $this->session->userdata('current_user');
$user_permissions=explode(",",$presentations[0]['user_permissions']);
if(in_array($user[0]->Id,$user_permissions)){
	$height=148;
	$width=200;
}
else
{
	$height=53;
	$width=0;
}
?>
<?php if($presentations[0]['Auto_slide_status'] =='1') { ?>
  <script type="text/javascript">
  var my_autoplay = true;
  </script>
<?php } else { ?>
  <script type="text/javascript">
  var my_autoplay = false;
  </script>
<?php } ?>
<?php
$current_img = 0;
if(!empty($presentations[0]['Image_current']))
{
    $array1 = json_decode($presentations[0]['Images']);
    $current_img = array_search($presentations[0]['Image_current'],$array1);  
}
if(!empty($presetation_tool[0]['lock_image']) && !in_array($user[0]->Id,$user_permissions))
{
  $array1 = json_decode($presentations[0]['Imagesno']);
  $current_img = array_search($presetation_tool[0]['lock_image'],$array1);
  $presentations[0]['Auto_slide_status']='0';
  ?>
  <script type="text/javascript">
  my_autoplay = false;
  var lock_swipe=false;
  </script>
  <?php 
}
else
{
  ?>
  <script type="text/javascript">
    var lock_swipe=true;
  </script>
  <?php
}
if(!empty($presetation_tool[0]['push_images']) && !in_array($user[0]->Id,$user_permissions))
{
	$array1 = json_decode($presentations[0]['Imagesno']);
	$current_img = array_search($presetation_tool[0]['push_images'],$array1);
}
if(!empty($presetation_tool[0]['push_result']) && !in_array($user[0]->Id,$user_permissions))
{
	$current_img = 0;
}
?>
<script>
var current_img = <?php echo $current_img; ?>;
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/slider-pro-master/dist/js/jquery.sliderPro.min.js"></script>
<script type="text/javascript">
var showpushresult;
	$( document ).ready(function( $ ) {
		$('body').attr('class',"sidebar-close");
		var minheight="<?php echo $height ?>";
		var bodyheight = $(window).height()-minheight;
		var bodywidth = $(window).width();
		$( '#presentation_slider_pro_slider_show' ).sliderPro({
			width: bodywidth,
			height: bodyheight,
			fade: true,
			shuffle: false,
			arrows: false,
			buttons: false,
			fullScreen: true,
			visibleSize:'90%',
			forceSize: 'fullWindow',
			smallSize: 500,
			mediumSize: 1000,
			largeSize: 3000,
      		fadeFullScreen:true,
			waitForLayers: true,
			thumbnailWidth: 110,
			thumbnailHeight: 110,
			thumbnailPointer: false,
			autoplay: my_autoplay,
			touchSwipe:lock_swipe,
    		thumbnailTouchSwipe:lock_swipe,
    		keyboard:lock_swipe,
			autoScaleLayers: false,
			startSlide: current_img,
		    gotoSlide: function( event ) {
		       $('#silde_no_text_box').val(event.index);
		       $('.sp-selected').panzoom({
		            $zoomIn: $(".zoom-in"),
		            $zoomOut: $(".zoom-out"),
		            disablePan: true,
		        });
		        $('.sp-slides').css("overflow","visible");
		        $('.sp-slide').each( function( index, listItem ) {
		            $(this).css("overflow","visible");
		        });
		    }
		});
		$('.list input').each(function(){
		    var self = $(this),
		      label = self.next(),
		      label_text = label.text();
		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-blue',
		      radioClass: 'iradio_line-blue',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		    });
  		});
	});
	$('.sp-full-screen-button').live("click",function(){
		if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
	   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
	    if (document.documentElement.requestFullScreen) {  
	      document.documentElement.requestFullScreen();  
	    } else if (document.documentElement.mozRequestFullScreen) {  
	      document.documentElement.mozRequestFullScreen();  
	    } else if (document.documentElement.webkitRequestFullScreen) {  
	      document.documentElement.mozRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
	    }  
	  } else {  
	    if (document.cancelFullScreen) {  
	      document.cancelFullScreen();  
	    } else if (document.mozCancelFullScreen) {  
	      document.mozCancelFullScreen();  
	    } else if (document.webkitCancelFullScreen) {  
	      document.webkitCancelFullScreen();  
	    }  
	  }
	});
	$('#link_prev').click(function(){
    	$('#presentation_slider_pro_slider_show').sliderPro('previousSlide');
  	});
	$('#link_next').click(function(){
    	$('#presentation_slider_pro_slider_show').sliderPro('nextSlide');
  	});
  	$('#link_view_result').click(function(){
  		$('#push_barresult_btn').hide();
	    var key=$('#silde_no_text_box').val();
	    var test='<?php echo $presentations[0]['Imagesno']; ?>';
	    var imgarr=jQuery.parseJSON(test);
	    if($.isNumeric(imgarr[key]))
	    {
	      $('#push_result_btn').show();
	      if($.trim($('#view_result_id_textbox').val())==imgarr[key] && $('#chart_result_type').val()=='0')
	      {
	        $('#push_result_btn').val('Remove Push');
	      }
	      else
	      {
	        $('#push_result_btn').val('Push Result'); 
	      }
	    }
	    $('#chart_show_iframe').attr("src","<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/showchartdata/'; ?>"+imgarr[key]); 
  	});
  	$('.link_view_bar_chart_result').click(function(){
  		$('#push_result_btn').hide();
	    var key=$('#silde_no_text_box').val();
	    var test='<?php echo $presentations[0]['Imagesno']; ?>';
	    var imgarr=jQuery.parseJSON(test);
	    if($.isNumeric(imgarr[key]))
	    {
	      $('#push_barresult_btn').show();
	      if($.trim($('#view_result_id_textbox').val())==imgarr[key] && $('#chart_result_type').val()=='1')
	      {
	        $('#push_barresult_btn').val('Remove Push');
	      }
	      else
	      {
	        $('#push_barresult_btn').val('Push Result'); 
	      }
	    }
	    else
	    {
	    	$('#push_barresult_btn').hide();
	    }
	    $('#chart_show_iframe').attr("src","<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/showbarchartdata/'; ?>"+imgarr[key]); 
  	});
  	$('#push_barresult_btn').click(function(){
  		if(showpushresult==1)
  		{
			var key=$('#silde_no_text_box').val();
			var test='<?php echo $presentations[0]['Imagesno']; ?>';
			var imgarr=jQuery.parseJSON(test);
			var datademo="";
			var testval='';
			if($.trim($('#push_barresult_btn').val())=="Remove Push")
			{
				datademo="poll_id=";
			    testval="";
			}
			else
			{
			    datademo="poll_id="+imgarr[key];
			    testval=imgarr[key];
			}
			$.ajax({
			  url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_result_in_presentation/'.$presentations[0]['Id']; ?>",
			  type:'POST',
			  data:datademo+'&chart_type=1',
			  success:function(resule)
			  {
			    if($.trim($('#push_barresult_btn').val())=="Remove Push")
			    {
			      $('#push_barresult_btn').val('Push Result');
			    }
			    else
			    {
			      $('#push_barresult_btn').val('Remove Push');
			    }
			    $('#view_result_id_textbox').val(testval);
			    $('#preview_mode_btn_link').show();
			    $('#live_mode_btn_link').hide();
			    $('#chart_result_type').val('1');
			  }
			});
		}
	});
  	$('#push_result_btn').click(function(){
  		if(showpushresult==1)
  		{
			var key=$('#silde_no_text_box').val();
			var test='<?php echo $presentations[0]['Imagesno']; ?>';
			var imgarr=jQuery.parseJSON(test);
			var datademo="";
			var testval='';
			if($.trim($('#push_result_btn').val())=="Remove Push")
			{
				datademo="poll_id=";
			    testval="";
			}
			else
			{
			    datademo="poll_id="+imgarr[key];
			    testval=imgarr[key];
			}
			$.ajax({
			  url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_result_in_presentation/'.$presentations[0]['Id']; ?>",
			  type:'POST',
			  data:datademo+'&chart_type=0',
			  success:function(resule)
			  {
			    if($.trim($('#push_result_btn').val())=="Remove Push")
			    {
			      $('#push_result_btn').val('Push Result');
			    }
			    else
			    {
			      $('#push_result_btn').val('Remove Push');
			    }
			    $('#view_result_id_textbox').val(testval);
			    $('#preview_mode_btn_link').show();
			    $('#live_mode_btn_link').hide();
			    $('#chart_result_type').val('0');
			  }
			});
		}
	});
	$('#link_question').click(function(){
    	$('#question_show_iframe').attr("src","<?php echo base_url().'speaker_Messages/fornt_index/'.$event_id; ?>"); 
  	});
  	$('#link_push').click(function(){
	    $.ajax({
	      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
	      type:'POST',
	      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
	      success:function(resule){
	        var intsli=parseInt($('#silde_no_text_box').val());
	        test(intsli);
	        $('#preview_mode_btn_link').hide();
	        $('#live_mode_btn_link').show();
	        $('#view_result_id_textbox').val('');
	      }
	    });
  	});
  	$('#preview_mode_btn_link').click(function(){
	    $.ajax({
	      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
	      type:'POST',
	      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
	      success:function(resule){
	        var intsli=parseInt($('#silde_no_text_box').val());
	        test(intsli);
	        $('#preview_mode_btn_link').hide();
	        $('#live_mode_btn_link').show();
	      }
	    });
  	});
  	$('#live_mode_btn_link').click(function(){
	    $.ajax({
	      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
	      success:function(resule){
	        var intsli=parseInt($('#silde_no_text_box').val());
	        test(intsli);
	        $('#preview_mode_btn_link').show();
	        $('#live_mode_btn_link').hide();
	      }
	    });
  	});
  	$('#link_unlocked').click(function(){
	    $.ajax({
	      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_lock_images_presentation/'.$presentations[0]['Id']; ?>",
	      type:'POST',
	      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
	      success:function(resule){
	        $('#link_unlocked').hide();
	        $('#link_locked').show();
	      }
	    });
	});
	$('#link_locked').click(function(){
	    $.ajax({
	      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_lock_images_presentation/'.$presentations[0]['Id']; ?>",
	      success:function(resule){
	        $('#link_unlocked').show();
	        $('#link_locked').hide();
	      }
	    });
	});
	function saveansbyquestion(sid)
	{
	  var id="<?php echo 'question_option'; ?>"+sid;
	  if($('input[name='+id+']:checked').length > 0)
	  {
	    var ans=$('input[name='+id+']:checked').val();
	  }
	  if($.trim(ans)!=""){
	  $.ajax({
	    url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_survey_ans_presentation'; ?>",
	    type:'POST',
	    data:"sid="+sid+"&ans="+ans,
	    success:function(resule){
	    	var data=resule.split('###');
	    	if($.trim(data[0])=="success"){
	      		$('#submit_ans_btn_'+sid).hide();
	      		$('#msg_question_'+sid).html('Thank you for your answer');
	  		}
	  		else
	  		{
	  			alert(data[1]);
	  		}
	    }
	  });
	  }
	  else
	  {
	    alert('Please Select One Answer');
	  }
	}
</script>	
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/dist/jquery.panzoom.js"></script>
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script type="text/javascript">
//google.load("visualization", "1", {packages:["corechart"]});
jQuery(document).ready(function() 
{	
  setInterval(function () {
    jQuery.ajax({
        type: "POST",
        data: '',
        url:  "<?php echo base_url(); ?>Presentation/<?php echo $this->uri->segment(2).'/'.$event_templates[0]['Subdomain']; ?>/Get_slider_images/<?php echo $this->uri->segment(5); ?>",
        dataType: "json",
        success: function(data)
        {
        	$('#presentation_slider_pro_slider_show').data('sliderPro').destroy();
        	$('.sp-slides').html('');
            $('.my-sp-thumbnails').html('');
            for(var aa in data['images']) {
              $('.sp-slides').append($("#siderTemplate").tmpl(data['images'][aa]));
              $('.my-sp-thumbnails').append($("#siderTemplate1").tmpl(data['thum'][aa]));
            }
	        $('.list input').each(function(){
			    var self = $(this),
			    label = self.next(),
			    label_text = label.text();
			    label.remove();
			    self.iCheck({
			      checkboxClass: 'icheckbox_line-blue',
			      radioClass: 'iradio_line-blue',
			      insert: '<div class="icheck_line-icon"></div>' + label_text
			    });
	  		});
        	var CImg=window.current_img_index;
        	var intsli=parseInt($('#silde_no_text_box').val());
            <?php if($presentations[0]['Auto_slide_status'] =='1') { ?>
            	my_autoplay = true;
            <?php } else { ?>
            	my_autoplay = false;
            <?php } ?>
            if(data.c_push_images)
            {
            	my_autoplay = false;
                var cimge=data.c_push_images;
                cimg=cimge.split("###");
                intsli=parseInt(cimg[1]);
            }
            if(data.c_loke_images)
            {
            	my_autoplay = false;
                lock_swipe=false;
                var cimge=data.c_loke_images;
                cimg=cimge.split("###");
                intsli=parseInt(cimg[1]);
            }
            if(!data.c_loke_images)
            {
            	lock_swipe=true;
            }
            if($.trim(data.push_result)!="NO"){
            	intsli=parseInt(0);
            }
            test(intsli);
            if($.trim(data.push_result)!="NO"){
                drawChart();
                intsli=parseInt(0);
            }
            $('#presentation_slider_pro_slider_show').sliderPro('gotoSlide',intsli);
            $('.sp-selected').panzoom({
	            $zoomIn: $(".zoom-in"),
	            $zoomOut: $(".zoom-out"),
	            disablePan: true,
	        });
	        $('.sp-slides').css("overflow","visible");
	        $('.sp-slide').each( function( index, listItem ) {
	            $(this).css("overflow","visible");
	        });
        }
    });

  },5000);
});

function test(no)
{	
  	var minheight="<?php echo $height ?>";
	var bodyheight = $(window).height()-minheight;
	var bodywidth = $(window).width();
	$( '#presentation_slider_pro_slider_show' ).sliderPro({
		width: bodywidth,
		height: bodyheight,
		fade: true,
		shuffle: false,
		arrows: false,
		buttons: false,
		fullScreen: true,
		visibleSize:'90%',
		forceSize: 'fullWindow',
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fadeFullScreen:true,
		waitForLayers: true,
		thumbnailWidth: 110,
		thumbnailHeight: 110,
		thumbnailPointer: false,
		autoplay: my_autoplay,
		touchSwipe:lock_swipe,
    	thumbnailTouchSwipe:lock_swipe,
    	keyboard:lock_swipe,
		autoScaleLayers: false,
		startSlide: no,
	    gotoSlide: function( event ) {
	       $('#silde_no_text_box').val(event.index);
	       $('.sp-selected').panzoom({
	            $zoomIn: $(".zoom-in"),
	            $zoomOut: $(".zoom-out"),
	            disablePan: true,
	        });
	        $('.sp-slides').css("overflow","visible");
	        $('.sp-slide').each( function( index, listItem ) {
	            $(this).css("overflow","visible");
	        });
	    }
	});
}
</script>
<script src="<?php echo base_url(); ?>assets/plugins/Highcharts-5.0.12/code/highcharts.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script>
    jQuery(document).ready(function() {
        Charts.init();
    });
</script>-->