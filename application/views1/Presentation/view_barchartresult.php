<style type="text/css">
body {
  width:100%;
  height: auto;
  margin:10% auto;
}
#chart_wrap {
  position: relative;
  padding-bottom: 100%;
  height: auto;
  overflow:hidden;
}
.chart {
  position: absolute;
  top: 0;
  left: 0;
  width:100%;
  height:50%;
}
.option-list{
  padding: 0 15px;
  font-family: 'Lato', sans-serif !important;
}
.option-list li {
  line-height: 22px;
  margin: 0 0 20px;
  position: relative;
  text-indent: 0;
  padding: 0 20px 0 20px;
  font-size:21px;
}
.option-list li label{display:inline-block;vertical-align:middle;width:20px;height:20px;}
ul.option-list li span{display:inline-block;vertical-align:middle;max-width:96%;width:auto !important;}

@media (min-width: 1920px){
	.option-list li {font-size:34px;}
}

@media (min-width: 1400px){
	.option-list li {font-size:24px;}
}

@media (max-width: 767px){
	.option-list li {font-size:16px;}
	ul.option-list li span{max-width:90%;width:auto !important;}
		.option-list{ height:100px;
  overflow:scroll;}
}
.highcharts-credits
{
  display:none;
}
</style>
<?php $color_arr=array('#109618','#ff9900','#3366cc','#dc3912','#800000','#FF0000','#C14C33','#593027','#96944C','#156A15','#3B2129','#54073E','#5711A7','#093447','#0C3F33'); 
if(count($final_Array)>0){ ?>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.css">
<?php foreach($final_Array as $key => $item) {  ?>  
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<html><body>
<h3 style="text-align: center;" class="title_show"><?php echo $key; ?></h3>
<div id="chart_wrap" align="center">
<div id='barchart<?php echo $key; ?>' class="chart"></div></div>
<?php $colorindex=0; foreach ($item as $key1 => $value1) { if($colorindex==count($item)-1){$colorta.="'".$color_arr[$colorindex]."'";}else{ $colorta.="'".$color_arr[$colorindex]."',"; } $colorindex++;  } ?>
<ul style="list-style: none;" class="option-list">
<?php $dint=0; foreach ($item as $key2 => $value2) { ?>
<li><label style="border-radius: 100%;background-color:<?php echo $color_arr[$dint]; $dint++;?>">&nbsp;&nbsp;&nbsp;&nbsp;</label>  <?php echo $key2; ?></li>
<?php } ?>
</ul>
</body>
</html>
<script type="text/javascript">
/*google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {                                       
var data = google.visualization.arrayToDataTable([['Options', 'No. of users',{ role: 'style' }],  <?php $i=0; foreach ($item as $a => $b) {?>
['<?php echo $a; ?>', <?php echo $b; ?>,"<?php echo $color_arr[$i]; $i++; ?>"],
<?php } ?>
]);
var options = {
width:'70%',
height:'90%',
align: 'center',  
pieSliceText: 'percentage',         
verticalAlign: 'middle',
pieSliceTextStyle: { color: 'black',},
fontSize: 20,
fontName:'Lato',
legend:{position:'none'},
chartArea: {
        left: "20%",
        top: "1%",
        height: "70%",
        width: "70%"
    }
};
var chart_div = document.getElementById('barchart<?php echo $key;?>');
var chart = new 
google.visualization.BarChart(chart_div);
google.visualization.events.addListener(chart, 'ready', function () {
  chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
  //console.log(chart_div.innerHTML);
});
chart.draw(data, options);
}*/
$(document).ready(function () {
  var data = <?php echo json_encode(array_values($item))?>;
  var dataSum = 0;
  for (var i=0;i < data.length;i++) {
      dataSum += data[i]
  }
  Highcharts.chart('barchart<?php echo $key; ?>', {
    chart: {
      type: 'column'
    },
    title: {
      text: '<?php echo 'Question: '. $key; ?>',
      style:{
        display:'none'
      }
    },
    tooltip: {
      valueSuffix: ' Users'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      allowDecimals: false,
      min: 0,
      title: {
        text: 'Total User Answers'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true,
          formatter:function() {
            var pcnt = (this.y / dataSum) * 100;
            return Highcharts.numberFormat(pcnt) + '%';
          },
          style: {
              fontSize: '25px',
              fontWeight: 'bold',
              color: 'contrast',
              textOutline: '1px contrast'
            },
        }
      }
    },
    colors:<?php echo json_encode($color_arr); ?>,
    legend: {
      enabled:false,
    },
    series: [{
      name: 'Answers',
      colorByPoint: true,
      data: 
      <?php $arr=array(); foreach ($item as $a => $b) { 
        $arr[]=array('name'=>$a,'y'=>$b);
      }
      echo json_encode($arr);
      ?>
    }]
  });
});
</script>
<?php
}
?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/Highcharts-5.0.12/code/highcharts.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script>
jQuery(document).ready(function() {
  Charts.init();
});
</script>-->
<style type="text/css">
svg text {
    font-family: 'Lato', sans-serif !important;
    }
.title_show{
   font-family: 'Lato', sans-serif !important;
}    
</style>
<script type="text/javascript">
    parent.window.showpushresult=1;
</script>
<?php }else { ?>
<div class="row">
	<h4><?php echo $vews_error_msg; ?></h4>
</div>
<script type="text/javascript">
    parent.window.showpushresult=0;
</script>
<?php } ?> 
