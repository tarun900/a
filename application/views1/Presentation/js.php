<?php if($presentations[0]['Auto_slide_status'] =='1') { ?>
  <script type="text/javascript">
  var my_autoplay = true;
  </script>
<?php } else { ?>
  <script type="text/javascript">
  var my_autoplay = false;
  </script>
<?php } ?>
<?php
$current_img = 0;
if(!empty($presentations[0]['Image_current']))
{
    $array1 = json_decode($presentations[0]['Images']);
    $current_img = array_search($presentations[0]['Image_current'],$array1);  
}
if(!empty($presetation_tool[0]['lock_image']))
{
  $array1 = json_decode($presentations[0]['Imagesno']);
  $current_img = array_search($presetation_tool[0]['lock_image'],$array1);
  $presentations[0]['Auto_slide_status']='0';
  ?>
  <script type="text/javascript">
  my_autoplay = false;
  var lock_swipe=false;
  </script>
  <?php 
}
else
{
  ?>
  <script type="text/javascript">
    var lock_swipe=true;
  </script>
  <?php
}
if(!empty($presetation_tool[0]['push_images']))
{  $array1 = json_decode($presentations[0]['Imagesno']);
  $current_img = array_search($presetation_tool[0]['push_images'],$array1);
}
?>

<script>

var current_img = <?php echo $current_img; ?>;

$(window).load(function(){ 
  function setheight() {
    var bodyheight = $(document).height()-53;
    $(".sp-slide, .sp-slides").css("height" , bodyheight);
  }
    setheight();
    $(window).resize(setheight);
});
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sliderPro.js"></script>
<!-- <script type="text/javascript" src="https://bqworks.com/slider-pro/js/examples.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slider-pro.css">

<script type="text/javascript">
function requestFullScreen(element) {     // Supports most browsers and their versions.     
var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;     
if (requestMethod) { // Native full screen.         
requestMethod.call(element);     
} else if (typeof window.ActiveXObject !== "undefined") { // Older IE.         
  var wscript = new ActiveXObject("WScript.Shell");        
   if (wscript !== null) {             
    wscript.SendKeys("{F11}");         
  }     
} 
} 
$('#full_screen_result_btn').click(function(){
var elem = document.getElementById("chart_show_iframe");
if (elem.requestFullscreen) {
  elem.requestFullscreen();
} else if (elem.msRequestFullscreen) {
  elem.msRequestFullscreen();
} else if (elem.mozRequestFullScreen) {
  elem.mozRequestFullScreen();
} else if (elem.webkitRequestFullscreen) {
  elem.webkitRequestFullscreen();
} 
});
$('#push_result_btn').click(function(){
var key=$('#silde_no_text_box').val();
var test='<?php echo $presentations[0]['Imagesno']; ?>';
var imgarr=jQuery.parseJSON(test);
var datademo="";
var testval='';
if($.trim($('#push_result_btn').val())=="Remove Push")
  {
    datademo="poll_id=RemoveCookies";
    testval="RemoveCookies";
  }
  else
  {
    datademo="poll_id="+imgarr[key];
    testval=imgarr[key];
  }
$.ajax({
  url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_result_in_presentation/'.$presentations[0]['Id']; ?>",
  type:'POST',
  data:datademo,
  success:function(resule)
  {
    if($.trim($('#push_result_btn').val())=="Remove Push")
    {
      $('#push_result_btn').val('Push Result');
    }
    else
    {
      $('#push_result_btn').val('Remove Push');
    }
    $('#view_result_id_textbox').val(testval);
    $('#preview_mode_btn_link').show();
    $('#live_mode_btn_link').hide();
  }
});
});
$('#full_screen_show').click(function(){
  if($.trim($('#full_screen_show').val())=="Full Screen")
  {
    $('#my_slider_silderpro_div').addClass('slider_full_width'); 
    $('#presenter_mode_menu_tool_div').toggle();
    $('#presenter_left_menu_tool_div').toggle();
    $('#full_screen_show').val('Exit Full Screen');
  }
  else
  {
    if($('#presenter_left_menu_tool_div').hasClass('presenter_left_menu_tool'))
    {
      $('#my_slider_silderpro_div').removeClass('slider_full_width'); 
    }
    $('#presenter_mode_menu_tool_div').toggle();
    $('#presenter_left_menu_tool_div').toggle();
    $('#full_screen_show').val('Full Screen');
  }
  $('#example3').data('sliderPro').destroy();
  var intsli=parseInt($('#silde_no_text_box').val());
  test(intsli);
  var bodyheight = $(window).height()-53;
  $('.sp-slides').css("height",bodyheight+'px');
  fullscreent_test();
});
function fullscreent_test()
{ 
  var elem = document.getElementById("view_fullscreen_div");
if (elem.requestFullscreen) {
  elem.requestFullscreen();
} else if (elem.msRequestFullscreen) {
  elem.msRequestFullscreen();
} else if (elem.mozRequestFullScreen) {
  elem.mozRequestFullScreen();
} else if (elem.webkitRequestFullscreen) {
  elem.webkitRequestFullscreen();
} 
  
  /*if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  } */       
}
function saveansbyquestion(sid)
{
  var id="<?php echo 'question_option'; ?>"+sid;
  if($('input[name='+id+']:checked').length > 0)
  {
    var ans=$('input[name='+id+']:checked').val();
  }
  if($.trim(ans)!=""){
  $.ajax({
    url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_survey_ans_presentation'; ?>",
    type:'POST',
    data:"sid="+sid+"&ans="+ans,
    success:function(resule){
      $('#submit_ans_btn_'+sid).hide();
      $('#msg_question_'+sid).html('Thank you for your answer');
    }
  });
  }
  else
  {
    alert('Please Select One Answer');
  }
}
jQuery(document).ready(function(){
  $('.list input').each(function(){
    var self = $(this),
      label = self.next(),
      label_text = label.text();
    label.remove();
    self.iCheck({
      checkboxClass: 'icheckbox_line-blue',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + label_text
    });
  });
  $('#link_view_result').click(function(){
    var key=$('#silde_no_text_box').val();
    var test='<?php echo $presentations[0]['Imagesno']; ?>';
    var imgarr=jQuery.parseJSON(test);
    if($.isNumeric(imgarr[key]))
    {
      $('#push_result_btn').show();
      if($.trim($('#view_result_id_textbox').val())==imgarr[key])
      {
        $('#push_result_btn').val('Remove Push');
      }
      else
      {
        $('#push_result_btn').val('Push Result'); 
      }
    }
    $('#chart_show_iframe').attr("src","<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/showchartdata/'; ?>"+imgarr[key]); 
  });
  $('#view_result_btn').click(function(){
    $('#chart_show_iframe').attr("src","<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/showchartdata/'; ?>"+$.trim($('#view_result_id_textbox').val())); 
  });
  $('#link_unlocked').click(function(){
    $.ajax({
      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_lock_images_presentation/'.$presentations[0]['Id']; ?>",
      type:'POST',
      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
      success:function(resule){
        $('#link_unlocked').hide();
        $('#link_locked').show();
        my_autoplay = false;
        lock_swipe=false;
        var slider = $('#example3').data('sliderPro');
        slider.destroy();
        var intsli=parseInt($('#silde_no_text_box').val());
        test(intsli);
        var bodyheight = $(window).height()-53;
        $('.sp-slides').css("height",bodyheight+'px');
      }
    });
  });
  $('#link_locked').click(function(){
    $.ajax({
      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_lock_images_presentation/'.$presentations[0]['Id']; ?>",
      success:function(resule){
        $('#link_unlocked').show();
        $('#link_locked').hide();
        my_autoplay = true;
        lock_swipe=true;
        var slider = $('#example3').data('sliderPro');
        slider.destroy();
        var intsli=parseInt($('#silde_no_text_box').val());
        test(intsli);
        var bodyheight = $(window).height()-53;
        $('.sp-slides').css("height",bodyheight+'px');
      }
    });
  });
  $('#link_push').click(function(){
    $.ajax({
      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
      type:'POST',
      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
      success:function(resule){
        var intsli=parseInt($('#silde_no_text_box').val());
        test(intsli);
        $('#preview_mode_btn_link').hide();
        $('#live_mode_btn_link').show();
        $('#view_result_id_textbox').val('');
      }
    });
  });
  $('#preview_mode_btn_link').click(function(){
    $.ajax({
      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
      type:'POST',
      data:"slid_no="+$.trim($('#silde_no_text_box').val()),
      success:function(resule){
        var intsli=parseInt($('#silde_no_text_box').val());
        test(intsli);
        $('#preview_mode_btn_link').hide();
        $('#live_mode_btn_link').show();
      }
    });
  });
  $('#live_mode_btn_link').click(function(){
    $.ajax({
      url:"<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/save_puch_images_presentation/'.$presentations[0]['Id']; ?>",
      success:function(resule){
        var intsli=parseInt($('#silde_no_text_box').val());
        test(intsli);
        $('#preview_mode_btn_link').show();
        $('#live_mode_btn_link').hide();
      }
    });
  });
  $('#link_next').click(function(){
    $('#example3').sliderPro('nextSlide');
  });
  $('#link_prev').click(function(){
    $('#example3').sliderPro('previousSlide');
  });
  $('#link_question').click(function(){
    $('#question_show_iframe').attr("src","<?php echo base_url().'speaker_Messages/fornt_index/'.$event_id; ?>"); 
  });
});

  jQuery( document ).ready(function( $ ) {
    if(location.hash)
    {
      var slidno=location.hash.split("#");
      var intsli=parseInt(slidno[1]);
      current_img=intsli;
    }
    var bodywidth=$(document).width();
    var bodyheight = $(document).height()-53;
    jQuery( '#example3' ).sliderPro({
      width: 2000,
      height: 'bodyheight',
      fade: true,
      arrows: false,
      buttons: false,
      fullScreen: true,
      fadeFullScreen:true,
      shuffle: false,
      smallSize: 500,
      mediumSize: 1000,
      touchSwipe:lock_swipe,
      thumbnailTouchSwipe:lock_swipe,
      largeSize: 3000,
      thumbnailArrows: true,
      autoplay: my_autoplay,
      startSlide: current_img,
      gotoSlide: function( event ) {
        $('#silde_no_text_box').val(event.index);
      }
    });
  });
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.easing.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/dist/jquery.panzoom.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});
function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (value * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
/*jQuery(document).ready(function() 
{
  setInterval(function () {
    jQuery.ajax({
          type: "POST",
          data: '',
          url:  "<?php echo base_url(); ?>Presentation/<?php echo $this->uri->segment(2).'/'.$event_templates[0]['Subdomain']; ?>/Get_slider_images/<?php echo $this->uri->segment(5); ?>",
          dataType: "json",
          success: function(data)
          {
            var flag=true;
            if(jQuery("#hide_thumb").val()=='Hide Thumb')
            {
                flag=true;
                var thumbtn='<div class="thumbnail_btn"><input type="button" value="Hide Thumb" id="hide_thumb" ></div>';
            }
            else
            {
                flag=false;
                var thumbtn='<div class="thumbnail_btn"><input type="button" value="Show Thumb" id="hide_thumb" ></div>';
            }
            $('#example3').data('sliderPro').destroy();
            var height = $(document).height() - 50;
            $('.sp-slides').html('');
            $('.my-sp-thumbnails').html('');
            var no=0;
            for(var aa in data['images']) {
              $('.sp-slides').append($("#siderTemplate").tmpl(data['images'][aa]));
              if($.trim(data.push_result)!="NO"){
                drawChart();
              }
              $('.my-sp-thumbnails').append($("#siderTemplate1").tmpl(data['thum'][aa]));
            }
            $('.list input').each(function(){
              var self = $(this),
                label = self.next(),
                label_text = label.text();
              label.remove();
              self.iCheck({
                checkboxClass: 'icheckbox_line-blue',
                radioClass: 'iradio_line-blue',
                insert: '<div class="icheck_line-icon"></div>' + label_text
              });
            });

            /*if($.trim(data.push_result)!="NO" && $.trim(getCookie('usershowcookie'))!=data.push_result)
            {
              if ($.trim(data.push_result)!="RemoveCookies") 
              {
                $('#chart_show_iframe').attr("src","<?php echo base_url().'Presentation/'.$this->uri->segment(2).'/'.$Subdomain.'/showchartdata/'; ?>"+data.push_result); 
                $('#full_screen_result_btn').show();
                $('#view_result_models').modal('show');
                $('#view_result_btn').show();
              }
              else
              {
                $('#view_result_id_textbox').val('');
              }
              if($.trim(getCookie('usershowcookie'))==""){
                setCookie('usershowcookie',data.push_result);
              }else{
                delete_cookie('usershowcookie');
                setCookie('usershowcookie',data.push_result);
              }
            }
            else
            {
              $('#view_result_btn').hide(); 
            }*/
              /*if(!data.c_loke_images && !data.c_push_images){
                lock_swipe=true;
                var CImg=window.current_img_index;
                var intsli=parseInt($('#silde_no_text_box').val());
                <?php if($presentations[0]['Auto_slide_status'] =='1') { ?>
                 test(intsli);
                <?php } else { ?>
                 test2(intsli);
                <?php } ?>
                var bodyheight = $(document).height()-53;
                $('.sp-slides').css("height",bodyheight+'px');
              }
              else
              {
                  var cimg;
                  if(data.c_loke_images){
                    my_autoplay = false;
                    lock_swipe=false;
                    var cimge=data.c_loke_images;
                    cimg=cimge.split("###");
                  }
                  else
                  {
                    lock_swipe=true;
                  }
                  if(data.c_push_images){
                    my_autoplay = false;
                    var cimge=data.c_push_images;
                    cimg=cimge.split("###");
                    if($.trim(getCookie('usershowcookie'))==""){
                      setCookie('usershowcookie',data.c_push_images);
                    }else{
                      delete_cookie('usershowcookie');
                      setCookie('usershowcookie',data.c_push_images);
                    }
                    $('#view_result_models').modal('hide');
                  }
                  if($.trim(data.push_result)!="NO")
                  {
                    var intsli=parseInt(0);  
                  }
                  else
                  {
                    var intsli=parseInt(cimg[1]);  
                  }
                  test(intsli);
                  var bodyheight = $(document).height()-53;
                  $('.sp-slides').css("height",bodyheight+'px');
              }
              $('.sp-selected').panzoom({
                $zoomIn: $(".zoom-in"),
                $zoomOut: $(".zoom-out"),
                disablePan: true,
              });
              $('.sp-slides').css("overflow","visible");
              $('.sp-slide').each( function( index, listItem ) {
                $(this).css("overflow","visible");
              });
              if(flag)
              {
                $('.sp-thumbnails-container').css('display','block');
              }
              else
              {
                $('.sp-thumbnails-container').css('display','none');
              }
              jQuery("#hide_thumb").on('click',function() 
              {
                jQuery(".sp-thumbnails-container").toggle();
                if(jQuery("#hide_thumb").val()=='Hide Thumb')
                {
                  jQuery("#hide_thumb").val('Show Thumb');
                }
                else
                {
                  jQuery("#hide_thumb").val('Hide Thumb');
                }
              }); 
          }
      });

  },5000);
});*/

function test(no)
{
  var bodyheight = $(document).height()-53;
  jQuery( '#example3' ).sliderPro({
    width: 2000,
    height: 'bodyheight',
    fade: true,
    arrows: false,
    buttons: false,
    fullScreen: true,
    fadeFullScreen:true,
    shuffle: false,
    smallSize: 500,
    mediumSize: 1000,
    touchSwipe:lock_swipe,
    thumbnailTouchSwipe:lock_swipe,
    largeSize: 3000,
    thumbnailArrows: true,
    autoplay: my_autoplay,
    startSlide: no,
    gotoSlide: function( event ) {
      $('#silde_no_text_box').val(event.index);
    }
  });
}

function test2(no)
{
  var bodyheight = $(document).height()-53;
  jQuery( '#example3' ).sliderPro({
    width: 2000,
    height: 'bodyheight',
    fade: true,
    arrows: false,
    buttons: false,
    fullScreen: true,
    fadeFullScreen:true,
    touchSwipe:lock_swipe,
    thumbnailTouchSwipe:lock_swipe,
    shuffle: false,
    smallSize: 500,
    mediumSize: 1000,
    largeSize: 3000,
    thumbnailArrows: true,
    autoplay: false,
    startSlide: no,
    gotoSlide: function( event ) {
      $('#silde_no_text_box').val(event.index);
    }
  });
}
</script>
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script>
    jQuery(document).ready(function() {
        Charts.init();
    });
</script>