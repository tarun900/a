<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">
<?php if(!empty($img_data)){ if(is_array($img_data)){ ?>
<div id="view_fullscreen_div">
<div class="my_slider_sliderPro" id="my_slider_silderpro_div">
<div id="example3" class="slider-pro">
<div class="sp-slides">
<div class="sp-slide">
<div class="sp-image">
    <h2 class="list-group-item-heading"><?php echo $img_data[0]['Question']; ?></h2>
    <?php if(!empty($img_data[0]['Answer'])){ ?>
    <span class="hdr-span"> Thank you for your answer.</span>
    <?php }else{ ?>
    <span class="hdr-span"> Please select one answer.</span>
    <?php } ?>
    <ul class="list">
    <?php $ans=json_decode($img_data[0]['Option'],TREU);
    foreach ($ans as $key1 => $value1) { ?>
        <li>
            <input type="radio" name="question_option" value="<?php echo $value1; ?>" class="required radio-callback" <?php if($value1==$img_data[0]['Answer']){ ?> checked="checked" <?php } ?>>
            <h2>
            <label for="line-radio-"><?php echo $value1; ?></label>
          </h2>
        </li>
    <?php } ?>
    </ul>
    <?php if(empty($img_data[0]['Answer'])){ ?>
    <div class="col-sm-12"><button name="next_slied" id="submit_ans_btn1" class="btn btn-green btn-block" value="Next">Submit</button></div>
    <?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>	
<?php }else{ ?>
<div id="view_fullscreen_div">
<img width="100%" height="100%" class="sp-image" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img_data; ?>"/>
</div>
<?php } }else{
  echo "<div id='view_fullscreen_div'><h3>No Slide Pushed Or Locked</h3></div>";
  } ?>
<div class="full_screen_btn"><button name="next_slied" id="submit_ans_btn" class="btn btn-green btn-block" value="Next">Full</button></div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
function requestFullScreen(element) {
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}
function fullscreent_test()
{ 
var elem = document.getElementById("view_fullscreen_div");
if (elem.requestFullscreen) {
  elem.requestFullscreen();
} else if (elem.msRequestFullscreen) {
  elem.msRequestFullscreen();
} else if (elem.mozRequestFullScreen) {
  elem.mozRequestFullScreen();
} else if (elem.webkitRequestFullscreen) {
  elem.webkitRequestFullscreen();
}  
	if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }     
}
$(document).ready(function(){ 
	$('body').attr('class','sidebar-close');
	requestFullScreen(document.getElementById("view_fullscreen_div"));
$('#submit_ans_btn').click(function(){
  	fullscreent_test();
});	
$('.list input').each(function(){
var self = $(this),
  label = self.next(),
  label_text = label.text();
label.remove();
self.iCheck({
  checkboxClass: 'icheckbox_line-blue',
  radioClass: 'iradio_line-blue',
  insert: '<div class="icheck_line-icon"></div>' + label_text
});
});
});
$(document).ready(function() 
{
	setInterval(function () {
    jQuery.ajax({
	    type: "POST",
	    data: '',
	    url:  "<?php echo base_url(); ?>Presentation/<?php echo $this->uri->segment(2).'/'.$event_templates[0]['Subdomain']; ?>/Get_slider_images/<?php echo $this->uri->segment(5); ?>",
	    dataType: "json",
	    success: function(data)
	    {
	    	$('#view_fullscreen_div').html(data.push_full_image);
        $('.list input').each(function(){
        var self = $(this),
          label = self.next(),
          label_text = label.text();
        label.remove();
        self.iCheck({
          checkboxClass: 'icheckbox_line-blue',
          radioClass: 'iradio_line-blue',
          insert: '<div class="icheck_line-icon"></div>' + label_text
        });
      });
      $('body').attr('class','sidebar-close'); 
	    }
	  });
	},5000);
});
</script>
<style type="text/css">
	.full_screen_btn{
		position: absolute;     
    top: 10px;     
    right: 10px;     
    z-index: 9999;
	}
  #view_fullscreen_div{
    width: 100%;
    height: 100%;
  }
  .sp-slide{
    width: 100%;
    height: 100%;
  }
  .sp-slide div{
    text-align: center;
    width: 100%;
    height: 100%;
  }
</style>