<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
/*	body {
  width: 80%;
  margin: 30px auto;
  font-family: sans-serif;
}*/
body{
	font-family: 'Arial';
}
.img-youtube{
  position: relative;
  width: 100%;
  display: block;
}
.img-youtube span{
    position: absolute;
    left: 50%;
    top: 50%;
    width: 68px;
    height: 48px;
    margin-left: -34px;
    margin-top: -24px;
    -moz-transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
    -webkit-transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
    transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
    z-index: 63;
    cursor: pointer;
  }

h3 {
  text-align: center;
  font-size: 1.65em;
  margin: 0 0 30px;
}
.Videosection {
    margin-top: 20px;
}


a.open_video p {
    min-height: 100px;
}
figure {
  margin: 0;
  overflow: hidden;
}


img {
  border: none;
  width: 100%;
  height: auto;
  display: block;
  background: #ccc;
  transition: transform .2s ease-in-out;
}

.p a {
  display: inline;
  font-size: 13px;
  margin: 0;
  text-decoration: underline;
  color: blue;
}

.p {
  text-align: center;
  font-size: 13px;
  padding-top: 100px;
}
.text-red{
  color: #BD202F;
}
figure.decoration {
    margin: 0;
    overflow: hidden;
    padding: 10px;
    box-sizing: border-box;
    margin-bottom: 15px;
    border-radius: 5px;
    display: block;
}
iframe{
  width: 100%;
  height: 350px;
}
figcaption h4 {
    font-size: 32px;
    font-weight: bold;
}
figcaption p {
    font-size: 22px;
}
.Videosection .row.video-inner-sec {
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	align-items: stretch;
	align-content: stretch;
	justify-content: flex-start;
}
.Videosection .video-item::before {
	content: '';
	border: 1px solid #000;
	position: absolute;
	left: 15px;
	right: 15px;
	top: 0;
	bottom: 0;
	border-radius: 10px;
	margin-bottom: 15px;
	z-index: -1;
}
/*
.modal-open{overflow:hidden; border:10px #ffff00 solid;height:1000px; position:fixed; overflow:hidden;   }
.modal-dialog{border:5px #ff00ff solid; position:fixed;top:50px!important;  max-height:800px; }
.modal-open .Videosection{border:5px #F00 solid;height:1000px; position:fixed; overflow:hidden;  }

*/
@media only screen and (max-width : 768px) {
.Videosection .row.video-inner-sec {
	display: inline-block;
}

}
@media only screen and (max-width : 640px) {
figcaption h4 {
	font-size: 30px;
}
figcaption p {
	font-size: 18px;
}
}
</style>
<!-- 
<div class="videos">
  <?php foreach ($data as $key => $value) { ?>
    <a class="open_video" data-id="<?php echo $value['url']; ?>">
      <figure>
        <?php $id = explode('/', $value['url']); ?>
        <div class="img-youtube">
          <img  src="https://img.youtube.com/vi/<?php echo $id[count($id)-1]; ?>/hqdefault.jpg" >
          <span class="text-red"><i class="fa fa-youtube-play fa-3x text-red" aria-hidden="true"></i></span>
         </div>
        <figcaption>
          <h4 style="max-height: 72px;"><?php echo (strlen($value['title']) > 30) ? substr($value['title'], 0,30) ."..." : $value['title']; ?></h4>
         <p><?php echo $value['desc']; ?></p>
        </figcaption>
      </figure>
    </a>
  <?php } ?>
</div> -->
<div class="Videosection">
<div class="container">
  <div class="row video-inner-sec">
  <?php foreach ($data as $key => $value) { ?>
  <div class="col-md-4 col-sm-12 col-xs-12 video-item">
    <a class="open_video" data-id="<?php echo $value['url']; ?>">
      <figure class="decoration">
        <?php $id = explode('/', $value['url']); ?>
        <div class="img-youtube">
          <img  src="https://img.youtube.com/vi/<?php echo $id[count($id)-1]; ?>/hqdefault.jpg" >
          <span class="text-red"><i class="fa fa-youtube-play fa-3x text-red" aria-hidden="true"></i></span>
         </div>
        <figcaption>
          <h4><?php echo (strlen($value['title']) > 30) ? substr($value['title'], 0,30) ."..." : $value['title']; ?></h4>
         <p><?php echo $value['desc']; ?></p>
        </figcaption>
      </figure>
    </a>
    </div>
  <?php } ?>

</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(".open_video").on('click',function(){
    $('.modal-body').html('');
    var src_url = $(this).data('id');
      $('<iframe />');  
      $('<iframe />', {
          name: 'frame1',
          id: 'frame1',
          src: src_url,
      }).appendTo('.modal-body');
      $("#myModal").modal('show');
  })
  $('#myModal').on('hidden.bs.modal', function () {
    $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
});
</script>

