<?php $acc_name=$this->session->userdata('acc_name'); ?>
<!-- start: PAGE CONTENT -->
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">

			<div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#speaker_list" data-toggle="tab">
                                Add Key People
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="speaker_list">
                        <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div id="wizard" class="swMain">
                                <ul style="margin-top:15px;">
                                    <li>
                                        <a href="#step-1">
                                            <div class="stepNumber">
                                                1
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Key Person Name & Email</small></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <div class="stepNumber">
                                                2
                                            </div>
                                            <span class="stepDesc">
                                            <small>Company & Bio</small> </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <div class="stepNumber">
                                                3
                                            </div>
                                            <span class="stepDesc">
                                            <small>Add Profile Image</small> </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <div class="stepNumber">
                                                4
                                            </div>
                                            <span class="stepDesc">
                                            <small>Contact Details</small> </span>
                                        </a>
                                    </li>
                                    <!-- <li>
                                        <a href="#step-5">
                                            <div class="stepNumber">
                                                5
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Invite Email</small></span>
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="#step-5">
                                            <div class="stepNumber">
                                                6
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Finish</small></span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress progress-xs transparent-black no-radius active">
                                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                    </div>
                                </div>
                                <div id="step-1">
                                    <div class="col-sm-6">
                                        <h4>What is the name of the Key Person you would like to add?</h4>
                                        <div class="form-group">
                                            <label class="control-label">First Name <span class="symbol required"></span>
                                            </label>
                                            <input type="text" placeholder="First Name" class="form-control" id="Firstname" name="Firstname">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Last Name <!-- <span class="symbol required"></span> -->
                                            </label>
                                            <input type="text" placeholder="Last Name" class="form-control" id="Lastname" name="Lastname">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Speaker Type
                                            </label>
                                            <select class="form-control" name="speaker_type">
                                                    <option value="">Select Speaker Type</option>
                                                <?php foreach ($speaker_type as $key => $value): ?>
                                                    <option value="<?=$value['type_id']?>"><?=$value['type_name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Enter the person’s email and set a password</h4>
                                        <p>By  entering the person’s email and setting a password they will be able to log in to the app, receive live questions and share their contact details upon request with other app users. If you do not want this person to have an account use another email address so you can moderate questions asked.</p>
                                        <div class="form-group">
                                            <label class="control-label">Email <!-- <span class="symbol required"></span> -->
                                            </label>
                                            <input type="text" placeholder="Email" class="form-control" id="email" name="email" onblur="checkemail()">
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                    <label class="control-label">Password <!-- <span class="symbol required"></span> --></label>
                                                    <input type="password" class="form-control" id="password" name="password">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Confirm Password <!-- <span class="symbol required"></span></label> -->
                                                <input type="password" class="form-control" id="password_again" name="password_again">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button class="btn btn-theme_green next-step btn-block">
                                                    Next Step >>>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2">
                                    <div class="col-sm-6">
                                        <h4>Company or Organization</h4>
                                        <p>Enter the name of the person’s company or organization</p>
                                        <div class="form-group">
                                            <input type="text" placeholder="Company Name" class="form-control" id="Company_name" name="Company_name">
                                        </div>

                                        <p>Enter the title of the person’s in a company or organization</p>
                                        <div class="form-group">
                                            <input type="text" placeholder="Title" class="form-control" id="Title" name="Title">
                                        </div>

                                        <h4>Link documents to this speaker profile</h4>
                                        <div class="form-group">
                                            <select id="form-field-select-4" class="form-control new_doc" name="document_id[]" multiple="multiple">
                                                <?php foreach ($doc_list as $key => $value){
                                                        echo'<option value="'.$value['id'].'">'.$value["title"].'</option>';
                                                    } 
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Biography</h4>
                                        <p>Enter the person’s bio in the box below. If you would like the person to fill this out themselves leave this blank. The person can login and edit the bio.</p>
                                        <div class="form-group">
                                            <textarea id="Speaker_desc" name="Speaker_desc" class="summernote"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button class="btn btn-green back-step btn-block">
                                                    <<< Back
                                                </button>
                                            </div>
                                            <div class="col-sm-4">
                                                <button class="btn btn-theme_green next-step btn-block">
                                                    Next Step >>>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3">
                                    <h4>Add a profile Image</h4>
                                    <p>This will appear on the person’s profile in the Key People directory</p>
                                    <div class="form-group">
                                        <div class="modal fade" id="spekerlogomodelscopetool" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">User Logo</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <section>
                                                        <div class="demo-wrap" style="display:none;">
                                                            <div class="container">
                                                                <div class="grid">
                                                                    <div class="col-1-2">
                                                                        <div id="vanilla-demo"></div>
                                                                    </div>
                                                                    <div class="col-1-2">
                                                                        <strong>Vanilla Example</strong>
                                                                        <div class="actions">
                                                                            <button class="vanilla-result">Result</button>
                                                                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div id="preview_user_logo_crop_tool"></div>
                                                            <div class="col-sm-3 crop-btn">
                                                                <button class="btn btn-green btn-block" id="user_upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                                            </div>
                                                        </div>         
                                                    </section>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                   <input type="file" name="userfile" id="speakeruserfile_upload">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_logo_data">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                            <input type="hidden" id="user_logo_crope_images_text" name="user_logo_crope_images_text" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2" style="padding-left:0px;">
                                            <button class="btn btn-green back-step btn-block">
                                                <<< Back
                                            </button>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-theme_green next-step btn-block">
                                                Next Step >>>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-4">
                                    <!-- <div class="col-sm-6">
                                        <h4>Enter contact details</h4>
                                        <p>All of these fields are optional–you don’t have to enter these in.If the person would like to add them later they can do so by logging in and opening their profile in the app. These details cannot be seen by other app users unless they request contact details from the person in the app and the person accepts the request.</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Phone Business </label>
                                                   <input type="text" placeholder="Phone Business" class="form-control" id="Phone_business" name="Phone_business">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Mobile No </label>
                                                    <input type="text" placeholder="Mobile No" class="form-control" id="Mobile" name="Mobile">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Address 1</label>
                                                    <input type="text" placeholder="Street" class="form-control" id="Street" name="Street">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Address 2</label>
                                                    <input type="text" placeholder="Suburb" class="form-control" id="Suburb" name="Suburb">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Address 3</label>
                                                    <input type="text" placeholder="Address 3" class="form-control" id="address3" name="address3">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                            Country 
                                                    </label>
                                                    <select onchange="get_state();" id="country" class="form-control" name="country">
                                                        <option value="">Select...</option>
                                                        <?php foreach($countrylist as $key=>$value) {  ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['country_name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Zip Code </label>
                                                    <input type="text" placeholder="Zip Code" id="zipcode" name="zipcode" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">State </label>
                                                    <select id="state" class="form-control" name="state">
                                                            <option value="">Select...</option>
                                                            <?php
                                                                foreach($Statelist as $key=>$value)
                                                                { 
                                                                ?>
                                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->state_name; ?></option>
                                                                <?php
                                                                }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-sm-6">
                                        <h4>Social Media Accounts</h4>
                                        <p>You can link social media accounts to the person in the fields below. Make sure you copy and paste the profile link in its entirety, including the “https://” at the start of the address.</p>
                                        <div class="row" style="margin-top:55px;">
                                             <div class="col-sm-2">
                                                <img src="<?php echo base_url().'assets/images/icon/facebook.png'; ?>" style="height:35px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Facebook Url" id="facebook_url" name="facebook_url" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-sm-2">
                                                <img src="<?php echo base_url().'assets/images/icon/twitter.jpeg'; ?>" style="height:35px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Twitter Url" id="twitter_url" name="twitter_url" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-sm-2">
                                                <img src="<?php echo base_url().'assets/images/icon/linkdin.jpeg'; ?>" style="height:35px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Linkdin Url" id="linkdin_url" name="linkdin_url" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-sm-2">
                                                <img src="<?php echo base_url().'assets/images/icon/youtube.png'; ?>" style="height:35px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Youtube Url" id="youtube_url" name="youtube_url" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-sm-2">
                                                <img src="<?php echo base_url().'assets/images/icon/instagram.png'; ?>" style="height:35px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Instagram Url" id="instagram_url" name="instagram_url" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button class="btn btn-green back-step btn-block">
                                                    <<< Back
                                                </button>
                                            </div>
                                            <div class="col-sm-4">
                                                <button class="btn btn-theme_green next-step btn-block">
                                                    Next Step >>>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div id="step-5">
                                    <h4>Invite your Key Person to log in, view and edit their profile details</h4>
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <div class="form-group">
                                            <textarea id="Speaker_mail" name="Speaker_mail" class="summernote">
                                                <p>Dear {{sfnm}},</p>
                                                <p>Your Speaker Profile has been created on our app.</p>
                                                <p>To access the app please visit {{applink}} and use the following credentials to login:</p>
                                                <p>Username:{{semail}}</p>
                                                <p>Password:{{spassword}}</p>
                                                <p>After you have logged in click the button on the top right and select  ‘My Profile’. There you can view and edit your contact details, biography and profile image.</p>
                                                <p>Best regards,</p>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Subject</label>
                                            <input type="text" placeholder="Enter Email Subject" class="form-control" id="subject" name="subject">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Sender Email</label>
                                            <input type="text" placeholder="Enter Sender Email Address" class="form-control" id="sender_email" name="sender_email">
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input id="emailpreview" type="button" class="btn btn-yellow btn-block" data-toggle="modal" data-target="#myModal" value="Preview">
                                            </div>
                                        </div>
                                        <div class="form-group">    
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <button class="btn btn-theme_green next-step btn-block">
                                                    Send Mail
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <button class="btn btn-theme_green next-skip btn-block">
                                                    Skip
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div id="step-5">
                                    <div class="col-sm-6">
                                        <!-- <h4>Your Key Person had been added to your app!</h4> -->
                                        <div id="result_div"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button type="button" id="finishform" class="btn btn-theme_green btn-block" value="Finish">Click here to Finish</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="addanotherspeaker" class="btn btn-yellow btn-block" value="Add another key person">Add another key person</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="viewport_speaker" class="iphone">
                                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<style type="text/css">
.form-horizontal .form-group { margin-right: 0px !important; margin-left: 0px !important; }
#viewport_speaker.iphone {
    background: url('../../../assets/images/demo-iphone5s.png') no-repeat;
    width: 437px;
    height: 868px;
    margin: 0 auto;
    position: relative;
}
</style>
<!-- end: PAGE CONTENT-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">

        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js"></script>
<script>
    Demo.init();
</script>
<script type="text/javascript">  
$('#remove_logo_data').click(function(){
    $('#user_logo_crope_images_text').val('');
});
$('#emailpreview').click(function(){
    var str='';
    if($.trim($('#subject').val())!="")
    {
        str="<p>Subject:"+$.trim($('#subject').val())+"</p>";
    }
    if($.trim($('#sender_email').val())!="")
    {
        str+="<p>Sender Email:"+$.trim($('#sender_email').val())+"</p>Contant:";
    }
    str+=$('#Speaker_mail').code();
    str=str.replace('{{sfnm}}',$('#Firstname').val());
    str=str.replace('{{applink}}',"<?php echo base_url().$acc_name.'/'.$event['Subdomain']; ?>");
    str=str.replace('{{semail}}',$('#email').val());
    str=str.replace('{{spassword}}',$('#password').val());
    $('.modal-body').html(str);
});
$('#finishform').click(function(){
    $('#finishform').attr("disabled", "disabled");
    $('#addanotherspeaker').attr("disabled", "disabled");
    var formData = new FormData($('form')[0]);
    formData.append('Speaker_desc',$('#Speaker_desc').code());
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Speaker/add/'.$this->uri->segment(3); ?>",
        data: formData,
        processData: false,
        contentType: false,
        success: function(result){
           var data=result.split('###');
            if(data[0]=="success")
            {
                $('#result_div').attr('class','alert alert-success');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            else
            {
                $('#result_div').attr('class','alert alert-danger');
                $('#result_div').html(data[1]);
                $('#result_div').show();
                $('#finishform').removeAttr('disabled');
                $('#addanotherspeaker').removeAttr('disabled');
            }
        }
    });
});
$('#addanotherspeaker').click(function(){
    $('#finishform').attr("disabled", "disabled");
    $('#addanotherspeaker').attr("disabled", "disabled");
    var formData = new FormData($('form')[0]);
    formData.append('Speaker_desc',$('#Speaker_desc').code());
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Speaker/add/'.$this->uri->segment(3); ?>",
        data: formData,
        processData: false,
        contentType: false,
        success: function(result){
            var data=result.split('###');
            if(data[0]=="success")
            {
                $('#result_div').attr('class','alert alert-success');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            else
            {
                $('#result_div').attr('class','alert alert-danger');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            window.location.href="<?php echo base_url().'Speaker/add/'.$this->uri->segment(3); ?>";
        }
    });
});
$("#country").change(function get_state(){
  
    $.ajax({
    url:"<?php echo base_url(); ?>Profile/getnewstate",    
    data: {id: $(this).val()},
    type: "POST",
    success: function(data)
    {
        
        $("#state").html(data);
    }
    
    });

});
        

            
</script>