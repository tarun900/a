<?php $acc_name=$this->session->userdata('acc_name');
$user = $this->session->userdata('current_user'); ?>
<!-- start: PAGE CONTENT -->
<style type="text/css">
#speakers_msg_list_processing {
  background: rgba(0, 0, 0, 0.5) url("<?php echo base_url(); ?>assets/images/ajax-loader.gif") no-repeat scroll center center;
  height: 100%;
  left: 0;
  position: fixed;
  text-indent: -999999px;
  top: 0;
  width: 100%;
  z-index: 9999;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
        	<div class="panel-body">
        	<a class="btn btn-success" href="<?php echo base_url().'speaker_Messages/exportcsv/'.$this->uri->segment(3); ?>" >Export Csv</a>
        	<a class="btn btn-green" href="<?php echo base_url().'speaker_Messages/index/'.$this->uri->segment(3); ?>" >See New Messages</a><br/>
        		<form action="<?php echo base_url().'speaker_Messages/forwardmsg/'.$this->uri->segment(3); ?>" method="post">
	        		<div id="speakers_list" class="table-responsive" style="overflow-x: auto;">
	                    <table class="table table-striped table-bordered table-hover table-full-width" id="speakers_msg_list">
	                    	<thead>
	                    		<tr>
	                    			<th>#</th>
	                    			<th>Select</th>
	                    			<th>Edit Priority</th>
	                    			<th>
	                    			<?php if($user[0]->is_moderator=='1'){ ?>
	                    			Desired Receiver
	                    			<?php }else{ ?>
	                    			Asec / Desc 
	                    			<?php } ?> 
	                    			</th>
	                    			<th>Sender</th>
	                    			<th>Title</th>
	                    			<th>Company</th>
	                    			<th>Forwarded by</th>
	                    			<th>Messages</th>
	                    			<th>Check if read</th>
	                    			<th>Check if Forward</th>
	                    			<th>Received</th>
	                    			<th>Priority No.</th>
	                    			<th></th>
	                    		</tr>
	                    	</thead>
	                    </table>
	                </div>
	                <?php if($user[0]->is_moderator!='1'){ ?>
	                <div class="col-sm-12" style="padding: 21px;">
	                	<input type="button" id="forwatd_order_save" value="Save Order" class="btn btn-green">
	                </div>
	                <?php } ?>
	                <div class="row">
	                	<?php if($user[0]->is_moderator!='1'){ ?>
	                	<label class="col-sm-2" for="form-field-1" style="padding-right: 0px;">Select Speakers and Attendees</label>
	                	<div class="col-sm-8 speaker_messages_search">
			                <select name="Receiver_id[]" id="Receiver_id" class="form-control search-select" style="width:100%;" multiple="true">
		                        <option value="">Select</option>
		                        <optgroup label="Speaker">
		                            <?php
		                              foreach ($speakers as $keysp => $values)
		                              {
		                               foreach ($values as $keyspeaker => $valuespeaker)
		                               {
		                                    if ($valuespeaker['Firstname'] != "") { ?>
		                                    <option value="<?php echo $valuespeaker['Id']; ?>">
		                                         <?php echo $valuespeaker['Firstname'] . ' ' . $valuespeaker['Lastname']; ?>
		                                    </option>
		                                    <?php
		                                    }
		                               }
		                              }
		                            ?>
		                        </optgroup>
		                        <optgroup label="Attendee">
		                            <?php
		                              foreach ($attendees as $keyat => $valuea)
		                              {
		                               foreach ($valuea as $keyattendee => $valueattendee)
		                               {
		                                    if ($valueattendee['Firstname'] != "") { ?>
		                                         <option value="<?php echo $valueattendee['Id']; ?>">
		                                              <?php echo $valueattendee['Firstname'] . ' ' . $valueattendee['Lastname']; ?>
		                                         </option>
		                                    <?php
		                                    }
		                               }
		                              }
		                            ?>
		                         </optgroup>
		                    </select>
	                    </div>
	                    <?php }else{ $spe_id=array_column($speaker_data,'user_id'); ?>
	                    	<label class="col-sm-2" for="form-field-1" style="padding-right: 0px;">Select Speakers</label>
		                	<div class="col-sm-8 speaker_messages_search">
				                <select name="Receiver_id[]" id="Receiver_id" class="form-control search-select" style="width:100%;" multiple="true">
			                        <optgroup label="Speaker">
			                            <?php
			                              foreach ($speakers as $keysp => $values)
			                              {
			                               foreach ($values as $keyspeaker => $valuespeaker)
			                               {
			                               		if(in_array($valuespeaker['Id'],$spe_id))
			                               		{
				                                    if ($valuespeaker['Firstname'] != "") { ?>
				                                    <option value="<?php echo $valuespeaker['Id']; ?>">
				                                         <?php echo $valuespeaker['Firstname'] . ' ' . $valuespeaker['Lastname']; ?>
				                                    </option>
				                                    <?php
				                                    }
			                                	}
			                               }
			                              }
			                            ?>
			                        </optgroup>
			                    </select>
		                    </div>
	                    <?php } ?>
                    	<div class="col-sm-2"style="padding: 0px;">
	                		<input type="button" id="forwatd_msg" value="Forward" class="btn btn-green">
	                	</div> 
		            </div>    
                </form>
        	</div>
        </div>
    </div>
</div>    
<!-- End: PAGE CONTENT -->
<div class="modal fade" id="replay_model" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Replay Messages</h4>
        </div>
        <form id="commentform" action="<?php echo base_url().'speaker_Messages/addcomment/'.$this->uri->segment(3); ?>" enctype="multipart/form-data" name="commentform" method="post">
	        <div class="modal-body">
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="comment" style="width: 100%;height: 100px;" name="comment" placeholder="Reply"></textarea>
		        		<input type="hidden" value="" name="msg_id" id="msg_id">
		        	</div>	
	        	</div>
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
	                    <label class="col-sm-2" for="form-field-1" style="padding-left: 0px;">Messages Images</label>
	                    <div class="col-sm-9">
	                        <div class="fileupload fileupload-new" data-provides="fileupload">
	                           <div class="fileupload-new thumbnail"></div>
	                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
	                           <div class="user-edit-image-buttons">
	                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
	                                     <input type="file" name="comment_images" id="comment_images">
	                                </span>
	                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
	                                     <i class="fa fa-times"></i> Remove
	                                </a>
	                           </div>
	                        </div>   
	                    </div>
	                </div>
                </div>
                <div class="col-sm-12" id="validation_error" style="display:none;padding-left: 0px;">
                	<div class="alert alert-block alert-danger fade in">
                		<h4 class="alert-heading"><i class="fa fa-times"></i> Validation</h4>
                		<p>Please write something or photo to add comment.</p>
                	</div>
                </div>
	        </div>
	        <div class="modal-footer" style="border: none;">
	          <button type="button" id="replay_btn" class="btn btn-default">Replay</button>	
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="view_full_messages" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Messagess</h4>
        </div>
        <div class="modal-body" id="viewmsg_div">
        	<img src="https://192.168.1.121/EventApp/assets/user_files/14642528811.jpeg" alt="">
        	<p>hiii testing</p>
        </div>
        <div class="modal-footer" style="border: none;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="open_model_div" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center">Messages</h4>
        </div>
        <div class="modal-body" id="view_msg_notes_div">
        	<div class="col-sm-12" id="validation_error_open_div" style="display:none;padding-left: 0px;">
            	<div class="alert alert-block alert-danger fade in">
            		<h4 class="alert-heading"><i class="fa fa-times"></i> Validation</h4>
            		<p id="error_msg">Please write something or photo to add comment.</p>
            	</div>
            </div>
	        <form role="form" id="form" action="" enctype="multipart/form-data" name="form" method="post">
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="mgs_content" style="width: 100%;height: 100px;" name="mgs_content" placeholder="Message Content Goes Here."></textarea>
		        		<input type="hidden" name="msg_id_open_div" id="msg_id_open_div" value="">
		        	</div>	
		        </div>
		        <h4 class="modal-title" align="center" style="margin-bottom:15px; ">Notes on the Message</h4>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="notes_content" style="width: 100%;height: 100px;" name="notes_content" placeholder="Notes Go here."></textarea>
		        	</div>	
		        </div>
		        <h4 class="modal-title" align="center" style="margin-bottom:15px; ">Reply</h4>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="reply_content" style="width: 100%;height: 100px;" name="reply_content" placeholder="Reply message is typed here"></textarea>
		        	</div>	
		        </div>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
	                    <label class="col-sm-2" for="form-field-1" style="padding-left: 0px;">Messages Images</label>
	                    <div class="col-sm-9">
	                        <div class="fileupload fileupload-new" data-provides="fileupload">
	                           <div class="fileupload-new thumbnail"></div>
	                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
	                           <div class="user-edit-image-buttons">
	                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
	                                     <input type="file" name="replay_images" id="replay_images">
	                                </span>
	                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
	                                     <i class="fa fa-times"></i> Remove
	                                </a>
	                           </div>
	                        </div>   
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-12" style="padding-left: 0px;">
	            	<div class="col-sm-2">
	            		<button type="button" onclick="userreplay('0');" class="btn btn-green">Reply privately</button>
	            	</div>
	            	<div class="col-sm-2">
	            		<button type="button" onclick="userreplay('1');" class="btn btn-green">Reply publicly</button>
	            	</div>
	            	<button type="button" onclick="sendnote();" class="btn btn-green">Save</button>
	            </div>
	        </form>    
        </div>
        <div class="modal-footer" style="border: none;">
          <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  TableData.init();
  $('#speakers_msg_list').dataTable( {
      "sDom": "<'row-fluid'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
      "sPaginationType": "bootstrap",
      "bFilter": true,
      "aLengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]],
      iDisplayLength: 10,
      "bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "<?php echo base_url().'speaker_Messages/ajax_manager/'.$event['Id'] ?>",
  });
});
	//$(document).ready(function(){
		$('#replay_btn').click(function(){
			var values = jQuery("input[name='comment_images']");
			if (jQuery.trim(jQuery("#comment").val()) != "" || values.val() != "")
          	{
          		$('#replay_btn').attr('type','submit');
          	}
          	else
          	{
          		$('#validation_error').slideDown('slow');
          		setInterval(function(){ $('#validation_error').slideUp(1000,"linear"); }, 3000);
          	}
		});
		$('#forwatd_msg').click(function(){
			$('#forwatd_msg').val('Forwarding...');
			var table = $('#sample_1').DataTable();
			var data = table.$('input, select').serialize();
			data+='&Receiver_id='+$('#Receiver_id').val();
			$.ajax({
				url:"<?php echo base_url().'speaker_Messages/forwardmsg/'.$this->uri->segment(3); ?>",
				type:'post',
				data:data,
				success:function(result)
				{
					$('#forwatd_msg').val('Forward');
					window.location.href=result;
				}
			});	
		});
		$('#forwatd_order_save').click(function(){
			var table = $('#sample_1').DataTable();
			var data = table.$('input, select').serialize();
			$.ajax({
				url:"<?php echo base_url().'speaker_Messages/forwardordersave/'.$this->uri->segment(3); ?>",
				method:'post',
				data:data,
				success:function(result)
				{
					window.location.href=result;
				}
			});
		});
	//});
	function readmsg(mid)
	{
		$.ajax({
            url: "<?php echo base_url()?>speaker_Messages/readmessages/<?php echo $this->uri->segment(3);?>",
            type: 'POST',
            data:"mid="+mid,
            success:function(result){
            	if($.trim(result)=="success")
            	{
	            	var shortCutFunction ='success';
	            	var msg = 'Messagess Read successfully.';
	                var title = 'Success';
	                var $showDuration = 1000;
	                var $hideDuration = 3000;
	                var $timeOut = 10000;
	                var $extendedTimeOut = 5000;
	                var $showEasing = 'swing';
	                var $hideEasing = 'linear';
	                var $showMethod = 'fadeIn';
	                var $hideMethod = 'fadeOut';
	                toastr.options = {
	                    closeButton: true,
	                    debug: false,
	                    positionClass:'toast-bottom-right',
	                    onclick: null
	                };
	                toastr.options.showDuration = $showDuration;
	                toastr.options.hideDuration = $hideDuration;
	                toastr.options.timeOut = $timeOut;                        
	                toastr.options.extendedTimeOut = $extendedTimeOut;
	                toastr.options.showEasing = $showEasing;
	                toastr.options.hideEasing = $hideEasing;
	                toastr.options.showMethod = $showMethod;
	                toastr.options.hideMethod = $hideMethod;
	                toastr[shortCutFunction](msg, title);
            	}
            }
        });
	}
	function msgidshow(id)
	{
		$('#msg_id').val(id);
	}
	function showfillmag(id,type)
	{
		$.ajax({
			url:"<?php echo base_url().'speaker_Messages/showfullmsg/'.$this->uri->segment(3).'/' ?>"+id+"/"+type,
			success:function(result)
			{
				if(type=='0')
				{
					$('#viewmsg_div').html(result);
				}else{
					$('#msg_id_open_div').val(id);
					$('#mgs_content').text(result);
				}
				/*if(!$('#key_people_'+id).is(":checked"))
				{
					readmsg(id);
					$('#key_people_'+id).prop("checked",true);
				}*/
			}
		});
	}
	function userreplay(rtype)
	{
		var formData = new FormData($('#form')[0]);
		var values = jQuery("input[name='replay_images']");
		if (jQuery.trim(jQuery("#reply_content").val()) != "" || values.val() != "")
      	{
	        $.ajax({
	            url : "<?php echo base_url().'speaker_Messages/userreplay/'.$this->uri->segment(3).'/'; ?>"+rtype,
	            data:formData,
	            type: "POST",  
	            async: true,
	            processData: false,
	            contentType: false,
	            success : function(data)
	            {
	            	var values=data.split('###');
	                if($.trim(values[0])=="success")
	                {
	                	window.location.href=values[1];   
	                }
	                else
	                {
	                	$('#error_msg').html(values[1]);
	                	$('#validation_error_open_div').slideDown('slow');
	          			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	                }    
	            }
	        });
	    }
	    else
	    {
	    	$('#error_msg').html('Please write something or photo to add Replay');
        	$('#validation_error_open_div').slideDown('slow');
  			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	    }
	}
	function sendnote()
	{
		var note=$.trim($('#notes_content').val());
		var msg_id=$.trim($('#msg_id_open_div').val());
		if(note!="" && msg_id!="")
		{
			var data="notes_content="+note+"&msg_id="+msg_id;
			$.ajax({
	            url : "<?php echo base_url().'speaker_Messages/savenote/'.$this->uri->segment(3); ?>",
	            data:data,
	            type: "POST",
	            success : function(data)
	            {
	            	var values=data.split('###');
	                if($.trim(values[0])=="success")
	                {
	                	window.location.href=values[1];   
	                }
	                else
	                {
	                	$('#error_msg').html(values[1]);
	                	$('#validation_error_open_div').slideDown('slow');
	          			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	                }    
	            }
	        });
		}
		else
		{
			if(msg_id==""){
				$('#error_msg').html('Something Want Worng Please Refresh The Page And Try Again');
			}else{
				$('#error_msg').html('Please write something For Notes');
			}
        	$('#validation_error_open_div').slideDown('slow');
  			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);	
		}
	}

</script>

<style type="text/css">
.speaker_messages_search {
    padding-right: 10px;
    padding-left: 0;
    padding-top: 0;
    padding-bottom: 0;
}
.select2-container {
    vertical-align: inherit;
}
</style>