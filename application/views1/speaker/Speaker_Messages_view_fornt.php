<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/theme-default.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
		<!-- end: CORE CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/EM-coreCSS.min.css">
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">
		<!-- <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/favicon.ico" /> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
		
		<style type="text/css">
			.no-js #loader { display: none;  }
			.js #loader { display: block; position: absolute; left: 100px; top: 0; }
			.se-pre-con {
				position: fixed;
				left: 0px;
				top: 0px;
				width: 100%;
				height: 100%;
				z-index: 9999;
				background: url("<?php echo base_url(); ?>assets/images/Preloader_41.gif") center no-repeat #fff;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">

<?php $acc_name=$this->session->userdata('acc_name');
$user = $this->session->userdata('current_user'); ?>
<!-- start: PAGE CONTENT -->
<body><div class="main-wrapper">
				<div class="main-content">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
        	<div class="panel-body">
        		<a class="btn btn-success" href="<?php echo base_url().'Speaker_Messages/exportcsv/'.$this->uri->segment(3); ?>" >Export Csv</a>
        		<form action="<?php echo base_url().'Speaker_Messages/forwardmsg/'.$this->uri->segment(3); ?>" method="post">
	        		<div id="speakers_list" class="table-responsive">
	                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
	                    	<thead>
	                    		<tr>
	                    			<th>#</th>
	                    			<th>Select</th>
	                    			<th>Edit Priority</th>
	                    			<th>
	                    			<?php if($user[0]->is_moderator=='1'){ ?>
	                    			Desired Receiver
	                    			<?php }else{ ?>
	                    			Asec / Desc 
	                    			<?php } ?>
	                    			</th>
	                    			<th>Sender</th>
	                    			<th>Forwarded by</th>
	                    			<th>Messages</th>
	                    			<th>Check if read</th>
	                    			<th>Check if Forward</th>
	                    			<th>Received</th>
	                    			<th>Priority No.</th>
	                    			<th></th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    			<?php for($i=0;$i<count($view_chats1);$i++){ ?>
		                    			<tr>
		                    			<td><?php echo  $i+1; ?></td>
		                    			<td>
	                                        <input type="checkbox" name="msg_forward[]" value="<?php echo $view_chats1[$i]['Id']; ?>" id="forward_key_people_<?php echo $view_chats1[$i]['Id'];?>" class="">
											<label for="forward_key_people_<?php echo $view_chats1[$i]['Id'];?>"></label>
		                    			</td>
		                    			<td align="center">
		                    			<input type="text" style="width:30px; " value="<?php echo $view_chats1[$i]['p_no']; ?>" name="order_to_forward[<?php echo $view_chats1[$i]['Id']; ?>]" id="order_to_forward_<?php echo $view_chats1[$i]['Id']; ?>" /></td>
		                    			<td><?php if($user[0]->is_moderator=='1'){ echo ucfirst($view_chats1[$i]['org_Firstname']).' '.$view_chats1[$i]['org_Lastname']; }else{ echo $view_chats1[$i]['p_no']; } ?></td>
		                    			<td><?php echo ucfirst($view_chats1[$i]['Sendername']); ?></td>
		                    			<td><?php echo ucfirst($view_chats1[$i]['forward_name']); ?></td>
		                    			<td>
		                    				<?php if($view_chats1[$i]['Message']!=""){ ?>
		                    				<a href="javascript:void(0);" onclick='showfillmag("<?php echo $view_chats1[$i]['Id']; ?>","0")' data-toggle="modal" data-target="#view_full_messages"><?php echo substr($view_chats1[$i]['Message'],0,200);?></a>
		                    				<?php }else{ 
		                    					$imgnm=json_decode($view_chats1[$i]['image'])
		                    					?>
		                    				<a href="javascript:void(0);" onclick='showfillmag("<?php echo $view_chats1[$i]['Id']; ?>")' data-toggle="modal" data-target="#view_full_messages"><img src="<?php echo base_url().'assets/user_files/'.$imgnm[0]; ?>" alt="Messages Images" style=" border-radius: 100%;	height: 40px;width: 40px;"></a>
		                    				<?php } ?>
		                    			</td>
		                    			<td>
		                    				<input type="checkbox" <?php if($view_chats1[$i]['isread']=='0'){ ?> checked="checked" <?php } ?> name="msg_read" onchange="readmsg('<?php echo $view_chats1[$i]["Id"];?>')" class="" id="key_people_<?php echo $view_chats1[$i]['Id'];?>">
											<label for="key_people_<?php echo $view_chats1[$i]['Id'];?>"></label>		
		                    			</td>
		                    			<td> 
		                    			<input type="checkbox" disabled="disabled" <?php if(!empty($view_chats1[$i]['forward_id'])){ ?> checked="checked" <?php } ?> name="msg_forward" class="" id="forward_people_<?php echo $view_chats1[$i]['forward_id'];?>">
											<label for="forward_people_<?php echo $view_chats1[$i]['forward_id'];?>"></label>
		                    			</td>
		                    			<td><?php date_default_timezone_set("UTC");
									    $cdate=$view_chats1[$i]['Time'];
									    if(!empty($event_templates[0]['Event_show_time_zone']))
									    {
									        if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
									        { 
									          $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
									          $intoffset=$arr[1]*3600;
									          $intNew = abs($intoffset);
									          $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
									        }
									        if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
									        {
									          $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
									          $intoffset=$arr[1]*3600;
									          $intNew = abs($intoffset);
									          $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
									        }
									    }
		                    			echo $cdate; ?></td>
		                    			<td><?php echo $view_chats1[$i]['priority_no']; ?></td>
		                    			<td>
		                    				<!-- <?php
		                    				 if(array_search($user->Id, array_column($view_chats1[$i]['comment'],'user_id')) != 'false'){ ?><a class="btn btn-xs btn-green tooltips" data-original-title="Replay" data-placement="top" href="javascript:void(0);" onclick='msgidshow("<?php echo $view_chats1[$i]['Id']; ?>");' data-toggle="modal" data-target="#replay_model"><i class="fa fa-share"></i></a><?php }else{ ?><span class="label label-sm label-success "> Replayed </span> <?php } ?> -->
		                    				 <button class="btn btn-success" type="button" data-toggle="modal" data-target="#open_model_div" onclick='showfillmag("<?php echo $view_chats1[$i]['Id']; ?>","1");'>Open</button>	
		                    			</td>
		                    			</tr>
	                    			<?php } ?> 
	                    	</tbody>
	                    </table>
	                </div>
	                <?php if($user[0]->is_moderator!='1'){ ?>
	                <div class="col-sm-12" style="padding: 21px;">
	                	<input type="button" id="forwatd_order_save" value="Save Order" class="btn btn-green">
	                </div>
	                <?php } ?>
	                <div class="row">
	                	<?php if($user[0]->is_moderator!='1'){ ?>
	                	<label class="col-sm-2" for="form-field-1" style="padding-right: 0px;">Select Speakers and Attendees</label>
	                	<div class="col-sm-8 speaker_messages_search">
			                <select name="Receiver_id[]" id="Receiver_id" class="form-control search-select" style="width:100%;" multiple="true">
		                        <option value="">Select</option>
		                        <optgroup label="Speaker">
		                            <?php
		                              foreach ($speakers as $keysp => $values)
		                              {
		                               foreach ($values as $keyspeaker => $valuespeaker)
		                               {
		                                    if ($valuespeaker['Firstname'] != "") { ?>
		                                    <option value="<?php echo $valuespeaker['Id']; ?>">
		                                         <?php echo $valuespeaker['Firstname'] . ' ' . $valuespeaker['Lastname']; ?>
		                                    </option>
		                                    <?php
		                                    }
		                               }
		                              }
		                            ?>
		                        </optgroup>
		                        <optgroup label="Attendee">
		                            <?php
		                              foreach ($attendees as $keyat => $valuea)
		                              {
		                               foreach ($valuea as $keyattendee => $valueattendee)
		                               {
		                                    if ($valueattendee['Firstname'] != "") { ?>
		                                         <option value="<?php echo $valueattendee['Id']; ?>">
		                                              <?php echo $valueattendee['Firstname'] . ' ' . $valueattendee['Lastname']; ?>
		                                         </option>
		                                    <?php
		                                    }
		                               }
		                              }
		                            ?>
		                         </optgroup>
		                    </select>
	                    </div>
	                    <?php }else{ $spe_id=array_column($speaker_data,'user_id'); ?>
	                    	<label class="col-sm-2" for="form-field-1" style="padding-right: 0px;">Select Speakers</label>
		                	<div class="col-sm-8 speaker_messages_search">
				                <select name="Receiver_id[]" id="Receiver_id" class="form-control search-select" style="width:100%;" multiple="true">
			                        <optgroup label="Speaker">
			                            <?php
			                              foreach ($speakers as $keysp => $values)
			                              {
			                               foreach ($values as $keyspeaker => $valuespeaker)
			                               {
			                               		if(in_array($valuespeaker['Id'],$spe_id))
			                               		{
				                                    if ($valuespeaker['Firstname'] != "") { ?>
				                                    <option value="<?php echo $valuespeaker['Id']; ?>">
				                                         <?php echo $valuespeaker['Firstname'] . ' ' . $valuespeaker['Lastname']; ?>
				                                    </option>
				                                    <?php
				                                    }
			                                	}
			                               }
			                              }
			                            ?>
			                        </optgroup>
			                    </select>
		                    </div>
	                    <?php } ?>
                    	<div class="col-sm-2"style="padding: 0px;">
	                		<input type="button" id="forwatd_msg" value="Forward" class="btn btn-green">
	                	</div> 
		            </div>    
                </form>
        	</div>
        </div>
    </div>
</div>    
<!-- End: PAGE CONTENT -->
<div class="modal fade" id="replay_model" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Replay Messages</h4>
        </div>
        <form id="commentform" action="<?php echo base_url().'Speaker_Messages/addcomment/'.$this->uri->segment(3); ?>" enctype="multipart/form-data" name="commentform" method="post">
	        <div class="modal-body">
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="comment" style="width: 100%;height: 100px;" name="comment" placeholder="Reply"></textarea>
		        		<input type="hidden" value="" name="msg_id" id="msg_id">
		        	</div>	
	        	</div>
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
	                    <label class="col-sm-2" for="form-field-1" style="padding-left: 0px;">Messages Images</label>
	                    <div class="col-sm-9">
	                        <div class="fileupload fileupload-new" data-provides="fileupload">
	                           <div class="fileupload-new thumbnail"></div>
	                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
	                           <div class="user-edit-image-buttons">
	                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
	                                     <input type="file" name="comment_images" id="comment_images">
	                                </span>
	                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
	                                     <i class="fa fa-times"></i> Remove
	                                </a>
	                           </div>
	                        </div>   
	                    </div>
	                </div>
                </div>
                <div class="col-sm-12" id="validation_error" style="display:none;padding-left: 0px;">
                	<div class="alert alert-block alert-danger fade in">
                		<h4 class="alert-heading"><i class="fa fa-times"></i> Validation</h4>
                		<p>Please write something or photo to add comment.</p>
                	</div>
                </div>
	        </div>
	        <div class="modal-footer" style="border: none;">
	          <button type="button" id="replay_btn" class="btn btn-default">Replay</button>	
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="view_full_messages" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Messagess</h4>
        </div>
        <div class="modal-body" id="viewmsg_div">
        	<img src="https://192.168.1.121/EventApp/assets/user_files/14642528811.jpeg" alt="">
        	<p>hiii testing</p>
        </div>
        <div class="modal-footer" style="border: none;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="open_model_div" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center">Messages</h4>
        </div>
        <div class="modal-body" id="view_msg_notes_div">
        	<div class="col-sm-12" id="validation_error_open_div" style="display:none;padding-left: 0px;">
            	<div class="alert alert-block alert-danger fade in">
            		<h4 class="alert-heading"><i class="fa fa-times"></i> Validation</h4>
            		<p id="error_msg">Please write something or photo to add comment.</p>
            	</div>
            </div>
	        <form role="form" id="form" action="" enctype="multipart/form-data" name="form" method="post">
	        	<div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="mgs_content" style="width: 100%;height: 100px;" name="mgs_content" placeholder="Message Content Goes Here."></textarea>
		        		<input type="hidden" name="msg_id_open_div" id="msg_id_open_div" value="">
		        	</div>	
		        </div>
		        <h4 class="modal-title" align="center" style="margin-bottom:15px; ">Notes on the Message</h4>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="notes_content" style="width: 100%;height: 100px;" name="notes_content" placeholder="Notes Go here."></textarea>
		        	</div>	
		        </div>
		        <h4 class="modal-title" align="center" style="margin-bottom:15px; ">Reply</h4>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
		        		<textarea id="reply_content" style="width: 100%;height: 100px;" name="reply_content" placeholder="Reply message is typed here"></textarea>
		        	</div>	
		        </div>
		        <div class="col-sm-12" style="padding-left: 0px;">
		        	<div class="form-group">
	                    <label class="col-sm-2" for="form-field-1" style="padding-left: 0px;">Messages Images</label>
	                    <div class="col-sm-9">
	                        <div class="fileupload fileupload-new" data-provides="fileupload">
	                           <div class="fileupload-new thumbnail"></div>
	                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
	                           <div class="user-edit-image-buttons">
	                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
	                                     <input type="file" name="replay_images" id="replay_images">
	                                </span>
	                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
	                                     <i class="fa fa-times"></i> Remove
	                                </a>
	                           </div>
	                        </div>   
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-12" style="padding-left: 0px;">
	            	<div class="col-sm-2">
	            		<button type="button" onclick="userreplay('0');" class="btn btn-green">Reply privately</button>
	            	</div>
	            	<div class="col-sm-2">
	            		<button type="button" onclick="userreplay('1');" class="btn btn-green">Reply publicly</button>
	            	</div>
	            	<button type="button" onclick="sendnote();" class="btn btn-green">Save</button>
	            </div>
	        </form>    
        </div>
        <div class="modal-footer" style="border: none;">
          <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#replay_btn').click(function(){
			var values = jQuery("input[name='comment_images']");
			if (jQuery.trim(jQuery("#comment").val()) != "" || values.val() != "")
          	{
          		$('#replay_btn').attr('type','submit');
          	}
          	else
          	{
          		$('#validation_error').slideDown('slow');
          		setInterval(function(){ $('#validation_error').slideUp(1000,"linear"); }, 3000);
          	}
		});
		$('#forwatd_msg').click(function(){
			$('#forwatd_msg').val('Forwarding...');
			var table = $('#sample_1').DataTable();
			var data = table.$('input, select').serialize();
			data+='&Receiver_id='+$('#Receiver_id').val();
			$.ajax({
				url:"<?php echo base_url().'Speaker_Messages/forwardmsg/'.$this->uri->segment(3); ?>",
				type:'post',
				data:data,
				success:function(result)
				{
					$('#forwatd_msg').val('Forward');
					window.location.href=result;
				}
			});	
		});
		$('#forwatd_order_save').click(function(){
			var table = $('#sample_1').DataTable();
			var data = table.$('input, select').serialize();
			$.ajax({
				url:"<?php echo base_url().'Speaker_Messages/forwardordersave/'.$this->uri->segment(3); ?>",
				method:'post',
				data:data,
				success:function(result)
				{
					window.location.href=result;
				}
			});
		});
	});
	function readmsg(mid)
	{
		$.ajax({
            url: "<?php echo base_url()?>Speaker_Messages/readmessages/<?php echo $this->uri->segment(3);?>",
            type: 'POST',
            data:"mid="+mid,
            success:function(result){
            	if($.trim(result)=="success")
            	{
	            	var shortCutFunction ='success';
	            	var msg = 'Messagess Read successfully.';
	                var title = 'Success';
	                var $showDuration = 1000;
	                var $hideDuration = 3000;
	                var $timeOut = 10000;
	                var $extendedTimeOut = 5000;
	                var $showEasing = 'swing';
	                var $hideEasing = 'linear';
	                var $showMethod = 'fadeIn';
	                var $hideMethod = 'fadeOut';
	                toastr.options = {
	                    closeButton: true,
	                    debug: false,
	                    positionClass:'toast-bottom-right',
	                    onclick: null
	                };
	                toastr.options.showDuration = $showDuration;
	                toastr.options.hideDuration = $hideDuration;
	                toastr.options.timeOut = $timeOut;                        
	                toastr.options.extendedTimeOut = $extendedTimeOut;
	                toastr.options.showEasing = $showEasing;
	                toastr.options.hideEasing = $hideEasing;
	                toastr.options.showMethod = $showMethod;
	                toastr.options.hideMethod = $hideMethod;
	                toastr[shortCutFunction](msg, title);
            	}
            }
        });
	}
	function msgidshow(id)
	{
		$('#msg_id').val(id);
	}
	function showfillmag(id,type)
	{
		$.ajax({
			url:"<?php echo base_url().'Speaker_Messages/showfullmsg/'.$this->uri->segment(3).'/' ?>"+id+"/"+type,
			success:function(result)
			{
				if(type=='0')
				{
					$('#viewmsg_div').html(result);
				}else{
					$('#msg_id_open_div').val(id);
					$('#mgs_content').text(result);
				}
				/*if(!$('#key_people_'+id).is(":checked"))
				{
					readmsg(id);
					$('#key_people_'+id).prop("checked",true);
				}*/
			}
		});
	}
	function userreplay(rtype)
	{
		var formData = new FormData($('#form')[0]);
		var values = jQuery("input[name='replay_images']");
		if (jQuery.trim(jQuery("#reply_content").val()) != "" || values.val() != "")
      	{
	        $.ajax({
	            url : "<?php echo base_url().'Speaker_Messages/userreplay/'.$this->uri->segment(3).'/'; ?>"+rtype,
	            data:formData,
	            type: "POST",  
	            async: true,
	            processData: false,
	            contentType: false,
	            success : function(data)
	            {
	            	var values=data.split('###');
	                if($.trim(values[0])=="success")
	                {
	                	window.location.href=values[1];   
	                }
	                else
	                {
	                	$('#error_msg').html(values[1]);
	                	$('#validation_error_open_div').slideDown('slow');
	          			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	                }    
	            }
	        });
	    }
	    else
	    {
	    	$('#error_msg').html('Please write something or photo to add Replay');
        	$('#validation_error_open_div').slideDown('slow');
  			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	    }
	}
	function sendnote()
	{
		var note=$.trim($('#notes_content').val());
		var msg_id=$.trim($('#msg_id_open_div').val());
		if(note!="" && msg_id!="")
		{
			var data="notes_content="+note+"&msg_id="+msg_id;
			$.ajax({
	            url : "<?php echo base_url().'Speaker_Messages/savenote/'.$this->uri->segment(3); ?>",
	            data:data,
	            type: "POST",
	            success : function(data)
	            {
	            	var values=data.split('###');
	                if($.trim(values[0])=="success")
	                {
	                	window.location.href=values[1];   
	                }
	                else
	                {
	                	$('#error_msg').html(values[1]);
	                	$('#validation_error_open_div').slideDown('slow');
	          			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);
	                }    
	            }
	        });
		}
		else
		{
			if(msg_id==""){
				$('#error_msg').html('Something Want Worng Please Refresh The Page And Try Again');
			}else{
				$('#error_msg').html('Please write something For Notes');
			}
        	$('#validation_error_open_div').slideDown('slow');
  			setInterval(function(){ $('#validation_error_open_div').slideUp(1500,"linear"); }, 3000);	
		}
	}

</script>

<style type="text/css">
.speaker_messages_search {
    padding-right: 10px;
    padding-left: 0;
    padding-top: 0;
    padding-bottom: 0;
}
.select2-container {
    vertical-align: inherit;
}
</style></div></div>
</body>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/agenda/form-validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pages-timeline.js"></script>
                
<script>
	jQuery(document).ready(function() {
		TableData.init();
        FormElements.init();
        FormValidator.init();
        Timeline.init();
	});
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/moment/min/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootbox/bootbox.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/velocity/jquery.velocity.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-mockjax/jquery.mockjax.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/truncate/jquery.truncate.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/subview.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<?php echo $js; ?>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE JAVASCRIPTS  -->
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/jquery.mtz.monthpicker.js"></script>
		<!-- end: CORE JAVASCRIPTS  -->
		<script>
			jQuery(document).ready(function() {
                            
                                options = {
                                    pattern: 'mm-yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
                                    monthNames: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
                                };

                                $('#monthname_text').monthpicker(options);
                            
				Main.init();
				SVExamples.init();
			});
		</script>

		<script>
			setTimeout(function(){$('.demo-popup').addClass('fade');}, 10000);

			var demo = $('#demo');
			var device = $('#devicePreview');   
			var demoFrame = $('#demo iframe');
			var laptopURL = $('.laptopLink');
			var laptopImg = $('.laptopImage');
			var demoLinks = $('#demo-select a');
			var launchLinks = $('#link-select a');
			var launchApp = $('#launch-app');
			var resizeLinks = $('#size-select a');

			launchLinks.click(function(e){
			    frameSrc = $(this).attr('href');
			    launchApp.attr('href', frameSrc);
			 });

			resizeLinks.click(function(e){
			    resizeLinks.removeClass('current');
			    $(this).addClass('current');
			    if ($(this).hasClass('phone')) {
			        demoFrame.removeClass('hide');
			        laptopImg.addClass('hide');
			        device.removeClass('tablet laptop');
			        device.addClass('phone');
			    } else if ($(this).hasClass('tablet')) {
			        demoFrame.removeClass('hide');
			        laptopImg.addClass('hide');
			        device.removeClass('phone laptop');
			        device.addClass('tablet')
			    } else {
			    	demoFrame.addClass('hide');
			    	laptopImg.removeClass('hide');
			        device.removeClass('phone tablet');
			        device.addClass('laptop')
			    }

			});
		</script>