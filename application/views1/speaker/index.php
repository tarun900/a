<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<style type="text/css">
.color_box {
  float: left;
  width: 20px;
  height: 20px;
  border: 1px solid rgba(0, 0, 0, .2);
  margin-right:5px;
}
</style>
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-body" style="padding: 0px;">
                
                <div class="btn-group pull-right" style="padding-right: 10px;">
                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" style="margin-top: 5px;">
                          Add Data <i class="fa fa-arrow-down"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-light pull-right">
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Speaker</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/add_speaker_type/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Speaker Type</a>
                        </li>
                        <?php if(!empty($speaker_list)) { ?>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/exist/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Existing Speaker</a>
                        </li>
                        <?php } ?>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/mass_upload_page/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-upload"></i> Mass Upload</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/add_moderators/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add New Moderators</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/import_speaker/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-download"></i> Import Speaker</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Speaker/downloadDeepLinks/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-download"></i> Get Deep Links</a>
                        </li>
                    </ul>
                </div>

                <?php if($this->session->flashdata('speaker_data')){ ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Speaker <?php echo $this->session->flashdata('speaker_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if($this->session->flashdata('moderator_data')){ ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Moderator <?php echo $this->session->flashdata('moderator_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                         <li class="<?php if(!$this->session->flashdata('moderator_data')){ ?> active <?php } ?>">
                            <a href="#speakers_list" data-toggle="tab">
                                Key People
                            </a>
                        </li>
                        <li class="<?php if($this->session->flashdata('moderator_data')){ ?> active <?php } ?>">
                            <a href="#moderators" data-toggle="tab">
                                Moderators
                            </a>
                        </li>
                        <li>
                            <a href="#speaker_types" data-toggle="tab">
                                Speaker Types
                            </a>
                        </li>
                        <li>
                            <a href="#SpeakerCategories" data-toggle="tab">
                                Speaker Categories
                            </a>
                        </li>
                        <li>
                            <a href="#alphabetical_order" data-toggle="tab">
                                Alphabetical Order
                            </a>
                        </li>
                        <li>
                            <a href="#messaging" data-toggle="tab">
                                Messaging
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						
						<!-- <li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li> -->
                      
                    </ul>
                </div>
          
                <div class="tab-content">
                    <div class="tab-pane fade <?php if(!$this->session->flashdata('moderator_data')){ ?> active in <?php } ?>" id="speakers_list">
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;">
                        </div>
                        <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Select Speaker Category</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="speaker_categories" name="attendee_categories" onchange="get_typepopup(this);">
                                                <option value="">Select Speaker Category</option>
                                                <?php foreach($speaker_categories as $key => $value) { ?>
                                                <option value="<?php echo $value['categorie_keywords'] ?>"><?php echo $value['category'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-green btn-block" id="assign_agenda_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox-table">
                                                <label>
                                                    <input name="selectall_checkbox" type="checkbox" class="flat-grey selectall checkbox_select_all_box">
                                                </label>
                                            </div>
                                        </th>
                                        <th>Name</th>
                                        <th class="hidden-xs">Email</th>
                                        <th>Hide</th>
                                        <th>Status</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($speakers);$i++) { ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="speaker_id[]" value="<?php echo $speakers[$i]['uid']; ?>" id="attendees_id_<?php echo $speakers[$i]['uid'];?>"   class="foocheck">
                                            <label for="attendees_id_<?php echo $speakers[$i]['uid'];?>"></label>    
                                        </td>
                                        <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $speakers[$i]['uid'] .'/'.$event_id; ?>"><?php echo $speakers[$i]['Firstname'].' '.$speakers[$i]['Lastname']; ?></a></td>
                                        <td class="hidden-xs"><?php echo $speakers[$i]['Email']; ?></td>
                                        <td><input class="" type="checkbox" onchange="update_key(<?php echo $speakers[$i]['uid']; ?>)" value="1" id="key_people_<?php echo $speakers[$i]['uid'];?>" <?php if($speakers[$i]['key_people']==1) echo 'checked';?> ></input>
                                        <label for="key_people_<?php echo $speakers[$i]['uid'];?>"></label>
                                        </td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($speakers[$i]['Active']=='1')
                                                    { 
                                                    ?> 
                                                      label-success 
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                        ?> label-danger 
                                                    <?php
                                                    } ?>">
                                                    <?php if($speakers[$i]['Active']=='1')
                                                        { 
                                                        echo "Active"; 
                                                        
                                                        }
                                                        else 
                                                        { 
                                                            echo "Inactive";
                                                        } 
                                                ?>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>Speaker/edit_speaker/<?php echo $event_id."/".$speakers[$i]['uid']; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_speaker(<?php echo $speakers[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade <?php if($this->session->flashdata('moderator_data')){ ?> active in <?php } ?>" id="moderators">
                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th class="hidden-xs">Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($speakers_moderator);$i++) { ?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $speakers_moderator[$i]['uid']; ?>"><?php echo ucfirst($speakers_moderator[$i]['Firstname'].' '.$speakers_moderator[$i]['Lastname']); ?></a></td>
                                        <td class="hidden-xs"><?php echo $speakers_moderator[$i]['Email']; ?></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($speakers_moderator[$i]['Active']=='1')
                                                    { 
                                                    ?> 
                                                      label-success 
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                        ?> label-danger 
                                                    <?php
                                                    } ?>">
                                                    <?php if($speakers_moderator[$i]['Active']=='1')
                                                        { 
                                                           echo "Active"; 
                                                        }
                                                        else 
                                                        { 
                                                            echo "Inactive";
                                                        } 
                                                ?>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>Speaker/edit_moderator/<?php echo $event_id."/".$speakers_moderator[$i]['uid']; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_speaker(<?php echo $speakers_moderator[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="SpeakerCategories">
                            <div class="row">
                                <div class="col-sm-3 pull-right" style="margin-bottom: 10px">
                                    <a href="<?php echo base_url().'Speaker/add_speaker_categorie/'.$event_id; ?>" class="btn btn-green btn-block"><i class="fa fa-plus"></i> Add Speaker Category</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_7">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Speaker Category</th>
                                            <th>Keywords</th>
                                            <th>Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($categorie_data as $key => $value){ ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td><?php echo ucfirst($value['category']); ?></td>
                                            <td><?php echo $value['categorie_keywords']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Speaker/speaker_categories_edit/<?php echo $value['event_id'].'/'.$value['id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_speaker_categorie(<?php echo $value['id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    <div class="tab-pane fade" id="alphabetical_order">
                        <div class="row padding-15">
                            <form class="" method="post" action="<?php echo base_url().'Speaker/saveorderby/'.$this->uri->segment(3); ?>">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input type="radio" value="0" class="square-orange" name="key_people_sort_by" <?php if($event['key_people_sort_by']=='0') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Sort by First Name</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" class="square-orange" name="key_people_sort_by" <?php if($event['key_people_sort_by']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Sort by Last Name</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="speaker_types">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Speaker Type</th>
                                        <th>Position</th>
                                        <th>Hex Color</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody> 
                                    <?php $i=1; foreach ($speaker_type as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo ucfirst($value['type_name']); ?></td>
                                            <td><?php echo ucfirst($value['type_position']); ?></td>
                                            <td><div class="color_box" style="background: <?=$value['type_color']?>"></div><?php echo ucfirst($value['type_color']); ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Speaker/speaker_type_edit/<?php echo $value['event_id'].'/'.$value['type_id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_speaker_type(<?php echo $value['type_id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messaging">
                        <div class="row padding-15">
                            <form class="" method="post" action="<?php echo base_url().'Speaker/saveallow_msg_keypeople_to_attendee/'.$this->uri->segment(3); ?>">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="allow_msg_keypeople_to_attendee" <?php if($event['allow_msg_keypeople_to_attendee']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Allow Attendees to Message Speakers</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="allow_meeting_attendee_to_speaker" <?php if($event['allow_meeting_attendee_to_speaker']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Allow Attendees to Request Meetings with Speakers</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <div class="fileupload-new thumbnail"></div>
                                             <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                             <div class="user-edit-image-buttons">
                                                  <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                       <input type="file" name="Images[]">
                                                  </span>
                                                  <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                       <i class="fa fa-times"></i> Remove
                                                  </a>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    function delete_speaker(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Speaker/delete/"+<?php echo $test; ?>+"/"+id
           
        }
    }
    function delete_speaker_type(id)
    {
         if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Speaker/delete_speaker_type/"+<?php echo $this->uri->segment(3); ?>+"/"+id
        }
    }
    function update_key(id)
    {
           
        if($("#key_people_"+id).is(':checked'))
        {
            data=1;
        }
        else
        {
             data=0;
        }
         <?php $test = $this->uri->segment(3); ?>
            $.ajax({
            url: "<?php echo base_url()?>Speaker/update_keypeople/<?php echo $test;?>",
            type: 'POST',
            data: {id:id,data:data},
            success: function(data) {
                var shortCutFunction ='success';
                            var msg = 'Key people updated successfully.';
                            var title = 'Success';
                            var $showDuration = 1000;
                            var $hideDuration = 3000;
                            var $timeOut = 10000;
                            var $extendedTimeOut = 5000;
                            var $showEasing = 'swing';
                            var $hideEasing = 'linear';
                            var $showMethod = 'fadeIn';
                            var $hideMethod = 'fadeOut';

                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass:'toast-bottom-right',
                                onclick: null
                            };


                            toastr.options.showDuration = $showDuration;
                            toastr.options.hideDuration = $hideDuration;
                            toastr.options.timeOut = $timeOut;                        
                            toastr.options.extendedTimeOut = $extendedTimeOut;
                            toastr.options.showEasing = $showEasing;
                            toastr.options.hideEasing = $hideEasing;
                            toastr.options.showMethod = $showMethod;
                            toastr.options.hideMethod = $hideMethod;
                            
                            toastr[shortCutFunction](msg, title);
            }
        });
    
    }
function delete_speaker_categorie(cid)
{
    if(confirm("Are you sure to delete this?"))
    {
        $.ajax({
                url:"<?php echo base_url(); ?>Speaker/delete_speaker_categorie/"+<?php echo $event['Id']; ?>+"/"+cid,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
    }
}
$('#assign_agenda_btn').click(function(){
    //var table=$('#sample_123').DataTable();
    var chkbox_checked=$('input[name="speaker_id[]"]:checked').length;
    if($('#speaker_categories').val()!="" && chkbox_checked>0)
    {
        $('#assign_agenda_error_div').hide();
        var data = $('input[name="speaker_id[]"]:checked').serialize();
        data=data+"&speaker_categories="+$('#speaker_categories').val();

        $.ajax({
            url : "<?php echo base_url().'Speaker/assign_speaker_category/'.$this->uri->segment(3); ?>",
            data:data,
            type: "POST",
            success : function(data)
            {
                var values=data.split('###');
                if($.trim(values[0])=="success")
                {
                    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                    if(regex .test(values[1]))
                    {
                        //window.location.href=values[1];   
                        window.location.reload();
                    }
                }
                else
                {
                    $('#assign_agenda_error_div').html(values[1]);
                    $('#assign_agenda_error_div').slideDown('slow');
                    setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
                }    
            }
        });
    }
    else
    {
        $('#assign_agenda_error_div').html('Please Select Speaker or Category');
        $('#assign_agenda_error_div').slideDown();
        setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
    }
});
</script>
<!-- end: PAGE CONTENT-->