<?php $k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
$timeformat="H:i"; 
if($time_format[0]['format_time']=='0')
{
  $timeformat="h:i A";
}
$date_format="d/m/Y";
if($event_templates[0]['date_format']=='1')
{
  $date_format="m/d/Y";
}
?>
<div class="row margin-left-10">
    <?php
    if(!empty($user)):
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
          
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'QA/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('50', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id']; ?>");'> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newsponsors.css?'.time(); ?>">
<div class="agenda_content">
  <div>
    <div class="input-group search_box_user">
      <i class="fa fa-search search_user_icon"></i>    
      <input type="text" class="form-control" id="search_user_content" placeholder="<?php echo $event_templates[0]['default_lang']['50__search_q&a']; ?>" style="width:100%">
    </div>
  </div>
  <div class="panel-white-new">
    <div class="margin-right-0">
      <?php if(!empty($qa_session)) { ?>
      <div class="gold1">
        <ul class="activities">
          <?php foreach ($qa_session as $key => $value) { ?> 
          <li>
            <a href="<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/view/'.$value['Id']; ?>" class="activity">
              <div class="desc">
                <p class="logo-img" style="margin-top: 20px;">
                  <img src="<?php echo base_url().'assets/images/QASessionListIcon.png'; ?>"/>
                </p>
                <h4>
                  <span class="user_container_name"><?php echo ucfirst($value['Session_name']); ?></span>
                  <div>
                    <h5><?php echo date($timeformat,strtotime($value['start_time']))." until ".date($timeformat,strtotime($value['end_time'])); ?></span>
                    <h5><?php echo date($date_format,strtotime($value['session_date'])); ?></span>
                  </div>
                </h4>
              </div>
            </a>
          </li>
          <?php } ?>
        </ul>
      </div>
      <?php } else{ ?>
      <div class="tab-content">
        <span>No Q&A Session for this event.</span>
      </div>
      <?php } ?>
    </div>
  </div>
</div>  
<script type="text/javascript">
jQuery("#search_user_content").keyup(function()
{
  jQuery(".activities li").each(function( index ){
    var str=jQuery(this).find('.user_container_name').html();
    if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
    {
      var str1=jQuery.trim(jQuery(this).find('.user_container_name').text().toLowerCase());
      var content=jQuery.trim(jQuery("#search_user_content").val().toLowerCase());
      if(content!=null)
      {
        var n = str1.indexOf(content);                    
        if(n!="-1")
        {
          jQuery(this).css('display','block');
        }
        else
        {
          jQuery(this).css('display','none');
        }
      }
    }
  });
});

</script>
