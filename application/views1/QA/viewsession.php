<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
$timeformat="H:i"; 
if($time_format[0]['format_time']=='0')
{
  $timeformat="h:i A";
}
$date_format="d/m/Y";
if($event_templates[0]['date_format']=='1')
{
  $date_format="m/d/Y";
} 
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<div class="panel panel-white" id="qa_section_msg">
	<div class="row" style="border-bottom: 3px Solid #A9A9A9;">
		<div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color:#A9A9A9;">
		<a class="sp-full-screen-button sp-fade-full-screen" style="cursor: pointer;margin-right: 10px;"><i class="fa fa-expand fa-2x" style="margin-top: 10px;"></i></a>
		</div>
		<div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color:#A9A9A9;">
			<h3 class="qa_session_name" style="color:#FFFFFF;font-size: 60px;"><?php echo ucfirst($qa_session[0]["Session_name"]); ?></h3>
		</div>
		<div class="qa_session_light_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color:#D3D3D3;">
			<p class="qa_session_desc" style="font-size: 40px;">
				<?php echo html_entity_decode($qa_session[0]["Session_description"]); ?>
			</p>
			<label class="qa_session_datetime" style="font-size: 40px;">
				<?php echo date($date_format,strtotime($qa_session[0]["session_date"])); ?>
				<span><?php echo $event_templates[0]['default_lang']['50__start']; ?> :</span><?php echo date($timeformat,strtotime($qa_session[0]["start_time"])); ?>
				<span><?php echo $event_templates[0]['default_lang']['50__end']; ?> -</span><?php echo date($timeformat,strtotime($qa_session[0]["end_time"])); ?>
			</label>
		</div>
	</div>
	<div class="qa_session_refresh_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin: 10px 0px;">
		<div class="col-sm-3 col-md-3 col-lg-3 pull-left">
			<a href="javascript:void(0);" onclick="opensendmshpopup();" class="btn btn-success btn-block" style="font-size: 22px;background-color: <?php echo $event_templates[0]['Top_background_color']; ?>"><?php echo $event_templates[0]['default_lang']['50__ask_a_question']; ?> <img width="20" height="20" src="<?php echo base_url(); ?>assets/images/QASessionListIcon.png"></a>
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
			<a href="javascript:void(0);" onclick="window.location.reload();" class="btn btn-success btn-block" style="font-size: 22px;"><?php echo $event_templates[0]['default_lang']['50__refresh']; ?> <img width="20" height="20" src="<?php echo base_url(); ?>assets/images/refreshlogo.png"></a>
		</div>
	</div>
	<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
		<h5><?php echo $event_templates[0]['default_lang']['50__ask_your_own_question_or_vote_other_to_the_top']; ?></h5>
	</div> -->
	<center>
		<div class="loader-j">
	    	<img src="https://www.allintheloop.net/demo/assets/user_files/ajax-loader-j.gif">
		</div>
	</center>
	<div class="question_main_container" id="qa_session_question_main_container" style="margin-top: 20px;">
		<?php foreach ($qa_session_question as $key => $value) { ?>
		<div class="message_container col-xs-12 col-sm-12 col-md-12 col-lg-12" id="message_container__<?php echo $value['message_id']; ?>" data-sort="<?php echo $value['votes']?: '0'; ?>">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<div class="msg_main_body">
					<div class="message_img">
						<?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?> 
						<img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
						<?php }else{ ?>
						<img src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
						<?php } ?>
					</div>
					<div class="msg_fromname" style="font-size: 40px;">
						<a style="font-size: 40px;" href="javascript:void(0);"><?php echo ucfirst($value['user_name']); ?></a>
					</div>
				</div>
				<div class="msg_message" style="font-size: 40px;">
					<?php echo $value['Message']; ?>
				</div>
			</div>
			<!-- <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="text-align: center;">
				<?php if(!empty($user[0]->Id)){ if($qa_session[0]['Moderator_speaker_id']==$user[0]->Id){ ?>
				<a href="<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/delete_qa_question/'.$value['message_id']; ?>" onclick='return confirm("Are You Sure Delete This Question ?");' class="btn btn-red btn-block">Delete</a>
				<?php }else{ ?>
				<a href="javascript:void(0);" onclick='vote("<?php echo $value['message_id']; ?>");'><img width="100" height="100" id="unvote_img__<?php echo $value['message_id']; ?>" src="<?php echo base_url().'assets/images/unvote.png'; ?>" <?php if(in_array($user[0]->Id,array_filter(explode(",",$value['users_id'])))){ ?> style="display: none;" <?php } ?>></a>
				<a href="javascript:void(0);" onclick='unvote("<?php echo $value['message_id']; ?>");'><img width="100" height="100" id="vote_img__<?php echo $value['message_id']; ?>" <?php if(!in_array($user[0]->Id,array_filter(explode(",",$value['users_id'])))){ ?> style="display: none;" <?php } ?> src="<?php echo base_url().'assets/images/vote.png'; ?>"></a>
				<?php } } ?>
				<h3 style="text-align: center;" id="total_vote__<?php echo $value['message_id']; ?>">
					<?php echo $value['votes']?: '0'; ?>
				</h3>
			</div> -->
		</div>
		<?php } ?>
	</div>
</div>
<div id="sendquestionpopup" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="border: none;">
				<button type="button" class="close" data-dismiss="modal">
					<img width="20" height="20" src="<?php echo base_url().'assets/images/close_popup.png'; ?>">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="imageform" method="post" enctype="multipart/form-data" action='' style="">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<textarea style="width: 100%;height: 150px;" id="Message" placeholder="<?php echo $event_templates[0]['default_lang']['50__ask_your_own_question_or_vote_other_to_the_top']; ?>..." name="Message"></textarea>
								<span id="question_error_msg_span" class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<label class="checkbox-inline">
                            		<input type="checkbox" value="1" class="grey" name="anonymous_user" id="anonymous_user">&nbsp;
                            		<span><?php echo $event_templates[0]['default_lang']['50__anonymous']; ?></span>
                        		</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0 auto;display: inline-block;float: none;">
								<a href="javascript:void(0)" onclick="sendmessage()" id="sendbtn" class="btn btn-green btn-block">
									Send <i class="fa fa-arrow-circle-right"></i>
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript">
$(window).load(function(){
    $('body').attr('class',"sidebar-close");
	$('.loader-j').hide();
    $('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
      return $(b).data('sort') - $(a).data('sort');
        }).each(function (_, message_container_question) {
      $(message_container_question).parent().append(message_container_question);
    });  
    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:#D3D3D3;");
    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    {
    	$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");
    }*/
});
function opensendmshpopup()
{
	$("#Message").val('');
	$('#anonymous_user').iCheck('uncheck');
	$('#question_error_msg_span').parent().parent().removeClass('has-error').removeClass('has-success');
	$('#question_error_msg_span').hide();
	$('#sendquestionpopup').modal('show');
}
function sendmessage()
{
	$("#sendbtn").html('Sending <i class="fa fa-refresh fa-spin"></i>');
	if($.trim($("#Message").val())=="")
	{
		$('#question_error_msg_span').html("Please write Message...");
		$('#question_error_msg_span').parent().parent().removeClass('has-success').addClass('has-error');
		$('#question_error_msg_span').show();
    	$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
	}
	else
	{
		$('#question_error_msg_span').parent().parent().removeClass('has-error').addClass('has-success');
		$('#question_error_msg_span').hide();
		var str = jQuery("#imageform").serialize();
		$.ajax({
			url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/send_message/'.$sid; ?>",
        	data: str,
        	type: "POST",
        	async: true,
        	success: function(result)
        	{
        		$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
        		$('#Message').val('');
        		$('#anonymous_user').iCheck('uncheck');
        		window.location.reload();
        	}
		});
	}
}
function vote(mid)
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/change_question_vote/'; ?>"+mid,
    	type: "POST",
    	async: true,
    	success: function(result)
    	{
    		$('#vote_img__'+mid).show();
			$('#unvote_img__'+mid).hide();
			var total_vote=parseInt($('#total_vote__'+mid).html());
			$('#total_vote__'+mid).html(total_vote+1);
			$('#message_container__'+mid).data('sort',total_vote+1);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:#D3D3D3;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");
    		}*/
    	}
	});
}
function unvote(mid)
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/change_question_vote/'; ?>"+mid,
    	type: "POST",
    	async: true,
    	success: function(result)
    	{
			$('#unvote_img__'+mid).show();
			$('#vote_img__'+mid).hide();
			var total_vote=parseInt($('#total_vote__'+mid).html());
			$('#total_vote__'+mid).html(total_vote-1);
			$('#message_container__'+mid).data('sort',total_vote-1);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:#D3D3D3;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");  
    		}*/  
		}
	});     
}
$(document).ready(function() {

  setInterval(function() {
    load_qa();
  }, 10000);
});

function load_qa()
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/view/'.$this->uri->segment(5); ?>",
    	type: "POST",
    	async: true,
    	beforeSend: function() {
		   //$('.loader-j').show();
		},
    	success: function(result)
    	{	
    		$('.loader-j').hide();
		   	$('#qa_session_question_main_container').empty();
			$('#qa_session_question_main_container').append(result);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:#D3D3D3;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");  
    		}*/
		}
	});     
}
$('.sp-full-screen-button').click(function(){
		if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
	   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
	    if (document.documentElement.requestFullScreen) {  
	      document.documentElement.requestFullScreen();  
	    } else if (document.documentElement.mozRequestFullScreen) {  
	      document.documentElement.mozRequestFullScreen();  
	    } else if (document.documentElement.webkitRequestFullScreen) {  
	      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
	    }
	    $('header').hide();  
	  } else {  
	    if (document.cancelFullScreen) {  
	      document.cancelFullScreen();  
	    } else if (document.mozCancelFullScreen) {  
	      document.mozCancelFullScreen();  
	    } else if (document.webkitCancelFullScreen) {  
	      document.webkitCancelFullScreen();  
	    }
	    $('header').show();  
	  }
	}); 
</script> 
