<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#session_list" data-toggle="tab">
                            Edit Rule
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="session_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Rule Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <input type="text" placeholder="Rule Name" name="rule_name" class="form-control required name_group" value="<?=$rule['rule_name']?>">
                                </div>
                            </div>
                            <span><p style="text-align: center;width: 60%;font-weight: 700;font-size: 24px;">If</p></span>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Profile Type <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control required" name="profile_type_if" id="profile_type_if">
                                        <option>Select Profile Type</option>
                                        <?php foreach($module as $key => $value): if($value['id'] != '43' && $value['id'] != '1'):?>
                                            <option value="<?=$value['id']?>" <?= ($rule['profile_type_if'] == $value['id']) ? 'selected=selected' : ''?>><?=$value['menuname']?></option>
                                        <?php endif;endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Field <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control required" name="field_if" id="field_if">
                                        <option value="">Select Field</option>
                                        <option <?= ($rule['field_then'] == 'Email') ? 'selected=selected' : ''?>>Email</option>
                                        <option <?= ($rule['field_then'] == 'Keywords') ? 'selected=selected' : ''?>>Keywords</option>
                                    </select>
                                </div>
                            </div>
                            <span><p style="text-align: center;width: 60%;font-weight: 700;font-size: 24px;">Contains</p></span>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;">
                                    Contains <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <input type="text" name="contains" class="form-control required" placeholder="Contains" value="<?=$rule['contains']?>">
                                </div>
                            </div>
                            <span><p style="text-align: center;width: 60%;font-weight: 700;font-size: 24px;">Suggest</p></span>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Modules <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control required" name="profile_type_then" id="profile_type_then">
                                        <option>Select Modules</option>
                                        <?php foreach($module as $key => $value):?>
                                            <option value="<?=$value['id']?>" <?= ($rule['profile_type_then'] == $value['id']) ? 'selected=selected' : ''?>><?=$value['menuname']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Field <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control required" name="field_then" id="field_then">
                                        <?php if($rule['profile_type_then'] == '1'):
                                            foreach ($agenda as $key => $value) {
                                                $selected = $rule['field_then'] == $value['Id'] ? 'selected=selected' : '';
                                                echo '<option value="' . $value['Id'] . '" ' . $selected . '>' . $value['category_name'] . '</option>';
                                            }
                                            else: ?>
                                            <option value="">Select Field</option>
                                            <option <?= ($rule['field_then'] == 'Email') ? 'selected=selected' : ''?>>Email</option>
                                            <option <?= ($rule['field_then'] == 'Keywords') ? 'selected=selected' : ''?>>Keywords</option>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>
                            <div id="contain">
                                <span><p style="text-align: center;width: 60%;font-weight: 700;font-size: 24px;">That Contains</p></span>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;">
                                        That Contains <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="text" name="contains_that" id="contains_that" class="form-control required" placeholder="Contains" value="<?=$rule['contains_that']?>" id="contains_that">
                                    </div>
                                </div>
                            </div>
                                <div class="form-group">    
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-2">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach ($agenda as $key => $value){   
    $tmp_options .='<option value="'.$value['Id'].'">'.$value['category_name'].'</option>';
}
$tmp_options_a = '<option value="">Select Field</option><option>Email</option><option>Keywords</option>';
foreach ($custom_column as $key => $value) {
    $tmp_options_a .= '<option value="' . $value['column_name'] . '">' . $value['column_name'] . '</option>';
}
?>
<script type="text/javascript">
    $('#profile_type_if').change(function(){
        var val = $(this).val();
        if(val == '2')
        {   
            $("#field_if").html('<?php echo $tmp_options_a;?>');
        }
        else
        {
            $("#field_if").html('<option value="">Select Field</option><option>Email</option><option>Keywords</option>');
        }
    });
    $('#profile_type_then').change(function(){
        var val = $(this).val();
        if(val == '43')
        {   
            $("#field_then").html("<option>Keywords</option>");
        }
        else if(val == '1')
        {
            $("#field_then").html('<?php echo $tmp_options;?>');
        }
        else
        {
            $("#field_then").html("<option>Email</option><option>Keywords</option>");
        }
    });
</script>