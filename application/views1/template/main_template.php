<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->

<html>
<!--<html manifest="example.appcache">-->
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<!-- <?php echo $event_templates[0]['Event_name']; ?> -->
		<title><?php echo $pagetitle; ?> - ALL IN THE LOOP</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />

		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<?php echo $css; ?>
        <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/theme-default.css?<?php echo time(); ?>" type="text/css" id="skin_color">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
		<!-- end: CORE CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/EM-coreCSS.min.css">
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
		
		<style type="text/css">
			.no-js #loader { display: none; }
			.js #loader {display: block; position: absolute; left: 100px; top: 0;}
			.se-pre-con {position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; 
				background: url("<?php echo base_url(); ?>assets/images/Preloader_41.gif") center no-repeat #fff;
			}
		</style>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
   
		<style type="text/css">
		#myModal { display: none; }
		</style>

		<!-- <div class="se-pre-con"></div> -->
		<div id="result"></div>
		<!-- start: SLIDING BAR (SB) -->
		<div id="slidingbar-area">
		</div>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
				<!-- start: TOPBAR CONTAINER -->
				<div class="container">
					<?php echo $header; ?>
				</div>
				<!-- end: TOPBAR CONTAINER -->
			</header>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<a class="closedbar inner hidden-sm hidden-xs" href="#">
			</a>
			<nav id="pageslide-left" class="pageslide inner">
				<?php echo $sidebar; ?>
			</nav>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<div id="pageslide-right" class="pageslide slide-fixed inner">
				<?php echo $right_sidebar; ?>
			</div>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<?php echo $inner_toolbar; ?>
						<!-- end: TOOLBAR -->					
						<!-- end: PAGE HEADER -->
						
						<!-- start: BREADCRUMB -->
						<?php echo $breadcrumb; ?>
						<!-- end: BREADCRUMB -->
						<?php echo $content; ?>
						<div class="subviews">
							<div class="subviews-container"></div>
						</div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<!-- start: FOOTER -->
			<footer class="inner">
				<div class="footer-inner">
					<?php echo $footer; ?>
				</div>
			</footer>
			<!-- end: FOOTER -->
			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php echo @$subview; ?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/moment/min/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootbox/bootbox.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/velocity/jquery.velocity.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-mockjax/jquery.mockjax.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<?php if($is_news_event): ?>
		<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dt-1.10.js"></script>
		<?php else: ?>
		<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<?php endif; ?>
		<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/truncate/jquery.truncate.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<!--<script src="<?php echo base_url(); ?>assets/js/subview.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>-->
		<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<?php echo $js; ?>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE JAVASCRIPTS  -->
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/jquery.mtz.monthpicker.js"></script>
		<!-- end: CORE JAVASCRIPTS  -->
		<script>
			$(document).ready(function() {
				$('.panel-tools').remove();
			    if (location.hash) {
			        $("a[href='" + location.hash + "']").tab("show");
			        $("a[href='" + location.hash + "']").parent().parent().closest('.active').find('span').text($("a[href='" + location.hash + "']").text());
			    }
			    $(document.body).on("click", "a[data-toggle]", function(event) {
			    	var str = this.getAttribute("href");
			    	if (!str.toLowerCase().includes("javascript") && str!="#")
			        	location.hash = str;
			    });
			});
			$(window).on("popstate", function() {
			    var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
			    $("a[href='" + anchor + "']").tab("show");
			});
			jQuery(document).ready(function() {
                            
                                options = {
                                    pattern: 'mm-yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
                                    monthNames: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
                                };

                                $('#monthname_text').monthpicker(options);
                            
				Main.init();
				SVExamples.init();
				
			});
			$(".pagination a").on('click',function(e){
	            e.preventDefault();
	            var url = $(this).attr('href');
	            var last = url.substr(url.length - 1);
	            var get = "<?php echo ($_SERVER['QUERY_STRING']!='') ? '?'.$_SERVER['QUERY_STRING'] : '' ?>";
	            
	            if(last == '/')
	              window.location.href = $(this).attr('href')+"0/"+concateQueryString();
	            else
	              window.location.href = $(this).attr('href')+"/"+concateQueryString();
	          })
			function chnagePagination(value)
                    {
                        var last = value.substr(value.length - 1);
                        if(last == '/')
                            window.location.href = value+"0/"+concateQueryString();
                        else
                            window.location.href = value+"/"+concateQueryString();
                    }
                    function changeOrder(column,order='asc')
                    {
                        var order_by = getQueryStringValue('order_by');
                        var order_by_order = (order_by == column) ? (getQueryStringValue('order')=='asc') ? 'desc': 'asc' : 'asc';
                        window.location.href=window.location.href.split('?')[0]+'?order_by='+column+'&order='+order_by_order;
                    }
                    function getQueryStringValue (key) {
                        return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
                    }
                    function concateQueryString()
                    {
                        return (window.location.href.split('?')[1]!=undefined) ? '?'+window.location.href.split('?')[1] :'';
                    }
                    
		</script>

		<script>
			setTimeout(function(){$('.demo-popup').addClass('fade');}, 10000);

			var demo = $('#demo');
			var device = $('#devicePreview');   
			var demoFrame = $('#demo iframe');
			var laptopURL = $('.laptopLink');
			var laptopImg = $('.laptopImage');
			var demoLinks = $('#demo-select a');
			var launchLinks = $('#link-select a');
			var launchApp = $('#launch-app');
			var resizeLinks = $('#size-select a');

			launchLinks.click(function(e){
			    frameSrc = $(this).attr('href');
			    launchApp.attr('href', frameSrc);
			 });

			resizeLinks.click(function(e){
			    resizeLinks.removeClass('current');
			    $(this).addClass('current');
			    if ($(this).hasClass('phone')) {
			        demoFrame.removeClass('hide');
			        laptopImg.addClass('hide');
			        device.removeClass('tablet laptop');
			        device.addClass('phone');
			    } else if ($(this).hasClass('tablet')) {
			        demoFrame.removeClass('hide');
			        laptopImg.addClass('hide');
			        device.removeClass('phone laptop');
			        device.addClass('tablet')
			    } else {
			    	demoFrame.addClass('hide');
			    	laptopImg.removeClass('hide');
			        device.removeClass('phone tablet');
			        device.addClass('laptop')
			    }

			});

		</script>

	</body>
	<!-- end: BODY -->
</html>