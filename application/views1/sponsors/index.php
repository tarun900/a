<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="btn-group pull-right" style="padding-right: 10px;">
                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" style="margin-top: 5px;">
                          Add Data <i class="fa fa-arrow-down"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-light pull-right">
                        <li>
                            <a href="<?php echo base_url().'Sponsors/add_group/'.$this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Group</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Sponsors/add_page/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add Sponsors</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>Sponsors/add_sponsor_type/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add New Sponsor Type</a>
                        </li>
                        <li>
    				        <a href="<?php echo base_url(); ?>Sponsors/mass_upload_page/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Mass Upload</a>
                        </li>
                    </ul>
                </div>
                <?php if($this->session->flashdata('sponsors_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Sponsor Content <?php echo $this->session->flashdata('sponsors_data'); ?> Successfully.
                </div>
                <?php } ?>

                 <?php if ($this->session->flashdata('ex_in_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Sponsor <?php echo $this->session->flashdata('ex_in_data'); ?> Successfully.
                    </div>
                <?php } ?>

                 <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#view_group" data-toggle="tab">
                                Groups
                            </a>
                        </li>
                        <li class="">
                            <a href="#sponsors_list" data-toggle="tab">
                                Sponsors List
                            </a>
                        </li>
						
						<li>
                            <a href="#sponsors_types" data-toggle="tab">
                                Sponsor Types
                            </a>
                        </li>

                        <?php if($user->Role_name=='Client'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
						
                    </ul>
                </div>  
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="view_group">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($module_group_list as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1; ?></td>
                                        <td><?=ucfirst($value['group_name']); ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Sponsors/edit_group/<?php echo $this->uri->segment(3).'/'.$value['module_group_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url().'Sponsors/delete_group/'.$this->uri->segment(3).'/'.$value['module_group_id']; ?>" onclick="return confirm('Are You Sure Delete This Group');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="sponsors_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sponsor Name</th>
                                        <th>Edit or Delete</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($sponsors);$i++) { ?>
                                    <tr>
                                        <td><?php echo $sponsors[$i]['Company_name']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Sponsors/page_edit/<?php echo $sponsors[$i]['Event_id'].'/'.$sponsors[$i]['Id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_sponsors(<?php echo $sponsors[$i]['Id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
					
					<div class="tab-pane" id="sponsors_types">
						<div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sponsor Type Name</th>
                                        <th>Position</th>
                                        <th>Hex Color</th>
                                        <th>Type Code</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody> 
                                    <?php $i=1; foreach ($sponsors_type as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo ucfirst($value['type_name']); ?></td>
                                            <td><?php echo ucfirst($value['type_position']); ?></td>
                                            <td><?php echo ucfirst($value['type_color']); ?></td>
                                            <td><?php  echo $value['type_ucode']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Sponsors/sponsors_type_edit/<?php echo $value['event_id'].'/'.$value['type_id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_sponsors_type(<?php echo $value['type_id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
					</div>

                    <div class="tab-pane" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                       <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script>
     jQuery(document).ready(function() 
     {
          TableData.init();
     });
</script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() 
{
    $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
});
</script>
<script type="text/javascript">
  
    function delete_user(id,eid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Sponsors/delete/"+eid+ "/" + id;
        }
    }

    function delete_sponsors(id,Event_id)
    {   
        <?php $Event_id = $event['Id']; ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Sponsors/delete_Sponsors/"+<?php echo $Event_id; ?>+"/"+id
        }
    }
    function delete_sponsors_type(id)
    {
         if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Sponsors/delete_sponsors_type/"+<?php echo $event['Id']; ?>+"/"+id
        }
    }
</script>
<script type="text/javascript">

    
    function preview1(strid)
    {
        
         //$('.display_notification').click();
         var Content=$("#"+strid+" #Content").code();
         if(strid=='form')
         {
            var fname=$("#first_name").val();
            var lname=$("#last_name").val();
            var Subject=$("#"+strid+" #subject1").val();   
         }
         else
         {
            var fname="{{fname}}";
            var lname="{{lname}}";
            var Subject=$("#"+strid+" #subject").val();   
         }
        
         var to="{{Senderemail}}";
       

         var Sendername=$("#"+strid+" #sent_from_name").val();
         var Senderemail=$("#"+strid+" #sent_from").val();
         var cont="<p><br>"+fname+" "+lname+""+"</p>";
         var cont2="<br/><a href='<?php echo base_url(); ?>'> Click here</a> to register<br/>";
         var cont3="<br/>Thank You"+"<br>"+Sendername+"<br/>";
         var data="<div style='padding:10px;'><strong>Subject:</strong>&nbsp;"+Subject+"<br>"+cont+Content+cont2+cont3+"</div>";
         $("#co").html(data);
        
     
    }
    </script>
<!-- end: PAGE CONTENT-->