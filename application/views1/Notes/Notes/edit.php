<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
<div id="boxes">
  <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window"> Edit Notes
  <div id="msg" style="display:none">
Updated Successfully
  </div>
  <?php  $acc_name = $this->session->userdata('acc_name');?>
  <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div id="lorem">
      <div class="panel-body notes_popup">
            <!-- <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
              -->   <div id="notes_edit">
                    <div class="form-group clearfix">
                        <label class="control-label col-sm-2" for="form-field-1">
                            Heading
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Heading" id="Heading" name="Heading" value="<?php echo $notes_by_id[0]['Heading']; ?>" class="form-control name_group">
                        </div>
                        <div id="error_heading">
                        </div>
                    </div>
                     <div class="form-group clearfix">
                        <label class="control-label col-sm-2" for="form-field-1">
                            Description
                        </label>
                        <div class="col-sm-9">
                            <textarea style="width:100%;height:200px;" placeholder="Description" id="summernote" name="Description" class="summernote"><?php echo $notes_by_id[0]['Description']; ?></textarea>
                        </div>
                        <div id="error_desc">
                        </div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-green btn-block" id="btn" type="submit" onclick="edit_data(<?php echo $id ?>)">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                         </div>
                    </div>
                </div>
            <!-- </form> -->
            <input type="hidden" value="<?php  echo $close_flag;?>" id="close1">
        </div> 
    </div>
  </div>
  <div style="width: 1478px; font-size: 32pt; color:white; height: 602px; display: none; opacity: 0.8;" id="mask"></div>
</div>
<script type="text/javascript">
var myVar;
       function edit_data(id)
        {
            var validate=true;
            $('#error_desc').html('');
            $('#error_heading').html('');
            if($('#Heading').val()=='')
            {
                $('#error_heading').html('&nbsp <i class="fa fa-warning"></i>&nbsp Please Enter Heading');
                validate=false;
            }
            if($('#summernote').val()=='')
            {
                $('#error_desc').html('&nbsp <i class="fa fa-warning"></i>&nbsp Please Enter Description');
                 validate=false;
            }
            if(validate==true)
            {
                 $('#error_desc').html('');
                 $('#error_heading').html('');
                 var flag1=<?php echo $close_flag?>;
                 $.ajax({
                    url: "<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/submit/"+id,
                    data: 'Heading=' +$('#Heading').val()+"&Description="+$('#summernote').val(),
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                         $("#msg").show();
                         var ajax=0;
                         if(flag1==1)
                         {
                            myVar = setInterval(function(){ hello() }, 2200);
                         }
                         else
                         {
                            setInterval(function(){
                            window.location.href="<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>";
                             },2500);
                        }

                    }
    
               });
            }
           
    
        }
   $(function() {
       $('#close').click(function() {
            $.ajax({
                    url: "<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/notes_close/"+<?php echo $id;?>,
                    data: 'flag='+<?php echo $close_flag?>,
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                        var values=result.split('###');
                        if($.trim(values[0])=="sucess")
                        {
                            window.location.href="<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>";
                        }
                    }
    
               });


          });
       });  
   function hello()
   {
       $('#boxes').hide();
       myStopFunction();
   }
   function myStopFunction() 
   {
      clearInterval(myVar);                        
   } 
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main1.js"></script>
<?php if($close_flag==1){ die; }?>

