  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
	body {
  width: 80%;
  margin: 30px auto;
  font-family: sans-serif;
}

h3 {
  text-align: center;
  font-size: 1.65em;
  margin: 0 0 30px;
}

div {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between; /* or space-around */
}

a {
  display: inline-block;
  margin-bottom: 8px;
  width: 100%;
  text-decoration: none;
  color: black;
  border: solid 1px #EEEEEE;

}

@media screen and (min-width: 50em) {
  a {
        width: calc(30% - 10px);
        border: solid 1px #EEEEEE;
        height: 350px;

  }
}

a:hover img {
  transform: scale(1.15);
}

figure {
  margin: 0;
  overflow: hidden;
}

figcaption {
  margin-top: 15px;
    padding: 10px;
}

img {
  border: none;
  width: 100%;
  height: 50%;
  display: block;
  background: #ccc;
  transition: transform .2s ease-in-out;
}

.p a {
  display: inline;
  font-size: 13px;
  margin: 0;
  text-decoration: underline;
  color: blue;
}

.p {
  text-align: center;
  font-size: 13px;
  padding-top: 100px;
}
</style>


<div>
	<?php foreach ($data as $key => $value) { ?>
	  <a style="cursor: text">
	    <figure>
	    <?php $id = explode('/', $value['url']); ?>
	    <a class="open_video" data-id="<?php echo $value['url']; ?>">
			<img  src="https://img.youtube.com/vi/<?php echo $id[count($id)-1]; ?>/hqdefault.jpg" >
		</a>
	      <figcaption>
	      	<h4 style="max-height: 72px;"><?php echo (strlen($value['title']) > 30) ? substr($value['title'], 0,30) ."..." : $value['title']; ?></h4>
	       <?php echo $value['desc']; ?>
	      </figcaption>
	    </figure>
	  </a>
  <?php } ?>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
	$(".open_video").on('click',function(){
		var src_url = $(this).data('id');
 			$('<iframe />');  // Create an iframe element
      $('<iframe />', {
          name: 'frame1',
          id: 'frame1',
          src: src_url,
      }).appendTo('.modal-body');
      $("#myModal").modal('show');
	})
</script>