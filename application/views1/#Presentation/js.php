<?php if($presentations[0]['Auto_slide_status'] =='1') { ?>
  <script type="text/javascript">
  var my_autoplay = true;
  </script>
<?php } else { ?>
  <script type="text/javascript">
  var my_autoplay = false;
  </script>
<?php } ?>

<?php
$current_img = 0;
if(!empty($presentations[0]['Image_current']))
{
    $array1 = json_decode($presentations[0]['Images']);
    $current_img = array_search($presentations[0]['Image_current'],$array1);
}
?>

<script>

var current_img = <?php echo $current_img; ?>;

$(window).load(function(){ 
  function setheight() {
    var bodyheight = $(window).height()-53;
    $(".sp-slide, .sp-slides").css("height" , bodyheight);
  }
    setheight();
    $(window).resize(setheight);
});
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.sliderPro.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slider-pro.css">

<script type="text/javascript">
  jQuery( document ).ready(function( $ ) {
	  var bodyheight = $(window).height()-53;
    jQuery( '#example3' ).sliderPro({
      width: 2000,
      height: 'bodyheight',
      fade: true,
      arrows: false,
      buttons: false,
      fullScreen: false,
      shuffle: false,
      smallSize: 500,
      mediumSize: 1000,
      largeSize: 3000,
      thumbnailArrows: true,
      autoplay: my_autoplay,
      startSlide: current_img,
    });
  });
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.easing.js"></script>


<script type="text/javascript">

jQuery(document).ready(function() 
{
  setInterval(function () {

    jQuery.ajax({
          type: "POST",
          data: '',
          url:  "<?php echo base_url(); ?>Presentation/<?php echo $this->uri->segment(2).'/'.$event_templates[0]['Subdomain']; ?>/Get_slider_images/<?php echo $this->uri->segment(5); ?>",
          dataType: "json",
          success: function(data)
          {
               var flag=true;
                if(jQuery("#hide_thumb").val()=='Hide Thumb')
                {
                    flag=true;
                    var thumbtn='<div class="thumbnail_btn"><input type="button" value="Hide Thumb" id="hide_thumb" ></div>';
                }
                else
                {
                    flag=false;
                    var thumbtn='<div class="thumbnail_btn"><input type="button" value="Show Thumb" id="hide_thumb" ></div>';
                }
               
            //jQuery('#example3').html('<div class="sp-slides"></div>'+thumbtn+'<div class="sp-thumbnails my-sp-thumbnails"></div>');

            var height = $(window).height() - 50;
            $('.sp-slides').html('');
            $('.my-sp-thumbnails').html('');

            for(var aa in data['images']) {
              $('.sp-slides').append($("#siderTemplate").tmpl(data['images'][aa]));
              $('.my-sp-thumbnails').append($("#siderTemplate1").tmpl(data['images'][aa]));
            }
            
              var CImg=window.current_img_index;

              <?php if($presentations[0]['Auto_slide_status'] =='1') { ?>         
               test(CImg);
              <?php } else { ?>
               test2(CImg);
              <?php } ?>
              
              if(flag)
              {
                    $('.sp-thumbnails-container').css('display','block');
              }
              else
              {
                    $('.sp-thumbnails-container').css('display','none');
              }
               
              jQuery("#hide_thumb").on('click',function() 
               {
                    jQuery(".sp-thumbnails-container").toggle();
                    if(jQuery("#hide_thumb").val()=='Hide Thumb')
                    {
                         jQuery("#hide_thumb").val('Show Thumb');
                    }
                    else
                    {
                         jQuery("#hide_thumb").val('Hide Thumb');
                    }
               }); 
          }
      });

  },20000);
});

function test(no)
{
//console.log(no);
 //jQuery( '#example3' ).sliderPro( 'destroy' );
  var bodyheight = $(window).height()-53;
 jQuery( '#example3' ).sliderPro({
      width: 2000,
      height: 'bodyheight',
      fade: true,
      arrows: false,
      buttons: false,
      fullScreen: false,
      shuffle: false,
      smallSize: 500,
      mediumSize: 1000,
      largeSize: 3000,
      thumbnailArrows: true,
      autoplay: true,
      //startSlide: no,
    });
}

function test2(no)
{
	 var bodyheight = $(window).height()-53;
  jQuery( '#example3' ).sliderPro({
      width: 2000,
      height: 'bodyheight',
      fade: true,
      arrows: false,
      buttons: false,
      fullScreen: false,
      shuffle: false,
      smallSize: 500,
      mediumSize: 1000,
      largeSize: 3000,
      thumbnailArrows: true,
      autoplay: false,
      startSlide: no,
    });
}

</script>