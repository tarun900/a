<!-- <meta http-equiv="refresh" content="30" /> -->
<?php if($presentations[0]['End_date'].' '.$presentations[0]['End_time'] >= date('Y-m-d H:i:s') && $presentations[0]['Status'] =='1') { ?>
<div class="my_slider_sliderPro">
  <div id="example3" class="slider-pro">
      <div class="sp-slides">
        <?php 
            $array1 = json_decode($presentations[0]['Images']);
            $array2 = json_decode($presentations[0]['Image_lock']);
            $final_array = array();
            foreach ($array2 as $key => $value) 
            {
              if($value)
              {
                $final_array[] = $array1[$key];
              }
            } ?>
          <?php foreach ($final_array as $key => $value) { ?>
          <div class="sp-slide">
            <img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-small="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-medium="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-large="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-retina="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>" />
          </div>
          <?php } ?>
      </div>
      
      <?php if($presentations[0]['Thumbnail_status'] == 1){ ?>
       <div class="thumbnail_btn"><input type="button" value="Hide Thumb" id="hide_thumb" ></div>
      <?php }else{ ?>
       <div class="thumbnail_btn"><input type="button" value="Show Thumb" id="hide_thumb" ></div>
      <?php } ?>

      <div class="sp-thumbnails my-sp-thumbnails">
        <?php foreach ($final_array as $key => $value) { ?>
          <img class="sp-thumbnail" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"/>
        <?php } ?>
      </div>
  </div>
</div>
<?php } else { ?>
  <h1 style="text-align:center;">Presentation is end.</h1>
<?php } ?>
<?php if($presentations[0]['Thumbnail_status'] == '0') { ?>
<style type="text/css">
.sp-thumbnails-container
  {
    display: none;
  }
</style>
<?php } ?>
<script type="text/javascript">
 jQuery(document).ready(function()
     {
          $('body').attr('class',"sidebar-close");
          jQuery("#hide_thumb").on('click',function() 
          {
               jQuery(".sp-thumbnails-container").toggle();
               if(jQuery("#hide_thumb").val()=='Hide Thumb')
               {
                    jQuery("#hide_thumb").val('Show Thumb');
               }
               else
               {
                    jQuery("#hide_thumb").val('Hide Thumb');
               }
                    
          });
     });
</script>

<script id="siderTemplate" type="text/x-jQuery-tmpl">
    <div class="sp-slide">
        <img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-small="<?php echo base_url(); ?>assets/user_files/${img}"
            data-medium="<?php echo base_url(); ?>assets/user_files/${img}"
            data-large="<?php echo base_url(); ?>assets/user_files/${img}"
            data-retina="<?php echo base_url(); ?>assets/user_files/${img}" />
    </div>
</script>
<script id="siderTemplate1" type="text/x-jQuery-tmpl">
    <img class="sp-thumbnail" src="<?php echo base_url(); ?>assets/user_files/${img}"/>
</script>