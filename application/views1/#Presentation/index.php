<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
?>
<div class="row margin-left-10">
    <?php
    if(!empty($user)):
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
        
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);

          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Presentation/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif; 
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('9', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>



<div class="agenda_content agenda_content-new" id="presentation_view">
  <div class="panel panel-white panel-white-new">
    <div class="tabbable panel-green">
      <?php if(!empty($presentations)) { ?>
      <ul id="myTab6" class="nav nav-tabs">
        <li class="active"> <a href="#myTab6_example1" data-toggle="tab"> Sort by Time </a> </li>
        <li> <a href="#myTab6_example2" data-toggle="tab"> Sort by Type </a> </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="myTab6_example1">
          <div class="panel-group accordion" id="accordion">
            <?php foreach ($presentations as $key1 => $value1) { ?>
            <div class="panel panel-white">
              <div class="panel-heading">
                <h5 class="panel-title"> 
                  <a class="accordion-toggle <?php static $f1=0;if($f1==0) { echo '';$f1++; } else { echo 'collapsed';$f1++;} ?>" data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo $key1; ?>"> <i class="icon-arrow"></i>
                    <?php 
                    $adate = $key1;
                    $a =  strtotime($adate);
                    echo date("l, M jS, Y",$a);
                    ?>
                  </a> 
                </h5>
              </div>
              <div id="accordion<?php echo $key1; ?>" class="collapse <?php static $f=0;if($f==0) { echo 'in';$f++; } ?>">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover" id="sample-table-1">
                    <thead>
                      <tr>
                        <th>Slides</th>
                        <!-- <th>Presentation Thumbnail</th> -->
                        <!-- <th>Presentation Place</th> -->
                        <th>Presentation Time</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($value1 as $key => $value) { ?>
                      <tr>
                        <td><a href="<?php echo base_url() ?>Presentation/<?php echo $acc_name."/".$Subdomain; ?>/View_presentation/<?php echo $value['Id']; ?>"><?php echo $value['Heading']; ?></a></td>
                        <!-- <td>
                          <?php
                            //if($value['presentation_file_type']==0){
                            $images_array = json_decode($value['Images']); 
                            //if(!empty($images_array)) { ?>
                            <img style="width:150px;height:100px;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>">
                          <?php //} }else{ ?>
                          <img style="width:150px;height:100px;" src="<?php echo base_url().'assets/images/icon/ppt.png'; ?>">
                            <?php //} ?>
                        </td> -->
                        <?php $domain = $this->uri->segment(2); ?>
                        <!-- <td><a target="_blank" href="<?php //echo base_url(); ?>Maps/<?php //echo $acc_name."/".$Subdomain; ?>/View/<?php //echo $value['Address_map']; ?>"><?php //echo $value['Map_title']; ?></a></td> -->
                        <?php
                        $asdate = $value['Start_time'];
                        $aedate = $value['End_time'];
                        if($time_format[0]['format_time']=='0')
                        {
                            $sdate = date('h:i A', strtotime($asdate));
                            $edate = date('h:i A', strtotime($aedate));
                        }
                        else
                        {
                           $sdate = date("H:i", strtotime($asdate));
                           $edate = date('H:i', strtotime($aedate));
                        }
                        $ttime = $value['End_date'].' '.$value['End_time'];
                        ?>
                        <td><i class="fa fa-clock-o"></i> <?php 
                        if(empty($timezonetype[0]['Name']))
                        {
                          echo $sdate.' '.'-'.' '.$edate;
                        }
                        else
                        {
                         echo $sdate.' '.'-'.' '.$edate." (".$timezonetype[0]['Name'].")";
                        }
                        ?></td>
                        <td><a href="<?php echo base_url() ?>Presentation/<?php echo $acc_name."/".$Subdomain; ?>/View_presentation/<?php echo $value['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View">View Slides</a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="tab-pane fade in" id="myTab6_example2">
         <div class="panel-group accordion" id="accordion_by_type">
            <?php
            foreach ($presentation_types as $key2 => $value2) { ?>
            <div class="panel panel-white">
              <div class="panel-heading">
                <h5 class="panel-title"> 
                  <a class="accordion-toggle <?php static $f2=0;if($f2==0) { echo '';$f2++; } else { echo 'collapsed';$f2++;} ?>" data-toggle="collapse" data-parent="#accordion_by_type" href="#accordion<?php echo $key2; ?>"> <i class="icon-arrow"></i>
                    <?php 
                    echo ucfirst($key2);
                    ?>
                  </a> 
                </h5>
              </div>
              <div id="accordion<?php echo $key2; ?>" class="collapse <?php static $f3=0;if($f3==0) { echo 'in';$f3++; } ?>">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover" id="sample-table-1">
                    <thead>
                      <tr>
                        <th>Presentation Name</th>
                        <!-- <th>Presentation Thumbnail</th> -->
                        <!-- <th>Presentation Place</th> -->
                        <th>Presentation Time</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($value2 as $key => $value) { ?>
                      <tr>
                        <?php $domain = $this->uri->segment(2); ?>
                        <td><a href="<?php echo base_url() ?>Presentation/<?php echo$acc_name."/".$Subdomain; ?>/View_presentation/<?php echo $value['Id']; ?>"><?php echo $value['Heading']; ?></a></td>
                        <!-- <td>
                          <?php
                          //if($value['presentation_file_type']==0){
                            $images_array = json_decode($value['Images']); 
                           // if(!empty($images_array)) { ?>
                            <img style="width:150px;height:100px;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>">
                          <?php //} }else{ ?>
                          <img style="width:150px;height:100px;" src="<?php echo base_url().'assets/images/icon/ppt.png'; ?>">
                          <?php //} ?>
                        </td> -->
                        <?php $domain = $this->uri->segment(2); ?>
                        <!-- <td><a target="_blank" href="<?php //echo base_url(); ?>Maps/<?php //echo $acc_name."/".$Subdomain; ?>/View/<?php //echo $value['Address_map']; ?>"><?php //echo $value['Map_title']; ?></a></td> -->
                        <?php
                        $asdate = $value['Start_time'];
                        $aedate = $value['End_time'];
                        if($time_format[0]['format_time']=='0')
                        {
                          $sdate = date('h:i A', strtotime($asdate));
                          $edate = date('h:i A', strtotime($aedate));
                        }
                        else
                        {
                          $sdate = date('H:i', strtotime($asdate));
                          $edate = date('H:i', strtotime($aedate));
                        }
                        $ttime = $value['End_date'].' '.$value['End_time'];
                        ?>
                        <td><i class="fa fa-clock-o"></i> <?php 
                        if(empty($timezonetype[0]['Name']))
                        {
                          echo $sdate.' '.'-'.' '.$edate;
                        }
                        else
                        {
                         echo $sdate.' '.'-'.' '.$edate." (".$timezonetype[0]['Name'].")";
                        }
                        ?></td>
                        <td><a href="<?php echo base_url() ?>Presentation/<?php echo $acc_name."/".$Subdomain; ?>/View_presentation/<?php echo $value['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View">View Slides</a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } else { ?>
        <div class="tab-content"> <span>No Presentation available for this time/type.</span> </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>

<script type="text/javascript">
  function add_advertise_hit()
  {
     
      $.ajax({
                url : '<?php echo base_url().Agenda."/".$Subdomain ?>/add_advertise_hit',
                type: "POST",  
                async: false,
                success : function(data1)
                {

                }
             });
  }
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script id="autonotifyTemplate" type="text/x-jQuery-tmpl">
     ${( $data.symbol = '' ),''}
    
     {{if Ispublic=='1'}}
         ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/publicmsg' ),''}
     {{/if}}
               
     {{if Ispublic=='0'}}
         ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/privatemsg' ),''}
     {{/if}}
               
     <li class="unread">
           <a href="${symbol}" class="unread">
                <div class="container_msg_notify clearfix">
                     <div class="thread-image">
                          <img src="${Logo}" alt="">
                     </div>
                     <div class="thread-content">
                          <span class="author">${Firstname} ${Lastname}</span>
                          <span class="preview">
                          ${Message}</span>
                     </div>
                </div>
           </a>
      </li>
</script>


