<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">User Permission</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                <?php if($this->session->flashdata('permission_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> User Permission <?php echo $this->session->flashdata('permission_data'); ?> Successfully.
                </div>
                <?php } ?>
                <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
			         <?php 
                        if(isset($event['menu_sidebar']))
                        {   
                            echo'<div class="col-md-12">';
                            foreach ($event['menu_sidebar'] as $key=>$value)
                            {  
                                ?>      <div class="col-md-4">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input class="flat-grey" type="checkbox" name="Menu_id[]" id="checkbox_<?php echo $value->id; ?>" <?php if(in_array($value->id, $array_permission)){ ?> checked="checked" <?php } ?> value="<?php echo $value->id; ?>"><span class="title"><?php echo $value->menuname; ?></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                <?php                                
                            }
                            echo'</div>';
                        }
                    ?>
                    <div class="row" style="clear:both;">
                        <div class="col-md-2">
                                <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->