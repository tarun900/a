<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Add <span class="text-bold">Document</span></h4>
        <div class="panel-tools">
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
          <div class="row">
            <?php if ($this->session->flashdata('site_setting_data') == "Updated") { ?>
            <div class="errorHandler alert alert-success no-display" style="display: block;">
              <i class="fa fa-remove-sign"></i> Updated Successfully.
            </div>
            <?php } ?>
            <div class="col-md-5">
              <div class="form-group">
                <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Title<span class="symbol required"></span>
                </label>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                  <input type="text" placeholder="Title" class="form-control required" id="title" name="title">
                  <input type="hidden" name="doc_type" value="1">
                </div>  
              </div>
              <div class="form-group">
                <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12"></label>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                  <div data-provides="fileupload" class="fileupload fileupload-new">
                    <span class="btn btn-file btn-light-grey">
                      <i class="fa fa-folder-open-o"></i> 
                      <span class="fileupload-new">Upload Your Document</span>
                      <span class="fileupload-exists">Change</span>
                      <input type="file" id="documents_files" name="document[]">
                      <span class="help-block" id="documents_files_error"  style="display:none;" for="documents_files"></span>
                    </span>
                    <span class="fileupload-preview"></span>
                    <a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
                      &times;
                    </a>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Upload Files Icon Image</label>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                  <div class="fileupload fileupload-new required" data-provides="fileupload">
                    <div class="fileupload-new thumbnail"></div>
                    <div class="fileupload-preview fileupload-exists thumbnail"></div>
                    <div class="user-edit-image-buttons">
                      <span class="btn btn-azure btn-file">
                        <span class="fileupload-new">
                          <i class="fa fa-picture"></i> Select image
                        </span>
                        <span class="fileupload-exists">
                          <i class="fa fa-picture"></i> Change
                        </span>
                        <input type="file" name="files_icon_images" id="files_icon_images">
                      </span>
                      <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                        <i class="fa fa-times"></i> Remove
                      </a>
                    </div>
                  </div>
                  <span class="help-block" id="files_icon_images_error"  style="display:none;" for="files_icon_images"></span>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                <div class="form-group">
                  <span>Choose a folder to place this document in</span><br/>
                  <span>(you can add one quickly here)</span><br/>
                  <span style="color:#007CE9;"><strong><i>To skip this part, just click the Save button below.</i></strong></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12"></label>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                  <select id="parent" onchange="openpop();" class="form-control old_folders" name="parent">
                    <option value="0">Select Folder</option>  
                    <option style="background-color: #ccc !important;" value="New">Add a new folder</option>
                    <?php foreach ($docfolder as $key=>$value)  { ?>
                      <option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                    <?php }  ?>
                  </select>
                </div>                                                                  
              </div>
              <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6" style="margin-top: 15px;">
                <button class="btn btn-green btn-block" type="submit">Save Your Document </button>
              </div>
            </div>          
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="full_popup">
  <div id="blanket" style="display: none; height: 2000px;"></div>
  <div id="popUpDiv" style="display: none; top: 191px; left: 505.5px;">
    <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv&#39;)">
    <img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a>
    <form role="form" method="post" class="ajax-form" id="form2">
      <div class="row">
        <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
        <div class="errorHandler alert alert-success no-display" style="display: block;">
          <i class="fa fa-remove-sign"></i> Updated Successfully.
        </div>
        <?php } ?>
        <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
          <div class="form-group">
            <h2 class="col-sm-12 col-md-12 col-xs-12 col-lg-12">Add a new folder</h2>
            <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Folder Name </span></label>
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
              <input type="text" class="form-control required" id="folder_title" name="folder_title">
              <span id="foldernamespan" style="color:red;display:none;" for="Folder Name" class="help-block">This field is required.</span>
            </div>  
          </div>
          <div class="form-group">
            <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Upload Folder Icon Image</label>
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
              <div class="fileupload fileupload-new required" data-provides="fileupload">
                <div class="fileupload-new thumbnail"></div>
                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                <div class="user-edit-image-buttons">
                  <span class="btn btn-azure btn-file">
                    <span class="fileupload-new">
                      <i class="fa fa-picture"></i> Select image
                    </span>
                    <span class="fileupload-exists">
                      <i class="fa fa-picture"></i> Change
                    </span>
                    <input type="file" name="folder_icon_images" id="folder_icon_images">
                  </span>
                  <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                    <i class="fa fa-times"></i> Remove
                  </a>
                </div>
              </div>
              <span class="help-block" id="folder_icon_images_error"  style="display:none;" for="folder_icon_images"></span>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
            <div class="col-sm-3 col-md-3 col-xs-3 col-lg-3">
              <button id="comment" class="btn btn-green btn-block" type="button">Add <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
        </div>                     
      </div>
    </form>
  </div> 
</div>
<style type="text/css">
#popUpDiv .form-group h2{
  color:#fff;font-weight: bold;
}
#blanket { 
  background-color:#111;
  opacity: 0.65;
  position:absolute;
  z-index: 10;
  top:0px;
  left:0px;
  width:100%;
  height: 100% !important;
}
.close_popup { 
  position: absolute;
  top: -10px;
  right: -10px;
}
#popUpDiv {
  position:fixed;
  background-color:#BDBDBD;
  width:500px;
  height:auto;
  z-index: 9002;
  padding: 15px;
  left: 42% !important;
  top: 25% !important; 
  border: 5px solid #333;
}
#popUpDiv .form-group label{
  color:#fff;
  font-weight: bold;
}
#popUpDiv .col-md-3{
  margin: 0; 
  padding: 0;
}
</style>
<script type="text/javascript">
$('#documents_files').change(function(){
  var ext = $('#documents_files').val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['pdf','ppt','doc','docx','odg']) == -1) {
    $('#documents_files_error').show();
    $('#documents_files_error').parent().parent().parent().parent().addClass('has-error');
    $('#documents_files_error').html('Invalid files..');
    $('#documents_files').val('');
  }
  else
  {
    $('#documents_files_error').hide();
  }
});
$('#files_icon_images').change(function(){
  var ext = $('#files_icon_images').val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['gif','png','jpg','jpeg','icon']) == -1) {
    $('#files_icon_images_error').show();
    $('#files_icon_images_error').parent().parent().parent().parent().addClass('has-error');
    $('#files_icon_images_error').html('Invalid files..');
    $('#files_icon_images').val('');
  }
  else
  {
    $('#files_icon_images_error').hide();
    $('#files_icon_images_error').parent().parent().parent().parent().removeClass('has-error').addClass('has-success');
  }
});
function openpop()
{
  if($(".old_folders").val()=='New')
  {
    popup('popUpDiv');
  }
}
$('#folder_title').blur(function(){
  if($(this).val() != '')
  {
    var form_data = 
    {
        title : $('#folder_title').val()
    };
    $('#foldernamespan').hide();
    $.ajax({
      url: "<?php echo base_url().'Document/checkfoldername/'.$this->uri->segment(3); ?>",
      type: 'POST',
      data: form_data,
      success: function(result)
      {
        var data=result.split('###');
        if(data[0]=="error")
        {
          $('#foldernamespan').html(data[1]);
          $('#foldernamespan').show();
          $('#folder_title').val('');
          $('#folder_title').focus();
        }
      }
    });
  }
  else
  {
    $('#foldernamespan').html('This field is required.');
    $('#foldernamespan').show();
  }

});

$('#comment').click(function() 
{
  var form_data=new FormData($('#form2')[0]);
  if($('#folder_title').val() != '')
  {
    $.ajax({
      url: "<?php echo base_url().'Document/addfolder/'.$this->uri->segment(3); ?>",
      type: 'POST',
      data: form_data,
      processData: false,
      contentType: false,
      success: function(data)
      {
        var result=data.split('###');
        if($.trim(result[0])=="error")
        {
          if($.trim(result[1])=="Folder Name Already Exists Plase Enter Another Name")
          {
            $('#foldernamespan').html(result[1]);
            $('#foldernamespan').show();
            $('#folder_title').val('');
            $('#folder_title').focus();
          }
          else
          {
            $('#folder_icon_images_error').html(result[1]);
            $('#folder_icon_images_error').show();
            $('#folder_icon_images').val('');
          }
        }
        else
        {
          $('#parent').html(data);
          $('#full_popup').css('display','none');
          $('.old_folders').html(data); 
          $('#folder_title').val('');
          $('.old_folders').on('change',function()
          {
            if($('.old_folders').val()=='New')
            {
              $("#full_popup").show();
              popup('popUpDiv');
              $("#blanket").show();
              $("#popUpDiv").show();
            }
            else
            {
              $("#full_popup").hide();
            }
          });
        }
      }
    });
  }
  else
  {
    $('#foldernamespan').html('This field is required.');
    $('#foldernamespan').show();
  }
});
</script>