<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php if ($this->session->flashdata('agenda_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Session <?php echo $this->session->flashdata('agenda_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('uuid_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('uuid_data'); ?> 
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#beacons" data-toggle="tab">
                                Beacons
                            </a>
                        </li>
                        <li class="">
                            <a href="#triggers" data-toggle="tab">
                                Triggers
                            </a>
                        </li>
                        <li class="">
                            <a href="#movements" data-toggle="tab">
                                Movements
                            </a>
                        </li>
                        <li class="">
                            <a href="#heat_maps" data-toggle="tab">
                                Heat Maps
                            </a>
                        </li>

                        <li class="">
                            <a href="#setting_up" data-toggle="tab">
                                Setting Up
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="beacons">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>Beacon Name</th>
                                        <th>UUID</th>
                                        <th>Major</th>
                                        <th>Minor</th>
                                        <th>Delete</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($beacon_data);$i++) { ?>
                                    <tr>
                                        <td><a href="javascript:void(0)"><?php echo $beacon_data[$i]['beacon_name']; ?></a></td>
                                        <td><?php echo $beacon_data[$i]['UDID']; ?></td>
                                        <td><?php echo $beacon_data[$i]['major']; ?></td>
                                        <td><?php echo $beacon_data[$i]['minor']; ?></td>
                                        <td>
                                            <a href="javascript:;" onclick="delete_beacon(<?php echo $beacon_data[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove">
                                            <i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                        <td>
                                        <a class="btn btn-green showmodel" data-toggle="modal" data-id="<?= $beacon_data[$i]['Id']; ?>" data-bname="<?= $beacon_data[$i]['beacon_name']; ?>" id="announce" >Rename</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="triggers">
                         <a style="top: -57px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Beacons/add_trigger/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add a new Trigger</a>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Trigger Name</th>
                                        <th>Connected Beacon</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($get_triggers);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() ?>Beacons/edit_trigger/<?php echo $get_triggers[$i]['event_id'].'/'.$get_triggers[$i]['Id']; ?>"><?php echo $get_triggers[$i]['trigger_name']; ?></a></td>
                                        <td><?php echo $get_triggers[$i]['Becons']; ?></td>
                                       <td>
                                            <a href="<?php echo base_url() ?>Beacons/edit_trigger/<?php echo $get_triggers[$i]['event_id'].'/'.$get_triggers[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>

                                            <a href="javascript:;" onclick="delete_trigger(<?php echo $get_triggers[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>

                                            <?php if($user->Role_name=='User'){ ?>
                                            <a href="<?php echo base_url() ?>Beacons/edit_trigger/<?php echo $get_triggers[$i]['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_trigger(<?php echo $get_triggers[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="movements">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>Beacon Name</th>
                                        <th>Number of Hits</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($number_of_hits);$i++) { ?>
                                    <tr>
                                        <td><a href="#"><?php echo $number_of_hits[$i]['beacon_name']; ?></a></td>
                                        <td><?php echo $number_of_hits[$i]['number_of_hits']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="heat_maps">
                         <a style="top: -57px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Beacons/add_heat_map/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add New Heat Map</a>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_4">
                                <thead>
                                    <tr>
                                        <th>Heat Maps</th>
                                        <th>Show Movement</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($heatmap);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url(); ?>Beacons/edit_heat_map/<?php echo $this->uri->segment(3); ?>/<?php echo $heatmap[$i]['beacon_heatmap_id'];?>"><?php echo $heatmap[$i]['image']; ?></a></td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a class="btn btn-primary" href="<?php echo base_url(); ?>Beacons/show_heat_movements/<?php echo $this->uri->segment(3); ?>/<?php echo $heatmap[$i]['beacon_heatmap_id'];?>"><i class="fa fa-eye">  Show Movements</i></a>
                                        </td>
                                        <td>
                                            <a href="javascript:;" onclick="delete_heatmap(<?php echo $heatmap[$i]['beacon_heatmap_id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="setting_up">
                        <div class="col-md-12">
                            <h4 style="text-align: center;">
                            <p>For full information on setting up your beacons please go to 
                            <a target="_blank" href="https://www.allintheloop.com/beacon-setup.html">https://www.allintheloop.com/beacon-setup.html</a></p>
                            </h4>
                            <br/><br/>
                        </div>
                    </div>

                   <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Rename Beacon</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Beacons/edit_beacon_name/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="beacon-name" name="beacon_name">
                <input type="hidden" name="id" id="beacon-id">
              </div>
                <button type="submit" class="btn btn-primary">Rename</button>
              </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
    $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
});    
</script>

<script type="text/javascript">
    jQuery(function(){
    jQuery("#myTab2 li a").click(function()
    { 
        var strTab = jQuery(this).prop("href");
        var arrTab = strTab.split("#");
        if(arrTab[1]=='triggers')
        {
          jQuery("#add_user_btn").show();
          jQuery("#add_role_btn").hide();
        }
        else if(arrTab[1]=='heat_maps')
        {
          jQuery("#add_role_btn").show();
          jQuery("#add_user_btn").hide();
        }
    });   
});

</script>
<script type="text/javascript">
    function delete_heatmap(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Beacons/delete_heatmap/"+<?php echo $test; ?>+"/"+id+"/<?php echo $acid; ?>"
        }
    }

    function delete_trigger(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Beacons/delete_trigger/"+<?php echo $test; ?>+"/"+id+"/<?php echo $acid; ?>"
        }
    }

    function delete_beacon(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Beacons/delete_beacon/"+<?php echo $test; ?>+"/"+id+"/<?php echo $acid; ?>"
        }
    }
</script>
<script>
  $(document).ready(function(){
       $(".showmodel").click(function()
       {
             $("#beacon-name").val($(this).data('bname'));
             $("#beacon-id").val($(this).data('id'));
             $('#exampleModal').modal('show');
       });
});
</script>
<!-- end: PAGE CONTENT-->