<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#edit_notification" data-toggle="tab">
                                Edit Trigger
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="edit_notification">
                    	<div class="col-sm-9">
                			<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                				<div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
	                                    Title <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <input type="text" maxlength="20" placeholder="Trigger" id="name" name="trigger_name" class="form-control name_group required limited" value="<?php echo $trigger_data
	                                    [0]['trigger_name']; ?>">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
	                                    Message <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <textarea maxlength="60" id="Description" name="message" class="form-control limited" style="width:100%;height:100px;"><?php echo $trigger_data[0]['message']; ?></textarea>
	                                </div>
	                            </div>
	                         
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Open button link <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <?php $arrkey=array(5,8,14,18,19,21,26,27,28,29); ?>
	                                    <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="modules">
	                                    <option value="">Select Modules</option>
	                                    <?php foreach ($menu_toal_data as $key => $value) { 
	                                            if(!in_array($value['id'], $arrkey)){
	                                            	if($value['id'] == $trigger_data[0]['modules'])
	                                            	{?>

	                                            <option id="menu<?php echo $key; ?>" value="<?php echo $value['id']; ?>" selected><?php echo $value['menuname']; ?></option>	
	                                            <?php
	                                            	}
	                                            	else
	                                            	{ ?>
	                                            <option id="menu<?php echo $key; ?>" value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
	                                           <?php } } } ?>
	                                    </select>
	                                </div>
	                            </div>
	                            <?php $selectedBecons = explode(",",$trigger_data[0]['beacon_id']);?>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Connect to Beacon <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                   <select multiple="multiple" style="margin-top:-8px;" id="noti_type" class="select2-container select2-container-multi form-control search-select menu-section-select required" name="beacon_id[]">
		                                    <?php foreach ($beacon_data as $key => $value) {
		                                    	if(in_array($value['Id'],$selectedBecons))
		                                    	{ ?>
		                                    	<option value="<?= $value['Id'];?>" selected> <?php echo $value['beacon_name']; ?> </option>
		                                    <?php
		                                    	}
		                                    	else
		                                    	{ ?>

		                                    	<option value="<?= $value['Id'];?>"> <?php echo $value['beacon_name']; ?> </option>
		                                    <?php

		                                    	}
		                                     ?>
		                                    	
		                                    <?php } ?>
	                                    </select>
                            		</div>
	                            </div> 
	                            <div class="form-group">
	                                <div class="col-md-2 pull-left">
	                                    <button class="btn btn-green btn-block" type="submit">
	                                        Update 
	                                    </button>
	                                </div>
	                            </div>      
                			</form>	
                    	</div>
                    </div>
                </div>    
            </div>
        </div>        
	</div>
</div>