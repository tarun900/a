<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
</script>
<!-- start: PAGE CONTENT -->

<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#map_list" data-toggle="tab">
                            Edit Map
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>

                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('error')){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="map_list">
                         <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data" onsubmit="return submitHandler()">

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                  Title <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Map Title" id="Map_titles"  name="Map_title" value = "<?php echo $map_data[0]['Map_title']; ?>" class="form-control required" onblur="checktitleedit();">
                                </div>
                            </div>

                            <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Description 
                                </label>
                                <div class="col-sm-9">
                                    <textarea maxlength="600" id="Map_desc" style="height: 135px;" class="form-control limited" name="Map_desc"><?php echo $map_data[0]['Map_desc']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Include Map 
                                </label>

                                 <div class="col-sm-1" style="padding-left: 0px;margin-left: -7px;">
                                   <input type="checkbox" name="include-map" id="include-map" value="yes" class="form-control" <?php if($map_data[0]['include_map']=='1') { echo 'checked'; } ?> />
                                </div> 
                            </div>
                            <!-- <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Satellite View 
                                </label>

                                 <div class="col-sm-1" style="padding-left: 0px;margin-left: -7px;">
                                   <input type="checkbox" name="satellite_view" id="satellite_view" value="yes" class="form-control" <?php if($map_data[0]['satellite_view']=='1') { echo 'checked'; } ?> />
                                </div> 
                            </div> -->
                            <div class="form-group" id="maps">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Location
                                </label>
                                <div class="col-sm-9" style="height: 500px;">
                                    <input type="hidden" name="zoom_level" id="zoom_level" value="<?php echo $map_data[0]['zoom_level']; ?>" />
                                    <input type="hidden" name="lat_long" id="lat_long" value="<?php echo $map_data[0]['lat_long']; ?>" />
                                    <input type="hidden" name="place" id="place" value="<?php echo $map_data[0]['place']; ?>" />
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box" value="<?php if($map_data[0]['include_map']=='1') { echo $map_data[0]['place']; } ?>">
                                    <div id="map"></div>
                                </div>
                            </div>


                             <!-- <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Area
                                </label>
                                <div class="col-sm-9">
                                   <input id="area" name="area" value="<?php echo $map_data[0]['area']; ?>" class="form-control" type="text" placeholder="Area"/>
                                </div>
                             </div> -->
                             
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Your Map Image</label>
                                <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:4000px & height:4000px)</em><br/>
                                <div class="col-sm-9">
                                    <?php $images_array = json_decode($map_data[0]['Images']); ?>
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      
                                        <?php for($i=0;$i<count($images_array);$i++) { ?>
                                        <div class="col-sm-3 fileupload-new thumbnail center">
                                            <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                            <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                          </div>
                                    <?php } ?>   
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<!-- end: PAGE CONTENT-->

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).parent().parent().remove(); } )
        });
</script>

<style>
#map
{
  height: 100%;
}
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

    </style>

<?php
$lat_long =explode(',',$map_data[0]['lat_long']);
$include_map=$map_data[0]['include_map'];
?>
    
    
<script type="text/javascript">

$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});

$('#include-map').change(function () {
           
    if(!$( "#include-map" ).prop("checked"))
    {
        $('#maps').hide();
    }
    else
    {

        $('#maps').show();
    }
});

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.


function initAutocomplete() { 

<?php if($include_map=='1') { ?> 
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: <?php echo $lat_long[0]; ?>, lng: <?php echo $lat_long[1]; ?>},
    zoom: <?php echo $map_data[0]['zoom_level']; ?>,
    <?php if($map_data[0]['satellite_view']=='0'){ ?>
    mapTypeId: google.maps.MapTypeId.ROADMAP
    <?php }else{ ?>
    mapTypeId: google.maps.MapTypeId.HYBRID
    <?php } ?>
  });
  
  var icon = {
        url: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
  
  var marker = new google.maps.Marker({
    position: {lat: <?php echo $lat_long[0]; ?>, lng: <?php echo $lat_long[1]; ?>},
    map: map,
    icon: icon,
    title: '<?php echo $map_data[0]['place']; ?>'
  });

<?php }else{ ?>

 var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 51.5000, lng: 0.1167},
    zoom: 4,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
<?php } ?>
  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
    $('#zoom_level').val(map.zoom);
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
      // Create a marker for each place.
      $('#lat_long').val(place.geometry.location);
      $('#place').val(place.name);
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}

<?php if($map_data[0]['include_map']=='0') { ?>
        $('#maps').delay(2500).fadeOut(1000);
<?php } ?>

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxNvehdm6UpsFMELIePDjwgXcCxxfQ_Sg&libraries=places&callback=initAutocomplete" async defer></script>  
<script type="text/javascript"></script>