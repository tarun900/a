<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#map_list" data-toggle="tab">
                             Image Mapping
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body" style="padding: 0px;">
                 <?php if ($this->session->flashdata('map_region_data')) { ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Map Region <?php echo $this->session->flashdata('map_region_data'); ?> Successfully.
                        </div>
                    <?php } ?>

                <div class="tab-content">
                 <?php $images_array = json_decode($map_data[0]['Images']); ?>
                   
                      <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">

                          <div id="ref">
                                <img  src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>" id="#photo" style="position: relative;" usemap="#mape" class="map1">
                                <map id="mape1" name="mape">
                                <?php foreach ($image_mapping as $key => $value) { ?>
                                  <area title="<?php echo $value['Firstname']; ?>" alt="Mapping"  id="are" shape="rect"  onclick='delete1("<?php echo $value['id'];?>")' href="javascript:void(0);" 
                                  coords="<?php echo  $value['coords']; ?>">
                                <?php } ?>
                                </map>
                            </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  <div class="modal-scrollable" style="z-index: 1060;">
  <div class="modal fade" id="myModal123" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content mapping-popup">
        <div class="modal-body">
                <strong>Please Select Exhibitor: </strong>
                <select id="user" style="width:73%;margin-bottom: 15px;">
                  <?php  
                  foreach ($exhibitor as $key => $row) { ?>
                     <option value="<?php echo $row['Id']; ?>"><?php echo ucfirst($row['Heading']).' '.'('.$row['Email'].')'; ?></option>
                  <?php } ?>
                </select> 
                 <button type="button" id="close"  class="btn btn-default" data-dismiss="modal">Submit</button>
                 <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">cancel</button>
                 <hr>
                 <strong>OR Select  Agenda  Sessions </strong>
                <select id="agenda_session" id="agenda_session[]" class="select2-container select2-container-multi form-control search-select menu-section-select select2-offscreen" multiple="multiple" >
                  <?php  
                  foreach ($agenda_list as $key => $value) { ?>
                     <option value="<?php echo $value['Id']; ?>"><?php echo ucfirst($value['Heading']); ?></option>
                  <?php } ?>
                </select>
                <button type="button" id="close_agenda_select"  class="btn btn-default" data-dismiss="modal">Submit</button>
                <button type="button" id="cancel_agenda_select" class="btn btn-default" data-dismiss="modal">cancel</button>
                <hr>
                <strong>OR Select Meeting Location </strong>
                <select id="meeting_locations"  class=" form-control  "  >
                  <?php  
                  foreach ($location_list as $key => $value) { ?>
                     <option value="<?php echo $value['location_id']; ?>"><?php echo ucfirst($value['location']); ?></option>
                  <?php } ?>
                </select>
                <p></p>
                 <button type="button" id="meeting_location"  class="btn btn-default" data-dismiss="modal">Submit</button>
                 <button type="button" id="cancel_meeting_select" class="btn btn-default" data-dismiss="modal">cancel</button>
          </div>

        </div>
      </div>
    </div>
  </div>
  </div>
<div class="modal-backdrop" style="z-index: 1050;display:none;"></div>
<script type="text/javascript">
function delete1(a)
{
  if (confirm("Are you sure to delete this region?"))
  {
    var urlde="<?php echo base_url().'Map/delete_coords/'.$event_id.'/'.$map_id.'/'; ?>"+a;
    setTimeout(function(){document.location.href = urlde;},500);
    return false;
  }  
}
$(document).ready(function() {
  $('.map1').mapster({    
    selected: true
  });
  var demo;
  $('.map1').imgAreaSelect({
    handles: true,
    autoHide:true,
    hide:true,
    onSelectEnd: function(img,selection){
      $('#myModal123').attr('class','modal fade in');
      $('#myModal123').attr('style','display:block;');
      $('body').attr('class','modal-open page-overflow');
      $('.modal-backdrop').addClass('fade in');
      $('.modal-backdrop').show();
      demo=selection.x1 + ',' + selection.y1+','+selection.x2+','+selection.y2;
    }
  });
  $('#cancel').click(function(){
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });
  $('#cancel_agenda_select').click(function(){
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });
  $('#cancel_meeting_select').click(function(){
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });
  $('#close').click(function(){
    var person=$('#user').val();
    $.ajax({
      url:'<?php echo base_url(); ?>Map/save_coords/<?php echo $event_id."/".$map_id;?>',
      data:'coor='+demo+'&name='+person,
      method:'post',
      success:function(data){           
        $('#are').mapster('deselect');
        $('#mape1').append(data);
        $('.map1').mapster({
          selected: true
        });
      },
    });
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });
  $('#close_agenda_select').click(function(){
    var session_id=$('#agenda_session').val();
    $.ajax({
      url:'<?php echo base_url(); ?>Map/save_coords_with_session/<?php echo $event_id."/".$map_id;?>',
      data:'coor='+demo+'&session_id='+session_id,
      method:'post',
      success:function(data){              
        $('#are').mapster('deselect');
        $('#mape1').append(data);
        $('.map1').mapster({
          selected: true
        });
      },
    });
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });

  
  $('#meeting_location').click(function(){
    var session_id=$('#meeting_locations').val();
    if(session_id!='')
    {
      $.ajax({
        url:'<?php echo base_url(); ?>Map/save_coords_with_locations/<?php echo $event_id."/".$map_id;?>',
        data:'coor='+demo+'&location_id='+session_id,
        method:'post',
        success:function(data){     
         $("#meeting_locations option[value='"+session_id+"']").remove(); 
         if($("#meeting_locations option").length == 0)
            $("#meeting_locations").append('<option value="">No Meeting Locations</option>');
          $('#are').mapster('deselect');
          $('#mape1').append(data);
          $('.map1').mapster({
            selected: true
          });
        },
      });
    }
    $('#myModal123').attr('class','modal fade');
    $('#myModal123').attr('style','display:none;');
    $('body').removeAttr('class');
    $('.modal-backdrop').removeClass('fade in');
    $('.modal-backdrop').hide();
  });
});
</script>
<style type="text/css">
.map1
{
  margin: 0 auto;
}
</style>


