<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/event_template.css" />
<div class="row">
    <div class="agenda_content">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body" style="padding: 0px;">

                    <?php //if($user->Role_name=='Client'){ ?>
                    <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Map/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Geographical Map</a>
                    <a style="top: 7px;margin-right: 10%;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Map/add_group/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Group</a>
                    <?php //} ?>

                    <?php if ($this->session->flashdata('map_data')) { ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Map <?php echo $this->session->flashdata('map_data'); ?> Successfully.
                        </div>
                    <?php } ?>

                    <div class="tabbable">
                        <ul id="myTab2" class="nav nav-tabs">
                            <li class="active">
                                <a href="#view_group" data-toggle="tab">
                                    Groups
                                </a>
                            </li>
                            <li class="">
                                <a href="#view_map" data-toggle="tab">
                                    Geographical Maps
                                </a>
                            </li>
                            <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                            <li class="">
                                <a href="#menu_setting" data-toggle="tab">
                                    Menu Name and Icon
                                </a>
                            </li>
                            <?php } ?>   
							<li class="">
								<a id="view_events1" href="#view_events" data-toggle="tab">
									View App
								</a>
							</li>

                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane fade in" id="view_map">
                            <div class="table-responsive custom_checkbox_table">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>Map Name</th>
                                            <th>Map Code</th>
                                            <th>Dwg File</th>
                                            <th>Edit or Delete</th>
                                            <th>Interaction</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0;$i<count($map_list);$i++) { ?>
                                        <tr>
                                            <td><a href="<?php echo base_url() ?>Map/edit/<?php echo $map_list[$i]['Event_id'].'/'.$map_list[$i]['Id']; ?>"><?php echo $map_list[$i]['Map_title']; ?></a></td>
                                            <td><?php echo $map_list[$i]['map_u_id']; ?></td>
                                            <td>
                                                <input type="checkbox" <?php if($map_list[$i]['check_dwg_files']=='1'){ ?> checked="checked" <?php } ?> onchange='changedwgfilesstatus("<?php echo $map_list[$i]['Id']; ?>");' name="check_dwg_files" value="<?php echo $map_list[$i]['Id']; ?>" id="check_dwg_files_<?php echo $map_list[$i]['Id'];?>" class="">
                                                <label for="check_dwg_files_<?php echo $map_list[$i]['Id'];?>"></label>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Map/edit/<?php echo $map_list[$i]['Event_id'].'/'.$map_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_map(<?php echo $map_list[$i]['Id']; ?>,<?php echo $map_list[$i]['Event_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                                
                                           </td>
                                           <td><a style="" class="btn btn-primary"  href="<?php echo base_url(); ?>Map/set_mapping/<?php echo $map_list[$i]['Event_id'].'/'.$map_list[$i]['Id']; ?>"><i class="fa fa-plus"></i>Make Map Interactive</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade active in" id="view_group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Group Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($module_group_list as $key => $value) { ?>
                                        <tr>
                                            <td><?=$key+1; ?></td>
                                            <td><?=ucfirst($value['group_name']); ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Map/edit_group/<?php echo $this->uri->segment(3).'/'.$value['module_group_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="<?php echo base_url().'Map/delete_group/'.$this->uri->segment(3).'/'.$value['module_group_id']; ?>" onclick="return confirm('Are You Sure Delete This Group');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="menu_setting">
                            <div class="row padding-15">
                                <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                    <div class="col-md-12 alert alert-info">
                                        <h5 class="space15">Menu Title & Image</h5>
                                        <div class="form-group">
                                            <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        </div>
                                        <div class="form-group">
                                           <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail"></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div class="user-edit-image-buttons">
                                                     <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                          <input type="file" name="Images[]">
                                                     </span>
                                                     <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                          <i class="fa fa-times"></i> Remove
                                                     </a>
                                                </div>
                                           </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                                Create Home Tab
                                            </label>
                                            <?php if($img !='') { ?>
                                            <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                            </label>
                                            <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                            <?php } ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                            <div class="col-sm-5">
                                                <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                        <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                        <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
						
						<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
							<div id="viewport" class="iphone">
									<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
							</div>
							<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
							<div id="viewport_images" class="iphone-l" style="display:none;">
								   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
							</div>
						</div>
					
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script>
window.addEventListener( "load", function ( event ) {
  var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
  if ( historyTraversal ) {
    window.location.reload();
  }
});

function delete_map(id,event_id)
{
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href = "<?php echo base_url(); ?>Map/delete/" + event_id+"/"+id;
    }
}
function changedwgfilesstatus(mid)
{

    $.ajax({
        url:"<?php echo base_url(); ?>Map/change_dwg_files_status/<?php echo $event_id; ?>/"+mid,
        success:function(result)
        {
            if($.trim(result) > 0)
            {
                var shortCutFunction ='success';
                if($("#check_dwg_files_"+mid).is(':checked'))
                {
                    var title = 'Active Successfully';
                    var msg = 'DWg File Active Successfully';
                }
                else
                {
                    var title = 'Inactive Successfully';
                    var msg = 'DWg File Inactive Successfully'; 
                }
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-top-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
            }
        }
    });
}
</script>
<!-- end: PAGE CONTENT-->