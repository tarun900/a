<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
</script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Map</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('error')){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>

                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Title <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Map Title" class="form-control" id="Map_title" name="Map_title" onblur="checktitle();">
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Description 
                        </label>
                        <div class="col-sm-9">
                            <textarea id="Map_desc" placeholder="Map Description" name="Map_desc" class="summernote"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Address URL 
                        </label>
                        <div class="col-sm-9">
                            <label class="control-label" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>(e.g: https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d7343.770899273804!2d72.53072571069376!3d23.027977800014522!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin<br/>!4v1421995440846)</em> 
                            </label>
                            <input type="text" placeholder="Google Map Embed URL" id="url" name="Address" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Address Images</label>
                        <div class="col-sm-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail"></div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                    <input type="file" name="images">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<!-- end: PAGE CONTENT-->