<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/agenda/form-validation.js"></script>
<?php $eid=$this->uri->segment(3);
$rid=$this->uri->segment(4);?>       
<script>
	jQuery(document).ready(function() {
				TableData.init();
                FormElements.init();
                FormValidator.init();
	});

function checktitle()
    {
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Map/checktitle/<?php echo $eid; ?>',
                data :'Map_title='+$("#Map_title").val()+'&idval='+$('#idval').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#Map_title').parent().parent().removeClass('has-success').addClass('has-error');
                        $('#Map_title').parent().parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#Map_title').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        //$('#Map_title').parent().parent().removeClass('has-error').addClass('has-success');
                        //$('#Map_title').parent().parent().find('.control-label span').removeClass('required').addClass('ok');
                        //$('#Map_title').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
</script>

<script>
 
        function checktitleedit()
        {
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Map/checktitle/<?php echo $eid; ?>/<?php echo $rid; ?>',
                data :'Map_title='+$("#Map_titles").val()+'&idval='+$('#idval').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#Map_titles').parent().parent().removeClass('has-success').addClass('has-error');
                        $('#Map_titles').parent().parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#Map_titles').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        //$('#Map_titles').parent().parent().removeClass('has-error').addClass('has-success');
                        //$('#Map_titles').parent().parent().find('.control-label span').removeClass('required').addClass('ok');
                        //$('#Map_titles').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;

        }
</script>