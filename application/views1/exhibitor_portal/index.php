<!-- start: PAGE CONTENT -->
<?php $acc_name=$this->session->userdata('acc_name'); ?>
<?php $page=$users[0]['page_cnt']; ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
            <?php if($page==0) { ?>
                <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Exhibitor_portal/add/<?php echo $event['Id']; ?>"><i class="fa fa-plus"></i> Add Exhibitor</a>
            <?php } ?>
                <?php if($this->session->flashdata('exibitor_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Exhibitor <?php echo $this->session->flashdata('exibitor_data'); ?> Successfully.
                </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#exibitor_list" data-toggle="tab">
                                Exhibitors List
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
                      <!--   <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li> -->
                    </ul>
                </div>

                <div class="tab-content">

                    <div class="tab-pane fade active in" id="exibitor_list">
                        <div class="table-responsive">
                            <!-- <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1"> -->
                            <table class="table table-striped table-bordered table-hover table-full-width">
                                <thead>
                                    <tr>
                                        <th>Company name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                    <?php for($i=0;$i<count($exibitor_list);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() ?>Exhibitor_portal/edit/<?php echo $exibitor_list[$i]['Event_id'].'/'.$exibitor_list[$i]['Id']; ?>"><?php echo $exibitor_list[$i]['Heading']; ?></a></td>
                                       
                                       <td>
                                            <a href="<?php echo base_url() ?>Exhibitor_portal/edit/<?php echo $exibitor_list[$i]['Event_id'].'/'.$exibitor_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><!-- <i class="fa fa-edit"></i> --> Click here to edit your Exhibitor Profile </a>
                                          
                                            <?php if($user->Role_name=='User'){ ?>
                                            <a href="<?php echo base_url() ?>Exhibitor_portal/edit/<?php echo $exibitor_list[$i]['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Edit"><!-- <i class="fa fa-edit"></i> --> Click here to edit your Exhibitor Profile </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                   <!-- <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    function delete_exibitor(id,Event_id)
    {   
        <?php $Event_id = $event['Id']; ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor_portal/delete/"+id+"/"+<?php echo $Event_id; ?>
        }
    }
</script>