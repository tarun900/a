<?php 
$acc_name=$this->session->userdata('acc_name');
$k = array_rand($advertisement_images);
$user = $this->session->userdata('current_user');
$advertisement_images = $advertisement_images[$k];

if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('1', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads alert alert-success alert-dismissable">
    <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery"> 

    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } ?>
 </div>
</div>
<?php } ?>
<div class="row" style="text-align: center;"><h3>My Agenda</h3></div>
<div class="agenda_content agenda_content-new">
  <div class="panel panel-white panel-white-new pending_agendas">
    <div class="tabbable panel-green">
      <?php if(!empty($agenda) || count($metting)>0) { ?>
      <ul id="myTab6" class="nav nav-tabs">
        <li class="active"> <a href="#myTab6_example1" data-toggle="tab"> Sort by Time </a> </li>
        <li> <a href="#myTab6_example2" data-toggle="tab"> Sort by Track </a> </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="myTab6_example1">
          <div class="panel-group accordion" id="accordion">
            <?php $couterkey=0; foreach ($agenda as $key1 => $value1) { ?>
            <div class="panel panel-white">
              <div class="panel-heading">
                <h5 class="panel-title"> <a class="accordion-toggle <?php static $f1=0;if($f1==0) { echo '';$f1++; } else { echo 'collapsed';$f1++;} ?> " data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo $couterkey; ?>"> <i class="icon-arrow"></i>
                  <?php 
                      $adate = $key1;
                      $a =  strtotime($adate);
                      if($event_templates[0]['show_session_by_time']=='1'){
                        if($time_format[0]['format_time']=='0')
                        {
                            echo date("h:i A",$a);
                        }
                        else
                        {     
                             echo date("H:i",$a);
                        }
                      }
                      else
                      {
                        if($event_templates[0]['date_format']=='1')
                        {
                          echo date("m/d/Y",$a);
                        }
                        else
                        {
                          echo date("d/m/Y",$a);
                        }
                        //echo $event_templates[0]['default_lang']['time_and_date__'.strtolower(date("l",$a))].", ".date("M jS, Y",$a);
                      }
                  ?>
                  </a> </h5>
              </div>
              <div id="accordion<?php echo $couterkey; ?>" class="collapse <?php static $f=0;if($f==0) { echo 'in';$f++; } ?>">
                <div class="table-responsive">
                <!--<div id="alldiv">-->
                  <table class="table table-bordered table-hover" id="sample-table-1">
                    <thead>
                      <tr>
                        <th>Session Name</th>
                        <th>Session Track</th>
                        <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                        <th>Session Speaker</th>
                        <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                        <th>Session Location</th>
                        <?php } if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                        <th>Places Left</th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $couterkey++;
                    $checkedval=  explode(',',$user_agenda_list[0]['agenda_id']);
                    sort($checkedval);
                    ?>
                    <?php foreach ($value1 as $key => $value) { ?>
                      <tr>
                        <td class="session_name">
                          <a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],15,"<br />",true); ?>
                          </a>
                        </td>
                        <td>
                          <?php echo ucwords($value['Types']); ?>
                        </td>
                        <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                        <td class="user_container_name">
                          <?php echo !empty($value['custom_speaker_name']) ? ucfirst($value['custom_speaker_name']) : '--'; ?>
                        </td>
                        <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                        <td class="user_container_name session_location">
                          <?php echo !empty($value['custom_location']) ? ucfirst($value['custom_location']) : '--' ; ?>
                        </td>
                        <?php }if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                        <td class="session_place_left">
                          <?php echo ($value['Place_left']) ? $value['Place_left'] : '--'; ?>
                        </td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>

                   <input type="hidden" name="checkbox_values" value="<?php echo $user_agenda_list[0]['agenda_id']; ?>" id="checkbox_values">
                  <!--</div>-->
                </div>
              </div>
            </div>
            <?php } ?>
            <?php foreach ($metting_data_by_datetime as $mettingkey => $datametting) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <h5 class="panel-title"> <a class="accordion-toggle accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo strtotime($mettingkey); ?>"> <i class="icon-arrow"></i>
                    <?php 
                      echo date("l, M jS, Y",strtotime($mettingkey));
                    ?>
                    </a> </h5>
                </div>
                <div id="accordion<?php  echo strtotime($mettingkey); ?>" class="collapse">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="sample-table-1">
                      <thead>
                        <tr>
                          <th>Meeting</th>
                          <th>Time</th>
                          <th>Status</th>
                          <th>Location</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($datametting as $key => $value) { ?>
                        <tr>
                          <td><?php if($user[0]->Rid=='6' && !empty($value['Company_name'])){
                            echo $value['Heading'].' - { '.$value['Company_name'].' }';
                          }else{
                          echo $value['Heading']; } ?></td>
                          <td><?php echo $value['time']; ?></td>
                          <td>
                          <?php if($value['status']=='0'){ ?>
                            <span class="label label-sm label-info"> Pending </span>
                          <?php }else{ ?>
                          <span class="label label-sm label-success"> Confirm </span>
                          <?php } ?>
                          </td>
                          <td><?php echo $value['stand_number']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            <?php } ?>
          </div>
        </div>
        <div class="tab-pane fade in" id="myTab6_example2">
        <div class="panel-group accordion" id="accordion_by_type">
            <?php 
            $typef=count($agenda);
            foreach ($agenda_list as $key2 => $value2) {  $kkey2=$key2; $key2=str_replace(" ", "_", $key2); ?>
            <div class="panel panel-white">
              <div class="panel-heading">
                <h5 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_by_type" href="#accordion<?php echo $typef; ?>"> <i class="icon-arrow"></i>
                  <?php 
                      echo $kkey2;
                  ?>
                  </a> </h5>
              </div>
              <div id="accordion<?php echo $typef; ?>" class="collapse">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover" id="sample-table-1">
                    <thead>
                      <tr>
                        <th>Session Name</th>
                        <th>Session Track</th>
                        <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                        <th>Session Speaker</th>
                        <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                        <th>Session Location</th>
                        <?php } if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                        <th>Places Left</th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $typef++; foreach ($value2 as $key => $value) { ?>
                      <tr>
                        <td class="session_name">
                          <a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],15,"<br />",true); ?>
                          </a>
                        </td>
                        <td>
                          <?php echo ucwords($value['Types']); ?>
                        </td>
                        <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                        <td class="user_container_name">
                          <?php echo !empty($value['custom_speaker_name']) ? ucfirst($value['custom_speaker_name']) : '--'; ?>
                        </td>
                        <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                        <td class="user_container_name session_location">
                          <?php echo !empty($value['custom_location']) ? ucfirst($value['custom_location']) : '--' ; ?>
                        </td>
                        <?php }if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                        <td class="session_place_left">
                          <?php echo ($value['Place_left']) ? $value['Place_left'] : '--'; ?>
                        </td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php } ?>
            <?php foreach ($metting_data_by_type as $mettingkey => $datametting) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <h5 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_by_type" href="#accordion<?php echo str_replace(' ','',$mettingkey); ?>"> <i class="icon-arrow"></i>
                    <?php 
                      echo $mettingkey;
                    ?>
                    </a> </h5>
                </div>
                <div id="accordion<?php  echo str_replace(' ','',$mettingkey); ?>" class="collapse">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="sample-table-1">
                      <thead>
                        <tr>
                          <th>Meeting</th>
                          <th>Time</th>
                          <th>Status</th>
                          <th>Location</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($datametting as $key => $value) { ?>
                        <tr>
                          <td><?php if($user[0]->Rid=='6' && !empty($value['Company_name'])){
                            echo $value['Heading'].' - { '.$value['Company_name'].' }';
                          }else{
                          echo $value['Heading']; } ?></td>
                          <td><?php echo $value['time']; ?></td>
                          <td>
                          <?php if($value['status']=='0'){ ?>
                            <span class="label label-sm label-info"> Pending </span>
                          <?php }else{ ?>
                          <span class="label label-sm label-success"> Confirm </span>
                          <?php } ?>
                          </td>
                          <td><?php echo $value['stand_number']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            <?php } ?>
          </div>
          
        </div>
        <?php } else { ?>
        <div class="tab-content"> <span>No Agenda available for this Category.
</span> </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script>
    jQuery(document).ready(function()
     {
       //Main.init();
       //ComingSoon.init();
       
     });
 
</script>
