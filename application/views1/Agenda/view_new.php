<link href="<?php echo base_url().'assets/css/agendastyle.css?'.time(); ?>" rel="stylesheet" />
<?php $isMobile = preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
                    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
                    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );
$acc_name=$this->session->userdata('acc_name');
$event=$this->uri->segment(3);
$user = $this->session->userdata('current_user');
$agenda_id= array_filter(explode(',',$user_agenda_list[0]['agenda_id']));
$pending_agenda_id=array_filter(explode(',',$user_agenda_list[0]['pending_agenda_id']));
$checkedval=array_merge($agenda_id,$pending_agenda_id);
sort($checkedval); ?>
<div class="errorHandler alert alert-success no-display" style="display: none;" id="msg">
	<i class="fa fa-remove-sign" id="con"> </i> 
</div>
<?php if($event_templates[0]['on_pending_agenda']=='1'){ ?>
	<input type="hidden" name="checkbox_values" value="<?php echo $user_agenda_list[0]['pending_agenda_id']; ?>" id="checkbox_values">
<?php }else{ ?>
	<input type="hidden" name="checkbox_values" value="<?php echo $user_agenda_list[0]['agenda_id']; ?>" id="checkbox_values">
<?php } ?>
<div class="col-sm-12">
  <div class="panel panel-white box-effet agenda-views-panel">
    <div class="agenda-views-content-wrapper">
      <div class="view-introduction-wrapper">
        
        <div class="venue-detail-wrapper">
          <div class="views-venue-block">
            <div class="venue-detail">
              <h2><?php echo ucfirst($agenda_value[0]['Heading']); ?></h2>
              <div class="venue-date-time">
                <div class="venue-time">
                  <?php if($time_format[0]['format_time']=='0'){ 
                    echo date('h:i A', strtotime($agenda_value[0]['Start_time'])).' - '.date('h:i A', strtotime($agenda_value[0]['End_time']));
                  }else{ 
                    echo date('H:i', strtotime($agenda_value[0]['Start_time'])).' - '.date('H:i', strtotime($agenda_value[0]['End_time']));
                  } ?>
                </div>
                <div class="venue-date"> <?php if($event_templates[0]['date_format']=='1')
                        {
                            echo date("m/d/Y",strtotime($agenda_value[0]['Start_date']));
                        }
                        else
                        {
                            echo date("d/m/Y",strtotime($agenda_value[0]['Start_date']));
                        } ?>
                </div>
              </div>
              <div class="seminar-location">
                <?php if(!empty($agenda_value[0]['custom_location'])){ ?>
                <i class="glyphicon glyphicon-map-marker" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></i>
                <?php } ?>
                <div class="seminar-room-name">
                  <h3><?php echo ucfirst($agenda_value[0]['custom_location']); ?></h3>
                  <?php if(!empty($agenda_value[0]['Address_map'])){ $a=base_url().'Maps/'.$acc_name.'/'.$event.'/View/'.$agenda_value[0]['Address_map_id']; echo "<a href='".$a."'>"."View on Map</a>"; } ?>
                </div>
              </div>
            </div>
            <?php if(!empty($agenda_value[0]['session_image'])){ ?>
            <div class="venue-img">
              <img src="<?php echo base_url().'assets/user_files/'.$agenda_value[0]['session_image']; ?>" alt="image">
            </div>
            <?php } ?>
          </div>
          <div class="button-box-wrapper text-center">
            <?php if(!empty($user[0]->Id)){ ?>
            <a href="javascript:void(0);" id="save_agenda_button" onclick="save_session();" <?php if(in_array($agenda_value[0]['Id'],$agenda_id) || in_array($agenda_value[0]['Id'],$pending_agenda_id)){ ?> class="btn btn-success" <?php }else{ ?> class="btn btn-green" <?php } ?>>save</a>
            <?php if($agenda_value[0]['show_checking_in']=='1'){  
              $chekin=explode(",",$user_agenda_list[0]['check_in_agenda_id']); 
              if(in_array($agenda_value[0]['Id'],$chekin))
              {
                $calss="btn btn-success pull-center";
                ?>
                <input type="hidden" value="1" id="is_session_checkin"/>
                <?php 
              }
              else
              {
                ?>
                <input type="hidden" value="0" id="is_session_checkin"/>
                <?php
                $calss="btn btn-green pull-center"; 
              }
            ?>
            <a <?php if(count($prv_rating)>0 && in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="margin-bottom:10px;color: #ffffff;" <?php }else{ ?> style="display:none;" <?php } ?>  class="<?php echo $calss; ?>" name="Check_In" id="check_btn_<?php echo $agenda_value[0]['Id']; ?>" value="check_btn_<?php echo $agenda_value[0]['Id'] ?>" onclick="updatecheckin();">
                Check In
            </a>  
            <a id="prev_session_rating_btn" <?php if(count($prv_rating)<=0 && in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="margin-bottom:10px;color: #ffffff;" <?php }else{ ?> style="display:none;" <?php } ?> class="btn btn-green" data-toggle="modal" data-target="#rating_model">Check In</a>
            <?php } ?>
            <?php if(date('Y-m-d H:i:s') <= date('Y-m-d H:i:s',strtotime($agenda_value[0]['Start_date'].' '.$agenda_value[0]['Start_time']))){ ?>
            <a type="button" <?php if(in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="color: #ffffff;" <?php }else{ ?> style="color: #ffffff;display: none;" <?php } if(count($reminder_data)>0){ ?> class="btn btn-success" <?php }else{?> class="btn btn-green"  <?php } ?> name="reminder" id="set_reminder_<?php echo $agenda_value[0]['Id']; ?>" value="set_reminder_<?php echo $agenda_value[0]['Id'] ?>" data-toggle="modal" data-target="#reminder_model">
            Set Reminder
            </a>
            <?php } ?>
            <?php } ?>           
            </div>
        </div>
      </div>
      <div class="views-intro-block text-center">
        <p>
        	<?php if(!empty($agenda_value[0]['description'])) {
  	      $user = $this->session->userdata('current_user');
  	      if($user[0]->Role_id==4)
  	      {
  	        $extra=json_decode($custom[0]['extra_column'],true);
  	        foreach ($extra as $key => $value) {
  	          $keyword="{".str_replace(' ', '', $key)."}";
  	          if(stripos(strip_tags($agenda_value[0]['description'],$keyword)) !== false)
  	          {
  	            $agenda_value[0]['description']=str_ireplace($keyword, $value,$agenda_value[0]['description']);
  	          }
  	        }
  	      }
  	    ?>
        <?php echo html_entity_decode($agenda_value[0]['description']); ?>
      	<?php } ?>
        </p>
        <?php if(($agenda_value[0]['show_rating']=='1' || $agenda_value[0]['allow_comments']=='1') && !empty($user[0]->Id)){ ?>
        <a href="javascript:void(0);" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>" data-toggle="modal" data-target="#feedback_models_popup" class="buttons">provide feedback</a>
        <?php } ?>
      </div>
      <?php if(count($agenda_value[0]['speaker']) > 0){ ?>
      <div class="hosted-by-wrapper">
        <h3>Hosted by:</h3>
        <div class="hosted-by-content">
          <div class="hosted-by-carousel">
            <?php foreach ($agenda_value[0]['speaker'] as $key => $value) { ?>
            <div class="item">
              <div class="hosted-by-box">
                <div class="host-img">
                  <?php $url= base_url()."assets/user_files/".$value['Logo']; if(file_exists("assets/user_files/".$value['Logo']) && !empty($value['Logo'])) {  ?>
                    <img class="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value['Logo'];?>" >
                  <?php } else { ?>
                    <img class="" src="<?php echo base_url(); ?>assets/images/anonymous_speaker.jpg">
                  <?php  }  ?>
                </div>
                <div class="host-name">
                  <a href="<?php echo base_url().'Speakers/'.$acc_name.'/'.$event.'/View/'.$value['speaker_id']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?><span class="host-post"><?php echo $value['is_moderator']=='1' ? 'moderator' : 'speaker'; ?></span></a>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } if(!empty($agenda_value[0]['document_id']) || !empty($agenda_value[0]['presentation_id']) || !empty($agenda_value[0]['survey_id']) || !empty($agenda_value[0]['qasession_id'])){ ?>
      <div class="support-section-wrapper">
        <h3>support material:</h3>
        <div class="support-section-content">
          <div class="support-section-box">
            <?php if($agenda_value[0]['document_id']!='') { ?> 
            <div class="support-boxes">
              <div class="support-boxes-img support-boxes-bgcolor" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>">
                <img src="<?php echo base_url().'assets/images/Docs_Icon.png'; ?>">
              </div>
              <div class="support-boxes-name">
                <a href="<?php echo base_url().'assets/user_documents/'.$agenda_value[0]['document_file']; ?>">
                  <span><?php echo $agenda_value[0]['docs_title']; ?></span>
                  <span>Documents</span>
                </a>
              </div>
            </div> 
            <?php } if(!empty($agenda_value[0]['presentation_id'])){ ?>  
            <div class="support-boxes">
              <div class="support-boxes-img support-boxes-bgcolor" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>">
                <img src="<?php echo base_url().'assets/images/Presentation_Icon.png'; ?>">
              </div>
              <div class="support-boxes-name">
                <a href="<?php echo base_url().'Presentation/'.$acc_name.'/'.$event.'/View_presentation/'.$agenda_value[0]['presentation_id']; ?>">
                  <span><?php echo $agenda_value[0]['presentation_heading']; ?></span>
                  <span>Presentation</span>  
                </a>
              </div>
            </div>
            <?php } if(!empty($agenda_value[0]['survey_id'])){ ?>  
            <div class="support-boxes">
              <div class="support-boxes-img support-boxes-bgcolor" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>">
                <img src="<?php echo base_url().'assets/images/Survey-Polls_Icon.png'; ?>">
              </div>
              <div class="support-boxes-name">
                <a href="<?php echo base_url().'Surveys/'.$acc_name.'/'.$event.'/question_index/'.$agenda_value[0]['survey_id']; ?>">
                  <span><?php echo $agenda_value[0]['survey_name']; ?></span>
                  <span>Surveys</span>  
                </a>
              </div>
            </div>
            <?php } if(!empty($agenda_value[0]['qasession_id'])){  ?> 
            <div class="support-boxes">
              <div class="support-boxes-img support-boxes-bgcolor" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>">
                <img src="<?php echo base_url().'assets/images/Q&A_Icon.png'; ?>">
              </div>
              <div class="support-boxes-name">
                <a href="<?php echo base_url().'QA/'.$acc_name.'/'.$event.'/view/'.$agenda_value[0]['qasession_id'];s ?>">
                  <span><?php echo $agenda_value[0]['Session_name']; ?></span>
                  <span>Q&amp;A</span>  
                </a>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<div id="feedback_models_popup" class="modal fade " role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">cancel <span>&times;</span></button>
        <h4>Rate This Session :</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php if($agenda_value[0]['show_rating']=='1'){ ?>
          <div class="row star-center session-rating-star" style="text-align:center;">
            <fieldset id='session_rating_field' class="rating">
              <input class="stars" <?php if($rating_data[0]['rating']=='5'){ ?> checked="checked" <?php } ?> type="radio" id="star5" name="rating" value="5" />
              <label class = "full" for="star5" title="Awesome - 5 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
              <input class="stars" <?php if($rating_data[0]['rating']=='4'){ ?> checked="checked" <?php } ?> type="radio" id="star4" name="rating" value="4" />
              <label class = "full" for="star4" title="Pretty good - 4 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
              <input class="stars" <?php if($rating_data[0]['rating']=='3'){ ?> checked="checked" <?php } ?> type="radio" id="star3" name="rating" value="3" />
              <label class = "full" for="star3" title="Meh - 3 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
              <input class="stars" <?php if($rating_data[0]['rating']=='2'){ ?> checked="checked" <?php } ?> type="radio" id="star2" name="rating" value="2" />
              <label class = "full" for="star2" title="Kinda bad - 2 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
              <input class="stars" <?php if($rating_data[0]['rating']=='1'){ ?> checked="checked" <?php } ?> type="radio" id="star1" name="rating" value="1" />
              <label class = "full" for="star1" title="Sucks big time - 1 star" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
            </fieldset>
          </div>
          <?php } if($agenda_value[0]['allow_comments']=='1' && count($agenda_comment) <= 0){ ?>
          <div class="col-sm-12" id="comment_box_div">
            <h4>Comments :</h4>
            <div class="form-group">
              <textarea name="comment_box_textarea" class="form-control name_group required" id="comment_box_textarea" placeholder="Leave your comments here…"></textarea>
              <span for="comment_box_textarea" id="comment_box_textarea_error_span" class="help-block" style="display: none;">This field is required.</span>
            </div> 
            <div class="errorHandler alert alert-danger no-display" style="display: none;" id="error_msg">
              <i class="fa fa-remove-sign" id="comment_error_con"> </i> 
            </div>
            <div class="popup-button-set text-center">
              <div class="col-md-5">
                <a href="javascript:void(0);" id="Comment_submit_button" name="Comment_submit_button" class="btn btn-green btn-block">
                Send <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="errorHandler alert alert-success no-display" style="display: none;" id="success_msg"><i class="fa fa-remove-sign" id="comment_success_con"></i></div> 
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="rating_model" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h3 style="text-align:center;">You must rate:</h3>
        <h3 style="text-align:center;"><?php echo $prev_agenda[0]['Heading']; ?></h3>
        <h4 style="text-align:center;"><?php echo date("h:i A",strtotime($prev_agenda[0]['Start_date'].' '.$prev_agenda[0]['Start_time'])) ?></h4>
        <h3>before  you can Check In  to  this  session</h3>
        <?php if($show_prev_checking=='1'){ ?>
        <div class="row">
          <a type="buton" onclick="saveprecheckingsave();" id="pre_checkin_btn" class="btn btn-green btn-block">Check In</a>
        </div>
        <?php } ?>
        <div class="row star-center session-rating-star" style="text-align:center;">
          <fieldset id='prev_rating_session' class="rating" <?php if($show_prev_checking=='1'){ ?> style="display:none;" <?php } ?>>
            <input class="stars" type="radio" id="prev_star5" name="rating" value="5" />
            <label class = "full" for="prev_star5" title="Awesome - 5 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
            <input class="stars" type="radio" id="prev_star4" name="rating" value="4" />
            <label class = "full" for="prev_star4" title="Pretty good - 4 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
            <input class="stars" type="radio" id="prev_star3" name="rating" value="3" />
            <label class = "full" for="prev_star3" title="Meh - 3 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
            <input class="stars" type="radio" id="prev_star2" name="rating" value="2" />
            <label class = "full" for="prev_star2" title="Kinda bad - 2 stars" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
            <input class="stars" type="radio" id="prev_star1" name="rating" value="1" />
            <label class = "full" for="prev_star1" title="Sucks big time - 1 star" style="color: <?php echo $event_templates[0]['Top_background_color']; ?>"></label>
          </fieldset>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="error_msg_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="display:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> 
        </div>
        <div class="modal-body" id="error_msg_body">
          <b>You must Check In before you can rate this session.</b>
        </div>
        <div class="modal-footer">
          <button type="button" style="border:none;color:blue;" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="reminder_model" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 style="text-align: center;" class="modal-title">Set Reminder Alert</h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-4">
              <a <?php if($reminder_data[0]['reminder_time']=='5'){ ?> class="btn btn-success btn-block" <?php }else{ ?> class="btn btn-green btn-block" <?php } ?> name="5_minutes" id="5_minutes" onclick="set_reminder('5');">5 Minutes</a>
            </div>
            <div class="col-sm-4">
              <a <?php if($reminder_data[0]['reminder_time']=='15'){ ?> class="btn btn-success btn-block" <?php }else{ ?> class="btn btn-green btn-block" <?php } ?> name="15_minutes" id="15_minutes" onclick="set_reminder('15');">15 Minutes</a>
            </div>
            <div class="col-sm-4">
              <a <?php if($reminder_data[0]['reminder_time']=='30'){ ?> class="btn btn-success btn-block" <?php }else{ ?> class="btn btn-green btn-block" <?php } ?> name="30_minutes" id="30_minutes" onclick="set_reminder('30');">30 Minutes</a>
            </div>
          </div>
          <div class="col-sm-12">
            <a <?php if($reminder_data[0]['reminder_time']=='0'){ ?> class="btn btn-success btn-block" <?php }else{ ?> class="btn btn-default btn-block" <?php } ?>  name="no_alert" id="no_alert" onclick="set_reminder('0');"><i class="fa fa-times"></i> No Alert</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function save_session()
{
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/save_user_session/'.$agenda_value[0]['Id']; ?>",
    error:function(err){

    },
    success:function(data){
      var result=data.split("###");
      if($.trim(result[0])=="Success")
      {
        $('#save_agenda_button').attr('class',result[1]);
        var on_pending="<?php echo $event_templates[0]['on_pending_agenda']; ?>";
        if($.trim(result[1])=="btn btn-success")
        {
          $("#con").html("Session is Saved Successfully.");
        }
        else
        {
          $("#con").html("Session is Deleted Successfully."); 
        }
        $("#msg").show();
        if(on_pending!='1'){
          var prevsession="<?php echo count($prv_rating); ?>"
          if(prevsession>0)
          {
            if($.trim(result[1])=="btn btn-success")
            {
              $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr("style","margin-bottom:10px;color: #ffffff;");
              $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").show();
              $("#set_reminder_<?php echo $agenda_value[0]['Id']; ?>").show();
            }
            else
            {
              $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").hide();
              $("#set_reminder_<?php echo $agenda_value[0]['Id']; ?>").hide(); 
            }
          }
          else
          {
            if($.trim(result[1])=="btn btn-success")
            {
              $('#prev_session_rating_btn').attr("style","margin-bottom:10px;color: #ffffff;");
              $("#prev_session_rating_btn").show();
              $("#set_reminder_<?php echo $agenda_value[0]['Id']; ?>").show();
            }
            else
            {
              $("#prev_session_rating_btn").hide(); 
              $("#set_reminder_<?php echo $agenda_value[0]['Id']; ?>").hide(); 
            }
          }
        }
      }
      else
      {
        var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        if(regex .test(result[1]))
        {
          window.location.href=result[1];
        }
      }
    },  
  });
}
function updatecheckin()
{
  $.ajax({
    url: "<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$event; ?>/user_check_in_update/<?php echo $agenda_value[0]['Id']; ?>",
    success: function(result)
    {
      var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
      if(regex .test(result))
      {
        window.location.href=result;
      }
      else
      { 
        if($.trim(result)=="btn btn-success pull-center")
        {
          $('#is_session_checkin').val('1');
        }
        else
        {
          $('#is_session_checkin').val('0');
        }
        $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr('class',result);
      }
    }
  });
}
$("#session_rating_field >.stars").click(function () {
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/user_rating_save/'.$agenda_value[0]['Id']; ?>",
    type:'post',
    data:"rating="+$(this).val(),
    success:function(result){
      var data=result.split('###');
      if($.trim(data[0])=="error")
      {
        $('#error_msg_body').html('<b>'+data[1]+'</b>');
        $('#error_msg_model').modal({show: 'false'}); 
      }
    }
  });
});
$('#prev_rating_session > .stars').click(function(){
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/user_rating_save/'.$prev_agenda[0]['Id']; ?>",
    type:'post',
    data:"rating="+$(this).val(),
    success:function(result){
      var data=result.split('###');
      if($.trim(data[0])=="error")
      {
        $('#error_msg_body').html('<b>'+data[1]+'</b>');
        $('#error_msg_model').modal({show: 'false'}); 
      }
      else
      {
        $('#prev_session_rating_btn').hide();
        $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr('style','margin-bottom:10px;color: #ffffff;');
        $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").show();
      }
      $('#rating_model').modal('toggle'); 
    }
  });
});
$('#Comment_submit_button').click(function(){
  if($('#comment_box_textarea').val()!="")
  { 
    $('#Comment_submit_button').find('i').attr('class','fa-li fa fa-spinner fa-spin');
    $('#Comment_submit_button').attr('disabled','disabled');
    $.ajax({
      url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/add_session_user_comment/'.$agenda_value[0]['Id']; ?>",
      type:'post',
      data:"comments="+$('#comment_box_textarea').val(),
      error:function(errormsg){
        $('#error_msg').html("Sothing Went wrong.");
        $('#error_msg').show();
      },
      success:function(result){
        var data=result.split("###");
        if($.trim(data[0])=="Success")
        {
          $('#success_msg').html(data[1]);
          $('#success_msg').show();
          setTimeout(function(){$('#success_msg').hide();}, 5000);
          $('#comment_box_div').remove();
        }
        else
        {
          $('#Comment_submit_button').find('i').attr('class','fa fa-arrow-circle-right');
          $('#Comment_submit_button').removeAttr('disabled');
          $('#error_msg').html(data[1]);
          $('#error_msg').show();
          setTimeout(function(){$('#error_msg').hide();}, 5000);
        }
      }
    });
  }
  else
  {
    $('#comment_box_textarea_error_span').parent().addClass('has-error');
    $('#comment_box_textarea_error_span').show();
    setTimeout(function(){$('#comment_box_textarea_error_span').hide();}, 5000);
  }
});
function saveprecheckingsave()
{
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/save_prev_check_save/'.$prev_agenda[0]['Id']; ?>",
    success:function(result){
      if($.trim(result)=="success")
      {
        $('#pre_checkin_btn').hide();
        $('#prev_rating_session').show();
      }
    }
  });
}
function set_reminder(time)
{
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/set_user_reminader/'.$agenda_value[0]['Id'] ?>",
    type:'post',
    data:'time='+time,
    success:function(data){
      $('#reminder_model').modal({show: 'false'}); 
      location.reload();
    }
  });
}    
</script>