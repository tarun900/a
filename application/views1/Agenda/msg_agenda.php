<?php $acc_name=$this->session->userdata('acc_name');
$event=$this->uri->segment(3); ?>
<div class="agenda_content agenda_content-new agenda_inner">
  <div class="panel panel-white box-effet">
		<div class="agenda_inner_wrap left" style="text-align: center;">
			<h3 class="left">
				<?php  echo $msg; ?>
			</h3>
		</div>
		<a href="<?php echo base_url().'Agenda/'.$acc_name.'/'.$event; ?>" style="margin-bottom:10px;" class="btn btn-green pull-center" >
            Go back to the agenda 
        </a>
	</div>
</div>
