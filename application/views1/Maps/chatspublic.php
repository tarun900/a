<?php
$Sid = $this->uri->segment(4);
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
if(empty($user))
{
   $url=base_url().'Messages/'.$acc_name.'/'.$subdomain.'/publicmsg';
}
?>

<div class="agenda_content agenda-user-content">
     <div class="row row-box">
          <form class="navbar-search" id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Speakers/<?php echo $acc_name."/".$subdomain; ?>/upload_imag' style="clear:both">
               <div class="facebok-container">
                    <div class="facebok-main">

                         <div class="facebok-head">
                               <h4><b>Send a public message to everyone in the app here</b></h4>
                              <ul>
                                   <li class="facebok-head-status">
                                        <span>Message</span>
                                   </li>
                                   <li class="facebok-head-photo"> 
                                        <span> Add Photo </span>
                                        <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/></div>
                                        <div id='imageloadbutton'>
                                             <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple />
                                        </div>
                                   </li>
                              </ul>
                         </div>
                         <div class="facebok-middle">
                              <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text search-query span2" id="searchbox" placeholder="Type your message here..." id="Message" name="Message"></textarea>
                         </div>
                         <div class="fb-upload-img-box">
                              <ul id='preview'>
                              </ul>
                              <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                         </div>
                         <div class="facebok-footer clearfix">
                              <ul class="footer-left">
                                   <li class="camera-icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                              </ul>
                              <ul class="footer-right">
                                   <input type="hidden" value="1" name="ispublic">
                                   <li class="submit-button-box">
                                        <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                             Publish Post <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                   </li>
                              </ul>
                         </div>
                    </div>    
               </div>
          </form> 
          <div class="tabbable msg_section">
               <div class="tab-content clearfix box-effect clearfix">
                    <div class="tab-pane fade in active clearfix" id="myTab6_example1">
                         <div class="">     
                              <?php
                              $Sid = $this->uri->segment(4);
                              $user = $this->session->userdata('current_user');
                              $lid = $user[0]->Id;
                              ?>
                              <div class="col-md-12 col-lg-12 col-sm-12" id="preview_msg_public">
                                   <?php
                                   echo '<div style="padding-top: 20px;">
                                   <div id="messages">';
                                   echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts<br/></h3>';
                                   foreach ($view_chats1 as $key => $value)
                                   {
                                        echo "<div class='message_container'>";
                                        if ($value['Sender_id'] == $user[0]->Id)
                                        {
                                             echo "<div class='msg_edit-view-box'>";
                                             echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                                             echo "</div>";
                                             echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                                             echo "Delete";
                                             echo "</div>";
                                             echo "</div>";
                                        }
                                        echo "<div class='msg_main_body'>";
                                        echo "<div class='message_img'>";
                                        if ($value['Senderlogo'] != "")
                                        {
                                             echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                                        }
                                        else
                                        {
                                             echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                                        }
                                        echo "</div>";
                                        echo "<div class='msg_fromname'>";
                                        $t=time().$key;
                                        echo '<a href="#" class="tooltip_data">';
                                        echo ucfirst($value['Sendername']);
                                        echo '</a>';
                                        echo "</div>";
                                        if (!empty($value['Receiver_id']))
                                        {
                                             echo "<div class='msg_with'>";
                                             echo "with";
                                             echo "</div>";
                                             echo "<div class='msg_toname'>";
                                             $t=time().$key;
                                             echo '<a href="#" class="tooltip_data">';
                                             echo ucfirst($value['Recivername']);
                                             echo '</a>';                                               
                                             echo "</div>";
                                        }
                                        echo "</div>";

                                        echo "<div class='msg_date'>";
                                        echo timeAgo(strtotime($value['Time']));
                                        echo "</div>";
                                        echo "<div class='msg_message'>";
                                        echo $value['Message'];
                                        echo "</div>";

                                        $img_data = json_decode($value['image']);
                                        foreach ($img_data as $kimg => $valimg)
                                        {
                                             echo "<div class='msg_photo'>";
                                             echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                                             echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                                             echo "</a>";
                                             echo "</div>";
                                        }

                                        echo "<div class='toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                                        echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                        <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                                        
                                        if ($user[0]->Logo!="")
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                        }
                                        
                                        echo "</div>
                                        <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
                                        <div class='photo_view_icon'>     
                                        <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                                        <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />
                                             
                                        </form>
                                        <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                                        if (!empty($view_chats1[$key]['comment']))
                                        {
                                             $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                                             $i = 0;
                                             $flag = false;
                                             foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                                             {
                                                  if ($i > 3)
                                                  {
                                                       $classadded = 'comment_msg_hide';
                                                  }
                                                  else
                                                  {
                                                       $classadded = '';
                                                  }

                                                  echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                                  if ($cval['Logo'] != "")
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                  }
                                                  else
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                  }

                                        echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>" . ucfirst($cval['user_name']) . "</div>
                                        <div class='comment_text'>" . ucfirst($cval['comment']) . "</div>
                                        <div class='comment_text' style='float:right;padding-left:13px;'>";
                                        ?>
                                        <?php timeAgo(strtotime($cval['Time'])); ?>
                                        <?php echo"</div> </div>";

                                             if ($cval['image'] != "")
                                             {
                                                  $image_comment = json_decode($cval['image']);
                                                  echo "<div class='msg_photo'>";
                                                  echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                                  echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                                  echo "</a>";
                                                  echo "</div>";
                                             }
                                             if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                             {
                                                  echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                             }
                                             echo "</div>";


                                             if ($i > 3 && $flag == false)
                                             {
                                                  $flag = true;
                                                  echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                             }

                                             $i++;
                                             }
                                        }
                                        echo "</div>
                                        </div>";
                                        echo "</div>";
                                   }
                                   echo "</div></div>";
                                   ?>
                              </div>
                              <?php if (count($view_chats1) > 4) { ?>
                                   <div id="loadmore_1" class="load_btn">
                                        <a href="javascript: void(0)" onclick="loadmore(10, 1);" class="loadmore panel-green"> 
                                             Load More
                                        </a>
                                   </div>
                              <?php } ?>
                              <?php if (!empty($social_url)) { ?>
                                   <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                             <div class="social-icons">
                                                  <ul class="navbar-right">
                                                       <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                                                                 Visit my Website
                                                            </a>
                                                       </li>
                                                       <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                                                                 Follow me on Twitter
                                                            </a>
                                                       </li>
                                                       <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                                                                 Follow me on Facebook
                                                            </a>
                                                       </li>
                                                       <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                                                            <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                                                                 Follow me on LinkedIn
                                                            </a>
                                                       </li>   
                                                  </ul>
                                             </div>
                                        </div>                                
                                   </div> 
                              <?php } ?>                                
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">

     function loadmore(str, istype)
     {

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name.'/'.$subdomain; ?>/loadmore/" + (str - 5) + "/" + str + "/" + istype,
               type: "POST",
               async: true,
               success: function(result)
               {
                    if (result != "")
                    {
                         jQuery('#Message').val('');

                         if (istype == '1')
                         {
                              // jQuery("#preview_msg_public").html('');
                              jQuery("#preview_msg_public").append(result);
                         }
                         else
                         {
                              //jQuery("#preview_msg_private").html('');
                              jQuery("#preview_msg_private").append(result);
                         }
                         var a = parseInt(str) + 5;
                         var loadfun = "loadmore(" + a + "," + istype + ");";
                         jQuery("#loadmore_" + istype).html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                         jQuery("#preview").html('');
                         jQuery(".addmore_photo").css('display', 'none');

                         jQuery(".msg_edit-arrow").on('click', function()
                         {
                              var id = jQuery(this).attr('data-id');
                              jQuery("#msg_edit-view" + id).slideToggle("slow");
                         });
                    }
                    else
                    {
                         jQuery("#loadmore_" + istype).css('display', 'none');
                    }
               }

          });
     }


     function sendmessage()
     {
       var user="<?php echo !empty($user) ? '1' : '0'; ?>";
       alert(user);
         if(user==1)
         {
          jQuery("#sendbtn").html('Submitting <i class="fa fa-refresh fa-spin"></i>');
          jQuery("#sendbtn").attr('disabled','disabled');
          var str = jQuery("#imageform").serialize();
          var values = jQuery("input[name='unpublished_photo[]']");

          var flag=false;
          var flag1=true;
          
          if(jQuery.trim(jQuery("#Message").val()) != "")
          {
               flag=true;
          }
          /*if(values.val() !=undefined)
          {
               console.log(values.val());
               flag1=true;
          }*/
          
          if ((flag==false && flag1==false))
          {
               alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               jQuery("#sendbtn").removeAttr('disabled');
               return false;
          }

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$subdomain; ?>/chatspublic",
                              data: str,
                              type: "POST",
                              async: true,
                              success: function(result)
                              {
                                   jQuery("#loadmore_1").css('display', 'none');
                                   
                                   var loadfun = "loadmore(10,1);";
                                   jQuery("#loadmore_1").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                                   
                                   jQuery('#Message').val('');

                                   jQuery("#preview_msg_public").html('');
                                   jQuery("#preview_msg_public").html(result);

                                   jQuery("#preview").html('');
                                   jQuery(".addmore_photo").css('display', 'none');

                                   jQuery(".msg_edit-arrow").on('click', function()
                                   {
                                        var id = jQuery(this).attr('data-id');
                                        jQuery("#msg_edit-view" + id).slideToggle("slow");
                                   });
                                   
                                   jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
                                   jQuery("#sendbtn").removeAttr('disabled');
                              }

                         });
                         }
                         else
                        {

                              var url="<?php echo $url.'?action=1'; ?>";
                              alert(url);
                              window.location.href=url;
                        }
                    }
</script>
<?php

function timeAgo($time_ago)
{
     $cur_time = time();
     $time_elapsed = $cur_time - $time_ago;

     $seconds = $time_elapsed;
     $minutes = round($time_elapsed / 60);
     $hours = round($time_elapsed / 3600);
     $days = round($time_elapsed / 86400);
     $weeks = round($time_elapsed / 604800);
     $months = round($time_elapsed / 2600640);
     $years = round($time_elapsed / 31207680);
     // Seconds
     if ($seconds <= 60)
     {
          echo "$seconds seconds ago";
     }
     //Minutes
     else if ($minutes <= 60)
     {
          if ($minutes == 1)
          {
               echo "one minute ago";
          }
          else
          {
               echo "$minutes minutes ago";
          }
     }
     //Hours
     else if ($hours <= 24)
     {
          if ($hours == 1)
          {
               echo "an hour ago";
          }
          else
          {
               echo "$hours hours ago";
          }
     }
     //Days
     else if ($days <= 7)
     {
          if ($days == 1)
          {
               echo "yesterday";
          }
          else
          {
               echo "$days days ago";
          }
     }
     //Weeks
     else if ($weeks <= 4.3)
     {
          if ($weeks == 1)
          {
               echo "a week ago";
          }
          else
          {
               echo "$weeks weeks ago";
          }
     }
     //Months
     else if ($months <= 12)
     {
          if ($months == 1)
          {
               echo "a month ago";
          }
          else
          {
               echo "$months months ago";
          }
     }
     //Years
     else
     {
          if ($years == 1)
          {
               echo "one year ago";
          }
          else
          {
               echo "$years years ago";
          }
     }
}
?>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
<script type="text/javascript">
     jQuery.noConflict();


     jQuery(document).ready(function() {

          jQuery(document).on('mouseover', '.msg_photo', function() {
               var my_class = $(this).find('a').attr('class');
               $("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
          });
          
          jQuery(document).on('mouseover', '.msg_fromname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'), maxHeight: '95%',width:"50%"});
          });
          
          jQuery(document).on('mouseover', '.msg_toname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'),maxHeight: '95%',width:"50%"});
          });

          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("#photoimg").removeAttr('multiple');
          }
          else
          {
               jQuery("#photoimg").attr('multiple');
          }

          jQuery(".msg_edit-arrow").on('click', function() {
               var id = jQuery(this).attr('data-id');
               jQuery("#msg_edit-view" + id).slideToggle("slow");
          });
     });

     jQuery(document).ready(function() {
          console.log('onReady');
          jQuery("#takePictureField").on("change", gotPic);
          jQuery("#preview").load(getSwatches);
          desiredWidth = window.innerWidth;

          if (!("url" in window) && ("webkitURL" in window)) {
               window.URL = window.webkitURL;
          }

     });

     function getSwatches() {
          var colorArr = createPalette(jQuery("#preview"), 5);
          for (var i = 0; i < Math.min(5, colorArr.length); i++) {
               jQuery("#swatch" + i).css("background-color", "rgb(" + colorArr[i][0] + "," + colorArr[i][1] + "," + colorArr[i][2] + ")");
               console.log(jQuery("#swatch" + i).css("background-color"));
          }
     }

     //Credit: https://www.youtube.com/watch?v=EPYnGFEcis4&feature=youtube_gdata_player
     function gotPic(event) {
          if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {
               jQuery("#preview").append("<li><div><input type='hidden' name='unpublished_photo[]' value='" + event.target.files[0] + "'><img src='" + URL.createObjectURL(event.target.files[0]) + "' class='imgList'><button class='photo_remove_btn' type='button' title='' id='" + event.target.files[0].name + "'>&nbsp;</button></div></li>");
               jQuery(".photo_remove_btn").click(function()
               {
                    jQuery(".addmore_photo").css("display", "none");
                    jQuery(this).parent().parent().remove();
                    jQuery("#preview li").each(function() {
                         jQuery(".addmore_photo").css("display", "block");
                    });
               });
               //jQuery("#preview").attr("src",URL.createObjectURL(event.target.files[0]));
          }
     }

     function addcomment(id, istype)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
          var str = jQuery("#commentform" + id).serialize();
          var values = jQuery("input[name='cmphto[]']");
          
          if (jQuery.trim(jQuery("#comment_text" + id).val()) != "" || values.val() != "")
          {
               jQuery.ajax({
                    url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/commentaddpublic/" + istype,
                    data: str,
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                         jQuery("#comment_conten" + id).html(result);
                         jQuery("#comment_text" + id).val('');
                         jQuery("#cpreview" + id).html('');
                         jQuery('#camera_icon_comment').show();
                    }

               });
          }
          else
          {
               alert("Please write something or photo to add comment.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               return false;
          }
          return false;
          }
          else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }
     }

     function viewmore_comment(id)
     {
          jQuery("#comment_conten" + id + " div").each(function() {
               jQuery(this).removeClass('comment_msg_hide');
          });
          jQuery("#comment_viewmore" + id).remove();
     }

     function removemsg(id, e)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/delete_message/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().parent().remove();
               }

          });
          }
          else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }
     }

     function removecomment(id, e)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/delete_comment/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().remove();
               }

          });
          }
          else
          {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
          }
     }

     function comment_photo(id)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
          //jQuery('#cmphto'+id).off('click').on('change', function() 
          //{
          jQuery("#commentform" + id).ajaxForm({target: '#cpreview' + id,
               beforeSubmit: function() {

                    //console.log('ttest');
                    jQuery('#camera_icon_comment').hide();
                    jQuery("#imageloadstatus").show();
                    jQuery("#imageloadbutton").hide();
               },
               success: function() {
                    //console.log('test');
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();
                    jQuery(".comment_section_btn").click(function()
                    {
                         jQuery(".addmore_photo").css("display", "none");
                         jQuery(this).parent().remove();
                         jQuery("#preview li").each(function() {
                              jQuery(".addmore_photo").css("display", "block");
                         });
                         jQuery('#camera_icon_comment').show();
                    });
               },
               error: function() {
                    //console.log('xtest');
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();

               }}).submit();
          //});

          return false;
          }
          else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }

     }
     function callfunmsg()
     {
          
     }
</script>
