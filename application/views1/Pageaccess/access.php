<div class="error-full-page" id="access-error">
	<div class="row">
		<div class="col-sm-12 page-error animated shake">
            <?php /*?><img src="<?php echo base_url(); ?>assets/images/access-logo.png" alt="access-logo"><?php */?>
			<div class="error-number text-azure">
				Please Sign up or Login to open this app
			</div>
			<div class="error-details col-sm-6 col-sm-offset-3">
				<!-- <p>
					Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum 
					Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum 
					Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum 
				</p> -->
				<div class="button-tag">
                    <a href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$Subdomain; ?>" class="btn btn-red">
						Login 
					</a>
                    <a href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$Subdomain; ?>?id=1" class="btn btn-primary">
						Sign Up
					</a>
                </div>
             
			</div>
		</div>
	</div>
</div>