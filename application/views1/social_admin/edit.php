<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#social_list" data-toggle="tab">
                            Add Social media links
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
					
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="social_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Facebook URL
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Facebook Url" id="facebook_url" name="facebook_url" value="<?php echo $social[0]['facebook_url']; ?>" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Twitter URL
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Twitter Url" id="twitter_url" name="twitter_url" class="form-control name_group" value="<?php echo $social[0]['twitter_url']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Instagram URL
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Instagram Url" id="instagram_url" name="instagram_url" class="form-control name_group" value="<?php echo $social[0]['instagram_url']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    LinkedIn URL
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="LinkedIn Url" id="linkedin_url" name="linkedin_url" class="form-control name_group" value="<?php echo $social[0]['linkedin_url']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Pinterest URL 
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Pinterest Url" id="pinterest_url" name="pinterest_url" class="form-control name_group" value="<?php echo $social[0]['pinterest_url']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Youtube URL 
                                </label>
                                <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Youtube Url" id="youtube_url" name="youtube_url" class="form-control name_group" value="<?php echo $social[0]['youtube_url']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Update <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
    </div>
</div>