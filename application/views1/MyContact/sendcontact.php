<div>
     <div class="panel-heading">
          <h4 class="panel-title">Send <span class="text-bold">Contact</span></h4>
     </div>
     <?php
                    if(!empty($contanctuser))
                    {
                         ?>
     <div class="panel-body">
          <form role="form" method="post" id="form" action="" enctype="multipart/form-data" onsubmit="return false;">
               <div class="form-group">
                    <div class="successHandler alert alert-success no-display" style="display: none;">
                         <i class="fa fa-ok"></i> Contact shared successful!
                    </div>
                    
                         <label class="control-label" for="form-field-1">
                              Select Contact <span class="symbol required" id="showdatetime_remark"></span>
                         </label>
                         <div class="input-group">
                              <select multiple="multiple" id="sendcontact" name="sendcontact[]" class="form-control search-select">
                                   <?php
                                   foreach ($contanctuser as $key => $valuesend)
                                   {
                                        ?>
                                        <option value="<?php echo $valuesend['uid']; ?>"><?php echo $valuesend['Firstname'] . ' ' . $valuesend['Lastname']; ?></option>
                                        <?php
                                   }
                                   ?>
                              </select>
                              <input type="hidden" name="Sharedwith_id" value="<?php echo $Sharedwith_id; ?>">
                              <input type="hidden" name="Event_id" value="<?php echo $Event_id; ?>">
                         </div>
                    
               </div>
               <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                    </label>
                    <div class="col-md-4">
                         <button class="btn btn-yellow btn-block" type="button" onclick="sendcontacts();">
                              Submit <i class="fa fa-arrow-circle-right"></i>
                         </button>
                    </div>
               </div>
          </form>
     </div>
     <?php
                    }
                    else
                    {
                         ?>
     <div class="panel-body">
     <span class="text-center"><?php echo "No contanct avilable"; ?> </span>
     </div>
     <?php
                    }
                    ?>
</div>
<script type="text/javascript">
     function sendcontacts()
     {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>MyContact/<?php echo $Subdomain; ?>/senduserdetails",
               type: "POST",
               data: jQuery("#form").serialize(),
               async: true,
               success: function(result)
               {    
                    jQuery(".alert-success").css('display','block');
                    
                    setTimeout(function(){
                         jQuery("#cboxClose").click();
                    }, 2000);
               }

          });
     }
</script>