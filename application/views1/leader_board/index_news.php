<?php $acc_name=$this->session->userdata('acc_name');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newanalytics.css?'.time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css"/>
<style type="text/css">
.chart_wrap {
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.chart {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.barchart_wrap {
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.barchart {
	position: absolute;
	top: 0;
	left: 0;
	width: 90%;
	height: 100%;
}

#from_datetime,#to_datetime {
    width: 70%;
    display: inline-block;
}

.form-group span { 
    float: left;
    width: 30%;
    display: inline-block;
    text-align: right;
    padding-right: 10px;
    box-sizing: border-box;
    vertical-align: middle;
    padding-top: 5px;
 }
.panel .form-group { margin-bottom: 0px; }
.panel #refresh { color: #5381ce; }
.panel #refresh:hover, #refresh:focus { color: #5381ce; }
</style>
<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url().'Leader_board/index_new_time/'.$event_id; ?>">
    <div class="panel" style="background: #5381ce;font-size: 22px;color: #fff;padding: 15px;"> 

      <div class="col-md-3">
        <span style="float: left;">Select a Timeframe</span> 
      </div>

      <div class="col-md-9">
        <div class="analyicts_heading">
            <div class="form-group">
              <div class="col-sm-5">
                <span> From </span>
                <input type="text" name="from_datetime" id="from_datetime" contenteditable="false" class="form-control datetimepicker_coutome" value="<?php echo $from_datetime; ?>">
              </div>
              <div class="col-sm-5">
                <span> To </span>
                <input type="text" name="to_datetime" id="to_datetime" contenteditable="false" class="form-control datetimepicker_coutome" value="<?php echo $to_datetime; ?>">
              </div>
              <div class="col-sm-2">
                <input type="submit" class="btn btn-white" value="Refresh" id="refresh">
              </div>
            </div>
          </div>
      </div>

      <div style="clear:both"></div>
    </div>
    </form>
  </div>
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-body" style="padding: 0px;">
        <div class="tabbable">
          <ul id="myTab2" class="nav nav-tabs">
            <li class="active"> <a href="#app_activity" data-toggle="tab"> App Activity </a> </li>
            <li class=""> <a href="#posts" data-toggle="tab"> Posts </a> </li>
            <li class=""> <a href="#surveys" data-toggle="tab"> Surveys </a> </li>
          </ul>
        </div>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="app_activity">
            <div class="col-sm-12">
              <div class="col-sm-12 register-part" style="display: block;font-size: 80px;margin: 0 auto 25px;text-align: center;background: #eeeeee none repeat scroll 0 0;height: 255px;margin: 0 40px 40px 0;min-height: 255px;overflow-x: hidden;overflow-y: hidden;padding: 10px 15px 10px 15px;width: 98%;">
                <h3 class="title_show1">Registrations</h3>
                <div class="row"> 
                  <span><?php echo $total_register_user[0]['total_user']; ?></span> 
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="col-sm-6 user-part">
                <h3 class="title_show1">Most Active Users</h3>
                <?php foreach ($most_active_user as $key => $value) { ?>
                <div class="col-sm-12 panel-white shadow-panel" style="cursor: default;"> 
                  <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                    <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?>
                    <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
                    <?php }else{ ?>
                    <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                    <?php } ?>
                  </div>
                  <div class="col-sm-9 col-xs-9 user-name" style="padding:0px;">
                    <div class="col-sm-12"> <?php echo ucfirst($value['username']); ?> </div>
                    <?php if(!empty($value['Title']) && !empty($value['Company_name'])){ ?>
                    <div class="col-sm-12"> 
                      <?php echo ucfirst($value['Title']).' At '.ucfirst($value['Company_name']); ?> 
                    </div>
                    <?php } ?>
                  </div>
                  <div class="col-sm-1 col-xs-1" style="padding:0px;float:right">
                    <i class="fa fa-angle-right"></i>
                  </div> 
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-6 os-part">
                <h3 class="title_show1">Logins by Operating System</h3>
                <?php $devicenm=array('Iphone','Android'); foreach ($most_login_os as $key => $value) { if(array_search($value['device_name'],$devicenm) !== FALSE){ unset($devicenm[array_search($value['device_name'],$devicenm)]); } ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 icn-img"> 
                    <img src="<?php echo base_url().'assets/images/'.$value['device_name'].'.png'; ?>" alt="<?php echo $value['device_name']; ?>"> 
                  </div>
                  <div class="col-sm-4 col-xs-4 user-number" style="font-size: x-large;">
                    <?php echo $value['total_login_user']; ?>
                  </div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id.'/'.$value['device_name']; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                  </div>
                </div>
                <?php } $devicenm=array_values($devicenm); for($i=0;$i<count($devicenm);$i++) { ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 total-name" style="color:#5d93c5;"> 
                    <img src="<?php echo base_url().'assets/images/'.$devicenm[$i].'.png'; ?>" alt="<?php echo $devicenm[$i]; ?>"> 
                  </div>
                  <div class="col-sm-4 col-xs-4 total-number" style="font-size: x-large;"><?php echo '0'; ?></div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id.'/'.$devicenm[$i]; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a>
                  </div>
                </div>
                <?php } ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 total-name" style="color:#5d93c5;font-size: x-large;"> Total </div>
                  <div class="col-sm-4 col-xs-4 total-number" style="font-size: x-large;">
                    <?php echo array_sum(array_column($most_login_os,'total_login_user')); ?>
                  </div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="surveys">
            <div class="col-sm-12">
              <?php if(count($survey_question_pie_chart)>0){ ?>
              <div class="col-sm-9" id="show_all_piechart">
                <?php foreach ($survey_question_pie_chart as $key => $value) { ?>
                <div class="col-sm-6">
                  <h3 style="text-align: center;" class="title_show"><?php echo $key.' ('.$value['S_Name'].')'; ?></h3>
                  <div class="chart_wrap" align="center">
                    <div id='piechart<?php echo $key; ?>' class="chart"></div>
                  </div>
                  <script type="text/javascript">
                  google.load("visualization", "1", {packages:["corechart"]});
                  google.setOnLoadCallback(drawChart);
                  function drawChart() {                                       
                  var data = google.visualization.arrayToDataTable([['Options', 'No. of users'],  <?php foreach ($value['chart'] as $a => $b) {?>
                  ['<?php echo $a; ?>', <?php echo $b; ?>],
                  <?php } ?>
                  ]);
                  var options = {
                  width:'100%',
                  height:'100%',
                  align: 'right', 
                  isStacked: true,   
                  pieSliceText: 'percentage',         
                  verticalAlign: 'middle',
                  pieSliceTextStyle: { color: 'black',},
                  fontSize: 20,
                  fontName:'Lato',
                  legend:{position:'right',textStyle: {color: 'blue', fontSize: 10,fontName:'Lato'}},
                  chartArea: {
                          left: "3%",
                          top: "5%",
                          height: "75%",
                          width: "94%"
                      }
                  };
                  var chart = new 
                  google.visualization.PieChart(document.getElementById('piechart<?php echo $key;?>'));
                  chart.draw(data, options);
                  }
                  </script>
                  <div class="col-sm-6 col-xs-6"> <a class="btn btn-default btn-block drill_down_button" href="<?php echo base_url().'Leader_board/get_question_details/'.$event_id.'/'.$value['Q_id']; ?>">Drill Down</a> </div>
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-9" id="show_all_barchart" style="display: none;">
                <?php foreach ($survey_question_pie_chart as $key => $value) { ?>
                <div class="col-sm-6">
                  <h3 style="text-align: center;" class="title_show"><?php echo $key; ?></h3>
                  <div class="barchart_wrap" align="center">
                    <div id='barchart<?php echo $key; ?>' class="barchart"></div>
                  </div>
                  <script type="text/javascript">
                  google.load("visualization", "1", {packages:["corechart"]});
                  google.setOnLoadCallback(drawChart);
                  function drawChart() {                                       
                  var data = google.visualization.arrayToDataTable([['Options','No. of users',{ role: 'style' }],  <?php foreach ($value['chart'] as $a => $b) {?>
                  ['<?php echo $a; ?>', <?php echo $b; ?>,'color:<?php echo sprintf("#%06x",rand(0,16777215)); ?>;'],
                  <?php } ?>
                  ]);
                  var options = {
                  width:'90%',
                  height:'100%',
                  align: 'center',           
                  verticalAlign: 'middle',
                  isStacked: true,
                  fontSize: 20,
                  fontName:'Lato',
                  legend: {position: 'none', maxLines: 3},
                  };
                  var chart = new 
                  google.visualization.BarChart(document.getElementById('barchart<?php echo $key;?>'));
                  var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                                 { calc: "stringify",
                                   sourceColumn: 1,
                                   type: "string",
                                   role: "annotation" },
                                 2]);
                  chart.draw(view, options);
                  }
                  </script>
                  <div class="col-sm-6 col-xs-6"> <a class="btn btn-default btn-block drill_down_button" href="<?php echo base_url().'Leader_board/get_question_details/'.$event_id.'/'.$value['Q_id']; ?>">Drill Down</a> </div>
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-3 chart-type">
                <h4>Change Chart Type</h4>
                <ul>
                  <li class="active"><a class="" onclick="show_chart('pie',this);" href="javascript:void(0);">Pie</a></li>
                  <li><a class="" onclick="show_chart('bar',this);" href="javascript:void(0);">Bar</a></li>
                </ul>
              </div>
              <?php }else{ ?>
              <div class="no-survey">
                <h4>No Surveys Question For This App.</h4>
              </div>
              <?php } ?>
            </div>
          </div>
    

        <div class="tab-pane fade" id="posts">
          <?php if(count($cms_click_data) > 0){ ?>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_30">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Post</th>
                  <th>Published date</th>
                  <th>Times Viewed</th>
                  <th>Average Time Viewed</th>
                  <th>Drill Down</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($rss_post as $key => $value) { ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <!-- <td><a href="<?php echo base_url().'###'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['title']); ?></a></td> -->
                  <td><a href="#"><?php echo ucfirst($value['title']); ?></a></td>
                  <td><?=$value['date']?></td>
                  <td><?=$value['view_count']?></td>
                  <td data-sort="<?=sec2int($value['avg_time'])?>"><?=sec2read($value['avg_time'])?></td>
                  <td> <a class="btn btn-default" href="<?php echo base_url().'Leader_board/get_post_details/'.$event_id.'/'.$value['id']; ?>">Drill Down</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php }else{ ?> <div class="row"><h2 style="text-align: center;">No Posts For This App.</h2></div> <?php  } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>--> 
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script> 
<script>
    jQuery(document).ready(function() {
        //Charts.init();
        jQuery('.datetimepicker_coutome').datetimepicker({
          formatTime:'H:i',
          formatDate:'m/d/Y',
          step:5
        });
    });
    function  show_chart(type,e) 
    {
        if(type=="pie")
        {
            $('#show_all_piechart').show();
            $('#show_all_barchart').hide();
        }
        else if(type=="column")
        {
            $('#').show();
            $('#').hide();
        }
        else
        {
            $('#show_all_piechart').hide();
            $('#show_all_barchart').show();
        }
        $(e).parent().parent().find('li').removeClass('active');
        $(e).parent().addClass('active');
    }
</script>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript">      
$(document).ready(function () {  
$('#sample_30').DataTable(); 
});
</script>

