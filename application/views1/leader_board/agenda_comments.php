<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="Leader_board">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Session Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($agenda_comment as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']).' '.ucfirst($value['Lastname']); ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['comments']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
