<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span id="textshow"> Click Tracking </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-info " style="display: none;width:90%">
                                <li class="">
                                    <a data-toggle="tab" href="#click_track">
                                            Clicks
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#adverts_users">
                                            Adverts Users
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#surveys_result">
                                            Surveys Results
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#notes_track">
                                            Notes Taker
                                    </a>
                                </li>
								
                            </ul>
                        </li>

                        <li class="">
                            <a href="#message_track" data-toggle="tab">
                                Messages Tracking
                            </a>
                        </li>
                        <li class="">
                            <a href="#metting_track" data-toggle="tab">
                                Booked Metting
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#agenda_users">
                                Agenda Tracking
                            </a>
                        </li>
                        <li class="">
                            <a href="#comments_tracking" data-toggle="tab">
                                Comments Tracking
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>

                <div class="tab-content">

                    <div class="tab-pane fade active in" id="click_track">
                    <?php if(count($module_click_data) > 0){ ?>
                    <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/1" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export to CSV
                    </a><?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Clicks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($module_click_data);$i++) { /*echo '<pre>'; print_r($speakers); exit;*/ ?>
                                    <tr>
                                        <td><?php echo $module_click_data[$i]['username'];?></td>
                                        <td><?php echo $module_click_data[$i]['total_hit']; ?></td>
                                        <td><a href="<?php echo base_url() ?>Leader_board/details/<?php echo $event_id; ?>/<?php echo $module_click_data[$i]['Id']; ?>" class="tooltips" data-placement="top" data-original-title="more ">More details</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization">
                        </div>
                        <div id="visualization7">
                        </div>
                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        
                        <script type="text/javascript">
                            //load package
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                 
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Firstname', 'Total Clicks'],
                                   <?php
                                   foreach ($click_track_chart as $value) 
                                   {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_hit'];
                                        echo "['{$name}', {$hit}],";
                                        
                                   }
                                   ?>
                                ]);


                var options = {title:"Click View Reports",'width':1000,'height':300};
 
                // Create and draw the visualization.
                  <?php if(!empty($click_track_chart)) { ?>
                new google.visualization.PieChart(document.getElementById('visualization')).
                draw(data, options);
                new google.visualization.LineChart(document.getElementById('visualization7')).
                draw(data, options);
                <?php } ?>
            }
 
            google.setOnLoadCallback(drawVisualization);
        </script>       
        </div>
                 
                <div class="tab-pane fade" id="view_bids">
                <?php if(count($view_bids) > 0){ ?>
                         <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/7" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export to CSV
                        </a><?php } ?>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover table-full-width" id="sample_7">  
                              <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total bid</th>
                                        <th>High bid amount</th>
                                        <th>Total bid amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($view_bids);$i++) { ?>
                                    <tr>
                                        <td><?php echo $view_bids[$i]['name']; ?></td>
                                        <td><?php echo $view_bids[$i]['total_bid']; ?></td>
                                        <td><i class="<?php echo 'fa '.$currency[1];?>"></i><?php echo $view_bids[$i]['total_win_amt']; ?></td>
                                        <td><i class="<?php echo 'fa '.$currency[1];?>"></i><?php echo $view_bids[$i]['total_bid_amt']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   <div class="tab-pane fade" id="message_track">
                   <?php if(count($msg_tracking) > 0){ ?>
                        <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/5" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                                Export to CSV
                        </a><?php } ?>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">  
                              <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Messages</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($msg_tracking);$i++) { ?>
                                    <tr>
                                        <td><?php echo $msg_tracking[$i]['username']; ?></td>
                                        <td><?php echo $msg_tracking[$i]['total_hit']; ?></td>
                                        <td><a href="<?php echo  base_url().'Leader_board/exportmsg/'.$event_id.'/'.$msg_tracking[$i]['Id']; ?>">Download Full Details(csv)</a> | <a href="<?php echo  base_url().'Leader_board/exportxlsxmsg/'.$event_id.'/'.$msg_tracking[$i]['Id']; ?>">Download Full Details(xls)</a></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization1">
                       
                        </div>
                        <div id="visualization8">
                       
                        </div>

                         <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        
                        <script type="text/javascript">
                            //load package
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                 
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Name', 'Total Messages'],
                                    <?php
                                   foreach ($message_track_chart as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_hit'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                 var options = {title:"Messages Hits Reports",'width':1000,'height':300,'float':'left'};
                 
                                // Create and draw the visualization.
                                  <?php if(!empty($message_track_chart)) { ?>
                                new google.visualization.PieChart(document.getElementById('visualization1')).
                                draw(data, options);
                                 new google.visualization.LineChart(document.getElementById('visualization8')).
                                draw(data, options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>


                    </div>
                    <div class="tab-pane fade" id="metting_track">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_9">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Exibitor Name</th>
                                        <th>Attendee Name</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach ($metting_data as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo ucfirst($value['Heading']); ?></td>
                                            <td><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></td>
                                            <td><?php echo $value['date']; ?></td>
                                            <td><?php echo $value['time']; ?></td>
                                            <td>
                                            <?php if($value['status']=='0'){ ?>
                                                <span class="label label-sm label-info">
                                                Pending
                                                </span>
                                            <?php }elseif($value['status']=='1'){ ?>
                                                <span class="label label-sm label-success">
                                                Confirmed
                                                </span>
                                            <?php }else{ ?>
                                                <span class="label label-sm label-danger">
                                                Reject
                                                </span>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="adverts_users">
                        <?php if(count($advert_click_data) > 0){ ?>
                        <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/2" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                                Export to CSV
                        </a><?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">  
                             <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Clicks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($advert_click_data);$i++) { /*echo '<pre>'; print_r($speakers); exit;*/ ?>
                                    <tr>
                                        <td><?php echo $advert_click_data[$i]['username']; ?></td>
                                        <td><?php echo $advert_click_data[$i]['total_hit']; ?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization4">
                        </div>

                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            
                        <script type="text/javascript">
                            //load package
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                     
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                
                                var data = google.visualization.arrayToDataTable([
                                    ['Firstname', 'Adver. Hits'],
                                    <?php
                                   foreach ($advert_click_data as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_hit'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                var options = {title:"Adverts Hits Reports",'width':1000,'height':300};
                                            
                 
                                // Create and draw the visualization.
                                  <?php if(!empty($advert_click_data)) { ?>
                                new google.visualization.LineChart(document.getElementById('visualization4')).

                                draw(data, options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>   

                    </div>

                    <div class="tab-pane fade" id="surveys_result">
                        <?php if(count($user_survey_ans) > 0){ ?>
                         <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/3" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export to CSV
                         </a><?php } ?>
                         <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_5">  
                             <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Answers</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($user_survey_ans);$i++) { ?>
                                    <tr>
                                        <td><?php echo $user_survey_ans[$i]['username']; ?></td>
                                        <td><?php echo $user_survey_ans[$i]['total_ans']; ?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization5">
                        </div>

                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            
                        <script type="text/javascript">
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                     
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Firstname', 'Answers'],
                                    <?php
                                   foreach ($user_survey_ans as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_ans'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                var options = {title:"Surveys Results Reports",'width':1000,'height':300};
                 
                                // Create and draw the visualization.
                                <?php if(!empty($user_survey_ans)) { ?>
                                new google.visualization.LineChart(document.getElementById('visualization5')).
                                draw(data, options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>
                    </div>

                    <div class="tab-pane fade" id="notes_track">
                    <?php if(count($user_notes) > 0){ ?>
                         <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/4" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export to CSV
                         </a><?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_6">  
                             <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($user_notes);$i++) { ?>
                                    <tr>
                                        <td><?php echo $user_notes[$i]['username']; ?></td>
                                        <td><?php echo $user_notes[$i]['total_notes']; ?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization6" >
                        </div>

                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            
                        <script type="text/javascript">
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                     
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Firstname', 'Notes'],
                                    <?php
                                   foreach ($user_notes as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_notes'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                var options = {title:"Notes Reports",'width':1000,'height':300};
                 
                                // Create and draw the visualization.
                                  <?php if(!empty($user_notes)) { ?>
                                new google.visualization.LineChart(document.getElementById('visualization6')).
                                draw(data, options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>
                        
                    </div>

                    <div class="tab-pane fade" id="message_track">
                    <?php if(count($msg_tracking) > 0){ ?>
                        <a href="<?php echo base_url().$event_detail['Subdomain']; ?>/panel/export_user_data" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export to CSV
                        </a><?php } ?>
                        <div class="table-responsive">
                             <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">  
                              <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Messages</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($msg_tracking);$i++) { ?>
                                    <tr>
                                        <td><?php echo $msg_tracking[$i]['Firstname']." ".$msg_tracking[$i]['Lastname']; ?></td>
                                        <td><?php echo $msg_tracking[$i]['total_hit']; ?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <div id="visualization1">
                       
                        </div>
                        <div id="visualization8">
                       
                        </div>

                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        
                        <script type="text/javascript">
                            //load package
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                 
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Name', 'Total Messages'],
                                    <?php
                                   foreach ($message_track_chart as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_hit'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                 var options = {title:"Messages Hits Reports",'width':1000,'height':300,'float':'left'};
                 
                                // Create and draw the visualization.
                                  <?php if(!empty($message_track_chart)) { ?>
                                new google.visualization.PieChart(document.getElementById('visualization1')).
                                draw(data, options);
                                 new google.visualization.LineChart(document.getElementById('visualization8')).
                                draw(data, options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>

                    </div>
                    <div class="tab-pane fade" id="agenda_users">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_8">
                                <thead>
                                    <tr>
                                        <th>Session Name</th>
                                        <th>Date/Time</th>
                                        <th>Agenda Rating</th>
                                        <th>Number of Users Who Have Saved</th>
                                        <th>Number of Users Who Have Checked In</th>
                                        <th>Number of Users Who Have Comment</th>
                                        <th>Drill Down</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($agenda as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo  $value['Heading'] ?></td>
                                        <td><?php echo date("H:i - d/m/Y",strtotime($value['End_time'].' '.$value['End_date'])); ?></td>
                                        <td><?php 
                                        $rating=$value['total_rating']/$value['no_user'];
                                        for($i=1;$i<=5;$i++){
                                            if($i<=$rating)
                                            {
                                                $img=base_url().'assets/images/star-on.png';
                                            }
                                            else
                                            {
                                                $img=base_url().'assets/images/star-off.png';
                                            }
                                            ?>
                                            <img src="<?php echo $img; ?>" alert="start img"/>
                                            <?php 
                                        }
                                         ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($value['total_save']) && !empty($value['Maximum_People'])){
                                            echo $value['total_save']."/".$value['Maximum_People']."=".number_format(($value['total_save']/$value['Maximum_People'])*100,2)."%";
                                        } ?></td>
                                        <td><?php if(!empty($value['total_check']) && !empty($value['total_save'])){ 
                                            echo $value['total_check']."/".$value['total_save']."=".number_format(($value['total_check']/$value['total_save'])*100,2)."%";
                                        }?></td>
                                        <td>
                                            <a href="<?php echo base_url().'Leader_board/agenda_comment_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo $value['total_comment']; ?></a>
                                        </td>
                                        <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>">More Details</a></td>
                                    </tr>
                                   <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="comments_tracking">
                        <?php if(count($comments_tracking) > 0){ ?>
                        <a href="<?php echo base_url(); ?>Leader_board/export/<?php echo $event_id; ?>/6" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                                Export to CSV
                        </a><?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">  
                             <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Total Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($comments_tracking);$i++) { /*echo '<pre>'; print_r($speakers); exit;*/ ?>
                                    <tr>
                                        <td><?php echo $comments_tracking[$i]['username']; ?></td>
                                        <td><?php echo $comments_tracking[$i]['total_hit']; ?></td>
                                      
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="visualization2" style="">
                        </div>
                         <div id="visualization9" style="">
                        </div>

                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                        
                        <script type="text/javascript">
                            //load package
                            google.load('visualization', '1', {packages: ['corechart']});
                        </script>
                 
                        <script type="text/javascript">
                            function drawVisualization() {
                                // Create and populate the data table.
                                var data = google.visualization.arrayToDataTable([
                                    ['Firstname', 'Total Comments'],
                                    <?php
                                   foreach ($comment_track_chart as $value) {
                                        $name=$value['Firstname']." ".$value['Lastname'];
                                        $hit=$value['total_hit'];
                                      echo "['{$name}', {$hit}],";
                                        //echo $name.",".$hit;
                                   }
                                    ?>
                                ]);

                                var options = {title:"Comments Hits Reports",'width':1000,'height':300};
                 
                                // Create and draw the visualization.
                                 <?php if(!empty($comment_track_chart)) { ?>
                                new google.visualization.PieChart(document.getElementById('visualization2')).
                                draw(data, options);
                                 new google.visualization.LineChart(document.getElementById('visualization9')).
                                draw(data,options);
                                <?php } ?>
                            }
                 
                            google.setOnLoadCallback(drawVisualization);
                        </script>

                    </div>

                    <!--<div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                           <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail"></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div class="user-edit-image-buttons">
                                                     <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                          <input type="file" name="Images[]">
                                                     </span>
                                                     <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                          <i class="fa fa-times"></i> Remove
                                                     </a>
                                                </div>
                                           </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>-->

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   $('.dropdown-menu > li > a').click(function(){
    $('#textshow').text($(this).text());
   });
  });
</script>