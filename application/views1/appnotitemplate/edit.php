<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			
            <!--<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Template</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>-->
                    
			<div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('appnoti_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('appnoti_data'); ?> Successfully.
                </div>
                <?php } ?>

                 <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#edit_template" data-toggle="tab">
                                Edit Template
                            </a>
                        </li>

                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="edit_template">
                        <form role="form" method="post" class="form-horizontal" id="mine-form" action="<?php echo base_url();?>Appnotitemplate/edit/<?php echo $arrTemplate[0]['event_id'];?>/<?php echo $arrTemplate[0]['Id'];?>" enctype="multipart/form-data">
                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                                Subject: <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="Subject" value="<?php echo $arrTemplate[0]['Subject'];?>">
                                        </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                            From: <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="From" value="<?php echo $arrTemplate[0]['From'];?>">
                                    </div>
                                </div>
                                    
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                            Content: <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea name="Content" class="ckeditor form-control" cols="10" rows="10"><?php echo $arrTemplate[0]['Content'];?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-4">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>


                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->