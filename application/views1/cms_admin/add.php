<?php $acc_name=$this->session->userdata('acc_name');
$eventname=$event['Subdomain'];?>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.summernotecmsimages').summernote({
    height: 300,                 
    minHeight: null,       
    maxHeight: null,
    onImageUpload: function(files, editor, welEditable) {
      sendFile(files[0],editor,welEditable);
    }
  });
});
function sendFile(file,editor,welEditable) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
      data: data,
      type: "POST",
      url: "<?php echo base_url().'Cms_admin/saveeditorfiles/'.$this->uri->segment(3); ?>",
      cache: false,
      contentType: false,
      processData: false,
      success: function(url) {
        editor.insertImage(welEditable, url);
      }
  });
}
</script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">  
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#cms_list" data-toggle="tab">
                            Add Custom Homescreen
                        </a>
                    </li>
                    <li class="">
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View Events
                        </a>
                    </li>
                </ul>
            </div>
           <!--  <div class="panel-heading">
                <h4 class="panel-title">
                    Add 
                    <span class="text-bold">
                        New Custom Homescreen
                    </span>
                </h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div> -->
            <div class="tab-content">
            <div class="tab-pane fade active in" id="cms_list">
            <div class="panel-body">

                <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div id="wizard" class="swMain">
                        <ul  style="display:none;margin-top:15px;">
                            <li>
                                <a href="#step-1">
                                    <div class="stepNumber">
                                        1
                                    </div>
                                    <span class="stepDesc"> 
                                    <small>step-1</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <div class="stepNumber">
                                        2
                                    </div>
                                    <span class="stepDesc">
                                    <small>step-2</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <div class="stepNumber">
                                        3
                                    </div>
                                    <span class="stepDesc">
                                    <small>step-3</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <div class="stepNumber">
                                        4
                                    </div>
                                    <span class="stepDesc">
                                    <small>step-4</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <div class="stepNumber">
                                        5
                                    </div>
                                    <span class="stepDesc"> 
                                    <small>Finish</small></span>
                                </a>
                            </li>
                        </ul>
                        <div class="progress progress-xs transparent-black no-radius active">
                            <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                            </div>
                        </div>
                        <div id="step-1">
                            <h3>Give your custom screen a name</h3>
                            <p>This will show in the left hand menu in your app. if you<br/> change your mind you can change the name later.</p>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <input type="text" value="" placeholder="Type your custom screen name here" id="Event_name" name="Menu_name" class="form-control required name_group">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Next Step >>>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="step-2">
                            <h3>Enter your custom screen's body content</h3>
                            <p>Here you can use the editor box below to add formatted text images,videos and links.The formatting tool allows you to place areas in columns and rows,similar to a webpage editor.if you have HTML code you can paste it into the code view of the editor box.</p>
                            <div class="form-group">
                                <div class="col-sm-9">
                                    <textarea id="Description" name="Description" class="summernotecmsimages"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <button class="btn btn-green back-step btn-block">
                                        <<< Back
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Next Step >>>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="step-3">
                            <h3>Add your screen's banner image and left hand menu logo</h3>
                            <p>On your screen you can add a banner image and a new logo which only appears on that page.</p>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <h3>
                                        Add Your custom screen banner image here
                                    </h3>
                                </div>
                                <div class="modal fade" id="headermodelscopetool" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Banner Image</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div>
                                                        <img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
                                                    </div>
                                                    <div class="col-sm-12 text-center">
                                                        <button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding-left: 0;">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div"> 
                                        </div>
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file">
                                                <span class="fileupload-new">
                                                    <i class="fa fa-picture"></i> 
                                                    Select image
                                                </span>
                                                <span class="fileupload-exists">
                                                    <i class="fa fa-picture"></i> 
                                                    Change
                                                </span>
                                                <input type="file" id="header_image_sponder" name="images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                                <div class="col-sm-2">
                                    <h3>
                                        Add Your custom screen logo image here
                                    </h3>
                                </div>
                                <div class="modal fade" id="logomodelscopetool" role="dialog">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close close_logo_popup" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Logo</h4>
                                            </div>
                                            <div class="modal-body" id="show_logoupload_div_data">
                                                <div class="row">
                                                    <div id="preview_logo_crop_tool">
                                                        <img class="img-responsive" id="show_crop_img_model" src="" alt="Picture">
                                                    </div>
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-green btn-block" style="width: 120px;margin: 0 auto;" type="button" id="upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default close_logo_popup" data-dismiss="modal">Exit crop tool</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding-left: 0;">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" id="company_logo_preview_div"> 
                                        </div>
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file">
                                                <span class="fileupload-new">
                                                    <i class="fa fa-picture"></i> 
                                                    Select image
                                                </span>
                                                <span class="fileupload-exists">
                                                    <i class="fa fa-picture"></i> Change
                                                </span>
                                                <input type="file" id="logo_image" name="logo_images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" id="remove_logo_image_data"  data-dismiss="fileupload">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                        <input type="hidden" name="company_logo_crop_data_textbox" id="company_logo_crop_data_textbox">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <button class="btn btn-green back-step btn-block">
                                        <<< Back
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Next Step >>>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="step-4">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <h4>Select the features you would like to show in the footer of your custom screen</h4>
                                    <p>if you have created home tabs in some of your other features you can include those home tabs with links to those features at the bottom of your custom screen.</p>
                                    <p>Choose the features you would like to show in the box below.</p>
                                    <div class="form-group">
                                        <div class="col-sm-10">
                                            <select id="feature_menu" class="select2-container select2-container-multi form-control search-select menu-section-select" multiple="multiple" name="feature_menu[]">
                                                <?php 
                                                $key = array_search('20', array_column($menu_toal_data, 'id'));
                                                if($key=="")
                                                {
                                                    $notshowmenu=array('20','21','26','27','28','22','23','24','25','42');
                                                }
                                                else
                                                {
                                                    $notshowmenu=array('20','21','26','27','28');
                                                }
                                                foreach ($menu_toal_data as $key=>$value)
                                                {
                                                    if(!in_array($value['id'],$notshowmenu)){
                                                    ?>
                                                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['menuname']; ?></option>
                                                   <?php }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if($event_id=='447'){ ?>
                                    <div class="form-group"> 
                                        <div class="col-sm-10">
                                          <select class="select2-container form-control search-select menu-section-select" id="exi_page_id" name="exi_page_id"> 
                                            <option value="">Select Exibitor</option>
                                            <?php foreach ($exibitor_page as $key => $value) { ?>
                                            <option value="<?php echo $value['Id']; ?>"><?php echo $value['Heading']; ?></option>
                                            <?php } ?>
                                          </select>    
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-4">
                                    <img alt="" src="<?php echo base_url(); ?>assets/images/image_Yd.png">
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 40px;">
                                <div class="col-sm-3">
                                    <button class="btn btn-green back-step btn-block">
                                        <<< Back
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <button id="cms_form_submit" class="btn btn-theme_green next-step btn-block">
                                        Finish
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="step-5">
                            <h3 class="StepTitle">Finish!</h3>
                            <div id="result_div"></div>
                            <p>You have successfully added your custom screen!</p>
                            <p>if you want to edit any part of your screen you can go back to the Custom Screens Module and open it.All your screens are shown in the table.</p>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <a id="go_back_to_the_main_url_btn" href="<?php echo base_url().'Cms_admin/index/'.$this->uri->segment(3); ?>" class="btn btn-theme_green btn-block" disabled="disabled">
                                        Go back to the main screen
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
            <?php $enm=$event['Subdomain']; ?>
            <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                    <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$enm; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$enm; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                    </div>
                </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $logoimage = $("#show_crop_img_model");
var $inputLogoImage = $('#logo_image'); 
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image_sponder'); 
$(document).ready(function(){
    $inputLogoImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              
              uploadedImageURL = URL.createObjectURL(file);
              $logoimage.attr('src', uploadedImageURL);
              $('#logomodelscopetool').modal('show');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#logomodelscopetool', function () {
        $logoimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_crop');
            $logoimage.cropper({
                aspectRatio: 1,
                built: function () {
                  croppable = true;
                }
            });
            $button.on('click', function () {
                var croppedCanvas;
                if (!croppable) {
                  return;
                }
                croppedCanvas = $logoimage.cropper('getCroppedCanvas');
                $('#company_logo_preview_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
                $('#company_logo_crop_data_textbox').val(croppedCanvas.toDataURL());
                $("#logo_image").val('');
            });
        }).on('hidden.bs.modal','#logomodelscopetool',function(){
             $logoimage.cropper('destroy');
    });
    $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              uploadedImageURL = URL.createObjectURL(file);
              $bannerimage.attr('src', uploadedImageURL);
              $('#company_banner_crop_data_textbox').val('');
              $('#headermodelscopetool').modal('toggle');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
            $('#banner_preview_image_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
            $('#company_banner_crop_data_textbox').val(croppedCanvas.toDataURL());
            $("#header_image_sponder").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    }); 
});
</script>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$('#remove_logo_image_data').click(function(){
  $('#company_logo_crop_data_textbox').val('');
});
$('#remove_company_banner_data').click(function(){
    $('#company_banner_crop_data_textbox').val('');
});
$('#cms_form_submit').click(function(){
    $('#go_back_to_the_main_url_btn').html("Submitting <i class='fa fa-spinner fa-spin'></i>");
    var formData = new FormData($('form')[0]);
    /*formData.append('Description',CKEDITOR.instances['Description'].getData());*/
    formData.append('Description',$('#Description').code());
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Cms_admin/add/'.$this->uri->segment(3); ?>",
        data: formData,
        processData: false,
        contentType: false,
        error:function(error){
            $('#result_div').attr('class','alert alert-danger');
            $('#result_div').html('Somthing Went Worng..');
            $('#result_div').show();
            $('#go_back_to_the_main_url_btn').html("Go back to the main screen");
            $('#go_back_to_the_main_url_btn').removeAttr('disabled');
        },
        success: function(result){
            var data=result.split('###');
            if(data[0]=="success")
            {
                $('#result_div').attr('class','alert alert-success');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            else
            {
                $('#result_div').attr('class','alert alert-danger');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            $('#go_back_to_the_main_url_btn').html("Go back to the main screen");
            $('#go_back_to_the_main_url_btn').removeAttr('disabled');
        }
    });
});
</script>