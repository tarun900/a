<?php $acc_name=$this->session->userdata('acc_name');
$cmsdata_edit=$cms_data[0];
?>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
</script>

<div class="row">
  <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php if ($this->session->flashdata('cms_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Custom Homescreen <?php echo $this->session->flashdata('agenda_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#cms_list" data-toggle="tab">
                                Edit Custom Homescreen
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Events
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="cms_list">
                      <div class="panel-body">

                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Homescreen Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-4" style="padding-right:0px;">
                                    <input type="text" value="<?php echo $cmsdata_edit['Menu_name']; ?>" placeholder="Homescreen Name" id="Event_name" name="Menu_name" class="form-control required name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Status<span class="symbol required"></span></label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" <?php if($cmsdata_edit['Status']=="1"){ ?> checked="checked" <?php } ?> name="Status">
                                        Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" <?php if($cmsdata_edit['Status']=="0"){ ?> checked="checked" <?php } ?> name="Status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Description </label>
                                <div class="col-sm-9">
                                    <textarea  id="Description" name="Description" class="summernote"><?php echo $cmsdata_edit['Description']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Banner Images</label>
                                <div class="col-sm-9">
                                    <?php $images_array = json_decode($cmsdata_edit['Images']); ?>
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div> 
                                    
                                     <?php
                                      for($i=0;$i<count($images_array);$i++)
                                      {
                                      ?>
                                          <div class="col-sm-3 fileupload-new thumbnail center">
                                              <img height="150" width="209" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                              <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                          </div>
                                      <?php
                                      }
                                    ?>                                                                                     
                                </div>
                            </div>

                            <div class="form-group">
                                <?php $Logo_array = json_decode($cmsdata_edit['Logo_images']); ?>
                                <label class="col-sm-2" for="form-field-1">Logo Image</label>
                                <div class="col-sm-9">
                                     <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="logo_images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div> 
                                      <?php
                                       for($i=0;$i<count($Logo_array);$i++)
                                       {
                                       ?>
                                           <div class="col-sm-3 fileupload-new thumbnail center">
                                               <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $Logo_array[$i]; ?>">
                                               <input type="hidden" name="old_images[]" value="<?php echo $Logo_array[$i]; ?>">
                                           </div>
                                       <?php
                                       }
                                     ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Banner image view</label>

                                <div class="col-sm-4">
                                    <select id="img_view" class="form-control" name="img_view">
                                            <option value="0" <?php if($cmsdata_edit['Img_view']=="0"){ ?> selected="selected" <?php } ?> >Square</option>
                                            <option value="1"  <?php if($cmsdata_edit['Img_view']=="1"){ ?> selected="selected" <?php } ?>>Round</option>
                                    </select>
                                </div>
                            </div>

                            <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Background Color 
                                </label>
                                <div class="col-sm-4" style="padding-right:0px;">
                                    <input type="text" value="<?php echo $cmsdata_edit['Background_color']; ?>" placeholder="Background color" id="Background_color" name="Background_color" class="color {hash:true} form-control name_group">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Select Feature 
                                </label>
                                <div class="col-sm-4" style="padding-right:0px;">
                                     <select id="feature_menu" class="form-control" multiple="multiple" name="feature_menu[]">
                                         <?php
                                         $menudata=  explode(',', $cmsdata_edit['fecture_module']);
                                         $cmsmenudata=  explode(',', $cmsdata_edit['cms_fecture_module']);
                                         
                                         foreach ($event_menu as $key=>$value)
                                         {
                                           ?>
                                         <option value="<?php echo $value->id; ?>" <?php if(in_array($value->id, $menudata)){ ?> selected="selected" <?php } ?> ><?php echo $value->menuname; ?></option>
                                           <?php
                                         }
                                         ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Select Custom Items 
                                </label>
                                <div class="col-sm-4" style="padding-right:0px;">
                                     <select id="cms_feature_menu" class="form-control" multiple="multiple" name="cms_feature_menu[]">
                                         <?php
                                         foreach ($cms_menu_list as $key=>$value)
                                         {
                                           ?>
                                         <option value="<?php echo $value->Id; ?>" <?php if(in_array($value->Id, $cmsmenudata)){ ?> selected="selected" <?php } ?> ><?php echo $value->Menu_name; ?></option>
                                           <?php
                                         }
                                         ?>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <input type="hidden" name="cms_id" id="cms_id" value="<?php echo $cms_id; ?>">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                            </div>
                            <label class="col-sm-2" for="form-field-1">
                                 Home Screen Image  
                             </label>
                            <div class="form-group">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail"></div>
                                   <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                   <div class="user-edit-image-buttons">
                                        <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                             <input type="file" name="Images[]">
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                             <i class="fa fa-times"></i> Remove
                                        </a>
                                   </div>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                     &nbsp;&nbsp;
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                    Create Home Tab
                                </label>
                                <?php 
                                if($img !='') { ?>
                                <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                <?php } ?>
                            </div>

                            <div class="form-group">
                                 
                                <label class="col-sm-2" for="form-field-1">Image view:</label>
                                <div class="col-sm-5">
                                    <select style="margin-top:-8px;" id="img_view_cms" class="form-control" name="img_view_cms">
                                            <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                            <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                    </select>
                                </div>
                            </div>
                        
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1"></label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Update <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                      </div>
                    </div>

                    <div class="tab-pane fade" id="view_events">
                    <div class="light intro">
                        <div class="row">
                            <section id="demo">
                                <figure id="devicePreview" class="phone">
                                    <iframe src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>" scrolling="auto" frameborder="0"></iframe>
                                    <a class="laptopLink" target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>">
                                        <img class="laptopImage hide" style="left: 145px;top: 46px;position: absolute;width: 67.3%;height: 383px;" src="<?php echo base_url(); ?>images/event_dummy.jpg">
                                    </a>
                                </figure>
                            </section>
                        </div>
                    </div>
                </div>
                
                </div>

            </div>
        </div>
    </div>
</div>
