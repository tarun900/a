<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php //if($user->Role_name=='Client'){ ?>
                    <a style="top:7px;right:359px" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Cms_admin/add_super_group/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add Super Group</a>
                    <a style="top:7px;right:248px" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Cms_admin/add_group/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add Group</a>
                    <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Cms_admin/add/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add New Custom Homescreen</a>
                <?php //} ?>

                <?php if ($this->session->flashdata('cms_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> CMS <?php echo $this->session->flashdata('cms_data'); ?> Successfully.
                    </div>
                <?php } ?>

              <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#s_group_list" data-toggle="tab">
                                Super Group List
                            </a>
                        </li>
                        <li>
                            <a href="#group_list" data-toggle="tab">
                                Group List
                            </a>
                        <li class="">
                            <a href="#cms_list" data-toggle="tab">
                                Custom Homescreens
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="s_group_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($super_group_list as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1; ?></td>
                                        <td><?=ucfirst($value['group_name']); ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Cms_admin/edit_super_group/<?php echo $this->uri->segment(3).'/'.$value['id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url().'Cms_admin/delete_super_group/'.$this->uri->segment(3).'/'.$value['id']; ?>" onclick="return confirm('Are You Sure Delete This Group');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="group_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($module_group_list as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1; ?></td>
                                        <td><?=ucfirst($value['group_name']); ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Cms_admin/edit_group/<?php echo $this->uri->segment(3).'/'.$value['module_group_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url().'Cms_admin/delete_group/'.$this->uri->segment(3).'/'.$value['module_group_id']; ?>" onclick="return confirm('Are You Sure Delete This Group');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="cms_list">
                      <div class="col-sm-12">
                          <h3 class="StepTitle">Custom Homescreens</h3>
                          <p>Create a custom screen in Your app where  you can add your own text,images and video on a competely open and free page. it works like a blank web page and is perfect for large amounts of text and sponsor pages.</p>
                          <p>
                            <a href="<?php echo base_url(); ?>Cms_admin/add/<?php echo $event_id; ?>">Click here to Create a new custom screen.</a>
                          </p>
                          <hr style="height: 5px; background-color: gray;">
                          <h3 class="StepTitle">Your Custom Homescreens</h3>
                        </div>
                      <div class="panel-body">

                        
                        <div id="cms_page_list" class="table-responsive">
                             <table class="table table-striped table-bordered table-hover table-full-width dataTable" id="sample_1">
                                  <thead>
                                       <tr>
                                          <th>#</th>
                                          <th>Menu Name</th>
                                          <th>Status</th>
                                           <?php if($is_hub_created=='1' && $event_in_hub=='1'){ ?>
                                          <th>Show In Hub</th>
                                          <?php } ?>
                                          <th>Show In App</th>
                                          <th>Action</th>                            
                                       </tr>
                                  </thead>
                                  <tbody>
                                  <?php for ($i = 0; $i < count($cms_list); $i++) { ?>
                                    <tr>
                                      <td><?php echo $i + 1; ?></td>
                                      <td><a href="<?php echo base_url() ?>Cms_admin/edit/<?php echo $event_id.'/'.$cms_list[$i]['Id']; ?>"><?php echo ucfirst($cms_list[$i]['Menu_name']); ?></a></td>
                                      <td>
                                        <span class="label label-sm 
                                          <?php if ($cms_list[$i]['Status'] == '1')
                                            { ?> 
                                               label-success 
                                               <?php }
                                               else
                                               { ?> label-danger 
                                               <?php } ?>">
                                               <?php
                                               if ($cms_list[$i]['Status'] == '1')
                                               {
                                                    echo "Active";
                                               }
                                               else
                                               {
                                                    echo "Inactive";
                                               }
                                          ?>
                                          </span>
                                         </td>
                                          <?php if($is_hub_created=='1' && $event_in_hub=='1'){ ?>
                                          <td>
                                            <input type="checkbox" <?php if($cms_list[$i]['show_in_hub']=='1'){ ?> checked="checked" <?php } ?> onchange='cmsaddinhub("<?php echo $cms_list[$i]['Id']; ?>");' name="show_in_hub" value="<?php echo $cms_list[$i]['Id']; ?>" id="show_in_hub_<?php echo $cms_list[$i]['Id'];?>" class="">
                                          <label for="show_in_hub_<?php echo $cms_list[$i]['Id'];?>"></label>
                                          </td>
                                          <?php } ?>
                                          <td>
                                            <input type="checkbox" <?php if($cms_list[$i]['show_in_app']=='1'){ ?> checked="checked" <?php } ?> onchange='cmsaddinapp("<?php echo $cms_list[$i]['Id']; ?>");' name="show_in_app" value="<?php echo $cms_list[$i]['Id']; ?>" id="show_in_app_<?php echo $cms_list[$i]['Id'];?>" class="">
                                          <label for="show_in_app_<?php echo $cms_list[$i]['Id'];?>"></label>
                                          </td>
                                         <td>
                                            <a href="<?php echo base_url() ?>Cms_admin/edit/<?php echo $event_id.'/'.$cms_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_cms('<?php echo $cms_list[$i]['Id']; ?>','<?php echo $event_id; ?>')" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                         </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                             </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                    <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                    </div>
                </div>

              </div>

              </div>
          </div>
      </div>
</div>
                    
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script>
     function delete_cms(id,event_id)
     {
          if (confirm("Are you sure to delete this?"))
          {
               window.location.href = "<?php echo base_url(); ?>Cms_admin/delete/" + id+"/"+event_id;
          }
     }
     function cmsaddinhub(cid)
    {
      $.ajax({
        url:"<?php echo base_url(); ?>Cms_admin/cms_page_add_in_hub/<?php echo $event_id; ?>/"+cid,
        success:function(result)
        {
          if($.trim(result)>0)
          {
            var shortCutFunction ='success';
            if($("#show_in_hub_"+cid).is(':checked'))
            {
              var title = 'Add Successfully';
              var msg = 'Cms Page Add in Hub Successfully';
            }
            else
            {
              var title = 'Remove Successfully';
              var msg = 'Cms Page Remove in Hub Successfully'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
          }
        }
      });
    }
function cmsaddinapp(cid)
{
  $.ajax({
    url:"<?php echo base_url(); ?>Cms_admin/cms_page_show_in_app/<?php echo $event_id; ?>/"+cid,
    success:function(result)
    {
      if($.trim(result)>0)
      {
        var shortCutFunction ='success';
        if($("#show_in_app_"+cid).is(':checked'))
        {
          var title = 'Show Successfully';
          var msg = 'Cms Page Show in App Successfully';
        }
        else
        {
          var title = 'Hide Successfully';
          var msg = 'Cms Page Hide in App Successfully'; 
        }
        var $showDuration = 1000;
        var $hideDuration = 3000;
        var $timeOut = 10000;
        var $extendedTimeOut = 5000;
        var $showEasing = 'swing';
        var $hideEasing = 'linear';
        var $showMethod = 'fadeIn';
        var $hideMethod = 'fadeOut';
        toastr.options = {
          closeButton: true,
          debug: false,
          positionClass:'toast-top-right',
          onclick: null
        };
        toastr.options.showDuration = $showDuration;
        toastr.options.hideDuration = $hideDuration;
        toastr.options.timeOut = $timeOut;                        
        toastr.options.extendedTimeOut = $extendedTimeOut;
        toastr.options.showEasing = $showEasing;
        toastr.options.hideEasing = $hideEasing;
        toastr.options.showMethod = $showMethod;
        toastr.options.hideMethod = $hideMethod;
        toastr[shortCutFunction](msg, title);
      }
    }
  });
}
</script>
<!-- end: PAGE CONTENT-->