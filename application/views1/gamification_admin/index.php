<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
                <?php if ($this->session->flashdata('gamification_data')) { ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('gamification_data'); ?> 
                </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#gamification_settings" data-toggle="tab">
                                Gamification Settings
                            </a>
                        </li>
                        <li class="">
                            <a href="#app_settings" data-toggle="tab">
                                App Settings
                            </a>
                        </li>
                        <li class="">
                            <a href="#header_content" data-toggle="tab">
                                Header Content
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                               Menu Name and Icon
                            </a>
                        </li>
                        <?php }?>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="gamification_settings">
                        <div class="col-md-12">
                            <h2>Fill out the fields below with the number of points you would like to award to each user for each of these actions.</h2>
                            <div class="col-md-9">
                                <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Gamification_admin/save_gamification_setting/<?php echo $event_id ?>">
                                    <?php 
                                    $rankids=array_column($rank_point,'rank_id');
                                    $points=array_column($rank_point,'point');
                                    $point=array_combine($rankids,$points);
                                    foreach ($rank as $key => $value) { ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="margin-bottom: 10px;">
                                            <?php echo ucfirst($value['rank']); ?>
                                        </label>
                                        <div class="col-sm-11" style="margin-bottom: 10px;">
                                            <input type="text" name="<?php echo $value['id']; ?>" value="<?= $point[$value['id']]?>" class="form-control" onkeypress="return isNumber(event)">
                                        </div>    
                                    </div>
                                    <?php } ?>
                                    <div class="form-group col-sm-2" style="margin-top: 10px;">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                    <p></p><p></p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="app_settings">
                        <div class="col-md-12">
                            <h2>Set Rank Colors</h2>
                            <div class="col-md-9">
                                <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Gamification_admin/save_rank_color/<?php echo $event_id ?>">
                                    <?php 
                                    $colors=array_column($rank_color,'color');
                                    for($i=1;$i<=5;$i++){ ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-1" style="margin-bottom: 10px;">
                                        <?php echo $i; ?>
                                        </label>
                                        <div class="col-sm-11" style="margin-bottom: 10px;">
                                            <input type="text" name="<?php echo $i; ?>" value="<?php echo $colors[$i-1] ?>" class="color {hash:true} form-control name_group">
                                        </div>    
                                    </div>
                                    <?php } ?>
                                    <div class="form-group col-sm-12" style="margin-top: 10px;">
                                        <label class="control-label col-sm-1">
                                        </label>
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                    <p></p><p></p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="header_content">
                        <div class="col-md-12">
                            <h2>Header Content</h2>
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Gamification_admin/save_header/<?php echo $event_id ?>">
                                <div class="form-group">
                                    <textarea name="header_content" class="form-control summernote"><?= $game_header ?>	</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                                <p></p><p></p>
                            </form>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index/<?php echo $event_id ?>" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                       <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }
</script>