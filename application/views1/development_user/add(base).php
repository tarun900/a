<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">user</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                            <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate" >
						<div class="row">
                                <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
                                <div class="errorHandler alert alert-success no-display" style="display: block;">
                                    <i class="fa fa-remove-sign"></i> Updated Successfully.
                                </div>
                                <?php } ?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										First Name <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="First Name" class="form-control" id="Firstname" name="Firstname">
                                                                    <input type="hidden" placeholder="idval" class="form-control" id="idval" name="idval" value="">
								</div>
                                                                <div class="form-group">
									<label class="control-label">
										Company Name <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Company Name" class="form-control" id="Company_name" name="Company_name">
								</div>
								<div class="form-group">
									<label class="control-label">
										Email <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="Email" class="form-control" id="email" name="email" onblur="checkemail();">
								</div>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                            <label class="control-label">
                                                                                    Password <span class="symbol required"></span>
                                                                            </label>
                                                                            <input type="password" class="form-control" id="password" name="password">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                            <label class="control-label">
                                                                                    Confirm Password <span class="symbol required"></span>
                                                                            </label>
                                                                            <input type="password" class="form-control" id="password_again" name="password_again">
                                                                    </div>
                                                                </div>
                                                                <?php 
                                                                    if($user->Role_name=='Administrator')
                                                                    { 
                                                                ?>
                                                                <div class="form-group">
									<label for="form-field-select-1">
                                                                                Status
                                                                        </label>
                                                                        <select id="form-field-select-1" class="form-control" name="Active">
                                                                                <option value="1">Active</option>                                                                                
                                                                                <option value="0">Inactive</option>
                                                                        </select>                                                                
								</div>
                                                                <?php 
                                                                    }
                                                                ?>
                                                                <div class="row">
                                                                        <div class="col-md-4">
                                                                            <button class="btn btn-green btn-block" type="submit">
                                                                                        Add <i class="fa fa-arrow-circle-right"></i>
                                                                                </button>
                                                                        </div>
                                                                </div>
							</div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                               <div class="col-md-6">
                                                                <div class="form-group">
									<label class="control-label">
										Phone Business <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Phone Business" class="form-control" id="Phone_business" name="Phone_business">
								</div>
                                                                </div><div class="col-md-6">
                                                                <div class="form-group">
									<label class="control-label">
										Mobile No <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Mobile No" class="form-control" id="Mobile" name="Mobile">
								</div>
                                                                </div>
                                                              </div>   
                                                            
                                                                <div class="row">
                                                                        <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                        <label class="control-label">
                                                                                                Street <span class="symbol required"></span>
                                                                                        </label>
                                                                                        <input type="text" placeholder="Street" class="form-control" id="Street" name="Street">
                                                                                </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                        <label class="control-label">
                                                                                                Suburb <span class="symbol required"></span>
                                                                                        </label>
                                                                                        <input type="text" placeholder="Suburb" class="form-control" id="Suburb" name="Suburb">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                            <div class="row">
                                                                        <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                        <label class="control-label">
                                                                                                Post Code <span class="symbol required"></span>
                                                                                        </label>
                                                                                    <input type="text" placeholder="Post Code" id="zipcode" name="zipcode" class="form-control">
                                                                                </div>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                                <div class="form-group">
                                                                                        <label class="control-label">
                                                                                                State <span class="symbol required"></span>
                                                                                        </label>
                                                                                        <select id="state" class="form-control" name="state">
                                                                                                <option value="">Select...</option>
                                                                                                <?php
                                                                                                    foreach($Statelist as $key=>$value)
                                                                                                    { 
                                                                                                    ?>
                                                                                                        <option value="<?php echo $value->id; ?>"><?php echo $value->state_name; ?></option>
                                                                                                    <?php
                                                                                                    }
                                                                                                ?>
                                                                                                
                                                                                        </select>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
									<label>
										Image Upload
									</label>
                                                                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail">
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail"></div>
										<div class="user-edit-image-buttons">
											<span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                                                            <input type="file" name="userfile">
											</span>
											<a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
												<i class="fa fa-times"></i> Remove
											</a>
										</div>
									</div>
								</div>
							</div>							
						</div>
					</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->