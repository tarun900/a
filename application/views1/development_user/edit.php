<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">user</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">

                            <?php if(!empty($this->session->flashdata('error'))){ ?>
			                <div class="errorHandler alert alert-danger no-display" style="display: block;">
			                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
			                </div>
			                <?php } ?>

                            <form action="<?php echo base_url();?>Developmentusers/edit/<?php echo $event_id;?>/<?php echo $user_data[0]['id'];?>" method="POST"  role="form" id="myform" enctype="multipart/form-data" novalidate="novalidate" >
						<div class="row">
                                
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										First Name <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="fname" value="<?php echo $user_data[0]['fname'];?>">
                                                                   
								</div>
                               <div class="form-group">
									<label class="control-label">
										Last Name <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="last Name" class="form-control required" id="Company_name" name="lname"  value="<?php echo $user_data[0]['lname'];?>">
								</div>
								<div class="form-group">
									<label class="control-label">
										Email <span class="symbol required"></span>
									</label>
                                                                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();" value="<?php echo $user_data[0]['email'];?>">
								</div>
                                                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="btn btn-green  submit_button" type="submit">
                                                        Update <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                             <a href="<?php echo base_url();?>Developmentusers/index/<?php echo $event_id;?>" class="btn btn-green">
                                               Back <i class="fa fa-arrow-circle-left"></i>
                                            </a>
                                        </div>
                                    </div>                            
							</div>
                                   						
						</div>
					</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

 $(".submit_button").click(function(){
        $( "#myform" ).validate();
    });
});
</script>


