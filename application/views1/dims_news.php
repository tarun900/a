<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>DIMS NEWS</title>
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900italic,900,700italic,700,600italic,600,400italic,300italic,300,200italic,200' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="img/favicon.png">
</head>

<body>
<section class="quickinfo-box">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="thumbnail patients">
                    <div class="thumb-box">
                      <h3>Patients</h3>
                      <img src="img/img1.jpg" alt="image">
                      <div class="caption">
                        
                        <p>If you are suffering from urinary incontinence, bladder prolapse or pelvic floor problems, our highly experienced team of urogynaecologists and nurses can help.</p>
                        <a href="#" class="btn">find out more</a>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="thumbnail doctors">
                    <div class="thumb-box">
                      <h3>Doctors</h3>
                      <img src="img/img2.jpg" alt="image">
                      <div class="caption">
                        
                        <p>We provide a wide range of specialist diagnostic services and medical management recommendations - to help you give your patients the best possible care.</p>
                        <a href="#" class="btn">find out more</a>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="thumbnail specialists">
                    <div class="thumb-box">
                        <h3>Specialists</h3>
                        <img src="img/img3.jpg" alt="image">
                        <div class="caption">
                            
                            <p>As leading sub-specialist urogynaecologists we are involved in training, research and publishing a range of educational material for patients and doctors.</p>
                            <a href="#" class="btn">find out more</a>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="thumbnail specialists">
                    <div class="thumb-box">
                        <h3>Specialists</h3>
                        <img src="img/img3.jpg" alt="image">
                        <div class="caption">
                            
                            <p>As leading sub-specialist urogynaecologists we are involved in training, research and publishing a range of educational material for patients and doctors.</p>
                            <a href="#" class="btn">find out more</a>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   
});
</script>
</body>
</html>
