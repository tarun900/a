<?php
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
if(empty($user))
{
   $url=base_url().'Activity/'.$acc_name.'/'.$Subdomain;
}
?>
<div class="agenda_content agenda-user-content">
     <div class="row row-box">
          <form id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/'; ?>upload_imag' style="clear:both">
               <div class="facebok-container">
                    <div class="facebok-main">
                         <div class="facebok-head">
                              <ul>
                                   <li class="facebok-head-status">
                                        <span>Message</span>
                                   </li>
                                   <li class="facebok-head-photo"> 
                                        <span> Add Photo </span>
                                        <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/></div>
                                        <div id='imageloadbutton'>
                                             <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple />
                                        </div>
                                   </li>
                              </ul>
                         </div>
                         <div class="facebok-middle">
                              <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text" placeholder="Post an Update....." id="Message" name="Message"></textarea>
                         </div>
                         <div class="fb-upload-img-box">
                              <ul id='preview'>
                              </ul>
                              <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                         </div>
                         <div class="facebok-footer clearfix">
                              <ul class="footer-left">
                                   <li class="camera-icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                              </ul>
                              <ul class="footer-right">
                                   <li class="submit-button-box">
                                        <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                             Publish Post <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                   </li>
                              </ul>
                         </div>
                    </div>    
               </div>
          </form> 
          <div class="tabbable msg_section">
               <div class="tab-content clearfix box-effect clearfix">
                    <div class="tab-pane fade in active clearfix" id="myTab6_example2">
                         <div id="messages">
                              <?php foreach ($activity_data as $key => $value) { ?>
                              <div class="message_container">
                                   <div class="msg_main_body">
                                        <div class="message_img">
											<?php if($value['Logo'] !='') { ?>
												<img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
											 <?php } else { ?>
											 <?php $color = sprintf("#%06x",rand(0,16777215)); ?>
											<span style="text-align: center;width:50px;height:50px;border-radius:50%;padding-top: 12px;font-size: 20px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($value['Firstname'], 0, 1)).''.ucfirst(substr($value['Lastname'], 0, 1)); ?></span>
											 <?php } ?>
                                        </div>
                                        <div class="msg_fromname">
                                             <a href="#" class="tooltip_data"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></a>
                                        </div>
                                        <div class="msg_toname activity_msg">
                                             <?php echo ucfirst($value['activity']); ?>
                                        </div>
                                        <?php if($value['activity_no']=='4'){ ?>
                                        <div class="activity_session_rating">
                                             <?php for ($i=1;$i<=5;$i++) { 
                                                  if($i<=$value['rating'])
                                                  {
                                                       $img="star-on.png";
                                                  }
                                                  else
                                                  {
                                                       $img="star-off.png";
                                                  } ?>
                                                  <img src="<?php echo base_url().'assets/images/'.$img; ?>">
                                             <?php } ?>     
                                        </div>
                                        <?php } ?> 
                                   </div>
                                   <div class="msg_date"><?php echo $value['Time']; ?></div>
                                   <div class="msg_message">
                                        <?php if(!empty($value['Message'])){ ?>
                                        <p id="short_msg_show_<?php echo $key; ?>"><?php echo substr($value['Message'],0,100); ?></p>
                                        <div id="fully_msg_show_<?php echo $key; ?>" style="display:none;">
                                        <p><?php echo $value['Message']; ?></p>
                                        </div>
                                        <?php if(strlen($value['Message'])>100){ ?>
                                        <a href="javascript:void(0);" onclick='readmore("<?php echo $key; ?>");' id="readmore_readless_link_<?php echo $key; ?>">Read More</a>
                                        <?php } } ?>
                                   </div>
                                   <?php  if(count(json_decode($value['image'],TRUE)) > 0){ $img=json_decode($value['image'],TRUE); $img1=1; foreach ($img as $ikey => $ivalue) {  ?>
                                   <div class="msg_photo">
                                        <a class="colorbox_<?php echo $key; ?>" href="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                        <?php if($img1=='1'){ ?>
                                             View Photos
                                        <?php $img1++; } ?>
                                        </a>
                                   </div>     
                                   <?php } } ?>
                                   <div style='clear:both'>
                                        <div class='like blue_like' <?php if($value['is_like']!='1'){ ?> style="display:none;" <?php } ?> id="<?php echo 'dislike_'.$key;?>">
                                             <a href='#'  class="like_btn blue_btn" onclick="unlike('<?php echo $key; ?>','<?php echo $value['id'];?>','<?php echo $value['type'];?>')" ><i class='fa fa-thumbs-up'></i>Like</a>
                                        </div>
                                        <div class='like' <?php if($value['is_like']=='1'){ ?> style="display:none;" <?php } ?> id="<?php echo 'like_'.$key;?>">
                                             <a href='#' class='like_btn'  onclick="like('<?php echo $key; ?>','<?php echo $value['id'];?>','<?php echo $value['type']; ?>')" ><i class='fa fa-thumbs-up'></i>Like</a>
                                        </div>
                                   </div> 
                                   <div id="<?php echo $key; ?>">
                                        <span style='float:left;padding:10px 0 0 0;'><?php echo '<p id="comment_count_'.$key.'" style="float:left;"> '. count($value['comments']).'</p> comments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?>
                                        </span>
                                        <span  style='float:left;padding:10px 0 0 0;'>
                                        <p id="like_count_<?php echo $key; ?>" style='float:left;'><?php echo $value['like_count']=='' ? 0 : $value['like_count']; ?></p> &nbsp;people like this</span>
                                   </div>
                                   <div class="clear"></div>
                                   <div class='toggle_comment msg_toggle_comment'>
                                        <a href='javascript: void(0);' id="comment_btn_<?php echo $key; ?>" onclick="showcommentbox('<?php echo $key; ?>');" class='btn btn-green btn-block comment_btn'>Comment</a>
                                   </div>
                                   <div class='comment_panel clearfix' id='slidepanel<?php echo $key; ?>' style="display:none;">
                                        <form method='post' name='commentform<?php echo $key; ?>' id="commentform<?php echo $key; ?>" enctype='multipart/form-data' action="<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag'  ?>">
                                             <div class='comment_message_img'>
                                                  <?php if ($user[0]->Logo!="")
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                                  }
                                                  else
                                                  {
                                                       echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                  } ?>
                                             </div>
                                             <textarea class='user_comment_input' id='comment_text<?php echo $key; ?>' placeholder='Comment' name='comment'></textarea>
                                             <div class='photo_view_icon'>
                                             <?php echo "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $key . "\").click();' >&nbsp;</div>";
                                                  echo "<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $key . "' onchange='comment_photo(".$key.")' multiple></div>";
                                                  echo "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
                                                  echo "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
                                                  echo "<ul id='cpreview".$key."' class='cpreview clearfix'></ul>";
                                                  echo "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
                                                  echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(".$key.");' />";
                                             ?>
                                        </form>
                                        <div class='comment_data' id='comment_conten<?php echo $key; ?>'>
                                             <?php if (!empty($value['comments']))
                                             {
                                                  $i = 0;
                                                  $flag="false";
                                                  foreach ($value['comments'] as $ckey => $cval) 
                                                  { 
                                                       if ($i > 3)
                                                       {
                                                            $classadded = 'comment_msg_hide';
                                                       }
                                                       else
                                                       {
                                                            $classadded = '';
                                                       }   
                                                       echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                                       if ($cval['Logo'] != "")
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                       }
                                                       else
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                       }
                                                       echo "</div>
                                                       <div class='comment_wrapper'>        
                                                       <div class='comment_username'>
                                                            " . ucfirst($cval['name']) . "
                                                       </div>
                                                       <div class='comment_text'>
                                                            " . $cval['comment'] . "
                                                       </div>
                                                       <div class='comment_text' style='float:right;padding-left:13px;'>";
                                                       echo $cval['datetime'];
                                                       echo"</div></div>";
                                                       if ($cval['comment_image'] != "")
                                                       {
                                                            $image_comment = json_decode($cval['comment_image']);
                                                            echo "<div class='msg_photo photos_feed'>";
                                                            foreach ($image_comment as $ikey => $ivalue) {
                                                            echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
                                                            if($ikey=='0'){                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
                                                            }
                                                            echo "</a>";
                                                            }
                                                            echo "</div>";
                                                       }
                                                       if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
                                                       {
                                                            echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['comment_id'] . ",".$key.",this)'>&nbsp;</button>";
                                                       }
                                                       echo "</div>";
                                                       if($i >= 3 && $flag=="false")
                                                       {
                                                            echo "<div id='comment_viewmore" . $key . "' class='comment_viewmore' onclick='viewmore_comment(" . $key . ")'>View more comments</div>";
                                                            $flag="true";
                                                       }
                                                       $i++;
                                                  } 
                                             } 
                                             ?>
                                        </div>
                                   </div>
                              </div>
                              <?php } ?>
                         </div>    
                         <?php if(count($activity_data) > 5){ ?> 
                         <div id="loadmore" class="load_btn">
                              <a href="javascript: void(0);" onclick="loadmore();" class="loadmore panel-green"> 
                                   Load More <i class="fa fa-spinner fa-spin" id="loader_logo" style="display:none;"></i>
                              </a>
                         </div> 
                         <?php } ?>                            
                    </div>
               </div>
          </div>
     </div>
</div>
<?php
function timeAgo($time_ago)
{
     $cur_time = time();
     $time_elapsed = $cur_time - $time_ago;

     $seconds = $time_elapsed;
     $minutes = round($time_elapsed / 60);
     $hours = round($time_elapsed / 3600);
     $days = round($time_elapsed / 86400);
     $weeks = round($time_elapsed / 604800);
     $months = round($time_elapsed / 2600640);
     $years = round($time_elapsed / 31207680);
     // Seconds
     if ($seconds <= 60)
     {
          echo "$seconds seconds ago";
     }
     //Minutes
     else if ($minutes <= 60)
     {
          if ($minutes == 1)
          {
               echo "one minute ago";
          }
          else
          {
               echo "$minutes minutes ago";
          }
     }
     //Hours
     else if ($hours <= 24)
     {
          if ($hours == 1)
          {
               echo "an hour ago";
          }
          else
          {
               echo "$hours hours ago";
          }
     }
     //Days
     else if ($days <= 7)
     {
          if ($days == 1)
          {
               echo "yesterday";
          }
          else
          {
               echo "$days days ago";
          }
     }
     //Weeks
     else if ($weeks <= 4.3)
     {
          if ($weeks == 1)
          {
               echo "a week ago";
          }
          else
          {
               echo "$weeks weeks ago";
          }
     }
     //Months
     else if ($months <= 12)
     {
          if ($months == 1)
          {
               echo "a month ago";
          }
          else
          {
               echo "$months months ago";
          }
     }
     //Years
     else
     {
          if ($years == 1)
          {
               echo "one year ago";
          }
          else
          {
               echo "$years years ago";
          }
     }
}
?>
<script type="text/javascript">
     setInterval(function(){
         refreshfeed(); // this will run every 1 Minitues
     }, 60000);
     function refreshfeed()
     {
          var user_id="<?php echo $user[0]->Id; ?>";
          if(user_id!=""){
               jQuery.ajax({
                    url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/refresh_all_feed'; ?>/",
                    async: true,
                    success: function(result)
                    {
                         jQuery("#messages").html(result);
                         jQuery("#loadmore").css('display', 'block');  
                    }
               });
          }
     }
     function showcommentbox(id)
     {
          $('#slidepanel'+id).slideToggle('slow');
     }
     function viewmore_comment(id)
     {
          jQuery("#comment_conten" + id + " div").each(function() {
               jQuery(this).removeClass('comment_msg_hide');
          });
          jQuery("#comment_viewmore" + id).remove();
     }
     function comment_photo(id)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
               jQuery("#commentform" + id).ajaxForm({target: '#cpreview' + id,
                    beforeSubmit: function() {
                         jQuery('#camera_icon_comment').hide();
                         jQuery("#imageloadstatus").show();
                         jQuery("#imageloadbutton").hide();
                    },
                    success: function() {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();
                         jQuery(".comment_section_btn").click(function()
                         {
                              jQuery(".addmore_photo").css("display", "none");
                              jQuery(this).parent().remove();
                              jQuery("#preview li").each(function() {
                                   jQuery(".addmore_photo").css("display", "block");
                              });
                              jQuery('#camera_icon_comment').show();
                         });
                    },
                    error: function() {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();

                    }
               }).submit();
               return false;
          }
          else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }

     }
     function addcomment(id)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
               var str = jQuery("#commentform" + id).serialize();
               var values = jQuery("input[name='cmphto[]']");
               if (jQuery.trim(jQuery("#comment_text" + id).val()) != "" || values.val() != "")
               {
                    jQuery.ajax({
                         url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name."/".$Subdomain; ?>/commentaddpublic/",
                         data: str+"&key_post="+id,
                         type: "POST",
                         async: true,
                         success: function(result)
                         {
                              var comm=parseInt(jQuery('#comment_count_'+id).html())+1;
                              jQuery('#comment_count_'+id).html(comm);
                              jQuery("#comment_conten" + id).html(result);
                              jQuery("#comment_text" + id).val('');
                              jQuery("#cpreview" + id).html('');
                              jQuery('#camera_icon_comment').show();
                         }

                    });
               }
               else
               {
                    alert("Please write something or photo to add comment.");
                    jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
                    return false;
               }
               return false;
          }
         else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }
     }
     function removecomment(id, post_id,e)
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
         if(user==1)
         {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name."/".$Subdomain; ?>/delete_comment/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    var comm=parseInt(jQuery('#comment_count_'+post_id).html())-1;
                    jQuery('#comment_count_'+post_id).html(comm);
                    jQuery(e).parent().remove();
               }
          });
          }
          else
         {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
         }
     }
     function like(btnid,post_id,post_type)
     {
          var user_id="<?php echo $user[0]->Id; ?>";
          if(user_id!=""){
               $.ajax({
                    url:"<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/Save_user_like_data/' ?>",
                    type:"POST",
                    data:"module_primary_id="+post_id+"&module_type="+post_type+"&user_id="+user_id,
                    success:function(result)
                    {
                         $('#like_count_'+btnid).html(result);
                         $('#like_'+btnid).hide();
                         $('#dislike_'+btnid).show();
                    }
               });
          }
          else
          {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
          }
     }
     function unlike(btnid,post_id,post_type)
     {
          var user_id="<?php echo $user[0]->Id; ?>";
          if(user_id!=""){
               $.ajax({
                    url:"<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/Save_user_like_data' ?>",
                    type:"POST",
                    data:"module_primary_id="+post_id+"&module_type="+post_type+"&user_id="+user_id,
                    success:function(result)
                    {
                         $('#like_count_'+btnid).html(result);
                         $('#like_'+btnid).show();
                         $('#dislike_'+btnid).hide();
                    }
               });
          }
          else
          {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
          }     
     }     
     function sendmessage()
     {
          var user="<?php echo !empty($user) ? '1' : '0'; ?>";
          if(user==1)
          {
               jQuery("#sendbtn").html('Sending <i class="fa fa-refresh fa-spin"></i>');
               jQuery("#sendbtn").attr('disabled','disabled');
               var str = jQuery("#imageform").serialize();
               var flag=false;
               if(jQuery("#Message").val() != "")
               {
                    flag=true;
               }
               if (flag==false)
               {
                    alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
                    jQuery("#sendbtn").html('Publish Post <i class="fa fa-arrow-circle-right"></i>');
                    jQuery("#sendbtn").removeAttr('disabled');
                    return false;
               }

               jQuery.ajax({
                    url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/addupdate_feed'; ?>/",
                    data: str,
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                         jQuery("#messages").html(result);
                         jQuery(".addmore_photo").css('display', 'none');      
                         jQuery("#sendbtn").html('Publish Post <i class="fa fa-arrow-circle-right"></i>');
                         jQuery("#sendbtn").removeAttr('disabled');
                         jQuery('#Message').val(''); 
                         jQuery("#preview").html('');    
                    }
               });
          }
          else
          {
               var url="<?php echo $url.'?action=1'; ?>";
               window.location.href=url;
          }
     }
     function readmore(id)
     {
          var text=$.trim($('#readmore_readless_link_'+id).text());
          if(text=="Read More")
          {
               $('#short_msg_show_'+id).hide();
               $('#fully_msg_show_'+id).show();
               $('#readmore_readless_link_'+id).html("Read Less");
          }
          else
          {
               $('#short_msg_show_'+id).show();
               $('#fully_msg_show_'+id).hide();
               $('#readmore_readless_link_'+id).html("Read More");
          }
     }
     function loadmore()
     {
          $('#loader_logo').show();
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/loadmore_feed'; ?>/"+$('#messages > div').length,
               async: true,
               success: function(result)
               {
                    if ($.trim(result)!="No Feed")
                    {
                         jQuery("#messages").append(result);
                    }
                    else
                    {
                         jQuery("#loadmore").css('display', 'none');
                    }
                    $('#loader_logo').hide();
               }

          });
     }
     jQuery.noConflict();
     jQuery(document).ready(function() {
          jQuery(document).on('mouseover', '.msg_photo', function() {
               var my_class = $(this).find('a').attr('class');
               $("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
          });
          jQuery(document).on('mouseover', '.msg_fromname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'), maxHeight: '95%',width:"50%"});
          });
          jQuery(document).on('mouseover', '.msg_toname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'),maxHeight: '95%',width:"50%"});
          });
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("#photoimg").removeAttr('multiple');
          }
          else
          {
               jQuery("#photoimg").attr('multiple');
          }
         FormElements.init();
     });
</script>

