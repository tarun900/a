<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];

if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('12', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads alert alert-success alert-dismissable">
    <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 

    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>

<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12 marg10 msg_mobile">
        <?php if(isset($speakers)) { ?>
        <h2>Speakers</h2>
        <div class="panel panel-white" id="exibitor">
            <div>
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                    <thead>
                        <tr>
                            <th>User name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($speakers as $key1 => $value1) {
                              foreach ($value1 as $key => $value) {
                         ?>
                        <tr>
                            <td><?php if(isset($value['Firstname']))
                                      { 
                                        echo $value['Firstname'] ; 
                                      }
                                      else
                                      {
                                        echo 'User';
                                      }
                                ?>
                            </td>
                            <td>
                                <a href="<?php echo base_url(); ?>Messages/<?php echo $event_templates[0]['Subdomain']; ?>/chats/<?php echo $value['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                            </td>
                        </tr>
                    <?php } } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php } ?>

        <?php if(isset($attendees)) { ?>

        <h2>Attendees</h2>
        <div class="panel panel-white" id="exibitor">
            <div>
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                    <thead>
                        <tr>
                            <th>User name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($attendees as $key1 => $value1) {
                              foreach ($value1 as $key => $value) {
                        ?>
                        <tr>
                            <td><?php if(isset($value['Firstname']))
                                      { 
                                        echo $value['Firstname'] ; 
                                      }
                                      else
                                      {
                                        echo 'User';
                                      }
                                ?>
                            </td>
                            <td>
                                <a href="<?php echo base_url(); ?>Messages/<?php echo $event_templates[0]['Subdomain']; ?>/chats/<?php echo $value['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                            </td>
                        </tr>
                    <?php } } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!-- end: PAGE CONTENT-->
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function()
     {
       //Main.init();
       TableData.init();
     });
</script>
<script type="text/javascript">
  function add_advertise_hit()
  {
     
      $.ajax({
                url : '<?php echo base_url().Agenda."/".$Subdomain ?>/add_advertise_hit',
                type: "POST",  
                async: false,
                success : function(data1)
                {

                }
             });
  }
</script>