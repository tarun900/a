<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Sponsored Post</span></h4>
			</div>
			<div class="panel-body">
                <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
					<div class="row">
                        <div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Post Title<span class="symbol required"></span>
								</label>
                                <input type="text" placeholder="Post Title" class="form-control required" id="title" name="title">
							</div>
                            <div class="form-group">
								<label class="control-label">Sponsors Link<span class="symbol required"></span>
								</label>
								<input type="url" placeholder="Sponsors Link" class="form-control required" id="url" name="url">
							</div>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="submit" name="submit" class="btn btn-green btn-block">
                                </div>
                            </div>
						</div>
                         <div class="col-md-6">
                                <div class="form-group">
								   <label>Image Upload</label>
                                   <p>(Image size must be 700px width x 100px height)</p>
                                   <span class="help-block" style="display: none;color: #A94442;" id="img_val"></span>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail"></div>
									<div class="fileupload-preview fileupload-exists thumbnail"></div>
									<div class="user-edit-image-buttons">
										<span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                           <input type="file" name="image" id="fileUpload" onchange="Upload();" class="required">
										</span>
										<a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
											<i class="fa fa-times"></i> Remove
										</a>
									</div>
								</div>
							</div>
						</div>							
					</div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
function Upload() {
    var fileUpload = document.getElementById("fileUpload");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (fileUpload.files) != "undefined") {
            var reader = new FileReader();
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (height != 100 || width != 700) {
                        $('#img_val').html("Image size should be 700px width x 100px height.<br> Your Image is "+width+"px width x "+height+"px height.");
                        $('#img_val').show();
                        return false;
                    }
                    else
                    {
                        $('#img_val').hide();
                    }
                };
            }
        } 
    }else {
        $('#img_val').html("Please Select Valid Image.");
        $('#img_val').show();
        return false;
    }
}
</script>