<!-- start: PAGE CONTENT -->
<div class="row">
     <div class="col-sm-12">
          <!-- start: TEXT FIELDS PANEL -->
          <div class="panel panel-white">
               <div class="panel-heading">
                    <h4 class="panel-title">Edit <span class="text-bold">Doc Category</span></h4>
                    <div class="panel-tools">
                         <a class="btn btn-xs btn-link panel-close" href="#">
                              <i class="fa fa-times"></i>
                         </a>
                    </div>
               </div>
               <div class="panel-body">
                    <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
                         <div class="row">
                              <?php if ($this->session->flashdata('site_setting_data') == "Updated")
                              {   
                                   
                                   ?>
                                   <div class="errorHandler alert alert-success no-display" style="display: block;">
                                        <i class="fa fa-remove-sign"></i> Updated Successfully.
                                   </div>
<?php }
?>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label class="control-label">Title </span>
                                        </label>
                                        <input type="text" placeholder="Title" class="form-control required" id="title" name="title" value="<?php echo $docscategory[0]['title']; ?>">
                                        <input type="hidden" class="form-control" id="idval" name="idval" value="<?php echo $docscategory[0]['id']; ?>">
                                   </div>

                                   <?php if ($user->Role_name == 'Client')
                                   {
                                        ?>
                                        <div class="form-group">
                                             <label for="form-field-select-1">Status <span class="symbol required"></span></label>
                                             <select id="form-field-select-1" class="form-control" name="status">
                                                  <option value="1" <?php if ($docscategory[0]['status'] == "1")
                                        { ?> selected="selected" <?php } ?>>Active</option>                                                                                
                                                  <option value="0" <?php if ($docscategory[0]['status'] == "0")
                                        { ?> selected="selected" <?php } ?>>Inactive</option>
                                             </select>                                                                
                                        </div>
<?php } ?>

                                   <div class="form-group">
                                        <label class="control-label">Doc Images</span>
                                        </label>

                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <div class="fileupload-new thumbnail"></div>
                                             <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                             <div class="user-edit-image-buttons">
                                                  <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                       <input type="file" name="images">
                                                  </span>
                                                  <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                       <i class="fa fa-times"></i> Remove
                                                  </a>
                                             </div>
                                             <?php
                                                  if($docscategory[0]['image']!=NULL)
                                                  {
                                             ?>
                                             <div class="row" style="margin-left: 0px;">
                                                  <div class="col-sm-3 fileupload-new thumbnail center">
                                                       <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $docscategory[0]['image']; ?>">
                                                  </div>
                                             </div>
                                             <?php
                                                  }
                                                  ?>
                                        </div>

                                   </div>
                                   
                                   <div class="form-group" style="float:left;">
                                        <label>Cover Image</label>
                                        <div class="fileupload fileupload-new required" data-provides="fileupload">
                                             <div class="fileupload-new thumbnail"></div>
                                             <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                             <div class="user-edit-image-buttons">
                                                  <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                       <input type="file" name="coverimages">
                                                  </span>
                                                  <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                       <i class="fa fa-times"></i> Remove
                                                  </a>
                                             </div>
                                        </div>
                                        <?php
                                             if(!empty($Document['coverimages']))
                                             {
                                        ?>
                                        <div class="row" style="margin-left: 0px;">
                                             <div class="col-sm-3 fileupload-new thumbnail center">
                                                  <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $Document['coverimages']; ?>" style="height: 142px;" width="209">
                                             </div>
                                        </div>
                                        <?php 
                                             }
                                        ?>
                                   </div> 
                                   
                                   <div class="row" style="float: left;width: 100%;">
                                        <div class="col-md-4">
                                             <button class="btn btn-green btn-block" type="submit">Update <i class="fa fa-arrow-circle-right"></i></button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>
               </div>
          </div>
          <!-- end: TEXT FIELDS PANEL -->
     </div>
</div>
<!-- end: PAGE CONTENT-->