<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/user/form-validation.js"></script>
<script>
	jQuery(document).ready(function() {
		FormElements.init();
                FormValidator.init();
	});
        
        function checkrole()
        {      
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Role_management/checkrole',
                data :'role='+$("#role").val()+'&idval='+$('#idval').val()+'&event_id='+$('#event_id').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    
                    if(values[0]=="error")
                    {   
                        $('#role').parent().removeClass('has-success').addClass('has-error');
                        $('#role').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#role').parent().find('.help-block').removeClass('valid').html(values[1]);
                       
                        sendflag=false;

                    }
                    else
                    {
                        $('#role').parent().removeClass('has-error').addClass('has-success');
                        $('#role').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#role').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
</script>
