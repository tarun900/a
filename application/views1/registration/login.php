                <html>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
                <title>Login</title>
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login">
		<div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="logo">
				<img src="<?php echo base_url(); ?>assets/images/logo.png">
			</div>
			<!-- start: LOGIN BOX -->
			<div class="box-login">
			    <div class="box-login1">
				<h1>All<span>inthe</span>loop</h1>
				<h3>Sign in to the CMS</h3>
				 <form method="POST" class="form-login" action="<?php echo base_url();?>Attendee_login/check/<?php echo $Subdomain; ?>">

				 	<?php if($this->session->flashdata('invlaidlogin_data')){ ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('invlaidlogin_data'); ?> 
                    </div>
                    <?php } ?>

					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> There is an error in your submission.
					</div>
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<input type="text" class="form-control" id="username" name="username" placeholder="Email">
								<i class="fa fa-user"></i> </span>
						</div>
						<div class="form-group form-actions">
							<span class="input-icon">
								<input type="password" class="form-control password" id="password" name="password" placeholder="Password">
								<i class="fa fa-lock"></i>
								
								</span>
						</div>
					
						<div class="form-actions">
						<!--	<a href="#" class="registerlink">
								<button type="button"  class="register btn">
		                                Create an account</button>
		                    </a> -->
							<div class="forgetpassworddiv">
							<div class="forgetpassworddiv1">
                                                            <a style="position: relative;" class="forgot" href="javascript:void(0);">
									I forgot my password
								</a>
								<a class="sitename">www.allintheloop.com</a>
							</div>
					<?php for($i=0;$i<count($event_templates);$i++) { ?>
                        <input type="hidden" name="Event_id" value="<?php echo $event_templates[$i]['Id']; ?>">
                        <input type="hidden" name="Organisor_id" value="<?php echo $event_templates[$i]['Organisor_id']; ?>">
                        <input type="hidden" name="Active" value="1">
                        <input type="hidden" name="attendee_flag" value="0">
                        <?php } ?>    
							<div class="forgetpassworddiv2">
							<button type="submit" class="btn btn-green pull-right">
								Login <i class="fa fa-arrow-circle-right"></i>
							</button>
							</div>
						</div>
						</div>
							
					</fieldset>
					</div>
						<div class="box-login2">
							<a href="javascript:void(0);" class="register">
								Create an account
							</a>
							<a href="<?php echo base_url(); ?>#" class="visitguides">
								Visit Guides
							</a>
						</div>
						 <h3 class="form-signin-heading">Login with Facebook</h3>
                        <input type="button" class="btn btn-lg btn-primary btn-block" role="button" onclick="login_fb()" value="Login">
               
			
				</form>


			<!--	<?php if (@$user_profile): ?>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <img class="img-thumbnail" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?=$user_profile['id']?>/picture?type=large" style="width: 140px; height: 140px;">
                                <h2><?php // echo $user_profile['session_name() ']; ?></h2>
                                <a href="<?php // echo $user_profile['link']; ?>" class="btn btn-lg btn-default btn-block" role="button">View Profile</a>
                                <a href="<?php // echo $logout_url; ?>" class="btn btn-lg btn-primary btn-block" role="button">Logout</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <h3 class="form-signin-heading">Login with Facebook</h3>
                        <input type="button" class="btn btn-lg btn-primary btn-block" role="button" onclick="login_fb()" value="Login">
                    <?php endif; ?>  -->

			</div>
			<!-- end: LOGIN BOX -->
			<!-- start: FORGOT BOX -->
			<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
			<div class="box-forgot">
				<h3>Forget Password?</h3>
				<p>
					Enter your e-mail address below to reset your password.
				</p>
				<form method="POST" class="form-forgot" action="return false;">
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> There is an error in your submission.
					</div>
                                        <div class="errorHandler alert alert-danger no-display" id="forgot_msg">
						<i class="fa fa-remove-sign"></i> There is an error in your submission.
					</div>
					<fieldset>
						<div class="form-group">
							<span class="input-icon">
								<input type="email" class="form-control" name="email" placeholder="Email" id="forgotemail">
								<i class="fa fa-envelope"></i> </span>
						</div>
						<div class="form-actions">
							<a class="btn btn-light-grey go-back">
								<i class="fa fa-chevron-circle-left"></i> Log-In
							</a>
							<button type="submit" class="btn btn-green pull-right" onclick="sendforgotmail(); return false;">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
						</div>
					</fieldset>
				</form>
			</div>
			<!-- end: FORGOT BOX -->
			<!-- start: REGISTER BOX -->
			<div class="box-register">
				<h3>Sign Up</h3>
				<p>
					Enter your personal details below:
				</p>
                                <form method="POST" class="form-register" action="<?php echo base_url();?>Attendee_login/add/<?php echo $Subdomain; ?>" id="form">
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> There is an error in your submission.
					</div>
					<fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                            <?php if($Email!=""){ ?>      
                            <input type="email1" class="form-control"  name="email1" id="email1" placeholder="Email" <?php if($Email!=""){ ?> disabled <?php } ?>  value="<?php if($Email!=""){ echo base64_decode(urldecode($Email)); } ?>">
                             <?php } ?>
                           
                            <input type="email" class="form-control"  name="email" id="email" placeholder="Email" <?php if($Email!=""){ ?> style="visibility:hidden;width: 0px; height: 0px;" <?php } ?> onblur="checkemail();" value="<?php if($Email!=""){ echo base64_decode(urldecode($Email)); } ?>">
                                <i class="fa fa-envelope"></i> </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
                                
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
                                
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <i class="fa fa-lock"></i> </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control" name="password_again" placeholder="Password Again">
                                <i class="fa fa-lock"></i> 
                            </span>
                        </div>
                         <div class="form-group">
                                <input type="text" class="form-control" name="Company_name1" placeholder="Company Name(optional)">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Title1" placeholder="Title(optional)">
                        </div>

                        <div class="form-group">
                             <select id="country" onchange="get_state();" class="form-control" name="Country">
                                <option value="">Select Country...</option>
                                <?php foreach($countrylist as $key=>$value) {  ?>
                                        <option value="<?php echo $value['id']; ?>" <?php if($user_overview[0]->Country==$value['id']) { ?> selected <?php } ?>><?php echo $value['country_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php for($i=0;$i<count($event_templates);$i++) { ?>
                        <input type="hidden" name="Event_id" value="<?php echo $event_templates[$i]['Id']; ?>">
                        <input type="hidden" name="Organisor_id" value="<?php echo $event_templates[$i]['Organisor_id']; ?>">
                        <input type="hidden" name="Active" value="1">
                        <input type="hidden" name="attendee_flag" value="0">
                        <?php } ?>
                        <div class="form-actions">
                            Already have an account?
                            <a href="#" class="go-back">Log-in</a>
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </fieldset>
				</form>
			</div>
			<!-- end: REGISTER BOX -->
		</div>
<style>
body.login {
    background-image: url('<?php echo base_url(); ?>assets/images/bg_login.jpg');
    background-size: cover;
}
body.login .main-login {
    
    position: relative;
   
}
body.login .box-login {
    background: none;
    border-radius: 5px;
    box-shadow: none;
    overflow: hidden;
    padding: 15px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}
.box-login1 {
    background-color: #ffffff;
    padding: 15px;
    box-sizing: border-box;
    border-radius: 5px;
    float: left;
    width: 100%;
}

form.form-login {
    float: left;
    width: 100%;
}
.box-login1 h1 {
    width: 100%;
    float: left;
    text-align: center;
    margin: 23px 0 0 0;
    font-size: 43px;
    text-transform: uppercase;
    color: #1fb9c1;
    font-weight: lighter;
}
.box-login1 h1 span {
    color: #000000;
}
.box-login1 h3 {
    margin: 25px 0 25px 0;
    float: left;
    color: #909090;
}
.input-icon > input {
    padding-left: 25px;
    padding-right: 6px;
    height: 35px;
}
.input-icon > [class*="fa-"], .input-icon > [class*="clip-"] {
    top: 0;
}
body.login .form-actions {
    margin-top: 0;
    padding-top: 0;
    display: block;
    margin-bottom: 0;
}
input#password {
    margin-top: 2px;
}
.forgetpassworddiv {
    float: left;
    width: 100%;
    margin: 15px 0 0 0;
}

.forgetpassworddiv1 {
    float: left;
    width: 50%;
}
.forgetpassworddiv2 {
    float: right;
    width: 50%;
}
.forgetpassworddiv a {
    color: #909090;
    font-size: 19px;
    font-weight: normal;
	cursor: pointer;
}
a.sitename {
    margin: 11px 0 0 0;
    float: left;
}
.forgetpassworddiv2 button {
    background-color: #1fb9c1 !important;
    height: 50px;
    border: 0 !important;
    margin: 25px 0 0 0;
}
.box-login2 {
    background-color: #ccccce;
    float: left;
    width: 100%;
    margin: 37px 0 0 0;
    padding: 41px;
    box-sizing: border-box;
    border-radius: 5px;
    opacity: 0.8;
}
a.register {
    float: left;
    width: 46%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
a.visitguides {
    float: right;
    width: 47%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
body.login .main-login {
    margin-top: 30px;
    position: relative;
}

body.login .main-login {
    margin-top: 30px;
    position: relative;
    
}
@media only screen and (max-width:1024px){
body.login .main-login {
    
    width: 38% !important;
}
.box-login1 h1 {
    
    font-size: 26px;
   
}
.forgetpassworddiv1 {
    float: left;
    width: 55%;
}
.forgetpassworddiv2 {
    float: right;
    width: 45%;
}
a.register {
    
    font-size: 12px;
    
}
a.visitguides {
    
    font-size: 12px;
}
}
@media only screen and (max-width:980px){
body.login .main-login {
   
    width: 66.66666667% !important;
}
}

@media only screen and (max-width:767px){
body.login .main-login {
   
    width: 83.33333333% !important;
}
}
@media only screen and (max-width:640px){
.box-login1 h3 {
   
    font-size: 17px;
}
}
@media only screen and (max-width:480px){
.input-icon > input {
  
    width: 100%;
}
}
@media only screen and (max-width:360px){
.forgetpassworddiv1 {
    float: left;
    width: 100%;
}
.forgetpassworddiv2 {
    float: right;
    width: 100%;
}
.forgetpassworddiv2 button {
   
    width: 100%;
}
a.register {
    font-size: 15px;
    width: 100%;
}
a.visitguides {
    font-size: 15px;
    width: 100%;
    margin: 15px 0 0 0;
}
}
</style>
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.transit/jquery.transit.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Registration.init();
				Login.init();
			});
		</script>
		
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="<?php echo base_url() ?>assets/js/registration.js"></script>
<script>
    jQuery(document).ready(function() {
       // Main.init();
        Registration.init();
         <?php
        if($Email!="")
        {
        echo 'jQuery(".register").click();';
        }
        ?>
          FormValidator.init();
       
    });
    function login_fb()
    {
        var name="";
        var email="";
        var img="";
        fbAsyncInit();
        FB.login(function(response) 
        {
            if (response.status== 'connected') 
            {
         
              FB.api("/me/picture?width=200&redirect=0&type=normal&height=200", function (response) 
              {
                     if (response && !response.error) {
                       /* handle the result */
                       console.log('PIC ::', response);
                       img=response.data.url;
                     }
              });
              FB.api('/me?fields=name,email', function(response)
              {
                    name=response.name;
                    email=response.email;
                    $.ajax({
                        url : '<?php echo base_url(); ?>Attendee_login/fblogin/<?php echo $Subdomain; ?>',
                        data :'name='+name+'&img='+img+'&email='+email,
                        type: "POST",           
                        success : function(data)
                        {
                            if(data=="success")
                            {
                                 window.location.href='<?php echo base_url(); ?>App/<?php echo $Subdomain; ?>'; 
                            }                           
                            
                        }
                    });
              });
            }
        }
       ,{
     scope: "email,public_profile"
        }
        );
    }
    function fbAsyncInit() 
    {
      FB.init({
       appId      : '1468562826783502',
       status     : true, // check login status
       cookie     : true, // enable cookies to allow the server to access the session
       xfbml      : true  // parse XFBML
      });
    }
</script>
<script type="text/javascript"> 
function sendforgotmail()
{  
      
    $.ajax({
    url : '<?php echo base_url(); ?>Attendee_login/forgot_pas',
    data :'email='+$("#forgotemail").val(),
    type: "POST",           
    success : function(data)
    {
        
        var values=data.split('###');
        if(values[0]=="error")
        {   
            $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
            $('#forgotemail').parent().find('.control-label span').removeClass('valid');
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display');
            $("#forgot_msg").addClass('alert-danger');
            $("#forgot_msg").fadeIn();
        }
        else
        {
            $('#forgot_msg').show();
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display').addClass('alert-success');
            $("#forgot_msg").removeClass('alert-danger');
            $("#forgot_msg").fadeOut(3000);
        }
        
    }
    });
}
</script>
<script type="text/javascript">
function checkemail()
{      
    

    var sendflag="";
    $.ajax({
        url : '<?php echo base_url(); ?>Attendee_login/checkemail',
        data :'email='+$("#email").val()+'&idval='+$('#idval').val()+'&event_id='+$("#Event_id").val(),
        type: "POST",  
        async: false,
        success : function(data)
        {
            var values=data.split('###');
            if(values[0]=="error")
            {   
                $('#email').parent().removeClass('has-success').addClass('has-error');
                $('#email').parent().find('.control-label span').removeClass('ok').addClass('required');
                $('#email').parent().find('.help-block').removeClass('valid').html(values[1]);
                sendflag=false;               
            }
            else
            {
                $('#email').parent().removeClass('has-error').addClass('has-success');
                $('#email').parent().find('.control-label span').removeClass('required').addClass('ok');
                $('#email').parent().find('.help-block').addClass('valid').html(''); 
                sendflag=true;               
            }
        }
    });
    return sendflag;
}
</script>

	