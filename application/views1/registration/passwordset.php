<script>

  $(document).ready(function() {
$('.box-login').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
    $(this).hide().removeClass("animated bounceOutRight");

});
$('.box-register').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
    $(this).show().removeClass("animated bounceInLeft");

});
  });  
</script>
<div class='popup responsive-pop'>
    <div class='cnt223'>
           <!-- start: LOGIN BOX -->
    <div class="main-login">
            <!-- start: LOGIN BOX -->
            
            <form id="form-register" method="POST"  class="form-register" action="<?php echo base_url();?>Attendee_login/setpassword/<?php echo $Subdomain; ?>" > 
              
               
            
           <?php if(!empty($arrCustomCols)): ?>
                <div class="box-register">
                    <div class="login-popup-header">
                     <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                     <p>
                        Please set your password to access the app <?php echo ucfirst($Subdomain); ?>
                     </p>
                     </div>
                    <div class="errorHandler alert alert-danger no-display">
                             <i class="fa fa-remove-sign"></i> There is an error in your submission.
                         </div>
                    <fieldset>
                        <?php foreach($arrCustomCols as $intKey=>$strVal): ?>
                        <div class="form-group">
                                 <span class="input-icon">
                                     <input type="text" class="form-control required" required  name="arrCustom[<?php echo $strVal['column_name'];?>]" placeholder="<?php echo $strVal['column_name'];?>">
                                 </span>
                        </div>
                        <?php endforeach;?>
                        <div class="form-actions">
                        <button class="btn btn-yellow next_signup" type="submit">Next</button>
                        </div>
                    </fieldset>
                </div>
           <?php else: ?>
            <script type="text/javascript">
              var intFlag=true;
            </script>
           <?php endif;?>
           <div class="box-next_signup" id="n1" <?php if(!empty($arrCustomCols)){ ?> style="display: none;" <?php } ?>>
                <!--<h3>Sign Up</h3>
                <p>
                    Enter your personal details below:
                </p>-->
                <div class="login-popup-header">
                <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                <p>
                   Please set your password to access the app <?php echo ucfirst($Subdomain); ?>
                </p>
                </div>

                
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> There is an error in your submission.
                    </div>
                    <fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                           
                            <input type="hidden" class="form-control"  name="email" id="email" placeholder="Email" <?php if($Email!=""){ ?> style="visibility:hidden;width: 0px; height: 0px;" <?php } ?> onblur="checkemail();" value="<?php if($Email!=""){ echo base64_decode(urldecode($Email)); } ?>">
                               </span>
                                                    </div>
                        
                        
                       
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control required" id="password" name="password" placeholder="Password">
                                <i class="fa fa-lock"></i> </span>
                             <span class="help-block" style="display:none;" id="password_error" for="password">This field is required.</span>   
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control required" id="password_again" name="password_again" placeholder=" Confirm Password">
                                <i class="fa fa-lock"></i> 
                            </span>
                            <span class="help-block" style="display:none;" id="password_again_error" for="password">This field is required.</span>
                        </div>
                        
                        <?php for($i=0;$i<count($event_templates);$i++) { ?>
                        <input type="hidden" name="Event_id" value="<?php echo $event_templates[$i]['Id']; ?>">
                        <input type="hidden" name="Organisor_id" value="<?php echo $event_templates[$i]['Organisor_id']; ?>">
                        <input type="hidden" name="Active" value="1">
                        <input type="hidden" name="attendee_flag" value="0">
                        <?php } ?>
                      
                        <a href="https://allintheloop.com/terms.html" target="_blank">Terms and conditions</a>
                        <br>
                        <div class="form-actions">
                              <input type="submit" value="Open" class="btn btn-yellow" id="formsubmitbtn">
                        </div>
                    </fieldset>
                
                <!-- start: COPYRIGHT -->
                <!-- end: COPYRIGHT -->
            </div>
              </form>
            <!-- end: FORGOT BOX -->
        </div>
    <!-- end: LOGIN BOX -->
    </div>
</div>
<div class="overlay" style="background:rgba(0, 0, 0, 0.7) none repeat scroll 0 0; height:100%;left:0;position:fixed;top:0;width:100%;z-index: 9999;
"></div>

<div class="slider-banner">
    <div class="row">
        <?php for($i=0;$i<count($event_templates);$i++) { ?>
        <?php $image_array = json_decode($event_templates[$i]['Images']); ?>
        <?php if(!empty($image_array))   { ?>
          <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
        <?php } }?>
       
 <!--<img title="" alt="" src="<?php //echo base_url().'assets/images/events_medium.jpg';?>"> -->   
 </div>
</div>
<?php if($event_templates[0]['Description']=="<br>" || $event_templates[0]['Description']=="") {  } else{ ?>
<div class="event-content panel-white">
     <div class="event_description">
          <?php for ($i = 0; $i < count($event_templates); $i++) { 
           ?>
          <p style="padding-left: 25px;">
            <?php
               $string = $event_templates[$i]['Description'];
               echo $string;
            ?>
          </p>
      <?php   }  ?>
     </div>
</div>
<?php } ?>
<div class="event-list" style="display:block;background:<?php for ($i = 0; $i < count($event_templates); $i++) {
     echo $event_templates[$i]['Background_color']; } ?>">
     <div class="row">
        <div class="col-md-12">
                    <?php
                    foreach ($menu as $key => $value)
                    {
                         
                         $img_view = $value['img_view'];
                         $img = $value['img'];
                         $url = base_url();
                         $bimg = "$url/assets/user_files/$img";
                         $dimg = $url."assets/images/EventApp_Default.jpg";
                         for ($i = 0; $i < count($event_templates); $i++)
                         {
                              if(!in_array($value['id'],array(22,23,24,25)))
                              {
                                   if($value['id']==12)
                                   {
                                        $url = base_url() .'Messages/'. $acc_name."/".$event_templates[$i]['Subdomain'].'/privatemsg';
                                   }
                                   else if($value['id']==13)
                                   {
                                       $url = base_url() .'Messages/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/publicmsg';
                                   }
                                   else
                                   {
                                        $url = base_url() . ucfirst($value['pagetitle']) . '/' . '' . $acc_name."/".$event_templates[$i]['Subdomain'];
                                   }
                                   //echo $url;
                              }
                              else
                              {
                                $url = base_url() . 'fundraising' . '/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/home/get_auction_pr';
                                $var_target="";
                                if($value['id']==22)
                                {
                                     $var_target='target="_blank"';
                                     $url=$url.'/1';
                                }
                                else if($value['id']==23)
                                {
                                     $var_target='target="_blank"';
                                     $url=$url.'/2';
                                }                                        
                                else if($value['id']==24)
                                {
                                     $var_target='target="_blank"';
                                     $url=$url.'/3';
                                }
                                else if($value['id']==25)
                                {
                                     $var_target='target="_blank"';
                                     $url=$url.'/4';
                                }
                              }
                           ?>
                              <?php if ($img_view == '0')
                              { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6">
                              <a href="<?php echo $url; ?>" <?php echo $var_target; ?>> 
               <?php if ($img != '')
               { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
               <?php }
               else
               { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
               <?php } ?>
                              </a>
                         </div>
          <?php }
          else
          { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6" style="">
                              <a href="<?php echo $url; ?>" <?php echo $var_target; ?>> 
                              <?php if ($img != '')
                              { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
                              <?php }
                              else
                              { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value['menuname']; ?></h3>
                                             </div>
                                        </div>
                              <?php } ?>
                              </a>
                         </div>
                         <?php }
                    }
               } ?>
          
          
          
          <?php
                    
                    foreach ($cms_feture_menu as $key => $value)
                    {
                         $img_view = $value->img_view;
                         $img = $value->img;
                         $url = base_url();
                         $bimg = $url."assets/user_files/".$img;
                         $dimg = $url."assets/images/defult-menuimg.png";
                         for ($i = 0; $i < count($event_templates); $i++)
                         {
                              $url = base_url() . ucfirst($value->menuurl);
                              ?>
                              <?php if ($img_view == '0')
                              { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6">
                              <a href="<?php echo $url; ?>"> 
               <?php if ($img != '')
               { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
               <?php }
               else
               { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-img">
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
               <?php } ?>
                              </a>
                         </div>
          <?php }
          else
          { ?>
                         <div class="col-md-6 col-lg-4 col-sm-6" style="">
                              <a href="<?php echo $url; ?>"> 
                              <?php if ($img != '')
                              { ?>
                                        <div style="background:url('<?php echo $bimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
                              <?php }
                              else
                              { ?>
                                        <div style="background:url('<?php echo $dimg ?>') no-repeat center #ccc; background-size: 100%;" class="event-round-img">
                                             &nbsp;
                                             <div class="event-tile">
                                                  <h3><?php echo $value->menuname; ?></h3>
                                             </div>
                                        </div>
                              <?php } ?>
                              </a>
                         </div>
                         <?php }
                    }
               } ?>
          
      </div>          
     </div>
</div>
<div class="row" style="margin-left: 0px; padding-left: 0;">
    <div class="event-timer">
        <div class="row">
            <?php for($i=0;$i<count($event_templates);$i++) 
                { 
                    $event = $event_templates[$i]['Start_date'];
                    $end_event = $event_templates[$i]['End_date'];
                    $today = date("Y-m-d");
                    if($event >= $today)
                    {
                       echo '<h3>Event starts in</h3>';
                       echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event >= $today || $end_event > $today)
                    {
                        echo '<h3>Event end in</h3>';
                        echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event < $today)
                    {
                        echo '<h3>Event have end</h3>';
                    }
                    else
                    {

                    }
                }
                ?>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script> 
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="<?php echo base_url() ?>assets/js/registration.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
         $('#form-register').validate({ // initialize the plugin
            errorElement : "span", // contain the error msg in a small tag
            errorClass : 'help-block',
            errorPlacement : function(error, element) {// render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") {// for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
        rules: {
            password:{
                required:true,
                minlength : 6,
            },
            password_again:{
                required:true,
                minlength : 6,
                equalTo : "#password"
            },
            
        },
        submitHandler: function(form) {
            //var intLength = $('.box-next_signup').length;
            var intLength = $('.box-register').length;
                if(intLength>0)
                {
                    if(intFlag==false && intC==0)
                    {
                        intC =1;
                        $('.box-register').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).hide().removeClass("animated bounceOutRight");

                        });
                        $('.box-next_signup').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                                $(this).show().removeClass("animated bounceInLeft");

                        });
                        return false;
                    }
                }
            $( ".form-register .btn" ).each(function( index ) {
                if($(this).attr('type') == "submit")
                {
                    $(this).html('Submitting <i class="fa fa-refresh fa-spin"></i>');
                }
            });
            form.Submit();
        },
        success : function(label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error');
            },
            highlight : function(element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').addClass('has-error');
                // add the Bootstrap error class to the control group
            },
            unhighlight : function(element) {// revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            }
    });
        $('.required').each(function() {
            $(this).rules('add', {
                required: true
            });
    });
    });
</script>
<script>
  function registersubmit(){
            var vali=true;
            if($.trim($('#password').val())=="")
            {
                vali=false;
                $('#password_error').show();
                $('#password').parent().parent().addClass('has-error');
            }
            else if($.trim($('#password').val()).length < 6)
            {
                vali=false;
                $('#password_error').html('Please enter at least 6 characters.');
                $('#password_error').show();
                $('#password').parent().parent().addClass('has-error');
            }
            if($.trim($('#password_again').val())=="")
            {
                vali=false;
                $('#password_again_error').show();
                $('#password_again').parent().parent().addClass('has-error');
            }
            else if($.trim($('#password_again').val()).length < 6)
            {
                vali=false;
                $('#password_again_error').html('Please enter at least 6 characters.');
                $('#password_again_error').show();
                $('#password_again').parent().parent().addClass('has-error');
            }
            if($.trim($('#password').val())!=$.trim($('#password_again').val()))
            {
                vali=false;
                $('#password_again_error').html('Please enter the same value again.');
                $('#password_again_error').show();
                $('#password_again').parent().parent().addClass('has-error');
            }
            if(vali==true)
            {
              $('#formsubmitbtn').attr('disabled','disabled');
              $('#formsubmitbtn').attr('style','background:#ffb848 !important;');
              //$('#form-register').Submit();
              return true;
            }
            else
            {
                return false;
            }
        }
    jQuery(document).ready(function() {

        Main.init();
        Registration.init();
         <?php
        if($Email!="")
        {
        echo 'jQuery(".register").click();';
        }
        ?>
    });
</script>
<script type="text/javascript"> 
function sendforgotmail()
{       
     var strEmail    =   $("#forgotemail").val();
    var intEventId  =   $("#hdnEvent_id").val();
    
    $.ajax({
    url : '<?php echo base_url(); ?>Attendee_login/forgot_pas',
    //data :'email='+$("#forgotemail").val(),
    data:{email:strEmail,intEvent:intEventId},
    type: "POST",           
    success : function(data)
    {
        var values=data.split('###');
        if(values[0]=="error")
        {   
            $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
            $('#forgotemail').parent().find('.control-label span').removeClass('valid');
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display');
            $("#forgot_msg").removeClass('alert-success');
            $("#forgot_msg").fadeIn();
        }
        else
        {
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display').addClass('alert-success');
            $("#forgot_msg").removeClass('alert-danger');
            $("#forgot_msg").fadeOut(3000);
        }
        
    }
    });
}
</script>
<script type="text/javascript">
function checkemail()
{      
    

    var sendflag="";
    $.ajax({
        url : '<?php echo base_url(); ?>Attendee_login/checkemail',
        data :'email='+$("#email").val()+'&idval='+$('#idval').val()+'&event_id='+$("#Event_id").val(),
        type: "POST",  
        async: false,
        success : function(data)
        {
            var values=data.split('###');
            if(values[0]=="error")
            {   
                $('#email').parent().removeClass('has-success').addClass('has-error');
                $('#email').parent().find('.control-label span').removeClass('ok').addClass('required');
                $('#email').parent().find('.help-block').removeClass('valid').html(values[1]);
                sendflag=false;               
            }
            else
            {
                $('#email').parent().removeClass('has-error').addClass('has-success');
                $('#email').parent().find('.control-label span').removeClass('required').addClass('ok');
                $('#email').parent().find('.help-block').addClass('valid').html(''); 
                sendflag=true;               
            }
        }
    });
    return sendflag;
}
</script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility-coming-soon.js"></script>
<script type="text/javascript">
        jQuery("#countdown").countdown({
            <?php for($i=0;$i<count($event_templates);$i++) { 
            $event = $event_templates[$i]['Start_date'];
            $end_event = $event_templates[$i]['End_date'];
            $end_event_date = date('Y/m/d H:i:s', strtotime($end_event));
            $edate = date('Y/m/d H:i:s', strtotime($event));
            $today = date('Y/m/d H:i:s');
            if($edate >= $today)
            {
    ?>
            date : '<?php echo $edate; ?>',
            format: "on"
    <?php }  else
    {?>
            date : '<?php echo $end_event_date; ?>',
            format: "on"

    <?php } } ?>
        });
</script>

<script type="text/javascript">
   jQuery(document).ready(function() 
   {
       $('.box-next_signup .form-title').append('<button id="btnBack" class="btn btn-yellow btnback" type="button">Back</button>');
       $('.btnback').on('click', function() {

            intC = 0;
            $('.box-next_signup').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).hide().removeClass("animated bounceOutRight");

            });
            $('.box-register').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).show().removeClass("animated bounceInLeft");

            });

        });
        Main.init();
        ComingSoon.init();
        
    });
</script>

<style type="text/css">
@import "compass/css3";

@import url(https://fonts.googleapis.com/css?family=Merriweather+Sans);
 div.social-wrap button {
    padding-right: 45px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 35px 35px;
    background-position: right center;
    background-repeat: no-repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .5) -1px -1px 0;
}
button#facebook {
    border-color: #2d5073;
    background-color: #3b5998;
    background-image: url(https://icons.iconarchive.com/icons/danleech/simple/512/facebook-icon.png);
}
button#twitter {
    border-color: #007aa6;
    background-color: #008cbf;
    background-image: url(https://twitter.com/images/resources/twitter-bird-white-on-blue.png);
}
button#googleplus {
    border-color: #111;
    background-color: #222;
    text-shadow: #333 -1px -1px 0;
    background-image:url(https://i.tinyuploads.com/KBZc6n.png);
}
div.social-wrap button:active {
    background-color: #222;
}
div.social-wrap.b button {
    padding-right: 45px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 25px 25px, cover;
    background-position: 170px center, center center;
    background-repeat: no-repeat, repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .4) -1px -1px 0;
}
div.social-wrap.b {
}
div.social-wrap.b > .googleplus {
    background-size: 30px 30px, cover;
    background-image: url(https://dl.dropbox.com/u/109135764/gplus.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #353233), color-stop(100%, #1d1b1c));
    ;
}
div.social-wrap.b > .facebook {
    background: url(https://dl.dropbox.com/u/109135764/facebookf.png), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #4c74c4), color-stop(100%, #3b5998));
    background-size: 25px 25px, cover;
    background-position: 170px center, center center;
    background-repeat: no-repeat, repeat;
}
div.social-wrap.b > .twitter {
    background-image: url(https://dl.dropbox.com/u/109135764/twitterbird.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #00aced), color-stop(99%, #00a9ff));
    ;
}
div.social-wrap.b button em {
    font-size: 18px;
    letter-spacing: 1px;
    font-family:"Times New Roman";
    margin-right: 4px;
    margin-left: 4px;
}
div.social-wrap.c button {
    padding-left: 35px;
    padding-right: 0px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 25px 25px, cover;
    background-position: 10px center, center center;
    background-repeat: no-repeat, repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .4) -1px -1px 0;
}
div.social-wrap.c {
}
div.social-wrap.c > .googleplus {
    background-size: 30px 30px, cover;
    background-image: url(https://dl.dropbox.com/u/109135764/gplus.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #353233), color-stop(100%, #1d1b1c));
    ;
}
div.social-wrap.c > .facebook {
    background: url(https://dl.dropbox.com/u/109135764/facebookf.png), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #4c74c4), color-stop(100%, #3b5998));
    background-size: 25px 25px, cover;
    background-position: 10px center, center center;
    background-repeat: no-repeat, repeat;
}
div.social-wrap.c > .twitter {
    background-image: url(https://dl.dropbox.com/u/109135764/twitterbird.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #00aced), color-stop(99%, #00a9ff));
    ;
}
div.social-wrap.c button em {
    font-size: 18px;
    letter-spacing: 1px;
    font-family:"Times New Roman";
    margin-right: 4px;
    margin-left: 4px;
}
ul.sm-wrap {
    list-style-type: none;
    padding: 0px;
}
ul.sm-wrap li {
    font-family:"Helvetica", sans-serif;
    font-size: 11px;
    margin-bottom: 10px;
    padding-left: 20px;
    background-size: 15px 15px;
    background-repeat: no-repeat;
}
ul.sm-wrap li#facebook-sm {
    background-image: url(https://icons.iconarchive.com/icons/danleech/simple/512/facebook-icon.png);
}
ul.sm-wrap li#twitter-sm {
    background-image:url(https://twitter.com/images/resources/twitter-bird-white-on-blue.png);
}
ul.sm-wrap li#gplus-sm {
    background-image: url(https://i.tinyuploads.com/KBZc6n.png);
}

ul.sm-wrap a{
    text-decoration: none;    
}
#overlaybox {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    z-index: 1500;
    display: block;
}
.cnt223 a{
    text-decoration: none;
}
.popup{
    width: 75%;
    margin: 0 auto;
    /*position: fixed;*/
    position: absolute;
    z-index: 99999;
}
.form-actions.sign-up-buttons{
    /*float: right;*/
    float:left;
    display: inline-block;
}
.form-actions.sign-up-buttons > a:first-child{
    margin-right:15px;
}
.cnt223{
    min-width: 60%;
    width: 60%;
    min-height: 150px;
    margin: 3% auto;
    background: #f3f3f3;
    position: relative;
    z-index: 103;
    padding: 10px 10px 15px;
    border-radius: 5px;
    box-shadow: 0 2px 5px #000;
}
.cnt223 p{
    clear: both;
    color: #555555;
    text-align: center;
}
.cnt223 p a{
    color: #d91900;
    font-weight: bold;
}
.cnt223 .x{
    float: right;
    height: 35px;
    left: 22px;
    position: relative;
    top: -25px;
    width: 34px;
}
.cnt223 .x:hover{
    cursor: pointer;
}
#btnBack{margin-right: 10px;float: left;}
</style>

<script type='text/javascript'>
    $(function(){
        var overlay = $('<div id="overlaybox"></div>');
        overlay.show();
        overlay.appendTo(document.body);
        $('.popup').show();
        $('.close').click(function(){
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });
    $('.x').click(function(){
       
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
        });
    });
</script>
<style type="text/css">
  .box-next_signup .form-title h2
  {
    display: none !important;
  }
  .box-next_signup .form-title h3
  {
    display: none !important;
  }
</style>

