<div class='popup responsive-pop'>
    <div class='cnt223'>
        <div class="main-login">
            <div class="box-login">
                <div class="login-popup-header">
                    <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                    <p>
                        Enter your e-mail to login in your account.
                    </p>
                </div>
                <form method="POST" class="form-login" action="<?php echo base_url();?>Attendee_login/check_auth_user_login/<?php echo $Subdomain; ?>">
                    <?php if($this->session->flashdata('invlaidlogin_data')){ ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('invlaidlogin_data'); ?> 
                    </div>
                    <?php } ?>
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> There is an error in your submission.
                    </div>
                    <fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="email" class="form-control" id="username" name="username" placeholder="Email">
                                <i class="fa fa-envelope"></i> 
                            </span>
                        </div>
                        <input type="hidden" id="Event_id" name="Event_id" value="<?php echo $event_templates[0]['Id']; ?>">
                        <div class="form-actions sign-up-buttons">
                            <button type="submit" id="login" class="btn btn-success pull-left">
                                Login
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="copyright">
                <?php echo $reserved_right; ?>
            </div>
        </div>
    </div> 
</div>            
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<div class="overlay" style="background:rgba(0, 0, 0, 0.7) none repeat scroll 0 0; height:100%;left:0;position:fixed;top:0;width:100%;z-index: 9999;
">
</div>
<div class="slider-banner">
    <div class="row">
        <?php $image_array = json_decode($event_templates[0]['Images']); if(!empty($image_array)) { ?>
          <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
        <?php } ?>
    </div>
</div>
<?php if($event_templates[0]['Description']=="<br>" || $event_templates[0]['Description']=="") {  } else{ ?>
<div class="event-content panel-white">
    <div class="event_description">
        <p style="padding-left: 25px;">
        <?php echo $event_templates[0]['Description']; ?>
        </p>
    </div>
</div>
<?php } ?>
<div class="event-list" style="display:block;background:<?php echo $event_templates[0]['Background_color']; ?>">
    <div class="row">
        <div class="col-md-12">
            <?php foreach ($menu as $key => $value) {
                $img_view = $value['img_view'];
                $img = $value['img'];
                $url = base_url();
                $bimg = "$url/assets/user_files/$img";
                $dimg = $url."assets/images/EventApp_Default.jpg";
                if(!in_array($value['id'],array(22,23,24,25)))
                {
                    if($value['id']==12)
                    {
                        $url = base_url() .'Messages/'. $acc_name."/".$event_templates[0]['Subdomain'].'/privatemsg';
                    }
                    else if($value['id']==13)
                    {
                        $url = base_url() .'Messages/'.$acc_name."/". $event_templates[0]['Subdomain'].'/publicmsg';
                    }
                    else
                    {
                        $url = base_url().ucfirst($value['pagetitle']).'/'.$acc_name."/".$event_templates[0]['Subdomain'];
                    }
                }
                else
                {
                    $url = base_url() . 'fundraising' . '/'.$acc_name."/". $event_templates[$i]['Subdomain'].'/home/get_auction_pr';
                    $var_target="";
                    if($value['id']==22)
                    {
                      $var_target='target="_blank"';
                      $url=$url.'/1';
                    }
                    else if($value['id']==23)
                    {
                      $var_target='target="_blank"';
                      $url=$url.'/2';
                    }                                        
                    else if($value['id']==24)
                    {
                      $var_target='target="_blank"';
                      $url=$url.'/3';
                    }
                    else if($value['id']==25)
                    {
                      $var_target='target="_blank"';
                      $url=$url.'/4';
                    }
                } ?>
                <div class="col-md-6 col-lg-4 col-sm-6">
                    <a href="<?php echo $url; ?>" <?php echo $var_target; ?>> 
                        <div style="background:url('<?php echo !empty($img) ? $bimg : $dimg; ?>') no-repeat center #ccc; background-size: 100%;" class="<?php echo $img_view=='0' ? 'event-img' : 'event-round-img'; ?>">
                            <div class="event-tile">
                              <h3><?php echo $value['menuname']; ?></h3>
                            </div>
                        </div>
                    </a>
                </div>
            <?php 
            }
            foreach ($cms_feture_menu as $key => $value)
            { 
                $img_view = $value->img_view;
                $img = $value->img;
                $url = base_url();
                $bimg = $url."assets/user_files/".$img;
                $dimg = $url."assets/images/defult-menuimg.png";
                $url = base_url() . $value->menuurl; ?>
                <div class="col-md-6 col-lg-4 col-sm-6">
                    <a href="<?php echo $url; ?>"> 
                        <div style="background:url('<?php echo !empty($img) ? $bimg : $dimg ; ?>') no-repeat center #ccc; background-size: 100%;" class="<?php echo $img_view=='0' ? 'event-img' : 'event-round-img'; ?>">
                            <div class="event-tile">
                                <h3><?php echo $value->menuname; ?></h3>
                            </div>
                        </div>
                    </a>
                </div>    
            <?php } ?>
        </div>
    </div>
</div>             
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script> 
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="<?php echo base_url() ?>assets/js/registration.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
    Main.init();
    Registration.init();
});
</script>
<style type="text/css">
@import "compass/css3";

@import url(https://fonts.googleapis.com/css?family=Merriweather+Sans);
#overlaybox {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    z-index: 1500;
    display: block;
}
.cnt223 a{
    text-decoration: none;
}
.popup{
    width: 75%;
    margin: 0 auto;
    /*position: fixed;*/
    position: absolute;
    z-index: 99999;
}
.form-actions.sign-up-buttons{
    /*float: right;*/
    float:left;
    display: inline-block;
}
.form-actions.sign-up-buttons > a:first-child{
    margin-right:15px;
}
.cnt223{
    min-width: 60%;
    width: 60%;
    min-height: 150px;
    margin: 3% auto;
    background: #f3f3f3;
    position: relative;
    z-index: 103;
    padding: 10px 10px 15px;
    border-radius: 5px;
    box-shadow: 0 2px 5px #000;
}
.cnt223 p{
    clear: both;
    color: #555555;
    text-align: center;
}
.cnt223 p a{
    color: #d91900;
    font-weight: bold;
}
.cnt223 .x{
    float: right;
    height: 35px;
    left: 22px;
    position: relative;
    top: -25px;
    width: 34px;
}
.cnt223 .x:hover{
    cursor: pointer;
}
</style>
<script type='text/javascript'>
$(function(){
    var overlay = $('<div id="overlaybox"></div>');
    overlay.show();
    overlay.appendTo(document.body);
    $('.popup').show();
    $('.overlay').css("background","none");
});
</script>

