<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#notes_list" data-toggle="tab">
                            Edit Notes
                        </a>
                    </li>
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="notes_list">
                         <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Heading <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Heading" id="Heading" name="Heading" value="<?php echo $notes[0]['Heading']; ?>" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Description <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <textarea  id="Description" name="Description" class="summernote"><?php echo $notes[0]['Description']; ?></textarea>                        </div>
                                </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Update <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>               
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
