<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php //if($user->Role_name=='Client'){ ?>
                <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>agenda/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Session</a>
                <?php //} ?>

                <?php if ($this->session->flashdata('agenda_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Agenda <?php echo $this->session->flashdata('agenda_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#agenda_list" data-toggle="tab">
                                Session List
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Setting
                            </a>
                        </li>
                         <?php } ?>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                        
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="agenda_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Heading</th>
                                        <th>Type</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($agenda_list);$i++) { /*echo '<pre>'; print_r($speakers); exit;*/ ?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><a href="<?php echo base_url() ?>agenda/edit/<?php echo $agenda_list[$i]['Event_id'].'/'.$agenda_list[$i]['Id']; ?>"><?php echo $agenda_list[$i]['Heading']; ?></a></td>
                                        <td><?php echo $agenda_list[$i]['Types']; ?></td>
                                        <td><?php echo $agenda_list[$i]['Start_date']; ?></td>
                                       <td><?php 
                                            if($time_format[0]['format_time']=='0')
                                            {
                                                echo date('h:i A',strtotime($agenda_list[$i]['Start_time']));    
                                            }
                                            else
                                            {
                                                echo date('H:i',strtotime($agenda_list[$i]['Start_time']));
                                            }
                                            
                                         ?></td>
                                        <td><?php
                                            if($time_format[0]['format_time']=='0')
                                            {
                                                echo date('h:i A',strtotime($agenda_list[$i]['End_time'])); 
                                            }
                                            else
                                            {
                                                echo date('H:i',strtotime($agenda_list[$i]['End_time']));
                                            }
                                            ?></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($agenda_list[$i]['Agenda_status']=='1') {  ?> 
                                                      label-success 
                                                <?php  }  else  { ?> 
                                                     label-danger 
                                                <?php } ?>">
                                                <?php if($agenda_list[$i]['Agenda_status']=='1')
                                                    { 
                                                    echo "Active"; 
                                                    
                                                    }
                                                    else 
                                                    { 
                                                        echo "Inactive";
                                                    } 
                                                ?>
                                            </span>
                                        </td>
                                       <td>
                                            <?php //if($user->Role_name=='Client'){ ?>
                                            <a href="<?php echo base_url() ?>agenda/edit/<?php echo $agenda_list[$i]['Event_id'].'/'.$agenda_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_agenda(<?php echo $agenda_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php //} ?>

                                            <?php if($user->Role_name=='User'){ ?>
                                            <a href="<?php echo base_url() ?>agenda/edit/<?php echo $agenda_list[$i]['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_agenda(<?php echo $agenda_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                   <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                        <!-- <div class="demo-controls" style="width:30%;">
                        <div class="device-list" id="size-select">
                            <a class="device-list phone current" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Phone']); return false;">Phone</a>
                            <a class="device-list tablet" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Tablet']); return false;">Tablet</a>
                            <a class="device-list laptop" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Laptop']); return false;">Laptop</a>
                        </div>
                        </div> -->
        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    function delete_agenda(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>agenda/delete/"+<?php echo $test; ?>+"/"+id
        }
    }
</script>
<!-- end: PAGE CONTENT-->