<?php  $arr=$this->session->userdata('current_user');  $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#import_attendees_div" data-toggle="tab">
                                Import Agenda
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <?php if ($this->session->flashdata('csv_flash_message')) { ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('csv_flash_message'); ?> 
                        </div>
                    <?php } ?>
                    <div class="tab-pane fade active in" id="import_attendees_div">
                        <form role="form" method="post" class="form-horizontal" id="form2" action="<?php echo  base_url().'Agenda_admin/save_import_agenda/'.$this->uri->segment(3);?>" enctype="multipart/form-data">
                            <h4>Add CSV or Json URL to import agenda data</h4>
                            <!-- <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Your Access Key <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-4">
                                    <input type="text" placeholder="Your Access Key" class="form-control required" id="access_key" name="access_key">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-2">
                                    File Type
                                </label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="file-type" name="file-type">
                                        <option>CSV</option>
                                        <option>JSON</option>
                                    </select>
                                </div>
                            </div>
                            <div class="csv_sec">
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Short instructions
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-4" style="padding:0px;">
                                            <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Agenda_admin/download_agenda_template_csv/<?php echo $this->uri->segment(3); ?>">Download CSV</a>
                                            <br/><br/><small>Download Example CSV of multiple insert agenda</small>
                                        </div>
                                        <div class="col-sm-4" style="padding:0px;">
                                            <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" target="_blank" href="<?php echo base_url(); ?>Agenda_admin/show_import_agenda_json/<?php echo $this->uri->segment(3); ?>">JSON Schema</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Your CSV File URL <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-8" style="padding:0px;">
                                            <input type="text" placeholder="Your CSV File URL" class="form-control required" id="csv_url" name="csv_url">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="json_sec" style="display: none;">
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Short instructions
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-4" style="padding:0px;">
                                            <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Agenda_admin/download_import_agenda_json/<?php echo $this->uri->segment(3); ?>">Download JSON</a>
                                            <br/><br/><small>Download Example JSON of multiple insert agenda</small>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Your JSON File URL <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-8" style="padding:0px;">
                                            <input type="text" placeholder="Your JSON File URL" class="form-control" id="json_url" name="json_url">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-3">
                                    <button class="btn btn-theme_green btn-block" type="submit">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#file-type').change(function()
    {
        if($('#file-type').val() == 'CSV')
        {
            $('.csv_sec').show();            
            $('.json_sec').hide();
            $('#json_url').removeClass('required');
            $('#csv_url').addClass('required')            

        }
        else
        {
            $('.csv_sec').hide();            
            $('.json_sec').show();
            $('#csv_url').removeClass('required');            
            $('#json_url').addClass('required');
        }
    });
</script>