<?php  
$arr=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#indiviual_attendee" data-toggle="tab">
                                CSV Attendees
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="alert alert-info"><?php echo $nomatch.' Column Are Not Match'; ?></div>
                    <div class="tab-pane fade active in" id="indiviual_attendee">
                        <form method="post" class="form-horizontal" action="<?php echo base_url().'Agenda_admin/upload_import_agenda/'.$this->uri->segment(3); ?>" id="attende_form">
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1" style="font-weight: bold;">
                               CMS Columns
                            </label>
                            <label class="col-sm-9" style="font-weight: bold;">
                               CSV Columns
                            </label>
                        </div>
                        <?php $fixedc=array('Start_date','Start_time','End_date','End_time','Heading','Types','description','session_image','custom_location','custom_speaker_name','session_code','agenda_name');
                        foreach ($fixedc as $key1 => $value1) { ?>
                        <div class="form-group">
                            <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">
                                <?php
                                if($value1 == "Start_date" || $value1 == "Start_time" || $value1 == "End_date" || $value1 == "End_time" || $value1 == "Heading" || $value1 == "Types" || $value1 == "session_code" || $value1 == "agenda_name")
                                {
                                    echo $value1; ?> <span class="symbol required"></span>
                                <?php 
                                }else
                                {
                                    echo $value1;
                                }
                                ?>
                            </label>
                            <select class="col-sm-9" name="<?php echo $value1; ?>" id="<?php echo $value1; ?>">
                                <?php foreach ($keysdata as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>" <?php if($value1==$value){ ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php  } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                            </label>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-theme_green btn-block">
                                    Submit
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>