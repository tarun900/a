<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <a style="top: 7px;right: 135px;" class="btn btn-primary list_page_btn" href="<?php echo base_url().'Agenda_admin/add_group/'.$this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Group</a>
                <?php if($lock_agenda!='1' || $event['End_date'] >= date('Y-m-d')){ ?>
               <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url().'Agenda_admin/agenda_index/'.$this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Agenda</a>
                <?php } if ($this->session->flashdata('category_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Agenda <?php echo $this->session->flashdata('category_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('category_error')) { ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('category_error'); ?>
                    </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#view_group" data-toggle="tab">
                                Groups
                            </a>
                        </li>
                        <li class="">
                            <a href="#agenda_list" data-toggle="tab">
                                Agenda List
                            </a>
                        </li>
                        <?php if($lock_agenda!='1' || $event['End_date'] >= date('Y-m-d')){ ?>
                        <li>
                            <a href="#duplicate_session" data-toggle="tab">
                                Duplicate Agenda    
                            </a>
                        </li>
                        <?php } ?>
                        <li class="">
                            <a href="#pending_agenda" data-toggle="tab">
                                Pending Agenda
                            </a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#show_hide_dropdown_link_div" data-close-others="true">
                               <span id="show_hide_textshow"> Show/Hide </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="show_hide_dropdown_link_div" class="dropdown-menu dropdown-info"> 
                                <li class="">
                                    <a href="#show_listing_by_time" data-toggle="tab">
                                        Show/Hide Date 
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#show_column" data-toggle="tab">
                                        Show/Hide Column 
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#show_all" data-toggle="tab">
                                        Show/Hide All Agenda
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#agenda_sort" data-toggle="tab">
                                        Agenda Sort
                                    </a>
                                </li>
                            </ul>
                        </li>
						<?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						<li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
						
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="view_group">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_21">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($module_group_list as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1; ?></td>
                                        <td><?=ucfirst($value['group_name']); ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Agenda_admin/edit_group/<?php echo $this->uri->segment(3).'/'.$value['module_group_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url().'Agenda_admin/delete_group/'.$this->uri->segment(3).'/'.$value['module_group_id']; ?>" onclick="return confirm('Are You Sure Delete This Group');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="agenda_list">
                        <div class="row">
                            <div class="col-md-12 space20">
                                <div class="btn-group pull-right">
                                 <a href="<?=base_url().'Agenda_admin/import_agenda/'.$this->uri->segment(3)?>" class="btn btn-green" style="margin-right: 5px;">
                                    Import Agenda
                                    </a>
                                    <button data-toggle="dropdown" class="btn btn-green dropdown-toggle">
                                        Export Session Data <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-light pull-right">
                                        <li>
                                            <a href="<?php echo base_url().'Agenda_admin/export_agenda_rating/'.$this->uri->segment(3); ?>">
                                                Export Session Rating
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url().'Agenda_admin/export_agenda_comment/'.$this->uri->segment(3); ?>">
                                                Export Session Comment
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="agenda_category_list" class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_11">
                                <thead>
                                    <tr>
                                        <th>Agenda Name</th>
                                        <th>Number of Sessions</th>
                                        <th>Hide Check In Time</th>
                                        <th>Edit or Delete</th>
                                        <th>Make It Primary</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($category_list);$i++) {
                                    if(!empty($category_list[$i]['cid'])){ ?>
                                    <tr>
                                        <td><?php echo ucfirst($category_list[$i]['category_name']); ?></td>
                                        <td><?php echo $category_list[$i]['total_agenda']; ?></td>
                                        <td>
                                        <input type="checkbox" <?php if($category_list[$i]['show_check_in_time']=='1'){ ?> checked="checked" <?php } ?> value="<?php echo $category_list[$i]['cid']; ?>" name="show_checkin_time" onchange='showcheckintime("<?php echo $category_list[$i]['cid']; ?>");' id="show_checkin_time_<?php echo $category_list[$i]['cid']; ?>" class="">
                                        <label for="show_checkin_time_<?php echo $category_list[$i]['cid']; ?>"></label>
                                        </td>
                                        <td>
                                            <?php if($lock_agenda!='1' || $event['End_date'] >= date('Y-m-d')){ ?>
                                            <a href="<?php echo base_url().'Agenda_admin/agenda_index/'.$this->uri->segment(3).'/'.$category_list[$i]['cid']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <?php } ?>
                                            <a href="javascript:;" onclick="delete_category(<?php echo $category_list[$i]['cid']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url().'Agenda_admin/make_it_primary_agenda_categories/'.$this->uri->segment(3).'/'.$category_list[$i]['cid']; ?>" class='<?php if($category_list[$i]['categorie_type']=="1"){ echo "btn btn-success btn-block"; }else{ echo "btn btn-green btn-block"; } ?>'> Make It Primary </a>
                                        </td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="duplicate_session">
                        <div class="panel-body">
                            <form role="form" method="post" class="form-horizontal" id="duplicate_session_form" action="<?php echo base_url().'Agenda_admin/duplicate_session/'.$this->uri->segment(3); ?>">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Title <span class="symbol required"></span>
                                        </label>
                                        <input type="text" placeholder="Agenda Title" class="form-control required" id="session_Title" name="session_Title">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Select Agenda <span class="symbol required"></span>
                                        </label>
                                        <select name="session_id" id="session_id" class="form-control required">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                            <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button class="btn btn-green btn-block" type="submit">Duplicate <i class="fa fa-arrow-circle-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>        
                    </div>
                    <div class="tab-pane fade" id="show_listing_by_time">
                        <form method="post" action="<?php echo base_url().'Agenda_admin/saveshowoption/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <label class="col-sm-1" for="form-field-select-1">
                                    Hide Date
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['show_session_by_time']=='1'){ ?> checked="checked" <?php } ?> name="show_session_by_time" id="show_session_by_time" class="" value="1"> 
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;">
                                <button name="show_btn" class="btn btn-yellow btn-block" value="show_submit" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="pending_agenda">
                        <form method="post" action="<?php echo base_url().'Agenda_admin/saveshowpendingoption/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <label class="col-sm-2" for="form-field-select-4">
                                    Turn on Pending Agenda
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['on_pending_agenda']=='1'){ ?> checked="checked" <?php } ?> name="on_pending_agenda" id="on_pending_agenda" class="" value="1"> 
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;">
                                <button name="show_pending_agenda_btn" class="btn btn-yellow btn-block" value="show_submit" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="show_column">
                        <form method="post" action="<?php echo base_url().'Agenda_admin/save_show_agenda_column_option/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <label class="col-md-2" for="form-field-select-4">
                                    Show Place Left Column
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['show_agenda_place_left_column']=='1'){ ?> checked="checked" <?php } ?> name="show_agenda_place_left_column" id="show_agenda_place_left_column" class="" value="1"> 
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2" for="form-field-select-4">
                                    Show Speaker Column
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['show_agenda_speaker_column']=='1'){ ?> checked="checked" <?php } ?> name="show_agenda_speaker_column" id="show_agenda_speaker_column" class="" value="1"> 
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2" for="form-field-select-4">
                                    Show Location Column
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['show_agenda_location_column']=='1'){ ?> checked="checked" <?php } ?> name="show_agenda_location_column" id="show_agenda_location_column" class="" value="1"> 
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;">
                                <button name="show_btn" class="btn btn-yellow btn-block" value="show_column_submit" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="show_all">
                        <form method="post" action="<?php echo base_url().'Agenda_admin/saveshowallagendaoption/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <label class="col-sm-2" for="form-field-select-4">
                                    Show All Agenda To All User
                                </label>
                                <div class="col-md-1">
                                    <input type="checkbox" <?php if($event['allow_show_all_agenda']=='1'){ ?> checked="checked" <?php } ?> name="allow_show_all_agenda" id="allow_show_all_agenda" class="" value="1"> 
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;">
                                <button name="show_pending_agenda_btn" class="btn btn-yellow btn-block" value="show_submit" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="agenda_sort">
                        <form method="post" action="<?php echo base_url().'Agenda_admin/saveagendasort/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-12">
                                        <label class="radio-inline hover">
                                            <input type="radio" class="purple" value="1" <?php if ($event['agenda_sort'] == '1') echo ' checked="checked"'; ?> name="agenda_sort">
                                            Time Sort
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0"  <?php if ($event['agenda_sort'] == '0') echo ' checked="checked"'; ?> name="agenda_sort">
                                            Order Sort
                                        </label>    
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;">
                                <button name="show_pending_agenda_btn" class="btn btn-yellow btn-block" value="show_submit" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>


					<div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTab2 > li > ul > li > a').click(function(){
        jQuery(this).parent().parent().parent().first('a').find('span').html($.trim(jQuery(this).text()));
    });
});
</script>
<script type="text/javascript">
    $(window).load(function(){
        $("#sample_21,#sample_11").DataTable({
             bStateSave: true,
            bDestroy: true,
            fnStateSave: function(settings,data) {
                  localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
                },
            fnStateLoad: function(settings) {
                return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
                }
        })

        $("#duplicate_session_form").validate({
            errorElement:"span",
            errorClass:"help-block",
            rules: {
                session_Title:{
                    required:true
                },
                session_id:{
                    required:true
                }
            },
            highlight: function (element) 
            {
              $(element).closest('.help-block').removeClass('valid');
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error');
            }
        });
    });
    function delete_category(id)
    {
        if(confirm("Are you sure to delete this?"))
        {
            //window.location.href="<?php echo base_url().'Agenda_admin/delete_agenda_category/'.$this->uri->segment(3).'/'; ?>"+id;
            $.ajax({
                url:"<?php echo base_url().'Agenda_admin/delete_agenda_category/'.$this->uri->segment(3).'/'; ?>"+id,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
    function showcheckintime(aid)
    {
        $.ajax({
        url:"<?php echo base_url(); ?>Agenda_admin/show_check_in_time/<?php echo $this->uri->segment(3); ?>/"+aid,
        success:function(result)
        {
          if($.trim(result)>0)
          {
            var shortCutFunction ='success';
            if($("#show_checkin_time_"+aid).is(':checked'))
            {
              var title = 'Add Successfully';
              var msg = 'Check In Time Hide Successfully';
            }
            else
            {
              var title = 'Remove Successfully';
              var msg = 'Check In Time Show Successfully'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
          }
        }
      });
    }
</script>