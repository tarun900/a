<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Session</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>

            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                 
					<div class="col-sm-12">
						<h2>Basic Information</h2>
						<div class="col-sm-6" style="margin-top:20px;">
							<div class="form-group">
								 <label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									Start Date <span class="symbol required" id="showdatetime_remark"></span>
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
										<input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
										<input type="text" name="Start_date" id="Start_date" contenteditable="false" value="<?php echo date('m/d/Y',strtotime($agenda_list[0]['Start_date'])); ?>" class="form-control">
										<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
									</div>
								</div>
								<span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
							</div>
							
							<div class="form-group">
								 <label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									Start Time <span class="symbol required" id="showdatetime_remark"></span>
								</label>
								<div class="col-sm-8">
									<div class="input-group input-append bootstrap-timepicker" contenteditable="false">
										<input type="text" id="Start_time" name="Start_time" class="form-control time-picker" value="<?php echo $agenda_list[0]['Start_time']; ?>">
										<span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
									</div>
								</div>
								<span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
							</div>

							<div class="form-group">
								 <label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									End Date <span class="symbol required" id="showdatetime_remark"></span>
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
										<input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
										<input type="text" name="End_date" id="End_date" contenteditable="false" value="<?php echo date('m/d/Y',strtotime($agenda_list[0]['End_date'])); ?>" class="form-control">
										<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
									</div>
								</div>
								<span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
							</div>
							
							<div class="form-group">
								 <label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									End Time <span class="symbol required" id="showdatetime_remark"></span>
								</label>
								<div class="col-sm-8">
									<div class="input-group input-append bootstrap-timepicker" contenteditable="false">
										<input type="text" id="End_time" name="End_time" class="form-control time-picker" value="<?php echo $agenda_list[0]['End_time']; ?>">
										<span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
									</div>
								</div>
								<span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									Name<span class="symbol required"></span>
								</label>
								<div class="col-sm-8">
									<input type="text" placeholder="Name" id="Heading" value="<?php echo $agenda_list[0]['Heading'] ?>" name="Heading" class="form-control name_group required">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									Session Type <span class="symbol required"></span>
								</label>
								<div class="col-md-8" id="select_box_type">
									<select id="cmd_select_box_type" onchange="openpopforsessiontype();" class="form-control" name="Types">
										<option value=''>Select Type</option>
										<!-- <?php $arr1=array("Select Type","Break","Networking","Main Session","Breakout Session","Focus Group","Other");
										$type_data_arr=array_column($type_data,'types_name');
										if(count($type_data_arr)>0)
										{
											$arr=array_merge($arr1,$type_data_arr);	
										}
										if(!in_array($agenda_list[0]['Types'],$arr)){ ?>
										<option value="<?php echo $agenda_list[0]['Types']; ?>" selected><?php echo $agenda_list[0]['Types']; ?></option>
										<?php } ?>
										<option value="Introduction" <?php if($agenda_list[0]['Types'] == 'Introduction') { ?> selected <?php } ?>>Introduction</option>
										<option value="Break" <?php if($agenda_list[0]['Types'] == 'Break') { ?> selected <?php } ?>>Break</option>
										<option value="Networking" <?php if($agenda_list[0]['Types'] == 'Networking') { ?> selected <?php } ?>>Networking</option>
										<option value="Main Session" <?php if($agenda_list[0]['Types'] == 'Main Session') { ?> selected <?php } ?>>Main Session</option>
										<option value="Breakout Session" <?php if($agenda_list[0]['Types'] == 'Breakout Session') { ?> selected <?php } ?>>Breakout Session</option>
										<option value="Focus Group" <?php if($agenda_list[0]['Types'] == 'Focus Group') { ?> selected <?php } ?>>Focus Group</option> -->
										<?php foreach ($type_data as $key => $value) { ?>
											<option value="<?php echo $value['type_id']; ?>" <?php if($agenda_list[0]['Types'] == $value['type_id']) { ?> selected <?php } ?>><?php echo $value['type_name']; ?></option>
													<?php } ?>
										<!-- <option value="Other" <?php if($agenda_list[0]['Types'] == 'Other') { ?> selected <?php } ?>>Other</option> -->
										<option style="background-color: #ccc !important;" value="New">Add a New Session Type</option>
									</select>
									 <?php if($agenda_list[0]['Types'] == 'Other') { ?>
									 <input type="text" value="<?php echo $agenda_list[0]['other_types']; ?>" placeholder="Session Type" id="other_types" name="other_types" class="form-control name_group" style="display:block;">
									 <?php } else {  ?>
									 <input type="text" placeholder="Session Type" id="other_types" name="other_types" class="form-control name_group" style="display:none;">
									 <?php } ?>
								 </div>
							</div> 
							
							<div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-left: 0%;" for="form-field-1">
									Sort Order <!-- <span class="symbol required"></span> -->
								</label>
								<div class="col-md-8" id="sort_order">

									<select id="sort_order" class="form-control" name="sort_order">
										<option value="">Select Sort Order</option>
										<?php 
										foreach ($sort_order as $key => $value)
										{
											if($value == $agenda_list[0]['sort_order'])
                                            { 
                                                $selected = "selected='selected'";
                                            }
                                            else
                                            {
                                                $selected = "";
                                            }
										?>
										    <option <?php echo $selected; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
								        <?php } ?>
									</select>
								 </div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3" style="padding-left: 0%;" for="form-field-1">Session Status</label>
								<div class="col-sm-8">
									<label class="radio-inline">
										<input type="radio" class="purple" value="1" <?php if ($agenda_list[0]['Agenda_status'] == '1') echo ' checked="checked"'; ?> name="Agenda_status">
										Active
									</label>
									<label class="radio-inline">
										<input type="radio" class="purple" value="0" <?php if ($agenda_list[0]['Agenda_status'] == '0') echo ' checked="checked"'; ?> name="Agenda_status">
										Inactive
									</label>    
								</div>
							</div>
							
							
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-left: 3%;" for="form-field-1">
									Description 
								</label>
								<div class="col-md-12">
								   <textarea class="summernote form-control" name="description"><?php echo $agenda_list[0]['description']; ?></textarea>
								</div>
							</div>
						</div>
					</div>

                   <span style="border:3px solid #ccc;width:96%;clear: both;display:block;margin: 0 auto;"></span>

                    <div class="col-sm-12" style="clear:both;">
						<div class="col-sm-6" style="padding-left: 0px;">
							<h3>Link your Session</h3>
							<span>Link other content from your App to show in this session</span>
							
							<div class="form-group" style="margin-top:20px;">
								<label class="col-sm-3" for="form-field-select-4">
									Select Map 
								</label>
								<div class="col-md-8">
									<select id="Address_map" class="form-control" name="Address_map">
										<?php 
											echo"<option value=''>Select Location</option>";
											foreach ($map_list as $k => $v) { ?>
											<option value="<?php echo $v['Id']; ?>" <?php if($agenda_list[0]['Address_map'] == $v['Id']) { ?> selected <?php } ?>><?php echo $v["Map_title"]; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3" for="form-field-select-4">
									Select Speakers
								</label>
								<div class="col-md-8">
									<select id="Speaker_id" onClick="getspeakers();" class="select2-container select2-container-multi form-control search-select menu-section-select old_speakers" name="Speaker_id[]" multiple="true">
										<?php 
											$agenda_speaker=explode(',',$agenda_list[0]['Speaker_id']);
											foreach ($speakers as $key => $value) 
											{ 
											?>
												<option value="<?php echo $value['uid']; ?>" <?php if(in_array($value['uid'], $agenda_speaker)) { ?> selected <?php } ?>><?php echo $value["Firstname"].' '.$value["Lastname"]; ?></option>
											<?php
											}
										?>
									</select>
									<a href="javascript:void(0);" onclick="openpop();">Add New Speaker</a>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3" for="form-field-select-4">
								   Select Q&A Session
								</label>
								<div class="col-md-8">
									<select id="qasession_id" class="form-control new_pre"class="form-control" name="qasession_id">
										<?php echo"<option value=''>Select Q&A Session</option>";
											foreach ($qasession_list as $key => $values) { ?>
											<option value="<?php echo $values['Id']; ?>" <?php if($agenda_list[0]['qasession_id'] == $values['Id']) { ?> selected <?php } ?>><?php echo $values["Session_name"]; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3" for="form-field-select-4">
								   Select Slides
								</label>
								<div class="col-md-8">
									<select id="form-field-select-4" onchange="newpre();" class="form-control new_pre"class="form-control" name="presentation_id">
										<?php 
											echo"<option value=''>Select Slides</option>";
											foreach ($presentation_list as $k => $v) { ?>
											<option value="<?php echo $v['Id']; ?>" <?php if($agenda_list[0]['presentation_id'] == $v['Id']) { ?> selected <?php } ?>><?php echo $v["Heading"]; ?></option>
										<?php } 
										echo"<option style='background-color: #ccc !important;' value='New'>Add New Presentation</option>";

										?>

									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3" for="form-field-select-4">
									Select Surveys
								</label>
								<div class="col-md-8">
									<select id="form-field-select-4" class="form-control" name="survey_id">
										<option value=''>Select Surveys</option>
										<?php 
										foreach ($surveys_list as $key => $value) 
										{ ?>
											<option value="<?php echo $value['survey_id']; ?>" <?php if($agenda_list[0]['survey_id']==$value['survey_id']){ ?> selected="selected" <?php } ?>><?php echo $value["survey_name"]; ?></option>
										<?php }
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3" for="form-field-select-4">
								   Select Document
								</label>
								<div class="col-md-8">
									<select id="form-field-select-4" onchange="newdoc();" class="form-control new_doc" name="doc_id">
										<?php 
											echo"<option value=''>Select Document</option>";
											foreach ($doc_list as $k => $v) { ?>
											<option value="<?php echo $v['id']; ?>" <?php if($agenda_list[0]['document_id'] == $v['id']) { ?> selected <?php } ?>><?php echo $v["title"]; ?></option>
										<?php } 
										echo"<option style='background-color: #ccc !important;' value='New'>Add New Documents</option>";
										?>
									</select>
								</div>
							</div>
							
							<h3>If you are not adding Speaker Profiles or Maps in your App you can enter this information below manually</h3>
							
							<div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Speaker Name
								</label>
							  <div class="col-sm-8">
								 <input id="custom_speaker_name" name="custom_speaker_name" value="<?php echo $agenda_list[0]['custom_speaker_name']; ?>" class="form-control" type="text" placeholder="Enter Speaker Name"/>
							  </div>
						   </div>
						   
						    <div class="form-group">
								<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Location
								</label>
							  <div class="col-sm-8">
								 <input id="custom_location" name="custom_location" value="<?php echo $agenda_list[0]['custom_location']; ?>" class="form-control" type="text" placeholder="Enter Location"/>
							  </div>
						    </div>
						    <h3>Session Image</h3>
							<div class="form-group" style="margin-top:20px;">
                    			<div class="col-sm-12">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                       <div class="fileupload-new thumbnail">
                                       	<?php if(!empty($agenda_list[0]['session_image'])){ ?>
                                       		<img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $agenda_list[0]['session_image']; ?>">
                                       	<?php } ?>
                                       </div>
                                       <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                       <?php if(!empty($agenda_list[0]['session_image'])){ ?>
                                       <a href="#" class="btn fileupload-new btn-red" data-dismiss="fileupload" onclick="delete_session_images();">
                                            <i class="fa fa-times"></i> Delete
                                        </a>
                                       <?php } ?>
                                       <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                 <input type="file" name="session_images" id="session_images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_h_image_data">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                       </div>
                                    </div>              
                                </div>
                    		</div>
						</div>
						
						<div class="col-sm-6">
							<h3>Attendee and Booking Management</h3>
							<span>Limit places and ask for feedback</span><br/><br/>
							
							<div class="form-group" style="margin-top:20px;">
								<label class="col-sm-4" for="form-field-select-4">
									Maximum People
								</label>
								<div class="col-md-7">
									<input type="text" name="Maximum_People" id="Maximum_People" class="form-control name_group" value="<?php echo $agenda_list[0]['Maximum_People']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Show number of places remaining
								</label>
								<div class="col-md-1">
									<input type="checkbox" <?php if($agenda_list[0]['show_places_remaining']=='1'){ ?> checked="checked" <?php } ?> name="show_places_remaining" id="show_places_remaining" class="" value="1"> 
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Activate Checking In
								</label>
								<div class="col-md-1">
									<input type="checkbox" <?php if($agenda_list[0]['show_checking_in']=='1'){ ?> checked="checked" <?php } ?> name="show_checking_in" id="show_checking_in" class="" value="1"> 
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Time to allow Check In
								</label>
								<div class="col-sm-7" style="padding-left:18px;">
									<input type="text" name="checking_datetime" id="checking_datetime" contenteditable="false" value="<?php echo $agenda_list[0]['checking_datetime']; ?>" class="form-control datetimepicker">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Activate Session Ratings
								</label>
								<div class="col-md-1">
									<input type="checkbox" <?php if($agenda_list[0]['show_rating']=='1'){ ?> checked="checked" <?php } ?> name="show_rating" id="show_rating" class="" value="1"> 
								</div>
							</div>
							
						   <div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Allow Users to save another session which clashes with this session
								</label>
								<div class="col-md-1">
									<input type="checkbox" <?php if($agenda_list[0]['allow_clashing']=='1'){ ?> checked="checked" <?php } ?> name="allow_clashing" id="allow_clashing" class="" value="1"> 
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Allow Users to Comment on Sessions
								</label>
								<div class="col-md-1">
									<input type="checkbox" <?php if($agenda_list[0]['allow_comments']=='1'){ ?> checked="checked" <?php } ?> name="allow_comments" id="allow_comments" class="" value="1"> 
								</div>
							</div>				
						</div>
						<div class="col-sm-6">
							<h3>Notification</h3>
							<div class="form-group">
								<label class="col-sm-10" for="form-field-select-4">
									Send Notification to Attendees who have saved this session.
								</label>
								<div class="col-md-1">
									<input type="checkbox" name="allow_notification" id="allow_notification" class="" value="1" onchange="makefieldmandotry(this);" <?php if(!empty($agenda_list[0]['notify_id'])){ ?> checked="checked" <?php } ?>> 
								</div>
							</div>
							<div class="form-group" style="display: none;">
								<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Notification Title <span class="symbol required"></span>
								</label>
								<div class="col-sm-8">
									<input maxlength="20" id="notification_title" name="notification_title" value="<?php echo $agenda_list[0]['title'];  ?>" class="form-control limited" type="text" placeholder="Enter Notification Title"/>
								</div>
						   </div>
						   <div class="form-group" style="display: none;">
								<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Notification Message <span class="symbol required"></span>
								</label>
								<div class="col-sm-8">
									<textarea maxlength="60" id="notification_msg" name="notification_msg" value="" class="form-control limited" type="text" placeholder="Enter Notification Message"><?php echo $agenda_list[0]['messages']; ?></textarea>
								</div>
						   </div>
							<div class="form-group" style="display: none;">
								 <label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
									Time to send notification <span class="symbol required"></span>
								</label>
								<div class="col-sm-8">
									<input type="text" name="notify_datetime" value="<?php echo $agenda_list[0]['notify_datetime']; ?>" id="notify_datetime" contenteditable="false" class="form-control datetimepicker">
								</div>
							</div>
						</div>
					</div>
                    <div class="form-group" style="clear:both;">
                        <div class="col-md-4" style=" margin-left: 1%;">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<div id="full_popup1">
    <div id="blanket1" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv1" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv1&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a><form>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Session Type</span>
                </label>
                <input type="text" placeholder="Session Type" class="form-control required" id="Session_type_testbox" name="Session_type_testbox">
                <span id="Session_type_testbox_error" style="color:red;display:none;" for="Session_type_testbox" class="help-block">This field is required.</span>
            </div>
            <div class="col-md-3">
                <button id="session_type_btn" class="btn btn-green btn-block" type="button">Add</button>
            </div>
        </div>
    </div>
    </div>
</div> 
<div id="full_popup">
    <div id="blanket" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a>
        <form role="form" method="post" class="ajax-form" id="form2">
        <div class="row">
            <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
            <div class="errorHandler alert alert-success no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> Updated Successfully.
            </div>
            <?php } ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">First Name </span>
                    </label>
                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name </span>
                    </label>
                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
                </div>
                <div class="form-group">
                    <label class="control-label">Email </span>
                    </label>
                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
                </div>
                <div class="form-group">
                        <label class="control-label">Password </label>
                        <input type="password" placeholder="Password" class="form-control required" id="password" name="password">
                </div>
                <div class="col-md-3">
                        <button id="comment" class="btn btn-green btn-block" type="button">Add <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>                     
        </div>
        </form>
    </div> 
</div>


<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
.close_popup
{
    position: absolute;
    top: -10px;
    right: -10px;
}
#popUpDiv {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}

#blanket1 {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}
#popUpDiv1 {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}
#popUpDiv1 .form-group label{color:#333;}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}

.fileupload-new.thumbnail img
{
 height: 140px !important;
}

</style>
<script type="text/javascript">
function delete_session_images() 
{
	if(confirm('Are You Sure Delete This Session Image'))
	{
		window.location.href="<?php echo base_url().'agenda_admin/delete_session_image/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>";
	}
} 
</script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">

$('#comment').click(function() {
    var form_data = {
        Firstname : $('#Firstname').val(),
        Lastname : $('#Lastname').val(),
        Email : $('#email').val(),
        Password : $('#password').val()
    };

    if($('#Firstname').val() != '' && $('#Lastname').val() && $('#email').val() != '' && $('#password').val() != '')
    {
        $.ajax({
            url: "<?php echo base_url().'speaker/add_speakers/'.$this->uri->segment(3); ?>",
            type: 'POST',
            data: form_data,
            success: function(data){
                    $('#full_popup').css('display','none');
                    $('#Speaker_id').html(data); 
                    $('#Speaker_id').select2({tokenSeparators: [',']});
                    $('#Speaker_id').trigger("change"); 
                    $('#Firstname').val('');
                    $('#Lastname').val('');
                    $('#email').val('');
                    $('#password').val('');
               }
        });
    }
});

function openpopforsessiontype()
{
  if($("#cmd_select_box_type").val()=='New')
  {
     popup('popUpDiv1');
  }
}
$('#session_type_btn').click(function(){
    var sessval=$.trim($('#Session_type_testbox').val());
    if(sessval!=""){
    	$.ajax({
    		url:"<?php echo base_url().'agenda_admin/ajax_save_new_types/'.$this->uri->segment(3); ?>",
    		type:'post',
    		data:"type_name="+sessval,
    		success:function(data){
        		var result=data.split('###');
    			if($.trim(result[0])=="success")
    			{
    				$('#cmd_select_box_type').append("<option selected='selected' value='"+result[1]+"'>"+sessval+"</option>");
    				popup('popUpDiv1');
        		}
        		else
        		{
        			$('#Session_type_testbox_error').html(result[1]);
        			$('#Session_type_testbox_error').show();
        		}
    		}
    	});
    }
    else
    {
        $('#Session_type_testbox_error').html("This field is required.");
        $('#Session_type_testbox_error').show();
    }       
});
function getspeakers()
{
	var textselecte="";
	$(".old_speakers :selected").each(function (i,sel) {
		if($.trim($(sel).val())!="" && $.trim($(sel).val())!='New'){
			if(textselecte=="")
			{
				textselecte=$(sel).text();
			}
			else{
				textselecte=textselecte+', '+$(sel).text();
			}
		}
 	});
 	$('#custom_speaker_name').val(textselecte);
}
function openpop()
{
    popup('popUpDiv');
}
$('#Address_map').change(function(){
	if($(this).val()!=""){
		$('#custom_location').val($("#Address_map option:selected").text());
	}
});
function newdoc()
{
    if($(".new_doc").val()=='New')
    {
        var url="<?php echo base_url();?>document/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}
function newpre()
{
    if($(".new_pre").val()=='New')
    {
        var url="<?php echo base_url();?>presentation/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}
function makefieldmandotry(elem)
{
	if($(elem).prop("checked") == true)
	{
		$('#notification_title').parent().parent().show();
		$('#notification_title').addClass('required');
		$('#notification_msg').parent().parent().show();
		$('#notification_msg').addClass('required');
		$('#notify_datetime').parent().parent().show();
		$('#notify_datetime').addClass('required');
	}
	else
	{
		$('#notification_title').parent().parent().hide();
		$('#notification_title').removeClass('required');
		$('#notification_title').parent().parent().removeClass('has-error');
		$('#notification_title').parent().find('.help-block').hide();
		$('#notification_title').val('');

		$('#notification_msg').parent().parent().hide();
		$('#notification_msg').removeClass('required');
		$('#notification_msg').parent().parent().removeClass('has-error');
		$('#notification_msg').parent().find('.help-block').hide();
		$('#notification_msg').val('');

		$('#notify_datetime').parent().parent().hide();
		$('#notify_datetime').removeClass('required');
		$('#notify_datetime').parent().parent().removeClass('has-error');
		$('#notify_datetime').parent().find('.help-block').hide();
		$('#notify_datetime').val('');
	}
}

</script>

<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
            //format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
            //format: 'yyyy-mm-dd',
            autoclose: true
            });
             $("#cmd_select_box_type").change(function()
            {
                if($(this).val()=="Other")
                {
                     $('#other_types').css('display','block');
                }
                else
                {
                     $('#other_types').css('display','none');
                }
          });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo date('m/d/Y',strtotime($event['Start_date'])); ?>', 
            endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
            //format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
            //format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function() {
    	makefieldmandotry($('#allow_notification'));
        $('.datetimepicker').datetimepicker({
            formatTime:'H:i',
            formatDate:'m/d/Y',
            step:5
        });
    });
</script>
<style type="text/css">
.old_speakers{
	height: auto;
	margin-bottom: 10px;
}
</style>
<!-- end: PAGE CONTENT-->