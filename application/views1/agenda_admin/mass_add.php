<!-- start: PAGE CONTENT -->   
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add Multiple <span class="text-bold">Sessions</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('session_error_data')){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('session_error_data'); ?> 
                </div>
                <?php } ?>
                <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Agenda_admin/upload_csv_session/'.$this->uri->segment(3).'/'.$this->uri->segment(4);  ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Short instructions
                        </label>
                        <div class="col-sm-9">
                            <div class="col-sm-4" style="padding:0px;">
                                <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Agenda_admin/download_template_csv/<?php echo $this->uri->segment(3); ?>">Download Template</a>
                                <br/><br/><small>Download Template Example of Add multiple Sessions.</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Upload CSV <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="col-sm-4" style="padding:0px;">
                              <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                  <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                          <input type="file" name="csv" id="csv" class="required">
                                  </span><br/><small>Browse CSV file only</small>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-3">
                            <button class="btn btn-theme_green btn-block" type="submit">
                                Add
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>