<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <a style="top: 7px;display: none;" class="btn btn-primary list_page_btn session_types" href="javascript:void(0);" data-toggle="modal" data-target="#session_type_form_popup"><i class="fa fa-plus"></i> Add Session Types</a>
                <a style="top: 7px;display: none;margin-right:15%;" class="btn btn-primary list_page_btn session_types" href="<?php echo base_url().'Agenda_admin/order_session_type/'.$this->uri->segment(3).'/'.$acid; ?>"> Reorder Session Types </a>
                <?php if(!empty($acid)){ ?>
                <a style="top: 7px;" class="btn btn-primary list_page_btn add_sesion" href="<?php echo base_url(); ?>Agenda_admin/add/<?php echo $this->uri->segment(3).'/'.$acid; ?>"><i class="fa fa-plus"></i> Add Session</a>
                <a style="top: 7px;margin-right:12%;" class="btn btn-primary list_page_btn add_sesion" href="<?php echo base_url(); ?>Agenda_admin/mass_upload_page/<?php echo $this->uri->segment(3).'/'.$acid; ?>"><i class="fa fa-plus"></i> Mass Upload</a>
                <a style="top: 7px;margin-right:24%;" class="btn btn-primary list_page_btn add_sesion" href="<?php echo base_url(); ?>Agenda_admin/downloadDeepLink/<?php echo $this->uri->segment(3).'/'.$acid; ?>"><i class="fa fa-download"></i> Get Deep Links</a>
                <?php } ?>

                <?php if ($this->session->flashdata('agenda_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Session <?php echo $this->session->flashdata('agenda_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#agenda_list" data-toggle="tab">
                                Session List
                            </a>
                        </li>
                        <li class="">
                            <a href="#session_types" data-toggle="tab">
                                Session Types
                            </a>
                        </li>
                         <?php if(!empty($acid)){ ?>
                        <li class="">
                            <a href="#category_menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
						 
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="agenda_list">
                        <div class="row agenda_row">
                        <div id="show_category_error_msg" class="alert alert-danger" <?php if (!$this->session->flashdata('category_error')) { ?> style="display:none;" <?php } ?>>
                            <?php echo $this->session->flashdata('category_error'); ?>
                        </div>
                        <?php if ($this->session->flashdata('category_success')) { ?>
                            <div class="errorHandler alert alert-success no-display" style="display: block;">
                                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('category_success'); ?>
                            </div>
                        <?php } ?>
                        <form class="" id="form" name="form" method="POST" action="<?php echo base_url().'Agenda_admin/add_caterogy/'.$this->uri->segment(3); ?>">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-field-1" style="text-align:left;padding-top: 1.2%;">Agenda Name</label>
                                <div class="col-md-4">
                                    <input type="hidden" name="category_id" id="category_id" value="<?php echo $acid; ?>">
                                    <input type="text" name="agenda_category_name" onblur="checkcategoryname();" id="agenda_category_name" value="<?php echo $category_data[0]['category_name']; ?>" class="form-control" placeholder="Agenda Name">
                                </div>
                            </div>
                            <div class="row col-md-12" style="margin-top: 20px;">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="form-field-2" style="text-align:left;padding-top: 1.2%;">Welcome Screen</label>
                                    <div class="col-md-10">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <textarea name="welcome_screen" id="welcome_screen" class="summernote form-control" ><?php echo $category_data[0]['welcome_screen']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12" style="margin-top: 20px">
                                <div class="form-group">
                                    <div class="col-md-12" align="center">
                                        <button type="button" class="btn btn-green" name="save_category" id="save_category">Save <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;"></div>
                        <div class="row attendee_row">
                            <div class="col-md-2 agenda_assign_label" style="width: 11%;">
                                <label>Assign Ticket Type</label>
                            </div>
                            <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="ticket_type" name="ticket_type">
                                            <option value="">Select Ticket Type</option>
                                            <?php 
                                            foreach ($reg_tickets as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>"><?php echo $value['ticket_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-green btn-block" id="assign_ticket_type_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                        <div class="table-responsive custom_checkbox_table">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_11" style="display: table-cell;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox-table">
                                                <label>
                                                    <input name="selectall_checkbox" type="checkbox" class="flat-grey selectall checkbox_select_all_box">
                                                </label>
                                            </div>
                                        </th>
                                        <th>Session Name</th>
                                        <th>Session Code</th>
                                        <th>Type</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Ticket Type</th>
                                        <th>Speaker Name</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th>Agenda Comments</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($agenda_list);$i++) {  ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="agenda_id[]" value="<?php echo $agenda_list[$i]['Id']; ?>" id="agenda_id_<?php echo $agenda_list[$i]['Id'];?>"   class="foocheck">
                                            <label for="agenda_id_<?php echo $agenda_list[$i]['Id'];?>"></label> 
                                        </td>
                                        <td><a href="<?php echo base_url() ?>Agenda_admin/edit/<?php echo $agenda_list[$i]['Event_id'].'/'.$agenda_list[$i]['Id'].'/'.$acid; ?>"><?php echo $agenda_list[$i]['Heading']; ?></a></td>
                                        <td><?php echo $agenda_list[$i]['agenda_code']; ?></td>
                                        <td><?php echo $agenda_list[$i]['Types']; ?></td>
                                        <td><?php echo $agenda_list[$i]['Start_date']; ?></td>
                                        <td><?php 
                                            if($time_format[0]['format_time']=='0')
                                            {
                                                echo date('h:i A',strtotime($agenda_list[$i]['Start_time']));    
                                            }
                                            else
                                            {
                                                echo date('H:i',strtotime($agenda_list[$i]['Start_time']));
                                            }
                                            
                                         ?></td>
                                        <td><?php
                                            if($time_format[0]['format_time']=='0')
                                            {
                                                echo date('h:i A',strtotime($agenda_list[$i]['End_time'])); 
                                            }
                                            else
                                            {
                                                echo date('H:i',strtotime($agenda_list[$i]['End_time']));
                                            }
                                            ?></td>
                                            <td><?php echo $agenda_list[$i]['ticket_type']; ?></td>
                                             <td><?php echo ucfirst($agenda_list[$i]['custom_speaker_name']); ?></td>
                                            <td><?php echo ucfirst($agenda_list[$i]['custom_location']); ?></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($agenda_list[$i]['Agenda_status']=='1') {  ?> 
                                                      label-success 
                                                <?php  }  else  { ?> 
                                                     label-danger 
                                                <?php } ?>">
                                                <?php if($agenda_list[$i]['Agenda_status']=='1')
                                                    { 
                                                    echo "Active"; 
                                                    
                                                    }
                                                    else 
                                                    { 
                                                        echo "Inactive";
                                                    } 
                                                ?>
                                            </span>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="<?php echo base_url() ?>Agenda_admin/view_session_comments/<?php echo $agenda_list[$i]['Event_id'].'/'.$agenda_list[$i]['Id'].'/'.$acid; ?>" class="btn btn-default" data-placement="top" data-original-title="View Session Comments"><i class="fa fa-eye"></i></a>
                                        </td>
                                       <td>
                                            <?php //if($user->Role_name=='Client'){ ?>
                                            <a href="<?php echo base_url() ?>Agenda_admin/edit/<?php echo $agenda_list[$i]['Event_id'].'/'.$agenda_list[$i]['Id'].'/'.$acid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_agenda(<?php echo $agenda_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php //} ?>

                                            <?php if($user->Role_name=='User'){ ?>
                                            <a href="<?php echo base_url() ?>Agenda_admin/edit/<?php echo $agenda_list[$i]['Id'].'/'.$acid; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_agenda(<?php echo $agenda_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="session_types">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_21">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Session Types</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($session_types as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($value['type_name']); ?></td>
                                        <td>
                                            <a class="btn btn-xs btn-green tooltips" onclick='edit_session_type("<?php echo $value['type_name']; ?>","<?php echo $value['type_id']; ?>")' data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_session_type(<?php echo $value['type_id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                    <div class="tab-pane fade" id="category_menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Agenda_admin/agenda_index/<?php echo $this->uri->segment(3).'/'.$acid; ?>" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $category_menu_data['title']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="agenda_id" id="agenda_id" value="<?php echo $acid; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $this->uri->segment(3); ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                    <span class="fileupload-new">
                                                        <i class="fa fa-picture"></i> Select image
                                                    </span>
                                                    <span class="fileupload-exists">
                                                        <i class="fa fa-picture"></i> Change
                                                    </span>
                                                    <input type="file" name="Images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($category_menu_data['is_feture_product']=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($category_menu_data['img'] !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $category_menu_data['img']; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($category_menu_data['img_view'] =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($category_menu_data['img_view'] =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
</div>
<div id="session_type_form_popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Session Type</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="session_type_form" action="<?php echo base_url().'Agenda_admin/save_new_types/'.$this->uri->segment(3).'/'.$acid; ?>" method="post">
                        <input type="hidden" id="type_id" name="type_id" value="">
                        <div class="form-group">
                            <label class="control-label col-sm-12">Session Type</label>
                            <div class="col-sm-12">
                                <input type="text" name="type_name" id="type_name" class="form-control required" placeholder="Enter Session Type" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-12" style="margin-top: 15px;">
                            <button type="submit" id="session_type_btn_save" class="btn btn-green btn-block">Save</button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>
<script type="text/javascript">
$(window).load(function() {
    $("#sample_21,#sample_11").DataTable({
             bStateSave: true,
            bDestroy: true,
            fnStateSave: function(settings,data) {
                  localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
                },
            fnStateLoad: function(settings) {
                return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
                }
        })

    $('#assign_ticket_type_btn').click(function(){
        var table=$('#sample_11').DataTable();
        var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
        if($('#ticket_type').val()!="" && chkbox_checked>0)
        {
            var data = table.$('input').serialize();
            data=data+"&ticket_type="+$('#ticket_type').val();
            $.ajax({
                url : "<?php echo base_url().'Agenda_admin/assign_agenda_ticket/'.$this->uri->segment(3).'/'.$this->uri->segment(4); ?>",
                data:data,
                type: "POST",
                success : function(data)
                {
                    location.reload();
                }
            });
        }
        else
        {
            $('#assign_agenda_error_div').html('Plase Select Ticket Type or Agenda');
            $('#assign_agenda_error_div').slideDown();
            setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
        }
    });
}); 
    $('#save_category').click(function(){
        if($.trim($('#agenda_category_name').val())!="")
        {
            $('#show_category_error_msg').html('');
            $('#show_category_error_msg').hide();
            $('#save_category').attr('type','submit');
        }
        else
        {
            $('#show_category_error_msg').html("Plase Enter Agenda Name");
            $('#show_category_error_msg').show();
        }
    });

    $('#myTab2 a').click(function(){
    var atrhref=$.trim($(this).attr('href'));
    if(atrhref=="#session_types")
    {
        $('.session_types').show();
        $('.add_sesion').hide();
    }
    else if(atrhref=="#agenda_list")
    {
        $('.session_types').hide();
        $('.add_sesion').show();
    }
    else
    {
        $('.session_types').hide();
        $('.add_sesion').hide();
    }
});
    function checkcategoryname()
    {
        if($.trim($('#agenda_category_name').val())!="")
        {
            $('#show_category_error_msg').html('');
            $('#show_category_error_msg').hide();
            $('#save_category').removeAttr('disabled');
            $.ajax({
                url:"<?php echo base_url().'Agenda_admin/check_agenda_cateory/'.$this->uri->segment(3); ?>",
                data:"Category_name="+$.trim($('#agenda_category_name').val())+"&cid="+$.trim($('#category_id').val()),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#show_category_error_msg').html(values[1]);
                        $('#show_category_error_msg').show();
                        $('#save_category').attr('disabled','disabled');
                    }
                    else
                    {
                        $('#show_category_error_msg').html(values[1]);
                        $('#show_category_error_msg').hide();
                        $('#save_category').removeAttr('disabled');
                    }
                }
            });
        }
        else
        {
            $('#show_category_error_msg').html("Plase Enter Agenda Name");
            $('#show_category_error_msg').show();
            $('#save_category').attr('disabled','disabled');
        }
    }

    function delete_agenda(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>agenda_admin/delete/"+<?php echo $test; ?>+"/"+id+"/<?php echo $acid; ?>"
            $.ajax({
                url:"<?php echo base_url(); ?>Agenda_admin/delete/"+<?php echo $test; ?>+"/"+id+"/<?php echo $acid; ?>",
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
    function edit_session_type(tnm,tid)
    {
        $('#type_id').val(tid);
        $('#type_name').val(tnm);
        $('#session_type_form_popup').modal('show');
    }
    function delete_session_type(tid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>agenda_admin/delete_session_type/"+<?php echo $this->uri->segment(3); ?>+"/"+tid+"/<?php echo $acid; ?>"
             $.ajax({
                url:"<?php echo base_url(); ?>Agenda_admin/delete_session_type/"+<?php echo $this->uri->segment(3); ?>+"/"+tid+"/<?php echo $acid; ?>",
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
    $(document).ready(function() {
        /*pen*/
        $('#type_name').keydown(function (e) {
              if (e.shiftKey || e.ctrlKey || e.altKey) {
                  e.preventDefault();
              } else {
                  var key = e.keyCode;
                  if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                      e.preventDefault();
                  }
              }
          });
        /*pen*/
        $('#session_type_form').validate({
            errorElement: "span",
            errorClass: 'help-block',  
            rules: 
            {
                type_name:{
                    required: true,
                }
            },
            highlight: function (element) 
            {
              $(element).closest('.help-block').removeClass('valid');
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error');
            }
        });
    });  
</script>
<!-- end: PAGE CONTENT-->