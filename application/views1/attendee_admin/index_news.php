<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
<style type="text/css" media="screen">
 #search_keyword{
    padding: 7px 4px !important;
 }   
</style>
<?php $arr=$this->session->userdata('current_user'); $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <!-- <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Attendee_admin/add/<?php echo $Event_id; ?>"><i class="fa fa-plus"></i> Invite Attendees</a> -->
                <?php if (count($skipcsvdata)>0 || $this->uri->segment(3) == '1511'){ ?>        
                <a style="top: 7px;margin-right:12%;" class="btn btn-primary list_page_btn" href="<?php echo base_url().'Attendee_admin/send_invitation/'.$Event_id; ?>">Invite By Email</a>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_view_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('attendee_view_data'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Users <?php echo $this->session->flashdata('attendee_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('add_without_invite')) { ?>
                    <div class="errorHandler alert alert-info no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('add_without_invite'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('org_limit_attendee')) { ?>
                    <div class="errorHandler alert alert-warning no-display" style="display: block; background-color: #FFA500 !important;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('org_limit_attendee'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('formbuild_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Form Builder <?php echo $this->session->flashdata('formbuild_data'); ?> Successfully.
                    </div>
                <?php } ?>
              
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a class="dropdown-toggle">
                               <span id="Attendees_textshow"> Users </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                        </li>      
                    </ul>

                    <div class="tab-content">
                    
                        <div class="tab-pane fade active in at_tabpan" id="attendees_list">
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;">

                        </div>
                        <form action="<?php echo base_url().'Attendee_admin/assign_agenda_category/'.$this->uri->segment(3); ?>" method="post">    
                            <div class="row">
                                <div class="col-sm-2 ">
                                    <a class="btn btn-success" data-toggle="tab" data-target="#add_attendee">Add Users </a>
                                </div>
                                <!-- <div class="col-sm-3 ">
                                    <a href="<?php echo base_url().'Attendee_admin/download_data_in_csv/'.$this->uri->segment(3); ?>" class="btn btn-green " id="download_csv">Download Your Attendee List</a> 
                                </div> -->
                                <div class="col-sm-2 ">
                                    <button type="button" class="btn btn-danger " disabled="disabled" id="delete_attendee">Delete Users</button>
                                </div>
                                <div class="col-sm-3">
                                    <a href="<?php echo base_url().'Attendee_admin/add_mass_attendee/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="download_csv">Mass Upload</a> 
                                </div>
                            </div>
                            <!-- <div class="row" style="margin-bottom: 10px;margin-top: 10px;">
                            </div> -->
                            <div class="row" style="margin-bottom: 10px;margin-top: 10px">
                                    <div class="col-md-6">
                                        Show: 
                                        <select class="form control" id="paginate">
                                            <option value="10" <?php echo ($limit_per_page == '10') ? 'selected' : '' ?>>10</option>
                                            <option value="20" <?php echo ($limit_per_page == '20') ? 'selected' : '' ?>>20</option>
                                            <option value="30" <?php echo ($limit_per_page == '30') ? 'selected' : '' ?>>30</option>
                                            <option value="0" <?php echo ($limit_per_page == '0') ? 'selected' : '' ?>>All</option>
                                        </select> Records
                                    </div>
                                    <div class="col-md-6 pull-right text-right" >
                                    <form action="<?php echo base_url().'Attendee_admin/index/'.$this->uri->segment(3) ?>" method="post" id="search_form" accept-charset="utf-8">
                                        Search: <input type="text" aria-controls="sample_123" name="keyword" id="search_keyword" value="<?= $keyword ?>">
                                        <button class="btn btn-primary" type="submit" name="submit"  id="search_button"><i class='fa fa-search'></i></button>
                                        <a href="<?php echo base_url().'Attendee_admin/index/'.$this->uri->segment(3) ?>" class="btn btn-primary" title="">Reset</a>
                                    </form>
                                    
                                </div></div>
                            <div id="attendee_list_table" class="table-responsive custom_checkbox_table" style="overflow-x:scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_123">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input name="selectall_checkbox" type="checkbox" class="flat-grey selectall checkbox_select_all_box">
                                                    </label>
                                                </div>
                                            </th>
                                            <th>Name</th>
                                             <?php if($Event_id == '1012' || $Event_id == '634' || $Event_id == '1378' || $Event_id == '1512' || $Event_id == '1580' || $Event_id == '1609' || $Event_id == '1804' || $Event_id == '1850'): ?>
                                            <th>Unique No.</th>
                                            <?php endif;?>
                                            <?php if($Event_id == '1012'): ?>
                                            <th>Barcode List</th>
                                            <?php endif; ?>
                                            <td>Email</td>
                                            <th>Edit or Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($Attendees); $i++) { ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="attendees_id[]" value="<?php echo $Attendees[$i]['uid']; ?>" id="attendees_id_<?php echo $Attendees[$i]['uid'];?>"   class="foocheck">
                                                    <label for="attendees_id_<?php echo $Attendees[$i]['uid'];?>"></label>    
                                                </td>
                                                <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $Attendees[$i]['uid'].'/'.$Event_id; ?>"><?php echo $Attendees[$i]['Firstname'].' '.$Attendees[$i]['Lastname']; ?></a></td>
                                                <?php if($Event_id == '1012' || $Event_id == '634' || $Event_id == '1378' || $Event_id == '1512' || $Event_id == '1580' || $Event_id == '1609' || $Event_id == '1804' || $Event_id == '1850'): ?>
                                                <td><?php echo $Attendees[$i]['Unique_no']; ?></td>
                                                <?php endif; ?>
                                                <?php if($Event_id == '1012'): ?>
                                                <td><?php echo $Attendees[$i]['barcodes']; ?></td>
                                                <?php endif; ?>
                                                <td><?php echo $Attendees[$i]['Email']; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>Profile/update/<?php echo $Attendees[$i]['uid'].'/'.$Event_id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                                    <a href="javascript:;" onclick="delete_attendee(<?php echo $Attendees[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pull-left pagination">
                                    <?php echo "Showing ".($start+1)." to ".($start+count($Attendees))." of ".$total_rows." entries"; ?>
                                </div>
                                <div class="pull-right" style="display: none;">
                                    <?php if (isset($links)) { ?>
                                        <?php echo $links ?>
                                    <?php } ?>
                                </div>
                                <div class="pagination pull-right" style="margin-right: 10px;">
                                  <?php echo $this->pagination->create_dropdown_links() ?>
                                </div>
                            </div>
                            </form>
                        </div>
						<div class="tab-pane fade at_tabpan" id="add_attendee">
                            <div class="panel-body">
							<form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Attendee_admin/add_attendee_without_invite/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
								<div class="row">
								  <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
								  <div class="errorHandler alert alert-success no-display" style="display: block;">
								  <i class="fa fa-remove-sign"></i> Add Successfully.
								  </div>
								  <?php } ?>
								  <div class="col-md-6">

									<div class="form-group">
									   <label class="control-label">First Name <span class="symbol required"></span></span>
									   </label>
										<input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
									</div>

									<div class="form-group">
									   <label class="control-label">Last Name <span class="symbol required"></span> </span>
									   </label>
										<input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
									</div>
									
									<div class="form-group">
									  <label class="control-label">Email <span class="symbol required"></span></label>
									  <input type="email" placeholder="Email" class="form-control required" id="email_attendee" name="email_attendee" onblur="checkemail_by_attendee();">
									  <input type="hidden" class="form-control" id="event_id" name="event_id" value="<?php echo $this->uri->segment(3); ?>">
									</div>

									<div class="row">
									   <div class="form-group col-md-6">
											<label class="control-label">Password <span class="symbol required"></span></label>
											<input type="password" class="form-control" id="password" name="password">
									   </div>
									   <div class="form-group col-md-6" style="margin-left: 7%;">
											 <label class="control-label">Confirm Password <span class="symbol required"></span></label>
											 <input type="password" class="form-control" id="password_again" name="password_again">
									   </div>
									</div>
									  <div class="row" style="margin-left:-30px;">
										  <div class="col-md-4">
											 <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
										  </div>
									  </div>
								  </div>
								</div>
							</form>
                            </div>
						</div>
                    <div class="tab-pane fade at_tabpan" id="add_hub_attendees">          
                      <div class="row">
                        <form method="post" action="<?php echo base_url().'Attendee_admin/add_hub_attendees/'.$this->uri->segment(3); ?>">
                        <div class="form-group">
                            <label class="col-sm-3" for="form-field-1">
                                <strong>Select Hub Attendees Group</strong>
                            </label>
                            <div class="col-sm-5" style="padding:0px;">
                                <select class="form-control" name="attendee_group_id" id="attendee_group_id">
                                    <option value="">Select Attendees Group</option>
                                    <?php foreach ($attendee_group as $key => $value) { ?>
                                        <option value="<?php echo $value['group_id']; ?>"><?php echo $value['group_name']; ?></option>
                                    <?php  } ?>
                                </select>
                            </div>
                        </div><div class="row"></div>
                        <div class="form-group">
                        <div class="col-md-3">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                        </div>
                        </form>
                        </div>
                      </div>        
                           
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>
<form action="<?php echo base_url().'Attendee_admin/index/'.$this->uri->segment(3) ?>" method="post" id="search_form" accept-charset="utf-8">
    <input type="hidden" id="keyword" name="keyword" value="" placeholder="">
</form>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script>
     jQuery(document).ready(function() {
          TableData.init();
          $('#column_btn_save').click(function(){
            if($.trim($('#column_name').val())=="")
            {
                $('#column_name_text_box_errror_msg').show();
            }
            else
            {
                $('#column_name_text_box_errror_msg').hide();
                $('#column_btn_save').attr('type','saumit');
            }
          });
        $('input.checkbox_select_all_box').on('ifChecked', function(event) {
            $('#send_mail_btn').removeAttr('disabled');
            $('#send_mail_pdf_btn').removeAttr('disabled');
             $("#delete_attendee").removeAttr('disabled');
        });
        $('input.checkbox_select_all_box').on('ifUnchecked', function(event) {
            $('#send_mail_btn').attr('disabled','disabled');
            $('#send_mail_pdf_btn').attr('disabled','disabled');
            $("#delete_attendee").attr('disabled','disabled');
        });
     });
        
    $('input[name="attendees_id[]"]').click(function(){
        //var table=$('#sample_123').DataTable();
        var chkbox_checked=$('input[name="attendees_id[]"]:checked').length;
        if(chkbox_checked > 0)
        {
            $('#send_mail_btn').removeAttr('disabled');
            $('#send_mail_pdf_btn').removeAttr('disabled');
            $("#delete_attendee").removeAttr('disabled');
        }
        else
        {
            $('#send_mail_btn').attr('disabled','disabled');
            $('#send_mail_pdf_btn').attr('disabled','disabled');
             $("#delete_attendee").attr('disabled','disabled');
        }
    });
</script>
<script type="text/javascript">
    function delete_attendee(id,Event_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete/"+<?php echo $test; ?>+"/"+id
            $.ajax({
                url:"<?php echo base_url(); ?>Attendee_admin/delete/"+<?php echo $test; ?>+"/"+id,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
    function delete_auth_user(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_authorized_user/"+<?php echo $this->uri->segment(3); ?>+"/"+id
            $.ajax({
                url:"<?php echo base_url(); ?>Attendee_admin/delete_authorized_user/"+<?php echo $this->uri->segment(3); ?>+"/"+id,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
</script>
<style type="text/css">
    #select2-result-label-7
    {
        display: none;
    }
</style>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    function getuseredit(uid,user_type)
    {
        $('#extra_info_edit_model').modal();
        $('#extra_user_id').val(uid);
        $('#user_type').val(user_type);
        $.ajax({
            url:"<?php echo base_url().'Attendee_admin/getuserextra_info/'.$this->uri->segment(3).'/'; ?>"+uid,
            data:"user_type="+user_type,
            type:'post',
            success:function(result){
                var result= jQuery.parseJSON(result);
                $.each(result, function( index, value ) {
                    $('#text_'+index).val(value);
                });
                $('#model_extrainfo_msg').hide();
                $('#load_extra_info_field').show();
            }
        });
    }


    function delete_invited_attendee(iid)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_invited_attendee/"+<?php echo $test; ?>+"/"+iid
            $.ajax({
                url:"<?php echo base_url(); ?>Attendee_admin/delete_invited_attendee/"+<?php echo $test; ?>+"/"+iid,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });
        }
    }
    function delete_inactive_attendee(temp_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            //window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_inactive_attendee/"+<?php echo $test; ?>+"/"+temp_id
            $.ajax({
                url:"<?php echo base_url(); ?>Attendee_admin/delete_inactive_attendee/"+<?php echo $test; ?>+"/"+temp_id,
                type:"post",
                success:function(result){
                   window.location.reload();
                }
            });

        }
    }
    function checkemail_by_attendee()
        {
            var sendflag="";
                $.ajax({
                url : '<?php echo base_url().'Attendee_admin/checkemail_by_attendee/'.$this->uri->segment(3); ?>',
                data :'email='+$("#email_attendee").val()+'&idval='+$('#idval').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#email_attendee').parent().removeClass('has-success').addClass('has-error');
                        $('#email_attendee').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#email_attendee').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        $('#email_attendee').parent().removeClass('has-error').addClass('has-success');
                        $('#email_attendee').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#email_attendee').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
</script>
<style type="text/css">
#custom_colunm
{
    width: 20% !important;
}
.box{
    display: none;
}
#assign_moderator_form .select2-container .select2-choice{
width:350px;
margin:0 auto 20px;
}
#assign_attendee_moderator_popup .modal-content{
    overflow:hidden;
}
#assign_attendee_moderator_popup .modal-header{
    background: #007ce9 none repeat scroll 0 0;
    color:#fff;
}
#assign_attendee_moderator_popup .modal-header .close{
    color:#fff;opacity:1;
}
.col-sm-9.moderator_selectbox {
  display: block;
  float: none;
  margin: 0 auto;
}
</style>
<!-- end: PAGE CONTENT-->
<script>
  $(document).ready(function(){

        $('#paginate').on('change', function (e) {
            var valueSelected = this.value;
            window.location.href = "<?php echo base_url().'Attendee_admin/index/'.$event_id.'/' ?>"+valueSelected;
        });

        $('#search_button').on('click',function(e){
            e.preventDefault();
            if($("#search_keyword").val()!='')
            {
                $("#keyword").val($("#search_keyword").val());
                $("#search_form").submit();
            }

        })
});
</script>
