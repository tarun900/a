<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />

<?php $arr=$this->session->userdata('current_user'); $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <!-- <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>attendee_admin/add/<?php echo $Event_id; ?>"><i class="fa fa-plus"></i> Invite Attendees</a> -->
                <?php if (count($skipcsvdata)>0){ ?>        
                <a style="top: 7px;margin-right:12%;" class="btn btn-primary list_page_btn" href="<?php echo base_url().'attendee_admin/send_invitation/'.$Event_id; ?>">Invite By Email</a>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_view_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('attendee_view_data'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Attendee <?php echo $this->session->flashdata('attendee_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('add_without_invite')) { ?>
                    <div class="errorHandler alert alert-info no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('add_without_invite'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('org_limit_attendee')) { ?>
                    <div class="errorHandler alert alert-warning no-display" style="display: block; background-color: #FFA500 !important;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('org_limit_attendee'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('formbuild_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Form Builder <?php echo $this->session->flashdata('formbuild_data'); ?> Successfully.
                    </div>
                <?php } ?>
              
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#attendee_dropdown_link_div" data-close-others="true">
                               <span id="Attendees_textshow"> Attendees </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="attendee_dropdown_link_div" class="dropdown-menu dropdown-info"> 
                                <li class="active">
                                    <a href="#attendees_list" data-toggle="tab">
                                        Attendees List
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#add_attendee" data-toggle="tab">
                                        Add Attendee Without Inviting
                                    </a>
                                </li>
                                <!-- <li class="">
                                    <a href="#invite_attendees" data-toggle="tab">
                                        Invited Attendees
                                    </a>
                                </li> -->
                                <?php if($event['Event_type']=='4'){ ?>
                                <li class="">
                                    <a href="#authorized_emails" data-toggle="tab">
                                        Authorized Emails
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a href="#singup" data-toggle="tab">
                                Sign Up Process
                            </a>
                        </li>
                        <?php if ($user->Role_name == 'Client') { ?>
                        <!-- <li class="dropdown">
                            <a data-toggle="dropdown" href="#Registration_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Registration_textshow"> Registration </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Registration_dropdown_link_div" class="dropdown-menu dropdown-default"> 
                                <li>
                                    <a href="#registration_screen" data-toggle="tab">Registration Screen</a>
                                </li>
                                <li>
                                    <a href="#badges_values" data-toggle="tab">Badges Values</a>
                                </li>
                                <li>
                                    <a href="#singup" data-toggle="tab">
                                        Sign Up Process
                                    </a>
                                </li>
                                <li>
                                    <a href="#payment_processor" data-toggle="tab">
                                        Payment Processor
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <?php } ?>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Viewing_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Viewing_textshow"> Viewing </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Viewing_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <?php if ($user->Role_name == 'Client') { ?> 
                                <li>
                                    <a href="#merge_fields" data-toggle="tab">Merge Fields</a>
                                </li>
                                <li>
                                    <a href="#viewer_permissions" data-toggle="tab">Viewer Permissions</a>
                                </li>
                                <?php } ?>
                                <li class="">
                                    <a href="#show_directory" data-toggle="tab">
                                        Show Directory
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#meetings_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Viewing_textshow">Meetings</span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="meetings_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <li class="">
                                    <a href="#allow_metting" data-toggle="tab">
                                        Meetings
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#metting_location" data-toggle="tab">
                                        Meetings Locations
                                    </a>
                                </li>
                                <?php if($this->uri->segment(3) == '259'): ?>
                                <li>
                                    <a href="#Meeting_Concierge" data-toggle="tab">
                                      Meeting Concierge
                                    </a>
                                </li>
                                <?php endif;?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Groups_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Viewing_textshow">Groups</span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Groups_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <li class="">
                                    <a href="#groups" data-toggle="tab">
                                        Groups
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#metting_location" data-toggle="tab">
                                        Messaging Permissions
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#AttendeeCategories" data-toggle="tab">
                                Attendee Categories
                            </a>
                        </li>
                        <li>
                            <a href="#advertising" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>        
                    </ul>

                    <div class="tab-content">
                    
                        <div class="tab-pane fade active in at_tabpan" id="attendees_list">
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;">

                        </div>
                        <form action="<?php echo base_url().'attendee_admin/assign_agenda_category/'.$this->uri->segment(3); ?>" method="post">    
                            <div class="row attendee_row" style="text-align: center;">
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New Agenda To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="agenda_category_id" name="agenda_category_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New View To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="attendee_view_id" name="attendee_view_id">
                                            <option value="">Select View</option>
                                            <?php foreach ($view_data as $key => $value) { ?>
                                                <option value="<?php echo $value['view_id']; ?>"><?php echo $value['view_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                    </div>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <button class="btn btn-green btn-block" id="assign_agenda_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Selected Attendees Type</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="attendee_type" name="attendee_type">
                                                <option value="">Select Registration Type</option>
                                                <?php $form=json_decode($registration_data[0]['registration_forms'],true);
                                                $field=$form['fields'][5]['choices'];
                                                foreach ($field as $key => $value) { ?>
                                                <option value="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Selected Attendees Group</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="user_group" name="user_group" onchange="get_typepopup(this);">
                                                <option value="">Select Attendee Group</option>
                                                <?php foreach($user_group as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>"><?php echo $value['group_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Selected Attendees Category</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="attendee_categories" name="attendee_categories" onchange="get_typepopup(this);">
                                                <option value="">Select Attendee Category</option>
                                                <?php foreach($attendee_categories as $key => $value) { ?>
                                                <option value="<?php echo $value['categorie_keywords'] ?>"><?php echo $value['category'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    <button class="btn btn-success btn-block" id="send_mail_pdf_btn" type="button" disabled="disabled">Send Badges</button>
                                </div>
                                <div class="col-sm-3 pull-right">
                                    <button class="btn btn-success btn-block" id="send_mail_btn" type="button" disabled="disabled">Send Email Invite</button>
                                </div>
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'attendee_admin/download_data_in_csv/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="download_csv">Download Your Attendee List</a> 
                                </div>
                                <div class="col-sm-3 pull-right">
                                <a class="btn btn-success btn-block" id="add_new_column_btn" data-toggle="modal" data-target="#add_cloumn_models">Add a New Column</a></div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;margin-top: 10px;">
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'attendee_admin/add_mass_attendee/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="download_csv">Mass Upload</a> 
                                </div>
                            </div>
                            <div id="attendee_list_table" class="table-responsive custom_checkbox_table" style="overflow-x:scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox-table">
                                                    <label>
                                 