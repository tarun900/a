<?php $user=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
        	<div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">View</span></h4>
           </div>
           <div class="panel-body">
                <form action="" method="POST"  role="form" id="form" novalidate="novalidate">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label required">Name<span class="symbol required"></span> </span></label>
                            <input type="text" placeholder="Role" class="form-control required" id="view_name" value="<?php echo $view_data[0]['view_name']; ?>" name="view_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label" style="" for="form-field-1">Modules <span class="symbol required"></span></label> 
                            <?php $modules=explode(",",$view_data[0]['view_modules']); ?> 
                            <select style="height:auto;" multiple="multiple" style="" id="view_modules[]" class="select2-container select2-container-multi required form-control search-select menu-section-select" name="view_modules[]">
                                <?php foreach ($role_menu as $key => $value) { ?>
                                <option id="checkbox_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>" <?php if(in_array($value->id,$modules)){ ?> selected="selected" <?php } ?>><?php echo $value->menuname; ?></option>
                                <?php } ?>
                            </select>             
                        </div>
                        <div class="form-group">
                            <label class="control-label" style="" for="form-field-1">Custom Modules <span class="symbol required"></span></label>
                            <?php $modules=explode(",",$view_data[0]['view_custom_modules']); ?>
                            <select style="height:auto;" multiple="multiple" style="" id="view_custom_modules[]" class="select2-container select2-container-multi required form-control search-select menu-section-select" name="view_custom_modules[]">
                                <?php foreach ($cms_menu as $key => $value) { ?>
                                <option id="checkbox_<?php echo $value['id']; ?>" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$modules)){ ?> selected="selected" <?php } ?>><?php echo $value['menuname']; ?></option>
                                <?php } ?>
                            </select>             
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </div>
</div>