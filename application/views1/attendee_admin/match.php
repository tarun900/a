<?php  
$arr=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#indiviual_attendee" data-toggle="tab">
                                CSV Attendee
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>                  
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="alert alert-info"><?php echo $nomatch.' Column Are Not Match'; ?></div>
                    <div class="tab-pane fade active in" id="indiviual_attendee">
                        <form method="post" class="form-horizontal" action="<?php echo base_url().'Attendee_admin/match_upload_data/'.$this->uri->segment(3); ?>" id="attende_form">
                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1" style="font-weight: bold;">
                               Cms Columns
                            </label>
                            <label class="col-sm-9" style="font-weight: bold;">
                               CSV Columns
                            </label>
                        </div>
                        <?php $fixedc=array('Emailaddress','firstname','lastname','Company','Title');
                        foreach ($fixedc as $key1 => $value1) { ?>
                        <div class="form-group">
                            <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1"><?php echo $value1; ?></label>
                            <select class="col-sm-9" name="<?php echo $value1; ?>" id="<?php echo $value1; ?>">
                                <?php foreach ($keysdata as $key => $value) { ?>
                                    <option value="<?php echo $value; ?>" <?php if($value1==$value){ ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php  } 
                        foreach ($customkey as $key1 => $value1) { ?>
                        <div class="form-group">
                            <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1"><?php echo $value1['column_name']; ?></label>
                            <select class="col-sm-9" name="<?php echo $value1['column_id']; ?>" id="<?php echo $value1['column_id']; ?>">
                                <?php foreach ($keysdata as $key => $value) { ?>
                                    <option value="<?php echo $value; ?>" <?php if($value1['column_name']==$value){ ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php  } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                            </label>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-theme_green btn-block">
                                    Next
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>   
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>