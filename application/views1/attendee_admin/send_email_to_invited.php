<?php  $arr=$this->session->userdata('current_user');  $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#multiple_attendee" data-toggle="tab">
                                Invite Multiple Attendees
                            </a>
                        </li>  
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>                  
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="multiple_attendee">
                        <form role="form" method="post" class="form-horizontal" id="form2" action="<?php echo base_url().'Attendee_admin/send_mail_to_invite_attendee/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
                            <input type="hidden" name="invite_attendees_id" value="<?php echo implode(",",$this->session->userdata('invite_attendees_id')); ?>">
                            <input type="hidden" name="temp_attendees_id" value="<?php echo implode(",",$this->session->userdata('temp_attendees_id')); ?>">
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Inviation Message
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea name="msg" id="content1" style="height: 200px;" class="ckeditor form-control"></textarea>
                                    </div>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Subject <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Subject" class="form-control required" id="subject" name="subject">
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control" id="sent_name" name="sent_name">
                                    </div>
                                </div>
                            </div>
                           <!-- <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Email <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="email" value="invite@allintheloop.com" placeholder="Sender Email Address" class="form-control required" id="sent_from" name="sent_from" disabled="disabled">
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function setEmailTemplate()
    {
        var Slug=$("#template").val();
        $.ajax({
            url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
            data :'Slug='+Slug,
            type: "POST",  
            async: false,
            success : function(data)
            {
                $("#content").code('');
                $("#content").code(data);
            }
        });
    }
    function setEmailTemplate1()
    {
        var Slug=$("#template").val();
       
        $.ajax({
            url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
            data :'Slug='+Slug,
            type: "POST",  
            async: false,
            success : function(data)
            {
                $("#content1").code('');
                $("#content1").code(data);
            }
        });
    }
</script>