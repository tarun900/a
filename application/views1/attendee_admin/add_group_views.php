<?php $user=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
        	<div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Group</span></h4>
           </div>
           <div class="panel-body">
                <form action="" method="POST"  role="form" id="form" novalidate="novalidate">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label required">Group Name<span class="symbol required"></span> </span></label>
                            <input type="text" placeholder="Group Name" class="form-control required" id="group_name" name="group_name">
                        </div>
                        <!-- make like live -->
                        <div class="form-group">
                            <label class="control-label">Limit number of meetings to<span class="symbol"></span> </span></label>
                            <input type="text" placeholder="Limit number of meetings to" class="form-control" id="max_meeting_limit" name="max_meeting_limit" onkeypress="return isNumber(event)">
                        </div>
                        <!-- end make -->
                        <div class="form-group">
                            <label class="control-label required">Permissions<span class="symbol required"></span> </span></label>
                            <div class="col-sm-12">
                                <label class="radio-inline hover">
                                    <input type="checkbox" class="purple" checked="checked" name="send_message">
                                    Send Message
                                </label>
                                <label class="radio-inline">
                                    <input type="checkbox" class="purple" checked="checked" name="send_request">
                                    Send Request
                                </label>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Options<span class="symbol required"></span> </span></label>
                            <div class="col-sm-12">
                                <label class="radio-inline hover">
                                    <input type="radio" class="purple" value="0" name="Status">
                                    Add Attendees Manually
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" class="purple" value="1" name="Status">
                                    Add Attendees Using Criteria
                                </label>    
                            </div>
                        </div> 
                        <div id="criteria_assign">
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns0">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator0">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values0" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns1">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator1">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values1" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns2">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator2">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values2" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns3">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator3">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values3" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns4">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator4">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values4" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <div class="col-sm-4">
                                        <select name="columns5">
                                            <option value="">Select</option>
                                            <option value="Firstname">First Name</option>
                                            <option value="Lastname">Last Name</option>
                                            <option value="Company_name">Company Name</option>
                                            <option value="Title">Title</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="operator5">
                                            <option value="">Select</option>
                                            <option value="=">Equals</option>
                                            <option value="!=">Does not equal</option>
                                            <option value="start_with">Starts with</option>
                                            <option value="end_with">Ends with</option>
                                            <option value="contains">Contains</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="search_values5" class="form-control"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<script type="text/javascript">
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>