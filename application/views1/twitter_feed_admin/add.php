<?php $acc_name=$this->session->userdata('acc_name'); ?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="panel-body" style="padding: 0px;">

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#hashtags_list" data-toggle="tab">
                                Add Hashtags
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="hashtags_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Hashtags <span id="symbol_required" class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="Enter Hashtags" id="hashtags" name="hashtags" class="form-control name_group required">
                                        <span id="hashtags_error" for="hashtags" style="display:none;" class="help-block">This field is required.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Hashtags Status</label>
                                    <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1" checked="checked" name="hashtags_status">
                                            Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0" name="hashtags_status">
                                            Inactive
                                        </label>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-4">
                                        <button id="form_submit_btn" class="btn btn-yellow btn-block" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
    $('#hashtags').blur(function(){
        $.ajax({
            url:"<?php echo base_url().'Twitter_feed_admin/checkvalidhashtags/'.$this->uri->segment(3); ?>",
            type:"post",
            data:"hashtags="+$.trim($(this).val()),
            success:function(result){
                var data=result.split('###');
                if($.trim(data[0])=="error")
                {
                    $('#hashtags_error').parent().parent().addClass('has-error');
                    $('#symbol_required').attr('class','symbol required');
                    $('#hashtags_error').html(data[1]);
                    $('#hashtags_error').show();
                    $('#form_submit_btn').attr('disabled','disabled');
                }
                else
                {
                     $('input[type="submit"]').removeAttr('disabled');
                }
            }
        });
    });
</script>