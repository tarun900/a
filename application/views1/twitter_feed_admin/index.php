 <?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                  <?php  if(count($hashtags_list) < 6){ ?> 
                        <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Twitter_feed_admin/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Twitter Feed</a>
                        <?php } ?>

                <?php if ($this->session->flashdata('feed_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Twitter <?php echo $this->session->flashdata('feed_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#hashtags_list" data-toggle="tab">
                                Twitter Hashtags
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                               Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						 
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>

                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="hashtags_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Hashtags</th>
                                        <th>Status</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($hashtags_list);$i++) { ?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><a href="<?php echo base_url() ?>Twitter_feed_admin/edit/<?php echo $this->uri->segment(3).'/'.$hashtags_list[$i]['Id']; ?>"><?php echo $hashtags_list[$i]['hashtags']; ?></a></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($hashtags_list[$i]['status']=='1') {  ?> 
                                                      label-success 
                                                <?php  }  else  { ?> 
                                                     label-danger 
                                                <?php } ?>">
                                                <?php if($hashtags_list[$i]['status']=='1')
                                                    { 
                                                    echo "Active"; 
                                                    
                                                    }
                                                    else 
                                                    { 
                                                        echo "Inactive";
                                                    } 
                                                ?>
                                            </span>
                                        </td>
                                       <td>
                                            <a href="<?php echo base_url() ?>Twitter_feed_admin/edit/<?php echo $this->uri->segment(3).'/'.$hashtags_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_hashtags(<?php echo $hashtags_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips fa-del-icon" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>	

					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    function delete_hashtags(id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Twitter_feed_admin/delete/"+<?php echo $test; ?>+"/"+id
        }
    }
</script>
<!-- end: PAGE CONTENT-->