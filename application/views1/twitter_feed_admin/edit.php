<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>


<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Hashtag</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>

            </div>
            <div class="panel-body">

<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Hashtags <span id="symbol_required" class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input id="hashtags" type="text" name="hashtags" class="form-control name_group required" value=<?php echo $hashtags['hashtags']; ?> >
                                        <span id="hashtags_error" for="hashtags" style="display:none;"  class="help-block">This field is required.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Hashtags Status</label>
                                    <div class="col-sm-9">
                                    <?php $checked = $hashtags['status'];
                                     ?>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1" <?php echo ($checked) ? "checked" : ""; ?> name="hashtags_status">
                                            Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0" <?php echo ($checked) ? "" : "checked"; ?> name="hashtags_status">
                                            Inactive
                                        </label>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-4">
                                        <button id="form_submit_btn" class="btn btn-yellow btn-block" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>

             </div>

        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
.close_popup
{
    position: absolute;
    top: -10px;
    right: -10px;
}
#popUpDiv {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}

#blanket1 {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}
#popUpDiv1 {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}
#popUpDiv1 .form-group label{color:#333;}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}

.fileupload-new.thumbnail img
{
 height: 140px !important;
}

</style>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
    $('#hashtags').blur(function(){
        $.ajax({
            url:"<?php echo base_url().'Twitter_feed_admin/checkvalidhashtags/'.$this->uri->segment(3); ?>",
            type:"post",
            data:"hashtags="+$.trim($(this).val()),
            success:function(result){
                var data=result.split('###');
                if($.trim(data[0])=="error")
                {
                    $('#hashtags_error').parent().parent().addClass('has-error');
                    $('#symbol_required').attr('class','symbol required');
                    $('#hashtags_error').html(data[1]);
                    $('#hashtags_error').show();
                    $('#form_submit_btn').attr('disabled','disabled');
                }
                else
                {
                     $('input[type="submit"]').removeAttr('disabled');
                }
            }
        });
    });
</script>