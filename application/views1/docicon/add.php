<!-- start: PAGE CONTENT -->
<div class="row">
     <div class="col-sm-12">
          <!-- start: TEXT FIELDS PANEL -->
          <div class="panel panel-white">
               <div class="panel-heading">
                    <h4 class="panel-title">Add <span class="text-bold">Doc Category</span></h4>
                    <div class="panel-tools">
                         <a class="btn btn-xs btn-link panel-close" href="#">
                              <i class="fa fa-times"></i>
                         </a>
                    </div>
               </div>
               <div class="panel-body">
                    <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
                         <div class="row">
                              <?php if ($this->session->flashdata('site_setting_data') == "Updated")
                              { ?>
                                   <div class="errorHandler alert alert-success no-display" style="display: block;">
                                        <i class="fa fa-remove-sign"></i> Added Successfully.
                                   </div>
<?php } ?>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label class="control-label">Name </span>
                                        </label>
                                        <input type="text" placeholder="Name" class="form-control required" id="title" name="name">
                                   </div>
                                   <div class="form-group">
                                        <label class="control-label">Doc Icon
                                        </label>

                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <div class="fileupload-new thumbnail"></div>
                                             <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                             <div class="user-edit-image-buttons">
                                                  <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                       <input type="file" name="images">
                                                  </span>
                                                  <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                       <i class="fa fa-times"></i> Remove
                                                  </a>
                                             </div>
                                        </div>

                                   </div>
                                   <div class="row">
                                        <div class="col-md-4">
                                             <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>
               </div>
          </div>
          <!-- end: TEXT FIELDS PANEL -->
     </div>
</div>
<!-- end: PAGE CONTENT-->