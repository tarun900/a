<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Doc Icon <span class="text-bold">List</span></h4>
                <?php if($user->Role_name=='Client'){ ?>
                <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Docicon/add/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add Doc Icon</a>
                <?php } ?>
                <div class="panel-tools"></div>
			</div>
			<div class="panel-body">
                <?php if($this->session->flashdata('docicon_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Doc Icon <?php echo $this->session->flashdata('docicon_data'); ?> Successfully.
                </div>
                <?php } ?>
                <div class="table-responsive">
    				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
    					<thead>
    						<tr>
    							<th>#</th>
    							<th>Name</th>
                                   <th>Action</th>
    						</tr>
    					</thead>
    					<tbody>
    						<?php 
                            for($i=0;$i<count($docicon);$i++)
    						{
    						?>
    						<tr>
    							<td><?php echo $i+1; ?></td>
    							<td><?php echo $docicon[$i]['name']; ?></td>
                                   <td>
                                        <?php if($user->Role_name=='Client'){ ?>
                                         <a href="<?php echo base_url(); ?>Docicon/edit/<?php echo $docicon[$i]['id']; ?>/<?php echo $event_id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                        <?php } ?>
                                        <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                        <a href="javascript:;" onclick="delete_user(<?php echo $docicon[$i]['id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                   </td>
    						</tr>
    						<?php } ?>
    					</tbody>
    				</table>
                </div>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>

<script>
    function delete_user(id)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Docicon/delete/"+id+"/<?php echo $event_id; ?>";
        }
    }
</script>
<!-- end: PAGE CONTENT-->