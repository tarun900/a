<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
            <div class="panel-body" style="padding: 0px;">
                <a style="margin-top: 0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Exibitor_survey/add/<?php echo @$event_id; ?>"><i class="fa fa-plus"></i> Add new Question</a>
                <a style="margin-top: 0.5%;margin-right: 5px;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Exibitor_survey/reorder_question/<?php echo @$event_id; ?>"> Reorder Questions</a>
                <?php if($this->session->flashdata('survey_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('survey_data'); ?>
                </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#survey_list" data-toggle="tab">
                                Questions List
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="survey_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Question</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (@$question_list as $key => $question) { ?>
                                    <tr>
                                        <td><?php echo @$key+1; ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Exibitor_survey/edit/<?php echo @$event_id.'/'.@$question['q_id']; ?>"><?php echo @$question['Question']; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Exibitor_survey/edit/<?php echo @$event_id.'/'.@$question['q_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_survey(<?php echo @$question['q_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
function delete_survey(id)
{   
    if(confirm("Are you sure to delete this?"))
    {
        window.location.href="<?php echo base_url().'Exibitor_survey/delete_question/'.@$event_id.'/'; ?>"+id;
    }
}
</script>