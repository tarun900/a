<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
  <div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-white">
      <div class="tabbable">
        <ul id="myTab2" class="nav nav-tabs">
          <li class="active">
            <a href="#surveylist" data-toggle="tab">
            Add Survey
            </a>
          </li>
          <li class="">
            <a id="view_events1" href="#view_events" data-toggle="tab">
            View Event
            </a>
          </li>
        </ul>
      </div>
      <div class="panel-body" style="padding: 0px;">
        <div class="tab-content">
          <div class="tab-pane fade active in" id="surveylist">
            <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                Question <span class="symbol required"></span>
                </label>
                <div class="col-sm-9">
                  <input type="text" placeholder="Question" id="Question" name="Question" class="form-control name_group required">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                Question Type <span class="symbol required"></span>
                </label>
                <div class="col-sm-9">
                  <select onchange="closediv();" id="Question_type" name="Question_type" class="name_group required">
                  <option value="">Select Question Type</option>
                  <option value="1">Multiple Choice (One Answer)</option>
                  <option value="2">Multiple Choice (More than one Answer)</option>
                  <option value="3">Comment Box</option>
                  <option value="4">Star Rating</option>
                  <?php if($user->Role_id != "3" and $user->role_type != '1'): ?>
                  <option value="5">Multiple Text Boxes</option>
                  <option value="6">Date/Time</option>
                  <?php endif; ?>
                  </select> <br>
                </div>
              </div>
              <div id="Question_option"> 
                <div class="form-group">
                  <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                  Add More 
                  </label>
                  <div class="col-sm-9">
                    <a class="btn btn-blue" href="javascript: void(0);" onclick="addmoreoption()" ><i class="fa fa-plus fa fa-white"></i></a>
                  </div>
                </div>
                <div id="option_container">
                  <div class="form-group">
                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                    Option <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                      <input type="text" style="margin-bottom:15px;" placeholder="Option" id="Option" name="Option[]" class="form-control required name_group">
                      <a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group" id="show_commmenttickbox_div" style="margin-bottom:0px;display: none;">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">    </label>
                <label class="col-sm-9">
                  <input type="checkbox" name="show_commentbox" id="show_commentbox" value="1">
                  <span>Add an "Other" Answer Option for Comments</span>
                </label>
              </div>
              <div id="comment_display_style_option_div" style="margin-left: 17%;display: none;">
                <div class="radio">
                  <label>
                    <input type="radio" name="commentbox_display_style" value="0">
                    <span>Display as answer choice</span>
                  </label>  
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="commentbox_display_style" value="1">
                    <span>Display as comment field</span>
                  </label>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-1" style="text-align:left;padding-top: 1.2%;" for="form-field-1"> 
                  Label: </label>
                  <div class="col-sm-5">
                    <input type="text" name="commentbox_label_text" id="commentbox_label_text" class="form-control name_group">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                </label>
                <div class="col-md-4">
                  <button class="btn btn-yellow btn-block" type="submit">
                  Submit <i class="fa fa-arrow-circle-right"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
            <div id="viewport" class="iphone">
              <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"></iframe>
            </div>
            <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
            <div id="viewport_images" class="iphone-l" style="display:none;">
              <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var count=0;
function addmoreoption()
{
  count++;
  var id="Option"+count;
  var name="Option["+count+"]";
  var html='<div class="form-group"><label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Option <span class="symbol required"></span></label><div class="col-sm-9"><input type="text" style="margin-bottom:15px;" placeholder="Option" id="'+id+'" name="'+name+'" class="form-control required name_group"><a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a></div></div>';
  jQuery("#option_container").append(html);
}
function removeoption(e)
{
  if(jQuery("#option_container div.form-group").length>1)
  {
    jQuery(e).parent().parent().remove();
  }
}
function closediv()
{
  if($("#Question_type").val()!='1' && $("#Question_type").val()!='2' && $("#Question_type").val()!='5')
  {
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $('#show_commmenttickbox_div').hide(); 
    jQuery('#comment_display_style_option_div').hide();
  }
  else
  {
    $("#Question_option").css('display','block');
    $("#option_container #Option").addClass('required');
    $("#option_container #Option").addClass('name_group');

    if($("#Question_type").val()=='1' || $("#Question_type").val()=='2')
    {
      $('#show_commmenttickbox_div').show();
      if (jQuery('#show_commentbox').is(':checked')) 
      {
        jQuery('#comment_display_style_option_div').show();
      }
      else
      {
        jQuery('#comment_display_style_option_div').hide();  
      }
    }
    else
    {
      $('#show_commmenttickbox_div').hide();
      jQuery('#comment_display_style_option_div').hide();
    }
  }
}
jQuery('#show_commentbox').click(function(){
  if (jQuery(this).is(':checked')) 
  {
    jQuery('#comment_display_style_option_div').show();
  }
  else
  {
    jQuery('#comment_display_style_option_div').hide();
  }
});
</script>