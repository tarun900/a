<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="tabbable">
        <ul id="myTab2" class="nav nav-tabs">
          <li class="active">
            <a href="#surveylist" data-toggle="tab">
            Edit Survey
            </a>
          </li>
          <?php if(@$question['Question_type']=='1'  && $user->Role_id != "3"){ ?>
          <li class="">
            <a href="#logic" data-toggle="tab">
              Logic    
            </a>
          </li>
          <?php } ?>
          <li class="">
            <a id="view_events1" href="#view_events" data-toggle="tab">
            View Event
            </a>
          </li>
        </ul>
      </div>
      <div class="panel-body" style="padding: 0px;">
        <div class="tab-content">
          <div class="tab-pane fade active in" id="surveylist">
            <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                  Question <span class="symbol required"></span>
                </label>
                <div class="col-sm-9">
                  <input type="text" placeholder="Question" id="Question" name="Question" value="<?php echo @$question['Question']; ?>" class="form-control name_group required">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                  Question Type <span class="symbol required"></span>
                </label>
                <div class="col-sm-9">
                  <select onchange="closediv();" id="Question_type" name="Question_type" class="name_group required">
                  <option value="">Select Question Type</option>
                  <option value="1" <?php if (@$question['Question_type'] == '1') echo ' selected="selected"'; ?>>Multiple Choice (One Answer)</option>
                  <option value="2" <?php if (@$question['Question_type'] == '2') echo ' selected="selected"'; ?>>Multiple Choice (More than one Answer)</option>
                  <option value="3" <?php if (@$question['Question_type'] == '3') echo ' selected="selected"'; ?>>Comment Box</option>
                  <option value="4" <?php if (@$question['Question_type'] == '4') echo ' selected="selected"'; ?>>Star Rating</option>
                  <?php if($user->Role_id != "3" and $user->role_type != '1'): ?>
                  <option value="5" <?php if (@$question['Question_type'] == '5') echo ' selected="selected"'; ?>>Multiple Text Boxes</option>
                  <option value="6" <?php if (@$question['Question_type'] == '6') echo ' selected="selected"'; ?>>Date/Time</option>
                <?php endif; ?>
                  </select> 
                </div>
              </div>
              <div id="Question_option"> 
                <div class="form-group">
                  <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                  Add More 
                  </label>
                  <div class="col-sm-9">
                    <a class="btn btn-blue" href="javascript: void(0);" onclick="addmoreoption()" ><i class="fa fa-plus fa fa-white"></i></a>
                  </div>
                </div>
                <?php $Option=array_filter(json_decode(@$question['Option'])); ?>
                <div id="option_container">
                  <?php if(!empty($Option)) {$i=1;foreach ($Option as $key=>$val){ ?>
                  <div class="form-group">
                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                      Option <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                      <input type="text" style="margin-bottom:15px;" placeholder="Option" id="Option<?php echo $i; ?>" name="Option[<?php echo $i-1; ?>]" class="form-control required name_group" value="<?php echo $val; ?>">
                      <input type="hidden" name="older_option[<?php echo $i-1; ?>]" value="<?php echo $val; ?>">
                      <a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                  </div>
                  <?php $i++; } } ?>
                </div>
              </div>
              <div class="form-group" id="show_commmenttickbox_div" style="margin-bottom:0px;display: none;">
                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">    </label>
                <label class="col-sm-9">
                  <input type="checkbox" name="show_commentbox" id="show_commentbox" value="1" <?php if(@$question['show_commentbox']=='1'){ ?> checked="checked" <?php } ?>>
                  <span>Add an "Other" Answer Option for Comments</span>
                </label>
              </div>
              <div id="comment_display_style_option_div" style="margin-left: 17%;display: none;">
                <div class="radio">
                  <label>
                    <input type="radio" name="commentbox_display_style" value="0" <?php if(@$question['commentbox_display_style']=='0'){ ?> checked="checked" <?php } ?>>
                    <span>Display as answer choice</span>
                  </label>  
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="commentbox_display_style" value="1" <?php if(@$question['commentbox_display_style']=='1'){ ?> checked="checked" <?php } ?>>
                    <span>Display as comment field</span>
                  </label>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-1" style="text-align:left;padding-top: 1.2%;" for="form-field-1"> 
                  Label: </label>
                  <div class="col-sm-5">
                    <input type="text" name="commentbox_label_text" id="commentbox_label_text" value="<?php echo @$question['commentbox_label_text']; ?>" class="form-control name_group">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                </label>
                <div class="col-md-4">
                  <button class="btn btn-yellow btn-block" type="submit">
                    Update <i class="fa fa-arrow-circle-right"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="logic">
            <form id="skiplogic_form" method="post" class="form-horizontal" action="<?php echo base_url().'Exibitor_survey/save_skiplogic/'.@$event_id.'/'.@$question['q_id']; ?>">
              <?php $skiplogic_option=array_column(@$skiplogic,'option');
              $skiplogic_rqid=array_column($skiplogic,'redirectexibitorquestion_id');
              $skiplogic_data=array_combine($skiplogic_option, $skiplogic_rqid);?>
              <h3 style="text-align: center;"><?php echo @$question['Question']; ?></h3>
              <?php $Option=array_filter(json_decode(@$question['Option'])); 
              foreach ($Option as $key => $ovalue) { ?>
              <div class="col-sm-12 question-skiploginmaindiv">
                <div class="col-sm-4 question-answer-div"><?php echo $ovalue ?></div>
                <div class="col-sm-4 skiplogin-middel-text">If this answer is given, redirect the respondent to</div>
                <div class="col-sm-4">
                  <select name="question_id__<?php echo str_ireplace(" ","_",$ovalue); ?>" id="question_id__<?php echo $ovalue ?>" class="form-control required name_group">
                    <option value="">Select Question</option>
                    <?php foreach (@$questions_list as $key => $value) { if($value['q_id']!=@$question['q_id']){ ?>
                    <option value="<?php echo $value['q_id']; ?>" <?php if($value['q_id']==$skiplogic_data[str_ireplace(" ","_",$ovalue)]){ ?> selected="selected" <?php } ?>><?php echo $value['Question']; ?></option>
                    <?php } } ?>
                  </select>
                </div>
              </div>
              <?php } ?>
              <div class="col-md-3" style="margin-bottom: 15px;padding: 0px;">
                <button id="submit_skiplogic_btn" class="btn btn-yellow btn-block" type="submit">
                  Update <i class="fa fa-arrow-circle-right"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;">
            <div id="viewport" class="iphone">
              <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"></iframe>
            </div>
            <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
            <div id="viewport_images" class="iphone-l" style="display:none;">
              <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
var count="<?php echo count($Option); ?>";
function addmoreoption()
{
  count++;
  var id="Option"+count;
  nm=count-1;
  var name="Option["+nm+"]";
  var html='<div class="form-group"><label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Option <span class="symbol required"></span></label><div class="col-sm-9"><input type="text" style="margin-bottom:15px;" placeholder="Option" id="'+id+'" name="'+name+'" class="form-control required name_group"><a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a></div></div>';
  jQuery("#option_container").append(html);
}
function removeoption(e)
{
  if(jQuery("#option_container div.form-group").length>1)
  {
    jQuery(e).parent().parent().remove();
  }
}
function closediv()
{
  if($("#Question_type").val()!='1' && $("#Question_type").val()!='2' && $("#Question_type").val()!='5')
  {
    $("#Question_option").css('display','none');
    $("#option_container #Option").removeClass('required');
    $("#option_container #Option").removeClass('name_group');
    $('#show_commmenttickbox_div').hide(); 
    jQuery('#comment_display_style_option_div').hide();
  }
  else
  {
    $("#Question_option").css('display','block');
    $("#option_container #Option").addClass('required');
    $("#option_container #Option").addClass('name_group');
    if($("#Question_type").val()=='1' || $("#Question_type").val()=='2')
    {
      $('#show_commmenttickbox_div').show();
      if (jQuery('#show_commentbox').is(':checked')) 
      {
        jQuery('#comment_display_style_option_div').show();
      }
      else
      {
        jQuery('#comment_display_style_option_div').hide();  
      }
    }
    else
    {
      $('#show_commmenttickbox_div').hide();
      jQuery('#comment_display_style_option_div').hide(); 
    }
  }
}
jQuery('#show_commentbox').click(function(){
  if (jQuery(this).is(':checked')) 
  {
    jQuery('#comment_display_style_option_div').show();
  }
  else
  {
    jQuery('#comment_display_style_option_div').hide();
  }
});
closediv();
$('#skiplogic_form').validate({
  errorElement: "label",
  errorClass: 'help-block',  
  rules: {},
  highlight: function (element) 
  {
    $(element).closest('.help-block').removeClass('valid');
    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
  },
  unhighlight: function (element) {
    $(element).closest('.form-group').removeClass('has-error');
  }
});
</script>
<style type="text/css">
.question-skiploginmaindiv{
  background: #DEDEDE;
  padding-top: 5px;
  padding-bottom: 5px;
  color: #000;
  border: 1px solid #D0D0D0;
  margin: 0 0 10px 0;
}
.question-answer-div{
  padding-top: 8px;
  padding-bottom: 8px;
}
.skiplogin-middel-text{
  padding-top: 8px;
  padding-bottom: 8px;
  color: #007CBA;
}
</style>