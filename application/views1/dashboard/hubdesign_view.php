<?php $acc_name = $this->session->userdata('acc_name');
$user = $this->session->userdata('current_user'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#hub_setting" data-toggle="tab">
                            Hub Setting
                        </a>
                    </li>
                    <li>
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            Preview
                        </a>
                    </li>
                </ul>
            </div>
        	<div class="panel-body">
            <div class="tab-content">    
                <?php if(!empty($this->session->flashdata('error'))){ ?>
                <div class="alert alert-block alert-danger fade in">
                    <button class="close" type="button" data-dismiss="alert"> × </button>
                    <h4 class="alert-heading"><i class="fa fa-times"></i> Error Messages</h4>
                    <p id="error_msg"><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php } ?>
                <?php if(!empty($this->session->flashdata('msg'))){ ?>
                <div class="alert alert-block alert-success fade in">
                    <button class="close" type="button" data-dismiss="alert"> × </button>
                    <h4 class="alert-heading"><i class="fa fa-check"></i> Success Messages</h4>
                    <p id="error_msg"><?php echo $this->session->flashdata('msg'); ?></p>
                </div>
                <?php } ?>
                <div class="tab-pane fade active in" id="hub_setting">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Hub Url
                        </label>
                        <div class="col-sm-9">
                            <a href="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"><?php echo base_url(); ?>Hub/<?php echo $acc_name; ?></a>
                        </div>
                    </div>
                	<div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Hub Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Hub Name" maxlength="50" id="hub_name" value="<?php echo $hub[0]['hub_name']; ?>" name="hub_name" class="form-control required limited">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Description</label>
                        <div class="col-sm-9">
                            <textarea  id="Description" class="click2edit form-control" name="Description" style="width: 100%;margin-top: 2%;height: 300px;"><?php echo $hub[0]['hub_Description']; ?></textarea>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Top bar background Color 
                        </label>
                        <div class="col-sm-9">
                            <input type="text" value="<?php echo $hub[0]['hub_top_bar_backgroundcolor']; ?>" placeholder="Top bar background color" id="Top_background_color" name="Top_background_color" class="color {hash:true} form-control name_group">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Top bar text Color 
                        </label>
                        <div class="col-sm-9">
                            <input type="text" value="<?php echo $hub[0]['hub_top_bar_text_color']; ?>" placeholder="Top bar text color" id="Top_text_color" name="Top_text_color" class="color {hash:true} form-control name_group">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Logo Image</label>
                        <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:2000px &amp; height:2000px)</em>
                        <div class="col-sm-9">
                            <?php $logo_images_array = json_decode($hub[0]['hub_logo_images']); ?>

                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail"></div>
                                   <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                   <div class="user-edit-image-buttons">
                                        <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                             <input type="file" name="logo_images" id="logo_images">
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                             <i class="fa fa-times"></i> Remove
                                        </a>
                                   </div>
                              </div>
                               <?php if($logo_images_array != '') { ?>
                                <div class="user-edit-image-buttons">
                                    <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_logo();">
                                      <i class="fa fa-times"></i> Delete Image
                                    </a>
                                </div>
                            <?php }  ?>
                            <?php  for($i=0;$i<count($logo_images_array);$i++) { ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $logo_images_array[$i]; ?>">
                                    <input type="hidden" name="old_logo_images[]" value="<?php echo $logo_images_array[$i]; ?>">
                                </div>
                            <?php } ?>                   
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Banner Images</label>
                        <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:2000px &amp; height:2000px)</em>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($hub[0]['hub_images']); ?>
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail"></div>
                                   <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                   <div class="user-edit-image-buttons">
                                        <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                             <input type="file" name="images" id="images">
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                             <i class="fa fa-times"></i> Remove
                                        </a>
                                   </div>
                              </div>
                              <?php if($images_array != '') { ?>
                                <div class="user-edit-image-buttons">
                                    <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_image();">
                                      <i class="fa fa-times"></i> Delete Image
                                    </a>
                                </div>
                            <?php }  ?>
                            <?php for($i=0;$i<count($images_array);$i++) { ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                    <input type="hidden" id="old_images" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                  </div>
                            <?php } ?>    
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1"></label>
                        <div class="col-md-4">
                            <div class="onoffswitch" style="float: left;margin-right:15px;">
                                <input style="display:none !important" <?php if($hub[0]['attendees_active']=='1'){ ?> checked="checked" <?php } ?> type="checkbox" class="onoffswitch-checkbox" name="attendees_active" id="attendees_active" value="1">
                                <label class="onoffswitch-label" for="attendees_active">
                                  <span class="onoffswitch-inner"></span>
                                  <span class="onoffswitch-switch"></span>
                                 </label>
                            </div>
                            <h4 style="margin-top: 1px;"><label class="control-label" for="form-field-1">Active Attendees</label></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1"></label>
                        <div class="col-md-4">
                            <div class="onoffswitch" style="float: left;margin-right:15px;">
                                <input style="display:none !important" <?php if($hub[0]['public_messages_active']=='1'){ ?> checked="checked" <?php } ?> type="checkbox" class="onoffswitch-checkbox" name="public_messages_active" id="public_messages_active" value="1">
                                <label class="onoffswitch-label" for="public_messages_active">
                                  <span class="onoffswitch-inner"></span>
                                  <span class="onoffswitch-switch"></span>
                                 </label>
                            </div>
                            <h4 style="margin-top: 1px;"><label class="control-label" for="form-field-1">Active Public Messages</label></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1"></label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>                                    
                        </div>
                    </div>	
                </form>
                </div>
                <div class="tab-pane fade" id="view_events">
                    <div id="viewport" class="iphone">
                        <iframe id="displayframe123" name="displayframe123" height="480" width="320" src="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                    </div>
                </div>
            </div>    
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delete_logo()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Dashboard/delete_hub_logo/";
        }
    }
    function delete_image()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Dashboard/delete_hub_images/";
        }
    }
</script>	    