<?php $acc_name = $this->session->userdata('acc_name');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/newdashboard.css?' . time(); ?>">
<?php if ($this->data['user']->Role_name == "Administrator1") {?>
<div class="col-md-6">
    <div class="row space20">
        <div class="col-md-7 col-lg-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <i class="clip-menu"></i>
                    <h4>Most Recent Organizer</h4>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body panel-scroll height-300">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($recentclient); $i++) {?>
                                    <tr>
                                        <td><?php echo $recentclient[$i]['Firstname']; ?></td>
                                        <td><?php echo $recentclient[$i]['Email']; ?></td>
                                        <td><span class="label label-sm
                                            <?php if ($recentclient[$i]['Active'] == '1') {?>
                                                  label-success
                                                <?php } else {?>
                                                  label-danger

                                                <?php }?>">

                                                <?php if ($recentclient[$i]['Active'] == '1') {
    echo "Active";

} else {
    echo "Inactive";
}
    ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php if ($i == 4) {break;}}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="row space20">
        <div class="col-md-7 col-lg-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <i class="clip-menu"></i>
                    <h4>Most Recent Attendees</h4>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body panel-scroll height-300">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($recentattendee); $i++) {?>
                                    <tr>
                                        <td><?php echo $recentattendee[$i]['Firstname']; ?></td>
                                        <td><?php echo $recentattendee[$i]['Email']; ?></td>
                                        <td>
                                            <span class="label label-sm
                                                <?php if ($recentattendee[$i]['Active'] == '1') {?>
                                                      label-success
                                                    <?php } else {?>
                                                       label-danger

                                                    <?php }?>">
                                                    <?php if ($recentattendee[$i]['Active'] == '1') {
    echo "Active";

} else {
    echo "Inactive";
}
    ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php if ($i == 4) {break;}}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-white">
        <div class="panel-heading border-light">
            <h4 class="panel-title">Event Allocation</h4>
    </div>
        <div class="panel-body no-padding">
            <div class="col-md-9 col-lg-10 no-padding partition-white">
                <div class="partition">
                    <div class="partition-body padding-15">
                        <div class="height-300">
                            <div id="chart1" class='with-3d-shadow with-transitions'>
                                <svg></svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.min.css" />
<div class="col-lg-12">
	<div class="row space20">
		<div class="col-md-12" style="padding-left:0px;padding-right:0px;">
			<div class="panel-scroll height-320 panel-white ps-container" style="padding-right:0px !important;height: auto;">
				<?php if ($this->session->flashdata('speaker_data')) {?>
			    <div class="errorHandler alert alert-success no-display" style="display: block;">
			        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_templates_data'); ?> Successfully.
			    </div>
			    <?php }?>
			    <?php if ($this->data['user']->Role_name == 'Client') {?>
			    <div class="col-md-2 pull-right">
			        <a style="top: 7px;right:4%;" class="btn btn-green list_page_btn" href="<?php echo base_url(); ?>Event/add"><i class="fa fa-plus"></i> Create New App</a>
			    </div>
     			<?php }?>
     			<div class="tabbable">
     				<ul id="myTab2" class="nav nav-tabs">
				        <li class="">
				            <a href="#archive_events" data-toggle="tab">
				                Archived Apps
				            </a>
				        </li>
				        <li class="active">
				            <a href="#latest_events" data-toggle="tab">
				                Active Apps
				            </a>
				        </li>
				        <li class="">
				            <a href="#feature_events" data-toggle="tab">
				                Future Apps
				            </a>
				        </li>
				    </ul>
				    <div class="tab-content">
        				<div class="tab-pane fade event_owl_div_list" id="archive_events">
                            <?php if (count($Archive_event) > 0) {?>
        					<div id="owl_archive_events" class="owl-carousel owl-theme event_list_loop">
                                <?php foreach ($Archive_event as $key => $value) {
    if ($this->data['user']->Role_id == 7) {
        $link = base_url() . 'Speaker_Messages/index/' . $value['Id'];
    } else if ($this->data['user']->Role_id == 6) {
        $link = base_url() . 'Exhibitor_portal/index/' . $value['Id'];
    } else {
        $link = base_url() . 'Event/eventhomepage/' . $value['Id'];
    }?>
                                <div class="event_list">
                                    <h2 style="text-align: center;"><?php echo ucfirst($value['Event_name']); ?></h2>
                                    <div class="event_logo_image_div">
                                    <?php if (!empty($value['Logo_images'])) {?>
                                    <img align="center" width="100" height="100" src="<?php echo base_url() . 'assets/user_files/' . $value['Logo_images']; ?>">
                                    <?php }?>
                                    </div>
                                    <div class="row"><p><?php echo substr(strip_tags($value['Description']), 0, 50); ?></p></div>
                                    <div class="col-sm-9">
                                        <a href="<?php echo $link; ?>" class="btn btn-green btn-block">Open in the CMS</a>
                                    </div>
                                    <?php if ($this->data['user']->Role_id == '3') {?>
                                    <div class="col-sm-9">
                                        <a href="javascript:void(0)" class="btn btn-blue btn-block" data-toggle="modal" data-target="#duplicate_event_models" onclick='show_app_name("<?php echo ucfirst($value['Event_name']); ?>","<?php echo ucfirst($value['Id']); ?>");'>Duplicate This App</a>
                                    </div>
                                    <?php }?>
                                    <div class="col-sm-9">
                                        <?php if ($this->data['user']->Role_id == '3' && $hub_active == '1' && $is_hub_created == '1') {?>
                                        <?php if ($value['is_add_hub'] == '0') {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-success btn-block">Add To Your Hub</a>
                                        <?php } else {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-red btn-block">Remove From Your Hub</a>
                                        <?php }}?>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <?php } else {?>
                                <h2 style="text-align: center;">No Archived Apps</h2>
                            <?php }?>
        				</div>
        				<div class="tab-pane active event_owl_div_list" id="latest_events">
                            <?php if (count($Event) > 0) {?>
        					<div id="owl_latest_events" class="owl-carousel owl-theme event_list_loop">
        						<?php foreach ($Event as $key => $value) {
    if ($this->data['user']->Role_id == 7) {
        $link = base_url() . 'Speaker_Messages/index/' . $value['Id'];
    } else if ($this->data['user']->Role_id == 6) {
        $link = base_url() . 'Exhibitor_portal/index/' . $value['Id'];
    } else {
        $link = base_url() . 'Event/eventhomepage/' . $value['Id'];
    }?>
        						<div class="event_list">
        							<h2 style="text-align: center;"><?php echo ucfirst($value['Event_name']); ?></h2>
                                    <div class="event_logo_image_div">
        							<?php if (!empty($value['Logo_images'])) {?>
        							<img align="center" width="100" height="100" src="<?php echo base_url() . 'assets/user_files/' . $value['Logo_images']; ?>">
        							<?php }?>
                                    </div>
        							<div class="row"><p><?php echo substr(strip_tags($value['Description']), 0, 50); ?></p></div>
        							<div class="col-sm-9">
        								<a href="<?php echo $link; ?>" class="btn btn-green btn-block">Open in the CMS</a>
        							</div>
                                    <?php if ($this->data['user']->Role_id == '3') {?>
        							<div class="col-sm-9">
        								<a href="javascript:void(0)" class="btn btn-blue btn-block" data-toggle="modal" data-target="#duplicate_event_models" onclick='show_app_name("<?php echo ucfirst($value['Event_name']); ?>","<?php echo ucfirst($value['Id']); ?>");'>Duplicate This App</a>
        							</div>
                                    <?php }?>
                                    <div class="col-sm-9">
                                        <?php if ($this->data['user']->Role_id == '3' && $hub_active == '1' && $is_hub_created == '1') {?>
                                        <?php if ($value['is_add_hub'] == '0') {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-success btn-block">Add To Your Hub</a>
                                        <?php } else {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-red btn-block">Remove From Your Hub</a>
                                        <?php }}?>
                                    </div>
        						</div>
        						<?php }?>
        					</div>
                            <?php } else {?>
                                <h2 style="text-align: center;">No Active Apps</h2>
                            <?php }?>
        				</div>
        				<div class="tab-pane fade event_owl_div_list" id="feature_events">
                            <?php if (count($Feature_event) > 0) {?>
        					<div id="owl_feature_events" class="owl-carousel owl-theme event_list_loop">
                                <?php foreach ($Feature_event as $key => $value) {
    if ($this->data['user']->Role_id == 7) {
        $link = base_url() . 'Speaker_Messages/index/' . $value['Id'];
    } else if ($this->data['user']->Role_id == 6) {
        $link = base_url() . 'Exhibitor_portal/index/' . $value['Id'];
    } else {
        $link = base_url() . 'Event/eventhomepage/' . $value['Id'];
    }?>
                                <div class="event_list">
                                    <h2 style="text-align: center;"><?php echo ucfirst($value['Event_name']); ?></h2>
                                    <div class="event_logo_image_div">
                                    <?php if (!empty($value['Logo_images'])) {?>
                                    <img align="center" width="100" height="100" src="<?php echo base_url() . 'assets/user_files/' . $value['Logo_images']; ?>">
                                    <?php }?>
                                    </div>
                                    <div class="row"><p><?php echo substr(strip_tags($value['Description']), 0, 50); ?></p></div>
                                    <div class="col-sm-9">
                                        <a href="<?php echo $link; ?>" class="btn btn-green btn-block">Open in the CMS</a>
                                    </div>
                                    <?php if ($this->data['user']->Role_id == '3') {?>
                                    <div class="col-sm-9">
                                        <a href="javascript:void(0)" class="btn btn-blue btn-block" data-toggle="modal" data-target="#duplicate_event_models" onclick='show_app_name("<?php echo ucfirst($value['Event_name']); ?>","<?php echo ucfirst($value['Id']); ?>");'>Duplicate This App</a>
                                    </div>
                                    <?php }?>
                                    <div class="col-sm-9">
                                        <?php if ($this->data['user']->Role_id == '3' && $hub_active == '1' && $is_hub_created == '1') {?>
                                        <?php if ($value['is_add_hub'] == '0') {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-success btn-block">Add To Your Hub</a>
                                        <?php } else {?>
                                        <a href="<?php echo base_url() . 'Event/showhubmenusetting/' . $value['Id']; ?>" class="btn btn-red btn-block">Remove From Your Hub</a>
                                        <?php }}?>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <?php } else {?>
                                <h2 style="text-align: center;">No Future Apps</h2>
                            <?php }?>
        				</div>
        			</div>
     			</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="duplicate_event_models" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" id="close_popup_button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="text-align: center;">Duplicate <span id="app_name_in_header_of_popup"></span></h4>
            </div>
            <div class="modal-body">
                <div id="form_popup_data">
                <form onsubmit="return checkvalidation();" method="post" action="<?php echo base_url() . 'Dashboard/add_duplicate_event'; ?>">
                <h4>Choose which content you would like to import to your new Duplicated App:</h4>
                <input type="hidden" name="event_id_popup" id="event_id_popup" value="">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-9">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="advertising_active" value="5">
                                        <label class="onoffswitch-label" for="advertising_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Advertising</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Presentations_active" value="9">
                                        <label class="onoffswitch-label" for="Presentations_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Presentations</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Maps_active" value="10">
                                        <label class="onoffswitch-label" for="Maps_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Maps</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Documents_active" value="16">
                                        <label class="onoffswitch-label" for="Documents_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Documents</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Social_active" value="17">
                                        <label class="onoffswitch-label" for="Social_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Social</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Custom_modules_active" value="21">
                                        <label class="onoffswitch-label" for="Custom_modules_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Custom modules</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Notes_active" value="6">
                                        <label class="onoffswitch-label" for="Notes_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Notes</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Photos_active" value="11">
                                        <label class="onoffswitch-label" for="Photos_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Photos</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Private_Message_active" value="12">
                                        <label class="onoffswitch-label" for="Private_Message_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Private Message</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Public_Messages_active" value="13">
                                        <label class="onoffswitch-label" for="Public_Messages_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Public Messages</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Surveys_active" value="15">
                                        <label class="onoffswitch-label" for="Surveys_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Surveys</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Form_Builder_active" value="26">
                                        <label class="onoffswitch-label" for="Form_Builder_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Form Builder</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Twitter_Feed_active" value="44">
                                        <label class="onoffswitch-label" for="Twitter_Feed_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Twitter Feed</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Activity_active" value="45">
                                        <label class="onoffswitch-label" for="Activity_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Activity</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Instagram_Feed_active" value="46">
                                        <label class="onoffswitch-label" for="Instagram_Feed_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Instagram Feed</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Agenda_active" value="1">
                                        <label class="onoffswitch-label" for="Agenda_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Agenda</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Attendees_active" value="2">
                                        <label class="onoffswitch-label" for="Attendees_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Attendees</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Exhibitors_active" value="3">
                                        <label class="onoffswitch-label" for="Exhibitors_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Exhibitors</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Analytics_active" value="8">
                                        <label class="onoffswitch-label" for="Analytics_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Analytics</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Sponsors_active" value="43">
                                        <label class="onoffswitch-label" for="Sponsors_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Sponsors</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="Speaker_active" value="7">
                                        <label class="onoffswitch-label" for="Speaker_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Speaker</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="QA_active" value="50">
                                        <label class="onoffswitch-label" for="QA_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Q&A</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="fav_active" value="49">
                                        <label class="onoffswitch-label" for="fav_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">My Favorites</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="game_active" value="52">
                                        <label class="onoffswitch-label" for="game_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Competition</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="photo_filter_active" value="54">
                                        <label class="onoffswitch-label" for="photo_filter_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Photo Filter</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="badge_scanner_active" value="57">
                                        <label class="onoffswitch-label" for="badge_scanner_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Badge Scanner</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="lead_retrieval_active" value="53">
                                        <label class="onoffswitch-label" for="lead_retrieval_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Lead Retrieval</label>
                                </div>
                                <div class="form-group">
                                    <div class="onoffswitch">
                                        <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="matchmaking_active" value="59">
                                        <label class="onoffswitch-label" for="matchmaking_active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                    <label class="control-label">Matchmaking</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <h4>Name your new App</h4>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" name="event_name_textbox" id="event_name_textbox" value="" class="form-control name_group required">
                                    <span style="display: none;" id="event_name_textbox_error" class="help-block" for="event_name_textbox">This field is required.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-9">
                            <div id="error_modules_copying" style="display: none;">
                                <span>Your Agenda sessions are linked to other modules (Speakers, Maps, Documents or Presentations), are you sure you do not want to duplicate the linked modules with your Agenda?</span>
                                <label><input type="radio" value="1" name="link_missing"> <span>Please duplicate the Agenda with the links missing.</span></label>
                                <label><input type="radio" checked="checked" value="0" name="link_missing"> <span>Please duplicate the Agenda and the other modules that the sessions link to.</span></label>
                            </div>
                            <span>Note: All Content from each module will transfer across automatically. Be careful when choosing any Interactive Features, Analytics and Attendees.</span>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-success btn-block" name="save_duplicate_app" value="Save" id="save_duplicate_app">Save</button>
                        </div>
                    </div>
                </div>
                </form>
                </div>
                <div class="row" id="form_submting_show_msg" style="display: none;">
                    <h2 style="text-align: center;">App Data is Duplicating...</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkvalidation()
    {
        var validform=true;
        var searchIDs =[];
          $("input:checkbox:checked").map(function(){
            searchIDs.push($(this).val());
          }).toArray();
        if($.trim($('#event_name_textbox').val())=="")
        {
            $('#event_name_textbox_error').parent().parent().addClass('has-error');
            $('#event_name_textbox_error').show();
            validform=false;
        }
        else if($.inArray('1',searchIDs)!==-1 && ($.inArray('7',searchIDs) == -1 || $.inArray('10',searchIDs) == -1 || $.inArray('16',searchIDs) == -1 || $.inArray('9',searchIDs) == -1) && $('#error_modules_copying').is(':hidden'))
        {
            $('#error_modules_copying').show();
            validform=false;
        }
        else
        {
            $('#event_name_textbox_error').parent().parent().removeClass('has-error');
            $('#event_name_textbox_error').hide();
            $('#form_popup_data').hide();
            $('#close_popup_button').hide();
            $('#form_submting_show_msg').show();
            validform=true;
        }
        return validform;
    }
    $('#event_name_textbox').keydown(function(){
        if($.trim($('#event_name_textbox').val())=="")
        {
            $('#event_name_textbox_error').parent().parent().addClass('has-error');
            $('#event_name_textbox_error').show();
        }
        else
        {
            $('#event_name_textbox_error').parent().parent().removeClass('has-error');
            $('#event_name_textbox_error').hide();
        }
    });
    $("input[type=checkbox]").click(function(){
        if($(this).val()=='1')
        {
            if($('#Agenda_active').is(':checked'))
            {
                $('#Presentations_active').prop('checked',true);
                $('#Maps_active').prop('checked',true);
                $('#Documents_active').prop('checked',true);
                $('#Speaker_active').prop('checked',true);
                $('#QA_active').prop('checked',true);
                $('#Surveys_active').prop('checked',true);
            }
        }
    });
    function show_app_name(eventname,eid) {
        $('#form_popup_data').show();
        $('#close_popup_button').show();
        $('#form_submting_show_msg').hide();
        $('#event_name_textbox').val('');
        $('#event_name_textbox_error').parent().parent().removeClass('has-error');
        $('#event_name_textbox_error').hide();
        $('#app_name_in_header_of_popup').html(eventname);
        $('#event_id_popup').val(eid);
    }
</script>
