<!-- start: PAGE CONTENT -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/event_template.css" />
<?php for ($i = 0; $i < count($map); $i++) { ?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function()
        {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(<?php echo $map[$i]['Lattitude']; ?>, <?php echo $map[$i]['Longitude']; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            var locations;
            var marker, i;
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                });

                google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }

                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                    return function() {

                        infowindow.close();
                    }

                })(marker, i));
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }

                })(marker, i));
            }
        });
    </script>
    
    <!-- <div class="row" style="margin-top: 2%;margin-right: 1%;">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo ucfirst($map[$i]['Event_name']); ?></h4>
                </div>
                <?php if ($map[$i]['Lattitude'] != '') { ?>
                    <div class="panel-body">
                        <div class="map" id="map3">
                            <div class="map" style="height: 320px; width: 100%; background-color: #F0F0F0;" data-address="16 Clocktower Mews, Newmarket, CB8 8LL, UK" data-zoom="12"><div id="map" class="googlemapimage"></div></div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="panel-body">
                        <div class="map" id="map3">
                            <h4 class="panel-title">No Map Found For This Event.</h4>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div> -->
<?php } ?>
<!-- end: PAGE CONTENT-->

<div class="main-ads">
<?php for ($i = 0; $i < count($advertisement_images); $i++) 
{ 
    $string_cms_id = $advertisement_images[0]['Cms_id'];
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images[0]['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if($test = in_array('10', $Menu_id)) {
?>
<?php $image_array = json_decode($advertisement_images[$i]['H_images']);
$h_url = $advertisement_images[0]['Header_link'];
$f_url = $advertisement_images[0]['Footer_link'];
?>
<?php foreach ($image_array as $key => $value) { ?>
    <div class="hdr-ads alert alert-success alert-dismissable">
       <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true">
          &times;
       </button>
       <a class="thumb-info" target="_blank" href="<?php echo $h_url; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery">
            <img style="height:75px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value; ?>" alt="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery">
        </a>
    </div>
<?php } } } ?>
</div>

<div class="row" style="margin-top: 2%;margin-right: 1%;">
    <div class="col-md-12">
        <div class="panel panel-white" style="padding: 15px;">
            <h4 class="panel-title"><?php echo ucfirst($event_templates[$i]['Event_name']); ?></h4>
            <div class="panel-body">
                <?php

                    $domain = $this->uri->segment(2);
                    foreach ($map as $key => $value) 
                    {
                        echo '<a href="'.base_url().'Maps/'.$domain.'/View/'.$value['Id'].'">'.$value['Place'].'</a><br/>';
                    }
                ?>
            </div>
        </div>
    </div>
</div>