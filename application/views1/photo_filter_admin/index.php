<?php $acc_name=$this->session->userdata('acc_name'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/onboarding_css.css<?php echo '?'.time()?>">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <?php if ($this->session->flashdata('filter_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('filter_data'); ?> 
                    </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#filter_options" data-toggle="tab">
                                Filter Options
                            </a>
                        </li>
                        <li class="">
                            <a href="#filters" data-toggle="tab">
                                Filters
                            </a>
                        </li>
                        <li class="">
                            <a href="#photos_taken" data-toggle="tab">
                                Photos Taken
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name And Icon
                            </a>
                        </li>
                        <?php } ?>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="filter_options"> 
                        <h4>Add your Photo Filter here</h4>
                        <!-- <h4>Please upload only Square PNG images</h4> -->
                        <form method="post" action="">
                            <input type="file" name="photo_filter_image" id="photo_filter_image" accept="image/x-png" style="display: none;">
                            <input type="hidden" name="photo_filter_image_crop_data" id="photo_filter_image_crop_data">
                            <div id="o_screen">
                                <div class="extra-greay" style="min-height: 287px; height: auto !important;">
                                    <?php if(!empty($event['photo_filter_image']) && file_exists('./assets/user_files/'.$event['photo_filter_image'])){ ?>
                                    <img width="100%" class="add_extra_images_button" height="auto" src="<?php echo base_url().'assets/user_files/'.$event['photo_filter_image']; ?>">
                                    <?php }else{ ?>
                                    <button type="button" class="add_extra_images_button btn btn-o-screen">
                                        +
                                    </button>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" id="save_photo_filter_images" style="margin: 15px 0px;" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="filters"> 
                        <div class="table-responsive custom_checkbox_table" style="overflow-x:auto;">
                            <a href="<?php echo base_url(); ?>photo_filter_admin/addFilter/<?=$this->uri->segment(3)?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">Add Filter</a>
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Filter</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($filter as $key => $value):?>
                                        <tr>
                                            <td><?=++$i;?></td>
                                            <td><?=$value['title']?></td>
                                            <td><img src="<?=base_url()?>assets/photo_filter/<?=$this->uri->segment(3).'/'.$value['image']?>" style="height: 70px;"></td>
                                            <td>
                                                <a href="javascript:;" onclick="delete_filter(<?=$value['id']?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="photos_taken"> 
                        <div class="table-responsive custom_checkbox_table" style="overflow-x:auto;">
                            <?php if(count($photos_taken)){ ?>
                            <a href="<?php echo base_url(); ?>photo_filter_admin/exportUploadedFiltersPhotos/<?=$this->uri->segment(3)?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">Export</a>
                            <?php } ?>
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Attendee</th>
                                        <th>Image</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($photos_taken as $key => $value):?>
                                        <tr>
                                            <td><?=++$key;?></td>
                                            <td><?=$value['Firstname'].' '.$valuu['Lastname']?></td>
                                            <td><img src="<?=base_url()?>assets/photo_filter_uploads/<?=$this->uri->segment(3).'/'.$value['image']?>" style="height: 70px;"></td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form1" method="POST" action="<?php echo base_url(); ?>Menu/index/<?php echo $event_id ?>" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                   <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="headermodelscopetool" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Photo Filter Image</h4>
            </div>
            <div class="modal-body">
                <div class="row"  id="cropping_banner_div">
                    <div>
                        <img class="img-responsive" id="show_photo_filter_image_preview_in_modal" src="" alt="Picture">
                    </div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $bannerimage = $("#show_photo_filter_image_preview_in_modal");
var $inputBannerImage = $('#photo_filter_image'); 
$('.add_extra_images_button').click(function(){
    $('#photo_filter_image').trigger('click');
});
$('#save_photo_filter_images').click(function(){
    $(this).html('Submitting <i class="fa fa-refresh fa-spin"></i>');
})
$(document).ready(function(){
    $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) 
        {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) 
            {
                if(file.type=="image/png")
                {  
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }
                    uploadedImageURL = URL.createObjectURL(file);
                    $bannerimage.attr('src', uploadedImageURL);
                    $('#photo_filter_image_crop_data').val('');
                    $('#headermodelscopetool').modal('toggle');
                }
                else
                {
                    $inputBannerImage.val('');
                    window.alert('Please choose only Png image file.');      
                }
            } 
            else 
            {
                $inputBannerImage.val('');
                window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
            //aspectRatio: 9 / 16,
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
            //croppedCanvas = $bannerimage.cropper("getCroppedCanvas",{ 'width': 1280, 'height': 2208});
            $('.extra-greay').html("<img width='100%' height='auto' src='"+croppedCanvas.toDataURL()+"'>");
            $('#photo_filter_image_crop_data').val(croppedCanvas.toDataURL());
            $("#photo_filter_image").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    }); 
});
function delete_filter(id)
{   
    <?php $event_id = $this->uri->segment(3); ?>
    if(confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url(); ?>photo_filter_admin/deleteFilter/"+<?php echo $event_id; ?>+"/"+id;
    }
}
</script>
<!-- end: PAGE CONTENT-->