<?php if (!defined('BASEPATH')) die();

/**
 * This is a complete rewrite to use the Mandrill API instead of the
 * weird thing that OpenCart people patched up in here.
 *
 * Inspired by Mindrill
 * @link https://github.com/darrenscerri/Mindrill
 * 
 * @author standa
 * @version 2014-03-16
 * @link https://mandrillapp.com/api/docs/messages.JSON.html#method=send
 */
class Mandrill 
{

    public $protocol = 'mail';
    public $hostname;
    public $username;
    public $password;
    public $port = 25;
    public $timeout = 5;
    public $newline = "\n";
    public $crlf = "\r\n";
    public $verp = false;
    public $parameter = '';

    protected $to;
    protected $bcc;
    protected $from;
    protected $sender;
    protected $subject;
    protected $text;
    protected $html;
    protected $attachments = array();
    protected $test = false;

    private $apiKey;
    private $trackingDomain;
    private $returnPathDomain;
    private $signingDomain;
    private $base = 'http://mandrillapp.com/api/1.0';
    private $suffix = '.json';

    public function setApiKey($key) {
        $this->apiKey = $key;
    }

    public function setTrackingDomain($trackingDomain) {
        $this->trackingDomain = $trackingDomain;
    }

    public function setReturnPathDomain($returnPathDomain) {
        $this->returnPathDomain = $returnPathDomain;
    }

    public function setSigningDomain($signingDomain) {
        $this->signingDomain = $signingDomain;
    }

    public function setTo($to) {
        $this->to = $to;
    }

    public function setBcc($bcc) {
        $this->bcc = $bcc;
    }

    public function setFrom($from) {
        $this->from = $from;
    }

    public function setSender($sender) {
        $this->sender = $sender;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setText($text) {
        $this->text = $text;
    }

    public function setHtml($html) {
        $this->html = $html;
    }

    public function setTest($test) {
        $this->test = (bool)$test;
    }

    public function addAttachment($filename) {
        $this->attachments[] = $filename;
    }

    /**
     * Send a message using the mandrill API.
     * 
     * @link https://mandrillapp.com/api/docs/messages.JSON.html#method=send
     */
    public function send() {
        if (!$this->to && !$this->bcc) {
            trigger_error('Error: E-Mail to or bcc required!');
            exit();         
        }

        if (!$this->from) {
            trigger_error('Error: E-Mail from required!');
            exit();                 
        }

        if (!$this->sender) {
            trigger_error('Error: E-Mail sender required!');
            exit();                 
        }

        if (!$this->subject) {
            trigger_error('Error: E-Mail subject required!');
            exit();                 
        }

        if ((!$this->text) && (!$this->html)) {
            trigger_error('Error: E-Mail message required!');
            exit();                 
        }
        $params['html']             = $this->html;
        $params['text']             = $this->text;
        $params['subject']    = $this->subject;
        $params['from_email'] = $this->from;
        $params['from_name']  = $this->sender;

        if (is_array($this->to)) {
            foreach ($this->to as $t) {
                $params['to'][] = array(
                    'email' => $t, 
                    'name'  => ucwords(str_replace('.', ' ', substr($t, 0, strpos($t, '@')))),
                    'type'  => 'to'
                );              
            }
        } else if (is_string($this->to)) {
            $params['to'][] = array(
                'email' => $this->to, 
                'name'  => ucwords(str_replace('.', ' ', substr($this->to, 0, strpos($this->to, '@')))),
                'type'    => 'to'
            );
        }

        // $params['headers']['Reply-To']   = $this->from;
        $params['important']            = false;
        $params['track_opens']          = true;
        $params['track_clicks']         = true;
        $params['auto_html']            = true;
        $params['auto_text']            = true;
        $params['inline_css']           = true;     
        $params['url_strip_qs']         = true;
        $params['preserve_recipients']  = false;
        $params['view_content_link']    = false;
        // $params['bcc_address']       = 'stanislav.jasa@abyto.com';
        
        if (is_array($this->bcc)) {
            foreach ($this->bcc as $t) {
                $params['to'][] = array(
                    'email' => $t, 
                    'name'  => ucwords(str_replace('.', ' ', substr($t, 0, strpos($t, '@')))),
                    'type'  => 'bcc'
                );             
            } // foreach
        } else if (is_string($this->bcc)) {
            $params['to'][] = array(
                'email' => $this->bcc, 
                'name'  => ucwords(str_replace('.', ' ', substr($this->bcc, 0, strpos($this->bcc, '@')))),
                'type'  => 'bcc'
            );                 
        }// if

        $params['tracking_domain']      = empty($this->trackingDomain) ? null : $this->trackingDomain;
        $params['signing_domain']       = empty($this->signingDomain) ? null : $this->signingDomain;
        $params['return_path_domain']   = empty($this->returnPathDomain) ? null : $this->returnPathDomain;
        $params['merge']                = true;
        // $params['global_merge_vars'] = null;
        // $params['merge_vars']           = array();
        $params['tags']  = array($_SERVER['HTTP_HOST']);
        // $params['google_analytics_domains']  = array($_SERVER['HTTP_HOST']);
        // $params['google_analytics_campaign'] = null;
        $params['metadata']             = array('website' => $_SERVER['HTTP_HOST']);
        $params['reciepent_metadata']   = null;
        $params['attachments']          = empty($this->attachments) ? null: array();
        $params['images']               = null;
        $params['async']                = false;
        // $params['ip_pool']           = 'Main Pool';
        // $params['send_at']           = 'example send_at';
        show_errors();
        foreach ($this->attachments as $attachment) {

                // $handle = fopen($attachment, 'r');              
                // $content = fread($handle, filesize($attachment)); 
                // fclose($handle);
                $attachment_encoded = base64_encode($attachment);              
                $att['type'] = 'application/plain';
                $att['name'] = 'test.txt';
                // $att['type'] = '';
                $att['content'] = 'ZXhhbXBsZSBmaWxl';
                // j($att);
                $params['attachments'][] = $att;
                /*$img = array("type" => "image/png",
                "name": "IMAGECID",
                "content": "ZXhhbXBsZSBmaWxl")
                $params['images'][] = $att;*/
        }
        // j($params);

        $ret = $this->call('/messages/send', array('message' => $params));
        // $this->log->write(json_encode($ret, JSON_PRETTY_PRINT));
        
        return $ret;
    }

    /**
     * Perform an API call.
     *
     * @param string $url='/users/ping' Endpoint URL. Will automatically add '.json' if necessary (both '/users/ping' and '/users/ping.jso'n are valid)
     * @param array $params=array() An associative array of parameters
     *
     * @return mixed Automatically decodes JSON responses. If the response is not JSON, the response is returned as is
     */
    public function call($url = '/users/ping', $params = array())
    {
        if (is_null($params))
        {
            $params = array();
        }

        $url = strtolower($url);

        if (substr_compare($url, $this->suffix, -strlen($this->suffix), strlen($this->suffix)) !== 0)
        {
            $url .= '.json';
        }

        $params = array_merge($params, array('key' => $this->apiKey));

        $json = json_encode($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->base}{$url}");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                                                                           
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($json)));

        $result = curl_exec($ch);
        curl_close($ch);
        $decoded = json_decode($result);

        return is_null($decoded) ? $result : $decoded;

    }

    /**
     * Tests the API using /users/ping
     *
     * @return bool Whether connection and authentication were successful
     */
    public function test()
    {
        return $this->call('/users/ping') === 'PONG!';
    }

}