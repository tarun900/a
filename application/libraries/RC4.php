<?php

Class RC4
{
     
    public function decrypt($data)
    {
        $key = '48AB96053D621359EC5495681D8AE6B5';
        $mtxt   = $this->hexToChars($data);
        $mkey   = $this->strToChars(str_split($key));
        $result = $this->calculate($mtxt, $mkey);
        return $this->charsToStr($result);
    }
    public function encrypt($b){
      $a = '48AB96053D621359EC5495681D8AE6B5';
      for($i,$c;$i<256;$i++)$c[$i]=$i;
      for($i=0,$d,$e,$g=strlen($a);$i<256;$i++){
         $d=($d+$c[$i]+ord($a[$i%$g]))%256;
         $e=$c[$i];
         $c[$i]=$c[$d];
         $c[$d]=$e;
      }
      for($y,$i,$d=0,$f;$y<strlen($b);$y++){
         $i=($i+1)%256;
         $d=($d+$c[$i])%256;
         $e=$c[$i];
         $c[$i]=$c[$d];
         $c[$d]=$e;
         $f.=chr(ord($b[$y])^$c[($c[$i]+$c[$d])%256]);
      }
      return bin2hex($f);
   }
    function calculate($plaintxt, $psw)
    {
        $b              = 0;
        $psw_length     = count($psw);
        $my_key         = array();
        $sbox           = array();
        for ($i = 0; $i <= 255; $i++)
        {
            $my_key[$i]  = $psw[($i % $psw_length)];
            $sbox[$i]    = $i;
        }
        for ($a = 0; $a <= 255; $a++)
        {
            $b          = ($b + $sbox[$a] + $my_key[$a]) % 256;
            $tempSwap   = $sbox[$a];
            $sbox[$a]   = $sbox[$b];
            $sbox[$b]   = $tempSwap;
        }
        $i      = 0;
        $j      = 0;
        $cipher = array();
        for ($a = 0; $a < count($plaintxt); $a++)
        {
            $i          = ($i + 1) % 256;
            $j          = ($j + $sbox[$i])%256;
            $temp       = $sbox[$i];
            $sbox[$i]   = $sbox[$j];
            $sbox[$j]   = $temp;
            $idx        = ($sbox[$i] + $sbox[$j]) % 256;
            $k          = $sbox[$idx];
            $cipherby   = $plaintxt[$a] ^ $k;
            array_push($cipher, $cipherby);
        }
        return $cipher;
    }
    function hexToChars($hex)
    {
        $chars = array();
        for ($i = (substr($hex, 0, 2) == "0x" ? 2 : 0); $i < strlen($hex); $i += 2)
        {
            array_push($chars, intval(substr($hex, $i,  2), 16));
        }
        return $chars;
    }
    function charsToStr($chars)
    {
        $str = "";
        for ($i = 0; $i < count($chars); $i++)
        {
            $str .= chr($chars[$i]);
        }
        return $str;
    }
    function strToChars($str)
    {
        $chars = array();
        foreach($str as $ord)
        {
            array_push($chars, ord($ord));
        }
        return $chars;
    }
}