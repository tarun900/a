<?php class Gcmr{
    
    protected $aitl_api_key;
    protected $event_api_key;
    protected $path;
    protected $event_id;
    protected $aitl_tokens;
    protected $event_tokens;

    public function __construct($event_id=NULL)
    {   
        $this->ci=& get_instance();
        $this->ci->load->database(); 
        // aitl key
        $this->aitl_api_key = $this->event_api_key = 'AAAAfLzDgzs:APA91bHfG3oJBrhV6u8FR6eNOjp-KbNg-Vri0HsAfU9I3tkxIbw5UOr-2RH6k9ZUlJN0ItluOIg_q3-XoMhIUuNZ0QDhtlRj1CIEpe1aDkpP57KiECQPex61B5w1oASpHVwgjvGHPuPT';
        // $this->aitl_api_key = $this->event_api_key = 'AAAAHQoWUXY:APA91bElYFrkU4J3G-0GSSK41scn5nC91sS2-9XOtX1YJQYH52vLZ8RIw9_JRFfgsIqDaXiT8lX3pSlzyNYj3F2wik08U5-cW-calg3zxvjbsH06svNwYmeZXm05UnwwOrvuEjkOmqX5';
        $this->path             = "https://fcm.googleapis.com/fcm/send";
        $this->event_id = $event_id;
        $this->event_tokens = $this->aitl_tokens = [];
        date_default_timezone_set('UTC');
    }

    public function send_notification($registatoin_id=NULL, $message=NULL,$extra="",$device="",$profile='',$ttl=-1) 
    {
        if(empty($registatoin_id))
            return '0';
        elseif(!is_array($registatoin_id))
            $registatoin_id = array($registatoin_id);

        $is_fcm = false;
        $users = $this->ci->db->select('*')
                              ->from('user u')
                              ->where_in('gcm_id',$registatoin_id)
                              ->join('version_code vc','vc.Id = u.version_code_id')
                              ->join('user_login_event ul','ul.user_id = u.Id')
                              ->get()->result_array();  
        foreach ($users as $key => $user) {
            if($user['event_id']  == $this->event_id)
            {
                if($user['is_aitl'] == '0')
                {
                    $this->event_tokens[] = $user['gcm_id'];
                    $this->event_api_key = $user['fcm_key'];
                }
                else
                {
                    $this->aitl_tokens[] = $user['gcm_id'];   
                    $this->aitl_api_key = $user['fcm_key'];
                }
                if($user['is_fcm'] == '1')
                {
                    $is_fcm = true;
                }
            }
            else
            {
                $data['user_id'] = $user['user_id'];
                $data['event_id'] = $this->event_id;
                $data['notification_id'] = $extra['notification_id'];
                $data['sent_time'] = date('Y-m-d H:i:s');
                $data['received_time'] = NULL;
                $data['open_time'] = NULL;
                $data['created_date'] = date('Y-m-d H:i:s');
                $data['updated_date'] = date('Y-m-d H:i:s');
                $data['status'] = '0';
                $tempd[] = $data;
            }
        }    
        if($tempd && !empty($extra['notification_id']))
            $this->ci->db->insert_batch('notification_logs', $tempd); 

        if($is_fcm)
            return $this->sendFcm($registatoin_id, $message,$extra,$device,$profile,$this->event_id,$ttl);
        else
        
            return $this->sendGcm($registatoin_id, $message,$extra,$device,$profile,$this->event_id,$ttl);
    }

    public function sendGcm($registatoin_id=NULL, $message=NULL,$extra="",$device="",$profile='',$event_id=null)
    {

        if($device == 'Iphone')
        {
            define( 'apnsHost','gateway.push.apple.com');
            define( 'apnsPort',2195);
            define( 'apnsCert','assets/ios/AllInTheLoop_Live.pem');
            $payload['aps'] = array(
                'alert' => $message, 
                'badge' => 1, 
                'sound' => 'default', 
                "content-available" => "1");
            $payload['extra'] = ($extra) ? $extra : [];
            $payload['profile'] = ($profile) ? $profile : [];
            
            $payload = json_encode($payload);

            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', apnsCert);
            $apns = stream_socket_client('ssl://' . apnsHost . ':' . apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
            
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $registatoin_id)) . chr(0) . chr(strlen($payload)) . $payload;

            $result = fwrite($apns, $apnsMessage);
           
            if (!$result)
            {
                $a = 0;
            }
            else
            {
                $a = $result;
            }
            @socket_close($apns);
            fclose($apns);
            return $result;
            
        }
        else
        {
            define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

            $registrationIds = array($registatoin_id);
                
            $fields = array
            (
                'priority' => 'high',
                'registration_ids'  => $registrationIds,
                'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
            );
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json',
            );
          
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
            curl_setopt($ch,CURLOPT_POST, true );
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);

            return json_decode($result);
        }
    }
    public function sendFcm($tokens=NULL, $message=NULL,$extra="",$device="",$profile='',$event_id=null,$count=NULL,$ttl=NULL)
    {
        //$this->aitl_tokens = $tokens;
        // $this->getEventFCMKey($this->event_id,$tokens);
        // AITL
        // j($tokens);

        if(!empty($this->aitl_tokens))
        $response = $this->curlFcm($this->aitl_tokens, $message,$extra,$device,$profile,$this->event_id,$count,$this->aitl_api_key,$ttl);

        //EVENT
        if(!empty($this->event_tokens))
        $response = $this->curlFcm($this->event_tokens, $message,$extra,$device,$profile,$this->event_id,$count,$this->event_api_key,$ttl);

        return $response;
    }
    public function getEventFCMKey($event_id=NULL,$tokens=NULL)
    {
        $users = $this->ci->db->where_in('gcm_id',$tokens)->get('user')->result_array();

        $fcm = $this->ci->db->where('event_id',$event_id)->get('event_settings')->row_array()['fcm_api_key'];

        $this->event_api_key = (!empty($fcm)) ? $fcm : $this->aitl_api_key;
        
        $this->event_tokens = $this->aitl_tokens = [];
        foreach ($users as $key => $user) {
            if($user['is_aitl'] == '0')
                $this->event_tokens[] = $user['gcm_id'];
            else
                $this->aitl_tokens[] = $user['gcm_id'];   
        }
    }

    public function curlFcm($tokens=NULL, $message=NULL,$extra="",$device="",$profile='',$event_id=null,$count=NULL,$api_key=NULL,$ttl=NULL)
    {   
        $ttl = (int)$ttl;
        $notification = array('title' =>($extra['title']) ? $extra['title'] : $message['title'] ,
                              'body' => is_array($message) && !empty($message['message']) ? $message['message'] : (is_array($message) && $message['vibrate'] == 0) ? $message['message']: $message,
                              'sound' => ($message['vibrate'] == 0) ?  $message['vibrate'] : 'default',
                              'badge' => ($message['vibrate'] == 0) ? '0' : '1' ,
                              "content_available" => "true");
                              
        if($device == 'Android')
        {   
            $extra['msg_type'] = $extra['message_type'];
            unset($extra['message_type']);
            $extra = array_merge($extra,$notification);
            $notification = null;
        }
        if($ttl!=-1)
        {
            $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification,'data' => $extra ,'priority'=>'high','time_to_live'=>$ttl);
        }
        else {
            $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification,'data' => $extra ,'priority'=>'high');
        }
        $json = json_encode($arrayToSend);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $api_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);        
        $response = json_decode($response,true);
        if($extra['notification_id'])
        {
            foreach ($tokens as $key => $value) {
                $user = $this->ci->db->where('gcm_id',$value)->get('user')->row_array();
                if($user)
                {
                    $data['user_id'] = $user['Id'];
                    $data['event_id'] = $this->event_id;
                    $data['notification_id'] = $extra['notification_id'];
                    $data['sent_time'] = date('Y-m-d H:i:s');
                    $data['received_time'] = NULL;
                    $data['open_time'] = NULL;
                    $data['created_date'] = date('Y-m-d H:i:s');
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $data['status'] = (array_key_exists('message_id', $response['results'][$key])) ? '1' : '0';
                    $tempd[] = $data;
                }
            }
            $this->ci->db->insert_batch('notification_logs', $tempd); 
        }
        return $response;
    }
}