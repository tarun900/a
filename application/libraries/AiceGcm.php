<?php
class Gcm{

    public function send_notification($registatoin_id, $message,$extra="",$device="",$profile='') {

    if($device == 'Iphone')
    {
        //define( 'apnsHost','gateway.sandbox.push.apple.com');
        define( 'apnsHost','gateway.push.apple.com');
        define( 'apnsPort',2195);
        //define( 'apnsCert','assets/ios/Aice2017_development.pem');
        define( 'apnsCert','assets/ios/Aice2017_live.pem');

        $payload['aps'] = array(
            'alert' => $message, 
            'badge' => 1, 
            'sound' => 'default', 
            "content-available" => "1");
        $payload['extra'] = ($extra) ? $extra : [];
        $payload['profile'] = ($profile) ? $profile : [];
        
        $payload = json_encode($payload);

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', apnsCert);
        $apns = stream_socket_client('ssl://' . apnsHost . ':' . apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
        
        $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $registatoin_id)) . chr(0) . chr(strlen($payload)) . $payload;

        $result = fwrite($apns, $apnsMessage);
       
        if (!$result)
        {
            $a = 0;
        }
        else
        {
            $a = $result;
        }
        @socket_close($apns);
        fclose($apns);
        return $result;
        
    }
    else
    {

        define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

        $registrationIds = array($registatoin_id);
            
        $fields = array
        (
			'priority' => 'high',
            'registration_ids'  => $registrationIds,
            'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
        );
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json',
        );
      
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
        curl_setopt($ch,CURLOPT_POST, true );
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return json_decode($result);
    }
}
 
}