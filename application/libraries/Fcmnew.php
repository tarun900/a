<?php
class Fcmnew
{
    protected $api_key;

    protected $ios_certificate;

    protected $path;

    public function __construct()
    {
        $this->api_key = 'AAAAHQoWUXY:APA91bElYFrkU4J3G-0GSSK41scn5nC91sS2-9XOtX1YJQYH52vLZ8RIw9_JRFfgsIqDaXiT8lX3pSlzyNYj3F2wik08U5-cW-calg3zxvjbsH06svNwYmeZXm05UnwwOrvuEjkOmqX5';
        $this->ios_certificate  = 'http://www.allintheloop.net/assets/ios/AllInTheLoop.pem';
        $this->path             = "https://fcm.googleapis.com/fcm/send";
    }

    public function ios($arrayToSend)
    {
        $json       = json_encode($arrayToSend);
        $headers    = array();
        $headers[]  = 'Content-Type: application/json';
        $headers[]  = 'Authorization: key= '.$this->api_key;
        $ch         = curl_init($this->path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

        //curl_exec($ch);

        //curl_close($ch);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
        //unset($response1);
        //return $response1;

    }

    public function android($fields)
    {

        $headers    = array(
            'Authorization: key='.$this->api_key,
            'Content-Type: application/json'
        );
        $ch         = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_exec($ch);

        curl_close($ch);
        //return $result;
    }

    public function send($count,$tokens,$message,$extra,$device)
    {   
        if($device == 'android')
        {
            $extra['badge'] = $count;
            $profile = [];
            $fields     = array(
                'to' => $tokens,
                'priority' => 'high',
                'notification' => array('body' => $message, 'extra' => $extra ,'title' => 'test','profile' => $profile),
            );
            $this->android($fields);
        }
        else
        {   
            $profile = [];
            $message = array('title' =>$extra['title'] , 'text' => $message, 'sound' => 'default', 'badge' => '1','vibrate' => 0,'extra' => $extra ,'profile' => $profile);
            
            $arrayToSend    = array('to' => $tokens, 'notification' => $message, 'priority' => 'high');
            $this->ios($arrayToSend);
        }
        //return "";
    }
    public function send_arab($count,$tokens,$message,$extra,$device)
    {   

        $profile = [];
        $message = array('title' =>$extra['title'] , 'text' => $message, 'sound' => 'default', 'badge' => '1','vibrate' => 0,'extra' => $extra ,'profile' => $profile);
        
        $arrayToSend    = array('registration_ids' => $tokens, 'notification' => $message, 'priority' => 'high');
        $this->ios($arrayToSend);
         //return "";
    }
}