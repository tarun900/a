<?php
class Gcm{

       function __construct($event_id)
       {
        $this->CI =& get_instance();
        $this->CI->load->model('API/settings_model');
        $organizer = $this->CI->settings_model->getOranizerData($event_id);
       
        switch ($organizer['acc_name']) 
        {
            case 'activateevents':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Activate_Live.pem');
                break;
            case 'coburns':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/coburns_live.pem');
                break;
            case 'test':
                if($event_id == "426")
                {
                    define( 'apnsHost','gateway.push.apple.com');
                    define( 'apnsCert','assets/ios/AllInTheLoop_Live.pem');
                }
                else
                {
                    define( 'apnsHost','gateway.push.apple.com');
                    define( 'apnsCert','assets/ios/Showcase_Live.pem');
                }
                break;
            case 'DufourCoProductions':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/AndEventsLive_20Jan.pem');
                break;
            case 'EGITINT':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/EIF2017_Live.pem');
                break;
            case 'waterfordhotels':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/WHG2017_Dist_PemFile.pem');
                break;
            case 'gulfood':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Gulfood_live.pem');
                break;
            case 'theconferenceorganizer':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/AsianBreak_Live.pem');
                break;
            case 'flemingevents':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/fleming_live.pem');
                break;
            case 'nationalassociationofrealtors':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Realtor_Live.pem');
                break;
            case 'MertelRGEventsGmbH':
    			define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/ErsteTalentBridge_Live.pem');
                break;
            case 'sdlexpo':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/IdealHomeShow_live.pem');
                break;  
    		case 'maines':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Maines_Live.pem');
                break;
            case 'iamericas':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/LJC2017_Live.pem');
                break;
            case 'EASI':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/EASIID_Live.pem');
                break;
            case 'enactus':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Enactus_live.pem');
                break;
    		case 'sahic':
    			define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Sahic_Developer.pem'); 
                break;
            case 'worldfzo':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Aice2017_live.pem');
                break;
    		case 'KironaUserGroup':
    			define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/kirona_live.pem');
                break;
            case 'MITCenterforRealEstate':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/MIT_Live.pem');
                break;
            case 'ircg':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/TGC_local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/TGC2017_Live.pem');
                break;
            case 'dwtc':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/GisecLive.pem');
                break;
            /*case 'concordia':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Concordia_Live.pem');
                break;*/
            case 'tricord':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Tricord_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Tricord_Live.pem');
                break;
            case 'greentechmedia2':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Greentech_Local.pem');
             	define( 'apnsHost','gateway.push.apple.com');
             	define( 'apnsCert','assets/ios/GTM_LIVE.pem');
                break; 
           case 'tabbgroup':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Fintech_Development.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Fintech_Live.pem');
                break; 
            case 'WarrenAverettEvents':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/WAEvents_Development.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/WAEvents_Live.pem');
                break;
            case 'mdrealtor':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Maryland_realtors_Development.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Maryland_REALTORSLive.pem');
                break;
            case 'AmericasWarriorPartnership':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/WarriorSymposium2017_Development.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/WarriorSymposium2017_Live.pem');
                break;
            case 'realtordotcom':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/RealtorSummit_Live.pem');
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/RealtorsSummit.pem');
                break;
            case 'BluewaterEventsLtd':
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Recruitment_Live.pem');
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Recruitment_Development.pem');
                break;
            case 'K.I.TGroup':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/ADARRC_Development.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/ADARRC_Live.pem');
                break;
            case 'concordia':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/ConcordiyaSummit_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/ConcordiyaAnnual_Live.pem');
                break;
            case 'LASDigitalAgency':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/STPConDevelopementPEM.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Joico_LIVE_5.pem');
                //define( 'apnsCert','assets/ios/STPCONLIVE_Live.pem');
                break;
            case 'crforum':
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/CRForumDevelopementPEM.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/CRForum_Live.pem');
                break;
            case 'gfc': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/BoandsAndLoansDevelopementPEM.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/BoansLoans_Live.pem');
                break;
            case 'SeaLand': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Sealand_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Sealand_Live.pem');
                break;
            case 'SightLine': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/SighLine_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/SighLine_Live.pem');
                break;
            case 'apexhotels': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/Apex_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Apex_Live.pem');
                break;
            case 'dwtc2': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/DubaiMotorShow_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/DIMS2017_Live.pem');
                break;
            case 'EDSC': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/EDSC_LOCAL.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/EDSC2017_Live.pem');
                break;
            case 'NAPEC': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/NAPEC2018_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/Napec2018_Live.pem');
                break;
            case 'closerstill': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/LTA2017_local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/LearningTechnologiesAsia2017_Live.pem');
                break;
            case 'CommunityHealthcareNetwork': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/CHN_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/CHN_Live.pem');
                break;
            case 'osneymedia': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/GemsForumLondon_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/GemsForumLondon_Live.pem');
                break;
            case 'KNect365': 
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/KNect365_Local.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/KNect_Live.pem');
                break; 
            default:
                //define( 'apnsHost','gateway.sandbox.push.apple.com');
                //define( 'apnsCert','assets/ios/AllInTheLoop.pem');
                define( 'apnsHost','gateway.push.apple.com');
                define( 'apnsCert','assets/ios/AllInTheLoop_Live.pem');
                break;

        }
       }
    public function send_notification($registatoin_id, $message,$extra="",$device="",$profile='')
    {
        if($device == 'Iphone')
        {
            define('apnsPort',2195);
     
            $payload['aps'] = array(
                'alert' => $message, 
                'badge' => 1, 
                'sound' => 'default', 
                "content-available" => "1");
            $payload['extra'] = ($extra) ? $extra : [];
            $payload['profile'] = ($profile) ? $profile : [];
            
            $payload = json_encode($payload);
            
            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', apnsCert);
            $apns = stream_socket_client('ssl://' . apnsHost . ':' . apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
            
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $registatoin_id)) . chr(0) . chr(strlen($payload)) . $payload;

            $result = fwrite($apns, $apnsMessage);
           
            if (!$result)
            {
                $a = 0;
            }
            else
            {
                $a = $result;
            }
            @socket_close($apns);
            fclose($apns);
            return $result;
            
        }
        else
        {
            define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

            $registrationIds = array($registatoin_id);
            $fields = array
            (
    			'priority' => 'high',
                'registration_ids'  => $registrationIds,
                'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
            );
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json',
            );
          
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
            curl_setopt($ch,CURLOPT_POST, true );
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return json_decode($result);
        }
    }
    public function send_notification_silent($registatoin_id, $message,$extra="",$device="",$profile='')
    {
        if($device == 'Iphone')
        {
            define( 'apnsPort',2195);
     
            $payload['aps'] = array(
                'alert' => $message, 
                'badge' => 0, 
                'sound' => '',
                'vibrate' => 0,
                "content-available" => "1");
            $payload['extra'] = ($extra) ? $extra : [];
            $payload['profile'] = ($profile) ? $profile : [];
            //print_r($payload);exit;
            $payload = json_encode($payload);

            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', apnsCert);
            $apns = stream_socket_client('ssl://' . apnsHost . ':' . apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
            
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $registatoin_id)) . chr(0) . chr(strlen($payload)) . $payload;

            $result = fwrite($apns, $apnsMessage);
           
            if (!$result)
            {
                $a = 0;
            }
            else
            {
                $a = $result;
            }
            @socket_close($apns);
            fclose($apns);
            return $result;
            
        }
        else
        {
            define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

            $registrationIds = array($registatoin_id);
                
            $fields = array
            (
                'priority' => 'high',
                'registration_ids'  => $registrationIds,
                'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
            );
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json',
            );
          
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
            curl_setopt($ch,CURLOPT_POST, true );
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);

            return json_decode($result);
        }
    }
    public function send_notification_arab($registatoin_id, $message,$extra="",$device="",$profile='')
    {
        define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

        //$registrationIds = array($registatoin_id);
            
        $fields = array
        (
            'priority' => 'high',
            'registration_ids'  => $registatoin_id,
            'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
        );
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json',
        );
      
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
        curl_setopt($ch,CURLOPT_POST, true );
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return json_decode($result);
        // echo $result;
    }
}