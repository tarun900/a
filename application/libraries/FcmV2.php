<?php
class Fcm
{
    protected $api_key;

    protected $ios_certificate;

    protected $path;

    public function __construct()
    {	
    	$this->ci=& get_instance();
        $this->ci->load->database(); 
        $this->api_key = 'AAAAfLzDgzs:APA91bHfG3oJBrhV6u8FR6eNOjp-KbNg-Vri0HsAfU9I3tkxIbw5UOr-2RH6k9ZUlJN0ItluOIg_q3-XoMhIUuNZ0QDhtlRj1CIEpe1aDkpP57KiECQPex61B5w1oASpHVwgjvGHPuPT';
        // $this->api_key = 'AAAAHQoWUXY:APA91bElYFrkU4J3G-0GSSK41scn5nC91sS2-9XOtX1YJQYH52vLZ8RIw9_JRFfgsIqDaXiT8lX3pSlzyNYj3F2wik08U5-cW-calg3zxvjbsH06svNwYmeZXm05UnwwOrvuEjkOmqX5';
        
        $this->ios_certificate  = 'http://www.allintheloop.net/assets/ios/AllInTheLoop.pem';
        $this->path             = "https://fcm.googleapis.com/fcm/send";
    }

    public function ios($arrayToSend=NULL,$event_id=NULL)
    {	
        $json       = json_encode($arrayToSend);
        $headers    = array();
        $headers[]  = 'Content-Type: application/json';
        $headers[]  = 'Authorization: key= '.$this->api_key;
        $ch         = curl_init($this->path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

        //curl_exec($ch);

        //curl_close($ch);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
        //unset($response1);
        return $response1;

    }

    public function android($fields=NULL)
    {

        $headers    = array(
            'Authorization: key='.$this->api_key,
            'Content-Type: application/json'
        );
        $ch         = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_exec($ch);

        curl_close($ch);
        return $result;
    }

    public function send($count=NULL,$tokens=NULL,$message=NULL,$extra=NULL,$device=NULL,$event_id=NULL)
    {   
        $this->getEventFCMKey($event_id,$tokens);
        if($device == 'android')
        {
            $extra['badge'] = $count;
            $profile = [];
            $fields     = array(
                'to' => $tokens,
                'priority' => 'high',
                'notification' => array('body' => $message, 'extra' => $extra ,'title' => 'test','profile' => $profile),
            );
            return $this->android($fields);
        }
        else
        {   
            $profile = [];
            $message = array('title' =>$extra['title'] , 'text' => $message, 'sound' => 'default', 'badge' => '1','vibrate' => 0,'extra' => $extra ,'profile' => $profile);
            
            $arrayToSend    = array('to' => $tokens, 'notification' => $message, 'priority' => 'high');
            return $this->ios($arrayToSend);
        }
        //return "";
    }
    public function send_arab($count=NULL,$tokens=NULL,$message=NULL,$extra=NULL,$device=NULL)
    {   

        $profile = [];
        $message = array('title' =>$extra['title'] , 'text' => $message, 'sound' => 'default', 'badge' => '1','vibrate' => 0,'extra' => $extra ,'profile' => $profile);
        
        $arrayToSend    = array('registration_ids' => $tokens, 'notification' => $message, 'priority' => 'high');
        $this->ios($arrayToSend);
         //return "";
    }

    public function send_silent($count=NULL,$tokens=NULL,$message=NULL,$extra=NULL,$device=NULL,$event_id=NULL)
    {   
        $this->getEventFCMKey($event_id,$tokens);
        $profile = [];
        $message = array('title' =>$extra['title'] , 'text' => $message,'badge' => '0','vibrate' => 0,'extra' => $extra ,'profile' => $profile);
        
        $arrayToSend    = array('to' => $tokens, 'notification' => $message, 'priority' => 'high','content_avilable' => 'true');
        $this->ios($arrayToSend);
    }
    public function getEventFCMKey($event_id=NULL,$tokens=NULL)
    {
    	$user = $this->ci->db->where('gcm_id',$tokens)->get('user')->row_array();
        if($user['is_aitl'] == '0')
        {
            $fcm = $this->ci->db->where('event_id',$event_id)->get('event_settings')->row_array()['fcm_api_key'];
            if(!empty($fcm))
            {     
                $this->api_key = $fcm;
            }
            else
            {   
                $res = $this->ci->db->where('Id',$event_id)->get('event')->row_array();
                $this->ci->db->where('(fcm_api_key IS NOT NULL OR fcm_api_key != "")');
                $this->ci->db->where('(event_id IN (select DISTINCT Event_id from relation_event_user where Organisor_id = '.$res['Organisor_id'].' ))');
                $this->api_key = $this->ci->db->get('event_settings')->row_array()['fcm_api_key'];
            }
    	}
    }
}
