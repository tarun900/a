<?php
class Fcmr
{
    protected $api_key;

    protected $path;

    public function __construct()
    {	
    	$this->ci=& get_instance();
        $this->ci->load->database(); 
        $this->api_key = 'AAAAfLzDgzs:APA91bHfG3oJBrhV6u8FR6eNOjp-KbNg-Vri0HsAfU9I3tkxIbw5UOr-2RH6k9ZUlJN0ItluOIg_q3-XoMhIUuNZ0QDhtlRj1CIEpe1aDkpP57KiECQPex61B5w1oASpHVwgjvGHPuPT';
        // $this->api_key = 'AAAAHQoWUXY:APA91bElYFrkU4J3G-0GSSK41scn5nC91sS2-9XOtX1YJQYH52vLZ8RIw9_JRFfgsIqDaXiT8lX3pSlzyNYj3F2wik08U5-cW-calg3zxvjbsH06svNwYmeZXm05UnwwOrvuEjkOmqX5';
        $this->path             = "https://fcm.googleapis.com/fcm/send";
        date_default_timezone_set('UTC');
    }

    public function send($count,$tokens,$message,$extra,$device,$event_id,$ttl=-1)
    {   
        //$this->getEventFCMKey($event_id,$tokens);
        $ttl = (int)$ttl;
        $profile = [];
        $notification = array('title' =>$extra['title'] , 'body' => $message, 'sound' => 'default', 'badge' => '1' ,"content_available" => "true");
        if($device == 'Android')
        {
            $extra['msg_type'] = $extra['message_type'];
            unset($extra['message_type']);
            $extra = array_merge($extra,$notification);
            $notification = null;
        }
        if($ttl!=-1)
        {
            $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification,'data' => $extra ,'priority'=>'high','time_to_live'=>$ttl);
        }
        else {
            $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification,'data' => $extra ,'priority'=>'high');
        }

        $json = json_encode($arrayToSend);
       
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $this->api_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);       
      
        $response = json_decode($response,true);
        
        foreach ($tokens as $key => $value) {
            $user = $this->ci->db->where('gcm_id',$value)->get('user')->row_array();
            if($user)
            {
                $data['user_id'] = $user['Id'];
                $data['event_id'] = $event_id;
                $data['notification_id'] = $extra['notification_id'];
                $data['sent_time'] = date('Y-m-d H:i:s');
                $data['received_time'] = NULL;
                $data['open_time'] = NULL;
                $data['created_date'] = date('Y-m-d H:i:s');
                $data['updated_date'] = date('Y-m-d H:i:s');
                $data['status'] = (array_key_exists('message_id', $response['results'][$key])) ? '1' : '0';
                $tempd[] = $data;
            }
        }
        $this->ci->db->insert_batch('notification_logs', $tempd); 

        return $response;
    }
    public function getEventFCMKey($event_id,$tokens)
    {
    	$user = $this->ci->db->where_in('gcm_id',$tokens)->get('user')->row_array();
    	if($user['is_aitl'] == '0')
    	{
    		$fcm = $this->ci->db->where('event_id',$event_id)->get('event_settings')->row_array()['fcm_api_key'];
    		if(!empty($fcm))
    		{
    			$this->api_key = $fcm;
    		}
    	}
    }
}
