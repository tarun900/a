<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
error_reporting(0);
/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || 
    $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO'])
    && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') 
{
  $config['base_url'] = 'https://test.allintheloop.net/';
}
else {
  $config['base_url'] = 'https://test.allintheloop.net/';
}
/*else
$config['base_url'] = 'http://www.allintheloop.net/';*/

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = '';
/*$config['api_username'] = 'paypal.adaptive_api1.elsner.in';
$config['api_password'] = 'NRF89SCLWHJ6FECJ';
$config['api_signature'] = 'AdiJgt1oOPN4RFp6jaZhTiGP50t9A2VytRPoZVz5hNDXWUAVXFIiLbya';
$config['test_mode'] = TRUE; //keep false on production, true for test
$config['currency'] = 'USD';*/

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
//$config['uri_protocol']	= 'REQUEST_URI';
$config['uri_protocol'] = 'AUTO';  

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = TRUE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'MY_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';
$config['dateFormat'] = 'Y-m-d';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = 'Admin@123';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= 'ci_session';
$config['sess_expiration']		= 720000;
$config['sess_expire_on_close']	= FALSE;
$config['sess_encrypt_cookie']	= TRUE;
$config['sess_use_database']	= TRUE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= FALSE;
$config['sess_time_to_update']	= 86400;


/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
| 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
|
*/
$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= TRUE;

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = false;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = FALSE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';


/*
User Role Configuration
*/

$config['Role']['Administrator'] = array(
    'forbidden/index',
    'pageaccess/index',
    'thankyou/index',
    'match_section/index',
    'site_setting/index',
    'site_setting/update',
    'attendee/index',
    'attendee/add',
    'attendee/delete',
    'client/index',
    'client/add',
    'client/delete',
    'profile/update',
    'profile/updateclient',
    'profile/index',
    'profile/deleteprofile',
    'event/index',
    'event/delete',
    'event/add',
    'paypal_setting/index',
    'event/edit',
    'event/delete_size',
    'event/search',
    'event/Page',
    'event/clearsearch',
    'event/detail',
    'event/detail_ajax',
    'client/checkemail',
    'setting/index',
    'setting/email_templates',
    'setting/edit',
    'menu/index'
   /* 'exhibitor_user/add',
    'exhibitor_user/edit',
    'exhibitor_user/delete',*/
    );

$config['Role']['Client'] = array(
    'forbidden/index',
    'pageaccess/index',
    'thankyou/index',
    'match_section/index',
    'category/index',
    'category/add',
    'category/edit',
    'category/delete',
    'category/checkcategory',
    'user_permission/index',
    'user_permission/delete',
    'user_permission/add',
    'user_permission/edit',
    'add_attendee/add',
    'event/index',
    'event/delete',
    'paypal_setting/index',
    'paypal_setting/initialize_settings',
    'event/add',
    'event/edit',
    'event/delete_banner',
    'event/delete_logo',
    'event/delete_size',
    'event/search',
    'event/Page',
    'event/clearsearch',
    'event/detail',
    'event/detail_ajax',
    'profile/update',
    'profile/deleteprofile',
    'profile/index',
    'profile/updateclient',
    'notifications/index',  
    'notifications/displaycontent',
    'order/index',
    'order/detail',
    'order/place_order',
    'order/checkout',
    'order/view_staus',
    'order/cancel',
    'order/approve',
    'notifications/unseenmsg',
    'notifications/softdeletenotification',
    'notifications/searchmsg',
    'notifications/anseenmsg',
    'notifications/autonotificationload',
    'attendee/index',
    'attendee/add',
    'attendee/delete',
    'exibitor/index',
    'exibitor/add',
    'exibitor/delete',
    'sponsor/index',
    'sponsor/add',
    'sponsor/delete',
    'advertising/index',
    'advertising/add',
    'advertising/delete',
    'notes/index',
    'notes/add',
    'notes/delete',
    'leader_board/index',
    'leader_board/add',
    'leader_board/delete',
    'presentation/index',
    'presentation/add',
    'presentation/delete',
    'photos/index',
    'photos/add',
    'photos/delete',
    'message/index',
    'message/add',
    'message/delete',
    'message/chats',
    'survey/index',
    'survey/add',
    'survey/delete',
    'survey/hide_survey',
    'document/index',
    'document/add',
    'document/delete',
    'social/index',
    'social/add',
    'social/delete',
    'qr_scanner/index',
    'qr_scanner/add',
    'qr_scanner/delete',
    'speaker/index',
    'speaker/add',
    'speaker/delete',
    'agenda/index',
    'agenda/add',
    'agenda/delete',
    'activity/index',
    'activity/add',
    'advertising/index',
    'advertising/add',
    'advertising/delete',
    'event_template/index',
    'event_template/add',
    'event_template/delete',
    'map/index',
    'map/add',
    'map/delete',
    'subdomain/index',
    'subdomain/add',
    'subdomain/delete',
    'subscription/index',
    'subscription/add',
    'subscription/edit',
    'subscription/delete',
    'subscription/checksubscription',
    'speech/index',
    'speech/delete',
    'speech/add',
    'speech/edit',
    'menu/index'
    /*'exhibitor_user/add',
    'exhibitor_user/edit',
    'exhibitor_user/delete',*/
);

$config['Role']['Attendee'] = array(
    'forbidden/index',
    'pageaccess/index',
    'thankyou/index',
    'match_section/index',
    'event/index',
    'event/delete',
    'event/add',
    'event/edit',
    'event/delete_size',
    'event/details',
    'event/search',
    'profile/update',
    'profile/deleteprofile',
    'profile/index',
    'event/detail',
    'event/detail_ajax',
    'profile/updateclient',
    'event/check_availablity',
    'notifications/index',  
    'notifications/displaycontent',
    'order/index',
    'notifications/unseenmsg',
    'notifications/softdeletenotification',
    'notifications/searchmsg',
    'notifications/anseenmsg',
    'notifications/autonotificationload',
    'event/clearsearch',
    'message/deletemssage',
    'doccategory/index'
    /*'exhibitor_user/add',
    'exhibitor_user/edit',
    'exhibitor_user/delete',*/
);


$config['Role']['Speaker'] = array(
    'speech/index',
    'speech/delete',
    'speech/add',
    'speech/edit',
);
$config['ip'] = "199.83.214.85";
$config['port'] = 6789;

date_default_timezone_set('UTC');
/* End of file config.php */
/* Location: ./application/config/config.php */
