<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';
$route['(?i)app/(:any)/preview'] = "app/preview/$1";
$route['(?i)app/(:any)/logindemo']="app/logindemo/$1";
$route['(?i)app/(:any)'] = "app/index/$1";
$route['(?i)app/(:any)/(:any)'] = "app/index/$1/$2";
$route['Cms/(:any)/View/(:any)'] = "Cms/View/$1/$2/$3";
	


//$route['Messages/(:any)'] = "Messages/chats/$1";

$route['Registration/(:any)/(:any)'] = "Registration/index/$1/$2";
$route['Agenda/(:any)/View/(:any)'] = "Agenda/View/$1";
$route['Agenda/(:any)/View_agenda/(:any)'] = "Agenda/View_agenda/$1/$2";
$route['Agenda/(:any)/(:any)/view_user_agenda'] = "Agenda/view_user_agenda/$2/$1";
$route['Agenda/(:any)/user_agenda_update'] = "Agenda/user_agenda_update";
$route['Agenda/(:any)/add_advertise_hit'] = "Agenda/add_advertise_hit";
$route['Agenda/(:any)/getpushnoti'] = "Agenda/getpushnoti";
$route['Agenda/(:any)'] = "Agenda/index/$1";


$route['Attendee/(:any)/View/(:any)'] = "Attendee/View/$1/$2";
$route['Attendee/(:any)'] = "Attendee/index/$1";


$route['Exhibitors/(:any)/View/(:any)'] = "Exhibitors/View/$1/$2";
$route['Exhibitors/(:any)'] = "Exhibitors/index/$1";


//$route['Agenda/(:any)/index/(:any)/(:any)'] = "Agenda/index/$3/$2/$1";

//192.168.1.28/EventApp/Agenda/test/demo
$route['Speakers/(:any)/upload_imag'] = "Speakers/upload_imag/$1/$2";
$route['Speakers/(:any)/upload_imag/(:any)'] = "Speakers/upload_imag/$1/$2";
/*$route['Speakers/(:any)/upload_commentimag'] = "Speakers/upload_commentimag/$1";*/
$route['Speakers/(:any)/upload_commentimag/(:any)'] = "Speakers/upload_commentimag/$1/$2";
$route['Speakers/(:any)/View/(:any)'] = "Speakers/View/$1/$2";
$route['Speakers/(:any)'] = "Speakers/index/$1";

$route['Notes/(:any)/add'] = "Notes/add/$1";
$route['Notes/(:any)/submit/(:any)'] = "Notes/submit/$1/$2";
$route['Notes/(:any)/notes_close/(:any)'] = "Notes/notes_close/$1/$2";
$route['Notes/(:any)/edit/(:any)'] = "Notes/edit/$1/$2";
$route['Notes/(:any)/View/(:any)'] = "Notes/View/$1/$2";
$route['Notes/(:any)'] = "Notes/index/$1";


$route['Leader/(:any)'] = "Leader/index/$1";

$route['Presentation/(:any)/Get_slider_images/(:any)'] = "Presentation/Get_slider_images/$1";
$route['Presentation/(:any)/View_presentation/(:any)'] = "Presentation/View_presentation/$1/$2";
$route['Presentation/(:any)'] = "Presentation/index/$1";


$route['Maps/(:any)/View/(:any)'] = "Maps/View/$1/$2";
$route['Maps/(:any)/Map_list/(:any)'] = "Maps/Map_list/$1";
$route['Maps/(:any)'] = "Maps/index/$1";





$route['Messages/(:any)/delete_comment/(:any)'] = "Messages/delete_comment/$1/$2";
$route['Messages/(:any)/autonotificationload'] = "Messages/autonotificationload/$1";
$route['Messages/(:any)/auctionoutbidnotify'] = "Messages/auctionoutbidnotify/$1";
$route['Messages/(:any)/delete_message/(:any)'] = "Messages/delete_message/$1/$2";
$route['Messages/(:any)/commentadd/(:any)'] = "Messages/commentadd/$2/$1";
$route['Messages/(:any)/commentaddpublic/(:any)'] = "Messages/commentaddpublic/$1/$2";
$route['Photos/(:any)/commentaddpublic/(:any)'] = "Photos/commentaddpublic/$1/$2";
$route['Photos/(:any)/dislike'] = "Photos/dislike/$1";
$route['Photos/(:any)/like'] = "Photos/like/$1";
$route['Photos/(:any)/delete_message/(:any)']="Photos/delete_message/$1/$2";
$route['Photos/(:any)/delete_comment/(:any)']="Photos/delete_comment/$1/$2";

$route['Messages/(:any)/loadmore/(:any)/(:any)/(:any)'] = "Messages/loadmore/$1/$2/$3/$4";
$route['Messages/(:any)/loadmore1/(:any)/(:any)/(:any)'] = "Messages/loadmore1/$1/$2/$3/$4";
$route['Messages/(:any)/loadmore/(:any)/(:any)/(:any)/(:any)'] = "Messages/loadmore/$1/$2/$3/$4/$5";
$route['Messages/(:any)/chatspublic'] = "Messages/chatspublic/$1";
$route['Photos/(:any)/chatspublic'] = "Photos/chatspublic/$1";
$route['Messages/(:any)/chatsviewdata/(:any)'] = "Messages/chatsviewdata/$2/$1";
$route['Messages/(:any)/chatsdata'] = "Messages/chatsdata/$1";
$route['Messages/(:any)/chatsspeaker/(:any)'] = "Messages/chatsspeaker/$2/$1";
$route['Messages/(:any)/readbyuser'] = "Messages/readbyuser/$1";
$route['Messages/(:any)/privatemsg'] = "Messages/chatsmsgprivate/$1/$2";
$route['Messages/(:any)/publicmsg'] = "Messages/chatsmsgpublic/$1/$2";
$route['Messages/(:any)/chatsexibitor/(:any)'] = "Messages/chatsexibitor/$2/$1";

$route['Photos/(:any)/index'] = "Photos/index/$1/$2";
$route['Photos/(:any)'] = "Photos/index/$1";
$route['MyContact/(:any)/getuserdetails/(:any)'] = "MyContact/getuserdetails/$1/$2";
$route['MyContact/(:any)/mycontact_update'] = "MyContact/mycontact_update/$1";
$route['MyContact/(:any)/viewcontact'] = "MyContact/viewcontact/$1";
$route['MyContact/(:any)/senduserdetails'] = "MyContact/senduserdetails/$1";

$route['Surveys/(:any)/View'] = "Surveys/View/$1";
$route['Surveys/(:any)'] = "Surveys/index/$1";


$route['Documents/(:any)/displaydocs/(:any)'] = "Documents/displaydocs/$1/$2";
$route['Documents/(:any)/viewdocument/(:any)'] = "Documents/viewdocument/$1/$2";
$route['Documents/(:any)'] = "Documents/index/$1";


$route['Qr_scanner/(:any)'] = "Qr_scanner/index/$1";
$route['MyContact/(:any)'] = "MyContact/index/$1";
$route['Social/(:any)'] = "Social/index/$1";



$route['Advertising/(:any)'] = "Advertising/index/$1/$2";
$route['Attendee_login/check/(:any)'] = "Attendee_login/check/$1";
$route['Attendee_login/checkemail'] = "Attendee_login/checkemail";
$route['Attendee_login/fblogin/(:any)'] = "Attendee_login/fblogin/$1";
$route['Attendee_login/logout/(:any)'] = "Attendee_login/logout/$1";
$route['Attendee_login/add/(:any)'] = "Attendee_login/add/$1";
$route['Attendee_login/(:any)'] = "Attendee_login/sendforgotmail";
$route['Attendee_login/(:any)'] = "Attendee_login/forgot_pas";
$route['Forbidden/(:any)'] = "Forbidden/index/$1";
$route['Pageaccess/(:any)'] = "Pageaccess/index/$1";
$route['Unauthenticate/(:any)'] = "Unauthenticate/index/$1";

//$route['(:any)'] = "event_template/view_template/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
