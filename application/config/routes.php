<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "Login/index";
$route['404_override'] = '';
$route['App/(:any)/preview'] = "App/preview/$1";
$route['App/reset_password/(:any)/(:any)/(:any)'] = "App/reset_password/$1/$2/$3";
$route['app/reset_password/(:any)/(:any)/(:any)'] = "App/reset_password/$1/$2/$3";
$route['App/(:any)/(:any)/add_advertise_hit/(:any)'] = "App/add_advertise_hit/$1/$2/$3";
$route['App/(:any)/(:any)/signature_form_data_save'] = "App/signature_form_data_save/$1/$2";
$route['App/(:any)/(:any)/signature_form'] = "App/signature_form/$1/$2";
$route['App/(:any)/(:any)/attendee_registration_screen'] = "App/attendee_registration_screen/$1/$2";
$route['App/(:any)/(:any)/save_tempusersession/(:any)'] = "App/save_tempusersession/$1/$2/$3";
$route['App/(:any)/(:any)/get_session_list'] = "App/get_session_list/$1/$2";
$route['App/(:any)/(:any)/show_multi_registration'] = "App/show_multi_registration/$1/$2";
$route['App/(:any)/(:any)/exibitorsetpassword/(:any)'] = "App/exibitorsetpassword/$1/$2/$3";
$route['App/(:any)/(:any)/passwordset/(:any)'] = "App/passwordset/$1/$2/$3/$4";
//$route['Agenda/(:any)/(:any)/view_user_agenda'] = "Agenda/view_user_agenda/$2/$1";
$route['App/(:any)/(:any)/logindemo']="App/logindemo/$1/$2";
$route['app/(:any)/(:any)/logindemo']="App/logindemo/$1/$2";
$route['App/(:any)'] = "App/index/$1";
$route['App/(:any)/(:any)'] = "App/index/$1/$2";
$route['app/(:any)/(:any)'] = "App/index/$1/$2";
$route['Cms/(:any)/(:any)/View/(:any)'] = "Cms/View/$1/$2/$3";
	
$route['hub/(:any)'] = "hub/index/$1";

$route['Attendee_hub/loadmore/(:any)/(:any)'] = "Attendee_hub/loadmore/$1/$2";
$route['Attendee_hub/commentadd/(:any)/(:any)'] = "Attendee_hub/commentadd/$1/$2";
$route['Attendee_hub/chatsviewdata/(:any)/(:any)'] = "Attendee_hub/chatsviewdata/$1/$2";
$route['Attendee_hub/View/(:any)/(:any)'] = "Attendee_hub/View/$1/$2";
$route['Attendee_hub/(:any)'] = "Attendee_hub/index/$1";


$route['Public_messages_hub/delete_comment/(:any)/(:any)'] = "Public_messages_hub/delete_comment/$1/$2";
$route['Public_messages_hub/delete_message/(:any)/(:any)'] = "Public_messages_hub/delete_message/$1/$2";
$route['Public_messages_hub/commentaddpublic/(:any)'] = "Public_messages_hub/commentaddpublic/$1";
$route['Public_messages_hub/chatspublic/(:any)'] = "Public_messages_hub/chatspublic/$1";
$route['Public_messages_hub/loadmore/(:any)/(:any)'] = "Public_messages_hub/loadmore/$1/$2";
$route['Public_messages_hub/(:any)'] = "Public_messages_hub/index/$1";

$route['Photos_hub/delete_comment/(:any)/(:any)'] = "Photos_hub/delete_comment/$1/$2";
$route['Photos_hub/delete_message/(:any)/(:any)'] = "Photos_hub/delete_message/$1/$2";
$route['Photos_hub/dislike/(:any)'] = "Photos_hub/dislike/$1";
$route['Photos_hub/like/(:any)'] = "Photos_hub/like/$1";
$route['Photos_hub/commentaddpublic/(:any)/(:any)'] = "Photos_hub/commentaddpublic/$1/$2";
$route['Photos_hub/upload_commentimag/(:any)/(:any)'] = "Photos_hub/upload_commentimag/$1/$2";
$route['Photos_hub/chatspublic/(:any)'] = "Photos_hub/chatspublic/$1";
$route['Photos_hub/upload_imag/(:any)'] = "Photos_hub/upload_imag/$1";
$route['Photos_hub/loadmore/(:any)/(:any)'] = "Photos_hub/loadmore/$1/$2";
$route['Photos_hub/(:any)'] = "Photos_hub/index/$1";

//$route['Messages/(:any)'] = "Messages/chats/$1";
$route['Registration/(:any)/(:any)'] = "Registration/index/$1/$2";
$route['Agenda/(:any)/(:any)/view_category_session/(:any)'] = "Agenda/view_category_session/$1/$2/$3";
$route['Agenda/(:any)/(:any)/add_session_user_comment/(:any)'] = "Agenda/add_session_user_comment/$1/$2/$3";
$route['Agenda/(:any)/View/(:any)'] = "Agenda/View/$1";
$route['Agenda/(:any)/(:any)/direct_save_user_session/(:any)'] = "Agenda/direct_save_user_session/$1/$2/$3";
$route['Agenda/(:any)/(:any)/save_user_session/(:any)'] = "Agenda/save_user_session/$1/$2/$3";
$route['Agenda/(:any)/(:any)/set_user_reminader/(:any)'] = "Agenda/set_user_reminader/$1/$2/$3";
$route['Agenda/(:any)/user_check_in_update/(:any)'] = "Agenda/user_check_in_update/$1/$2";
$route['Agenda/(:any)/remove_from_view_my_agenda/(:any)'] = "Agenda/remove_from_view_my_agenda/$1/$2";
$route['Agenda/(:any)/user_rating_save/(:any)'] = "Agenda/user_rating_save/$1/$2";
$route['Agenda/(:any)/(:any)/save_prev_check_save/(:any)'] = "Agenda/save_prev_check_save/$1/$2/$3";
$route['Agenda/(:any)/view_msg/(:any)/(:any)'] = "Agenda/view_msg/$1/$2/$3";
$route['Agenda/(:any)/(:any)/View_agenda/(:any)'] = "Agenda/View_agenda/$1/$2/$3";
$route['Agenda/(:any)/(:any)/view_user_pending_agenda'] = "Agenda/view_user_pending_agenda/$2/$1";
$route['Agenda/(:any)/(:any)/save_all_pending_agenda']="Agenda/save_all_pending_agenda/$1/$2";
$route['Agenda/(:any)/(:any)/view_user_agenda'] = "Agenda/view_user_agenda/$2/$1";
$route['Agenda/(:any)/user_agenda_update'] = "Agenda/user_agenda_update";
$route['Agenda/(:any)/getpushnoti'] = "Agenda/getpushnoti";
$route['Agenda/(:any)/(:any)'] = "Agenda/index/$1/$2";

$route['My_Favorites/(:any)/(:any)/save_myfavorites'] = "My_Favorites/save_myfavorites/$1/$2";
$route['My_Favorites/(:any)/(:any)'] = "My_Favorites/index/$1/$2";

$route['Facebook_feed/(:any)/(:any)'] = "Facebook_feed/index/$1/$1";

$route['QA/(:any)/(:any)/send_message/(:any)'] = "QA/send_message/$1/$2/$3";
$route['QA/(:any)/(:any)/view/(:any)'] = "QA/view/$1/$2/$3";
$route['QA/(:any)/(:any)'] = "QA/index/$1/$2";

$route['Gamification/(:any)/(:any)'] = "Gamification/index/$1/$2";

$route['Instagram_feed/(:any)/(:any)'] = "Instagram_feed/index/$1/$2";
$route['Twitter_feed/(:any)/(:any)/show_feed/(:any)'] = "Twitter_feed/show_feed/$1/$2/$3";
$route['Twitter_feed/(:any)/(:any)'] = "Twitter_feed/index/$1/$2";
$route['twitter_feed/(:any)/(:any)'] = "Twitter_feed/index/$1/$2";

$route['activity/(:any)/(:any)/socialwall'] = "activity/indextest/$1/$2";
$route['activity/(:any)/(:any)/sw'] = "activity/sw/$1/$2";
$route['activity/(:any)/(:any)/all_loadmore_feed_socialwall/(:any)'] = "activity/all_loadmore_feed_socialwall/$1/$2/$3";






$route['Activity/(:any)/(:any)/delete_comment/(:any)']="Activity/delete_comment/$1/$2/$3";
$route['Activity/(:any)/(:any)/commentaddpublic']="Activity/commentaddpublic/$1/$2";
$route['Activity/(:any)/(:any)/upload_commentimag']="Activity/upload_commentimag/$1/$2";
$route['Activity/(:any)/(:any)/Save_user_like_data']="Activity/Save_user_like_data/$1/$2";
$route['Activity/(:any)/(:any)/refresh_all_feed']="Activity/refresh_all_feed/$1/$2";
$route['Activity/(:any)/(:any)/addupdate_feed'] = "Activity/addupdate_feed/$1/$2";
$route['Activity/(:any)/(:any)/upload_imag'] = "Activity/upload_imag/$1/$2";
$route['Activity/(:any)/(:any)/alerts_loadmore_feed/(:any)'] = "Activity/alerts_loadmore_feed/$1/$2/$3";
$route['Activity/(:any)/(:any)/social_loadmore_feed/(:any)'] = "Activity/social_loadmore_feed/$1/$2/$3";
$route['Activity/(:any)/(:any)/internal_loadmore_feed/(:any)'] = "Activity/internal_loadmore_feed/$1/$2/$3";
$route['Activity/(:any)/(:any)/all_loadmore_feed/(:any)'] = "Activity/all_loadmore_feed/$1/$2/$3";
$route['Activity/(:any)/(:any)'] = "Activity/index/$1/$2";

$route['Sponsors_list/(:any)/View/(:any)'] = "Sponsors_list/View/$1/$2";
$route['Sponsors_list/(:any)/(:any)'] = "Sponsors_list/index/$1/$2";


$route['Checkinportal/(:any)/get_attendee_user_info/(:any)'] = "Checkinportal/get_attendee_user_info/$1/$2/$3";
$route['Checkinportal/(:any)/(:any)/user_check_in/(:any)'] = "Checkinportal/user_check_in/$1/$2/$3";
$route['Checkinportal/(:any)/Save_Attendee_informaion'] = "Checkinportal/Save_Attendee_informaion/$1/$2";
$route['Checkinportal/(:any)/(:any)'] = "Checkinportal/index/$1/$2";

$route['Attendee/(:any)/(:any)/changemeetingdate/(:any)/(:any)'] = "Attendee/changemeetingdate/$1/$2/$3/$4";
$route['Attendee/(:any)/(:any)/changemettingstatus/(:any)'] = "Attendee/changemettingstatus/$1/$2/$3";
$route['Attendee/(:any)/(:any)/suggest_new_datetime'] = "Attendee/suggest_new_datetime/$1/$2";
$route['Attendee/(:any)/(:any)/checpendingkmetting'] = "Attendee/checpendingkmetting/$1/$2";
$route['Attendee/(:any)/(:any)/request_meeting/(:any)'] = "Attendee/request_meeting/$1/$2/$3";
$route['Attendee/(:any)/update_approval_status/(:any)'] = "Attendee/update_approval_status/$1/$2/$3";
$route['Attendee/(:any)/confirm_user_request/(:any)'] = "Attendee/confirm_user_request/$1/$2/$3";
$route['Attendee/(:any)/reject_user_request/(:any)'] = "Attendee/reject_user_request/$1/$2/$3";
$route['Attendee/(:any)/share_contact_info/(:any)'] = "Attendee/share_contact_info/$1/$2/$3";
$route['Attendee/(:any)/(:any)/View/(:any)'] = "Attendee/View/$1/$2/$3";
$route['Attendee/(:any)/(:any)'] = "Attendee/index/$1/$2";

$route['Exhibitors/(:any)/(:any)/(:any)/(:any)/load_more'] = "Exhibitors/load_more/$1/$2/$3/$4";
$route['Exhibitors/(:any)/(:any)/changemeetingdate/(:any)/(:any)'] = "Exhibitors/changemeetingdate/$1/$2/$3/$4";
$route['Exhibitors/(:any)/get_categorie_exibitorlist/(:any)'] = "Exhibitors/get_categorie_exibitorlist/$1/$2/$3";
$route['Exhibitors/(:any)/get_categorie_exibitorlist_new/(:any)'] = "Exhibitors/get_categorie_exibitorlist_new/$1";
$route['Exhibitors/(:any)/(:any)/changemettingstatus/(:any)'] = "Exhibitors/changemettingstatus/$1/$2/$3";
$route['Exhibitors/(:any)/(:any)/suggest_new_datetime'] = "Exhibitors/suggest_new_datetime/$1/$2";
$route['Exhibitors/(:any)/(:any)/checpendingkmetting'] = "Exhibitors/checpendingkmetting/$1/$2";
$route['Exhibitors/(:any)/(:any)/request_meeting/(:any)'] = "Exhibitors/request_meeting/$1/$2/$3";
// $route['Exhibitors/(:any)/View/(:any)'] = "Exhibitors/View/$1/$2";
$route['Exhibitors/(:any)/(:any)/View/(:any)'] = "Exhibitors/View/$1/$2/$3";
$route['Exhibitors/(:any)/(:any)'] = "Exhibitors/index/$1/$2";

$route['Virtual_supermarket/(:any)/(:any)'] = "Virtual_supermarket/index/$1/$2";

//$route['Agenda/(:any)/index/(:any)/(:any)'] = "Agenda/index/$3/$2/$1";

//192.168.1.28/EventApp/Agenda/test/demo
$route['Speakers/(:any)/(:any)/upload_imag/(:any)'] = "Speakers/upload_imag/$1/$2";
$route['Speakers/(:any)/(:any)/upload_imag'] = "Speakers/upload_imag/$1/$2";
$route['Speakers/(:any)/(:any)/checpendingkmetting'] = "Speakers/checpendingkmetting/$1/$2";
$route['Speakers/(:any)/(:any)/changemettingstatus/(:any)'] = "Speakers/changemettingstatus/$1/$2/$3";
$route['Speakers/(:any)/(:any)/changemeetingdate/(:any)/(:any)'] = "Speakers/changemeetingdate/$1/$2/$3/$4";
$route['Speakers/(:any)/(:any)/suggest_new_datetime'] = "Speakers/suggest_new_datetime/$1/$2";
$route['Speakers/(:any)/(:any)/save_request_comments'] = "Speakers/save_request_comments/$1/$2";
/*$route['Speakers/(:any)/upload_commentimag'] = "Speakers/upload_commentimag/$1";*/
$route['Speakers/(:any)/upload_commentimag/(:any)'] = "Speakers/upload_commentimag/$1/$2";
$route['Speakers/(:any)/(:any)/View/(:any)'] = "Speakers/View/$1/$2/$3";
$route['Speakers/(:any)/(:any)'] = "Speakers/index/$1/$2";

$route['Notes/(:any)/(:any)/add'] = "Notes/add/$1/$2";
$route['Notes/(:any)/(:any)/submit/(:any)'] = "Notes/submit/$1/$2";
$route['Notes/(:any)/notes_close/(:any)'] = "Notes/notes_close/$1/$2";
$route['Notes/(:any)/(:any)/edit/(:any)'] = "Notes/edit/$1/$2/$3";
$route['Notes/(:any)/View/(:any)'] = "Notes/View/$1/$2";
$route['Notes/(:any)/(:any)'] = "Notes/index/$1/$2";


$route['Leader/(:any)'] = "Leader/index/$1";

//$route['Presentation/(:any)/Get_slider_images/(:any)'] = "Presentation/Get_slider_images/$1";
//$route['Presentation/(:any)/View_presentation/(:any)'] = "Presentation/View_presentation/$1/$2";
//$route['Presentation/(:any)'] = "Presentation/index/$1";
$route['Presentation/(:any)/(:any)/View_fullscreen_presentation/(:any)']="Presentation/View_fullscreen_presentation/$1/$2/$3";
$route['Presentation/(:any)/(:any)/save_puch_result_in_presentation/(:any)']="Presentation/save_puch_result_in_presentation/$1/$2/$3";
$route['Presentation/(:any)/(:any)/showbarchartdata/(:any)']="Presentation/showbarchartdata/$1/$2/$3";
$route['Presentation/(:any)/(:any)/showchartdata/(:any)']="Presentation/showchartdata/$1/$2/$3";
$route['Presentation/(:any)/(:any)/save_survey_ans_presentation']="Presentation/save_survey_ans_presentation/$1/$2";
$route['Presentation/(:any)/(:any)/view_questions']="Presentation/view_questions/$1/$2";
$route['Presentation/(:any)/(:any)/view_help']="Presentation/view_help/$1/$2";
$route['Presentation/(:any)/(:any)/save_puch_images_presentation/(:any)']="Presentation/save_puch_images_presentation/$1/$2/$3";
$route['Presentation/(:any)/(:any)/save_lock_images_presentation/(:any)']="Presentation/save_lock_images_presentation/$1/$2/$3";
$route['Presentation/(:any)/(:any)/Get_slider_images/(:any)'] = "Presentation/Get_slider_images/$1/$2";
$route['Presentation/(:any)/(:any)/View_presentation/(:any)'] = "Presentation/View_presentation/$1/$2/$3";
$route['Presentation/(:any)/(:any)'] = "Presentation/index/$1/$2";

$route['Maps/(:any)/View/(:any)'] = "Maps/View/$1/$2";
$route['Maps/(:any)/Map_list/(:any)'] = "Maps/Map_list/$1";
$route['Maps/(:any)/(:any)'] = "Maps/index/$1/$2";



$route['Linkedin_signup/initiate/(:any)']="Linkedin_signup/initiate/$1";
$route['Linkedin_signup/data/(:any)']="Linkedin_signup/data/$1";

$route['Messages/(:any)/delete_comment/(:any)'] = "Messages/delete_comment/$1/$2";
$route['Messages/(:any)/(:any)/autonotificationload'] = "Messages/autonotificationload/$1";
$route['Messages/(:any)/(:any)/auctionoutbidnotify'] = "Messages/auctionoutbidnotify/$1";
$route['Messages/(:any)/delete_message/(:any)'] = "Messages/delete_message/$1/$2";
$route['Messages/(:any)/commentadd/(:any)'] = "Messages/commentadd/$2/$1";
$route['Messages/(:any)/commentaddpublic/(:any)'] = "Messages/commentaddpublic/$1/$2";
$route['Photos/(:any)/commentaddpublic/(:any)'] = "Photos/commentaddpublic/$1/$2";
$route['Photos/(:any)/dislike'] = "Photos/dislike/$1";
$route['Photos/(:any)/like'] = "Photos/like/$1";
$route['Photos/(:any)/delete_message/(:any)']="Photos/delete_message/$1/$2";
$route['Photos/(:any)/delete_comment/(:any)']="Photos/delete_comment/$1/$2";

$route['Messages/(:any)/loadmore/(:any)/(:any)/(:any)'] = "Messages/loadmore/$1/$2/$3/$4";
$route['Messages/(:any)/loadmore1/(:any)/(:any)/(:any)'] = "Messages/loadmore1/$1/$2/$3/$4";
$route['Messages/(:any)/loadmore/(:any)/(:any)/(:any)/(:any)'] = "Messages/loadmore/$1/$2/$3/$4/$5";
$route['Messages/(:any)/chatspublic'] = "Messages/chatspublic/$1";
$route['Photos/(:any)/(:any)/chatspublic'] = "Photos/chatspublic/$1/$2";
$route['Messages/(:any)/(:any)/chatsviewdata/(:any)'] = "Messages/chatsviewdata/$3/$1/$2";
$route['Messages/(:any)/chatsdata'] = "Messages/chatsdata/$1";
$route['Messages/(:any)/(:any)/chatsspeaker/(:any)'] = "Messages/chatsspeaker/$3/$1/$2";
$route['Messages/(:any)/readbyuser'] = "Messages/readbyuser/$1";
$route['Messages/(:any)/(:any)/privatemsg'] = "Messages/chatsmsgprivate/$1/$2";
$route['Messages/(:any)/(:any)/publicmsg'] = "Messages/chatsmsgpublic/$1/$2";
$route['Messages/(:any)/chatsexibitor/(:any)'] = "Messages/chatsexibitor/$2/$1";

$route['Photos/(:any)/index'] = "Photos/index/$1/$2";
$route['Photos/(:any)/(:any)'] = "Photos/index/$1/$2";
$route['MyContact/(:any)/getuserdetails/(:any)'] = "MyContact/getuserdetails/$1/$2";
$route['MyContact/(:any)/mycontact_update'] = "MyContact/mycontact_update/$1";
$route['MyContact/(:any)/viewcontact'] = "MyContact/viewcontact/$1";
$route['MyContact/(:any)/senduserdetails'] = "MyContact/senduserdetails/$1";

$route['Surveys/(:any)/View'] = "Surveys/View/$1";
$route['Surveys/(:any)/(:any)/question_index/(:any)'] = "Surveys/question_index/$1/$2/$3";
$route['Surveys/(:any)/(:any)'] = "Surveys/index/$1/$2";


$route['Documents/(:any)/displaydocs/(:any)'] = "Documents/displaydocs/$1/$2";
$route['Documents/(:any)/viewdocument/(:any)'] = "Documents/viewdocument/$1/$2";
$route['Documents/(:any)/(:any)'] = "Documents/index/$1/$2";


$route['Qr_scanner/(:any)'] = "Qr_scanner/index/$1";
$route['MyContact/(:any)'] = "MyContact/index/$1";
$route['Social/(:any)/(:any)'] = "Social/index/$1/$2";


$route['Lead_retrieval/(:any)/(:any)/delete_my_representative/(:any)'] = "Lead_retrieval/delete_my_representative/$1/$2/$3";
$route['Lead_retrieval/(:any)/(:any)/scan_lead_user/(:any)'] = "Lead_retrieval/scan_lead_user/$1/$2/$3";
$route['Lead_retrieval/(:any)/(:any)/email_my_lead'] = "Lead_retrieval/email_my_lead/$1/$2";
$route['Lead_retrieval/(:any)/(:any)/export_my_lead'] = "Lead_retrieval/export_my_lead/$1/$2";
$route['Lead_retrieval/(:any)/(:any)/add_new_resp'] = "Lead_retrieval/add_new_resp/$1/$2";
$route['Lead_retrieval/(:any)/(:any)'] = "Lead_retrieval/index/$1/$2/$3";



$route['Advertising/(:any)/(:any)'] = "Advertising/index/$1/$2";
$route['Attendee_login/check/(:any)'] = "Attendee_login/check/$1";
$route['Attendee_login/checkemail'] = "Attendee_login/checkemail";
$route['Attendee_login/checkmultiuseremail'] = "Attendee_login/checkmultiuseremail";
$route['Attendee_login/fblogin/(:any)'] = "Attendee_login/fblogin/$1";
$route['Attendee_login/logout/(:any)'] = "Attendee_login/logout/$1";
$route['Attendee_login/add/(:any)'] = "Attendee_login/add/$1";
$route['Attendee_login/check_auth_user_login/(:any)'] = "Attendee_login/check_auth_user_login/$1";
$route['Attendee_login/add_registration_screen_user/(:any)/(:any)'] = "Attendee_login/add_registration_screen_user/$1/$2";
$route['Attendee_login/multi_user_save_in_temp/(:any)/(:any)'] = "Attendee_login/multi_user_save_in_temp/$1/$2";
$route['Attendee_login/registraion_user_save_in_temp/(:any)/(:any)'] = "Attendee_login/registraion_user_save_in_temp/$1/$2";
$route['Attendee_login/payment_success_save_user/(:any)/(:any)'] = "Attendee_login/payment_success_save_user/$1/$2";
$route['Attendee_login/payment_failed_show_error_msg/(:any)/(:any)'] = "Attendee_login/payment_failed_show_error_msg/$1/$2";
$route['Attendee_login/multi_user_save/(:any)/(:any)'] = "Attendee_login/multi_user_save/$1/$2";
$route['Attendee_login/setpassword/(:any)'] = "Attendee_login/setpassword/$1";
$route['Attendee_login/(:any)'] = "Attendee_login/sendforgotmail";
$route['Attendee_login/(:any)'] = "Attendee_login/forgot_pas";
$route['Forbidden/(:any)'] = "Forbidden/index/$1";
$route['Pageaccess/(:any)'] = "Pageaccess/index/$1";
$route['Unauthenticate/(:any)/(:any)'] = "Unauthenticate/index/$1";
$route['profile/edit'] = "Profile/edit";
$route['login'] = "Login/index";
$route['forbidden'] = "Forbidden/index";
$route['event/edit/(:any)'] = "Event/edit/$1";

//$route['(:any)'] = "event_template/view_template/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */



/* -------------------------------------DATE : 31-07-2018  -------------------------------*/


$route['test'] = "native_single_fcm/Test/abc";

$route['native_single_fcm/login/getFormBuillderDataByEvent'] = "native_single_fcm/Login/getFormBuillderDataByEvent";
$route['native_single_fcm/login/check'] = "native_single_fcm/Login/check";
$route['native_single_fcm/login/saveFormBuilderData'] = "native_single_fcm/Login/saveFormBuilderData";
$route['native_single_fcm/login/forgot_password'] = "native_single_fcm/Login/forgot_password";
$route['native_single_fcm/login/linkedInSignup'] = "native_single_fcm/Login/linkedInSignup";
$route['native_single_fcm/login/delete_user'] = "native_single_fcm/Login/delete_user";
$route['native_single_fcm/app/getUpdatedByModulesNew'] = "native_single_fcm/App/getUpdatedByModulesNew";
$route['native_single_fcm/app/get_login_screen_settings'] = "native_single_fcm/App/get_login_screen_settings";
$route['native_single_fcm/app/event_id'] = "native_single_fcm/App/event_id";      /* ******************* */
$route['native_single_fcm/app/event_id_advance_design'] = "native_single_fcm/App/event_id_advance_design";
$route['native_single_fcm/profile/update_contact_details'] = "native_single_fcm/Profile/update_contact_details";
$route['native_single_fcm/profile/update_contact_details_v2'] = "native_single_fcm/Profile/update_contact_details_v2";
$route['native_single_fcm/cms/get_cms_page'] = "native_single_fcm/Cms/get_cms_page";
$route['native_single_fcm/event/getAllPublicEvents'] = "native_single_fcm/Event/getAllPublicEvents";
$route['native_single_fcm/event/searchEvent'] = "native_single_fcm/Event/searchEvent";
$route['native_single_fcm/login/fb_signup'] = "native_single_fcm/Login/fb_signup";
$route['native_single_fcm/login/authorizedLogin'] = "native_single_fcm/Login/authorizedLogin";
$route['native_single_fcm/event/openApp'] = "native_single_fcm/Event/openApp";      /* ******************* */
$route['native_single_fcm/event/searchEventBySecurekey'] = "native_single_fcm/Event/searchEventBySecurekey";
$route['native_single_fcm/agenda/getAgendaByTime'] = "native_single_fcm/Agenda/getAgendaByTime";  /* ***** */
$route['native_single_fcm/agenda/getAgendaByType'] = "native_single_fcm/Agenda/getAgendaByType";
$route['native_single_fcm/login/getFormBuillderDataByEvent'] = "native_single_fcm/Login/getFormBuillderDataByEvent";
$route['native_single_fcm/profile/getStateList'] = "native_single_fcm/Profile/getStateList";
$route['native_single_fcm/attendee/attendee_list_native_new'] = "native_single_fcm/Attendee/attendee_list_native_new";
$route['native_single_fcm/attendee/getAttendeeCategories'] = "native_single_fcm/Attendee/getAttendeeCategories";
$route['native_single_fcm/attendee/attendee_view'] = "native_single_fcm/Attendee/attendee_view";

$route['native_single_fcm/attendee/attendee_view_unread_count'] = "native_single_fcm/Attendee/attendee_view_unread_count";

$route['native_single_fcm/attendee/delete_comment'] = "native_single_fcm/Attendee/delete_comment";
$route['native_single_fcm/attendee/attendee_contact_list_new'] = "native_single_fcm/Attendee/attendee_contact_list_new";
$route['native_single_fcm/exhibitor/exhibitor_list_native_new'] = "native_single_fcm/Exhibitor/exhibitor_list_native_new";
$route['native_single_fcm/exhibitor/exhibitor_parent_category'] = "native_single_fcm/Exhibitor/exhibitor_parent_category";
$route['native_single_fcm/exhibitor/exhibitor_category'] = "native_single_fcm/Exhibitor/exhibitor_category";
$route['native_single_fcm/exhibitor/exhibitorSearch'] = "native_single_fcm/Exhibitor/exhibitorSearch";

$route['native_single_fcm/exhibitor/exhibitor_view_unread_count'] = "native_single_fcm/Exhibitor/exhibitor_view_unread_count";
$route['native_single_fcm/exhibitor/delete_comment'] = "native_single_fcm/Exhibitor/delete_comment";
$route['native_single_fcm/exhibitor/make_comment'] = "native_single_fcm/Exhibitor/make_comment";
$route['native_single_fcm/exhibitor/delete_message'] = "native_single_fcm/Exhibitor/delete_message";
$route['native_single_fcm/exhibitor/shareContactInformation'] = "native_single_fcm/Exhibitor/shareContactInformation";
$route['native_single_fcm/speaker/speaker_list_offline'] = "native_single_fcm/Speaker/speaker_list_offline";



$route['native_single_fcm/speaker/speaker_list_offline'] = "native_single_fcm/Speaker/speaker_list_offline";
$route['native_single_fcm/speaker/speaker_view_unread_count'] = "native_single_fcm/Speaker/speaker_view_unread_count";
$route['native_single_fcm/speaker/delete_comment'] = "native_single_fcm/Speaker/delete_comment";
$route['native_single_fcm/maps/map_list_new'] = "native_single_fcm/Maps/map_list_new";
$route['native_single_fcm/cms/get_cms_list_details'] = "native_single_fcm/Cms/get_cms_list_details";
$route['native_single_fcm/maps/map_details'] = "native_single_fcm/Maps/map_details";


$route['native_single_fcm/social/social_list'] = "native_single_fcm/Social/social_list";
$route['native_single_fcm/note/add_note_new'] = "native_single_fcm/Note/add_note_new";
$route['native_single_fcm/note/edit_note'] = "native_single_fcm/Note/edit_note";
$route['native_single_fcm/note/note_list_new'] = "native_single_fcm/Note/note_list_new";
$route['native_single_fcm/profile/update_profile'] = "native_single_fcm/Profile/update_profile";
$route['native_single_fcm/profile/update_profile_v2'] = "native_single_fcm/Profile/update_profile_v2";
$route['native_single_fcm/agenda/getAgendaById'] = "native_single_fcm/Agenda/getAgendaById";


$route['native_single_fcm/agenda/saveAgendaComments'] = "native_single_fcm/Agenda/saveAgendaComments";
$route['native_single_fcm/agenda/saveUserAgenda'] = "native_single_fcm/Agenda/saveUserAgenda";
$route['native_single_fcm/agenda/deleteUserAgenda'] = "native_single_fcm/Agenda/deleteUserAgenda";
$route['native_single_fcm/agenda/checkInAgenda'] = "native_single_fcm/Agenda/checkInAgenda";
$route['native_single_fcm/agenda/saveRating'] = "native_single_fcm/Agenda/saveRating";
$route['native_single_fcm/agenda/getUserAgendaByTime'] = "native_single_fcm/Agenda/getUserAgendaByTime";
$route['native_single_fcm_v2/agenda/getUserAgendaByTime'] = "native_single_fcm_v2/Agenda/getUserAgendaByTime";

$route['native_single_fcm/agenda/getUserAgendaByType'] = "native_single_fcm/Agenda/getUserAgendaByType";
$route['native_single_fcm/agenda/getPendingUserAgendaByTime'] = "native_single_fcm/Agenda/getPendingUserAgendaByTime";
$route['native_single_fcm/agenda/getPendingUserAgendaByType'] = "native_single_fcm/Agenda/getPendingUserAgendaByType";
$route['native_single_fcm/agenda/savePendingUserAgenda'] = "native_single_fcm/Agenda/savePendingUserAgenda";
$route['native_single_fcm/agenda/deleteUserPendingAgenda'] = "native_single_fcm/Agenda/deleteUserPendingAgenda";
$route['native_single_fcm/document/documents_list_new'] = "native_single_fcm/Document/documents_list_new";



$route['native_single_fcm/document/searchDocuments'] = "native_single_fcm/Document/searchDocuments";
$route['native_single_fcm/document/folder_view'] = "native_single_fcm/Document/folder_view";
$route['native_single_fcm/fundraising/fundraising_home'] = "native_single_fcm/Fundraising/fundraising_home";
$route['native_single_fcm/fundraising/fundraising_products'] = "native_single_fcm/Fundraising/fundraising_products";
$route['native_single_fcm/survey/get_category_wise_survey'] = "native_single_fcm/Survey/get_category_wise_survey";
$route['native_single_fcm/survey/saveSurvey'] = "native_single_fcm/Survey/saveSurvey";


$route['native_single_fcm/sponsors/sponsors_list'] = "native_single_fcm/Sponsors/sponsors_list";
$route['native_single_fcm/sponsors/sponsors_list_offline'] = "native_single_fcm/Sponsors/sponsors_list_offline";
$route['native_single_fcm/sponsors/sponsors_view'] = "native_single_fcm/Sponsors/sponsors_view";
$route['native_single_fcm/sponsors/get_favorited_sponser'] = "native_single_fcm/Sponsors/get_favorited_sponser";
$route['native_single_fcm/speaker/get_favorited_speakers'] = "native_single_fcm/Speaker/get_favorited_speakers";
$route['native_single_fcm/login/updateUserGCMId'] = "native_single_fcm/Login/updateUserGCMId";


$route['native_single_fcm/message/publicMessageSend'] = "native_single_fcm/Message/publicMessageSend";
$route['native_single_fcm/message/public_msg_images_request'] = "native_single_fcm/Message/public_msg_images_request";
$route['native_single_fcm/message/getPublicMessages'] = "native_single_fcm/Message/getPublicMessages";
$route['native_single_fcm/message/make_comment'] = "native_single_fcm/Message/make_comment";
$route['native_single_fcm/message/delete_comment'] = "native_single_fcm/Message/delete_comment";
$route['native_single_fcm/message/delete_message'] = "native_single_fcm/Message/delete_message";



$route['native_single_fcm/message/getPrivateMessages'] = "native_single_fcm/Message/getPrivateMessages";
$route['native_single_fcm/message/getAllSpeakersAttendee'] = "native_single_fcm/Message/getAllSpeakersAttendee";
$route['native_single_fcm/message/privateMessageSend'] = "native_single_fcm/Message/privateMessageSend";
$route['native_single_fcm/message/privateMessageSendText'] = "native_single_fcm/Message/privateMessageSendText";
$route['native_single_fcm/message/privateMessageSendImage'] = "native_single_fcm/Message/privateMessageSendImage";
$route['native_single_fcm/photo/getPublicFeeds'] = "native_single_fcm/Photo/getPublicFeeds";


$route['native_single_fcm/photo/UploadPublicFeeds'] = "native_single_fcm/Photo/UploadPublicFeeds";
$route['native_single_fcm/photo/like_feed'] = "native_single_fcm/Photo/like_feed";
$route['native_single_fcm/photo/dislike_feed'] = "native_single_fcm/Photo/dislike_feed";
$route['native_single_fcm/photo/delete_feed'] = "native_single_fcm/Photo/delete_feed";
$route['native_single_fcm/photo/make_comment'] = "native_single_fcm/Photo/make_comment";
$route['native_single_fcm/photo/delete_comment'] = "native_single_fcm/Photo/delete_comment";



$route['native_single_fcm/note/delete_note'] = "native_single_fcm/Note/delete_note";
$route['native_single_fcm/product/product_listing'] = "native_single_fcm/Product/product_listing";
$route['native_single_fcm/product/slient_products_details'] = "native_single_fcm/Product/slient_products_details";
$route['native_single_fcm/product/getAuctionWiseProductId'] = "native_single_fcm/Product/getAuctionWiseProductId";
$route['native_single_fcm/product/live_products_details'] = "native_single_fcm/Product/live_products_details";
$route['native_single_fcm/product/online_shop_details'] = "native_single_fcm/Product/online_shop_details";


$route['native_single_fcm/product/donation_details'] = "native_single_fcm/Product/donation_details";
$route['native_single_fcm/product/cart_details'] = "native_single_fcm/Product/cart_details";
$route['native_single_fcm/product/add_to_cart'] = "native_single_fcm/Product/add_to_cart";
$route['native_single_fcm/product/save_bid_details'] = "native_single_fcm/Product/save_bid_details";
$route['native_single_fcm/product/checkout_details'] = "native_single_fcm/Product/checkout_details";
$route['native_single_fcm/product/delete_cart_product'] = "native_single_fcm/Product/delete_cart_product";



$route['native_single_fcm/product/update_cart_quantity'] = "native_single_fcm/Product/update_cart_quantity";
$route['native_single_fcm/social/getTwitetrFeedsNew'] = "native_single_fcm/Social/getTwitetrFeedsNew";
$route['native_single_fcm/social/getTwitetrHashtags'] = "native_single_fcm/Social/getTwitetrHashtags";
$route['native_single_fcm/photo/getImageList'] = "native_single_fcm/Photo/getImageList";
$route['native_single_fcm/message/getImageList'] = "native_single_fcm/Message/getImageList";
$route['native_single_fcm/attendee/getAllMeetingRequestModerator'] = "native_single_fcm/Attendee/getAllMeetingRequestModerator";



$route['native_single_fcm/attendee/getAllMeetingRequestWithDateModerator'] = "native_single_fcm/Attendee/getAllMeetingRequestWithDateModerator";
$route['native_single_fcm/attendee/respondRequestModerator'] = "native_single_fcm/Attendee/respondRequestModerator";
$route['native_single_fcm/attendee/suggestMeetingTimeModerator'] = "native_single_fcm/Attendee/suggestMeetingTimeModerator";
$route['native_single_fcm/attendee/SaveCommentModerator'] = "native_single_fcm/Attendee/SaveCommentModerator";
$route['native_single_fcm/product/confirmOrder'] = "native_single_fcm/Product/confirmOrder";
$route['native_single_fcm/fundraising/save_fundraising_donation_details'] = "native_single_fcm/Fundraising/save_fundraising_donation_details";


$route['native_single_fcm/fundraising/save_instant_donation_details'] = "native_single_fcm/Fundraising/save_instant_donation_details";
$route['native_single_fcm/fundraising/get_fundraising_donation_details'] = "native_single_fcm/Fundraising/get_fundraising_donation_details";
$route['native_single_fcm/fundraising/orders'] = "native_single_fcm/Fundraising/orders";
$route['native_single_fcm/product/add_item'] = "native_single_fcm/Product/add_item";
$route['native_single_fcm/product/get_item_list'] = "native_single_fcm/Product/get_item_list";
$route['native_single_fcm/product/delete_item'] = "native_single_fcm/Product/delete_item";



$route['native_single_fcm/product/get_categories'] = "native_single_fcm/Product/get_categories";
$route['native_single_fcm/product/save_image'] = "native_single_fcm/Product/save_image";
$route['native_single_fcm/product/get_item_details'] = "native_single_fcm/Product/get_item_details";
$route['native_single_fcm/product/product_thumbnail'] = "native_single_fcm/Product/product_thumbnail";
$route['native_single_fcm/activity/getFeeds'] = "native_single_fcm/Activity/getFeeds";




$route['native_single_fcm/activity/get_all'] = "native_single_fcm/Activity/get_all";
$route['native_single_fcm/activity/get_internal'] = "native_single_fcm/Activity/get_internal";
$route['native_single_fcm/activity/get_social'] = "native_single_fcm/Activity/get_social";
$route['native_single_fcm/activity/get_alert'] = "native_single_fcm/Activity/get_alert";
$route['native_single_fcm/activity/feedLikeNew'] = "native_single_fcm/Activity/feedLikeNew";
$route['native_single_fcm/activity/feedCommentNew'] = "native_single_fcm/Activity/feedCommentNew";



$route['native_single_fcm/activity/deleteFeedComment'] = "native_single_fcm/Activity/deleteFeedComment";
$route['native_single_fcm/activity/saveActivityImages'] = "native_single_fcm/Activity/saveActivityImages";
$route['native_single_fcm/activity/add_photo_filter_image'] = "native_single_fcm/Activity/add_photo_filter_image";
$route['native_single_fcm/activity/get_photofilter_url'] = "native_single_fcm/Activity/get_photofilter_url";
$route['native_single_fcm/activity/postUpdate'] = "native_single_fcm/Activity/postUpdate";
$route['native_single_fcm/activity/getFeedDetails'] = "native_single_fcm/Activity/getFeedDetails";



$route['native_single_fcm/activity/getImageList'] = "native_single_fcm/Activity/getImageList";
$route['native_single_fcm/product/authorize_payment'] = "native_single_fcm/Product/authorize_payment";
$route['native_single_fcm/settings/getAdvertising_new'] = "native_single_fcm/Settings/getAdvertising_new";
$route['native_single_fcm/product/stripePayment'] = "native_single_fcm/Product/stripePayment";
$route['native_single_fcm/message/notificationCounter'] = "native_single_fcm/Message/notificationCounter";
$route['native_single_fcm/message/messageRead'] = "native_single_fcm/Message/messageRead";



$route['native_single_fcm/social/getInstagramFeeds'] = "native_single_fcm/Social/getInstagramFeeds";
$route['native_single_fcm/app/getFavouritedExhibitors'] = "native_single_fcm/App/getFavouritedExhibitors";
$route['native_single_fcm/app/getModuleUpdateDate'] = "native_single_fcm/App/getModuleUpdateDate";
$route['native_single_fcm/exhibitor/getExhibitorListCategoryPcategoriesoffline'] = "native_single_fcm/Exhibitor/getExhibitorListCategoryPcategoriesoffline";
$route['native_single_fcm/group/get_group_data'] = "native_single_fcm/Group/get_group_data";
$route['native_single_fcm/group/get_menu_group'] = "native_single_fcm/Group/get_menu_group";


$route['native_single_fcm/cms/get_cms_super_group'] = "native_single_fcm/Cms/get_cms_super_group";
$route['native_single_fcm/cms/get_chid_cms_group'] = "native_single_fcm/Cms/get_chid_cms_group";
$route['native_single_fcm/get_child_cms_list'] = "native_single_fcm/get_child_cms_list";
$route['native_single_fcm/agenda/get_agenda_offline'] = "native_single_fcm/Agenda/get_agenda_offline";
$route['native_single_fcm/presentation1/lockUnlockSlides'] = "native_single_fcm/Presentation1/lockUnlockSlides";
$route['native_single_fcm/presentation1/pushSlide'] = "native_single_fcm/Presentation1/pushSlide";


$route['native_single_fcm/presentation1/saveSurveyAnswer'] = "native_single_fcm/Presentation1/saveSurveyAnswer";
$route['native_single_fcm/presentation1/pushResult'] = "native_single_fcm/Presentation1/pushResult";
$route['native_single_fcm/presentation1/getChartResult'] = "native_single_fcm/Presentation1/getChartResult";
$route['native_single_fcm/presentation1/removeChartResult'] = "native_single_fcm/Presentation1/removeChartResult";
$route['native_single_fcm/presentation1/removePushSlide'] = "native_single_fcm/Presentation1/removePushSlide";
$route['native_single_fcm/presentation1/getPrsentationByTime'] = "native_single_fcm/Presentation1/getPrsentationByTime";


$route['native_single_fcm/presentation1/getPrsentationImagesByIdRefresh'] = "native_single_fcm/Presentation1/getPrsentationImagesByIdRefresh";
$route['native_single_fcm/presentation1/getPrsentationByType'] = "native_single_fcm/Presentation1/getPrsentationByType";
$route['native_single_fcm/presentation1/viewPresentationByRole'] = "native_single_fcm/Presentation1/viewPresentationByRole";
$route['native_single_fcm/presentation1/sendmessage'] = "native_single_fcm/Presentation1/sendmessage";
$route['native_single_fcm/checkin/attendeeList'] = "native_single_fcm/Checkin/attendeeList";
$route['native_single_fcm/checkin/get_checkedin_attendee'] = "native_single_fcm/Checkin/get_checkedin_attendee";



$route['native_single_fcm/checkin/attendeeCheckIn'] = "native_single_fcm/Checkin/attendeeCheckIn";
$route['native_single_fcm/checkin/viewAttendeeInfo'] = "native_single_fcm/Checkin/viewAttendeeInfo";
$route['native_single_fcm/checkin/saveAttendeeInfo'] = "native_single_fcm/Checkin/saveAttendeeInfo";
$route['native_single_fcm/attendee/shareContactInformation'] = "native_single_fcm/Attendee/shareContactInformation";
$route['native_single_fcm/attendee/approveStatus'] = "native_single_fcm/Attendee/approveStatus";
$route['native_single_fcm/attendee/rejectStatus'] = "native_single_fcm/Attendee/rejectStatus";


$route['native_single_fcm/exhibitor/getRequestMeetingDateTime'] = "native_single_fcm/Exhibitor/getRequestMeetingDateTime";
$route['native_single_fcm/exhibitor/requestMeeting'] = "native_single_fcm/Exhibitor/requestMeeting";
$route['native_single_fcm/exhibitor/saveRequestMeeting'] = "native_single_fcm/Exhibitor/saveRequestMeeting";
$route['native_single_fcm/exhibitor/getAllMeetingRequestNew'] = "native_single_fcm/Exhibitor/getAllMeetingRequestNew";
$route['native_single_fcm/exhibitor/getAllMeetingRequestWithDate'] = "native_single_fcm/Exhibitor/getAllMeetingRequestWithDate";
$route['native_single_fcm/exhibitor/respondRequest'] = "native_single_fcm/Exhibitor/respondRequest";


$route['native_single_fcm/reminder/setAgendaReminder'] = "native_single_fcm/reminder/setAgendaReminder";
$route['native_single_fcm/message/getNotificationListing'] = "native_single_fcm/Message/getNotificationListing";
$route['native_single_fcm/message/deleteNotification'] = "native_single_fcm/Message/deleteNotification";
$route['native_single_fcm/settings/userClickBoard'] = "native_single_fcm/Settings/userClickBoard";
$route['native_single_fcm/exhibitor/getRequestMeetingDateTime'] = "native_single_fcm/Exhibitor/getRequestMeetingDateTime";
$route['native_single_fcm/exhibitor/suggestMeetingTime'] = "native_single_fcm/Exhibitor/suggestMeetingTime";


$route['native_single_fcm/exhibitor/getSuggestedTimings'] = "native_single_fcm/Exhibitor/getSuggestedTimings";
$route['native_single_fcm/exhibitor/bookSuggestedTime'] = "native_single_fcm/Exhibitor/bookSuggestedTime";
$route['native_single_fcm/social/getFacebookFeed'] = "native_single_fcm/Social/getFacebookFeed";
$route['native_single_fcm/settings/checkVersionCode'] = "native_single_fcm/Settings/checkVersionCode";
$route['native_single_fcm/attendee/getRequestMeetingDateTime'] = "native_single_fcm/Attendee/getRequestMeetingDateTime";
$route['native_single_fcm/attendee/getRequestMeetingDate'] = "native_single_fcm/Attendee/getRequestMeetingDate";


$route['native_single_fcm/attendee/suggestMeetingTime'] = "native_single_fcm/Attendee/suggestMeetingTime";
$route['native_single_fcm/favorites/addOrRemove'] = "native_single_fcm/Favorites/addOrRemove";
$route['native_single_fcm/favorites/addOrRemoveAttendee'] = "native_single_fcm/Favorites/addOrRemoveAttendee";
$route['native_single_fcm/exhibitor/saveToFavorites'] = "native_single_fcm/Exhibitor/saveToFavorites";
$route['native_single_fcm/favorites/getFavoritesList'] = "native_single_fcm/Favorites/getFavoritesList";
$route['native_single_fcm/qa/getAllSession'] = "native_single_fcm/Qa/getAllSession";


$route['native_single_fcm/qa/getSessionDetail'] = "native_single_fcm/Qa/getSessionDetail";
$route['native_single_fcm/qa/VoteMessage'] = "native_single_fcm/Qa/VoteMessage";
$route['native_single_fcm/qa/sendMessage'] = "native_single_fcm/Qa/sendMessage";
$route['native_single_fcm/message/getPrivateUnreadMessagesList_tmp'] = "native_single_fcm/Message/getPrivateUnreadMessagesList_tmp";
$route['native_single_fcm/message/getPrivateMessagesConversation'] = "native_single_fcm/Message/getPrivateMessagesConversation";
$route['native_single_fcm/gulfood/getVirtualSuperMarket'] = "native_single_fcm/Gulfood/getVirtualSuperMarket";


$route['native_single_fcm/gulfood/getExhibitorProfile'] = "native_single_fcm/Gulfood/getExhibitorProfile";
$route['native_single_fcm/beacons/save_beacon'] = "native_single_fcm/Beacons/save_beacon";
$route['native_single_fcm/checkin/addCheckin_new'] = "native_single_fcm/Checkin/addCheckin_new";
$route['native_single_fcm/beacons/get_all_beacon'] = "native_single_fcm/Beacons/get_all_beacon";
$route['native_single_fcm/beacons/get_notification'] = "native_single_fcm/Beacons/get_notification";
$route['native_single_fcm/beacons/rename_beacon'] = "native_single_fcm/Beacons/rename_beacon";


$route['native_single_fcm/beacons/delete_beacon'] = "native_single_fcm/Beacons/delete_beacon";
$route['native_single_fcm/checkin/get_pdf'] = "native_single_fcm/Checkin/get_pdf";
$route['native_single_fcm/profile/get_additional_data'] = "native_single_fcm/Profile/get_additional_data";
$route['native_single_fcm/profile/update_profile_with_additional_data'] = "native_single_fcm/Profile/update_profile_with_additional_data";
$route['native_single_fcm/survey/get_survey_category'] = "native_single_fcm/Survey/get_survey_category";
$route['native_single_fcm/qa/DeleteMessage'] = "native_single_fcm/Qa/DeleteMessage";


$route['native_single_fcm/qa/Approve_qa'] = "native_single_fcm/Qa/Approve_qa";
$route['native_single_fcm/qa/show_on_web'] = "native_single_fcm/Qa/show_on_web";
$route['native_single_fcm/gamification/get_leaderboard'] = "native_single_fcm/Gamification/get_leaderboard";
$route['native_single_fcm/settings/get_o_screen'] = "native_single_fcm/Settings/get_o_screen";
$route['native_single_fcm/lead/scan_lead'] = "native_single_fcm/Lead/scan_lead";
$route['native_single_fcm/lead/get_exhi_leads'] = "native_single_fcm/Lead/get_exhi_leads";


$route['native_single_fcm/lead/remove_rep'] = "native_single_fcm/Lead/remove_rep";
$route['native_single_fcm/lead/save_questions'] = "native_single_fcm/Lead/save_questions";
$route['native_single_fcm/lead/reset_questions_answer'] = "native_single_fcm/Lead/reset_questions_answer";
$route['native_single_fcm/lead/update_questions_answer'] = "native_single_fcm/Lead/update_questions_answer";
$route['native_single_fcm/lead/get_attendee_list_for_rep'] = "native_single_fcm/Lead/get_attendee_list_for_rep";
$route['native_single_fcm/lead/add_new_representative'] = "native_single_fcm/Lead/add_new_representative";


$route['native_single_fcm/lead/rep_list'] = "native_single_fcm/Lead/rep_list";
$route['native_single_fcm/lead/export_lead'] = "native_single_fcm/Lead/export_lead";
$route['native_single_fcm/lead/save_scan_lead'] = "native_single_fcm/Lead/save_scan_lead";
$route['native_single_fcm/lead/get_offline_data'] = "native_single_fcm/Lead/get_offline_data";
$route['native_single_fcm/lead/save_scan_upload'] = "native_single_fcm/Lead/save_scan_upload";
$route['native_single_fcm/lead/get_lead_details'] = "native_single_fcm/Lead/get_lead_details";


$route['native_single_fcm/lead/updateLead'] = "native_single_fcm/Lead/updateLead";
$route['native_single_fcm/lead/get_exhi_leads_offline'] = "native_single_fcm/Lead/get_exhi_leads_offline";
$route['native_single_fcm/note/download_pdf'] = "native_single_fcm/Note/download_pdf";
$route['native_single_fcm/attendee/getAttendeeForInviting'] = "native_single_fcm/Attendee/getAttendeeForInviting";
$route['native_single_fcm/attendee/GetInvitedAttendee'] = "native_single_fcm/Attendee/GetInvitedAttendee";
$route['native_single_fcm/attendee/InviteAttendee'] = "native_single_fcm/Attendee/InviteAttendee";


$route['native_single_fcm/badge_scanner/ScanBadge'] = "native_single_fcm/Badge_scanner/ScanBadge";
$route['native_single_fcm/attendee/getRequestMeetingTime'] = "native_single_fcm/Attendee/getRequestMeetingTime";
$route['native_single_fcm/app/get_default_lang'] = "native_single_fcm/App/get_default_lang";
$route['native_single_fcm/attendee/blockAttendee'] = "native_single_fcm/Attendee/blockAttendee";



/* not in android but in ios doc */

$route['native_single_fcm/login/registrationByEvent'] = "native_single_fcm/Login/registrationByEvent";
$route['native_single_fcm/login/getAllCountryList'] = "native_single_fcm/Login/getAllCountryList";
$route['native_single_fcm/speaker/speaker_view'] = "native_single_fcm/Speaker/speaker_view";
$route['native_single_fcm/exhibitor/exhibitor_view'] = "native_single_fcm/Exhibitor/exhibitor_view";
$route['native_single_fcm/exhibitor/exhibitor_view_unread_count_V2'] = "native_single_fcm/Exhibitor/exhibitor_view_unread_count_V2";
$route['native_single_fcm/social/getTwitetrFeeds'] = "native_single_fcm/Social/getTwitetrFeeds";



$route['native_single_fcm/presentation1/getPrsentationById'] = "native_single_fcm/Presentation1/getPrsentationById";
$route['native_single_fcm/exhibitor/msg_images_request'] = "native_single_fcm/Exhibitor/msg_images_request";
$route['native_single_fcm/speaker/make_comment'] = "native_single_fcm/Speaker/make_comment";
$route['native_single_fcm/speaker/sendMessage'] = "native_single_fcm/Speaker/sendMessage";
$route['native_single_fcm/speaker/msg_images_request'] = "native_single_fcm/Speaker/msg_images_request";
$route['native_single_fcm/speaker/delete_message'] = "native_single_fcm/Speaker/delete_message";


$route['native_single_fcm/attendee/msg_images_request'] = "native_single_fcm/Attendee/msg_images_request";
$route['native_single_fcm/attendee/delete_message'] = "native_single_fcm/Attendee/delete_message";
$route['native_single_fcm/message/private_msg_images_request'] = "native_single_fcm/Message/private_msg_images_request";
$route['native_single_fcm/settings/notification'] = "native_single_fcm/Settings/notification";
$route['native_single_fcm/login/logout'] = "native_single_fcm/Login/logout";
$route['native_single_fcm/attendee/respondRequest'] = "native_single_fcm/Attendee/respondRequest";


$route['native_single_fcm/attendee/attendee_contact_list'] = "native_single_fcm/Attendee/attendee_contact_list";
$route['native_single_fcm/message/delete_private_message'] = "native_single_fcm/Message/delete_private_message";
$route['native_single_fcm/gulfood/getUsefulInfo'] = "native_single_fcm/Gulfood/getUsefulInfo";
$route['native_single_fcm/favorites/addOrRemoveAttendee'] = "native_single_fcm/Favorites/addOrRemoveAttendee";
$route['native_single_fcm/checkin/addCheckin'] = "native_single_fcm/Checkin/addCheckin";
$route['native_single_fcm/exhibitor/exhibitor_list_new'] = "native_single_fcm/Exhibitor/exhibitor_list_new";


$route['native_single_fcm/exhibitor/getExhibitorListCategoryPcategories'] = "native_single_fcm/Exhibitor/getExhibitorListCategoryPcategories";
$route['native_single_fcm/exhibitor/getExhibitorListCategoryPcategoriestest'] = "native_single_fcm/Exhibitor/getExhibitorListCategoryPcategoriestest";
$route['native_single_fcm/exhibitor/getExhibitorListCategoryPcategoriesoffline_V2'] = "native_single_fcm/Exhibitor/getExhibitorListCategoryPcategoriesoffline_V2";
$route['native_single_fcm/attendee/getAllMeetingRequestWithDate'] = "native_single_fcm/Attendee/getAllMeetingRequestWithDate";
$route['native_single_fcm/attendee/getRequestMeetingLocation'] = "native_single_fcm/Attendee/getRequestMeetingLocation";
$route['native_single_fcm/app/getUpdatedByModules'] = "native_single_fcm/App/getUpdatedByModules";



/* NEW ON 07-08-2018 */

$route['native_single_fcm/native_single_fcm_v2/getConcordiaAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getConcordiaAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/ConcordiaSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ConcordiaSearchEvent";

$route['native_single_fcm/rss/get_top_news_page'] = "native_single_fcm/Rss/get_top_news_page";

$route['native_single_fcm/native_single_fcm_v2/NCRealtorSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/NCRealtorSearchEvent";

$route['native_single_fcm/attendee/hideMyIdentity'] = "native_single_fcm/Attendee/hideMyIdentity";

$route['native_single_fcm/agenda/getUserAgendaByTimeWithMeeting'] = "native_single_fcm/Agenda/getUserAgendaByTimeWithMeeting";

$route['native_single_fcm/attendee/saveRequestMeeting'] = "native_single_fcm/Attendee/saveRequestMeeting";


$route['native_single_fcm/matchmaking/getModules'] = "native_single_fcm/Matchmaking/getModules";
$route['native_single_fcm/matchmaking/getAttendees'] = "native_single_fcm/Matchmaking/getAttendees";
$route['native_single_fcm/matchmaking/getExhibitor'] = "native_single_fcm/Matchmaking/getExhibitor";
$route['native_single_fcm/matchmaking/getSpeaker'] = "native_single_fcm/Matchmaking/getSpeaker";
$route['native_single_fcm/matchmaking/getSponsor'] = "native_single_fcm/Matchmaking/getSponsor";
$route['native_single_fcm/matchmaking/markAsVisited'] = "native_single_fcm/Matchmaking/markAsVisited";


/* -----------------------------Date:14-08-2018-----------------------------------------*/

$route['native_single_fcm/login/check_v2'] = "native_single_fcm/Login/check_v2";
$route['native_single_fcm/login/forgot_password_v2'] = "native_single_fcm/Login/forgot_password_v2";
$route['native_single_fcm/login/registrationByEvent_v2'] = "native_single_fcm/Login/registrationByEvent_v2";
$route['native_single_fcm/attendee/requestMeeting'] = "native_single_fcm/Attendee/requestMeeting";
$route['native_single_fcm/profile/update_profile_with_additional_data_v2'] = "native_single_fcm/Profile/update_profile_with_additional_data_v2";

$route['native_single_fcm/native_single_fcm_v2/Certificatevalid'] = "native_single_fcm/Native_single_fcm_v2/Certificatevalid";






/* -----------------------------Date:18-09-2018-----------------------------------------*/

$route['Messages/(:any)/(:any)/commentadd/(:any)'] = "Messages/commentadd/$3/$1/$2";

$route['Maps/(:any)/(:any)/View/(:any)'] = "Maps/View/$1/$2/$3";

$route['Photos/(:any)/(:any)/commentaddpublic/(:any)'] = "Photos/commentaddpublic/$1/$2/$3";

$route['Messages/(:any)/(:any)/commentaddpublic/(:any)'] = "Messages/commentaddpublic/$1/$2/$3";

$route['Messages/(:any)/(:any)/chatspublic'] = "Messages/chatspublic/$1/$2";

$route['Documents/(:any)/(:any)/(:any)'] = "Documents/index/$1/$2/$3";

$route['Sponsors_list/(:any)/(:any)/View/(:any)'] = "Sponsors_list/View/$1/$2/$3";

//$route['Matchmaking/(:any)/(:any)'] = "Matchmaking/index/$1/$2";

$route['app/(:any)'] = "App/index";
$route['app/(:any)/(:any)/attendee_registration_screen'] = "App/attendee_registration_screen/$1/$2";

$route['Exhibitors/(:any)/(:any)/categorywise/(:any)'] = "Exhibitors/index/$1/$2/$3/$4";


/* -----------------------------Date:20-09-2018-----------------------------------------*/


$route['native_single_fcm/native_single_fcm_v2/getPensionBridgeAnnualDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPensionBridgeAnnualDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getPrivateEquityExclusiveDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPrivateEquityExclusiveDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkPensionBridgeVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkPensionBridgeVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getNapecDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getNapecDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkNapecVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkNapecVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getHFTPAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getHFTPAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/HFTPSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/HFTPSearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getBathandWestShowDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBathandWestShowDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkBathandWestShowVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBathandWestShowVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getIOTAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getIOTAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/checkIOTVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIOTVersionCode";

$route['native_single_fcm/native_single_fcm_v2/IOTsearchEvent'] = "native_single_fcm/Native_single_fcm_v2/IOTsearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getNACFBDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getNACFBDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkNACFBVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkNACFBVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getIdahoRealtorsDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getIdahoRealtorsDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getUHGAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getUHGAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/checkUHGVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkUHGVersionCode";

$route['native_single_fcm/native_single_fcm_v2/checkIdahoRealtorsVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIdahoRealtorsVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getMuveoAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getMuveoAllPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/checkMuveoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMuveoVersionCode";

$route['native_single_fcm/native_single_fcm_v2/MuveosearchEvent'] = "native_single_fcm/Native_single_fcm_v2/MuveosearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getCityWeekDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCityWeekDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkCityWeekVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCityWeekVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAICEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAICEDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkAICEVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAICEVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getVenusDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getVenusDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkVenusVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkVenusVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getEuroviaDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuroviaDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkEuroviaVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkEuroviaVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getSourceMediaAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getSourceMediaAllPublicEvents";



$route['native_single_fcm/native_single_fcm_v2/checkSourceMediaVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkSourceMediaVersionCode";

$route['native_single_fcm/native_single_fcm_v2/SourceMediasearchEvent'] = "native_single_fcm/Native_single_fcm_v2/SourceMediasearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getConnectechDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getConnectechDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkConnectechVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConnectechVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getEpsonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEpsonDefaultEvent";




$route['native_single_fcm/native_single_fcm_v2/searchEpsonEventBySecurekey'] = "native_single_fcm/Native_single_fcm_v2/searchEpsonEventBySecurekey";

$route['native_single_fcm/native_single_fcm_v2/checkEpsonVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkEpsonVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getiAmericas2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getiAmericas2018DefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkiAmericas2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkiAmericas2018VersionCode";

$route['native_single_fcm/native_single_fcm_v2/getMIT2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getMIT2018DefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkMIT2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMIT2018VersionCode";

$route['native_single_fcm/native_single_fcm_v2/getGlobalRegTechDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGlobalRegTechDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkGlobalRegTechVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkGlobalRegTechVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getBigDataTorontoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBigDataTorontoDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkBigDataTorontoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBigDataTorontoVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getSIAMAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getSIAMAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/checkSIAMVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkSIAMVersionCode";

$route['native_single_fcm/native_single_fcm_v2/SIAMsearchEvent'] = "native_single_fcm/Native_single_fcm_v2/SIAMsearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getRetailBulletinMasterEvent'] = "native_single_fcm/Native_single_fcm_v2/getRetailBulletinMasterEvent";

$route['native_single_fcm/native_single_fcm_v2/checkRetailBulletinVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkRetailBulletinVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getRetailBulletinPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getRetailBulletinPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/getClosingTheGapDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getClosingTheGapDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkClosingTheGapVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkClosingTheGapVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getPodcastMovementDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPodcastMovementDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkPodcastMovementVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkPodcastMovementVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getUHGLatestDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getUHGLatestDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getFIMEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFIMEDefaultEvent";



$route['native_single_fcm/native_single_fcm_v2/checkFIMEVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFIMEVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getSmartAirportsDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getSmartAirportsDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkSmartAirportsVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkSmartAirportsVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getHowardhughesDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getHowardhughesDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkHowardhughesVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkHowardhughesVersionCode";

$route['native_single_fcm/native_single_fcm_v2/checkDynamiteVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkDynamiteVersionCode";

$route['native_single_fcm/native_single_fcm_v2/DynamiteSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/DynamiteSearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getFoodMattersDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFoodMattersDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkFoodMattersVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFoodMattersVersionCode";

$route['native_single_fcm/native_single_fcm_v2/checkConcordiaVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConcordiaVersionCode";

$route['native_single_fcm/native_single_fcm_v2/searchSmartAirportEventBySecurekey'] = "native_single_fcm/Native_single_fcm_v2/searchSmartAirportEventBySecurekey";

$route['native_single_fcm/native_single_fcm_v2/getConcordiaMasterEvent'] = "native_single_fcm/Native_single_fcm_v2/getConcordiaMasterEvent";

$route['native_single_fcm/native_single_fcm_v2/getCompassionateFriendsDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCompassionateFriendsDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkCompassionateFriendsVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCompassionateFriendsVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getNCRealtorAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getNCRealtorAllPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/checkNCRealtorVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkNCRealtorVersionCode";

$route['native_single_fcm/native_single_fcm_v2/NCRealtorDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/NCRealtorDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getPurposeBuiltCommunitiesDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPurposeBuiltCommunitiesDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkPurposeBuiltCommunitiesVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkPurposeBuiltCommunitiesVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getPPMADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPPMADefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkPPMAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkPPMAVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getTricordAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getTricordAllPublicEvents";

$route['native_single_fcm/native_single_fcm_v2/checkTricordVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkTricordVersionCode";

$route['native_single_fcm/native_single_fcm_v2/TricordSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/TricordSearchEvent";

$route['native_single_fcm/native_single_fcm_v2/searchTricordEventBySecurekey'] = "native_single_fcm/Native_single_fcm_v2/searchTricordEventBySecurekey";

$route['native_single_fcm/native_single_fcm_v2/searchCoburnsEventBySecurekey'] = "native_single_fcm/Native_single_fcm_v2/searchCoburnsEventBySecurekey";

$route['native_single_fcm/native_single_fcm_v2/getWarriorDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getWarriorDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkWarriorVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkWarriorVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAASADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAASADefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkAASAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAASAVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getDCACDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getDCACDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkDCACersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkDCACersionCode";

$route['native_single_fcm/native_single_fcm_v2/getVitafoodsDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getVitafoodsDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkVitafoodsersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkVitafoodsersionCode";

$route['native_single_fcm/native_single_fcm_v2/getFinancialServicesDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFinancialServicesDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkFinancialServicesVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFinancialServicesVersionCode";



$route['native_single_fcm/native_single_fcm_v2/getAlignHRDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAlignHRDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkAlignHRVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAlignHRVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getFranklinCoveyDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFranklinCoveyDefaultEvent";



$route['native_single_fcm/native_single_fcm_v2/checkFranklinCoveyVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFranklinCoveyVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getPCAWaterproofingDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getPCAWaterproofingDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkPCAWaterproofingVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkPCAWaterproofingVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getCRForumLisbonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCRForumLisbonDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkCRForumLisbonVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCRForumLisbonVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getIPExpoEuropeDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getIPExpoEuropeDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkIPExpoEuropeVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIPExpoEuropeVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getBTSConferenceDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBTSConferenceDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkBTSConferenceVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBTSConferenceVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getOTRFilmFestDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getOTRFilmFestDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkOTRFilmFestVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkOTRFilmFestVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getGemConferenceDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGemConferenceDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkGemConferenceVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkGemConferenceVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getATCDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getATCDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkATCVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkATCVersionCode";





$route['native_single_fcm/native_single_fcm/getGlidewellDefaultEvent'] = "native_single_fcm/Native_single_fcm/getGlidewellDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getGlidewellDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGlidewellDefaultEvent";



$route['native_single_fcm/native_single_fcm_v2/getApexPublicEventsApexSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/getApexPublicEventsApexSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/ApexSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ApexSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/getNHSDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getNHSDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkNHSVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkNHSVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getSIALMEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getSIALMEDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getCABSATDefaultEventcheckCABSATVersionCode'] = "native_single_fcm/Native_single_fcm_v2/getCABSATDefaultEventcheckCABSATVersionCode";


$route['native_single_fcm/native_single_fcm_v2/checkCABSATVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCABSATVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getComicConPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getComicConPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/checkComicConVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkComicConVersionCode";


$route['native_single_fcm/native_single_fcm_v2/ComicconSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ComicconSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/getONTDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getONTDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkCABSATVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCABSATVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getComicConPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getComicConPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/checkComicConVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkComicConVersionCode";


$route['native_single_fcm/native_single_fcm_v2/ComicconSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ComicconSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/getONTDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getONTDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getWPBDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getWPBDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getGamesforumDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGamesforumDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/KnectSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/KnectSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/getArabHelthDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getArabHelthDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkArabHelthVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkArabHelthVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getFHADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFHADefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkFHAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFHAVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getBondsAndLoansMasterEvent'] = "native_single_fcm/Native_single_fcm_v2/getBondsAndLoansMasterEvent";


$route['native_single_fcm/native_single_fcm_v2/getBondsAndLoansDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBondsAndLoansDefaultEvent";

$route['native_single_fcm/native_single_fcm/getBondsAndLoansDefaultEvent'] = "native_single_fcm/Native_single_fcm/getBondsAndLoansDefaultEvent";



$route['native_single_fcm/native_single_fcm_v2/checkBondsAndLoansVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBondsAndLoansVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getBondsAndLoansPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getBondsAndLoansPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/getConcreteExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getConcreteExpoDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkConcreteExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConcreteExpoVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getConnect2bDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getConnect2bDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkConnect2bVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConnect2bVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getMjBakerDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getMjBakerDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkMjBakerVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMjBakerVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getAvangridDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAvangridDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkAvangridVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAvangridVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getMedlabDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getMedlabDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkMedlabVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMedlabVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getCoburnsPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getCoburnsPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/CoburnsSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/CoburnsSearchEvent";


$route['native_single_fcm/native_single_fcm_v2/checkCoburnsVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCoburnsVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getGreyDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGreyDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkGreyVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkGreyVersionCode";


$route['native_single_fcm/native_single_fcm_v2/checkGreyVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkGreyVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getLTLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getLTLondonDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkLTLondonVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkLTLondonVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getLTFranceDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getLTFranceDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkLTFranceVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkLTFranceVersionCode";

$route['native_single_fcm/native_single_fcm/checkLTFranceVersionCode'] = "native_single_fcm/Native_single_fcm/checkLTFranceVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getDerivConeDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getDerivConeDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkDerivConVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkDerivConVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getFixIncome2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFixIncome2018DefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkFixIncome2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFixIncome2018VersionCode";


$route['native_single_fcm/native_single_fcm_v2/getIMEAPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getIMEAPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/IMEASearchEvent'] = "native_single_fcm/Native_single_fcm_v2/IMEASearchEvent";

$route['native_single_fcm/native_single_fcm_v2/checkIMEAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIMEAVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getUCASPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getUCASPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/UCASSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/UCASSearchEvent";

$route['native_single_fcm/native_single_fcm_v2/getAICDMemberDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAICDMemberDefaultEvent";

$route['native_single_fcm/native_single_fcm/getAICDMemberDefaultEvent'] = "native_single_fcm/Native_single_fcm/getAICDMemberDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getAICDShow2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAICDShow2018DefaultEvent";

$route['native_single_fcm/native_single_fcm/getAICDShow2018DefaultEvent'] = "native_single_fcm/Native_single_fcm/getAICDShow2018DefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkUCASVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkUCASVersionCode";

$route['native_single_fcm/native_single_fcm_v2/checkAICDVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAICDVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getErsteTalentDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getErsteTalentDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkErsteTalentVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkErsteTalentVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getEuromedicomAMWCDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomAMWCDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getEuromedicomFACEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomFACEDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getEuromedicomAMECLiveVISAGEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomAMECLiveVISAGEDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkEuromedicomVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkEuromedicomVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getArlarDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getArlarDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkArlarVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkArlarVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getIPExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getIPExpoDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkIPExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIPExpoVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getUCExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getUCExpoDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkUCExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkUCExpoVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getCityScapeAbuDhabiDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCityScapeAbuDhabiDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkCityScapeAbuDhabiVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCityScapeAbuDhabiVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAEI2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAEI2018DefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkAEI2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAEI2018VersionCode";


$route['native_single_fcm/native_single_fcm_v2/getBBGADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBBGADefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkBBGAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBBGAVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getBathAndWestDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBathAndWestDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkBathAndWestVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBathAndWestVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getTSAMLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMLondonDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getTSAMTorontoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMTorontoDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getTSAMNewYorkDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMNewYorkDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/getTSAMHongKongDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMHongKongDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/getTSAMBostonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMBostonDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkTSAMVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkTSAMVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAhicDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAhicDefaultEvent";


$route['native_single_fcm/native_single_fcm_v2/checkAhicVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAhicVersionCode";


$route['native_single_fcm/native_single_fcm_v2/getAviationMediaDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAviationMediaDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/checkAviationMediaVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAviationMediaVersionCode";


// $route['developmentusers/softdelete/(:any)'] = "Developmentusers/softdelete/$1";



/* ---------------------------------Dt : - 05/10/2018 --------------------------------------------------*/

$route['apiv2/add_temp_attendees_ucas'] = "Apiv2/add_temp_attendees_ucas";

$route['apiv2/add_attendees_ucas'] = "Apiv2/add_attendees_ucas";
$route['apiv2/add_ipexpo_attendee'] = "Apiv2/add_ipexpo_attendee";
$route['apiv2/add_ucexpo_attendee'] = "Apiv2/add_ucexpo_attendee";
$route['apiv2/Euromedicom_Speakers'] = "Apiv2/Euromedicom_Speakers";
$route['apiv2/Euromedicom_Speakers_add'] = "Apiv2/Euromedicom_Speakers_add";
$route['apiv2/Euromedicom_Agenda'] = "Apiv2/Euromedicom_Agenda";
$route['apiv2/Euromedicom_Agenda_add'] = "Apiv2/Euromedicom_Agenda_add";
$route['apiv2/Euromedicom_Sponsors'] = "Apiv2/Euromedicom_Sponsors";
$route['apiv2/Euromedicom_Sponsors_add'] = "Apiv2/Euromedicom_Sponsors_add";
$route['apiv2/Euromedicom_Exhi'] = "Apiv2/Euromedicom_Exhi";
$route['apiv2/Euromedicom_Exhi_add'] = "Apiv2/Euromedicom_Exhi_add";
$route['apiv2/add_medlab_attendee'] = "Apiv2/add_medlab_attendee";
$route['apiv2/assign_ucas_agenda'] = "Apiv2/assign_ucas_agenda";
$route['apiv2/arab_fill_lead_firstname'] = "Apiv2/arab_fill_lead_firstname";
$route['apiv2/arab_fill_lead_email'] = "Apiv2/arab_fill_lead_email";
$route['apiv2/arab_fill_lead_lastname'] = "Apiv2/arab_fill_lead_lastname";
$route['apiv2/cityscape_temp_exhi'] = "Apiv2/cityscape_temp_exhi";
$route['apiv2/add_cityscape_exhi'] = "Apiv2/add_cityscape_exhi";
$route['apiv2/fha_exhi_upload'] = "Apiv2/fha_exhi_upload";
$route['apiv2/connectech_asia_exhi_upload'] = "Apiv2/connectech_asia_exhi_upload";
$route['apiv2/ipexpo_speaker'] = "Apiv2/ipexpo_speaker";
$route['apiv2/ipexpo_speaker_add'] = "Apiv2/ipexpo_speaker_add";
$route['apiv2/ipexpo_agenda'] = "Apiv2/ipexpo_agenda";
$route['apiv2/ipexpo_agenda_add'] = "Apiv2/ipexpo_agenda_add";
$route['apiv2/ucexpo_speaker'] = "Apiv2/ucexpo_speaker";
$route['apiv2/ucexpo_speaker_add'] = "Apiv2/ucexpo_speaker_add";
$route['apiv2/ucexpo_agenda'] = "Apiv2/ucexpo_agenda";
$route['apiv2/ucexpo_agenda_add'] = "Apiv2/ucexpo_agenda_add";
$route['apiv2/Euromedicom_Speakers_MCA'] = "Apiv2/Euromedicom_Speakers_MCA";
$route['apiv2/Euromedicom_Speakers_add_MCA'] = "Apiv2/Euromedicom_Speakers_add_MCA";
$route['apiv2/Euromedicom_Agenda_face'] = "Apiv2/Euromedicom_Agenda_face";
$route['apiv2/Euromedicom_Agenda_add_face'] = "Apiv2/Euromedicom_Agenda_add_face";
$route['apiv2/Euromedicom_Sponsors_face'] = "Apiv2/Euromedicom_Sponsors_face";
$route['apiv2/Euromedicom_Sponsors_add_face'] = "Apiv2/Euromedicom_Sponsors_add_face";
$route['apiv2/Euromedicom_Exhi_face'] = "Apiv2/Euromedicom_Exhi_face";
$route['apiv2/Euromedicom_Exhi_add_face'] = "Apiv2/Euromedicom_Exhi_add_face";
$route['apiv2/mjbaker_meeting_upload'] = "Apiv2/mjbaker_meeting_upload";
$route['apiv2/napec_exhi'] = "Apiv2/napec_exhi";
$route['apiv2/napec_exhi_add'] = "Apiv2/napec_exhi_add";
$route['apiv2/HFTP'] = "Apiv2/HFTP";
$route['apiv2/HFTP_Exhi'] = "Apiv2/HFTP_Exhi";
$route['apiv2/HFTP_Exhi_add'] = "Apiv2/HFTP_Exhi_add";
$route['apiv2/modifyEmailsHFTP'] = "Apiv2/modifyEmailsHFTP";
$route['apiv2/check_unique_user'] = "Apiv2/check_unique_user";
$route['apiv2/add_HITECAmsterdam_attendee'] = "Apiv2/add_HITECAmsterdam_attendee";
$route['apiv2/add_HITECHouston_attendee'] = "Apiv2/add_HITECHouston_attendee";
$route['apiv2/add_HITEC3rdAnnualConvention_attendee'] = "Apiv2/add_HITEC3rdAnnualConvention_attendee";
$route['apiv2/add_Podcast_Movement_attendee'] = "Apiv2/add_Podcast_Movement_attendee";
$route['apiv2/add_temp_attendees_ucas_annual_admissions'] = "Apiv2/add_temp_attendees_ucas_annual_admissions";
$route['apiv2/add_temp_sessions_ucas_annual_admissions'] = "Apiv2/add_temp_sessions_ucas_annual_admissions";
$route['apiv2/add_attendees_ucas_annual_admission'] = "Apiv2/add_attendees_ucas_annual_admission";
$route['apiv2/assign_ucas_agenda_admissions'] = "Apiv2/assign_ucas_agenda_admissions";
$route['apiv2/delete_ucas_sessions_users'] = "Apiv2/delete_ucas_sessions_users";
$route['apiv2/test'] = "Apiv2/test";
$route['apiv2/ipexpo_users_saved_agenda'] = "Apiv2/ipexpo_users_saved_agenda";
$route['apiv2/ipexpo_users_checkin_agenda'] = "Apiv2/ipexpo_users_checkin_agenda";
$route['apiv2/import_attendee'] = "Apiv2/import_attendee";
$route['apiv2/add_ipexpoeurope_attendee'] = "Apiv2/add_ipexpoeurope_attendee";

$route['apiv3/index'] = "Apiv3/index";
$route['apiv3/add_temp_SugarEthanolBrazil'] = "Apiv3/add_temp_SugarEthanolBrazil";
$route['apiv3/add_SugarEthanolBrazil_data'] = "Apiv3/add_SugarEthanolBrazil_data";
$route['apiv3/add_temp_GlobalPortMarineOperations'] = "Apiv3/add_temp_GlobalPortMarineOperations";
$route['apiv3/add_GlobalPortMarineOperations_data'] = "Apiv3/add_GlobalPortMarineOperations_data";
$route['apiv3/TestSql'] = "Apiv3/TestSql";
$route['apiv3/db_connection_test'] = "Apiv3/db_connection_test";
$route['apiv3/add_temp_Knect_GlobalLiner'] = "Apiv3/add_temp_Knect_GlobalLiner";
$route['apiv3/add_Knect_GlobalLiner_data'] = "Apiv3/add_Knect_GlobalLiner_data";
$route['apiv3/EPSON_attendee'] = "Apiv3/EPSON_attendee";
$route['apiv3/add_attendees_epson'] = "Apiv3/add_attendees_epson";
$route['apiv3/csv_to_sql'] = "Apiv3/csv_to_sql";
$route['apiv3/upload_mass_attendee_source_media'] = "Apiv3/upload_mass_attendee_source_media";

$route['apiv3/assign_epson_agenda'] = "Apiv3/assign_epson_agenda";
$route['apiv3/scrape_iot_speakers'] = "Apiv3/scrape_iot_speakers";
$route['apiv3/backupDB'] = "Apiv3/backupDB";
$route['apiv3/add_attendees_dig_banking'] = "Apiv3/add_attendees_dig_banking";
$route['apiv3/testWebhook'] = "Apiv3/testWebhook";

$route['apiv3/podcast_tmp_attendee'] = "Apiv3/podcast_tmp_attendee";
$route['apiv3/podcast_attendee_add'] = "Apiv3/podcast_attendee_add";
$route['apiv3/getEventUpdateLog'] = "Apiv3/getEventUpdateLog";
$route['apiv3/poadcastWebhook'] = "Apiv3/poadcastWebhook";
$route['apiv3/testUpdateDate'] = "Apiv3/testUpdateDate";

$route['apiv3/updateModuleDate'] = "Apiv3/updateModuleDate";
$route['apiv3/add_attendees_fime'] = "Apiv3/add_attendees_fime";
$route['apiv3/add_attendees_invest'] = "Apiv3/add_attendees_invest";
$route['apiv3/invest_fill_lead_firstname'] = "Apiv3/invest_fill_lead_firstname";
$route['apiv3/invest_fill_lead_email'] = "Apiv3/invest_fill_lead_email";

$route['apiv3/invest_fill_lead_lastname'] = "Apiv3/invest_fill_lead_lastname";
$route['apiv3/pullWPData'] = "Apiv3/pullWPData";
$route['apiv3/assignHowardAgenda'] = "Apiv3/assignHowardAgenda";
$route['apiv3/vitafood_Exhi'] = "Apiv3/vitafood_Exhi";
$route['apiv3/add_vitafood_attendee'] = "Apiv3/add_vitafood_attendee";

$route['apiv3/fime_fill_lead_firstname'] = "Apiv3/fime_fill_lead_firstname";
$route['apiv3/fime_fill_lead_email'] = "Apiv3/fime_fill_lead_email";
$route['apiv3/fime_fill_lead_lastname'] = "Apiv3/fime_fill_lead_lastname";
$route['apiv3/test'] = "Apiv3/test";
$route['apiv3/add_mema_attendee'] = "Apiv3/add_mema_attendee";

$route['apiv3/add_ppma_attendee'] = "Apiv3/add_ppma_attendee";
$route['apiv3/add_HITEC_HOUSTON_attendee'] = "Apiv3/add_HITEC_HOUSTON_attendee";
$route['apiv3/add_Annual_Convention_attendee'] = "Apiv3/add_Annual_Convention_attendee";
$route['apiv3/removeAttendee'] = "Apiv3/removeAttendee";
$route['apiv3/vitafoodSaveExtraColumn'] = "Apiv3/vitafoodSaveExtraColumn";
$route['apiv3/vitafoodProducts'] = "Apiv3/vitafoodProducts";
$route['apiv3/vitafoodAssignGroup'] = "Apiv3/vitafoodAssignGroup";
$route['apiv3/vitafoodAssignKeywords'] = "Apiv3/vitafoodAssignKeywords";
$route['apiv3/Vitafood_Agenda'] = "Apiv3/Vitafood_Agenda";
$route['apiv3/Vitafood_Agenda_add'] = "Apiv3/Vitafood_Agenda_add";
$route['apiv3/updateExtraColumn'] = "Apiv3/updateExtraColumn";
$route['apiv3/updateCustomColumn'] = "Apiv3/updateCustomColumn";
$route['apiv3/ipexpo_europe_speaker'] = "Apiv3/ipexpo_europe_speaker";
$route['apiv3/ipexpo_europe_speaker_add'] = "Apiv3/ipexpo_europe_speaker_add";
$route['apiv3/ipexpo_agenda'] = "Apiv3/ipexpo_agenda";
$route['apiv3/ipexpo_agenda_add'] = "Apiv3/ipexpo_agenda_add";
$route['apiv3/PPMAaddExhi'] = "Apiv3/PPMAaddExhi";
$route['apiv3/addExhiUser'] = "Apiv3/addExhiUser";
$route['apiv3/getCountryIdFromName'] = "Apiv3/getCountryIdFromName";
$route['apiv3/getExhiTypeId'] = "Apiv3/getExhiTypeId";
$route['apiv3/add_exhibitor'] = "Apiv3/add_exhibitor";
$route['apiv3/Euromedicom_Exhi_MCA'] = "Apiv3/Euromedicom_Exhi_MCA";
$route['apiv3/Euromedicom_Exhi_add_MCA'] = "Apiv3/Euromedicom_Exhi_add_MCA";
$route['apiv3/Euromedicom_Sponsors_MCA'] = "Apiv3/Euromedicom_Sponsors_MCA";
$route['apiv3/Euromedicom_Sponsors_add_MCA'] = "Apiv3/Euromedicom_Sponsors_add_MCA";
$route['apiv3/add_attendees_digmort'] = "Apiv3/add_attendees_digmort";
$route['apiv3/euromedicom_agenda_update'] = "Apiv3/euromedicom_agenda_update";
$route['apiv3/NCrealotrsAttendee'] = "Apiv3/NCrealotrsAttendee";
$route['apiv3/NCrealotrsAttendee_add'] = "Apiv3/NCrealotrsAttendee_add";
$route['apiv3/add_attendees_BenefitsForum'] = "Apiv3/add_attendees_BenefitsForum";
$route['apiv3/CloaseTheGapAttendee_add'] = "Apiv3/CloaseTheGapAttendee_add";


$route['apiv4/index'] = "Apiv4/index";
$route['apiv4/VentureFestaddExhi'] = "Apiv4/VentureFestaddExhi";
$route['apiv4/VentureFestaddSpeaker'] = "Apiv4/VentureFestaddSpeaker";
$route['apiv4/getUserByEvent'] = "Apiv4/getUserByEvent";
$route['apiv4/setUserWithEvent'] = "Apiv4/setUserWithEvent";
$route['apiv4/VentureFestaddSession'] = "Apiv4/VentureFestaddSession";
$route['apiv4/VentureFestaddSponsor'] = "Apiv4/VentureFestaddSponsor";
$route['apiv4/resetNCRToken'] = "Apiv4/resetNCRToken";
$route['apiv4/NCRSessions'] = "Apiv4/NCRSessions";
$route['apiv4/NCRDocuments'] = "Apiv4/NCRDocuments";
$route['apiv4/NCRAgenda_add'] = "Apiv4/NCRAgenda_add";
$route['apiv4/NCRSessionsTypes'] = "Apiv4/NCRSessionsTypes";
$route['apiv4/NCRSpeakerSessions'] = "Apiv4/NCRSpeakerSessions";
$route['apiv4/NCRUsers'] = "Apiv4/NCRUsers";
$route['apiv4/NCRUsersAdd'] = "Apiv4/NCRUsersAdd";


$route['cronjob/tmpAttendeesImport'] = "Cronjob/tmpAttendeesImport";
$route['cronjob/tmpExhibitorImport'] = "Cronjob/tmpExhibitorImport";
$route['cronjob/saveAttendeeTMPtableData'] = "Cronjob/saveAttendeeTMPtableData";

$route['cronjob/import_attendee'] = "Cronjob/import_attendee";
$route['cronjob/import_exhi'] = "Cronjob/import_exhi";
$route['cronjob/test'] = "Cronjob/test";
$route['cronjob/json_to_sql'] = "Cronjob/json_to_sql";
$route['cronjob/json_to_sql_2'] = "Cronjob/json_to_sql_2";
$route['cronjob/import_data_to_temp'] = "Cronjob/import_data_to_temp";
$route['cronjob/import_data_to_temp_exhi'] = "Cronjob/import_data_to_temp_exhi";
$route['cronjob/csv_to_sql'] = "Cronjob/csv_to_sql";
$route['cronjob/escape'] = "Cronjob/escape";
$route['cronjob/quote'] = "Cronjob/quote";
$route['cronjob/is_utf8'] = "Cronjob/is_utf8";
$route['cronjob/insertUpdate'] = "Cronjob/insertUpdate";
$route['cronjob/csv_to_sql_2'] = "Cronjob/csv_to_sql_2";
$route['cronjob/import_agenda'] = "Cronjob/import_agenda";
$route['cronjob/import_speaker'] = "Cronjob/import_speaker";
$route['cronjob/import_data_temp'] = "Cronjob/import_data_temp";



/* --------------Date:06-10-2018 for native_single_fcm_v2----------------------*/

$route['native_single_fcm_v2/login/getFormBuillderDataByEvent'] = "native_single_fcm_v2/Login/getFormBuillderDataByEvent";
$route['native_single_fcm_v2/login/check'] = "native_single_fcm_v2/Login/check";
$route['native_single_fcm_v2/login/saveFormBuilderData'] = "native_single_fcm_v2/Login/saveFormBuilderData";
$route['native_single_fcm_v2/login/forgot_password'] = "native_single_fcm_v2/Login/forgot_password";
$route['native_single_fcm_v2/login/linkedInSignup'] = "native_single_fcm_v2/Login/linkedInSignup";
$route['native_single_fcm_v2/login/delete_user'] = "native_single_fcm_v2/Login/delete_user";
$route['native_single_fcm_v2/app/getUpdatedByModulesNew'] = "native_single_fcm_v2/App/getUpdatedByModulesNew";
$route['native_single_fcm_v2/app/get_login_screen_settings'] = "native_single_fcm_v2/App/get_login_screen_settings";
$route['native_single_fcm_v2/app/event_id'] = "native_single_fcm_v2/App/event_id";      /* ******************* */
$route['native_single_fcm_v2/app/event_id_advance_design'] = "native_single_fcm_v2/App/event_id_advance_design";
$route['native_single_fcm_v2/profile/update_contact_details'] = "native_single_fcm_v2/Profile/update_contact_details";
$route['native_single_fcm_v2/profile/update_contact_details_v2'] = "native_single_fcm_v2/Profile/update_contact_details_v2";
$route['native_single_fcm_v2/cms/get_cms_page'] = "native_single_fcm_v2/Cms/get_cms_page";
$route['native_single_fcm_v2/event/getAllPublicEvents'] = "native_single_fcm_v2/Event/getAllPublicEvents";
$route['native_single_fcm_v2/event/searchEvent'] = "native_single_fcm_v2/Event/searchEvent";
$route['native_single_fcm_v2/login/fb_signup'] = "native_single_fcm_v2/Login/fb_signup";
$route['native_single_fcm_v2/login/authorizedLogin'] = "native_single_fcm_v2/Login/authorizedLogin";
$route['native_single_fcm_v2/event/openApp'] = "native_single_fcm_v2/Event/openApp";      /* ******************* */
$route['native_single_fcm_v2/event/searchEventBySecurekey'] = "native_single_fcm_v2/Event/searchEventBySecurekey";
$route['native_single_fcm_v2/agenda/getAgendaByTime'] = "native_single_fcm_v2/Agenda/getAgendaByTime";  /* ***** */
$route['native_single_fcm_v2/agenda/getAgendaByType'] = "native_single_fcm_v2/Agenda/getAgendaByType";
$route['native_single_fcm_v2/login/getFormBuillderDataByEvent'] = "native_single_fcm_v2/Login/getFormBuillderDataByEvent";
$route['native_single_fcm_v2/profile/getStateList'] = "native_single_fcm_v2/Profile/getStateList";
$route['native_single_fcm_v2/attendee/attendee_list_native_new'] = "native_single_fcm_v2/Attendee/attendee_list_native_new";
$route['native_single_fcm_v2/attendee/getAttendeeCategories'] = "native_single_fcm_v2/Attendee/getAttendeeCategories";
$route['native_single_fcm_v2/attendee/attendee_view'] = "native_single_fcm_v2/Attendee/attendee_view";

$route['native_single_fcm_v2/attendee/attendee_view_unread_count'] = "native_single_fcm_v2/Attendee/attendee_view_unread_count";

$route['native_single_fcm_v2/attendee/delete_comment'] = "native_single_fcm_v2/Attendee/delete_comment";
$route['native_single_fcm_v2/attendee/attendee_contact_list_new'] = "native_single_fcm_v2/Attendee/attendee_contact_list_new";
$route['native_single_fcm_v2/exhibitor/exhibitor_list_native_new'] = "native_single_fcm_v2/Exhibitor/exhibitor_list_native_new";
$route['native_single_fcm_v2/exhibitor/exhibitor_parent_category'] = "native_single_fcm_v2/Exhibitor/exhibitor_parent_category";
$route['native_single_fcm_v2/exhibitor/exhibitor_category'] = "native_single_fcm_v2/Exhibitor/exhibitor_category";
$route['native_single_fcm_v2/exhibitor/exhibitorSearch'] = "native_single_fcm_v2/Exhibitor/exhibitorSearch";

$route['native_single_fcm_v2/exhibitor/exhibitor_view_unread_count'] = "native_single_fcm_v2/Exhibitor/exhibitor_view_unread_count";
$route['native_single_fcm_v2/exhibitor/delete_comment'] = "native_single_fcm_v2/Exhibitor/delete_comment";
$route['native_single_fcm_v2/exhibitor/make_comment'] = "native_single_fcm_v2/Exhibitor/make_comment";
$route['native_single_fcm_v2/exhibitor/delete_message'] = "native_single_fcm_v2/Exhibitor/delete_message";
$route['native_single_fcm_v2/exhibitor/shareContactInformation'] = "native_single_fcm_v2/Exhibitor/shareContactInformation";
$route['native_single_fcm_v2/speaker/speaker_list_offline'] = "native_single_fcm_v2/Speaker/speaker_list_offline";



$route['native_single_fcm_v2/speaker/speaker_list_offline'] = "native_single_fcm_v2/Speaker/speaker_list_offline";
$route['native_single_fcm_v2/speaker/speaker_view_unread_count'] = "native_single_fcm_v2/Speaker/speaker_view_unread_count";
$route['native_single_fcm_v2/speaker/delete_comment'] = "native_single_fcm_v2/Speaker/delete_comment";
$route['native_single_fcm_v2/maps/map_list_new'] = "native_single_fcm_v2/Maps/map_list_new";
$route['native_single_fcm_v2/cms/get_cms_list_details'] = "native_single_fcm_v2/Cms/get_cms_list_details";
$route['native_single_fcm_v2/maps/map_details'] = "native_single_fcm_v2/Maps/map_details";


$route['native_single_fcm_v2/social/social_list'] = "native_single_fcm_v2/Social/social_list";
$route['native_single_fcm_v2/note/add_note_new'] = "native_single_fcm_v2/Note/add_note_new";
$route['native_single_fcm_v2/note/edit_note'] = "native_single_fcm_v2/Note/edit_note";
$route['native_single_fcm_v2/note/note_list_new'] = "native_single_fcm_v2/Note/note_list_new";
$route['native_single_fcm_v2/profile/update_profile'] = "native_single_fcm_v2/Profile/update_profile";
$route['native_single_fcm_v2/profile/update_profile_v2'] = "native_single_fcm_v2/Profile/update_profile_v2";
$route['native_single_fcm_v2/agenda/getAgendaById'] = "native_single_fcm_v2/Agenda/getAgendaById";


$route['native_single_fcm_v2/agenda/saveAgendaComments'] = "native_single_fcm_v2/Agenda/saveAgendaComments";
$route['native_single_fcm_v2/agenda/saveUserAgenda'] = "native_single_fcm_v2/Agenda/saveUserAgenda";
$route['native_single_fcm_v2/agenda/deleteUserAgenda'] = "native_single_fcm_v2/Agenda/deleteUserAgenda";
$route['native_single_fcm_v2/agenda/checkInAgenda'] = "native_single_fcm_v2/Agenda/checkInAgenda";
$route['native_single_fcm_v2/agenda/saveRating'] = "native_single_fcm_v2/Agenda/saveRating";
$route['native_single_fcm_v2/agenda/getUserAgendaByTime'] = "native_single_fcm_v2/Agenda/getUserAgendaByTime";


$route['native_single_fcm_v2/agenda/getUserAgendaByType'] = "native_single_fcm_v2/Agenda/getUserAgendaByType";
$route['native_single_fcm_v2/agenda/getPendingUserAgendaByTime'] = "native_single_fcm_v2/Agenda/getPendingUserAgendaByTime";
$route['native_single_fcm_v2/agenda/getPendingUserAgendaByType'] = "native_single_fcm_v2/Agenda/getPendingUserAgendaByType";
$route['native_single_fcm_v2/agenda/savePendingUserAgenda'] = "native_single_fcm_v2/Agenda/savePendingUserAgenda";
$route['native_single_fcm_v2/agenda/deleteUserPendingAgenda'] = "native_single_fcm_v2/Agenda/deleteUserPendingAgenda";
$route['native_single_fcm_v2/document/documents_list_new'] = "native_single_fcm_v2/Document/documents_list_new";



$route['native_single_fcm_v2/document/searchDocuments'] = "native_single_fcm_v2/Document/searchDocuments";
$route['native_single_fcm_v2/document/folder_view'] = "native_single_fcm_v2/Document/folder_view";
$route['native_single_fcm_v2/fundraising/fundraising_home'] = "native_single_fcm_v2/Fundraising/fundraising_home";
$route['native_single_fcm_v2/fundraising/fundraising_products'] = "native_single_fcm_v2/Fundraising/fundraising_products";
$route['native_single_fcm_v2/survey/get_category_wise_survey'] = "native_single_fcm_v2/Survey/get_category_wise_survey";
$route['native_single_fcm_v2/survey/saveSurvey'] = "native_single_fcm_v2/Survey/saveSurvey";


$route['native_single_fcm_v2/sponsors/sponsors_list'] = "native_single_fcm_v2/Sponsors/sponsors_list";
$route['native_single_fcm_v2/sponsors/sponsors_list_offline'] = "native_single_fcm_v2/Sponsors/sponsors_list_offline";
$route['native_single_fcm_v2/sponsors/sponsors_view'] = "native_single_fcm_v2/Sponsors/sponsors_view";
$route['native_single_fcm_v2/sponsors/get_favorited_sponser'] = "native_single_fcm_v2/Sponsors/get_favorited_sponser";
$route['native_single_fcm_v2/speaker/get_favorited_speakers'] = "native_single_fcm_v2/Speaker/get_favorited_speakers";
$route['native_single_fcm_v2/login/updateUserGCMId'] = "native_single_fcm_v2/Login/updateUserGCMId";


$route['native_single_fcm_v2/message/publicMessageSend'] = "native_single_fcm_v2/Message/publicMessageSend";
$route['native_single_fcm_v2/message/public_msg_images_request'] = "native_single_fcm_v2/Message/public_msg_images_request";
$route['native_single_fcm_v2/message/getPublicMessages'] = "native_single_fcm_v2/Message/getPublicMessages";
$route['native_single_fcm_v2/message/make_comment'] = "native_single_fcm_v2/Message/make_comment";
$route['native_single_fcm_v2/message/delete_comment'] = "native_single_fcm_v2/Message/delete_comment";
$route['native_single_fcm_v2/message/delete_message'] = "native_single_fcm_v2/Message/delete_message";



$route['native_single_fcm_v2/message/getPrivateMessages'] = "native_single_fcm_v2/Message/getPrivateMessages";
$route['native_single_fcm_v2/message/getAllSpeakersAttendee'] = "native_single_fcm_v2/Message/getAllSpeakersAttendee";
$route['native_single_fcm_v2/message/privateMessageSend'] = "native_single_fcm_v2/Message/privateMessageSend";
$route['native_single_fcm_v2/message/privateMessageSendText'] = "native_single_fcm_v2/Message/privateMessageSendText";
$route['native_single_fcm_v2/message/privateMessageSendImage'] = "native_single_fcm_v2/Message/privateMessageSendImage";
$route['native_single_fcm_v2/photo/getPublicFeeds'] = "native_single_fcm_v2/Photo/getPublicFeeds";


$route['native_single_fcm_v2/photo/UploadPublicFeeds'] = "native_single_fcm_v2/Photo/UploadPublicFeeds";
$route['native_single_fcm_v2/photo/like_feed'] = "native_single_fcm_v2/Photo/like_feed";
$route['native_single_fcm_v2/photo/dislike_feed'] = "native_single_fcm_v2/Photo/dislike_feed";
$route['native_single_fcm_v2/photo/delete_feed'] = "native_single_fcm_v2/Photo/delete_feed";
$route['native_single_fcm_v2/photo/make_comment'] = "native_single_fcm_v2/Photo/make_comment";
$route['native_single_fcm_v2/photo/delete_comment'] = "native_single_fcm_v2/Photo/delete_comment";



$route['native_single_fcm_v2/note/delete_note'] = "native_single_fcm_v2/Note/delete_note";
$route['native_single_fcm_v2/product/product_listing'] = "native_single_fcm_v2/Product/product_listing";
$route['native_single_fcm_v2/product/slient_products_details'] = "native_single_fcm_v2/Product/slient_products_details";
$route['native_single_fcm_v2/product/getAuctionWiseProductId'] = "native_single_fcm_v2/Product/getAuctionWiseProductId";
$route['native_single_fcm_v2/product/live_products_details'] = "native_single_fcm_v2/Product/live_products_details";
$route['native_single_fcm_v2/product/online_shop_details'] = "native_single_fcm_v2/Product/online_shop_details";


$route['native_single_fcm_v2/product/donation_details'] = "native_single_fcm_v2/Product/donation_details";
$route['native_single_fcm_v2/product/cart_details'] = "native_single_fcm_v2/Product/cart_details";
$route['native_single_fcm_v2/product/add_to_cart'] = "native_single_fcm_v2/Product/add_to_cart";
$route['native_single_fcm_v2/product/save_bid_details'] = "native_single_fcm_v2/Product/save_bid_details";
$route['native_single_fcm_v2/product/checkout_details'] = "native_single_fcm_v2/Product/checkout_details";
$route['native_single_fcm_v2/product/delete_cart_product'] = "native_single_fcm_v2/Product/delete_cart_product";



$route['native_single_fcm_v2/product/update_cart_quantity'] = "native_single_fcm_v2/Product/update_cart_quantity";
$route['native_single_fcm_v2/social/getTwitetrFeedsNew'] = "native_single_fcm_v2/Social/getTwitetrFeedsNew";
$route['native_single_fcm_v2/social/getTwitetrHashtags'] = "native_single_fcm_v2/Social/getTwitetrHashtags";
$route['native_single_fcm_v2/photo/getImageList'] = "native_single_fcm_v2/Photo/getImageList";
$route['native_single_fcm_v2/message/getImageList'] = "native_single_fcm_v2/Message/getImageList";
$route['native_single_fcm_v2/attendee/getAllMeetingRequestModerator'] = "native_single_fcm_v2/Attendee/getAllMeetingRequestModerator";



$route['native_single_fcm_v2/attendee/getAllMeetingRequestWithDateModerator'] = "native_single_fcm_v2/Attendee/getAllMeetingRequestWithDateModerator";
$route['native_single_fcm_v2/attendee/respondRequestModerator'] = "native_single_fcm_v2/Attendee/respondRequestModerator";
$route['native_single_fcm_v2/attendee/suggestMeetingTimeModerator'] = "native_single_fcm_v2/Attendee/suggestMeetingTimeModerator";
$route['native_single_fcm_v2/attendee/SaveCommentModerator'] = "native_single_fcm_v2/Attendee/SaveCommentModerator";
$route['native_single_fcm_v2/product/confirmOrder'] = "native_single_fcm_v2/Product/confirmOrder";
$route['native_single_fcm_v2/fundraising/save_fundraising_donation_details'] = "native_single_fcm_v2/Fundraising/save_fundraising_donation_details";


$route['native_single_fcm_v2/fundraising/save_instant_donation_details'] = "native_single_fcm_v2/Fundraising/save_instant_donation_details";
$route['native_single_fcm_v2/fundraising/get_fundraising_donation_details'] = "native_single_fcm_v2/Fundraising/get_fundraising_donation_details";
$route['native_single_fcm_v2/fundraising/orders'] = "native_single_fcm_v2/Fundraising/orders";
$route['native_single_fcm_v2/product/add_item'] = "native_single_fcm_v2/Product/add_item";
$route['native_single_fcm_v2/product/get_item_list'] = "native_single_fcm_v2/Product/get_item_list";
$route['native_single_fcm_v2/product/delete_item'] = "native_single_fcm_v2/Product/delete_item";



$route['native_single_fcm_v2/product/get_categories'] = "native_single_fcm_v2/Product/get_categories";
$route['native_single_fcm_v2/product/save_image'] = "native_single_fcm_v2/Product/save_image";
$route['native_single_fcm_v2/product/get_item_details'] = "native_single_fcm_v2/Product/get_item_details";
$route['native_single_fcm_v2/product/product_thumbnail'] = "native_single_fcm_v2/Product/product_thumbnail";
$route['native_single_fcm_v2/activity/getFeeds'] = "native_single_fcm_v2/Activity/getFeeds";




$route['native_single_fcm_v2/activity/get_all'] = "native_single_fcm_v2/Activity/get_all";
$route['native_single_fcm_v2/activity/get_internal'] = "native_single_fcm_v2/Activity/get_internal";
$route['native_single_fcm_v2/activity/get_social'] = "native_single_fcm_v2/Activity/get_social";
$route['native_single_fcm_v2/activity/get_alert'] = "native_single_fcm_v2/Activity/get_alert";
$route['native_single_fcm_v2/activity/feedLikeNew'] = "native_single_fcm_v2/Activity/feedLikeNew";
$route['native_single_fcm_v2/activity/feedCommentNew'] = "native_single_fcm_v2/Activity/feedCommentNew";



$route['native_single_fcm_v2/activity/deleteFeedComment'] = "native_single_fcm_v2/Activity/deleteFeedComment";
$route['native_single_fcm_v2/activity/saveActivityImages'] = "native_single_fcm_v2/Activity/saveActivityImages";
$route['native_single_fcm_v2/activity/add_photo_filter_image'] = "native_single_fcm_v2/Activity/add_photo_filter_image";
$route['native_single_fcm_v2/activity/get_photofilter_url'] = "native_single_fcm_v2/Activity/get_photofilter_url";
$route['native_single_fcm_v2/activity/postUpdate'] = "native_single_fcm_v2/Activity/postUpdate";
$route['native_single_fcm_v2/activity/getFeedDetails'] = "native_single_fcm_v2/Activity/getFeedDetails";



$route['native_single_fcm_v2/activity/getImageList'] = "native_single_fcm_v2/Activity/getImageList";
$route['native_single_fcm_v2/product/authorize_payment'] = "native_single_fcm_v2/Product/authorize_payment";
$route['native_single_fcm_v2/settings/getAdvertising_new'] = "native_single_fcm_v2/Settings/getAdvertising_new";
$route['native_single_fcm_v2/product/stripePayment'] = "native_single_fcm_v2/Product/stripePayment";
$route['native_single_fcm_v2/message/notificationCounter'] = "native_single_fcm_v2/Message/notificationCounter";
$route['native_single_fcm_v2/message/messageRead'] = "native_single_fcm_v2/Message/messageRead";



$route['native_single_fcm_v2/social/getInstagramFeeds'] = "native_single_fcm_v2/Social/getInstagramFeeds";
$route['native_single_fcm_v2/app/getFavouritedExhibitors'] = "native_single_fcm_v2/App/getFavouritedExhibitors";
$route['native_single_fcm_v2/app/getModuleUpdateDate'] = "native_single_fcm_v2/App/getModuleUpdateDate";
$route['native_single_fcm_v2/exhibitor/getExhibitorListCategoryPcategoriesoffline'] = "native_single_fcm_v2/Exhibitor/getExhibitorListCategoryPcategoriesoffline";
$route['native_single_fcm_v2/group/get_group_data'] = "native_single_fcm_v2/Group/get_group_data";
$route['native_single_fcm_v2/group/get_menu_group'] = "native_single_fcm_v2/Group/get_menu_group";


$route['native_single_fcm_v2/cms/get_cms_super_group'] = "native_single_fcm_v2/Cms/get_cms_super_group";
$route['native_single_fcm_v2/cms/get_chid_cms_group'] = "native_single_fcm_v2/Cms/get_chid_cms_group";
$route['native_single_fcm_v2/get_child_cms_list'] = "native_single_fcm_v2/get_child_cms_list";
$route['native_single_fcm_v2/agenda/get_agenda_offline'] = "native_single_fcm_v2/Agenda/get_agenda_offline";
$route['native_single_fcm_v2/presentation1/lockUnlockSlides'] = "native_single_fcm_v2/Presentation1/lockUnlockSlides";
$route['native_single_fcm_v2/presentation1/pushSlide'] = "native_single_fcm_v2/Presentation1/pushSlide";


$route['native_single_fcm_v2/presentation1/saveSurveyAnswer'] = "native_single_fcm_v2/Presentation1/saveSurveyAnswer";
$route['native_single_fcm_v2/presentation1/pushResult'] = "native_single_fcm_v2/Presentation1/pushResult";
$route['native_single_fcm_v2/presentation1/getChartResult'] = "native_single_fcm_v2/Presentation1/getChartResult";
$route['native_single_fcm_v2/presentation1/removeChartResult'] = "native_single_fcm_v2/Presentation1/removeChartResult";
$route['native_single_fcm_v2/presentation1/removePushSlide'] = "native_single_fcm_v2/Presentation1/removePushSlide";
$route['native_single_fcm_v2/presentation1/getPrsentationByTime'] = "native_single_fcm_v2/Presentation1/getPrsentationByTime";


$route['native_single_fcm_v2/presentation1/getPrsentationImagesByIdRefresh'] = "native_single_fcm_v2/Presentation1/getPrsentationImagesByIdRefresh";
$route['native_single_fcm_v2/presentation1/getPrsentationByType'] = "native_single_fcm_v2/Presentation1/getPrsentationByType";
$route['native_single_fcm_v2/presentation1/viewPresentationByRole'] = "native_single_fcm_v2/Presentation1/viewPresentationByRole";
$route['native_single_fcm_v2/presentation1/sendmessage'] = "native_single_fcm_v2/Presentation1/sendmessage";
$route['native_single_fcm_v2/checkin/attendeeList'] = "native_single_fcm_v2/Checkin/attendeeList";
$route['native_single_fcm_v2/checkin/get_checkedin_attendee'] = "native_single_fcm_v2/Checkin/get_checkedin_attendee";



$route['native_single_fcm_v2/checkin/attendeeCheckIn'] = "native_single_fcm_v2/Checkin/attendeeCheckIn";
$route['native_single_fcm_v2/checkin/viewAttendeeInfo'] = "native_single_fcm_v2/Checkin/viewAttendeeInfo";
$route['native_single_fcm_v2/checkin/saveAttendeeInfo'] = "native_single_fcm_v2/Checkin/saveAttendeeInfo";
$route['native_single_fcm_v2/attendee/shareContactInformation'] = "native_single_fcm_v2/Attendee/shareContactInformation";
$route['native_single_fcm_v2/attendee/approveStatus'] = "native_single_fcm_v2/Attendee/approveStatus";
$route['native_single_fcm_v2/attendee/rejectStatus'] = "native_single_fcm_v2/Attendee/rejectStatus";


$route['native_single_fcm_v2/exhibitor/getRequestMeetingDateTime'] = "native_single_fcm_v2/Exhibitor/getRequestMeetingDateTime";
$route['native_single_fcm_v2/exhibitor/requestMeeting'] = "native_single_fcm_v2/Exhibitor/requestMeeting";
$route['native_single_fcm_v2/exhibitor/saveRequestMeeting'] = "native_single_fcm_v2/Exhibitor/saveRequestMeeting";
$route['native_single_fcm_v2/exhibitor/getAllMeetingRequestNew'] = "native_single_fcm_v2/Exhibitor/getAllMeetingRequestNew";
$route['native_single_fcm_v2/exhibitor/getAllMeetingRequestWithDate'] = "native_single_fcm_v2/Exhibitor/getAllMeetingRequestWithDate";
$route['native_single_fcm_v2/exhibitor/respondRequest'] = "native_single_fcm_v2/Exhibitor/respondRequest";


$route['native_single_fcm_v2/reminder/setAgendaReminder'] = "native_single_fcm_v2/reminder/setAgendaReminder";
$route['native_single_fcm_v2/message/getNotificationListing'] = "native_single_fcm_v2/Message/getNotificationListing";
$route['native_single_fcm_v2/message/deleteNotification'] = "native_single_fcm_v2/Message/deleteNotification";
$route['native_single_fcm_v2/settings/userClickBoard'] = "native_single_fcm_v2/Settings/userClickBoard";
$route['native_single_fcm_v2/exhibitor/getRequestMeetingDateTime'] = "native_single_fcm_v2/Exhibitor/getRequestMeetingDateTime";
$route['native_single_fcm_v2/exhibitor/suggestMeetingTime'] = "native_single_fcm_v2/Exhibitor/suggestMeetingTime";


$route['native_single_fcm_v2/exhibitor/getSuggestedTimings'] = "native_single_fcm_v2/Exhibitor/getSuggestedTimings";
$route['native_single_fcm_v2/exhibitor/bookSuggestedTime'] = "native_single_fcm_v2/Exhibitor/bookSuggestedTime";
$route['native_single_fcm_v2/social/getFacebookFeed'] = "native_single_fcm_v2/Social/getFacebookFeed";
$route['native_single_fcm_v2/settings/checkVersionCode'] = "native_single_fcm_v2/Settings/checkVersionCode";
$route['native_single_fcm_v2/attendee/getRequestMeetingDateTime'] = "native_single_fcm_v2/Attendee/getRequestMeetingDateTime";
$route['native_single_fcm_v2/attendee/getRequestMeetingDate'] = "native_single_fcm_v2/Attendee/getRequestMeetingDate";


$route['native_single_fcm_v2/attendee/suggestMeetingTime'] = "native_single_fcm_v2/Attendee/suggestMeetingTime";
$route['native_single_fcm_v2/favorites/addOrRemove'] = "native_single_fcm_v2/Favorites/addOrRemove";
$route['native_single_fcm_v2/favorites/addOrRemoveAttendee'] = "native_single_fcm_v2/Favorites/addOrRemoveAttendee";
$route['native_single_fcm_v2/exhibitor/saveToFavorites'] = "native_single_fcm_v2/Exhibitor/saveToFavorites";
$route['native_single_fcm_v2/favorites/getFavoritesList'] = "native_single_fcm_v2/Favorites/getFavoritesList";
$route['native_single_fcm_v2/qa/getAllSession'] = "native_single_fcm_v2/Qa/getAllSession";


$route['native_single_fcm_v2/qa/getSessionDetail'] = "native_single_fcm_v2/Qa/getSessionDetail";
$route['native_single_fcm_v2/qa/VoteMessage'] = "native_single_fcm_v2/Qa/VoteMessage";
$route['native_single_fcm_v2/qa/sendMessage'] = "native_single_fcm_v2/Qa/sendMessage";
$route['native_single_fcm_v2/message/getPrivateUnreadMessagesList_tmp'] = "native_single_fcm_v2/Message/getPrivateUnreadMessagesList_tmp";
$route['native_single_fcm_v2/message/getPrivateMessagesConversation'] = "native_single_fcm_v2/Message/getPrivateMessagesConversation";
$route['native_single_fcm_v2/gulfood/getVirtualSuperMarket'] = "native_single_fcm_v2/Gulfood/getVirtualSuperMarket";


$route['native_single_fcm_v2/gulfood/getExhibitorProfile'] = "native_single_fcm_v2/Gulfood/getExhibitorProfile";
$route['native_single_fcm_v2/beacons/save_beacon'] = "native_single_fcm_v2/Beacons/save_beacon";
$route['native_single_fcm_v2/checkin/addCheckin_new'] = "native_single_fcm_v2/Checkin/addCheckin_new";
$route['native_single_fcm_v2/beacons/get_all_beacon'] = "native_single_fcm_v2/Beacons/get_all_beacon";
$route['native_single_fcm_v2/beacons/get_notification'] = "native_single_fcm_v2/Beacons/get_notification";
$route['native_single_fcm_v2/beacons/rename_beacon'] = "native_single_fcm_v2/Beacons/rename_beacon";


$route['native_single_fcm_v2/beacons/delete_beacon'] = "native_single_fcm_v2/Beacons/delete_beacon";
$route['native_single_fcm_v2/checkin/get_pdf'] = "native_single_fcm_v2/Checkin/get_pdf";
$route['native_single_fcm_v2/profile/get_additional_data'] = "native_single_fcm_v2/Profile/get_additional_data";
$route['native_single_fcm_v2/profile/update_profile_with_additional_data'] = "native_single_fcm_v2/Profile/update_profile_with_additional_data";
$route['native_single_fcm_v2/survey/get_survey_category'] = "native_single_fcm_v2/Survey/get_survey_category";
$route['native_single_fcm_v2/qa/DeleteMessage'] = "native_single_fcm_v2/Qa/DeleteMessage";


$route['native_single_fcm_v2/qa/Approve_qa'] = "native_single_fcm_v2/Qa/Approve_qa";
$route['native_single_fcm_v2/qa/show_on_web'] = "native_single_fcm_v2/Qa/show_on_web";
$route['native_single_fcm_v2/gamification/get_leaderboard'] = "native_single_fcm_v2/Gamification/get_leaderboard";
$route['native_single_fcm_v2/settings/get_o_screen'] = "native_single_fcm_v2/Settings/get_o_screen";
$route['native_single_fcm_v2/lead/scan_lead'] = "native_single_fcm_v2/Lead/scan_lead";
$route['native_single_fcm_v2/lead/get_exhi_leads'] = "native_single_fcm_v2/Lead/get_exhi_leads";


$route['native_single_fcm_v2/lead/remove_rep'] = "native_single_fcm_v2/Lead/remove_rep";
$route['native_single_fcm_v2/lead/save_questions'] = "native_single_fcm_v2/Lead/save_questions";
$route['native_single_fcm_v2/lead/reset_questions_answer'] = "native_single_fcm_v2/Lead/reset_questions_answer";
$route['native_single_fcm_v2/lead/update_questions_answer'] = "native_single_fcm_v2/Lead/update_questions_answer";
$route['native_single_fcm_v2/lead/get_attendee_list_for_rep'] = "native_single_fcm_v2/Lead/get_attendee_list_for_rep";
$route['native_single_fcm_v2/lead/add_new_representative'] = "native_single_fcm_v2/Lead/add_new_representative";


$route['native_single_fcm_v2/lead/rep_list'] = "native_single_fcm_v2/Lead/rep_list";
$route['native_single_fcm_v2/lead/export_lead'] = "native_single_fcm_v2/Lead/export_lead";
$route['native_single_fcm_v2/lead/save_scan_lead'] = "native_single_fcm_v2/Lead/save_scan_lead";
$route['native_single_fcm_v2/lead/get_offline_data'] = "native_single_fcm_v2/Lead/get_offline_data";
$route['native_single_fcm_v2/lead/save_scan_upload'] = "native_single_fcm_v2/Lead/save_scan_upload";
$route['native_single_fcm_v2/lead/get_lead_details'] = "native_single_fcm_v2/Lead/get_lead_details";


$route['native_single_fcm_v2/lead/updateLead'] = "native_single_fcm_v2/Lead/updateLead";
$route['native_single_fcm_v2/lead/get_exhi_leads_offline'] = "native_single_fcm_v2/Lead/get_exhi_leads_offline";
$route['native_single_fcm_v2/note/download_pdf'] = "native_single_fcm_v2/Note/download_pdf";
$route['native_single_fcm_v2/attendee/getAttendeeForInviting'] = "native_single_fcm_v2/Attendee/getAttendeeForInviting";
$route['native_single_fcm_v2/attendee/GetInvitedAttendee'] = "native_single_fcm_v2/Attendee/GetInvitedAttendee";
$route['native_single_fcm_v2/attendee/InviteAttendee'] = "native_single_fcm_v2/Attendee/InviteAttendee";


$route['native_single_fcm_v2/badge_scanner/ScanBadge'] = "native_single_fcm_v2/Badge_scanner/ScanBadge";
$route['native_single_fcm_v2/attendee/getRequestMeetingTime'] = "native_single_fcm_v2/Attendee/getRequestMeetingTime";
$route['native_single_fcm_v2/app/get_default_lang'] = "native_single_fcm_v2/App/get_default_lang";
$route['native_single_fcm_v2/attendee/blockAttendee'] = "native_single_fcm_v2/Attendee/blockAttendee";



/* not in android but in ios doc */

$route['native_single_fcm_v2/login/registrationByEvent'] = "native_single_fcm_v2/Login/registrationByEvent";
$route['native_single_fcm_v2/login/getAllCountryList'] = "native_single_fcm_v2/Login/getAllCountryList";
$route['native_single_fcm_v2/speaker/speaker_view'] = "native_single_fcm_v2/Speaker/speaker_view";
$route['native_single_fcm_v2/exhibitor/exhibitor_view'] = "native_single_fcm_v2/Exhibitor/exhibitor_view";
$route['native_single_fcm_v2/exhibitor/exhibitor_view_unread_count_V2'] = "native_single_fcm_v2/Exhibitor/exhibitor_view_unread_count_V2";
$route['native_single_fcm_v2/social/getTwitetrFeeds'] = "native_single_fcm_v2/Social/getTwitetrFeeds";



$route['native_single_fcm_v2/presentation1/getPrsentationById'] = "native_single_fcm_v2/Presentation1/getPrsentationById";
$route['native_single_fcm_v2/exhibitor/msg_images_request'] = "native_single_fcm_v2/Exhibitor/msg_images_request";
$route['native_single_fcm_v2/speaker/make_comment'] = "native_single_fcm_v2/Speaker/make_comment";
$route['native_single_fcm_v2/speaker/sendMessage'] = "native_single_fcm_v2/Speaker/sendMessage";
$route['native_single_fcm_v2/speaker/msg_images_request'] = "native_single_fcm_v2/Speaker/msg_images_request";
$route['native_single_fcm_v2/speaker/delete_message'] = "native_single_fcm_v2/Speaker/delete_message";


$route['native_single_fcm_v2/attendee/msg_images_request'] = "native_single_fcm_v2/Attendee/msg_images_request";
$route['native_single_fcm_v2/attendee/delete_message'] = "native_single_fcm_v2/Attendee/delete_message";
$route['native_single_fcm_v2/message/private_msg_images_request'] = "native_single_fcm_v2/Message/private_msg_images_request";
$route['native_single_fcm_v2/settings/notification'] = "native_single_fcm_v2/Settings/notification";
$route['native_single_fcm_v2/login/logout'] = "native_single_fcm_v2/Login/logout";
$route['native_single_fcm_v2/attendee/respondRequest'] = "native_single_fcm_v2/Attendee/respondRequest";


$route['native_single_fcm_v2/attendee/attendee_contact_list'] = "native_single_fcm_v2/Attendee/attendee_contact_list";
$route['native_single_fcm_v2/message/delete_private_message'] = "native_single_fcm_v2/Message/delete_private_message";
$route['native_single_fcm_v2/gulfood/getUsefulInfo'] = "native_single_fcm_v2/Gulfood/getUsefulInfo";
$route['native_single_fcm_v2/favorites/addOrRemoveAttendee'] = "native_single_fcm_v2/Favorites/addOrRemoveAttendee";
$route['native_single_fcm_v2/checkin/addCheckin'] = "native_single_fcm_v2/Checkin/addCheckin";
$route['native_single_fcm_v2/exhibitor/exhibitor_list_new'] = "native_single_fcm_v2/Exhibitor/exhibitor_list_new";


$route['native_single_fcm_v2/exhibitor/getExhibitorListCategoryPcategories'] = "native_single_fcm_v2/Exhibitor/getExhibitorListCategoryPcategories";
$route['native_single_fcm_v2/exhibitor/getExhibitorListCategoryPcategoriestest'] = "native_single_fcm_v2/Exhibitor/getExhibitorListCategoryPcategoriestest";
$route['native_single_fcm_v2/exhibitor/getExhibitorListCategoryPcategoriesoffline_V2'] = "native_single_fcm_v2/Exhibitor/getExhibitorListCategoryPcategoriesoffline_V2";
$route['native_single_fcm_v2/attendee/getAllMeetingRequestWithDate'] = "native_single_fcm_v2/Attendee/getAllMeetingRequestWithDate";
$route['native_single_fcm_v2/attendee/getRequestMeetingLocation'] = "native_single_fcm_v2/Attendee/getRequestMeetingLocation";
$route['native_single_fcm_v2/app/getUpdatedByModules'] = "native_single_fcm_v2/App/getUpdatedByModules";



$route['native_single_fcm_v2/native_single_fcm_v2/getConcordiaAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getConcordiaAllPublicEvents";

$route['native_single_fcm_v2/native_single_fcm_v2/ConcordiaSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/ConcordiaSearchEvent";

$route['native_single_fcm_v2/rss/get_top_news_page'] = "native_single_fcm_v2/Rss/get_top_news_page";

$route['native_single_fcm_v2/native_single_fcm_v2/NCRealtorSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/NCRealtorSearchEvent";

$route['native_single_fcm_v2/attendee/hideMyIdentity'] = "native_single_fcm_v2/Attendee/hideMyIdentity";

$route['native_single_fcm_v2/agenda/getUserAgendaByTimeWithMeeting'] = "native_single_fcm_v2/Agenda/getUserAgendaByTimeWithMeeting";

$route['native_single_fcm_v2/attendee/saveRequestMeeting'] = "native_single_fcm_v2/Attendee/saveRequestMeeting";


$route['native_single_fcm_v2/matchmaking/getModules'] = "native_single_fcm_v2/Matchmaking/getModules";
$route['native_single_fcm_v2/matchmaking/getAttendees'] = "native_single_fcm_v2/Matchmaking/getAttendees";
$route['native_single_fcm_v2/matchmaking/getExhibitor'] = "native_single_fcm_v2/Matchmaking/getExhibitor";
$route['native_single_fcm_v2/matchmaking/getSpeaker'] = "native_single_fcm_v2/Matchmaking/getSpeaker";
$route['native_single_fcm_v2/matchmaking/getSponsor'] = "native_single_fcm_v2/Matchmaking/getSponsor";
$route['native_single_fcm_v2/matchmaking/markAsVisited'] = "native_single_fcm_v2/Matchmaking/markAsVisited";




$route['native_single_fcm_v2/login/check_v2'] = "native_single_fcm_v2/Login/check_v2";
$route['native_single_fcm_v2/login/forgot_password_v2'] = "native_single_fcm_v2/Login/forgot_password_v2";
$route['native_single_fcm_v2/login/registrationByEvent_v2'] = "native_single_fcm_v2/Login/registrationByEvent_v2";
$route['native_single_fcm_v2/attendee/requestMeeting'] = "native_single_fcm_v2/Attendee/requestMeeting";
$route['native_single_fcm_v2/profile/update_profile_with_additional_data_v2'] = "native_single_fcm_v2/Profile/update_profile_with_additional_data_v2";

$route['native_single_fcm_v2/native_single_fcm_v2/Certificatevalid'] = "native_single_fcm_v2/Native_single_fcm_v2/Certificatevalid";



/* ----------------------06-10-2018 for native_singel_fcm_v2.php -----------------------------------------*/


$route['native_single_fcm_v2/native_single_fcm_v2/checkIAAArkansasVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkIAAArkansasVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getIAAArkansasDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getIAAArkansasDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkParaxelVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkParaxelVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/searchParaxelEventBySecurekey'] = "native_single_fcm_v2/Native_single_fcm_v2/searchParaxelEventBySecurekey";
$route['native_single_fcm_v2/native_single_fcm_v2/checkShareFair2018VersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkShareFair2018VersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getShareFair2018DefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getShareFair2018DefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkClarionVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkClarionVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getLogisticsDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getLogisticsDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkGemConferenceVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkGemConferenceVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getGemConferenceDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getGemConferenceDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkOTRFilmFestVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkOTRFilmFestVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getOTRFilmFestDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getOTRFilmFestDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkBTSConferenceVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkBTSConferenceVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getBTSConferenceDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getBTSConferenceDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkIPExpoEuropeVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkIPExpoEuropeVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getIPExpoEuropeDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getIPExpoEuropeDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkCRForumLisbonVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkCRForumLisbonVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getCRForumLisbonDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getCRForumLisbonDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkPCAWaterproofingVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkPCAWaterproofingVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getPCAWaterproofingDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPCAWaterproofingDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkFranklinCoveyVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkFranklinCoveyVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getFranklinCoveyDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getFranklinCoveyDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkAlignHRVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkAlignHRVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getAlignHRDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getAlignHRDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkFinancialServicesVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkFinancialServicesVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getFinancialServicesDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getFinancialServicesDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkVitafoodsersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkVitafoodsersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getVitafoodsDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getVitafoodsDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkDCACersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkDCACersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getDCACDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getDCACDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkAASAVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkAASAVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getAASADefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getAASADefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkWarriorVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkWarriorVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getWarriorDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getWarriorDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/searchCoburnsEventBySecurekey'] = "native_single_fcm_v2/Native_single_fcm_v2/searchCoburnsEventBySecurekey";
$route['native_single_fcm_v2/native_single_fcm_v2/searchTricordEventBySecurekey'] = "native_single_fcm_v2/Native_single_fcm_v2/searchTricordEventBySecurekey";
$route['native_single_fcm_v2/native_single_fcm_v2/TricordSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/TricordSearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkTricordVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkTricordVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getTricordAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getTricordAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkPPMAVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkPPMAVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getPPMADefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPPMADefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkPurposeBuiltCommunitiesVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkPurposeBuiltCommunitiesVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getPurposeBuiltCommunitiesDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPurposeBuiltCommunitiesDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/NCRealtorSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/NCRealtorSearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/NCRealtorDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/NCRealtorDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkNCRealtorVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkNCRealtorVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getNCRealtorAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getNCRealtorAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkCompassionateFriendsVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkCompassionateFriendsVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getCompassionateFriendsDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getCompassionateFriendsDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/getConcordiaMasterEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getConcordiaMasterEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/searchSmartAirportEventBySecurekey'] = "native_single_fcm_v2/Native_single_fcm_v2/searchSmartAirportEventBySecurekey";
$route['native_single_fcm_v2/native_single_fcm_v2/ConcordiaSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/ConcordiaSearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkConcordiaVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkConcordiaVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getConcordiaAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getConcordiaAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkFoodMattersVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkFoodMattersVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getFoodMattersDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getFoodMattersDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/DynamiteSearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/DynamiteSearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkDynamiteVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkDynamiteVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getDynamiteAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getDynamiteAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkHowardhughesVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkHowardhughesVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getSmartAirportsDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getSmartAirportsDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkFIMEVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkFIMEVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getFIMEDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getFIMEDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/getUHGLatestDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getUHGLatestDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkPodcastMovementVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkPodcastMovementVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getPodcastMovementDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPodcastMovementDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkClosingTheGapVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkClosingTheGapVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getClosingTheGapDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getClosingTheGapDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/getRetailBulletinPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getRetailBulletinPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkRetailBulletinVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkRetailBulletinVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getRetailBulletinMasterEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getRetailBulletinMasterEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/SIAMsearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/SIAMsearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkSIAMVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkSIAMVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getSIAMAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getSIAMAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkBigDataTorontoVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkBigDataTorontoVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getBigDataTorontoDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getBigDataTorontoDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkGlobalRegTechVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkGlobalRegTechVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getGlobalRegTechDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getGlobalRegTechDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkMIT2018VersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkMIT2018VersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getMIT2018DefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getMIT2018DefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkiAmericas2018VersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkiAmericas2018VersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getiAmericas2018DefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getiAmericas2018DefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkEpsonVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkEpsonVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/searchEpsonEventBySecurekey'] = "native_single_fcm_v2/Native_single_fcm_v2/searchEpsonEventBySecurekey";
$route['native_single_fcm_v2/native_single_fcm_v2/getEpsonDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getEpsonDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkConnectechVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkConnectechVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getConnectechDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getConnectechDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/SourceMediasearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/SourceMediasearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkSourceMediaVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkSourceMediaVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getSourceMediaAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getSourceMediaAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkEuroviaVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkEuroviaVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getEuroviaDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getEuroviaDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkVenusVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkVenusVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getVenusDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getVenusDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkAICEVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkAICEVersionCode";

$route['native_single_fcm_v2/native_single_fcm_v2/getAICEDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getAICEDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkCityWeekVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkCityWeekVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getCityWeekDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getCityWeekDefaultEvent";

$route['native_single_fcm_v2/native_single_fcm_v2/MuveosearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/MuveosearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkMuveoVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkMuveoVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getMuveoAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getMuveoAllPublicEvents";

$route['native_single_fcm_v2/native_single_fcm_v2/checkIdahoRealtorsVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkIdahoRealtorsVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getIdahoRealtorsDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getIdahoRealtorsDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/UHGsearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/UHGsearchEvent";

$route['native_single_fcm_v2/native_single_fcm_v2/checkUHGVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkUHGVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getUHGAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getUHGAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkNACFBVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkNACFBVersionCode";

$route['native_single_fcm_v2/native_single_fcm_v2/getNACFBDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getNACFBDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/IOTsearchEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/IOTsearchEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkIOTVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkIOTVersionCode";

$route['native_single_fcm_v2/native_single_fcm_v2/getIOTAllPublicEvents'] = "native_single_fcm_v2/Native_single_fcm_v2/getIOTAllPublicEvents";
$route['native_single_fcm_v2/native_single_fcm_v2/checkBathandWestShowVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkBathandWestShowVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getBathandWestShowDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getBathandWestShowDefaultEvent";

$route['native_single_fcm/native_single_fcm_v2/HFTPSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/HFTPSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/checkHFTPVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkHFTPVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getHFTPAllPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getHFTPAllPublicEvents";

$route['native_single_fcm_v2/native_single_fcm_v2/checkNapecVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkNapecVersionCode";
$route['native_single_fcm_v2/native_single_fcm_v2/getNapecDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getNapecDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/checkPensionBridgeVersionCode'] = "native_single_fcm_v2/Native_single_fcm_v2/checkPensionBridgeVersionCode";

$route['native_single_fcm_v2/native_single_fcm_v2/getPrivateEquityExclusiveDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPrivateEquityExclusiveDefaultEvent";
$route['native_single_fcm_v2/native_single_fcm_v2/getPensionBridgeAnnualDefaultEvent'] = "native_single_fcm_v2/Native_single_fcm_v2/getPensionBridgeAnnualDefaultEvent";


$route['native_single_fcm_v2/attendee/attendee_list_v2'] = "native_single_fcm_v2/Attendee/attendee_list_v2";
$route['native_single_fcm_v2/attendee/getAttendeeFilters'] = "native_single_fcm_v2/Attendee/getAttendeeFilters";


/* ********************  */

$route['native_single_fcm/native_single_fcm/checkBondsAndLoansVersionCode'] = "native_single_fcm/Native_single_fcm/checkBondsAndLoansVersionCode";

$route['native_single_fcm/native_single_fcm/getApexPublicEvents'] = "native_single_fcm/Native_single_fcm/getApexPublicEvents";
$route['native_single_fcm/native_single_fcm/ApexSearchEvent'] = "native_single_fcm/Native_single_fcm/ApexSearchEvent";
$route['native_single_fcm/native_single_fcm/getNHSDefaultEvent'] = "native_single_fcm/Native_single_fcm/getNHSDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkNHSVersionCode'] = "native_single_fcm/Native_single_fcm/checkNHSVersionCode";
$route['native_single_fcm/native_single_fcm/getSIALMEDefaultEvent'] = "native_single_fcm/Native_single_fcm/getSIALMEDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkSIALMEVersionCode'] = "native_single_fcm/Native_single_fcm/checkSIALMEVersionCode";
$route['native_single_fcm/native_single_fcm/getCABSATDefaultEvent'] = "native_single_fcm/Native_single_fcm/getCABSATDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkCABSATVersionCode'] = "native_single_fcm/Native_single_fcm/checkCABSATVersionCode";
$route['native_single_fcm/native_single_fcm/getComicConPublicEvents'] = "native_single_fcm/Native_single_fcm/getComicConPublicEvents";
$route['native_single_fcm/native_single_fcm/checkComicConVersionCode'] = "native_single_fcm/Native_single_fcm/checkComicConVersionCode";
$route['native_single_fcm/native_single_fcm/ComicconSearchEvent'] = "native_single_fcm/Native_single_fcm/ComicconSearchEvent";
$route['native_single_fcm/native_single_fcm/getONTDefaultEvent'] = "native_single_fcm/Native_single_fcm/getONTDefaultEvent";
$route['native_single_fcm/native_single_fcm/getWPBDefaultEvent'] = "native_single_fcm/Native_single_fcm/getWPBDefaultEvent";
$route['native_single_fcm/native_single_fcm/getGamesforumDefaultEvent'] = "native_single_fcm/Native_single_fcm/getGamesforumDefaultEvent";
$route['native_single_fcm/native_single_fcm/KnectSearchEvent'] = "native_single_fcm/Native_single_fcm/KnectSearchEvent";
$route['native_single_fcm/native_single_fcm/getArabHelthDefaultEvent'] = "native_single_fcm/Native_single_fcm/getArabHelthDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkArabHelthVersionCode'] = "native_single_fcm/Native_single_fcm/checkArabHelthVersionCode";
$route['native_single_fcm/native_single_fcm/getFHADefaultEvent'] = "native_single_fcm/Native_single_fcm/getFHADefaultEvent";
$route['native_single_fcm/native_single_fcm/checkFHAVersionCode'] = "native_single_fcm/Native_single_fcm/checkFHAVersionCode";
$route['native_single_fcm/native_single_fcm/getBondsAndLoansPublicEvents'] = "native_single_fcm/Native_single_fcm/getBondsAndLoansPublicEvents";
$route['native_single_fcm/native_single_fcm/getConcreteExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm/getConcreteExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkConcreteExpoVersionCode'] = "native_single_fcm/Native_single_fcm/checkConcreteExpoVersionCode";
$route['native_single_fcm/native_single_fcm/getConnect2bDefaultEvent'] = "native_single_fcm/Native_single_fcm/getConnect2bDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkConnect2bVersionCode'] = "native_single_fcm/Native_single_fcm/checkConnect2bVersionCode";
$route['native_single_fcm/native_single_fcm/getMjBakerDefaultEvent'] = "native_single_fcm/Native_single_fcm/getMjBakerDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkMjBakerVersionCode'] = "native_single_fcm/Native_single_fcm/checkMjBakerVersionCode";
$route['native_single_fcm/native_single_fcm/getAvangridDefaultEvent'] = "native_single_fcm/Native_single_fcm/getAvangridDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkAvangridVersionCode'] = "native_single_fcm/Native_single_fcm/checkAvangridVersionCode";
$route['native_single_fcm/native_single_fcm/getMedlabDefaultEvent'] = "native_single_fcm/Native_single_fcm/getMedlabDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkMedlabVersionCode'] = "native_single_fcm/Native_single_fcm/checkMedlabVersionCode";
$route['native_single_fcm/native_single_fcm/getCoburnsPublicEvents'] = "native_single_fcm/Native_single_fcm/getCoburnsPublicEvents";
$route['native_single_fcm/native_single_fcm/CoburnsSearchEvent'] = "native_single_fcm/Native_single_fcm/CoburnsSearchEvent";
$route['native_single_fcm/native_single_fcm/checkCoburnsVersionCode'] = "native_single_fcm/Native_single_fcm/checkCoburnsVersionCode";
$route['native_single_fcm/native_single_fcm/getGreyDefaultEvent'] = "native_single_fcm/Native_single_fcm/getGreyDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkGreyVersionCode'] = "native_single_fcm/Native_single_fcm/checkGreyVersionCode";
$route['native_single_fcm/native_single_fcm/getLTLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm/getLTLondonDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkLTLondonVersionCode'] = "native_single_fcm/Native_single_fcm/checkLTLondonVersionCode";
$route['native_single_fcm/native_single_fcm/getLTFranceDefaultEvent'] = "native_single_fcm/Native_single_fcm/getLTFranceDefaultEvent";
$route['native_single_fcm/native_single_fcm/getDerivConeDefaultEvent'] = "native_single_fcm/Native_single_fcm/getDerivConeDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkDerivConVersionCode'] = "native_single_fcm/Native_single_fcm/checkDerivConVersionCode";
$route['native_single_fcm/native_single_fcm/getFixIncome2018DefaultEvent'] = "native_single_fcm/Native_single_fcm/getFixIncome2018DefaultEvent";
$route['native_single_fcm/native_single_fcm/checkFixIncome2018VersionCode'] = "native_single_fcm/Native_single_fcm/checkFixIncome2018VersionCode";
$route['native_single_fcm/native_single_fcm/getIMEAPublicEvents'] = "native_single_fcm/Native_single_fcm/getIMEAPublicEvents";


$route['native_single_fcm/native_single_fcm/IMEASearchEvent'] = "native_single_fcm/Native_single_fcm/IMEASearchEvent";
$route['native_single_fcm/native_single_fcm/checkIMEAVersionCode'] = "native_single_fcm/Native_single_fcm/checkIMEAVersionCode";
$route['native_single_fcm/native_single_fcm/getUCASPublicEvents'] = "native_single_fcm/Native_single_fcm/getUCASPublicEvents";
$route['native_single_fcm/native_single_fcm/UCASSearchEvent'] = "native_single_fcm/Native_single_fcm/UCASSearchEvent";
$route['native_single_fcm/native_single_fcm/checkUCASVersionCode'] = "native_single_fcm/Native_single_fcm/checkUCASVersionCode";
$route['native_single_fcm/native_single_fcm/checkAICDVersionCode'] = "native_single_fcm/Native_single_fcm/checkAICDVersionCode";
$route['native_single_fcm/native_single_fcm/getErsteTalentDefaultEvent'] = "native_single_fcm/Native_single_fcm/getErsteTalentDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkErsteTalentVersionCode'] = "native_single_fcm/Native_single_fcm/checkErsteTalentVersionCode";
$route['native_single_fcm/native_single_fcm/getEuromedicomAMWCDefaultEvent'] = "native_single_fcm/Native_single_fcm/getEuromedicomAMWCDefaultEvent";
$route['native_single_fcm/native_single_fcm/getEuromedicomFACEDefaultEvent'] = "native_single_fcm/Native_single_fcm/getEuromedicomFACEDefaultEvent";
$route['native_single_fcm/native_single_fcm/getEuromedicomAMECLiveVISAGEDefaultEvent'] = "native_single_fcm/Native_single_fcm/getEuromedicomAMECLiveVISAGEDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkEuromedicomVersionCode'] = "native_single_fcm/Native_single_fcm/checkEuromedicomVersionCode";
$route['native_single_fcm/native_single_fcm/getArlarDefaultEvent'] = "native_single_fcm/Native_single_fcm/getArlarDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkArlarVersionCode'] = "native_single_fcm/Native_single_fcm/checkArlarVersionCode";
$route['native_single_fcm/native_single_fcm/getIPExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm/getIPExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkIPExpoVersionCode'] = "native_single_fcm/Native_single_fcm/checkIPExpoVersionCode";
$route['native_single_fcm/native_single_fcm/getUCExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm/getUCExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkUCExpoVersionCode'] = "native_single_fcm/Native_single_fcm/checkUCExpoVersionCode";
$route['native_single_fcm/native_single_fcm/getCityScapeAbuDhabiDefaultEvent'] = "native_single_fcm/Native_single_fcm/getCityScapeAbuDhabiDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkCityScapeAbuDhabiVersionCode'] = "native_single_fcm/Native_single_fcm/checkCityScapeAbuDhabiVersionCode";
$route['native_single_fcm/native_single_fcm/getAEI2018DefaultEvent'] = "native_single_fcm/Native_single_fcm/getAEI2018DefaultEvent";
$route['native_single_fcm/native_single_fcm/checkAEI2018VersionCode'] = "native_single_fcm/Native_single_fcm/checkAEI2018VersionCode";
$route['native_single_fcm/native_single_fcm/getBBGADefaultEvent'] = "native_single_fcm/Native_single_fcm/getBBGADefaultEvent";
$route['native_single_fcm/native_single_fcm/getBBGADefaultEvent'] = "native_single_fcm/Native_single_fcm/getBBGADefaultEvent";
$route['native_single_fcm/native_single_fcm/checkBBGAVersionCode'] = "native_single_fcm/Native_single_fcm/checkBBGAVersionCode";
$route['native_single_fcm/native_single_fcm/getBathAndWestDefaultEvent'] = "native_single_fcm/Native_single_fcm/getBathAndWestDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkBathAndWestVersionCode'] = "native_single_fcm/Native_single_fcm/checkBathAndWestVersionCode";

$route['native_single_fcm/native_single_fcm/getTSAMLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm/getTSAMLondonDefaultEvent";
$route['native_single_fcm/native_single_fcm/getTSAMTorontoDefaultEvent'] = "native_single_fcm/Native_single_fcm/getTSAMTorontoDefaultEvent";
$route['native_single_fcm/native_single_fcm/getTSAMNewYorkDefaultEvent'] = "native_single_fcm/Native_single_fcm/getTSAMNewYorkDefaultEvent";
$route['native_single_fcm/native_single_fcm/getTSAMHongKongDefaultEvent'] = "native_single_fcm/Native_single_fcm/getTSAMHongKongDefaultEvent";
$route['native_single_fcm/native_single_fcm/getTSAMBostonDefaultEvent'] = "native_single_fcm/Native_single_fcm/getTSAMBostonDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkTSAMVersionCode'] = "native_single_fcm/Native_single_fcm/checkTSAMVersionCode";

$route['native_single_fcm/native_single_fcm/getAhicDefaultEvent'] = "native_single_fcm/Native_single_fcm/getAhicDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkAhicVersionCode'] = "native_single_fcm/Native_single_fcm/checkAhicVersionCode";

$route['native_single_fcm/native_single_fcm/getAviationMediaDefaultEvent'] = "native_single_fcm/Native_single_fcm/getAviationMediaDefaultEvent";
$route['native_single_fcm/native_single_fcm/checkAviationMediaVersionCode'] = "native_single_fcm/Native_single_fcm/checkAviationMediaVersionCode";




$route['native_single_fcm/native_single_fcm_v2/checkBondsAndLoansVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBondsAndLoansVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getApexPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getApexPublicEvents";
$route['native_single_fcm/native_single_fcm_v2/ApexSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ApexSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/getNHSDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getNHSDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkNHSVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkNHSVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getSIALMEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getSIALMEDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkSIALMEVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkSIALMEVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getCABSATDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCABSATDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkCABSATVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCABSATVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getComicConPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getComicConPublicEvents";
$route['native_single_fcm/native_single_fcm_v2/checkComicConVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkComicConVersionCode";
$route['native_single_fcm/native_single_fcm_v2/ComicconSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/ComicconSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/getONTDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getONTDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getWPBDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getWPBDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getGamesforumDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGamesforumDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/KnectSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/KnectSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/getArabHelthDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getArabHelthDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkArabHelthVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkArabHelthVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getFHADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFHADefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkFHAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFHAVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getBondsAndLoansPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getBondsAndLoansPublicEvents";
$route['native_single_fcm/native_single_fcm_v2/getConcreteExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getConcreteExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkConcreteExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConcreteExpoVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getConnect2bDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getConnect2bDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkConnect2bVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkConnect2bVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getMjBakerDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getMjBakerDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkMjBakerVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMjBakerVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getAvangridDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAvangridDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkAvangridVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAvangridVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getMedlabDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getMedlabDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkMedlabVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkMedlabVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getCoburnsPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getCoburnsPublicEvents";
$route['native_single_fcm/native_single_fcm_v2/CoburnsSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/CoburnsSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/checkCoburnsVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCoburnsVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getGreyDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getGreyDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkGreyVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkGreyVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getLTLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getLTLondonDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkLTLondonVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkLTLondonVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getLTFranceDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getLTFranceDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getDerivConeDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getDerivConeDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkDerivConVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkDerivConVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getFixIncome2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getFixIncome2018DefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkFixIncome2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkFixIncome2018VersionCode";
$route['native_single_fcm/native_single_fcm_v2/getIMEAPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getIMEAPublicEvents";


$route['native_single_fcm/native_single_fcm_v2/IMEASearchEvent'] = "native_single_fcm/Native_single_fcm_v2/IMEASearchEvent";
$route['native_single_fcm/native_single_fcm_v2/checkIMEAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIMEAVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getUCASPublicEvents'] = "native_single_fcm/Native_single_fcm_v2/getUCASPublicEvents";
$route['native_single_fcm/native_single_fcm_v2/UCASSearchEvent'] = "native_single_fcm/Native_single_fcm_v2/UCASSearchEvent";
$route['native_single_fcm/native_single_fcm_v2/checkUCASVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkUCASVersionCode";
$route['native_single_fcm/native_single_fcm_v2/checkAICDVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAICDVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getErsteTalentDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getErsteTalentDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkErsteTalentVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkErsteTalentVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getEuromedicomAMWCDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomAMWCDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getEuromedicomFACEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomFACEDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getEuromedicomAMECLiveVISAGEDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getEuromedicomAMECLiveVISAGEDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkEuromedicomVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkEuromedicomVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getArlarDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getArlarDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkArlarVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkArlarVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getIPExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getIPExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkIPExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkIPExpoVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getUCExpoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getUCExpoDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkUCExpoVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkUCExpoVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getCityScapeAbuDhabiDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getCityScapeAbuDhabiDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkCityScapeAbuDhabiVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkCityScapeAbuDhabiVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getAEI2018DefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAEI2018DefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkAEI2018VersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAEI2018VersionCode";
$route['native_single_fcm/native_single_fcm_v2/getBBGADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBBGADefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getBBGADefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBBGADefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkBBGAVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBBGAVersionCode";
$route['native_single_fcm/native_single_fcm_v2/getBathAndWestDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getBathAndWestDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkBathAndWestVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkBathAndWestVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getTSAMLondonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMLondonDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getTSAMTorontoDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMTorontoDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getTSAMNewYorkDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMNewYorkDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getTSAMHongKongDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMHongKongDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/getTSAMBostonDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getTSAMBostonDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkTSAMVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkTSAMVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAhicDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAhicDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkAhicVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAhicVersionCode";

$route['native_single_fcm/native_single_fcm_v2/getAviationMediaDefaultEvent'] = "native_single_fcm/Native_single_fcm_v2/getAviationMediaDefaultEvent";
$route['native_single_fcm/native_single_fcm_v2/checkAviationMediaVersionCode'] = "native_single_fcm/Native_single_fcm_v2/checkAviationMediaVersionCode";



// BY TARUN


$route['native_single_fcm/rss/update_rss'] = "native_single_fcm/Rss/update_rss";
$route['native_single_fcm/rss/get_top_news'] = "native_single_fcm/Rss/get_top_news";
$route['native_single_fcm/rss/get_my_news'] = "native_single_fcm/Rss/get_my_news";
$route['native_single_fcm/rss/get_tags'] = "native_single_fcm/Rss/get_tags";
$route['native_single_fcm/rss/get_news_details'] = "native_single_fcm/Rss/get_news_details";
$route['native_single_fcm/rss/like_news'] = "native_single_fcm/Rss/like_news";
$route['native_single_fcm/rss/make_comment'] = "native_single_fcm/Rss/make_comment";
$route['native_single_fcm/rss/saveCommentImages'] = "native_single_fcm/Rss/saveCommentImages";
$route['native_single_fcm/rss/update_rss_global'] = "native_single_fcm/Rss/update_rss_global";
$route['native_single_fcm/rss/get_top_news_global'] = "native_single_fcm/Rss/get_top_news_global";
$route['native_single_fcm/rss/get_my_news_global'] = "native_single_fcm/Rss/get_my_news_global";
$route['native_single_fcm/rss/get_tags_global'] = "native_single_fcm/Rss/get_tags_global";
$route['native_single_fcm/rss/get_news_details_global'] = "native_single_fcm/Rss/get_news_details_global";
$route['native_single_fcm/rss/like_news_global'] = "native_single_fcm/Rss/like_news_global";
$route['native_single_fcm/rss/make_comment_global'] = "native_single_fcm/Rss/make_comment_global";
$route['native_single_fcm/rss/saveCommentImages_global'] = "native_single_fcm/Rss/saveCommentImages_global";


$route['native_single_fcm_v2/rss/update_rss'] = "native_single_fcm_v2/Rss/update_rss";
$route['native_single_fcm_v2/rss/get_top_news'] = "native_single_fcm_v2/Rss/get_top_news";
$route['native_single_fcm_v2/rss/get_my_news'] = "native_single_fcm_v2/Rss/get_my_news";
$route['native_single_fcm_v2/rss/get_tags'] = "native_single_fcm_v2/Rss/get_tags";
$route['native_single_fcm_v2/rss/get_news_details'] = "native_single_fcm_v2/Rss/get_news_details";
$route['native_single_fcm_v2/rss/like_news'] = "native_single_fcm_v2/Rss/like_news";
$route['native_single_fcm_v2/rss/make_comment'] = "native_single_fcm_v2/Rss/make_comment";
$route['native_single_fcm_v2/rss/saveCommentImages'] = "native_single_fcm_v2/Rss/saveCommentImages";
$route['native_single_fcm_v2/rss/update_rss_global'] = "native_single_fcm_v2/Rss/update_rss_global";
$route['native_single_fcm_v2/rss/get_top_news_global'] = "native_single_fcm_v2/Rss/get_top_news_global";
$route['native_single_fcm_v2/rss/get_my_news_global'] = "native_single_fcm_v2/Rss/get_my_news_global";
$route['native_single_fcm_v2/rss/get_tags_global'] = "native_single_fcm_v2/Rss/get_tags_global";
$route['native_single_fcm_v2/rss/get_news_details_global'] = "native_single_fcm_v2/Rss/get_news_details_global";
$route['native_single_fcm_v2/rss/like_news_global'] = "native_single_fcm_v2/Rss/like_news_global";
$route['native_single_fcm_v2/rss/make_comment_global'] = "native_single_fcm_v2/Rss/make_comment_global";
$route['native_single_fcm_v2/rss/saveCommentImages_global'] = "native_single_fcm_v2/Rss/saveCommentImages_global";


$route['native_single_fcm/app/getModulesUpdatedLogs'] = "native_single_fcm/App/getModulesUpdatedLogs";
$route['native_single_fcm_v2/app/getModulesUpdatedLogs'] = "native_single_fcm_v2/App/getModulesUpdatedLogs";

$route['native_single_fcm/agenda/getTimeZoneDate'] = "native_single_fcm/Agenda/getTimeZoneDate";
$route['native_single_fcm_v2/agenda/getTimeZoneDate'] = "native_single_fcm_v2/Agenda/getTimeZoneDate";


$route['native_single_fcm/agenda/getUserAgendaByTimeNew'] = "native_single_fcm/Agenda/getUserAgendaByTimeNew";
$route['native_single_fcm_v2/agenda/getUserAgendaByTimeNew'] = "native_single_fcm_v2/Agenda/getUserAgendaByTimeNew";
$route['native_single_fcm/agenda/getPendingUserAgendaByTimeNew'] = "native_single_fcm/Agenda/getPendingUserAgendaByTimeNew";
$route['native_single_fcm_v2/agenda/getPendingUserAgendaByTimeNew'] = "native_single_fcm_v2/Agenda/getPendingUserAgendaByTimeNew";


$route['native_single_fcm/speaker/speaker_list'] = "native_single_fcm/Speaker/speaker_list";
$route['native_single_fcm_v2/speaker/speaker_list'] = "native_single_fcm_v2/Speaker/speaker_list";
$route['native_single_fcm/speaker/getImageList'] = "native_single_fcm/Speaker/getImageList";
$route['native_single_fcm_v2/speaker/getImageList'] = "native_single_fcm_v2/Speaker/getImageList";
$route['native_single_fcm/speaker/speaker_view_new'] = "native_single_fcm/Speaker/speaker_view_new";
$route['native_single_fcm_v2/speaker/speaker_view_new'] = "native_single_fcm_v2/Speaker/speaker_view_new";


$route['native_single_fcm/survey/get_survey'] = "native_single_fcm/Survey/get_survey";
$route['native_single_fcm_v2/survey/get_survey'] = "native_single_fcm_v2/Survey/get_survey";
$route['native_single_fcm/survey/add_user_game_point'] = "native_single_fcm/Survey/add_user_game_point";
$route['native_single_fcm_v2/survey/add_user_game_point'] = "native_single_fcm_v2/Survey/add_user_game_point";
$route['native_single_fcm/survey/check_survey_status'] = "native_single_fcm/Survey/check_survey_status";
$route['native_single_fcm_v2/survey/check_survey_status'] = "native_single_fcm_v2/Survey/check_survey_status";



$route['native_single_fcm/qr_code/getQrCodeOfLoggedInAttendee'] = "native_single_fcm/Qr_code/getQrCodeOfLoggedInAttendee";
$route['native_single_fcm_v2/qr_code/getQrCodeOfLoggedInAttendee'] = "native_single_fcm_v2/Qr_code/getQrCodeOfLoggedInAttendee";
$route['native_single_fcm/qr_code/getQrCodeOfLoggedInAttendeeNew'] = "native_single_fcm/Qr_code/getQrCodeOfLoggedInAttendeeNew";
$route['native_single_fcm_v2/qr_code/getQrCodeOfLoggedInAttendeeNew'] = "native_single_fcm_v2/Qr_code/getQrCodeOfLoggedInAttendeeNew";
$route['native_single_fcm/qr_code/checkAgendaByUserId'] = "native_single_fcm/Qr_code/checkAgendaByUserId";
$route['native_single_fcm_v2/qr_code/checkAgendaByUserId'] = "native_single_fcm_v2/Qr_code/checkAgendaByUserId";



$route['native_single_fcm/qa/hideMessage'] = "native_single_fcm/Qa/hideMessage";
$route['native_single_fcm_v2/qa/hideMessage'] = "native_single_fcm_v2/Qa/hideMessage";
$route['native_single_fcm/qa/getHiddenQuestion'] = "native_single_fcm/Qa/getHiddenQuestion";
$route['native_single_fcm_v2/qa/getHiddenQuestion'] = "native_single_fcm_v2/Qa/getHiddenQuestion";
$route['native_single_fcm/qa/ModeratorVoteMessage'] = "native_single_fcm/Qa/ModeratorVoteMessage";
$route['native_single_fcm_v2/qa/ModeratorVoteMessage'] = "native_single_fcm_v2/Qa/ModeratorVoteMessage";


$route['native_single_fcm/activity/getFeeds'] = "native_single_fcm/Activity/getFeeds";
$route['native_single_fcm_v2/activity/getFeeds'] = "native_single_fcm_v2/Activity/getFeeds";


$route['native_single_fcm/activity/getFeedDetails'] = "native_single_fcm/Activity/getFeedDetails";
$route['native_single_fcm_v2/activity/getFeedDetails'] = "native_single_fcm_v2/Activity/getFeedDetails";

$route['native_single_fcm/activity/postUpdate'] = "native_single_fcm/Activity/postUpdate";
$route['native_single_fcm_v2/activity/postUpdate'] = "native_single_fcm_v2/Activity/postUpdate";

$route['native_single_fcm/activity/saveActivityImages'] = "native_single_fcm/Activity/saveActivityImages";
$route['native_single_fcm_v2/activity/saveActivityImages'] = "native_single_fcm_v2/Activity/saveActivityImages";

$route['native_single_fcm/activity/getImageList'] = "native_single_fcm/Activity/getImageList";
$route['native_single_fcm_v2/activity/getImageList'] = "native_single_fcm_v2/Activity/getImageList";

$route['native_single_fcm/activity/feedLike'] = "native_single_fcm/Activity/feedLike";
$route['native_single_fcm_v2/activity/feedLike'] = "native_single_fcm_v2/Activity/feedLike";

$route['native_single_fcm/activity/feedComment'] = "native_single_fcm/Activity/feedComment";
$route['native_single_fcm_v2/activity/feedComment'] = "native_single_fcm_v2/Activity/feedComment";


$route['native_single_fcm/activity/saveCommentImages'] = "native_single_fcm/Activity/saveCommentImages";
$route['native_single_fcm_v2/activity/saveCommentImages'] = "native_single_fcm_v2/Activity/saveCommentImages";

$route['native_single_fcm/activity/getCommentImageList'] = "native_single_fcm/Activity/getCommentImageList";
$route['native_single_fcm_v2/activity/getCommentImageList'] = "native_single_fcm_v2/Activity/getCommentImageList";

$route['native_single_fcm/activity/deleteFeedComment'] = "native_single_fcm/Activity/deleteFeedComment";
$route['native_single_fcm_v2/activity/deleteFeedComment'] = "native_single_fcm_v2/Activity/deleteFeedComment";

$route['native_single_fcm/activity/get_internal_data'] = "native_single_fcm/Activity/get_internal_data";
$route['native_single_fcm_v2/activity/get_internal_data'] = "native_single_fcm_v2/Activity/get_internal_data";

$route['native_single_fcm/activity/get_alert_data'] = "native_single_fcm/Activity/get_alert_data";
$route['native_single_fcm_v2/activity/get_alert_data'] = "native_single_fcm_v2/Activity/get_alert_data";



$route['native_single_fcm/activity/get_twitter_data'] = "native_single_fcm/Activity/get_twitter_data";
$route['native_single_fcm_v2/activity/get_twitter_data'] = "native_single_fcm_v2/Activity/get_twitter_data";

$route['native_single_fcm/activity/get_facebook_data'] = "native_single_fcm/Activity/get_facebook_data";
$route['native_single_fcm_v2/activity/get_facebook_data'] = "native_single_fcm_v2/Activity/get_facebook_data";

$route['native_single_fcm/activity/get_instagram_data'] = "native_single_fcm/Activity/get_instagram_data";
$route['native_single_fcm_v2/activity/get_instagram_data'] = "native_single_fcm_v2/Activity/get_instagram_data";

$route['native_single_fcm/activity/get_all'] = "native_single_fcm/Activity/get_all";
$route['native_single_fcm_v2/activity/get_all'] = "native_single_fcm_v2/Activity/get_all";

$route['native_single_fcm/activity/get_internal'] = "native_single_fcm/Activity/get_internal";
$route['native_single_fcm_v2/activity/get_internal'] = "native_single_fcm_v2/Activity/get_internal";

$route['native_single_fcm/activity/get_alert'] = "native_single_fcm/Activity/get_alert";
$route['native_single_fcm_v2/activity/get_alert'] = "native_single_fcm_v2/Activity/get_alert";

$route['native_single_fcm/activity/get_social'] = "native_single_fcm/Activity/get_social";
$route['native_single_fcm_v2/activity/get_social'] = "native_single_fcm_v2/Activity/get_social";

$route['native_single_fcm/activity/get_timeago'] = "native_single_fcm/Activity/get_timeago";
$route['native_single_fcm_v2/activity/get_timeago'] = "native_single_fcm_v2/Activity/get_timeago";



$route['native_single_fcm/activity/feedLikeNew'] = "native_single_fcm/Activity/feedLikeNew";
$route['native_single_fcm_v2/activity/feedLikeNew'] = "native_single_fcm_v2/Activity/feedLikeNew";

$route['native_single_fcm/activity/feedCommentNew'] = "native_single_fcm/Activity/feedCommentNew";
$route['native_single_fcm_v2/activity/feedCommentNew'] = "native_single_fcm_v2/Activity/feedCommentNew";



$route['native_single_fcm/activity/add_photo_filter_image'] = "native_single_fcm/Activity/add_photo_filter_image";
$route['native_single_fcm_v2/activity/add_photo_filter_image'] = "native_single_fcm_v2/Activity/add_photo_filter_image";

$route['native_single_fcm/activity/get_photofilter_url'] = "native_single_fcm/Activity/get_photofilter_url";
$route['native_single_fcm_v2/activity/get_photofilter_url'] = "native_single_fcm_v2/Activity/get_photofilter_url";

$route['native_single_fcm/activity/get_internal_data_UHG'] = "native_single_fcm/Activity/get_internal_data_UHG";
$route['native_single_fcm_v2/activity/get_internal_data_UHG'] = "native_single_fcm_v2/Activity/get_internal_data_UHG";

$route['native_single_fcm/activity/compress_image'] = "native_single_fcm/Activity/compress_image";
$route['native_single_fcm_v2/activity/compress_image'] = "native_single_fcm_v2/Activity/compress_image";

$route['native_single_fcm/activity/get_internal_data_UHG_test'] = "native_single_fcm/Activity/get_internal_data_UHG_test";
$route['native_single_fcm_v2/activity/get_internal_data_UHG_test'] = "native_single_fcm_v2/Activity/get_internal_data_UHG_test";

$route['native_single_fcm/activity/deletePost'] = "native_single_fcm/Activity/deletePost";
$route['native_single_fcm_v2/activity/deletePost'] = "native_single_fcm_v2/Activity/deletePost";

$route['native_single_fcm/activity/getLikes'] = "native_single_fcm/Activity/getLikes";
$route['native_single_fcm_v2/activity/getLikes'] = "native_single_fcm_v2/Activity/getLikes";

$route['native_single_fcm/presentation1/viewChartResult'] = "native_single_fcm/Presentation1/viewChartResult";
$route['native_single_fcm_v2/presentation1/viewChartResult'] = "native_single_fcm/Presentation1/viewChartResult";
$route['native_single_fcm/presentation1/sendNotification'] = "native_single_fcm/Presentation1/sendNotification";
$route['native_single_fcm_v2/presentation1/sendNotification'] = "native_single_fcm/Presentation1/sendNotification";
$route['native_single_fcm/presentation1/startSocketServer'] = "native_single_fcm/Presentation1/startSocketServer";
$route['native_single_fcm_v2/presentation1/startSocketServer'] = "native_single_fcm/Presentation1/startSocketServer";


$route['native_single_fcm/attendee/attendee_list_v2'] = "native_single_fcm/Attendee/attendee_list_v2";
$route['native_single_fcm/attendee/getAttendeeFilters'] = "native_single_fcm/Attendee/getAttendeeFilters";
$route['native_single_fcm/attendee/getRequestMeetingDateTime'] = "native_single_fcm/Attendee/getRequestMeetingDateTime";
$route['native_single_fcm/attendee/getRequestMeetingDate'] = "native_single_fcm/Attendee/getRequestMeetingDate";
$route['native_single_fcm/attendee/attendee_list_native_load'] = "native_single_fcm/Attendee/attendee_list_native_load";
$route['native_single_fcm_v2/attendee/attendee_list_native_load'] = "native_single_fcm_v2/Attendee/attendee_list_native_load";
$route['native_single_fcm/attendee/get_ipexpo_meeting_time'] = "native_single_fcm/Attendee/get_ipexpo_meeting_time";
$route['native_single_fcm_v2/attendee/get_ipexpo_meeting_time'] = "native_single_fcm_v2/Attendee/get_ipexpo_meeting_time";
$route['native_single_fcm/attendee/get_solar_plaza_meeting_time'] = "native_single_fcm/Attendee/get_solar_plaza_meeting_time";
$route['native_single_fcm_v2/attendee/get_solar_plaza_meeting_time'] = "native_single_fcm_v2/Attendee/get_solar_plaza_meeting_time";
$route['native_single_fcm/attendee/getAllMeetingRequestNew'] = "native_single_fcm/Attendee/getAllMeetingRequestNew";
$route['native_single_fcm_v2/attendee/getAllMeetingRequestNew'] = "native_single_fcm_v2/Attendee/getAllMeetingRequestNew";
$route['native_single_fcm/attendee/add_user_game_point'] = "native_single_fcm/Attendee/add_user_game_point";
$route['native_single_fcm_v2/attendee/add_user_game_point'] = "native_single_fcm_v2/Attendee/add_user_game_point";
$route['native_single_fcm/attendee/bookSuggestedTime'] = "native_single_fcm/Attendee/bookSuggestedTime";
$route['native_single_fcm_v2/attendee/bookSuggestedTime'] = "native_single_fcm_v2/Attendee/bookSuggestedTime";
$route['native_single_fcm/attendee/getSuggestedTimings'] = "native_single_fcm/Attendee/getSuggestedTimings";
$route['native_single_fcm_v2/attendee/getSuggestedTimings'] = "native_single_fcm_v2/Attendee/getSuggestedTimings";
$route['native_single_fcm/attendee/getAllMeetingRequest'] = "native_single_fcm/Attendee/getAllMeetingRequest";
$route['native_single_fcm_v2/attendee/getAllMeetingRequest'] = "native_single_fcm_v2/Attendee/getAllMeetingRequest";



$route['native_single_fcm/app/index'] = "native_single_fcm/App/index";
$route['native_single_fcm_v2/app/index'] = "native_single_fcm_v2/App/index";



$route['native_single_fcm/app/event_id_advance_design'] = "native_single_fcm/App/event_id_advance_design";
$route['native_single_fcm_v2/app/event_id_advance_design'] = "native_single_fcm_v2/App/event_id_advance_design";


$route['native_single_fcm/app/get_default_lang'] = "native_single_fcm/App/get_default_lang";
$route['native_single_fcm_v2/app/get_default_lang'] = "native_single_fcm_v2/App/get_default_lang";


$route['native_single_fcm/app/compress_image'] = "native_single_fcm/App/compress_image";
$route['native_single_fcm_v2/app/compress_image'] = "native_single_fcm_v2/App/compress_image";


$route['native_single_fcm/app/get_event'] = "native_single_fcm/App/get_event";
$route['native_single_fcm_v2/app/get_event'] = "native_single_fcm_v2/App/get_event";


$route['native_single_fcm/app/getModulesUpdatedLogs'] = "native_single_fcm/App/getModulesUpdatedLogs";
$route['native_single_fcm_v2/app/getModulesUpdatedLogs'] = "native_single_fcm_v2/App/getModulesUpdatedLogs";


$route['native_single_fcm/app/getFavouritedExhibitors'] = "native_single_fcm/App/getFavouritedExhibitors";
$route['native_single_fcm_v2/app/getFavouritedExhibitors'] = "native_single_fcm_v2/App/getFavouritedExhibitors";


$route['native_single_fcm/app/test_arab_token'] = "native_single_fcm/App/test_arab_token";
$route['native_single_fcm_v2/app/test_arab_token'] = "native_single_fcm_v2/App/test_arab_token";


$route['native_single_fcm/app/get_login_screen_settings'] = "native_single_fcm/App/get_login_screen_settings";
$route['native_single_fcm_v2/app/get_login_screen_settings'] = "native_single_fcm_v2/App/get_login_screen_settings";


$route['native_single_fcm/app/getUpdatedByModules'] = "native_single_fcm/App/getUpdatedByModules";
$route['native_single_fcm_v2/app/getUpdatedByModules'] = "native_single_fcm_v2/App/getUpdatedByModules";


$route['native_single_fcm/app/getModuleUpdateDate'] = "native_single_fcm/App/getModuleUpdateDate";
$route['native_single_fcm_v2/app/getModuleUpdateDate'] = "native_single_fcm_v2/App/getModuleUpdateDate";


$route['native_single_fcm/app/getUpdatedByModulesNew'] = "native_single_fcm/App/getUpdatedByModulesNew";
$route['native_single_fcm_v2/app/getUpdatedByModulesNew'] = "native_single_fcm_v2/App/getUpdatedByModulesNew";

$route['native_single_fcm/checkin/get_pdf_new'] = "native_single_fcm/Checkin/get_pdf_new";
$route['native_single_fcm_v2/checkin/get_pdf_new'] = "native_single_fcm_v2/Checkin/get_pdf_new";

$route['native_single_fcm/product/update_checkout_quantity'] = "native_single_fcm/Product/update_checkout_quantity";
$route['native_single_fcm_v2/product/update_checkout_quantity'] = "native_single_fcm/Product/update_checkout_quantity";
$route['native_single_fcm/product/getCurrencySymbol'] = "native_single_fcm/Product/getCurrencySymbol";
$route['native_single_fcm_v2/product/getCurrencySymbol'] = "native_single_fcm/Product/getCurrencySymbol";
$route['native_single_fcm/product/get_event'] = "native_single_fcm/Product/get_event";
$route['native_single_fcm_v2/product/get_event'] = "native_single_fcm/Product/get_event";

$route['native_single_fcm/native_single_fcm/getBondsAndLoansMasterEvent'] = "native_single_fcm/Native_single_fcm/getBondsAndLoansMasterEvent";



$route['native_single_fcm/document/compress_image'] = "native_single_fcm/Document/compress_image";
$route['native_single_fcm_v2/document/compress_image'] = "native_single_fcm_v2/Document/compress_image";



$route['native_single_fcm/event/getKnectPublicEvents'] = "native_single_fcm/Event/getKnectPublicEvents";
$route['native_single_fcm_v2/event/getKnectPublicEvents'] = "native_single_fcm_v2/Event/getKnectPublicEvents";
$route['native_single_fcm/event/getDefaultEvent_AsiaBrake'] = "native_single_fcm/Event/getDefaultEvent_AsiaBrake";
$route['native_single_fcm_v2/event/getDefaultEvent_AsiaBrake'] = "native_single_fcm_v2/Event/getDefaultEvent_AsiaBrake";
$route['native_single_fcm/event/getSolarplazaPublicEvents'] = "native_single_fcm/Event/getSolarplazaPublicEvents";
$route['native_single_fcm_v2/event/getSolarplazaPublicEvents'] = "native_single_fcm_v2/Event/getSolarplazaPublicEvents";


$route['native_single_fcm/agenda/getSystemMemInfo'] = "native_single_fcm/Agenda/getSystemMemInfo";
$route['native_single_fcm_v2/agenda/getSystemMemInfo'] = "native_single_fcm_v2/Agenda/getSystemMemInfo";

$route['native_single_fcm/agenda/getAgendaByTime_load'] = "native_single_fcm/Agenda/getAgendaByTime_load";
$route['native_single_fcm_v2/agenda/getAgendaByTime_load'] = "native_single_fcm_v2/Agenda/getAgendaByTime_load";

$route['native_single_fcm/agenda/get_agenda_offline_test'] = "native_single_fcm/Agenda/get_agenda_offline_test";
$route['native_single_fcm_v2/agenda/get_agenda_offline_test'] = "native_single_fcm_v2/Agenda/get_agenda_offline_test";

$route['native_single_fcm/attendee/attendee_list_v2'] = "native_single_fcm/Attendee/attendee_list_v2";


$route['native_single_fcm/maps/map_list'] = "native_single_fcm/Maps/map_list";
$route['native_single_fcm_v2/maps/map_list'] = "native_single_fcm_v2/Maps/map_list";



$route['native_single_fcm/maps/map_details'] = "native_single_fcm/Maps/map_details";
$route['native_single_fcm_v2/maps/map_details'] = "native_single_fcm_v2/Maps/map_details";


$route['native_single_fcm/maps/map_details_new'] = "native_single_fcm/Maps/map_details_new";
$route['native_single_fcm_v2/maps/map_details_new'] = "native_single_fcm_v2/Maps/map_details_new";


$route['native_single_fcm/maps/map_list_new'] = "native_single_fcm/Maps/map_list_new";
$route['native_single_fcm_v2/maps/map_list_new'] = "native_single_fcm_v2/Maps/map_list_new";


$route['native_single_fcm/maps/get_exi_info_arab'] = "native_single_fcm/Maps/get_exi_info_arab";
$route['native_single_fcm_v2/maps/get_exi_info_arab'] = "native_single_fcm_v2/Maps/get_exi_info_arab";


$route['native_single_fcm/maps/get_exi_info_medlab'] = "native_single_fcm/Maps/get_exi_info_medlab";
$route['native_single_fcm_v2/maps/get_exi_info_medlab'] = "native_single_fcm_v2/Maps/get_exi_info_medlab";


$route['native_single_fcm/maps/exhi_map_relation'] = "native_single_fcm/Maps/exhi_map_relation";
$route['native_single_fcm_v2/maps/exhi_map_relation'] = "native_single_fcm_v2/Maps/exhi_map_relation";


$route['native_single_fcm/maps/get_geojson'] = "native_single_fcm/Maps/get_geojson";
$route['native_single_fcm_v2/maps/get_geojson'] = "native_single_fcm_v2/Maps/get_geojson";


$route['native_single_fcm/maps/get_login_screen_settings'] = "native_single_fcm/Maps/get_login_screen_settings";
$route['native_single_fcm_v2/maps/get_login_screen_settings'] = "native_single_fcm_v2/Maps/get_login_screen_settings";


$route['native_single_fcm/maps/exhi_map_relation_FHA'] = "native_single_fcm/Maps/exhi_map_relation_FHA";
$route['native_single_fcm_v2/maps/exhi_map_relation_FHA'] = "native_single_fcm_v2/Maps/exhi_map_relation_FHA";


$route['native_single_fcm/maps/exhi_map_relation_connect_tech'] = "native_single_fcm/Maps/exhi_map_relation_connect_tech";
$route['native_single_fcm_v2/maps/exhi_map_relation_connect_tech'] = "native_single_fcm_v2/Maps/exhi_map_relation_connect_tech";



$route['native_single_fcm/login/generate_password'] = "native_single_fcm/Login/generate_password";
$route['native_single_fcm_v2/login/generate_password'] = "native_single_fcm_v2/Login/generate_password";


$route['native_single_fcm/gamification/add_user_game_point'] = "native_single_fcm/Gamification/add_user_game_point";
$route['native_single_fcm_v2/gamification/add_user_game_point'] = "native_single_fcm_v2/Gamification/add_user_game_point";


$route['native_single_fcm/message/getPublicMessages'] = "native_single_fcm/Message/getPublicMessages";
$route['native_single_fcm_v2/message/getPublicMessages'] = "native_single_fcm_v2/Message/getPublicMessages";



$route['native_single_fcm/message/publicMessageSend'] = "native_single_fcm/Message/publicMessageSend";
$route['native_single_fcm_v2/message/publicMessageSend'] = "native_single_fcm_v2/Message/publicMessageSend";


$route['native_single_fcm/message/public_msg_images_request'] = "native_single_fcm/Message/public_msg_images_request";
$route['native_single_fcm_v2/message/public_msg_images_request'] = "native_single_fcm_v2/Message/public_msg_images_request";


$route['native_single_fcm/message/delete_message'] = "native_single_fcm/Message/delete_message";
$route['native_single_fcm_v2/message/delete_message'] = "native_single_fcm_v2/Message/delete_message";


$route['native_single_fcm/message/make_comment'] = "native_single_fcm/Message/make_comment";
$route['native_single_fcm_v2/message/make_comment'] = "native_single_fcm_v2/Message/make_comment";


$route['native_single_fcm/message/delete_comment'] = "native_single_fcm/Message/delete_comment";
$route['native_single_fcm_v2/message/delete_comment'] = "native_single_fcm_v2/Message/delete_comment";


$route['native_single_fcm/message/getAllSpeakersAttendee'] = "native_single_fcm/Message/getAllSpeakersAttendee";
$route['native_single_fcm_v2/message/getAllSpeakersAttendee'] = "native_single_fcm_v2/Message/getAllSpeakersAttendee";


$route['native_single_fcm/message/getPrivateMessages'] = "native_single_fcm/Message/getPrivateMessages";
$route['native_single_fcm_v2/message/getPrivateMessages'] = "native_single_fcm_v2/Message/getPrivateMessages";


$route['native_single_fcm/message/privateMessageSend'] = "native_single_fcm/Message/privateMessageSend";
$route['native_single_fcm_v2/message/privateMessageSend'] = "native_single_fcm_v2/Message/privateMessageSend";


$route['native_single_fcm/message/private_msg_images_request'] = "native_single_fcm/Message/private_msg_images_request";
$route['native_single_fcm_v2/message/private_msg_images_request'] = "native_single_fcm_v2/Message/private_msg_images_request";


$route['native_single_fcm/message/getImageList'] = "native_single_fcm/Message/getImageList";
$route['native_single_fcm_v2/message/getImageList'] = "native_single_fcm_v2/Message/getImageList";




$route['native_single_fcm/message/notificationCounter'] = "native_single_fcm/Message/notificationCounter";
$route['native_single_fcm_v2/message/notificationCounter'] = "native_single_fcm_v2/Message/notificationCounter";
$route['native_single_fcm/message/getNotificationListing'] = "native_single_fcm/Message/getNotificationListing";
$route['native_single_fcm_v2/message/getNotificationListing'] = "native_single_fcm_v2/Message/getNotificationListing";

$route['native_single_fcm/message/deleteNotification'] = "native_single_fcm/Message/deleteNotification";
$route['native_single_fcm_v2/message/deleteNotification'] = "native_single_fcm_v2/Message/deleteNotification";

$route['native_single_fcm/message/getPrivateMessagesConversation'] = "native_single_fcm/Message/getPrivateMessagesConversation";
$route['native_single_fcm_v2/message/getPrivateMessagesConversation'] = "native_single_fcm_v2/Message/getPrivateMessagesConversation";

$route['native_single_fcm/message/getPrivateUnreadMessagesList'] = "native_single_fcm/Message/getPrivateUnreadMessagesList";
$route['native_single_fcm_v2/message/getPrivateUnreadMessagesList'] = "native_single_fcm_v2/Message/getPrivateUnreadMessagesList";

$route['native_single_fcm/message/getPrivateUnreadMessagesList_tmp'] = "native_single_fcm/Message/getPrivateUnreadMessagesList_tmp";
$route['native_single_fcm_v2/message/getPrivateUnreadMessagesList_tmp'] = "native_single_fcm_v2/Message/getPrivateUnreadMessagesList_tmp";

$route['native_single_fcm/message/privateMessageSendImage'] = "native_single_fcm/Message/privateMessageSendImage";
$route['native_single_fcm_v2/message/privateMessageSendImage'] = "native_single_fcm_v2/Message/privateMessageSendImage";

$route['native_single_fcm/message/privateMessageSendText'] = "native_single_fcm/Message/privateMessageSendText";
$route['native_single_fcm_v2/message/privateMessageSendText'] = "native_single_fcm_v2/Message/privateMessageSendText";
$route['native_single_fcm/message/delete_private_message'] = "native_single_fcm/Message/delete_private_message";
$route['native_single_fcm_v2/message/delete_private_message'] = "native_single_fcm_v2/Message/delete_private_message";
$route['native_single_fcm/message/get_all_contacts'] = "native_single_fcm/Message/get_all_contacts";
$route['native_single_fcm_v2/message/get_all_contacts'] = "native_single_fcm_v2/Message/get_all_contacts";

$route['native_single_fcm/message/get_my_messages'] = "native_single_fcm/Message/get_my_messages";
$route['native_single_fcm_v2/message/get_my_messages'] = "native_single_fcm_v2/Message/get_my_messages";

$route['native_single_fcm/message/delete_converstaion'] = "native_single_fcm/Message/delete_converstaion";
$route['native_single_fcm_v2/message/delete_converstaion'] = "native_single_fcm_v2/Message/delete_converstaion";
$route['native_single_fcm/message/get_my_messages_test'] = "native_single_fcm/Message/get_my_messages_test";
$route['native_single_fcm_v2/message/get_my_messages_test'] = "native_single_fcm_v2/Message/get_my_messages_test";

$route['native_single_fcm/message/add_user_game_point'] = "native_single_fcm/Message/add_user_game_point";
$route['native_single_fcm_v2/message/add_user_game_point'] = "native_single_fcm_v2/Message/add_user_game_point";




$route['native_single_fcm/fundraising/getProductsByCategory'] = "native_single_fcm/Fundraising/getProductsByCategory";
$route['native_single_fcm_v2/fundraising/getProductsByCategory'] = "native_single_fcm_v2/Fundraising/getProductsByCategory";

$route['native_single_fcm/fundraising/get_event'] = "native_single_fcm/Fundraising/get_event";
$route['native_single_fcm_v2/fundraising/get_event'] = "native_single_fcm_v2/Fundraising/get_event";

