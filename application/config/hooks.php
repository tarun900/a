<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/


$hook['post_controller_constructor'][] = array(
     'class' 	    => 'SecureCookies',
     'function' 	=> 'rewrite', 
     'filename' 	=> 'SecureCookies.php',
     'filepath' 	=> 'hooks',
     'params' 	    => array()
 );


$hook['post_controller_constructor'][] = array( 
  'class'    => 'RolePermission',
  'function' => 'check',
  'filename' => 'RolePermission.php',
  'filepath' => 'hooks'
);
$url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
// need to live when app updates
$url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
/*if(strpos($url,'native_single_fcm') == FALSE)
{
  $hook['pre_controller'][] = array( 
    'class'    => 'HTMLEntities',
    'function' => 'validate_input',
    'filename' => 'html_entity.php',
    'filepath' => 'hooks'
  );
}*/
if(strpos($url,'native_single_fcm') == TRUE)
{
  /*$hook['pre_controller'][] = array( 
    'class'    => 'HTMLEntities',
    'function' => 'validate_api_input',
    'filename' => 'html_entity.php',
    'filepath' => 'hooks'
  );*/
 
    $hook['post_controller_constructor'][] = array( 
      'class'    => 'ACL',
      'function' => 'validate_user',
      'filename' => 'Acl.php',
      'filepath' => 'hooks'
    );
  
}


if ((strpos($url,'Login') !== false || strpos($url,'App') !== false || $_SERVER['REQUEST_URI'] == '/') && strpos($url,'attendee_registration_screen') == false && strpos($url,'save_tempusersession') == false && strpos($url,'registraion_user_save_in_temp') == false && strpos($url,'native_single_fcm') == false && strpos($url,'forgot_pas') == false && strpos($url,'Attendee_login') == false  && strpos($url,'forceloginsave') == false && strpos($url,'save_login_screen') == false) {
  if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $hook['post_controller_constructor'][] = array( 
        'class'    => 'CSRF_Protection',
        'function' => 'validate_tokens',
        'filename' => 'csrf.php',
        'filepath' => 'hooks'
      );
  }
    $hook['post_controller_constructor'][] = array( 
      'class'    => 'CSRF_Protection',
      'function' => 'generate_token',
      'filename' => 'csrf.php',
      'filepath' => 'hooks'
    );
    $hook['display_override'] = array(
      'class'    => 'CSRF_Protection',
      'function' => 'inject_tokens',
      'filename' => 'csrf.php',
      'filepath' => 'hooks'
    );
}
/* End of file hooks.php */
/* Location: ./application/config/hooks.php */