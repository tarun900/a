<?php
/**
* CSRF Protection Class
*/
class CSRF_Protection
{
  /**
   * Holds CI instance
   *
   * @var CI instance
   */
  private $CI;
  
  /**
   * Name used to store token on session
   *
   * @var string
   */
  private static $token_name = 'csrf_token';
  
  /**
   * Stores the token
   *
   * @var string
   */
  private static $token;
  
  // -----------------------------------------------------------------------------------
  
  public function __construct()
  {
    session_start();
    $this->CI =& get_instance();


  }
  
  // -----------------------------------------------------------------------------------
  
  /**
   * Generates a CSRF token and stores it on session. Only one token per session is generated.
   * This must be tied to a post-controller hook, and before the hook
   * that calls the inject_tokens method().
   *
   * @return void
   * @author Ian Murray
   */
  public function generate_token()
  {
   
    // Load session library if not loaded
    if ($_SESSION['csrf_token']=='')
    {
      // Generate a token and store it on session, since old one appears to have expired.
      $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(16));
      
    }
    else
    {
      // Set it to local variable for easy access
      self::$token = $_SESSION['csrf_token'];
    }
  }
  
  // -----------------------------------------------------------------------------------
  
  /**
   * This injects hidden tags on all POST forms with the csrf token.
   * Also injects meta headers in <head> of output (if exists) for easy access
   * from JS frameworks.
   *
   * @return void
   * @author Ian Murray
   */
  public function inject_tokens()
  {
    $output = $this->CI->output->get_output();
    // Inject into form
    $output = preg_replace('/(<(form|FORM)[^>]*(method|METHOD)="(post|POST)"[^>]*>)/',
                           '$0<input type="hidden" name="' . 'csrf_token' . '" value="' . $_SESSION['csrf_token'] . '">', 
                           $output);
    
    // Inject into <head>
    $output = preg_replace('/(<\/head>)/',
                           '<meta name="csrf-name" content="' . 'csrf_token' . '">' . "\n" . '<meta name="csrf-token" content="' . $_SESSION['csrf_token'] . '">' . "\n" . '$0', 
                           $output);
    
    $this->CI->output->_display($output);
  }
  
  // -----------------------------------------------------------------------------------
  
  /**
   * Validates a submitted token when POST request is made.
   *
   * @return void
   * @author Ian Murray
   */
  public function validate_tokens()
  {  
    // Is this a post request?
    // @link http://stackoverflow.com/questions/1372147/php-check-whether-a-request-is-get-or-post

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && !$this->CI->input->is_ajax_request())
    {
      // Is the token field set and valid?
      $posted_token = $this->CI->input->post('csrf_token');
      if ($posted_token === FALSE || $posted_token != $_SESSION['csrf_token'])
      {
        // Invalid request, send error 400.
        //show_error('Request was invalid. Tokens did not match.', 400);
        $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(16));
      }
      unset($_POST['csrf_token']);
    }
  }
}