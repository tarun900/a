<?php

class SecureCookies
{

	/*
    * @author     : Kevin Morssink
    * @website    : https://www.kevinmorssink.nl
    *
    * @description    
    *  : A CodeIgniter `post_controller_constructor` hook in order to secure cookies better.
    *  : The hook does five things with the default CodeIgniter CSRF and/or session cookies:
    *  : 1. It enables a Strict SameSite flag (altough officially not yet supported in PHP's `setcookie` function).
    *  : 2. It forces the browser to use the session cookie as a session cookie only (browser stores it in memory only, not in  cache).
    *  : 3. It scopes the domain to the current domain, making it HostOnly (excluding other domains or subdomains than the current host).
    *  : 4. It enables the HttpOnly cookie flag (no cookie access through client side script).
    *  : 5. It enables the Secure cookie flag (browser will only send cookies over an SSL/TLS (HTTPS) encrypted connection).
    *
    * @installation 
    *  : 1. Place this file in the folder "application/hooks/" with filename "SecureCookies.php".
    *  : 2. Enable CodeIgniter hooks with setting "$config['enable_hooks'] = true;" in the file "application/config/config.php"
    *  : 3. Add the following code to the file "application/config/hooks.php":
    *
    *       $hook['post_controller_constructor'][] = array(
    *          'class'        => 'SecureCookies', 
    *          'function'     => 'rewrite', 
    *          'filename'     => 'SecureCookies.php',
    *          'filepath'     => 'hooks',
    *          'params'       => array()
    *      );
    *
    *  : 4. If your site does not use SSL/TLS (HTTPS) then comment out the `cookie_secure` line in the `__construct` function or set it's
    *       value to boolean false.
    *
    * @testing
    *  : Use `Google Chrome` with the `Gnaw Cookie extension` in order to test your domains cookies properly.
    *  : Don't forget to clear your browser cache while testing.
    */
    
	private $CII;

	public function __construct()
	{
		if (function_exists('get_instance')) {
			$this->CII = &get_instance();
			$this->CII->config->set_item('cookie_httponly', true);
			$this->CII->config->set_item('cookie_secure', true);
		}
	}

	private function makeHostOnly($cookieString)
	{
		$cookieDomain = $this->CII->config->item('cookie_domain');
		return (string) preg_replace('/(?:domain=' . preg_quote($cookieDomain) . '[;][ ])/i', '', $cookieString);
	}
    
	private function addSameSitePolicy($cookieString, $policy = 'strict')
	{
		return (string) $cookieString . '; SameSite=' . $policy . ';';
	}

	private function makeSessionOnly($cookieString)
	{
		return (string) preg_replace('/(?:(?:expires|Max-Age)=[a-zA-Z0-9, -:]{0,}[;][ ])/i', '', $cookieString);
	}
    
	private function fixCookieHeader($header)
	{
		$sessCookieName = $this->CII->config->item('sess_cookie_name');
		$header = $this->makeHostOnly($header);
		$header = $this->addSameSitePolicy($header, 'strict');
		if (preg_match('/^(?:Set-Cookie:(?:.*)' . preg_quote($sessCookieName) . '=)/i', $header)) {
			$header = $this->makeSessionOnly($header);
		}
		return $header;
	}

	public function rewrite()
	{
		$headersList = headers_list();
		foreach ($headersList as $header) {
			if (preg_match('/^(?:Set-Cookie:(?:.*))/i', $header)) {
				$header = $this->fixCookieHeader($header);
				headers_sent() or header($header);
			}
		}
	}
}

/* End of file SecureCookies.php */