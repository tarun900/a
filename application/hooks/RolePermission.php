<?php
class RolePermission
{
    private $CI;
    public function __construct()
    {
        /*error_reporting(E_ALL);
        ini_set('display_errors', '1');*/
        $this->CI = & get_instance();
        $this->CI->load->library('session');
        $this->CI->load->helper('url');
        $this->CI->load->database();
    }
    public function check()
    {
        $user = $this->CI->session->userdata('current_user');
        if ($user)
        {   
            $assign_menus = $this->CI->db->select('group_concat(m.menuurl SEPARATOR "/") as assigned_menu')->from('menu m')->join('role_permission rp', 'rp.Menu_id = m.id')->join('role r', 'r.Id = rp.Role_id')->where('rp.Role_id', $user[0]->Role_id)->group_by('rp.Role_id')->get()->row_array();
            $total_menu = $this->CI->db->select('group_concat(m.menuurl SEPARATOR "/") as menu')->from('menu m')->get()->row_array();
            $assign_menus = explode('/', $assign_menus['assigned_menu']);
            $total_menu = explode('/', $total_menu['menu']);
            $current_url = $this->CI->uri->segment(1);
            // echo "<pre>"; print_r($current_url); exit();
            $frontend_menus = ['App', 'Advertising', 'Agenda','Activity', 'Attendee', 'Exhibitors', 'Notes', 'Speakers', 'Presentation', 'Maps', 'Photos', 'Messages', 'Surveys', 'Documents', 'Social', 'fundraising', 'Sponsors_list', 'twitter_feed', 'activity', 'instagram_feed', 'facebook_feed', 'My_Favorites', 'QA', 'Gamification', 'Cms', 'Attendee_hub', 'Attendee_login', 'Events', 'Exibitors', 'Forbidden', 'Leader', 'MyContact', 'Pageaccess', 'Photos_hub', 'Public_messages_hub', 'Qr_scanner', 'Unauthenticate', 'profile'];
            $url = explode('/', uri_string());
            // echo "<pre>";print_r($url); exit();
            switch ($user[0]->Role_id)
            {
            case '3': // org
                break;

            case '4': // attendee
                if ($current_url != '' && $current_url != 'Dashboard' && $current_url != 'Forbidden' && $current_url != 'Login' && $current_url != 'Profile' && !in_array($current_url, $frontend_menus))
                {
                    redirect('forbidden');
                }
                break;

            case '5': // Administrator
                break;

            case '6': // Exhibitor
                if (($current_url == 'Exhibitor_portal' || $current_url == 'Exibitor_survey' || $current_url == 'Exhibitor_leads') || $current_url == '' || $current_url == 'Dashboard' || $current_url == 'Forbidden' || $current_url == 'Login' || $current_url == 'Profile' || in_array($current_url, $frontend_menus))
                {
                }
                else
                {
                    redirect('forbidden');
                }
                break;

            case '7': // speaker/moderator
                if ($user[0]->is_moderator == '0')
                {
                    if (!in_array($current_url, $assign_menus) && in_array($current_url, $total_menu) && $current_url != '' && $current_url != 'Dashboard' && $current_url != 'Forbidden' && $current_url != 'Login' && $current_url != 'Profile' && !in_array($current_url, $frontend_menus) || $current_url=="Sponsors")
                    {
                        redirect('forbidden');
                    }
                    if($url[0]."/".$url[1]=='Profile/update'){
                        redirect('forbidden');
                    }
                }
                if ($current_url == 'Speaker_Messages' || $current_url == '' || $current_url == 'Dashboard' || $current_url == 'Forbidden' || $current_url == 'Login' || $current_url == 'Profile' || in_array($current_url, $frontend_menus))
                {
                }
                elseif ($user[0]->is_moderator == '1')
                {
                    //redirect('forbidden');
                }
                break;

            default: // all
                if (!in_array($current_url, $assign_menus) && in_array($current_url, $total_menu) && $current_url != '' && $current_url != 'dashboard' && $current_url != 'forbidden' && $current_url != 'login' && $current_url != 'profile' && in_array($current_url, $frontend_menus))
                {
                    if ($current_url == 'event' && in_array('eventhomepage', $url))
                    {
                    }
                    else
                    {
                        // redirect('forbidden');
                    }
                }
                break;
            }
            // lead retrival

            if ($user[0]->role_type == '1')
            {   
                // j($current_url);
                if (($current_url == 'event' && in_array('eventhomepage', $url)) || $current_url == 'lead_representative_admin' || $current_url == 'exibitor_survey' || $current_url == 'lead_retrieval' || $current_url == '' || $current_url == 'dashboard' || $current_url == 'login' || $current_url == 'profile' || $current_url == 'forbidden' || in_array($current_url, $frontend_menus))
                {
                }
                /*else
                {
                    redirect('forbidden');
                }*/
            }
        }
    }
}