<?php
/**
* HTMLEntities Class
*/
class HTMLEntities
{
  /**
   * Holds CI instance
   *
   * @var CI instance
   */
  private $CI;
  
  // -----------------------------------------------------------------------------------
  
  public function __construct()
  {
   
    $this->CI =& get_instance();
  }
  
  // -----------------------------------------------------------------------------------

  /**
   * Validates a submitted values when POST request is made.
   *
   * @return void
   * @author Ian Murray
   */
  public function validate_input()
  {  
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
    /*echo "<pre>";
    print_r($_POST);exit;*/
      foreach ($_POST as $key => $value) {
        $type = gettype($value);
        switch ($type) {
          case 'string':
              if(!$this->isJson($value))
                $_POST[$key] = htmlentities($this->clean($value));
            break;
        }
      }
    }
  }

  public function validate_api_input()
  {  

    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      foreach ($_POST as $key => $value) {
        
        if($value != strip_tags($value) || $this->checkJs($value))
        {
          $data['success'] = false;
          $data['message'] = "Invalid Input. Please try with valid input.";
          echo json_encode($data);exit;
        }
        
      }
    }
  }

  public function clean($value)
  {
    $value = str_replace('<script>', '', $value);
    $value = str_replace('</script>', '', $value);
    $value = str_replace('alert(', '', $value);
    $value = str_replace('alert("', '', $value);
    $value = str_replace('alert(\'', '', $value);
    $value = str_replace('<marquee>', '', $value);
    $value = str_replace('marquee', '', $value);
    $value = str_replace('</marquee>', '', $value);
    $value = str_replace('onfocus', '', $value);
    $value = str_replace('onclick', '', $value);
    $value = str_replace('onblur', '', $value);
    $value = str_replace('autofocus', '', $value);
    $value = str_replace('javascript', '', $value);
    $value = str_replace('onerror', '', $value);
    $value = str_replace('onscroll', '', $value);
    $value = str_replace('onload', '', $value);
    $value = str_replace('oninput', '', $value);
    $value = str_replace('onpageshow', '', $value);
    $value = str_replace('window.location', '', $value);
    $value = str_replace('onwebkittransitionend', '', $value);
    return $value;
  }

  public function checkJs($value)
  {
    if (strpos($value, '<script>') !== false) { return true; }
    if (strpos($value,'</script>') !== false) { return true; }
    if (strpos($value,'alert(') !== false) { return true; }
    if (strpos($value,'alert("') !== false) { return true; }
    if (strpos($value,'alert(\'') !== false) { return true; }
    if (strpos($value,'alert') !== false) { return true; }
    if (strpos($value,'<marquee>') !== false) { return true; }
    if (strpos($value,'marquee') !== false) { return true; }
    if (strpos($value,'</marquee>') !== false) { return true; }
    if (strpos($value,'onfocus') !== false) { return true; }
    if (strpos($value,'onclick') !== false) { return true; }
    if (strpos($value,'onblur') !== false) { return true; }
    if (strpos($value,'autofocus') !== false) { return true; }
    if (strpos($value,'javascript') !== false) { return true; }
    if (strpos($value,'onerror') !== false) { return true; }
    if (strpos($value,'onscroll') !== false) { return true; }
    if (strpos($value,'onload') !== false) { return true; }
    if (strpos($value,'oninput') !== false) { return true; }
    if (strpos($value,'onpageshow') !== false) { return true; }
    if (strpos($value,'window.location') !== false) { return true; }
    if (strpos($value,'onwebkittransitionend') !== false) { return true; }
    return false;
  }
/*  public function validate_output()
  {  
    $output = $this->CI->output->get_output();
    if ($this->isJson($output))
    {
        //$json = json_decode($output,true);
        $str = html_entity_decode($output);
       
        $output = $str;
        $this->CI->output->_display($output);
        
    }
    
  }*/

  public function isJson($string) 
  {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }
}

