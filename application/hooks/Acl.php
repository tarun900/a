<?php

/**
 * Class ACL
 */
class ACL
{
    /**
     * @var
     */
    private $CI;

    /**
     * ACL constructor.
     */
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }

    /**
     /* validate user
    //  */
    public function validate_user()
    {
        if (isset($_POST)) {
            // User Authentication
            $user_id = (isset($_POST['loggedin_user_id'])) ? $_POST['loggedin_user_id'] : '';
            $token = $_POST['user_token'];

            if ($user_id != '' && $token != '') {
                $count = $this->CI->db->select('Id')->from('user')->where('Id', $user_id)->where('token', $token)->get()->num_rows();

                if ($count == 0) {
                    $data['success'] = false;
                    $data['is_authorized'] = '0';
                    $data['message'] = 'You are not authorized to access this functionality.';
                    echo json_encode($data);
                    exit;
                }
            }

            // Event Authentication
            $event_id       = (isset($_POST['event_id'])) ? $_POST['event_id'] : $_POST['auth_event_id'] ;
            $event_token    = $_POST['auth_event_token'];

            if ($event_id != '' && $event_token != '') {
                $count = $this->CI->db->select('Id')->from('event')->where('Id', $event_id)->where('event_token', $event_token)->get()->num_rows();

                if ($count == 0) {
                    $data['success'] = false;
                    $data['message'] = 'You are not authorized to access this functionality.';
                    echo json_encode($data);
                    exit;
                }
            }

            // Password Authentication

            $password = $_POST['password'];

            if($password!='' && isset($event_token))
            {
                $_POST['password'] = substr($password, 0, -4); 
            }
        }
    }
}