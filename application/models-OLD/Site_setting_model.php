<?php
class site_Setting_model extends CI_Model{
function __construct()
    {
        parent::__construct();
	$this->load->library('session');
    }
public function get_site_setting()
    {
        $this->db->select('*');
        $this->db->from('settings');
        $query = $this->db->get();
        $res = $query->result();
        return $res[0];
    }
public function update($data)
    {   
        $datatoupdate["Site_title"] = $data['site_title']; 
        $datatoupdate["Reserved_right"] = $data['reserved_right']; 
        if(array_key_exists("image_name", $data)) {
          $datatoupdate["Site_logo"] = $data['image_name'];
        }
        $this->db->where('Id',1);
        $this->db->update('settings',$datatoupdate);
        return $this->db->affected_rows();
    }
}
?>
