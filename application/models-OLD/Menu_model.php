<?php

class menu_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function add_menu($data)
    { 
        $this->db->select('em.id');
        $this->db->from('event_menu em');
        
        if(!empty($data['agenda_array']['menu_id']))
        {
          $this->db->where('em.menu_id', $data['agenda_array']['menu_id']);
        }
        
        if(!empty($data['agenda_array']['cms_id']))
        {
          $this->db->where('em.cms_id', $data['agenda_array']['cms_id']);
        }

        if(!empty($data['agenda_array']['agenda_id']))
        {
          $this->db->where('em.agenda_id', $data['agenda_array']['agenda_id']);
        }
        
        $this->db->where('em.event_id', $data['agenda_array']['event_id']);
        $query = $this->db->get();
        
        
        $menu_id = !empty($data['agenda_array']['menu_id']) ? $data['agenda_array']['menu_id'] : NULL;
        $cms_id = !empty($data['agenda_array']['cms_id']) ? $data['agenda_array']['cms_id'] : NULL;
        $agenda_id = !empty($data['agenda_array']['agenda_id']) ? $data['agenda_array']['agenda_id'] : NULL;
        
        $event_id = $data['agenda_array']['event_id'];
        $res = $query->result();
        
        if(empty($res))
        {
            $data['agenda_array']['menu_id'] = !empty($data['agenda_array']['menu_id']) ? $data['agenda_array']['menu_id'] : NULL;
            $data['agenda_array']['cms_id'] = !empty($data['agenda_array']['cms_id']) ? $data['agenda_array']['cms_id'] : NULL;
            $data['agenda_array']['agenda_id'] = !empty($data['agenda_array']['agenda_id']) ? $data['agenda_array']['agenda_id'] : NULL; 
            $this->db->insert('event_menu', $data['agenda_array']);
            $event_menu_id = $this->db->insert_id();
        }
        else
        {
            
            $array_agenda['menu_id']= $menu_id;
            $array_agenda['cms_id']= $cms_id;
            $array_agenda['agenda_id']=$agenda_id;
            $array_agenda['event_id']=$event_id;

            if($data['agenda_array']['img'] != NULL OR !empty($_POST['delete_image']))
            $array_agenda['img'] = ($_POST['delete_image']) ? '' : $data['agenda_array']['img'];

            if($data['agenda_array']['is_feture_product'] != NULL)
            $array_agenda['is_feture_product'] = $data['agenda_array']['is_feture_product'];

            if($data['agenda_array']['title'] != NULL)
            $array_agenda['title'] = $data['agenda_array']['title'];

            if($data['agenda_array']['img_view'] != NULL)
            $array_agenda['img_view'] = $data['agenda_array']['img_view'];

            if($data['agenda_array']['Redirect_url'] != NULL)
            $array_agenda['Redirect_url'] = $data['agenda_array']['Redirect_url'];
            
            if(!empty($menu_id))
            {
               $this->db->where('menu_id', $menu_id);
            }
            
            if(!empty($cms_id))
            {
                 $this->db->where('cms_id', $cms_id);
            }
            if(!empty($agenda_id))
            {
                $this->db->where('agenda_id',$agenda_id);
            }
            $this->db->where('event_id', $event_id);
            $this->db->update('event_menu', $array_agenda);
        }
        
        return true;        
    }

}

?>