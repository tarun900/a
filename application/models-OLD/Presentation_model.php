<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Presentation_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function add_presentation($data)
    {       

        if(!empty($data['presentation_array']['Images']))
        {
            foreach($data['presentation_array']['Images'] as $k=>$v)
            {
                $Images[] = $v;
            }
            
        }
        $array_presentation['Images'] = json_encode($Images);

        if(!empty($data['presentation_array']['Image_lock']))
        {
            foreach($data['presentation_array']['Image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }   
        }
        $array_presentation['Image_lock'] = json_encode($Image_lock);

        if($data['presentation_array']['user_permissions'] != NULL)
            $array_presentation['user_permissions']=$data['presentation_array']['user_permissions'];

        if($data['presentation_array']['Organisor_id'] != NULL)
        $array_presentation['Organisor_id'] = $data['presentation_array']['Organisor_id'];

        if($data['presentation_array']['Event_id'] != NULL)
        $array_presentation['Event_id'] = $data['presentation_array']['Event_id'];

        if($data['presentation_array']['Start_date'] != NULL)
        $array_presentation['Start_date'] = $data['presentation_array']['Start_date'];

        if($data['presentation_array']['Start_time'] != NULL)
        $array_presentation['Start_time'] = $data['presentation_array']['Start_time'];

        if($data['presentation_array']['End_date'] != NULL)
        $array_presentation['End_date'] = $data['presentation_array']['End_date'];

        if($data['presentation_array']['End_time'] != NULL)
        $array_presentation['End_time'] = $data['presentation_array']['End_time'];

        if($data['presentation_array']['Status'] != NULL)
        $array_presentation['Status'] = $data['presentation_array']['Status'];

        if($data['presentation_array']['Heading'] != NULL)
        $array_presentation['Heading'] = $data['presentation_array']['Heading'];

        if($data['presentation_array']['Thumbnail_status'] != NULL)
        $array_presentation['Thumbnail_status'] = $data['presentation_array']['Thumbnail_status'];

        if($data['presentation_array']['Auto_slide_status'] != NULL)
        $array_presentation['Auto_slide_status'] = $data['presentation_array']['Auto_slide_status'];

        if($data['presentation_array']['Address_map'] != NULL)
        $array_presentation['Address_map'] = $data['presentation_array']['Address_map'];

        if($data['presentation_array']['Types'] != NULL)
        $array_presentation['Types'] = $data['presentation_array']['Types'];

        if($data['presentation_array']['presentation_file_type'] != NULL)
            $array_presentation['presentation_file_type']=$data['presentation_array']['presentation_file_type'];

        $this->db->insert('presentation',$array_presentation);
        $p_id =  $this->db->insert_id();
        $tool_data['presentation_id'] = $p_id;
        $tool_data['lock_image'] = $Images[0];

        $this->db->insert('presentation_tool',$tool_data);

        
        return $p_id;
    }

    public function get_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('*');
        $this->db->from('presentation');
        if($logged_in_user_id)
        {
            //$this->db->where('Organisor_id',$logged_in_user_id);
        }
        $this->db->where('Event_id',$id);
        $this->db->where('Id',$this->uri->segment(4));
        $query = $this->db->get();
        $res = $query->result_array();
        if(!$res && $this->uri->segment(2) == 'edit')
            redirect('Forbidden');
        return $res;
    }


    public function get_all_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('p.*,e.Event_name');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        if($logged_in_user_id)
        {
            //$this->db->where('p.Organisor_id',$logged_in_user_id);
        }
        $this->db->where('p.Event_id',$id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }

    public function get_speaker_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('p.*,e.Event_name');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        if($logged_in_user_id)
        {
            $this->db->where('p.Organisor_id',$logged_in_user_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }

    public function update_presentation($data)
    {           

        $Speakers = $this->Speaker_model->get_speaker_list();
        $this->data['Speakers'] = $Speakers;

        $images=array();
        if(!empty($data['presentation_array']['old_images']))
        {
            foreach($data['presentation_array']['old_images'] as $k=>$v)
            {
                $images[] = $v;
            }  
            $array_presentation['Images'] = json_encode($images);              
        }
        if(!empty($data['presentation_array']['Images']))
        {
            foreach($data['presentation_array']['Images'] as $k=>$v)
            {
                $images[] = $v;
            }
            $array_presentation['Images'] = json_encode($images);
        }
        $Image_lock=array();
        if(!empty($data['presentation_array']['old_image_lock']))
        {
            foreach($data['presentation_array']['old_image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }  
            $array_presentation['Image_lock'] = json_encode($Image_lock);              
        }
        if(!empty($data['presentation_array']['Image_lock']))
        {
            foreach($data['presentation_array']['Image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }
            $array_presentation['Image_lock'] = json_encode($Image_lock); 
        }
    

        if($data['presentation_array']['Start_date'] != NULL)
        $array_presentation['Start_date'] = $data['presentation_array']['Start_date'];

        if($data['presentation_array']['Start_time'] != NULL)
        $array_presentation['Start_time'] = $data['presentation_array']['Start_time'];

        if($data['presentation_array']['End_date'] != NULL)
        $array_presentation['End_date'] = $data['presentation_array']['End_date'];

        if($data['presentation_array']['End_time'] != NULL)
        $array_presentation['End_time'] = $data['presentation_array']['End_time'];

        if($data['presentation_array']['Status'] != NULL)
        $array_presentation['Status'] = $data['presentation_array']['Status'];

        if($data['presentation_array']['Heading'] != NULL)
        $array_presentation['Heading'] = $data['presentation_array']['Heading'];

        if($data['presentation_array']['Auto_slide_status'] != NULL)
        $array_presentation['Auto_slide_status'] = $data['presentation_array']['Auto_slide_status'];

        if($data['presentation_array']['Thumbnail_status'] != NULL)
        $array_presentation['Thumbnail_status'] = $data['presentation_array']['Thumbnail_status'];

        if($data['presentation_array']['Address_map'] != NULL)
        {
            $array_presentation['Address_map'] = $data['presentation_array']['Address_map'];
        }
        else
        {
            $array_presentation['Address_map'] = NULL;
        }

        if($data['presentation_array']['Types'] != NULL)
        $array_presentation['Types'] = $data['presentation_array']['Types'];
        
        if(empty($data['presentation_array']['Image_current']))
        {
            $array_presentation['Image_current'] = NULL;
        }
        else {
            $array_presentation['Image_current'] = $data['presentation_array']['Image_current'];
        }

        if($data['presentation_array']['presentation_file_type'] != NULL)
            $array_presentation['presentation_file_type']=$data['presentation_array']['presentation_file_type'];
        if($data['presentation_array']['user_permissions'] != NULL)
            $array_presentation['user_permissions']=$data['presentation_array']['user_permissions'];

        $this->db->where('Id', $this->uri->segment(4));
        $this->db->update('presentation', $array_presentation);

        /*$this->db->where('presentation_id', $this->uri->segment(4));
        $this->db->delete('presentation_survey');
        foreach ($data['presentation_array']['add_survey'] as $key => $value) {
            $ps['presentation_id']=$this->uri->segment(4);
            $ps['survey_id']=$value;
            $ps['priority']=$key+1;
            $this->db->insert('presentation_survey',$ps);
        }*/
        //echo $this->db->last_query(); exit();
    }

    public function get_permission_list()
    {
        $orid = $this->data['user']->Id;
        if($this->data['user']->Role_name == 'User')
        {
            $this->db->select('*');
            $this->db->from('user');
            if($orid)
            {
                $this->db->where('user.Id',$orid);
            }
            $query = $this->db->get();
            $res = $query->result();
            return $res;
        }
    }

    public function delete_presentation($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('presentation');
        $str = $this->db->last_query();
    }

    public function get_menu_name($id)
    {

        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('menu_Setting');
        $this->db->where('Event_id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    /*public function get_presentation_survey_by_presentation_id($pid)
    {
        $this->db->select('*')->from('presentation_survey ps');
        $this->db->join('survey s','s.Id=ps.survey_id','left');
        $this->db->where('ps.presentation_id',$pid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }*/
    public function get_edit_survey_in_presentation($id,$sid)
    {
        $this->db->select('*');
        $this->db->from('survey');
        $this->db->where('Event_id',$id);
        $this->db->where('Id',$sid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_survey_and_by_user_id($sid,$uid=NULL)
    {
        $this->db->select('*');
        $this->db->from('poll_survey');
        $this->db->where('Question_id',$sid);
        if(!empty($uid)){
            $this->db->where('User_id',$uid);
        }
        $que=$this->db->get();
        $res=$que->result_array();
        return $res;   
    }
    public function save_ans_in_presentation($ans)
    {
        $res=$this->get_survey_and_by_user_id($ans['Question_id'],$ans['User_id']);
        if(count($res) > 0)
        {
            $this->db->where('Question_id',$ans['Question_id']);
            unset($ans['Question_id']);
            $this->db->where('User_id',$ans['User_id']);
            unset($ans['User_id']);
            $this->db->update('poll_survey',$ans);
        }
        else
        {
            $ans['answer_date']=date('Y-m-d H:i:s');
            $this->db->insert('poll_survey',$ans);
        }
    }
    public function get_survey_answer_chart_by_survry_ids($qids)
    {
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->where('s.Id', $qids);
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_all_speaker_list($id=null)
    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('r.Id',7);
        if($id)
        {
            $this->db->where('ru.Event_id',$id);
        }
        $this->db->where('r.Id !=',3);
        $this->db->where('r.Id !=',4);
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
        
?>