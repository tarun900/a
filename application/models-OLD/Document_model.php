<?php class Document_model extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }
  public function get_document_list($eventid,$id=null)
  {
    $this->db->select('d.title as doctitle,d1.title as parent_name,d.new_tab_status as dnew_tab_status,d.title_status as dtitle_status,d.parent,d.doc_type,d.Event_id,d.id,d.type,d.link,d.docicon,d.coverimages');
    $this->db->from('documents d');
    $this->db->join('documents d1','d.parent=d1.id','left');
    if($id)
    {
    $this->db->where('d.id',$id);
    }
    $this->db->where('d.Event_id',$eventid);
    $this->db->where('d.doc_type','1');
    $this->db->group_by('d.id');
    $this->db->order_by('d.id desc');
    $query = $this->db->get();
    $res = $query->result_array();
    if(!$res && $id!=null)
            redirect('Forbidden');
    return $res;
  }
  public function get_folder_list_event($eid)
  {
    $this->db->select('*')->from('documents d');
    $this->db->where('d.Event_id',$eid);
    $this->db->where('d.doc_type','0');
    $this->db->order_by('d.id desc');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function get_document_foldername_list($eventid,$id=null)
  {         
    $this->db->select('d.title as doctitle,d.parent,d.doc_type,d.Event_id,d.id,d.type');
    $this->db->from('documents d');
    if($id)
    {
      $this->db->where('d.id',$id);
    }
    $this->db->where('d.Event_id',$eventid);
    $this->db->where('d.doc_type','0');
    $this->db->order_by('d.id desc');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function get_document_file_list($document_id=null)
  {         
    $this->db->select('*');
    $this->db->from('document_files df');
    if($document_id)
    {
      $this->db->where('df.document_id',$document_id);
    }
    $this->db->order_by('df.id desc');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }       
  public function add_document($array_add)
  {
    $this->db->insert('documents', $array_add);
    $doc_id = $this->db->insert_id();
    return $doc_id;
  }
  public function checkfoldername($data,$fid)
  {
    $this->db->select('*')->from('documents');
    $this->db->where($data);
    if(!empty($fid))
    {
      $this->db->where('id !=',$fid);      
    }
    $res=$this->db->get()->result_array();
    if(count($res)>0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  public function edit_document_files($array_add,$did)
  {
    $this->db->select('*')->from('document_files');
    $this->db->where('document_id',$did);
    $res=$this->db->get()->result_array();
    if(count($res) > 0)
    {
      $this->db->where('document_id',$did);
      $this->db->update('document_files',$array_add);
    }
    else
    {
      $array_add['document_id']=$did;
      $this->db->insert('document_files',$array_add); 
    }
  }
  public function add_document_files($array_add)
  {
    $this->db->insert('document_files', $array_add);
    $docfiles_id = $this->db->insert_id();
    return $docfiles_id;
  }
  public function delete_document($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('documents');
    return true;
  }
  public function delete_document_files($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('document_files');
    return true;
  }
  public function edit_document($data,$id)
  {
    $this->db->where('id',$id);
    $this->db->update('documents', $data);
    return true;
  } 
  public function get_docfolder_list($eventid,$id=null)
  {
    $this->db->select('id,title');
    $this->db->from('documents d');
    if($id)
    {
      $this->db->where('d.id !=',$id);
    }
    $this->db->where('d.Event_id',$eventid);
    $this->db->where('d.doc_type','0');
    $this->db->order_by('d.title');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function getalldocumentflolder($eventid,$docid)
  {
    $this->db->select('*');
    $this->db->from('documents d');
    $this->db->where('parent', $docid);
    $this->db->where('Event_id', $eventid);
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function getalldocumentfiles($docid)
  {    
    $this->db->select('*');
    $this->db->from('document_files df');
    $this->db->where('df.document_id', $docid);
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function save_folder_name($eid,$folder_name,$folder_id,$folder_icon=NULL)
  {
    if(!empty($folder_icon))
    {
      $f_data['docicon']=$folder_icon;
    }
    $f_data['title']=$folder_name;
    $this->db->where('Event_id',$eid);
    $this->db->where('id',$folder_id);
    $this->db->update('documents',$f_data);
  }
  public function delete_folder($eid,$fid)
  {
    $this->db->where('Event_id',$eid);
    $this->db->where('id',$fid);
    $this->db->delete('documents');

    $this->db->where('Event_id',$eid);
    $this->db->where('parent',$fid);
    $this->db->update('documents',array('parent'=>'0'));
  }
  public function remove_icon_images($eid,$did)
  {
    $this->db->where('Event_id',$eid);
    $this->db->where('id',$did);
    $this->db->update('documents',array('docicon'=>NULL));

    $this->db->where('document_id',$did);
    $this->db->update('document_files',array('icon'=>NULL));
  }
}