<?php class Login_model extends CI_Model{
	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->db1 = $this->load->database('db1', TRUE);
            // $this->load->database();

       // $this->db->reconnect();

    }
   public function update_active_status($email)
    {
        $this->db1->select('*')->from('organizer_user ou');
        $this->db1->join('subscription_billing_cycle sbc','sbc.id=ou.subscriptiontype');
        $this->db1->where('ou.Email',$email);
        $qu=$this->db1->get();
        $res=$qu->result_array();
        $date=date("Y-m-d",strtotime("today"));
        if($res[0]['year']!=""){
            $Expiry_date=$date=date("Y-m-d",strtotime($date."+".$res[0]['year']." Year"));
        }
        else if($res[0]['month']!="")
        {
            $Expiry_date=$date=date("Y-m-d",strtotime($date."+".$res[0]['month']." months"));   
        }
        else if($res[0]['day']!="")
        {
            $Expiry_date=$date=date("Y-m-d",strtotime($date."+".$res[0]['day']." days"));   
        }
        else
        {
            $Expiry_date=$date;
        }
        $this->db->where('Email', $email);
        $this->db->update('user',array("Active"=>'1'));
        $this->db1->where('Email', $email);
        $this->db1->update('organizer_user',array("Active"=>'1',"Expiry_date"=>$Expiry_date));
       
    }
    public function check_login($data)
    {
        $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid');
        $this->db->from('user');
        $this->db->where('Email', $data['username']);
        $this->db->where('Password', md5($data['password']));
        $this->db->where('user.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->join('relation_event_user as ru','user.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->limit(1);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result();
        
        if(!empty($res))
        {
          unset($res[0]->Speaker_desc);
        }

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id',$res[0]->Organisor_id);
        $qu=$this->db->get();
        $organizeruser=$qu->result_array();
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('Email',$organizeruser[0]['Email']);
        $this->db1->where('Expiry_date >=',date("Y-m-d"));
        $this->db1->where('ou.Active','1');
        $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
        $query=$this->db1->get();
        $res1=$query->result_array();
        if(count($res1)==0 || $res1[0]['status']==0)
        {
            if(count($res)==0)
            {
                $res=$res1;
            }
            else{
            $res=$res1;
            $res="inactive";
            }
        }
        return $res;
    }
    
    public function check_token($token)
    {
        $this->db->select('u.*,role.Name as Role_name,u.Id as Id,role.Id as Rid,mobile_data.token as _token');
        $this->db->from('mobile_data');
        $this->db->where('token', $token);
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->join('user as u','u.Id=mobile_data.uid');
        $this->db->join('relation_event_user as ru','u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    
    public function check_token_with_event($token,$event_id)
    {
        $this->db->select('e.*,u.Id as user_id');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('mobile_data m', 'm.uid = u.Id');
        $this->db->where('m.token', $token);
        //$this->db->where('e.Id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function check_user_email($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $data['email']);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function change_pas($data)
    {
        $eid=$data['event_id'];
        unset($data['event_id']);
        $pass=array();
        if($eid=='447')
        {
            $this->load->library('RC4');
            $rc4 = new RC4();
            $pass=array("Password"=>$rc4->encrypt($data['new_pass']));
        }
        else
        {
            $pass=array("Password"=>md5($data['new_pass']));
        }
        $this->db->where('Email',$data['email']);
        $this->db->update('user',$pass);
    }
    public function get_site_setting()
    {
        $this->db->select('*');
        $this->db->from('settings');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
     public function get_user_details_by_user_id($uid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Id',$uid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function super_admin_user_account_login($email)
    {
        $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,usl.Website_url,usl.Facebook_url,usl.Twitter_url,usl.Linkedin_url');
        $this->db->from('user');
        $this->db->where('Email',$email);
        $this->db->where('user.Active', '1');
        $this->db->where('ru.Role_id =',3);
        $this->db->where('role.Active', '1');
        $this->db->join('relation_event_user as ru','user.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->join('user_social_links usl','usl.User_id=user.Id','left');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        if(!empty($res))
        {
          unset($res[0]->Speaker_desc);
        }

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id',$res[0]->Organisor_id);
        $qu=$this->db->get();
        $organizeruser=$qu->result_array();
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('Email',$email);
        $this->db1->where('Expiry_date >=',date("Y-m-d"));
        $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
        $query=$this->db1->get();
        $res1=$query->result_array();
        if(count($res1)==0 || $res1[0]['status']==0)
        {
            if(count($res)==0)
            {
                $res=$res1;
            }
            else{
            $res=$res1;
            $res="inactive";
            }
        }
        return $res;
    }
    public function logout()
    {
    $this->session->unset_userdata('some_name');
    }
    public function getUserAttempts($user_id,$key_name)
    {
        $where['user_id'] = $user_id;
        $where['key_name'] = $key_name;
        return $this->db->select('*')->from('user_attempts')->where($where)->order_by('id','desc')->limit(1)->get()->row_array();
    }
    public function updateUserAttempts($id,$date,$attempts)
    {
        $c_date = date('Y-m-d');
        if($c_date != date('Y-m-d',strtotime($date)))
        {
            $update['created_date'] = $c_date;
            $update['value'] = 1;
        }
        else
        {
            $update['value'] = $attempts;
        }

        $this->db->where('id',$id);
        $this->db->update('user_attempts',$update) ;

    }
    public function insertUserAttempts($user_id,$keyname,$value = 1)
    {
        date_default_timezone_set('UTC');
        $data['user_id'] = $user_id;
        $data['key_name'] = $keyname;
        $data['value'] = $value;
        $data['created_date'] = date('Y-m-d H:i:s');
        $data['updated_date'] = date('Y-m-d H:i:s');
       
        $this->db->insert('user_attempts',$data) ;

    }
    public function getUser($email)
    {
        return $this->db->where('Email',$email)->from('user')->get()->row_array();
    }

    public function add_login_log($array_log)
    {
        //$this->db->reconnect();
        $this->db->insert('login_details', $array_log);
        return true;
    }
    public function checkToken($token)
    {
        return $this->db->select('id')->from('user_attempts')->where('value',$token)->get()->num_rows();
    }
    public function getUserFromToken($token)
    {
        $this->db->join('user u','u.Id = ua.user_id');
        $this->db->where('ua.value',$token);
        $res = $this->db->get('user_attempts ua')->row_array();
        return $res;
    }
}
?>
