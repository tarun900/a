<?php
class Exhibitor_model extends CI_Model{
    function __construct()
    {   
       // error_reporting(E_ALL);
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);

    }

   
    public function getExhibitorListByEventId($Event_id=null,$page_no,$user_id)
    {
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        $exibitors = [];
        if($Event_id == '447')
        {
            $total = $this->db->select('*')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('e.Event_id',$Event_id)->get()->num_rows();
            $limit          = 20;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;
            foreach ($types as $key => $value) 
            {
                $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                                  CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                                  CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                                  CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                                  CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                                  CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                                  CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                                  CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                                  CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                                  CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                                  CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                                  CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                                  CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                                  CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                                  CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
                $this->db->from('exibitor e');
                $this->db->join('user u','u.Id = e.user_id');
                $this->db->where('e.Event_id',$Event_id);
                $this->db->order_by('e.Heading');
                $this->db->where('e.et_id',$value['type_id']);
                $this->db->limit($limit, $start);
                $this->db->order_by('e.Heading');

                $query = $this->db->get();
                $result_data = $query->result_array(); 
                if(!empty($result_data))
                {
                    $results['type'] = $value['type_name'];
                    $results['bg_color'] = $value['type_color'];
                    $results['data'] = $result_data;
                    $exibitors[] = $results; 
                }
            }
           // echo count($exibitors['data']);exit;
            $count = count($exibitors['data']);
            if(($count < $limit) && ($count != 0))
            {
                $limit = $limit - $count;
                $start = $start + ($count+1);
            }
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                                CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                                CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                                CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                                CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                                CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                                CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                                CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                                CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                                CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                                CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                                CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                                CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                                CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                                CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->where('e.Event_id',$Event_id);
            $this->db->where('e.et_id IS NULL');
            $this->db->order_by('e.Heading');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $data_result =  $query->result_array(); 
            if(!empty($data_result))
            {
                $results['type'] = '';
                $results['bg_color'] = '';
                $results['data'] =$data_result;
                $exibitors[] = $results; 
            }
            foreach ($exibitors as $key => $value) {
                foreach ($value['data'] as $key1 => $value) {
                    $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                    $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                    
                    $images_decode = json_decode($value['Images']);
                    $cmpy_logo_decode = json_decode($value['company_logo']);
                    if(empty($images_decode[0]))
                    {
                        $images_decode[0]="";
                    }
                    if(empty($cmpy_logo_decode[0]))
                    {
                        $cmpy_logo_decode[0]="";
                    }
                    $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                    
                    $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                    $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                    $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                    $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                    $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                    $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                    $exibitors[$key]['data'][$key1]['instagram_url']=$value['instagram_url'];
                    $exibitors[$key]['data'][$key1]['youtube_url']=$value['youtube_url'];
                    $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                    $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                }
            }
            $data['exhibitors'] = $exibitors;
            $total_page     = ceil($total/$limit);
            $data['total'] = $total_page;
            return $data;
        }
        else
        {
            
            foreach ($types as $key => $value) 
            {
                $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                                CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                                CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                                CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                                CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                                CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                                CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                                CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                                CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                                CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                                CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                                CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                                CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                                CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                                CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
                $this->db->from('exibitor e');
                $this->db->where('e.Event_id',$Event_id);
                $this->db->where('e.et_id',$value['type_id']);
                $this->db->order_by('e.Heading');

                $query = $this->db->get();
                $data1 = $query->result_array();
                if(!empty($data1))
                {
                    $results['type'] = $value['type_name'];
                    $results['bg_color'] = $value['type_color'];
                    $results['data'] = $query->result_array(); 
                    $exibitors[] = $results; 
                }
            }

            //$this->db->select('*');
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                             CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                             CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                             CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                             CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                             CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                             CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                             CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                             CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                             CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                             CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                             CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                             CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                             CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                             CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->where('e.Event_id',$Event_id);
            $this->db->where('e.et_id IS NULL');
            $this->db->order_by('e.Heading');
            $query = $this->db->get();
            $data2 = $query->result_array();
            if(!empty($data2))
            {
                $results['type'] = '';
                $results['bg_color'] = '';
                $results['data'] = $query->result_array(); 
                $exibitors[] = $results; 
            }
            foreach ($exibitors as $key => $value) 
            {
                foreach ($value['data'] as $key1 => $value) 
                {
                    $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                    $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                    
                    $images_decode = json_decode($value['Images']);
                    $cmpy_logo_decode = json_decode($value['company_logo']);
                    if(empty($images_decode[0]))
                    {
                        $images_decode[0]="";
                    }
                    if(empty($cmpy_logo_decode[0]))
                    {
                        $cmpy_logo_decode[0]="";
                    }
                    $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                    
                    $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                    $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                    $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                    $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                    $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                    $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                    $exibitors[$key]['data'][$key1]['instagram_url']=$value['instagram_url'];
                    $exibitors[$key]['data'][$key1]['youtube_url']=$value['youtube_url'];
                    $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                    $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                    $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','3')->where('module_id',$value['exhibitor_page_id'])->get()->num_rows();
                    $exibitors[$key]['data'][$key1]['is_favorites']=($count) ? '1' : '0';
                }
            }
            return $exibitors;
        }
    }  
    public function getExhibitorDetails($id,$user_id,$event_id,$lang_id=NULL)
    {
        $this->db->_protect_identifiers=false;
        $this->db->select('CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                        CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                        CASE WHEN e.main_contact_name IS NULL THEN "" ELSE e.main_contact_name END as main_contact_name,
                        CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                        (CASE WHEN elmc.content IS NULL Then CASE WHEN e.Description IS NULL THEN "" ELSE e.Description END ELSE elmc.content End) as Description,
                        CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                        CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                        CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                        CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                        CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                        CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                        CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                        CASE WHEN e.HQ_phone_number IS NULL THEN "" ELSE e.HQ_phone_number END as HQ_phone_number,
                        CASE WHEN e.main_email_address IS NULL THEN "" ELSE e.main_email_address END as main_email_address,
                        CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                        CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                        CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id, CONCAT( u.Firstname , " ",u.Lastname) as user_name, 
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name',FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=3 and elmc.modules_id=e.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('e.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();   
        $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
        $extra=json_decode($extra['extra_column'],true);
        if(!empty($res))
        {
        foreach ($extra as $key => $value) {
          $keyword="{".str_replace(' ', '', $key)."}";
          if(stripos(strip_tags($res[0]['Description'],$keyword)) !== false)
          {
            $res[0]['Description']=str_ireplace($keyword, $value,$res[0]['Description']);
          }
        }
        }
        // #exhibitor_to_map
        $map_details = $this->db->select('mim.coords,m.Id as map_id,(CASE WHEN elmc.title IS NULL THEN m.Map_title ELSE elmc.title END) as Map_title,m.Images')
        ->from('map_image_mapping mim')
        ->join('map m','m.Id = mim.map_id','left')
        ->join('event_lang_modules_content elmc','elmc.menu_id=10 and elmc.modules_id=m.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left')
        ->where('mim.user_id',$res[0]['exhibitor_id'])
        ->where('m.Event_id',$event_id)
        ->get()->row_array();
        if(count($map_details))
        {
            $res[0]['is_visible_view_btn']='1';
            $decode_image=json_decode($map_details['Images']);
            $map_details['Images']=$decode_image[0];

            $res[0]['map_details']=$map_details;

        }
        else
        {
            $res[0]['is_visible_view_btn']='0';
            $res[0]['map_details']=new stdClass;
        }
        $res[0]['user_name']=($res[0]['user_name']) ? $res[0]['user_name'] : '';

        $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('user_id',$user_id)->where('module_type','3')->where('module_id',$id)->get()->num_rows();
        $res[0]['is_favorites']=($count) ? '1' : '0';
        $tmp = $this->db->select('CASE WHEN check_dwg_files IS NULL THEN "0" ELSE check_dwg_files END AS check_dwg_files',false)->from('map')->where('Event_id',$event_id)->get()->row_array();
        $res[0]['check_dwg_files'] = $tmp['check_dwg_files'];
        //$res[0]['is_visible_view_btn'] = ($res[0]['check_dwg_files'] == '1' || $res[0]['is_visible_view_btn'] == '1') ? '1' : '0';
        return $res;
    }  
    public function get_exibitors_data($exibitor_id)
    {

        $this->db->select('*');
        $this->db->from('exibitor e');
        $this->db->where('e.Event_id',$res[0]['Id']);
        $this->db->where('e.Id',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;

    } 
    public function getImages($event_id,$message_id){
        $this->db->select("image");
        $this->db->from('speaker_msg');
        $this->db->where('Id',$message_id);
        $this->db->where('Event_id',$event_id);
        $rows = $this->db->get()->row();
        return $rows->image;
    }
    
    public function getSerachableRecords($Event_id,$keyword,$user_id)
    {
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        $exibitors = [];
        
            foreach ($types as $key => $value) {
                $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                                CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                                CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                                CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                                CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                                CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                                CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                                CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                                CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                                CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                                CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                                CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                                CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                                CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                                CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
                $this->db->from('exibitor e');
                $this->db->where('e.Event_id',$Event_id);
                $where = "(Short_desc like '%$keyword%' or Heading like '%$keyword%')";
                $this->db->where($where);
                $this->db->where('e.et_id',$value['type_id']);
                $this->db->order_by('e.Heading');

                $query = $this->db->get();
                $data1 = $query->result_array();
                if(!empty($data1))
                {
                    $results['type'] = $value['type_name'];
                    $results['bg_color'] = $value['type_color'];
                    $results['data'] = $query->result_array(); 
                    $exibitors[] = $results; 
                }
            }

            //$this->db->select('*');
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                                CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                                CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                                CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                                CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                                CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                                CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                                CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                                CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                                CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                                CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                                CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                                CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                                CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                                CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id = e.user_id');
            $this->db->where('e.Event_id',$Event_id);
            $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
            $this->db->where($where);
            $this->db->where('e.et_id IS NULL');
            $this->db->order_by('e.Heading');
            $query = $this->db->get();
            $data2 = $query->result_array();
            //echo $this->db->last_query();exit;
            if(!empty($data2))
            {
                $results['type'] = '';
                $results['bg_color'] = '';
                $results['data'] = $query->result_array(); 
                $exibitors[] = $results; 
            }
            foreach ($exibitors as $key => $value) 
            {
                foreach ($value['data'] as $key1 => $value) 
                {
                    $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                    $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                    
                    $images_decode = json_decode($value['Images']);
                    $cmpy_logo_decode = json_decode($value['company_logo']);
                    if(empty($images_decode[0]))
                    {
                        $images_decode[0]="";
                    }
                    if(empty($cmpy_logo_decode[0]))
                    {
                        $cmpy_logo_decode[0]="";
                    }
                    $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                    
                    $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                    $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                    $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                    $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                    $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                    $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                    $exibitors[$key]['data'][$key1]['instagram_url']=$value['instagram_url'];
                    $exibitors[$key]['data'][$key1]['youtube_url']=$value['youtube_url'];
                    $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                    $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                    $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','3')->where('module_id',$value['Id'])->get()->num_rows();
                    $exibitors[$key]['data'][$key1]['is_favorites']=($count) ? '1' : '0';
                }
            }
            return $exibitors;
       
    }  
    public function saveRequest($data)
    {
        $count = $this->db->select('*')->from('exhibitor_attendee_meeting')->where($data)->get()->num_rows();
        if(!$count)
        {
            $data['created_datetime'] = date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_attendee_meeting',$data);
            return true;
        }
        else
        {
            return false;
        }
    } 
    public function checkSessionClash($attendee_id,$date,$time)
    {
        $result['result'] = true;
        $data = $this->db->select('*')->from('users_agenda')->where('user_id',$attendee_id)->get()->row_array();
        $agenda_ids = explode(',',$data['agenda_id']);
        $agenda = $this->db->select('*')->from('agenda')->where_in('Id',$agenda_ids)->get()->result_array();
        $d_date = strtotime($date);
        $time = strtotime($time);
        foreach ($agenda as $key => $value) 
        {
           $start_date = strtotime($value['Start_date']);
           $end_date = strtotime($value['End_date']);
           $start_time = strtotime($value['Start_time']);
           $end_time = strtotime($value['End_time']);
           if($d_date >= $start_date && $d_date <= $end_date)
           {
                $result['result'] = false;
                $result['agenda_name'] = $value['Heading'];
                break;
           }
        }
        return $result;
    }  
    public function getUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Exhibitors/'.$data['acc_name'].'/'.$data['Subdomain'].'/checpendingkmetting/';
    }
    public function getUser($exhibitor_id)
    {
        return $this->db->select('u.*')->from('user u')->join('exibitor e','e.user_id = u.Id')->where('e.Id',$exhibitor_id)->get()->row_array();
    }
    public function saveSpeakerMessage($data)
    {
        $this->db->insert("speaker_msg",$data);
        $message_id=$this->db->insert_id();
        if($message_id!='')
        {

             $data1 = array(
                    'msg_id' => $message_id,
                    'resiver_id' => $data['Receiver_id'],
                    'sender_id' => $data['Sender_id'],
                    'event_id' => $data['Event_id'],
                    'isread' => '1',
                    'type' => '0'
                );
            $this->db->insert('msg_notifiy', $data1);
            return $message_id;
        }
        else
        {
            return 0;
        }
    }
    public function notifypublic($eventid,$user_id)
    {    
          $this->db->select('group_concat(ru.User_id) as User_id');
          $this->db->from('relation_event_user ru');
          $this->db->where('ru.Event_id', $eventid);
          $this->db->where('ru.User_id !=',$user_id);
          $query = $this->db->get();
          $res1 = $query->result_array();
          
          $finalarray=array();
          if($res1[0]['User_id']!="")
          {
               $finalarray=  explode(',', $res1[0]['User_id']);
               $finalarray=  array_unique($finalarray);
          }
          return $finalarray;
    }
    public function getEventName($event_id)
    {
        $data = $this->db->select('Event_name')->from('event')->where('Id',$event_id)->get()->row_array();
        return $data['Event_name'];
    }
    public function getDateTimeArray($event_id)
    {
        $data = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $data1 = $this->db->select('format_time')->from('fundraising_setting')->where('Event_id',$event_id)->get()->row_array();

        $start = date('Y-m-d',strtotime($data['Start_date']));
        $time1 = date('H:i',strtotime($data['Start_time']));
        $time2 = date('H:i',strtotime($data['End_time']));
        
        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        
        $res=$query->result_array();

        while($start <= $data['End_date'])
        {   
            if($res[0]['date_format'] == 0)
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            else
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }
        $time=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
        $format="H:i";
        if($data1['format_time'] == '0')
        {
            $format="h:i A";
        }   
        else
        {
            $format="H:i";
        } 
        foreach ($time as $key => $value) {
           if((date($format,$value)) == "00:00")
               continue;
           $time_arr[] = date($format,$value);
        }
        $result['date'] = $date;
        $result['time'] = $time_arr;
        return $result;
    }
    public function getAllMeetingRequest1($event_id,$user_id)
    {
        $data = $this->db->select('e.Id')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('u.token',$user_id)->where('e.event_id',$event_id)->get()->row_array();

        return $this->db->select('u.Firstname,u.Lastname,DATE_FORMAT(eam.date,"%d/%m/%Y") as date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
    }
    public function getAllMeetingRequest($event_id,$user_id)
    {
        $data = $this->db->select('Id')->from('exibitor')->where('user_id',$user_id)->where('event_id',$event_id)->get()->row_array();
       return  $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
        DATE_FORMAT(eam.date,"%d/%m/%Y") as date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id,
        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
       
    }
    public function updateRequest($update_data,$where)
    {
        $this->db->where($where);
        $this->db->update('exhibitor_attendee_meeting',$update_data);
        if($update_data['status'] == '1')
        {
            $data['event_id'] = $ex['event_id'];
            $data['user_id'] = $ex['attendee_id'];
            $data['modules_id'] = '0';
            $data['reminder_time'] = '15';
            $data['read_status'] = '0';
            $data['created_date'] = date('Y-m-d H:i:s');

           // $this->db->insert('user_reminder',$data);
        }
        return $this->db->select('*')->from('exhibitor_attendee_meeting')->where($where)->get()->row_array();
    }
    public function getExhibitor($id)
    {
        return $this->db->select('*')->from('exibitor')->where('Id',$id)->get()->row_array();
    }
    public function getUsersData($id)
    {
        return $this->db->select('*')->from('user')->where('Id',$id)->get()->row_array();
    }  
    public function getUserIdByToken($token)
    {
        $token = ($token == '') ? '' : $token;
        $user = $this->db->select('Id')->from('user')->where('token',$token)->get()->row_array();
        return $user['Id'];
    }
    public function saveSuggestedDate($suggest)
    {
        $this->db->insert('suggest_meeting',$suggest);
    }
    public function getSuggestUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Exhibitors/'.$data['acc_name'].'/'.$data['Subdomain'].'/';
    }
    public function getAvailableTimes($where)
    {
        return $this->db->select('sm.Id,date_time,Heading')->from('suggest_meeting sm')->join('exibitor e','e.user_id = sm.exhibitor_user_id')->where($where)->get()->result_array();
    }
    public function bookSuggestedTime($where,$date_time)
    {
        $result = $this->db->select('*')->from('suggest_meeting')->where($where)->get()->row_array();   
        if(!empty($result))
        {
            $exhibitor = $this->db->select('*')->from('exibitor')->where('user_id',$result['exhibitor_user_id'])->get()->row_array();   
            
            $update['exhibiotor_id'] = $exhibitor['Id'];
            $update['attendee_id'] = $where['attendee_id'];
            $update['event_id'] = $where['event_id'];
            $update['date'] = date('Y-m-d',strtotime($date_time));
            $update['time'] = date('H:i:s',strtotime($date_time));
            $update['status'] = '1';

            $this->db->where('Id',$result['meeting_id']);
            $this->db->update('exhibitor_attendee_meeting',$update);

            $where1['attendee_id'] = $where['attendee_id'];
            $where1['event_id'] = $where['event_id'];
            $this->db->where($where1);
            $this->db->delete('suggest_meeting');

            return $this->db->select('*')->from('user')->where('Id',$result['exhibitor_user_id'])->get()->row_array();
        }
    }
    public function getAttenee($id)
    {
        return $this->db->select('u.*')->from('user u')->join('exhibitor_attendee_meeting eam','eam.attendee_id = u.Id')->where('eam.Id',$id)->get()->row_array();
    }
    // #gulfoodsearch
    public function GulfoodSearch($where,$event_id,$where_keyword,$sector_where,$category_where,$page_no)
    {
        $this->db->select('*')->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $total = $this->db->get()->num_rows();
        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                         CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                         CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                         CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                         CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                         CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                         CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                         CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                         CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                         CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.twitter_url END as instagram_url,
                         CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                         CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                         CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                         CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                         CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $this->db->order_by('e.Heading');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $res = $query->result();  
        $exibitors = array();
        for($i=0; $i<count($res); $i++)
        {
            $exibitors[$i]['exhibitor_id']=$res[$i]->exhibitor_id;
            $exibitors[$i]['exhibitor_page_id']=$res[$i]->exhibitor_page_id;
            $exibitors[$i]['Heading']=ucfirst($res[$i]->Heading);
            $exibitors[$i]['Heading']=ucfirst($res[$i]->Heading);
            $exibitors[$i]['Short_desc']=$res[$i]->Short_desc;
            
            $images_decode = json_decode($res[$i]->Images);
            $cmpy_logo_decode = json_decode($res[$i]->company_logo);
            if(empty($images_decode[0]))
            {
                $images_decode[0]="";
            }
            if(empty($cmpy_logo_decode[0]))
            {
                $cmpy_logo_decode[0]="";
            }
            $exibitors[$i]['Images']=$images_decode[0];
            
            $exibitors[$i]['stand_number']=$res[$i]->stand_number;
            $exibitors[$i]['company_logo']= $cmpy_logo_decode[0];
            $exibitors[$i]['website_url']=$res[$i]->website_url;
            $exibitors[$i]['facebook_url']=$res[$i]->facebook_url;
            $exibitors[$i]['twitter_url']=$res[$i]->twitter_url;
            $exibitors[$i]['linkedin_url']=$res[$i]->linkedin_url;
            $exibitors[$i]['instagram_url']=$res[$i]->instagram_url;
            $exibitors[$i]['youtube_url']=$res[$i]->youtube_url;
            $exibitors[$i]['phone_number1']=$res[$i]->phone_number1;
            $exibitors[$i]['email_address']=$res[$i]->email_address;
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
    // #gulfoodsearch
    public function getSectorList()
    {
        return $this->db->select('*')->from('exhibitor_sector')->get()->result_array();
    }
    // #gulfoodsearch
    public function getGulfoodCategories()
    {
        return $this->db->select('*')->from('exhibitor_category')->get()->result_array();
    }
    // #gulfoodsearch
    public function getCountryById($id)
    {
        $data = $this->db->select('country_name')->from('country')->where('id',$id)->get()->row_array();
        return ($data['country_name']) ? $data['country_name'] : '';
    }
    // #gulfoodsearch
    public function getCategoryById($id)
    {
        $data = $this->db->select('category')->from('exhibitor_category')->where('id',$id)->get()->row_array();
        return ($data['category']) ? $data['category'] : '';
    }
    // #gulfoodsearch
    public function getSectorById($id)
    {
        $data = $this->db->select('sector')->from('exhibitor_sector')->where('id',$id)->get()->row_array();
        return ($data['sector']) ? $data['sector'] : '';
    }

    public function getRequestData($id)
    {
        return $this->db->select('*')->from('exhibitor_attendee_meeting')->where('Id',$id)->get()->row_array();
    } 
    
    public function getExUser($id,$event_id)
    {
        return $this->db->select('u.*')->from('user u')
        ->join('exibitor e','e.user_id = u.Id')->where('e.Id',$id)->where('e.Event_id',$event_id)->get()->row_array();
    } 

    public function checkEventDateFormat($event_id)
    {
        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        
        $res=$query->result_array();
        return $res;
    }
    public function getExhibitorListByEventIdNative($Event_id=null,$page_no,$user_id,$where='')
    {
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        $exibitors = [];
        
        $this->db->select('*');
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($where!='')
            $this->db->where($where);
        $this->db->where('e.Event_id',$Event_id);
        $total          = $this->db->get()->num_rows();
        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;
        foreach ($types as $key => $value) 
        {   
            
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                  CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                  CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                  CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                  CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                  CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                  CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                  CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                  CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                  CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                  CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                  CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                  CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                  CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                  CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id = e.user_id');
            $this->db->where('e.Event_id',$Event_id);
            if($where!='')
            $this->db->where($where);
            $this->db->order_by('e.Heading');
            $this->db->where('e.et_id',$value['type_id']);
            $this->db->limit($limit, $start);
            $this->db->order_by('e.Heading');

            $query = $this->db->get();
            $result_data = $query->result_array(); 
            /*if($value['type_id']=='1')
            {
                echo $this->db->last_query();
                print_r($result_data);exit;
            }*/
            if(!empty($result_data))
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['data'] = $result_data;
                $exibitors[] = $results; 
            }
        }
       // echo count($exibitors['data']);exit;
        $count = count($exibitors['data']);
        if(($count < $limit) && ($count != 0))
        {
            $limit = $limit - $count;
            $start = $start + ($count+1);
        }
        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                            CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                            CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                            CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                            CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                            CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                            CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                            CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                            CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                            CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                            CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                            CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                            CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                            CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        $this->db->where('e.Event_id',$Event_id);
        $this->db->where('e.et_id IS NULL');
        if($where!='')
            $this->db->where($where);
        $this->db->order_by('e.Heading');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $data_result =  $query->result_array(); 
        if(!empty($data_result))
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['data'] =$data_result;
            $exibitors[] = $results; 
        }
        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['instagram_url']=$value['instagram_url'];
                $exibitors[$key]['data'][$key1]['youtube_url']=$value['youtube_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','3')->where('module_id',$value['exhibitor_page_id'])->get()->num_rows();
                $exibitors[$key]['data'][$key1]['is_favorites']=($count) ? '1' : '0';
            }
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
        
        
    }
    public function get_ex_category($event_id,$parent_c_id='')
    {
        $this->db->select('ec.id as c_id,
                           CASE WHEN ec.category IS NULL THEN "" ELSE category END AS category,
                           CASE WHEN ec.categorie_icon IS NULL THEN "" ELSE categorie_icon END AS categorie_icon',false);
        if(!empty($parent_c_id))
        {
            $this->db->join('exibitor_category_relation er','er.exibitor_category_id = ec.id');
            $this->db->where('er.parent_category_id',$parent_c_id);
        }
        $this->db->where('ec.event_id',$event_id);
        $this->db->where('ec.category_type','0');
        $q = $this->db->get('exhibitor_category ec');
        $res = $q->result_array();
        return $res;
    }
    public function get_ex_parent_category($event_id)
    {
        $this->db->select('id as c_id,
                           CASE WHEN category IS NULL THEN "" ELSE category END AS category,
                           CASE WHEN categorie_icon IS NULL THEN "" ELSE categorie_icon END AS categorie_icon',false);
        $this->db->where('event_id',$event_id);
        $this->db->where('category_type','1');
        $q = $this->db->get('exhibitor_category');
        $res = $q->result_array();
        return $res;
    }
    public function get_keyword_by_category($c_id='',$parent_c_id='')
    {
        $this->db->select('group_concat(ec.categorie_keywords) as categorie_keywords');
        if(!empty($parent_c_id) && empty($c_id))
        {
            $this->db->where('ec.id in (select exibitor_category_id from exibitor_category_relation where parent_category_id='.$parent_c_id.')',NULL,FALSE);
        }   
        else
        {
            $this->db->where('ec.id',$c_id);
        }
        $q = $this->db->get('exhibitor_category ec');
        $res = $q->row_array();
        $res = explode(',',$res['categorie_keywords']);
        return $res;
    }
    public function get_linked_attendee($id)
    {
        $this->db->select('link_user');
        $this->db->where('Id',$id);
        $query = $this->db->get('exibitor');
        $res = $query->row_array();
        $attendee_id = explode(',',$res['link_user']);
        if(!empty($attendee_id))
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                               CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                               CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                               CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
            $this->db->from('user u');
            $this->db->order_by('u.Firstname');
            $this->db->where_in('u.Id',$attendee_id);
            $query = $this->db->get();
            $res = $query->result();  
            $attendees = array();
            for($i=0; $i<count($res); $i++)
            {
                $attendees[$i]['Id']=$res[$i]->Id;
                $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
                $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
                $attendees[$i]['Logo']=$res[$i]->Logo;
            }
        }
        else
        {
            $attenddess = [];     
        }
        return $attendees;
    }
    public function getExhibitorListByEventIdNative_new($Event_id=null,$page_no,$user_id,$where='',$last_type='')
    {
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        $exibitors = [];
        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                            CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                            CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
                            CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                            CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                            CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
                            CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
                            CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
                            CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
                            CASE WHEN e.instagram_url IS NULL THEN "" ELSE e.instagram_url END as instagram_url,
                            CASE WHEN e.youtube_url IS NULL THEN "" ELSE e.youtube_url END as youtube_url,
                            CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
                            CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
                            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                            CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id,et.type_id,et.type_name,et.type_color',FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        $this->db->join('exhibitor_type et','e.et_id=et.type_id','left');
        $this->db->where('e.Event_id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->order_by('et.type_id');
        $this->db->order_by('e.Heading');
        $query = $this->db->get();
        $data_result =  $query->result_array();

        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;
        $total          = count($data_result);
        $total_page     = ceil($total/$limit);
        $data_result    = array_slice($data_result,$start,$limit);

        foreach ($data_result as $key => $value)
        {   
            $tmp[$value['type_name']][] = $value;
        }

        $i = 0; 
        foreach ($tmp as $key => $value)
        {   
            $new[$i]['type'] = $key;
            $new[$i]['bg_color'] = $value[0]['type_color'];
            $new[$i]['data'] = $value;
            $i++;
        }
        $exibitors =$new;        

        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['instagram_url']=$value['instagram_url'];
                $exibitors[$key]['data'][$key1]['youtube_url']=$value['youtube_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','3')->where('module_id',$value['exhibitor_page_id'])->get()->num_rows();
                $exibitors[$key]['data'][$key1]['is_favorites']=($count) ? '1' : '0';
            }
        }
        foreach ($exibitors as $key => $value)
        {   
            if($last_type == $value['type'])
            {
                $exibitors[$key]['type'] = '';
                $exibitors[$key]['bg_color'] = '';
            }
        }        
        $data['exhibitors'] = $exibitors;
        $data['total'] = $total_page;
        return $data; 
    }
    public function getAllMeetingRequestNew($event_id,$user_id)
    {
        $data = $this->db->select('Id')->from('exibitor')->where('user_id',$user_id)->where('event_id',$event_id)->get()->row_array();
        $res1 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  DATE_FORMAT(eam.date,"%d/%m/%Y") as date,
                                  eam.time,
                                  CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,                               
                                  eam.Id as request_id,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN "" ELSE eam.exhibiotor_id END AS exhibiotor_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location END as location,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.attendee_id')
                        ->where('eam.exhibiotor_id',$data['Id'])
                        ->where('eam.event_id',$event_id)
                        // ->where('eam.status != ','2')
                        ->order_by('eam.Id','DESC')->get()->result_array();

        $res2 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  DATE_FORMAT(eam.date,"%d/%m/%Y") as date,
                                  eam.time,
                                  eam.status,                               
                                  eam.Id as request_id,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN "" ELSE eam.exhibiotor_id END AS exhibiotor_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location END as location,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.recever_attendee_id')
                        ->where('eam.sender_exhibitor_id',$user_id)
                        ->where('eam.event_id',$event_id)
                        ->order_by('eam.Id','DESC')->get()->result_array();

        $data = array_merge($res1,$res2);
        $data = array_unique($data, SORT_REGULAR);
        return $data;
    }
}
        
?>