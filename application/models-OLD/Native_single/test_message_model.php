<?php
class Test_message_model extends CI_Model{
	function __construct()
	{       
		parent::__construct();

	}
	public function view_private_msg_coversation($user_id,$att_id,$Event_id,$page_no,$limit)
	{
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time,sm.Sender_id,msg_type as is_clickable',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id');
		$this->db->join('user u1','u1.Id=sm.Receiver_id');
		
		if($user_id == $att_id)
		{
			
			$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Sender_id = '".$att_id."')  
					AND (sm.Receiver_id = '".$user_id."' OR  sm.Receiver_id = '".$att_id."')  
					AND (u1.Id = '".$user_id."' OR  u1.Id = '".$att_id."') 
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
		}
		else
		{
			$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Receiver_id = '".$user_id."')  
					AND (sm.Sender_id = '".$att_id."' OR  sm.Receiver_id = '".$att_id."')  
					AND (u1.Id = '".$user_id."' OR  u1.Id = '".$att_id."') 
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
		}
		$this->db->where($where);
		$this->db->group_by('sm.id');
		$this->db->order_by('sm.id','DESC');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		foreach ($res1 as $key=>$value)
		{
			$res1[$key]['message'] = strip_tags($value['message']);
			$res1[$key]['time_stamp']=$this->time_cal($value['Time']);
			if(!empty($value['image']))
			{
			   $res1[$key]['image']=json_decode($value['image']);
			}
			else
			{
				$res1[$key]['image']=array([0]=>"");
			}
		   
			$this->db->select('CONCAT(u.Firstname," ",(CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END)) as user_name,sc.id as comment_id,sc.comment,u.Logo,sc.image,sc.Time,sc.user_id',FALSE);
			$this->db->from('speaker_comment sc');
			$this->db->join('user u','u.Id=sc.user_id');
			$this->db->where('sc.msg_id',$value['message_id']);
			$query_comment = $this->db->get();
			$res_comment = $query_comment->result_array();
			if(!empty($res_comment))
			{
				foreach ($res_comment as $key1 => $value1) 
				{
					$res_comment[$key1]['time_stamp']=$this->time_cal($value1['Time']);
					if(!empty($value1['image']))
					{
						$img_arr=json_decode($value1['image']);
						$res_comment[$key1]['image']=$img_arr[0];
					}
					else
					{
						$res_comment[$key1]['image']="";
					}
				}
				$res1[$key]['comment']=$res_comment;
			}
			$res1[$key]['comment']=$res_comment;
		}

		return $res1;
	}
	public function view_private_section_msg_coversation($Event_id,$user_id,$page_no,$limit)
	{
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time,sm.Sender_id,CASE WHEN sm.org_msg_receiver_id IS NULL THEN "" ELSE (select CONCAT(Firstname," ", CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END) AS desired_receiver from user where Id = sm.org_msg_receiver_id) END as desired_receiver,msg_type as is_clickable',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id');
		$this->db->join('user u1','u1.Id=sm.Receiver_id');
		$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Receiver_id = '".$user_id."')  
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
	   
		$this->db->where($where);
		$this->db->order_by('sm.Time','DESC');
		$this->db->group_by('sm.id');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		foreach ($res1 as $key=>$value)
		{
			
			$res1[$key]['time_stamp']=$this->time_cal($value['Time']);
			if(!empty($value['image']))
			{
				$res1[$key]['image']=json_decode($value['image']);
			}
			else
			{
				$res1[$key]['image']=array([0]=>"");
			}
		   
			$this->db->select('CONCAT(u.Firstname," ",(CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END)) as user_name,sc.id as comment_id,sc.comment,u.Logo,sc.image,sc.Time,sc.user_id',FALSE);
			$this->db->from('speaker_comment sc');
			$this->db->join('user u','u.Id=sc.user_id');
			$this->db->where('sc.msg_id',$value['message_id']);
			$query_comment = $this->db->get();
			$res_comment = $query_comment->result_array();
			if(!empty($res_comment))
			{
				foreach ($res_comment as $key1 => $value1) 
				{
					$res_comment[$key1]['time_stamp']=$this->time_cal($value1['Time']);
					if(!empty($value1['image']))
					{
						$img_arr=json_decode($value1['image']);
						$res_comment[$key1]['image']=$img_arr[0];
					}
					else
					{
						$res_comment[$key1]['image']="";
					}
				}
				$res1[$key]['comment']=$res_comment;
			}
			$res1[$key]['comment']=$res_comment;
		}
		return $res1;
	}
	public function view_public_msg_coversation($Event_id,$page_no,$limit)
	{
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time,CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,sm.Sender_id',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id');
		$this->db->join('user u1','u1.Id=sm.Receiver_id','left');
		$where = "sm.Event_id = '".$Event_id."'
					AND sm.ispublic = '1' AND  sm.Parent = '0' " ;
		$this->db->join('relation_event_user ru','ru.User_id =sm.Sender_id and ru.Event_id='.$Event_id.'');
		$this->db->join('relation_event_user ru1','ru1.User_id =sm.Receiver_id and ru1.Event_id='.$Event_id.'','left');
		
		$this->db->where($where);
		$this->db->order_by('sm.Time','DESC');
		$this->db->group_by('sm.id');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		foreach ($res1 as $key=>$value)
		{ 
			$res1[$key]['time_stamp']=$this->time_cal($value['Time']);
			if(!empty($value['image']))
			{
				$res1[$key]['image']=json_decode($value['image']);
			}
			else
			{
				$res1[$key]['image']=array([0]=>"");
			}
		   
			$this->db->select('CONCAT(u.Firstname," ",(CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END)) as user_name,sc.id as comment_id,sc.comment,u.Logo,sc.image,sc.Time,sc.user_id',FALSE);
			$this->db->from('speaker_comment sc');
			$this->db->join('user u','u.Id=sc.user_id');
			$this->db->where('sc.msg_id',$value['message_id']);
			$query_comment = $this->db->get();
			$res_comment = $query_comment->result_array();
			if(!empty($res_comment))
			{
				foreach ($res_comment as $key1 => $value1) 
				{
					$res_comment[$key1]['time_stamp']=$this->time_cal($value1['Time']);
					if(!empty($value1['image']))
					{
						$img_arr=json_decode($value1['image']);
						$res_comment[$key1]['image']=$img_arr[0];
					}
					else
					{
						$res_comment[$key1]['image']="";
					}
				}
				$res1[$key]['comment']=$res_comment;
			}
			$res1[$key]['comment']=$res_comment;
		}
		return $res1;
	}
	public function delete_message($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('speaker_msg');

		$this->db->where('Parent', $id);
		$this->db->delete('speaker_msg');
		
		$this->db->where('msg_id', $id);
		$this->db->delete('speaker_comment');
	}
	public function saveMessage($data)
	{
		$this->db->insert("speaker_msg",$data);
		$message_id=$this->db->insert_id();
		if($message_id!='')
		{
			$data1 = array(
				  'msg_id' => $message_id,
				  'resiver_id' => $data['Receiver_id'],
				  'sender_id' => $data['Sender_id'],
				  'event_id' => $data['Event_id'],
				  'isread' => '1',
				  'type' => '0',
			);
			$this->db->insert('msg_notifiy', $data1);
			$msg_notifiy_id=$this->db->insert_id();
			if($msg_notifiy_id!='')
			{
				return $message_id;
			}
			else
			{
				return $message_id;
			}
		}
		else
		{
			return 0;
		}

	}
	public function savePublicMessage($data)
	{
		$this->db->insert("speaker_msg",$data);
		$message_id=$this->db->insert_id();
		if($message_id!='')
		{

			$res=$this->notifypublic($data['Event_id'],$data['Sender_id']);
			foreach ($res as $key=>$values)
			{
			   $data1 = array(
					   'msg_id' => $message_id,
					   'resiver_id' => $values,
					   'sender_id' => $data['Sender_id'],
					   'event_id' => $data['Event_id'],
					   'isread' => '1',
					   'type' => '0',
			   );
			   $this->db->insert('msg_notifiy', $data1);
			}
			return $message_id;
		}
		else
		{
			return 0;
		}
	}
	/*public function savePrivateMessage($data)
	{
		$msg_id_arr=array();
		$data['receiver_id']=json_decode($data['receiver_id']);
		foreach ($data['receiver_id'] as $key => $value) 
		{
			$message_data['Message']=$data['message'];
			$message_data['Sender_id']=$data['user_id'];
			$message_data['Event_id']=$data['event_id'];
			$message_data['Receiver_id']=$value;
			$message_data['Parent']=0;
			$message_data['image']="";
			$message_data['Time']=date("Y-m-d H:i:s");
			$message_data['ispublic']='0';
			$message_data['msg_creator_id']=$data['user_id'];
			$this->db->insert("speaker_msg",$message_data);
			$message_id=$this->db->insert_id();
			$msg_id_arr[]=$message_id;
			$data1 = array(
					'msg_id' => $message_id,
					'resiver_id' => $value,
					'sender_id' => $data['user_id'],
					'event_id' => $data['event_id'],
					'isread' => '1',
					'type' => '0'
				);
			$this->db->insert('msg_notifiy', $data1);
			$this->add_msg_hit($data['user_id'],date('Y-m-d'),$value,$data['event_id']);
		}
		return $msg_id_arr;
	}*/
	
	public function savePrivateImageImage($data)
	{
		$this->db->insert("speaker_msg",$data);
	}
	public function getMessageDetails($message_id)
	{
		 $this->db->select('image');
		 $this->db->from('speaker_msg');
		 $this->db->where("Id",$message_id);
		 $query=$this->db->get();
		 $res=$query->result_array();
		 return $res;
	}
	public function updateMessageImage($message_id,$data)
	{
	   $this->db->where("Id",$message_id);
	   $this->db->update("speaker_msg",$data);
	}
	public function add_msg_hit($user_id,$current_date,$receiver_id,$event_id)
	{

		 $this->db->select('*');
		 $this->db->from('users_leader_board');
		 $this->db->where("user_id",$user_id);
		 $this->db->where("date",$current_date);
		 $this->db->where("received_id",$receiver_id);
		 $this->db->where("event_id",$event_id);
		 $query=$this->db->get();
		 $res=$query->result_array();
		 $msg_hit=$res[0]['msg_hit'];
		
		 if($msg_hit=='')
		 {
			$data['msg_hit']=1;
			$data['user_id']=$user_id;
			$data['date']=$current_date;
			$data['event_id']=$event_id;
			$data['received_id']=$receiver_id;
			$this->db->insert("users_leader_board",$data);
		 }
		 else
		 {
			$data['msg_hit']=$msg_hit+1;
			$this->db->where("user_id",$user_id);
			$this->db->where("date",$current_date);
			$this->db->where("received_id",$receiver_id);
			$this->db->where("event_id",$event_id);
			$this->db->update("users_leader_board",$data);
		 }
	}
	public function add_comment_hit($user_id,$current_date,$commenter_id,$event_id)
	{

		 $this->db->select('*');
		 $this->db->from('users_leader_board');
		 $this->db->where("user_id",$user_id);
		 $this->db->where("date",$current_date);
		 $this->db->where("comment_id",$commenter_id);
		 $this->db->where("event_id",$event_id);
		 $query=$this->db->get();
		 $res=$query->result_array();
		 $comment_hit=$res[0]['comment_hit'];
		
		 if($comment_hit=='')
		 {
			$data['comment_hit']=1;
			$data['user_id']=$user_id;
			$data['date']=$current_date;
			$data['comment_id']=$commenter_id;
			$data['event_id']=$event_id;
			$this->db->insert("users_leader_board",$data);
		 }
		 else
		 {
			$data['comment_hit']=$comment_hit+1;
			$this->db->where("user_id",$user_id);
			$this->db->where("date",$current_date);
			$this->db->where("comment_id",$commenter_id);
			$this->db->where("event_id",$event_id);
			$this->db->update("users_leader_board",$data);
		 }
	}
	public function total_pages($user_id,$att_id,$Event_id,$limit)
	{
		$this->db->select('sm.id');
		$this->db->from('speaker_msg sm');
		$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Sender_id = '".$att_id."')  
					AND (sm.Receiver_id = '".$user_id."' OR  sm.Receiver_id = '".$att_id."')  
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
	   
		$this->db->where($where);
		$this->db->group_by('sm.id');
		$rows = $this->db->get()->num_rows();
		$total=ceil($rows/$limit);
		return $total;
	}
	public function public_msg_total_pages($Event_id,$limit)
	{
		$this->db->select('sm.id');
		$this->db->from('speaker_msg sm');
		$where = "sm.Event_id = '".$Event_id."' 
					AND (sm.ispublic = '1' AND  sm.Parent = '0') " ;
	   
		$this->db->where($where);
		$this->db->group_by('sm.id');
		$rows = $this->db->get()->num_rows();
		$total=ceil($rows/$limit);
		return $total;
	}
	public function private_msg_total_pages($user_id,$Event_id,$limit)
	{
		$this->db->select('sm.id');
		$this->db->from('speaker_msg sm');
		$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Receiver_id = '".$att_id."')  
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
	   
		$this->db->where($where);
		$this->db->group_by('sm.id');
		$rows = $this->db->get()->num_rows();
		$total=ceil($rows/$limit);
		return $total;
	}
	
	public function make_comment($comment,$event_id)
	{
		$this->db->insert("speaker_comment",$comment);
		$comment_id=$this->db->insert_id();
		$current_date=date('Y/m/d');
		$this->add_comment_hit($comment['user_id'],$current_date,$comment_id,$event_id);
		return true;
	}
	public function delete_comment($comment_id)
	{
		$this->db->where('id', $comment_id);
		$this->db->delete('speaker_comment');
	}
	public function time_cal($ago_time)
	{
			$cur_time = time();
			$time_ago=strtotime($ago_time);
			$time_elapsed = $cur_time - $time_ago;
			$seconds = $time_elapsed;
			$minutes = round($time_elapsed / 60);
			$hours = round($time_elapsed / 3600);
			$days = round($time_elapsed / 86400);
			$weeks = round($time_elapsed / 604800);
			$months = round($time_elapsed / 2600640);
			$years = round($time_elapsed / 31207680);
			$time_status;
			if ($seconds <= 60)
			{
				$time_status=$seconds." seconds ago";
			}
			 //Minutes
			else if ($minutes <= 60)
			{
				  if ($minutes == 1)
				  {
						$time_status="one minute ago";
				  }
				  else
				  {
						$time_status=$minutes." minutes ago";
				  }
			 }
			 //Hours
			 else if ($hours <= 24)
			 {
				  if ($hours == 1)
				  {
						$time_status="an hour ago";
				  }
				  else
				  {
						$time_status=$hours." hours ago";
				  }
			 }
			 //Days
			 else if ($days <= 7)
			 {
				  if ($days == 1)
				  {
						$time_status="yesterday";
				  }
				  else
				  {
						$time_status=$days." days ago";
				  }
			 }
			 //Weeks
			 else if ($weeks <= 4.3)
			 {
				  if ($weeks == 1)
				  {
						$time_status="a week ago";
				  }
				  else
				  {
						$time_status=$weeks." weeks ago";
				  }
			 }
			 //Months
			 else if ($months <= 12)
			 {
				  if ($months == 1)
				  {
						$time_status= "a month ago";
				  }
				  else
				  {
						$time_status= $months." months ago";
				  }
			 }
			 //Years
			 else
			 {
				  if ($years == 1)
				  {
						$time_status= "one year ago";
				  }
				  else
				  {
						$time_status=$years." years ago";
				  }
			}
			return $time_status;
	}
	public function notifypublic($eventid,$user_id)
	{    
		  
	   
		  $this->db->select('group_concat(ru.User_id) as User_id');
		  $this->db->from('relation_event_user ru');
		  $this->db->where('ru.Event_id', $eventid);
		  $this->db->where('ru.User_id !=',$user_id);
		  $query = $this->db->get();
		  $res1 = $query->result_array();
		  
		  $finalarray=array();
		  if($res1[0]['User_id']!="")
		  {
			   $finalarray=  explode(',', $res1[0]['User_id']);
			   $finalarray=  array_unique($finalarray);
		  }
		  return $finalarray;
	}

	public function getImages($event_id,$message_id)
	{
		$this->db->select('image');
		$this->db->from('speaker_msg');
		$this->db->where('Id',$message_id);
		$this->db->where('Event_id',$event_id);
		$rows = $this->db->get()->row();
		return $rows->image;
	}
	public function getGcmIds($user_id)
	{
		$this->db->select('gcm_id')->from('user');
		$this->db->where_in('Id',$user_id);
		$query = $this->db->get();
		$res1 = $query->result_array();
		return $res1;
	}
	public function getDevice($user_id)
	{
		$this->db->select('device')->from('user');
		$this->db->where_in('Id',$user_id);
		$query = $this->db->get();
		$res1 = $query->result_array();
		return $res1;
	}
	public function getSender($Sender_id)
	{
		$this->db->select('Firstname as name')->from('user');
		$this->db->where('Id',$Sender_id);
		$query = $this->db->get();
		$res1 = $query->row();
		return $res1->name;
	}
   /*public function getNotificationCounter($event_id,$receiver_id)
   {
		$this->db->select('mn.id,sm.Message,mn.issent,sm.ispublic,u.Firstname,u.Lastname,u.Logo,u1.Firstname as Senderfname,u1.Lastname as Senderlname,u1.Logo as Senderlogo,mn.type');
		$this->db->from('msg_notifiy mn');
		$this->db->join('speaker_msg sm','sm.Id=mn.msg_id');
		$this->db->join('user u','u.id=mn.resiver_id','left');
		$this->db->join('user u1','u1.id=mn.sender_id','left');
		$this->db->where('mn.resiver_id', $receiver_id);
		$this->db->where('mn.event_id', $event_id);
		$this->db->where('mn.isread', '1');
		$query = $this->db->get();
		$res = $query->result_array();

		$listdata=array();
		foreach($res as $mkey=>$mvalue)
		{
			 if($mvalue['type']==0)
			 {
					$listdata[$mkey]['id']=$mvalue['id'];
					if(strlen($mvalue['Message'])>25)
					{
						$listdata[$mkey]['Message']=substr($mvalue['Message'], 0,25)."...";
					}
					else
					{
						$listdata[$mkey]['Message']=$mvalue['Message'];
					}
					$listdata[$mkey]['Firstname']=$mvalue['Senderfname'];
					$listdata[$mkey]['Lastname']=$mvalue['Senderlname'];
					$listdata[$mkey]['Ispublic']=$mvalue['ispublic'];
					$listdata[$mkey]['Issent']=$mvalue['issent'];
					if ($mvalue['Senderlogo']!="")
					{
						$listdata[$mkey]['Logo']=base_url().'assets/user_files/'.$mvalue['Senderlogo'];
					}
					else
					{
						$listdata[$mkey]['Logo']=base_url() . "assets/images/anonymous.jpg";
					}
			 }
			 else
			 {
					$listdata[$mkey]['id']=$mvalue['id'];
					$listdata[$mkey]['Message']="replied on your message";
					$listdata[$mkey]['Firstname']=$mvalue['Senderfname'];
					$listdata[$mkey]['Lastname']=$mvalue['Senderlname'];
					$listdata[$mkey]['Ispublic']=$mvalue['ispublic'];
					$listdata[$mkey]['Issent']=$mvalue['issent'];
					if ($mvalue['Senderlogo']!="")
					{
						$listdata[$mkey]['Logo']=base_url().'/assets/user_files/'.$mvalue['Senderlogo'];
					}
					else
					{
						$listdata[$mkey]['Logo']=base_url() . "/assets/images/anonymous.jpg";
					}
			 }
			 
		  $update_allocation = array(
			   'issent' => '0'
		  );
		  $this->db->where('id',$mvalue['id']);
		  $this->db->update('msg_notifiy',$update_allocation);
		  switch ( $mvalue['msg_type']) {
				case '1':
					$listdata[$mkey]['Ispublic'] = '2';
					break;
				case '2':
					$listdata[$mkey]['Ispublic'] = '3';
					break;
				case '3':
					$listdata[$mkey]['Ispublic'] = '4';
					break;
				case '4':
					$listdata[$mkey]['Ispublic'] = '5';
					break;
				
			} 
		}
		return $listdata;
	
   }*/
   public function getNotificationCounter($event_id,$receiver_id)
   {
		$this->db->select('mn.id,sm.Message,mn.issent,sm.ispublic,u.Firstname,u.Lastname,u1.Id as user_id,u.Logo,u1.Firstname as Senderfname,u1.Lastname as Senderlname,u1.Logo as Senderlogo,mn.type,sm.Sender_id,sm.Receiver_id,sm.msg_type,ru.Role_id');
		$this->db->from('msg_notifiy mn');
		$this->db->join('speaker_msg sm','sm.Id=mn.msg_id');
		$this->db->join('user u','u.Id=mn.resiver_id','left');
		$this->db->join('user u1','u1.Id=mn.sender_id','left');
		$this->db->join('relation_event_user ru','ru.User_id = sm.Sender_id AND ru.Event_id = "'.$event_id.'"');
		$this->db->where('mn.resiver_id', $receiver_id);
		$this->db->where('sm.Receiver_id', $receiver_id);
		$this->db->where('mn.Event_id', $event_id);
		$this->db->where('sm.ispublic', '0');
		$this->db->where('sm.Event_id', $event_id);
		$this->db->where('mn.isread', '1');
		$this->db->order_by('sm.Id', 'DESC');
		$query = $this->db->get();
		$res1 = $query->result_array();

		$this->db->select('mn.id,sm.Message,mn.issent,sm.ispublic,u.Firstname,u.Lastname,u.Logo,u1.Id as user_id,u1.Firstname as Senderfname,u1.Lastname as Senderlname,u1.Logo as Senderlogo,mn.type,sm.Sender_id,sm.Receiver_id,sm.msg_type,ru.Role_id');
		$this->db->from('msg_notifiy mn');
		$this->db->join('speaker_msg sm','sm.Id=mn.msg_id');
		$this->db->join('user u','u.id=mn.resiver_id','left');
		$this->db->join('user u1','u1.id=mn.sender_id','left');
		$this->db->join('relation_event_user ru','ru.User_id = sm.Sender_id AND ru.Event_id = "'.$event_id.'"');
		$this->db->where('mn.resiver_id', $receiver_id);
		$this->db->where('mn.Event_id', $event_id);
		$this->db->where('sm.ispublic', '1');
		$this->db->where('sm.Event_id', $event_id);
		$this->db->where('mn.isread', '1');
		$this->db->order_by('sm.Id', 'DESC');
		$query = $this->db->get();
		$res2 = $query->result_array();

		$res = array_merge($res1,$res2);
		
		$listdata=array();
		foreach($res as $mkey=>$mvalue)
		{
			   
		   /* if($mvalue['type']==0)
			{*/     if($mvalue['Role_id'] == '6')
					{
						 $ex = $this->db->select('Heading')->from('exibitor')->where('Event_id',$event_id)->where('user_id',$mvalue['user_id'])->get()->row_array();
						 $ex_name = $ex['Heading'];
					}    
			   
					$listdata[$mkey]['id']=$mvalue['id'];
					$string = $mvalue['Message'];
					if($string != strip_tags($string)) 
					{
						$mvalue['Message'] = strip_tags($mvalue['Message']);
					}
					if(strlen($mvalue['Message'])>25)
					{
						$listdata[$mkey]['Message']=substr($mvalue['Message'], 0,25)."...";
					}
					else
					{
						$listdata[$mkey]['Message']=$mvalue['Message'];
					}

					$listdata[$mkey]['Firstname']=($mvalue['Role_id'] == '6') ? $ex_name: $mvalue['Senderfname'];
					$listdata[$mkey]['Lastname']= ($mvalue['Role_id'] == '6') ? "": $mvalue['Senderlname'];
					$listdata[$mkey]['Ispublic']=$mvalue['ispublic'];
					$listdata[$mkey]['Issent']=$mvalue['issent'];
					$listdata[$mkey]['Sender_id']=$mvalue['Sender_id'];
					$listdata[$mkey]['Receiver_id']=$mvalue['Receiver_id'];
					if ($mvalue['Senderlogo']!="")
					{
						$listdata[$mkey]['Logo']=base_url().'assets/user_files/'.$mvalue['Senderlogo'];
					}
					else
					{
						$listdata[$mkey]['Logo']=base_url() . "assets/images/anonymous.jpg";
					}
		   // }
			/*else
			{
					$listdata[$mkey]['id']=$mvalue['id'];
					$listdata[$mkey]['Message']="replied on your message";
					$listdata[$mkey]['Firstname']=$mvalue['Senderfname'];
					$listdata[$mkey]['Lastname']=$mvalue['Senderlname'];
					$listdata[$mkey]['Ispublic']=$mvalue['ispublic'];
					$listdata[$mkey]['Issent']=$mvalue['issent'];
					$listdata[$mkey]['Sender_id']=$mvalue['Sender_id'];
					$listdata[$mkey]['Receiver_id']=$mvalue['Receiver_id'];
					if ($mvalue['Senderlogo']!="")
					{
						$listdata[$mkey]['Logo']=base_url().'/assets/user_files/'.$mvalue['Senderlogo'];
					}
					else
					{
						$listdata[$mkey]['Logo']=base_url() . "/assets/images/anonymous.jpg";
					}
			}*/
		  $listdata[$mkey]['msg_type']=($mvalue['msg_type']) ? $mvalue['msg_type'] : 0; 
		  $update_allocation = array(
			   'issent' => '0'
		  );
		  $this->db->where('id',$mvalue['id']);
		  $this->db->update('msg_notifiy',$update_allocation);
		  switch ( $mvalue['msg_type']) {
				case '1':
					$listdata[$mkey]['Ispublic'] = '2';
					break;
				case '2':
					$listdata[$mkey]['Ispublic'] = '3';
					break;
				case '3':
					$listdata[$mkey]['Ispublic'] = '4';
					break;
				case '4':
					$listdata[$mkey]['Ispublic'] = '5';
					break;
				
			} 

		}

		return $listdata;
	
   }
   public function updateMessageReadStatus($event_id,$user_id,$message_type)
   {
		$this->db->select("group_concat(Id separator ',') as id",false)->from('speaker_msg');
		 if($message_type == '0')
		{
			$this->db->where('Receiver_id',$user_id);
		}
		$this->db->where('Event_id',$event_id);
		$this->db->where('ispublic',$message_type);
		$query = $this->db->get();
		$res = $query->row_array();
		$ids = explode(',', $res['id']);
		//print_r($ids);exit;
		$this->db->where('resiver_id',(int)$user_id);
		$this->db->where('event_id',(int)$event_id);
	   // $this->db->where('type',"0"); // 0 meand msg 1 means comment
		$this->db->where_in('msg_id',$ids);
		$update['isread'] = "0";
		$this->db->update('msg_notifiy',$update);

		return true;
   } 
  /* public function updateMessageReadStatus($event_id,$user_id,$message_type)
   {
	   
		
		$this->db->where('resiver_id',(int)$user_id);
		$this->db->where('event_id',(int)$event_id);
	  
		$update['isread'] = "0";
		$this->db->update('msg_notifiy',$update);

		return true;
   } */

   public function getNotificationListing($event_id,$user_id)
   {
		$this->db->select('content,title,umn.Id')->from('user_module_notification umn')->join('notification n','n.Id = umn.notification_id');
		$this->db->where("umn.Id not in(select umn_id from deleted_user_notifications where user_id = '".$user_id."')");
		$this->db->where("FIND_IN_SET('".$user_id."',umn.user_id) !=", 0);
		$this->db->where('n.event_id',$event_id)->order_by('umn.id','DESC');
		$result = $this->db->get()->result_array();
		$data = [];
		foreach ($result as $key => $value) {
			$data1['id'] = $value['Id'];
			$data1['title'] = $value['title'];
			$data1['content'] = $value['content'];
			$data[] = $data1;
		}
		return $data;
   }
   public function deleteNotification($data1)
   {
		$this->db->insert('deleted_user_notifications',$data1);
   }
   public function savePrivateMessage($data)
   {
		$msg_id_arr=array();
		$data['receiver_id']=json_decode($data['receiver_id']);
		foreach ($data['receiver_id'] as $key => $value) 
		{
			$moderators = $this->getModerators($value,$data['event_id']);
			if(empty($moderators))
			{
				$msg_id_arr[]= $this->saveSpeaker($data,$value);
			}
			else
			{
				foreach ($moderators as $key => $moderator) {
					$msg_id_arr[]= $this->saveSpeaker($data,$moderator,$value);
				}
			}
		}
		return $msg_id_arr;
	}
	public function saveSpeaker($data,$value,$org_id=NULL)
	{
		$message_data['Message']=$data['message'];
		$message_data['Sender_id']=$data['user_id'];
		$message_data['Event_id']=$data['event_id'];
		$message_data['Receiver_id']=$value;
		$message_data['Parent']=0;
		$message_data['image']="";
		$message_data['Time']=date("Y-m-d H:i:s");
		$message_data['ispublic']='0';
		$message_data['msg_creator_id']=$data['user_id'];
		$message_data['org_msg_receiver_id']=$org_id;
		$this->db->insert("speaker_msg",$message_data);
		$message_id=$this->db->insert_id();
		$data1 = array(
				'msg_id' => $message_id,
				'resiver_id' => $value,
				'sender_id' => $data['user_id'],
				'event_id' => $data['event_id'],
				'isread' => '1',
				'type' => '0'
			);
		$this->db->insert('msg_notifiy', $data1);
		$this->add_msg_hit($data['user_id'],date('Y-m-d'),$value,$data['event_id']);
		return $message_id;
	}
	public function getModerators($user_id,$event_id)
	{
		$result = $this->db->select('GROUP_CONCAT(moderator_id) as moderator',false)->from('moderator_relation')->where('user_id',$user_id)->where('event_id',$event_id)->get()->row_array();
		$data = explode(',', $result['moderator']);
	  
		return ($data[0]!='') ? $data : [];
	   
	}
	public function getModeratorsGcmIds($receiver_id,$event_id)
	{
		return $this->db->select('u.gcm_id,u.Email')->from('user u')->join('moderator_relation mr','mr.moderator_id = u.Id')->where_in('mr.user_id',$receiver_id)->where('event_id',$event_id)->get()->result_array();
	}
	public function getModeratorDevice($receiver_id,$event_id)
	{
		return $this->db->select('u.device')->from('user u')->join('moderator_relation mr','mr.moderator_id = u.Id')->where_in('mr.user_id',$receiver_id)->where('event_id',$event_id)->get()->result_array();
	}
	public function getNotificationTemplate($event_id,$slug)
	{
		return $this->db->select('*')->from('event_app_push_template')->where('event_id',$event_id)->where('Slug',$slug)->get()->row_array();
	}
	
	public function view_private_section_msg_coversation_list($Event_id,$user_id,$page_no,$limit,$sender_id)
	{
		
		$ex = $this->db->select('user_id')->from('exibitor')->where('Id',$sender_id)->where('event_id',$Event_id)->get()->row_array();
		if(!empty($ex))
		$sender_id = $ex['user_id'];
		
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,
						   CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,
						   CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,
						   CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,
						   CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,
						   CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,
						   CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,
						   CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time,sm.Sender_id,
						   CASE WHEN sm.org_msg_receiver_id IS NULL THEN "" ELSE (select CONCAT(Firstname," ", CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END) AS desired_receiver from user where Id = sm.org_msg_receiver_id) END as desired_receiver,msg_type as is_clickable,sm.msg_type as is_clickable',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id  ');
		$this->db->join('user u1','u1.Id=sm.Receiver_id');
		$where = "sm.Event_id = '".$Event_id."' AND ((sm.Sender_id = '".$user_id."' AND sm.msg_type != '3') AND (sm.Sender_id = '".$user_id."' AND sm.msg_type != '2') OR  sm.Receiver_id = '".$user_id."')  AND (sm.Sender_id = '".$sender_id."' OR sm.Receiver_id='".$sender_id."')
					AND sm.ispublic = '0' AND  sm.Parent = '0'" ;
	   
		$this->db->where($where);
		
		$where2 = "(sm.Sender_id = $user_id  AND sm.msg_type != '3') AND ( sm.Sender_id = $user_id AND sm.msg_type != '2')";
		//$this->db->where($where2);
		$this->db->order_by('sm.Time','DESC');
		$this->db->group_by('sm.id');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
	   // echo $this->db->last_query();exit;
		//mark as read
		$this->db->set('isread', '0');
		$this->db->set('issent', '1');
		 $where = "event_id = '".$Event_id."' AND (sender_id = '".$sender_id."' AND  resiver_id = '".$user_id."')  
					AND isread = '1' " ;  
		$this->db->where($where);   
		$this->db->update('msg_notifiy'); 
	   // echo $this->db->last_query();exit;

		foreach ($res1 as $key=>$value)
		{
			
			$res1[$key]['time_stamp']=$this->time_cal($value['Time']);

			if(!empty($value['image']))
			{
				$image=json_decode($value['image']);
				$res1[$key]['image'] = $image[0];
			}
			else
			{
				$res1[$key]['image']="";
			}
		}

		return $res1;
	}
	public function getUserIdByEx($sender_id,$event_id)
	{
		$ex = $this->db->select('user_id')->from('exibitor')->where('Id',$sender_id)->where('event_id',$event_id)->get()->row_array();
		if(!empty($ex))
		$sender_id = $ex['user_id'];
		return $sender_id;
	}
	public function getSenderDetails($sender_id,$event_id)
	{
		$ex = $this->db->select('user_id')->from('exibitor')->where('Id',$sender_id)->where('event_id',$event_id)->get()->row_array();
		if(!empty($ex))
		$sender_id = $ex['user_id'];

	  $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as id,
						   CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sender_name,
						   CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
						   CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
						   CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END as logo,
						   CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as description,
						   CASE WHEN ru.Role_id IS NULL THEN "" ELSE ru.Role_id END as Role_id'
						   ,FALSE);
		$this->db->from('user u');
		$this->db->join('relation_event_user ru','ru.User_id=u.Id');
		$where = "u.Id = '".$sender_id."' AND 
				  ru.User_id = '".$sender_id."' AND 
				  ru.Event_id ='".$event_id."'";
		$this->db->where($where);
		$query2 = $this->db->get();

		$res2 = $query2->result_array();
		$res2[0]['Page_id'] = "";

		if($res2[0]['Role_id'] == "6")
		{
		$this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as id,
						   CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sender_name,
						   CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
						   CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
						   CASE WHEN ex.company_logo IS NULL THEN "" ELSE ex.company_logo END as logo,
						   CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as description,
						   CASE WHEN ru.Role_id IS NULL THEN "" ELSE ru.Role_id END as Role_id,
						   CASE WHEN ex.Id IS NULL THEN "" ELSE ex.ID END as Page_id'
						   ,FALSE);
		$this->db->from('user u');
		$this->db->join('relation_event_user ru','ru.User_id=u.Id');
		$this->db->join('exibitor ex','ex.Organisor_id = u.Organisor_id');
		$where = "u.Id = '".$sender_id."' AND 
				  ru.User_id = '".$sender_id."' AND 
				  ru.Event_id ='".$event_id."' AND
				  ex.user_id = '".$sender_id."' AND
				  ex.Event_id = '".$event_id."' AND
				  ex.Organisor_id = u.Organisor_id";
		$this->db->where($where);
		$query2 = $this->db->get();
		unset($res2);
		$res2 = $query2->result_array();
		$company_logo = json_decode($res2[0]['logo']);
		$res2[0]['logo'] = $company_logo[0];
		}

		if($res2[0]['Role_id'] == "4")
		{
			unset($res2[0]['Role_id']);
			$res2[0]['module_type'] = "2";
		}
		if($res2[0]['Role_id'] == "6")
		{
			unset($res2[0]['Role_id']);
			$res2[0]['module_type'] = "3";
		}
		if($res2[0]['Role_id'] == "7")
		{
			unset($res2[0]['Role_id']);
			$res2[0]['module_type'] = "7";
		}
		
		return $res2;
	}

	public function private_msg_total_pages_list($sender_id,$user_id,$Event_id,$limit)
	{
		$this->db->select('sm.id');
		$this->db->from('speaker_msg sm');
		$where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$sender_id."' AND sm.Receiver_id = '".$user_id."')  
					AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
	   
		$this->db->where($where);
		$this->db->group_by('sm.id');
		$rows = $this->db->get()->num_rows();
		$total=ceil($rows/$limit);
		return $total;
	}
	public function getExhibitorListByEventId($event_id)
	{
		$this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
						  CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
						  CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,
						  CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
						  CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
						  CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,
						  CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,
						  CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,
						  CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,
						  CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,
						  CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
						  CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
						  CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as Id',FALSE);
		$this->db->from('exibitor e');
		$this->db->where('e.Event_id',$event_id);
		$this->db->order_by('e.Heading');
		$query = $this->db->get();
		$data =  $query->result_array(); 
		$data1 = array();
		foreach ($data as $key => $value) 
		{
			$data1[$key]['Heading'] = ($value['Heading']) ? $value['Heading'] : '';
			$data1[$key]['Company_name'] = ($value['Company_name']) ? $value['Company_name'] : '';
			$data1[$key]['Firstname'] = "";
			$data1[$key]['Lastname'] = "";
			$data1[$key]['Title'] = "";
			$data1[$key]['Email'] = "";
			$company_logo = ($value['company_logo']) ? json_decode($value['company_logo']): '';
			$data1[$key]['Logo'] = $company_logo[0];
			$data1[$key]['Id'] = $value['Id'];
			$data1[$key]['exhibitor_page_id'] = $value['exhibitor_page_id'];
		}
		return $data1;
	}
	public function view_private_unread_message_list($Event_id,$user_id,$page_no,$limit)
	{
		$this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,
			u.Id, CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo, sm.Sender_id,sm.Receiver_id ,COUNT(DISTINCT(mn.id)) as unread_count',FALSE);
		
		$this->db->from('speaker_msg sm');
		
		$this->db->join('user u','u.Id=sm.Sender_id  OR u.Id = sm.Receiver_id');
		$this->db->join('relation_event_user ru','ru.User_id =u.Id');
		$this->db->join('msg_notifiy mn','mn.msg_id=sm.Id AND isread = "1" AND mn.resiver_id = "'.$user_id.'" ','left');
		$this->db->where("sm.Event_id",$Event_id);
		$this->db->where("sm.ispublic","0");
		$this->db->where("u.Id != ",$user_id);
		$where = "(sm.Receiver_id = '".$user_id."' 
					OR 
				  sm.Sender_id = '".$user_id."'  AND sm.msg_type != '3' AND sm.msg_type != '2' 
				  )";
	   
		$this->db->where($where);
		/*$where2 = "( sm.Sender_id = $user_id AND sm.msg_type != '3') AND ( sm.Sender_id = $user_id AND sm.msg_type != '2')";
		$this->db->where($where2);*/
		$this->db->group_by('u.Id');
		$this->db->order_by('unread_count','DESC');
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		
		foreach ($res1 as $key => $value)
		{
			$res1[$key]['unread_count'] = ($value['unread_count'] == '0') ? "" : $value['unread_count'];
			$res1[$key]['Sender_id'] = ($value['Sender_id'] == $user_id) ? $value['Receiver_id'] : $value['Sender_id'];
		   

		}
		
		return $res1;
	}

	public function savePrivateMessageImage($data)
	{
		$msg_id_arr=array();
		$data['receiver_id']=json_decode($data['receiver_id']);
		foreach ($data['receiver_id'] as $key => $value) 
		{
			$moderators = $this->getModerators($value,$data['event_id']);
			if(empty($moderators))
			{
				$msg_id_arr[]= $this->saveSpeaker($data,$value);
			}
			else
			{
				foreach ($moderators as $key => $moderator) {
					$msg_id_arr[]= $this->saveSpeaker($data,$moderator,$value);
				}
			}
		}
		return $msg_id_arr;
	}
	public function savePrivateImageImageOnly($data)
	{
	   $this->db->insert("speaker_msg",$data);
	   return true;
	}
	public function savePrivateMessageText($data)
	{
			$msg_id_arr=array();
		
			$moderators = $this->getModerators($data['receiver_id'],$data['event_id']);

			if(empty($moderators))
			{
				$msg_id_arr[]= $this->saveSpeaker($data,$data['receiver_id']);
			}
			else
			{
				foreach ($moderators as $key => $moderator) {
					$msg_id_arr[]= $this->saveSpeaker($data,$moderator,$data['receiver_id']);
				}
			}
	  
		return $msg_id_arr;
	}



	public function delete_private_message($message_id)
	{
		$this->db->where('Id', $message_id);
		$this->db->delete('speaker_msg');
		
		$this->db->where('msg_id', $message_id);
		$this->db->delete('msg_notifiy');
		return true;
	}

	public function view_exibitor_private_unread_message_list($Event_id,$user_id,$page_no,$limit,$sender_id)
	{
		//echo "2112"; exit();
		$this->db->select('count(mn.id) as unread_count,',FALSE);
		$this->db->from('msg_notifiy mn');
		$where = "mn.event_id = '".$Event_id."' AND mn.resiver_id = '".$user_id."' AND mn.Sender_id = '".$sender_id."' AND isread = '1'" ;
		$this->db->where($where);
		
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		if(empty($res1))
		{
			$res1[0]['unread_count'] = "0";
		} 
		return $res1;
	}
	public function get_all_contacts($Event_id,$user_id,$page_no,$limit)
	{
		$this->db->select('block');
		$this->db->where('block_by',$user_id);
		$this->db->where('event_id',$Event_id);
		$query = $this->db->get('block_user');
		$res = $query->result_array();
		foreach ($res as $key => $value) 
		{
			$block[] = $value['block'];
		}
		
		$this->db->select('u.Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
						   CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
						   CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
						   CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
						   CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as logo,',FALSE);
		
		$this->db->from('user u');
		$this->db->join('relation_event_user ru','ru.User_id =u.Id');
		$this->db->where("ru.Event_id",$Event_id);
		$role = ['4','6','7'];
		$this->db->where_in('ru.Role_id',$role);
		$this->db->where("u.Id != ",$user_id);
		// $this->db->where("u.Firstname != NULL and u.Lastname != NULL");
		$this->db->order_by('u.Firstname','ASC');
		$query1 = $this->db->get();
		$res1 = $query1->result_array();

		foreach ($res1 as $key => $value)
		{	
            $block_all = $this->test_message_model->is_block_all($value['Id'],$Event_id);
            if($block_all == 1)
            {
            	unset($res1[$key]);
            }
            else
            {
				if(in_array($value['Id'], $block))
				{
					$res1[$key]['is_block'] = 1;
				}
				else
				{
					$res1[$key]['is_block'] = 0;
				}
            }
		}
		return $res1;       
	}

	public function get_my_messages($Event_id,$user_id,$page_no,$limit)
	{   
		$this->db->select('u.Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
						   CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
						   CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
						   CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
						   CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as logo,
						   sm.Sender_id,sm.Receiver_id ,
						   COUNT(DISTINCT(mn.id)) as unread_count',FALSE);
		
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id  OR u.Id = sm.Receiver_id');
		$this->db->join('relation_event_user ru','ru.User_id =u.Id');
		$this->db->join('msg_notifiy mn','mn.msg_id=sm.Id AND isread = "1" AND mn.resiver_id = "'.$user_id.'" ','left');

		$where = "(sm.sender_delete != $user_id or sm.sender_delete IS NULL)
			and (sm.reciver_delete != $user_id or sm.reciver_delete IS NULL)";
		$this->db->where($where);
		$this->db->where("sm.Event_id",$Event_id);
		$this->db->where("sm.ispublic","0");
		$this->db->where("u.Id != ",$user_id);
		$where = "(sm.Receiver_id = '".$user_id."' OR sm.Sender_id = '".$user_id."'  AND sm.msg_type != '3' AND sm.msg_type != '2')";
	   
		$this->db->where($where);

		$this->db->where('u.Id NOT IN (select block from block_user where block_by='.$user_id.' and event_id='.$Event_id.')');
		
		/*$where2 = "( sm.Sender_id = $user_id AND sm.msg_type != '3') AND ( sm.Sender_id = $user_id AND sm.msg_type != '2')";
		$this->db->where($where2);*/
		

		
		$this->db->group_by('u.Id');
		$this->db->order_by('unread_count','DESC');
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
		foreach ($res1 as $key => $value)
		{	
			$res1[$key]['unread_count'] = ($value['unread_count'] == '0') ? "" : $value['unread_count'];
			$res1[$key]['Sender_id'] = ($value['Sender_id'] == $user_id) ? $value['Receiver_id'] : $value['Sender_id'];
    		$res1[$key]['is_group'] = 0;
    		//echo $this->db->last_query();exit;
		}
		
		return $res1;
	}
	public function delete_converstaion($event_id,$user_id,$receiver_id)
	{   
		$this->db->_protect_identifiers=false; 
		$where = "(sm.Receiver_id = '".$user_id."' AND sm.Sender_id = '".$receiver_id."' AND sm.msg_type != '3' AND sm.msg_type != '2' AND sm.event_id = $event_id)";
		
		$this->db->where($where);
		$data = ['sm.reciver_delete' => $user_id];
		$this->db->update('speaker_msg sm', $data);
		//echo $this->db->last_query();exit;
		$where = "(sm.Receiver_id = '".$receiver_id."' AND sm.Sender_id = '".$user_id."' AND sm.msg_type != '3' AND sm.msg_type != '2' AND sm.event_id = $event_id)";
		$this->db->where($where);
		$data = ['sm.sender_delete' => $user_id];
		$this->db->update('speaker_msg sm', $data);
		return true;   
	}
	public function block_user($data)
	{
		$this->db->insert('block_user',$data);
		return true;
	}
	public function unblock_user($data)
	{   
		$this->db->where($data);
		$this->db->delete('block_user');
		return true;
	}
	public function view_private_msg_coversation_list_new($Event_id,$user_id,$page_no,$limit,$sender_id)
	{
		
		$ex = $this->db->select('user_id')->from('exibitor')->where('Id',$sender_id)->where('event_id',$Event_id)->get()->row_array();
		if(!empty($ex))
		$sender_id = $ex['user_id'];
		
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,
						   CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,
						   CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,
						   CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,
						   CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,
						   CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,
						   CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,
						   CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time,sm.Sender_id,
						   CASE WHEN sm.org_msg_receiver_id IS NULL THEN "" ELSE (select CONCAT(Firstname," ", CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END) AS desired_receiver from user where Id = sm.org_msg_receiver_id) END as desired_receiver,msg_type as is_clickable,sm.msg_type as is_clickable',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id  ');
		$this->db->join('user u1','u1.Id=sm.Receiver_id');
		$where = "sm.Event_id = '".$Event_id."' AND ((sm.Sender_id = '".$user_id."' AND sm.msg_type != '3') AND (sm.Sender_id = '".$user_id."' AND sm.msg_type != '2') OR  sm.Receiver_id = '".$user_id."')  AND (sm.Sender_id = '".$sender_id."' OR sm.Receiver_id='".$sender_id."')
					AND sm.ispublic = '0' AND  sm.Parent = '0'" ;
	   
		$this->db->where($where);
		
		$where2 = "(sm.Sender_id = $user_id  AND sm.msg_type != '3') AND ( sm.Sender_id = $user_id AND sm.msg_type != '2')";
		//$this->db->where($where2);
		$where = "(sm.sender_delete != $user_id or sm.sender_delete IS NULL)
			and (sm.reciver_delete != $user_id or sm.reciver_delete IS NULL)";
		$this->db->where($where); 

		$this->db->order_by('sm.Time','DESC');
		$this->db->group_by('sm.id');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->result_array();
	   // echo $this->db->last_query();exit;
		//mark as read
		$this->db->set('isread', '0');
		$this->db->set('issent', '1');
		 $where = "event_id = '".$Event_id."' AND (sender_id = '".$sender_id."' AND  resiver_id = '".$user_id."')  
					AND isread = '1' " ;  
		$this->db->where($where);

		$this->db->update('msg_notifiy'); 
	  
		foreach ($res1 as $key=>$value)
		{
			
			$res1[$key]['time_stamp']=$this->time_cal($value['Time']);

			if(!empty($value['image']))
			{
				$image=json_decode($value['image']);
				$res1[$key]['image'] = $image[0];
			}
			else
			{
				$res1[$key]['image']="";
			}
		}

		return $res1;
	}
	public function create_group($data)
	{   
		$this->db->insert('group',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
   public function get_group_details($id,$event_id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get('group');
		$res = $query->result_array();
		
		$members = explode(',', $res[0]['members_id']);
		//$members =  json_decode($res[0]['members_id']);
		foreach ($members as $key => $value)
		{   
			$tmp = $this->getSenderDetails($value,$event_id);
			$members_detail[] = $tmp[0];
			$count++;
		}
		$admin_detail = $this->getSenderDetails($res[0]['admin_id'],$event_id);
		
		$data['group_id'] = $res[0]['id'];
		$data['group_name'] = $res[0]['name'];
		$data['admin'] = $admin_detail[0];
		$data['members'] = $members_detail;
		$data['total'] = $count+1;
		return $data;
	}
	public function update_group($id,$data)
	{   
		//error_reporting(E_ALL);
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get('group');
		$res = $query->row_array();

		$old_members = explode(',', $res['members_id']);
		$new_members = explode(',', $data['members_id']);

		$removed = array_diff($old_members,$new_members);
		$added = array_diff($new_members,$old_members);

		if(!empty($removed))
		{
			foreach ($removed as $key => $value)
			{   
				$this->db->where('user_id',$value);
				$this->db->where('group_id',$id);
				$this->db->where('left_time',NULL);
				$update['left_time']  = date('Y-m-d H:i:s');
				$this->db->update('group_users',$update);
			}
		}

		if(!empty($added))
		{   
			foreach ($added as $key => $value)
			{   
				$insert['user_id'] =  $value;
				$insert['group_id'] = $id;
				$insert['join_time'] = date('Y-m-d H:i:s'); 
				$this->db->insert('group_users',$insert);
			}
		}

		$this->db->where("id",$id);
		$this->db->update("group",$data);
		return true;
	}

	public function delete_group($id)
	{   
		$data['status'] = "0";
		$this->db->where('id',$id);
		$this->db->update('group',$data);
		return true;
	}
	public function send_group_message($data)
	{   
		$this->db->insert('group_message',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	} 
	public function get_group_message($id,$event_id,$user_id)
	{   
		
		$this->db->select('CASE WHEN gm.id IS NULL THEN "" ELSE gm.id END AS message_id,
						   CASE WHEN gm.sender_id IS NULL THEN "" ELSE gm.sender_id END AS sender_id,
						   CASE WHEN gm.message IS NULL THEN "" ELSE gm.message END AS message,
						   CASE WHEN gm.image IS NULL THEN "" ELSE gm.image END AS image,
						   CASE WHEN gm.time IS NULL THEN "" ELSE gm.time END AS Time',false);
		$this->db->from('group_message gm');
		$this->db->where('gm.group_id',$id);
		$this->db->where('gm.id NOT IN (select message_id from delete_group_message where user_id = '.$user_id.')',NULL,FALSE);
		/*$this->db->where('gm.time >= (select join_time from group_users where user_id = '.$user_id.')',NULL,FALSE);
		$this->db->where('gm.time <= (select left_time from group_users where user_id = '.$user_id.')',NULL,FALSE);*/
		$this->db->order_by('time','DESC');
		$query = $this->db->get();
		$res = $query->result_array();
		//echo $this->db->last_query();
		foreach ($res as $key => $value) 
		{   
			$img = json_decode($res[$key]['image']);
			$res[$key]['image'] = ($img[0]) ? $img[0]: "";
			$res[$key]['time_stamp']=$this->time_cal($value['Time']);
			$user_detail = $this->getSenderDetails($value['sender_id'],$event_id);
			//$res[$key]['sender_detail'] = $user_detail[0];
			$res[$key]['Sendername'] = $user_detail[0]['Sender_name'];
			$res[$key]['Recivername'] = "";
			$res[$key]['is_clickable'] = "";
			$res[$key]['desired_receiver'] = "";
			$res[$key]['Reciverlogo'] = "";
			$res[$key]['Senderlogo'] = $user_detail[0]['logo'];
		}
	   return $res;
	}
	public function delete_group_message($data)
	{   
		$this->db->insert('delete_group_message',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	public function left_group($data)
	{   
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('group_id',$data['group_id']);
		$this->db->where('left_time',NULL);
		$update['left_time']  = date('Y-m-d H:i:s');
		$this->db->update('group_users',$update);

		$this->db->select('*');
		$this->db->where('id',$data['group_id']);
		$query = $this->db->get('group');
		$res = $query->row_array();
		
		$search = preg_replace('/\s+/', '', $data['user_id']);
		$input = $res['members_id'];
		$nums = explode(',', $input);
		$nums = array_diff($nums, array($search));
		$output['members_id'] = implode(',', $nums);

		$this->db->where('id',$data['group_id']);
		$this->db->update('group',$output);
		return  true;
	}
	public function add_join_time($data,$id)
	{
		$data = explode(",", $data);
		foreach ($data as $key => $value)
		{   
			$insert['group_id'] = $id;
			$insert['user_id'] = $value;
			$insert['join_time'] = date('Y-m-d H:i:s');
			$this->db->insert('group_users',$insert);
		}
		return  true;
	}
	public function get_groups($event_id,$user_id)
	{   
		//$this->db->_protect_identifiers=false; 
		$this->db->select('CASE WHEN id IS NULL THEN "" ELSE id END as Id,
						   CASE WHEN name IS NULL THEN "" ELSE name END as Firstname,
						   CASE WHEN admin_id IS NULL THEN "" ELSE admin_id END as admin_id,
						   CASE WHEN members_id IS NULL THEN "" ELSE members_id END as members_id',false);
		//LENGTH(members_id) - LENGTH(REPLACE(members_id,",","")) + 2 as total
		$this->db->where('event_id',$event_id);
		$this->db->where('status','1');
		$this->db->where('(admin_id = '.$user_id.' OR FIND_IN_SET('.$user_id.',members_id) != 0)');
		//$this->db->or_where("FIND_IN_SET('".$user_id."',members_id) !=", 0);
		$query = $this->db->get('group');
		$res = $query->result_array();

		foreach ($res as $key => $value)
		{	
			$res[$key]['Lastname'] = "";
			$res[$key]['Company_name'] = "";
			$res[$key]['Title'] = "";
			$res[$key]['Sender_id'] = "";
			$res[$key]['Receiver_id'] = "";
			$res[$key]['unread_count'] = "";
			$res[$key]['count'] = count(explode(',',$value['members_id']));
			$data =  $this->db->select('
						   CASE WHEN image IS NULL THEN "" ELSE image END as image,
						   CASE WHEN message IS NULL THEN "" ELSE message END as message,
						   CASE WHEN time IS NULL THEN "" ELSE time END as time
						   ',FALSE)
					 ->where('group_id',$value['Id'])
					 ->order_by('time')
					 ->get('group_message')
					 ->row_array();
			$data['time']=$this->time_cal($data['time']);
			$res[$key]['message'] = $data['message'];
			$res[$key]['image'] = $data['image'];
			$res[$key]['time'] = $data['time'];
			$res[$key]['is_group'] = 1;

			$admin_detail = $this->getSenderDetails($value['admin_id'],$event_id);
			$res[$key]['logo'] = $admin_detail[0]['logo'];

		}
		return $res;
	}
	public function get_last_message($Event_id,$user_id,$page_no,$limit,$sender_id)
	{
		
		$ex = $this->db->select('user_id')->from('exibitor')->where('Id',$sender_id)->where('event_id',$Event_id)->get()->row_array();
		if(!empty($ex))
		$sender_id = $ex['user_id'];
		
		$page_no = (!empty($page_no))?$page_no:1;
		$start=($page_no-1)*$limit;
		$this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,
						   CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,
						   CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,
						   CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time
						   ',FALSE);
		$this->db->from('speaker_msg sm');
		$this->db->join('user u','u.Id=sm.Sender_id');
		$this->db->join('user u1','u1.Id=sm.Receiver_id');
		$where = "sm.Event_id = '".$Event_id."' AND ((sm.Sender_id = '".$user_id."' AND sm.msg_type != '3') AND (sm.Sender_id = '".$user_id."' AND sm.msg_type != '2') OR  sm.Receiver_id = '".$user_id."')  AND (sm.Sender_id = '".$sender_id."' OR sm.Receiver_id='".$sender_id."' AND sm.Sender_id = '".$user_id."' or sm.Receiver_id='".$user_id."')
					AND sm.ispublic = '0' AND  sm.Parent = '0'" ;
		$this->db->where($where);
		$where = "(sm.sender_delete != $user_id or sm.sender_delete IS NULL)
			and (sm.reciver_delete != $user_id or sm.reciver_delete IS NULL)";
		$this->db->where($where); 

		$this->db->order_by('sm.Time','DESC');
		$this->db->group_by('sm.id');
		$this->db->limit($limit,$start); 
		$query1 = $this->db->get();
		$res1 = $query1->row_array();
			
		$res1['time_stamp']=$this->time_cal($res1['Time']);
		unset($res1['Time']);
		if(!empty($res1['image']))
		{
			$image=json_decode($res1['image']);
			$res1['image'] = $image[0];
		}
		else
		{
			$res1['image']="";
		}
		return $res1;
	}
	public function saveGroupImageOnly($data)
	{
	   $this->db->insert("group_message",$data);
	   return true;
	}
	public function block_all($data)
	{	
		$tmp = $this->db->select('*')->where($data)->get('block_user')->row_array();
		if(!empty($tmp))
		{
			$this->db->where($data);
			$this->db->delete('block_user');
			$msg['msg'] = "All Unblocked";
			$msg['state'] = 0;
		}
		else
		{
			$data['date'] = date('Y-m-d H:i:s');
			$this->db->insert('block_user',$data);
			$msg['msg'] = "All Blocked";
			$msg['state'] = 1;
		}
		return $msg;
	}
	public function is_block_all($user_id,$Event_id)
	{
		$block_all = $this->db->select('*')->where('block_all',$user_id)
		                      ->where('event_id',$Event_id)
						      ->get('block_user')->row_array();
		$res =  ($block_all) ? 1 : 0;
		return $res;
	}
    public function get_group_members($id,$event_id)
	{
		$this->db->select('members_id,name');
		$this->db->where('id',$id);
		$query = $this->db->get('group');
		$res = $query->row_array();
		$res['members_id'] = explode(',',$res['members_id']);
		return $res;
	}
}        
?>