<?php
class Exibitor_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    
    public function check_auth($title, $roleid, $rolename, $eventid)
    {
        if ($rolename == 'Client')
        {
            return 1;
        }
        else
        {
            $this->db->select('id');
            $this->db->from('menu');
            $this->db->where('pagetitle', $title);
            $query = $this->db->get();
            $res = $query->result_array();
            $menuid = $res[0]['id'];
            $this->db->select('Menu_id');
            $this->db->from('role_permission');
            $this->db->where('Role_id', $roleid);
            $this->db->where('Menu_id', $menuid);
            $this->db->where('Event_id', $eventid);
            $query1 = $this->db->get();
            $res1 = $query1->result_array();
            if (count($res1) >= 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    
    public function addinvite_exibitor($emails, $id, $firstname, $lastname, $stand_number)
    {
        $this->db->select('u.Email');
        $this->db->from('user u');
        // $this->db->where('u.role_id', '4');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('ru.Event_id', $id);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        $emails_array = array();
        foreach($res as $key_e => $val_e)
        {
            $emails_array[] = trim($val_e['Email']);
        }
        $dataemails = explode(',', $emails);
        $finalemail = "";
        foreach($dataemails as $key => $val)
        {
            if (!in_array(trim($val) , $emails_array))
            {
                $this->db->select('ai.Id');
                $this->db->from('attendee_invitation ai');
                $this->db->where('ai.Event_id', $id);
                $this->db->where('ai.Emailid', trim($val));
                $this->db->order_by('ai.Id desc');
                $query = $this->db->get();
                $res = $query->result_array();
                if (empty($res))
                {
                    $finalemail.= trim($val) . ",";
                    $array_add['firstname'] = trim($firstname);
                    $array_add['lastname'] = trim($lastname);
                    $array_add['Emailid'] = trim($val);
                    $array_add['Event_id'] = $id;
                    $array_add['link_code'] = $stand_number;
                    $array_add['Status'] = '0';
                    $array_add['role_status'] = '1';
                    $this->db->insert('attendee_invitation', $array_add);
                }
            }
        }
        $finalemail = substr($finalemail, 0, strlen($finalemail) - 1);
        return $finalemail;
    }
    
    public function get_invited_exhibi_bymail($email, $event_id)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_invitation ai');
        $this->db->where('ai.Emailid', $email);
        $this->db->where('ai.Event_id', $event_id);
        $this->db->where('ai.role_status', 1);
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_event_wise_exibitor_page_list($eventid)
    {
        $this->db->select('e.*,u.Email');
        $this->db->from('exibitor e');
        $this->db->join('user u', 'e.user_id=u.id', 'left');
        $this->db->where('e.Event_id', $eventid);
        $qry = $this->db->get();
        $res = $qry->result_array();
        return $res;
    }
    
    public function add_user_as_exibitor($data)
    {
        if (!empty($data['exibitor_array']['company_logo']))
        {
            $company_logo[] = $data['exibitor_array']['company_logo'];
        }
        $array_exibitor['company_logo'] = json_encode($company_logo);
        if (!empty($data['exibitor_array']['Images']))
        {
            foreach($data['exibitor_array']['Images'] as $k => $v)
            {
                $Images[] = $v;
            }
        }
        $array_exibitor['Images'] = json_encode($Images);
        if ($data['exibitor_array']['Organisor_id'] != NULL) $array_exibitor['Organisor_id'] = $data['exibitor_array']['Organisor_id'];
        if ($data['exibitor_array']['Event_id'] != NULL) $array_exibitor['Event_id'] = $data['exibitor_array']['Event_id'];
        if ($data['exibitor_array']['Heading'] != NULL) $array_exibitor['Heading'] = $data['exibitor_array']['Heading'];
        if ($data['exibitor_array']['Description'] != NULL) $array_exibitor['Description'] = $data['exibitor_array']['Description'];
        if ($data['exibitor_array']['Short_desc'] != NULL) $array_exibitor['Short_desc'] = $data['exibitor_array']['Short_desc'];
        if ($data['exibitor_array']['main_contact_name'] != NULL) $array_exibitor['main_contact_name'] = $data['exibitor_array']['main_contact_name'];
        if ($data['exibitor_array']['main_email_address'] != NULL) $array_exibitor['main_email_address'] = $data['exibitor_array']['main_email_address'];
        if ($data['exibitor_array']['stand_number'] != NULL) $array_exibitor['stand_number'] = $data['exibitor_array']['stand_number'];
        if ($data['exibitor_array']['website_url'] != NULL) $array_exibitor['website_url'] = $data['exibitor_array']['website_url'];
        if ($data['exibitor_array']['facebook_url'] != NULL) $array_exibitor['facebook_url'] = $data['exibitor_array']['facebook_url'];
        if ($data['exibitor_array']['twitter_url'] != NULL) $array_exibitor['twitter_url'] = $data['exibitor_array']['twitter_url'];
        if ($data['exibitor_array']['linkedin_url'] != NULL) $array_exibitor['linkedin_url'] = $data['exibitor_array']['linkedin_url'];
        if ($data['exibitor_array']['youtube_url'] != NULL) $array_exibitor['youtube_url'] = $data['exibitor_array']['youtube_url'];
        if ($data['exibitor_array']['instagram_url'] != NULL) $array_exibitor['instagram_url'] = $data['exibitor_array']['instagram_url'];
        if ($data['exibitor_array']['phone_number1'] != NULL) $array_exibitor['phone_number1'] = $data['exibitor_array']['phone_number1'];
        if ($data['exibitor_array']['HQ_phone_number'] != NULL) $array_exibitor['HQ_phone_number'] = $data['exibitor_array']['HQ_phone_number'];
        if ($data['exibitor_array']['email_address'] != NULL) $array_exibitor['email_address'] = $data['exibitor_array']['email_address'];
        if ($data['exibitor_array']['country_id'] != NULL) $array_exibitor['country_id'] = $data['exibitor_array']['country_id'];
        if (array_key_exists('et_id', $data['exibitor_array'])) $array_exibitor['et_id'] = $data['exibitor_array']['et_id'];
        if (array_key_exists('eg_id', $data['exibitor_array'])) $array_exibitor['eg_id'] = $data['exibitor_array']['eg_id'];
        $array_exibitor['Address_map'] = $data['exibitor_array']['Address_map'];
        if (array_key_exists('link_user', $data['exibitor_array']))
        {
            $array_exibitor['link_user'] = $data['exibitor_array']['link_user'];
        }
        if (array_key_exists('sponsored_category', $data['exibitor_array']))
        {
            $array_exibitor['sponsored_category'] = $data['exibitor_array']['sponsored_category'];
        }
        $array_exibitor['user_id'] = $data['exibitor_array']['user_id'];
        $array_exibitor['updated_date'] = date('Y-m-d H:i:s');
        $this->db->insert('exibitor', $array_exibitor);
        return $this->db->insert_id();
    }
    
    public function add_exibitor($data)
    {
        $orid = $this->data['user']->Id;
        $Event_id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id', 6);
        $query = $this->db->get();
        $result = $query->result_array();
        $array_add['Password'] = md5($array_add['Password']);
        $array_add['Email'] = $data['Email_address'];
        $array_add['Active'] = '1';
        $array_add['Organisor_id'] = $orid;
        $this->db->insert('user', $array_add);
        $exibitor_id = $this->db->insert_id();
        $array_relation_add['User_id'] = $this->db->insert_id();
        $array_relation_add['Event_id'] = $Event_id;
        $array_relation_add['Organisor_id'] = $orid;
        $array_relation_add['Role_id'] = $result[0]['Id'];
        $this->db->insert('relation_event_user', $array_relation_add);
        return $exibitor_id;
    }
    
    public function get_exibitor_list($id = null)
    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id', 6);
        if ($id)
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->where('r.Id !=', 3);
        $this->db->where('r.Id !=', 4);
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_users_exibitor_list($id = null, $event_id = null)
    {
        $this->db->select('*');
        $this->db->from('exibitor');
        $this->db->where('user_id', $id);
        if ($event_id != null)
        {
            $this->db->where('Event_id', $event_id);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_exibitor_by_id($id = null, $eid = null)
    {
        $total_permission = $this->Exibitor_model->get_permission_list();
        $this->data['total_permission'] = $total_permission;
        $Organisor_id = $total_permission[0]->Organisor_id;
        $orid = $this->data['user']->Id;
        $this->db->select('e.*');
        $this->db->from('exibitor e');
        $this->db->where('e.Event_id', $id);
        $this->db->where('e.Id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'page_edit') return redirect('Forbidden');
        return $res;
    }
    
    public function update_exibitor($data)
    {
        if (!empty($data['exibitor_array']['company_logo']))
        {
            $company_logo[] = $data['exibitor_array']['company_logo'];
            $array_exibitor['company_logo'] = json_encode($company_logo);
        }
        $images = array();
        if (!empty($data['exibitor_array']['Images']))
        {
            foreach($data['exibitor_array']['Images'] as $k => $v)
            {
                $images[] = $v;
            }
        }
        $array_exibitor['Images'] = json_encode($images);
        if (!empty($data['exibitor_array']['old_images']))
        {
            foreach($data['exibitor_array']['old_images'] as $k => $v)
            {
                $images[] = $v;
            }
        }
        $array_exibitor['Images'] = json_encode($images);
        if ($data['exibitor_array']['Heading'] != NULL) $array_exibitor['Heading'] = $data['exibitor_array']['Heading'];
        $array_exibitor['Short_desc'] = $data['exibitor_array']['Short_desc'];
        $array_exibitor['Description'] = $data['exibitor_array']['Description'];
        $array_exibitor['website_url'] = $data['exibitor_array']['website_url'];
        $array_exibitor['facebook_url'] = $data['exibitor_array']['facebook_url'];
        $array_exibitor['twitter_url'] = $data['exibitor_array']['twitter_url'];
        $array_exibitor['linkedin_url'] = $data['exibitor_array']['linkedin_url'];
        $array_exibitor['youtube_url'] = $data['exibitor_array']['youtube_url'];
        $array_exibitor['instagram_url'] = $data['exibitor_array']['instagram_url'];
        $array_exibitor['phone_number1'] = $data['exibitor_array']['phone_number1'];
        $array_exibitor['email_address'] = $data['exibitor_array']['email_address'];
        $array_exibitor['country_id'] = ($data['exibitor_array']['country_id'] != 0) ? $data['exibitor_array']['country_id'] : NULL;
        if ($data['exibitor_array']['main_contact_name'] != NULL) $array_exibitor['main_contact_name'] = $data['exibitor_array']['main_contact_name'];
        $array_exibitor['main_email_address'] = $data['exibitor_array']['main_email_address'];
        if ($data['exibitor_array']['stand_number'] != NULL) $array_exibitor['stand_number'] = $data['exibitor_array']['stand_number'];
        if ($data['exibitor_array']['HQ_phone_number'] != NULL) $array_exibitor['HQ_phone_number'] = $data['exibitor_array']['HQ_phone_number'];
        if ($data['exibitor_array']['user_id'] != NULL) $array_exibitor['user_id'] = $data['exibitor_array']['user_id'];
        if (array_key_exists('et_id', $data['exibitor_array'])) $array_exibitor['et_id'] = $data['exibitor_array']['et_id'];
        if (array_key_exists('eg_id', $data['exibitor_array'])) $array_exibitor['eg_id'] = $data['exibitor_array']['eg_id'];
        $array_exibitor['Address_map'] = $data['exibitor_array']['Address_map'];
        if (array_key_exists('link_user', $data['exibitor_array']))
        {
            $array_exibitor['link_user'] = $data['exibitor_array']['link_user'];
        }
        if (array_key_exists('sponsored_category', $data['exibitor_array']))
        {
            $array_exibitor['sponsored_category'] = $data['exibitor_array']['sponsored_category'];
        }
        $array_exibitor['updated_date'] = date('Y-m-d H:i:s');
        $this->db->where('Id', $data['exibitor_array']['Id']);
        $this->db->update('exibitor', $array_exibitor);
    }
    
    public function get_permission_list()
    {
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $this->db->select('*');
            $this->db->from('user');
            if ($orid)
            {
                $this->db->where('user.Id', $orid);
            }
            $query = $this->db->get();
            $res = $query->result();
            return $res;
        }
    }
    
    public function delete_exibitor($id, $Event_id = '')
    {
        $this->db->where('Id', $id);
        $this->db->delete('exibitor');
        $str = $this->db->last_query();
        if ($Event_id != '')
        {
            $a_cat = $this->db->select('*')->from('exibitor')->where('Event_id', $Event_id)->order_by('updated_date', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('Id', $a_cat['Id'])->update('exibitor', $update);
        }
    }
    
    public function get_exhib_list_byevent($eventid, $user_id)
    {
        $this->db->select('user.Id,user.Firstname,user.Lastname,user.Email,user.Active,count(ex.user_id) as page_cnt,ex.id as eid');
        $this->db->from('user');
        $this->db->join('exibitor ex', 'ex.user_id = user.Id', 'left');
        $this->db->where('ex.Event_id', $eventid);
        $this->db->where("user.Id", $user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_menu_name($id)
    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('menu_Setting');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_invited_exibitor($id)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_invitation ai');
        $this->db->where('ai.Event_id', $id);
        $this->db->where('ai.role_status', '1');
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_invited_exibitor_data($id, $email)
    {
        $this->db->select('*')->from('attendee_invitation');
        $this->db->where('Event_id', $id);
        $this->db->where('Emailid', $email);
        $this->db->where('role_status', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_exhibitor_type_list($id)
    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id', $id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    
    public function get_exhibitor_group_list($id)
    {
        $this->db->select('*')->from('exhibitor_group');
        $this->db->where('event_id', $id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    
    public function get_edit_exhibitor_type($id, $tid)
    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id', $id);
        $this->db->where('type_id', $tid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    
    public function save_exhibitor_type($typedata)
    {
        $typedata['type_ucode'] = substr(sha1(uniqid()) , 0, 5);
        $this->db->insert('exhibitor_type', $typedata);
        return $this->db->insert_id();
    }
    
    public function save_exhibitor_group($groupdata)
    {
        $groupdata['group_ucode'] = substr(sha1(uniqid()) , 0, 5);
        $this->db->insert('exhibitor_group', $groupdata);
        return $this->db->insert_id();
    }
    
    public function remove_exhibitor_type($id, $tid)
    {
        $this->db->where('event_id', $id);
        $this->db->where('type_id', $tid);
        $this->db->delete('exhibitor_type');
        $st_data['et_id'] = NULL;
        $this->db->where('et_id', $tid);
        $this->db->update('exibitor', $st_data);
    }
    
    public function remove_exhibitor_group($id, $gid)
    {
        $this->db->where('event_id', $id);
        $this->db->where('group_id', $gid);
        $this->db->delete('exhibitor_group');
        $st_data['eg_id'] = NULL;
        $this->db->where('eg_id', $gid);
        $this->db->update('exibitor', $st_data);
    }
    
    public function update_exhibitor_type($tdata, $eid, $tid)
    {
        $this->db->where('event_id', $eid);
        $this->db->where('type_id', $tid);
        $this->db->update('exhibitor_type', $tdata);
    }
    
    public function update_exhibitor_group($gdata, $eid, $gid)
    {
        $this->db->where('event_id', $eid);
        $this->db->where('group_id', $gid);
        $this->db->update('exhibitor_group', $gdata);
    }
    
    public function get_exhibitor_type_id_by_type_code($tcode, $eid)
    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id', $eid);
        $this->db->where('type_ucode', $tcode);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res[0]['type_id'];
    }
    
    public function get_edit_exhibitors_type($id, $tid)
    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id', $id);
        $this->db->where('type_id', $tid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (!$res && $this->uri->segment(2) == 'exhibitor_type_edit') return redirect('Forbidden');
        return $res;
    }
    
    public function get_edit_exhibitors_group($id, $gid)
    {
        $this->db->select('*')->from('exhibitor_group');
        $this->db->where('event_id', $id);
        $this->db->where('group_id', $gid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    
    public function add_user_from_csv($user_data, $eid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Email', $user_data['Email']);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            /*$this->db->where('Id',$res[0]['Id']);
            $this->db->update('user',$user_data);*/
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id', $eid);
            $this->db->where('User_id', $res[0]['Id']);
            $reldata = $this->db->get()->result_array();
            if (count($reldata) > 0)
            {
                $this->db->where('Event_id', $eid);
                $this->db->where('User_id', $res[0]['Id']);
                $this->db->update('relation_event_user', array(
                    'Role_id' => '6'
                ));
            }
            else
            {
                $reldata['User_id'] = $res[0]['Id'];
                $reldata['Event_id'] = $eid;
                $reldata['Organisor_id'] = $user_data['Organisor_id'];
                $reldata['Role_id'] = '6';
                $this->db->insert('relation_event_user', $reldata);
            }
            return $res[0]['Id'];
        }
        else
        {
            $this->db->insert('user', $user_data);
            $user_id = $this->db->insert_id();
            $reldata['User_id'] = $user_id;
            $reldata['Event_id'] = $eid;
            $reldata['Organisor_id'] = $user_data['Organisor_id'];
            $reldata['Role_id'] = '6';
            $this->db->insert('relation_event_user', $reldata);
            return $user_id;
        }
    }
    
    public function add_exhibitor_page_data_from_csv($exdata)
    {
        $this->db->select('*')->from('exibitor');
        $this->db->where('Event_id', $exdata['Event_id']);
        $this->db->where('user_id', $exdata['user_id']);
        $ex_res = $this->db->get()->result_array();
        if (count($ex_res) > 0)
        {
            $user_id = $exdata['user_id'];
            $eid = $exdata['Event_id'];
            unset($exdata['Event_id']);
            unset($exdata['user_id']);
            $this->db->where('Event_id', $eid);
            $this->db->where('user_id', $user_id);
            $this->db->update('exibitor', $exdata);
            return $ex_res[0]['Id'];
        }
        else
        {
            $this->db->insert('exibitor', $exdata);
            return $this->db->insert_id();
        }
    }
    
    public function get_exibitor_type_id_by_type_code($code, $eid)
    {
        $this->db->select('type_id')->from('exhibitor_type');
        $this->db->where('type_ucode', trim($code));
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res[0]['type_id'];
    }
    
    public function save_authorized_user_data($data)
    {
        $res = $this->check_already_exists_email_in_authorized($data['Email'], $data['Event_id']);
        if (count($res) > 0)
        {
            $this->db->where('Email', $data['Email']);
            $this->db->where('Event_id', $data['Event_id']);
            $this->db->update('authorized_user', $data);
        }
        else
        {
            $this->db->insert('authorized_user', $data);
        }
    }
    
    public function get_authorized_user_data($eid)
    {
        $this->db->select('au.*,ac.category_name')->from('authorized_user au');
        $this->db->join('agenda_categories ac', 'au.agenda_id=ac.Id', 'left');
        $this->db->where('au.Event_id', $eid);
        $this->db->where('au.email_type', '1');
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function get_edit_authorized_user_data_by_id($eid, $aid)
    {
        $this->db->select('*')->from('authorized_user');
        $this->db->where('Event_id', $eid);
        $this->db->where('authorized_id', $aid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function update_authorized_user_data_by_id($data, $eid, $aid)
    {
        $res = $this->check_already_exists_email_in_authorized($data['Email'], $eid, $aid);
        if (count($res) > 0)
        {
            return false;
        }
        else
        {
            $this->db->where('Event_id', $eid);
            $this->db->where('authorized_id', $aid);
            $this->db->update('authorized_user', $data);
            return true;
        }
    }
    
    public function check_already_exists_email_in_authorized($email, $eid, $aid = Null)
    {
        $this->db->select('*')->from('authorized_user');
        $this->db->where('Email', $email);
        $this->db->where('Event_id', $eid);
        if (!empty($aid))
        {
            $this->db->where('authorized_id !=', $aid);
        }
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function delete_authorized_email($eid, $aid)
    {
        $this->db->where('Event_id', $eid);
        $this->db->where('authorized_id', $aid);
        $this->db->delete('authorized_user');
    }
    
    public function get_all_event_attendee_user_list($eid)
    {
        $this->db->select('u.*')->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function save_all_temp_exhi_id($all_data)
    {
        $this->db->insert_batch('temp_api_exibitor_id', $all_data);
        $this->db->query('DELETE n1 FROM temp_api_exibitor_id n1, temp_api_exibitor_id n2 WHERE n1.id > n2.id AND n1.exhid = n2.exhid');
    }
    
    public function get_all_temp_exhi_id()
    {
        $this->db->select('id,exhid')->from('temp_api_exibitor_id');
        $this->db->limit(100);
        return $this->db->get()->result_array();
    }
    
    public function add_user_from_api($udata, $eid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Email', $udata['Email']);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id', $eid);
            $this->db->where('User_id', $res[0]['Id']);
            $reldata = $this->db->get()->result_array();
            if (count($reldata) > 0)
            {
                $this->db->where('Event_id', $eid);
                $this->db->where('User_id', $res[0]['Id']);
                $this->db->update('relation_event_user', array(
                    'Role_id' => '6'
                ));
            }
            else
            {
                $reldata['User_id'] = $res[0]['Id'];
                $reldata['Event_id'] = $eid;
                $reldata['Organisor_id'] = $udata['Organisor_id'];
                $reldata['Role_id'] = '6';
                $this->db->insert('relation_event_user', $reldata);
            }
            return $res[0]['Id'];
        }
        else
        {
            $this->db->insert('user', $udata);
            $user_id = $this->db->insert_id();
            $reldata['User_id'] = $user_id;
            $reldata['Event_id'] = $eid;
            $reldata['Organisor_id'] = $udata['Organisor_id'];
            $reldata['Role_id'] = '6';
            $this->db->insert('relation_event_user', $reldata);
            return $user_id;
        }
    }
    
    public function add_exhibitor_type_from_api($etd)
    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('type_name', $etd['type_name']);
        $this->db->where('event_id', $etd['event_id']);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            return $res[0]['type_id'];
        }
        else
        {
            $etd['type_ucode'] = substr(sha1(uniqid()) , 0, 5);
            $this->db->insert('exhibitor_type', $etd);
            return $this->db->insert_id();
        }
    }
    
    public function add_exibitor_from_api($exdata)
    {
        $this->db->select('*')->from('exibitor');
        $this->db->where('Event_id', $exdata['Event_id']);
        $this->db->where('user_id', $exdata['user_id']);
        $ex_res = $this->db->get()->result_array();
        if (count($ex_res) > 0)
        {
            $user_id = $exdata['user_id'];
            $eid = $exdata['Event_id'];
            unset($exdata['Event_id']);
            unset($exdata['user_id']);
            $this->db->where('Event_id', $eid);
            $this->db->where('user_id', $user_id);
            $this->db->update('exibitor', $exdata);
            return $ex_res[0]['Id'];
        }
        else
        {
            $this->db->insert('exibitor', $exdata);
            return $this->db->insert_id();
        }
    }
    
    public function add_sector_relation($exsectorid)
    {
        $this->db->insert_batch('exibitor_sector_relation', $exsectorid);
        $this->db->query('DELETE n1 FROM exibitor_sector_relation n1, exibitor_sector_relation n2 WHERE n1.sector_rel_id > n2.sector_rel_id AND n1.exibitor_id = n2.exibitor_id And n1.sector_id = n2.sector_id');
    }
    
    public function add_category_and_relation($cat_name, $ex_id, $eventid)
    {
        $this->db->select('*')->from('exhibitor_category');
        $this->db->where('category', $cat_name);
        $this->db->where('event_id', $eventid);
        $cat_res = $this->db->get()->result_array();
        if (count($cat_res) > 0)
        {
            $cat_id = $cat_res[0]['id'];
        }
        else
        {
            $this->db->insert('exhibitor_category', array(
                'category' => $cat_name,
                'event_id' => $eventid
            ));
            $cat_id = $this->db->insert_id();
        }
        $cat_rel['category_id'] = $cat_id;
        $cat_rel['exibitor_id'] = $ex_id;
        $this->db->select('*')->from('exibitor_category_relation');
        $this->db->where($cat_rel);
        $rel_res = $this->db->get()->result_array();
        if (count($rel_res) < 1)
        {
            $this->db->insert('exibitor_category_relation', $cat_rel);
        }
    }
    
    public function remove_temp_exibitor_id($exhid)
    {
        $this->db->where('exhid', $exhid);
        $this->db->delete('temp_api_exibitor_id');
    }
    
    public function get_all_exibitor_user_id_by_date($fdate, $tdate, $eid, $rid)
    {
        $this->db->select('e.Id')->from('user u');
        $this->db->join('relation_event_user reu', 'u.Id=reu.User_id', 'right');
        $this->db->join('exibitor e', 'u.Id=e.user_id and e.Event_id=' . $eid, 'right');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', $rid);
        $this->db->where('u.Created_date BETWEEN "' . $fdate . '" AND "' . $tdate . '"', NULL, false);
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function get_exibitor_user_details_by_exibitor_id($exid, $eid)
    {
        $exdata = array();
        $this->db->select('e.Id as exid,e.Heading,e.website_url,e.facebook_url,e.twitter_url,e.linkedin_url,e.company_logo,e.Images,e.stand_number,e.Short_desc,e.Description,e.halal,u.Firstname,u.Lastname,u.Email,u.Password,(case when u.Street IS NULL then "" ELSE u.Street end) as address,u.Password,(case when u.Suburb IS NULL then "" ELSE u.Suburb end) as address2,u.State,u.Country,u.Postcode,(case when u.Mobile IS NULL then "" ELSE u.Mobile end) as phone,(case when u.Phone_business IS NULL then "" ELSE u.Phone_business end) as phone2,concat("' . base_url() . 'assets/user_files/","",u.Logo) as user_logo,u.Created_date,u.Active,(case when u.Salutation IS NULL then "" ELSE u.Salutation end) as Salutation,(case when u.Title IS NULL then "" ELSE u.Title end) as Title', false)->from('exibitor e');
        $this->db->join('user u', 'u.Id=e.user_id', 'left');
        $this->db->where('e.Id', $exid);
        $this->db->where('e.Event_id', $eid);
        $res = $this->db->get()->result_array();
        $exdata = $res[0];
        $this->db->select('es.sector as sector_name,es.img as sector_images')->from('exhibitor_sector es');
        $this->db->join('exibitor_sector_relation esr', 'esr.sector_id=es.Id', 'right');
        $this->db->where('esr.exibitor_id', $exid);
        $eres = $this->db->get()->result_array();
        if (count($eres[0]) > 0)
        {
            $exdata['exhibitor_sector'] = $eres;
        }
        $this->db->select('ec.category as category_name,ec.id as category_id')->from('exhibitor_category ec');
        $this->db->join('exibitor_category_relation ecr', 'ecr.category_id=ec.id', 'right');
        $this->db->where('ecr.exibitor_id', $exid);
        $ecres = $this->db->get()->result_array();
        if (count($ecres[0]) > 0)
        {
            $exdata['exhibitor_category'] = $ecres;
        }
        return $exdata;
    }
    
    public function add_exibitor_categorie($cdata)
    {
        $this->db->insert('exhibitor_category', $cdata);
        return $this->db->insert_id();
    }
    
    public function update_exibitor_categorie($cdata, $cid)
    {
        $this->db->where('id', $cid);
        $this->db->update('exhibitor_category', $cdata);
    }
    
    public function get_exibitor_categories_by_event($eid, $cid = NULL)
    {
        $this->db->select('*')->from('exhibitor_category');
        if (!empty($cid))
        {
            $this->db->where('id', $cid);
        }
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        if (!$res && $this->uri->segment(2) == 'exhibitor_categories_edit') return redirect('Forbidden');
        if (!empty($cid))
        {
            $child_category = $this->db->select('exibitor_category_id')->where('parent_category_id', $cid)->get('exibitor_category_relation')->result_array();
            $res[0]['child_category'] = array_values(array_column($child_category, 'exibitor_category_id'));
        }
        return $res;
    }
    
    public function remove_exhibitor_categories($cid, $event_id = '')
    {
        $this->db->where('id', $cid);
        $this->db->delete('exhibitor_category');
        if ($event_id != '')
        {
            $a_cat = $this->db->select('*')->from('exhibitor_category')->where('event_id', $event_id)->order_by('updated_date', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $a_cat['id'])->update('exhibitor_category', $update);
        }
    }
    
    public function save_add_surveys_permission($uid, $eid, $data_arr)
    {
        $res = $this->get_exibitor_user_premission($eid, $uid);
        if (count($res) > 0)
        {
            $this->db->where('event_id', $eid);
            $this->db->where('user_id', $uid);
            $this->db->update('exibitor_premission', $data_arr);
        }
        else
        {
            $data_arr['event_id'] = $eid;
            $data_arr['user_id'] = $uid;
            $this->db->insert('exibitor_premission', $data_arr);
        }
    }
    
    public function get_exibitor_user_premission($eid, $uid)
    {
        $this->db->select('*')->from('exibitor_premission');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $uid);
        $res = $this->db->get()->result_array();
        return $res;
    }
 
    /*public function insert_temp_exibitor_data($email,$eid,$exhid)
    
    $this->db->select('*')->from('user');
    $this->db->where('Email',$email);
    $res=$this->db->get()->result_array();
    if(count($res)>0)
    {
    $this->db->select('*')->from('exibitor');
    $this->db->where('Event_id',$eid);
    $this->db->where('user_id',$res[0]['Id']);
    $ex_res=$this->db->get()->result_array();
    if(count($ex_res)>0)
    {
    $exid=$ex_res[0]['Id'];
    }
    else
    {
    $exid=$email.",".$exhid;
    }
    }
    else
    {
    $exid=$email.",".$exhid;
    }
    $this->db->insert('temp_exibitor_data',array('exhid'=>$exid));
    }
    */
    public function get_all_exibitor_category_for_assign_parent_category($eid, $pcid = NULL)
    {
        $this->db->select('*')->from('exhibitor_category');
        $this->db->where('event_id', $eid);
        $this->db->where('category_type', '0');
        if (!empty($pcid))
        {
            $this->db->where('id NOT IN (select exibitor_category_id from exibitor_category_relation where parent_category_id!=' . $pcid . ')');
        }
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    public function add_parent_categorie_ralation($rel_data)
    {
        $this->db->insert_batch('exibitor_category_relation', $rel_data);
    }
    
    public function update_category_relation($pcid, $child_category)
    {
        $this->db->where('parent_category_id', $pcid);
        $this->db->delete('exibitor_category_relation');
        foreach($child_category as $value)
        {
            $insert['parent_category_id'] = $pcid;
            $insert['exibitor_category_id'] = $value;
            $this->db->insert('exibitor_category_relation', $insert);
        }
    }
    
    public function getAllCountries()
    {
        return $this->db->select('*')->from('country')->get()->result_array();
    }
    
    public function getAllEventCountries($event_id)
    {
        $data = $this->db->select('country_id')->from('event_countries')->where('event_id', $event_id)->get()->result_array();
        $countries = [];
        foreach($data as $key => $value)
        {
            $countries[] = $value['country_id'];
        }
        return $countries;
    }
    
    public function saveEventCountries($insert_data, $event_id)
    {
        $count = $this->db->select('*')->from('event_countries')->where($insert_data)->get()->num_rows();
        if ($count == 0)
        {
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $this->db->insert('event_countries', $insert_data);
        }
    }
    
    public function getEventCountriesDropDown($event_id)
    {
        $data = $this->db->select('country.id,country.country_name')->from('country')->join('event_countries', 'event_countries.country_id = country.id')->where('event_countries.event_id', $event_id)->get()->result_array();
        return $data;
    }
    
    public function getCountryIdFromName($name)
    {
        $country = $this->db->select('id')->from('country')->where('country_name', $name)->get()->row_array();
        if (empty($country) && !empty($name))
        {
            $insert['country_name'] = $name;
            $this->db->insert('country', $insert);
            return $this->db->insert_id();
        }
        else
        {
            return $country['id'];
        }
    }
    
    public function updateEvent($data, $where)
    {
        $this->db->where($where);
        $this->db->update('event', $data);
    }
    
    public function get_exibitor_by_id_exhi($id = null, $eid = null)
    {
        $total_permission = $this->Exibitor_model->get_permission_list();
        $this->data['total_permission'] = $total_permission;
        $Organisor_id = $total_permission[0]->Organisor_id;
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('e.*');
        $this->db->from('exibitor e');
        $this->db->join('user u', 'u.Id = e.user_id');
        $this->db->where('e.Event_id', $id);
        $this->db->where('u.Id', $orid);
        $this->db->where('e.Id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'edit') return redirect('Forbidden');
        return $res;
    }
    
    public function get_category_group($event_id, $id = NULL)
    {
        $this->db->where('event_id', $event_id);
        if (!empty($id))
        {
            $this->db->where('id', $id);
        }
        $res = $this->db->get('exhi_category_group')->result_array();
        if (!empty($id))
        {
            $child_category = $this->db->select('c_c_id')->where('group_id', $id)->get('exhi_category_group_relation')->result_array();
            $res[0]['child_category'] = array_values(array_column($child_category, 'c_c_id'));
        }
        return $res;
    }
    
    public function add_categorie_group($insert)
    {
        $this->db->insert('exhi_category_group', $insert);
        return $this->db->insert_id();
    }
    
    public function update_exibitor_categorie_group($update, $gid)
    {
        $this->db->where('id', $gid);
        $this->db->update('exhi_category_group', $update);
    }
    
    public function add_categorie_group_ralation($rel_data)
    {
        foreach($rel_data as $key => $value)
        {
            $rel_data[$key]['p_c_id'] = $this->db->where('exibitor_category_id', $value['c_c_id'])->get('exibitor_category_relation')->row_array() ['parent_category_id'];
        }
        $this->db->insert_batch('exhi_category_group_relation', $rel_data);
    }
    
    public function update_category_group_relation($gid, $group_category)
    {
        $event_id = $this->uri->segment(3);
        $this->db->where('group_id', $gid);
        $this->db->delete('exhi_category_group_relation');
        foreach($group_category as $value)
        {
            $insert['group_id'] = $gid;
            $insert['c_c_id'] = $value;
            $insert['p_c_id'] = $this->db->where('exibitor_category_id', $value)->get('exibitor_category_relation')->row_array() ['parent_category_id'];
            $insert['event_id'] = $event_id;
            $this->db->insert('exhi_category_group_relation', $insert);
        }
    }
    
    public function remove_exhibitor_categories_group($cid, $event_id = '')
    {
        $this->db->where('id', $cid);
        $this->db->delete('exhi_category_group');
        if ($event_id != '')
        {
            $a_cat = $this->db->select('*')->from('exhi_category_group')->where('event_id', $event_id)->order_by('updated_date', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $a_cat['id'])->update('exhi_category_group', $update);
        }
    }
    
    public function get_exhibitors_list($id, $sWhere = '', $limit = '', $start = '', $order_by = '')
    {
        $slimit = '';
        if ($limit != 0)
        {
            $slimit = "limit $start , $limit";
        }
        if ($id == '694')
        {
            $sQuery = "SELECT user.Id, user.Firstname, user.Lastname, user.Email, user.Active, count(ex.user_id) as page_cnt, ex.id as eid, ex.Heading,ep.add_surveys,ep.maximum_Reps,count(er.reps_id) as total_resp FROM (exibitor ex)   JOIN user  ON ex.user_id = user.Id AND ex.Event_id=" . $id . " JOIN relation_event_user ON user.Id = relation_event_user.User_id  JOIN role r ON relation_event_user.Role_id = r.Id  Left join exibitor_premission ep on ex.event_id=" . $id . " And ep.user_id=user.Id  LEFT Join exhibitor_representatives er on er.exibitor_user_id=user.Id and er.event_id=" . $id . " $sWhere GROUP BY ex.Id " . $order_by . $slimit;
            $rResult = $this->db->query($sQuery)->result_array();
        }
        else
        {
            $sQuery = "SELECT `user`.`Id`, `user`.`Firstname`, `user`.`Lastname`, `user`.`Email`, `user`.`Active`, count(ex.user_id) as page_cnt, `ex`.`id` as eid, `ex`.`Heading`,ep.add_surveys,ep.maximum_Reps,count(er.reps_id) as total_resp FROM (`user`) JOIN `relation_event_user` ON `user`.`Id` = `relation_event_user`.`User_id` JOIN `role` r ON `relation_event_user`.`Role_id` = `r`.`Id` LEFT JOIN `exibitor` ex ON `ex`.`user_id` = `user`.`Id` AND ex.Event_id=" . $id . " Left join `exibitor_premission` ep on ex.event_id=" . $id . " And ep.user_id=user.Id LEFT Join exhibitor_representatives er on er.exibitor_user_id=user.Id and er.event_id=" . $id . " $sWhere GROUP BY `user`.`Id` $order_by $slimit ";
            return $this->db->query($sQuery)->result_array();
        }
    }
}
?>