<?php

class Gulfood_model extends CI_Model
{

     function __construct()
     {
          $this->db1 = $this->load->database('db1', TRUE);
          parent::__construct();
     }
     
    public function getGulfFoodEvent()
    {
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      /*$this->db->where('e.Event_type','3');
      $this->db->where('e.Status','1');*/
      $this->db->where('e.id',447);
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }

    // #gulfoodsearch
    /*public function gulfoodExhibitorSearch($where,$event_id,$where_keyword,$sector_where,$category_where,$page_no)
    {
        $this->db->select('*')->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $total = $this->db->get()->num_rows();
        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $this->db->order_by('e.Heading');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $res = $query->result();  
        $exibitors = array();
        for($i=0; $i<count($res); $i++)
        {
            $exibitors[$i]['exhibitor_id']=$res[$i]->exhibitor_id;
            $exibitors[$i]['exhibitor_page_id']=$res[$i]->exhibitor_page_id;
            $exibitors[$i]['Heading']=ucfirst($res[$i]->Heading);
            $exibitors[$i]['Heading']=ucfirst($res[$i]->Heading);
            $exibitors[$i]['Short_desc']=$res[$i]->Short_desc;
            
            $images_decode = json_decode($res[$i]->Images);
            $cmpy_logo_decode = json_decode($res[$i]->company_logo);
            if(empty($images_decode[0]))
            {
                $images_decode[0]="";
            }
            if(empty($cmpy_logo_decode[0]))
            {
                $cmpy_logo_decode[0]="";
            }
            $exibitors[$i]['Images']=$images_decode[0];
            
            $exibitors[$i]['stand_number']=$res[$i]->stand_number;
            $exibitors[$i]['company_logo']= $cmpy_logo_decode[0];
            $exibitors[$i]['website_url']=$res[$i]->website_url;
            $exibitors[$i]['facebook_url']=$res[$i]->facebook_url;
            $exibitors[$i]['twitter_url']=$res[$i]->twitter_url;
            $exibitors[$i]['linkedin_url']=$res[$i]->linkedin_url;
            $exibitors[$i]['phone_number1']=$res[$i]->phone_number1;
            $exibitors[$i]['email_address']=$res[$i]->email_address;
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }*/
    // #gulfoodsearch
    public function getExhibitorSectorList()
    {
        return $this->db->select('*')->from('exhibitor_sector')->get()->result_array();
    }
    // #gulfoodsearch
    public function getGulfoodExhibitorCategories()
    {
        return $this->db->select('*')->from('exhibitor_category')->get()->result_array();
    }
    // #gulfoodsearch
    public function getAllMeetingRequest1($event_id,$user_id)
    {
        $data = $this->db->select('e.Id')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('u.token',$user_id)->where('e.event_id',$event_id)->get()->row_array();

        return $this->db->select('u.Firstname,u.Lastname,eam.date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id')->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
    }
    public function gulfoodExhibitorSearch($where,$event_id,$where_keyword,$sector_where,$category_where,$page_no)
    {
        $exibitors = [];
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$event_id)->order_by('type_position')->get()->result_array();

        $this->db->select('*')->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $total = $this->db->get()->num_rows();
        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;
        $select = 'CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id';
        foreach ($types as $key => $value) 
        {
            $this->db->select($select,FALSE);
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id = e.user_id');
            if($sector_where!='')
                $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
            if($category_where!='')
                $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
            $this->db->where('e.Event_id',$event_id);
            if($where_keyword!='')
                $this->db->where($where_keyword);
            if($sector_where!='')
                $this->db->where($sector_where);
            if($category_where!='')
                $this->db->where($category_where);
            if(isset($where))
                $this->db->where($where);
            $this->db->where('e.et_id',$value['type_id']);
            $this->db->order_by('e.Heading');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $result_data = $query->result_array(); 
            if(!empty($result_data))
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['data'] = $result_data;
                $exibitors[] = $results; 
            }
        }   
        $count = count($exibitors['data']);
        if(($count < $limit) && ($count != 0))
        {
            $limit = $limit - $count;
            $start = $start + ($count+1);
        }
        $this->db->select($select,FALSE);
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $this->db->where('e.et_id IS NULL');
        $this->db->order_by('e.Heading');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $result_data = $query->result_array(); 
        if(!empty($result_data))
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['data'] = $result_data;
            $exibitors[] = $results; 
        }
        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
            }
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }

    /*public function getVirtualSupermarkertData($event_id,$page_no)
    {
        $exibitors = [];
        $types = $this->db->select('*')->from('exhibitor_group')->where('event_id',$event_id)->order_by('group_position')->get()->result_array();

        $this->db->select('*')->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        $this->db->where('e.Event_id',$event_id);
        $this->db->where('(e.eg_id IS NOT NULL AND e.eg_id != "0")');
        $total = $this->db->get()->num_rows();

        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;
        $select = 'CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id';
        foreach ($types as $key => $value) 
        {
            $this->db->select($select,FALSE);
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id = e.user_id');
            $this->db->where('e.Event_id',$event_id);
            $this->db->where('e.eg_id',$value['group_id']);
            $this->db->order_by('e.Heading');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $result_data = $query->result_array(); 
            if(!empty($result_data))
            {
                $results['type'] = $value['group_name'];
                $results['bg_color'] = $value['group_color'];
                $results['data'] = $result_data;
                $exibitors[] = $results; 
            }
        }   
        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
            }
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }*/
    public function searchVirtualSuperMarket($where,$event_id,$where_keyword,$sector_where,$category_where,$page_no)
    {
        $exibitors = [];
        $types = $this->db->select('*')->from('exhibitor_group')->where('event_id',$event_id)->order_by('group_position')->get()->result_array();

        $this->db->select('*')->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        if($sector_where!='')
            $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
        if($category_where!='')
            $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
        $this->db->where('e.Event_id',$event_id);
        if($where_keyword!='')
            $this->db->where($where_keyword);
        if($sector_where!='')
            $this->db->where($sector_where);
        if($category_where!='')
            $this->db->where($category_where);
        if(isset($where))
            $this->db->where($where);
        $this->db->where('(e.eg_id IS NOT NULL AND e.eg_id != "0")');

        $total = $this->db->get()->num_rows();
        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;
        $select = 'CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id';
        foreach ($types as $key => $value) 
        {
            $this->db->select($select,FALSE);
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id = e.user_id');
            if($sector_where!='')
                $this->db->join('exibitor_sector_relation esr','esr.exibitor_id = e.Id');
            if($category_where!='')
                $this->db->join('exibitor_category_relation ecr','ecr.exibitor_id = e.Id');
            $this->db->where('e.Event_id',$event_id);
            if($where_keyword!='')
                $this->db->where($where_keyword);
            if($sector_where!='')
                $this->db->where($sector_where);
            if($category_where!='')
                $this->db->where($category_where);
            if(isset($where))
                $this->db->where($where);
            $this->db->where('e.eg_id',$value['group_id']);
            $this->db->order_by('e.Heading');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $result_data = $query->result_array(); 
            if(!empty($result_data))
            {
                $results['type'] = $value['group_name'];
                $results['bg_color'] = $value['group_color'];
                $results['data'] = $result_data;
                $exibitors[] = $results; 
            }
        }   
        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
            }
        }
        $data['exhibitors'] = $exibitors;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
    public function getExhibitorProfile($event_id,$email)
    {
        $this->db->select('e.Id as exhibitor_page_id,e.Id as exhibitor_id,e.event_id');
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id = e.user_id');
        $this->db->where('e.event_id',$event_id);
        $this->db->where('u.Email',$email);
        return $this->db->get()->row_array();
    }
     public function getAttendeeListByEventId($Event_id,$where,$page_no)
    {
       // $Event_id = '447';
        $this->db->select('*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $total = $this->db->get()->num_rows();

        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Firstname');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $res = $query->result();  

        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name']=$res[$i]->Company_name;
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
           
        }
        $data['attendees'] = $attendees;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
    public function getContactAttendeeListByEventId($event_id,$user_id=0,$where,$page_no)
    {
       // $event_id = '447';
        $user_id = ($user_id=='') ? '0' : $user_id;

        $this->db->select('*')->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." AND asc2.approval_status!='2'",'left');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." AND asc1.approval_status='1'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id." OR asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        if($where!='')
            $this->db->where($where);
        $total = $this->db->get()->num_rows();


        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." AND asc2.approval_status!='2'",'left');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." AND asc1.approval_status='1'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id." OR asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        if($where!='')
            $this->db->where($where);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname');
        $this->db->limit($limit, $start);
        $aquery = $this->db->get();
        $ares = $aquery->result();
        
        $attendees = array();
        for($i=0; $i<count($ares); $i++)
        {
            $prev="";
            $fc = strtoupper($ares[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares[$i]->Lastname!=$prev)
            {
                $prev=$ares[$i]->Start_date;
                $data['Id']=$ares[$i]->uid;
                $data['Firstname']=$ares[$i]->Firstname;
                $data['Lastname']=$ares[$i]->Lastname;
                $data['Company_name']=$ares[$i]->Company_name;
                $data['Title']=$ares[$i]->Title;
                $data['Email']=$ares[$i]->Email;
                $data['Logo']=$ares[$i]->Logo;
                array_walk($data,function(&$item){$item=strval($item);});
                $attendees[] = $data;
            }
        }
        $data['attendees'] = $attendees;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
    public function getAttendeeListByEventIdTemp($Event_id,$where,$page_no)
    {
       // $Event_id = '447';
        /*$this->db->select('*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $total = $this->db->get()->num_rows();

        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;*/
        $time1 = round(microtime(true) * 1000);
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Firstname');
        $this->db->limit(10);
        $query = $this->db->get();
        $res = $query->result();  
        $time2 = round(microtime(true) * 1000);
        $time = $time2-$time1;
        echo "get query time = ".$time."<br>";
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name']=$res[$i]->Company_name;
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
           
        }
        $data['attendees'] = $attendees;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
}

?>