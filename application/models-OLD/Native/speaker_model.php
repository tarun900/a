<?php
class Speaker_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function getSpeakersListByEventId($event_id,$user_id)
    {
        $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id','right');
        $this->db->join('user u', 'u.Id = ru.User_id',false);
        $this->db->join('role r','ru.Role_id = r.Id',false);
        $this->db->where('e.Id',$event_id);
        $this->db->where('u.key_people','0');
        $this->db->where('u.is_moderator','0');
        $this->db->where('e.allow_msg_keypeople_to_attendee','1');
        $this->db->where('r.Id =',7);
        if($event['key_people_sort_by'] == '1')
            $this->db->order_by('u.Lastname');
        else
            $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        //$this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();  
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $speakers[$i]['Id']=$res[$i]->Id;
            $speakers[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name']=$res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0 ,60);
            $speakers[$i]['Title']=(strlen($title) >= 60 )  ? $subtitle."..." : $title;
            $speakers[$i]['Speaker_desc']=$res[$i]->Speaker_desc;
            $speakers[$i]['Email']=$res[$i]->Email;
            $speakers[$i]['Logo']=$res[$i]->Logo;
            $speakers[$i]['Heading']="";

            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('user_id',$user_id)->where('module_type','7')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites']=($count) ? '1' : '0';
        }
        return $speakers;
    }
    public function getSpeakerDocument($speaker_id,$event_id,$lang_id=NULL)
    {
        $document_id=$this->db->select('document_id')->from('user')->where('Id',$speaker_id)->get()->row_array();
        $doc_ids=array_filter(explode(",",$document_id['document_id']));
        if(count($doc_ids) > 0)
        {
            $this->db->_protect_identifiers=false;
            $this->db->select('d.id,(CASE WHEN elmc.title IS NULL THEN case when d.title="" then "" ELSE d.title end ELSE elmc.title END) as title,(case when df.document_file="" then "" ELSE concat("'.base_url().'assets/user_documents","/",df.document_file) end) as document_file',false)->from('documents d');
            $this->db->join('event_lang_modules_content elmc','elmc.menu_id="16" and elmc.modules_id=d.id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
            $this->db->join('document_files df','d.id=df.document_id','left');
            $this->db->where('d.Event_id',$event_id);
            $this->db->where_in('d.id',$doc_ids);
            return $this->db->get()->result();
        }
        else
        {
            return [];
        }
    }

    public function getSpeakersListByEvent($event_id,$user_id,$lang_id=NULL)
    {
        $this->db->_protect_identifiers=false;
        $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        (CASE WHEN elmc.content IS NULL THEN CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END ELSE elmc.content END) as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id','right');
        $this->db->join('user u', 'u.Id = ru.User_id',false);
        $this->db->join('role r','ru.Role_id = r.Id',false);
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('e.Id',$event_id);
        $this->db->where('u.key_people','0');
        $this->db->where('u.is_moderator','0');
       // $this->db->where('e.allow_msg_keypeople_to_attendee','1');
        $this->db->where('r.Id =',7);
        if($event['key_people_sort_by'] == '1')
            $this->db->order_by('u.Lastname');
        else
            $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        //$this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();  
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $speakers[$i]['Id']=$res[$i]->Id;
            $speakers[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name']=$res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0 ,60);
            //$speakers[$i]['Title']=(strlen($title) >= 60 )  ? $subtitle."..." : $title;
            $speakers[$i]['Title']=$title;
            $speakers[$i]['Speaker_desc']=$res[$i]->Speaker_desc;
            $speakers[$i]['Email']=$res[$i]->Email;
            $speakers[$i]['Logo']=$res[$i]->Logo;
            $speakers[$i]['Heading']="";

            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('user_id',$user_id)->where('module_type','7')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites']=($count) ? '1' : '0';
        }
        return $speakers;
    }

    public function getSpeakerDetails($speaker_id,$event_id,$user_id,$lang_id=NULL)
    {
        $this->db->_protect_identifiers=false;
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                            CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                            CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                            CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                            (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                            CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                            (CASE WHEN elmc.content IS NULL THEN CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END ELSE elmc.content END) as Speaker_desc,
                            CASE WHEN usl.Facebook_url IS NULL THEN "" ELSE usl.Facebook_url END as Facebook_url,
                            CASE WHEN usl.Twitter_url IS NULL THEN "" ELSE usl.Twitter_url END as Twitter_url,
                            CASE WHEN usl.Instagram_url IS NULL THEN "" ELSE usl.Instagram_url END as Instagram_url,
                            CASE WHEN usl.Youtube_url IS NULL THEN "" ELSE usl.Youtube_url END as Youtube_url,
                            CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url',FALSE);
        $this->db->from('user u');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join('user_social_links usl', 'u.Id = usl.User_id','left');
        $this->db->where('u.Id',$speaker_id);
        $query = $this->db->get();
        $res = $query->result();  
        $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
        $extra=json_decode($extra['extra_column'],true);
        foreach ($extra as $key => $value) 
        {
              $keyword="{".str_replace(' ', '', $key)."}";
              if(stripos(strip_tags($res[0]->Speaker_desc,$keyword)) !== false)
              {
                $res[0]->Speaker_desc=str_ireplace($keyword, $value,$res[0]->Speaker_desc);
              }
        }
        return $res;
    }
    public function getUserId($token)
    {
        $data = $this->db->select('Id')->from('user')->where('token',$token)->get()->row_array();
        return $data['Id'];
    }
    public function getConnectedAgenda($speaker_id,$event_id,$lang_id=NULL)
    {
        $this->db->_protect_identifiers=false;
        $data = $this->db->select('(CASE WHEN elmc.title IS NULL THEN a.Heading ELSE elmc.title END) as Heading,(CASE WHEN elmc.type IS NULL THEN st.type_name ELSE elmc.type END) as Types,a.Id,a.Start_date,a.Start_time,a.custom_location')
                ->from('agenda a')
                ->join('session_types st','st.type_id=a.Types')
                ->join('agenda_category_relation ac','ac.agenda_id = a.Id')
                ->join('event_lang_modules_content elmc','elmc.menu_id="1" and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left')
                ->where("FIND_IN_SET('$speaker_id',a.Speaker_id) != 0",NULL,FALSE)
                ->where('a.Event_id',$event_id)
                ->group_by('a.Id')
                ->get()
                ->result_array();
        foreach ($data as $key => $value) 
        {
            $data[$key]['Start_date'] = date('m/d/Y',strtotime($value['Start_date']));
            $data[$key]['Start_time'] = date('H:i',strtotime($value['Start_time']));
        }
        return $data;
    }
    public function getSpeakerDetailsNew($speaker_id,$event_id,$user_id)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,
                           CASE WHEN usl.Facebook_url IS NULL THEN "" ELSE usl.Facebook_url END as Facebook_url,
                           CASE WHEN usl.Twitter_url IS NULL THEN "" ELSE usl.Twitter_url END as Twitter_url,
                           CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url,
                           CASE WHEN usl.Youtube_url IS NULL THEN "" ELSE usl.Youtube_url END as Youtube_url,
                           CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url',FALSE);
        $this->db->from('user u');
        $this->db->join('user_social_links usl', 'u.Id = usl.User_id','left');
        $this->db->where('u.Id',$speaker_id);
        $query = $this->db->get();
        $res = $query->result();  
        $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
        $extra=json_decode($extra['extra_column'],true);
        foreach ($extra as $key => $value) 
        {
              $keyword="{".str_replace(' ', '', $key)."}";
              if(stripos(strip_tags($res[0]->Speaker_desc,$keyword)) !== false)
              {
                $res[0]->Speaker_desc=str_ireplace($keyword, $value,$res[0]->Speaker_desc);
              }
        }
        return $res;

    }
  }
        
?>
