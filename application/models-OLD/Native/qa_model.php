<?php

class qa_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db->_protect_identifiers=false;
    }
    public function getAllSesssions($event_id,$lang_id='')
    {   
        $this->db->select('q.Id as session_id,
                           CASE WHEN elmc.Title is NULL THEN q.Session_name ELSE elmc.Title END AS Session_name,q.Event_id,
                           CASE WHEN q.session_date IS NULL THEN "" ELSE q.session_date END AS session_date,
                           CASE WHEN q.start_time IS NULL THEN "" ELSE q.start_time END AS start_time,
                           CASE WHEN q.end_time IS NULL THEN "" ELSE q.end_time END AS end_time',false);
        $this->db->where('q.Event_id',$event_id);
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=50 and elmc.modules_id=q.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->order_by('q.session_date', 'asc');
        $this->db->order_by('q.start_time', 'asc');
        $query = $this->db->get('qa_session q');
        $res   = $query->result_array();
        
        
        $date_format = $this->checkEventDateFormat($event_id);
        $format = ($date_format['format_time'] == '0') ? 'h:i A':'H:i';
        $d_format = ($date_format['date_format'] == '0') ? 'd/m/Y' : 'm/d/Y';

        foreach ($res as $key => $value)
        {
            $res[$key]['time'] = date($format,strtotime($value['start_time']))." until ".date($format,strtotime($value['end_time']));
            $res[$key]['session_date'] = date($d_format,strtotime($value['session_date']));
            $res[$key]['total_question'] =  $this->getMessagesCount($value['session_id']);
        }
        
        uasort($res, function($a,$b){
        $c = $a['session_date'] - $b['session_date'];
        $c .= $a['start_time'] - $b['start_time'];
        return $c;
        });
        return array_values($res);
    } 
    public function getSessiondetail($event_id,$session_id,$lang_id='')
    {
        $res = $this->db->select('q.Id as session_id,
                                  CASE WHEN elmc.Title is NULL THEN q.Session_name ELSE elmc.Title END AS Session_name,
                                  q.Event_id,
                                  CASE WHEN elmc.content is NULL THEN q.Session_description ELSE elmc.content END AS Session_description,
                                  q.Moderator_speaker_id,
                                  CASE WHEN q.session_date IS NULL THEN "" ELSE q.session_date END AS session_date,
                                  CASE WHEN q.start_time IS NULL THEN "" ELSE q.start_time END AS start_time,
                                  CASE WHEN q.end_time IS NULL THEN "" ELSE q.end_time END AS end_time',false)
                                  ->from('qa_session q')
                                  ->join('event_lang_modules_content elmc','elmc.menu_id=50 and elmc.modules_id=q.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left')
                                  ->where('q.Event_id',$event_id)
                                  ->where('q.Id',$session_id)->get()->result_array();
        $date_format = $this->checkEventDateFormat($event_id);
        $format="H:i";
        if($date_format['format_time'] == '0')
        {
            $format="h:i A";
        }
        $d_format = "m/d/Y";
        if($date_format['date_format'] == 0)
        {
            $d_format = "d/m/Y";
        }  
        foreach ($res as $key => $value)
        {

        $res[$key]['session_date'] = date($d_format,strtotime($value['session_date']));
        $res[$key]['start_time'] = date($format,strtotime($value['start_time']));
        $res[$key]['end_time'] = date($format,strtotime($value['end_time']));
        $res[$key]['time'] = $res[$key]['session_date']." START: ".date($format,strtotime($value['start_time']))." END: ".date($format,strtotime($value['end_time']));
        }
        return $res;
    }
  
    public function getMessages($session_id,$user_id)
    {

        $data = $this->db->select('qm.Id,qm.Message,
            CONCAT((CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END )," ",(CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END )) as username,
            CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
            CASE WHEN v.votes IS NULL THEN 0 ELSE v.votes END as votes,
            CASE WHEN ru.Role_id IS NULL THEN "" ELSE ru.Role_id END as Role_id,
            CASE WHEN qm.Sender_id IS NULL THEN "" ELSE qm.Sender_id END as user_id',false)
               ->from('speaker_msg qm')
               ->join('user u','u.Id = qm.Sender_id OR qm.Sender_id!=NULL','left')
               ->join('relation_event_user ru','ru.User_id = qm.Sender_id AND ru.Event_id = qm.Event_id OR qm.Sender_id!=NULL','left')
               ->join('qa_message_votes v','v.message_id = qm.Id',left)
               ->where('qm.qasession_id',$session_id)
               ->order_by('v.votes','desc')
               ->group_by('qm.Id')
               ->get()
               ->result_array();
        foreach ($data as $key => $value)
        {   
            $data[$key]['is_anonymous'] = ($value['username'] == ' ') ? 1 : 0;  
            $data[$key]['username'] = ($value['username'] == ' ') ? "Anonymous" : $value['username'];
            $num = $this->db->select("*")
                     ->where('message_id',$value['Id'])
                     ->where("FIND_IN_SET('".$user_id."',users_id) != ",0)
                     ->get('qa_message_votes')->num_rows();

            $num2 = $this->db->select("*")
                     ->where('message_id',$value['Id'])
                     ->where("FIND_IN_SET('".$user_id."',device_id) != ",0)
                     ->get('qa_message_votes')->num_rows();

            $data[$key]['users_vote'] = ($num || $num2) ? 1 : 0 ;
            $data[$key]['is_higgest'] = ($key == 0 ) ? 1 : 0 ;         
        }
        return $data;
    }

    public function saveMessage($data,$user_id)
    {
        //echo "<pre>";print_r($data);exit();
        $moderators = $this->getModerators($user_id,$data['event_id']);
        if(empty($moderators))
        {
            $msg_id_arr[]= $this->saveSpeaker($data,$user_id);
        }
        else
        {
            foreach ($moderators as $key => $moderator) {
            $msg_id_arr[]= $this->saveSpeaker($data,$moderator,$user_id);
            }
        }
        return $msg_id_arr;
    }
    public function saveSpeaker($data,$value,$org_id=NULL)
    {
        $message_data['Message']=$data['message'];
        $message_data['Sender_id']=$data['Sender_id'];
        $message_data['Event_id']=$data['event_id'];
        $message_data['Receiver_id']=$value;
        $message_data['qasession_id']=$data['qasession_id'];
        $message_data['Parent']=0;
        $message_data['image']="";
        $message_data['Time']=date("Y-m-d H:i:s");
        $message_data['ispublic']='0';
        $message_data['msg_creator_id']=$data['Sender_id'];
        $message_data['org_msg_receiver_id']=$org_id;
        $this->db->insert("speaker_msg",$message_data);
        $message_id=$this->db->insert_id();
        $data1 = array(
                'msg_id' => $message_id,
                'resiver_id' => $value,
                'sender_id' => $data['Sender_id'],
                'event_id' => $data['event_id'],
                'isread' => '1',
                'type' => '0'
            );
        $this->db->insert('msg_notifiy', $data1);
        $this->add_msg_hit($data['Sender_id'],date('Y-m-d'),$value,$data['event_id']);
        return $message_id;
    }
    public function getModerators($user_id,$event_id)
    {
        $result = $this->db->select('GROUP_CONCAT(moderator_id) as moderator',false)
                           ->from('moderator_relation')
                           ->where('user_id',$user_id)
                           ->where('event_id',$event_id)->get()->row_array();
        $data = explode(',', $result['moderator']);

        return ($data[0]!='') ? $data : [];
    }

    public function add_msg_hit($user_id,$current_date,$receiver_id,$event_id)
    {

        $this->db->select('*');
        $this->db->from('users_leader_board');
        $this->db->where("user_id",$user_id);
        $this->db->where("date",$current_date);
        $this->db->where("received_id",$receiver_id);
        $this->db->where("event_id",$event_id);
        $query=$this->db->get();
        $res=$query->result_array();
        $msg_hit=$res[0]['msg_hit'];

        if($msg_hit=='')
        {
            $data['msg_hit']=1;
            $data['user_id']=$user_id;
            $data['date']=$current_date;
            $data['event_id']=$event_id;
            $data['received_id']=$receiver_id;
            $this->db->insert("users_leader_board",$data);
        }
        else
        {
            $data['msg_hit']=$msg_hit+1;
            $this->db->where("user_id",$user_id);
            $this->db->where("date",$current_date);
            $this->db->where("received_id",$receiver_id);
            $this->db->where("event_id",$event_id);
            $this->db->update("users_leader_board",$data);
        }
    }

    public function checkEventDateFormat($event_id,$where)
    {
        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        $res=$query->row_array();

        $data1 = $this->db->select('format_time')
                          ->from('fundraising_setting')
                          ->where('Event_id',$event_id)->get()->row_array();
        
        $res['format_time'] = $data1['format_time'];
        return $res;
    }

    public function vote($message_id,$user_id)
    {
        $this->db->select('*');
        $this->db->where('message_id',$message_id);
        $query = $this->db->get('qa_message_votes');
        $res = $query->row_array();
        if(!empty($res))
        {
            $tmp_users = explode(',',$res['users_id']);

            if(in_array($user_id,$tmp_users))
            {   
                $tmp[] =$user_id;
                $tmp_users =  array_diff($tmp_users,$tmp);
                $update['users_id'] = implode(',',$tmp_users);
                $update['votes'] = $res['votes'] - 1;
                $where['message_id'] = $message_id;
                if($update['votes'] == 0)
                {
                    $this->db->delete('qa_message_votes', $where); 
                }
                else
                {
                    $this->db->update('qa_message_votes', $update, $where);
                }
            }
            else
            {
                $tmp_users[] = $user_id;
                $update['users_id'] = implode(',',$tmp_users);
                $update['votes'] = $res['votes'] + 1;
                $where['message_id'] = $message_id;
                $this->db->update('qa_message_votes', $update, $where);
            }       
        }  
        else
        {
            $data['message_id'] = $message_id;
            $data['users_id'] = $user_id;
            $data['votes'] = 1;
            $this->db->insert('qa_message_votes', $data);
        }
        return true;
    }
    public function getMessagesCount($session_id)
    {
        $data = $this->db->select('count(qm.Id) as total_question',false)
               ->from('speaker_msg qm')
               ->where('qm.qasession_id',$session_id)
               ->get()
               ->row_array();
        return $data['total_question'];
    }
    public function getEventTimeZone($event_id)
    {
        $data =  $this->db->select('Event_show_time_zone')
                 ->where('Id',$event_id)
                 ->get('event')
                 ->row_array();

        return $data['Event_show_time_zone'];
    }
    public function voteAnonymous($message_id,$device_id)
    {
        $this->db->select('*');
        $this->db->where('message_id',$message_id);
        $query = $this->db->get('qa_message_votes');
        $res = $query->row_array();
        if(!empty($res))
        {
            $tmp_users = explode(',',$res['device_id']);

            if(in_array($device_id,$tmp_users))
            {   
                $tmp[] =$device_id;
                $tmp_users =  array_diff($tmp_users,$tmp);
                $update['device_id'] = implode(',',$tmp_users);
                $update['votes'] = $res['votes'] - 1;
                $where['message_id'] = $message_id;
                if($update['votes'] == 0)
                {
                    $this->db->delete('qa_message_votes', $where); 
                }
                else
                {
                    $this->db->update('qa_message_votes', $update, $where);
                }
            }
            else
            {
                $tmp_users[] = $device_id;
                $update['device_id'] = implode(',',$tmp_users);
                $update['votes'] = $res['votes'] + 1;
                $where['message_id'] = $message_id;
                $this->db->update('qa_message_votes', $update, $where);
            }       
        }  
        else
        {
            $data['message_id'] = $message_id;
            $data['device_id'] = $device_id;
            $data['votes'] = 1;
            $this->db->insert('qa_message_votes', $data);
        }
        return true;
    }
    public function deleteMessage($id)
    {   
        $where['Id'] = $id;
        $this->db->delete('speaker_msg', $where);
        return true;
    }
    public function getAllUsersGCM_id_by_event_id($event_id)
    {
        return $this->db->select('u.*')->from('relation_event_user reu')->join('user u','u.Id = reu.User_id')->where('reu.Event_id',$event_id)->get()->result_array();
    }
}

?>
    
    