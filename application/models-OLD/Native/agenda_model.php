<?php 
class agenda_model extends CI_Model{
    function __construct()
    {
        
            parent::__construct();

    }
    public function checkAgendaByUserId($agenda_id,$user_id)
    {
          $this->db->select('*');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $user_id);
          /*$this->db->where_in('agenda_id', $agenda_id);*/
          $query = $this->db->get();
          $res = $query->result_array();
          if(!empty($res))
          {
                $agenda_id_str=$res[0]['pending_agenda_id'];
                $arr_pagenda_id=explode(',', $agenda_id_str);

                $agenda_id_str=$res[0]['agenda_id'];
                $arr_agenda_id=explode(',', $agenda_id_str);

                $agenda_id_str=$res[0]['check_in_agenda_id'];
                $arr_cagenda_id=explode(',', $agenda_id_str);

                if(in_array($agenda_id,$arr_pagenda_id))
                {
                    return "pending_1";
                }
                elseif(in_array($agenda_id,$arr_agenda_id))
                {
                   return "save_1";
                }
                else
                {
                    return "0";
                }
          }
          else
          {
            return "0";
          }
          exit();
    }
    public function getMaxPeople($agenda_id,$user_id)
    {
        $this->db->select('Maximum_People');
        $this->db->from('agenda');
        $this->db->where('Id', $agenda_id);
        $tqu=$this->db->get();
        $data=$tqu->row();

        $this->db->select('*')->from('users_agenda');
        $where = "FIND_IN_SET($agenda_id,`agenda_id`) > 0";
        $this->db->where($where);
        $tqu=$this->db->get();
        $total=$tqu->result_array();
        $placeleft=$data->Maximum_People-count($total);
        return $placeleft;
    }
    public function getAgendaByUserId($user_id)
    {
        $this->db->select('*');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
         
    }
    public function getCheckInAgendaByUserId($user_id){
        $this->db->select('*');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
       // $this->db->where('check_in_agenda_id !=', "");
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function updateUserAgenda($user_id,$agenda)
    {
        $this->db->where("user_id",$user_id);
        $this->db->update("users_agenda",$agenda);
    }
    public function addUserAgenda($data)
    {
        $this->db->select('*');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get();
        $res = $query->row();
        if(count($res)){
            $this->db->where("user_id",$data['user_id']);
            $this->db->update("users_agenda",$data);
        }else{
            $this->db->insert("users_agenda",$data);
        }
    }
    public function getAllAgendaByTimeEvent($event_id,$id_arr,$user_id,$lang_id='',$category_id='')
    {
        $checkAgneda = $this->checkAgneda($event_id);
        if(empty($category_id))
        {
            $this->db->select('*')->from('agenda_categories');
            $this->db->where('event_id',$event_id);
            $query=$this->db->get();
            $ares=$query->result_array();
            if(count($ares)==1)
            {
                $category_id = $ares[0]['Id'];
            }
            else
            {
                $agenda_array = $this->getAgendaArray($user_id,$event_id);
                if(empty($agenda_array))
                {
                    foreach ($ares as $key => $value)
                    {
                       if($value['categorie_type'] == '1')
                            $category_id = $value['Id'];
                    }
                }
            }
        }

        $this->db->_protect_identifiers=false;
        $this->db->select('a.*,
                          CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,
                          CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,
                          CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End as tname,
                          (CASE WHEN elmc.type IS NULL Then CASE WHEN st.type_name IS NULL THEN "" ELSE st.type_name End ELSE elmc.type End) as Types,
                          (CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End) as Heading',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=1 and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join('event e', 'e.Id = a.Event_id');
        if(!empty($agenda_array)){
            $this->db->where_in('a.Id',$agenda_array);
        }else
        {
            $this->db->join('agenda_categories ac', 'ac.event_id = a.event_id');
            $this->db->join('agenda_category_relation acr', 'acr.agenda_id = a.Id');
            $this->db->where("ac.Id",$category_id);
            $this->db->where("acr.category_id",$category_id);
        }
       
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);

        $this->db->order_by('tname desc');
        $query = $this->db->get();
        $res = $query->result();
        //echo "<pre>".$this->db->last_query();exit();
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           $date_time = ($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Types']=$res[$i]->Types;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agendas['session_image']= ($res[$i]->session_image) ? $res[$i]->session_image : '';
                $agenda[$date_time][] = $agendas;
            }
        }
        $j= 0;
        function sortByStartTime($a, $b)
        {
            $a = strtotime($a['Start_time']);
            $b = strtotime($b['Start_time']);

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }
        $this->load->model('native/app_login_model');
        $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
        
        foreach ($agenda as $key => $value) {
            $tmp_day = $this->get_lang_day(date("l",strtotime($key)),$default_lang);
            $date_time = ($res[0]->show_session_by_time) ? $key :$tmp_day.date(", jS",strtotime($key));//date(", jS M , Y",strtotime($key));
            $data[$j]['date_time'] = $date_time;
            $data[$j]['date'] = $key;
            usort($value, 'sortByStartTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        function sortByTime($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a < $b) ? -1 : 1;
            } 
        usort($data, 'sortByTime');
        return $data;
    }
    public function get_lang_day($tmp_day,$default_lang)
    {   
        switch ($tmp_day)
        {
            case 'Monday':
                $day = $default_lang['time_and_date__monday'];
                break;
            case 'Tuesday':
                $day = $default_lang['time_and_date__tuesday'];
                break;
            case 'Wednesday':
                $day = $default_lang['time_and_date__wednesday'];
                break;
            case 'Thursday':
                $day = $default_lang['time_and_date__thursday'];
                break;
            case 'Friday':
                $day = $default_lang['time_and_date__friday'];
                break;
            case 'Saturday':
                $day = $default_lang['time_and_date__saturday'];
                break;
            case 'Sunday':
                $day = $default_lang['time_and_date__sunday']; 
                break;
        }
        return $day;
    }
    public function getAllAgendaByTime($event_id,$id_arr,$user_id,$lang_id)
    {   
        $this->db->_protect_identifiers=false;
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End as tname,(CASE WHEN elmc.type IS NULL Then st.type_name ELSE elmc.type End) as Types,(CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End) as Heading',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=1 and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date desc');
       // $this->db->order_by('a.Start_time desc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           $date_time = ($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Types']=$res[$i]->Types;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['session_image']=($res[$i]->session_image) ? $res[$i]->session_image : '';
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agenda[$date_time][] = $agendas;
            }
        }
        $j= 0;
        function sortByStartTime($a, $b)
        {
            $a = strtotime($a['Start_time']);
            $b = strtotime($b['Start_time']);

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }
        $this->load->model('native/app_login_model');
        $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
         
        foreach ($agenda as $key => $value) {
            $tmp_day = $this->get_lang_day(date("l",strtotime($key)),$default_lang);
            $date_time = ($res[0]->show_session_by_time) ? $key :$tmp_day.date(", jS",strtotime($key));
            $data[$j]['date_time'] = $date_time;
            $data[$j]['date'] = $key;
            usort($value, 'sortByStartTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        function sortByTime($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a < $b) ? -1 : 1;
            } 
        usort($data, 'sortByTime');
        return $data;
    }
    public function checkAgneda($event_id){
        $this->db->select('ac.Id,aci.agenda_id');
        $this->db->from('agenda_categories ac');
        $this->db->where('ac.event_id',$event_id);
        $this->db->join('agenda_category_relation aci', 'aci.category_id = ac.Id');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getAgendaArray($user_id,$event_id)
    {   
        $allow_show_all_agenda = $this->db->select('allow_show_all_agenda')->where('Id',$event_id)->get('event')->row_array();
        $allow_show_all_agenda = $allow_show_all_agenda['allow_show_all_agenda'];
        
        if($allow_show_all_agenda == '0')
        {
            $this->db->select('aci.agenda_id');
            $this->db->from('attendee_agenda_relation ar');
            $this->db->where('ar.attendee_id',$user_id);
            $this->db->join('agenda_category_relation aci', 'aci.category_id = ar.agenda_category_id');
            $query = $this->db->get();
            $res = $query->result_array();
            $agenda_array = array();
            foreach ($res as $key => $value) {
                    $agenda_array[] = $value['agenda_id'];
                }
        }
        else
        {
            $res = $this->db->select('GROUP_CONCAT(a.Id) as Id')->where('Event_id',$event_id)
                            ->join('agenda_category_relation ac','ac.agenda_id = a.Id')
                            ->get('agenda a')->row_array();
            $agenda_array = explode(',',$res['Id']);
        }
        return $agenda_array;        
    }
    public function getAgendaById($eid = NULL, $aid = NULL,$user_id = null,$lang_id)
    {   
        $this->db->_protect_identifiers=false;
        $this->db->select('a.*,CASE WHEN doc.document_file IS NULL THEN "" ELSE doc.document_file END as document_file,
                        CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,
                        CASE WHEN m.Map_title IS NULL THEN "" ELSE m.Map_title END as Map_title,
                        CASE WHEN m.Address IS NULL THEN "" ELSE m.Address END as Map_address,
                        CASE WHEN m.Images IS NULL THEN "" ELSE m.Images END as Images,
                        CASE WHEN m.check_dwg_files IS NULL THEN "0" ELSE m.check_dwg_files END as check_dwg_files,
                        CASE WHEN a.Start_date IS NULL THEN "" ELSE a.Start_date END as Start_date,
                        CASE WHEN a.Start_time IS NULL THEN "" ELSE a.Start_time END as Start_time,
                        CASE WHEN a.End_date IS NULL THEN "" ELSE a.End_date END as End_date,
                        CASE WHEN a.End_time IS NULL THEN "" ELSE a.End_time END as End_time,
                        CASE WHEN a.checking_datetime IS NULL THEN "" ELSE a.checking_datetime END as checking_datetime,
                        CASE WHEN a.Maximum_People IS NULL THEN "" ELSE a.Maximum_People END as Maximum_People,
                        (CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End) as Heading,
                        (CASE WHEN elmc.type IS NULL Then st.type_name ELSE elmc.type End) as Types,
                        CASE WHEN a.Agenda_status IS NULL THEN "" ELSE a.Agenda_status END as Agenda_status,
                        CASE WHEN a.Speaker_id IS NULL THEN "" ELSE a.Speaker_id END as Speaker_id,
                        CASE WHEN a.document_id IS NULL THEN "" ELSE a.document_id END as document_id,
                        CASE WHEN a.presentation_id IS NULL THEN "" ELSE a.presentation_id END as presentation_id,
                        CASE WHEN a.Address_map IS NULL THEN "" ELSE a.Address_map END as Address_map,
                        CASE WHEN a.other_types IS NULL THEN "" ELSE a.other_types END as other_types,
                        CASE WHEN a.short_desc IS NULL THEN "" ELSE a.short_desc END as short_desc,
                        (CASE WHEN elmc.content IS NULL Then case when a.description is null then "" else a.description end ELSE elmc.content End) as description,
                        CASE WHEN e.Event_show_time_zone IS NULL THEN "" ELSE e.Event_show_time_zone END as Event_show_time_zone,
                        CASE WHEN e.Event_time_zone IS NULL THEN "" ELSE e.Event_time_zone END as time_zone,
                        CASE WHEN a.Documents IS NULL THEN "" ELSE a.Documents END as Documents, 
                        CASE WHEN a.session_image is NULL THEN "" ELSE a.session_image END as session_image,
                        CASE WHEN a.show_checking_in is NULL THEN "" ELSE a.show_checking_in END as show_checking_in,
                        CASE WHEN a.survey_id IS NULL THEN "" ELSE a.survey_id END as survey_id',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=1 and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$eid,'left');
        $this->db->join('document_files doc', 'doc.document_id = a.document_id','left');
        $this->db->where('e.Id',$eid);
        $this->db->where('a.Id',$aid);
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($res)) 
        {
            $res[0]['show_places_remaining']=$res[0]['show_places_remaining']!='1' ? '0' : '1'; 
            $res[0]['show_places_remaining']=empty($res[0]['Maximum_People']) ? '0' : $res[0]['show_places_remaining']; 
            $show_check_in = $res[0]['show_checking_in'];
            $arr=explode(",",$res[0]['Speaker_id']);

            $this->db->select('Id as user_id,
                               CASE WHEN Logo IS NULL THEN "" ELSE Logo END as Logo,
                               CASE WHEN Firstname IS NULL THEN "" ELSE  Firstname END as Firstname,
                               CASE WHEN Lastname IS NULL THEN "" ELSE  Lastname END as Lastname,
                               is_moderator',false);
            $this->db->from('user');
            $this->db->where_in('Id',$arr);
            $result=$this->db->get()->result_array();
               
           
            $arr=json_decode($res[0]['Map_image']);
            if(empty($arr))
            {
                $res[0]['Map_image']='';
                $res[0]['is_map_visible_btn']='0';
                $res[0]['map_details']=new stdClass;
            }
            else
            {
                $res[0]['Map_image']=$arr;
                $map_details = $this->db->select('mim.coords,m.Id as map_id,m.Map_title,m.Images')
                    ->from('map_image_mapping mim')
                    ->join('map m','m.Id = mim.map_id','left')
                    ->where('mim.user_id',$user_id)
                    ->where('m.Event_id',$eid)
                    ->get()->row_array();
                    if(count($map_details))
                    {
                        $res[0]['is_map_visible_btn']='1';
                        $decode_image=json_decode($map_details['Images']);
                        $map_details['Images']=$decode_image[0];
                        $res[0]['map_details']=$map_details;
                    }
                    else
                    {
                        $res[0]['is_map_visible_btn']='0';
                        $res[0]['map_details']=new stdClass;
                    }
            }
            if(!empty($result))
            {
                foreach ($result as $key => $value) 
                {
                     $value['Speaker_desc'] = ($value['is_moderator']=='0') ? 'Speaker' : 'Moderator';
                     $result[$key]=$value;
                }
                $res[0]['speaker']=$result;
            }
            else
                $res[0]['speaker']=[];
           
            $this->db->select('*')->from('users_agenda');
            $this->db->where('user_id',$user_id);
            $query=$this->db->get();
            $result=$query->result_array();
            $agenda_ids = explode(',', $result[0]['agenda_id']);
            $check_in = explode(',', $result[0]['check_in_agenda_id']);
            $res[0]['is_agenda_saved'] = in_array($aid, $agenda_ids) ? true : false;
            $res[0]['is_checked_in'] = in_array($aid, $check_in) ? true : false;
                    
            $this->db->select('rating')->from('user_session_rating');
            $this->db->where('user_id',$user_id);
            $this->db->where('session_id',$aid);
            $query=$this->db->get();
            $result=$query->row();
            $res[0]['rating'] = ($result->rating) ? $result->rating : ""; 
            $res[0]['Heading'] = ucfirst($res[0]['Heading']); 
            $res[0]['check_max_people'] = empty($res[0]['Maximum_People']) ? '0' : '1';
            
            $mx_peple = $this->getMaxPeople($res[0]['Id'],'');
            $res[0]['Maximum_People'] = ($mx_peple > 0) ? $mx_peple : 0;
            
            $res[0]['qasession_id'] = ($res[0]['qasession_id']) ? $res[0]['qasession_id'] : '';
            $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$eid)->where('Attendee_id',$user_id)->get()->row_array();
            $extra=json_decode($extra['extra_column'],true);
            foreach ($extra as $key => $value) 
            {
              $keyword="{".str_replace(' ', '', $key)."}";
              if(stripos(strip_tags($res[0]['description'],$keyword)) !== false)
              {
                $res[0]['description']=str_ireplace($keyword, $value,$res[0]['description']);
              }
            }
            $presen = $this->db->select('*')->from('presentation')->where('Id',$res[0]['presentation_id'])->get()->row_array();
            if($presen)
            {
                $p['type'] = "presentation";
                $p['value'] = ($presen) ? $presen['Heading'] : '';
                $p['id'] = ($presen) ? $presen['Id'] : '';
                $material[] = $p;
            }
            $survay = $this->db->select('*')->from('survey_category')->where('survey_id',$res[0]['survey_id'])->get()->row_array();
            if($survay)
            {
                $s['type'] = "survey";
                $s['value'] = ($survay) ? $survay['survey_name'] : '';
                $s['id'] = ($survay) ? $survay['survey_id'] : '';
                $material[] = $s;
            }
            $qa = $this->db->select('*')->from('qa_session')->where('Id',$res[0]['qasession_id'])->get()->row_array();
            if($qa)
            {
                $qas['type'] = "qa_session";
                $qas['value'] = ($qa) ? $qa['Session_name'] : '';
                $qas['id'] = ($qa) ? $qa['Id'] : '';
                $material[] = $qas;
            }

            
            $doc_d = $this->db->select('*')->from('documents')->where('Id',$res[0]['document_id'])->get()->row_array();
            if($doc_d)
            {
                $doc['type'] = "document";
                $doc['value'] = $doc_d['title'];
                $doc['id'] = $res[0]['document_file'];
                $material[] = $doc;
            }
            
            
            $res[0]['support_materials'] = ($material)? $material : [];
            $res[0]['show_feedback'] = ($res[0]['show_rating'] == '0' && $res[0]['allow_comments'] == '0') ? '0' : '1';
        }

        //array_walk($res[0],function(&$item){$item=strval($item);});

        return $res;
    }
    public function getTimeFormat($eid)
    {
      $this->db->select('format_time')->from('fundraising_setting');
      $this->db->where('Event_id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res;
    }
    public function getTimeZone($eid)
    {
      $this->db->select('Event_time_zone')->from('event');
      $this->db->where('Id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      $time = $res[0]['Event_time_zone'];

      $this->db->select('Name')->from('timezone');
      $this->db->where('GMT',$time);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res[0]['Name'];

    }
    public function get_agenda_types($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Types,other_types');
        $this->db->from('agenda');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        $this->db->where("End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('Start_date desc');
        $this->db->order_by('Start_time desc');
        $this->db->group_by("Types"); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array(); 
        return $res1;

    }

    public function getAllAgendaByTypeEvent($event_id=null,$id=null,$id_arr=null,$user_id,$lang_id='',$category_id='')
    {
        $this->db->_protect_identifiers=false;
        if(empty($category_id))
        {
            $this->db->select('*')->from('agenda_categories');
            $this->db->where('event_id',$event_id);
            $query=$this->db->get();
            $ares=$query->result_array();
            if(count($ares)==1){
                $category_id = $ares[0]['Id'];
            }else{
                $agenda_array = $this->getAgendaArray($user_id,$event_id);
                if(empty($agenda_array))
                {
                    foreach ($ares as $key => $value) {
                       if($value['categorie_type'] == '1')
                            $category_id = $value['Id'];
                    }
                }
            }
        }
        $id = $this->uri->segment(4);
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End as tname,(CASE WHEN elmc.type IS NULL Then st.type_name ELSE elmc.type End) as Types,(CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End) as Heading',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=1 and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        if(!empty($agenda_array))
        {
            $this->db->where_in('a.Id',$agenda_array);
        }
        $this->db->join('map m', 'm.Id = a.Address_map','left');
       
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         if(!empty($agenda_array)){
            $this->db->where_in('a.Id',$agenda_array);
        }else
        {
            $this->db->join('agenda_categories ac', 'ac.event_id = a.event_id');
            $this->db->join('agenda_category_relation acr', 'acr.agenda_id = a.Id');
            $this->db->where("ac.Id",$category_id);
            $this->db->where("acr.category_id",$category_id);
        }
        $this->db->where('a.Event_id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('st.order_no=0,st.order_no asc');
        $this->db->order_by('tname asc');
        //$this->db->order_by('Start_date asc');
        ////$this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            $prev = '';
            if($prev=="" || $res[$i]->Types!=$prev)
            {    
                if($res1[$i]->Types == 'Other')
                {
                    $res[$i]->Types = $res[$i]->other_types;
                }
                $prev=$res[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Types']=$res[$i]->Types;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                //$agendas['Types']=$res[$i]->Types;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agendas['session_image']= ($res[$i]->session_image) ? $res[$i]->session_image : '';
                $agenda[$res[$i]->Types][] = $agendas;
            }
        }
       /* function sortByTime($a, $b)
        {
            $a = $a['Start_time'];
            $b = $b['Start_time'];

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }*/
        function sortByTime($a, $b)
        {
            $a =  strtotime($a['Start_date'] ." ". $a['Start_time']);
            $b =  strtotime($b['Start_date'] ." ".$b['Start_time']);

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }

        /*uasort($agenda, function($a,$b){

        $c = $a['Start_date'] - $b['Start_date'];
        $c .= $a['Start_time'] - $b['Start_time'];
        return $c;
        });*/

        $j= 0;
        foreach ($agenda as $key => $value) {
            $data[$j]['Types'] = $key;
             usort($value, 'sortByTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        return $data;
    }
    public function getAllAgendaByType($event_id=null,$id=null,$id_arr=null,$user_id,$lang_id)
    {
        $this->db->_protect_identifiers=false; 
        $id = $this->uri->segment(4);
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End as tname,(CASE WHEN elmc.type IS NULL Then st.type_name ELSE elmc.type End) as Types,(CASE WHEN elmc.title IS NULL Then a.Heading ELSE elmc.title End) as Heading',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');

        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=1 and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         
        $this->db->where('a.Event_id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('Start_date asc');
        $this->db->order_by('Start_time asc');
       // $this->db->order_by('a.Start_time desc');
        //$this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev="";
        $j=0;
        $k=0;
       
        for($i=0; $i<count($res); $i++)
        {
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            $prev = '';
            if($prev=="" || $res[$i]->Types!=$prev)
            {    
                if($res1[$i]->Types == 'Other')
                {
                    $res[$i]->Types = $res[$i]->other_types;
                }
                $prev=$res[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Types']=$res[$i]->Types;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['session_image']=($res[$i]->session_image) ? $res[$i]->session_image : '';
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agenda[$res[$i]->Types][] = $agendas;
            }
        }
        function sortByTime($a, $b)
        {
            $a = $a['Start_time'];
            $b = $b['Start_time'];

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }
        uasort($agenda, function($a,$b){
        $c = $a['Start_date'] - $b['Start_date'];
        $c .= $a['Start_time'] - $b['Start_time'];
        return $c;
        });  
        $j= 0;
        foreach ($agenda as $key => $value) {
            $data[$j]['Types'] = $key;
            //usort($value, 'sortByTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        return $data;
    }
    public function deleteUserAgenda($user_id)
    {
        $this->db->where("user_id",$user_id);
        $this->db->delete("users_agenda");
    }

    public function getRatingDataByUserId($user_id,$event_id){
        $this->db->select('usr.session_id')->from('user_session_rating usr');
        $this->db->join('agenda a','a.Id=usr.session_id','left');
        $this->db->where('usr.user_id',$user_id);
        $this->db->where('a.Event_id',$event_id);
        $query = $this->db->get();
        $result = $query->result_array(); 
        return $result;
    }
    public function getAgendaNameTimeById($id){
        $this->db->select('Id,Heading,Start_time')->from('agenda');
        $this->db->where_in('Id',$id);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }
    public function getRatingData($user_id,$agenda_id){
        $this->db->select('*')->from('user_session_rating');
        $this->db->where('user_id',$user_id);
        $this->db->where('session_id',$agenda_id);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }
    public function updateRating($data,$id){
        $this->db->where('Id',$id);
        $this->db->update('user_session_rating',$data);
    }
    public function insertRating($data){
        $this->db->insert('user_session_rating',$data);
    }
    public function updateAgendaCheckInDateTime($agenda_id){
        $this->db->where('Id',$id);
        $this->db->update('agenda',['checking_datetime'=>$agenda_id]);
    }
     public function getPendingAgendaByUserId($user_id)
    {
        $this->db->select('pending_agenda_id');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
         
    }
    public function savePendingAgenda($user_id)
    {

          $msg=array();
          $counter = 0;
          $this->db->select('*')->from('users_agenda');
          $this->db->where('user_id',$user_id);
          $qu=$this->db->get();
          $res=$qu->result_array();
          $paid=array_filter(explode(",",$res[0]['pending_agenda_id']));
          $pending_id=$paid;
          $agenda_id=array_filter(explode(",",$res[0]['agenda_id']));
          $said=$agenda_id;
          if(count($agenda_id)>0){
               foreach ($paid as $pkey => $pvalue) {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id',$pvalue);
                    $dqu=$this->db->get();
                    $data=$dqu->result_array();
                    if(empty($data))
                        continue;
               if(!in_array($pvalue,$agenda_id)){     
               foreach ($agenda_id as $key => $value) {
                if($data[0]['allow_clashing']=='0'){
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Start_date',$data[0]['Start_date']);
                    $this->db->where("(Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                    $this->db->where("(End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                    $this->db->where('Id',$value);
                     $this->db->where('allow_clashing','0');
                    $qu1=$this->db->get();
                    $res1=$qu1->result_array();
                }
                    if(count($res1)>0)
                    {
                         $msg[$counter]['Heading']=$data[0]['Heading'];
                         $msg[$counter]['Start_time']=$data[0]['Start_time'];
                         $msg[$counter]['book']='0';
                         $msg[$counter]['resion']="Session Clash";
                         $return=false;
                         $counter++;
                         break;
                    }
                    else
                    {
                         if(!empty($data[0]['Maximum_People']))
                         {
                              $this->db->select('*')->from('users_agenda');
                              $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                              $this->db->where($where);
                              $tqu=$this->db->get();
                              $total=$tqu->result_array();
                              $placeleft=$data[0]['Maximum_People']-count($total);
                         }
                         else
                         {
                              $placeleft=1;
                         }
                         if($placeleft>0)
                         {
                              $return=true;
                         }
                         else
                         {
                              $msg[$counter]['Heading']=$data[0]['Heading'];
                              $msg[$counter]['Start_time']=$data[0]['Start_time'];
                              $msg[$counter]['book']='0';
                              $msg[$counter]['resion']="No Spaces Left";
                              $return=false;
                              $counter++;
                              break;
                         }
                    }
               }
               if($return=="true")
               {
                    array_push($said,$pvalue);          
                    unset($pending_id[array_search($pvalue,$pending_id)]);
                    $msg[$counter]['Heading']=$data[0]['Heading'];
                    $msg[$counter]['Start_time']=$data[0]['Start_time'];
                    $msg[$counter]['book']='1';
                    $msg[$counter]['resion']="";
                    $counter++;
               }
               }
               else
               {
                    $msg[$counter]['Heading']=$data[0]['Heading'];
                    $msg[$counter]['Start_time']=$data[0]['Start_time'];
                    $msg[$counter]['book']='0';
                    $msg[$counter]['resion']="Already Save Session";   
                    $counter++; 
               }
               }
               $ua['agenda_id']=implode(",",$said);
               $ua['pending_agenda_id']=implode(",",$pending_id);
          }
          else
          {
               foreach ($paid as $pkey => $pvalue) {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id',$pvalue);
                    $dqu=$this->db->get();
                    $data=$dqu->result_array();
                    if(!empty($data[0]['Maximum_People']))
                    {
                         $this->db->select('*')->from('users_agenda');
                         $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                         $this->db->where($where);
                         $tqu=$this->db->get();
                         $total=$tqu->result_array();
                         $placeleft=$data[0]['Maximum_People']-count($total);
                    }
                    else
                    {
                         $placeleft=1;
                    }
                    if($placeleft>0)
                    {
                         $return=true;
                    }
                    else
                    {
                         $msg[$counter]['Heading']=$data[0]['Heading'];
                         $msg[$counter]['Start_time']=$data[0]['Start_time'];
                         $msg[$counter]['book']='0';
                         $msg[$counter]['resion']="No Spaces Left";
                         $return=false;
                         $counter++;
                    }
                    if($return=="true")
                    {
                         array_push($said,$pvalue);          
                         unset($pending_id[array_search($pvalue,$pending_id)]);
                         $msg[$counter]['Heading']=$data[0]['Heading'];
                         $msg[$counter]['Start_time']=$data[0]['Start_time'];
                         $msg[$counter]['book']='1';
                         $msg[$counter]['resion']="";
                         $counter++;
                    }
               }
               $ua['agenda_id']=implode(",",$said);
               $ua['pending_agenda_id']=implode(",",$pending_id);    
          }
          $this->db->where('user_id',$user_id);
          $this->db->update('users_agenda',$ua);
          return $msg;
    }
    public function saveCheckIn($agenda_id,$user_id)
    {
        date_default_timezone_set(ini_get('date.timezone'));
        $this->db->select('count(*) as count')->from('user_check_in');
        $this->db->where('agenda_id',$agenda_id);
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        $result = $query->row();
        
        if($result->count)
        {
            $this->db->where('agenda_id',$agenda_id);
            $this->db->where('user_id',$user_id);
            $this->db->update('user_check_in',['date'=>date("Y-m-d h:i:s")]);
        }
        else
        {
            $data['agenda_id'] = $agenda_id;
            $data['user_id'] = $user_id;
            $data['date'] = date("Y-m-d H:i:s");
            
            $this->db->insert('user_check_in',$data);
        }
    }
    public function getIsPendingAgenda($event_id)
    {
        $this->db->select('on_pending_agenda')->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result->on_pending_agenda;
    }
    /* public function get_overlapping_agenda($data,$user_id,$on_pending)
     {
          $this->db->select('*')->from('users_agenda ua');
          $this->db->where('ua.user_id',$user_id);
          $qu=$this->db->get();
          $res=$qu->result_array();
          if(count($res)>0)
          {
               if($on_pending=='1')
               {
                    $agenda_id=explode(",",$res[0]['pending_agenda_id']);
               }
               else
               {
                    $agenda_id=explode(",",$res[0]['agenda_id']);    
               }
               foreach ($agenda_id as $key => $value) {
                    if(!empty($value)){
                         $this->db->select('*')->from('agenda');
                         $dt=$data[0]['Start_date'].' '.$data[0]['Start_time'];
                         $et=$data[0]['End_date'].' '.$data[0]['End_time'];
                         $wherecon="(('".$dt."' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)) OR ('".$et."' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)))";
                         //$wherecon="End_date = ".$data[0]['Start_date']." AND End_date= ".$data[0]['End_date'];
                         //"Start_date",$data[0]['Start_date']
                         $this->db->where($wherecon);
                         //$this->db->where("(('".$data[0]['Start_time']."' BETWEEN `Start_time` AND `End_time`) OR (Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                        // $this->db->or_where("('".$data[0]['End_time']."' BETWEEN `Start_time` AND `End_time`) OR (End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."'))");
                         $this->db->where('allow_clashing','0');
                         $this->db->where('Id !=',$data[0]['Id']);
                         $this->db->where('Id',$value);
                         $qu1=$this->db->get();
                         $res1=$qu1->result_array();
                         if(count($res1)>0)
                         {
                              $return=false;
                              break; 
                         }
                         else
                         {
                              $return=true;
                         }
                    }
                    else
                    {
                         $return=true;
                    }
               }
          }
          else
          {
               $return=true;
          }
          return $return;
     }*/
     public function get_overlapping_agenda($data,$user_id,$on_pending)
     {
          $this->db->select('*')->from('users_agenda ua');
          $this->db->where('ua.user_id',$user_id);
          $qu=$this->db->get();
          $res=$qu->result_array();
          if(count($res)>0)
          {
               if($on_pending=='1')
               {
                    $agenda_id=explode(",",$res[0]['pending_agenda_id']);
               }
               else
               {
                    $agenda_id=explode(",",$res[0]['agenda_id']);    
               }
               foreach ($agenda_id as $key => $value) {
                    if(!empty($value)){
                         $this->db->select('*')->from('agenda');
                         $dt=$data[0]['Start_date'].' '.$data[0]['Start_time'];
                         $et=$data[0]['End_date'].' '.$data[0]['End_time'];
                         $wherecon="(('".$dt."' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)) OR ('".$et."' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)))";
                         //$wherecon="End_date = ".$data[0]['Start_date']." AND End_date= ".$data[0]['End_date'];
                         //"Start_date",$data[0]['Start_date']
                         $this->db->where($wherecon);
                         //$this->db->where("(('".$data[0]['Start_time']."' BETWEEN `Start_time` AND `End_time`) OR (Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                        // $this->db->or_where("('".$data[0]['End_time']."' BETWEEN `Start_time` AND `End_time`) OR (End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."'))");
                         $this->db->where('allow_clashing','0');
                         $this->db->where('Id !=',$data[0]['Id']);
                         $this->db->where('Id',$value);
                         $qu1=$this->db->get();
                         $res1=$qu1->result_array();
                         if(count($res1)>0)
                         {
                              $return=false;
                              break; 
                         }
                         else
                         {
                              $return=true;
                         }
                    }
                    else
                    {
                         $return=true;
                    }
               }
          }
          else
          {
               $return=true;
          }
          return $return;
     }
     public function getAgendaData($agenda_id)
     {
        $this->db->select('a.*,st.type_name as Types')->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->where('Id',$agenda_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
     }

     public function updateCheckIn($user_id,$agenda_id)
     {
        $this->db->where('user_id',$user_id);
        $this->db->where('agenda_id',$agenda_id);
        $this->db->delete('user_check_in');
     }
     public function getRatingDataNew($user_id,$agenda_ids,$event_id)
     {
        foreach ($agenda_ids as $key => $value) {
            $this->db->select('count(u.user_id) as count');
            $this->db->from('user_session_rating u');
            $this->db->join('agenda a','a.Id = u.session_id');
            $this->db->where('u.user_id',$user_id);
            $this->db->where('u.session_id',$value);
            $this->db->where('a.Event_id',$event_id);
            $query = $this->db->get();
            $data = $query->row();
            if($data->count == 0)
            {
                $data1['count'] = $data->count;
                $data1['agenda_id'] = $value;
                break;
            }
        }
       return $data1; 

     }
     public function getAllMeetings($event_id,$user_id)
     {
        $role = $this->db->select('*')->from('relation_event_user')->where('User_id',$user_id)->where('Event_id',$event_id)->get()->row_array();
        if($role['Role_id']=='4' || $role['Role_id']=='6')
        {
            $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,e.Heading,e.stand_number')->from('exhibitor_attendee_meeting eam');
            $this->db->where('eam.event_id',$event_id);
            if($role['Role_id']=='4')
            $this->db->where('eam.attendee_id',$user_id);
            if($role['Role_id']=='6')
            $this->db->where('e.user_id',$user_id);
            $this->db->where('eam.status !=','2');
            $this->db->join('user u','u.Id=eam.attendee_id','right');
            if($role['Role_id']=='4')
                $this->db->join('exibitor e','e.Id=eam.exhibiotor_id','right');
            elseif($role['Role_id']=='6')
                $this->db->join('exibitor e','e.Id=eam.exhibiotor_id','left');
            $this->db->group_by('eam.Id');
            $this->db->order_by('eam.date asc');
            $qu=$this->db->get();
            $res=$qu->result_array();
        }
        $data = array();
        for($i=0; $i<count($res); $i++)
        {
               $timedate=$res[$i]['date'];
               $prev="";
               if($prev=="" || $timedate!=$prev)
               {
                
                $prev=$timedate;
                $agendas['metting_id']=$res[$i]['Id'];
                $agendas['exhibiotor_id']=$res[$i]['exhibiotor_id'];
                $agendas['is_meeting']='1';
                $agendas['attendee_id']=$res[$i]['attendee_id'];
                $agendas['date']=$res[$i]['date'];
                $agendas['time']=$res[$i]['time'];
                $agendas['status']=$this->getStatus($res[$i]['status']);
                $name = $res[$i]['Firstname']." ".$res[$i]['Lastname'];
                $name = ($name) ? $name : '';
                $agendas['Heading'] = ($role['Role_id']=='6') ? $name : $res[$i]['Heading'];
                $agendas['stand_number'] = $res[$i]['stand_number'];
                
                $agenda[$timedate][] = $agendas;
            }
        }
         $j= 0;
       foreach ($agenda as $key => $value) {
            $data[$j]['date'] = $key;
            $data[$j]['date_time'] = $key;
            $data[$j]['data'] = $value;
            $j++;
        }
        function sortByTime1($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a < $b) ? -1 : 1;
            } 
        usort($data, 'sortByTime1');
        return $data;
          
     }
     public function getAllMeetingsByType($event_id,$user_id)
     {
        $role = $this->db->select('*')->from('relation_event_user')->where('User_id',$user_id)->where('Event_id',$event_id)->get()->row_array();
        if($role['Role_id']=='4' || $role['Role_id']=='6')
        {
            $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,e.Heading,e.stand_number')->from('exhibitor_attendee_meeting eam');
            $this->db->where('eam.event_id',$event_id);
            if($role['Role_id']=='4')
                $this->db->where('eam.attendee_id',$user_id);
            if($role['Role_id']=='6')
                $this->db->where('e.user_id',$user_id);
            $this->db->where('eam.status !=','2');
            $this->db->join('user u','u.Id=eam.attendee_id','right');
            if($role['Role_id']=='4')
                $this->db->join('exibitor e','e.Id=eam.exhibiotor_id','right');
            elseif($role['Role_id']=='6')
                $this->db->join('exibitor e','e.Id=eam.exhibiotor_id','left');
            $this->db->group_by('eam.Id');
            $this->db->order_by('eam.date asc');
            $qu=$this->db->get();
            $res=$qu->result_array();
        }
        $data = array();
        for($i=0; $i<count($res); $i++)
        {
            $agendas['metting_id']=$res[$i]['Id'];
            $agendas['exhibiotor_id']=$res[$i]['exhibiotor_id'];
            $agendas['is_meeting']='1';
            $agendas['attendee_id']=$res[$i]['attendee_id'];
            $agendas['date']=$res[$i]['date'];
            $agendas['time']=$res[$i]['time'];
            $agendas['status']=$this->getStatus($res[$i]['status']);
            $name = $res[$i]['Firstname']." ".$res[$i]['Lastname'];
            $name = ($name) ? $name : '';
            $agendas['Heading'] = ($role['Role_id']=='6') ? $name : $res[$i]['Heading'];
            $agendas['stand_number'] = $res[$i]['stand_number'];
            $agenda['Exhibitor Meeting'][] = $agendas;
            
        }
        $j= 0;
        foreach ($agenda as $key => $value) {
            $data[$j]['Types'] = $key;
            $data[$j]['data'] = $value;
            $j++;
        }
        return $data;
          
     }
     public function getStatus($status)
     {
        $return = '';
        switch ($status) {
            case '0':
                $return = "Pending";
                break;
            case '1':
                $return = "Confirmed";
                break;
            case '1':
                $return = "Rejected";
                break;
            
        }
        return $return;
     }
     public function getShowSuggestionButton($user_id,$event_id)
     {
        $where['sm.event_id'] = $event_id;
        $where['sm.recever_user_id'] = $user_id;
        $data1 = $this->db->select('*')->from('suggest_meeting sm')->join('exibitor e','e.user_id = sm.exhibitor_user_id')->where($where)->get()->result_array();
        $data2 = $this->db->select('*')->from('suggest_meeting sm')->join('user e','e.Id = sm.attendee_id')->where($where)->get()->result_array();
        $data = array_merge($data1,$data2);
        return (!empty($data)) ? '1' : '0';
        
     }
     // #agenda_comments
     public function saveAgendaComments($insert_data)
     {
        $this->db->insert('agenda_comments',$insert_data);
     }
     // #agenda_comments
     public function checkShowCommentBox($where)
     {
        $data = $this->db->select('*')->from('agenda_comments')->where($where)->get()->result_array();
        if(count($data) >= 1)
            return "0";
        else
           return "1";
    }

    public function checkEventDateFormat($event_id)
    {
        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        
        $res=$query->result_array();
        return $res;
        
    }
    public function getAllAgendaByTimeEventNew($event_id,$id_arr,$user_id)
    {
        $checkAgneda = $this->checkAgneda($event_id);
        $this->db->select('*')->from('agenda_categories');
        $this->db->where('event_id',$event_id);
        $query=$this->db->get();
        $ares=$query->result_array();
        if(count($ares)==1){
            $category_id = $ares[0]['Id'];
        }else{

            $agenda_array = $this->getAgendaArray($user_id,$event_id);
            if(empty($agenda_array))
            {
                foreach ($ares as $key => $value) {
                   if($value['categorie_type'] == '1')
                        $category_id = $value['Id'];
                }
            }
        }
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,e.show_session_by_time,a.Heading as tname,CASE WHEN a.session_image IS NULL THEN "" ELSE a.session_image END as session_image,st.type_name as Types',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event e', 'e.Id = a.Event_id');
        if(!empty($agenda_array)){
            $this->db->where_in('a.Id',$agenda_array);
        }else
        {
            $this->db->join('agenda_categories ac', 'ac.event_id = a.event_id');
            $this->db->join('agenda_category_relation acr', 'acr.agenda_id = a.Id');
            $this->db->where("ac.Id",$category_id);
            $this->db->where("acr.category_id",$category_id);
        }
       
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);

        $this->db->order_by('tname desc');
        $query = $this->db->get();
        $res = $query->result();
       // echo $this->db->last_query();exit;
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           $date_time = ($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agenda[$date_time][] = $agendas;
                $agendas['Types']=$res[$i]->Types;
                $agendas['session_image']=$res[$i]->session_image;

            }
        }
        $j= 0;
        function sortByStartTime($a, $b)
        {
            $a = strtotime($a['Start_time']);
            $b = strtotime($b['Start_time']);

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        } 
        foreach ($agenda as $key => $value) {
            $date_time = ($res[0]->show_session_by_time) ? $key :date("l, jS M , Y",strtotime($key));
            $data[$j]['date_time'] = $date_time;
            $data[$j]['date'] = $key;
            usort($value, 'sortByStartTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        function sortByTime($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a < $b) ? -1 : 1;
            } 
        usort($data, 'sortByTime');
        $groups = array();
        for($j=0;$j<sizeof($data);$j++)
        {   
           foreach ($data[$j]['data'] as $item)
           {
            // print_r($item);exit();
           $key = $item['Types'];
                if (!isset($groups[$key]))
                 {  
                      $data[$j][$key][] = array($item);     
                    /*$groups[$key] = array(
                        'items' => array($item),
                        'count' => 1,
                    );*/
                 } 
                 else
                  {   
                    $data[$j][$key][] = $item;
                  }
            }
            unset($data[$j]['data']);
        }
        return $data;
    }
    public function getAllAgendaByTypeEventNew($event_id=null,$id=null,$id_arr=null,$user_id)
    {
         $this->db->select('*')->from('agenda_categories');
        $this->db->where('event_id',$event_id);
        $query=$this->db->get();
        $ares=$query->result_array();
        if(count($ares)==1){
            $category_id = $ares[0]['Id'];
        }else{
            $agenda_array = $this->getAgendaArray($user_id,$event_id);
            if(empty($agenda_array))
            {
                foreach ($ares as $key => $value) {
                   if($value['categorie_type'] == '1')
                        $category_id = $value['Id'];
                }
            }
        }
        $id = $this->uri->segment(4);
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,a.Heading as tname,CASE WHEN a.session_image IS NULL THEN "" ELSE a.session_image END as session_image,st.type_name as Types',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        if(!empty($agenda_array))
        {
            $this->db->where_in('a.Id',$agenda_array);
        }
        $this->db->join('map m', 'm.Id = a.Address_map','left');
       
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         if(!empty($agenda_array)){
            $this->db->where_in('a.Id',$agenda_array);
        }else
        {
            $this->db->join('agenda_categories ac', 'ac.event_id = a.event_id');
            $this->db->join('agenda_category_relation acr', 'acr.agenda_id = a.Id');
            $this->db->where("ac.Id",$category_id);
            $this->db->where("acr.category_id",$category_id);
        }
        $this->db->where('a.Event_id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
      
        $this->db->order_by('Start_date asc');
        $this->db->order_by('Start_time asc');
        //$this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            $prev = '';
            if($prev=="" || $res[$i]->Types!=$prev)
            {    
                if($res1[$i]->Types == 'Other')
                {
                    $res[$i]->Types = $res[$i]->other_types;
                }
                $prev=$res[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Types']=$res[$i]->Types;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agenda[$res[$i]->Types][] = $agendas;
                $agendas['session_image'] = $res[$i]->session_image;
            }
        }
        function sortByTime($a, $b)
        {
            $a = $a['Start_time'];
            $b = $b['Start_time'];

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        }
        uasort($agenda, function($a,$b){
        $c = $a['Start_date'] - $b['Start_date'];
        $c .= $a['Start_time'] - $b['Start_time'];
        return $c;
        }); 
        $j= 0;
        foreach ($agenda as $key => $value) {
            $data[$j]['Types'] = $key;
            //usort($value, 'sortByTime');
            $data[$j]['data'] = $value;
            $j++;
        }

        return $data;
    }
    public function getAllAgendaByTimeNew($event_id,$id_arr,$user_id)
    { 
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title,e.show_session_by_time,st.type_name as Types',false);
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id =a.Types');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date desc');
       // $this->db->order_by('a.Start_time desc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        /*for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           $date_time = ($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
            if($date_time!=$prev)
            {
                $k=0;
                $myarr[$j]["date"]=($res[$i]->show_session_by_time) ? $res[$i]->Start_time : $res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j]["data"][$k]['placeleft'] = $this->getMaxPeople($res[$i]->Id,'');
                $myarr[$j]["data"][$k]['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '--';
                $myarr[$j]["data"][$k]['location'] = ucfirst(($res[$i]->custom_speaker_name) ? $res[$i]->custom_location : '--');
                $myarr[$j]["data"][$k]['time_zone']= $timezone;
                $j++;
               
            }
            else
            {
                $myarr[$j-1]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j-1]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j-1]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j-1]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j-1]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j-1]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j-1]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j-1]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j-1]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j-1]["data"][$k]['placeleft'] = $this->getMaxPeople($res[$i]->Id,'');
                $myarr[$j-1]["data"][$k]['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '--';
                $myarr[$j-1]["data"][$k]['location'] = ucfirst(($res[$i]->custom_speaker_name) ? $res[$i]->custom_location : '--');
                $myarr[$j-1]["data"][$k]['time_zone']= $timezone;
            }
            $k++;
            $prev=($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
        }
        return $myarr;*/
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           $date_time = ($res[$i]->show_session_by_time) ? $res[$i]->Start_time :$res[$i]->Start_date;
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas['Id']=$res[$i]->Id;
                $agendas['Heading']=$res[$i]->Heading;
                $agendas['is_meeting']='0';
                $agendas['Start_date']=$res[$i]->Start_date;
                $agendas['Start_time']=$res[$i]->Start_time;
                $agendas['End_date']=$res[$i]->End_date;
                $agendas['End_time']=$res[$i]->End_time;
                $agendas['Address_map']=$res[$i]->Address_map_id;
                $agendas['Map_title']=$res[$i]->Map_title;
                $agendas['Event_id']=$res[$i]->Event_id;
                $mx_peple = $this->getMaxPeople($res[$i]->Id,'');
                $agendas['placeleft'] = ($mx_peple > 0) ? $mx_peple : '';
                $agendas['speaker'] = ($res[$i]->custom_speaker_name) ? $res[$i]->custom_speaker_name : '';
                $agendas['location'] = (($res[$i]->custom_location) ? ucfirst($res[$i]->custom_location) : '');
                $agendas['time_zone']= $timezone;
                $agenda[$date_time][] = $agendas;
                $agendas['Types']=$res[$i]->Types; 
            }
        }
        $j= 0;
        function sortByStartTime($a, $b)
        {
            $a = strtotime($a['Start_time']);
            $b = strtotime($b['Start_time']);

            if ($a == $b)
            {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        } 
        foreach ($agenda as $key => $value) {
            $date_time = ($res[0]->show_session_by_time) ? $key :date("l, jS M , Y",strtotime($key));
            $data[$j]['date_time'] = $date_time;
            $data[$j]['date'] = $key;
            usort($value, 'sortByStartTime');
            $data[$j]['data'] = $value;
            $j++;
        }
        function sortByTime($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a < $b) ? -1 : 1;
            } 
        usort($data, 'sortByTime');
        $groups = array();
        for($j=0;$j<sizeof($data);$j++)
        {   
           foreach ($data[$j]['data'] as $item)
           {
            // print_r($item);exit();
           $key = $item['Types'];
                if (!isset($groups[$key]))
                 {  
                      $data[$j][$key][] = array($item);     
                    /*$groups[$key] = array(
                        'items' => array($item),
                        'count' => 1,
                    );*/
                 } 
                 else
                  {   
                    $data[$j][$key][] = $item;
                  }
            }
            unset($data[$j]['data']);
        }
        return $data;
    }    
}
        
?>