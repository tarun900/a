<?php

class gamification_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

    }
    public function get_leaderboard($event_id)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sender_name,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END as logo,SUM(gu.points) as total,ru.Role_id'
                           ,FALSE);
        $this->db->from('user u');
        $this->db->join('game_users_point gu','gu.user_id = u.Id');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->where('gu.event_id',$event_id);
        $this->db->where('ru.Event_id',$event_id);
        $this->db->group_by('u.Id');
        $this->db->order_by('total','desc');
        $this->db->limit('5');
        $query = $this->db->get();
        $res   = $query->result_array();

        $this->db->select('*');
        $this->db->where('event_id',$event_id);
        $query =  $this->db->get('game_rank_color');
        $color = $query->result_array();
        foreach ($res as $key => $value)
        {   
           $res[$key]['color'] = $color[$key]['color'];
           $res[$key]['rank'] = $color[$key]['rank_no'];
           if($key > 0)
           {
                if($value['total'] == $res[$key-1]['total'])
                {
                    $res[$key]['rank'] = $res[$key-1]['rank'];
                }
           }
           if($value['Role_id'] == '6')
           {
                $tmp = $this->db->select('id')->from('exibitor')->where('user_id',$value['id'])->where('Event_id',$event_id)->get()->row_array();
                $res[$key]['id'] = $tmp['id'];
           }
        }

        $data['top_five'] = $res;
        $this->db->select('header');
        $this->db->where('event_id',$event_id);
        $header = $this->db->get('game_header')->row_array();

        $data['header'] = $header['header'];
        $data['image']['L'] = 'http://www.allintheloop.net/assets/user_files/Star.png';
        $data['image']['R'] = 'http://www.allintheloop.net/assets/user_files/Star-R.png';

        $this->db->select('gr.point,g.rank');
        $this->db->from('game_rank_point gr');
        $this->db->join('game_rank g','g.id = gr.rank_id');
        $this->db->where('event_id',$event_id);
        $this->db->where('gr.point !=','0');
        $info = $this->db->get()->result_array();
        foreach ($info as $key => $value)
        {   
            $info[$key]['point'] .= ($value['point'] == 1) ? " Point" : " Points";
        }
        $data['info'] =  $info;
        return $data;
    }
    public function add_user_point($user_id,$event_id,$rank_id)
    {   
        $this->db->select('point');
        $this->db->where('rank_id',$rank_id);
        $this->db->where('event_id',$event_id);
        $point = $this->db->get('game_rank_point')->row_array();
        
        if(!empty($point))
        {
            $this->db->select('*');
            $this->db->where('user_id',$user_id);
            $this->db->where('rank_id',$rank_id);
            $this->db->where('event_id',$event_id);
            $res = $this->db->get('game_users_point')->row_array();
            if(!empty($res))
            {
                $update['points'] = $res['points'] + $point['point'];
                $this->db->where('id',$res['id']);
                $this->db->update('game_users_point',$update);
            }
            else
            {
                $insert['points'] = $point['point'];
                $insert['rank_id'] = $rank_id;
                $insert['user_id'] = $user_id;
                $insert['event_id'] = $event_id;
                $this->db->insert('game_users_point',$insert);
            }
        }
        return true;
    }
    public function getAllUsersGCM_id_by_event_id($event_id)
    {
        return $this->db->select('u.*')->from('relation_event_user reu')->join('user u','u.Id = reu.User_id')->where('reu.Event_id',$event_id)->get()->result_array();
    } 
}

?>
    
    