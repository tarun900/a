<?php

class product_model extends CI_Model
{

     function __construct()
     {

          $this->db1 = $this->load->database('db1', TRUE);
          parent::__construct();
     }
     
    
    
    
    public function getProductsByAuctionType($event_id,$auctionType,$page_no=1)
    {
      $limit=12;
      $page_no = (!empty($page_no))?$page_no:1;
      $start=($page_no-1)*$limit;
      $Subdomain = $this->getSubdomain($event_id);
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.product_preview',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.is_superadmin','1'); 
     
      $this->db->where('p.auctionType',$auctionType);
      $this->db->where('p.event_id',$event_id);
      if($Subdomain == 'aeinstitute17')
      $this->db->where('p.product_id != ','1899'); 
      $this->db->where('p.auction_show','1'); 
      $this->db->group_by("p.product_id");
      
      $query = $this->db->get();
      $superadmin = $query->result_array();
      if($Subdomain == 'aeinstitute17')
      {
        $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.product_preview',false);
        $this->db->from('product p');
        $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
        $this->db->where('p.event_id',$event_id); 
        $this->db->where('p.approved_status','1'); 
        $this->db->where('p.auctionType',$auctionType); 
        $this->db->where('p.product_id','1899'); 
        $this->db->group_by("p.product_id");
        $query = $this->db->get();
        $product = $query->result_array();
      }
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.product_preview',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('p.auctionType',$auctionType); 
      if($Subdomain == 'aeinstitute17')
      $this->db->where('p.product_id != ','1899'); 
      $this->db->group_by("p.product_id");
      $query = $this->db->get();
      $res2 = $query->result_array();
      if($Subdomain == 'aeinstitute17')
        $res=array_merge($product,$superadmin,$res2);
      else
        $res=array_merge($superadmin,$res2);
      $total=count($res);
      $total_page=ceil($total/$limit);
      $res=array_slice($res,$start,$limit);
      $data['products']=$res;
      $data1['total_page']=$total_page;
      if(!empty($data['products'])) 
      {
        foreach ($data['products'] as $key => $value) 
        {
            
            $price="";
            $max="";
            $data1['products'][$key]['product_id']=$value['product_id'];
            $data1['products'][$key]['name']=$value['name'];
            $data1['products'][$key]['description']=$value['description'];
            $data1['products'][$key]['thumb']=$value['thumb'];
            $data1['products'][$key]['auctionType']=$value['auctionType'];
            if($value['auctionType']==1 || $value['auctionType']==2)
            {
                  $price=$value['startPrice'];
                  $data1['products'][$key]['price']=$price;
                  $max=$value['max_bid'];
                  $data1['products'][$key]['max_bid']=$max;
                  if(empty($value['max_bid']))
                  {
                    $data1['products'][$key]['flag_price']=1;
                    $data1['products'][$key]['max_bid']="";
                  }
                  else
                  {
                    $data1['products'][$key]['flag_price']=0;
                  }
                  
            }
            else if($value['auctionType']==3)
            {
                  $price=$value['price'];
                  $data1['products'][$key]['price']=$price;
                  $data1['products'][$key]['flag_price']=1;
                  $data1['products'][$key]['max_bid']='';
            }
            else if($value['auctionType']==4)
            {
                $price=$value['pledge_amt'];
                if(!empty($value['price']))
                {
                    $price=$value['pledge_amt'];
                }
                $data1['products'][$key]['price']=$price;
                $data1['products'][$key]['max_bid']='';
                $data1['products'][$key]['flag_price']=1;
            }
          $data1['products'][$key]['product_preview'] = $value['product_preview'];
        }
        return $data1;
      } 
      else 
      {
        return false;
      }
  }
  public function getSlientAuctionProductsDetails($product_id,$event_id)
  {
      $date = date('Y-m-d h:i:s');
      $this->db->select("p.product_id,CASE WHEN pb.bid_amt IS NULL THEN 0 ELSE pb.bid_amt END as bid_amt,CASE WHEN max(pb.bid_amt) IS NULL THEN '' ELSE max(pb.bid_amt) END as max_bid,CASE WHEN p.name IS NULL THEN '' ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN '' ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN '' ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN '' ELSE p.startPrice END as startPrice,CASE WHEN p.increment_amt IS NULL THEN '' ELSE p.increment_amt END as increment_amt,CASE WHEN p.price IS NULL THEN '' ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN '' ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN '' ELSE p.thumb END as thumb,p.auctionStartDateTime,p.auctionEndDateTime,CASE WHEN p.auctionStartDateTime <= '$date' AND p.auctionEndDateTime >= '$date' THEN 1 ELSE 0 END as enable_bid,p.product_preview",false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->join("product_image pi","pi.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      
      $this->db->where('p.approved_status','1'); 
      $this->db->where('p.product_id',$product_id); 
      $query = $this->db->get();
      $res2 = $query->result_array();
     
      if(!empty($res2[0]['product_id']))
      {
          $product_id=$res2[0]['product_id'];
          $this->db->select('CASE WHEN pi.image_url IS NULL THEN "" ELSE pi.image_url END as image_url',false);
          $this->db->from('product_image pi');
          $this->db->where('pi.product_id',$product_id); 
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
         
          if(!empty($res1))
          {
            $image=array_column($res1,'image_url');
            $res2[0]['image_arr']=$image;
          }
          else
          {
            
            $res2[0]['image_arr']=[];
          }
          return $res2;
        
      }
     
      
  }

  public function getLiveAuctionProductsDetails($product_id,$event_id){
    $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,CASE WHEN max(pb.bid_amt) IS NULL THEN "" ELSE max(pb.bid_amt) END as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL OR p.short_description = "0" THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.increment_amt IS NULL THEN "" ELSE p.increment_amt END as increment_amt,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.reserveBid IS NULL THEN "" ELSE p.reserveBid END as reserve_bid_price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,CASE WHEN p.auctionStartDateTime IS NULL THEN "" ELSE p.auctionStartDateTime END as auctionStartDateTime,CASE WHEN p.auctionEndDateTime IS NULL THEN "" ELSE p.auctionEndDateTime END as auctionEndDateTime,p.product_preview',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->join("product_image pi","pi.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('p.product_id',$product_id); 
      $query = $this->db->get();
      $res2 = $query->result_array();
     // print_r($res2);exit;
      if(($res2[0]['product_id'])!='')
      {
          $product_id=$res2[0]['product_id'];
          $this->db->select('CASE WHEN pi.image_url IS NULL THEN "" ELSE pi.image_url END as image_url',false);
          $this->db->from('product_image pi');
          $this->db->where('pi.product_id',$product_id); 
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
         
          if(!empty($res1))
          {
            $image=array_column($res1,'image_url');
            $res2[0]['image_arr']=$image;
          }
          else
          {
            
            $res2[0]['image_arr']=[];
          }
          return $res2;
        
      }
     
      
  }
  public function getOnlineShopDetails($product_id,$event_id){
    $this->db->select('p.product_id,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL OR p.short_description = "0" THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN pq.quantity IS NULL THEN "" ELSE pq.quantity END as quantity,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,CASE WHEN p.product_preview IS NULL THEN 0 ELSE p.product_preview END as product_preview',false);
      $this->db->from('product p');
      $this->db->join("product_quantity pq","pq.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('p.product_id',$product_id); 
      $query = $this->db->get();
      $res2 = $query->result_array();
      if(!empty($res2[0]['product_id']))
      {
          $product_id=$res2[0]['product_id'];
          $this->db->select('CASE WHEN pi.image_url IS NULL THEN "" ELSE pi.image_url END as image_url',false);
          $this->db->from('product_image pi');
          $this->db->where('pi.product_id',$product_id); 
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
         
          if(!empty($res1))
          {
            $image=array_column($res1,'image_url');
            $res2[0]['image_arr']=$image;
          }
          else
          {
            
            $res2[0]['image_arr']="";
          }
          
        return $res2;
      }
     
      
    }
    public function getDonationDetails($product_id,$event_id){

      $this->db->select('p.product_id,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL OR p.short_description = "0" THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as amount,CASE WHEN p.no_price IS NULL THEN "" ELSE p.no_price END as no_price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.product_preview',false);
      $this->db->from('product p');
      $this->db->join("product_image pi","pi.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('p.product_id',$product_id); 
      $query = $this->db->get();
      $res2 = $query->result_array();
      if(!empty($res2[0]['product_id']))
      {
          $product_id=$res2[0]['product_id'];
          $this->db->select('CASE WHEN pi.image_url IS NULL THEN "" ELSE pi.image_url END as image_url',false);
          $this->db->from('product_image pi');
          $this->db->where('pi.product_id',$product_id); 
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
         
          if(!empty($res1))
          {
            $image=array_column($res1,'image_url');
            $res2[0]['image_arr']=$image;
          }
          else
          {
            
            $res2[0]['image_arr']="";
          }
          
          $res2[0]['amount'] = explode(',', $res2[0]['amount']);
        return $res2;
      }
     
      
    }
    public function getCartDetails($event_id,$user_id){

    $this->db->select('c.Id,p.product_id,c.user_id,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN c.price IS NULL THEN "" ELSE c.price END as price,CASE WHEN c.quantity IS NULL THEN "" ELSE c.quantity END as quantity,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN c.visibility IS NULL THEN "" ELSE c.visibility END as visibility,(c.price * c.quantity) as subtotal',false);
      $this->db->from('cart c');
      $this->db->join("product p","p.product_id=c.product_id",'left');
      $this->db->where('c.user_id',$user_id); 
      $query = $this->db->get();
      $res2 = $query->result_array();
      return $res2;
    }
    public function getCartDetailsByProductId($product_id,$event_id,$user_id){

    $this->db->select('c.Id,p.product_id,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN c.price IS NULL THEN "" ELSE c.price END as price,CASE WHEN c.quantity IS NULL THEN "" ELSE c.quantity END as quantity,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN c.price_type IS NULL THEN "" ELSE c.price_type END as price_type,CASE WHEN c.visibility IS NULL THEN "" ELSE c.visibility END as visibility,CASE WHEN c.comment IS NULL THEN "" ELSE c.comment END as comment,(c.price * c.quantity) as subtotal',false);
      $this->db->from('product p');
      $this->db->join("cart c","c.product_id=p.product_id");
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('c.product_id',$product_id); 
      $this->db->where('p.approved_status','1'); 
      $query = $this->db->get();
      $res2 = $query->result_array();
      return $res2;
    }
  public function get_last_bid($id)
  {
          $this->db->select('u.Firstname,u.Email,u.Lastname,pub.*,pub.bid_amt as bider,p.name as product_name')->from('product_user_bids pub');
          $this->db->join("user u", "u.Id=pub.user_id");
          $this->db->join("product p", "p.product_id=pub.product_id");
          $this->db->order_by('pub.bid_amt', 'desc');
          $this->db->where('pub.product_id', $id);
          $this->db->limit(1);
          $query = $this->db->get();
          $result = $query->result_array();

          return $result;
  }
  public function get_outbid($id)
  {
          $this->db->select('u.*,pub.*,pub.bid_amt as bider')->from('product_user_bids pub');
          $this->db->join("user u", "u.Id=pub.user_id");
          $this->db->order_by('pub.bid_amt', 'desc');
          $this->db->where('pub.product_id', $id);
          $this->db->limit(1,2);
          $query = $this->db->get();
          $result = $query->result_array();

          return $result;
  }
  public function count_total_bid($product_id)
  {
          $this->db->select('')->from('product_user_bids pub');
          $this->db->where('pub.product_id', $product_id);
          $query = $this->db->get();
          $result = $query->result_array();
          $cnt=count($result);
          return $cnt;
  }
  public function check_winner($product_id)
  {
        $this->db->select('pub.*,u.Firstname,u.Lastname')->from('product_user_bids pub');
        $this->db->join('user u','u.Id=pub.user_id');
        $this->db->where('product_id',$product_id);
        $this->db->where('win_status','1');
        $qry1=$this->db->get();
        $res1=$qry1->result_array();
        $name=$res1[0]['Firstname']." ".$res1[0]['Lastname'];
        return $name;
  }
  public function getProductIdByAuctionType($event_id,$auctionType)
  {
        $this->db->select('p.product_id',false);
        $this->db->from('product p');
        $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
        $this->db->where('p.event_id',$event_id); 
        $this->db->where('p.approved_status','1'); 
        $this->db->where('p.auctionType',$auctionType); 
        $this->db->group_by('p.product_id'); 
      
        $query = $this->db->get();
        $res2 = $query->result_array();
        return $res2;
  }
  public function insertCartDetails($data){
        $this->db->select('*')->from('cart');
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('product_id',$data['product_id']);
        $query=$this->db->get();
        $result=$query->row();
        if(count($result) == 0){
          $this->db->insert("cart",$data);  
        }else{
          $this->db->where("user_id",$data['user_id']);
          $this->db->where("product_id",$data['product_id']);
          $this->db->update("cart",['quantity'=>($result->quantity + $data['quantity'])]);  
        }   
  }
  public function addToPledgeAmountDonate($data){
        $this->db->insert("pledge_amt_donate",$data);
        $id = $this->db->insert_id();
        $this->db->select('*');
        $this->db->from('pledge_amt_donate');
        $this->db->where('Id',$id); 
        $this->db->where('user_id',$data['user_id']); 
        $query = $this->db->get();
        $res2 = $query->result_array();
       
        return $res2;
  }
  public function getProductById($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id); 
        $query = $this->db->get();
        $res2 = $query->result_array();
        return $res2;
  }
  public function insertOrderTransaction($order_data,$order_transaction_data){
        $this->db->insert("order",$order_data);
        $order_id = $this->db->insert_id();
        $order_transaction_data['order_id'] = $order_id;
        $this->db->insert("order_transaction",$order_transaction_data);
        return $this->db->insert_id();
  }
  public function insertBidDetails($data){
        $this->db->insert("product_user_bids",$data);
  }
  public function getBillingInfomration($user_id){
        $this->db->select('CASE WHEN Firstname IS NULL THEN "" ELSE Firstname END as Firstname,CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END as Lastname,CASE WHEN Email IS NULL THEN "" ELSE Email END as Email,CASE WHEN mobile IS NULL THEN "" ELSE mobile END as mobile',false);
        $this->db->from('user');
        $this->db->where('Id',$user_id); 
        $query = $this->db->get();
        $res2 = $query->result_array();
        return $res2[0];
  }

  public function getProductsByUserId($user_id){

        $this->db->select('c.Id,
                          CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,
                          CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,
                          CASE WHEN c.quantity IS NULL THEN "" ELSE c.quantity END as quantity,
                          CASE WHEN c.price IS NULL THEN "" ELSE c.price END as price',false);
        $this->db->from('cart c');
        $this->db->join("product p","p.product_id=c.product_id",'left');
        $this->db->where('c.user_id',$user_id); 
        $query = $this->db->get();
        $res2 = $query->result_array();
        return $res2;
  }
  public function getstripeshow($event_id)
    {
      $this->db->select('u.*')->from('event e');
      $this->db->where('e.Id',$event_id);
      $this->db->join('user u','u.Id=e.Organisor_id');
      $qu=$this->db->get();
      $res=$qu->result_array();

      $user_id = $res[0]['Id'];
     
      $this->db1->select('ou.*')->from('organizer_user ou');
      $this->db1->where('ou.Email',$res[0]['Email']);
      $que=$this->db1->get();
      $str=$que->result_array();
      $stripe = $str[0]['stripe_show'];
       $i= 0;
      if($stripe == 0){
        $paypal = $this->checkPaymentMethodAvailable('paypal_settings','paypal_enable',$user_id);
        $authorize = $this->checkPaymentMethodAvailable('authorize_settings','authorize_enable',$user_id);
        $cod = $this->checkPaymentMethodAvailable('cod_settings','cod_enable',$user_id);
       
        if($paypal){
          $payment_method[$i] = "Paypal Payment";
          $i++;
        }
        if($authorize){
          $payment_method[$i] = "Authorize Payment";
          $i++;
        }
        if($cod){
          $payment_method[$i] = "Cash on Delivery";
          $i++;
        } 

      }
      else{
         $stripe = $this->checkPaymentMethodAvailable('stripe_settings','stripe_enable',$user_id);
        if($stripe){
          $payment_method[$i] = "Stripe Payment";
        }
      }
      return $payment_method;
    }
    public function checkPaymentMethodAvailable($table_name,$field_name,$where){
        $this->db->select($field_name)->from($table_name);
        $this->db->where('organisor_id',$where);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return ($res[0][$field_name]);
    }
    public function getPaymentInfo($event_id)
    {
      $this->db->select('u.*')->from('event e');
      $this->db->where('e.Id',$event_id);
      $this->db->join('user u','u.Id=e.Organisor_id');
      $qu=$this->db->get();
      $res=$qu->row();
      
      $this->db1->select('sbc.fund_percantage,ou.stripe_show')->from('subscription_billing_cycle sbc');
      $this->db1->join('organizer_user ou','ou.subscriptiontype=sbc.id');
      $this->db1->where('ou.Email',$res->Email);
      $que=$this->db1->get();
      $fund_info=$que->row();
      $data['fund_info']  = ($fund_info) ? array($fund_info) : array() ;

      $this->db1->select('*')->from('stripe_settings');
      $que=$this->db1->get();
      $superadmin_info=$que->row();
      $data['super_admin_info']  = ($superadmin_info) ? array($superadmin_info) : array();

      $this->db->select('*')->from('stripe_settings');
      $this->db->where('organisor_id',$res->Id);
      
      $que=$this->db->get();
      $organizer_info=$que->row();
      $data['organisor_info']  = ($organizer_info) ? array($organizer_info) : array();
      
      return $data;
      
    }
    public function getCheckoutContent($event_id){
        $this->db->select('checkout_content');
        $this->db->from('fundraising_setting');
        $this->db->where('Event_id',$event_id); 
        $query = $this->db->get();
        $res2 = $query->result_array();
        return ($res2[0]['checkout_content']!= NULL) ? $res2[0]['checkout_content'] : '';
    }
    public function updateCartDetails($product,$user_id){
      foreach ($product as $key => $value) {
        $this->db->select('auctionType');
        $this->db->from('product');
        $this->db->where('product_id',$value['product_id']); 
        $query = $this->db->get();
        $res2 = $query->result_array();
        $auction_type =  $res2[0]['auctionType'];
        if($auction_type == 3){
          $this->db->where("user_id",$user_id);
          $this->db->where("product_id",$value['product_id']);
          $this->db->update("cart",['quantity'=>$value['qty']]);
        }
      }
        
    }
    public function getCartUpdatedDetails($product_id,$user_id){
        $this->db->select('CASE WHEN c.product_id IS NULL THEN "" ELSE c.product_id END as product_id,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN c.price IS NULL THEN "" ELSE c.price END as price,CASE WHEN c.quantity IS NULL THEN "" ELSE c.quantity END as quantity,(c.price * c.quantity) as subtotal',false);
        $this->db->from('cart c');
        $this->db->where('c.user_id',$user_id); 
        $this->db->where_in('c.product_id',$product_id);
        $this->db->join('product p','p.product_id=c.product_id');
        $query = $this->db->get();
        $res2 = $query->result_array();
        return $res2;
    }
    public function deleteCartDetails($product_id,$user_id){
         $this->db->where('user_id',$user_id); 
         $this->db->where('product_id',$product_id); 
         $this->db->delete('cart');
    }
    public function insertProductDetails($data,$image,$product_id=0,$quantity)
    {
      $this->db->select('count(*) as count');
      $this->db->from('product p');
      $this->db->where('p.userid',$data['userid']); 
      $this->db->where('p.product_id',$product_id);
      $this->db->where_in('p.auctionType',[1,3]);
      $query = $this->db->get();
      $res2 = $query->row();

      if(($res2->count) > 0)
      {
        $this->db->where('product_id',$product_id); 
        $this->db->update('product',$data);

        $this->db->where('product_id',$product_id); 
        $this->db->update('product_quantity',['quantity'=>$quantity,'product_id'=>$id]);
      }
      else
      {
        $this->db->insert('product',$data);
        $id = $this->db->insert_id();
      
        $this->db->insert('product_quantity',['quantity'=>$quantity,'product_id'=>$id]);
        return $id;
      }
    }
    public function save_image($images,$product_id,$user_id)
    {
      $target_path= "././assets/user_files/".$images;
      copy($target_path,"./fundraising/assets/images/products/".$images);
      copy($target_path,"./fundraising/assets/images/products/thumb".$images);
      $data1['product_id'] = $product_id;
      $data1['image_url'] = "assets/images/products/".$images;

      $this->insert_product_image($data1);

      $this->db->select('thumb')->from('product');
      $this->db->where('product_id',$product_id);
      $query = $this->db->get();
      $result = $query->row();
     
      if($result->thumb == "" || $result->thumb == NULL)
      {
        $this->make_thumbnail($user_id,"assets/images/products/".$images,$product_id);
      }

    }
    public function insert_product_image($data)
    {
       $this->db->insert('product_image',$data);
    }
    public function delete_item($user_id,$product_id)
    {
        $this->db->where('product_id',$product_id); 
        $this->db->where('userid',$user_id); 
        $this->db->delete('product');

        $this->db->where('product_id',$product_id); 
        $this->db->delete('product_image');

    }
    public function get_item_list($user_id,$auction_type)
    {
      if($auction_type == 1)
      {
        $this->db->select('p.product_id,p.userid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,,CASE WHEN p.reserveBid IS NULL THEN "" ELSE p.reserveBid END as reserveBid,CASE WHEN p.approved_status IS NULL THEN "" ELSE p.approved_status END as approved_status,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.auctionStartDateTime,p.auctionEndDateTime',false);
      }
      elseif($auction_type == 3)
      {
        $this->db->select('p.product_id,p.userid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.approved_status IS NULL THEN "" ELSE p.approved_status END as approved_status,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.auctionStartDateTime,p.auctionEndDateTime',false);
      }
      else
      {
        $this->db->select('*');
      }
      $this->db->from('product p');
      $this->db->where('p.userid',$user_id); 
      $this->db->where('p.auctionType',$auction_type);
      $query = $this->db->get();
      $res2 = $query->result_array();

      return $res2;
    }
    public function get_item_details($user_id,$auction_type,$product_id)
    {

      if($auction_type == 1)
      {
        $this->db->select('p.product_id,p.userid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.reserveBid IS NULL THEN "" ELSE p.reserveBid END as reserveBid,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,CASE WHEN p.approved_status IS NULL THEN "" ELSE p.approved_status END as approved_status,p.auctionStartDateTime,p.auctionEndDateTime',false);
      }
      elseif($auction_type == 3)
      {
        $this->db->select('p.product_id,p.userid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.approved_status IS NULL THEN "" ELSE p.approved_status END as approved_status,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,p.auctionStartDateTime,p.auctionEndDateTime,pq.quantity',false);
      }
      $this->db->from('product p');
      $this->db->join('product_quantity pq','p.product_id','pq.product_id');
      $this->db->where('p.userid',$user_id); 
      $this->db->where('p.auctionType',$auction_type);
      if($auction_type == 3)
      {
         $this->db->where('pq.product_id',$product_id);
      }
    
        $this->db->where('p.product_id',$product_id);
        $query = $this->db->get();
        $res2 = $query->row();
        
        $this->db->select('CASE WHEN pi.image_url IS NULL THEN "" ELSE pi.image_url END as image_url',false);
        $this->db->from('product_image pi');
        $this->db->where('pi.product_id',$res2->product_id); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
         
          if(!empty($res1))
          {
            $image=array_column($res1,'image_url');
            $res2->image_arr=$image;
          }
          else
          {
            $res2->image_arr="";
          }
          $thumb_image = explode('/',$res2->thumb);
          
          foreach ($res2->image_arr as $key => $value) {
            $array[$key]['name'] = $value;
            if($value == "assets/images/products/".$thumb_image[count($thumb_image)-1]){
              $array[$key]['status']  = 1;
            }else{
              $array[$key]['status']  = 0;
            }
          }
          $res2->image_arr = (!empty($array)) ? $array : [] ;
       
        
        return $res2;
    }
    public function get_product_data($user_id)
    {
        $this->db->select('Id')->from('order');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('created_date','DESC');
        $this->db->limit(1);
        $query    = $this->db->get();
        $order_id = $query->row()->Id;

        $this->db->select('*')->from('order_transaction');
        $this->db->where('order_id',$order_id);
        $query    = $this->db->get();
        $product = $query->result_array();
        return $product;
    }
    public function emptyCartValues($user_id)
    {
        $this->db->where('user_id',$user_id);
        $this->db->delete('cart');
    }

    public function confirm_order($user_id,$transaction_id,$order_status,$mode,$visibility_ids)
    {
      foreach ($visibility_ids as $key => $value) {
        $this->db->select('order_id')->from('order_transaction');
        $this->db->where('Id',$value);
        $query = $this->db->get();
        $order_id = $query->row()->order_id;
        $this->db->where('Id',$order_id); 
      $this->db->update('order',['transaction_id'=>$transaction_id,'order_status'=>$order_status,'mode'=>$mode]);
      }

     // $this->db->where('user_id',$user_id); 
      //$this->db->insert('order',['transaction_id'=>$transaction_id,'order_status'=>$order_status,'mode'=>$mode,'user_id'=>$user_id]);
    }

    public function remove_quantity($product)
    {
      foreach ($product as $key => $value) {
          $this->db->where('product_id',$value['product_id']); 
          $this->db->set('sale_qty', 'sale_qty +'.$value['qty'], FALSE);
          $this->db->update('product_quantity');
        }
    }
    public function confirm_pledge_order($user_id,$transaction_id,$order_status,$mode,$product_ids)
    {
      $this->db->where('user_id',$user_id);
      $this->db->where_in('product_id',$product_ids); 
      $this->db->update('pledge_amt_donate',['transaction_id'=>$transaction_id,'payment_status'=>1,'mode'=>$mode]);
    }
    public function save_shipping_address($user_id,$address)
    {
        $this->db->select('Id')->from('order');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('created_date','DESC');
        $this->db->limit(1);
        $query    = $this->db->get();
        $order_id = $query->row()->Id;
        $address['order_id'] = $order_id;
        $address['user_id'] = $user_id;
        $this->db->insert('ship_address',$address);
    }
    public function update_address($address,$user_id)
    {
      $this->db->where('Id',$user_id);
      $this->db->update('user',$address);
    }
   public function make_thumbnail($user_id,$image_name,$product_id)
    {
      try {
          
          $query_array = array('product_id' => $product_id,'image_url'=>$image_name);
          $image_query = $this->db->get_where('product_image', $query_array)->result();
         
          $image_url   = $image_query[0]->image_url;

          
          $query_array = array('product_id' => $product_id);
          $thumb_query = $this->db->get_where('product', $query_array)->result();
          $thumb_path  = "./".$thumb_query[0]->thumb;

          $image_path  = $image_url;
          $file_name   = str_replace("assets/images/products/", "", $image_path);

          $destination = "./fundraising/assets/images/products/thumb/".$file_name;
         
          copy("./fundraising/assets/images/products/".$file_name, $destination);

          $config['image_library']    = 'gd2';
          $config['source_image']     = $destination;
          $config['file_name']        = $file_name;
          $config['create_thumb']     = FALSE;
          $config['maintain_ratio']   = TRUE;
          $config['width']            = 309;
          $config['height']           = 309;

          $this->load->library('image_lib');
          $this->image_lib->initialize($config);
          $this->image_lib->resize();

          $new_image_path = str_replace("./", "", $destination);
          $update_data  = array('thumb' => "assets/images/products/thumb/".$file_name);
          $this->db->where('product_id', $product_id);
          $this->db->update('product', $update_data);

          unlink($thumb_path);
          return true;
      } catch (Exception $e) {
        return false;
      }
      
    }
    public function get_category_list($event_id)
    {
        $this->db->select('category')->from('product_category');
        $this->db->where('event_id',$event_id);
        $this->db->group_by('category');
        $query    = $this->db->get();
        $category = $query->result_array();
        return array_column($category, 'category');
    }

    public function getpaymentoption($organisor_id)
    {
        $this->db->select('*')->from('authorize_settings');
        $this->db->where('organisor_id',$organisor_id);
        $query    = $this->db->get();
        $category = $query->result_array();
        return $category;
    }
    public function getSubdomain($event_id)
    {
      $data = $this->db->select('Subdomain')->from('event')->where('Id',$event_id)->get()->row_array();
      return $data['Subdomain'];
    }
    public function getTemplate($slug,$event_id)
    {
        $this->db->select('*');
        $this->db->where('event_id',$event_id);
        if($slug!='')
        {
            $this->db->where('Slug',$slug);
        }
        $this->db->from('event_email_templates');
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        return $et_query_res;
    }
    public function getUserEmail($user_id)
    {
      return $this->db->select('*')->from('user')->where('Id',$user_id)->get()->row_array();
    }
    public function getNotificationTemplate($event_id,$slug)
    {
        return $this->db->select('*')->from('event_app_push_template')->where('event_id',$event_id)->where('Slug',$slug)->get()->row_array(); 
    }
    public function getEventName($event_id)
    {
      $data = $this->db->select('Event_name')->from('event')->where('Id',$event_id)->get()->row_array();
      return $data['Event_name'];
    }
}

?>