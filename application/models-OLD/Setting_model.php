<?php class Setting_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    public function saveTemplate($slug=NULL,$id=NULL,$msg)
    {
        $arrUpdate['Content']  = $msg;
        $this->db->where('Slug',$slug);
        $this->db->where('event_id', $id);
        $this->db->update('event_email_templates', $arrUpdate);
    }
    public function email_template($intEventId=NULL,$intTempId=NULL)
    {
        $this->db->select('*');
        $this->db->where('event_id',$intEventId);
        if($intTempId!='')
        {
            $this->db->where('Id',$intTempId);
        }
        $this->db->from('event_email_templates');
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        if(!$et_query_res && $intTempId)
          return redirect('Forbidden');
        return $et_query_res;
    }
    public function check_authe_url($email,$code)
    {

        $this->db->select('Id');
        $this->db->from('attendee_invitation');
        $this->db->where('Emailid',$email);
        $this->db->where('link_code',$code);
        $this->db->where('isUsedLink','0');
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        $cnt=count($et_query_res);
        return $cnt;
    }
    public function getDataFromIvAttendeeByEmailCode($email,$code)
    {

        $this->db->select('firstname,lastname');
        $this->db->from('attendee_invitation');
        $this->db->where('Emailid',$email);
        $this->db->where('link_code',$code);
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        return $et_query_res;
    }
    public function update_email_template($arrUpdate)
    {
        $arrUpdate['Subject']  = $this->input->post('Subject');    
        $arrUpdate['From']  = $this->input->post('From');    
        $arrUpdate['Content']  = $this->input->post('Content');
        $this->db->where('Id', $arrUpdate['id']);
        $this->db->where('event_id', $arrUpdate['event_id']);
        $this->db->update('event_email_templates', $arrUpdate);
        return true;
    }
    public function getnotificationsetting($eid)
    {
        $this->db->select('*');
        $this->db->from('fundraising_setting');
        $this->db->where('Event_id',$eid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    
    public function addnotificationsetting(array $arr)
    {
        $this->db->select('Event_id')->from('fundraising_setting');
        $this->db->where('Event_id',$arr['Event_id']);
        $query=$this->db->get();
        $res=$query->result_array();
        if(count($res) > 0)
        {
            $data=array('email_display'=>$arr['email_display'],
                        'pushnoti_display'=>$arr['pushnoti_display']);
            $this->db->where('Event_id',$arr['Event_id']);
            $this->db->update('fundraising_setting',$data);
        }
        else
        {   
            $this->db->insert('fundraising_setting',$arr);
        }
    }
    public function front_email_template($slug=NULL,$intEventId=NULL)
    {   
        $this->db->select('*');
        $this->db->where('event_id',$intEventId);
        if($slug!='')
        {
            $this->db->where('Slug',$slug);
        }
        $this->db->from('event_email_templates');
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        return $et_query_res;
    }
    public function back_email_template($slug=NULL)
    {
        $this->db->select('*');
        $this->db->from('email_notification_templates');
        $this->db->where('Slug',$slug);
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        return $et_query_res;
    }
    public function check_exsist_invited_exibitor($eid,$email)
    {
        $this->db->select('*');
        $this->db->from('attendee_invitation');
        $this->db->where('Event_id',$eid);
        $this->db->where('Emailid',$email);
        $this->db->where('role_status','1');
        $this->db->where('Status','0');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
}
?>
