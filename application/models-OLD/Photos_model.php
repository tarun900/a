<?php

class Photos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_eventinfo($id = null) 
    {

        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function record_sort($records, $field, $reverse = false) {
        $hash = array();

        foreach ($records as $record) {
            $hash[$record[$field]] = $record;
        }

        ($reverse) ? krsort($hash) : ksort($hash);

        $records = array();

        foreach ($hash as $record) {
            $records [] = $record;
        }

        return $records;
    }

    public function view_hangouts_public_msg($id, $start = 0, $end = 5, $msg_id = null) {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,fl.*', FALSE);
        $this->db->from('feed_img sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('feed_like fl', 'fl.post_id=sm.id','left');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or sm.Receiver_id  IS NULL');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');

        //$this->db->where('sm.Sender_id',$user[0]->Id);
        $this->db->where('sm.ispublic', '1');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Parent', '0');

        if ($msg_id != "") {
            $this->db->where('sm.id', $msg_id);
        }

        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
       
      
        foreach ($res1 as $key => $value) {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('feed_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;


        }
        //echo "<pre>";print_r($this->db->last_query());exit;
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,fl.*', FALSE);
        $this->db->from('feed_img sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('feed_like fl', 'fl.post_id=sm.id','left');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or (sm.Receiver_id IS NULL and `ispublic`="1")');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('sm.ispublic', '1');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Parent', '0');
        $query1 = $this->db->get();
        $res = $query1->result_array();

        foreach ($res as $key => $value) {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('feed_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res[$key]['comment'] = $res_comment;
        }

        if (!empty($res1)) {
            $mainarray = array_merge($res1, $res);
        } else {
            $mainarray = array_merge($res1, $res);
        }

        $airports = $this->record_sort($mainarray, "Id", true);
        $totalcount = count($airports);

        $recordarray = array();

        for ($i = $start; $i < $end; $i++) 
        {
            if ($totalcount >= $i) 
            {
                if (!empty($airports[$i])) 
                {
                    $recordarray[$i] = $airports[$i];
                }
            }
        }
        return $recordarray;
    }
    public function update_feed_like($post_id,$user_id)
    {
      $this->db->select('*');
      $this->db->from('feed_like');
      $this->db->where('user_id',$user_id);
      $this->db->where('post_id',$post_id);
      $query=$this->db->get();
      $res=$query->result_array();
      if(count($res) > 0)
      {
        $this->db->where('user_id',$user_id);
        $this->db->where('post_id',$post_id);
        $this->db->delete('feed_like');
      }
      else
      {
        $arr=array('post_id'=>$post_id,'user_id'=>$user_id,'like_date'=>date('Y-m-d H:i:s'));
        $this->db->insert('feed_like',$arr);
      }
    }
    public function get_userid_by_post($post_id)
    {
      $this->db->select('user_id');
      $this->db->from('feed_like');
      $this->db->where('post_id', $post_id);
      $query = $this->db->get();
      $res = $query->result_array();
      return $res;
    }
    public function send_speaker_message($data) {
        $this->db->insert('feed_img', $data);
        $msgid = $this->db->insert_id();
        $user = $this->session->userdata('current_user');

        foreach (json_decode($data['image']) as $imgkey => $imgvalus) {
            $dataimages = array(
                "Message" => "",
                "Sender_id" => $data['Sender_id'],
                "Receiver_id" => $data['Receiver_id'],
                "Parent" => $msgid,
                "Event_id" => $data['Event_id'],
                "Time" => $data['Time'],
                "image" => json_encode(array($imgvalus)),
                "ispublic" => $data['ispublic'],
            );
            $this->db->insert('feed_img', $dataimages);
        }
    }
    public function get_senderid($msg_id)
    {
         $this->db->select('Sender_id');
         $this->db->from('feed_img');
         $this->db->where("Id",$msg_id);
         $query=$this->db->get();
         $res=$query->result_array();
         $sender_id=$res[0]['Sender_id'];
         return $sender_id;
    }
    public function send_grp_speaker_message($data) 
    {
        $i = 0;
        $msgid;
        $user = $this->session->userdata('current_user');
        if (!empty($data['Receiver_id'])) {

            foreach ($data['Receiver_id'] as $key => $value) {
                $data1['Receiver_id'] = $value;
                $data1['Message'] = $data['Message'];
                $data1['Sender_id'] = $data['Sender_id'];
                $data1['Parent'] = $data['Parent'];
                $data1['Event_id'] = $data['Event_id'];
                $data1['Time'] = $data['Time'];
                $data1['image'] = $data['image'];
                $data1['ispublic'] = $data['ispublic'];
                $this->db->insert('feed_img', $data1);
                $msgid[$i] = $this->db->insert_id();
                $i++;
            }
            foreach (json_decode($data['image']) as $imgkey => $imgvalus) {
                $i = 0;
                foreach ($data['Receiver_id'] as $key => $value) {
                    $dataimages = array(
                        "Message" => "",
                        "Sender_id" => $data['Sender_id'],
                        "Receiver_id" => $value,
                        "Parent" => $msgid[$i],
                        "Event_id" => $data['Event_id'],
                        "Time" => $data['Time'],
                        "image" => json_encode(array($imgvalus)),
                        "ispublic" => $data['ispublic'],
                    );
                    $this->db->insert('feed_img', $dataimages);
                    $i++;
                }
            }
        } else {
            $this->db->insert('feed_img', $data);
            $msgid = $this->db->insert_id();
            foreach (json_decode($data['image']) as $imgkey => $imgvalus) {
                $dataimages = array(
                    "Message" => "",
                    "Sender_id" => $data['Sender_id'],
                    "Receiver_id" => $data['Receiver_id'],
                    "Parent" => $msgid,
                    "Event_id" => $data['Event_id'],
                    "Time" => $data['Time'],
                    "image" => json_encode(array($imgvalus)),
                    "ispublic" => $data['ispublic'],
                );
                $this->db->insert('feed_img', $dataimages);
            }
        }
    }
    public function add_comment($eventid,$array_add)
    {  

        $this->db->insert('feed_comment', $array_add);
        $insertid=$this->db->insert_id();
        $user = $this->session->userdata('current_user');
        $alluserdata=$this->commentalleventuser($eventid,$array_add['msg_id']);
       
        return $insertid;
    }
    public function commentalleventuser($eventid,$msgid)
    {    
          $user = $this->session->userdata('current_user');
        
          $this->db->select('Sender_id as User_id');
          $this->db->from('feed_img sm');
          $this->db->where('sm.Id', $msgid);
          $this->db->where('sm.Sender_id !=', $user[0]->Id);
          $query = $this->db->get();
          $res = $query->result_array();
        
          $this->db->select('Receiver_id as User_id');
          $this->db->from('feed_img sm');
          $this->db->where('sm.Id', $msgid);
          $this->db->where('sm.Receiver_id !=', $user[0]->Id);
          $query = $this->db->get();
          $res2 = $query->result_array();
         
          if(!empty($res) && !empty($res2))
          {
               $this->db->select('group_concat(distinct(user_id)) as User_id');
               $this->db->from('feed_comment sc');
               $this->db->where('sc.msg_id', $msgid);
               $this->db->where('(sc.user_id !='.$res[0]['User_id'],NULL,FALSE);
               $this->db->or_where('sc.user_id !='.$res1[0]['User_id'].')',NULL,FALSE);
               $query = $this->db->get();
               $res1 = $query->result_array();
          }
          else if(!empty($res))
          {
               $this->db->select('group_concat(distinct(user_id)) as User_id');
               $this->db->from('feed_comment sc');
               $this->db->where('sc.msg_id', $msgid);
               $this->db->where('sc.user_id !=', $res[0]['User_id']);
               $query = $this->db->get();
               $res1 = $query->result_array();
          }
          else if(!empty($res2))
          {
               $this->db->select('group_concat(distinct(user_id)) as User_id');
               $this->db->from('feed_comment sc');
               $this->db->where('sc.msg_id', $msgid);
               $this->db->where('sc.user_id !=', $res2[0]['User_id']);
               $query = $this->db->get();
               $res1 = $query->result_array();
          }     
         
        
          if(!empty($res1))
          {
               if(!empty($res) && !empty($res2))
               {
                    $sringuser=$res2[0]['User_id'].",".$res[0]['User_id'].",".$res1[0]['User_id'];
               }
               else if(!empty($res))
               {
                    $sringuser=$res[0]['User_id'].",".$res1[0]['User_id'];
               }
               else if(!empty($res2))
               {
                    $sringuser=$res2[0]['User_id'].",".$res1[0]['User_id'];
               }
               else
               {
                    $sringuser=$res1[0]['User_id'];
               }
          }
          else
          {
               $sringuser=$res[0]['User_id'];
          }
          
          if($sringuser!="")
          {
               $finalarray=  explode(',', $sringuser);
               $finalarray=  array_unique($finalarray);
               $pos = array_search($user[0]->Id, $finalarray);
               unset($finalarray[$pos]);
          }
          else
          {
               $finalarray=array();
          }
          
          return $finalarray;
    }

    public function delete_message($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('feed_img');
        
        $this->db->where('msg_id', $id);
        $this->db->delete('feed_comment');
        
        //$str = $this->db->last_query();
        //echo $str; exit();
    }
    
    public function delete_comment($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('feed_comment');
        
        $str = $this->db->last_query();
    }
    public function get_public_photomsg_by_org($org_id,$start=0,$msg_id=null)
    {
      $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,fl.*',false)->from('hub_event he');
      $this->db->join('event e','e.Id=he.event_id','left');
      $this->db->join('feed_img sm','sm.Event_id=he.event_id','left');
      $this->db->join('user u', 'u.Id=sm.Sender_id');
      $this->db->join('feed_like fl', 'fl.post_id=sm.id','left');
      $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
      $this->db->join('role r', 'r.Id =ru.Role_id');
      $this->db->join('user u1', 'u1.Id=sm.Receiver_id or sm.Receiver_id  IS NULL');
      $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
      $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
      if(empty($org_id))
      {
        $user = $this->session->userdata('current_user');
        $org_id=$user[0]->Organisor_id;
      }
      if ($msg_id != "") {
            $this->db->where('sm.id', $msg_id);
      }
      $this->db->where('e.Organisor_id',$org_id);
      $where = "FIND_IN_SET(11,`checkbox_values`) > 0";
      $this->db->where($where);
      $this->db->where('he.show_photos','1');
      $this->db->where('he.is_feture_product','1');
      $this->db->where('sm.ispublic', '1');
      $this->db->where('sm.Parent', '0');
      $this->db->group_by('sm.id');
      $this->db->order_by('sm.id desc');
      $hquery=$this->db->get();
      $res1=$hquery->result_array();
      foreach ($res1 as $key => $value) {
        $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
        $this->db->from('feed_comment sc');
        $this->db->join('user u', 'u.Id=sc.user_id');
        $this->db->where('sc.msg_id', $value['Id']);
        $query_comment = $this->db->get();
        $res_comment = $query_comment->result_array();
        $res1[$key]['comment'] = $res_comment;
      }

      $this->db->protect_identifiers = false;
      $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,fl.*', FALSE)->from('hub_event he');
      $this->db->join('event e','e.Id=he.event_id','left');
      $this->db->join('feed_img sm','sm.Event_id=he.event_id','left');
      $this->db->join('user u', 'u.Id=sm.Sender_id');
      $this->db->join('feed_like fl', 'fl.post_id=sm.id','left');
      $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
      $this->db->join('role r', 'r.Id =ru.Role_id');
      $this->db->join('user u1', 'u1.Id=sm.Receiver_id or (sm.Receiver_id IS NULL and `ispublic`="1")');
      $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
      $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
      if ($msg_id != "") {
          $this->db->where('sm.id', $msg_id);
      }
      $this->db->where('e.Organisor_id',$org_id);
      $where = "FIND_IN_SET(11,`checkbox_values`) > 0";
      $this->db->where($where);
      $this->db->where('he.show_photos','1');
      $this->db->where('sm.ispublic', '1');
      $this->db->where('sm.Parent', '0');
      $query1 = $this->db->get();
      $res = $query1->result_array();
      foreach ($res as $key => $value) {
        $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
        $this->db->from('feed_comment sc');
        $this->db->join('user u', 'u.Id=sc.user_id');
        $this->db->where('sc.msg_id', $value['Id']);
        $query_comment = $this->db->get();
        $res_comment = $query_comment->result_array();
        $res[$key]['comment'] = $res_comment;
      }
      if (!empty($res1)) {
          $mainarray = array_merge($res1, $res);
      } else {
          $mainarray = array_merge($res1, $res);
      }
      $airports = $this->record_sort($mainarray, "Id", true);
      $totalcount = count($airports);
      $airports=array_slice($airports, $start,5);
      return $airports;
    }
}

?>
