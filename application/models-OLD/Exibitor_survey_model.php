<?php
class exibitor_survey_model extends CI_Model
{
	function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function get_all_exibitor_user_questions($eid,$exuid)
	{
		$this->db->protect_identifiers=false;
		$this->db->select('eq.*')->from('exibitor_question eq');
		$this->db->where('eq.event_id',$eid);
		$this->db->where('eq.exibitor_user_id',$exuid);
		$this->db->order_by('eq.sort_order=0,eq.sort_order',NULL,false);
		$res=$this->db->get()->result_array();
		return $res;
	}
	public function save_exibitor_user_question($question_array,$qid=NULL)
	{
		if(!empty($qid))
		{
			$this->db->where('q_id',$qid);
			$this->db->update('exibitor_question',$question_array);
		}
		else
		{
			$question_array['created_date']=date('Y-m-d H:i:s');
			$this->db->insert('exibitor_question',$question_array);
		}
	}
	/*public function get_exibitor_user_edit_question($qid)
	{
		$this->db->select('eq.*')->from('exibitor_question eq');
		$this->db->where('eq.q_id',$qid);
		return $this->db->get()->row_array();
	}*/
    public function get_exibitor_user_edit_question($qid,$event_id = null,$user_id = null)
    {
        $this->db->select('eq.*')->from('exibitor_question eq');
        $this->db->where('eq.q_id',$qid);
        if($event_id!=null)
          $this->db->where('eq.event_id',$event_id);
        if($user_id!=null)
          $this->db->where('eq.exibitor_user_id',$user_id);
        $res = $this->db->get()->row_array();
        if(!$res)
            return redirect('Forbidden');
        return $res;
    }
	public function save_question_ordering($qid,$sno)
    {
        $this->db->where('q_id',$qid);
        $this->db->update('exibitor_question',array('sort_order'=>$sno));
    }
	public function update_option_in_skiplogic($qid,$old_val,$new_val)
    {
        $this->db->where('exibitor_question_id',$qid);
        $this->db->where('option',$old_val);
        $this->db->update('exibitor_question_skiplogic',array('option'=>$new_val));
    }
    public function delete_skip_login_option($qid,$old_val)
    {
        $this->db->where('exibitor_question_id',$qid);
        $this->db->where('option',$old_val);
        $this->db->delete('exibitor_question_skiplogic');
    }
    public function save_question_skiplogin($question_id,$option,$redirectquestion_id)
    {
        $this->db->select('*')->from('exibitor_question_skiplogic');
        $this->db->where('exibitor_question_id',$question_id);
        $this->db->where('option',$option);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('exibitor_question_id',$question_id);
            $this->db->where('option',$option);
            $this->db->update('exibitor_question_skiplogic',array('redirectexibitorquestion_id'=>$redirectquestion_id));
        }
        else
        {
            $logic_data['exibitor_question_id']=$question_id;
            $logic_data['option']=$option;
            $logic_data['redirectexibitorquestion_id']=$redirectquestion_id;
            $this->db->insert('exibitor_question_skiplogic',$logic_data);
        }
    }
    public function get_skip_logic_by_question_id($qid)
    {
        $this->db->select('*')->from('exibitor_question_skiplogic');
        $this->db->where('exibitor_question_id',$qid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_question_for_skiplogic_add($eid,$uid,$qid)
    {
        $this->db->select('eq.*');
        $this->db->from('exibitor_question eq');
        $this->db->where('eq.q_id NOT IN (select (case when group_concat(distinct exibitor_question_id) IS NULL Then "" ELSE group_concat(distinct exibitor_question_id) End) as exibitor_question_id from exibitor_question_skiplogic where redirectexibitorquestion_id="'.$qid.'")');
        $this->db->where('eq.q_id !=',$qid);
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$uid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function remove_exibitor_user_question($qid)
    {
        $this->db->where('q_id',$qid);
        $this->db->delete('exibitor_question');
    }
}
