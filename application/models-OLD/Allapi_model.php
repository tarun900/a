<?php 
class allapi_model extends CI_Model
{
	function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function add_cityscape_temp_exibitor($ebibitor)
	{
		$this->db->select('*')->from('cityscape_temp_exibitor');
		$this->db->where('email',$ebibitor['email']);
		$res=$this->db->get()->result_array();
		if(count($res) > 0)
		{
			$this->db->where('email',$ebibitor['email']);
			$this->db->update('cityscape_temp_exibitor',$ebibitor);
		}
		else
		{
			$this->db->insert('cityscape_temp_exibitor',$ebibitor);
		}
	}

	public function add_temp_KNECT365_exibitor($exibitors_list)
	{
		foreach ($exibitors_list as $key => $value) 
		{
			foreach ($value['organisations'] as $key => $exibitor) 
			{
				$temp_exibitor['api_user_id']=trim($exibitor['id']);
				$temp_exibitor['Heading']=trim($exibitor['name']);
				$temp_exibitor['Description']=$exibitor['profile'];
				$temp_exibitor['Logo']=$exibitor['logoUrl'];
				$temp_exibitor['linkedin_url']=$exibitor['contacts']['linkedInUrl'];
				$temp_exibitor['twitter_url']=$exibitor['contacts']['twitterUsername'];
				$temp_exibitor['facebook_url']=$exibitor['contacts']['facebookUsername'];
				$temp_exibitor['website_url']=$exibitor['contacts']['url'];
				$temp_exibitor['stand_number']=$exibitor['atEventContact']['location'];
				$temp_exibitor['Email']=trim(preg_replace("/[^A-Za-z0-9]/", "",trim($exibitor['name']))).substr(sha1(uniqid()),0,3).'@venturiapps.com';
				$this->db->insert('temp_knect_api_exibitor',$temp_exibitor);
			}
		}
	}
	public function add_KNECT365_exibitor($eid)
	{
		$org_id=17181;
		$temp_exibitors=$this->db->select('*')->from('temp_knect_api_exibitor')->limit(50)->get()->result_array();

		foreach ($temp_exibitors as $key => $value) 
		{
			$userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['api_user_id'])))->row_array();
			if(!empty($userdata))
			{
				$user_id=$userdata['Id'];
			}
			else
			{
				$user_data['Email']=$value['Email'];
				$user_data['Company_name']=$value['Heading'];
				$user_data['Created_date']=date('Y-m-d H:i:s');
				$user_data['Active']='1';
				$user_data['Organisor_id']=$org_id;
				$user_data['knect_api_user_id']=trim($value['api_user_id']);
				$this->db->insert('user',$user_data);
				$user_id=$this->db->insert_id();
			}
			unset($userdata);
			unset($user_data);

			$reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
			if(empty($reldata))
			{
				$rel_data['Event_id']=$eid;
				$rel_data['User_id']=$user_id;
				$rel_data['Organisor_id']=$org_id;
				$rel_data['Role_id']=6;
				$this->db->insert('relation_event_user',$rel_data);
				unset($rel_data);
			}
			unset($reldata);

			$exibitordata=$this->db->get_where('exibitor',array('Event_id'=>$eid,'user_id'=>$user_id))->row_array();
			$exb_data['Heading']=$value['Heading'];
			$exb_data['Description']=$value['Description'];
			$exb_data['linkedin_url']=$value['linkedin_url'];
			$exb_data['twitter_url']=$value['twitter_url'];
			$exb_data['facebook_url']=$value['facebook_url'];
			$exb_data['website_url']=$value['website_url'];
			$exb_data['stand_number']=$value['stand_number'];
			if(!empty($value['Logo']))
			{
				$logoname="company_logo".uniqid().'.png';
				copy($value['Logo'],"./assets/user_files/".$logoname);
				$exb_data['company_logo']=json_encode(array($logoname));
			}
			else
			{
				$exb_data['company_logo']=NULL;
			}
			if(!empty($exibitordata))
			{
				$this->db->where(array('Event_id'=>$eid,'user_id'=>$user_id))->update('exibitor',$exb_data);
			}
			else
			{	
				$exb_data['user_id']=$user_id;
				$exb_data['Organisor_id']=$org_id;
				$exb_data['Event_id']=$eid;
				$this->db->insert('exibitor',$exb_data);
			}
			unset($exibitordata);
			unset($exb_data);
			$this->db->where('temp_ex_id',$value['temp_ex_id'])->delete('temp_knect_api_exibitor');
		}
	}
	public function add_temp_KNECT365_speakers($speakers_list)
	{
		foreach ($speakers_list as $key => $speaker) 
		{
			$temp_speaker['api_user_id']=trim($speaker['id']);
			$temp_speaker['Salutation']=$speaker['honorific'];
			$temp_speaker['Firstname']=trim($speaker['forename']);
			$temp_speaker['Lastname']=trim($speaker['surname']);
			$temp_speaker['Title']=$speaker['jobTitle'];
			$temp_speaker['Company_name']=$speaker['company'];
			$temp_speaker['Speaker_desc']=$speaker['bio'];
			$temp_speaker['Linkedin_url']=$speaker['contacts']['linkedInUrl'];
			$temp_speaker['Twitter_url']=$speaker['contacts']['twitterUsername'];
			$temp_speaker['Facebook_url']=$speaker['contacts']['facebookUsername'];
			$temp_speaker['Website_url']=$speaker['contacts']['url'];
			$temp_speaker['Logo']=$speaker['photoUrl'];
			$temp_speaker['Email']=trim(preg_replace("/[^A-Za-z0-9]/", "",trim($speaker['company']))).substr(sha1(uniqid()),0,3).'@venturiapps.com';
			$this->db->insert('temp_knect_api_speaker',$temp_speaker);
		}
	}
	public function add_KNECT365_speakers($eid)
	{
		$org_id=17181;
		$temp_speakers=$this->db->select('*')->from('temp_knect_api_speaker')->limit(50)->get()->result_array();
		foreach ($temp_speakers as $key => $value) 
		{
			$userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['api_user_id'])))->row_array();
			$user_data['Salutation']=$value['Salutation'];
			$user_data['Firstname']=$value['Firstname'];
			$user_data['Lastname']=$value['Lastname'];
			$user_data['Email']=$value['Email'];
			$user_data['Title']=$value['Title'];
			$user_data['Company_name']=$value['Company_name'];
			if(!empty($value['Logo']))
			{
				$logoname="company_logo".uniqid().'.png';
				copy($value['Logo'],"./assets/user_files/".$logoname);
				$user_data['Logo']=$logoname;
			}
			else
			{
				$user_data['Logo']=NULL;
			}
			$user_data['Speaker_desc']=$value['Speaker_desc'];	
			
			if(!empty($userdata))
			{
				$user_id=$userdata['Id'];
				$this->db->where('Id',$user_id);
				$this->db->update('user',$user_data);
			}
			else
			{
				$user_data['Created_date']=date('Y-m-d H:i:s');
				$user_data['Active']='1';
				$user_data['Organisor_id']=$org_id;
				$user_data['knect_api_user_id']=trim($value['api_user_id']);
				$this->db->insert('user',$user_data);
				$user_id=$this->db->insert_id();
			}
			unset($userdata);
			unset($user_data);


			$reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
			if(empty($reldata))
			{
				$rel_data['Event_id']=$eid;
				$rel_data['User_id']=$user_id;
				$rel_data['Organisor_id']=$org_id;
				$rel_data['Role_id']=7;
				$this->db->insert('relation_event_user',$rel_data);
			}

			unset($reldata);
			unset($rel_data);

			$sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
			$social_link_data['Linkedin_url']=$value['Linkedin_url'];
			$social_link_data['Twitter_url']=$value['Twitter_url'];
			$social_link_data['Facebook_url']=$value['Facebook_url'];
			$social_link_data['Website_url']=$value['Website_url'];
			if(empty($sociallinkdata))
			{
				$social_link_data['User_id']=$user_id;
				$social_link_data['Event_id']=$eid;
				$this->db->insert('user_social_links',$social_link_data);
			}
			else
			{
				$this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
			}
			unset($sociallinkdata);
			unset($social_link_data);
			$this->db->where('temp_sp_id',$value['temp_sp_id'])->delete('temp_knect_api_speaker');
		}
	}
	public function add_temp_KNECT365_session($sessions_list)
	{
		foreach ($sessions_list['days'] as $key => $sessions) 
		{
			foreach ($sessions['sessions'] as $key => $session) 
			{
				$temp_session['api_session_id']=trim($session['id']);
				$temp_session['Start_date']=$sessions['date'];
				$temp_session['Start_time']=$session['startTime'];
				$temp_session['End_date']=$sessions['date'];
				$temp_session['End_time']=$session['endTime'];
				$temp_session['Heading']=$session['title'];
				$temp_session['description']=$session['description'];
				$temp_session['Types']=$session['stream'];
				$temp_session['speakers'] = count($session['speakers']) > 0 ? json_encode($session['speakers']) : NULL;
				$this->db->insert('temp_knect_api_session',$temp_session);
				//lq();
			}
		}
	}
	public function add_KNECT365_session($eid,$Agenda_id)
	{
		$org_id=17181;
		$temp_session=$this->db->select('*')->from('temp_knect_api_session')->limit(50)->get()->result_array();
		foreach ($temp_session as $key => $value) 
		{
			if(!empty($value['speakers']))
			{
				$speaker=array();
				foreach (json_decode($value['speakers']) as $skey => $svalue) {
					$sidarr=$this->db->get_where('user',array('knect_api_user_id'=>$svalue->speakerId))->row_array();
					if(!empty($sidarr['Id']))
					{
						array_push($speaker,$sidarr['Id']);
					}
				}
				$session_data['Speaker_id']=implode(",", $speaker);
			}
			else
			{
				$session_data['Speaker_id']=NULL;
			}
			$session_data['Start_date']=$value['Start_date'];
			$session_data['Start_time']=$value['Start_time'];
			$session_data['End_date']=$value['End_date'];
			$session_data['End_time']=$value['End_time'];
			$session_data['Heading']=trim($value['Heading']);
			$session_data['description']=$value['description'];
			$typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Types']),'event_id'=>$eid))->row_array();
			if(!empty($typedata))
			{
				$type_id=$typedata['type_id'];
			}
			elseif(trim($value['Types']) == '')
			{
				//$type_id= ($eid == 936) ? 1780 : 1779;
				if($eid == 1235)
					$type_id = 2346  ;
			}
			else
			{
				$type_data['type_name']=trim($value['Types']);
				$type_data['event_id']=$eid;
				$type_data['created_date']=date('Y-m-d H:i:s');	
				$this->db->insert('session_types',$type_data);
				$type_id=$this->db->insert_id();
			}
			$session_data['Types']=$type_id;
			$sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['api_session_id']))->get()->row_array();
			if(!empty($sessiondata))
			{
				$this->db->where('Event_id',$eid)->where('agenda_code',trim($value['api_session_id']))->update('agenda',$session_data);
				$session_id=$sessiondata['Id'];
			}
			else
			{
				$session_data['Event_id']=$eid;
				$session_data['Organisor_id']=$org_id;
				$session_data['agenda_code']=trim($value['api_session_id']);
				$session_data['Agenda_status']='1';
				$session_data['created_date']=date('Y-m-d H:i:s');
				$this->db->insert('agenda',$session_data);
				$session_id=$this->db->insert_id();
			}
			unset($sessiondata);
			unset($session_data);

			$reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
			if(empty($reldata))
			{
				$rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
				$this->db->insert('agenda_category_relation',$rel_data);
			}
			unset($reldata);
			unset($rel_data);
			$this->db->where('temp_sess_id',$value['temp_sess_id'])->delete('temp_knect_api_session');
		}
	}
}