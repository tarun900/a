<?php
class Client_model extends CI_Model{
	function __construct()
	{
	        parent::__construct();
	}

	public function get_client_list($id=null)
	{
		$this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
		$this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
		if($id)
		{
			$this->db->where('u.Id',$id);
		}
        $this->db->where('r.Id',3);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
	}
        
    public function add_client($array_add)
    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id',3);
        $query = $this->db->get();
        $result = $query->result_array();
        $array_add['Role_id'] = $result[0]['Id'];
        $array_add['Password'] = md5($array_add['Password']);
        $this->db->insert('user', $array_add);
        $this->session->set_flashdata('client_data', 'Added');
        redirect('client');
    }
        
    public function delete_client($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('user');
    }

    public function checkemail($email,$id=null,$eventid=null)
    {

        $this->db->select('u.Id as uid');
        $this->db->from(' user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        if($id!=NULL)
        {
            $this->db->where('u.Id !=',$id);
        }   
        
        if($eventid != NULL)
        {
             $this->db->where('ru.Event_id =',$eventid);
        }
        
        $this->db->where('u.Email',$email);            
        $query = $this->db->get();
        $res = $query->result_array();
    
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $email1=$res1[0]['Email'];
        if(count($res)>=1)
        {
          if($email1==$email)
          {
            return false;
          }
           else
           {
            return true;
           }
        }
        
        else
        {
            return false;
        }
    }
    
    
    public function get_recent_client_list($id=null)
    {
		$this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
		$this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
		if($id)
		{
			$this->db->where('u.Id',$id);
		}
        $this->db->where('r.Id',3);
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
		$res = $query->result_array();
        return $res;
   }
        
  }
      
?>
