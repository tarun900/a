<?php
class Document_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        $this->db->protect_identifiers=false;
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getFoldersByEventId($event_id,$lang_id='')
    {
        $this->db->select('CASE WHEN d.id IS NULL THEN "" ELSE d.id END as doc_id,
                           CASE WHEN d.doc_type IS NULL THEN "" ELSE d.doc_type END as doc_type,
                           (CASE WHEN elmc.title IS NULL THEN  CASE WHEN d.title IS NULL THEN "" ELSE d.title END ELSE elmc.title END) as title,
                           CASE WHEN d.docicon IS NULL THEN "" ELSE d.docicon END as docicon,
                           count(dp.id) as count',false);
        $this->db->from('documents d');
        $this->db->join('documents dp','dp.parent=d.id and dp.parent IS NOT NULL','left');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=16 and elmc.modules_id=d.id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('d.Event_id',$event_id);
        $this->db->where('d.doc_type','0');
        $this->db->group_by('d.id');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function getFilesByEventId($event_id,$doc_id=NULL,$lang_id='')
    {
        $this->db->select('CASE WHEN d.id IS NULL THEN "" ELSE d.id END as doc_id,
                           CASE WHEN d.doc_type IS NULL THEN "" ELSE d.doc_type END as doc_type,
                          (CASE WHEN elmc.title IS NULL THEN  CASE WHEN d.title IS NULL THEN "" ELSE d.title END ELSE elmc.title END) as title,
                           CASE WHEN df.document_file IS NULL THEN "" ELSE df.document_file END as document_file,
                           CASE WHEN d.docicon IS NULL THEN "" ELSE d.docicon END as docicon',false);
        $this->db->from('documents d');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=16 and elmc.modules_id=d.id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join("document_files df","df.document_id=d.id");
        $this->db->where('d.Event_id',$event_id);
        $this->db->where('d.doc_type','1');
        if($doc_id!='')
        {
            $this->db->where('d.parent',$doc_id);
        }
        else
        {
            $this->db->where('d.parent',0);
        }
        
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function getDocuments($event_id,$lang_id='',$parent='',$keyword='')
    {
        $this->db->select('CASE WHEN d.id IS NULL THEN "" ELSE d.id END as doc_id,
                           CASE WHEN d.doc_type IS NULL THEN "" ELSE d.doc_type END as doc_type,
                           (CASE WHEN elmc.title IS NULL THEN  CASE WHEN d.title IS NULL THEN "" ELSE d.title END ELSE elmc.title END) as title,
                           CASE WHEN d.docicon IS NULL THEN "" ELSE d.docicon END as docicon,
                           CASE WHEN df.document_file IS NULL THEN "" ELSE df.document_file END as document_file,
                           count(dp.id) as count',false);
        $this->db->from('documents d');
        $this->db->join('documents dp','dp.parent=d.id and dp.parent IS NOT NULL','left');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id=16 and elmc.modules_id=d.id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->join("document_files df","df.document_id=d.id",'left');
        $this->db->where('d.Event_id',$event_id);
        $this->db->group_by('d.id');
        $this->db->order_by('d.doc_type!=0,d.doc_type asc',NULL,FALSE);
        if(!empty($keyword) && $parent == '0')
        {
            $this->db->like('d.title',$keyword);
        }
        elseif(!empty($keyword))
        {   
            $this->db->like('d.title',$keyword);
            $this->db->where('d.parent',$parent);
        }
        else
        {
            $this->db->where('d.parent',$parent);
        }
        $this->db->order_by('d.sort_order=0,d.sort_order asc',NULL,FALSE);
        $this->db->order_by('d.title',NULL,FALSE);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
}
        
?>
