<?php
class Attendee_model extends CI_Model{
    function __construct()
    {   
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);
    }
    public function getAttendeeListByEventId_new($Event_id,$where,$page_no,$user_id,$filter_keywords)
    {   

        if($Event_id == '1114')
        {
            $regUserIds = $this->getRegisterdUserIds($Event_id);

        }
        
       
        $this->db->select('*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        if(!empty($filter_keywords))
        {
             $filter_keywords = "'" . str_replace(array("'", ","), array("'", "','"), $filter_keywords) . "'";
            $this->db->where('u.Id in (select user_id from attendee_keywords where keyword in ('.$filter_keywords.') and event_id = '.$Event_id.')');
        }
        if($Event_id == '1114')
          $this->db->where_in('u.Id',$regUserIds);
        $this->db->where('r.Id',4);
        $total = $this->db->get()->num_rows();

        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->protect_identifiers=false;
        #123

        if($Event_id == '1397')
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,ac.value',FALSE);
            $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
            $this->db->join('user u', 'u.Id = ru.User_id');
            $this->db->join('role r','ru.Role_id = r.Id');
            $this->db->join('attendee_extra_column ac','ac.user_id = u.Id and ac.key1="Country" and ac.event_id=e.Id');  
        }
        else
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
            $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
            $this->db->join('user u', 'u.Id = ru.User_id');
            $this->db->join('role r','ru.Role_id = r.Id');
        }

        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $this->db->where('u.Firstname !=""');

        if(!empty($filter_keywords))
        {   
            // $filter_keywords = "'" . str_replace(array("'", ","), array("\\'", "','"), $filter_keywords) . "'";
            $this->db->where('u.Id in (select user_id from attendee_keywords where keyword in ('.$filter_keywords.') and event_id = '.$Event_id.')');
        }
        if($Event_id == '1114')
          $this->db->where_in('u.Id',$regUserIds);
        $this->db->where('u.Id not in (select Attendee_id from event_attendee where Event_id='.$Event_id.' and hide_identity="1")');
        #123

        $this->db->order_by("concat(u.Firstname,' ',u.Lastname)");
        $this->db->limit($limit, $start);
        $this->db->group_by('u.Id');
        $query = $this->db->get('event e');
        $res = $query->result();
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            if(!empty($res[$i]->value))
            {
                $attendees[$i]['Company_name']=$res[$i]->Company_name.', '.$res[$i]->value;
            }
            else
            {
                $attendees[$i]['Company_name']=$res[$i]->Company_name;
            }
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','2')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $attendees[$i]['is_favorites']=($count) ? '1' : '0';
           
        }
        $data['attendees'] = $attendees;
        $total_page     = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    } 
    public function getAttendeeListByEventId($Event_id,$user_id)
    {
       
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result();  
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('module_type','2')->where('user_id',$user_id)->where('module_id',$res[$i]->Id)->get()->num_rows();
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name']=$res[$i]->Company_name;
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','2')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $attendees[$i]['is_favorites']=($count) ? '1' : '0';
            $attendees[$i]['Heading']="";
        }
        return $attendees;
    } 
    public function getNewAttendeeListByEventId($Event_id,$user_id)
    {
        // remove unused fields for the listing screen
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,"2" as type,
                        CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result_array();  
       // $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $res[$i]['Firstname']=ucfirst($res[$i]['Firstname']);
            $res[$i]['Lastname']=ucfirst($res[$i]['Lastname']);
            $res[$i]['Heading']="";
        }
        return $res;
    } 

    public function getAttendeemyfav($attendee_id,$event_id,$user_id)
    {
        $this->db->select('CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Id,
                           CASE WHEN m.module_id IS NULL THEN "" ELSE m.module_id END as module_id,
                           CASE WHEN m.user_id IS NULL THEN "" ELSE m.user_id END as user_id,
                           CASE WHEN m.event_id IS NULL THEN "" ELSE m.event_id END as event_id',FALSE);
        $this->db->from('my_favorites m');
        $this->db->where('m.module_id',$attendee_id);
        $this->db->where('m.user_id',$user_id);
        $this->db->where('m.module_type','2');
        $this->db->where('m.event_id',$event_id);
        $query = $this->db->get();
        $res = $query->result();  
        return $res;
    }

    public function getAttendeeDetails($attendee_id,$event_id)
    {   
        if($event_id == '1397')
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Biography,ac.value',FALSE);
            $this->db->join('attendee_extra_column ac','ac.user_id = u.Id and ac.key1="Country" and ac.event_id='.$event_id);  
        }
        else
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Biography',FALSE);
        }
        $this->db->where('u.Id',$attendee_id);
        $query = $this->db->get('user u');
        $res = $query->result();
        foreach ($res as $key => $value)
        {
            $res[$key]->Biography = strip_tags($value->Biography);
            if(!empty($value->value))
            {
                $res[$key]->Company_name .=', '.$value->value;
            }
        }  
        return $res;
    }

    public function getSocialLinks($attendee_id)
    {
        $this->db->select('CASE WHEN Facebook_url IS NULL THEN "" ELSE Facebook_url END as Facebook_url,
                          CASE WHEN Twitter_url IS NULL THEN "" ELSE Twitter_url END as Twitter_url,
                          CASE WHEN Linkedin_url IS NULL THEN "" ELSE Linkedin_url END as Linkedin_url,
                          CASE WHEN Instagram_url IS NULL THEN "" ELSE Instagram_url END as Instagram_url,
                          CASE WHEN Youtube_url IS NULL THEN "" ELSE Youtube_url END as Youtube_url',FALSE);
        $this->db->from('user_social_links');
        $this->db->where('User_id',$attendee_id);
        $query = $this->db->get();
        $res = $query->result();  
        return $res;
    }

    public function getContactAttendeeListByEventId($event_id,$user_id=0)
    {
        $user_id = ($user_id=='') ? '0' : $user_id;
        $this->db->select('*,u.Id as uid,asc2.contact_type',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." ",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id.")";
        $this->db->where($wher);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares1 = $aquery->result();

        $this->db->select('*,u.Id as uid,asc1.contact_type',false)->from('user u');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." ",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares2 = $aquery->result();
        $ares = array_merge($ares1,$ares2);

        $attendees = array();
        for($i=0; $i<count($ares); $i++)
        {
            $prev="";
            $fc = strtoupper($ares[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares[$i]->Lastname!=$prev)
            {
                $prev=$ares[$i]->Start_date;
                $data['Id']=$ares[$i]->uid;
                $data['Firstname']=$ares[$i]->Firstname;
                $data['Lastname']=$ares[$i]->Lastname;
                $data['Company_name']=$ares[$i]->Company_name;
                $data['Title']=$ares[$i]->Title;
                $data['Email']=$ares[$i]->Email;
                $data['type']=$ares[$i]->contact_type;
                
                $ex = $this->db->select('*')->from('exibitor')->where('Event_id',$event_id)->where('user_id',$ares[$i]->uid)->get()->row_array();
                if(!empty($ex))
                {
                    $data['type']='1';
                    $data['exhibitor_id']=$ex['user_id'];
                    $data['exhibitor_page_id']=$ex['Id'];
                }
                else
                {
                    $data['type']='0';
                    $data['exhibitor_id']='';
                    $data['exhibitor_page_id']='';
                }
                $data['Logo']=$ares[$i]->Logo;
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('module_type','2')->where('user_id',$user_id)->where('module_id',$ares[$i]->Id)->get()->num_rows();
                $data['is_favorites']=($count) ? '1' : '0';
                array_walk($data,function(&$item){$item=strval($item);});
                $attendees[] = $data;
            }
        }
        return $attendees;
    }

    public function getContactAttendeeListByEventId_new($event_id,$user_id=0,$where,$page_no)
    {
        
        $user_id = ($user_id=='') ? '0' : $user_id;

        /*$this->db->select('*')->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." AND asc2.approval_status!='2'",'left');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." AND asc1.approval_status='1'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id." OR asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        if($where!='')
            $this->db->where($where);
        $total = $this->db->get()->num_rows();*/


        /*$limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;*/

        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." AND asc2.approval_status!='2'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id.")";
        $this->db->where($wher);
        if($where!='')
            $this->db->where($where);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares1 = $aquery->result_array();

        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." AND asc1.approval_status='1'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        if($where!='')
            $this->db->where($where);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares2 = $aquery->result_array();

        $ares_data = array_merge($ares1,$ares2);
           
        $attendees = array();
        foreach($ares_data as $ares)
        {
            $prev="";
            $fc = strtoupper($ares['Lastname']);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares['Lastname']!=$prev)
            {
                $prev=$ares['Start_date'];
                $data['Id']=$ares['uid'];
                $data['Firstname']=$ares['Firstname'];
                $data['Lastname']=$ares['Lastname'];
                $data['Company_name']=$ares['Company_name'];
                $data['Title']=$ares['Title'];
                $data['Email']=$ares['Email'];
                $data['Logo']=$ares['Logo'];
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('user_id',$user_id)->where('module_type','2')->where('module_id',$ares['uid'])->get()->num_rows();
                $data['is_favorites']=($count) ? '1' : '0';
                array_walk($data,function(&$item){$item=strval($item);});
                $attendees[] = $data;
            }
        }

        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $total          = count($attendees);
        $total_page     = ceil($total/$limit);
        $attendees      = array_slice($attendees,$start,$limit);

        $data['attendees'] = $attendees;
        $data['total'] = $total_page;
        return $data;
    }
    public function getApprovalStatus($where,$event_id)
    {
        $test = '2';
        $result = $this->db->select('*')->from('attendee_share_contact')->where($where)->where('event_id',$event_id)->get()->row_array();
       
        $result2 = $this->db->select('*')->from('attendee_share_contact')->where('from_id',$where['to_id'])->where('to_id',$where['from_id'])->where('event_id',$event_id)->get()->row_array();
           
        if(!empty($result2))
        {
            if($result2['approval_status'] == '0')
                $test = '3';
            else
                $test = '1';
        }

        return (!empty($result)) ? $result['approval_status'] : $test;
    }
    public function saveShareContactInformation($where,$share_data)
    {
        $result = $this->db->select('*')->from('attendee_share_contact')->where($where)->get()->row_array();
        if(!count($result))
        {
            $share_data['share_contact_datetime']=date('Y-m-d H:i:s');
            $this->db->insert('attendee_share_contact',$share_data);
            $contact_id = $this->db->insert_id();
        }
        else
        {
            $this->db->where($where);
            $this->db->update('attendee_share_contact',$share_data);
            $contact_id=$result['contact_id'];
        }
        return $contact_id;
    }
    public function getUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Attendee/'.$data['acc_name'].'/'.$data['Subdomain'].'/update_approval_status/';
    }
    public function getUser($user_id)
    {
        return $this->db->select('u.*,ru.Role_id')->from('user u')->join('relation_event_user ru','ru.User_id = u.Id')->where('u.Id',$user_id)->get()->row_array();
    }
    public function getShareDetails($where,$event_id)
    {
        $attenddess = $this->db->select('*')->from('attendee_share_contact')->where($where)->where('event_id',$event_id)->where('approval_status','0')->get()->result_array();

        foreach ($attenddess as $key => $value) 
        {
          $user = $this->db->select('u.Id as attendee_id,u.Email,u.Country,u.Mobile as phone_no')->from('user u')->where('u.Id',$value['from_id'])->get()->row_array();
          $country = $this->db->select('*')->from('country')->where('id',$user['Country'])->get()->row_array();

          $data[$key]['attendee_id'] = $user['attendee_id'];
          $data[$key]['Email'] = $user['Email'];
          $data[$key]['country_name'] = ($country['country_name']) ? $country['country_name'] : '';
          $data[$key]['phone_no'] = ($user['phone_no']) ? $user['phone_no'] : '';
        }
         
        return ($data) ? $data : [];
    }
    public function getAttendeeConatctDetails($attendee_id)
    {
       $data = $this->db->select('Email,Mobile,Id,Country')->from('user')->where('Id',$attendee_id)->get()->row_array();

       //$data = $this->db->select('e.main_email_address as Email,u.Mobile,u.Id,u.Country')->from('user u')->join('exibitor e','e.user_id = u.Id')->where('u.Id',$attendee_id)->get()->row_array();

       $country = $this->db->select('country_name')->from('country')->where('id',$data['Country'])->get()->row_array();
       $data['Country'] = $country['country_name'];
       array_walk($data,function(&$item){$item=strval($item);});
       $extra = $this->db->select('json_data')->from('signup_form_data')->where('user_id',$attendee_id)->get()->row_array();
       $extra = json_decode($extra['json_data'],true);
       $temp = [];
       foreach ($extra as $key => $value) 
       {
           $key = ($key == "untitled") ? "" : $key;
           $key = ucfirst($key);
           $key = str_replace('_', " ", $key);
           $extra_data['key']   = $key;
           $extra_data['value_type'] = (gettype($value) == "array") ? '1' : '0';
           $extra_data['value'] = $value;
           $temp[] = $extra_data;
       }
       $data['extra'] = $temp;
       $array[] = $data;
       return $array;
    }
    public function getAllowAttendee($attendee_id,$event_id)
    {
        $data = $this->db->select('CASE WHEN allow_contact_me IS NULL THEN "" ELSE allow_contact_me END as allow_contact_me',false)->from('event_attendee')->where('Attendee_id',$attendee_id)->where('Event_id',$event_id)->get()->row_array();

        return $data;
    }
    public function saveRequest($data)
    {   

        if($data['event_id'] == '993' && empty($data['location']))
        {
            $res['msg'] = "The meeting location is not available at the specified time. Please try a different time.";
            $res['status'] = false;
            return $res;
        }
        else
        {

            date_default_timezone_set('UTC');
            $status = ['0','1'];
            /*$num = $this->db->select('*')->from('exhibitor_attendee_meeting')
                            ->where('event_id',$data['event_id'])
                            ->where('location',$data['location'])
                            ->where('date',$data['date'])
                            ->where('time',$data['time'])
                            ->where_in('status',$status)
                            ->get()
                            ->num_rows();*/
            /*$num = '1';
            if(!$num)
            {*/ 

                $tmp_data = $data;
                unset($tmp_data['attendee_id']);
                unset($tmp_data['location']);

                $check_avibl = $this->db->where($tmp_data)->get('exhibitor_attendee_meeting')->num_rows();
                if($check_avibl)
                {   
                    $reciver_id = ($data['recever_attendee_id']) ?: $data['recipient_attendee_id'];
                    $user = $this->db->where('Id',$reciver_id)->get('user')->row_array();
                    $res['status'] = false;
                    $res['msg'] = $user['Firstname'].' '.$user['Lastname'].' is unavailable at this time, please choose another time.';
                }
                else
                {
                      
                    $tmp_location = $data['location'];
                    unset($data['location']);
                    $count = $this->db->select('*')->from('exhibitor_attendee_meeting')->where('status !=','2')->where($data)->get()->num_rows();
                    if(!$count)
                    {   
                        $data['location'] = $tmp_location;
                        $data['created_datetime'] = date('Y-m-d H:i:s');
                        $this->db->insert('exhibitor_attendee_meeting',$data);
                        $res['status'] = true;
                    }
                    else
                    {               
                        $res['msg'] = "You have already set meeting with this time and date.";
                        $res['status'] = false;
                    }
                }

            /*}
            else
            {   
                unset($data['attendee_id']);
                unset($data['location']);

                $check_avibl = $this->db->where($data)->get('exhibitor_attendee_meeting')->num_rows();

                $res['status'] = false;
                if($check_avibl)
                {   
                    $reciver_id = ($data['recever_attendee_id']) ?: $data['recipient_attendee_id'];
                    $user = $this->db->where('Id',$reciver_id)->get('user')->row_array();
                    $res['msg'] = $user['Firstname'].' '.$user['Lastname'].' is unavailable at this time, please choose another time.';
                }
                else
                {
                    $res['msg'] = "This meeting location is not available at the specified time. Please try a different time or a different Meeting Location.";
                }   
            }*/
        }
        return $res;  
    } 
    public function saveSpeakerMessage($data)
    {
        $this->db->insert("speaker_msg",$data);
        $message_id=$this->db->insert_id();
        if($message_id!='')
        {
             $data1 = array(
                    'msg_id' => $message_id,
                    'resiver_id' => $data['Receiver_id'],
                    'sender_id' => $data['Sender_id'],
                    'event_id' => $data['Event_id'],
                    'isread' => '1',
                    'type' => '0'
                );
            $this->db->insert('msg_notifiy', $data1);
            return $message_id;
        }
        else
        {
            return 0;
        }
    }
    public function notifypublic($eventid,$user_id)
    {    
          $this->db->select('group_concat(ru.User_id) as User_id');
          $this->db->from('relation_event_user ru');
          $this->db->where('ru.Event_id', $eventid);
          $this->db->where('ru.User_id !=',$user_id);
          $query = $this->db->get();
          $res1 = $query->result_array();
          
          $finalarray=array();
          if($res1[0]['User_id']!="")
          {
               $finalarray=  explode(',', $res1[0]['User_id']);
               $finalarray=  array_unique($finalarray);
          }
          return $finalarray;
    }
    public function getEventName($event_id)
    {
        $data = $this->db->select('Event_name')->from('event')->where('Id',$event_id)->get()->row_array();
        return $data['Event_name'];
    }

    public function getAllMeetingRequest($event_id,$user_id)
    {
        $data1 =   $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                    CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                    eam.date,eam.time,eam.status,eam.Id as request_id,
                                    CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                    CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                    CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id OR u.Id = eam.sender_exhibitor_id')->where('eam.recever_attendee_id',$user_id)->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();

        $data2 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,eam.time,eam.status,eam.Id as request_id,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.recever_attendee_id')->where('eam.attendee_id',$user_id)->where('eam.event_id',$event_id)->where('eam.status ','1')->order_by('eam.Id','DESC')->get()->result_array();

       
        $data3 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.time,
                                  eam.status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')
                                  ->join('exibitor e','e.Id = eam.exhibiotor_id')
                                  ->join('user u','u.Id = e.user_id')
                                  ->where('eam.attendee_id',$user_id)
                                  ->where('eam.event_id',$event_id)
                                  ->where('eam.status ','1')
                                  ->order_by('eam.Id','DESC')->get()->result_array();

        return array_merge($data2,$data3,$data1);
       
    }
    public function getAllMeetingRequest1($event_id,$user_id)
    {
        $data = $this->db->select('e.Id')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('u.token',$user_id)->where('e.event_id',$event_id)->get()->row_array();

        return $this->db->select('u.Firstname,u.Lastname,DATE_FORMAT(eam.date,"%d/%m/%Y") as date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
    }
    public function updateRequest($update_data,$where)
    {
        $this->db->where($where);
        $this->db->update('exhibitor_attendee_meeting',$update_data);
        if($update_data['status'] == '1')
        {
            $data['event_id'] = $ex['event_id'];
            $data['user_id'] = $ex['attendee_id'];
            $data['modules_id'] = '0';
            $data['reminder_time'] = '15';
            $data['read_status'] = '0';
            $data['created_date'] = date('Y-m-d H:i:s');
        }
        return $this->db->select('*')->from('exhibitor_attendee_meeting')->where($where)->get()->row_array();
    }
    public function getUsersData($id)
    {
        return $this->db->select('*')->from('user')->where('Id',$id)->get()->row_array();
    }  
    public function getDateTimeArray($event_id)
    {
        $data = $this->db->where('Id',$event_id)->get('event')->row_array();
        $data1 = $this->db->select('format_time')->where('Event_id',$event_id)->get('fundraising_setting')->row_array();
        
        if(strtotime($data['Start_date']) > time())
            $start = date('Y-m-d',strtotime($data['Start_date']));
        else
            $start = date('Y-m-d');

        $time1 = date('H:i',strtotime($data['Start_time']));
        $time2 = date('H:i',strtotime($data['End_time']));

        while($start <= $data['End_date'])
        {   
            if($data['date_format'] == 0)
            {
                $date[] = date('d-m-Y',strtotime($start));
            }
            else
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }

        $time=range(strtotime(date($data['meeting_start_time'])),strtotime($data['meeting_end_time']),$data['meeting_time_slot']*60);

        $format = ($data1['format_time'] == '0') ? 'h:i A' : 'H:i';

        foreach ($time as $key => $value) {
           if((date('H:i',$value)) == "00:00")
               continue;
           $time_arr[] = date($format,$value);
        }
        $result['date'] = $date;
        $result['time'] = $time_arr;
        return $result;
    }
    public function checkSessionClash($attendee_id,$date,$time)
    {
        $result['result'] = true;
        $data = $this->db->select('*')->from('users_agenda')->where('user_id',$attendee_id)->get()->row_array();
        $agenda_ids = explode(',',$data['agenda_id']);
        $agenda = $this->db->select('*')->from('agenda')->where_in('Id',$agenda_ids)->get()->result_array();
        $d_date = strtotime($date);
        $time = strtotime($time);
        foreach ($agenda as $key => $value) 
        {
           $start_date = strtotime($value['Start_date']);
           $end_date = strtotime($value['End_date']);
           $start_time = strtotime($value['Start_time']);
           $end_time = strtotime($value['End_time']);
           if($d_date >= $start_date && $d_date <= $end_date)
           {
                $result['result'] = false;
                $result['agenda_name'] = $value['Heading'];
                break;
           }
        }
        return $result;
    } 

    public function checkEventDateFormat($event_id)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        $res=$query->result_array();
        return $res;
    }  

    public function getExhibitor($user_id,$event_id)
    {
        return $this->db->select('*')->from('exibitor')->where('user_id',$user_id)->where('Event_id',$event_id)->get()->row_array();
    }

    public function getEvenetData($event_id)
    {
        return $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
    }
    public function saveSuggestedDate($arr)
    {
        $this->db->insert('suggest_meeting',$arr);
    }
    public function getAvailableTimes($where)
    {
        return $this->db->select('sm.Id,date_time,Heading')->from('suggest_meeting sm')->join('exibitor e','e.user_id = sm.exhibitor_user_id')->where($where)->get()->result_array();
    }
    public function getSuggestUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Exhibitors/'.$data['acc_name'].'/'.$data['Subdomain'].'/';
    }
    public function check_moderator($event_id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('moderator_relation');
        $this->db->where('user_id',$user_id);
        $this->db->where('event_id',$event_id);
        $data = $this->db->get()->row_array();
        return (!empty($data) ? $data : false);
    }
    public function getAllMeetingRequestModerator($event_id,$user_id)
    {
        $data1 =   $this->db->select('eam.date,
                                      eam.time,
                                      eam.status,
                                      eam.Id as request_id,
                                      CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                      CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.attendee_id END as sender_id,
                                      CASE WHEN eam.recipient_attendee_id IS NULL THEN "" ELSE eam.recipient_attendee_id END as receiver_id',false)
        ->from('exhibitor_attendee_meeting eam')
        ->where('eam.moderator_id',$user_id)
        ->where('eam.event_id',$event_id)
        ->where('eam.status != ','2')
        ->where('eam.recipient_attendee_id !=','NULL')
        ->order_by('eam.Id','DESC')->get()->result_array();
        
        $format_time = $this->db->select('format_time')->where('Event_id',$event_id)->get('fundraising_setting')->row_array()['format_time'];
        $format['format'] = ($format_time == '0') ? 'h:i A' : 'H:i:s';

        foreach ($data1 as $key => $value)
        {
            $tmp = $this->getAttendeeDetails($value['sender_id']);
            $data1[$key]['sender_name'] = $tmp[0]->Firstname." ".$tmp[0]->Lastname;
            $tmp = $this->getAttendeeDetails($value['receiver_id']);
            $data1[$key]['reciver_name'] = $tmp[0]->Firstname." ".$tmp[0]->Lastname;
            $data1[$key]['time_new'] = date($format['format'],strtotime($value['time']));
        }
        return $data1;
    }
    public function updateRequestModerator($update_data,$where)
    {
        $this->db->where($where);
        $this->db->update('exhibitor_attendee_meeting',$update_data);
        if(!empty($update_data['status']))
        {
            if($update_data['status'] == '1')
            {
                $data['event_id'] = $ex['event_id'];
                $data['user_id'] = $ex['attendee_id'];
                $data['modules_id'] = '0';
                $data['reminder_time'] = '15';
                $data['read_status'] = '0';
                $data['created_date'] = date('Y-m-d H:i:s');
            }
        }
        return $this->db->select('*')->from('exhibitor_attendee_meeting')->where($where)->get()->row_array();
    }
    public function get_custom_view($eventid,$user_id)
    {
        $this->db->select('*')->from('event_attendee ea');
        $this->db->join('user_views uv','uv.view_id=ea.views_id','right');
        $this->db->where('ea.Event_id',$eventid);
        $this->db->where('ea.Attendee_id',$user_id);
        $vqu=$this->db->get();   
        $vres=$vqu->result_array();
        if(!empty($vres[0]['view_modules']))
        {
            $activemodules=explode(",",$vres[0]['view_modules']);   
        }
        if($vres[0]['view_type']=='1' && empty($vres[0]['view_modules']))
        {
            $activemodules=array('21');
        }
        return $activemodules;

    }
    public function get_meeting_location($event_id)
    {
        $this->db->select('CASE WHEN location IS NULL THEN "" ELSE location END as location',false);
        $this->db->where('event_id',$event_id);
        $query = $this->db->get('meeting_location');
        $res = $query->result_array();
        foreach ($res as $key => $value):
        $data[] = $value['location'];
        endforeach;
        return $data;
    }
    public function get_game_point($event_id,$user_id)
    {
        $activemodules = $this->db->select('checkbox_values')->where('Id',$event_id)->get('event')->row_array();
        $activemodules = explode(',',$activemodules['checkbox_values']);
        if(in_array('52',$activemodules))
        {
            $points = $this->db->select('CASE WHEN SUM(points) IS NULL THEN 0 ELSE SUM(points) END as points',FALSE)->where('user_id',$user_id)->where('event_id',$event_id)->get('game_users_point')->row_array();
            $data['points'] = $points['points'];
            $data['game_is_on'] = "1";
        }
        else
        {
            $data['game_is_on'] = "0";
            $data['points'] = 0;
        }       
        return $data;
    }
    public function get_linked_exhibitors($attendee_id)
    {
        $this->db->select('Id');
        $this->db->where("FIND_IN_SET($attendee_id,link_user) != ",0);
        $query = $this->db->get('exibitor');
        $res = $query->row_array();
        if(!empty($res))
        {
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
                               CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                               CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
                               CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
                               CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->order_by('e.Heading',NULL,FALSE);
            $this->db->where_in('e.Id',$res);
            $query = $this->db->get();
            $data =  $query->result_array(); 
            foreach ($data as $key => $value)
            {
                $data[$key]['Heading'] = ucfirst($value['Heading']);
                $tmp = json_decode($value['company_logo']);
                $data[$key]['company_logo'] =   $tmp[0];
            }
        }
        else
        {
            $data = [];
        }
        return $data;
    }
    public function get_my_contact($event_id,$user_id=0,$where,$page_no)
    {
        $user_id = ($user_id=='') ? '0' : $user_id;
        $this->db->select('*,u.Id as uid,asc2.contact_type',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." ",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id.")";
        $this->db->where($wher);
        if(!empty($where))
            $this->db->where($where);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares1 = $aquery->result();

        $this->db->select('*,u.Id as uid,asc1.contact_type',false)->from('user u');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." ",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        if(!empty($where))
            $this->db->where($where);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname',NULL,FALSE);
        $aquery = $this->db->get();
        $ares2 = $aquery->result(); 
        $ares = array_merge($ares1,$ares2);

        $attendees = array();
        for($i=0; $i<count($ares); $i++)
        {
            $prev="";
            $fc = strtoupper($ares[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares[$i]->Lastname!=$prev)
            {
                $prev=$ares[$i]->Start_date;
                $data['Id']=$ares[$i]->uid;
                $data['Firstname']=$ares[$i]->Firstname;
                $data['Lastname']=$ares[$i]->Lastname;
                $data['Company_name']=$ares[$i]->Company_name;
                $data['Title']=$ares[$i]->Title;
                $data['Email']=$ares[$i]->Email;
                $data['type']=$ares[$i]->contact_type;
                
                $ex = $this->db->select('*')->from('exibitor')->where('Event_id',$event_id)->where('user_id',$ares[$i]->uid)->get()->row_array();
                if(!empty($ex))
                {
                    $data['type']='1';
                    $data['exhibitor_id']=$ex['user_id'];
                    $data['exhibitor_page_id']=$ex['Id'];
                }
                else
                {
                    $data['type']='0';
                    $data['exhibitor_id']='';
                    $data['exhibitor_page_id']='';
                }
                $data['Logo']=$ares[$i]->Logo;
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('module_type','2')->where('user_id',$user_id)->where('module_id',$ares[$i]->Id)->get()->num_rows();
                $data['is_favorites']=($count) ? '1' : '0';
                array_walk($data,function(&$item){$item=strval($item);});
                $attendees[] = $data;
            }
        }

        $limit          = 10;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $total          = count($attendees);
        $total_page     = ceil($total/$limit);
        $attendees      = array_slice($attendees,$start,$limit);

        $data['attendees'] = $attendees;
        $data['total'] = $total_page;
        return $data;
    }

    public function get_org_id($event_id)
    {   
        return $this->db->select('Organisor_id')->where('Id',$event_id)->get('event')->row_array()['Organisor_id'];
    }
    public function check_message_permisson($user_id,$cur_user,$event_id)
    {   
        
        $this->db->select('ea.*,u.Id,ug.send_message,ug.send_request');
        $this->db->from('event_attendee ea');
        $this->db->join('user u','u.Id = ea.Attendee_id');
        $this->db->join('user_group ug','ug.id = ea.group_id');
        $this->db->where('u.Id',$cur_user);
        $this->db->where('ea.Event_id',$event_id);
        $res = $this->db->get()->row_array();

        $org_id = $this->get_org_id($event_id);
        if(empty($res) && $org_id != '20152')
        {
            return '1';
        }
        if(!empty($res['group_id']))
        {
            $this->db->protect_identifiers=false;
            $this->db->select();
            $this->db->from('event_attendee ea');
            $this->db->where('ea.Attendee_id',$user_id);
            $this->db->where('ea.Event_id',$event_id);
            $this->db->where('find_in_set(ea.group_id,(select ug.permitted_group from event_attendee ea join user_group ug on ea.group_id=ug.id where ea.Attendee_id = '.$cur_user.' and ea.Event_id ='.$event_id.'))');
            $res = $this->db->get()->row_array();
        }
        return (!empty($res)) ? '1' : '0';
    }
    public function check_group_permissions($user_id,$cur_user,$event_id)//Wednesday 11 April 2018 05:42:42 PM
    {   
        $this->db->select('ea.*,u.Id,ug.send_message,ug.send_request');
        $this->db->from('event_attendee ea');
        $this->db->join('user u','u.Id = ea.Attendee_id');
        $this->db->join('user_group ug','ug.id = ea.group_id');
        $this->db->where('u.Id',$cur_user);
        $this->db->where('ea.Event_id',$event_id);
        $res = $this->db->get()->row_array();
        $org_id = $this->get_org_id($event_id);

        if(empty($res) && $org_id != '20152')
        {
            $res['send_message'] = '1';
            $res['send_request'] = '1';
            return $res;
        }
        if(!empty($res['group_id']))
        {   
            $this->db->protect_identifiers=false;
            $this->db->select();
            $this->db->from('event_attendee ea');
            $this->db->where('ea.Attendee_id',$user_id);
            $this->db->where('ea.Event_id',$event_id);
            $this->db->where('find_in_set(ea.group_id,(select ug.permitted_group from event_attendee ea join user_group ug on ea.group_id=ug.id where ea.Attendee_id = '.$cur_user.' and ea.Event_id ='.$event_id.'))');
            $res2 = $this->db->get()->row_array();
        }
        return (!empty($res2)) ? $res : '0';
    }
    public function getAllMeetingRequestNew($event_id,$user_id)
    {
        $data1 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                    CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                    eam.date,
                                    eam.time,
                                    CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,
                                    eam.Id as request_id,
                                    CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                    CASE WHEN eam.attendee_id IS NULL THEN eam.sender_exhibitor_id ELSE eam.attendee_id END AS exhibiotor_id,
                                    CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                    CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                    CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.attendee_id OR u.Id = eam.sender_exhibitor_id')
                        ->where('eam.recever_attendee_id',$user_id)
                        ->where('eam.event_id',$event_id)
                        //->where('eam.status != ','2')
                        ->order_by('eam.Id','DESC')->get()->result_array();
           //CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
        $data2 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.time,
                                  CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.recever_attendee_id')
                        ->where('eam.attendee_id',$user_id)
                        ->where('eam.event_id',$event_id)
                        ->where('eam.status ','1')
                        ->order_by('eam.Id','DESC')->get()->result_array();

       
        $data3 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.time,
                                  CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')
                                  ->join('exibitor e','e.Id = eam.exhibiotor_id')
                                  ->join('user u','u.Id = e.user_id')
                                  ->where('eam.attendee_id',$user_id)
                                  ->where('eam.event_id',$event_id)
                                  ->where('eam.status ','1')
                                  ->order_by('eam.Id','DESC')->get()->result_array();

        $data4 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.time,
                                  eam.status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')
                                  ->join('user u','u.Id = eam.recever_attendee_id')
                                  ->where('eam.attendee_id = '.$user_id.' OR eam.sender_exhibitor_id = '.$user_id)
                                  ->where('eam.event_id',$event_id)
                                  ->order_by('eam.Id','DESC')->get()->result_array();
                                  
        $data = array_merge($data2,$data3,$data1,$data4);    
        $data = array_unique($data, SORT_REGULAR);
        return $data;
    }
    public function getUserNew($user_id,$event_id)
    {
        return $this->db->select('u.*,ru.Role_id')->from('user u')->join('relation_event_user ru','ru.User_id = u.Id')->where('u.Id',$user_id)->where('ru.Event_id',$event_id)->get()->row_array();
    }
    public function getDateTimeArray_for_Solar($event_id)
    {
        $data = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $data1 = $this->db->select('format_time')->from('fundraising_setting')->where('Event_id',$event_id)->get()->row_array();
        
        $data['Start_date']='2017-11-07';
        $data['End_date']='2017-11-08';

        /*$data['Start_time']='10:40';
        $data['End_time']='19:10';*/
        $start = date('Y-m-d',strtotime($data['Start_date']));
        $time1 = date('H:i',strtotime($data['Start_time']));
        $time2 = date('H:i',strtotime($data['End_time']));

        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        
        $res=$query->result_array();

        while($start <= $data['End_date'])
        {   
            if($res[0]['date_format'] == 0)
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            else
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }
        $time=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
        $format="H:i";
        if($data1['format_time'] == '0')
        {
            $format="h:i A";
        }   
        else
        {
            $format="H:i";
        } 
        foreach ($time as $key => $value) {
           if((date('H:i',$value)) == "00:00")
               continue;
           $time_arr[] = date($format,$value);
        }
        $result['date'] = $date;
        $result['time'] = $time_arr;
        return $result;
    }
    public function getDateTimeArray_for_ipexpo($event_id,$date)
    {
        $data = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $data1 = $this->db->select('format_time')->from('fundraising_setting')->where('Event_id',$event_id)->get()->row_array();
        
        $data['Start_date']='2018-04-25';
        $data['End_date']='2018-04-26';

        /*$data['Start_time']='10:40';
        $data['End_time']='19:10';*/
        $start = date('Y-m-d',strtotime($data['Start_date']));
        $time1 = date('H:i',strtotime($data['Start_time']));
        $time2 = date('H:i',strtotime($data['End_time']));

        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        
        $res=$query->result_array();

        while($start <= $data['End_date'])
        {   
            if($res[0]['date_format'] == 0)
            {
                $date[] = date('d-m-Y',strtotime($start));
            }
            else
            {
                $date[] = date('m-d-Y',strtotime($start));
            }
            
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }
        $time=range(strtotime(date('9:30')),strtotime("16:45"),15*60);
        $format="H:i";
        if($data1['format_time'] == '0')
        {
            $format="h:i A";
        }   
        else
        {
            $format="H:i";
        } 
        foreach ($time as $key => $value) {
           if((date('H:i',$value)) == "00:00")
               continue;
           $time_arr[] = date($format,$value);
        }
        $result['date'] = $date;
        $result['time'] = $time_arr;
        return $result;
    }
    public function getAttendeeForInviting($event_id,$meeting_id,$user_id,$where)
    {   
        
        $meeting = $this->db->where('Id',$meeting_id)->get('exhibitor_attendee_meeting')->row_array();
        $attendee = $this->db->where('event_id',$event_id)
                         ->where('(date = "'.$meeting['date'].'" AND time = "'.$meeting['time'].'") OR (is_invited = "'.$meeting_id.'")')
                         ->where('status','1')
                         ->get('exhibitor_attendee_meeting')
                         ->result_array();
        $busy_attendee1  = array_column($attendee,'attendee_id');
        $busy_attendee2  = array_column($attendee,'recever_attendee_id');
        $busy_attendee   = array_filter(array_merge($busy_attendee1,$busy_attendee2));
        $busy_attendee[] = $user_id;
        $busy_attendee = array_unique($busy_attendee);

        $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           u.Id',false);
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('reu.Event_id',$event_id);
        $this->db->where('reu.Role_id',4);
        $this->db->where_not_in('u.Id',$busy_attendee);
        if(!empty($where))
            $this->db->where($where);
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $this->db->group_by('u.Id');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function GetInvitedAttendee($event_id,$meeting_id,$user_id,$is_accepted=0)
    {   

        $meeting = $this->db->where('Id',$meeting_id)->get('exhibitor_attendee_meeting')->row_array();
        $this->db->where('event_id',$event_id)->where('is_invited',$meeting_id);
        if($is_accepted)
        {
            $this->db->where('status','1');
        }
        $attendee = $this->db->get('exhibitor_attendee_meeting')->result_array();

        $invited_attendee1 = array_column($attendee,'attendee_id');
        $invited_attendee2 = array_column($attendee,'recever_attendee_id');
        $invited_attendee  = array_filter(array_merge($invited_attendee1,$invited_attendee2));

        if(!empty($meeting['exhibiotor_id']))
        {
            $primary_attendee = $this->db->where('Id',$meeting['exhibiotor_id'])
                                         ->get('exibitor')->row_array()['user_id'];
        }
        else
        {
            $primary_attendee = $meeting['recever_attendee_id'];
        }
        $invited_attendee[] = $primary_attendee;

        $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Id = '.$primary_attendee.' THEN "1" ELSE "0" END as is_primary,
                           u.Id',false);
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('reu.Event_id',$event_id);
        // $this->db->where('reu.Role_id',4);
        $this->db->where('u.Id !=',$user_id);
        $this->db->where_in('u.Id',$invited_attendee);
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $this->db->order_by('is_primary',NULL,FALSE);
        $this->db->group_by('u.Id');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function getMapMeetingLocation($event_id)
    {
        $this->db->select('mm.map_id,mm.coords,ml.location');
        $this->db->where('m.Event_id',$event_id);
        $this->db->where('mm.location_id IS NOT NULL');
        $this->db->join('map_image_mapping mm','mm.map_id = m.Id');
        $this->db->join('meeting_location ml','ml.location_id = mm.location_id');
        $res = $this->db->get('map m')->result_array();
        $meeting_ids = array_column($res,'location');
        $res = array_combine($meeting_ids,$res);
        return $res;
    }
    public function getAllMeetingRequestWithDate($event_id,$user_id)
    {
        $data1 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                    CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                    eam.date,
                                    eam.time,
                                    eam.is_invited,
                                    CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,
                                    eam.Id as request_id,
                                    CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                    CASE WHEN eam.attendee_id IS NULL THEN eam.sender_exhibitor_id ELSE eam.attendee_id END AS exhibiotor_id,
                                    CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                    CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                    CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN eam.sender_exhibitor_id IS NULL THEN "" ELSE eam.sender_exhibitor_id END as exhi_user_id',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.attendee_id OR u.Id = eam.sender_exhibitor_id')
                        ->where('eam.recever_attendee_id',$user_id)
                        ->where('eam.event_id',$event_id)
                        ->where("eam.Id not in (select Id from exhibitor_attendee_meeting as eam2 where eam2.event_id = '$event_id' and FIND_IN_SET('$user_id',eam.main_attendee) != 0)")
                        /*->where('eam.is_invited IS NULL')*/
                        //->where('eam.status != ','2')
                        ->order_by('eam.Id','DESC')->get()->result_array();// Recived Meetings
           //CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
        $data2 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.is_invited,
                                  eam.time,
                                  CASE WHEN eam.status = "0" THEN "" ELSE eam.status END as status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,"" as exhi_user_id',false)
                        ->from('exhibitor_attendee_meeting eam')
                        ->join('user u','u.Id = eam.recever_attendee_id')
                        ->where('eam.attendee_id',$user_id)
                        ->where('eam.event_id',$event_id)
                        ->where('eam.status ','1')
                        ->where("eam.Id not in (select Id from exhibitor_attendee_meeting as eam2 where eam2.event_id = '$event_id' and FIND_IN_SET('$user_id',eam.main_attendee) != 0)")
                        ->order_by('eam.Id','DESC')->get()->result_array(); //sent to attendee

        $data3 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.is_invited,
                                  eam.time,
                                  CASE WHEN eam.status = "0" THEN "0" ELSE eam.status END as status,
                                  eam.Id as request_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN eam.exhibiotor_id IS NULL THEN "" ELSE eam.exhibiotor_id END as exhi_user_id',false)->from('exhibitor_attendee_meeting eam')
                                  ->join('exibitor e','e.Id = eam.exhibiotor_id')
                                  ->join('user u','u.Id = e.user_id')
                                  ->where('eam.attendee_id',$user_id)
                                  ->where('eam.event_id',$event_id)
                                  ->where("eam.Id not in (select Id from exhibitor_attendee_meeting as eam2 where eam2.event_id = '$event_id' and FIND_IN_SET('$user_id',eam.main_attendee) != 0)")
                                  // ->where('eam.status ','1')
                                  ->order_by('eam.Id','DESC')->get()->result_array(); //sent attendee to exhi

        $data4 = $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                                  CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                                  eam.date,
                                  eam.time,
                                  eam.status,
                                  eam.is_invited,
                                  eam.Id as request_id,
                                  CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
                                  CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                                  CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                                  CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                                  CASE WHEN eam.exhibiotor_id IS NULL THEN "" ELSE eam.exhibiotor_id END as exhi_user_id',false)->from('exhibitor_attendee_meeting eam')
                                  ->join('user u','u.Id = eam.recever_attendee_id')
                                  ->where('(eam.attendee_id = '.$user_id.' OR eam.sender_exhibitor_id = '.$user_id.')')
                                  ->where("eam.Id not in (select Id from exhibitor_attendee_meeting as eam2 where eam2.event_id = '$event_id' and FIND_IN_SET('$user_id',eam.main_attendee) != 0)")
                                  ->where('eam.event_id',$event_id)
                                  ->order_by('eam.Id','DESC')->get()->result_array();

        /*$tmp[] = $data1;
        $tmp[] = $data2;
        $tmp[] = $data3;
        $tmp[] = $data4;
        j($tmp);*/
        $data = array_merge($data2,$data3,$data1,$data4);    
        $data = array_unique($data, SORT_REGULAR);
        $format_time = $this->db->select('format_time')->where('Event_id',$event_id)->get('fundraising_setting')->row_array()['format_time'];
        $format['format'] = ($format_time == '0') ? 'h:i A' : 'H:i:s';
        foreach ($data as $key => $value)
        {   
            if(!empty($value['is_invited']))
            {
                $is_invited = $this->db->where('Id',$value['is_invited'])->get('exhibitor_attendee_meeting')->row_array();
                $this->db->select('group_concat(concat(u.Firstname," ",u.Lastname)) as name')->where('eam.is_invited',$value['is_invited'])->where('eam.status','1');
                $this->db->where('u.Id !=',$user_id);
                $this->db->join('user u','u.Id = eam.recever_attendee_id');
                $users = $this->db->get('exhibitor_attendee_meeting eam')->row_array()['name'];

                $data[$key]['Lastname'] .= ", ".$users;                
                if($is_invited['recever_attendee_id'] == $user_id)
                {
                    $data[$key]['show_invite_more'] = '1';
                }
                else
                {   
                    $data[$key]['show_invite_more'] = '0';
                }

            }
            else
            {                                                       
                $invited_attendee = $this->GetInvitedAttendee($event_id,$value['request_id'],$user_id,1);
                foreach ($invited_attendee as $key1 => $value1)
                {
                    $invited_name[] = $value1['Firstname'].' '.$value1['Lastname'];
                }
                $invited_name[] = $value['Firstname'].' '.$value['Lastname'];
                $invited_name = array_unique($invited_name);
                $data[$key]['Firstname'] = "";
                $data[$key]['Lastname'] = implode(', ',$invited_name);
                $data[$key]['show_invite_more'] = '1';
                unset($invited_name);
            }
            if(!empty($value['exhi_user_id']))
            {
                $data[$key]['exhi_user_id'] = $this->db->where('Id',$value['exhi_user_id'])->get('exibitor')->row_array()['user_id'];
            }
            $data[$key]['time_new'] = date($format['format'],strtotime($value['time']));
        }
        // j($data);
        return $data;
    }
    public function getRequestMeetingDate($event_id)
    {
        $data = $this->db->where('Id',$event_id)->get('event')->row_array();
        
        if(strtotime($data['Start_date']) > time())
            $start = date('Y-m-d',strtotime($data['Start_date']));
        else
            $start = date('Y-m-d');

        while($start <= $data['End_date'])
        {   
            if($data['date_format'] == 0)
            {
                $date[] = date('d-m-Y',strtotime($start));
            }
            else
            {
                $date[] = date('m-d-Y',strtotime($start));
            }            
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }
        return $date?:[];
    }
    public function getRequestMeetingTime($event_id,$user_id,$date,$is_exhibitor='0')
    {
        $date = date('Y-m-d',strtotime($date));
        $event = $this->db->where('Id',$event_id)->get('event')->row_array();
        $format_time = $this->db->select('format_time')->where('Event_id',$event_id)->get('fundraising_setting')->row_array()['format_time'];

        if($is_exhibitor == '1')
        {
            $this->db->where('(exhibiotor_id = '.$user_id.' OR sender_exhibitor_id = '.$user_id.')');
        }
        else
        {
            $this->db->where('(attendee_id = '.$user_id.' OR recever_attendee_id = '.$user_id.')');
        }
        $this->db->where('status','1');
        $this->db->where('date',$date);
        $this->db->where('event_id',$event_id);
        $booked_meeting = $this->db->get('exhibitor_attendee_meeting')->result_array();
        $booked_time = array_unique(array_column($booked_meeting,'time'));
        $time=range(strtotime(date($event['meeting_start_time'])),strtotime($event['meeting_end_time']),$event['meeting_time_slot']*60);
        $format['format'] = ($format_time == '0') ? 'h:i A' : 'H:i';

        function ChangeTimeFormat(&$value,$key,$format)
        {   
            if($format['is_string'])
                $value = date($format['format'],strtotime($value));
            else
                $value = date($format['format'],$value);
        }

        $format['is_string'] = '1';
        array_walk($booked_time,"ChangeTimeFormat",$format);
        $format['is_string'] = '0';
        array_walk($time,"ChangeTimeFormat",$format);
        $time = array_values(array_diff($time,$booked_time));
        return $time?:[];
    }
    public function getRequestMeetingLocation($event_id,$date,$time)
    {
        $date = date('Y-m-d',strtotime($date));
        $time = date('H:i:s',strtotime($time));

        $this->db->where('date',$date);
        $this->db->where('time',$time);
        $booked_location = $this->db->get('exhibitor_attendee_meeting')->result_array();
        $booked_location = array_column($booked_location,'location');
        
        $this->db->select('CASE WHEN location IS NULL THEN "" ELSE location END as location',false);
        $this->db->where('event_id',$event_id);
        $query = $this->db->get('meeting_location');
        $location = array_column($query->result_array(),'location');
        
        $location = array_values(array_diff($location,$booked_location));
        return $location?:[];
    }
    public function getMainMeeting($meeting_id)
    {
        $meeting = $this->db->where('Id',$meeting_id)->get('exhibitor_attendee_meeting')->row_array();
        return $meeting;
    }
    public function getAttendeeCategories($event_id)
    {
        $this->db->where('event_id',$event_id);
        $this->db->where('menu_id','2');
        $res = $this->db->get('attendee_category')->result_array();
        return $res;
    }
    public function blockAttendee($data,$status)
    {   
        if($status == '1')
        {
            $this->db->insert('block_attendee',$data);
        }
        else
        {
            $this->db->where($data);
            $this->db->delete('block_attendee');
        }
    }
     public function checkUserBlocked($data)
    {

        if($user_id_from !='')
        {
            $where .= '(user_id_to = '.$data['user_id_to'].' AND user_id_from = '.$data['user_id_from'];
            $where .= ' OR user_id_to = '.$data['user_id_from'].' AND user_id_from = '.$data['user_id_to'].')';

            $this->db->where($where);
            $this->db->where('event_id',$data['event_id']);
            $res = $this->db->get('block_attendee')->row_array();
        }
        return !empty($res) ? '1' : '0';
    }
    public function checkBlockedByMe($data)
    {
        $this->db->where($data);
        $res = $this->db->get('block_attendee')->row_array();
        return !empty($res) ? '1' : '0';
    }

    public function checkMaxMeetingLimit($user_id,$event_id)
    {
        $max_meeting = $this->db->where('Id',$event_id)->get('event')->row_array()['max_meeting'];
        if($max_meeting == '0'):
            return false;
        elseif(!empty($max_meeting)):

            $this->db->where('attendee_id',$user_id);
            $this->db->where('event_id',$event_id);
            $sent_meeting = $this->db->get('exhibitor_attendee_meeting')->num_rows();
            if($sent_meeting >= $max_meeting)
            {   
                return true;
            }
            else
            {   
                return false;
            }
        else:
            return false;
        endif;
    }

    public function checkGroupMaxMeetingLimit($user_id,$event_id)
    {   
        $this->db->join('event_attendee ea','ea.group_id = ug.id');
        $this->db->where('ea.Event_id',$event_id);
        $this->db->where('ea.Attendee_id',$user_id);
        $res = $this->db->get('user_group ug')->row_array();
        if(!empty($res))
        {
            if($res['max_meeting_limit'])
            {   
                /*$this->db->where('(attendee_id IN (select Attendee_id from event_attendee where Event_id="'.$event_id.'" AND group_id="'.$res['id'].'"))');
                $this->db->where('event_id',$event_id);
                $sent_meeting = $this->db->get('exhibitor_attendee_meeting')->num_rows();*/
                $this->db->where('attendee_id',$user_id);
                $this->db->where('event_id',$event_id);
                $sent_meeting = $this->db->get('exhibitor_attendee_meeting')->num_rows();
                if($sent_meeting >= $res['max_meeting_limit'])
                {   
                    return true;
                }
                else
                {   
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function hideMyIdentity($event_id,$user_id,$status)
    {   

        $data['Event_id'] = $event_id;
        $data['Attendee_id'] = $user_id;
        $this->db->where($data);
        $res = $this->db->get('event_attendee')->row_array();
        if($res)
        {
            
            $update['hide_identity'] = $status;
            $this->db->where($data)->update('event_attendee',$update); 
        }
        else
        {   
            $data['hide_identity'] = $status;
            $this->db->insert('event_attendee',$data);
        }
    }
    public function getAttendeeFilters($event_id,$category_id)
    {   
        $this->db->select('CASE WHEN group_concat(distinct aec.value) IS NULL THEN "" ELSE group_concat(distinct aec.value) END AS keywords,CASE WHEN af.label IS NULL THEN c.column_name ELSE af.label END AS column_name,ac.category,c.column_id as id',FALSE);
        $this->db->join('custom_column c','c.column_id = af.custom_column');
        $this->db->join('attendee_extra_column aec','aec.key1 = c.column_name','left');
        $this->db->join('attendee_category ac','ac.id = af.category_id');
        $this->db->where('af.event_id',$event_id);
        $this->db->where('af.category_id',$category_id);
        $this->db->group_by('af.id');
        $res = $this->db->get('attendee_filter_relation af')->result_array();
        foreach ($res as $key => $value)
        {   
            if(!empty($value['keywords']))
            $res[$key]['keywords'] = array_values(array_filter(array_unique(explode(',',$value['keywords']))));
            else
            $res[$key]['keywords'] = [];
        }
        return $res;
    }
    public function attendee_list_v2($Event_id,$where,$page_no,$user_id,$filter_keywords,$advance_filter)
    {
        if(!empty($advance_filter))
        {   
            foreach ($advance_filter as $key => $value)
            {   
                $this->db->where('column_id',$value['id']);
                $res = $this->db->get('custom_column')->row_array()['column_name'];
                foreach ($value['keywords'] as $key1 => $value1)
                {   
                    $where1[] = '(key1 = "'.$res.'" AND value = "'.$value1.'")';
                }
                $where_fin[] = '('.implode(' OR ',$where1).')';
            }
            if($_POST['test'] == '1')
            {
                j($where_fin);
            }
            $str = "select user_id from attendee_extra_column where event_id = ".$Event_id." AND ";
            $str2 = "";
            $j = 0;
            foreach ($where_fin as $key => $value)
            {
                if($key == (count($where_fin)-1))
                {   
                    if(count($where_fin) == '1')
                    {
                        $str1 .= $str.' '.$value;
                    }
                    else
                    {
                        $str1 .= $str.' '.$value.')';
                    }
                }
                else
                {
                    $str1 .= $str.' '.$value.' AND user_id IN (';
                    $j++;
                }
            }
            $str1 = $str.' user_id IN ('.$str1.')';
            for ($i=1; $i < $j; $i++){ 
                $str1 .= ')';    
            }
            $filtered_attendee = array_column($this->db->query($str1)->result_array(),'user_id');
            if($_POST['test'] == '2')
            {
                lq();
            }
        }
        if($Event_id == '1114')
        {
            $regUserIds = $this->getRegisterdUserIds($Event_id);
        }
        $this->db->select('*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        if(!empty($filtered_attendee))
        {
            $this->db->where_in('u.Id',$filtered_attendee);
        }
        if(!empty($filter_keywords))
        {
             $filter_keywords = "'" . str_replace(array("'", ","), array("'", "','"), $filter_keywords) . "'";
            $this->db->where('u.Id in (select user_id from attendee_keywords where keyword in ('.$filter_keywords.') and event_id = '.$Event_id.')');
        }
        $this->db->where('u.Id not in (select Attendee_id from event_attendee where Event_id='.$Event_id.' and hide_identity="1")');
        if($Event_id == '1114')
            $this->db->where_in('u.Id',$regUserIds);
        $this->db->where('r.Id',4);

        $total = $this->db->get()->num_rows();


        $limit          = 20;
        $page_no        = (!empty($page_no))?$page_no:1;
        $start          = ($page_no-1)*$limit;

        $this->db->protect_identifiers=false;
        #123
        if($Event_id == '1397')
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,ac.value',FALSE);
            $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
            $this->db->join('user u', 'u.Id = ru.User_id');
            $this->db->join('role r','ru.Role_id = r.Id');
            $this->db->join('attendee_extra_column ac','ac.user_id = u.Id and ac.key1="Country" and ac.event_id=e.Id');  
        }
        else
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
            $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
            $this->db->join('user u', 'u.Id = ru.User_id');
            $this->db->join('role r','ru.Role_id = r.Id');
        }

        if(!empty($filtered_attendee))
        {
            $this->db->where_in('u.Id',$filtered_attendee);
        }
        $this->db->where('e.Id',$Event_id);
        if($where!='')
            $this->db->where($where);
        $this->db->where('r.Id',4);
        $this->db->where('u.Firstname !=""');

        if(!empty($filter_keywords))
        {   
            // $filter_keywords = "'" . str_replace(array("'", ","), array("\\'", "','"), $filter_keywords) . "'";
            $this->db->where('u.Id in (select user_id from attendee_keywords where keyword in ('.$filter_keywords.') and event_id = '.$Event_id.')');
        }
        if($Event_id == '1114')
            $this->db->where_in('u.Id',$regUserIds);
        $this->db->where('u.Id not in (select Attendee_id from event_attendee where Event_id='.$Event_id.' and hide_identity="1")');
        $this->db->order_by("concat(u.Firstname,' ',u.Lastname)");
        $this->db->limit($limit, $start);
        $this->db->group_by('u.Id');
        $query = $this->db->get('event e');
        $res = $query->result();
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            if(!empty($res[$i]->value))
            {
                $attendees[$i]['Company_name']=$res[$i]->Company_name.', '.$res[$i]->value;
            }
            else
            {
                $attendees[$i]['Company_name']=$res[$i]->Company_name;
            }
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','2')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $attendees[$i]['is_favorites']=($count) ? '1' : '0';
           
        }
        $data['attendees'] = $attendees;
        $total_page    = ceil($total/$limit);
        $data['total'] = $total_page;
        return $data;
    }
    public function getCategoryKeywords($category_id)
    {
        $this->db->where('id',$category_id);
        $res = $this->db->get('attendee_category')->row_array();
        return $res['categorie_keywords'];
    }
    public function getRegisterdUserIds($event_id)
    {   
        $this->db->where('(user_id IN (select User_id FROM `relation_event_user` WHERE `Event_id` = "'.$event_id.'" AND `Role_id` = "4"))');
        $res = array_column($this->db->get('users_agenda')->result_array(),'user_id');
        return $res;
    }
}
        
?>