<?php
class Event_template_model extends CI_Model{
    function __construct()
    {
        
        //$this->db2 = $this->load->database('db1', TRUE);

    }
    public function get_event_template_by_id_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();    
        return $res;   
    }
    public function geteventmenu_list($eventid,$menu_id=null,$is_feture=null)
    {
          $this->db->select('e.checkbox_values',FALSE);
          $this->db->from('event e');
        
          $this->db->where('e.Id', $eventid);
          $query = $this->db->get();
          $res = $query->result_array();
          $resdata=array();
          
          if(!empty($res))
          {
               $menudata=explode(',', $res[0]['checkbox_values']);

               $menudata = array_diff($menudata, array('5'));
               $menudata=  array_merge($menudata,array());

               $this->db->select('m.id,m.pagetitle,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,em.is_feture_product',FALSE);
               $this->db->from('menu m');
               $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
               $this->db->join('event e', 'e.Id=em.event_id', 'left');

               if($menu_id!=null)
               {
                   $this->db->where('m.id', $menu_id);
               }

               $this->db->where_in('m.id',$menudata);

               $query = $this->db->get();
               $resdata = $query->result_array();
          }
          $this->load->model('event_model');
          $eventmodule=$this->event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          $arr=array('22','23','24','25','42');
          $module=array_merge($module,$arr);
          foreach ($resdata as $key => $value) {
             if(!in_array($value['id'],$module))
             {
               unset($resdata[$key]);
             }
          }

          return $resdata;
    }
    public function get_advertising_images($Subdomain=null)
    {
        
        $Menu_id = $this->uri->segment(1);
        $this->db->select('Id');
        $this->db->from('menu');
        $this->db->where('pagetitle',$Menu_id);
        $query3 = $this->db->get();
        $res3 = $query3->result_array();

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id',$res[0]['Id']);
        if(!empty($res3))
        {
            $this->db->where('INSTR(Menu_id, '.$res3[0]["Id"].')');
        }
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function get_agenda_list($Subdomain=null,$id_arr)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title');
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$res[0]['Id']);
        $this->db->where("a.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('a.Start_date asc',NULL,FALSE);
        $this->db->order_by('a.Start_time asc',NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result();

        $agendas = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            if($prev=="" || $res[$i]->Start_date!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Id']=$res[$i]->Id;
                $agendas[$res[$i]->Start_date][$i]['Heading']=$res[$i]->Heading;
                $agendas[$res[$i]->Start_date][$i]['Start_date']=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Start_time']=$res[$i]->Start_time;
                $agendas[$res[$i]->Start_date][$i]['End_date']=$res[$i]->End_date;
                $agendas[$res[$i]->Start_date][$i]['End_time']=$res[$i]->End_time;
                $agendas[$res[$i]->Start_date][$i]['Address_map']=$res[$i]->Address_map;
                $agendas[$res[$i]->Start_date][$i]['Map_title']=$res[$i]->Map_title;
                $agendas[$res[$i]->Start_date][$i]['Event_id']=$res[$i]->Event_id;
            }
        }
        return $agendas;
    }
    public function get_menu_list($roleid,$eventid)
    {         
        $this->db->select('*');
        $this->db->from('role_permission r');
        $this->db->where('r.Role_id',$roleid);
        $this->db->where('r.event_id',$eventid);
        $this->db->join('menu m', 'm.id=r.Menu_id');
        $this->db->order_by("m.type",NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result_array(); 
       
     /*    echo "fdf"."<pre>";
        print_r($res);die;*/
        return $res;
    }
    public function delete_pptfile($eid,$pid)
    {
        $arr['Images']='';
        $arr['Image_lock']='';
        $this->db->where('Id',$pid);
        $this->db->where('Event_id',$eid);
        $this->db->update('presentation',$arr);
    }
    public function getaddnewspekeremail($sluge)
    {
        $this->db2->select('*');
        $this->db2->from('email_notification_templates');
        $this->db2->where('Slug',$sluge);
        $res=$this->db2->get();
       return $res->result_array();
    }
    public function get_singup_forms($eid)
    {
        $this->db->select('*');
        $this->db->from('signup_form');
        $this->db->where('event_id',$eid);
        $query = $this->db->get();
        $res = $query->result_array();  
        return $res;
    }
    public function get_subdomain($id)
    {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();  
        $Subdomain=$res[0]['Subdomain'];
        return $Subdomain;

    }
    public function get_event_template_list($id=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        if($user[0]->Id)
        {
            $this->db->where('e.Organisor_id',$user[0]->Id);
        }
        $query = $this->db->get();
        $res = $query->result();    

        $event_res  = array();
        for($i=0;$i<count($res);$i++)
        {
            $event_res[$i]['Id']=$res[$i]->Id;
            $event_res[$i]['Event_name']=$res[$i]->Event_name;
            $event_res[$i]['Status']=$res[$i]->Status;
            $event_res[$i]['Event_type']=$res[$i]->Event_type;
            $event_res[$i]['Event_time']=$res[$i]->Event_time;
            $event_res[$i]['Event_time_option']=$res[$i]->Event_time_option;
            $event_res[$i]['Subdomain']=$res[$i]->Subdomain;
            $event_res[$i]['Description']=$res[$i]->Description;
            $event_res[$i]['Images']=$res[$i]->Images;
            $event_res[$i]['Logo_images']=$res[$i]->Logo_images;
            $event_res[$i]['Created_date']=$res[$i]->Created_date;
            $event_res[$i]['Start_date']=$res[$i]->Start_date;
            $event_res[$i]['End_date']=$res[$i]->End_date;
            $event_res[$i]['Organisor_id']=$res[$i]->Organisor_id;
            $event_res[$i]['Address']=$res[$i]->Address;
            $event_res[$i]['Background_color']=$res[$i]->Background_color;
            $event_res[$i]['Contact_us']=$res[$i]->Contact_us;

        }
        if($this->data['user']->Role_name == 'Client')
        {
              $event_res_val=  array_merge(array(),$event_res); 
        }
        else if($this->data['user']->Role_name == 'Attendee')
        {
              $event_res_val=  array_merge(array(),$event_res); 
        }
        else
        {
            $event_res_val = $event_res;
        }
       return $event_res_val;
    }

    public function get_sub($Event_id=null)
    {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id',$Event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_acc_name($Organisor_id)
    {
        $this->db->select('acc_name');
        $this->db->from('user');
        $this->db->where('Id',$Organisor_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $acc_name=$res[0]['acc_name'];
        return $acc_name;
    }
    
    public function get_event_template_by_id_list_preview($Subdomain=null,$data)
    {
        $user = $this->session->userdata('current_user');

        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array(); 
        $this->db->select('e.*');
        $this->db->from('temp_event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;   
    }
    public function update_temp_events($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
    
    }
    public function update_banner_images($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
       
    }
    public function update_logo_images($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
    }
    public function get_speaker_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }
        $this->db->where('r.Id =',7);
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Firstname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Firstname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Title']=$res[$i]->Title;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
    public function get_ex_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }
        $this->db->where('r.Id =',6);
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        
        $this->db->order_by('u.Firstname',NULL,FALSE);
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Firstname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Firstname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
   
    public function get_agenda_list_by_types($Subdomain=null,$id=null,$id_arr=null)
    {
        $id = $this->uri->segment(4);
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        
        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title');
        $this->db->from('agenda a');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         
        $this->db->where('a.Event_id',$res[0]['Id']);
        $this->db->where("a.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('a.Start_date asc',NULL,FALSE);
        $this->db->order_by('a.Start_time asc',NULL,FALSE);
        $this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result();

        $agendas = array();
        for($i=0; $i<count($res1); $i++)
        {
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas[$res1[$i]->Types][$i]['Id']=$res1[$i]->Id;
                $agendas[$res1[$i]->Types][$i]['Types']=$res1[$i]->Types;
                $agendas[$res1[$i]->Types][$i]['Heading']=$res1[$i]->Heading;
                $agendas[$res1[$i]->Types][$i]['Start_date']=$res1[$i]->Start_date;
                $agendas[$res1[$i]->Types][$i]['Start_time']=$res1[$i]->Start_time;
                $agendas[$res1[$i]->Types][$i]['End_date']=$res1[$i]->End_date;
                $agendas[$res1[$i]->Types][$i]['End_time']=$res1[$i]->End_time;
                $agendas[$res1[$i]->Types][$i]['Address_map']=$res1[$i]->Address_map;
                $agendas[$res1[$i]->Types][$i]['Map_title']=$res1[$i]->Map_title;
                $agendas[$res1[$i]->Types][$i]['Event_id']=$res1[$i]->Event_id;
            }
        }   

        return $agendas;
    }
    public function get_agenda_value_by_id($Subdomain = NULL, $id = NULL)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,m.Address as Map_address, m.Images as Map_image,doc.document_file');
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
         $this->db->join('document_files doc', 'doc.document_id = a.document_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $this->db->where('a.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        $arr=explode(",",$res[0]['Speaker_id']);

        for($i=0;$i<count($arr);$i++)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('Id',$arr[$i]);
            $q=$this->db->get();
            $result=$q->result_array();
            $info[$i]=$result[0];
        }
        $res[0]['speaker']=$info;
       
        return $res;
    }

    public function get_agenda_types($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Types,other_types');
        $this->db->from('agenda');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        $this->db->where("End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('Start_date asc',NULL,FALSE);
        $this->db->order_by('Start_time asc',NULL,FALSE);
        $this->db->group_by("Types"); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array(); 
        return $res1;

    }
    
}
        
?>
