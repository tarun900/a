<?php
class Survay_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }
    public function get_survey_screens($id=null,$sid=null)
    {
        $this->db->select('*,CASE WHEN welcome_data IS NULL THEN "" ELSE welcome_data END as welcome_data,CASE WHEN thanku_data IS NULL THEN "" ELSE thanku_data END as thanku_data',false);
        $this->db->from('survey_data');
        $this->db->where('Event_id',$id);
        if(!empty($sid))
            $this->db->where('surveys_id',$sid);
        $query = $this->db->get();
        $res = $query->result_array();
        if(empty($res))
        {
            $res[0]['Id'] = '';
            $res[0]['welcome_data'] = '';
            $res[0]['thanku_data'] = '';
            $res[0]['Event_id'] = $id;
        }
        return $res;
    }
    public function get_survey($event_id=null,$user_id=null)
    {
        $this->db->select('CASE WHEN s.id IS NULL THEN "" ELSE s.id END as Question_id,CASE WHEN s.Question IS NULL THEN "" ELSE s.Question END as Question,CASE WHEN s.Question_type IS NULL THEN "" ELSE s.Question_type END as Question_type,CASE WHEN s.Option IS NULL THEN "" ELSE s.Option END as option1',false);
        $this->db->from('survey s');
        if($user_id!='')
        {
           //$this->db->join('poll_survey p', 's.Id = p.Question_id');
            $this->db->where('s.id not IN (SELECT ps.Question_id  FROM `poll_survey` ps join survey s on s.id=ps.Question_id WHERE `User_id` = '.$user_id.' and Event_id='.$event_id.' group by ps.Question_id ORDER BY `User_id` DESC )', NULL, FALSE);
            $this->db->where('s.Event_id',$event_id);
        }
        else
        {   

            $this->db->where('s.Event_id',$event_id);
        }
        $this->db->where('s.show_question','1');
        $this->db->group_by('s.id');

        $query2 = $this->db->get();
        $res2 = $query2->result_array();
      
        if(!empty($res2))
        {

            foreach ($res2 as $key => $value) 
            {
               
                $fin_arr[$key]['Question_id']=$value['Question_id'];
                $fin_arr[$key]['Question']=$value['Question'];
                $fin_arr[$key]['Question_type']=$value['Question_type'];
                $fin_arr[$key]['Option']=json_decode($value['option1']);
            }
        }
        return $fin_arr;
    }
    public function saveSurvey($survey_data)
    {
        $this->db->insert_batch("poll_survey",$survey_data);
        return ($this->db->affected_rows() > 0) ? 1 : 0;
    }

    public function get_survey_category($event_id)
    {
        $this->db->select('CASE WHEN sc.survey_id IS NULL THEN "" ELSE sc.survey_id END as category_id,
                           CASE WHEN sc.survey_name IS NULL THEN "" ELSE sc.survey_name END as survey_name
                           ',false);
        $this->db->from('survey_category sc');
        $this->db->where('sc.Event_id',$event_id);
        $this->db->where('sc.show_survey','1');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2    ;
    }
    public function get_category_wise_survey($event_id=null,$user_id=null,$category_id,$lang_id=NULL)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('CASE WHEN s.id IS NULL THEN "" ELSE s.id END as Question_id,
                           (CASE WHEN elmc.title IS NULL THEN CASE WHEN s.Question IS NULL THEN "" ELSE s.Question END ELSE elmc.title END) as Question,
                           CASE WHEN s.Question_type IS NULL THEN "" ELSE s.Question_type END as Question_type,
                           CASE WHEN s.Option IS NULL THEN "" ELSE s.Option END as option1,
                           CASE WHEN s.show_commentbox IS NULL THEN 0 ELSE s.show_commentbox END as show_commentbox,
                           CASE WHEN s.commentbox_display_style IS NULL THEN 0 ELSE s.commentbox_display_style END as commentbox_display_style,
                           CASE WHEN s.commentbox_label_text IS NULL THEN "" ELSE s.commentbox_label_text END as commentbox_label_text,
                           CASE WHEN s.sort_order IS NULL THEN 0 ELSE s.sort_order END as sort_order,
                           CASE WHEN elmc.option IS NULL THEN "" ELSE elmc.option END as lang_option 
                           ',false);
        $this->db->from('survey s');
        $this->db->join('survey_category_relation scr','scr.Question_id = s.id');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="15" and elmc.modules_id=s.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('scr.survey_category_id',$category_id);
        $this->db->order_by('s.sort_order',NULL,FALSE);
        if($user_id!='')
        {
            $this->db->where('s.id not IN (SELECT ps.Question_id  FROM `poll_survey` ps join survey s on s.id=ps.Question_id WHERE `User_id` = '.$user_id.' and Event_id='.$event_id.' group by ps.Question_id ORDER BY `User_id` DESC )', NULL, FALSE);
            $this->db->where('s.Event_id',$event_id);
        }
        else
        {   
            $this->db->where('s.Event_id',$event_id);
        }
        $this->db->where('s.show_question','1');
        $this->db->group_by('s.id');

        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        //echo $this->db->last_query();exit;
        if(!empty($res2))
        {

            foreach ($res2 as $key => $value) 
            {
               
                $fin_arr[$key]['Question_id']=$value['Question_id'];
                $fin_arr[$key]['Question']=$value['Question'];
                $fin_arr[$key]['Question_type']=$value['Question_type'];
                $fin_arr[$key]['Option']=json_decode($value['option1']);
                $fin_arr[$key]['show_commentbox']=$value['show_commentbox'];
                $fin_arr[$key]['commentbox_display_style']=$value['commentbox_display_style'];
                $fin_arr[$key]['commentbox_label_text']=$value['commentbox_label_text'];
                $fin_arr[$key]['lang_option']= empty($value['lang_option']) ? [] : json_decode($value['lang_option']);
                //$fin_arr[$key]['sort_order']=$value['sort_order'];

            }
        }
        return $fin_arr;
    }
    public function is_survey_avilable($event_id=null,$category_id)
    {
        $this->db->select('CASE WHEN s.id IS NULL THEN "" ELSE s.id END as Question_id,
                           CASE WHEN s.Question IS NULL THEN "" ELSE s.Question END as Question,
                           CASE WHEN s.Question_type IS NULL THEN "" ELSE s.Question_type END as Question_type,
                           CASE WHEN s.Option IS NULL THEN "" ELSE s.Option END as option1',false);
        $this->db->from('survey s');
        $this->db->join('survey_category_relation scr','scr.Question_id = s.id');
        $this->db->where('scr.survey_category_id',$category_id);
        $this->db->where('s.Event_id',$event_id);
        $this->db->where('s.show_question','1');
        $this->db->group_by('s.id');

        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        if(empty($res2))
        {
            $msg['msg'] = "There are no available surveys right now.";
            $msg['state'] = 1;
        }
        else
        {   
            $data = $this->db->select('relese_datetime')->where('survey_id',$category_id)->get('survey_category')->row_array();
            if(!empty($data['relese_datetime']))
            {   
                $date = date('Y-m-d H:i:s',strtotime($data['relese_datetime']));
                date_default_timezone_set("UTC");
                $time_zone = $this->get_event_timezone($event_id);
                $cdate=date('Y-m-d H:i:s');
                if(!empty($time_zone['Event_show_time_zone']))
                {
                if(strpos($time_zone['Event_show_time_zone'],"-")==true)
                { 
                  $arr=explode("-",$time_zone['Event_show_time_zone']);
                  $intoffset=$arr[1]*3600;
                  $intNew = abs($intoffset);
                  $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                }
                if(strpos($time_zone['Event_show_time_zone'],"+")==true)
                {
                  $arr=explode("+",$time_zone['Event_show_time_zone']);
                  $intoffset=$arr[1]*3600;
                  $intNew = abs($intoffset);
                  $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                }
                }      
                if($date > $cdate)
                {   
                    $date_format = $this->get_date_time_format($event_id);
                    $format = ($date_format['format_time'] == '0') ? 'h:i A':'H:i';
                    $d_format = ($date_format['date_format'] == '0') ? 'd/m/Y' : 'm/d/Y';
                    
                    $date = date($d_format,strtotime($data['relese_datetime']));
                    $time = date($format,strtotime($data['relese_datetime']));

                    $msg['msg'] = "This Survey is not available yet. It will be available at ".$time." – ".$date;
                    $msg['state'] = 1;
                }
                else
                {
                    $msg['msg'] = "";
                    $msg['state'] = 0;
                }
            }
            else
            {
                $msg['msg'] = "";
                $msg['state'] = 0;
            }
        }
        return $msg;
    }
    public function get_date_time_format($event_id)
    {
        $this->db->select('e.date_format,f.format_time');
        $this->db->from('event e');
        $this->db->join('fundraising_setting f','f.Event_id = e.Id',left);
        $this->db->where('e.Id',$event_id);
        $query = $this->db->get();
        $res=$query->row_array();
        return $res;
    }
    public function get_event_timezone($event_id)
    {
        $this->db->select('Event_show_time_zone');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();
        $res=$query->row_array();
        return $res;
    }
    public function check_survey_status($survey_id,$event_id)
    {
        //echo $survey_id; exit();
        $this->db->select('*');
        $this->db->from('survey_category sc');
        $this->db->where('sc.survey_id',$survey_id);
        $this->db->where('sc.event_id',$event_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        //echo $this->db->last_query(); exit();
        return $res2;
    }
}
        
?>