<?php
class Map_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }
    public function getMapsListByEventId($event_id)
    {
        $this->db->select('CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Id,CASE WHEN m.Map_title IS NULL THEN "" ELSE m.Map_title END as Map_title',false);
        $this->db->from('map m');
        $this->db->where('m.Event_id',$event_id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;
    }
    public function getMapsDetailsByMapId($event_id,$map_id,$lang_id=NULL)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Id,(CASE WHEN elmc.title IS NULL THEN CASE WHEN m.Map_title IS NULL THEN "" ELSE m.Map_title END ELSE elmc.title END) as Map_title,(CASE WHEN elmc.content IS NULL THEN CASE WHEN m.Map_desc IS NULL THEN "" ELSE m.Map_desc END ELSE elmc.content END) as Map_desc,CASE WHEN m.satellite_view IS NULL THEN "0" ELSE m.satellite_view END as satellite_view,CASE WHEN m.place IS NULL THEN "" ELSE m.place END as place,CASE WHEN m.lat_long IS NULL THEN "" ELSE m.lat_long END as lat_long,CASE WHEN m.zoom_level IS NULL THEN "" ELSE m.zoom_level END as zoom_level,CASE WHEN m.Address IS NULL THEN "" ELSE m.Address END as Address,CASE WHEN m.Images IS NULL THEN "" ELSE m.Images END as Images,CASE WHEN m.include_map IS NULL THEN "1" ELSE m.include_map END as include_map,CASE WHEN m.area IS NULL THEN "" ELSE m.area END as area',false);
        $this->db->from('map m');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="10" and elmc.modules_id=m.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('m.Event_id',$event_id);
        $this->db->where('m.Id',$map_id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        if(!empty($res1[0]['Images']))
        {
            $decode_image=json_decode($res1[0]['Images']);
            $res1[0]['Images']=$decode_image[0];
            list($width,$height) = getimagesize($_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$res1[0]['Images']);
            
            $res1[0]['dimensions'] = ['width'=>($width) ? $width : '',"height"=>($height) ? $height : ''];
        }
        if(!empty($res1[0]['lat_long']))
        {
            $arr=explode(',',$res1[0]['lat_long']);
            $res1[0]['lat']=$arr[0];
            $res1[0]['long']=$arr[1];
        }
        else
        {
            if(!empty($res1))
            {
                $res1[0]['lat']="";
                $res1[0]['long']="";
            }
            
        }
        return $res1;
    }

    public function getImageMappingDetailsByMapId($map_id,$event_id,$user_id,$lang_id=NULL)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('m.*,u.Firstname,u.Lastname,e.Id as exid,CASE WHEN m.coords IS NULL THEN "" ELSE m.coords END as coords,CASE WHEN m.user_id IS NULL THEN "" ELSE m.user_id END as user_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exid,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN m.session_id IS NULL THEN "" ELSE m.session_id END as session_id,CASE WHEN m.seq_no IS NULL THEN "" ELSE m.seq_no END as seq_no,u.Id as id',false);
        $this->db->from('map_image_mapping m');
        $this->db->join('user u','m.user_id=u.Id','left');
        $this->db->join('exibitor e','e.user_id=u.Id','left');
        $this->db->where('e.Event_id',$event_id);
        $this->db->where('m.map_id',$map_id);
        $this->db->group_by('m.id');
        $query = $this->db->get();
        $res = $query->result_array();
        foreach ($res as $key => $value) {
            if(!empty($value['company_logo']))
            {
                $decode_image=json_decode($value['company_logo']);
                $res[$key]['company_logo']=$decode_image[0];
            }
            if(!empty($value['session_id']))
            {
                $session_id=explode(",",$value['session_id']);
                $array_ids = $this->getAgendaArray($user_id,$event_id);
                $res[$key]['session'] = array();
                $res[$key]['is_session'] = '0';
                foreach ($session_id as $skey => $svalue) {
                    if(in_array($svalue,$array_ids))
                    {
                        $this->db->select('a.Id as sid,(CASE WHEN elmc.title IS NULL THEN a.Heading ELSE elmc.title END) as session_heading,a.Start_date,a.Start_time')->from('agenda a');
                        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="1" and elmc.modules_id=a.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
                        $this->db->where('a.Id',$svalue);
                        $squery = $this->db->get();
                        $sres = $squery->result_array();
                        if(!empty($sres[0]))
                        {
                            $res[$key]['session'][]=$sres[0];
                            $res[$key]['is_session'] = '1';
                        }
                        else
                        {
                            $res[$key]['session'] = array();
                            $res[$key]['is_session'] = '0';
                        }
                    }
                }
            }
            else
            {
                $res[$key]['session'] = array();
                $res[$key]['is_session'] = '0';
            }
        }
        return $res;
    }
    public function getAgendaArray($user_id,$event_id)
    {
        $this->db->select('aci.agenda_id,');
        $this->db->from('attendee_agenda_relation ar');
        
        $this->db->join('agenda_category_relation aci', 'aci.category_id = ar.agenda_category_id');
        $this->db->join('agenda_categories ac', 'ac.Id = aci.category_id AND ac.Id = ar.agenda_category_id');
        if($user_id!='')
            $this->db->where('ar.attendee_id',$user_id);
        else
            $this->db->where('ac.categorie_type','1');
        $this->db->where('ar.event_id',$event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        if($user_id!='' && empty($res))
        {
            $this->db->select('aci.agenda_id,');
            $this->db->from('attendee_agenda_relation ar');
            
            $this->db->join('agenda_category_relation aci', 'aci.category_id = ar.agenda_category_id');
            $this->db->join('agenda_categories ac', 'ac.Id = aci.category_id');
            $this->db->where('ac.categorie_type','1');
            $this->db->where('ar.event_id',$event_id);
            $query = $this->db->get();
            $res = $query->result_array();
        }
        $agenda_array = array();
        foreach ($res as $key => $value) {
                $agenda_array[] = $value['agenda_id'];
            }
        return $agenda_array;        
    }
    public function getUser($token)
    {
        $token = ($token == NULL) ? '' : $token;
        return $this->db->select('*')->from('user')->where('token',$token)->limit(1)->get()->row_array();
    }
    public function getMapsListByEventIdNew($event_id,$lang_id=NULL)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Id,
                           (CASE WHEN elmc.title IS NULL THEN CASE WHEN m.Map_title IS NULL THEN "" ELSE m.Map_title END ELSE elmc.title END) as Map_title,
                           CASE WHEN m.lat_long IS NULL THEN "" ELSE m.lat_long END as lat_long,
                           CASE WHEN m.Address IS NULL THEN "" ELSE m.Address END as Address,
                           CASE WHEN m.Images IS NULL THEN "" ELSE m.Images END as Images,
                           CASE WHEN m.include_map IS NULL THEN "1" ELSE m.include_map END as include_map,
                           CASE WHEN m.check_dwg_files="" THEN "0" ELSE m.check_dwg_files END as check_dwg_files',
                           false);
        $this->db->from('map m');
        $this->db->join('event_lang_modules_content elmc','elmc.menu_id="10" and elmc.modules_id=m.Id and elmc.lang_id="'.$lang_id.'" and elmc.event_id='.$event_id,'left');
        $this->db->where('m.Event_id',$event_id);
        /*if($event_id == '585')
        {
            $this->db->where('m.Id != ','1565');
            $this->db->where('m.Id != ','2097');
            $this->db->where('m.Id != ','2098');
        }*/
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        for ($i=0; $i < count($res1); $i++)
        { 
            if(!empty($res1[$i]['Address']) || !empty($res1[$i]['lat_long']) && !empty($res1[$i]['Images']))
            {   
                $res1[$i]['google_map_icon'] = base_url().'assets/images/Google_Maps_icon.jpg';
                $res1[$i]['floor_plan_icon'] = base_url().'assets/images/Floor_Plan_icon.png';

            }
            elseif (!empty($res1[$i]['Images']))
            {
                $res1[$i]['google_map_icon'] = '';
                $res1[$i]['floor_plan_icon'] = base_url().'assets/images/Floor_Plan_icon.png';
            }
            elseif(!empty($res1[$i]['Address']) || !empty($res1[$i]['lat_long']))
            {
                $res1[$i]['google_map_icon'] = base_url().'assets/images/Google_Maps_icon.jpg';
                $res1[$i]['floor_plan_icon'] = '';   
            }
            else
            {
                $res1[$i]['google_map_icon'] = "";
                $res1[$i]['floor_plan_icon'] = "";
            }

            if(!empty($res1[$i]['Images']))
            {                   
                $decode_image=json_decode($res1[$i]['Images']);
                $res1[$i]['Images']=$decode_image[0];
            }

            if(!empty($res1[$i]['lat_long']))
            {
                $arr=explode(',',$res1[$i]['lat_long']);
                $res1[$i]['lat']=$arr[0];
                $res1[$i]['long']=$arr[1];
            }
            else
            {
                if(!empty($res1))
                {
                    $res1[$i]['lat']="";
                    $res1[$i]['long']="";
                }
                
            }

        }
        return $res1;
    }
    public function get_geojson($event_id,$map_id)
    {   
        if(!empty($map_id))
        {
            $file_path = $this->db->where('event_id',$event_id)->where('map_id',$map_id)->get('event_geojson')->row_array()['file_path'];
        }
        else
        {
            $file_path = $this->db->where('event_id',$event_id)->get('event_geojson')->row_array()['file_path'];
        }
        $file = './assets/geojson/'.$file_path;
        $data = file_get_contents($file);
        $data = json_decode($data,true);
        $this->load->model('native_single_fcm/App_login_model');
        foreach ($data['features'] as $key => $value)
        {   
            $tmp = $this->App_login_model->get_exhi_info_map($value['properties']['Text'],$event_id);
            $data['features'][$key]['properties']['page_id'] = (!empty($tmp['Id'])) ? $tmp['Id'] : '';
            $data['features'][$key]['properties']['exi_id'] = (!empty($tmp['user_id'])) ? $tmp['user_id'] : '';
            $img = (!empty($tmp['company_logo']) && $tmp['company_logo'] != '[null]') ? json_decode($tmp['company_logo']) : [];
            $data['features'][$key]['properties']['company_logo'] = (!empty($img[0])) ? $img[0] : '';
            $data['features'][$key]['properties']['comapany_name'] = (!empty($tmp['Heading'])) ? $tmp['Heading'] : '';
            $data['features'][$key]['properties']['exhi_desc'] = "";                
            $data['features'][$key]['properties']['map_id'] = $tmp['map_id']?:''; 
            if(empty($tmp['map_id']))
                unset($data['features'][$key]);
        }
        $data['features'] = array_values($data['features']);
        return $data;
    }
}        
?>