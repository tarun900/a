<?php
class Event_template_model extends CI_Model{
    function __construct()
    {
        
            parent::__construct();
            $this->db2 = $this->load->database('db1', TRUE);

    }
    public function get_menu_list($roleid,$eventid)
    {         
        $this->db->select('checkbox_values')->from('event');
        $this->db->where('Id',$eventid);
        $eres=$this->db->get()->result_array();
        $che=explode(",",$eres[0]['checkbox_values']);
        array_push($che,'27','28');
        $this->db->select('*');
        $this->db->from('role_permission r');
        $this->db->join('menu m', 'm.id=r.Menu_id');
        $this->db->where_in('r.Menu_id',$che);
        $this->db->where('r.Role_id',$roleid);
        $this->db->where('r.event_id',$eventid);
        $this->db->order_by("m.type");
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function delete_pptfile($eid,$pid)
    {
        $arr['Images']='';
        $arr['Image_lock']='';
        $this->db->where('Id',$pid);
        $this->db->where('Event_id',$eid);
        $this->db->update('presentation',$arr);
    }
    public function getaddnewspekeremail($sluge)
    {
        $this->db2->select('*');
        $this->db2->from('email_notification_templates');
        $this->db2->where('Slug',$sluge);
        $res=$this->db2->get();
       return $res->result_array();
    }
    public function get_singup_forms($eid)
    {
        $this->db->select('*');
        $this->db->from('signup_form');
        $this->db->where('event_id',$eid);
        $query = $this->db->get();
        $res = $query->result_array();  
        return $res;
    }
    public function get_subdomain($id)
    {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();  
        $Subdomain=$res[0]['Subdomain'];
        return $Subdomain;

    }
    public function get_event_template_list($id=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        if($user[0]->Id)
        {
            $this->db->where('e.Organisor_id',$user[0]->Id);
        }
        $query = $this->db->get();
        $res = $query->result();    

        $event_res  = array();
        for($i=0;$i<count($res);$i++)
        {
            $event_res[$i]['Id']=$res[$i]->Id;
            $event_res[$i]['Event_name']=$res[$i]->Event_name;
            $event_res[$i]['Status']=$res[$i]->Status;
            $event_res[$i]['Event_type']=$res[$i]->Event_type;
            $event_res[$i]['Event_time']=$res[$i]->Event_time;
            $event_res[$i]['Event_time_option']=$res[$i]->Event_time_option;
            $event_res[$i]['Subdomain']=$res[$i]->Subdomain;
            $event_res[$i]['Description']=$res[$i]->Description;
            $event_res[$i]['Images']=$res[$i]->Images;
            $event_res[$i]['Logo_images']=$res[$i]->Logo_images;
            $event_res[$i]['Created_date']=$res[$i]->Created_date;
            $event_res[$i]['Start_date']=$res[$i]->Start_date;
            $event_res[$i]['End_date']=$res[$i]->End_date;
            $event_res[$i]['Organisor_id']=$res[$i]->Organisor_id;
            $event_res[$i]['Address']=$res[$i]->Address;
            $event_res[$i]['Background_color']=$res[$i]->Background_color;
            $event_res[$i]['Contact_us']=$res[$i]->Contact_us;

        }
        if($this->data['user']->Role_name == 'Client')
        {
              $event_res_val=  array_merge(array(),$event_res); 
        }
        else if($this->data['user']->Role_name == 'Attendee')
        {
              $event_res_val=  array_merge(array(),$event_res); 
        }
        else
        {
            $event_res_val = $event_res;
        }
       return $event_res_val;
    }

    public function get_sub($Event_id=null)
    {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id',$Event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_acc_name($Organisor_id)
    {
        $this->db->select('acc_name');
        $this->db->from('user');
        $this->db->where('Id',$Organisor_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $acc_name=$res[0]['acc_name'];
        return $acc_name;
    }
    public function get_event_template_by_id_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');

        $this->db->select('Id,allow_show_all_agenda');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*')->from('fundraising_setting');
        $this->db->where('Event_id',$res[0]['Id']);
        $fquery = $this->db->get();
        $fres = $fquery->result_array(); 
        $res[0]['linkedin_login_enabled']=$fres[0]['linkedin_login_enabled'];
        $this->db->select('*')->from('user');
        $this->db->where('Id',$res[0]['Organisor_id']);
        $uqu=$this->db->get();
        $ures=$uqu->result_array();
        $this->db2->select('*')->from('organizer_user');
        $this->db2->where('Email',$ures[0]['Email']);
        $oqu=$this->db2->get();
        $ores=$oqu->result_array();
        $res[0]['signature_active']=$ores[0]['signature_active']; 


        $menulist=$this->geteventmenu_list($res[0]['Id'],NULL,NULL);

        $category_list=$this->db->where('event_id',$res[0]['Id'])->get('agenda_categories')->result_array();
        $agenda_category=$this->get_agenda_category_id_by_user($res[0]['Id']);
        $primary_agenda=$this->get_primary_category_id($res[0]['Id']);
        if(count($category_list)==1)
        {
            $cid=array($category_list[0]['cid']); 
        }
        else
        {
            if(!empty($agenda_category[0]['agenda_category_id']))
            {
              $cid=array_column($agenda_category,'agenda_category_id');
            }
            else
            {
              $cid=array($primary_agenda[0]['Id']);
            }
        }
        $this->db->select('*')->from('event_banner_image');
        $this->db->where('event_id',$res[0]['Id']);
        $this->db->where('image_type','1');
        $lefthandmenu_images_list=$this->db->get()->result_array();
        foreach ($lefthandmenu_images_list as $key => $value) {
            $this->db->select('ebc.*,m.pagetitle,m.menuname')->from('event_banner_coord ebc');
            $this->db->join('menu m','ebc.menuid = m.Id and ebc.menuid!=""','left');
            if(!empty(array_filter($cid)) && $res[0]['allow_show_all_agenda']!='1')
            {
                if(in_array('1',array_column($menulist,'id')))
                {
                    $this->db->where('(ebc.agenda_id IN ("'.implode(",",array_filter($cid)).'") OR ebc.agenda_id IS NULL)');
                }
                else
                {
                    $this->db->where('ebc.agenda_id IS NULL');   
                }
            }
            $this->db->where('ebc.banner_id',$value['id']);
            $coords=$this->db->get()->result_array();
            $lefthandmenu_images_list[$key]['coords']=$coords;
        }
        $res[0]['lefthandmenu_images_list']=$lefthandmenu_images_list;   
        return $res;   
    }
    public function get_event_template_by_id_list_preview($Subdomain=null,$data)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array(); 
        $this->db->select('e.*');
        $this->db->from('temp_event e');
        $this->db->join('user u', 'u.id=e.Organisor_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;   
    }
    public function update_temp_events($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
    
    }
    public function update_banner_images($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
       
    }
    public function update_logo_images($Subdomain,$data)
    {
        $this->db->select('Id');
        $this->db->from('temp_event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res)==0)
        {
            $this->db->insert("temp_event",$data);
        }
        else
        {
            $this->db->where("Subdomain",$Subdomain);
            $this->db->update("temp_event",$data);
        }
    }
    public function get_speaker_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        /*if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }*/
        $this->db->where('r.Id =',7);
        $this->db->where('u.is_moderator','0');
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Lastname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Lastname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Title']=$res[$i]->Title;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
    public function get_speaker_list_index($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        /*if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }*/
        $this->db->where('r.Id =',7);
        $this->db->where('u.key_people =',1);
        $this->db->where('u.is_moderator','0');
        
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Lastname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Lastname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Title']=$res[$i]->Title;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
    public function get_ex_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }
        $this->db->where('r.Id =',6);
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        
        $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Firstname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Firstname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
    public function get_speaker_list_index_by_firstname($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.*,u.Id as Id,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$res[0]['Id']);
        /*if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }*/
        $this->db->where('r.Id =',7);
        $this->db->where('u.key_people =',1);
        $this->db->where('u.is_moderator','0');
        
        //$this->db->where('r.Id !=',3);
        //$this->db->where('r.Id !=',4);
        $this->db->group_by('u.Id');
        
        $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();
                
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = $res[$i]->Firstname;
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Firstname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $speakers[ucfirst($al)][$i]['Id']=$res[$i]->Id;
                $speakers[ucfirst($al)][$i]['Firstname']=$res[$i]->Firstname;
                $speakers[ucfirst($al)][$i]['Lastname']=$res[$i]->Lastname;
                $speakers[ucfirst($al)][$i]['Company_name']=$res[$i]->Company_name;
                $speakers[ucfirst($al)][$i]['Title']=$res[$i]->Title;
                $speakers[ucfirst($al)][$i]['Speaker_desc']=$res[$i]->Speaker_desc;
                $speakers[ucfirst($al)][$i]['Email']=$res[$i]->Email;
                $speakers[ucfirst($al)][$i]['Logo']=$res[$i]->Logo;
            }
        }

        return $speakers;
    }
    public function get_agenda_list_by_time($Subdomain=null,$id_arr,$cid=null)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,count(reu.User_id) as totalsave,a.Heading as tname,st.type_name as Types');
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id=a.Types');
        $where = "FIND_IN_SET(a.Id,ua.agenda_id) > 0";
        $this->db->join('users_agenda ua',$where,'left');
        $this->db->join('relation_event_user reu','ua.user_id=reu.User_id and reu.Event_id='.$res[0]['Id'],'left');
        $this->db->join('agenda_category_relation acr','acr.agenda_id=a.Id','right');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        else
        {
            $cid=implode(",",$cid);
            $this->db->where_in('acr.category_id',explode(",",$cid));    
        }
        $this->db->where('e.Id',$res[0]['Id']);
        //$this->db->where("a.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->group_by('a.Id');
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $this->db->order_by('CAST(tname AS UNSIGNED), tname asc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        for($i=0; $i<count($res); $i++)
        {
            $timedate=$res[$i]->Start_time;
            $prev="";
            if($prev=="" || $timedate!=$prev)
            {
                $prev=$timedate;
                $agendas[$timedate][$i]['Id']=$res[$i]->Id;
                $agendas[$timedate][$i]['Heading']=$res[$i]->Heading;
                $agendas[$timedate][$i]['Types']=$res[$i]->Types;
                $agendas[$timedate][$i]['Start_date']=$res[$i]->Start_date;
                $agendas[$timedate][$i]['Start_time']=$res[$i]->Start_time;
                $agendas[$timedate][$i]['End_date']=$res[$i]->End_date;
                $agendas[$timedate][$i]['End_time']=$res[$i]->End_time;
                $agendas[$timedate][$i]['Address_map']=$res[$i]->Address_map;
                $agendas[$timedate][$i]['Map_title']=$res[$i]->Map_title;
                $agendas[$timedate][$i]['custom_speaker_name']=$res[$i]->custom_speaker_name;
                $agendas[$timedate][$i]['custom_location']=$res[$i]->custom_location;
                $agendas[$timedate][$i]['Event_id']=$res[$i]->Event_id;
                if(!empty($res[$i]->Maximum_People))
                {
                    $agendas[$timedate][$i]['Place_left']=$res[$i]->Maximum_People-$res[$i]->totalsave;
                }
                else
                {
                    $agendas[$timedate][$i]['Place_left']='';
                }
            }
        }

        return $agendas;
    }
    public function get_agenda_list($Subdomain=null,$id_arr,$cid=null)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,count(reu.User_id) as totalsave,st.type_name as Types');
        $this->db->from('agenda a');
        $this->db->join('session_types st','st.type_id=a.Types');
        $where = "FIND_IN_SET(a.Id,ua.agenda_id) > 0";
        $this->db->join('users_agenda ua',$where,'left');
        $this->db->join('relation_event_user reu','ua.user_id=reu.User_id and reu.Event_id='.$res[0]['Id'],'left');
        $this->db->join('agenda_category_relation acr','acr.agenda_id=a.Id','right');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        else
        {
            $cid=implode(",",$cid);
            $this->db->where_in('acr.category_id',explode(",",$cid));    
        }
        $this->db->where('e.Id',$res[0]['Id']);
        //$this->db->where("a.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->group_by('a.Id');
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
		$this->db->order_by('a.Heading asc');
        $query = $this->db->get();
        $res = $query->result();

        $agendas = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            if($prev=="" || $res[$i]->Start_date!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Id']=$res[$i]->Id;
                $agendas[$res[$i]->Start_date][$i]['Heading']=$res[$i]->Heading;
                $agendas[$res[$i]->Start_date][$i]['Types']=$res[$i]->Types;
                $agendas[$res[$i]->Start_date][$i]['Start_date']=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Start_time']=$res[$i]->Start_time;
                $agendas[$res[$i]->Start_date][$i]['End_date']=$res[$i]->End_date;
                $agendas[$res[$i]->Start_date][$i]['End_time']=$res[$i]->End_time;
                $agendas[$res[$i]->Start_date][$i]['Address_map']=$res[$i]->Address_map;
                $agendas[$res[$i]->Start_date][$i]['Map_title']=$res[$i]->Map_title;
                $agendas[$res[$i]->Start_date][$i]['custom_speaker_name']=$res[$i]->custom_speaker_name;
                $agendas[$res[$i]->Start_date][$i]['custom_location']=$res[$i]->custom_location;
                $agendas[$res[$i]->Start_date][$i]['Event_id']=$res[$i]->Event_id;
                if(!empty($res[$i]->Maximum_People)){
                $agendas[$res[$i]->Start_date][$i]['Place_left']=$res[$i]->Maximum_People-$res[$i]->totalsave;
                }
                else
                {
                    $agendas[$res[$i]->Start_date][$i]['Place_left']='';
                }
                $agendas[$res[$i]->Start_date][$i]['session_image']=$res[$i]->session_image;
            }
        }
        return $agendas;
    }

    /*public function get_agenda_value_by_id($Subdomain = NULL, $id = NULL)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,m.Address as Map_address, m.Images as Map_image,doc.document_file,sc.survey_name');
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        $this->db->join('document_files doc', 'doc.document_id = a.document_id','left');
        $this->db->join('survey_category sc','sc.survey_id=a.survey_id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        $this->db->where('a.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        $arr=explode(",",$res[0]['Speaker_id']);
        for($i=0;$i<count($arr);$i++)
        {
            $this->db->select('*');
            $this->db->from('user u');
            $this->db->join('relation_event_user reu','reu.User_id=u.Id AND reu.Event_id='.$res[0]['Id'],'right');
            $this->db->where('reu.Event_id',$res[0]['Id']);
            $this->db->where('reu.Role_id','7');
            $this->db->where('u.Id',$arr[$i]);
            $q=$this->db->get();
            $result=$q->result_array();
            $info[$i]=$result[0];
        }
        $res[0]['speaker']=$info;
       
        return $res;
    }*/
    public function get_agenda_value_by_id($Subdomain = NULL, $id = NULL)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res1 = $query->result_array();

        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,m.Address as Map_address, m.Images as Map_image,doc.document_file,d.title as docs_title,sc.survey_name,p.Heading as presentation_heading,qs.Session_name');
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        $this->db->join('documents d','d.id=a.document_id','left');
        $this->db->join('document_files doc', 'doc.document_id = a.document_id','left');
        $this->db->join('presentation p','p.Id=a.presentation_id','left');
        $this->db->join('survey_category sc','sc.survey_id=a.survey_id','left');
        $this->db->join('qa_session qs','qs.Id=a.qasession_id','left');
        $this->db->where('e.Id',$res1[0]['Id']);
        $this->db->where('a.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        $arr=array_filter(explode(",",$res[0]['Speaker_id']));
        $info=array();
        if(count($arr)>0)
        {
            for($i=0;$i<count($arr);$i++)
            {
                $this->db->select('*,u.Id as speaker_id');
                $this->db->from('user u');
                $this->db->join('relation_event_user reu','reu.User_id=u.Id AND reu.Event_id='.$res1[0]['Id'],'right');
                $this->db->where('reu.Event_id',$res1[0]['Id']);
                $this->db->where('reu.Role_id','7');
                $this->db->where('u.Id',$arr[$i]);
                $q=$this->db->get();
                $result=$q->result_array();
                $info[$i]=$result[0];
            }
        }
        $res[0]['speaker']=$info;
        return $res;
    }

    public function get_agenda_types($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Types,other_types');
        $this->db->from('agenda');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        //$this->db->where("End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('Start_date asc');
        $this->db->order_by('Start_time asc');
        $this->db->group_by("Types"); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array(); 
        return $res1;

    }

    public function get_agenda_list_by_types($Subdomain=null,$id=null,$id_arr=null,$cid=null)
    {
        $this->db->protect_identifiers=false;
        $id = $this->uri->segment(6);
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        
        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,count(reu.User_id) as totalsave,a.Heading as tname,st.type_name as Types');
        $this->db->from('agenda a');
        $this->db->join('session_types st','a.Types=st.type_id');
        $where = "FIND_IN_SET(a.Id,ua.agenda_id) > 0";
        $this->db->join('users_agenda ua',$where,'left');
        $this->db->join('relation_event_user reu','ua.user_id=reu.User_id and reu.Event_id='.$res[0]['Id'],'left');
        $this->db->join('agenda_category_relation acr','acr.agenda_id=a.Id','right');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        else
        {
            $cid=implode(",",$cid);
            $this->db->where_in('acr.category_id',explode(",",$cid));    
        }
        $this->db->where('a.Event_id',$res[0]['Id']);
        //$this->db->where("a.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->group_by('a.Id');
        $this->db->order_by('st.order_no=0,st.order_no',NULL,false);
        $this->db->order_by('CAST(tname AS UNSIGNED), tname asc');
		//$this->db->order_by('tname asc');
        $query1 = $this->db->get();
        $res1 = $query1->result();

        $agendas = array();
        for($i=0; $i<count($res1); $i++)
        {
            $prev="";
            if($prev=="" || $res1[$i]->Types!=$prev)
            {
                if($res1[$i]->Types == 'Other')
                {
                    $res1[$i]->Types = $res1[$i]->other_types;
                }
                $prev=$res1[$i]->Types;
                $agendas[$res1[$i]->Types][$i]['Id']=$res1[$i]->Id;
                $agendas[$res1[$i]->Types][$i]['Types']=$res1[$i]->Types;
                $agendas[$res1[$i]->Types][$i]['Heading']=$res1[$i]->Heading;
                $agendas[$res1[$i]->Types][$i]['Start_date']=$res1[$i]->Start_date;
                $agendas[$res1[$i]->Types][$i]['Start_time']=$res1[$i]->Start_time;
                $agendas[$res1[$i]->Types][$i]['End_date']=$res1[$i]->End_date;
                $agendas[$res1[$i]->Types][$i]['End_time']=$res1[$i]->End_time;
                $agendas[$res1[$i]->Types][$i]['Address_map']=$res1[$i]->Address_map;
                $agendas[$res1[$i]->Types][$i]['custom_speaker_name']=$res1[$i]->custom_speaker_name;
                $agendas[$res1[$i]->Types][$i]['custom_location']=$res1[$i]->custom_location;
                $agendas[$res1[$i]->Types][$i]['Map_title']=$res1[$i]->Map_title;
                $agendas[$res1[$i]->Types][$i]['Event_id']=$res1[$i]->Event_id;
                if(!empty($res1[$i]->Maximum_People))
                {
                    $agendas[$res1[$i]->Types][$i]['Place_left']=$res1[$i]->Maximum_People-$res1[$i]->totalsave;
                }
                else
                {
                    $agendas[$res1[$i]->Types][$i]['Place_left']='';
                }
                $agendas[$res1[$i]->Types][$i]['session_image']=$res1[$i]->session_image;
            }
        }   

        return $agendas;

    }

    public function get_attendee_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.Id as Id,u.*,r.Id as Rid,u.Id as uid,u.Active,ea.extra_column,ea.check_in_attendee,ea.views_id,(CASE WHEN ea.allow_contact_me IS NULL THEN "1" ELSE ea.allow_contact_me END) as allow_contact_me');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->join('event_attendee ea','u.Id=ea.Attendee_id and ea.Event_id=e.Id','left');
        $this->db->where('e.Id',$res[0]['Id']);
        /*if($orid!=NULL)
        {
          $this->db->where('u.Id !=',$orid);
        }*/
        $this->db->where('r.Id',4);
        $this->db->where( '(u.Firstname!="" Or u.Lastname!="")' );
        $this->db->order_by('u.Lastname');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result();  
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = strtoupper($res[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Lastname!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $attendees[$al][$i]['Id']=$res[$i]->Id;
                $attendees[$al][$i]['Firstname']=$res[$i]->Firstname;
                $attendees[$al][$i]['Lastname']=$res[$i]->Lastname;
                $attendees[$al][$i]['Company_name']=$res[$i]->Company_name;
                $attendees[$al][$i]['Title']=$res[$i]->Title;
                $attendees[$al][$i]['Email']=$res[$i]->Email;
                $attendees[$al][$i]['Logo']=$res[$i]->Logo;
                $attendees[$al][$i]['Website_url']=$res[$i]->Website_url;
                $attendees[$al][$i]['Facebook_url']=$res[$i]->Facebook_url;
                $attendees[$al][$i]['Twitter_url']=$res[$i]->Twitter_url;
                $attendees[$al][$i]['Linkedin_url']=$res[$i]->Linkedin_url;
                $attendees[$al][$i]['extra_column']=$res[$i]->extra_column;
                $attendees[$al][$i]['check_in_attendee']=$res[$i]->check_in_attendee;
                $attendees[$al][$i]['views_id']=$res[$i]->views_id;
                $attendees[$al][$i]['allow_contact_me']=$res[$i]->allow_contact_me;
            }
        }
        return $attendees;
    } 

    public function get_exibitors_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $user_id =$user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('e.*');
        $this->db->from('exibitor e');
        $this->db->join('relation_event_user reu','reu.User_id = e.user_id');
        $this->db->where('reu.Role_id','6');
        $this->db->where('reu.Event_id',$res[0]['Id']);
        $this->db->where('e.Event_id',$res[0]['Id']);
        if($user_id)
        {
            $this->db->where('e.user_id !=',$user_id);
        }
        
        $this->db->order_by('e.Heading');
        //$this->db->limit(10);
        $this->db->group_by('e.Id');
        $query = $this->db->get();
        $res = $query->result();  
        $exibitors = array();

        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = strtoupper($res[$i]->Heading);
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Heading!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $exibitors[$al][$i]['Id']=$res[$i]->Id;
                $exibitors[$al][$i]['Heading']=$res[$i]->Heading;
                $exibitors[$al][$i]['Short_desc']=$res[$i]->Short_desc;
                $exibitors[$al][$i]['Status']=$res[$i]->Status;
                $exibitors[$al][$i]['Notes']=$res[$i]->Notes;
                $exibitors[$al][$i]['Images']=$res[$i]->Images;
                $exibitors[$al][$i]['company_logo']=$res[$i]->company_logo;
                $exibitors[$al][$i]['website_url']=$res[$i]->website_url;
                $exibitors[$al][$i]['facebook_url']=$res[$i]->facebook_url;
                $exibitors[$al][$i]['twitter_url']=$res[$i]->twitter_url;
                $exibitors[$al][$i]['linkedin_url']=$res[$i]->linkedin_url;
                $exibitors[$al][$i]['phone_number1']=$res[$i]->phone_number1;
                $exibitors[$al][$i]['phone_number2']=$res[$i]->phone_number2;
                $exibitors[$al][$i]['email_address']=$res[$i]->email_address;
                $exibitors[$al][$i]['stand_number']=$res[$i]->stand_number;
            }
        }
        return $exibitors;
    }
    public function get_attendee_my_contact_user($Subdomain)
    {
                
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        if(empty($orid)){
            $orid="NULL";
        }
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$orid." AND asc2.approval_status!='2'",'left',false);
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$orid." AND asc1.approval_status='1'",'left',false);
        $this->db->where('u.Id !=',$orid);
        $wher="(asc2.event_id=".$res[0]['Id']." OR asc1.event_id=".$res[0]['Id'].")";
        $this->db->where($wher);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname');
        $aquery = $this->db->get();
        $ares = $aquery->result();
        $attendees = array();
        for($i=0; $i<count($ares); $i++)
        {
            $prev="";
            $fc = strtoupper($ares[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares[$i]->Lastname!=$prev)
            {
                $prev=$ares[$i]->Start_date;
                $attendees[$al][$i]['uid']=$ares[$i]->uid;
                $attendees[$al][$i]['Firstname']=$ares[$i]->Firstname;
                $attendees[$al][$i]['Lastname']=$ares[$i]->Lastname;
                $attendees[$al][$i]['Company_name']=$ares[$i]->Company_name;
                $attendees[$al][$i]['Title']=$ares[$i]->Title;
                $attendees[$al][$i]['Email']=$ares[$i]->Email;
                $attendees[$al][$i]['Logo']=$ares[$i]->Logo;
                $attendees[$al][$i]['contact_id']=$ares[$i]->contact_id;
                $attendees[$al][$i]['event_id']=$ares[$i]->event_id;
                $attendees[$al][$i]['from_id']=$ares[$i]->from_id;
                $attendees[$al][$i]['to_id']=$ares[$i]->to_id;
                $attendees[$al][$i]['approval_status']=$ares[$i]->approval_status;
            }
        }
        return $attendees;
    }
    public function get_sponsors_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $user_id =$user[0]->Id;
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*');
        $this->db->from('sponsors sp');
        $this->db->where('sp.Event_id',$res[0]['Id']);
        $this->db->order_by('sp.Company_name');
        $query = $this->db->get();
        $res = $query->result();  
        $sponsors = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            $fc = strtoupper($res[$i]->Company_name);
            $al = substr($fc, 0, 1);
            if($prev=="" || $res[$i]->Company_name!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $sponsors[$al][$i]['Id']=$res[$i]->Id;
                $sponsors[$al][$i]['Company_name']=$res[$i]->Company_name;
                $sponsors[$al][$i]['Short_desc']=$res[$i]->Short_desc;
                $sponsors[$al][$i]['Status']=$res[$i]->Status;
                $sponsors[$al][$i]['Notes']=$res[$i]->Notes;
                $sponsors[$al][$i]['Images']=$res[$i]->Images;
                $sponsors[$al][$i]['company_logo']=$res[$i]->company_logo;
                $sponsors[$al][$i]['website_url']=$res[$i]->website_url;
                $sponsors[$al][$i]['facebook_url']=$res[$i]->facebook_url;
                $sponsors[$al][$i]['twitter_url']=$res[$i]->twitter_url;
                $sponsors[$al][$i]['linkedin_url']=$res[$i]->linkedin_url;
            }
        }
        return $sponsors;
    }
    public function get_exibitors_data($Subdomain=null,$id = null)
    {

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('exibitor e');
        $this->db->where('e.Event_id',$res[0]['Id']);
        $this->db->where('e.Id',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;

    }
    public function get_sponsors_data($Subdomain=null,$id = null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('sponsors sp');
        $this->db->where('sp.Event_id',$res[0]['Id']);
        $this->db->where('sp.Id',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;
    }
    public function get_attendee_user_url($Subdomain=null,$id = null)
    {   
        $User_id = $this->uri->segment(5);
        $this->db->select('u.*,ru.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('r.Id',4);
        $this->db->where('u.Id',$User_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_attendee_social_url($Subdomain=null,$id = null)
    {   
        $this->db->select('ul.*,u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('user_social_links ul', 'ul.User_id = u.Id');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id',4);
        if($id!=null)
        {
          $this->db->where('u.Id',$id);
        }
        $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result_array(); 
        return $res;
    }

    public function get_speaker_social_url($Subdomain=null,$id = null)
    {   
        $this->db->select('ul.*,u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('user_social_links ul', 'ul.User_id = u.Id');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id',7);
        if($id != null)
        {
          $this->db->where('u.Id',$id);
        }
        $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result_array(); 
        return $res;
    }
    public function get_ex_by_page($id)
    {
        $this->db->select('user_id');
        $this->db->from('exibitor');
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array(); 
        $uid=$res[0]['user_id'];
        return $uid;
    }
    public function get_sp_by_page($id)
    {
        $this->db->select('user_id');
        $this->db->from('sponsors');
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array(); 
        $uid=$res[0]['user_id'];
        return $uid;
    }
    public function get_ex_social_url($Subdomain=null,$id = null)
    {   
        $this->db->select('u.*,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->where('u.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array(); 
        return $res;
    }
    public function get_speaker_user_url($Subdomain=null,$id = null)
    {   
        $this->db->select('u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->join('user_social_links ul', 'ul.User_id = u.Id','left');
        $this->db->where('r.Id',7);
        if($id != null)
        {
          $this->db->where('u.Id',$id);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
        
    }

    public function get_speaker_value($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('u.Id,u.Firstname,u.Lastname');
        $this->db->from('user u');
        $this->db->where('u.Id',$user[0]->Id);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('u.Id,u.Firstname,u.Lastname');
        $this->db->from('user u');
        $this->db->where('u.Id',$this->uri->segment(3));
        $query1 = $this->db->get();
        $res1 = $query1->result_array();

        $this->db->select('u.Firstname,s.Message');
        $this->db->from('speaker_msg s');
        $this->db->join('user u', 's.Sender_id = u.Id');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }

    public function get_s_id($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*');
        $this->db->from('speaker_msg');
        $this->db->where('Sender_id', $user[0]->Id);
        $this->db->where('Receiver_id', $this->input->post('Receiver_id'));
        $this->db->or_where('Sender_id', $this->input->post('Receiver_id'));
        $this->db->where('Receiver_id', $user[0]->Id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }   

    public function send_speaker_message($data)
    {
       $this->db->insert('speaker_msg', $data);
    }

    public function get_map_list($Subdomain=null)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('*');
        $this->db->from('map');
        $this->db->where('Event_id',$res[0]['Id']);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;
    }

    public function get_map($Subdomain=null,$id = null)
    {
       
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('*');
        $this->db->from('map');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Id',$id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;
    }

    public function get_social_list($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $orid = $this->data['user']->Id;
        $eid = $res[0]['Id'];
        $this->db->select('*');
        $this->db->from('social');
        if($orid)
        {
            $this->db->where('Organisor_id',$orid);
        }
        $this->db->where('Event_id',$eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_survey($Subdomain=null,$sid)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $user = $this->session->userdata('current_user');
        
        $this->db->select('s.*');
        $this->db->from('survey s');
        $this->db->join('survey_category_relation scr','scr.Question_id=s.Id');
        $this->db->join('poll_survey p', 's.Id = p.Question_id','left');
        $this->db->where('scr.survey_category_id',$sid);
        $this->db->where('s.id not IN (SELECT ps.Question_id  FROM `poll_survey` as ps inner join survey as s on s.id=ps.Question_id WHERE `User_id` = '.$user[0]->Id.' and Event_id='.$res[0]['Id'].' group by ps.Question_id ORDER BY `User_id` DESC )', NULL, FALSE);
        $this->db->where('s.Event_id',$res[0]['Id']);
        $this->db->where('show_question','1');
        $this->db->group_by('s.Id');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function get_surveyByPublicOpenEvent($event_id=null)
    {

     
        
        $this->db->select('s.*');
        $this->db->from('survey s');
        /*$this->db->join('poll_survey p', 's.Id = p.Question_id','left');
        $this->db->where('s.id not IN (SELECT ps.Question_id  FROM `poll_survey` as ps inner join survey as s on s.id=ps.Question_id WHERE `User_id` = '.$user[0]->Id.' and Event_id='.$res[0]['Id'].' group by ps.Question_id ORDER BY `User_id` DESC )', NULL, FALSE);*/
        $this->db->where('s.Event_id',$event_id);
        $this->db->where('show_question','1');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function add_survey($data)
    {

        if ($data['survey_array']['User_id'] != NULL)
        $array_survey['User_id'] = $data['survey_array']['User_id'];

        if ($data['survey_array']['Question_id'] != NULL)
        $array_survey['Question_id'] = $data['survey_array']['Question_id'];

        if($data['survey_array']['Answer'] != NULL)
        {
            $array_survey['Answer'] = $data['survey_array']['Answer'];
            $array_survey['answer_date'] = date('Y-m-d H:i:s');
            $this->db->insert('poll_survey', $array_survey);
            $survey_id = $this->db->insert_id();
            return $survey_id;
        }
        return 0;
    }

    public function get_survey_value($Subdomain=null,$sid)
    {

        $user = $this->session->userdata('current_user');
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res1 = $query->result_array();

        $this->db->select('p.*,s.*');
        $this->db->from('survey s');
        $this->db->join('survey_category_relation scr','scr.Question_id=s.Id');
        $this->db->join('poll_survey p', 's.Id = p.Question_id','left');
        $this->db->where('s.Event_id',$res1[0]['Id']);
        $this->db->where('scr.survey_category_id',$sid);
        $this->db->where('(p.User_id='.$user[0]->Id,NULL,FALSE);
        $this->db->or_where('p.User_id IS NULL)',NULL,FALSE);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }

    public function get_photos($Subdomain=null)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_presentation($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('p.*,m.Id as Address_map_id,m.Map_title as Map_title');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        $this->db->join('map m', 'm.Id = p.Address_map','left');
        $this->db->where('e.Id',$res[0]['Id']);
        // $this->db->where("p.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('p.Start_date asc');
        $this->db->order_by('p.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
        
        $presentations = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            if($prev=="" || $res[$i]->Start_date!=$prev)
            {
                $prev = $res[$i]->Start_date;
                $presentations[$res[$i]->Start_date][$i]['Id']=$res[$i]->Id;
                $presentations[$res[$i]->Start_date][$i]['Heading']=$res[$i]->Heading;
                $presentations[$res[$i]->Start_date][$i]['Short_desc']=$res[$i]->Short_desc;
                $presentations[$res[$i]->Start_date][$i]['Images']=$res[$i]->Images;
                $presentations[$res[$i]->Start_date][$i]['Place']=$res[$i]->Place;
                $presentations[$res[$i]->Start_date][$i]['Types']=$res[$i]->Types;
                $presentations[$res[$i]->Start_date][$i]['Start_date']=$res[$i]->Start_date;
                $presentations[$res[$i]->Start_date][$i]['Start_time']=$res[$i]->Start_time;
                $presentations[$res[$i]->Start_date][$i]['End_date']=$res[$i]->End_date;
                $presentations[$res[$i]->Start_date][$i]['End_time']=$res[$i]->End_time;
                $presentations[$res[$i]->Start_date][$i]['Status']=$res[$i]->Status;
                $presentations[$res[$i]->Start_date][$i]['Map_title']=$res[$i]->Map_title;
                $presentations[$res[$i]->Start_date][$i]['Address_map']=$res[$i]->Address_map;
                $presentations[$res[$i]->Start_date][$i]['presentation_file_type']=$res[$i]->presentation_file_type;
                $presentations[$res[$i]->Start_date][$i]['Event_id']=$res[$i]->Event_id;
            }
        }

        return $presentations;
    }

    public function get_presentation_with_type($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('p.*,m.Id as Address_map_id,m.Map_title as Map_title');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        $this->db->join('map m', 'm.Id = p.Address_map');
        $this->db->where('e.Id',$res[0]['Id']);
        // $this->db->where("p.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('p.Start_date asc');
        $this->db->order_by('p.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
        
        $presentation_types = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            if($prev=="" || $res[$i]->Types!=$prev)
            {
                $prev = $res[$i]->Types;
                $presentation_types[$res[$i]->Types][$i]['Id']=$res[$i]->Id;
                $presentation_types[$res[$i]->Types][$i]['Heading']=$res[$i]->Heading;
                $presentation_types[$res[$i]->Types][$i]['Short_desc']=$res[$i]->Short_desc;
                $presentation_types[$res[$i]->Types][$i]['Images']=$res[$i]->Images;
                $presentation_types[$res[$i]->Types][$i]['Place']=$res[$i]->Place;
                $presentation_types[$res[$i]->Types][$i]['Types']=$res[$i]->Types;
                $presentation_types[$res[$i]->Types][$i]['Start_date']=$res[$i]->Start_date;
                $presentation_types[$res[$i]->Types][$i]['Start_time']=$res[$i]->Start_time;
                $presentation_types[$res[$i]->Types][$i]['End_date']=$res[$i]->End_date;
                $presentation_types[$res[$i]->Types][$i]['End_time']=$res[$i]->End_time;
                $presentation_types[$res[$i]->Types][$i]['Status']=$res[$i]->Status;
                $presentation_types[$res[$i]->Types][$i]['Map_title']=$res[$i]->Map_title;
                $presentation_types[$res[$i]->Types][$i]['Address_map']=$res[$i]->Address_map;
                $presentation_types[$res[$i]->Types][$i]['presentation_file_type']=$res[$i]->presentation_file_type;
                $presentation_types[$res[$i]->Types][$i]['Event_id']=$res[$i]->Event_id;
            }
        }

        return $presentation_types;
    }

    public function get_presentation_by_id($Subdomain=null, $id=null)
    {
        
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*');
        $this->db->from('presentation');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_advertising_image($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id',$res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }

    public function get_advertising_images($Subdomain=null)
    {
        
        $Menu_id = $this->uri->segment(1);
        $this->db->select('Id');
        $this->db->from('menu');
        $this->db->where('pagetitle',$Menu_id);
        $query3 = $this->db->get();
        $res3 = $query3->result_array();

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id',$res[0]['Id']);
        if(!empty($res3))
        {
            $this->db->where('INSTR(Menu_id, '.$res3[0]["Id"].')');
        }
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }

    public function get_advertising_images_cms($Subdomain=null)
    {
        $Cms_id = $this->uri->segment(5);
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id',$res[0]['Id']);
        //$this->db->where('Cms_id',$Cms_id);
        //$this->db->where('INSTR(Cms_id, '.$Cms_id.')');
        $this->db->where("find_in_set('".$Cms_id."',`Cms_id`)>0");
        $query2 = $this->db->get();
        //echo  $this->db->last_query(); exit();
        $res2 = $query2->result_array();
        return $res2;
    }

    public function get_notes($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        if($user[0]->Role_name == 'Client')
        {
            $orid = $user[0]->Id;
        }
        else
        {
            $orid = $user[0]->Organisor_id;
            // /$orid=$res[0]['Organisor_id'];
        }

        $this->db->select('*');
        $this->db->from('notes');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        $this->db->where('User_id',$user[0]->Id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;

    }

    public function get_notes_by_id($Subdomain=null, $id=null)
    {
        $user = $this->session->userdata('current_user');
        $logged_id = $user[0]->Id;

        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        if($user[0]->Role_name == 'Client')
        {
            $orid = $user[0]->Id;
        }
        else
        {
            $orid = $user[0]->Organisor_id;
            //$orid = $res[0]['Organisor_id'];
        }

        $this->db->select('*');
        $this->db->from('notes');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        $this->db->where('User_id',$logged_id);
        $this->db->where('Id',$id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        if(!$res2)
            return redirect('Forbidden');
        return $res2;

    }

    public function add_notes($data)
    {
        $this->db->insert('notes', $data);
    }

    public function update_notes($data,$id)
    {
        $this->db->where('Id',$id);
        $this->db->update('notes', $data); 
    }

    public function delete_notes($id)
    {
        $this->db->where('Id',$id);
        $this->db->delete('notes');
    }

    public function get_survey_answer_chart($Subdomain = NULL)
    {
       
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $Event_id = $res[0]['Id'];
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id','left');
        $this->db->join('survey s', 's.Id = p.Question_id','left');
        $this->db->where('s.Event_id',$Event_id);
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }

    public function geteventmenu($eventid,$menu_id=null,$is_feture=null)
    {
        if(!empty($eventid))
        {
            $user = $this->session->userdata('current_user');
            $this->db->select('*')->from('event');
            $this->db->where('Id',$eventid);
            $qu=$this->db->get();
            $res=$qu->result_array();
            $eventmodule=explode(",",$res[0]['checkbox_values']);
            unset($eventmodule[array_search(8, $eventmodule)]);
            if($user[0]->Id!="" && $user[0]->Rid=='4')
            {
                $this->db->select('*')->from('event_attendee ea');
                $this->db->join('user_views uv','uv.view_id=ea.views_id','right');
                $this->db->where('ea.Event_id',$eventid);
                $this->db->where('ea.Attendee_id',$user[0]->Id);
                $vqu=$this->db->get();   
                $vres=$vqu->result_array();
                if(!empty($vres[0]['view_modules'])){
                    $check=explode(',', $res[0]['checkbox_values']);
                    $activemodules=explode(",",$vres[0]['view_modules']);   
                    $eventmodule=array_intersect($check, $activemodules);
                }
            }
            $this->db->select('m.id,m.pagetitle,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,em.is_feture_product,em.Background_color,em.show_in_front',FALSE);
            $this->db->from('menu m');
            $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
            
            if($menu_id!=null)
            {
                $this->db->where('m.id', $menu_id);
            }

            if($is_feture!=null)
            {
                $this->db->where('em.is_feture_product','1');
            }
            $this->db->where_in('m.Id',$eventmodule);
            //$this->db->limit(9);
            $query = $this->db->get();
            $res = $query->result_array();
        }
        return $res;
    }
    
    public function geteventmenu_list($eventid,$menu_id=null,$is_feture=null)
    {
        $this->db->select('e.checkbox_values,allow_show_all_agenda',FALSE);
        $this->db->from('event e');
        $this->db->where('e.Id', $eventid);
        $query = $this->db->get();
        $res = $query->result_array();
        $resdata=array();
        $user = $this->session->userdata('current_user');
        if(!empty($res))
        {
           $menudata=explode(',', $res[0]['checkbox_values']);
           $menudata = array_diff($menudata, array('5'));
           $menudata=  array_merge($menudata,array());
           if($user[0]->Id!="" && $user[0]->Rid=='4')
           {
                $this->db->select('*')->from('event_attendee ea');
                $this->db->join('user_views uv','uv.view_id=ea.views_id','right');
                $this->db->where('ea.Event_id',$eventid);
                $this->db->where('ea.Attendee_id',$user[0]->Id);
                $vqu=$this->db->get();   
                $vres=$vqu->result_array();
                if(!empty($vres[0]['view_modules'])){
                    $check=explode(',', $res[0]['checkbox_values']);
                    $activemodules=explode(",",$vres[0]['view_modules']);   
                    $menudata=array_intersect($check, $activemodules);
                } 
           }
           /*if($user[0]->Rid!='6')
           {
                if($user[0]->Rid=='4')
                {
                    $this->db->select('*')->from('exhibitor_representatives');
                    $this->db->where('event_id',$eventid);
                    $this->db->where('reps_user_id',$user[0]->Id);
                    $att_pre=$this->db->get()->result_array();
                    if(count($att_pre) <= 0)
                    {
                        $menukey=array_search(53,$menudata);
                        if($menukey!='')
                        {
                            unset($menudata[$menukey]);
                        }
                    }
                }
           }*/
            if($user[0]->role_type != '1')
            {   
                if($user[0]->Rid != '3')
                {
                    $menukey=array_search(53,$menudata);
                    if($menukey!='')
                    {
                        unset($menudata[$menukey]);
                    }
                }
            }
           $this->db->select('m.id,m.pagetitle,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,em.is_feture_product,em.show_in_front',FALSE);
           $this->db->from('menu m');
           $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
           $this->db->join('event e', 'e.Id=em.event_id', 'left');

           if($menu_id!=null)
           {
               $this->db->where('m.id', $menu_id);
           }

           $this->db->where_in('m.id',$menudata);

           $query = $this->db->get();
           $resdata = $query->result_array();
        }
        $this->load->model('Event_model');
        $eventmodule=$this->Event_model->geteventmodulues($eventid);
        $module=json_decode($eventmodule[0]['module_list']);
        $arr=array('22','23','24','25','42');
        $module=array_merge($module,$arr);
        foreach ($resdata as $key => $value) {
            if(!in_array($value['id'],$module))
            {
               unset($resdata[$key]);
            }
        }
        if(in_array('1',array_column($resdata,'id')))
        {
            $category_list=$this->db->where('event_id',$eventid)->get('agenda_categories')->result_array();
            if(count($category_list) > 1)
            {
                $agenda_category=$this->get_agenda_category_id_by_user($eventid);
                $primary_agenda=$this->get_primary_category_id($eventid);
                if(count($category_list)==1)
                {
                    $cid=array($category_list[0]['cid']); 
                }
                else
                {
                    if(!empty($agenda_category[0]['agenda_category_id']))
                    {
                      $cid=array_column($agenda_category,'agenda_category_id');
                    }
                    else
                    {
                      $cid=array($primary_agenda[0]['Id']);
                    }
                }

                $this->db->select('ac.*,(CASE WHEN em.title IS NULL THEN ac.category_name ELSE em.title END) as categoryname')->from('agenda_categories ac');
                $this->db->join('event_menu em','ac.Id=em.agenda_id and em.event_id='.$eventid,'left');
                $this->db->where('ac.event_id',$eventid);
                if($res[0]['allow_show_all_agenda']!='1')
                {
                    $cid=implode(",",$cid);
                    $this->db->where_in('ac.Id',explode(",",$cid));
                }
                $categorylist=$this->db->get()->result_array();
                $resdata['category_list']=$categorylist;
            }
        }
        return $resdata;
    }
    public function getAccname($id='')
    {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu','reu.User_id = tu.Id','left');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",3);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_agenda_category_id_by_user($eid)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*')->from('attendee_agenda_relation');
        $this->db->where('attendee_id',$user[0]->User_id);
        $this->db->where('event_id',$eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
	public function get_primary_category_id($eid)
    {
        $this->db->select('*')->from('agenda_categories');
        $this->db->where('event_id',$eid);
        $this->db->where('categorie_type','1');
        $res=$this->db->get()->result_array();
        return $res;
    }
	public function save_presentation_lock_images($pid,$data)
    {
        $this->db->select('*')->from('presentation_tool pt');
        $this->db->where('presentation_id',$pid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res) > 0)
        {
            $this->db->where('presentation_id',$pid);
            $this->db->update('presentation_tool',$data);
        }
        else
        {
            $data['presentation_id']=$pid;
            $this->db->insert('presentation_tool',$data);
        }
    }
    public function get_presentation_tool_data($pid)
    {
        $this->db->select('*')->from('presentation_tool pt');
        $this->db->where('presentation_id',$pid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function getCustomColumns($intEventId=0)
    {
        $this->db->select("*");
        $this->db->from("custom_column");
        $this->db->where("event_id",$intEventId);
        $this->db->where("crequire",'1');
        $this->db->order_by("column_id");
        $objQry = $this->db->get();
        $arrRes = $objQry->result_array();
        return $arrRes;
    }
    public function share_contact_infomation($share_data)
    {
        $this->db->select('*')->from('attendee_share_contact');
        $this->db->where('event_id',$share_data['event_id']);
        $this->db->where('from_id',$share_data['from_id']);
        $this->db->where('to_id',$share_data['to_id']);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)>0)
        {
            $this->db->where('event_id',$share_data['event_id']);
            $this->db->where('from_id',$share_data['from_id']);
            $this->db->where('to_id',$share_data['to_id']);
            unset($share_data['event_id']);
            unset($share_data['from_id']);
            unset($share_data['to_id']);
            $this->db->update('attendee_share_contact',$share_data);
            $shar_id=$res[0]['contact_id'];
        }
        else
        {
            $share_data['share_contact_datetime']=date('Y-m-d H:i:s');
            $this->db->insert('attendee_share_contact',$share_data);
            $shar_id=$this->db->insert_id();
        }
        return $shar_id;
    }
    public function get_share_conact($event_id,$from_id,$to_id)
    {
        $this->db->select('*')->from('attendee_share_contact');
        $this->db->where('event_id',$event_id);
        $where="(from_id='".$from_id."' OR to_id='".$from_id."')";
        $this->db->where($where);
        $where1="(from_id='".$to_id."' OR to_id='".$to_id."')";
        $this->db->where($where1);
        $this->db->where('approval_status !=','2');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_pending_share_conact($event_id,$to_id)
    {
        $this->db->select('asct.*,u.*,cu.country_name')->from('attendee_share_contact asct');
        $this->db->join('user u','u.Id=asct.from_id','left');
        $this->db->join('country cu','cu.id=u.Country','left');
        $this->db->where('asct.event_id',$event_id);
        $this->db->where('asct.to_id',$to_id);
        $this->db->where('asct.approval_status','0');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_share_contact_share_id($sid)
    {
        $this->db->select('*')->from('attendee_share_contact');
        $this->db->where('contact_id',$sid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
	public function get_sponsors_list_by_type($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*')->from('sponsors_type');
        $this->db->where('event_id',$res[0]['Id']);
        $this->db->order_by('type_position','asc');
        $stype = $this->db->get();
        $st_data = $stype->result_array();
        foreach ($st_data as $key => $value) 
        {
            $this->db->select('*');
            $this->db->from('sponsors sp');
            $this->db->where('sp.Event_id',$res[0]['Id']);
            $this->db->where('sp.st_id',$value['type_id']);
            $this->db->order_by('sp.Sponsors_name');
            $query = $this->db->get();
            $sres = $query->result_array();
            $st_data[$key]['sponsors']=$sres; 
        }
        $this->db->select('*');
        $this->db->from('sponsors sp');
        $this->db->where('sp.Event_id',$res[0]['Id']);
        $this->db->where('sp.st_id IS NULL');
        $this->db->order_by('sp.Sponsors_name');
        $query = $this->db->get();
        $sres = $query->result_array();
        $st_data[count($st_data)]['sponsors']=$sres; 
        return $st_data;
    }
    public function get_exibitors_list_by_type($Subdomain=null,$parent_c_id)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id',$res[0]['Id']);
        $this->db->order_by('type_position','asc');
        $stype = $this->db->get();
        $st_data = $stype->result_array();

        $this->load->model('Native_single_fcm/Exhibitor_model');
        $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
        foreach($c_keyword as $key => $value)
        {
            if($key == 0)
            {
                $where = "(FIND_IN_SET('".$value."',Short_desc))";
            }
            else
            {
                $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
            }
        }
        if(count($c_keyword) == '1')
        {   
            $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
        }
        else
        {   
            if(!empty($where))
            {   
                $where = '('.$where.')';
                if($keyword!='')
                $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
            }
        }
        foreach ($st_data as $key => $value) 
        {
            $this->db->select('e.*,u.Firstname,u.Lastname,c.country_name');
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id=e.user_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id','left');
            $this->db->join('country c','c.id = e.country_id');
            $this->db->where('e.Event_id',$res[0]['Id']);
            $this->db->where('e.et_id',$value['type_id']);
            $this->db->where($where);
            $this->db->order_by('e.Heading');
            $this->db->group_by('u.Id');
            $query = $this->db->get();
            $sres = $query->result_array();
            $st_data[$key]['exhibitor']=$sres; 
        }
        $this->db->select('e.*,u.Firstname,u.Lastname,c.country_name');
        $this->db->from('exibitor e');
        $this->db->join('user u','u.Id=e.user_id');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id','left');
        $this->db->join('country c','c.id = e.country_id');
        $this->db->where('e.Event_id',$res[0]['Id']);
        $this->db->where('e.et_id IS NULL');
        $this->db->where($where);
        $this->db->order_by('e.Heading');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $sres = $query->result_array();
        $st_data[count($st_data)]['exhibitor']=$sres;
        return $st_data;
    }
	
	public function get_exibitors_group_list_by_type($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->select('*')->from('exhibitor_group');
        $this->db->where('event_id',$res[0]['Id']);
        $this->db->order_by('group_position','asc');
        $stype = $this->db->get();
        $st_data = $stype->result_array();
        foreach ($st_data as $key => $value) 
        {
            $this->db->select('e.*,u.Firstname,u.Lastname');
            $this->db->from('exibitor e');
            $this->db->join('user u','u.Id=e.user_id');
            $this->db->where('e.Event_id',$res[0]['Id']);
            $this->db->where('e.eg_id',$value['group_id']);
            $this->db->order_by('e.Heading');
            $query = $this->db->get();
            $sres = $query->result_array();
            $st_data[$key]['exhibitor']=$sres; 
        }
        return $st_data;
    }
    public function myfavorites_save($mf_data)
    {
        $this->db->select('*')->from('my_favorites');
        $this->db->where($mf_data);
        $res=$this->db->get()->result_array();
        if(count($res)>0)
        {
          $this->db->where($mf_data);
          $this->db->delete('my_favorites');
          return '0';
        }
        else
        {
          $mf_data['datetime']=date('Y-m-d H:i:s');
          $this->db->insert('my_favorites',$mf_data);
          return '1';
        }
    }
    public function get_my_favorites_user_by_event_menu_id($eid,$uid,$mid)
    {
        $this->db->select('*')->from('my_favorites');
        $this->db->where('event_id',$eid);
        $this->db->where('user_id',$uid);
        $this->db->where('module_type',$mid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_favorites_contect_by_event_user($eid,$uid)
    {
        $this->db->select('m.Id as modules_id,(case WHEN em.title IS NULL THEN m.menuname ELSE em.title end) as menuname,(case when fmo.color IS NULL THEN "#5381ce" else fmo.color end) as color,fmo.position')->from('menu m');
        $this->db->join('event_menu em','m.Id=em.menu_id','left');
        $this->db->join('favorites_modules_order fmo','m.Id=fmo.modules_id AND fmo.event_id='.$eid,'left');
        $this->db->where('em.event_id',$eid);
        $this->db->where_in('m.Id',array('2','3','7','43'));
        $this->db->order_by('fmo.position');
        $res=$this->db->get()->result_array();
        foreach ($res as $key => $value) {
            $this->db->select('mf.datetime,mf.module_id,mf.module_type,mf.user_id,u.Firstname,u.Lastname,u.Company_name,u.Salutation,u.Email,u.Logo,e.Heading,e.stand_number,e.company_logo,s.Sponsors_name,s.Company_name,s.company_logo')->from('my_favorites mf');
            $this->db->join('user u','u.Id=mf.module_id AND (mf.module_type="2" OR mf.module_type="7")','left');
            $this->db->join('exibitor e','e.Id=mf.module_id AND mf.module_type="3"','left');
            $this->db->join('sponsors s','s.Id=mf.module_id AND mf.module_type="43"','left');
            $this->db->where('mf.event_id',$eid);
            $this->db->where('mf.module_type',$value['modules_id']);
            $this->db->where('mf.user_id',$uid);
            $res1=$this->db->get()->result_array();
            $res[$key]['user']=$res1;
        }
        return $res;
    }
    public function get_all_favorites_user_with_type_by_event_user($eid,$uid)
    {
        $this->db->select('mf.datetime,mf.module_id,mf.module_type,mf.user_id,u.Firstname,u.Lastname,u.Company_name,u.Salutation,u.Email,u.Logo,e.Heading,e.stand_number,e.company_logo,s.Sponsors_name,s.Company_name,s.company_logo')->from('my_favorites mf');
        $this->db->join('user u','u.Id=mf.module_id AND (mf.module_type="2" OR mf.module_type="7")','left');
        $this->db->join('exibitor e','e.Id=mf.module_id AND mf.module_type="3"','left');
        $this->db->join('sponsors s','s.Id=mf.module_id AND mf.module_type="43"','left');
        $this->db->where('mf.event_id',$eid);
        $this->db->where('mf.user_id',$uid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function save_my_favorites_modules_order($arr)
    {
        foreach ($arr as $key => $value) 
        {
            $this->db->select('*')->from('favorites_modules_order fmo');
            $this->db->where('event_id',$value['event_id']);
            $this->db->where('modules_id',$value['modules_id']);
            $res=$this->db->get()->result_array();
            if(count($res) > 0)
            {
                $this->db->where('event_id',$value['event_id']);
                $this->db->where('modules_id',$value['modules_id']);
                $this->db->update('favorites_modules_order',$value);
            }
            else
            {
                $this->db->insert('favorites_modules_order',$value);
            }
        }
    }
    public function get_all_modules_order_by_event_id($eid)
    {
        $this->db->select('*')->from('favorites_modules_order fmo');
        $this->db->where('event_id',$eid);
        $this->db->order_by('modules_id');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_hashtags_by_id($hid)
    {
        $this->db->select('*')->from('event_hashtags');
        $this->db->where('Id',$hid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_moderator_by_user($uid,$eid)
    {
        $moderator=$this->db->select('*')->from('moderator_relation')->where('user_id',$uid)->where('event_id',$eid)->get()->row_array();
        return $moderator;
    }
    public function get_agenda_list_by_ticket_type($eid,$ticket_id)
    {
        $this->db->select('a.*,m.Id as Address_map_id,m.Map_title as Map_title,count(ua.Id) as totalsave');
        $this->db->from('agenda a');
        $where = "FIND_IN_SET(a.Id,ua.agenda_id) > 0";
        $this->db->join('users_agenda ua',$where,'left');
        $this->db->join('agenda_ticket at','at.agenda_id=a.Id','right');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        $this->db->where('at.ticket_id',$ticket_id); 
        $this->db->where('e.Id',$eid);
        $this->db->group_by('a.Id');
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        for($i=0; $i<count($res); $i++)
        {
            $prev="";
            if($prev=="" || $res[$i]->Start_date!=$prev)
            {
                $prev=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Id']=$res[$i]->Id;
                $agendas[$res[$i]->Start_date][$i]['Heading']=$res[$i]->Heading;
                $agendas[$res[$i]->Start_date][$i]['Start_date']=$res[$i]->Start_date;
                $agendas[$res[$i]->Start_date][$i]['Start_time']=$res[$i]->Start_time;
                $agendas[$res[$i]->Start_date][$i]['End_date']=$res[$i]->End_date;
                $agendas[$res[$i]->Start_date][$i]['End_time']=$res[$i]->End_time;
                $agendas[$res[$i]->Start_date][$i]['Address_map']=$res[$i]->Address_map;
                $agendas[$res[$i]->Start_date][$i]['Map_title']=$res[$i]->Map_title;
                $agendas[$res[$i]->Start_date][$i]['custom_speaker_name']=$res[$i]->custom_speaker_name;
                $agendas[$res[$i]->Start_date][$i]['custom_location']=$res[$i]->custom_location;
                $agendas[$res[$i]->Start_date][$i]['Event_id']=$res[$i]->Event_id;
                $agendas[$res[$i]->Start_date][$i]['Maximum_People']=$res[$i]->Maximum_People;
                if(!empty($res[$i]->Maximum_People))
                {
                    //$agendas[$res[$i]->Start_date][$i]['Place_left']=$res[$i]->Maximum_People-($res[$i]->totalsave+$res[$i]->totaltempsave);
                    $agendas[$res[$i]->Start_date][$i]['Place_left']=$res[$i]->Maximum_People-$res[$i]->totalsave;
                }
                else
                {
                    $agendas[$res[$i]->Start_date][$i]['Place_left']='';
                }
                $agendas[$res[$i]->Start_date][$i]['session_image']=$res[$i]->session_image;
            }
        }

        return $agendas;
    }
    public function get_save_agenda_list_by_temp_useremil($user_email,$event_id)
    {
        $this->db->select('*')->from('temp_registration_user tru');
        $this->db->where('tru.email',$user_email);
        $this->db->where('event_id',$event_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_overlapping_agenda_by_temp_registre_user($data,$agenda_id,$event_id)
    {
        foreach ($agenda_id as $key => $value) 
        {
            if(!empty($value))
            {
                $this->db->select('*')->from('agenda');
                $this->db->where('Start_date',$data[0]['Start_date']);
                $this->db->where("(('".$data[0]['Start_time']."' BETWEEN `Start_time` AND `End_time`) OR (Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                $this->db->or_where("('".$data[0]['End_time']."' BETWEEN `Start_time` AND `End_time`) OR (End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."'))");
                $this->db->where('allow_clashing','0');
                $this->db->where('Id !=',$data[0]['Id']);
                $this->db->where('Id',$value);
                $qu1=$this->db->get();
                $res1=$qu1->result_array();
                if(count($res1)>0)
                {
                  $return=false;
                  break; 
                }
                else
                {
                  $return=true;
                }
            }
            else
            {
                $return=true;
            }
        }
        return $return;
    }
    /*public function update_email_temp_user($temp_uid,$email)
    {
        $this->db->where('temp_uid',$temp_uid);
        $this->db->update('temp_registration_user',array('email'=>$email));
    }*/
    public function save_temp_user_session($adata,$email,$eid)
    {
        $res=$this->get_save_agenda_list_by_temp_useremil($email,$eid);
        if(count($res) > 0)
        {   
            $tmp_sessions = explode(',',$res[0]['save_agenda_ids']);
            if(in_array($adata,$tmp_sessions))
            {
                $tmp_sessions = array_diff($tmp_sessions,[$adata]);
            }
            else
            {
                $tmp_sessions[] = $adata;
            }
            $data['save_agenda_ids'] = count($tmp_sessions) > 1 ? implode(',',array_filter($tmp_sessions)) : $tmp_sessions[0];

            $this->db->where('email',$email);
            $this->db->where('event_id',$eid);
            $this->db->update('temp_registration_user',$data);
            return $res[0]['temp_uid'];
        }
        else
        {
            $data['event_id']=$eid;
            $data['email']=$email;
            $data['save_agenda_ids'] = $adata;
            $this->db->insert('temp_registration_user',$data);
            return $this->db->insert_id();
        }
    }
    public function get_all_meeting_locations_by_event($eid)
    {
        $this->db->select('*')->from('meeting_location');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_surveys_category_by_event($eid)
    {
        $this->db->select('*')->from('survey_category');
        $this->db->where('event_id',$eid);
        $this->db->where('show_survey','1');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_favorites_data_Export($eid)
    {
        $this->db->select('mf.module_type,u.Firstname as fbyfnm,u.Lastname as fbylnm,u.Email as fbyemail,u.Company_name as fbycompany,u.Title as fbytitle,u1.Firstname,u1.Lastname,u1.Email,u1.Company_name,u1.Title,s.Company_name as sCompany_name,s.Sponsors_name')->from('my_favorites mf');
        $this->db->join('user u','u.Id=mf.user_id');
        $this->db->join('user u1','u1.Id=mf.module_id and mf.module_type!=43','left');
        $this->db->join('sponsors s','s.Id=mf.module_id and mf.module_type=43','left');
        $this->db->order_by('u.Id');
        $this->db->where('mf.event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_gamification_point_by_user_id($eid,$uid)
    {
        $this->db->select('sum(points) as gamification_points')->from('game_users_point');
        $this->db->where('user_id',$uid); 
        $this->db->where('event_id',$eid);
        $this->db->group_by('user_id');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_organizer_email_by_id($orgid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Id',$orgid);
        $res=$this->db->get()->row_array();
        return $res;
    }
    public function get_exibitor_categories($eid,$cid=NULL,$parent_c_id=NULL)
    {
      $this->db->select('*')->from('exhibitor_category ec');
      if(is_array($cid))
      {
         $this->db->where_in('ec.id',$cid);
      }
      elseif(!empty($cid) && $cid != 'null')
      {
        $this->db->where('ec.id',$cid);
      }
      if(!empty($parent_c_id))
      {
        $this->db->join('exibitor_category_relation ecr','ecr.exibitor_category_id = ec.id');
        $this->db->where('ecr.parent_category_id',$parent_c_id);
      }
      $this->db->where('ec.event_id',$eid);
      $this->db->order_by('ec.categorie_keywords');
      $res=$this->db->get()->result_array();
      return $res;
    }
    public function get_exibitors_parent_categories($eid)
    {
       $this->db->select('id as c_id,
                           CASE WHEN category IS NULL THEN "" ELSE category END AS category,
                           CASE WHEN categorie_icon IS NULL THEN "" ELSE categorie_icon END AS categorie_icon',false);
        $this->db->where('event_id',$eid);
        $this->db->where('category_type','1');
        $q = $this->db->get('exhibitor_category');
        $res = $q->result_array();
        return $res;
    }

    public function get_exibitor_list_by_category($eid,$keywords,$cntry_id,$exhi_ids,$venue=0,$start=null,$no_limit=false)
    {   
        $arr_keyword=array_filter(explode(",", $keywords));
        if(in_array('Halal',$arr_keyword))
        {
            $arr_keyword = array_diff($arr_keyword,array('Halal'));
            $check_halal = '1';
        }
        $countarr=count($arr_keyword);
        $this->db->select('e.*,c.country_name')->from('exibitor e');
        $this->db->join('country c','c.id = e.country_id');
        $this->db->where('e.Event_id',$eid);
        if(count($arr_keyword)>1)
        {
            $strwhere="(";
            foreach ($arr_keyword as $key => $value) {
                if($countarr==$key+1)
                {
                    $strwhere.="FIND_IN_SET('".$value."',e.Short_desc)";
                }
                else
                {
                    $strwhere.="FIND_IN_SET('".$value."',e.Short_desc) OR ";
                }
            }
            $strwhere.=")";
            $this->db->where($strwhere);

        }
        elseif(count($arr_keyword) != 0)
        {
            $strwhere="FIND_IN_SET('".$arr_keyword[0]."',e.Short_desc) != 0";   
            $this->db->where($strwhere);
        }
        if(is_array($cntry_id))
        {
            $this->db->where_in('e.country_id',$cntry_id);
            $this->db->where_in('e.Id',$exhi_ids);
        }
        elseif(!empty($cntry_id) && $cntry_id != 'null')
        {
            $this->db->where('e.country_id',$cntry_id);
            $this->db->where_in('e.Id',$exhi_ids);
        }
        if($check_halal)
        {
            $this->db->where("FIND_IN_SET('Halal',e.Short_desc) != 0");
            $this->db->where_in('e.Id',$exhi_ids);
        }
        if($venue == '1')
        {
            $this->db->where("FIND_IN_SET('Singapore Expo',e.Short_desc) != 0");
            $this->db->where_in('e.Id',$exhi_ids);
        }
        elseif($venue == '2')
        {
            $this->db->where("FIND_IN_SET('Suntec Singapore',e.Short_desc) != 0");
            $this->db->where_in('e.Id',$exhi_ids);
        }
        $start = ($start == '9') ? '10' : $start;
        if(!$no_limit && $eid == '585')
        {
            $this->db->limit(10,$start);   
        }
        $res=$this->db->order_by('e.Heading')->get()->result_array();
        return $res;
    }
    public function check_message_permisson($user_id,$cur_user,$event_id)
    {   
        $this->db->select('ea.*,u.Id');
        $this->db->from('event_attendee ea');
        $this->db->join('user u','u.Id = ea.Attendee_id');
        $this->db->where('u.Id',$cur_user);
        $this->db->where('ea.Event_id',$event_id);
        $res = $this->db->get()->row_array();
        
        if(!empty($res['group_id']))
        {
            $this->db->protect_identifiers=false;
            $this->db->select();
            $this->db->from('event_attendee ea');
            $this->db->where('ea.Attendee_id',$user_id);
            $this->db->where('ea.Event_id',$event_id);
            $this->db->where('find_in_set(ea.group_id,(select ug.permitted_group from event_attendee ea join user_group ug on ea.group_id=ug.id where ea.Attendee_id = '.$cur_user.' and ea.Event_id ='.$event_id.'))');
            $res = $this->db->get()->row_array();
        }
        return (!empty($res)) ? '1' : '0';
    }
    public function get_all_default_label()
    {
        $res=$this->db->get('default_label')->result_array();
        $finel_array=array();
        foreach ($res as $key => $value) {
            $check=!empty($value['menu_id']) ? $value['menu_id'] : $value['custom_feature_name'];
            $finel_array[$check][]=$value; 
        }
        return $finel_array;
    }
    public function check_valid_language_name($eid,$lang_name,$lang_id)
    {
        $this->db->select('*')->from('event_language');
        $this->db->where('event_id',$eid);
        $this->db->where('lang_name',$lang_name);
        if(!empty($lang_id))
        {
            $this->db->where('lang_id !=',$lang_id);
        }
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function save_user_custom_language($lang_data,$lang_id=NULL)
    {
        if(!empty($lang_id))
        {
            $this->db->where('lang_id',$lang_id);
            $this->db->update('event_language',$lang_data);
        }
        else
        {   
            $lang_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('event_language',$lang_data);
        }
    }
    public function get_language_list_by_event($eid)
    {
        $this->db->select('*')->from('event_language');
        $this->db->where('event_id',$eid);
        $this->db->where('lang_name !=','English');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_language_all_list_by_event($eid)
    {
        $this->db->select('*')->from('event_language');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function make_event_primary_language($eid,$lang_id)
    {   
        if(!empty($lang_id))
        {
            $this->db->where('event_id',$eid);
            $this->db->update('event_language',array('lang_default'=>'0'));
        }

        $lang_data['lang_default']=!empty($lang_id) ? '1' : '0'; 
        $this->db->where('event_id',$eid);
        if(!empty($lang_id))
        {
            $this->db->where('lang_id',$lang_id);
        }
        $this->db->update('event_language',$lang_data);
    }
    public function save_language_label_data($kv_data)
    {
        $this->db->select('*')->from('event_language_label');
        $this->db->where('event_id',$kv_data['event_id']);
        $this->db->where('lang_id',$kv_data['lang_id']);
        if(!empty($kv_data['menu_id']))
        {
            $this->db->where('menu_id',$kv_data['menu_id']);
        }
        if(!empty($kv_data['custom_modules_name']))
        {
            $this->db->where('custom_modules_name',$kv_data['custom_modules_name']);
        }
        $this->db->where('label_key',$kv_data['label_key']);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('event_id',$kv_data['event_id']);
            $this->db->where('lang_id',$kv_data['lang_id']);
            if(!empty($kv_data['menu_id']))
            {
                $this->db->where('menu_id',$kv_data['menu_id']);
            }
            if(!empty($kv_data['custom_modules_name']))
            {
                $this->db->where('custom_modules_name',$kv_data['custom_modules_name']);
            }
            $this->db->where('label_key',$kv_data['label_key']);
            $this->db->update('event_language_label',$kv_data);
        }
        else
        {
            $this->db->insert('event_language_label',$kv_data);
        }
    }
    public function save_language_menu_data($menu_data)
    {
        $this->db->select('*')->from('event_lang_menu');
        $this->db->where('event_id',$menu_data['event_id']);
        $this->db->where('menu_id',$menu_data['menu_id']);
        $this->db->where('lang_id',$menu_data['lang_id']);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('event_id',$menu_data['event_id']);
            $this->db->where('menu_id',$menu_data['menu_id']);
            $this->db->where('lang_id',$menu_data['lang_id']);
            $this->db->update('event_lang_menu',$menu_data);
        }
        else
        {
            $this->db->insert('event_lang_menu',$menu_data);
        }
    }
    public function save_language_content_data($content_data)
    {
        $this->db->select('*')->from('event_lang_modules_content');
        $this->db->where('event_id',$content_data['event_id']);
        $this->db->where('lang_id',$content_data['lang_id']);
        $this->db->where('menu_id',$content_data['menu_id']);
        $this->db->where('modules_id',$content_data['modules_id']);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('event_id',$content_data['event_id']);
            $this->db->where('lang_id',$content_data['lang_id']);
            $this->db->where('menu_id',$content_data['menu_id']);
            $this->db->where('modules_id',$content_data['modules_id']);
            $this->db->update('event_lang_modules_content',$content_data);
        }
        else
        {
            $this->db->insert('event_lang_modules_content',$content_data);
        }
    }
    public function get_language_label_values_by_event($eid)
    {
        $this->db->select("*,concat(concat(lang_id,'','__'), IFNULL(concat(menu_id,'','__'), concat(custom_modules_name,'','__')), label_key) as arr_keys",false)->from('event_language_label');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        $finalarray=array_combine(array_column($res,'arr_keys'),array_column($res,'label_values'));
        return $finalarray;
    }
    public function get_language_menu_values_by_event($eid)
    {
        $this->db->select("*,concat(menu_id,'__',lang_id) as arr_keys",false)->from('event_lang_menu');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        $finalarray=array_combine(array_column($res,'arr_keys'),array_column($res,'lang_menu_name'));
        return $finalarray;
    }
    public function get_language_content_by_event($eid)
    {
        $this->db->select("*,concat(concat(modules_id,'','__'),concat(menu_id,'__'),lang_id) as arr_keys",false)->from('event_lang_modules_content');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        $finalarray=array_combine(array_column($res,'arr_keys'),$res);
        return $finalarray;
    }
    public function get_language_label_values_by_event_front($eid)
    {
        $cookies_lang=get_cookie('allintheloop_lang_id');
        if(!empty($cookies_lang))
        {
            $lang_id=$cookies_lang;
        }
        else
        {
            $this->db->select('lang_id')->from('event_language');
            $this->db->where('lang_default','1');
            $this->db->where('event_id',$eid);
            $defaultlang=$this->db->get()->row_array();
            $lang_id=$defaultlang['lang_id'];
        }
        $this->db->protect_identifiers=false;
        $this->db->select("el.lang_id,el.lang_name,el.lang_icon,concat(IFNULL(concat(dl.menu_id,'','__'), concat(dl.custom_feature_name,'','__')), dl.label_key) as arr_keys,CASE WHEN ell.label_values IS NULL THEN dl.label_value ELSE ell.label_values END AS label_vales",false)->from('default_label dl');
        $this->db->join('event_language_label ell',"ell.label_key=dl.default_label_id AND ell.lang_id='".$lang_id."' AND ell.event_id=".$eid,'left');
        $this->db->join('event_language el','el.lang_id="'.$lang_id.'" AND el.event_id='.$eid,'left');
        $res=$this->db->get()->result_array();
        $finel_array =array_combine(array_column($res,'arr_keys'),array_column($res,'label_vales'));
        $finel_array['lang_id']=$res[0]['lang_id'];
        $finel_array['lang_name']=$res[0]['lang_name'];
        $finel_array['lang_icon']=$res[0]['lang_icon'];
        return $finel_array;
    }
    public function get_all_session_in_event_for_multilanguage($eid,$cid)
    {
        $this->db->select('a.*,st.type_name')->from('agenda a');
        $this->db->join('agenda_category_relation acr','a.Id=acr.agenda_id');
        $this->db->join('session_types st','a.Types=st.type_id');
        $this->db->where('acr.category_id',$cid);
        $this->db->where('a.Event_id',$eid);
        return $this->db->get()->result_array();
    }
    public function get_all_session_category_for_multilanguage($eid)
    {
        $this->db->select('*')->from('agenda_categories ac');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_exibitor_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('exibitor');
        $this->db->where('Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_speaker_in_event_for_multilanguage($eid)
    {
        $this->db->select('u.*')->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->where('u.is_moderator','0');
        $this->db->where('reu.Role_id','7');
        $this->db->where('reu.Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_presentation_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('presentation');
        $this->db->where('Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_map_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('map');
        $this->db->where('Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_survey_category_for_multilanguage($eid)
    {
        $this->db->select('*')->from('survey_category');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_question_in_event_for_multilanguage($eid,$sid)
    {
        $this->db->select('q.*')->from('survey q');
        $this->db->join('survey_category_relation scr','q.Id=scr.Question_id');
        $this->db->where('q.Event_id',$eid);
        $this->db->where('scr.survey_category_id',$sid);
        return $this->db->get()->result_array();
    }
    public function get_all_document_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('documents');
        $this->db->where('Event_id',$eid);
        return $this->db->get()->result_array();
    } 
    public function get_all_cms_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('cms');
        $this->db->where('Event_id',$eid);
        return $this->db->get()->result_array();  
    }  
    public function get_all_sponsors_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('sponsors');
        $this->db->where('Event_id',$eid);
        return $this->db->get()->result_array();  
    }
    public function get_all_qa_session_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('qa_session');
        $this->db->where('Event_id',$eid);
        return $this->db->get()->result_array();  
    }
    public function get_all_notification_in_event_for_multilanguage($eid)
    {
        $this->db->select('*')->from('notification');
        $this->db->where('event_id',$eid);
        return $this->db->get()->result_array(); 
    }
    public function geteventmenu_list_for_language($eventid)
    {
        $menudata=array('5','8','14','18','19','21','26','27','28','29','31','32','33','34','35','36','37','38','39','40','51','55');
        $this->db->select('*');
        $this->db->from('menu m');
        $this->db->where_not_in('m.id',$menudata);
        $query = $this->db->get();
        $resdata = $query->result_array();

        $this->load->model('Event_model');
        $eventmodule=$this->Event_model->geteventmodulues($eventid);
        $module=json_decode($eventmodule[0]['module_list']);
        $arr=array('22','23','24','25','42');
        $module=array_merge($module,$arr);
        foreach ($resdata as $key => $value) 
        {
            if(!in_array($value['id'],$module))
            {
                unset($resdata[$key]);
            }
        }
        return $resdata;
    }
    public function delete_language_from_event($lang_id)
    {
        $this->db->where('lang_id',$lang_id);
        $this->db->delete('event_language');
    }
    public function update_language_data($lang_data,$lang_id)
    {
        $this->db->where('lang_id',$lang_id);
        $this->db->update('event_language',$lang_data);
    }
    public function get_all_stage_data_by_event($eid)
    {
        //$this->db->protect_identifiers=false;
        $this->db->select('rs.*,(CASE WHEN rsk.id IS NULL THEN "0" ELSE "1" END) as skip_logic,rsk.que_id as skiplogic_questionid')->from('reg_stages rs');
        $this->db->join('reg_skiplogic rsk','rsk.que_id IN (select id from reg_que where stage_id=rs.id)','left');
        $this->db->where('rs.event_id',$eid);
        $this->db->order_by('rsk.id IS NULL,rsk.id',NULL,false);
       $this->db->order_by('rs.sort_order=0,rs.sort_order',NULL,false);
        $this->db->group_by('rs.id');
        $stages=$this->db->get()->result_array();
        if($eid == '1511')
        {       
            $data = $stages;
            // j($data);
            /*$data = array(
                array('id' => 1, 'title' => 'whatever'),
                array('id' => 2, 'title' => 'whatever'),
                array('id' => 3, 'title' => 'whatever')
            );*/
            // $order = array(212,213,214,215,216,217,218,211,210,219);

            $order = array(212,210,211,213,214,215,216,217,218,219);
            $order = array_flip($order);
            
            usort($data, function($a, $b)  use ($order)
            {   
                $posA = $order[$a['id']];
                $posB = $order[$b['id']];

                if ($posA == $posB) {
                    return 0;
                }
                return ($posA < $posB) ? -1 : 1;
            });
            $stages = $data;
        }
        foreach ($stages as $key => $value) 
        {
            if($value['skip_logic']=='1')
            {
                $this->db->select('`option`,redriect_stage_id')->from('reg_skiplogic');
                $this->db->where('que_id',$value['skiplogic_questionid']);
                $res=$this->db->get()->result_array();
                $option=array_column($res,'option');
                $redriect_stage_id=array_column($res,'redriect_stage_id');
                $finalarray=array_combine($option, $redriect_stage_id);
                $stages[$key]['redirectids']=json_encode($finalarray);
            }


            $this->db->select('rq.*,rd.code as discountcode,rd.price as discountprice,rt.ticket_name')->from('reg_que rq');
            $this->db->join('reg_discount rd',"rq.id=rd.que_id and rd.event_id=".$eid." and rd.status='1' and rd.code_type='0'",'left');
            $this->db->join('reg_tickets rt','rq.ticket_id=rt.id','left');
            $this->db->where('rq.stage_id',$value['id']);
            $this->db->where('rq.event_id',$eid);
            $this->db->order_by('rq.id');
            $questions=$this->db->get()->result_array();

            foreach ($questions as $qkey => $qvalue) 
            {
                if($qvalue['type']=='5')
                {
                    $session=$this->get_agenda_list_by_ticket_type($eid,$qvalue['ticket_id']);
                    $questions[$qkey]['session_list']=$session;
                }
                else
                {
                    $questions[$qkey]['session_list']=array();
                }
            }
            $stages[$key]['stages_questions']=$questions;
            if($value['skip_logic']!='1')
            {
                $stages_array=$this->get_stages_array($eid);
                $dependent_ids=$stages_array['dependent_ids'];
                $independent_ids=array_values($stages_array['independent_ids']);
                if(!in_array($value['id'],$independent_ids))
                {
                    $redirectkey=$this->get_inarrayval($value['id'],$dependent_ids,$independent_ids);
                    $rekey=array_search($redirectkey,$independent_ids);
                    $redirectid=$independent_ids[$rekey+1];
                }
                else
                {
                    $rekey=array_search($value['id'],$independent_ids);
                    $redirectid=$independent_ids[$rekey+1];
                }
                $stages[$key]['redirectid']=$redirectid;   
            }     
        }

        return $stages;
    }
    public function get_stages_array($eid)
    {
        $this->db->select('rsk.*,rs.id as stage_id')->from('reg_stages rs');
        $this->db->join('reg_skiplogic rsk','rsk.que_id IN (select id from reg_que where stage_id=rs.id)');
        $this->db->where('rs.event_id',$eid);
        $this->db->order_by('rsk.id IS NULL,rsk.id',NULL,false);
        $this->db->order_by('rs.sort_order=0,rs.sort_order',NULL,false);
        $this->db->group_by('rsk.id');
        $dependentstages=$this->db->get()->result_array();
        $idsarr=array_unique(array_filter(array_column($dependentstages,"stage_id")));
        $masterqarr=array_filter(array_column($dependentstages,"stage_id"));
        $dependent_ids=array_filter(array_column($dependentstages,"redriect_stage_id"));
        $dependentquestionarr=array_combine($dependent_ids, $masterqarr);

        $this->db->select('rs.*')->from('reg_stages rs');
        $this->db->join('reg_skiplogic rsk','rsk.que_id IN (select id from reg_que where stage_id=rs.id)','left');
        $this->db->where('rs.event_id',$eid);
        if(count($idsarr) > 0)
        {
            $this->db->where_not_in('rs.id',$idsarr);
        }
        $this->db->order_by('rsk.id IS NULL,rsk.id',NULL,false);
        $this->db->order_by('rs.sort_order=0,rs.sort_order',NULL,false);
        $this->db->group_by('rs.id');
        $independentstages=$this->db->get()->result_array();
        $independent_ids=array_unique(array_filter(array_column($independentstages,"id")));
        $res2['dependent_ids']=$dependentquestionarr;
        $res2['independent_ids']=$independent_ids;
        return $res2;
    }
    public function get_discount_code_for_all($eid)
    {
        $this->db->select('code,percentage')->from('reg_discount');
        $this->db->where('event_id',$eid);
        $this->db->where('code_type','1');
        $this->db->where('status','1');
        return $this->db->get()->row_array();
    }
    public function get_country_list_by_event($eid=NULL)
    {
        $this->db->select('id,country_name')->from('country');
        $this->db->order_by("country_name", "ASC");
        $res = $this->db->get()->result_array();

        if(!empty($eid)){
            $primary_country=$this->db->select('primary_country')->from('event')->where('Id',$eid)->get()->row_array();
            $primary_country_ids=array_filter(explode(",",$primary_country['primary_country']));
            $i=count($primary_country_ids);
        }
        else
        {
            $i=2;
            $primary_country_ids=array('223','225');   
        }
        $j=0;
        $arr;
        foreach ($res as $key => $value) {
            if(in_array($value['id'],$primary_country_ids))
            {
                $arr[$j]=$value;
                $j++;
            }
            else
            {
                $arr[$i]=$value;
                $i++;
            }
        }
        ksort($arr);
        return $arr;
    }
    public function get_user_reg_ans($user_id)
    {
        $res = $this->db->where('user_id',$user_id)->get('reg_que_ans')->result_array();
        return $res;
    }
    public function save_modules_group($eid,$group_data,$gid=NULL)
    {   
        $relation_data=$group_data['group_relation'];
        unset($group_data['group_relation']);
        if(!empty($gid))
        {   
            $group_data['updated_date']=date('Y-m-d H:i:s');
            $this->db->where('module_group_id',$gid)->update('modules_group',$group_data);
            $this->db->where('group_id',$gid)->delete('modules_group_relation');
        }
        else
        {   
            $group_data['updated_date']=date('Y-m-d H:i:s');
            $group_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('modules_group',$group_data);
            $gid=$this->db->insert_id();
        }
        $group_ralation['menu_id']=$group_data['menu_id'];
        $group_ralation['group_id']=$gid;
        foreach ($relation_data as $key => $value) 
        {
            $group_ralation['module_id']=$value;
            $this->db->insert('modules_group_relation',$group_ralation);
        }
    }
    public function get_all_modules_group($eid,$mid=null)
    {
        $this->db->select('*')->from('modules_group');
        $this->db->where('event_id',$eid);
        if(!empty($mid))
            $this->db->where('menu_id',$mid);
        return $this->db->get()->result_array();
    }
    /*public function get_edit_group_data($mgid)
    {
        $this->db->select('*')->from('modules_group');
        $this->db->where('module_group_id',$mgid);
        $group_data=$this->db->get()->row_array();

        $this->db->select('*')->from('modules_group_relation');
        $this->db->where('group_id',$mgid);
        $relation_data=$this->db->get()->result_array();
        $group_data['reation_data']=$relation_data;
        return $group_data;
    }*/
    public function get_edit_group_data($mgid,$event_id=null,$menu_id = null)
    {
        $this->db->select('*')->from('modules_group');
        $this->db->where('module_group_id',$mgid);
        if($event_id!=null)
            $this->db->where('event_id',$event_id);
        if($menu_id!=null)
            $this->db->where('menu_id',$menu_id);
        $group_data=$this->db->get()->row_array();
        if(!$group_data)
            redirect('Forbidden');

        $this->db->select('*')->from('modules_group_relation');
        $this->db->where('group_id',$mgid);
        $relation_data=$this->db->get()->result_array();
        $group_data['reation_data']=$relation_data;
        return $group_data;
    }
    public function delete_group($group_id,$event_id = '')
    {
        $this->db->where('module_group_id',$group_id);
        $this->db->delete('modules_group');

        if($event_id!='')
        {
           $a_cat = $this->db->select('*')->from('modules_group')->where('event_id',$event_id)->order_by('updated_date','DESC')->get()->row_array();
           $update['updated_date'] = date('Y-m-d H:i:s');
           $this->db->where('module_group_id',$a_cat['module_group_id'])->update('modules_group',$update);
        }
    }
    public function get_exibitor_parent_categories($eid)
    {
        $this->db->select('*')->from('exhibitor_category');
        $this->db->where('category_type','1');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_exibitor_child_categories($eid)
    {
        $this->db->select('*')->from('exhibitor_category');
        $this->db->where('category_type','0');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_old_banner_url($event_id)
    {
        $this->db->where('Id',$event_id);
        $res = $this->db->get('event')->row_array();
        return $res['banner_url'];
    }
    public function update_banner_url($event_id,$update)
    {   
        $this->db->where('Id',$event_id);
        $this->db->update('event',$update);
    }
    public function get_all_super_group($eid,$mid)
    {
        $this->db->select('*')->from('modules_super_group');
        $this->db->where('event_id',$eid);
        $this->db->where('menu_id',$mid);
        return $this->db->get()->result_array();
    }
    public function save_super_group($eid,$group_data,$gid=NULL)
    {   
        $relation_data=$group_data['group_relation'];
        unset($group_data['group_relation']);
        if(!empty($gid))
        {   
            $group_data['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('id',$gid)->update('modules_super_group',$group_data);
            $this->db->where('super_group_id',$gid)->delete('super_group_relation');
        }
        else
        {   
            $group_data['updated_date'] = date('Y-m-d H:i:s');
            $group_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('modules_super_group',$group_data);
            $gid=$this->db->insert_id();
        }
        $group_ralation['menu_id']=$group_data['menu_id'];
        $group_ralation['super_group_id']=$gid;
        foreach ($relation_data as $key => $value) 
        {
            $group_ralation['child_group_id']=$value;
            $this->db->insert('super_group_relation',$group_ralation);
        }
    }
    /*public function get_edit_super_group_data($mgid)
    {
        $this->db->select('*')->from('modules_super_group');
        $this->db->where('id',$mgid);
        $group_data=$this->db->get()->row_array();

        $this->db->select('*')->from('super_group_relation');
        $this->db->where('super_group_id',$mgid);
        $relation_data=$this->db->get()->result_array();
        $group_data['reation_data']=$relation_data;
        return $group_data;
    }*/
    public function get_edit_super_group_data($mgid,$event_id=null,$menu_id=null)
    {
        $this->db->select('*')->from('modules_super_group');
        $this->db->where('id',$mgid);
        if($event_id!=null)
            $this->db->where('event_id',$event_id);
        if($menu_id!=null)
            $this->db->where('menu_id',$menu_id);
        $group_data=$this->db->get()->row_array();
        if(!$group_data)
            redirect('Forbidden');
        
        $this->db->select('*')->from('super_group_relation');
        $this->db->where('super_group_id',$mgid);
        $relation_data=$this->db->get()->result_array();
        $group_data['reation_data']=$relation_data;
        return $group_data;
    }
    public function delete_super_group($group_id,$event_id=NULL)
    {
        $this->db->where('id',$group_id);
        $this->db->delete('modules_super_group');
        
        $update['updated_date'] = date('Y-m-d H:i:s');
        $this->db->where('event_id',$event_id);
        $this->db->limit('1')->order_by('updated_date','desc')->update('modules_super_group',$update);
    }

    public function getUserByEmail($email,$event_id)
    {   
        $this->db->select('u.*');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('u.Email',$email);
        $this->db->where('reu.Event_id',$event_id);
        $this->db->where('reu.Role_id','4');
        $res = $this->db->get('user u')->row_array();
        return $res;
    }

    public function saveUserSession($data)
    {   
        $this->db->where('user_id',$data['user_id']);
        $res = $this->db->get('users_agenda')->row_array();
        if($res)
        {
            $this->db->where($res)->delete('users_agenda');
        }
        $this->db->insert('users_agenda',$data);
    }
}
        
?>
