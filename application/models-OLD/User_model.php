<?php class User_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }
     public function get_user_details($user_id)
     {
          $this->db->select('*');
          $this->db->from('user');
          $this->db->where('Id',$user_id);
          $query = $this->db->get();
          $res = $query->result_array();

          return $res;
     }
    
     /*public function add_user_as_speaker_inv($id_arr)
     {
          foreach ($id_arr as $key => $value) 
          {
              $arr=array("user_id"=>$value,'status'=>'1');
              $this->db->insert("speaker_invitation",$arr);
          }
          
          
     }*/
	 
     public function get_attendee_user_list($id,$Organisor_id)
     {
        $orid = $Organisor_id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id) 
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->where('r.Id',4);
        $this->db->where('u.attendee_flag','0');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
     }
     public function update_user_as_attendee($att_id_arr)
     {
          $data['attendee_flag']='1';
          $this->db->where_in("Id",$att_id_arr);
          $this->db->update("user",$data);
     }
     public function get_role_list($id)
     {
          $this->db->select('*');
          $this->db->from('role');
          $this->db->where('id IN (select Role_id from role_permission where event_id=' . $id . ')', NULL, FALSE);
          $this->db->where('status', '0');
          $this->db->where('Active', '1');
          $this->db->or_where('id', '5');

          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_role_list_version2($id)
     {
          $this->db->select('*');
          $this->db->from('role');
          $this->db->where('id IN (select Role_id from role_permission where event_id=' . $id . ')', NULL, FALSE);
          //$this->db->where('status', '0');
          $this->db->where('Active', '1');
          $this->db->where('Id !=',3);
          $this->db->where('Id !=',4);
          $this->db->where('Id !=',5);
          $this->db->where('Id !=',6);
          $this->db->where('Id !=',7);
          $this->db->where('Name != ','Client');
          
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_role_list_notcurrent($id, $roleid=  array())
     {
          $this->db->select('*');
          $this->db->from('role');
          $this->db->where('id IN (select Role_id from role_permission where event_id=' . $id . ')', NULL, FALSE);
          $this->db->where('status', '0');
          $this->db->where('Active', '1');
          if(!empty($roleid))
          {
               $this->db->where_not_in('id', $roleid);
          }
          $this->db->or_where('id', '5');

          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

     public function get_user_list_byevent($eventid)
     {
          $user = $this->session->userdata('current_user');
          $id=$user[0]->Id;
          $this->db->select('user.Id,user.Firstname,user.Email,user.Active');
          $this->db->from('user');
          $this->db->join('relation_event_user', 'user.Id = relation_event_user.User_id');
          $this->db->join('role r', 'relation_event_user.Role_id = r.Id');
          $this->db->where('relation_event_user.Event_id', $eventid);
          $this->db->where('r.Id !=',3);
          $this->db->where('r.Id !=',4);
          $this->db->where('r.Id !=',5);
          $this->db->where('r.Id !=',6);
          $this->db->where('r.Id !=',7);
          $this->db->where('user.Id !=',$id);

          //$this->db->or_where('user.admin_flag','1');
          //$this->db->or_where('user.editor_flag','1');
          $this->db->group_by('user.Id');
          $query = $this->db->get();
          return $query->result_array();
     }
     public function get_exhib_list_byevent($eventid)
     {
          $this->db->select('user.Id,user.Firstname,user.Lastname,ex.Heading,user.Email,user.Active,count(ex.user_id) as page_cnt,ex.id as eid');
          $this->db->from('user');
          $this->db->join('relation_event_user', 'user.Id = relation_event_user.User_id');
          $this->db->join('role r', 'relation_event_user.Role_id = r.Id');
          $this->db->join('exibitor ex', 'ex.user_id = user.Id and ex.Event_id='.$eventid,'left');
          $this->db->where('relation_event_user.Event_id', $eventid);
          $this->db->group_by("user.Id");
          $this->db->where('r.Id',6);
          $query = $this->db->get();
          $res=$query->result_array();
          return $res;
     }

     public function add_user($array_add, $event_id)
     {
          $orid = $this->data['user']->Id;
          $role_id = $array_add['Role_id'];
         
          unset($array_add['Role_id']);

          $eventid = $array_add['event_id'];
          unset($array_add['event_id']);


          if($role_id==37)
          {
               $array_add['editor_flag']='1';
          }
          if($role_id==5)
          {
                $array_add['admin_flag']='1';
          }
          $array_add['updated_date']=date('Y-m-d H:i:s');
          $this->db->insert('user', $array_add);
          $user_id = $this->db->insert_id();

          
          $array_relation_user_attendee['User_id'] = $user_id;
         
          if($role_id==5 || $role_id==37)
          {
               $array_relation_user_attendee['Organisor_id'] = $user_id;
               $array_relation_user_attendee['Role_id'] = 3;

          }
          else
          {
               $array_relation_user_attendee['Organisor_id'] = $orid;
               $array_relation_user_attendee['Role_id'] = $role_id;
               $array_relation_user_attendee['Event_id'] = $eventid;
          }

          $this->db->insert('relation_event_user', $array_relation_user_attendee);
          $this->session->set_flashdata('user_data', 'Added');
     }

     public function get_all_users($id = null)
     {
          $orid = $this->data['user']->Id;
          $this->db->select('u.Email');
          $this->db->from('user u');
          $this->db->join('relation_event_user ru', 'ru.User_id = u.Id', 'left');
          $this->db->join('role r', 'ru.Role_id = r.Id', 'left');
          if ($orid)
          {
               $this->db->where('u.Organisor_id', $orid);
          }

          if ($id != NULL)
          {
               $this->db->where('ru.Event_id =', $id);
          }

          $this->db->where('r.Id !=',7);
          $this->db->where('r.Id !=',4);
          
          $this->db->where('r.Id !=',3);
          $this->db->where('u.admin_flag','1');
          $this->db->order_by('u.Id desc');
          $this->db->group_by('u.Id');
          $query = $this->db->get();
          $res = $query->result_array();

          $string = array();

          if (!empty($res))
          {
               foreach ($res as $key => $value)
               {
                    $string[] = $value['Email'];
               }
          }

          $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
          $this->db->from('user u');
          $this->db->join('relation_event_user ru', 'ru.User_id = u.Id', 'left');
          $this->db->join('role r', 'ru.Role_id = r.Id', 'left');


          if ($id != NULL)
          {
               $this->db->where('ru.Event_id !=', $id);
          }

          if ($orid)
          {
               $this->db->where('u.Organisor_id', $orid);
          }
          if (!empty($string))
          {
               $this->db->where_not_in('u.Email', $string);
          }
          $this->db->where('r.Id !=',7);
          $this->db->where('r.Id !=',4);
          $this->db->where('r.Id !=',3);
          $this->db->order_by('u.Id desc');
          $this->db->group_by('u.Id');
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
          return $res1;
     }

     public function add_exist_user($data)
     {
          $user = $this->session->userdata('current_user');

          if ($user[0]->Role_name == 'Client')
          {
               $Organisor_id = $user[0]->Id;
          }
          else
          {
               $Organisor_id = $user[0]->Organisor_id;
          }

          if ($data['user_array']['User_id'] != NULL)
               $array_user['User_id'] = $data['user_array']['User_id'];

          if ($data['user_array']['Role_id'] != NULL)
               $array_user['Role_id'] = $data['user_array']['Role_id'];

          if ($data['user_array']['Event_id'] != NULL)
               $array_user['Event_id'] = $data['user_array']['Event_id'];

          if ($Organisor_id != NULL)
               $array_user['Organisor_id'] = $Organisor_id;

          $this->db->insert('relation_event_user', $array_user);
          $user_id = $this->db->insert_id();
          return $user_id;
     }

     public function delete_user($id,$Event_id)
     {
          $this->db->where('Id', $id);
          $this->db->delete('user');
          $str = $this->db->last_query();
          if($Event_id!='')
          {
             $a_cat = $this->db->select('user.*')->from('user')->join('relation_event_user ru','ru.User_id = user.Id')->where('ru.Event_id',$Event_id)->order_by('user.updated_date','DESC')->get()->row_array();
             $update['updated_date'] = date('Y-m-d H:i:s');
             $this->db->where('Id',$a_cat['Id'])->update('user',$update);
          }
     }

     public function delete_exibitor_user($event_id,$user_id)
     {
          $where['Event_id'] = $event_id;
          $where['User_id'] = $user_id;
          $where['Role_id'] = '6';
          $this->db->where($where);
          $this->db->delete('relation_event_user');
          if($event_id!='')
          {
             $a_cat = $this->db->select('*')->from('exibitor')->where('Event_id',$event_id)->order_by('updated_date','DESC')->get()->row_array();
             $update['updated_date'] = date('Y-m-d H:i:s');
             $this->db->where('Id',$a_cat['Id'])->update('exibitor',$update);
          }
     }

     public function get_recent_user_list()
     {
          $orid = $this->data['user']->Id;
          $this->db->select('u.*');
          $this->db->from('user u');
          $this->db->join('role r', 'u.Role_id = r.Id');
          if ($user[0]->Id)
          {
               $this->db->where('u.Id', $user[0]->Id);
          }
          $this->db->where('r.Name', 'User');
          $this->db->order_by('u.Id desc');
          $this->db->limit(5);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     
     public function get_user_by_email($email)
     {
          $this->db->select('u.*');
          $this->db->from('user u');
          $this->db->where('u.Email',$email);
          $this->db->order_by('u.Id desc');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function getUserFromIds($strIds =NULL)
     {
         if($strIds=="")
         {
             return false;
         }
          $this->db->select('*');
          $this->db->from('user');
          $this->db->where("Id IN (".$strIds.")",NULL,FALSE);
          $this->db->order_by('Id desc');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_user_by_email_event($email,$event_id)
     {
          $this->db->select('u.*');
          $this->db->from('user u');
          $this->db->join('relation_event_user ru','ru.User_id = u.Id','left');
          $this->db->where('ru.Event_id',$event_id);
          $this->db->where('u.Email',$email);
          $this->db->order_by('u.Id desc');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

}

?>