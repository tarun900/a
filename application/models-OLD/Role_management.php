<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Role_management extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Role Management';
          $this->data['smalltitle'] = 'Role Management Details';
          $this->data['page_edit_title'] = 'Role Management';
          $this->data['breadcrumb'] = 'Role Management';
          parent::__construct($this->data);
          $this->load->model('Role_management_model');
          $this->load->model('User_model');
          
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Profile_model');
     }

     public function add($eventid)
     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          $permissions = $this->Role_management_model->get_role_menu($id);
          $role_menu = $this->Event_model->geteventmenu_rolemanagement($eventid);

          $this->data['role_menu'] = $role_menu;
          $this->data['event_id'] = $eventid;

          $array_permission = array();
          foreach ($permissions as $key => $value)
          {
               $array_permission[] = $value['Menu_id'];
          }
          $this->data['array_permission'] = $array_permission;
          if ($this->input->post())
          {
               $role_arr = array();
               $per_arr = array();
               $id_arr = $this->Role_management_model->get_last_roleid();
               $id = $id_arr[0]['Id'] + 1;
               
               foreach ($this->input->post() as $k => $v)
               {
                    if ($k == "role")
                    {
                         $k = "Name";
                         $role_arr[$k] = $v;
                    }
                    else if ($k == "Active")
                    {
                         $role_arr[$k] = $v;
                    }
                    else
                    {
                         $per_arr['Role_id'] = $id;
                         $per_arr['Menu_id'] = $v;
                    }
               }
               
               $name = ($this->input->post('Firstname') == NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
               $user = $this->Role_management_model->add_role($role_arr, $per_arr, $eventid);
               $this->session->set_flashdata('user_data', 'Added');
               redirect("role_management/index/" . $eventid);
          }

          $this->template->write_view('css', 'role_management/add_css', $this->data, true);
          $this->template->write_view('content', 'role_management/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'role_management/add_js', $this->data, true);
          $this->template->render();
     }

     public function index($id)
     {
          $this->data['event_id11'] = $id;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $user = $this->session->userdata('current_user');

          $role_name = $user[0]->Role_name;

          $roles = $this->Role_management_model->get_role_list($id);
          $users = $this->User_model->get_user_list_byevent($id);
          $this->data['users'] = $users;

          $this->data['role'] = $roles;

          $this->template->write_view('css', 'role_management/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'role_management/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'role_management/js', $this->data, true);
          $this->template->render();
     }

     public function edit($eid, $id)
     {
          $event = $this->Event_model->get_admin_event($eid);
          $this->data['event'] = $event[0];
          $this->data['edited_event'] = $this->Role_management_model->get_edit_event($eid);
          $this->data['all_event'] = $this->Role_management_model->get_all_event();
          $permissions = $this->Role_management_model->get_role_menu($id, $eid);
          $this->data['event_id'] = $eid;
          $this->data['idval'] = $id;
          $role_menu = $this->Event_model->geteventmenu_rolemanagement($eventid);
          $this->data['role_menu'] = $role_menu;
          $array_permission = array();
          foreach ($permissions as $key => $value)
          {
               $array_permission[] = $value['Menu_id'];
          }
          $this->data['array_permission'] = $array_permission;

          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('role_management');
               }

               if ($this->input->post())
               {
                    $diffdata = array_diff($array_permission, $this->input->post('Menu_id'));

                    foreach ($diffdata as $key => $val)
                    {
                         $this->Role_management_model->delete_permssion($id, $val, $eid);
                    }

                    $adddata = array_diff($this->input->post('Menu_id'), $array_permission);
                    foreach ($adddata as $key => $val)
                    {
                         $data = array();
                         $data['Menu_id'] = $val;
                         $data['Role_id'] = $id;
                         $data['event_id'] = $eid;
                         $this->Role_management_model->insert_permssion($data);
                    }

                    $data['role_array']['Id'] = $id;
                    $data['role_array']['Name'] = $this->input->post('role');
                    $data['role_array']['event_id'] = $this->input->post('event_id');
                    $data['role_array']['Active'] = $this->input->post('Active');
                    $this->Role_management_model->update_admin_role($data);
                    $this->session->set_flashdata('user_data', 'Updated');
                    redirect("role_management/index/" . $eid);
               }

               $role = $this->Role_management_model->get_role_byid($id);
               $this->data['role'] = $role[0];
               $this->data['flag'] = 1;
               $this->session->set_userdata($data);

               $this->template->write_view('js', 'role_management/add_js', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('css', 'role_management/css', $this->data, true);
               $this->template->write_view('content', 'role_management/edit', $this->data, true);
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               $this->template->render();
          }
     }

     public function delete($Event_id, $id)
     {
          $role = $this->Role_management_model->delete_role($id);
          $this->session->set_flashdata('user_data', 'Deleted');
          redirect("role_management/index/" . $Event_id);
     }
     
     
     public function checkrole()
     {            
          if($this->input->post())
          {
              $client = $this->Role_management_model->checkrole($this->input->post('role'),$this->input->post('idval'),$this->input->post('event_id'));
              if($client)
              {
                  echo "error###Role already exist. Please choose another role.";
              }
              else
              {
                  echo "success###";
              }
          }
          exit;
     }

}
