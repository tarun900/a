<?php

class Notifications_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_notifications_list($id = null, $userid = null, $limit = 'All',$sarchtext=null) {

        if ($id != NULL && $userid != NULL) {
            $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,Date_Format(n.Created_date,"%d %b %Y, %r") as Created_date', FALSE);
        } else {
            $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,n.Created_date,nu.Read_unread,nu.Seen_unseen', FALSE);
        }

        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        $this->db->join('user u', 'u.id =nu.User_id', 'Left');
        $this->db->join('user uc', 'uc.id =n.Created_by', 'Left');
        if ($id) {
            $this->db->where('nu.Notification_id', $id);
        }
        
        if ($userid) {
            $this->db->where('nu.User_id', $userid);
            $this->db->where('nu.Softdelete', '0');
        }
        
        if($sarchtext!=null)
        {   
            $this->db->like('(case u.Firstname when NULL then u.Company_name Else u.Firstname End)', $sarchtext);
            $this->db->or_like('n.Summary', $sarchtext);
        }
        
        //$this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');
        
        if ($limit != 'All') {
            $this->db->limit($limit, 0);
        }

        $query = $this->db->get();
        
        if ($id != NULL && $userid != NULL) {
            $res = $query->first_row();
        } else {
            $res = $query->result_array();
        }        
        return $res;
    }

    public function updatemsgtoread($notifiyid, $userid) {
        $category = array(
            'Read_unread' => 'read'
        );
        $this->db->where('Notification_id', $notifiyid);
        $this->db->where('User_id', $userid);
        $data = $this->db->update('notifications_users', $category);
        return $data;
    }

    public function userdata($rolename) {
        $this->db->select('u.id,case u.Firstname when NULL then u.Company_name Else u.Firstname End as name ', FALSE);
        $this->db->from('role r');
        $this->db->join('relation_event_user r', 'ru.Role_id = r.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        
        $this->db->where('r.Name', $rolename);
        $this->db->where('u.Active', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function add_notification($data, $id = null) {
        if ($id != NULL) {
            $this->db->where('Id', $id);
            $this->db->update('notifications', $data); // update the record
        } else {
            $this->db->insert('notifications', $data);
        }
        return $this->db->insert_id();
    }

    public function add_usernotification($data, $notificationid = null, $userid = null) {
        $id = $this->db->insert('notifications_users', $data);
        return $id;
    }

    public function deletenotification($id, $userid,$flag=0) {

        $this->db->where('Notification_id', $id);
        $this->db->where('User_id', $userid);
        $this->db->delete('notifications_users');

        $res = $this->getcurrentuserwithnotification($id);

        if (empty($res) && $flag==0) {
            $this->db->where('Id', $id);
            $this->db->delete('notifications');
        }
        return true;
    }

    public function get_notifications($id = null) {

        $this->db->select('n.*,n.Id as Notification_id,nu.*,nu.Id as notification_user_id ', FALSE);

        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        if ($id) {
            $this->db->where('nu.Notification_id', $id);
        }
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();

        $res = $query->result_array();

        $finalarr = array();
        for ($i = 0; $i < count($res); $i++) {
            $finalarr['Id'] = $res[$i]['Notification_id'];
            $finalarr['Title'] = $res[$i]['Title'];
            $finalarr['Summary'] = $res[$i]['Summary'];
            $finalarr['Content'] = $res[$i]['Content'];
            $finalarr['Notification_by'] = $res[$i]['Notification_by'];
            $finalarr['Notification_by'] = $res[$i]['Notification_by'];
            $finalarr['Schedule'] = $res[$i]['Schedule'];
            $finalarr['Schedual_date'] = $res[$i]['Schedual_date'];
            $finalarr['Schedule_for'] = $res[$i]['Schedule_for'];
            $finalarr['User_type'] = $res[$i]['User_type'];
            $finalarr['CheckdUSer'][$i] = $res[$i]['User_id'];
        }
        return $finalarr;
    }

    public function getcurrentuserwithnotification($id) {
        $this->db->select('User_id', FALSE);
        $this->db->from('notifications_users');
        $this->db->where('Notification_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();

        return $res;
    }

    public function getcountandseencount($id = null, $userid = null) {
        $this->db->select('count(n.id) as msg_unseencount', FALSE);

        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');

        if ($id) {
            $this->db->where('nu.Notification_id', $id);
        }

        if ($userid) {
            $this->db->where('nu.User_id', $userid);
        }
        $this->db->where('nu.Seen_unseen', 'unseen');
        $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');

        $query = $this->db->get();
        $res = $query->first_row();

        $this->db->select('count(n.id) as msg_unreadcount', FALSE);

        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');

        if ($id) {
            $this->db->where('nu.Notification_id', $id);
        }

        if ($userid) {
            $this->db->where('nu.User_id', $userid);
        }
        $this->db->where('nu.Read_unread', 'unread');
        $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');

        $query = $this->db->get();
        $res1 = $query->first_row();

        $arrcount = array();
        $arrcount['msg_unseencount'] = $res->msg_unseencount;
        $arrcount['msg_unreadcount'] = $res1->msg_unreadcount;

        return $arrcount;
    }
    
    public function timeAgo($ts) 
    {
        if(!ctype_digit($ts))
        $ts = strtotime($ts);

        $diff = time() - $ts;
        if($diff == 0)
            return 'now';
        elseif($diff > 0)
        {
            $day_diff = floor($diff / 86400);
            if($day_diff == 0)
            {
                if($diff < 60) return 'just now';
                if($diff < 120) return '1 min';
                if($diff < 3600) return floor($diff / 60) . ' mins';
                if($diff < 7200) return '1 hr';
                if($diff < 86400) return floor($diff / 3600) . ' hrs';
            }
            if($day_diff == 1) return 'Yesterday';
            if($day_diff < 7) return $day_diff . ' ds';
            if($day_diff < 31) return ceil($day_diff / 7) . ' ws';
            if($day_diff < 60) return 'last month';
            return date('F Y', $ts);
        }
        else
        {
            $diff = abs($diff);
            $day_diff = floor($diff / 86400);
            if($day_diff == 0)
            {
                if($diff < 120) return 'in a minute';
                if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
                if($diff < 7200) return 'in an hour';
                if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
            }
            if($day_diff == 1) return 'Tomorrow';
            if($day_diff < 4) return date('l', $ts);
            if($day_diff < 7 + (7 - date('w'))) return 'next week';
            if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
            if(date('n', $ts) == date('n') + 1) return 'next month';
            return date('F Y', $ts);
        }
    }
    
    public function unseenmsg($userid)
    {
        $data['Seen_unseen']="seen";
        $this->db->where('User_id', $userid);
        $this->db->update('notifications_users', $data); // update the record
        return true;
    }
    
    public function softdeletenotification($id, $userid) {

        $data['Softdelete']="1";
        $this->db->where('User_id', $userid);
        $this->db->where('Notification_id', $id);
        $this->db->update('notifications_users', $data); // update the record
        return true;
    }
    
    public function get_notifications_searchlist($id = null, $userid = null,$sarchtext=null) 
    {        
        $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,n.Created_date,nu.Read_unread,nu.Seen_unseen', FALSE);
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        $this->db->join('user u', 'u.id =nu.User_id', 'Left');
        $this->db->join('user uc', 'uc.id =n.Created_by', 'Left');
        if ($id) {
            $this->db->where('nu.Notification_id', $id);
        }
        
        if ($userid) {
            $this->db->where('nu.User_id', $userid);
            $this->db->where('nu.Softdelete', '0');
        }
        
        if($sarchtext!=null)
        {   
            $this->db->where('((case u.Firstname when NULL then u.Company_name Else u.Firstname End) like "%'.$sarchtext.'%" or n.Summary like "%'.$sarchtext.'%")');
        }
        
        //$this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');

        $query = $this->db->get();
        
        if ($id != NULL && $userid != NULL) {
            $res = $query->first_row();
        } else {
            $res = $query->result_array();
        }
        
        foreach($res as $key=>$value)
        {
                if($key=="Created_date")
                {
                    if(date('Y-m-d')==date('Y-m-d',strtotime($value['Created_date'])))
                    {
                       $res[$key]['Created_date']=date('H:s A',strtotime($value['Created_date'])); 
                    }
                    else
                    {
                        $res[$key]['Created_date']=date('d M y',strtotime($value['Created_date']));
                    }
                }
                
                if($key=="Title")
                {
                    if(strlen($value['Title'])>35)
                    {
                        $res[$key]['Title']= substr($value['Title'], 0, 35).'...';
                    }
                    else
                    {
                        $res[$key]['Title']=$value['Title'];
                    }
                }
                
                if($key=="Summary")
                {
                    if(strlen($value['Summary'])>73)
                    {
                        $res[$key]['Summary']=substr($value['Summary'], 0, 73).'...';
                    }
                    else
                    {
                        $res[$key]['Summary']=$value['Summary'];
                    }
                }
        }
        
        return $res;
    }
    
    public function getautometednotificationlist($limit)
    {   
        $this->db->select('an.To_id,an.Link,case tou.Firstname when NULL then tou.Company_name Else tou.Firstname End as Toname,case fru.Firstname when NULL then fru.Company_name Else fru.Firstname End as Frname,an.From_id,an.Event_id,an.Type_id,an.Message,an.Created_date as Createddate,p.Event_name,p1.Event_name as eventname, ps.*',FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou','tou.id=an.To_id','left');
        $this->db->join('user fru','fru.id=an.From_id','left');
        $this->db->join('event p','p.id=an.Event_id','left');
        $this->db->join('event_size ps','ps.id=an.Event_size_id','left');
        $this->db->join('event p1','p1.id=ps.Event_id','left');
        $this->db->where('an.To_id',$this->data['user']->Id);
        $this->db->order_by('an.id desc');
        if($limit!="All")
        {
            $this->db->limit($limit, 0);
        }
        $notify_query = $this->db->get();
               
        $notify_res['notification_res'] = $notify_query->result_array();
        
        $this->db->select('count(an.id) as notifycnt',FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou','tou.id=an.To_id','left');
        $this->db->join('user fru','fru.id=an.From_id','left');
        $this->db->join('event p','p.id=an.Event_id','left');
        $this->db->join('event_size ps','ps.id=an.Event_size_id','left');
        $this->db->where('an.To_id',$this->data['user']->Id);
        $this->db->where('an.Status','0');
        $notify_query = $this->db->get();
        $datacnt=$notify_query->result_array();
        $notify_res['notification_res_cnt'] = $datacnt[0]['notifycnt'];
        
        
        $this->db->select('count(an.id) as notifytcnt',FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou','tou.id=an.To_id','left');
        $this->db->join('user fru','fru.id=an.From_id','left');
        $this->db->join('event p','p.id=an.Event_id','left');
        $this->db->join('event_size ps','ps.id=an.Event_size_id','left');
        $this->db->where('an.To_id',$this->data['user']->Id);
        $notify_query = $this->db->get();
        $datacnt=$notify_query->result_array();
        $notify_res['notification_res_tcnt'] = $datacnt[0]['notifytcnt'];
               
        foreach ($notify_res['notification_res'] as $key=>$value)
        { 
            $notify_res['notification_res'][$key]['Createddate']=$this->notifications_model->timeAgo($value['Createddate']);
            
            if($value['Type_id']=='0' || $value['Type_id']=='1')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                
                $replacements = array();                
                $replacements[0] = $value['Event_name'];                
                
                $msg=$this->replacemsg($value['Message'],$patterns,$replacements);
                $notify_res['notification_res'][$key]['Message']=$msg;
                //$notify_res['notification_res'][$key]['Link']=base_url().'event/detail/'.$value['Event_id'];
            } 
            else if($value['Type_id']=='9')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                
                $replacements = array();                
                $replacements[0] = $value['eventname'];                
                
                $msg=$this->replacemsg($value['Message'],$patterns,$replacements);
                $notify_res['notification_res'][$key]['Message']=$msg;
                //$notify_res['notification_res'][$key]['Link']=base_url().'order';
            }
            else if($value['Type_id']=='2')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{attendee}}/';
                
                $replacements = array();                
                $replacements[0] = $value['Event_name'];                
                $replacements[1] = $value['Frname'];                
                
                $msg=$this->replacemsg($value['Message'],$patterns,$replacements);
                $notify_res['notification_res'][$key]['Message']=$msg;
                //$notify_res['notification_res'][$key]['Link']=base_url().'event/detail/'.$value['Event_id'];
            }
            else if($value['Type_id']=='3' || $value['Type_id']=='5' || $value['Type_id']=='8')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{client}}/';
                
                $replacements = array();                
                $replacements[0] = $value['eventname'];                
                $replacements[1] = $value['Frname'];                
                
                $msg=$this->replacemsg($value['Message'],$patterns,$replacements);
                $notify_res['notification_res'][$key]['Message']=$msg;
                //$notify_res['notification_res'][$key]['Link']=base_url().'order';
            }
            else if($value['Type_id']=='4' || $value['Type_id']=='6' || $value['Type_id']=='7')
            {                
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{client}}/';
                $patterns[2] = '/{{size}}/';
                
                $replacements = array();                
                $replacements[0] = $value['eventname'];                
                $replacements[1] = $value['Toname'];                
                $replacements[2] = $value['Size'];                
                
                $msg=$this->replacemsg($value['Message'],$patterns,$replacements);
                $notify_res['notification_res'][$key]['Message']=$msg;                
            }
            
        } 
        return $notify_res;        
    }
    
    function replacemsg($message,$patterns,$replacements)
    {
        return preg_replace($patterns, $replacements, $message);        
    }
    
    
    public function seenautonotify()
    {
        $category = array(
            'Status' => '1'
        );        
        $this->db->where('To_id', $this->data['user']->Id);
        $this->db->update('auto_notify', $category);
        return true;
    }
    
}

?>
