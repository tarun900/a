<?php

class event_model extends CI_Model
{

     function __construct()
     {
          $this->db1 = $this->load->database('db1', TRUE);
          parent::__construct();
     }
     
     /*public function get_event_list($user_id)
     {
          $this->db->select('e.*');
          $this->db->from('event e');
          $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
          $this->db->join('user u', 'u.Id = ru.User_id');
          $this->db->where('u.ID', $user_id);
          $this->db->group_by('e.Id');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }*/
     
     public function getFetureProduct($event_id)
     {

        $this->db->select('em.*,m.menuname, m.pagetitle, m.menuurl, m.isvisible');
        $this->db->from('event_menu em');
        $this->db->join('menu m', 'm.id = em.menu_id');
        //$this->db->where_not_in('m.id', array(26,27,28));
        $this->db->where('em.event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
     }

     public function geteventmodulues($event_id)
     {
        $this->db->select('*');
        $this->db->from('event e');
        $this->db->where('e.Id', $event_id);
        $this->db->join('user u','u.Id=e.Organisor_id');
        $query=$this->db->get();
        $res=$query->result_array();
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('ou.Email', $res[0]['Email']);
        $this->db1->join('subscription_billing_cycle sbc','sbc.id=ou.subscriptiontype');
        $this->db1->join('products p','FIND_IN_SET(CAST(p.id AS CHAR),sbc.product_id)');
        $query = $this->db1->get();
        $res1 = $query->result_array();
        $module;
        if(count($res1)>0)
        {
          for ($i=0; $i <count($res1); $i++) 
          { 
              $arr[]=json_decode($res1[$i]['module_list']);
          }
          $output = array();
          foreach ($arr as $key=>$data ) {
            $output = array_merge($output, $data);
          }
          $uarray=array_keys(array_flip($output));
          $module=json_encode($uarray);
        }
        $res1[0]['module_list']=$module;
        
        return $res1;
     }

     public function geteventcmsmenu($eventid, $cms_id = null, $is_feture = null)
     {
          $this->db->select('s.Subdomain as ssubdomain');
          $this->db->from('event s');
          $this->db->where('s.Id =', $eventid);
          $query1 = $this->db->get();
          $res1 = $query1->result_array();
          $acc_name = $this->session->userdata('acc_name');
          $this->db->select('c.Id,c.Menu_name,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,c.Menu_name as menuname,CONCAT("Cms/' .$acc_name. '/'.$res1[0]['ssubdomain'] . '/View/",c.Id) as menuurl,em.is_feture_product', FALSE);
          $this->db->from('cms c');
          $this->db->join('event_menu em', 'em.cms_id=c.Id and em.event_id="' . $eventid . '"', 'left');

          if ($cms_id != null)
          {
               $this->db->where('c.Id', $cms_id);
          }

          if ($is_feture != null)
          {
               $this->db->where('em.is_feture_product', '0');
          }

          $query = $this->db->get();
          $res = $query->result();
          return $res;
     }
/*    public function geteventmenu_list($eventid,$menu_id=null,$is_feture=null)
    {
          $this->db->select('e.checkbox_values',FALSE);
          $this->db->from('event e');
        
          $this->db->where('e.Id', $eventid);
          $query = $this->db->get();
          $res = $query->result_array();
          $resdata=array();
          if(!empty($res))
          {
               $menudata=explode(',', $res[0]['checkbox_values']);

               $menudata = array_diff($menudata, array('5','8','21','26','27','28'));
               $menudata=  array_merge($menudata,array());

               $this->db->select('m.id,m.pagetitle, CASE WHEN em.img IS NULL THEN "" ELSE em.img END as img,CASE WHEN em.img_view IS NULL THEN "0" ELSE em.img_view END as img_view,CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl',FALSE);
               $this->db->from('menu m');
               $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
               $this->db->join('event e', 'e.Id=em.event_id', 'left');

               if($menu_id!=null)
               {
                   $this->db->where('m.id', $menu_id);
               }
               if(empty($menudata))
               {
                  $menudata[0]=NULL;
               }
              $this->db->where_in('m.id',$menudata);

               $query = $this->db->get();
               $resdata = $query->result_array();
          }
          $this->load->model('event_model');
          $eventmodule=$this->event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          $arr=array('22','23','24','25');
          $module=array_merge($module,$arr);
          foreach ($resdata as $key => $value) 
          {
             if(!in_array($value['id'],$module))
             {
               unset($resdata[$key]);
             }
          }
          $resdata=array_values($resdata);
         
        return $resdata;
    }*/
    /*public function geteventmenu_list($eventid,$menu_id=null,$is_feture=null)
    {
          $this->db->select('e.checkbox_values,e.icon_set_type',FALSE);
          $this->db->from('event e');
        
          $this->db->where('e.Id', $eventid);
          $query = $this->db->get();
          $res = $query->result_array();
          $resdata=array();
          if(!empty($res))
          {
               $menudata=explode(',', $res[0]['checkbox_values']);

               $menudata = array_diff($menudata, array('5','8','21','26','27','28'));
               $menudata=  array_merge($menudata,array());

               $this->db->select('m.id,m.pagetitle, CASE WHEN em.img IS NULL THEN "" ELSE em.img END as img,CASE WHEN em.img_view IS NULL THEN "0" ELSE em.img_view END as img_view,CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,CASE WHEN em.Background_color IS NULL THEN "" ELSE em.Background_color END as Background_color',FALSE);
               $this->db->from('menu m');
               $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
               $this->db->join('event e', 'e.Id=em.event_id', 'left');

               if($menu_id!=null)
               {
                   $this->db->where('m.id', $menu_id);
               }
               if(empty($menudata))
               {
                  $menudata[0]=NULL;
               }
              $this->db->where_in('m.id',$menudata);

               $query = $this->db->get();
               $resdata = $query->result_array();
          }
          $this->load->model('event_model');
          $eventmodule=$this->event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          $arr=array('22','23','24','25');
          $module=array_merge($module,$arr);
          $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
          foreach ($resdata as $key => $value) 
          {
             $resdata[$key]['default_img'] = '';
             $resdata[$key]['is_icon'] = '0';
             $resdata[$key]['icon_path'] = '';

             if($value['img'] == '' && in_array($value['id'], $iconset) && $value['is_feture_products']=='1')
             {
                $path = base_url().'assets/css/images/icons/icon'.$res[0]['icon_set_type'].'/'.$value['id'].'.png';
                $resdata[$key]['is_icon'] = '1';
                $resdata[$key]['icon_path'] = $path;
             }
             else if($value['img'] == '' && $value['is_feture_products']=='1')
             {
                $resdata[$key]['default_img'] = base_url().'assets/images/EventApp_Default.jpg';
             }
                  
             if(!in_array($value['id'],$module))
             {
               unset($resdata[$key]);
             }

          }
          $resdata=array_values($resdata);
          
        return $resdata;
    }*/
    public function geteventmenu_list($eventid,$menu_id=null,$is_feture=null,$user_id='')
    {
          $this->db->select('*',FALSE);
          $this->db->from('event e');
        
          $this->db->where('e.Id', $eventid);
          $query = $this->db->get();
          $res = $query->result_array();
          $resdata=array();
          if(!empty($res))
          {
               $menudata=explode(',', $res[0]['checkbox_values']);

               $menudata = array_diff($menudata, array('5','8','21','26','27','28'));
               $menudata=  array_merge($menudata,array());
               if($user_id!="")
                {
                    $user = $this->db->select('*')->from('user')->join('relation_event_user reu','reu.User_id = user.Id')->where('reu.User_id',$user_id)->get()->row_array();
                    if($user['Role_id']=='4')
                    {
                      $this->db->select('*')->from('event_attendee ea');
                      $this->db->join('user_views uv','uv.view_id=ea.views_id','right');
                      $this->db->where('ea.Event_id',$eventid);
                      $this->db->where('ea.Attendee_id',$user_id);
                      $vqu=$this->db->get();   
                      $vres=$vqu->result_array();
                      if(!empty($vres[0]['view_modules'])){
                          $check=explode(',', $res[0]['checkbox_values']);
                          $activemodules=explode(",",$vres[0]['view_modules']);   
                          $menudata=array_intersect($check, $activemodules);
                      }
                    }
                }
               $this->db->select('m.id,m.pagetitle, CASE WHEN em.img IS NULL THEN "" ELSE em.img END as img,CASE WHEN em.img_view IS NULL THEN "0" ELSE em.img_view END as img_view,CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,CASE WHEN em.Background_color IS NULL THEN "" ELSE em.Background_color END as Background_color',FALSE);
               $this->db->from('menu m');
               $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
               $this->db->join('event e', 'e.Id=em.event_id', 'left');

               if($menu_id!=null)
               {
                   $this->db->where('m.id', $menu_id);
               }
               if(empty($menudata))
               {
                  $menudata[0]=NULL;
               }
              $this->db->where_in('m.id',$menudata);

               $query = $this->db->get();
               $resdata = $query->result_array();
          }
          $this->load->model('event_model');
          $eventmodule=$this->event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          $arr=array('22','23','24','25');
          $module=array_merge($module,$arr);
          $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
          foreach ($resdata as $key => $value) 
          {
             $resdata[$key]['default_img'] = '';
             $resdata[$key]['is_icon'] = '0';
             $resdata[$key]['icon_path'] = '';

             if($value['img'] == '' && in_array($value['id'], $iconset) && $value['is_feture_products']=='1')
             {
                $path = base_url().'assets/css/images/icons/icon'.$res[0]['icon_set_type'].'/'.$value['id'].'.png';
                $resdata[$key]['is_icon'] = '1';
                $resdata[$key]['icon_path'] = $path;
             }
             else if($value['img'] == '' && $value['is_feture_products']=='1')
             {
                $resdata[$key]['default_img'] = base_url().'assets/images/EventApp_Default.jpg';
             }
                  
             if(!in_array($value['id'],$module))
             {
               unset($resdata[$key]);
             }

          }
          $resdata=array_values($resdata);
          /*foreach ($resdata as $key => $value) {
            if(strlen($value['menuname']) > 15 ){
              $string = $value['menuname'];
              $substr = substr($value['menuname'], 15,2);
              $attachment = '-';
              $position = strlen($value['menuname'])/2;
              $resdata[$key]['menuname'] = substr_replace($string, $attachment, $position,0);
          }
        }*/
       // print_r($resdata);
        function sortById($resdata)
        {
            $resdata1 = $resdata;
            $sort_key_val=array("1","7","10","16","20","43","2","6","13","15","44","17");
           foreach ($resdata as $key => $value) {
             $kkey=array_search($value['id'], $sort_key_val);
            
             if($kkey!='')
                $array[$kkey] = $value;
             else
                $array[$key] = $value;
           }
           for ($i = 0 ;$i<(count($array)); $i++) {
             $array1[] = $array[$i];
           }
           return $array1;
        }
        if($res[0]['Subdomain'] == 'aeinstitute17')
        {
          $resdata = sortById($resdata);
        }
        
        return $resdata;
    }


    public function get_event_list($id = null)
     {
          $user = $this->session->userdata('current_user');
          $this->db->select('e.*');
          $this->db->from('event e');
          $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
          if (!empty($user[0]->Id))
          {
               $this->db->where('e.Organisor_id', $user[0]->Id);
          }
          $this->db->where("e.Start_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')");
          $this->db->where("e.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
          $query = $this->db->get();
          $res = $query->result();
          $event_res = array();
          for ($i = 0; $i < count($res); $i++)
          {
               $event_res[$i]['Id'] = $res[$i]->Id;
               $event_res[$i]['Event_name'] = $res[$i]->Event_name;
               $event_res[$i]['Event_type'] = $res[$i]->Event_type;
               $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
               $event_res[$i]['URL'] = base_url().'app/test/'.$res[$i]->Subdomain;
          }
          $event_res_val = $event_res;
          return $event_res_val;
     }
    /*public function getPublicEvents()
    {
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      $this->db->where('e.Event_type','3');
      $this->db->where('e.Status','1');
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }*/

    /********For Searching*******/
    public function getPublicEvents($gcm_id)
    {
      $date = date('Y-m-d H:i:s',strtotime("-1 week"));
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      
     // $this->db->where_in('e.Event_type',);
      $this->db->where('e.Status','1');
      $this->db->where('e.Created_date >=',$date);
      $this->db->where('(e.secure_key IS NULL)');
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res1=$oqu->result_array();
      foreach ($res1 as $key => $value) {
          $ids[] = $value['event_id'];
      }
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
      $this->db->join("user_event_relation ur","ur.event_id=e.id");
      //$this->db->where('e.Event_type','3');
      $this->db->where('ur.gcm_id',$gcm_id);
      $this->db->where('e.Status','1');
      $this->db->where('(e.secure_key IS NULL)');
      if(!empty($ids))
      $this->db->where_not_in('e.id',$ids);
      $this->db->order_by("e.Start_date","desc");
      $this->db->group_by("e.id");
      $oqu=$this->db->get();
      $res2=$oqu->result_array();

      $res = array_merge($res1,$res2);
      $res = array_unique($res, SORT_REGULAR);
      return $res;
    }
    /**** For seraching + linkedin status*****/
    /*public function getPublicEvents($gcm_id)
    {
      $date = date('Y-m-d H:i:s',strtotime("-1 week"));
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      
      //$this->db->where_in('e.Event_type',);
      $this->db->where('e.Status','1');
      $this->db->where('e.Created_date >=',$date);
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res1=$oqu->result_array();
      foreach ($res1 as $key => $value) {
          $ids[] = $value['id'];
      }
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      $this->db->join("user_event_relation ur","ur.event_id=e.Id");
      //$this->db->where('e.Event_type','3');
      $this->db->where('ur.gcm_id',$gcm_id);
      $this->db->where('e.Status','1');
      if(!empty($ids))
      $this->db->where_not_in('e.id',$ids);
      $this->db->order_by("e.Start_date","desc");
      $this->db->group_by("e.id");
      $oqu=$this->db->get();
      $res2=$oqu->result_array();
      $res = array_merge($res1,$res2);

      //$res1[] = array_unique($res, SORT_REGULAR);
      return $res;
    }*/
    public function getPublicPrivateEventByKey($key)
    {
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images, CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      $event_type=array("1","2");
      //$this->db->where_in('e.Event_type',$event_type);
      $this->db->where('e.secure_key',$key);
      $this->db->where('e.Status','1');
      $this->db->group_by('e.id');
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }
    public function getcartdata($user_id)
    {

        $this->db->select('c.*,p.name,p.via_xml,p.event_id,p.thumb,(pq.quantity-pq.sale_qty) as product_quantity,pq.product_color,pq.product_size')->from('cart c');
        $this->db->where('c.user_id',$user_id);
        $this->db->join('product p','p.product_id=c.product_id');
        $this->db->join('product_quantity pq','pq.product_id=p.product_id','left');
        $q=$this->db->get();
        $cart=$q->result_array();
       
        for ($i=0; $i <count($cart); $i++) { 
          $subtotal=$cart[$i]['price']*$cart[$i]['quantity'];
          $grantotal=$grantotal+$subtotal;
        }
        if($grantotal!="")
        {
          $cart[0]['grantotal']=$grantotal;
        }
        return $cart;
    }
    public function getCategoryEvent($event_id)
    {
      $this->db->select('pc.product_category_id,pc.category')->from('product_category pc'); 
      $this->db->where('pc.event_id',$event_id); 
      $this->db->order_by("pc.product_category_id",'asc');
      $this->db->group_by("pc.product_category_id");
      $query = $this->db->get();
      $res = $query->result_array();
      return $res;
    }
    public function getSlidesEvent($event_id)
    {
      $this->db->select('*')->from('slideshow'); 
      $this->db->where('event_id',$event_id); 
      $query = $this->db->get();
      $res = $query->result_array();
      return $res;
    }
    public function getFundraisingSettings($event_id,$user_id)
    {
        $this->db->select('CASE WHEN fs.Fundraising_target IS NULL THEN "0" ELSE fs.Fundraising_target END as Fundraising_target,CASE WHEN fs.bg_color IS NULL THEN "#FFFFFF" ELSE fs.bg_color END as bg_color,CASE WHEN fs.font_color IS NULL THEN "#FFFFFF" ELSE fs.font_color END as font_color,CASE WHEN fs.content IS NULL THEN "" ELSE fs.content END as content,CASE WHEN fs.raisedsetting IS NULL THEN "1" ELSE fs.raisedsetting END as raisedsetting,CASE WHEN fs.target_raisedsofar_display IS NULL THEN "1" ELSE fs.target_raisedsofar_display END as target_raisedsofar_display,CASE WHEN fs.event_video_link IS NULL THEN "" ELSE fs.event_video_link END as event_video_link,CASE WHEN fs.instant_donate_enbled IS NULL THEN "1" ELSE fs.instant_donate_enbled END as instant_donate_enbled,CASE WHEN fs.instant_donate_backgroundcolor IS NULL THEN "#FFFFF" ELSE fs.instant_donate_backgroundcolor END as instant_donate_backgroundcolor,CASE WHEN fs.instant_donate_color IS NULL THEN "#FFFFFF" ELSE fs.instant_donate_color END as instant_donate_color,CASE WHEN fs.instant_donate_name IS NULL THEN "Make an  Instant Donation!" ELSE fs.instant_donate_name END as instant_donate_name,CASE WHEN fs.fundraising_donation_enbled IS NULL THEN "0" ELSE fs.fundraising_donation_enbled END as fundraising_donation_enbled,CASE WHEN fs.fundraising_donate_backgroundcolor IS NULL THEN "#FFFFFF" ELSE fs.fundraising_donate_backgroundcolor END as fundraising_donate_backgroundcolor,CASE WHEN fs.fundraising_donate_color IS NULL THEN "#FFFFFF" ELSE fs.fundraising_donate_color END as fundraising_donate_color,CASE WHEN fs.fundraising_donate_name IS NULL THEN "Fundraising Donation" ELSE fs.fundraising_donate_name END as fundraising_donate_name,CASE WHEN fs.raised_display IS NULL THEN "1" ELSE fs.raised_display END as raised_display,CASE WHEN fs.raised_price IS NULL THEN "0" ELSE fs.raised_price END as raised_price,CASE WHEN fs.updatedate IS NULL THEN "" ELSE fs.updatedate END as updatedate,CASE WHEN fs.bids_donations_display IS NULL THEN "1" ELSE fs.bids_donations_display END as bids_donations_display',false);
        $this->db->from('fundraising_setting fs'); 
        $this->db->where('fs.Event_id',$event_id); 
        $query = $this->db->get();
        $res = $query->result_array();
        $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
        $extra=json_decode($extra['extra_column'],true);
        foreach ($extra as $key => $value) {
          $keyword="{".str_replace(' ', '', $key)."}";
          if(stripos(strip_tags($res[0]['content'],$keyword)) !== false)
          {
            $res[0]['content']=str_ireplace($keyword, $value,$res[0]['content']);
          }
        }
        return $res;
    }
    public function getCurrencyByEvent($event_id)
    {
      $this->db->select('currency')->from('fundraising_currency');
      $this->db->where('event_id', $event_id);
      $qry = $this->db->get();
      $res = $qry->result_array();
      if(!empty($res))
      {
        $currency = $res[0]['currency'];
        $currency = explode("#", $currency);
        $currency=$currency[0];
        return $currency;
      }
      else
      {
        return 'usd';
      }
    }
    public function get_ordered_total($event_id,$update_date)
    {
          $this->db->select('p.product_id,ot.*,SUM(ot.price*ot.qty) as total')->from('product p');
          $this->db->join("order_transaction ot", "ot.product_id=p.product_id");
          $this->db->where('p.event_id', $event_id);
          $this->db->group_by("ot.order_id");
          $query = $this->db->get();
          $res1 = $query->result_array();
        
          $this->db->select('p.product_id,ot.*,SUM(ot.price*ot.qty) as total')->from('product p');
          $this->db->join("order_transaction ot", "ot.product_id=p.product_id");
          $this->db->where('ot.event_id', $event_id);
          $this->db->where('p.is_superadmin','1');
          $this->db->group_by("ot.order_id");
          $query = $this->db->get();
          $res2 = $query->result_array();
          $res=array_merge($res1,$res2);
          $myarr=array();
          $i = 0;
        
          foreach ($res as $key => $value)
          {
               $this->db->select('order_status,created_date')->from('order');
               //$this->db->where('order_status', 'completed');
               //$this->db->where('transaction_id IS NOT NULL');
               $this->db->where('Id', $value['order_id']);
               if(!empty($update_date))
               {
                  $this->db->where('created_date >  ',$update_date);
               }
               $query = $this->db->get();
               $res1 = $query->result_array();
               $myarr[$i]['price'] = $res[$i]['total'];
               $myarr[$i]['Id'] = $res[$i]['Id'];
               $myarr[$i]['update_date'] = $res1[0]['created_date'];
               $i++;
          }
        return $myarr;
    }

    public function get_bid_total($event_id,$update_date)
    {
          
          $this->db->select('p.product_id,max(pub.bid_amt) as TotalPrice,pub.date')->from('product p');
          $this->db->join("product_user_bids pub", "pub.product_id=p.product_id");
          if(!empty($update_date))
          {
              $this->db->where('pub.date >', $update_date);
          }
          $this->db->where('p.event_id', $event_id);
          $this->db->order_by("pub.bid_amt","desc");
          $this->db->group_by("pub.product_id");
          $query = $this->db->get();
          $res1 = $query->result_array();


           $this->db->select('p.product_id,max(pub.bid_amt) as TotalPrice,pub.date')->from('product p');
         
          $this->db->join("product_user_bids pub", "pub.product_id=p.product_id");
          if(!empty($update_date))
          {
              $this->db->where('pub.date >', $update_date);
          }
          $this->db->where('pub.event_id', $event_id);
          $this->db->where('p.is_superadmin', '1');
          $this->db->order_by("pub.bid_amt","desc");
          $this->db->group_by("pub.product_id");
          $query = $this->db->get();
          $res2 = $query->result_array();
          $res=array_merge($res1,$res2);
          $bid_amt=0;
          foreach ($res as $key => $value) {
            $bid_amt+=$value['TotalPrice'];
          }
          
          return $bid_amt;
    }
    public function get_pledge_total($event_id,$update_date)
    {

          $this->db->select('p.product_id,sum(pad.pledge_amt) as tot_pledge_amt,sum(pad.pledge_othr_amt) as total')->from('product p');
          $this->db->join("pledge_amt_donate pad", "pad.product_id=p.product_id");
          if(!empty($update_date))
          {
            $this->db->where('pad.created_date >', $update_date);
          }
          $this->db->where('p.event_id', $event_id);
          //$this->db->where('pad.payment_status','1');
          //$this->db->where('pad.transaction_id IS NOT NULL');
          $query = $this->db->get();
          $res = $query->result_array();
        
          if(!empty($res))
          {
               $tot = $res[0]['tot_pledge_amt'] + $res[0]['total'];
          }
          else
          {
               $tot=0;
          }
          return $tot;
    }
    public function getinstantdonationtotal($eid)
    {
      $this->db->select('sum(donation_amount) as total')->from('instant_donation');
      $this->db->where('event_id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res[0]['total'];
    }
    public function getfundraisingdonationtotal($eid)
    {
      $this->db->select('sum(donation_amount) as total')->from('fundraising_donation');
      $this->db->where('event_id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res[0]['total'];
    }
    public function get_latest_total_bids($event_id,$limit)
     {

        $this->db->select('CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN pub.bid_amt IS NULL THEN "" ELSE pub.bid_amt END as bid_amt,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as product_name',false);
        $this->db->from('product p'); 
        $this->db->join("product_user_bids pub","pub.product_id = p.product_id");
        $this->db->join('user u','pub.user_id=u.Id');
        $this->db->where('p.event_id',$event_id); 
        $this->db->order_by("pub.id", "desc");
        $this->db->limit($limit);
        $query=$this->db->get();
        $res=$query->result_array();
        if(!empty($res))
        {
          foreach ($res as $key => $value) 
          {
            $myarr[$key]['Firstname']=ucfirst($value['Firstname']);
            $myarr[$key]['Lastname']=ucfirst($value['Lastname']);
            $myarr[$key]['Logo']=$value['Logo'];
            $myarr[$key]['product_name']=$value['product_name'];
            $myarr[$key]['amt']=$value['bid_amt'];
          }
        }
        return $myarr;

     }
    public function get_latest_pleadges($event_id,$limit)
     {

        $this->db->select('CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN pad.pledge_amt IS NULL THEN "" ELSE pad.pledge_amt END as pledge_amt,CASE WHEN pad.pledge_othr_amt IS NULL THEN "" ELSE pad.pledge_othr_amt END as pledge_othr_amt,
          CASE WHEN p.name IS NULL THEN "" ELSE p.name END as product_name',false);
        $this->db->from('product p'); 
        $this->db->join("pledge_amt_donate pad","pad.product_id = p.product_id");
        $this->db->join('user u','pad.user_id=u.Id');
        $this->db->where('p.event_id',$event_id); 
        $this->db->order_by("pad.id", "desc");
        $this->db->limit($limit);
        $query=$this->db->get();
        $res1=$query->result_array();
        if(!empty($res1))
        {
          foreach ($res1 as $key => $value) 
          {
            
            $myarr[$key]['Firstname']=ucfirst($value['Firstname']);
            $myarr[$key]['Lastname']=ucfirst($value['Lastname']);
            $myarr[$key]['Logo']=$value['Logo'];
            $myarr[$key]['product_name']=$value['product_name'];
            if(!empty($value['pledge_amt']))
            {
               $myarr[$key]['amt']=$value['pledge_amt'];
            }
            else
            {
               $myarr[$key]['amt']=$value['pledge_othr_amt'];
            }
          }
        }
        $instant_donation=$this->get_insta_donation($event_id);
        $fundraising_donation=$this->get_fundraising_donation_list($event_id,5);
        $res=array_merge($myarr,$instant_donation,$fundraising_donation);
        return $res;

    }
    public function get_insta_donation($eid)
    {
      $this->db->select('CASE WHEN d.name IS NULL THEN "" ELSE d.name END as Firstname,CASE WHEN d.donation_amount IS NULL THEN "0" ELSE d.donation_amount END as donation_amount',false);
        $this->db->from('instant_donation d'); 
      $this->db->where('d.event_id',$eid);
      $this->db->order_by('d.id','desc');
      $this->db->limit(5);
      $query=$this->db->get();
      $res=$query->result_array();
      foreach ($res as $key => $value) 
      {
        $myarr[$key]['Firstname']=ucfirst($value['Firstname']);
        $myarr[$key]['Lastname']='';
        $myarr[$key]['Logo']='';
        $myarr[$key]['product_name']='Instant Donation';
        $myarr[$key]['amt']=$value['donation_amount'];
      }
      return $myarr;
    }
    public function get_fundraising_donation_list($eid,$limit=NULL)
    {
       $this->db->select('CASE WHEN fn.name IS NULL THEN "" ELSE fn.name END as Firstname,CASE WHEN fn.donation_amount IS NULL THEN "0" ELSE fn.donation_amount END as donation_amount',false);
        $this->db->from('fundraising_donation fn'); 
      $this->db->where('fn.event_id',$eid);
      $this->db->order_by('fn.id','desc');
      if($limit!="") {
        $this->db->limit($limit);
      }
      $query=$this->db->get();
      $res=$query->result_array();
      foreach ($res as $key => $value) {
        $myarr[$key]['Firstname']=ucfirst($value['Firstname']);
        $myarr[$key]['Lastname']='';
        $myarr[$key]['Logo']='';
        $myarr[$key]['product_name']='Fundraising Donation';
        $myarr[$key]['amt']=$value['donation_amount'];
      }
      return $myarr;
    }
    public function products($event_id,$page_no=1)
    {
      $limit=12;
      $page_no = (!empty($page_no))?$page_no:1;
      $start=($page_no-1)*$limit;
      
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.is_superadmin','1'); 
      $this->db->where('p.auction_show','1'); 
      $this->db->where('p.features_product','1');
      $this->db->group_by("p.product_id");
      $query = $this->db->get();
      $superadmin = $query->result_array();
    
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('features_product','1');
      $this->db->group_by("p.product_id");
      $query = $this->db->get();
      $res2 = $query->result_array();
      $res=array_merge($superadmin,$res2);
      $total=count($res);
      $total_page=ceil($total/$limit);
      $res=array_slice($res,$start,$limit);
      $data['products']=$res;
      $data1['total_page']=$total_page;
      if(!empty($data)) 
      {
       foreach ($data['products'] as $key => $value) 
        {
            $price="";
            $max="";
            $data1['products'][$key]['product_id']=$value['product_id'];
            $data1['products'][$key]['name']=$value['name'];
            $data1['products'][$key]['description']=$value['description'];
            $data1['products'][$key]['thumb']=$value['thumb'];
            $data1['products'][$key]['auctionType']=$value['auctionType'];
            if($value['auctionType']==1 || $value['auctionType']==2)
            {
                  $price=$value['startPrice'];
                  $data1['products'][$key]['price']=$price;
                  $max=$value['max_bid'];
                  $data1['products'][$key]['max_bid']=$max;
                  if(empty($value['max_bid']))
                  {
                    $data1['products'][$key]['flag_price']=1;
                    $data1['products'][$key]['max_bid']="";
                  }
                  else
                  {
                    $data1['products'][$key]['flag_price']=0;
                  }
                  
            }
            else if($value['auctionType']==3)
            {
                  $price=$value['price'];
                  $data1['products'][$key]['price']=$price;
                  $data1['products'][$key]['flag_price']=1;
                  $data1['products'][$key]['max_bid']='';
            }
            else if($value['auctionType']==4)
            {
                $price=$value['pledge_amt'];
                if(!empty($value['price']))
                {
                    $price=$value['pledge_amt'];
                }
                $data1['products'][$key]['price']=$price;
                $data1['products'][$key]['max_bid']='';
                $data1['products'][$key]['flag_price']=1;
            }
        }
        return $data1;
      } 
      else 
      {
        return false;
      }
  }
  public function productsByCategory($event_id,$page_no=1,$category)
    {
      $limit=12;
      $page_no = (!empty($page_no))?$page_no:1;
      $start=($page_no-1)*$limit;
      
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,CASE WHEN p.category IS NULL THEN "" ELSE p.category END as category',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.is_superadmin','1'); 
      $this->db->where('p.auction_show','1'); 
      $this->db->where('p.features_product','1');
      $this->db->where('p.category',$category);
      $this->db->group_by("p.product_id");
      $query = $this->db->get();
      $superadmin = $query->result_array();
    
      $this->db->select('p.product_id,CASE WHEN pb.bid_amt IS NULL THEN "0" ELSE pb.bid_amt END as bid_amt,max(pb.bid_amt) as max_bid,CASE WHEN p.name IS NULL THEN "" ELSE p.name END as name,CASE WHEN p.short_description IS NULL THEN "" ELSE p.short_description END as short_description,CASE WHEN p.description IS NULL THEN "" ELSE p.description END as description,CASE WHEN p.startPrice IS NULL THEN "" ELSE p.startPrice END as startPrice,CASE WHEN p.price IS NULL THEN "" ELSE p.price END as price,CASE WHEN p.auctionType IS NULL THEN "" ELSE p.auctionType END as auctionType,CASE WHEN p.pledge_amt IS NULL THEN "" ELSE p.pledge_amt END as pledge_amt,CASE WHEN p.thumb IS NULL THEN "" ELSE p.thumb END as thumb,CASE WHEN p.category IS NULL THEN "" ELSE p.category END as category',false);
      $this->db->from('product p');
      $this->db->join("product_user_bids pb","pb.product_id=p.product_id",'left');
      $this->db->where('p.event_id',$event_id); 
      $this->db->where('p.approved_status','1'); 
      $this->db->where('features_product','1');
      $this->db->where('p.category',$category);
      $this->db->group_by("p.product_id");
      $query = $this->db->get();
      $res2 = $query->result_array();
      $res=array_merge($superadmin,$res2);
      $total=count($res);
      $total_page=ceil($total/$limit);
      $res=array_slice($res,$start,$limit);
      $data['products']=$res;
      $data1['total_page']=$total_page;
      if(!empty($data)) 
      {
       foreach ($data['products'] as $key => $value) 
        {
            $price="";
            $max="";
            $data1['products'][$key]['product_id']=$value['product_id'];
            $data1['products'][$key]['name']=$value['name'];
            $data1['products'][$key]['description']=$value['description'];
            $data1['products'][$key]['thumb']=$value['thumb'];
            $data1['products'][$key]['auctionType']=$value['auctionType'];
            $data1['products'][$key]['category']=$value['category'];
            if($value['auctionType']==1 || $value['auctionType']==2)
            {
                  $price=$value['startPrice'];
                  $data1['products'][$key]['price']=$price;
                  $max=$value['max_bid'];
                  $data1['products'][$key]['max_bid']=$max;
                  if(empty($value['max_bid']))
                  {
                    $data1['products'][$key]['flag_price']=1;
                    $data1['products'][$key]['max_bid']="";
                  }
                  else
                  {
                    $data1['products'][$key]['flag_price']=0;
                  }
                  
            }
            else if($value['auctionType']==3)
            {
                  $price=$value['price'];
                  $data1['products'][$key]['price']=$price;
                  $data1['products'][$key]['flag_price']=1;
                  $data1['products'][$key]['max_bid']='';
            }
            else if($value['auctionType']==4)
            {
                $price=$value['pledge_amt'];
                if(!empty($value['price']))
                {
                    $price=$value['pledge_amt'];
                }
                $data1['products'][$key]['price']=$price;
                $data1['products'][$key]['max_bid']='';
                $data1['products'][$key]['flag_price']=1;
            }
        }
        return $data1;
      } 
      else 
      {
        return false;
      }
  }
  public function getfrontfunmenu($eventid,$menu_id=null,$is_feture=null)
  {
        $this->db->select('m.id,CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN m.menuname IS NULL THEN m.menuname ELSE m.menuname END as menuname',FALSE);
        $this->db->from('menu m');
        $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id='.$eventid.'', 'left');
        $menu_id=array("22","24","25");
        $this->db->where_in('m.id', $menu_id);
        if($is_feture!=null)
        {
            $this->db->where('em.is_feture_product','1');
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
  }
  public function getAccname($id='')
  {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu','reu.User_id = tu.Id','left');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",3);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
  }
  public function get_subdomain($id)
  {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();  
        $Subdomain=$res[0]['Subdomain'];
        return $Subdomain;

  }
  public function getDonationDetails($event_id)
  {
        $this->db->select('*,
          CASE WHEN bg_color IS NULL THEN "" ELSE bg_color END as bg_color,
          CASE WHEN font_color IS NULL THEN "" ELSE font_color END as font_color,
          CASE WHEN content IS NULL THEN "" ELSE content END as content,
          CASE WHEN raised_price IS NULL THEN "" ELSE raised_price END as raised_price,
          CASE WHEN checkout_content IS NULL THEN "" ELSE checkout_content END as checkout_content,
          CASE WHEN signup IS NULL THEN "" ELSE signup END as signup,
          CASE WHEN email IS NULL THEN "" ELSE email END as email,
          CASE WHEN event_video_link IS NULL THEN "" ELSE event_video_link END as event_video_link,
          CASE WHEN product_color IS NULL THEN "" ELSE product_color END as product_color,
          CASE WHEN instant_donate_backgroundcolor IS NULL THEN "" ELSE instant_donate_backgroundcolor END as instant_donate_backgroundcolor,
          CASE WHEN instant_donate_color IS NULL THEN "" ELSE instant_donate_color END as instant_donate_color,
          CASE WHEN instant_donate_name IS NULL THEN "" ELSE instant_donate_name END as instant_donate_name,
          CASE WHEN fun_donation_first IS NULL THEN "" ELSE fun_donation_first END as fun_donation_first,
          CASE WHEN fun_donation_second IS NULL THEN "" ELSE fun_donation_second END as fun_donation_second,
          CASE WHEN fundraising_donate_backgroundcolor IS NULL THEN "" ELSE fundraising_donate_backgroundcolor END as fundraising_donate_backgroundcolor,
          CASE WHEN fundraising_donate_color IS NULL THEN "" ELSE fundraising_donate_color END as fundraising_donate_color,
          CASE WHEN fundraising_donate_name IS NULL THEN "" ELSE fundraising_donate_name END as fundraising_donate_name',false);
        $this->db->from('fundraising_setting');
        $this->db->where('Event_id',$event_id);
        $query = $this->db->get();
        $res = $query->row();  
        return $res;
  }
  public function getSupporterData($event_id)
  {
        $this->db->select('*');
        $this->db->from('fundraising_donation');
        $this->db->where('event_id',$event_id);
        $query = $this->db->get();
        $res = $query->result_array();  
        foreach ($res as $key => $value) {
            $res[$key]['time'] = $this->get_timeago(strtotime($value['create_date']));
        }
        return $res;
  }
  public function get_timeago($ptime)
  {
    $estimate_time = time() - $ptime;

    if ($estimate_time < 1)
    {
         return '1 second ago';
    }

    $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
    );
    foreach ($condition as $secs => $str)
    {
     $d = $estimate_time / $secs;

     if ($d >= 1)
     {
          $r = round($d);
          return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
     }
    }
  }
  public function saveDonationDetails($data)
  {
      $this->db->insert('fundraising_donation',$data);
      return true;
  }
  public function saveInstantDonationDetails($data)
  {
    $this->db->insert('instant_donation',$data);
    return true;
  }
  public function get_order_list($user_id)
    {
          $this->db->select('CASE WHEN mode IS NULL THEN "" ELSE mode END as mode,CASE WHEN order_status IS NULL THEN "" ELSE order_status END as order_status,created_date',false);
          $this->db->from('order');
          $this->db->where('user_id', $user_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }

    public function get_auction_list($user_id,$event_id)
    {
          $this->db->select("p.name,pub.bid_amt,pub.date,CASE WHEN (pub.win_status='1') THEN 'Winner' ELSE 'Not Win' END as win_status,pub.payment_status",false);
          $this->db->from('product_user_bids pub');
          $this->db->join('product p','pub.product_id=p.product_id');
          $this->db->where('pub.user_id', $user_id);
          $this->db->where('pub.event_id', $event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }

    public function get_pledgelist($user_id,$event_id)
    {
          $this->db->select("pad.*,p.name,CASE WHEN pad.pledge_othr_amt IS NULL THEN '' ELSE pad.pledge_othr_amt END as pledge_othr_amt,CASE WHEN pad.payment_status IS NULL THEN '' ELSE pad.payment_status END as payment_status,CASE WHEN pad.transaction_id IS NULL THEN '' ELSE pad.transaction_id END as transaction_id,CASE WHEN pad.mode IS NULL THEN '' ELSE pad.mode END as mode,CASE WHEN pad.comment IS NULL THEN '' ELSE pad.comment END as comment,CASE WHEN pad.pledge_visibility IS NULL THEN '' ELSE pad.pledge_visibility END as pledge_visibility,CASE WHEN p.thumb IS NULL THEN '' ELSE p.thumb END as thumb",false);
          $this->db->from('pledge_amt_donate pad');
          $this->db->join('product p','pad.product_id=p.product_id');
          $this->db->where('pad.user_id', $user_id);
          $this->db->where('p.event_id', $event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }
    public function getEvent($event_id)
    {
          $this->db->select('*');
          $this->db->from('event');
          $this->db->where('Id', $event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }
    public function get_stripe_setting($event_id)
    {
        $this->db->select('Organisor_id');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query = $this->db->get();
        $res = $query->row();
       

        $this->db->select('*')->from('stripe_settings');
        $this->db->where('organisor_id',$res->Organisor_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function saveOpenAppData($data)
    {
        $count = $this->db->select('*')->from('user_event_relation')->where($data)->get()->num_rows();
        if($count == 0)
        {
          $this->db->insert('user_event_relation',$data);
        }
    }
    public function serachEventByName($event_name)
    {
        $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      $this->db->like('e.Event_name',$event_name);
      $this->db->where('(e.secure_key IS NULL)');
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }
    public function getEventData($event_id)
    {
      return $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
    }
}

?>