<?php

class cms_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }

     public function get_all_cms_list($id,$cms_id=NULL)
     {
          $user = $this->session->userdata('current_user');
          $orid = $this->data['user']->Id;
          $this->db->select('*');
          $this->db->from('cms');
          //$this->db->where('Organisor_id', $user[0]->Id);
          $this->db->where('Event_id', $id);
          
          if($cms_id!=NULL)
          {
               $this->db->where('Id', $cms_id);
          }
          
          $this->db->order_by("Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
    
    /*public function get_cms_page($id,$cms_id=NULL)
     {
          $this->db->select('CASE WHEN c.Id IS NULL THEN "" ELSE c.Id END as Id,CASE WHEN em.title IS NULL THEN "" ELSE em.title END as Menu_name,CASE WHEN em.menu_id IS NULL THEN "" ELSE em.menu_id END as menu_id,CASE WHEN em.cms_id IS NULL THEN "" ELSE em.cms_id END as cms_id,CASE WHEN em.img IS NULL THEN "" ELSE em.img END as img,CASE WHEN em.Background_color IS NULL THEN "#FFFFFF" ELSE em.Background_color END as Background_color,CASE WHEN em.is_feture_product IS NULL THEN "" ELSE em.is_feture_product END as is_feture_product,CASE WHEN em.img_view IS NULL THEN "" ELSE em.img_view END as img_view,CASE WHEN em.Redirect_url IS NULL THEN "" ELSE em.Redirect_url END as Redirect_url,CASE WHEN em.event_id IS NULL THEN "" ELSE em.event_id END as event_id,CASE WHEN c.Description IS NULL THEN "" ELSE c.Description END as Description,CASE WHEN c.Images IS NULL THEN "" ELSE c.Images END as Images,CASE WHEN c.Logo_images IS NULL THEN "" ELSE c.Logo_images END as Logo_images,CASE WHEN c.Img_view IS NULL THEN "" ELSE c.Img_view END as Img_view,CASE WHEN em.Background_color IS NULL THEN "#FFFFFF" ELSE c.Background_color END as Background_color,CASE WHEN c.fecture_module IS NULL THEN "" ELSE c.fecture_module END as fecture_module,CASE WHEN c.cms_fecture_module IS NULL THEN "" ELSE c.cms_fecture_module END as cms_fecture_module,CASE WHEN c.Status IS NULL THEN "" ELSE c.Status END as Status',false);
          $this->db->from('cms c');
          $this->db->join('event_menu em','em.cms_id=c.Id','left');
          $this->db->where('c.Event_id', $id);
          if($cms_id!=NULL)
          {
               $this->db->where('c.Id', $cms_id);
          }
          $this->db->where('c.status', '1');
          $this->db->order_by("c.Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          foreach ($res as $key => $value) {
               
               preg_match_all('/(src)=("[^"]*")/i',$value['Description'], $url['url']);
               $res[$key]['url'] =str_replace('"', '', $url['url'][2][0]);

          }
          return $res;
     }*/
     public function get_cms_page($id,$cms_id=NULL,$token)
     {
          $token = ($token == NULL) ? '' : $token;
          $this->db->select('e.checkbox_values,e.icon_set_type',FALSE);
          $this->db->from('event e');
        
          $this->db->where('e.Id', $id);
          $query = $this->db->get();
          $event = $query->result_array();

          $this->db->select('CASE WHEN c.Id IS NULL THEN "" ELSE c.Id END as Id,CASE WHEN em.title IS NULL THEN "" ELSE em.title END as Menu_name,CASE WHEN em.menu_id IS NULL THEN "" ELSE em.menu_id END as menu_id,CASE WHEN em.cms_id IS NULL THEN "" ELSE em.cms_id END as cms_id,CASE WHEN em.img IS NULL THEN "" ELSE em.img END as img,CASE WHEN em.Background_color IS NULL THEN "#FFFFFF" ELSE em.Background_color END as Background_color,CASE WHEN em.is_feture_product IS NULL THEN "" ELSE em.is_feture_product END as is_feture_product,CASE WHEN em.img_view IS NULL THEN "" ELSE em.img_view END as img_view,CASE WHEN em.Redirect_url IS NULL THEN "" ELSE em.Redirect_url END as Redirect_url,CASE WHEN em.event_id IS NULL THEN "" ELSE em.event_id END as event_id,CASE WHEN c.Description IS NULL THEN "" ELSE c.Description END as Description,CASE WHEN c.Images IS NULL THEN "" ELSE c.Images END as Images,CASE WHEN c.Logo_images IS NULL THEN "" ELSE c.Logo_images END as Logo_images,CASE WHEN c.Img_view IS NULL THEN "" ELSE c.Img_view END as Img_view,CASE WHEN em.Background_color IS NULL THEN "#FFFFFF" ELSE c.Background_color END as Background_color,CASE WHEN c.fecture_module IS NULL THEN "" ELSE c.fecture_module END as fecture_module,CASE WHEN c.cms_fecture_module IS NULL THEN "" ELSE c.cms_fecture_module END as cms_fecture_module,CASE WHEN c.Status IS NULL THEN "" ELSE c.Status END as Status,CASE WHEN em.Background_color IS NULL THEN "" ELSE em.Background_color END as Background_color,CASE WHEN c.exi_page_id IS NULL THEN "0" ELSE "1" END as is_linked_with_exhibitor,CASE WHEN c.exi_page_id IS NULL THEN "" ELSE c.exi_page_id END as exi_page_id,show_in_app',false);
          $this->db->from('cms c');
          $this->db->join('event_menu em','em.cms_id=c.Id','left');
          $this->db->where('c.Event_id', $id);
          if($cms_id!=NULL)
          {
               $this->db->where('c.Id', $cms_id);
          }
          $this->db->where('c.status', '1');
         // $this->db->where('em.is_feture_product', '1');
          $this->db->order_by("c.Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
          $user = $this->db->select('*')->from('user')->where('token',$token)->get()->row_array();
          $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$id)->where('Attendee_id',$user['Id'])->get()->row_array();
          $extra=json_decode($extra['extra_column'],true);
          foreach ($res as $key => $value) 
          {
             $res[$key]['default_img'] = '';
             $res[$key]['is_icon'] = '0';
             $res[$key]['icon_path'] = '';
             preg_match_all('/(src)=("[^"]*")/i',$value['Description'], $url['url']);
               $res[$key]['url'] =str_replace('"', '', $url['url'][2][0]);
             if($value['img'] == '' && in_array($value['id'], $iconset) && $value['is_feture_products']=='1')
             {
                $path = base_url().'assets/css/images/icons/icon'.$event[0]['icon_set_type'].'/'.$value['id'].'.png';
                $res[$key]['is_icon'] = '1';
                $res[$key]['icon_path'] = $path;
             }
             else if($value['img'] == '' && $value['is_feture_products']=='1')
             {
                $res[$key]['default_img'] = base_url().'assets/images/EventApp_Default.jpg';
             }
            foreach ($extra as $key => $value) {
                $keyword="{".str_replace(' ', '', $key)."}";
                if(stripos(strip_tags($res[0]['Description'],$keyword)) !== false)
                {
                  $res[0]['Description']=str_ireplace($keyword, $value,$res[0]['Description']);
                }
              }    
          }
          return $res;
     }
}

?>