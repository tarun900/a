<?php
class App_login_model extends CI_Model{
function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->db1 = $this->load->database('db1', TRUE);
      
    }
    public function fb_signup($email,$facebook_id,$event_id,$firstname,$facebook_logo,$device)
    {
        $this->db->select('u.*,CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as User_id,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id',FALSE);
        $this->db->from('user u');
        $this->db->where('u.Email',$email);
        //$this->db->where('u.facebook_id',$facebook_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        $data=$res[0];
        if(!empty($res))
        {
            $up['Logo'] = $facebook_logo;
            $up['facebook_id'] = $facebook_id;
            $this->db->where('Email',$email);
           // $this->db->where('facebook_id',$facebook_id);
            $this->db->update('user',$up);
            

            $event_arr["Event_id"]=$event_id;
            $event_arr["User_id"]=$data['Id'];
            $event_arr["Role_id"]=4;
            $event_arr["Organisor_id"]=$data['Organisor_id'];

            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id',$event_id);
            $this->db->where('User_id',$data['Id']);
            $this->db->where('Organisor_id',$data['Organisor_id']);
            $qu=$this->db->get();
            $res=$qu->row();
            if(!count($res))
            {
                $this->db->insert("relation_event_user",$event_arr);
            }
            $data1["Rid"]=$res->Role_id;
            $data1=$this->getUserDetailsId($data['Id']);
            array_walk($data1,function(&$item){$item=strval($item);});
            return $data1;
        }
        else
        {

            $this->db->select('*')->from('event e');
            $this->db->where('e.id',$event_id);
            $qu=$this->db->get();
            $res=$qu->result_array();
            $Organisor_id=$res[0]['Organisor_id'];
            $data['Organisor_id']=$Organisor_id;
            $data['Active']='1';
            $data['token'] = sha1($email.$facebook_id.$event_id);
            $data['Email']=$email;
            $data['facebook_id']=$facebook_id;
            $data['Logo']=$facebook_logo;
            $data['Firstname']=$firstname;
            $data['device'] = $device;
            $data['token'] = sha1($email.$password.$event_id);
            $this->db->insert("user",$data);
            $user_id=$this->db->insert_id();
           
            $event_arr["Event_id"]=$event_id;
            $event_arr["User_id"]=$user_id;
            $event_arr["Role_id"]=4;
            $event_arr["Organisor_id"]=$Organisor_id;
            $this->db->insert("relation_event_user",$event_arr);
            $data=$this->getUserDetailsId($user_id);
           // $data["Rid"]=4;
            return $data;
        }
    }
    public function getAllCountryList()
    {
        $this->db->select('*')->from('country');
        $this->db->order_by("country_name");
        $qu=$this->db->get();
        $res=$qu->result_array();
         $i=2;
          $j=0;
          $arr;
          foreach ($res as $key => $value) {
            if($value['id']==223 || $value['id']==225)
            {
                $arr[$j]=$value;
                $j++;
            }
            else
            {
                $arr[$i]=$value;
                $i++;
            }
          }
         
          ksort($arr);
        return $arr;
    }
    public function checkEmailAlreadyByEvent($email,$event_id)
    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs","rs.User_id=u.Id");
        $this->db->where('u.Email',$email);
        $this->db->where('rs.Event_id',$event_id);
        $this->db->where(array('u.facebook_id' => ''));
        $qu=$this->db->get();
        $res=$qu->result_array();
        $cnt=count($res);
        return $cnt;
    }
    public function getCountry($country_id)
    {
        $this->db->select('c.country_name')->from('country c');
        $this->db->where('c.id',$country_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(!empty($res))
        {
            $country=$res[0]['country_name'];
            return $country;
        }
        else
        {
            return "";
        }
    }
    public function getState($state_id)
    {
        $this->db->select('s.state_name')->from('state s');
        $this->db->where('s.id',$state_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(!empty($res))
        {
            $state=$res[0]['state_name'];
            return $state;
        }
        else
        {
            return "";
        }
    }
    public function checkEmailAlreadyByEvent1($email,$event_id)
    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs","rs.User_id=u.Id");
        $this->db->where('u.Email',$email);
        $this->db->where('rs.Event_id',$event_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        $cnt=count($res);
        return $cnt;
    }
    public function getUserDetailsId($user_id)
    {

        $this->db->select('u.*,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id',
            FALSE);
        $this->db->from('user u');
        $this->db->where('u.Id',$user_id);
        $this->db->join('relation_event_user as ru','u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $qu=$this->db->get();
        $res=$qu->result_array();

        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id',$res[0]['Id']);
        $links=$this->db->get();
        $links=$links->row_array();

        $res[0]['linkedin_url'] = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
        $res[0]['facebook_url'] = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
        $res[0]['twitter_url'] = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
        $res[0]['Website_url'] = ($links['Website_url']) ? $links['Website_url'] : '';
        $res=$res[0];
        array_walk($res,function(&$item){$item=strval($item);});
        return $res;
    }
    
    public function check_app_login($email,$password,$event_id)
    {
        $this->db->select('u.*,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id,ru.Organisor_id as Organisor_id',
            FALSE);
        $this->db->from('user u');
        $this->db->where('u.Email', $email);
        $this->db->where('u.Password', md5($password));
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->where('ru.Event_id', $event_id);
        $this->db->join('relation_event_user as ru','u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        if(!empty($res))
        {
          unset($res[0]->Speaker_desc);
        }

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id',$res[0]->Organisor_id);
        $qu=$this->db->get();
        $organizeruser=$qu->result_array();

        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id',$res[0]->Id);
        $links=$this->db->get();
        $links=$links->row_array();
        $res[0]->linkedin_url = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
        $res[0]->facebook_url = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
        $res[0]->twitter_url = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
        $res[0]->Website_url = ($links['Website_url']) ? $links['Website_url'] : '';

        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('Email',$organizeruser[0]['Email']);
        $this->db1->where('Expiry_date >=',date("Y-m-d"));
        $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
        $query=$this->db1->get();
        $res1=$query->result_array();
        
        if(count($res1)==0 || $res1[0]['status']==0)
        {
            if(count($res)==0)
            {
                $res=$res1;
            }
            else
            {
                $res=$res1;
                $res="inactive";
            }
        }
        return $res;
    }
    public function get_additionalFormData($user_id)
    {
        $this->db->select('json_data');
        $this->db->from('signup_form_data');
        $this->db->where('user_id',$user_id);
        $qu=$this->db->get();
        $data=$qu->result_array();
        return $data;
    }
    public function check_token_with_event($token,$event_id,$type)
    {

        $this->db->select('e.*,u.Id as user_id');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->where('ru.Event_id', $event_id);
        /*if($type==1 || $type==2)
        {
            if($token=="")
            {
                $token="NULL";
            }
            $this->db->where('u.token', $token);
        }*/
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function check_token($token)
    {
        $token = ($token == NULL) ? '' : $token;
        $this->db->select('u.*,role.Name as Role_name,u.Id as Id,role.Id as Rid,mobile_data.token as _token');
        $this->db->from('mobile_data');
        $this->db->where('token', $token);
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->join('user as u','u.Id=mobile_data.uid');
        $this->db->join('relation_event_user as ru','u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function check_email_with_event($token,$email)
    {

        $this->db->select('e.Id,e.Start_date,e.End_date,e.Subdomain,coalesce(e.description," ") as description1,e.Created_date,e.Event_name,e.Organisor_id,e.Img_view,e.Status,e.Event_type,e.Event_time,e.Event_time_option,e.checkbox_values,coalesce(e.fun_header_status," ") as fun_header_status1,coalesce(e.Background_img," ") as Background_img1,u.Id as user_id, coalesce(Logo_images," ") as Logo_images1, coalesce(fun_logo_images," ") as fun_logo_images1, coalesce(Contact_us," ") as Contact_us1, coalesce(Icon_text_color," ") as Icon_text_color1, coalesce(fun_top_background_color," ") as fun_top_background_color1, coalesce(fun_top_text_color," ") as fun_top_text_color1, coalesce(fun_footer_background_color," ") as fun_footer_background_color1, coalesce(fun_block_background_color," ") as fun_block_background_color1, coalesce(fun_block_text_color," ") as fun_block_text_color1, coalesce(theme_color," ") as theme_color1, coalesce(theme_color," ") as theme_color1',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        //$this->db->join('mobile_data m', 'm.uid = u.Id');
        $this->db->where('u.token', $token);
        $this->db->where('u.email', $email);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function check_event_with_id($event_id,$user_id=0)
    {
        
         $this->db->select('e.*,CASE WHEN e.Description IS NULL THEN "" ELSE e.Description END as Description,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.Logo_images IS NULL THEN "" ELSE e.Logo_images END as Logo_images,CASE WHEN e.Background_img IS NULL THEN "" ELSE e.Background_img END as Background_img,CASE WHEN e.Background_color IS NULL THEN "#FFFFFF" ELSE e.Background_color END as Background_color,CASE WHEN e.Top_background_color IS NULL THEN "#FFFFFF" ELSE e.Top_background_color END as Top_background_color,CASE WHEN e.Top_text_color IS NULL THEN "#FFFFFF" ELSE e.Top_text_color END as Top_text_color,CASE WHEN e.Icon_text_color IS NULL THEN "#FFFFFF" ELSE e.Icon_text_color END as Icon_text_color,CASE WHEN e.Footer_background_color IS NULL THEN "#FFFFFF" ELSE e.Footer_background_color END as Footer_background_color,CASE WHEN e.fun_logo_images IS NULL THEN "" ELSE e.fun_logo_images END as fun_logo_images,CASE WHEN e.fun_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_background_color END as fun_background_color,CASE WHEN e.fun_top_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_background_color END as fun_top_background_color,CASE WHEN e.fun_top_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_text_color END as fun_top_text_color,CASE WHEN e.fun_footer_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_footer_background_color END as fun_footer_background_color,CASE WHEN e.fun_block_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_background_color END as fun_block_background_color,CASE WHEN e.fun_block_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_text_color END as fun_block_text_color,CASE WHEN e.theme_color IS NULL THEN "#FFFFFF" ELSE e.theme_color END as theme_color,CASE WHEN e.Contact_us IS NULL THEN "" ELSE e.Contact_us END as Contact_us,CASE WHEN e.checkbox_values IS NULL THEN "" ELSE e.checkbox_values END as checkbox_values,CASE WHEN e.fun_header_status IS NULL THEN "" ELSE e.fun_header_status END as fun_header_status,CASE WHEN e.secure_key IS NULL THEN "" ELSE e.secure_key END as secure_key,ru.Role_id',
            FALSE);
            $this->db->from('event e');
            $this->db->where('e.Id', $event_id);
            $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
            $this->db->join('user u', 'u.Id = ru.User_id');
           
            if($user_id != 0)
            {
                $this->db->where('ru.User_id', $user_id);    
            }
            $this->db->group_by("e.Id");
            $query = $this->db->get();
            $res = $query->result_array();
            if($res[0]['Role_id'] == '4')
            {
               $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
               $extra=json_decode($extra['extra_column'],true);
               foreach ($extra as $key => $value) {
                  $keyword="{".str_replace(' ', '', $key)."}";
                  if(stripos(strip_tags($res[0]['Description'],$keyword)) !== false)
                  {
                    $res[0]['Description']=str_ireplace($keyword, $value,$res[0]['Description']);
                  }
                }
            }
            return $res;
    }
    
    public function check_user_email($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $data['email']);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    
    public function get_site_setting()
    {
        $this->db->select('*');
        $this->db->from('settings');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getFormBuillderDataByEvent($event_id)
    {
        $this->db->select('*');
        $this->db->from('signup_form');
        $this->db->where('event_id',$event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function signup($email,$firstname,$last_name,$password,$country_id,$title,$company_name,$event_id,$org_id,$device)
    {
        $data['Firstname']=$firstname;
        $data['Lastname']=$last_name;
        $data['Email']=$email;
        $data['Password']=md5($password);
        $data['Country']=$country_id;
        $data['Organisor_id ']=$org_id;
        $data['Active']='1';
        $data['device']=$device;
        if($title!='')
        $data['Title']=$title;
        if($company_name!='')
        $data['Company_name']=$company_name;
        $data['token'] = sha1($email.$password.$event_id);
        $where['Email']=$email;
        $user = $this->db->select('*')->from('user')->where($where)->get()->row_array();
        if(!empty($user))
        {
            return $user['Id'];
        }
        $this->db->insert("user",$data);
        $user_id=$this->db->insert_id();
        return $user_id;
    }
    public function update_token($user_id,$data)
    {
        $this->db->where("Id",$user_id);
        $this->db->update("user",$data);
    }
    public function getOrganizerByEvent($event_id)
    {
        $this->db->select('Organisor_id');
        $this->db->from('relation_event_user');
        $this->db->where('Event_id',$event_id);
        $this->db->where('Role_id',3);
        $query = $this->db->get();
        $res = $query->result_array();
        $org_id=$res[0]['Organisor_id'];
        return $org_id;
    }
    public function setupUserWithEvent($user_id,$event_id,$org_id)
    {
        $data['Event_id']=$event_id;
        $data['User_id']=$user_id;
        $data['Organisor_id']=$org_id;
        $data['Role_id']=4;
        $this->db->insert("relation_event_user",$data);
        $event_data['Event_id'] = $event_id;
        $event_data['Attendee_id'] = $user_id;
        $event_data['views_id'] = 1;
        $this->db->insert("event_attendee",$event_data);
    }
    public function addFormBuilderData($user_id,$form_data)
    {
        $data['user_id']=$user_id;
        $data['json_data']=$form_data;
        $this->db->insert("signup_form_data",$data);
    }
   /* public function updateGCMID($user_id,$gcm_id)
    {
        $data['gcm_id']=$gcm_id;
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->where('u.gcm_id',$gcm_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $ids = array_map(function ($ar) { return $ar['Id']; }, $res);
        $str=implode(',', $ids);
        if(count($res)>0)
        {
            $sql="update user set gcm_id='' WHERE Id IN ($str);";
            $query = $this->db->query($sql);
        }
        $this->db->where("Id",$user_id);
        $this->db->update("user",$data);    
        $user_details=$this->getUserDetailsId($user_id);
        return $user_details;

    }*/
    public function updateGCMID($user_id,$gcm_id,$device)
    {
        $user = $this->db->select('*')->from('user')->where('Id',$user_id)->get()->row_array();
        if($user['gcm_id']!=$gcm_id)
        {
            if($user['gcm_id']!='')
            {
                $this->load->library('Gcm');
                $obj = new Gcm();
                $extra['message_type'] = 'updateGCMID';
                $extra['message_id'] = $user['Id'];
                if($user['device'] == 'Iphone')
                    $result = $obj->send_notification($user['gcm_id'],'You have successfully logged out',$extra,$user['device']);
                else
                {
                    $message['title']       = "Test";
                    $message['message']     = "test";
                    $message['vibrate']     = 1;
                    $message['sound']       = 0;
                    $result = $obj->send_notification($user['gcm_id'],$message,$extra,$user['device']);
                }
            }
        }
        $data['gcm_id']=$gcm_id;
        $data['device']=$device;
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->where('u.gcm_id',$gcm_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $ids = array_map(function ($ar) { return $ar['Id']; }, $res);
        $str=implode(',', $ids);
        if(count($res)>0)
        {
            $sql="update user set gcm_id='' WHERE Id IN ($str);";
            $query = $this->db->query($sql);
        }
        $this->db->where("Id",$user_id);
        $this->db->update("user",$data);    
        $user_details=$this->getUserDetailsId($user_id);
        return $user_details;

    }
    public function linkedInSignup($email,$facebook_id,$event_id,$firstname,$last_name,$img,$device,$company_name,$title)
    {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu','reu.User_id = tu.Id','left');
        $this->db->where("reu.Event_id",$event_id);
        $this->db->where("reu.Role_id",3);
        $query = $this->db->get();
        $res = $query->result_array();
        
        $accname = $res[0]['acc_name'];
        $this->db->select('Id,Organisor_id');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $eventid=$res1[0]['Id'];
        $Organisor_id=$res1[0]['Organisor_id'];
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email',$email);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        if(count($res1)==0)
        {
            $data['Email']=$email;
            $data['Firstname']=$firstname;
            $data['Lastname']=$last_name;
            $data['acc_name']=$accname;
            $data['Logo']=$img;
            $data['Company_name']=$company_name;
            $data['device']=$device;
            $data['Title']=$title;
            $data['attendee_flag']='0';
            $data['token'] = sha1($email.$password.$event_id);
            $chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $auto_password=substr(str_shuffle( $chars ), 0, 6 );
            $data['Password'] = md5($auto_password);
            $data['Active']='1';
            $data['Organisor_id']=$Organisor_id;
            $this->db->insert("user",$data);
            $user_id=$this->db->insert_id();
            $relation_event_user['Event_id']=$eventid;
            $relation_event_user['User_id']=$user_id;
            $relation_event_user['Role_id']=4;
            $relation_event_user['Organisor_id']=$Organisor_id;
            // send mail to first time facebook user login
            $this->load->library('email');
            
            /*$config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = ""; 
            $config['smtp_pass'] = "";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "text/html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);*/


            $this->email->from('info@eventmteam.com','Event Organizor');
            $this->email->subject('EventApp Auto-generated Password');
            $this->email->to($email);
            $this->email->set_mailtype("html");
            $message="Hello ".$firstname." ".$last_name .","."<br>"."Your Auto-generated EventApp password is ".$auto_password;
            $this->email->message($message);
            $this->email->send();

            $this->db->insert("relation_event_user  ",$relation_event_user);
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid');
            $this->db->from('user');
            $this->db->where('user.Id',$user_id);
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->limit(1);
            $query1 = $this->db->get();
            $result_user = $query1->row_array();
            array_walk($result_user,function(&$item){$item=strval($item);});

           // return $result_user;
            
        }
        else
        {
            $up['Logo'] = $img;
            $this->db->where('Email',$email);
            $this->db->update('user',$up);

           $user_id=$res1[0]['Id'];
           $this->db->select('*')->from('relation_event_user');
           $this->db->where('Event_id',$eventid);
           $this->db->where('User_id',$user_id);
           $this->db->where('Organisor_id',$Organisor_id);
           $qu=$this->db->get();
           $res=$qu->result_array();
           if(count($res)==0){
                $rel['Event_id']=$eventid;
                $rel['User_id']=$user_id;
                $rel['Organisor_id']=$Organisor_id;
                $rel['Role_id']=4;
                $this->db->insert('relation_event_user',$rel);
           }
           $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid');
            $this->db->from('user');
            $this->db->where('user.Id',$user_id);
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->limit(1);
            $query1 = $this->db->get();
            $result_user = $query1->row_array();
            
        }
        $result = $this->getUserDetailsId($user_id)  ;
        return $result;
    }
    public function saveSocialLinks($user_id,$social)
    {
        $this->db->where('User_id',$user_id);
        $this->db->update('user_social_links',$social);
    }
}
?>
