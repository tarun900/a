<?php
class Checkin_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);

    }
    public function getAttendeeListByEventId($Event_id)
    {
       
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();  
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name']=$res[$i]->Company_name;
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
            $is_checked_in = $this->db->select('*')->from('event_attendee')->where('Event_id',$Event_id)->where('Attendee_id',$res[$i]->Id)->get()->row_array();
            $attendees[$i]['is_checked_in']=(!empty($is_checked_in)) ? $is_checked_in['check_in_attendee'] : '0';
        }
        return $attendees;
    } 
    public function getAttendeeDetails($attendee_id)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('user u');
        $this->db->where('u.Id',$attendee_id);
        $res = $this->db->get()->row_array();
        return $res;
    }
    public function saveCheckIn($data)
    {
        $result = $this->db->select('*')->from('event_attendee')->where($data)->get()->row_array();
        if(count($result))
        {
            $flag=($result['check_in_attendee']=='0') ? 1 : 0;
            $this->db->where($data);
            $update['check_in_attendee']=($result['check_in_attendee']=='0') ? '1' : '0';
            $this->db->update('event_attendee',$update);
        }
        else
        {
            $flag=1;
            $data['check_in_attendee']='1';
            $this->db->insert('event_attendee',$data);
        }
        return $flag;
    }
    public function getCustomInfo($where)
    {
        $custom_coulmns = $this->db->select('*')->from('custom_column')->where('event_id',$where['Event_id'])->order_by('column_id')->get()->result_array();
        $extra_info = $this->db->select('*')->from('event_attendee')->where($where)->get()->row_array();
        $extra_value =json_decode($extra_info['extra_column'],TRUE);
        foreach ($custom_coulmns as $key => $value) {
            $val = $extra_value[$value['column_name']];
            $data['key'] = $value['column_name'];
            $data['value'] = ($val) ? $val : '';
            $result[] = $data;
        }
        return $result;
    }
    public function saveAttendeeDetails($personal_info,$attendee_id)
    {
        $this->db->where('Id',$attendee_id);
        $this->db->update('user',$personal_info);
    }
    public function saveCustomInfo($custom_info,$where)
    {
        $this->db->where($where);
        $this->db->update('event_attendee',$custom_info);
    }
}
        
?>
