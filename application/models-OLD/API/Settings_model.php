<?php 
class settings_model extends CI_Model{
    function __construct()
    {  
        parent::__construct();
    }

    public function getNotesHeader($event_id){
        $this->db->select('checkbox_values')->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }
    public function event_name($event_id)
    {
        $this->db->select('Event_name')->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();
        $result = $query->row_array(); 
        return $result['Event_name'];
    }
    public function is_notification_read($event_id,$menu_id,$user_id)
    {
        $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        date_default_timezone_set("UTC");
        $cdate=date('Y-m-d H:i:s');
        if(!empty($event['Event_show_time_zone']))
        {
            if(strpos($event['Event_show_time_zone'],"-")==true)
            { 
              $arr=explode("-",$event['Event_show_time_zone']);
              $intoffset=$arr[1]*3600;
              $intNew = abs($intoffset);
              $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
            }
            if(strpos($event['Event_show_time_zone'],"+")==true)
            {
              $arr=explode("+",$event['Event_show_time_zone']);
              $intoffset=$arr[1]*3600;
              $intNew = abs($intoffset);
              $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
            }
        }
        //echo $cdate;exit;
        $this->db->select('n.Id,n.title,n.content,n.datetime,n.event_id,n.moduleslink,n.notification_type,un.id, un.user_id as visited_user_id')->from('notification n');
        $this->db->join('user_module_notification un','un.notification_id=n.id AND un.menu_id = '.$menu_id, 'left');
        $this->db->where("FIND_IN_SET('$menu_id',n.moduleslink) != 0",NULL,FALSE);
        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0",NULL,FALSE);
        $this->db->or_where("FIND_IN_SET('$user_id',un.user_id) IS NULL)",NULL,FALSE);
        $this->db->where('n.event_id',$event_id);
        $this->db->where("(n.datetime IS NULL",NULL, FALSE);
        $this->db->or_where("n.datetime <= '".$cdate."')",NULL,FALSE);
        $notify_query = $this->db->get()->result_array();
        foreach($notify_query as $key => $value)
        {
            if(empty($value['id']))
            {
                $data = array(
                    'notification_id' => $value['Id'],
                    'user_id' => $user_id,
                    'menu_id' => $menu_id
                );
                
                $this->db->insert('user_module_notification',$data);
            }
            else
            {
                if(empty($value['visited_user_id']))
                {
                    $str = $value['visited_user_id'];
                }
                else
                {
                    $str = $value['visited_user_id'].','.$user_id;
                }
                
                $update_data = array(
                    'user_id' => $str
                );        
                $this->db->where('id', $value['id']);
                $this->db->update('user_module_notification', $update_data);
            }
        }
        
        return ($notify_query);
        /*$this->db->select('n.Id,n.title,n.content,n.datetime,n.event_id,n.moduleslink,n.notification_type,un.id, un.user_id as visited_user_id')->from('notification n');
        $this->db->join('user_module_notification un','un.notification_id=n.id AND un.menu_id = '.$menu_id, 'left');
        $this->db->where("FIND_IN_SET('$menu_id',n.moduleslink) != 0",NULL,FALSE);
        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0",NULL,FALSE);
        $this->db->or_where("FIND_IN_SET('$user_id',un.user_id) IS NULL)",NULL,FALSE);
        $this->db->where('n.event_id',$event_id);
        $this->db->where("(n.datetime IS NULL",NULL, FALSE);
        $this->db->or_where("n.datetime <= '".date('Y-m-d H:i:s')."')",NULL,FALSE);
        $notify_query = $this->db->get()->result_array();
        //echo $this->db->last_query();die;
        foreach($notify_query as $key => $value)
        {
            if(empty($value['id']))
            {
                $data = array(
                    'notification_id' => $value['Id'],
                    'user_id' => $user_id,
                    'menu_id' => $menu_id
                );
                
                $this->db->insert('user_module_notification',$data);
            }
            else
            {
                if(empty($value['visited_user_id']))
                {
                    $str = $value['visited_user_id'];
                }
                else
                {
                    $str = $value['visited_user_id'].','.$user_id;
                }
                
                $update_data = array(
                    'user_id' => $str
                );        
                $this->db->where('id', $value['id']);
                $this->db->update('user_module_notification', $update_data);
            }
        }
        
        return $notify_query;*/
    }
    public function getGcmId($event_id){
        $this->db->select('group_concat(gcm_id) as gcm_ids',false)->from('user');
        $this->db->join('relation_event_user ru','ru.User_id = user.Id');
        $this->db->where('ru.Event_id',$event_id);
        $query = $this->db->get();
        $res1 = $query->row();
        return explode(',', $res1->gcm_ids);
    }
     public function getDevice($user_id){
       $this->db->select('device');
       $this->db->from('user');
       $this->db->where('Id',$user_id);
       $query = $this->db->get();
       $result = $query->row();
       return $result->device;
    }

    public function getAdvertisingData($event_id,$menu_id)
    {
       $this->db->select('*,CASE WHEN H_images is NULL THEN "" ELSE H_images END as H_images,CASE WHEN F_images is NULL THEN "" ELSE F_images END as F_images, CASE WHEN Header_link is NULL THEN "" ELSE Header_link END as Header_link,CASE WHEN Footer_link is NULL THEN "" ELSE Footer_link END as Footer_link,CASE WHEN Cms_id IS NULL THEN "" ELSE Cms_id END as Cms_id,CASE WHEN Google_header_adsense IS NULL THEN "" ELSE Google_header_adsense END as Google_header_adsense,CASE WHEN Google_footer_adsense IS NULL THEN "" ELSE Google_footer_adsense END as Google_footer_adsense',false)->from('advertising');
       $this->db->where('Event_id',$event_id);
       $this->db->where("FIND_IN_SET('$menu_id',Menu_id) != 0",NULL,FALSE);
       $query = $this->db->get();
       $result = $query->row();
       return $result;
       
    }
    public function getVersionCode($where,$device)
    {
        $data = $this->db->select('*')->from('version_code')->where($where)->get()->row_array();
        return $data[$device.'_code'];
    }
    public function getNotification($add_id,$user_id,$gcm_id)
    {
        $this->db->select('n.Id,n.title,n.content,n.datetime,n.event_id,n.moduleslink,n.notification_type,un.id, un.user_id as visited_user_id')->from('notification n');
        $this->db->join('user_module_notification un','un.notification_id=n.id ', 'left');
        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0",NULL,FALSE);
        $this->db->or_where("FIND_IN_SET('$user_id',un.user_id) IS NULL)",NULL,FALSE);
        $this->db->where('n.Id',$add_id);
        $this->db->where("n.datetime IS NULL",NULL, FALSE);
        $value = $this->db->get()->row_array();
        
        if(empty($value['id']))
        {
            $data = array(
                'notification_id'   => $add_id,
                'user_id'           => $user_id,
            );
            
            $this->db->insert('user_module_notification',$data);
        }
        else
        {
            if(empty($value['visited_user_id']))
            {
                $str = $value['visited_user_id'];
            }
            else
            {
                $str = $value['visited_user_id'].','.$user_id;
            }
            
            $update_data = array(
                'user_id' => $str,

            );        
            $this->db->where('id', $value['id']);
            $this->db->update('user_module_notification', $update_data);
        }
       
       
        return ($value);
    }
    public function getAllUsersGCM($event_id)
    {
        return $this->db->select('u.*')->from('relation_event_user reu')->join('user u','u.Id = reu.User_id')->where('reu.Event_id',$event_id)->get()->result_array();
    }
    public function getAllUsers()
    {
        return $this->db->select('user.*,reu.Id as event_id')->from('user')->join('relation_event_user reu','reu.User_id = user.Id')->where('gcm_id IS NOT NULL', NULL, FALSE)->where('gcm_id !=','')->group_by('user.Id')->limit(5)->get()->result_array();
    }
    public function getSceduledNotification($user_id,$gcm_id,$event_id)
    {
        $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        date_default_timezone_set("UTC");
        $cdate=date('Y-m-d H:i:s');
        if(!empty($event['Event_show_time_zone']))
        {
            if(strpos($event['Event_show_time_zone'],"-")==true)
            { 
              $arr=explode("-",$event['Event_show_time_zone']);
              $intoffset=$arr[1]*3600;
              $intNew = abs($intoffset);
              $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
            }
            if(strpos($event['Event_show_time_zone'],"+")==true)
            {
              $arr=explode("+",$event['Event_show_time_zone']);
              $intoffset=$arr[1]*3600;
              $intNew = abs($intoffset);
              $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
            }
        }
        $this->db->select('n.Id,n.title,n.content,n.datetime,n.event_id,n.moduleslink,n.notification_type,un.id, un.user_id as visited_user_id')->from('notification n');
        $this->db->join('user_module_notification un','un.notification_id=n.id ', 'left');

        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0",NULL,FALSE);
        $this->db->or_where("FIND_IN_SET('$user_id',un.user_id) IS NULL)",NULL,FALSE);
        $this->db->where('n.event_id',$event_id);
        $this->db->where("n.datetime <= '".$cdate."'",NULL,FALSE);
        $notification = $this->db->get()->result_array();
        
        foreach ($notification as $key => $value) 
        {
            if(empty($value['id']))
            {
                $data = array(
                    'notification_id' => $value['Id'],
                    'user_id' => $user_id,
                );
                
                $this->db->insert('user_module_notification',$data);
            }
            else
            {
                if(empty($value['visited_user_id']))
                {
                    $str = $value['visited_user_id'];
                }
                else
                {
                    $str = $value['visited_user_id'].','.$user_id;
                }
                
                $update_data = array(
                    'user_id' => $str,

                );        
                $this->db->where('id', $value['id']);
                $this->db->update('user_module_notification', $update_data);
            }
        }
        return ($notification);
    }
    public function hitUserClickBoard($where)
    {
        $data = $this->db->select('*')->from('user_click_board')->where($where)->get()->row_array();
        if(count($data))
        {
            $update_data['click_hit'] = $data['click_hit'] + 1;
            $this->db->where($where);
            $this->db->update('user_click_board',$update_data);
        }
        else
        {
            $where['click_hit'] = 1;
            $this->db->insert('user_click_board',$where);
        }
    }
    public function hitUserLeaderBoard($where,$hit)
    {
        $data = $this->db->select('*')->from('users_leader_board')->where($where)->get()->row_array();
        if(count($data))
        {
            $update_data[$hit] = $data[$hit] + 1;
            $this->db->where($where);
            $this->db->update('users_leader_board',$update_data);
        }
        else
        {
            $where[$hit] = 1;
            $this->db->insert('users_leader_board',$where);
        }
    }
    public function getOranizerData($event_id)
    {
        return $this->db->select('u.*')->from('user u')->join('event e','e.Organisor_id = u.Id')->where('e.Id',$event_id)->get()->row_array();
    }
    public function getAllUsersGCMN($event_id)
    {
        return $this->db->select('u.*')->from('relation_event_user reu')->join('user u','u.Id = reu.User_id')->where('reu.Event_id',$event_id)->get()->result_array();
    }
}
        
?>