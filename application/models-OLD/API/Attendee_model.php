<?php
class Attendee_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);

    }
    public function getAttendeeListByEventId($Event_id,$user_id)
    {
       
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('e.Id',$Event_id);
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();  
        $attendees = array();
        for($i=0; $i<count($res); $i++)
        {
            $attendees[$i]['Id']=$res[$i]->Id;
            $attendees[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name']=$res[$i]->Company_name;
            $attendees[$i]['Title']=$res[$i]->Title;
            $attendees[$i]['Email']=$res[$i]->Email;
            $attendees[$i]['Logo']=$res[$i]->Logo;
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('module_type','2')->where('user_id',$user_id)->where('module_id',$res[$i]->Id)->get()->num_rows();
            $attendees[$i]['is_favorites']=($count) ? '1' : '0';
           
        }
        return $attendees;
    } 
    public function getAttendeeDetails($attendee_id)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',FALSE);
        $this->db->from('user u');
        $this->db->where('u.Id',$attendee_id);
        $query = $this->db->get();
        $res = $query->result();  
        return $res;
    }

    public function getSocialLinks($attendee_id)
    {
        $this->db->select('CASE WHEN Facebook_url IS NULL THEN "" ELSE Facebook_url END as Facebook_url,CASE WHEN Twitter_url IS NULL THEN "" ELSE Twitter_url END as Twitter_url,CASE WHEN Linkedin_url IS NULL THEN "" ELSE Linkedin_url END as Linkedin_url',FALSE);
        $this->db->from('user_social_links');
        $this->db->where('User_id',$attendee_id);
        $query = $this->db->get();
        $res = $query->result();  
        return $res;
    }
    public function getContactAttendeeListByEventId($event_id,$user_id=0)
    {
        $user_id = ($user_id=='') ? '0' : $user_id;
        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc2',"asc2.to_id=u.Id AND asc2.from_id=".$user_id." AND asc2.approval_status!='2'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc2.event_id=".$event_id.")";
        $this->db->where($wher);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname');
        $aquery = $this->db->get();
        $ares1 = $aquery->result();

        $this->db->select('*,u.Id as uid',false)->from('user u');
        $this->db->join('attendee_share_contact asc1',"asc1.from_id=u.Id AND asc1.to_id=".$user_id." AND asc1.approval_status='1'",'left');
        $this->db->where('u.Id !=',$user_id);
        $wher="(asc1.event_id=".$event_id.")";
        $this->db->where($wher);
        $this->db->group_by('u.Id');
        $this->db->order_by('u.Lastname');
        $aquery = $this->db->get();
        $ares2 = $aquery->result();
        $ares = array_merge($ares1,$ares2);
        $attendees = array();
        for($i=0; $i<count($ares); $i++)
        {
            $prev="";
            $fc = strtoupper($ares[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if($prev=="" || $ares[$i]->Lastname!=$prev)
            {
                $prev=$ares[$i]->Start_date;
                $data['Id']=$ares[$i]->uid;
                $data['Firstname']=$ares[$i]->Firstname;
                $data['Lastname']=$ares[$i]->Lastname;
                $data['Company_name']=$ares[$i]->Company_name;
                $data['Title']=$ares[$i]->Title;
                $data['Email']=$ares[$i]->Email;
                $data['Logo']=$ares[$i]->Logo;
                $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('module_type','2')->where('user_id',$user_id)->where('module_id',$res[$i]->Id)->get()->num_rows();
                $data['is_favorites']=($count) ? '1' : '0';
                array_walk($data,function(&$item){$item=strval($item);});
                $attendees[] = $data;
            }
        }
        return $attendees;
    }
    public function getApprovalStatus($where,$event_id)
    {
        $test = '2';
        $result = $this->db->select('*')->from('attendee_share_contact')->where($where)->where('event_id',$event_id)->get()->row_array();
       
        $result2 = $this->db->select('*')->from('attendee_share_contact')->where('from_id',$where['to_id'])->where('to_id',$where['from_id'])->where('event_id',$event_id)->get()->row_array();
           
        if(!empty($result2))
        {
            if($result2['approval_status'] == '0')
                $test = '3';
            else
                $test = '1';
        }

        return (!empty($result)) ? $result['approval_status'] : $test;
    }
    public function saveShareContactInformation($where,$share_data)
    {
       // print_r($share_data);exit;
        $result = $this->db->select('*')->from('attendee_share_contact')->where($where)->get()->row_array();
        if(!count($result))
        {
            $share_data['share_contact_datetime']=date('Y-m-d H:i:s');
            $this->db->insert('attendee_share_contact',$share_data);
            $contact_id = $this->db->insert_id();
        }
        else
        {
            $this->db->where($where);
            $this->db->update('attendee_share_contact',$share_data);
            $contact_id=$result['contact_id'];
        }
        return $contact_id;
    }
 
  
    public function getShareDetails($where,$event_id)
    {
        $attenddess = $this->db->select('*')->from('attendee_share_contact')->where($where)->where('event_id',$event_id)->where('approval_status','0')->get()->result_array();

        foreach ($attenddess as $key => $value) {
          $user = $this->db->select('u.Id as attendee_id,u.Email,u.Country,u.Mobile as phone_no')->from('user u')->where('u.Id',$value['from_id'])->get()->row_array();
          $country = $this->db->select('*')->from('country')->where('id',$user['Country'])->get()->row_array();

          $data[$key]['attendee_id'] = $user['attendee_id'];
          $data[$key]['Email'] = $user['Email'];
          $data[$key]['country_name'] = ($country['country_name']) ? $country['country_name'] : '';
          $data[$key]['phone_no'] = ($user['phone_no']) ? $user['phone_no'] : '';
        }
         
        return ($data) ? $data : [];
    }
    public function getAttendeeConatctDetails($attendee_id)
    {
       $data = $this->db->select('Email,Mobile,Id,Country')->from('user')->where('Id',$attendee_id)->get()->row_array();
       $country = $this->db->select('country_name')->from('country')->where('id',$data['Country'])->get()->row_array();
       $data['Country'] = $country['country_name'];
       array_walk($data,function(&$item){$item=strval($item);});
       $array[] = $data;
       return $array;
    }

    public function getEventName($event_id)
    {
        $data = $this->db->select('Event_name')->from('event')->where('Id',$event_id)->get()->row_array();
        return $data['Event_name'];
    }
    public function getDateTimeArray($event_id)
    {
        $data = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();

        $start = date('Y-m-d',strtotime($data['Start_date']));
        $time1 = date('H:i',strtotime($data['Start_time']));
        $time2 = date('H:i',strtotime($data['End_time']));
        while($start <= $data['End_date'])
        {
            $date[] = date('m-d-Y',strtotime($start));
            $start = date('Y-m-d',strtotime($start.'+1 day'));
        }
        $time=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
     
        foreach ($time as $key => $value) {
           if((date('H:i',$value)) == "00:00")
               continue;
           $time_arr[] = date('H:i',$value);
        }
        $result['date'] = $date;
        $result['time'] = $time_arr;
        return $result;
    }
    public function getAllMeetingRequest1($event_id,$user_id)
    {
        $data = $this->db->select('e.Id')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('u.token',$user_id)->where('e.event_id',$event_id)->get()->row_array();

        return $this->db->select('u.Firstname,u.Lastname,eam.date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id')->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
    }
    public function getAllMeetingRequest($event_id,$user_id)
    {
       return  $this->db->select('CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
        eam.date,eam.time,eam.status,eam.Id as request_id, CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.exhibiotor_id END as exhibiotor_id,
        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false)->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.recever_attendee_id',$user_id)->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
       
    }
    public function updateRequest($update_data,$where)
    {
        $this->db->where($where);
        $this->db->update('exhibitor_attendee_meeting',$update_data);
        // pending
        /*if($update_data['status'] == '1')
        {
            $data['event_id'] = $ex['event_id'];
            $data['user_id'] = $ex['attendee_id'];
            $data['modules_id'] = '0';
            $data['reminder_time'] = '15';
            $data['read_status'] = '0';
            $data['created_date'] = date('Y-m-d H:i:s');

            $this->db->insert('user_reminder',$data);
        }*/
        return $this->db->select('*')->from('exhibitor_attendee_meeting')->where($where)->get()->row_array();
    }
    public function getExhibitor($id)
    {
        return $this->db->select('*')->from('exibitor')->where('Id',$id)->get()->row_array();
    }
    public function getUsersData($id)
    {
        return $this->db->select('*')->from('user')->where('Id',$id)->get()->row_array();
    }  
    public function getUserIdByToken($token)
    {
        $token = ($token == NULL) ? '' : $token;
        $user = $this->db->select('Id')->from('user')->where('token',$token)->get()->row_array();
        return $data['Id'];
    }
    public function saveSuggestedDate($suggest)
    {
        $this->db->insert('suggest_meeting',$suggest);
    }
    public function getSuggestUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Exhibitors/'.$data['acc_name'].'/'.$data['Subdomain'].'/';
    }
    public function getAvailableTimes($where)
    {
        return $this->db->select('sm.Id,date_time,concat(Firstname," ",  Lastname) as Heading',false)->from('suggest_meeting sm')->join('user u','u.Id = sm.recever_user_id')->where($where)->get()->result_array();
    }
    public function bookSuggestedTime($where,$date_time)
    {
        $result = $this->db->select('*')->from('suggest_meeting')->where($where)->get()->row_array();   
        if(!empty($result))
        {
            $exhibitor = $this->getUser($result['recever_user_id']);   
            
            $update['recever_attendee_id'] = $exhibitor['Id'];
            $update['attendee_id'] = $where['attendee_id'];
            $update['event_id'] = $where['event_id'];
            $update['date'] = date('Y-m-d',strtotime($date_time));
            $update['time'] = date('H:i:s',strtotime($date_time));
            $update['status'] = '1';

            $this->db->where('Id',$result['meeting_id']);
            $this->db->update('exhibitor_attendee_meeting',$update);

            $where1['attendee_id'] = $where['attendee_id'];
            $where1['event_id'] = $where['event_id'];
            $this->db->where($where1);
            $this->db->delete('suggest_meeting');

            return $this->db->select('*')->from('user')->where('Id',$result['recever_user_id'])->get()->row_array();
        }
    }
    public function getAttenee($id)
    {
        return $this->db->select('u.*')->from('user u')->join('exhibitor_attendee_meeting eam','eam.attendee_id = u.Id')->where('eam.Id',$id)->get()->row_array();
    }
    public function saveRequest($data)
    {
        $count = $this->db->select('*')->from('exhibitor_attendee_meeting')->where($data)->get()->num_rows();
        if(!$count)
        {
            $data['created_datetime']=date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_attendee_meeting',$data);
            return true;
        }
        else
        {
            return false;
        }
    } 
    public function checkSessionClash($attendee_id,$date,$time)
    {
        $result['result'] = true;
        $data = $this->db->select('*')->from('users_agenda')->where('user_id',$attendee_id)->get()->row_array();
        $agenda_ids = explode(',',$data['agenda_id']);
        $agenda = $this->db->select('*')->from('agenda')->where_in('Id',$agenda_ids)->get()->result_array();
        $d_date = strtotime($date);
        $time = strtotime($time);
        foreach ($agenda as $key => $value) {
           $start_date = strtotime($value['Start_date']);
           $end_date = strtotime($value['End_date']);
           $start_time = strtotime($value['Start_time']);
           $end_time = strtotime($value['End_time']);
           if($d_date >= $start_date && $d_date <= $end_date)
           {
                $result['result'] = false;
                $result['agenda_name'] = $value['Heading'];
                break;
           }
        }
        return $result;
    }  
    public function getUrlData($event_id)
    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u','u.Id = e.Organisor_id');
        $this->db->where('e.Id',$event_id);
        $data = $this->db->get()->row_array();
        return 'Exhibitors/'.$data['acc_name'].'/'.$data['Subdomain'].'/checpendingkmetting/';
    }
    public function getUser($user_id)
    {
        return $this->db->select('u.*')->from('user u')->where('u.Id',$user_id)->get()->row_array();
    }
    public function saveSpeakerMessage($data)
    {
        $this->db->insert("speaker_msg",$data);
        $message_id=$this->db->insert_id();
        if($message_id!='')
        {

            $res=$this->notifypublic($data['Event_id'],$data['Sender_id']);
            foreach ($res as $key=>$values)
            {
               $data1 = array(
                       'msg_id' => $message_id,
                       'resiver_id' => $values,
                       'sender_id' => $data['Sender_id'],
                       'event_id' => $data['Event_id'],
                       'isread' => '1',
                       'type' => '0',
               );
               $this->db->insert('msg_notifiy', $data1);
            }
            return $message_id;
        }
        else
        {
            return 0;
        }
    }
    public function notifypublic($eventid,$user_id)
    {    
          
       
          $this->db->select('group_concat(ru.User_id) as User_id');
          $this->db->from('relation_event_user ru');
          $this->db->where('ru.Event_id', $eventid);
          $this->db->where('ru.User_id !=',$user_id);
          $query = $this->db->get();
          $res1 = $query->result_array();
          
          $finalarray=array();
          if($res1[0]['User_id']!="")
          {
               $finalarray=  explode(',', $res1[0]['User_id']);
               $finalarray=  array_unique($finalarray);
          }
          return $finalarray;
    }
}
        
?>
