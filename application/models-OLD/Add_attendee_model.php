<?php class Add_attendee_model extends CI_Model{
	function __construct()
	{
	    parent::__construct();
        $this->db1 = $this->load->database('db1', TRUE);
        $this->load->library('session');
        $this->load->model('User_model');
	}

	public function get_user_list($id=null)
	{
        $orid = $this->data['user']->Id;
    	$this->db->select('*,r.Id as Rid,u.Id as uid,u.Active,r.role_type,r.ask_survey,r.add_survey');
    	$this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
    	if($user[0]->Id)
    	{
    	   $this->db->where('u.Id',$user[0]->Id);
    	}
        $this->db->where('r.Name','User');
        $this->db->or_where('r.Name','Manager');
        $this->db->or_where('r.Name','Marketing');
        $this->db->or_where('r.Name','Social_media_manager');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
	    $res = $query->result_array();
	    return $res;
	}
    public function add_user_form_data($form_data)
    {
          $this->db->insert('signup_form_data',$form_data);
         
    }
    public function add_user($array_add,$role_flag)
    {
        $agenda_id=$array_add['agenda_id'];
        $extra_column=$array_add['extra_column'];
        unset($array_add['extra_column']);
        unset($array_add['agenda_id']);
        $Subdomain = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $result1 = $query->result_array();

        $this->db->select('*');
        $this->db->from('role');
        if($role_flag==1)
        {
            $this->db->where('Id',6);
      
        }
        else
        {
            $this->db->where('Id',4);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        
        $userinfo = $this->User_model->get_user_by_email($array_add['Email']);
        if(empty($userinfo))
        {
           if($result1[0]['Id']=='447')
           {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $array_add['Password'] = $rc4->encrypt($array_add['Password']);
           }
           else
           {
                $array_add['Password'] = md5($array_add['Password']);
           }
           unset($array_add['Event_id']);
           $this->db->insert('user',$array_add);
           $user_id = $this->db->insert_id();
        }
        else
        {
            $user_id=$userinfo[0]['Id'];
            if(empty($userinfo[0]['Company_name']))
            {
                $uuser['Company_name']=$array_add['Company_name'];
            }
            if(empty($userinfo[0]['Title']))
            {
                $uuser['Title']=$array_add['Title'];
            }
            if(count($uuser) > 0)
            {
                $this->db->where('Id',$user_id);
                $this->db->update('user',$uuser);
            }
        }
        if($role_flag!=1)
        {
            $this->db->select('*')->from('user_views');
            $this->db->where('event_id',$result1[0]['Id']);
            $this->db->where('view_type','1');
            $vqu=$this->db->get();
            $vres=$vqu->result_array();
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Attendee_id',$user_id);
            $this->db->where('Event_id',$result1[0]['Id']);
            $qu=$this->db->get();
            $res=$qu->result_array();
            if(count($res) > 0)
            {
                $array_attendee_add['extra_column']= $extra_column;
                $array_attendee_add['views_id']=$vres[0]['view_id'];
                $this->db->where('Attendee_id',$user_id);
                $this->db->where('Event_id',$result1[0]['Id']);
                $this->db->update('event_attendee',$array_attendee_add);
            }
            else
            {
                $array_attendee_add['Event_id'] = $result1[0]['Id'];
                $array_attendee_add['Attendee_id'] = $user_id;
                $array_attendee_add['extra_column']= $extra_column;
                $array_attendee_add['views_id']=$vres[0]['view_id'];
                $this->db->insert('event_attendee',$array_attendee_add);
            }    
        }
        $this->db->select('*')->from('relation_event_user');
        $this->db->where('User_id',$user_id);
        $this->db->where('Event_id',$result1[0]['Id']);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(empty($res))
        {
            $array_relation_user_attendee['Event_id'] = $result1[0]['Id'];
            $array_relation_user_attendee['User_id'] = $user_id; 
            $array_relation_user_attendee['Organisor_id'] = $result1[0]['Organisor_id']; 
            $array_relation_user_attendee['Role_id'] = $result[0]['Id']; 
            $this->db->insert('relation_event_user',$array_relation_user_attendee);
        }
        if(!empty($agenda_id))
        {
            $this->db->select('*')->from('attendee_agenda_relation');
            $this->db->where('attendee_id',$user_id);
            $this->db->where('event_id',$result1[0]['Id']);
            $qu=$this->db->get();
            $res=$qu->result_array();
            if(count($res)>0)
            {
                $arr['agenda_category_id']=$agenda_id;
                $this->db->where('attendee_id',$user_id);
                $this->db->where('event_id',$result1[0]['Id']);
                $this->db->update('attendee_agenda_relation',$arr);
            }
            else
            {
                $arr['attendee_id']=$user_id;
                $arr['agenda_category_id']=$agenda_id;
                $arr['event_id']=$result1[0]['Id'];
                $this->db->insert('attendee_agenda_relation',$arr);
            }
        }
        else
        {
            if($role_flag!=1)
            {
                $this->db->select('*')->from('agenda_categories ac');
                $this->db->where('event_id',$result1[0]['Id']);
                $qu=$this->db->get();
                $eares=$qu->result_array();
                if(count($eares)<=1)
                {
                    $agenda_id=$eares[0]['Id'];
                }
                else
                {
                    $this->db->select('*')->from('agenda_categories ac');
                    $this->db->where('event_id',$result1[0]['Id']);
                    $this->db->where('categorie_type','1');
                    $qu=$this->db->get();
                    $peares=$qu->result_array();
                    $agenda_id=$peares[0]['Id'];
                }
                if(!empty($agenda_id))
                {
                    $this->db->select('*')->from('attendee_agenda_relation');
                    $this->db->where('attendee_id',$user_id);
                    $this->db->where('event_id',$result1[0]['Id']);
                    $qu=$this->db->get();
                    $res=$qu->result_array();
                    if(count($res)>0)
                    {
                        $arr['agenda_category_id']=$agenda_id;
                        $this->db->where('attendee_id',$user_id);
                        $this->db->where('event_id',$result1[0]['Id']);
                        $this->db->update('attendee_agenda_relation',$arr);
                    }
                    else
                    {
                        $arr['attendee_id']=$user_id;
                        $arr['agenda_category_id']=$agenda_id;
                        $arr['event_id']=$result1[0]['Id'];
                        $this->db->insert('attendee_agenda_relation',$arr);
                    }
                }
            }   
        }
        return $user_id;
    }
    public function add_exhibitor_user($post_arr)
    {   
        $event_id=$post_arr['event_id'];
        $email=$post_arr['exhibitor_email'];
        $password=$post_arr['new_confirm_password'];
        $stand_nubmer=$post_arr['stand_nubmer'];
        $this->db->select('*')->from('event');
        $this->db->where('Id',$event_id);
        $evqu = $this->db->get();
        $everes = $evqu->result_array();

        $this->db->select('*')->from('attendee_invitation');
        $this->db->where('Emailid',$email);
        $this->db->where('Event_id',$event_id);
        $this->db->where('role_status','1');
        $this->db->where('Status','0');
        $equ = $this->db->get();
        $eres = $equ->result_array();

        $this->db->select('*')->from('user');
        $this->db->where('Email',$email);
        $uqu = $this->db->get();
        $ures = $uqu->result_array();
        if(count($ures) > 0)        
        {
            $user_id=$ures[0]['Id'];
        }
        else
        {
            $user['Firstname']=$eres[0]['firstname'];
            $user['Lastname']=$eres[0]['lastname'];
            $user['Email']=$email;
            if($event_id=='447')
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $user['Password'] = $rc4->encrypt($password);
            }
            else
            {
                $user['Password'] = md5($password);
            }
            $user['Created_date']=date('Y-m-d H:i:s');
            $user['Active']='1';
            $user['Organisor_id']=$everes[0]['Organisor_id'];
            $this->db->insert('user',$user);
            $user_id=$this->db->insert_id();
        }

        $this->db->select('*')->from('relation_event_user');
        $this->db->where('User_id',$user_id);
        $this->db->where('Event_id',$event_id);
        $rqu=$this->db->get();
        $rres=$rqu->result_array();
        if(count($rres) > 0)
        {
            $this->db->where('User_id',$user_id);
            $this->db->where('Event_id',$event_id); 
            $array_relation_user_attendee['Role_id'] = '6'; 
            $this->db->update('relation_event_user',$array_relation_user_attendee);
        }
        else
        {
            $array_relation_user_attendee['Event_id'] = $event_id;
            $array_relation_user_attendee['User_id'] = $user_id; 
            $array_relation_user_attendee['Organisor_id'] = $everes[0]['Organisor_id']; 
            $array_relation_user_attendee['Role_id'] = '6'; 
            $this->db->insert('relation_event_user',$array_relation_user_attendee);
        }
        
        $exi_invite_update['Status']='1';
        $exi_invite_update['isUsedLink']='1';
        $this->db->where('Emailid',$email);
        $this->db->where('Event_id',$event_id);
        $this->db->where('role_status','1');
        $this->db->update('attendee_invitation',$exi_invite_update);
        if(!empty($stand_nubmer))
        {
            $exibitor['Organisor_id']=$everes[0]['Organisor_id'];
            $exibitor['Event_id']=$event_id;
            $exibitor['user_id']=$user_id;
            $exibitor['stand_number']=$stand_nubmer;
            $this->db->insert('exibitor',$exibitor);
            $ex_id=$this->db->insert_id();
        }
        return $ex_id;
    }         
    public function delete_user($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('user');
        $str = $this->db->last_query();
    }
    
    public function get_recent_user_list()
    {
        $orid = $this->data['user']->Id;
        $this->db->select('u.*');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if($user[0]->Id)
        {
            $this->db->where('u.Id',$user[0]->Id);  
        }
        $this->db->where('r.Name','User');
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function checkemail($email,$id=null,$eventid=null)
    {

        $this->db->select('u.Id as uid');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        if($id!=NULL)
        {
            $this->db->where('u.Id !=',$id);
        }   
        
        if($eventid!=NULL)
        {
             $this->db->where('ru.Event_id =',$eventid);
        }
        
        $this->db->where('u.Email',$email);            
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result_array();
        
        if(count($res)>=1)
        {
            return true;
        }
        
        else
        {
            return false;
        }
    }

    public function check_user_email($data)
    {
        $strWhere = "Email = '".$data['email']."' and Email!='' ";
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($strWhere);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function check_sign_login($data)
    {
        $this->db->select('*,ru.*,role.Name as Role_name,user.Id as Id,role.Id as Rid,role.role_type,role.ask_survey,role.add_survey');
        $this->db->from('user');
        
        $this->db->where('Email', $data['email']);
        $this->db->where('Password', md5($data['password']));
        $this->db->where('user.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('ru.Event_id', $data['Event_id']);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function get_acc_name($org_id)
    {
        $this->db->select('acc_name');
        $this->db->from('user');
        $this->db->where('Id',$org_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $name=$res[0]['acc_name'];
        return $name;

    }
    public function check_login($data,$eventid)
    {   
        $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,role.role_type,role.ask_survey,role.add_survey');
        $this->db->from('user');
        $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('Email', $data['username']);
        if($eventid=="447")
        {
            $this->load->library('RC4');
            $rc4 = new RC4();
            $pass = $rc4->encrypt($data['password']);
            $this->db->where('Password',$pass);
        }
        else
        {
            $this->db->where('Password', md5($data['password']));
        }
        $this->db->where('user.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->where('(ru.Event_id='.$eventid,NULL,FALSE);
        $this->db->or_where('(ru.Event_id='.$eventid.' and ru.Role_id=3))',NULL,FALSE);
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result_array();

        if($res[0]['Role_name'] == 'Client')
        {
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,role.role_type,role.ask_survey,role.add_survey');
            $this->db->from('user');
            $this->db->where('Email', $data['username']);
            $this->db->where('Password', md5($data['password']));
            $this->db->where('user.Active', '1');
            $this->db->where('role.Active', '1');
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->where('(ru.Event_id='.$eventid,NULL,FALSE);
            //$this->db->or_where('(ru.Event_id is NULL and ru.Role_id=3))',NULL,FALSE);
            $this->db->or_where('(ru.Event_id='.$eventid.' and ru.Role_id=3))',NULL,FALSE);
            $this->db->limit(1);
            $query1 = $this->db->get();;
            $res1 = $query1->result();
            if(!empty($res1))
            {
               unset($res1[0]->Speaker_desc);
            }
            $this->db1->select('*');
            $this->db1->from('organizer_user ou');
            $this->db1->where('Email',$res1[0]->Email);
            $this->db1->where('Expiry_date >=',date("Y-m-d"));
            $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
            $query=$this->db1->get();
            $res=$query->result_array();
            if(count($res)==0 || $res[0]['status']==0)
            {
                if(count($res1)==0)
                {
                    $res1=$res;
                }
                else
                {
                    $res1=$res;
                    $res1="inactive";
                }
            }
            return $res1;

        }
        else
        {
            $this->db->select('u.*,ru.*,r.Name as Role_name,u.Id as Id,r.Id as Rid,r.role_type,r.ask_survey,r.add_survey');
            $this->db->from('user u');
            $this->db->join('relation_event_user ru', 'ru.User_id = u.Id','left');
            $this->db->join('role r', 'r.Id = ru.Role_id');
            $this->db->where('u.Email', $data['username']);
            if($eventid=="447")
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $pass = $rc4->encrypt($data['password']);
                $this->db->where('u.Password',$pass);
            }
            else
            {
                $this->db->where('u.Password', md5($data['password']));
            }
            $this->db->where('u.Active', '1');
            $this->db->where('r.Active', '1');
            $this->db->where('ru.Event_id', $data['Event_id']);
            $this->db->group_by('ru.Event_id', $data['Event_id']);
            $this->db->limit(1);
            $query1 = $this->db->get();
            $res1 = $query1->result();
            if(!empty($res1))
            {
               unset($res1[0]->Speaker_desc);
            }
            $arr=array("Login_date"=>date ("Y-m-d H:i:s"));
            $this->db->where('Email', $data['username']);
             if($eventid=="447")
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $pass = $rc4->encrypt($data['password']);
                $this->db->where('Password',$pass);
            }
            else
            {
                $this->db->where('Password', md5($data['password']));
            }
            $this->db->update("user",$arr);
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('Id',$res1[0]->Organisor_id);
            $qu=$this->db->get();
            $organizeruser=$qu->result_array();
            $this->db1->select('*');
            $this->db1->from('organizer_user ou');
            $this->db1->where('Email',$organizeruser[0]['Email']);
            $this->db1->where('Expiry_date >=',date("Y-m-d"));
            $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
            $query=$this->db1->get();
            $res=$query->result_array();
            if(count($res)==0 || $res[0]['status']==0)
            {
                if(count($res1)==0)
                {
                    $res1=$res;
                }
                else
                {
                    $res1=$res;
                    $res1="inactive";
                }

            }
            return $res1;
        }
    }

    public function change_pas($data)
    {
        $this->db->where('Email',$data['email']);
        $this->db->update('user',array("Password"=>md5($data['new_pass'])));
    }
    public function check_exists_users($email,$name,$img,$Subdomain)
    {
            $arr=explode(" ", $name);

            $this->db->select('Id,Organisor_id');
            $this->db->from('event');
            $this->db->where('Subdomain',$Subdomain);
            $query1 = $this->db->get();
            $res1 = $query1->result_array();
            $eventid=$res1[0]['Id'];
            $Organisor_id=$res1[0]['Organisor_id'];
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('Email',$email);
            $query1 = $this->db->get();
            $res1 = $query1->result_array();
            if(count($res1)==0)
            {
                $data['Email']=$email;
                $data['Firstname']=$arr[0];
                $data['Lastname']=$arr[1];
                $data['Logo']=$img;
                $data['attendee_flag']='0';
                $chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                $auto_password=substr(str_shuffle( $chars ), 0, 6 );
                if($eventid=='447')
                {
                    $this->load->library('RC4');
                    $rc4 = new RC4();
                    $data['Password'] = $rc4->encrypt($auto_password);
                }
                else
                {
                    $data['Password'] = md5($auto_password);
                }
                $data['Active']='1';
                $data['Organisor_id']=$Organisor_id;
                $this->db->insert("user",$data);
                $user_id=$this->db->insert_id();
                $relation_event_user['Event_id']=$eventid;
                $relation_event_user['User_id']=$user_id;
                $relation_event_user['Role_id']=4;
                $relation_event_user['Organisor_id']=$Organisor_id;
                // send mail to first time facebook user login
                $this->load->library('email');
                
                /*$config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = ""; 
                $config['smtp_pass'] = "";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "text/html";
                $config['newline'] = "\r\n";
                $this->email->initialize($config);*/


                $this->email->from('admin@admin.com','Event Organizor');
                $this->email->subject('EventApp Auto-generated Password');
                $this->email->to($email);
                $this->email->set_mailtype("html");
                $message="Hello ".$arr[0].","."<br>"."Your Auto-generated EventApp password is ".$auto_password;
                $this->email->message(html_entity_decode($message));
                $this->email->send();
                
                $this->db->select('*')->from('relation_event_user');
                $this->db->where('Event_id',$relation_event_user['Event_id']);
                $this->db->where('User_id',$relation_event_user['User_id']);
                $res=$this->db->get()->result_array();
                if(count($res) <= 0)
                {
                    $this->db->insert("relation_event_user",$relation_event_user);
                }
                $session_data=$this->create_fb_session($user_id);
                return $session_data;
            }
            else
            {
               $user_id=$res1[0]['Id'];
               $this->db->select('*')->from('relation_event_user');
               $this->db->where('Event_id',$eventid);
               $this->db->where('User_id',$user_id);
               $this->db->where('Organisor_id',$Organisor_id);
               $qu=$this->db->get();
               $res=$qu->result_array();
               if(count($res)==0){
                    $rel['Event_id']=$eventid;
                    $rel['User_id']=$user_id;
                    $rel['Organisor_id']=$Organisor_id;
                    $rel['Role_id']=4;
                    $this->db->insert('relation_event_user',$rel);
               }

                $this->db->select('*')->from('agenda_categories ac');
                $this->db->where('event_id',$eventid);
                $qu=$this->db->get();
                $eares=$qu->result_array();
                if(count($eares)<=1)
                {
                    $agenda_id=$eares[0]['Id'];
                }
                else
                {
                    $this->db->select('*')->from('agenda_categories ac');
                    $this->db->where('event_id',$eventid);
                    $this->db->where('categorie_type','1');
                    $qu=$this->db->get();
                    $peares=$qu->result_array();
                    $agenda_id=$peares[0]['Id'];
                }
                if(!empty($agenda_id))
                {
                    $this->db->select('*')->from('attendee_agenda_relation');
                    $this->db->where('attendee_id',$user_id);
                    $this->db->where('event_id',$result1[0]['Id']);
                    $qu=$this->db->get();
                    $res=$qu->result_array();
                    if(count($res)>0)
                    {
                        $arr1['agenda_category_id']=$agenda_id;
                        $this->db->where('attendee_id',$user_id);
                        $this->db->where('event_id',$eventid);
                        $this->db->update('attendee_agenda_relation',$arr1);
                    }
                    else
                    {
                        $arr1['attendee_id']=$user_id;
                        $arr1['agenda_category_id']=$agenda_id;
                        $arr1['event_id']=$eventid;
                        $this->db->insert('attendee_agenda_relation',$arr1);
                    }
                }
               $session_data=$this->create_fb_session($user_id);
               return $session_data;
            }
          
    }
     public function check_exists_linkdin_users($email,$name,$img,$Subdomain,$accname)
    {
        $arr=explode(" ", $name);

        $this->db->select('Id,Organisor_id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $eventid=$res1[0]['Id'];
        $Organisor_id=$res1[0]['Organisor_id'];
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email',$email);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        if(count($res1)==0)
        {
            $data['Email']=$email;
            $data['Firstname']=$arr[0];
            $data['Lastname']=$arr[1];
            $data['acc_name']=$accname;
            $data['Logo']=$img;
            $data['attendee_flag']='0';
            $chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $auto_password=substr(str_shuffle( $chars ), 0, 6 );
            if($eventid=='447')
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $data['Password'] = $rc4->encrypt($auto_password);
            }
            else
            {
                $data['Password'] = md5($auto_password);
            }
            $data['Active']='1';
            $data['Organisor_id']=$Organisor_id;
            $this->db->insert("user",$data);
            $user_id=$this->db->insert_id();
            $relation_event_user['Event_id']=$eventid;
            $relation_event_user['User_id']=$user_id;
            $relation_event_user['Role_id']=4;
            $relation_event_user['Organisor_id']=$Organisor_id;
            // send mail to first time facebook user login
            $this->load->library('email');
            
            /*$config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = ""; 
            $config['smtp_pass'] = "";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "text/html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);*/


            $this->email->from('admin@admin.com','Event Organizor');
            $this->email->subject('EventApp Auto-generated Password');
            $this->email->to($email);
            $this->email->set_mailtype("html");
            $message="Hello ".$arr[0].","."<br>"."Your Auto-generated EventApp password is ".$auto_password;
            $this->email->message(html_entity_decode($message));
            $this->email->send();
                
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id',$relation_event_user['Event_id']);
            $this->db->where('User_id',$relation_event_user['User_id']);
            $res=$this->db->get()->result_array();
            if(count($res) <= 0)
            {
                $this->db->insert("relation_event_user",$relation_event_user);
            }
            $session_data=$this->create_fb_session($user_id);
            return $session_data;
        }
        else
        {
           $user_id=$res1[0]['Id'];
           $this->db->select('*')->from('relation_event_user');
           $this->db->where('Event_id',$eventid);
           $this->db->where('User_id',$user_id);
           $this->db->where('Organisor_id',$Organisor_id);
           $qu=$this->db->get();
           $res=$qu->result_array();
           if(count($res)==0){
                $rel['Event_id']=$eventid;
                $rel['User_id']=$user_id;
                $rel['Organisor_id']=$Organisor_id;
                $rel['Role_id']=4;
                $this->db->insert('relation_event_user',$rel);
           }

            $this->db->select('*')->from('agenda_categories ac');
            $this->db->where('event_id',$eventid);
            $qu=$this->db->get();
            $eares=$qu->result_array();
            if(count($eares)<=1)
            {
                $agenda_id=$eares[0]['Id'];
            }
            else
            {
                $this->db->select('*')->from('agenda_categories ac');
                $this->db->where('event_id',$eventid);
                $this->db->where('categorie_type','1');
                $qu=$this->db->get();
                $peares=$qu->result_array();
                $agenda_id=$peares[0]['Id'];
            }
            if(!empty($agenda_id))
            {
                $this->db->select('*')->from('attendee_agenda_relation');
                $this->db->where('attendee_id',$user_id);
                $this->db->where('event_id',$eventid);
                $qu=$this->db->get();
                $res=$qu->result_array();
                if(count($res)>0)
                {
                    $arr['agenda_category_id']=$agenda_id;
                    $this->db->where('attendee_id',$user_id);
                    $this->db->where('event_id',$eventid);
                    $this->db->update('attendee_agenda_relation',$arr);
                }
                else
                {
                    $arr['attendee_id']=$user_id;
                    $arr['agenda_category_id']=$agenda_id;
                    $arr['event_id']=$eventid;
                    $this->db->insert('attendee_agenda_relation',$arr);
                }
            }
           $session_data=$this->create_fb_session($user_id);
           return $session_data;
        }   
    }
    public function create_fb_session($user_id)
    {
           
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,role.role_type,role.ask_survey,role.add_survey');
            $this->db->from('user');
            $this->db->where('user.Id',$user_id);
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->limit(1);
            $query1 = $this->db->get();
            $res1 = $query1->result();
            return $res1;
    }
    public function check_user_already_exists($email,$event_id)
    {
        $this->db->select('*')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
        $this->db->where('reu.Event_id',$event_id);
        $this->db->where('u.Email',$email);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function check_auth_user_login_only_email($username,$eventid)
    {
        $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,usl.Website_url,usl.Facebook_url,usl.Twitter_url,usl.Linkedin_url,role.role_type,role.ask_survey,role.add_survey');
        $this->db->from('user');
        $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->join('user_social_links usl','usl.User_id=user.Id','left');
        $this->db->where('Email', $username);
        $this->db->where('user.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->where('(ru.Event_id='.$eventid,NULL,FALSE);
        $this->db->or_where('(ru.Event_id='.$eventid.' and ru.Role_id=3))',NULL,FALSE);
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result_array();

        if($res[0]['Role_name'] == 'Client')
        {
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid,usl.Website_url,usl.Facebook_url,usl.Twitter_url,usl.Linkedin_url,role.role_type,role.ask_survey,role.add_survey');
            $this->db->from('user');
            $this->db->where('Email', $username);
            $this->db->where('user.Active', '1');
            $this->db->where('role.Active', '1');
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id','left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->join('user_social_links usl','usl.User_id=user.Id','left');
            $this->db->where('(ru.Event_id='.$eventid,NULL,FALSE);
            $this->db->or_where('(ru.Event_id='.$eventid.' and ru.Role_id=3))',NULL,FALSE);
            $this->db->limit(1);
            $query1 = $this->db->get();;
            $res1 = $query1->result();
            if(!empty($res1))
            {
               unset($res1[0]->Speaker_desc);
            }
            $this->db1->select('*');
            $this->db1->from('organizer_user ou');
            $this->db1->where('Email',$res1[0]->Email);
            $this->db1->where('Expiry_date >=',date("Y-m-d"));
            $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
            $query=$this->db1->get();
            $res=$query->result_array();
            if(count($res)==0 || $res[0]['status']==0)
            {
                if(count($res1)==0)
                {
                    $res1=$res;
                }
                else
                {
                    $res1=$res;
                    $res1="inactive";
                }
            }
            return $res1;
        }
        else
        {
            $this->db->select('u.*,ru.*,r.Name as Role_name,u.Id as Id,r.Id as Rid,usl.Website_url,usl.Facebook_url,usl.Twitter_url,usl.Linkedin_url,r.role_type,r.ask_survey,r.add_survey');
            $this->db->from('user u');
            $this->db->join('relation_event_user ru', 'ru.User_id = u.Id','left');
            $this->db->join('role r', 'r.Id = ru.Role_id');
            $this->db->join('user_social_links usl','usl.User_id=u.Id','left');
            $this->db->where('u.Email', $username);
            $this->db->where('u.Active', '1');
            $this->db->where('r.Active', '1');
            $this->db->where('ru.Event_id', $eventid);
            $this->db->group_by('ru.Event_id', $eventid);
            $this->db->limit(1);
            $query1 = $this->db->get();
            $res1 = $query1->result();
            if(!empty($res1))
            {
               unset($res1[0]->Speaker_desc);
            }
            $arr=array("Login_date"=>date ("Y-m-d H:i:s"));
            $this->db->where('Email', $username);
            $this->db->update("user",$arr);
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('Id',$res1[0]->Organisor_id);
            $qu=$this->db->get();
            $organizeruser=$qu->result_array();
            $this->db1->select('*');
            $this->db1->from('organizer_user ou');
            $this->db1->where('Email',$organizeruser[0]['Email']);
            $this->db1->where('Expiry_date >=',date("Y-m-d"));
            $this->db1->join('subscription_billing_cycle sbc','ou.subscriptiontype=sbc.id');
            $query=$this->db1->get();
            $res=$query->result_array();
            if(count($res)==0 || $res[0]['status']==0)
            {
                if(count($res1)==0)
                {
                    $res1=$res;
                }
                else
                {
                    $res1=$res;
                    $res1="inactive";
                }

            }
            return $res1;
        }
    }
    public function check_authorized_email_by_email($email,$event_id)
    {
        $this->db->select('*')->from('authorized_user');
        $this->db->where('Email',$email);
        $this->db->where('Event_id',$event_id);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function add_authoried_Emails_user($data)
    {
        $userinfo = $this->User_model->get_user_by_email($data['Email']);
        if(count($userinfo) > 0)
        {
            $User_id=$userinfo[0]['Id'];
        }
        else
        {
            $userinfo['Firstname']=$data['firstname'];
            $userinfo['Lastname']=$data['lastname'];
            $userinfo['Email']=$data['Email'];
            $userinfo['Title']=$data['title'];
            $userinfo['Company_name']=$data['company_name'];
            $userinfo['Active']='1';
            $this->db->insert('user',$userinfo);
            $User_id=$this->db->insert_id();
        }
        $this->db->select('*')->from('relation_event_user');
        $this->db->where('Event_id',$data['Event_id']);
        $this->db->where('User_id',$User_id);
        $rel=$this->db->get()->result_array();
        if(count($rel) > 0)
        {
            $role=$rel[0]['Role_id'];
        }
        else
        {
            $reldata['Event_id']=$data['Event_id'];
            $reldata['User_id']=$User_id;
            $reldata['Organisor_id']=$data['Organisor_id'];
            if($data['email_type']=='1')
            {
                $reldata['Role_id']=6;
            }
            else
            {
                $reldata['Role_id']=4;
            }
            $this->db->insert('relation_event_user',$reldata);
            $role=$reldata['Role_id'];
        }
        if($role==4)
        {
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id',$data['Event_id']);
            $this->db->where('Attendee_id',$User_id);
            $eventatt=$this->db->get()->result_array();
            if(count($eventatt) <= 0)
            {
                $this->db->select('view_id')->from('user_views');
                $this->db->where('event_id',$data['Event_id']);
                $this->db->where('view_type','1');
                $view=$this->db->get()->result_array();

                $eventuser['Event_id']=$data['Event_id'];
                $eventuser['Attendee_id']=$User_id;
                if($data['check_in']=='1')
                {
                    $eventuser['check_in_attendee']='1';
                }
                else
                {
                    $eventuser['check_in_attendee']='0';   
                }
                $eventuser['extra_column']=$data['extra_column'];
                $eventuser['views_id']= !empty($data['views_id']) ? $data['views_id'] : $view[0]['view_id'];
                $this->db->insert('event_attendee',$eventuser);
            }
            if(empty($data['agenda_id']))
            {
                $this->db->select('*')->from('agenda_categories ac');
                $this->db->where('event_id',$data['Event_id']);
                $qu=$this->db->get();
                $eares=$qu->result_array();
                if(count($eares)<=1)
                {
                    $agenda_id=$eares[0]['Id'];
                }
                else
                {
                    $this->db->select('*')->from('agenda_categories ac');
                    $this->db->where('event_id',$data['Event_id']);
                    $this->db->where('categorie_type','1');
                    $qu=$this->db->get();
                    $peares=$qu->result_array();
                    $agenda_id=$peares[0]['Id'];
                }
            }
            else
            {
                $agenda_id=$data['agenda_id'];
            }
            if(!empty($agenda_id))
            {
                $this->db->select('*')->from('attendee_agenda_relation');
                $this->db->where('attendee_id',$User_id);
                $this->db->where('event_id',$data['Event_id']);
                $qu=$this->db->get();
                $res=$qu->result_array();
                if(count($res) <= 0)
                {
                    $arr['attendee_id']=$User_id;
                    $arr['agenda_category_id']=$agenda_id;
                    $arr['event_id']=$data['Event_id'];
                    $this->db->insert('attendee_agenda_relation',$arr);
                }
            }
        }
    }
    public function remove_authorized_user($aid)
    {
        $this->db->where('authorized_id',$aid);
        $this->db->delete('authorized_user');
    }
    /*public function save_registration_screen_user($postarr,$event_id,$org_id)
    {
        unset($postarr['stripeToken']);
        $userdata=$this->db->select('*')->from('user')->where('Email',$postarr['email'])->get()->row_array();
        if(count($userdata) > 0)
        {
            $user_id=$userdata['Id'];
            $user_email=$userdata['Email'];
        }
        else
        {
            $attendee_type=$postarr['attendee_type'];
            $user_data['Firstname']=$postarr['first_name'];
            $user_data['Lastname']=$postarr['last_name'];
            $user_data['Email']=$postarr['email'];
            $user_data['Company_name']=$postarr['company_name'];
            $user_data['Title']=$postarr['title'];
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['Created_date']=date('Y-m-d H:i:s');
            $this->db->insert('user',$user_data);
            $user_id=$this->db->insert_id();
            $user_email=$user_data['Email'];
        }
        
        unset($postarr['attendee_type']);
        unset($postarr['first_name']);
        unset($postarr['last_name']);
        unset($postarr['email']);
        unset($postarr['company_name']);
        unset($postarr['title']);

        $rel_data['Event_id']=$event_id;
        $rel_data['User_id']=$user_id;
        $relation=$this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->row_array();
        if(count($relation) < 1)
        {
            $rel_data['Organisor_id']=$org_id;
            $rel_data['Role_id']=4;
            $this->db->insert('relation_event_user',$rel_data);
        }

        $view=$this->db->select('*')->from('user_views')->where('event_id',$event_id)->where('view_type','1')->get()->row_array();

        $attendee_data['Event_id']=$event_id;
        $attendee_data['Attendee_id']=$user_id;
        $attendee=$this->db->select('*')->from('event_attendee')->where($attendee_data)->get()->row_array();
        if(count($attendee) < 1)
        {
            $attendee_data['views_id']=$view['view_id'];
            $this->db->insert('event_attendee',$attendee_data);
        }

        $agenda_categories=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->get()->result_array();
        if(count($eares)<=1)
        {
            $agenda_id=$agenda_categories[0]['Id'];
        }
        else
        {
            $peares=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->where('categorie_type','1')->get()->result_array();
            $agenda_id=$peares[0]['Id'];
        }
        if(!empty($agenda_id))
        {
            $assigagenda=$this->db->select('*')->from('attendee_agenda_relation')->where('attendee_id',$user_id)->where('event_id',$event_id)->get()->result_array();
            if(count($assigagenda) < 1)
            {
                $rel_agenda['attendee_id']=$user_id;
                $rel_agenda['event_id']=$event_id;
                $rel_agenda['agenda_category_id']=$agenda_id;
                $this->db->insert('attendee_agenda_relation',$rel_agenda);
            }
        }
        $this->db->select('*')->from('temp_registration_user tru');
        $this->db->where('tru.email',$user_email);
        $this->db->where('tru.event_id',$event_id);
        $temp_user=$this->db->get()->result_array();
        if(!empty($temp_user[0]['extra_info']))
        {
            $postarr=array_merge($postarr,json_decode($temp_user[0]['extra_info'],true));
        }
        $extra=array();
        foreach ($postarr as $key => $value) {
            $prekey=str_ireplace('_',' ',$key);
            $ccolumn=$this->db->select('*')->from('custom_column')->where('event_id',$event_id)->where('column_name',$prekey)->get()->result_array();
            if(count($ccolumn) <= 0)
            {
                $cc_data['column_name']=$prekey;
                $cc_data['event_id']=$event_id;
                $this->db->insert('custom_column',$cc_data);
            }
            $extra[$prekey]=$value;
        }
        if(count($extra) > 0)
        {
            $attendee['Event_id']=$event_id;
            $attendee['Attendee_id']=$user_id;
            $extracolumn_data['extra_column']=json_encode($extra);
            $this->db->where($attendee);
            $this->db->update('event_attendee',$extracolumn_data);
        }
        if(!empty($temp_user[0]['save_agenda_ids']))
        {
            $this->db->select('*')->from('users_agenda');
            $this->db->where('user_id',$user_id);
            $user_agenda=$this->db->get()->row_array();
            if(count($user_agenda) > 0)
            {
                $updatedata['agenda_id']=$user_agenda['agenda_id'].','.$temp_user[0]['save_agenda_ids'];
                $this->db->where('user_id',$user_id);
                $this->db->update('users_agenda',$updatedata);
            }
            else
            {
                $updatedata['agenda_id']=$temp_user[0]['save_agenda_ids'];
                $updatedata['user_id']=$user_id;
                $this->db->insert('users_agenda',$updatedata);
            }
        }
        $this->db->where('temp_uid',$temp_user[0]['temp_uid']);
        $this->db->delete('temp_registration_user');
        return $this->db->select('*')->from('user')->where('Id',$user_id)->get()->row_array();
    }
    public function save_multiple_user_registration($udata,$temp_uid=NULL)
    {
        if(!empty($temp_uid))
        {
            $this->db->where('temp_uid',$temp_uid);
            $this->db->update('temp_registration_user',$udata);
        }
        else
        {
            $this->db->select('*')->from('temp_registration_user');
            $this->db->where('email',trim($udata['email']));
            $this->db->where('event_id',$udata['event_id']);
            $res=$this->db->get()->result_array();
            if(count($res) > 0)
            {
                $this->db->where('email',trim($udata['email']));
                $this->db->where('event_id',$udata['event_id']);
                $this->db->update('temp_registration_user',$udata);
            }
            else
            {
                $this->db->insert('temp_registration_user',$udata);
            }
        }
    }
    public function get_all_temp_multiuser_by_event($event_id)
    {
        $this->db->select('*')->from('temp_registration_user');
        $this->db->where('event_id',$event_id);
        return $this->db->get()->result_array();
    }
    public function delete_temp_multiuser($event_id)
    {
        $this->db->where('event_id',$event_id);
        $this->db->delete('temp_registration_user');
    }
    public function delete_another_user_data($event_id,$emails)
    {
        $this->db->where('event_id',$event_id);
        $this->db->where_not_in('email',$emails);
        $this->db->delete('temp_registration_user');
    }*/
    public function save_registration_screen_user($event_id,$org_id,$email=NULL)
    {   
        $temp_user=$this->get_all_temp_registration_screen_by_event($event_id,$email)[0];
        $userdata=$this->db->select('*')->from('user')->where('Email',$temp_user['email'])->get()->row_array();
        if(count($userdata) > 0)
        {
            $user_id=$userdata['Id'];
        }
        else
        {
            $user_data['Firstname']=$temp_user['first_name'];
            $user_data['Lastname']=$temp_user['last_name'];
            $user_data['Email']=$temp_user['email'];
            $user_data['Company_name']=$temp_user['company_name'];
            $user_data['Title']=$temp_user['title'];
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['Created_date']=date('Y-m-d H:i:s');
            $this->db->insert('user',$user_data);
            $user_id=$this->db->insert_id();
        }
        unset($user_data);
        $rel_data['Event_id']=$event_id;
        $rel_data['User_id']=$user_id;
        $relation=$this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->row_array();
        if(count($relation) < 1)
        {
            $rel_data['Organisor_id']=$org_id;
            $rel_data['Role_id']=4;
            $this->db->insert('relation_event_user',$rel_data);
        }
        unset($rel_data);
        $view=$this->db->select('*')->from('user_views')->where('event_id',$event_id)->where('view_type','1')->get()->row_array();
        $attendee_data['Event_id']=$event_id;
        $attendee_data['Attendee_id']=$user_id;
        $attendee=$this->db->select('*')->from('event_attendee')->where($attendee_data)->get()->row_array();
        if(count($attendee) < 1)
        {
            $attendee_data['extra_column']=!empty($temp_user['extra_column']) ? $temp_user['extra_column'] : NULL;
            $attendee_data['views_id']=$view['view_id'];
            $this->db->insert('event_attendee',$attendee_data);
        }
        unset($attendee_data);
        if(!empty($temp_user['save_agenda_ids']))
        {   

            $this->db->select('*')->from('users_agenda');
            $this->db->where('user_id',$user_id);
            $user_agenda=$this->db->get()->row_array();
            if(count($user_agenda) > 0)
            {
                $updatedata['agenda_id']=$user_agenda['agenda_id'].$temp_user['save_agenda_ids'];
                $this->db->where('user_id',$user_id);
                $this->db->update('users_agenda',$updatedata);
            }
            else
            {
                $updatedata['agenda_id']=$temp_user['save_agenda_ids'];
                $updatedata['user_id']=$user_id;
                $this->db->insert('users_agenda',$updatedata);
            }
            unset($updatedata);
            $this->db->select('*')->from('agenda_category_relation acr');
            $this->db->where_in('acr.agenda_id',array_filter(explode(",",$temp_user['save_agenda_ids'])));
            $agenda_categories=$this->db->get()->result_array();
            $category_ids=array_unique(array_column($agenda_categories,'category_id'));
            foreach ($category_ids as $key => $value) 
            {
                $agenda_rel['attendee_id']=$user_id;
                $agenda_rel['agenda_category_id']=$value;
                $agenda_rel['event_id']=$event_id;
                $agendarel_data=$this->db->select('*')->from('attendee_agenda_relation')->where($agenda_rel)->get()->result_array();
                if(count($agendarel_data) < 1)
                {
                    $this->db->insert('attendee_agenda_relation',$agenda_rel);
                }
                unset($agenda_rel);
            }
        }
        else
        {
            $agenda_categories=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->get()->result_array();
            if(count($eares)<=1)
            {
                $agenda_id=$agenda_categories[0]['Id'];
            }
            else
            {
                $peares=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->where('categorie_type','1')->get()->result_array();
                $agenda_id=$peares[0]['Id'];
            }
            if(!empty($agenda_id))
            {
                $assigagenda=$this->db->select('*')->from('attendee_agenda_relation')->where('attendee_id',$user_id)->where('event_id',$event_id)->get()->result_array();
                if(count($assigagenda) < 1)
                {
                    $rel_agenda['attendee_id']=$user_id;
                    $rel_agenda['event_id']=$event_id;
                    $rel_agenda['agenda_category_id']=$agenda_id;
                    $this->db->insert('attendee_agenda_relation',$rel_agenda);
                }
                unset($rel_agenda);
            }
        }
        if(!empty($temp_user['question_info']))
        {
            $question_info=json_decode($temp_user['question_info'],true);
            // j($question_info);
            $reg_question_id = $this->db->select('group_concat(id) as id')->where('event_id',$event_id)->get('reg_que')->row_array();
            $reg_question_id = explode(',',$reg_question_id['id']);
            foreach ($question_info as $key => $value) 
            {   
                if(in_array($value['question_id'],$reg_question_id))
                {
                    $ans_data['user_id']=$user_id;
                    $ans_data['que_id']=$value['question_id'];
                    $ansdata=$this->db->select('*')->from('reg_que_ans')->where($ans_data)->get()->result_array();
                    if(count($ansdata) > 0)
                    {
                        $this->db->where($ans_data);
                        if(is_array($value['answer']))
                            $ans_data['ans']=implode(',',$value['answer']);
                        else
                            $ans_data['ans']=$value['answer'];
                        $this->db->update('reg_que_ans',$ans_data);
                    }
                    else
                    {   
                        if(is_array($value['answer']))
                            $ans_data['ans']=implode(',',$value['answer']);
                        else
                            $ans_data['ans']=$value['answer'];
                        
                        $ans_data['ans_date']=date('Y-m-d H:i:s');
                        $this->db->insert('reg_que_ans',$ans_data);
                    }
                    unset($ans_data);
                }
            }
        }
        $this->db->where('temp_uid',$temp_user['temp_uid']);
        $this->db->delete('temp_registration_user');

        return $this->db->select('*')->from('user')->where('Id',$user_id)->get()->row_array();
    }
    public function save_registration_temp_user($udata)
    {
        $this->db->select('*')->from('temp_registration_user');
        $this->db->where('email',$udata['email']);
        $this->db->where('event_id',$udata['event_id']);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('email',$udata['email']);
            $this->db->where('event_id',$udata['event_id']);
            $this->db->update('temp_registration_user',$udata);
        }
        else
        {
            $this->db->insert('temp_registration_user',$udata);
        }
    }
    public function get_all_temp_registration_screen_by_event($event_id,$email=NULL)
    {
        $this->db->select('*')->from('temp_registration_user');
        if(!empty($email))
        {
            $this->db->where('email',$email);
        }
        $this->db->where('event_id',$event_id);
        return $this->db->get()->result_array();
    }
    public function delete_temp_multiuser($event_id)
    {
        $this->db->where('event_id',$event_id);
        $this->db->delete('temp_registration_user');
    }
    public function get_reg_question($q_id)
    {
        return $this->db->select('*')->from('reg_que')->where('id',$q_id)->get()->row_array();
    }
    public function update_user_data($id,$update=NULL,$question_info,$event_id=null,$extra_column=null)
    {   
        if(!empty($update))
        {
            $update['reg_date'] = date('Y-m-d H:i:s');
            $this->db->where('Id',$id);
            $this->db->update('user',$update);
        }
        $rel_user = $this->db->where('User_id',$id)->where('Event_id',$event_id)->where('Role_id','4')->get('relation_event_user')->row_array();

        if(empty($rel_user) && !empty($event_id))
        {   
            $event_data = $this->db->where('Id',$event_id)->get('event')->row_array();
            $insert['User_id'] = $id;
            $insert['Event_id'] = $event_id;
            $insert['Role_id'] = '4';
            $insert['Organisor_id'] = $event_data['Organisor_id'];
            $this->db->insert('relation_event_user',$insert);

        }
        if(!empty($extra_column) && !empty($event_id))
        {   
            $where['Event_id'] = $event_id;
            $where['Attendee_id'] = $id;
            $tmp_extra = $this->db->where($where)->get('event_attendee')->row_array();
            $e_update['extra_column'] = json_encode($extra_column);
            if(!empty($tmp_extra))
            {       
                $this->db->where($where);
                $this->db->update('event_attendee',$e_update);
            }
            else
            {   
                $e_insert = $where;
                $e_insert['extra_column'] = json_encode($extra_column);
                $this->db->insert('event_attendee',$e_insert);
            }
        }
        foreach ($question_info as $key => $value) 
        {   
            $ans_data['user_id']=$id;
            $ans_data['que_id']=$value['question_id'];
            $ansdata=$this->db->select('*')->from('reg_que_ans')->where($ans_data)->get()->result_array();

            if(count($ansdata) > 0)
            {   
                $this->db->where($ans_data);
                if(is_array($value['answer']))
                    $ans_data['ans']=implode(',',$value['answer']);
                else
                    $ans_data['ans']=$value['answer'];
                $this->db->update('reg_que_ans',$ans_data);
                unset($ans_data);
            }
            else
            {   
                if(is_array($value['answer']))
                    $ans_data['ans']=implode(',',$value['answer']);
                else
                    $ans_data['ans']=$value['answer'];

                $ans_data['ans_date']=date('Y-m-d H:i:s');
                $this->db->insert('reg_que_ans',$ans_data);
                unset($ans_data);

            }
        }
    }
    public function get_temp_reg_user($event_id,$email)
    {
        $res = $this->db->where('event_id',$event_id)->where('email',$email)->get('temp_registration_user')->row_array();
        return $res;
    }
    public function get_registered_user_answer($event_id,$user_id)
    {   
        $this->db->where('ra.user_id',$user_id);
        $this->db->join('reg_que rq','rq.id = ra.que_id','left');
        $this->db->where('rq.event_id',$event_id);
        $res = $this->db->get('reg_que_ans ra')->result_array();
        return $res;
    }
    public function get_users_agenda_list($event_id,$user_id)
    {
        $this->db->select('*')->from('users_agenda');
        $this->db->where('user_id',$user_id);
        $user_agenda=$this->db->get()->row_array();
        return $user_agenda;
    }
    public function check_org_email($email)
    {
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('u.Email',$email);
        $this->db->where('reu.Role_id','3');
        $res = $this->db->get('user u')->row_array();
        return $res;
    }
    public function get_user($user_id)
    {
        $res = $this->db->where('Id',$user_id)->get('user')->row_array();
        return $res;
    }
    public function getUserAttempts($user_id,$key_name)
    {
        $where['user_id'] = $user_id;
        $where['key_name'] = $key_name;
        return $this->db->select('*')->from('user_attempts')->where($where)->limit(1)->get()->row_array();
    }
    public function updateUserAttempts($id,$date,$attempts)
    {
        $c_date = date('Y-m-d');
        if($c_date != date('Y-m-d',strtotime($date)))
        {
            $update['created_date'] = $c_date;
            $update['value'] = 1;
        }
        else
        {
            $update['value'] = $attempts;
        }

        $this->db->where('id',$id);
        $this->db->update('user_attempts',$update) ;

    }
    public function insertUserAttempts($user_id,$keyname)
    {
        $data['user_id'] = $user_id;
        $data['key_name'] = $keyname;
        $data['value'] = 1;
       
        $this->db->insert('user_attempts',$data) ;

    }
    public function getUser($email)
    {
        return $this->db->where('Email',$email)->from('user')->get()->row_array();
    }
    public function checkToken($token)
    {
        return $this->db->select('id')->from('user_attempts')->where('value',$token)->get()->num_rows();
    }
}      
?>