<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Activity_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  public function getPublicMessageFeeds($event_id,$user_id)
  {
    $this->db->select("sm.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,sm.Message,sm.Time,sm.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = sm.Id AND module_type='public' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = sm.Id AND module_type='public' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Posted a public Message' as activity,'1' as activity_no,'public' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('speaker_msg sm');
    $this->db->join('user u','u.Id=sm.Sender_id');
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);    
    $this->db->where('sm.Event_id',$event_id);
    $this->db->where('sm.ispublic','1');
    $this->db->where('sm.Parent','0');
    $this->db->order_by('sm.Time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getPublicMessageFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("sm.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,sm.Message,sm.Time,sm.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = sm.Id AND module_type='public' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = sm.Id AND module_type='public' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Posted a public Message' as activity,'1' as activity_no,'public' as type,reu.Role_id,u.Id as user_id",false);
    $this->db->from('speaker_msg sm');
    $this->db->join('user u','u.Id=sm.Sender_id');
    $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$event_id);
    $this->db->where('reu.Role_id','3');
    $this->db->where('sm.Event_id',$event_id);
    $this->db->where('sm.ispublic','1');
    $this->db->where('sm.Parent','0');
    $this->db->order_by('sm.Time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  } 
  public function getPhotoFeeds($event_id,$user_id)
  {
    $this->db->select("fi.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,fi.Time,fi.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = fi.Id AND module_type='photo' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = fi.Id AND module_type='photo' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Shared a Photo' as activity,'2' as activity_no,'photo' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('feed_img fi');
    $this->db->join('user u','u.Id=fi.Sender_id');
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);    
    $this->db->where('fi.Event_id',$event_id);
    $this->db->where('fi.ispublic','1');
    $this->db->where('fi.Parent',0);
    $this->db->order_by('fi.Time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getPhotoFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("fi.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,fi.Time,fi.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = fi.Id AND module_type='photo' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = fi.Id AND module_type='photo' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Shared a Photo' as activity,'2' as activity_no,'photo' as type,reu.Role_id,u.Id as user_id",false);
    $this->db->from('feed_img fi');
    $this->db->join('user u','u.Id=fi.Sender_id');
    $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$event_id);
    $this->db->where('reu.Role_id','3');
    $this->db->where('fi.Event_id',$event_id);
    $this->db->where('fi.ispublic','1');
    $this->db->where('fi.Parent',0);
    $this->db->order_by('fi.Time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getCheckInFeeds($event_id,$user_id)
  {
    $this->db->select("uc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,uc.date as Time,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.id AND module_type='check_in' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.id AND module_type='check_in' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Checked in to a Session' as activity,'3' as activity_no,'check_in' as type,ru.Role_id,u.Id as user_id,a.Start_date as agenda_start_date,a.Start_time as agenda_start_time,",false);
    $this->db->from('user_check_in uc');
    $this->db->join('user u','u.Id=uc.user_id');
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);
    $this->db->join('agenda a','a.Id=uc.agenda_id');
    $this->db->where('a.Event_id',$event_id);
    $this->db->order_by('uc.date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    
    foreach ($result as $key => $value)
    {   
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
        $result[$key]['like_count'] = (!empty($value['like_count'])) ? $value['like_count'] : '0';
        $result[$key]['is_like'] = (!empty($value['is_like'])) ? $value['is_like'] : '0';
    }
    return $result;
  }
  public function getCheckInFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("uc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,uc.date as Time,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.id AND module_type='check_in' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.id AND module_type='check_in' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Checked in to a Session' as activity,'3' as activity_no,'check_in' as type,reu.Role_id,u.Id as user_id,
            a.Start_date as agenda_start_date,
            a.Start_time as agenda_start_time",false);
    $this->db->from('user_check_in uc');
    $this->db->join('user u','u.Id=uc.user_id');
    $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$event_id);
    $this->db->where('reu.Role_id','3');
    $this->db->join('agenda a','a.Id=uc.agenda_id');
    $this->db->where('a.Event_id',$event_id);
    $this->db->order_by('uc.date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    foreach ($result as $key => $value)
    {
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
    }
    return $result;
  }
  public function getRatingFeeds($event_id,$user_id)
  {
    $this->db->select("uc.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,uc.date as Time,uc.rating,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.Id AND module_type='rating' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.Id AND module_type='rating' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Rated A Session' as activity,'4' as activity_no,'rating' as type,ru.Role_id,u.Id as user_id,a.Start_date as agenda_start_date,
            a.Start_time as agenda_start_time",false);
    $this->db->from('user_session_rating uc');
    $this->db->join('user u','u.Id=uc.user_id');
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);
    $this->db->join('agenda a','a.Id=uc.session_id');
    $this->db->where('a.Event_id',$event_id);
    $this->db->order_by('uc.date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    foreach ($result as $key => $value)
    {
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
    }
    return $result;
  }
  public function getRatingFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("uc.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,uc.date as Time,uc.rating,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.Id AND module_type='rating' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.Id AND module_type='rating' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Rated A Session' as activity,'4' as activity_no,'rating' as type,reu.Role_id,u.Id as user_id,a.Start_date as agenda_start_date,
            a.Start_time as agenda_start_time",false);
    $this->db->from('user_session_rating uc');
    $this->db->join('user u','u.Id=uc.user_id');
    $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$event_id);
    $this->db->where('reu.Role_id','3');
    $this->db->join('agenda a','a.Id=uc.session_id');
    $this->db->where('a.Event_id',$event_id);
    $this->db->order_by('uc.date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    foreach ($result as $key => $value)
    {
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
    }
    return $result;
  }
  public function getUserFeeds($event_id,$user_id)
  {
    $this->db->select("u.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,u.updated_date as Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = u.Id AND module_type='update_profile' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = u.Id AND module_type='update_profile' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Updated profile picture' as activity,'5' as activity_no,'update_profile' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('user u');
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Event_id',$event_id);
    $this->db->where('u.updated_date is NOT NULL', NULL, FALSE);
    $this->db->order_by('u.updated_date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getUserFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("u.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,u.updated_date as Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = u.Id AND module_type='update_profile' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = u.Id AND module_type='update_profile' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Updated profile picture' as activity,'5' as activity_no,'update_profile' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('user u');
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');
    $this->db->where('ru.Event_id',$event_id);
    $this->db->where('u.updated_date is NOT NULL', NULL, FALSE);
    $this->db->order_by('u.updated_date','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getActivityFeeds($event_id,$user_id)
  {
    $this->db->select("a.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.message as Message,a.time as Time,a.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = a.id AND module_type='activity' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = a.id AND module_type='activity' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'posted a new Update' as activity,'6' as activity_no,'activity' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('activity a');
    $this->db->join('user u','u.Id=a.sender_id');
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('a.event_id',$event_id);
    $this->db->order_by('a.time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
  public function getActivityFeeds_by_org($event_id,$user_id)
  {
    $this->db->select("a.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.message as Message,a.time as Time,a.image,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = a.id AND module_type='activity' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = a.id AND module_type='activity' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'posted a new Update' as activity,'6' as activity_no,'activity' as type,ru.Role_id,u.Id as user_id",false);
    $this->db->from('activity a');
    $this->db->join('user u','u.Id=a.sender_id');
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');
    $this->db->where('a.event_id',$event_id);
    $this->db->order_by('a.time','DESC');
    $query = $this->db->get();
    $result =  $query->result_array();
    return $result;
  }
 public function getSaveSessionFeed($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("uc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.id AND module_type='session_save' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.id AND module_type='session_save' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Save Session' as activity,'7' as activity_no,'activity' as type,ru.Role_id,u.Id as user_id,a.Start_date as agenda_start_date,
            a.Start_time as agenda_start_time",false);
    $this->db->from("agenda a");
    $this->db->join("users_agenda uc","FIND_IN_SET(a.Id,(TRIM(BOTH ',' FROM uc.pending_agenda_id)))");
    $this->db->join("user u","u.Id=uc.user_id");
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where("a.Event_id",$event_id);
    $this->db->order_by('uc.id','DESC');
    $objQuery = $this->db->get();
    $result = $objQuery->result_array();
    foreach ($result as $key => $value)
    {
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
    }
    return $result;
 }
 public function getSaveSessionFeed_by_org($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("uc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,a.Heading as Message,a.Id as agenda_id,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = uc.id AND module_type='session_save' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = uc.id AND module_type='session_save' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Save Session' as activity,'7' as activity_no,'activity' as type,ru.Role_id,u.Id as user_id,a.Start_date as agenda_start_date,
            a.Start_time as agenda_start_time",false);
    $this->db->from("agenda a");
    $this->db->join("users_agenda uc","FIND_IN_SET(a.Id,(TRIM(BOTH ',' FROM uc.pending_agenda_id)))");
    $this->db->join("user u","u.Id=uc.user_id");
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');
    $this->db->where("a.Event_id",$event_id);
    $this->db->order_by('uc.id','DESC');
    $objQuery = $this->db->get();
    $result = $objQuery->result_array();
    foreach ($result as $key => $value)
    {
        $tmp = $this->checkEventDateTimeFormat($event_id,$value['agenda_start_date'],$value['agenda_start_time']);
        unset($result[$key]['agenda_start_date']);
        unset($result[$key]['agenda_start_time']);
        $result[$key]['agenda_time'] = $tmp;
    }
    return $result;
 }
 public function getauctionbidsFeed($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("pub.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,p.name as Message,pub.date as Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = pub.id AND module_type='auction_bids' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = pub.id AND module_type='auction_bids' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Bids On Product' as activity,'8' as activity_no,'auction_bids' as type,ru.Role_id,u.Id as user_id")->from('product_user_bids pub');
    $this->db->join('product p','p.product_id=pub.product_id');
    $this->db->join("user u","u.Id=pub.user_id");
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);      
    $this->db->where("p.Event_id",$event_id);
    $this->db->order_by("pub.date",'DESC');
    $objQuery = $this->db->get();
    $arrFinal = $objQuery->result_array();
    return $arrFinal;
 }
 public function getauctionbidsFeed_by_org($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("pub.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,p.name as Message,pub.date as Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = pub.id AND module_type='auction_bids' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = pub.id AND module_type='auction_bids' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Bids On Product' as activity,'8' as activity_no,'auction_bids' as type,ru.Role_id,u.Id as user_id")->from('product_user_bids pub');
    $this->db->join('product p','p.product_id=pub.product_id');
    $this->db->join("user u","u.Id=pub.user_id"); 
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');     
    $this->db->where("p.Event_id",$event_id);
    $this->db->order_by("pub.date",'DESC');
    $objQuery = $this->db->get();
    $arrFinal = $objQuery->result_array();
    return $arrFinal;
 }
 public function getcommentfeed($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("fc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,fc.comment as Message,fc.Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = fc.id AND module_type='photos_comments' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = fc.id AND module_type='photos_comments' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Comment On Photos' as activity,'9' as activity_no,'photos_comments' as type,ru.Role_id,u.Id as user_id")->from('feed_comment fc');
    $this->db->join('feed_img fi','fi.Id=fc.msg_id');
    $this->db->join("user u","u.Id=fc.user_id");   
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);
    $this->db->where('fi.Event_id',$event_id);
    $this->db->order_by('fc.Time','DESC');
    $feedcomment=$this->db->get()->result_array();

    $this->db->select("sc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,sc.comment as Message,sc.Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = sc.id AND module_type='msg_comments' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = sc.id AND module_type='msg_comments' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Comment On Message' as activity,'10' as activity_no,'msg_comments' as type,ru.Role_id,u.Id as user_id")->from('speaker_comment sc');
    $this->db->join('speaker_msg sm','sm.Id=sc.msg_id');
    $this->db->join("user u","u.Id=sc.user_id");
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);   
    $this->db->where('sm.Event_id',$event_id);
    $this->db->order_by('sc.Time','DESC');
    $msgcomment=$this->db->get()->result_array();
    $result=array_merge($feedcomment,$msgcomment);
    return $result;
 }
 public function getcommentfeed_by_org($event_id,$user_id)
 {
    $this->db->protect_identifiers=false;
    $this->db->select("fc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,fc.comment as Message,fc.Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = fc.id AND module_type='photos_comments' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = fc.id AND module_type='photos_comments' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Comment On Photos' as activity,'9' as activity_no,'photos_comments' as type,ru.Role_id,u.Id as user_id")->from('feed_comment fc');
    $this->db->join('feed_img fi','fi.Id=fc.msg_id');
    $this->db->join("user u","u.Id=fc.user_id");
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');    
    $this->db->where('fi.Event_id',$event_id);
    $this->db->order_by('fc.Time','DESC');
    $feedcomment=$this->db->get()->result_array();

    $this->db->select("sc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,sc.comment as Message,sc.Time,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = sc.id AND module_type='msg_comments' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = sc.id AND module_type='msg_comments' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'Comment On Message' as activity,'10' as activity_no,'msg_comments' as type,ru.Role_id,u.Id as user_id")->from('speaker_comment sc');
    $this->db->join('speaker_msg sm','sm.Id=sc.msg_id');
    $this->db->join("user u","u.Id=sc.user_id"); 
    $this->db->join('relation_event_user ru','ru.User_id=u.Id and ru.Event_id='.$event_id);
    $this->db->where('ru.Role_id','3');   
    $this->db->where('sm.Event_id',$event_id);
    $this->db->order_by('sc.Time','DESC');
    $msgcomment=$this->db->get()->result_array();
    $result=array_merge($feedcomment,$msgcomment);
    return $result;
 }
 public function get_all_notification_by_event($event_id,$user_id)
 {  
    $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        date_default_timezone_set("UTC");
    $cdate=date('Y-m-d H:i:s');
    if(!empty($event['Event_show_time_zone']))
    {
        if(strpos($event['Event_show_time_zone'],"-")==true)
        { 
          $arr=explode("-",$event['Event_show_time_zone']);
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
        }
        if(strpos($event['Event_show_time_zone'],"+")==true)
        {
          $arr=explode("+",$event['Event_show_time_zone']);
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
        }
    }

    $this->db->protect_identifiers=false;
    $this->db->select("n.Id as id,n.title as activity,n.content as Message,n.created_at as Time,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.Title as Title,u.Company_name as Company_name,(select sum(CASE WHEN `like` = '1' THEN 1 ELSE 0 END) from activity_like where module_primary_id = n.Id AND module_type='org_alerts' AND event_id='".$event_id."') as like_count,(select activity_like.like from activity_like where module_primary_id = n.Id AND module_type='org_alerts' AND user_id = '".$user_id."' AND event_id='".$event_id."') as is_like,'11' as activity_no,'org_alerts' as type,ru.Role_id,u.Id as user_id")->from('notification n');
    $this->db->join('event e','e.Id=n.event_id');
    $this->db->join('user u','e.Organisor_id=u.Id');
    $this->db->join('relation_event_user ru','ru.User_id = u.Id and ru.Event_id='.$event_id);
    $this->db->where('n.event_id',$event_id);
    $whr = "(n.notification_type = '1' OR (n.notification_type = '2' AND n.datetime <= '".$cdate."')) ";
    $this->db->where($whr);
    $res=$this->db->get()->result_array();
    return $res;
 }
 public function get_timeago($ptime)
 {
    $estimate_time = time() - $ptime;

    if ($estimate_time < 1)
    {
      return '1 second ago';
    }

    $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
    );
    foreach ($condition as $secs => $str)
    {
       $d = $estimate_time / $secs;

       if ($d >= 1)
       {
            $r = round($d);
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
       }
    }
  }
  public function save_update_feed($data)
  {
    $this->db->insert('activity',$data);
  }
  public function getFeedLike($data)     
  {         
    $this->db->select('like');         
    $this->db->from('activity_like');         
    $this->db->where($data);         
    $rows = $this->db->get()->row();         
    return $rows->like;     
  }     
  public function getFeed_total_Like($data)
  {
    $this->db->select('count(`like`) as likes');         
    $this->db->from('activity_like');         
    $this->db->where($data);         
    $rows = $this->db->get()->row_array();         
    return $rows['likes']; 
  }
  public function updateFeedLike($update_data,$where)     
  {       
    $this->db->where($where);       
    $this->db->update('activity_like',$update_data);     
  }     
  public function makeFeedLike($data)     
  {       
    $this->db->insert('activity_like',$data);     
  }
  public function makeFeedComment($data)
  {
    $this->db->insert('activity_comment',$data);
  }
  public function getComments($id,$type,$eid)
  {
    $this->db->select("ac.comment_id,ac.user_id as comment_user_id,ac.comment_image,ac.comment,concat(usr.Firstname,' ',usr.Lastname) as name,ac.datetime,usr.Logo",false);
    $this->db->from('activity_comment ac');
    $this->db->join('user usr','ac.user_id = usr.Id');
    $this->db->where('ac.event_id',$eid);
    $this->db->where('ac.module_primary_id',$id);
    $this->db->where('ac.module_type',$type);
    $this->db->order_by('datetime','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    foreach ($result as $key => $value) {
      $result[$key]['datetime'] = $this->get_timeago(strtotime($value['datetime']));
    }
    return $result;
  }
  public function delete_comment_form_post($id)
  {
    $this->db->where('comment_id',$id);
    $this->db->delete('activity_comment');
  }
  public function getPermission($intEventId=0)
  {
    $this->db->select("*");
    $this->db->from('activity_permission');
    $this->db->where("event_id",$intEventId);
    $query = $this->db->get();
    $arrResult = $query->result_array();
    return $arrResult;
  }
  public function update_acvity_permission($intEventId = 0,$arrData)
  {
    $res=$this->getPermission($intEventId);
    if(count($res) > 0)
    {
      $this->db->where('event_id',$intEventId);
      $this->db->update('activity_permission',$arrData);
    }
    else
    {
      $arrData['event_id']=$intEventId;
      $this->db->insert('activity_permission',$arrData); 
    }
  }

  public function update_socialwall($event_id,$arrData)
  {
  
   $this->db->select('*');
   $this->db->from('event_settings');
   $this->db->where('event_id',$event_id);
   $query=$this->db->get()->result_array();
      
   if(count($query)>0)
   {
      $this->db->where('event_id',$event_id);
      $this->db->update('event_settings',$arrData);
   }
  else{
      $arrData['event_id']=$intEventId;
      $this->db->insert('event_settings',$arrData);
   }


  }
  

    public function get_socialwallData($event_id)
  {
  
   $this->db->select('*');
   $this->db->from('event_settings');
   $this->db->where('event_id',$event_id);
   $query=$this->db->get()->result_array();
   return $query;   

  }

  public function getshow_on_homescreen($id)
  {

    $this->db->select('e.activity_feed_on_homescreen');
    $this->db->from('event e');
    $this->db->where('e.Id', $id);
    $query = $this->db->get();  
    $res=$query->row_array();
    return $res;

    /*echo "<pre>";
    print_r($res); exit();*/
  }

  public function get_activity_share_icon_setting($id)
  {

    $this->db->select('e.activity_share_icon_setting');
    $this->db->from('event e');
    $this->db->where('e.Id', $id);
    $query = $this->db->get();  
    $res=$query->row_array();
    return $res;
  }


  public function update_show_on_homescreen($intEventId = 0,$arrData)
  {

    $this->db->where('Id',$intEventId);
    $this->db->update('event',$arrData);
  }

  public function update_activity_share_icon_setting($intEventId = 0,$arrData)
  {

    $this->db->where('Id',$intEventId);
    $this->db->update('event',$arrData);
  }

  public function checkEventDateTimeFormat($event_id,$date,$time)
  {
      $this->db->select('e.date_format,f.format_time');
      $this->db->from('event e');
      $this->db->join('fundraising_setting f','f.Event_id = e.Id');
      $this->db->where('e.Id',$event_id);
      $query = $this->db->get();  
      $res=$query->row_array();
      $date_format = ($res['date_format'] == '0') ? "d/m/Y":"m/d/Y";
      $time_format = ($res['format_time'] == '0') ? "h:ia":"H:i";
      $date =  date($date_format,strtotime($date));
      $time =  date($time_format,strtotime($time));
      $data = $time." ".$date;
      return $data;
  }
    public function remove_activityfeed($eid,$id,$activity_no)
    {
        if($activity_no=='1')
        {
            $this->db->where('Id',$id);
            $this->db->delete('speaker_msg');
        }
        else if($activity_no=='2')
        {
            $this->db->where('Id',$id);
            $this->db->delete('feed_img');
        }
        else if($activity_no=='6')
        {
            $this->db->where('id',$id);
            $this->db->delete('activity');
        }
        else if($activity_no=='9')
        {
            $this->db->where('id',$id);
            $this->db->delete('feed_comment');
        }
        else if($activity_no=='10')
        {
            $this->db->where('id',$id);
            $this->db->delete('speaker_comment');
        }
        else
        {
        }
    }
    public function getSurvey($event_id,$id=NULL)
    {   
        $this->db->where('event_id',$event_id);
        if($id)
            $this->db->where('Id',$id);
        $res = $this->db->get('activity_survey')->result_array();
        // j($res);
        return $id ? $res[0] : $res;
    }
    public function addSurveyQuestion($data)
    {   
        $this->db->insert('activity_survey',$data);
    }
    public function changeQuestionStatus($id)
    {   
        $res = $this->db->where('Id',$id)->get('activity_survey')->row_array();
        $status = $res['show_question'] == '0' ? '1' : '0';
        $update['show_question'] = $status;
        $this->db->where($res)->update('activity_survey',$update);
        return $status; 
    }
    public function updateSurveyQuestion($where,$update)
    {
        $this->db->where($where);
        $this->db->update('activity_survey',$update);
    }
    public function deleteSurvey($id)
    {
        $this->db->where('Id',$id)->delete('activity_survey');
    }

    public function export($event_id)
    {   
        $this->load->dbutil();
        $this->db->select('Question,answer as Answer,Firstname,Lastname,Title,Company_name,Email');
        $this->db->join('activity_survey s','s.Id = asa.question_id');
        $this->db->join('user u','u.Id = asa.user_id');
        $this->db->where('asa.event_id',$event_id);
        $this->db->order_by('s.Id');
        $res = $this->db->get('activity_survey_ans asa');

        $backup=$this->dbutil->csv_from_result($res);
        $this->load->helper('file');
        write_file('surveys_answer_user.csv', $backup); 
        $this->load->helper('download');
        force_download('surveys_answer_user.csv', $backup);
    }
    public function addSponsoredPost($data)
    {
        $this->db->insert('activity_advert',$data);
    }
    public function getAdvert($event_id,$id)
    {
        $this->db->where('event_id',$event_id);
        if(!empty($id))
            $this->db->where('id',$id);
        $res = $this->db->get('activity_advert')->result_array();

        return ($id) ? $res[0] : $res;
    }
    public function editSponsoredPost($where,$update)
    {   
        $this->db->where($where);
        $this->db->update('activity_advert',$update);
    }
    public function deleteAdvert($id,$event_id)
    {   
        $this->deleteAdvertImage($event_id,$id);
        $this->db->where('id',$id)->delete('activity_advert');
    }
    public function deleteAdvertImage($event_id,$id)
    {   
        $res = $this->db->where('id',$id)->get('activity_advert')->row_array();
        unlink("./assets/activity_advert/".$event_id."/".$res['image']);
    }
}
        
?>
