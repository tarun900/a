<?php

class Sponsors_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }

     public function check_auth($title, $roleid, $rolename,$eventid)
     {
          if ($rolename == 'Client')
          {
               return 1;
          }
          else
          {
             $this->db->select('id');
             $this->db->from('menu');
             $this->db->where('pagetitle', $title);
             $query = $this->db->get();
             $res = $query->result_array();
             $menuid = $res[0]['id'];
             
             $this->db->select('Menu_id');
             $this->db->from('role_permission');
             $this->db->where('Role_id', $roleid);
             $this->db->where('Menu_id', $menuid);
             $this->db->where('Event_id', $eventid);
             $query1 = $this->db->get();
             $res1=$query1->result_array();
             
             if(count($res1)>=1)
             {
                  return 1;
             }
             else
             {
                  return 0;
             }
          }
     }

    public function get_sponsors_list($eventid)
    {
          $this->db->select('e.*,u.Email');
          $this->db->from('sponsors e');
          $this->db->join('user u','e.user_id=u.id','left');
          $this->db->where('e.Event_id',$eventid);
          $qry=$this->db->get();
          $res=$qry->result_array();
          return $res;

     }

     public function add_user_as_sponsors($data)
     {
          if (!empty($data['sponsors_array']['company_logo']))
          {
               $company_logo[] = $data['sponsors_array']['company_logo'];
          }
          $array_sponsors['company_logo'] = json_encode($company_logo);

          if (!empty($data['sponsors_array']['Images']))
          {
               foreach ($data['sponsors_array']['Images'] as $k => $v)
               {
                    $Images[] = $v;
               }
          }
          $array_sponsors['Images'] = json_encode($Images);

          if ($data['sponsors_array']['Organisor_id'] != NULL)
               $array_sponsors['Organisor_id'] = $data['sponsors_array']['Organisor_id'];

          if ($data['sponsors_array']['Event_id'] != NULL)
               $array_sponsors['Event_id'] = $data['sponsors_array']['Event_id'];

          if ($data['sponsors_array']['Sponsors_name'] != NULL)
               $array_sponsors['Sponsors_name'] = $data['sponsors_array']['Sponsors_name'];

          if ($data['sponsors_array']['Company_name'] != NULL)
               $array_sponsors['Company_name'] = $data['sponsors_array']['Company_name'];

          if ($data['sponsors_array']['Description'] != NULL)
               $array_sponsors['Description'] = $data['sponsors_array']['Description'];

          if ($data['sponsors_array']['website_url'] != NULL)
               $array_sponsors['website_url'] = $data['sponsors_array']['website_url'];

          if ($data['sponsors_array']['facebook_url'] != NULL)
               $array_sponsors['facebook_url'] = $data['sponsors_array']['facebook_url'];

          if ($data['sponsors_array']['twitter_url'] != NULL)
               $array_sponsors['twitter_url'] = $data['sponsors_array']['twitter_url'];

          if ($data['sponsors_array']['linkedin_url'] != NULL)
               $array_sponsors['linkedin_url'] = $data['sponsors_array']['linkedin_url'];

          if ($data['sponsors_array']['youtube_url'] != NULL)
            $array_sponsors['youtube_url'] = $data['sponsors_array']['youtube_url'];
               

          if ($data['sponsors_array']['instagram_url'] != NULL)
            $array_sponsors['instagram_url'] = $data['sponsors_array']['instagram_url'];        


           if ($data['sponsors_array']['st_id'] != NULL)
            $array_sponsors['st_id'] = $data['sponsors_array']['st_id'];   


          $array_sponsors['user_id']=$data['sponsors_array']['user_id'];
          $this->db->insert('sponsors', $array_sponsors);
     } 

     public function get_sponsors_by_id($id = null, $eid = null)
     {
          $total_permission = $this->Exibitor_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;

          $orid = $this->data['user']->Id;
          $this->db->select('s.*');
          $this->db->from('sponsors s');
          $this->db->where('s.Event_id', $id);
          $this->db->where('s.Id', $eid);
          $query = $this->db->get();
          $res = $query->result_array();
          if(!$res && $this->uri->segment(2)=='page_edit')
            return redirect('Forbidden');
          return $res;
     }

     public function update_sponsors($data)
     {

          if (!empty($data['sponsors_array']['company_logo']))
          {
               $company_logo[] = $data['sponsors_array']['company_logo'];
               $array_sponsors['company_logo'] = json_encode($company_logo);
          }

          $images = array();
          if (!empty($data['sponsors_array']['Images']))
          {
               foreach ($data['sponsors_array']['Images'] as $k => $v)
               {
                    $images[] = $v;
               }
               
          }
          $array_sponsors['Images'] = json_encode($images);

          if (!empty($data['sponsors_array']['old_images']))
          {
               foreach ($data['sponsors_array']['old_images'] as $k => $v)
               {
                    $images[] = $v;
               }   
          }
          $array_sponsors['Images'] = json_encode($images);

          if ($data['sponsors_array']['Sponsors_name'] != NULL)
            $array_sponsors['Sponsors_name'] = $data['sponsors_array']['Sponsors_name'];

          if ($data['sponsors_array']['Company_name'] != NULL)
               $array_sponsors['Company_name'] = $data['sponsors_array']['Company_name'];

          if ($data['sponsors_array']['Description'] != NULL)
               $array_sponsors['Description'] = $data['sponsors_array']['Description'];

          $array_sponsors['website_url'] = $data['sponsors_array']['website_url'];
          $array_sponsors['facebook_url'] = $data['sponsors_array']['facebook_url'];
          $array_sponsors['twitter_url'] = $data['sponsors_array']['twitter_url'];
          $array_sponsors['linkedin_url'] = $data['sponsors_array']['linkedin_url'];
          $array_sponsors['youtube_url'] = $data['sponsors_array']['youtube_url'];
          $array_sponsors['instagram_url'] = $data['sponsors_array']['instagram_url'];   

          if ($data['sponsors_array']['user_id'] != NULL)
               $array_sponsors['user_id'] = $data['sponsors_array']['user_id'];  



          $array_sponsors['st_id'] = $data['sponsors_array']['st_id']!=NULL ? $data['sponsors_array']['st_id'] : NULL;   
             

          $this->db->where('Id', $data['sponsors_array']['Id']);
          $this->db->update('sponsors', $array_sponsors);
     }

     public function get_permission_list()
     {
          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $this->db->select('*');
               $this->db->from('user');
               if ($orid)
               {
                    $this->db->where('user.Id', $orid);
               }
               $query = $this->db->get();
               $res = $query->result();
               return $res;
          }
     }

     public function delete_sponsors($id,$Event_id)
     {
          $this->db->where('Id', $id);
          $this->db->delete('sponsors');
          $str = $this->db->last_query();

          if($Event_id!='')
          {
             $a_cat = $this->db->select('*')->from('sponsors')->where('Event_id',$Event_id)->order_by('Id','DESC')->get()->row_array();
             $update['updated_date'] = date('Y-m-d H:i:s',strtotime('+1 seconds'.$a_cat['updated_date']));
             $this->db->where('Id',$a_cat['Id'])->update('sponsors',$update);
          }
     }

     public function get_sponsors_list_byevent($eventid,$user_id)
     {
          $this->db->select('user.Id,user.Firstname,user.Lastname,user.Email,user.Active,count(s.user_id) as page_cnt,s.id as eid');
          $this->db->from('user');
          $this->db->join('sponsors s', 's.user_id = user.Id','left');  
          $this->db->where("user.Id",$user_id);
          $query = $this->db->get();
          $res=$query->result_array();
          return $res;
     }

     public function get_menu_name($id)
     {

          $orid = $this->data['user']->Id;
          $this->db->select('*');
          $this->db->from('menu_Setting');
          $this->db->where('Event_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
	 public function get_sponsors_type_list($id)
     {
        $this->db->select('*')->from('sponsors_type');
        $this->db->where('event_id',$id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
     }
     public function get_edit_sponsors_type($id,$tid)
     {
        $this->db->select('*')->from('sponsors_type');
        $this->db->where('event_id',$id);
        $this->db->where('type_id',$tid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(!$res && $this->uri->segment(2)=='sponsors_type_edit')
          return redirect('Forbidden');
        return $res;
     }
     public function save_sponsor_type($typedata)
     {
        $typedata['type_ucode']=substr(sha1(uniqid()),0,5);
        $this->db->insert('sponsors_type',$typedata);
        return $this->db->insert_id();
     }
     public function remove_sponsors_type($id,$tid)
     {
        $this->db->where('event_id',$id);
        $this->db->where('type_id',$tid);
        $this->db->delete('sponsors_type');

        $st_data['st_id']=NULL;
        $this->db->where('st_id',$tid);
        $this->db->update('sponsors',$st_data);

        if($id!='')
        {
           $a_cat = $this->db->select('*')->from('sponsors_type')->where('event_id',$id)->order_by('type_id','DESC')->get()->row_array();
           $update['updated_date'] = date('Y-m-d H:i:s',strtotime('+1 seconds'.$a_cat['updated_date']));
           $this->db->where('type_id',$a_cat['type_id'])->update('sponsors_type',$update);
        }
     }
     public function update_sponsor_type($tdata,$eid,$tid)
     {
        $this->db->where('event_id',$eid);
        $this->db->where('type_id',$tid);
        $this->db->update('sponsors_type',$tdata);
     }
     public function get_sponsors_type_id_by_type_code($tcode,$eid)
     {
        $this->db->select('*')->from('sponsors_type');
        $this->db->where('event_id',$eid);
        $this->db->where('type_ucode',$tcode);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res[0]['type_id'];
     }
     public function add_sponsors_categorie($cdata) //Monday 26 March 2018 06:08:00 PM IST
     {
          $cdata['created_date'] = date('Y-m-d H:i:s');
          $this->db->insert('sponsors_category', $cdata);
          return $this->db->insert_id();
     }

     public function update_sponsors_categorie($cdata, $cid) //Monday 26 March 2018 06:08:05 PM IST
     {
          $this->db->where('id', $cid);
          $this->db->update('sponsors_category', $cdata);
     }

     public function get_sponsors_categories_by_event($eid, $cid = NULL) //Monday 26 March 2018 06:08:10 PM IST
     {
          $this->db->select('*')->from('sponsors_category');
          if (!empty($cid))
          {
               $this->db->where('id', $cid);
          }
          $this->db->where('menu_id', '7');
          $this->db->where('event_id', $eid);
          $res = $this->db->get()->result_array();
          return $res;
     }

     public function delete_sponsors_categorie($cid, $event_id = '') //Monday 26 March 2018 06:08:15 PM IST
     {
          $this->db->where('id', $cid);
          $this->db->delete('sponsors_category');
          if ($event_id != '')
          {
               $a_cat = $this->db->select('*')->from('sponsors_category')->where('event_id', $event_id)->order_by('updated_date', 'DESC')->get()->row_array();
               $update['updated_date'] = date('Y-m-d H:i:s');
               $this->db->where('id', $a_cat['id'])->update('sponsors_category', $update);
          }
     }

     public function assign_sponsors_categories($insert_data, $event_id)
     {     
          $keywords = explode(',', $insert_data['keyword']);
          
          foreach($keywords as $key => $value)
          {
               $insert['keyword'] = $value;
               $insert['user_id'] = $insert_data['user_id'];
               $insert['event_id'] = $event_id;
               $res = $this->db->where($insert)->get('sponsors_keywords')->row_array();
               if (empty($res))
               {
                    $this->db->insert('sponsors_keywords', $insert);
               }
          }
     }
}

?>
