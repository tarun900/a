<?php
class Notification_model extends CI_Model{
	function __construct()
	{
	    parent::__construct();
      $this->db1 = $this->load->database('db1', TRUE);
	}
  public function get_all_notification_by_event_id($id)
  {
    $this->db->select('*')->from('notification');
    $this->db->where_in('notification_type',array('1','2'));
    $this->db->where("event_id",$id);
    $res = $this->db->get()->result_array();
    return $res;
  }
  public function getGeoNoti($id)
  {
    $this->db->select('*')->from('notification');
    $this->db->where('notification_type','3');
    $this->db->where("event_id",$id);
    $res = $this->db->get()->result_array();
    return $res;
  }
  public function get_all_attendees_by_event($event_id)
  {
    $this->db->select('u.*')->from('user u');
    $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
    $this->db->where('reu.Role_id','4');
    $this->db->where('reu.Event_id',$event_id);
    $res=$this->db->get()->result_array();
    return $res;
  }

  public function get_all_attendees_by_event_new($event_id)
  {
    $this->db->select('u.*')->from('user u');
    $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
    $this->db->where('reu.Role_id','4');
    $this->db->where('reu.Event_id',$event_id);
    $this->db->where('u.Unique_no',NULL);
    $this->db->where('u.Login_date IS NOT NULL', null, false);
    $res=$this->db->get()->result_array();
    return $res;
  }

  public function get_edit_notification_data_by_id($eid,$nid)
  {
    $this->db->select('*')->from('notification');
    $this->db->where("event_id",$eid);
    $this->db->where('Id',$nid);
    $res = $this->db->get()->result_array();
    if(!$res)
          return redirect('Forbidden');
    return $res;
  }
	public function get_scedulednotification_list($id)
	{
		$this->db->select('');
		$this->db->from('notification');
    $this->db->where("event_id",$id);
    $this->db->where("notification_type",'2');
    $query = $this->db->get();
		$res = $query->result_array();
    return $res;
	}
        
  public function get_sentnotification_list($id)
	{
		$this->db->select('');
		$this->db->from('notification');
                $this->db->where("event_id",$id);
                $this->db->where("notification_type",'1');
                $query = $this->db->get();
		$res = $query->result_array();
                return $res;
	}
        
  public function add_notification($data)
  {
    $data['created_at']=date('Y-m-d H:i:s');
    $this->db->insert('notification',$data);
    return $this->db->insert_id();
  }  
  public function get_notification_by_id($id)
  {
      $this->db->select('')->from('notification');
      $this->db->where('id',$id);
      $query = $this->db->get();
      $res = $query->result_array();
      return $res;
  }
  public function update_notification($id,$notificationid,$data)
  { 
      $this->db->from('notification');
      $this->db->where('id',$notificationid);
      $this->db->where('event_id',$id);
      $this->db->update('notification',$data);
  }
  public function delete_notification($id)
  {
      $this->db->where('id', $id);
      $this->db->delete('notification');
      
  }
   public function pushwEmail()
    {
        $this->db->select("notf.*");
        $this->db->from("notification notf");
        $this->db->join("event evt","notf.event_id = evt.Id");
        $this->db->where("notf.notification_type",2);
        /*$this->db->where("notf.send_email",'1');*/
        //$this->db->where("notf.datetime=NOW()",NULL,FALSE);
        $query = $this->db->get();
        $arrRes = $query->result_array();
        return $arrRes;
    }
  public function get_asign_notification_user_list($eid,$nid)
  {
    $this->db->protect_identifiers=false;
    $this->db->select('u.*,n.title,n.content')->from('user u');
    $where="find_in_set(u.Id,n.user_ids) > 0";
    $this->db->join('notification n',$where);
    $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$eid);
    $this->db->where('n.Id',$nid);
    $this->db->where('n.event_id',$eid);
    $res=$this->db->get()->result_array();
    return $res;
  }

  public function getAnalytics($event_id)
  {
    return $this->db->select('*,
      
      (select count(*) from notification_logs where status = "1" and notification_id = n.Id) as scount,
      (select count(*) from notification_logs where status = "0" and notification_id = n.Id) as fcount,
      (select count(*) from notification_logs where received_time != "NULL" and notification_id = n.Id) as recount,
      (select count(*) from notification_logs where open_time != "NULL" and notification_id = n.Id) as opcount,
      ',false)
            ->from('notification n')
            ->join('notification_logs nl','nl.notification_id = n.Id')
            ->where('n.event_id',$event_id)
            ->get()
            ->result_array();
  }
  public function getAnalyticsById($id)
  {
      return $this->db->select('*')->from('notification_logs nl')->join('user u','u.Id = nl.user_id')->where('notification_id',$id)->get()->result_array();
  }

}
      
?>
