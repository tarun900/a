<?php
class Profile_model extends CI_Model{
    function __construct()
    {
      $this->db1=$this->load->database('db1',true);
            parent::__construct();
    }

    public function getUserRole($id,$eventid)
    {
        $this->db->select('r.Name');
        $this->db->from('role r');
        $this->db->join('relation_event_user ru','ru.Role_id = r.Id');
        $this->db->where('ru.User_id',$id);
        $this->db->where('ru.Event_id',$eventid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getAllGcmID()
    {
        $this->db->select('u.gcm_id,u.device');
        $this->db->from('user u');
        $this->db->where('u.gcm_id !=','');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_profile_list($id=null,$eid=null)
    {
        $this->db->select('*,user.Active as Active,r.Name as Role_name,user.Id as Id,r.Id as Rid');
        $this->db->from('user');
        if($id)
        {
            $this->db->where('user.Id',$id);
        }
        //$this->db->where('user.Active','1');
        $this->db->join('relation_event_user ru','ru.User_id = user.Id');
        if(!empty($eid))
        {
          $this->db->where('ru.Event_id',$eid); 
        }
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
       return $res;
    }
        /**
        * This function will take the post data passed from the controller
        * If id is present, then it will do an update
        * else an insert. One function doing both add and edit.
        * @param $data
        */
      public function updateprofile($data,$user_soc=null) 
      {         
         $data['updated_date'] = date('Y-m-d H:i:s');
         if (isset($data['Id'])) 
         {
              $this->db->select('*');
              $this->db->from('user');
              $this->db->where('id',$data['Id']);
              $query=$this->db->get();
              $res=$query->result_array();
              $data1=$data;
              unset($data1['Id']);
              unset($data1['Salutation']);
              unset($data1['Title']);
              unset($data1['Speaker_desc']);
              unset($data1['Isprofile']);
              unset($data1['updated_date']);
              $this->db1->where('Email',$res[0]['Email']);
              $this->db1->update('organizer_user',$data1);

              $this->db->where('Id',$data['Id']);
              $this->db->update('user',$data); 
         }
         else 
         {
              $this->db->insert('user', $data);
         }

         $this->db->select('Id');
         $this->db->where('User_id',$user_soc['User_id']);
         $this->db->from('user_social_links');
         $query = $this->db->get();
         $res = $query->result_array();

         if(count($res)>0)
         {
              $this->db->where('User_id',$user_soc['User_id']);
              $this->db->update('user_social_links',$user_soc); 
         }
         else
         {
              if(!empty($user_soc))
              {
                $this->db->insert('user_social_links',$user_soc);
              }
         } 

      }
      public function getGcmID($id)
      {
          $this->db->select('gcm_id');
          $this->db->from('user u');
          $this->db->where('u.Id',$id); 
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
      }
      public function updateprofile_linkdin($data,$userid)
      {
        $this->db->where('Id',$userid);
        $this->db->update('user',$data); 
      }
      public function updateroledata($data,$id,$eventid)
      {
          $this->db->where('User_id',$id);
          $this->db->where('Event_id',$eventid);
          $this->db->update('relation_event_user',$data); 
      }
      public function get_social_links($id)
      {
        $this->db->select('');
        $this->db->where('User_id',$id);
        $this->db->from('user_social_links');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
      }
      public function statelist($id=null)
      {
          $this->db->select('id,state_name');
          $this->db->from('state');
          if($id)
          {
              $this->db->where('id',$id);
          }
          $query = $this->db->get();
          $res = $query->result();
          return $res;
      }
      public function countrylist($id=null)
      {
          $this->db->select('id,country_name');
          $this->db->from('country');
          $this->db->order_by("country_name", "ASC");
          $query = $this->db->get();
          $res = $query->result_array();
           
          $i=2;
          $j=0;
          $arr;
          foreach ($res as $key => $value) {
            if($value['id']==223 || $value['id']==225)
            {
                $arr[$j]=$value;
                $j++;
            }
            else
            {
                $arr[$i]=$value;
                $i++;
            }
          }
         
          ksort($arr);
          return $arr;
      }

      public function getstate($id_country=string,$flag=null)
      {
          $this->db->select('Id,state_name');
          $this->db->from('state');
          if(!$flag)
          {
              $this->db->where('country_id',$id_country); 
          }
          $this->db->order_by("state_name", "ASC");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
      }

      public function get_user_detail($id_user=string)
      {
          $this->db->select('*');
          $this->db->from('user u');
          $this->db->join('relation_event_user ru', 'ru.User_id = u.Id', 'left');
          $this->db->join('role r', 'ru.Role_id = r.Id', 'left');
          $this->db->join('country c', 'c.id = u.Country','left');
          $this->db->join('state s', 's.id = u.State','left');
          $this->db->where('u.Id',$id_user); 
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
      }

      /*public function add_session_time($data)
      {
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Organisor_id;
          $this->db->select('*');
          $this->db->from('session_setting');
          $this->db->where('organizer_id',$user[0]->Id);
          $query=$this->db->get();
          $res=$query->result_array();
          if(count($res) > 0)
          {
            if($data['session_array']['session_time'] != NULL)
            {
              $array_session['session_time'] = $data['session_array']['session_time'];
              $this->db->where('organizer_id', $logged_in_user_id);
              $this->db->update('session_setting', $array_session);
            }
          }
          else
          {
            $data=array("session_time"=>$data['session_array']['session_time'],
                        "organizer_id"=>$logged_in_user_id);
            $this->db->insert('session_setting',$data);
          }
      }*/
	  
      public  function update_front_user_by_id($data,$uid)
      {
        $this->db->where('Id',$uid);
        $this->db->update('user',$data);
      }
      public function getstate_by_country_name($cnm)
      {
        $this->db->select('*')->from('country');
        $this->db->where('country_name',$cnm);
        $cres=$this->db->get()->result_array();
        $this->db->select('*')->from('state');
        $this->db->where('country_id',$cres[0]['id']);
        $this->db->order_by("state_name", "ASC");
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
      }

}
?>