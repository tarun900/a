<?php
class Leader_board_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }
    public function get_userwise_click_tracking($event_id,$user_id)
    {
        $this->db->select('sum(ulb.menu_hit) as total_hit,ulb.menu_hit,m.menuname');
        $this->db->from('users_leader_board ulb');
        $this->db->join('menu m', 'm.id = ulb.menu_id');
        $this->db->where("ulb.user_id",$user_id);
        $this->db->group_by('ulb.menu_id');
        $this->db->order_by('total_hit','desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_userwise_survey_ans($id)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('concat(u.Firstname," ",u.Lastname) as username,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,count(*) as total_ans',FALSE);
        $this->db->from('poll_survey ps');
        $this->db->join('user u', 'u.Id = ps.User_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.user_id');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->group_by('ps.User_id');
        $this->db->order_by('total_ans','desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

     public function get_all_bids($eid)
     {
         $this->db->select('p.product_id,p.event_id,pub.*,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,sum(pub.bid_amt) as total_bid_amt,count(pub.bid_amt) as cnt')->from('product_user_bids pub');
          $this->db->join('product p','pub.product_id=p.product_id');
          $this->db->join('user u','u.Id=pub.user_id');
          $this->db->where('p.event_id',$eid);
          $this->db->group_by('pub.user_id');
          $this->db->order_by('cnt','desc');
          $qry=$this->db->get();
          $res=$qry->result_array();
          $my_arr;$i=0;
          foreach ($res as $key => $value) 
          {
              $userid=$value['user_id'];
              $this->db->select('sum(pub.bid_amt) as total_win_bid_amt,count(pub.win_status) as winner')->from('product_user_bids pub');
              $this->db->join('product p','pub.product_id=p.product_id');
              $this->db->where('p.event_id',$eid);
              $this->db->where('pub.user_id',$userid);
              $this->db->where('pub.win_status','1');
              $this->db->group_by('pub.user_id');
              $qry1=$this->db->get();
              $res1=$qry1->result_array();
              $my_arr[$i]['name']=$value['Firstname']." ".$value['Lastname'];
              $my_arr[$i]['Title']=$value['Title'];
              $my_arr[$i]['Company_name']=$value['Firstname'];
              $my_arr[$i]['Email']=$value['Email'];
              $my_arr[$i]['Street']=$value['Street'];
              $my_arr[$i]['Suburb']=$value['Suburb'];
              $my_arr[$i]['Postcode']=$value['Postcode'];
              $my_arr[$i]['total_bid']=$value['cnt'];
              $my_arr[$i]['total_bid_amt']=$value['total_bid_amt'];
              $my_arr[$i]['total_win']=$res1[0]['winner'];
              $my_arr[$i]['total_win_amt']=$res1[0]['total_win_bid_amt'];
              $i++;
          }
          return $my_arr;
     }

    public function get_current_byevent($event_id)
     {
          $this->db->select('currency')->from('fundraising_currency');
          $this->db->where('event_id', $event_id);
          $qry = $this->db->get();
          $res = $qry->result_array();
          $currency = $res[0]['currency'];
          $currency = explode("#", $currency);
          return $currency;
     }
    public function get_all_userwise_notes($id)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('concat(u.Firstname," ",u.Lastname) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,count(*) as total_notes',FALSE);
        $this->db->from('notes nt');
        $this->db->join('user u', 'u.Id = nt.User_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.user_id');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->group_by('nt.User_id');
        $this->db->order_by('total_notes','desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_click_tracking($id=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('concat(u.Firstname," ",u.Lastname) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,sum(ulb.menu_hit) as total_hit,ulb.date as hit_date,ulb.menu_hit,u.Id',FALSE);
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.user_id');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }
    public function get_advert_click_tracking($event_id)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('concat(u.Firstname," ",u.Lastname) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,sum(ulb.menu_hit) as total_hit',FALSE);
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.user_id');
        $this->db->join('menu m', 'm.id = ulb.menu_id');
        $this->db->where("reu.Event_id",$event_id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("m.id",5);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_msg_tracking($id=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('u.Id,concat(u.Firstname," ",u.Lastname) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,sum(ulb.msg_hit) as total_hit',FALSE);
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.User_id');
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $name="";
        $this->db->where('ulb.received_id !=',$name);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_comments_tracking($id=null)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('concat(u.Firstname," ",u.Lastname) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,sum(ulb.comment_hit) as total_hit',FALSE);
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.User_id');
        $name="";
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $this->db->where('ulb.comment_id !=',$name);
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_leader_board_list($id=null)
    {
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if($id)
        {
            $this->db->where('u.Id',$id);

        }

        $this->db->where('r.Name','Leader_board');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
        
        public function add_leader_board($array_add)
        {
            $this->db->select('*');
            $this->db->from('role');
            $this->db->where('Name','Leader_board');
            $query = $this->db->get();
            $result = $query->result_array();
            $array_add['Role_id'] = $result[0]['Id'];
            $array_add['Password'] = md5($array_add['Password']);
            $this->db->insert('user', $array_add);
            $this->session->set_flashdata('notes_data', 'Added');
            redirect('notes');
        }
            
        public function delete_leader_board($id)
        {
            $this->db->where('Id', $id);
            $this->db->delete('user');
            $str = $this->db->last_query();
        }
        
    public function get_recent_leader_board_list($id=null)
    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if($id)
        {
            $this->db->where('u.Id',$id);  
        }
        $this->db->where('r.Name','Leader_board');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_click_tracking_chart($id = NULL)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('sum(ulb.menu_hit) as total_hit,u.Id,u.Firstname,u.Lastname');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu', 'u.Id = reu.User_id');
        $this->db->join('users_leader_board ulb', 'u.Id = ulb.user_id');
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $this->db->where("reu.Event_id",$id);
        $this->db->where("reu.Role_id",4);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_message_tracking_chart($id = NULL)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('sum(ulb.msg_hit) as total_hit,u.Firstname,u.Lastname');
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.User_id');
        $name="";
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $this->db->where("reu.Event_id",$id);
        $this->db->where('ulb.received_id !=',$name);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->where("reu.Role_id",4);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }
    public function getmsgcsvdata($eid,$uid)
    {
        $this->db->select('sum(ulb.msg_hit) as total_hit,u.Id,u.Firstname,u.Lastname');
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u','u.Id = ulb.user_id','left');
        $this->db->where('ulb.user_id',$uid);
        $name="";
        $this->db->where('ulb.received_id !=',$name);
        $query = $this->db->get();  
        $res = $query->result_array();

        $this->db->select('sum(ulb.msg_hit) as total_hit,ulb.msg_hit,ulb.received_id,u.Firstname,u.Lastname');
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u','u.Id = ulb.received_id','left');
        $this->db->where('ulb.user_id',$uid);
        $name="";
        $this->db->where('ulb.received_id !=',$name);
        $this->db->group_by('ulb.received_id');
        $query = $this->db->get();  
        $res[0]['Receive'] = $query->result_array();
        return $res;
    }
    public function get_comment_tracking_chart($id = NULL)
    {
        $user = $this->session->userdata('current_user');
        $this->db->select('sum(ulb.comment_hit) as total_hit,u.Firstname,u.Lastname');
        $this->db->from('users_leader_board ulb');
        $this->db->join('user u', 'u.Id = ulb.user_id');
        $this->db->join('relation_event_user reu', 'u.Id = reu.User_id');
        $name="";
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $this->db->where("reu.Event_id",$id);
        $this->db->where('ulb.comment_id !=',$name);
        $this->db->where("u.Id != ",$user[0]->Organisor_id);
        $this->db->where("reu.Role_id",4);
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_hit desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
     public function get_all_agenda_list($eid,$limit = NULL,$start=0,$keyword = '')
    {
        $this->db->select('a.*,count(usr.user_id) as no_user,sum(usr.rating) as total_rating,count(ac.user_id) as total_comment')->from('agenda a');
        $this->db->join('agenda_category_relation acare','acare.agenda_id=a.Id','right');
        $this->db->join('user_session_rating usr','usr.session_id=a.Id','left');
        $this->db->join('agenda_comments ac','ac.agenda_id=a.Id','left');
        $this->db->where('a.Event_id',$eid);
        if($keyword!='')
        {
            $where = "(a.Heading like '%".$keyword."%')";
            $this->db->where($where);
        }
        $this->db->group_by('a.Id');
        if($limit!=NULL)
            $this->db->limit($limit,$start);

        $query = $this->db->get();
        $res = $query->result_array();
        foreach ($res as $key => $value) 
        {
            $aid=$value['Id'];
            $this->db->select('count(ua.user_id) as total_save,agenda_id')->from('users_agenda ua');
            $this->db->join('relation_event_user reu','ua.user_id=reu.User_id','left');
            $where = "FIND_IN_SET($aid,`ua`.`agenda_id`) > 0";
            $this->db->where($where);
            $this->db->where('reu.Event_id',$eid);
            $save=$this->db->get();
            $saveres=$save->result_array();
            $res[$key]['total_save']=$saveres[0]['total_save'];

            $this->db->select('count(ua.user_id) as total_check,agenda_id')->from('users_agenda ua');
            $this->db->join('relation_event_user reu','ua.user_id=reu.User_id','left');
            $where = "FIND_IN_SET($aid,`ua`.`check_in_agenda_id`) > 0";
            $this->db->where($where);
            $this->db->where('reu.Event_id',$eid);
            $check=$this->db->get();
            $checkres=$check->result_array();
            $res[$key]['total_check']=$checkres[0]['total_check'];
        } 
        return $res;
    }
    public function get_all_agenda_list_total($eid,$keyword='')
    {
        $this->db->select('a.*,count(usr.user_id) as no_user,sum(usr.rating) as total_rating,count(ac.user_id) as total_comment')->from('agenda a');
        $this->db->join('agenda_category_relation acare','acare.agenda_id=a.Id','right');
        $this->db->join('user_session_rating usr','usr.session_id=a.Id','left');
        $this->db->join('agenda_comments ac','ac.agenda_id=a.Id','left');
        $this->db->where('a.Event_id',$eid);
        if($keyword!='')
        {
            $where = "(a.Heading like '%".$keyword."%')";
            $this->db->where($where);
        }
        $this->db->group_by('a.Id');
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function get_agenda_details($eid,$aid,$limit=null,$start = 0)
    {
        /*$this->db->select('u.*,ea.extra_column,c.country_name as Country')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        
        $qy=$this->db->get();
        $res=$qy->result_array();
        
        foreach ($res as $key => $value) {
            $this->db->select('*')->from('users_agenda ua');
            $this->db->where('user_id',$value['Id']);
            $where = "FIND_IN_SET(".$aid.",ua.agenda_id) > 0";
            $this->db->where($where);
            $quu=$this->db->get();
            $users_agenda=$quu->result_array();

            $this->db->select('*')->from('users_agenda ua');
            $this->db->where('user_id',$value['Id']);
            $where = "FIND_IN_SET(".$aid.",ua.check_in_agenda_id) > 0";
            $this->db->where($where);
            $quu=$this->db->get();
            $users_checki=$quu->result_array();

            $this->db->select('*')->from('user_session_rating usr');
            $this->db->where('user_id',$value['Id']);
            $this->db->where('session_id',$aid);
            $ra=$this->db->get();
            $rat=$ra->result_array();
            if(count($users_agenda)<=0 && count($rat)<=0 && count($users_checki)<=0)
            {
                unset($res[$key]);
            }
            else
            {
                if(count($rat)>0)
                {
                    $res[$key]['rating']=$rat[0]['rating'];
                }
                if(count($users_agenda)>0)
                {
                    $res[$key]['agenda_id']=$users_agenda[0]['agenda_id'];
                    $res[$key]['check_in_agenda_id']=$users_agenda[0]['check_in_agenda_id'];
                }
                if(count($users_checki)>0)
                {
                    $res[$key]['agenda_id']=$users_checki[0]['agenda_id'];
                    $res[$key]['check_in_agenda_id']=$users_checki[0]['check_in_agenda_id'];
                }
            }
        }
        return $res;*/

        $this->db->select('u.*,ea.extra_column,c.country_name as Country,au.*')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('users_agenda au','au.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $where = "FIND_IN_SET(".$aid.",au.agenda_id) > 0";
        $this->db->where($where);
        $qy=$this->db->group_by('u.Id')->get();
        $c1 = $qy->result_array();


        $this->db->select('u.*,ea.extra_column,c.country_name as Country,au.*')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('users_agenda au','au.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $where = "FIND_IN_SET(".$aid.",au.check_in_agenda_id) > 0";
        $this->db->where($where);
        $qy=$this->db->group_by('u.Id')->get();
        $c2 = $qy->result_array();


        $this->db->select('u.*,ea.extra_column,c.country_name as Country,usr.*')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('user_session_rating usr','usr.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where("usr.session_id",$aid);
        $qy=$this->db->group_by('u.Id')->get();
        $c3 = $qy->result_array();
        $result = array_merge($c1,$c2,$c3);
        if($limit!=null)
            return array_slice($result,$start,$limit);
        else
            return $result;
    }
    public function get_agenda_details_total($eid,$aid)
    {

        /*$this->db->select('u.*,ea.extra_column,c.country_name as Country')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $qy=$this->db->get();
        $res=$qy->result_array();
        foreach ($res as $key => $value) {
            $this->db->select('*')->from('users_agenda ua');
            $this->db->where('user_id',$value['Id']);
            $where = "FIND_IN_SET(".$aid.",ua.agenda_id) > 0";
            $this->db->where($where);
            $quu=$this->db->get();
            $users_agenda=$quu->result_array();

            $this->db->select('*')->from('users_agenda ua');
            $this->db->where('user_id',$value['Id']);
            $where = "FIND_IN_SET(".$aid.",ua.check_in_agenda_id) > 0";
            $this->db->where($where);
            $quu=$this->db->get();
            $users_checki=$quu->result_array();

            $this->db->select('*')->from('user_session_rating usr');
            $this->db->where('user_id',$value['Id']);
            $this->db->where('session_id',$aid);
            $ra=$this->db->get();
            $rat=$ra->result_array();
            if(count($users_agenda)<=0 && count($rat)<=0 && count($users_checki)<=0)
            {
                unset($res[$key]);
            }
            else
            {
                if(count($rat)>0)
                {
                    $res[$key]['rating']=$rat[0]['rating'];
                }
                if(count($users_agenda)>0)
                {
                    $res[$key]['agenda_id']=$users_agenda[0]['agenda_id'];
                    $res[$key]['check_in_agenda_id']=$users_agenda[0]['check_in_agenda_id'];
                }
                if(count($users_checki)>0)
                {
                    $res[$key]['agenda_id']=$users_checki[0]['agenda_id'];
                    $res[$key]['check_in_agenda_id']=$users_checki[0]['check_in_agenda_id'];
                }
            }
        }
        return count($res);*/
        $this->db->select('u.Id')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('users_agenda au','au.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $where = "FIND_IN_SET(".$aid.",au.agenda_id) > 0";
        $this->db->where($where);
        $qy=$this->db->group_by('u.Id')->get();
        $c1 = $qy->num_rows();


        $this->db->select('u.Id')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('users_agenda au','au.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $where = "FIND_IN_SET(".$aid.",au.check_in_agenda_id) > 0";
        $this->db->where($where);
        $qy=$this->db->group_by('u.Id')->get();
        $c2 = $qy->num_rows();


        $this->db->select('u.Id')->from('user u');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('relation_event_user reu','reu.user_id=u.Id');
        $this->db->join('user_session_rating usr','usr.user_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where("usr.session_id",$aid);
        $qy=$this->db->group_by('u.Id')->get();
        $c3 = $qy->num_rows();

        return $c1+$c2+$c3;
    }
    public function get_all_metting_by_event_id($id)
    {
        $this->db->select('eam.*,(case when e.Heading IS NULL THEN concat(IFNULL(u2.Firstname,"")," ",IFNULL(u2.Lastname,"")) ELSE e.Heading end) as Heading,u.Firstname,u.Lastname',FALSE)->from('exhibitor_attendee_meeting eam');
        $this->db->join('exibitor e','eam.exhibiotor_id=e.Id','left');
        $this->db->join('user u2','eam.recever_attendee_id=u2.Id','left');
        $this->db->join('user u','eam.attendee_id=u.Id','left');
        $this->db->where('eam.event_id',$id);
        $ra=$this->db->get();
        $rat=$ra->result_array();
        return $rat;
    }
    public function get_all_comment_by_agenda_id($eid,$aid)
    {
        $this->db->select('*')->from('user u');
        $this->db->join('agenda_comments ac','u.Id=ac.user_id','right');
        $this->db->where('event_id',$eid);
        $this->db->where('agenda_id',$aid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_agenda_session_rating_by_event($eid)
    {
        $this->db->select('usr.user_id,a.Id as agenda_id,a.Heading,usr.rating,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email')->from('agenda a');
        $this->db->join('user_session_rating usr','a.Id=usr.session_id','left');
        $this->db->join('user u','u.Id=usr.user_id','right');
        $this->db->where('a.Event_id',$eid);
        $this->db->where('usr.rating !=','0');
        $res=$this->db->get()->result_array();
        $demoarr=array();
        foreach ($res as $key => $value) {
            $demoarr[$value['user_id'].'-'.$value['agenda_id']]=$value;
        }
        $this->db->select('ac.user_id,a.Id as agenda_id,a.Heading,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email,ac.comments')->from('agenda a');
        $this->db->join('agenda_comments ac','a.Id=ac.agenda_id','left');
        $this->db->join('user u','u.Id=ac.user_id','right');
        $this->db->where('a.Event_id',$eid);
        $res2=$this->db->get()->result_array();
        foreach ($res2 as $key => $value) {
            $intKey = $value['user_id'].'-'.$value['agenda_id'];
            if(array_key_exists($intKey, $demoarr))
            {
                $demoarr[$intKey]['comments'] = $value['comments'];
            }
            else
            {
                $demoarr[$intKey] = $value;
            }
        }
        return $demoarr;
    }
    public function get_total_register_user_by_event($id,$orid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('count(User_id) as total_user')->from('relation_event_user reu');
        $this->db->join('user u','u.Id=reu.User_id','left');
        $this->db->where('reu.Event_id',$id);
        $this->db->where('reu.User_id !=',$orid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('u.Created_date BETWEEN "'.$fdatetime.'" AND "'.$todatetime.'"',NULL,false);
        }
        if($limit!=NULL)
            $this->db->limit($limit);
        $rqu=$this->db->get();
        $res=$rqu->result_array();
        return $res;
    }
    public function get_most_popular_areas_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('sum(ulb.menu_hit) as total_menu_hit,m.menuname,m.pagetitle,m.Id')->from('menu m ');
        $this->db->join('users_leader_board ulb','ulb.menu_id=m.Id','right');
        $this->db->where('event_id',$eid);
        $this->db->where('ulb.menu_id IS NOT NULL');
        $this->db->where('ulb.event_id IS NOT NULL');
        $this->db->where('m.menuname IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ulb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('ulb.menu_id');
        $this->db->order_by('total_menu_hit desc');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();

        $this->db->select('sum(ulb.menu_hit) as total_menu_hit,m.Menu_name as menuname,m.Menu_name as pagetitle,m.Id')->from('cms m ');
        $this->db->join('users_leader_board ulb','ulb.menu_id=m.Id','left');
        $this->db->where('m.Event_id',$eid);
        $this->db->where('ulb.menu_id IS NOT NULL');
        $this->db->where('ulb.event_id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ulb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('ulb.menu_id');
        $this->db->order_by('total_menu_hit desc');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res2=$qu->result_array();

        function sortByName($a, $b)
        {
            $a = $a['total_menu_hit'];
            $b = $b['total_menu_hit'];

            if ($a == $b) return 0;
            return ($a > $b) ? -1 : 1;
        }
        $new =  array_merge($res,$res2);
        
        usort($new, 'sortByName');
        return $new;
        

         
    }
    public function get_most_active_user_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('sum(ulb.menu_hit) as total_menu_hit,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as username,u.Title,u.Company_name,u.Email,u.Street,u.Suburb,u.Postcode,u.Logo,u.Id',FALSE)->from('users_leader_board ulb');
        $this->db->join('user u','ulb.user_id=u.Id','left');
        $this->db->where('event_id',$eid);
        $this->db->where('ulb.user_id IS NOT NULL');
        $this->db->where('ulb.event_id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ulb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('ulb.user_id');
        $this->db->order_by('total_menu_hit desc');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_most_login_operating_system_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select("case when u.device!='' THEN u.device ELSE 'Web' End as device_name,count(*) as total_login_user,u.Id",FALSE)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->where('reu.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('u.Login_date BETWEEN "'.$fdatetime.'" AND "'.$todatetime.'"',NULL,false);
        }
        $this->db->group_by('device_name');
        $this->db->order_by('total_login_user DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_top_rated_session_by_event($eid,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select('a.*,(sum(usr.rating)/count(usr.user_id)) as total_rating')->from('agenda a');
        $this->db->join('user_session_rating usr','a.Id=usr.session_id','left');
        $this->db->where('a.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('usr.date BETWEEN "'.$fdatetime.'" AND "'.$todatetime.'"',NULL,false);
        }
        $this->db->group_by('a.Id');
        $this->db->order_by('total_rating DESC');
        $this->db->limit(5);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_most_popular_session_by_event($eid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('a.*,count(ua.user_id) as total_save',FALSE)->from('agenda a');
        $this->db->join("users_agenda ua","FIND_IN_SET(a.Id,ua.agenda_id) > 0","left");
        $this->db->join('relation_event_user reu','ua.user_id=reu.User_id','left');
        $this->db->where('a.Event_id',$eid);
        $this->db->where('reu.Event_id',$eid);
        $this->db->group_by('a.Id');
        $this->db->order_by('total_save DESC');
        $this->db->limit(5);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_most_check_in_session_by_event($eid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('a.*,count(ua.user_id) as total_checkin',FALSE)->from('agenda a');
        $this->db->join("users_agenda ua","FIND_IN_SET(a.Id,ua.check_in_agenda_id) > 0","left");
        $this->db->join('relation_event_user reu','ua.user_id=reu.User_id','left');
        $this->db->where('a.Event_id',$eid);
        $this->db->where('reu.Event_id',$eid);
        $this->db->group_by('a.Id');
        $this->db->order_by('total_checkin DESC');
        $this->db->limit(5);
        $qu=$this->db->get();
        $res=$qu->result_array();
        foreach ($res as $key => $value) {
            $aid=$value['Id'];
            $this->db->select('count(ua.user_id) as total_save,agenda_id')->from('users_agenda ua');
            $this->db->join('relation_event_user reu','ua.user_id=reu.User_id','left');
            $where = "FIND_IN_SET($aid,`ua`.`agenda_id`) > 0";
            $this->db->where($where);
            $this->db->where('reu.Event_id',$eid);
            $save=$this->db->get();
            $saveres=$save->result_array();
            $res[$key]['total_save']=$saveres[0]['total_save'];
        }
        return $res;
    }
    public function get_survey_question_by_pie_chart($eid,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->where('s.Event_id', $eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('p.answer_date BETWEEN "'.$fdatetime.'" AND "'.$todatetime.'"',NULL,false);
        }
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;   
    }
    public function get_all_forms_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit =NULL)
    {
        $this->db->select('*')->from('forms');
        $this->db->where('event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('created_date BETWEEN "'.$fdatetime.'" AND "'.$todatetime.'"',NULL,false);
        }
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_formsubmitdata_by_form_id($eid,$fid)
    {
        $this->db->select('fd.*,u.Company_name,u.Firstname,u.Lastname,u.Title,u.Email')->from('form_data fd');
        $this->db->join('user u','u.Id=fd.user_id','left');
        $this->db->where('event_id',$eid);
        $this->db->where('f_id',$fid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_form_data_by_form_id($eid,$fid)
    {
        $this->db->select('*')->from('forms');
        $this->db->where('event_id',$eid);
        $this->db->where('id',$fid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_all_and_most_popular_advert_by_event($eid,$limit=Null,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select('a.Id,a.Advertisement_name,a.Organisor_id,sum(ac.click_hit) as total_hit')->from('advertising a');
        $this->db->join('user_click_board ac','ac.advert_id=a.Id AND ac.event_id='.$eid,'left');
        $this->db->where('a.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('a.Id');
        if(!empty($limit))
        {
            $this->db->having('total_hit IS NOT NULL');
            $this->db->limit(5);
        }
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_adverts_click_user_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.advert_id IS NOT NULL AND ac.event_id='.$eid,'right');
        $this->db->where('ac.advert_id IS NOT NULL');
        $this->db->where('u.Id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_advert_details_by_adverts_id($eid,$aid,$limit=null,$start = 0)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.event_id='.$eid,'right');
        $this->db->where('ac.advert_id',$aid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=null)
            $this->db->limit($limit,$start);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_advert_details_by_adverts_id_total($eid,$aid)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.event_id='.$eid,'right');
        $this->db->where('ac.advert_id',$aid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return count($res);
    }
    public function get_user_advert_details_by_user_id($eid,$uid)
    {
        $this->db->select('a.Id,a.Advertisement_name,a.Organisor_id,sum(ac.click_hit) as total_hit',false)->from('advertising a');
        $this->db->join('user_click_board ac','ac.advert_id=a.Id AND ac.event_id='.$eid,'left');
        $this->db->where('ac.user_id',$uid);
        $this->db->where('a.Event_id',$eid);
        $this->db->group_by('a.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_top_views_profiles_sponsors($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('s.Id,s.Sponsors_name,s.Company_name,s.company_logo,sum(ucb.click_hit) as total_hit')->from('sponsors s');
        $this->db->join('user_click_board ucb','ucb.sponsor_id=s.Id AND ucb.sponsor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('s.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ucb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('s.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_attendee_visited_sponsors_profiles($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.sponsor_id IS NOT NULL AND ac.event_id='.$eid,'right');
        $this->db->where('ac.event_id',$eid);
        $this->db->where('ac.sponsor_id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_user_list_visited_sponsor_profile($eid,$sid)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.sponsor_id='.$sid.' AND ac.event_id='.$eid,'right');
        $this->db->where('ac.sponsor_id',$sid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_sponsors_list_visite_by_attendee($eid,$uid)
    {
        $this->db->select('s.Id,s.Sponsors_name,s.Company_name,s.company_logo,sum(ucb.click_hit) as total_hit')->from('sponsors s');
        $this->db->join('user_click_board ucb','ucb.sponsor_id=s.Id AND ucb.sponsor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('s.Event_id',$eid);
        $this->db->where('ucb.user_id',$uid);
        $this->db->group_by('s.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_user_send_messages_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,sum(case when sm.ispublic='1' THEN 1 ELSE 0 END) as total_public_msg,sum(case when sm.ispublic='0' THEN 1 ELSE 0 END) as total_private_msg,count(sm.Id) as total_msg")->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('speaker_msg sm',"u.Id=sm.Sender_id AND sm.parent='0' AND sm.Event_id=".$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->where('sm.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('sm.Time BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_msg DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_user_comment_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,((select count(*) from speaker_comment sc join speaker_msg sm on sm.Id=sc.msg_id where user_id=u.Id And sm.Event_id='".$eid."' and sc.Time BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."')+(select count(*) from feed_comment fc join feed_img fi on fi.Id=fc.msg_id where fi.Event_id='".$eid."' and fc.Time BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."')+(select count(*) from activity_comment where event_id='".$eid."' AND user_id=u.Id and datetime BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."')) as total_comment")->from('user u');
        }
        else
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,((select count(*) from speaker_comment sc join speaker_msg sm on sm.Id=sc.msg_id where user_id=u.Id And sm.Event_id='".$eid."')+(select count(*) from feed_comment fc join feed_img fi on fi.Id=fc.msg_id where fi.Event_id='".$eid."')+(select count(*) from activity_comment where event_id='".$eid."' )) as total_comment")->from('user u');
        }
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->group_by('u.Id');
        $this->db->having('total_comment !=','0');
        $this->db->order_by('total_comment DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();

        return $res;
    }
    public function get_user_comment_by_event_photos($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from feed_comment fc join feed_img fi on fi.Id=fc.msg_id where  fi.Event_id='".$eid."' and fc.Time BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."') as total_comment")->from('user u');
        }
        else
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from feed_comment fc join feed_img fi on fi.Id=fc.msg_id where  fi.Event_id='".$eid."') as total_comment")->from('user u');
        }
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->group_by('u.Id');
        $this->db->having('total_comment !=','0');
        $this->db->order_by('total_comment DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();

        return $res;
    }
    public function get_user_comment_by_event_msg($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from speaker_comment sc join speaker_msg sm on sm.Id=sc.msg_id where  sm.Event_id='".$eid."' and sc.Time BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."') as total_comment")->from('user u');
        }
        else
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from speaker_comment sc join speaker_msg sm on sm.Id=sc.msg_id where  sm.Event_id='".$eid."') as total_comment")->from('user u');
        }
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->group_by('u.Id');
        $this->db->having('total_comment !=','0');
        $this->db->order_by('total_comment DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        //echo $this->db->last_query();exit;
        return $res;
    }
    public function get_user_comment_by_event_activity($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from activity_comment where event_id='".$eid."' AND user_id=u.Id and datetime BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."') as total_comment")->from('user u');
        }
        else
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,(select count(*) from activity_comment where event_id='".$eid."' AND user_id=u.Id) as total_comment")->from('user u');
        }
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->group_by('u.Id');
        $this->db->having('total_comment !=','0');
        $this->db->order_by('total_comment DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();

        return $res;
    }
    public function get_most_shared_photos_user_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,count(fi.Id) as total_photos")->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('feed_img fi',"u.Id=fi.Sender_id AND fi.parent='0' AND fi.Event_id=".$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->where('fi.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('fi.Time BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_photos DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_user_likes_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->select("u.Id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,u.Logo,((select count(*) from feed_like fc join feed_img fi on fi.Id=fc.post_id where user_id=u.Id And fi.Event_id='".$eid."' AND fc.like_date BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."')+(select count(*) from activity_like where `like`='1' AND event_id='".$eid."' AND user_id=u.Id AND datetime BETWEEN '".date('Y-m-d H:i:s',strtotime($fdatetime))."' AND '".date('Y-m-d H:i:s',strtotime($todatetime))."')) as total_likes",false)->from('user u');
        }
        else
        {
            $this->db->select("u.Id,concat(u.Firstname,' ',u.Lastname) as user_name,u.Email,u.Logo,((select count(*) from feed_like fc join feed_img fi on fi.Id=fc.post_id where user_id=u.Id And fi.Event_id='".$eid."')+(select count(*) from activity_like where `like`='1' AND event_id='".$eid."' AND user_id=u.Id)) as total_likes",false)->from('user u');
        }
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->where('u.Id IS NOT NULL');
        $this->db->group_by('u.Id');
        $this->db->having('total_likes !=','0');
        $this->db->order_by('total_likes DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_all_user_messages_by_user_event($eid,$uid)
    {
        $this->db->select("sm.Id as msg_id,sm.Message,(case when concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) IS NULL THEN 'Public Messages' ElSE concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) END) as user_name",false)->from('speaker_msg sm');
        $this->db->join('user u','sm.Receiver_id=u.Id','left');
        $this->db->where('sm.Event_id',$eid);
        $this->db->where('sm.Sender_id',$uid);
        $this->db->where('sm.Parent','0');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_swap_contact_user_by_event($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select("asco.contact_id,asco.from_id,asco.to_id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as from_name,u.Company_name as from_company_name,u.Title as from_title,u.Logo as from_logo,u1.Company_name as to_company_name,u1.Title as to_title,u1.Logo as to_logo,concat(IFNULL(u1.Firstname,''),' ',IFNULL(u1.Lastname,'')) as to_name",false)->from('attendee_share_contact asco');
        $this->db->join('user u','asco.from_id=u.Id','left');
        $this->db->join('user u1','asco.to_id=u1.Id','left');
        $this->db->where('asco.event_id',$eid);
        $this->db->where('asco.approval_status','1');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('asco.share_contact_datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_views_profiles_exhibitor($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('e.Id,e.Heading,e.company_logo,sum(ucb.click_hit) as total_hit')->from('exibitor e');
        $this->db->join('user_click_board ucb','ucb.exhibitor_id=e.Id AND ucb.exhibitor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('e.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ucb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('e.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_attendee_visited_exhibitor_profiles($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL,$start=0,$keyword='')
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.exhibitor_id IS NOT NULL AND ac.event_id='.$eid,'right');
        $this->db->where('ac.event_id',$eid);
        $this->db->where('ac.exhibitor_id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        if($keyword!='' && preg_match('/\\d/', $keyword) == 0)
        {
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";
            $this->db->where($where);
        }
        if($limit!=NULL)
            $this->db->limit($limit,$start);
        $this->db->group_by('u.Id');
        if($keyword!='' && preg_match('/\\d/', $keyword) > 0)
            $this->db->having("total_hit = $keyword");
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();

        return $res;
    }
    public function get_attendee_visited_exhibitor_profiles_total($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL,$start=0,$keyword='')
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.exhibitor_id IS NOT NULL AND ac.event_id='.$eid,'right');
        $this->db->where('ac.event_id',$eid);
        $this->db->where('ac.exhibitor_id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        if($keyword!='' && preg_match('/\\d/', $keyword) == 0)
        {
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";
            $this->db->where($where);
        }
        if($limit!=NULL)
            $this->db->limit($limit,$start);
        $this->db->group_by('u.Id');
        if($keyword!='' && preg_match('/\\d/', $keyword) > 0)
            $this->db->having("total_hit = $keyword");
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return count($res);
    }
    public function get_user_list_visited_exibitor_profile($id,$eid)
    {
        $this->db->select('u.*,c.country_name as Country,ea.extra_column,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$id,'left');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.exhibitor_id='.$eid.' AND ac.event_id='.$id,'right');
        $this->db->where('ac.exhibitor_id',$eid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_exibitor_list_visite_by_attendee($eid,$uid,$limit,$start)
    {
        $this->db->select('e.Id,e.Heading,e.company_logo,sum(ucb.click_hit) as total_hit')->from('exibitor e');
        $this->db->join('user_click_board ucb','ucb.exhibitor_id=e.Id AND ucb.exhibitor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('e.Event_id',$eid);
        $this->db->where('ucb.user_id',$uid);
        $this->db->group_by('e.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=null)
            $this->db->limit($limit,$start);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_exibitor_list_visite_by_attendee_total($eid,$uid)
    {
        $this->db->select('e.Id,e.Heading,e.company_logo,sum(ucb.click_hit) as total_hit')->from('exibitor e');
        $this->db->join('user_click_board ucb','ucb.exhibitor_id=e.Id AND ucb.exhibitor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('e.Event_id',$eid);
        $this->db->where('ucb.user_id',$uid);
        $this->db->group_by('e.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return count($res);
    }
    public function get_top_exhibitor_have_requested_meeting($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('e.Id,e.Heading,e.company_logo,count(eam.Id) as total_meeting')->from('exibitor e');
        $this->db->join('exhibitor_attendee_meeting eam',"eam.exhibiotor_id=e.Id AND eam.status='1'",'right');
        $this->db->where('eam.status','1');
        $this->db->where('e.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('eam.created_datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('e.Id');
        $this->db->order_by('total_meeting DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_attendee_have_requested_meeting($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,count(eam.Id) as total_meeting',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->join('exhibitor_attendee_meeting eam',"u.Id=eam.attendee_id AND eam.status='1' AND eam.event_id='".$eid."'",'right');
        $this->db->where('eam.status','1');
        $this->db->where('eam.exhibiotor_id is NOT NULL');
        $this->db->where('eam.event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('eam.created_datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_meeting DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_attendee_list_requested_meeting_by_exhibitor_id($id,$eid)
    {
        $this->db->select('u.*,c.country_name as Country,ea.extra_column,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,count(eam.Id) as total_meeting',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$id,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$id,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('exhibitor_attendee_meeting eam',"u.Id=eam.attendee_id AND eam.status='1' AND eam.event_id='".$id."'",'right');
        $this->db->where('eam.exhibiotor_id',$eid);
        $this->db->where('eam.status','1');
        $this->db->where('eam.event_id',$id);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_meeting DESC');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_exhibitor_list_requested_meeting_by_attendee_id($eid,$uid)
    {
        
        $this->db->select('e.Id,e.Heading,e.company_logo,count(eam.Id) as total_meeting',false)->from('exibitor e');
        $this->db->join('exhibitor_attendee_meeting eam',"eam.exhibiotor_id=e.Id AND eam.status='1'",'right');
        $this->db->where('eam.attendee_id',$uid);
        $this->db->where('eam.status','1');
        $this->db->where('e.Event_id',$eid);
        $this->db->group_by('e.Id');
        $this->db->order_by('total_meeting DESC');
        $res=$this->db->get()->result_array();
        //echo $this->db->last_query(); exit();
        return $res;
    }
    public function get_user_have_click_on_modules_by_menu_id($eid,$mid)
    {
        $this->db->select('u.*,c.country_name as Country,ea.extra_column,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ulb.menu_hit ) as total_hit',false)->from('user u');
        $this->db->join('users_leader_board ulb',"u.Id=ulb.user_id AND ulb.event_id='".$eid."' AND ulb.menu_id='".$mid."'",'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->where('ulb.event_id',$eid);
        $this->db->where('ulb.menu_id',$mid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_user_have_click_on_modules_by_user_id($eid,$uid)
    {
        $this->db->select('m.menuname,m.pagetitle,sum(ulb.menu_hit ) as total_hit')->from('menu m');
        $this->db->join('users_leader_board ulb',"m.Id=ulb.menu_id AND ulb.event_id='".$eid."' AND ulb.user_id='".$uid."'",'right');
        $this->db->where('ulb.event_id',$eid);
        $this->db->where('ulb.user_id',$uid);
        $this->db->where('ulb.menu_id IS NOT NULL');
        $this->db->group_by('m.Id');
        $this->db->order_by('total_hit DESC');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_user_list_by_login_os($eid,$os)
    {
        $this->db->select("u.*,u.Id,ea.extra_column,c.country_name as Country,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as user_name,u.Email,case when u.device IS NULL THEN 'Web' ELSE u.device End as device_name",FALSE)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'left');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->where('reu.Event_id',$eid);
        if(!empty($os))
        {
            if($os=='Web')
            {
                $this->db->where('u.device IS NULL');
            }
            else
            {
                $this->db->where('u.device',$os);
            }
        }
        $this->db->group_by('u.Id');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_user_list_by_question_id($id,$qid)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,ps.Answer as ans',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$id,'left');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$id,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('poll_survey ps','ps.User_id=u.Id AND ps.Question_id='.$qid,'right');
        $this->db->where('ps.Question_id',$qid);
        $this->db->where('reu.Event_id',$id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_top_exibitor_have_favorites($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('*,count(mf.user_id) as total_hit')->from('my_favorites mf');
        $this->db->join('exibitor e','mf.module_id = e.Id');
        $this->db->where('mf.module_type','3');
        $this->db->where('e.Event_id',$eid);
        $this->db->where('mf.event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('mf.datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('mf.module_id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_sponsors_have_favorites($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('*,count(mf.user_id) as total_hit')->from('my_favorites mf');
        $this->db->join('sponsors s','mf.module_id = s.Id');
        $this->db->where('mf.module_type','43');
        $this->db->where('s.Event_id',$eid);
        $this->db->where('mf.event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('mf.datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('mf.module_id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_speakers_have_favorites($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('u.*,mf.module_type,count(mf.user_id) as total_hit,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name',false)->from('my_favorites mf');
        $this->db->join('user u','mf.module_id = u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->where('mf.module_type','7');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('mf.event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('mf.datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('mf.module_id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_attendees_have_favorites($eid,$fdatetime=NULL,$todatetime=NULL,$limit=NULL)
    {
        $this->db->select('u.*,mf.module_type,count(mf.user_id) as total_hit,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name',false)->from('my_favorites mf');
        $this->db->join('user u','mf.module_id = u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->where('mf.module_type','2');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('mf.event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('mf.datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('mf.module_id');
        $this->db->order_by('total_hit DESC');
        if($limit!=NULL)
            $this->db->limit($limit);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_user_list_favorites($eid,$mid,$mtype,$limit=null,$start=0)
    {
        $this->db->select('*,c.country_name as Country,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,ea.extra_column',false)->from('my_favorites mf')->join('user u','mf.user_id = u.Id')->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left')->join('country c','u.Country = c.id and u.Country IS NOT NULL','left')->where('mf.event_id',$eid)->where('mf.module_id',$mid)->where('mf.module_type',$mtype);
        if($limit!=null)
            $this->db->limit($limit,$start);
        return  $this->db->get()->result_array();
    }
    public function get_user_list_favorites_total($eid,$mid,$mtype)
    {
        $this->db->select('*,c.country_name as Country,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,ea.extra_column',false)->from('my_favorites mf')->join('user u','mf.user_id = u.Id')->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left')->join('country c','u.Country = c.id and u.Country IS NOT NULL','left')->where('mf.event_id',$eid)->where('mf.module_id',$mid)->where('mf.module_type',$mtype);
        return  $this->db->get()->num_rows();
    }
    public function get_all_resgister_user_in_this_event($eid,$orid)
    {
        $this->db->select('u.*,c.country_name as Country,ea.extra_column')->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id','left');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('u.Id !=',$orid);
        $this->db->group_by('u.Id');
        $rqu=$this->db->get();
        $res=$rqu->result_array();
        return $res;
    }
    public function get_all_messages_csv_data($eid)
    {
        /*$this->db->select('u.Id as Sender_id,concat(u.Firstname, " ", u.Lastname) as Sender_name,count(sm.Id) as total_msg,concat(u1.Firstname, " ", u1.Lastname) as Receiver_name,u1.Id as Receiver_id',false)->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->join('speaker_msg sm',"u.Id=sm.Sender_id AND sm.Event_id=".$eid." AND sm.ispublic='0' AND sm.parent=0");
        $this->db->join('user u1','sm.Receiver_id=u1.Id');
        $this->db->where('reu.Event_id',$eid);
        $this->db->group_by('sm.Sender_id,sm.Receiver_id');
        $res=$this->db->get()->result_array();*/


        $this->db->select('u.Id as Sender_id,concat(u.Firstname, " ", u.Lastname) as Sender_name,sm.Message,(CASE WHEN u1.Id IS NULL THEN "Public Message" ELSE concat(u1.Firstname, " ", u1.Lastname) END) as Receiver_name,u1.Id as Receiver_id',false)->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->join('speaker_msg sm',"u.Id=sm.Sender_id AND sm.Event_id=".$eid." AND sm.parent=0",'left');
        $this->db->join('user u1','sm.Receiver_id=u1.Id','left');
        $this->db->where('reu.Event_id',$eid);
        $this->db->group_by('sm.Id');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_and_most_popular_cms_by_event($eid,$limit=Null,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select('c.Id,c.Menu_name,sum(ac.click_hit) as total_hit')->from('cms c');
        $this->db->join('user_click_board ac','ac.cms_id=c.Id AND ac.event_id='.$eid,'left');
        $this->db->where('c.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('c.Id');
        if(!empty($limit))
        {
            $this->db->having('total_hit IS NOT NULL');
            $this->db->limit(5);
        }
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_cms_click_user_by_event($eid,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select('u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.cms_id IS NOT NULL AND ac.event_id='.$eid,'right');
        $this->db->where('ac.cms_id IS NOT NULL');
        $this->db->where('u.Id IS NOT NULL');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ac.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function get_cms_details_by_adverts_id($eid,$aid,$limit=null,$start=0)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.event_id='.$eid,'right');
        $this->db->where('ac.cms_id',$aid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        if($limit!=null)
            $this->db->limit($limit,$start);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }

    public function get_cms_details_by_adverts_id_total($eid,$aid)
    {
        $this->db->select('u.*,ea.extra_column,c.country_name as Country,u.Id,concat(IFNULL(u.Firstname,"")," ",IFNULL(u.Lastname,"")) as user_name,u.Email,sum(ac.click_hit) as total_hit',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id AND reu.Event_id='.$eid,'right');
        $this->db->join('event_attendee ea','u.Id = ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->join('country c','u.Country = c.id and u.Country IS NOT NULL','left');
        $this->db->join('user_click_board ac','u.Id=ac.user_id AND ac.event_id='.$eid,'right');
        $this->db->where('ac.cms_id',$aid);
        $this->db->group_by('u.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return count($res);
    }



    public function get_swap_contact_user_by_event_temp($eid,$fdatetime=NULL,$todatetime=NULL)
    {
        $this->db->select("asco.contact_id,asco.from_id,asco.to_id,concat(IFNULL(u.Firstname,''),' ',IFNULL(u.Lastname,'')) as from_name,u.Company_name as from_company_name,u.Title as from_title,u.Logo as from_logo,u1.Company_name as to_company_name,u1.Title as to_title,u1.Logo as to_logo,concat(IFNULL(u1.Firstname,''),' ',IFNULL(u1.Lastname,'')) as to_name",false)->from('attendee_share_contact asco');
        $this->db->join('user u','asco.from_id=u.Id','left');
        $this->db->join('user u1','asco.to_id=u1.Id','left');
        $this->db->where('asco.event_id',$eid);
        $this->db->where('asco.approval_status','1');
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('asco.share_contact_datetime BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_top_views_profiles_exhibitor_temp($eid,$fdatetime=NULL,$todatetime=NULL,$limit,$start)
    {
        $this->db->select('e.Id,e.Heading,e.company_logo,sum(ucb.click_hit) as total_hit')->from('exibitor e');
        $this->db->join('user_click_board ucb','ucb.exhibitor_id=e.Id AND ucb.exhibitor_id IS NOT NULL AND ucb.event_id='.$eid,'left');
        $this->db->where('e.Event_id',$eid);
        if(!empty($fdatetime) && !empty($todatetime))
        {
            $this->db->where('ucb.date BETWEEN "'.date('Y-m-d',strtotime($fdatetime)).'" AND "'.date('Y-m-d',strtotime($todatetime)).'"',NULL,false);
        }
        $this->db->group_by('e.Id');
        $this->db->order_by('total_hit DESC');
        $qu=$this->db->limit($limit,$start)->get();
        $res=$qu->result_array();
        return $res;
    }
}   
?>
