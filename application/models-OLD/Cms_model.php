<?php

class cms_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }

     public function get_all_cms_list($id,$cms_id=NULL)
     {
          $user = $this->session->userdata('current_user');
          $orid = $this->data['user']->Id;
          $this->db->select('*');
          $this->db->from('cms');
          //$this->db->where('Organisor_id', $user[0]->Id);
          $this->db->where('Event_id', $id);
          
          if($cms_id!=NULL)
          {
               $this->db->where('Id', $cms_id);
          }
          
          $this->db->order_by("Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

     public function add_admin_cms($data)
     {
          if (!empty($data['cms_array']['Images']))
          {
               $images[] = $data['cms_array']['Images'];
               $array_cms['Images'] = json_encode($images);
          }

          if (!empty($data['cms_array']['Logo_images']))
          {
               $logo_images[] = $data['cms_array']['Logo_images'];
               $array_cms['Logo_images'] = json_encode($logo_images);
          }

          if ($data['cms_array']['Organisor_id'] != NULL)
          {
               $array_cms['Organisor_id'] = $data['cms_array']['Organisor_id'];
          }

          if ($data['cms_array']['Event_id'] != NULL)
          {
               $array_cms['Event_id'] = $data['cms_array']['Event_id'];
          }

//          if ($data['cms_array']['Slug'] != NULL)
//          {
//               $array_cms['Slug'] = $data['cms_array']['Slug'];
//          }

          if ($data['cms_array']['Description'] != NULL)
          {
               $array_cms['Description'] = $data['cms_array']['Description'];
          }

          if ($data['cms_array']['Menu_name'] != NULL)
          {
               $array_cms['Menu_name'] = $data['cms_array']['Menu_name'];
          }

          if ($data['cms_array']['Status'] != NULL)
          {
               $array_cms['Status'] = $data['cms_array']['Status'];
          }

          if ($data['cms_array']['Background_color'] != NULL)
          {
               $array_cms['Background_color'] = $data['cms_array']['Background_color'];
          }

          if ($data['cms_array']['Img_view'] != NULL)
          {
               $array_cms['Img_view'] = $data['cms_array']['Img_view'];
          }
          
          if ($data['cms_array']['Img_view'] != NULL)
          {
               $array_cms['Img_view'] = $data['cms_array']['Img_view'];
          }
          
          if ($data['cms_array']['fecture_module'] != NULL)
          {
               $array_cms['fecture_module'] = $data['cms_array']['fecture_module'];
          }
          $array_cms['exi_page_id']=$data['cms_array']['exi_page_id'];
          $this->db->insert('cms', $array_cms);
          $cms_id = $this->db->insert_id();
          return $cms_id;
     }
     
     
     public function edit_admin_cms($data,$cms_id)
     {
          if (!empty($data['cms_array']['Images']))
          {
               $images[] = $data['cms_array']['Images'];
               $array_cms['Images'] = json_encode($images);
          }

          if (!empty($data['cms_array']['Logo_images']))
          {
               $logo_images[] = $data['cms_array']['Logo_images'];
               $array_cms['Logo_images'] = json_encode($logo_images);
          }

          if ($data['cms_array']['Organisor_id'] != NULL)
          {
               $array_cms['Organisor_id'] = $data['cms_array']['Organisor_id'];
          }

          if ($data['cms_array']['Event_id'] != NULL)
          {
               $array_cms['Event_id'] = $data['cms_array']['Event_id'];
          }

//          if ($data['cms_array']['Slug'] != NULL)
//          {
//               $array_cms['Slug'] = $data['cms_array']['Slug'];
//          }

          if ($data['cms_array']['Description'] != NULL)
          {
               $array_cms['Description'] = $data['cms_array']['Description'];
          }
          

          if ($data['cms_array']['Menu_name'] != NULL)
          {
               $array_cms['Menu_name'] = $data['cms_array']['Menu_name'];
          }

          if ($data['cms_array']['Status'] != NULL)
          {
               $array_cms['Status'] = $data['cms_array']['Status'];
          }

          if ($data['cms_array']['Background_color'] != NULL)
          {
               $array_cms['Background_color'] = $data['cms_array']['Background_color'];
          }

          if ($data['cms_array']['Img_view'] != NULL)
          {
               $array_cms['Img_view'] = $data['cms_array']['Img_view'];
          }
          
          if ($data['cms_array']['fecture_module'] != NULL)
          {
               $array_cms['fecture_module'] = $data['cms_array']['fecture_module'];
          }
          else
          {
               $array_cms['fecture_module'] =NULL;
          }
          
          if ($data['cms_array']['cms_fecture_module'] != NULL)
          {
               $array_cms['cms_fecture_module'] = $data['cms_array']['cms_fecture_module'];
          }
          else
          {
               $array_cms['cms_fecture_module'] = NULL;
          }

//        echo "<pre>";
//        print_r($array_cms);
//        exit;
          $array_cms['exi_page_id']=$data['cms_array']['exi_page_id'];
          $this->db->where('Id', $cms_id);
          $this->db->update('cms', $array_cms);
          $event_id = $this->db->insert_id();
          return $event_id;
     }
     
     public function delete_cmspage($cms_id)
     {
        $this->db->where('Id', $cms_id);
        $this->db->delete('cms');
     }

    public function delete_cms($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('cms');
        $str = $this->db->last_query();
    }
    
    public function get_cms_page($id,$cms_id=NULL)
     {
          $user = $this->session->userdata('current_user');
          if($user[0]->Id!="" && $user[0]->Rid=='4')
          { 
               $this->db->select('*')->from('event_attendee ea');
               $this->db->join('user_views uv','uv.view_id=ea.views_id','right');
               $this->db->where('ea.Event_id',$id);
               $this->db->where('uv.view_type','0');
               $this->db->where('ea.Attendee_id',$user[0]->Id);
               $vqu=$this->db->get();   
               $vres=$vqu->result_array();
          }
          
          $this->db->select('*,CASE WHEN em.title IS NULL THEN c.Menu_name ELSE em.title END as Menu_name',FALSE);
          $this->db->from('cms c');
          $this->db->join('event_menu em','em.cms_id=c.Id','left');
          $this->db->where('c.Event_id', $id);
          $this->db->where('c.show_in_app','1');
          if($cms_id!=NULL)
          {
               $this->db->where('c.Id', $cms_id);
          }
          if($user[0]->Id!="" && $user[0]->Rid=='4' && count($vres) > 0)
          {
               $this->db->where_in('c.Id',explode(",",$vres[0]['view_custom_modules']));
          }
          $this->db->where('c.status', '1');
          $this->db->order_by("c.Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     
     public function get_cms_menulist($id,$cms_id=NULL)
     {
          $user = $this->session->userdata('current_user');
          $orid = $this->data['user']->Id;
          $this->db->select('*',FALSE);
          $this->db->from('cms');
          $this->db->where('Event_id', $id);
          
          if($cms_id!=NULL)
          {
               $this->db->where('Id !=', $cms_id);
          }
          $this->db->where('status', '1');
          $this->db->order_by("Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
//          echo $this->db->last_query();
//          exit;
          return $res;
     }
     public function get_hub_cms_page($org_id,$cms_id=NULL)
     {
          $this->db->select('e.Id as eventid,e.Subdomain,c.*,CASE WHEN em.title IS NULL THEN c.Menu_name ELSE em.title END as Menu_name',FALSE);
          $this->db->from('event e');
          $this->db->join('hub_event he','he.event_id=e.Id','right');
          $this->db->join('cms c','c.Event_id=he.event_id');
          $this->db->join('event_menu em','em.cms_id=c.Id','left');
          $this->db->where('e.Organisor_id',$org_id);
          $where = "FIND_IN_SET(21,`checkbox_values`) > 0";
          $this->db->where($where);
          if($cms_id!=NULL)
          {
               $this->db->where('c.Id', $cms_id);
          }
          $this->db->where('show_in_hub','1');
          $this->db->where('c.status', '1');
          $this->db->order_by("c.Id", "desc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function cms_page_add_in_hub_cms_id($cms_id)
     {
          $this->db->select('show_in_hub')->from('cms');
          $this->db->where('Id', $cms_id);
          $query = $this->db->get();
          $res = $query->result_array();
          if($res[0]['show_in_hub']=='1')
          {
               $cmsdata['show_in_hub']='0';
          }
          else
          {
               $cmsdata['show_in_hub']='1';
          }
          $this->db->where('Id', $cms_id);
          $this->db->update('cms',$cmsdata);
          return $this->db->affected_rows();
     }
     public function cms_page_show_in_app_cms_id($cms_id)
     {
          $this->db->select('show_in_app')->from('cms');
          $this->db->where('Id', $cms_id);
          $query = $this->db->get();
          $res = $query->result_array();
          if($res[0]['show_in_app']=='1')
          {
               $cmsdata['show_in_app']='0';
          }
          else
          {
               $cmsdata['show_in_app']='1';
          }
          $this->db->where('Id', $cms_id);
          $this->db->update('cms',$cmsdata);
          return $this->db->affected_rows();
     }
     public  function get_all_exibitor_page_by_event($eid)
     {
          $this->db->select('*')->from('exibitor');
          $this->db->where('Event_id',$eid);
          $res=$this->db->get()->result_array();
          return $res;
     }
     public function delete_cms_images($cms_id,$array_cms)
     {
          $this->db->where('Id', $cms_id);
          $this->db->update('cms', $array_cms);
     }
}

?>