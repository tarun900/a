<?php

class Attendee_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_attendee_list($id = null) 
    {
        $orid = $this->data['user']->Id;
        //$this->db->query('SET group_concat_max_len = 2048');
        $this->db->select('u.*,c.country_name as Country,r.Id as Rid,u.Id as uid,u.Active,an.Id as note_id,ea.extra_column,ea.views_id,ea.group_id,uv.view_name,ug.group_name,concat(IFNULL(umr.Firstname,"")," ",IFNULL(umr.Lastname,"")) as moderator_name,sfd.json_data,ea.check_in_attendee,(select GROUP_CONCAT(ac.category_name) as category from attendee_agenda_relation aar left join agenda_categories ac on ac.Id=aar.agenda_category_id where aar.event_id='.$id.' and ac.event_id='.$id.' and aar.attendee_id=u.Id) as category_name,ea.check_in_time,ea.check_out_time',false);
        //$this->db->select('u.*,r.Id as Rid,u.Id as uid,u.Active,an.Id as note_id,ea.extra_column,ea.views_id,ug.group_name,uv.view_name,concat(IFNULL(umr.Firstname,"")," ",IFNULL(umr.Lastname,"")) as moderator_name,ac.category_name,sfd.json_data,ea.check_in_attendee,ea.check_in_time,ea.check_out_time',false);
        $this->db->from('user u');
        $this->db->join('event_attendee ea','ea.Attendee_id=u.Id and ea.Event_id='.$id,'left');
        $this->db->join('user_views uv','ea.views_id=uv.view_id','left');
        $this->db->join('user_group ug','ea.group_id=ug.id','left');
        //$this->db->join('attendee_agenda_relation aar','aar.attendee_id=u.Id and aar.event_id='.$id,'left');
        //$this->db->join('agenda_categories ac','ac.Id=aar.agenda_category_id','left');
        $this->db->join('signup_form_data sfd','sfd.user_id=u.Id','left');
        $this->db->join('moderator_relation mr','mr.user_id=u.Id','left');
        $this->db->join('user umr','umr.Id=mr.moderator_id','left');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('attendee_notes an','an.User_id=u.Id','left');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id) 
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->join('country c','c.id = u.Country','left');
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Id','desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function delete_signup_form($id)
    {
        $this->db->where('event_id', $id);
        $this->db->delete('signup_form');
    }
    public function add_attendee($array_add) 
    {
        $Event_id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id',4);
        $query = $this->db->get();
        $result = $query->result_array();
        $array_add['Role_id'] = $result[0]['Id'];
        $array_add['Password'] = md5($array_add['Password']);
        //$array_add['Event_id'] = $Event_id;
        $this->db->insert('user', $array_add);
        $this->session->set_flashdata('attendee_data', 'Added');
        redirect('attendee');
    }

    public function delete_attendee($id,$Event_id) 
    {
        $this->db->select('*')->from('user');
        $this->db->where('Id',$id);
        $qu=$this->db->get();
        $res=$qu->result_array();

        $this->db->where('User_id', $id);
        $this->db->where('Event_id', $Event_id);
        $this->db->delete('relation_event_user');
        //$str = $this->db->last_query();

        $this->db->where('Attendee_id',$id);
        $this->db->where('Event_id',$Event_id);
        $this->db->delete('event_attendee');
        
        $this->db->where('Emailid',$res[0]['Email']);
        $this->db->delete('attendee_invitation');
    }
    public function delete_mass_attendee($ids,$Event_id) 
    {
        $this->db->select('group_concat(Email) as emails')->from('user');
        $this->db->where_in('Id',$ids);
        $qu=$this->db->get();
        $res=$qu->row_array();

        $this->db->where_in('User_id', $ids);
        $this->db->where('Event_id', $Event_id);
        $this->db->delete('relation_event_user');
        $str = $this->db->last_query();
        
        $this->db->where_in('Attendee_id',$ids);
        $this->db->where('Event_id',$Event_id);
        $this->db->delete('event_attendee');
         $str = $this->db->last_query();
        
        $this->db->where_in('Emailid',$res['emails']);
        $this->db->delete('attendee_invitation');
         $str = $this->db->last_query();
    }
    public function delete_attendee_in_hub($id,$org_id) 
    {
        $this->db->where('User_id', $id);
        $this->db->where('Organisor_id', $org_id);
        $this->db->delete('relation_event_user');

        $this->db->where('user_id', $id);
        $this->db->where('organizer_id', $org_id);
        $this->db->delete('hub_attendee');
    }

    public function get_recent_attendee_list() 
    {

        $orid = $this->data['user']->Id;
        $this->db->select('u.*');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($user[0]->Id) 
        {
            $this->db->where('u.Id', $user[0]->Id);
        }
        $this->db->where('r.Id',4);
        $this->db->where('u.Organisor_id',$orid);
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function addinvite_attendee($emails,$id,$firstname,$lastname,$cmpy,$title,$code,$session_id)
    {
        $this->db->select('u.Email');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('ru.Event_id',$id);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        $emails_array = array();
        foreach ($res as $key_e =>$val_e)
        {
            $emails_array[]=trim($val_e['Email']);
        }
        $dataemails=  explode(',',$emails);
        $finalemail="";
        foreach($dataemails as $key=>$val)
        {
            if(!in_array(trim($val),$emails_array))
            {
                $this->db->select('ai.Id');
                $this->db->from('attendee_invitation ai');
                $this->db->where('ai.Event_id', $id);
                $this->db->where('ai.Emailid', trim($val));
                $this->db->order_by('ai.Id desc');
                $query = $this->db->get();
                $res = $query->result_array();
                if(empty($res))
                {
                    $finalemail.= trim($val).",";
                    if(trim($firstname)!="")
                        $array_add['firstname'] = trim($firstname);
                    if(trim($cmpy)!="")
                        $array_add['Company_name'] = trim($cmpy);
                    if(trim($title)!="")
                        $array_add['Title'] = trim($title);
                    if(trim($lastname)!="")
                        $array_add['lastname'] = trim($lastname);
                    if(trim($val)!="")
                        $array_add['Emailid'] = trim($val);
                    if(trim($code)!="")
                         $array_add['link_code'] = $code;
                    $array_add['Event_id'] = $id;
                    $array_add['Status'] = '0';
                    $array_add['role_status'] = '0';
                    $array_add['agenda_id']=$session_id;
                    if($array_add['firstname']!="" || $array_add['lastname']!="" || $array_add['Emailid']!="")
                            $this->db->insert('attendee_invitation', $array_add);
                }
            }            
        }
        $finalemail=  substr($finalemail,0,  strlen($finalemail)-1);
        return $finalemail;
    }
    public function update_register_user($event_id,$email)
    {
        $data=array("link_code"=>"","isUsedLink"=>"1");
        $this->db->where("Event_id",$event_id);
        $this->db->where("Emailid",$email);
        $this->db->update("attendee_invitation",$data);
        $afftectedRows = $this->db->affected_rows();
        return $afftectedRows;
    }
    public function get_invited_attendee($id)
    {
        $this->db->select('ai.*,cn.category_name');
        $this->db->from('attendee_invitation ai');
        $this->db->join('agenda_categories cn','cn.Id=ai.agenda_id','left');
        $this->db->where('ai.Event_id', $id);
        $this->db->where('ai.role_status','0');
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    
    public function get_invited_attendee_bymail($email,$event_id)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_invitation ai');
        $this->db->where('ai.Emailid', $email);
        $this->db->where('ai.Event_id', $event_id); 
        $this->db->where('ai.role_status','0'); 
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function check_invited_attendee($id,$email)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_invitation ai');
        $this->db->where('ai.Event_id', $id);
        $this->db->where('ai.Emailid', trim($email));
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        
        if(!empty($res))
        {
            $array_add['Status'] = '1';
            $this->db->where('Id', $res[0]['Id']);
            $this->db->update('attendee_invitation', $array_add);
        }
        $role_flag=$res[0]['role_status'];
        return $role_flag;
    }
    public function add_agenda_relation($data)
    {
        $this->db->select('*')->from('attendee_agenda_relation');
        $this->db->where($data);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)>0)
        {
            $this->db->where($data);
            $this->db->update('attendee_agenda_relation',$data);
        }
        else
        {
            $this->db->insert('attendee_agenda_relation',$data);
        }
    }
    public function check_hub_invited_attendee($oid,$email)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_hub_invitation ai');
        $this->db->where('ai.organizer_id', $oid);
        $this->db->where('ai.Emailid', trim($email));
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($res))
        {
            $array_add['Status'] = '1';
            $this->db->where('Id', $res[0]['Id']);
            $this->db->update('attendee_hub_invitation', $array_add);
        }
        $role_flag=0;
        return $role_flag;
    }
    public function get_hub_invited_attendee_bymail($email,$oid)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_hub_invitation ai');
        $this->db->where('ai.Emailid', $email);
        $this->db->where('ai.organizer_id', $oid); 
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_hub_invited_attendee($oid)
    {
        $this->db->select('ai.*');
        $this->db->from('attendee_hub_invitation ai');
        $this->db->where('ai.organizer_id', $oid);
        $this->db->order_by('ai.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_hub_invite_attendee($emails,$id,$firstname,$lastname,$cmpy,$title,$code)
    {
        $this->db->select('u.Email');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('ru.Organisor_id',$id);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        $emails_array = array();
        foreach ($res as $key_e =>$val_e)
        {
            $emails_array[]=trim($val_e['Email']);
        }
        $dataemails=  explode(',',$emails);
        $finalemail="";
        foreach($dataemails as $key=>$val)
        {
            if(!in_array(trim($val),$emails_array))
            {
                $this->db->select('ai.Id');
                $this->db->from('attendee_hub_invitation ai');
                $this->db->where('ai.organizer_id', $id);
                $this->db->where('ai.Emailid', trim($val));
                $this->db->order_by('ai.Id desc');
                $query = $this->db->get();
                $res = $query->result_array();
                if(empty($res))
                {
                    $finalemail.= trim($val).",";
                    if(trim($firstname)!="")
                        $array_add['firstname'] = trim($firstname);
                    if(trim($cmpy)!="")
                        $array_add['Company_name'] = trim($cmpy);
                    if(trim($title)!="")
                        $array_add['Title'] = trim($title);
                    if(trim($lastname)!="")
                        $array_add['lastname'] = trim($lastname);
                    if(trim($val)!="")
                        $array_add['Emailid'] = trim($val);
                    if(trim($code)!="")
                         $array_add['link_code'] = $code;
                    $array_add['organizer_id'] = $id;
                    $array_add['Status'] = '0';
                    if($array_add['firstname']!="" || $array_add['lastname']!="" || $array_add['Emailid']!="")
                        $this->db->insert('attendee_hub_invitation', $array_add);
                }
            }            
        }
        $finalemail=  substr($finalemail,0,  strlen($finalemail)-1);
        return $finalemail;
    }
    public function update_register_hub_user($org_id,$email)
    {
        $data=array("link_code"=>"","isUsedLink"=>"1");
        $this->db->where("organizer_id",$org_id);
        $this->db->where("Emailid",$email);
        $this->db->update("attendee_hub_invitation",$data);
        $afftectedRows = $this->db->affected_rows();
        return $afftectedRows;
    }
    public function get_hub_attendee_list($oid) 
    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('hub_attendee ha','ha.user_id=u.Id','left');
        $this->db->join('hub_attendee_group hag','hag.group_id=ha.attendee_group_id','left');
        $this->db->join('signup_form_data sfd','sfd.user_id=u.Id','left');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('ru.Organisor_id', $oid);
        $this->db->where('r.Id',4);
        $this->db->where('ru.Event_id IS NULL');
        $this->db->order_by('u.Id','desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function check_attendee_group_name($org_id,$gnm,$gid)
    {
        $this->db->select('*')->from('hub_attendee_group');
        $this->db->where('organizer_id',$org_id);
        $this->db->where('group_name',$gnm);
        if(!empty($gid))
        {
            $this->db->where('group_id !=',$gid);
        }
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)>0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function add_attendee_group($org_id,$gnm,$gid)
    {
        $array_group['group_name']=$gnm;
        if(!empty($gid))
        {
            $this->db->where('group_id',$gid);
            $this->db->update('hub_attendee_group',$array_group);
        }
        else
        {
            $array_group['organizer_id']=$org_id;
            $array_group['date']=date("Y-m-d H:i:s");
            $this->db->insert('hub_attendee_group',$array_group);
        }
    }
    public function get_attendee_group_list($org_id)
    {
        $this->db->select('hag.*,count(ha.user_id) as no_of_member')->from('hub_attendee_group hag');
        $this->db->join('hub_attendee ha','ha.attendee_group_id=hag.group_id','left');
        $this->db->where('hag.organizer_id',$org_id);
        $this->db->group_by('hag.group_id');
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function delete_attendee_group($gid)
    {
        $this->db->where('group_id',$gid);
        $this->db->delete('hub_attendee_group');
    }
    public function assign_group_to_attendee($oid,$uid,$gid)
    {
        $this->db->where('organizer_id',$oid);
        $this->db->where('user_id',$uid);
        $this->db->update('hub_attendee',array('attendee_group_id'=>$gid));
    }
    public function get_all_group_member_by_group_id($gid)
    {
        $this->db->select('*')->from('hub_attendee');
        $this->db->where('attendee_group_id',$gid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function add_hub_attendees_in_event($event_id,$org_id,$user_id)
    {
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id',$event_id);
        $this->db->where('Attendee_id',$user_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)==0)
        {
            $this->db->insert('event_attendee', array('Event_id'=>$event_id,'Attendee_id'=>$user_id));
        }
        $this->db->select('*')->from('relation_event_user');
        $this->db->where('Event_id',$event_id);
        $this->db->where('User_id',$user_id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)==0)
        {
            $this->db->insert('relation_event_user', array('Event_id'=>$event_id,'User_id'=>$user_id,'Organisor_id'=>$org_id,'Role_id'=>'4'));
        }
    }
    public function save_notes_in_attendee($eid,$data)
    {
        $this->db->select('*')->from('attendee_notes');
        $this->db->where('Event_id',$eid);
        $this->db->where('User_id',$data['User_id']);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)>0)
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('User_id',$data['User_id']);
            unset($data['User_id']);
            $this->db->update('attendee_notes',$data);
        }
        else
        {
            $data['Event_id']=$eid;
            $this->db->insert('attendee_notes',$data);
        }
    }
    public function get_note_data_note_id($nid,$uid)
    {
        $this->db->select('*')->from('attendee_notes');
        $this->db->where('Id',$nid);
        $this->db->where('User_id',$uid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res[0];
    }
    public function addinvite_attendee_in_temp($data)
    {
        $this->db->select('*')->from('temp_attendee_invitation');
        $this->db->where('Emailid',$data['Emailid']);
        $this->db->where('Event_id',$data['Event_id']);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(count($res)>0)
        {
            $this->db->where('Emailid',$data['Emailid']);
            $this->db->where('Event_id',$data['Event_id']);
            unset($data['Emailid']);
            unset($data['Event_id']);
            $this->db->update('temp_attendee_invitation',$data);
        }
        else
        {
            $this->db->insert('temp_attendee_invitation',$data);
        }
    }
    public function get_all_csv_data_by_event($eid,$skip_to_invite=null)
    {
        $this->db->select('*')->from('temp_attendee_invitation tai');
        $this->db->join('agenda_categories ac','ac.Id=tai.agenda_id','left');
        $this->db->where('tai.Event_id',$eid);
        if($skip_to_invite!=NULL)
        {
            $this->db->where('skip_to_invite',$skip_to_invite);
        }
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function add_invitaion__attendees($savedata)
    {
       $this->db->insert('attendee_invitation',$savedata);
       $ai_id=$this->db->insert_id();

       $this->db->where('Event_id',$savedata['Event_id']);
       $this->db->where('Emailid',$savedata['Emailid']);
       $this->db->delete('temp_attendee_invitation');
       return $ai_id;
    }
    public function get_all_custom_modules($id)
    {   
        
        $this->db->select('column_id,rq.question as column_name,rq.event_id,crequire,rq.id',
            false)->from('reg_que rq');
        $this->db->join('reg_stages rs','rs.id = rq.stage_id');
        $this->db->join('custom_column cc','cc.column_name = rq.question','right');
        $this->db->where('rq.event_id',$id);
        $this->db->order_by('rs.sort_order=0,rs.sort_order',NULL,false);
        $res = $this->db->get()->result_array();
        foreach($res as $element){
            $hash = $element['id'];
            $unique_array[$hash] = $element;
            unset($unique_array[$hash]['id']);
        }
        $res = array_values($unique_array);

        if($id == '1802')
            unset($res);

        if(empty($res))
        {
            $this->db->select('*')->from('custom_column');
            $this->db->where('event_id',$id);
            $this->db->order_by('column_id');
            $qu=$this->db->get();
            $res=$qu->result_array();
        }
        return $res;
    }
    public function update_column_name($cnm,$cid,$id)
    {
        $this->db->select('*')->from('custom_column');
        $this->db->where('column_id',$cid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        $new_key=$cnm['column_name'];
        $old_cnm=$res[0]['column_name'];
        $this->db->select('*')->from('event_attendee');
        $this->db->where('event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $eres=$equ->result_array();
        foreach ($eres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($old_cnm,$upkey)){
                $upkey =$this->replace_key_function($upkey, $old_cnm, $new_key);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('Attendee_id',$value['Attendee_id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('event_attendee',$u_key);
            }
        }
        $this->db->select('*')->from('temp_attendee_invitation');
        $this->db->where('Event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $teres=$equ->result_array();
        foreach ($teres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($old_cnm,$upkey)){
                $upkey =$this->replace_key_function($upkey, $old_cnm, $new_key);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('temp_id',$value['temp_id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('temp_attendee_invitation',$u_key);
            }
        }
        $this->db->select('*')->from('attendee_invitation');
        $this->db->where('Event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $teres=$equ->result_array();
        foreach ($teres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($old_cnm,$upkey)){
                $upkey =$this->replace_key_function($upkey, $old_cnm, $new_key);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('Id',$value['Id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('attendee_invitation',$u_key);
            }
        }
        $this->db->where('column_id',$cid);
        $this->db->where('event_id',$id);
        $this->db->update('custom_column',$cnm);
    }
    public function replace_key_function($array, $key1, $key2)
    {
        $keys = array_keys($array);
        $index = array_search($key1, $keys);
        if ($index !== false) {
            $keys[$index] = $key2;
            $array = array_combine($keys, $array);
        }
        return $array;
    }
    public function add_column_name($cnm)
    {
        $this->db->insert('custom_column',$cnm);
    }
    public function delete_custom_column($id,$cid)
    {
        $this->db->select('*')->from('custom_column');
        $this->db->where('column_id',$cid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        $cnm=$res[0]['column_name'];
        $this->db->select('*')->from('event_attendee');
        $this->db->where('event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $eres=$equ->result_array();
        foreach ($eres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($cnm,$upkey)){
                unset($upkey[$cnm]);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('Attendee_id',$value['Attendee_id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('event_attendee',$u_key);
            }
        }
        $this->db->select('*')->from('temp_attendee_invitation');
        $this->db->where('Event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $teres=$equ->result_array();
        foreach ($teres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($old_cnm,$upkey)){
                unset($upkey[$cnm]);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('temp_id',$value['temp_id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('temp_attendee_invitation',$u_key);
            }
        }
        $this->db->select('*')->from('attendee_invitation');
        $this->db->where('Event_id',$id);
        $this->db->where('extra_column Is NOT NULL');
        $equ=$this->db->get();
        $teres=$equ->result_array();
        foreach ($teres as $key => $value) {
            $upkey=json_decode($value['extra_column'],TRUE);
            if(array_key_exists($old_cnm,$upkey)){
                $upkey =$this->replace_key_function($upkey, $old_cnm, $new_key);
                $u_key['extra_column']=json_encode($upkey);
                $this->db->where('Id',$value['Id']);
                $this->db->where('Event_id',$value['Event_id']);
                $this->db->update('attendee_invitation',$u_key);
            }
        }
        $this->db->where('column_id',$cid);
        $this->db->where('event_id',$id);
        $this->db->delete('custom_column');
    }
    public function get_extra_info_user($id,$uid,$user_type)
    {
        if($user_type=='1')
        {
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id',$id);
            $this->db->where('Attendee_id',$uid);
            $qu=$this->db->get();
            $res=$qu->result_array();
            return $res;  
        }
        else if($user_type=='2')
        {
            $this->db->select('*')->from('attendee_invitation');
            $this->db->where('Event_id',$id);
            $this->db->where('Id',$uid);
            $qu=$this->db->get();
            $res=$qu->result_array();
            return $res;  
        }
        else if($user_type=='3')
        {
            $this->db->select('*')->from('temp_attendee_invitation');
            $this->db->where('Event_id',$id);
            $this->db->where('temp_id',$uid);
            $qu=$this->db->get();
            $res=$qu->result_array();
            return $res;
        }
        else if($user_type=='4')
        {
            return $this->get_edit_authorized_user_data_by_id($id,$uid);
        }
    }
    public function update_extra_information_by_user($eid,$uid,$extra,$user_type)
    {
        $ei['extra_column']=$extra;
        if($user_type=='1'){
            $extra_info=$this->get_extra_info_user($eid,$uid,$user_type);
            if(count($extra_info)>0){
                $this->db->where('Event_id',$eid);
                $this->db->where('Attendee_id',$uid);
                $this->db->update('event_attendee',$ei);    
            }
            else
            {
                $ei['Event_id']=$eid;
                $ei['Attendee_id']=$uid;
                $this->db->insert('event_attendee',$ei);   
            }   
        }
        else if($user_type=='2')
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('Id',$uid);
            $this->db->update('attendee_invitation',$ei);
        }
        else if($user_type=='3')
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('temp_id',$uid);
            $this->db->update('temp_attendee_invitation',$ei);
        }
        else if($user_type=='4')
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('authorized_id',$uid);
            $this->db->update('authorized_user',$ei);
        }
    }
    public function update_assign_agenda_invite_attendee($aid,$eid,$acid)
    {
        $a['agenda_id']=$acid;
        $this->db->where_in('Id',$aid);
        $this->db->where('Event_id',$eid);
        $this->db->update('attendee_invitation',$a);
    }
    public function update_assign_agenda_temp_attendee($aid,$eid,$acid)
    {
        $a['agenda_id']=$acid;
        $this->db->where_in('temp_id',$aid);
        $this->db->where('Event_id',$eid);
        $this->db->update('temp_attendee_invitation',$a);
    }
    public function update_skip_to_invite_status($aid,$eid,$sti)
    {
        $this->db->where_in('temp_id',$aid);
        $this->db->where('Event_id',$eid);
        $this->db->update('temp_attendee_invitation',$sti);
    }
    public function add_attendee_without_invite_as_user($id,$user)
    {
        $this->db->insert('user',$user);
        $user_id=$this->db->insert_id();
        $ruser['Event_id']=$id;
        $ruser['User_id']=$user_id;
        $ruser['Organisor_id']=$user['Organisor_id'];
        $ruser['Role_id']='4';
        $this->db->insert('relation_event_user',$ruser);

        $this->db->select('*')->from('user_views');
        $this->db->where('event_id',$id);
        $this->db->where('view_type','1');
        $qu=$this->db->get();
        $res=$qu->result_array();
        $ea['views_id']=$res[0]['view_id'];
        $ea['Event_id']=$id;
        $ea['Attendee_id']=$user_id;
        $this->db->insert('event_attendee',$ea);
        $this->assign_user_group_CRF($id,$user_id);

        $this->db->select('*')->from('agenda_categories ac');
        $this->db->where('event_id',$id);
        $qu=$this->db->get();
        $eares=$qu->result_array();
        if(count($eares)<=1)
        {
            $agenda_id=$eares[0]['Id'];
        }
        else
        {
            $this->db->select('*')->from('agenda_categories ac');
            $this->db->where('event_id',$id);
            $this->db->where('categorie_type','1');
            $qu=$this->db->get();
            $peares=$qu->result_array();
            $agenda_id=$peares[0]['Id'];
        }
        if(!empty($agenda_id))
        {
            $arr['attendee_id']=$user_id;
            $arr['agenda_category_id']=$agenda_id;
            $arr['event_id']=$id;
            $this->db->insert('attendee_agenda_relation',$arr);
        }
    }
    public function getinvited_attendee_by_id($eid,$data)
    {
        $this->db->select('*')->from('attendee_invitation');
        $this->db->where_in('Id',$data);
        $this->db->where('Event_id',$eid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function gettempinvited_attendee_by_ids($eid,$data)
    {
        $this->db->select('*')->from('temp_attendee_invitation');
        $this->db->where_in('temp_id',$data);
        $this->db->where('Event_id',$eid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function add_msg_queue_mail($msg)
    {
        $this->db->insert('queue_mail',$msg);
    }
    public function get_queue_mail_list_by_event($eid)
    {
        $this->db->select('*')->from('queue_mail');
        $this->db->where('event_id',$eid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function update_assign_linkcode_invite_attendee($aid,$eid,$link_code)
    {
        $a['link_code']=$link_code;
        $this->db->where_in('Id',$aid);
        $this->db->where('Event_id',$eid);
        $this->db->update('attendee_invitation',$a);
    }
    public function get_user_bymail($email,$event_id)
    {
        $this->db->select('u.Id as uid');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->where('ru.Event_id =',$event_id);
        $this->db->where('u.Email',$email);            
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function delete_invited_attendee($iid,$eid)
    {
        $this->db->select('*')->from('attendee_invitation');
        $this->db->where('Id',$iid);
        $this->db->where('Event_id',$eid);
        $query = $this->db->get();
        $res = $query->result_array();

        $this->db->where('Emailid',$res[0]['Emailid']);
        $this->db->delete('temp_attendee_invitation');

        $this->db->where('Id',$iid);
        $this->db->where('Event_id',$eid);
        $this->db->delete('attendee_invitation');
    }
    public function delete_inactive_attendee($temp_id,$eid)
    {
        $this->db->where('temp_id',$temp_id);
        $this->db->where('Event_id',$eid);
        $this->db->delete('temp_attendee_invitation');
    }
    public function attendee_check_in_get($eid,$aid)
    {
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id',$eid);
        $this->db->where('Attendee_id',$aid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function attendee_check_in_save($eid,$aid)
    {
        $res=$this->attendee_check_in_get($eid,$aid);
        if(count($res) > 0)
        {
            if($res[0]['check_in_attendee']=='0')
            {
                $at['check_in_attendee']='1';
                $at['check_in_time']=date('Y-m-d H:i:s');
                $at['check_out_time']=NULL;
            }
            else
            {
                $at['check_in_attendee']='0';
                $at['check_out_time']=date('Y-m-d H:i:s');
            }
            $this->db->where('Event_id',$eid);
            $this->db->where('Attendee_id',$aid);
            $this->db->update('event_attendee',$at);
        }
        else
        {
            $at['Event_id']=$eid;
            $at['check_in_attendee']='1';
            $at['check_in_time']=date('Y-m-d H:i:s');
            $at['check_out_time']=NULL;
            $at['Attendee_id']=$aid;
            $this->db->insert('event_attendee',$at);
        }
    }
    public function add_view_data($view_data)
    {
        $this->db->insert('user_views',$view_data);
    }
    public function edit_view_data($vid,$eid,$view_data)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('view_id',$vid);
        $this->db->update('user_views',$view_data);
    }
    public function get_all_view_by_event($id,$vid=NULL)
    {
        $this->db->select('*')->from('user_views');
        if(!empty($vid))
        {
            $this->db->where('view_id',$vid);
        }
        $this->db->where('event_id',$id);
        $qu=$this->db->get();
        $res=$qu->result_array();
        if(!$res && $this->uri->segment(2)=='edit_view_role')
            return redirect('Forbidden');
        return $res;
    }
    public function delete_view_data($eid,$vid)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('view_id',$vid);
        $this->db->delete('user_views');
    }
    public function assign_view_id($view_data,$eid)
    {
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id',$eid);
        $this->db->where('Attendee_id',$view_data['Attendee_id']);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res) > 0)
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('Attendee_id',$view_data['Attendee_id']);
            unset($view_data['Attendee_id']);
            $this->db->update('event_attendee',$view_data);
        }
        else
        {
            $view_data['Event_id']=$eid;
            $this->db->insert('event_attendee',$view_data);
            $this->assign_user_group_CRF($eid,$view_data['Attendee_id']);
        }
    }
    public function get_user_info($uid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Id',$uid);
        $qu=$this->db->get();
        $res=$qu->result_array();
        return $res;
    }
    public function save_attendee_user_information($uid,$user_data)
    {
        $this->db->where('Id',$uid);
        $this->db->update('user',$user_data);
    }
    public function get_registration_screen_data($eid,$rsid=null)
    {
        $this->db->select('*')->from('registration_screen_data');
        $this->db->where('event_id',$eid);
        if(!empty($rsid))
        {
            $this->db->where('screen_id',$rsid);
        }
        $rqu=$this->db->get();
        $res=$rqu->result_array();
        return $res;
    }
    public function save_registration_screen_data($rs_data)
    {
        $rsdata=$this->get_registration_screen_data($rs_data['event_id'],null);
        if (count($rsdata)>0) 
        {
            $this->db->where('event_id',$rs_data['event_id']);   
            unset($rs_data['event_id']);
            $this->db->update('registration_screen_data',$rs_data);
        }
        else
        {
            $this->db->insert('registration_screen_data',$rs_data);
        }
    }
    public function delete_registration_screen_data($eid,$rsid)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('screen_id',$rsid);
        $this->db->delete('registration_screen_data');
    }
    public function save_authorized_user_data($data)
    {
        $res=$this->check_already_exists_email_in_authorized($data['Email'],$data['Event_id']);
        if(count($res)>0)
        {
            $this->db->where('Email',$data['Email']);
            $this->db->where('Event_id',$data['Event_id']);        
            $this->db->update('authorized_user',$data);
        }
        else
        {
            $this->db->insert('authorized_user',$data);
        }
    }
    public function get_authorized_user_data($eid)
    {
        $this->db->select('au.*,ac.category_name,uv.view_name')->from('authorized_user au');
        $this->db->join('agenda_categories ac','au.agenda_id=ac.Id','left');
        $this->db->join('user_views uv','au.views_id=uv.view_id','left');
        $this->db->where('au.Event_id',$eid);
        $this->db->where('au.email_type','0');
        $res=$this->db->get()->result_array();  
        return $res; 
    }
    public function get_edit_authorized_user_data_by_id($eid,$aid)
    {
        $this->db->select('*')->from('authorized_user');
        $this->db->where('Event_id',$eid);
        $this->db->where('authorized_id',$aid);
        $res=$this->db->get()->result_array();  
        return $res; 
    }
    public function update_authorized_user_data_by_id($data,$eid,$aid)
    {
        $res=$this->check_already_exists_email_in_authorized($data['Email'],$eid,$aid);
        if(count($res) > 0)
        {
            return false;
        }   
        else
        {
            $this->db->where('Event_id',$eid);
            $this->db->where('authorized_id',$aid);
            $this->db->update('authorized_user',$data);
            return true;
        }
    }
    public function check_already_exists_email_in_authorized($email,$eid,$aid=Null)
    {
        $this->db->select('*')->from('authorized_user');
        $this->db->where('Email',$email);
        $this->db->where('Event_id',$eid);
        if(!empty($aid))
        {
            $this->db->where('authorized_id !=',$aid);
        }
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function delete_authorized_email($eid,$aid)
    {
        $this->db->where('Event_id',$eid);
        $this->db->where('authorized_id',$aid);
        $this->db->delete('authorized_user');
    }
    public function Authorized_check_in_save($eid,$aid)
    {
        $res=$this->get_edit_authorized_user_data_by_id($eid,$aid);
        if(count($res) > 0)
        {
            $at['check_in']=$res[0]['check_in']=='0' ? '1' : '0';
            $this->db->where('Event_id',$eid);
            $this->db->where('authorized_id',$aid);
            $this->db->update('authorized_user',$at);
        }
    }
    public function authorized_user_assign_agenda($eid,$aids,$acid,$views_id)
    {
        if(!empty($acid))
        {
            $assdata['agenda_id']=$acid;
        }
        if(!empty($views_id))
        {
            $assdata['views_id']=$views_id;   
        }
        $this->db->where('Event_id',$eid);
        $this->db->where_in('authorized_id',$aids);
        $this->db->update('authorized_user',$assdata);
    }
    public function get_event_signup_form_by_event_id($eid)
    {
        $res=$this->db->select('*')->from('signup_form')->where('event_id',$eid)->get()->result_array();
        return $res;
    }
    public function get_badges_values_by_event($eid)
    {
        $this->db->select('*')->from('badges_values');
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function save_badges_email_values($badgesdata,$eid)
    {
        $res=$this->get_badges_values_by_event($eid);
        if(count($res) > 0)
        {
            $this->db->where('event_id',$eid);
            $this->db->update('badges_values',$badgesdata);
        }
        else
        {
            $badgesdata['event_id']=$eid;
            $this->db->insert('badges_values',$badgesdata);
        }
    }
    public function get_moderator_list_by_event($id)
    {
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('role r','ru.Role_id = r.Id');
        $this->db->where('r.Id',7);
        $this->db->where('ru.Event_id',$id);
        $this->db->where('r.Id !=',3);
        $this->db->where('r.Id !=',4);
        $this->db->where('u.is_moderator','1');
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function save_attendee_moderator_relation($reldata,$moderator_id)
    {
        $res=$this->db->select('*')->from('moderator_relation')->where($reldata)->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where($reldata);
            $reldata['moderator_id']=$moderator_id;
            $this->db->update('moderator_relation',$reldata);
        }
        else
        {
            $reldata['moderator_id']=$moderator_id;
            $this->db->insert('moderator_relation',$reldata);
        }
    }
    public function save_meeting_locations_in_event($ml_data,$lid=NULL)
    {
        if(!empty($lid))
        {
            $this->db->where('location_id',$lid);
            $this->db->update('meeting_location',$ml_data);
        }
        else
        {
            $this->db->insert('meeting_location',$ml_data);   
        }
    }
    public function delete_meeting_locations_in_event($eid,$lid)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('location_id',$lid);
        $this->db->delete('meeting_location');
    }
    public function get_save_session_list_by_user($uid,$eid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('*',false)->from('users_agenda ua');
        $where = "FIND_IN_SET(a.Id,ua.agenda_id) > 0";
        $this->db->join('agenda a',$where);
        $this->db->where('ua.user_id',$uid);
        $this->db->where('a.Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function add_mass_attendee_without_invite_as_user($postarr,$event_id,$org_id)
    {   
        $userdata=$this->db->select('*')->from('user')->where('Email',$postarr['Email'])->get()->row_array();
        if(count($userdata) > 0)
        {
            $user_id=$userdata['Id'];
            $user_data['Firstname']=$postarr['firstname'];
            $user_data['Lastname']=$postarr['lastname'];
            $user_data['Email']=$postarr['Email'];
            $user_data['Company_name']=$postarr['company_name'];
            $user_data['Title']=$postarr['title'];
            $user_data['Active']='1';
            $user_data['Logo']=$postarr['Logo'];
            $user_data['Unique_no']=$postarr['Unique_no'];
            if(!empty($postarr['Password']))
                $user_data['Password']=$postarr['Password'];
            //$user_data['Organisor_id']=$org_id;
            $this->db->where('Id',$user_id);
            $this->db->update('user',$user_data);
        }
        else
        {
            $user_data['Firstname']=$postarr['firstname'];
            $user_data['Lastname']=$postarr['lastname'];
            $user_data['Email']=$postarr['Email'];
            $user_data['Company_name']=$postarr['company_name'];
            $user_data['Title']=$postarr['title'];
            $user_data['Active']='1';
            $user_data['Logo']=$postarr['Logo'];
            $user_data['Organisor_id']=$org_id;
            $user_data['Created_date']=date('Y-m-d H:i:s');
            $user_data['Unique_no']=$postarr['Unique_no'];
            if(!empty($postarr['Password']))
                $user_data['Password']=$postarr['Password'];
            $this->db->insert('user',$user_data);
            $user_id=$this->db->insert_id();
        }
           
        if(!empty($postarr['keywords']))
        {
            $attendee_keywords = explode(',',$postarr['keywords']);
            $this->db->where('user_id',$user_id);
            $this->db->where('event_id',$event_id);
            $this->db->delete('attendee_keywords');
            foreach ($attendee_keywords as $key => $value)
            {
                $insert_keyword[$key]['user_id']  = $user_id;
                $insert_keyword[$key]['event_id'] = $event_id;
                $insert_keyword[$key]['keyword'] = $value;
            }
            $this->db->insert_batch('attendee_keywords',$insert_keyword);
        }
        
        $rel_data['Event_id']=$event_id;
        $rel_data['User_id']=$user_id;
        $relation=$this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->row_array();
        if(count($relation) < 1)
        {
            $rel_data['Organisor_id']=$org_id;
            $rel_data['Role_id']=4;
            $this->db->insert('relation_event_user',$rel_data);
        }

        $view=$this->db->select('*')->from('user_views')->where('event_id',$event_id)->where('view_type','1')->get()->row_array();
        $attendee_data['Event_id']=$event_id;
        $attendee_data['Attendee_id']=$user_id;
        $attendee=$this->db->select('*')->from('event_attendee')->where($attendee_data)->get()->row_array();
        if(count($attendee) < 1)
        {
            $attendee_data['views_id']=$view['view_id'];
            $attendee_data['extra_column']=!empty($postarr['extra_column']) ? $postarr['extra_column'] : NULL;
            $this->db->insert('event_attendee',$attendee_data);
            $this->assign_user_group_CRF($event_id,$attendee_data['Attendee_id']);
        }

        $agenda_categories=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->get()->result_array();
        if(count($agenda_categories)<=1)
        {
            $agenda_id=$agenda_categories[0]['Id'];
        }
        else
        {
            $peares=$this->db->select('*')->from('agenda_categories ac')->where('event_id',$event_id)->where('categorie_type','1')->get()->result_array();
            $agenda_id=$peares[0]['Id'];
        }
        if(!empty($agenda_id))
        {
            $assigagenda=$this->db->select('*')->from('attendee_agenda_relation')->where('attendee_id',$user_id)->where('event_id',$event_id)->get()->result_array();
            if(count($assigagenda) < 1)
            {
                $rel_agenda['attendee_id']=$user_id;
                $rel_agenda['event_id']=$event_id;
                $rel_agenda['agenda_category_id']=$agenda_id;
                $this->db->insert('attendee_agenda_relation',$rel_agenda);
            }
        }
    }
    public function get_all_session_by_event($eid)
    {
        $this->db->select('a.*')->from('agenda a');
        $this->db->join('agenda_category_relation acr','acr.agenda_id=a.Id');
        $this->db->where('a.Event_id',$eid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function check_session_save($aid,$uid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('*',false)->from('users_agenda ua');
        $where = "FIND_IN_SET('".$aid."',ua.agenda_id) > 0";
        $this->db->where($where);
        $this->db->where('ua.user_id',$uid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    /*Api Attendee data Code Start*/
    public function save_all_temp_attendee_email($email_arr)
    {
        $this->db->insert_batch('temp_api_attendee_email',$email_arr);

        $this->db->query('DELETE n1 FROM temp_api_attendee_email n1, temp_api_attendee_email n2 WHERE n1.id > n2.id AND n1.email = n2.email');
    }
    public function get_all_temp_attendee_email()
    {
        $this->db->select('id,email')->from('temp_api_attendee_email');
        $this->db->limit(100);
        return $this->db->get()->result_array();
    }
    public function add_attendee_from_api($attendee,$eid)
    {
        $allowexhcontact=$attendee['allowexhcontact'];
        unset($attendee['allowexhcontact']);
        $this->db->select('*')->from('user');
        $this->db->where('Email',$attendee['Email']);
        $res=$this->db->get()->result_array();
        if(count($res)>0)
        {
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id',$eid);
            $this->db->where('User_id',$res[0]['Id']);
            $reldata=$this->db->get()->result_array();
            if(count($reldata) > 0)
            {
                $this->db->where('Event_id',$eid);
                $this->db->where('User_id',$res[0]['Id']);
                $this->db->update('relation_event_user',array('Role_id'=>'4'));
            }
            else
            {
                $reldata['User_id']=$res[0]['Id'];
                $reldata['Event_id']=$eid;
                $reldata['Organisor_id']=$attendee['Organisor_id'];
                $reldata['Role_id']='4';
                $this->db->insert('relation_event_user',$reldata);
            }
            $user_id=$res[0]['Id'];
        }
        else
        {
            $this->db->insert('user',$attendee);
            $user_id=$this->db->insert_id();
            $reldata['User_id']=$user_id;
            $reldata['Event_id']=$eid;
            $reldata['Organisor_id']=$attendee['Organisor_id'];
            $reldata['Role_id']='4';
            $this->db->insert('relation_event_user',$reldata);
        }

        $event_attendee['Event_id']=$eid;
        $event_attendee['Attendee_id']=$user_id;
        $this->db->select('*')->from('event_attendee');
        $this->db->where($event_attendee);
        $eares=$this->db->get()->result_array();
        if(count($eares) > 0)
        {
            $this->db->update('event_attendee',array('allow_contact_me'=>$allowexhcontact));
        }
        else
        {
            $event_attendee['views_id']='163';
            $event_attendee['allow_contact_me']=$allowexhcontact;
            $this->db->insert('event_attendee',$event_attendee);
        }
        return $user_id;
    }
    public function remove_temp_email_address($email)
    {
        $this->db->where('email',$email);
        $this->db->delete('temp_api_attendee_email');
    }
    public  function get_user_id_by_email_in_api($email)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Email',$email);
        $res=$this->db->get()->result_array();
        return $res[0]['Id'];
    }
    public function get_exhibitor_id_by_email_in_api($email,$eid)
    {
        $user_id=$this->get_user_id_by_email_in_api($email);

        $this->db->select('*')->from('exibitor');
        $this->db->where('Event_id',$eid);
        $this->db->where('user_id',$user_id);
        $res=$this->db->get()->result_array();
        return $res[0]['Id'];
    }
    public function add_attendee_exibitor_metting($mettingdata)
    {
        $this->db->select('*')->from('exhibitor_attendee_meeting');
        $this->db->where($mettingdata);
        $mres=$this->db->get()->result_array();
        if(count($mres) > 0)
        {
            $this->db->update('exhibitor_attendee_meeting',array('status'=>'1')); 
            return $mres[0]['Id'];  
        }
        else
        {
            $mettingdata['status']='1';
            $this->db->insert('exhibitor_attendee_meeting',$mettingdata);
            return $this->db->insert_id();
        }
    }
    public function get_all_user_email_by_date($fdate,$tdate,$eid,$rid)
    {
        $this->db->select('u.Email')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id','right');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('reu.Role_id',$rid);
        $this->db->where('u.Created_date BETWEEN "'.$fdate.'" AND "'.$tdate.'"',NULL,false);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_attendee_user_details_by_user_email($email,$eid)
    {
        $this->db->select('(case when u.Company_name IS NULL then "" ELSE u.Company_name end) as Company_name,u.Firstname,u.Lastname,u.Email,u.Password,(case when u.Street IS NULL then "" ELSE u.Street end) as address,u.Password,(case when u.Suburb IS NULL then "" ELSE u.Suburb end) as address2,(case when u.State IS NULL then "" ELSE u.State end) as State,(case when u.Country IS NULL then "" ELSE u.Country end) as Country,(case when u.Postcode IS NULL then "" ELSE u.Postcode end) as Postcode,(case when u.Mobile IS NULL then "" ELSE u.Mobile end) as phone,(case when u.Phone_business IS NULL then "" ELSE u.Phone_business end) as phone2,concat("'.base_url().'assets/user_files/","",u.Logo) as user_logo,u.Created_date,u.Active,u.Salutation,u.Title,u.Title,ea.allow_contact_me as allowexhcontact',false)->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
        $this->db->join('event_attendee ea','ea.Attendee_id=u.Id  AND ea.Event_id='.$eid,'left');
        $this->db->where('u.Email',$email);
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id',$eid);
        $res=$this->db->get()->result_array();   
        return $res;
    }
    public function get_all_metting_by_user_email($email,$eid)
    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('u.Email',$email);
        $ures=$this->db->get()->result_array();

        if(!empty($ures[0]['Id'])){
            $this->db->select('Id as metting_id,exhibiotor_id,location,date,time,status')->from('exhibitor_attendee_meeting');
            $this->db->where('attendee_id',$ures[0]['Id']);
            $this->db->where('event_id',$eid);
            $this->db->where('exhibiotor_id IS NOT NULL');
            $res=$this->db->get()->result_array();
        }
        return $res;
    }
    public function get_token()
    {
        $this->db->select('*')->from('api_token');
        $this->db->where('token_id',1);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function add_token_in_db($tarr)
    {
        $res=$this->get_token();
        if(count($res) > 0)
        {
            $this->db->where('token_id','1');
            $this->db->update('api_token',$tarr);
        }
        else
        {
            $this->db->insert('api_token',$tarr);
        }
    }
    public function insert_temp_metting_data($demodata)
    {
        $this->db->insert_batch('temp_meetting_data',$demodata);
    }
    public function get_all_temp_metting_data()
    {
        $this->db->select('*')->from('temp_meetting_data');
        $this->db->limit(50);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function remove_temp_meeting_data($mid)
    {
        $this->db->where('mid',$mid);
        $this->db->delete('temp_meetting_data');
    }
    /*Api Attendee data Code End*/
     public function add_user_group($data)
    {   
        $this->db->insert('user_group',$data);
        return $this->db->insert_id();
    }
    public function get_user_group($event_id)
    {
        $this->db->select('*');
        $this->db->where('event_id',$event_id);
        $res = $this->db->from('user_group')->get()->result_array();
        return $res;
    }
    public function assign_group($data,$event_id)
    {   
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id',$event_id);
        $this->db->where('Attendee_id',$data['Attendee_id']);
        $query = $this->db->get();
        $res = $query->result_array();
        if(count($res) > 0)
        {
            $this->db->where('Event_id',$event_id);
            $this->db->where('Attendee_id',$data['Attendee_id']);
            unset($data['Attendee_id']);
            $this->db->update('event_attendee',$data);
        }
        else
        {
            $data['Event_id']=$event_id;
            $this->db->insert('event_attendee',$data);
        }
    }
    public function user_group_check($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $res = $this->db->get('user_group')->row_array();
        if(!empty($res))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function add_group_permisson($where,$update)
    {
        $this->db->where($where);
        $this->db->update('user_group',$update);
        return true;
    }
    public function get_attendee_id($event_id,$where,$group_id)
    {   
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$event_id,'left');
        $this->db->where($where);
        $this->db->where('reu.Role_id','4');
        $res = $this->db->get()->result_array();
        foreach($res as $key => $value)
        {
            $insert_data['Attendee_id']=$value['Id'];
            $insert_data['group_id']=$group_id;
            $this->assign_group($insert_data,$event_id);
        }
        return true;
    }   
    public function delete_group($id,$event_id)
    {
        $this->db->where('id',$id);
        $this->db->delete('user_group');

        $this->db->select('*');
        $this->db->where('event_id',$event_id);
        $this->db->where("FIND_IN_SET($id,permitted_group) != ",0);
        $res = $this->db->get('user_group')->result_array();

        foreach($res as $key => $value)
        {
            $tmp = explode(',',$value['permitted_group']);
            $tmp = array_diff($tmp,[$id]);
            $update['permitted_group'] = implode(',',$tmp);
            $this->db->where('id',$value['id']);
            $this->db->update('user_group',$update);
        }
        return true;
    }
    public function update_group($update,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('user_group',$update);
        return true;
    }
    public function direct_assign_agenda_category($email,$category_name,$event_id)
    {
        /*$agendares=$this->db->select('*')->from('agenda_categories')->where('event_id',$event_id)->like('category_name',$category_name)->get()->row_array();*/
        $agenda_category_id=383;
        $agenda_category_id=$agendares['Id'];

        $userres=$this->db->select('u.*')->from('user u')->join('relation_event_user reu','u.Id=reu.User_id')->where('reu.Event_id',$event_id)->where('u.Email',$email)->group_by('u.Id')->get()->row_array();
        $user_id=$userres['Id'];

        if(!empty($agenda_category_id) && !empty($user_id))
        {
            $relation['attendee_id']=$user_id;
            $relation['agenda_category_id ']=$agenda_category_id;
            $relation['event_id']=$event_id;   
            $this->add_agenda_relation($relation);
        }
    }
    public function save_email_agenda($email,$agenda_code,$event_id)
    {
        $sessionres=$this->db->select('*')->from('agenda')->where('Event_id',$event_id)->where('agenda_code',$agenda_code)->get()->row_array();
        $sessionid=$sessionres['Id'];

        $userres=$this->db->select('u.*')->from('user u')->join('relation_event_user reu','u.Id=reu.User_id')->where('reu.Event_id',$event_id)->where('u.Email',$email)->group_by('u.Id')->get()->row_array();
        $user_id=$userres['Id'];

        if(!empty($sessionid) && !empty($user_id))
        {
            $save_session=$this->db->select('*')->from('users_agenda')->where('user_id',$user_id)->get()->row_array();
            if(count($save_session) > 0)
            {
                $save_session_arr=array_filter(explode(",",$save_session['agenda_id']));
                array_push($save_session_arr,$sessionid);
                $save_session_arr=array_unique($save_session_arr);
                $this->db->where('user_id',$user_id);
                $this->db->update('users_agenda',array('agenda_id'=>implode(",", $save_session_arr)));
            }
            else
            {
                $this->db->insert('users_agenda',array('agenda_id'=>$sessionid,'user_id'=>$user_id));
            }
        }
    }
    public function assign_user_group_CRF($event_id,$attendee_id)
    {   
        if($event_id == '570')
        {
            $data['Event_id'] = $event_id;
            $data['Attendee_id'] = $attendee_id;
            $role = $this->db->where('Event_id',$event_id)->where('User_id',$attendee_id)->get('relation_event_user')->row_array();
            if($role['Role_id'] == '4')
            {
                $res = $this->db->where($data)->get('event_attendee')->row_array();
                if(!empty($res))
                {   
                    $update['group_id'] = '20';
                    $this->db->update('event_attendee',$update);
                }
                else
                {
                    $data['group_id'] = '20';
                    $this->db->insert('event_attendee',$data);
                }
            }
        }
    }
    // make event_attendee extra_column json
    /*public function add_extra_column_temp($event_id)
    {
        //error_reporting(E_ALL);
        $attendees = $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->get()->result_array();
        $questions = $this->db->select('*')->from('reg_que')->where('event_id',$event_id)->get()->result_array();
        echo "<pre>";
           
        foreach ($attendees as $key => $value) {
            if($value['extra_column'] == '"null"' || $value['extra_column'] == ''  )
            {
                foreach ($questions as $key => $que) {
                    $ans = $this->db->select('*')->from('reg_que_ans')->where('que_id',$que['id'])->where('user_id',$value['Attendee_id'])->get()->row_array();
                    if($que['type'] == '2')
                        $json[$que['question']] = (!empty($ans)) ? explode(',', $ans['ans']) : [];
                    else
                        $json[$que['question']] = (!empty($ans)) ? $ans['ans'] : '';
                    
                }
                $update['extra_column'] = json_encode($json);
                $this->db->where('Id',$value['Id'])->update('event_attendee',$update);

            }
        }
    }*/

    public function get_attendee_list_pagination($id = null,$limit=0,$start,$keyword='') 
    {
        $orid = $this->data['user']->Id;
        //$this->db->query('SET group_concat_max_len = 2048');
        $this->db->select('u.*,c.country_name as Country,r.Id as Rid,u.Id as uid,u.Active,an.Id as note_id,ea.extra_column,ea.views_id,ea.group_id,uv.view_name,ug.group_name,concat(IFNULL(umr.Firstname,"")," ",IFNULL(umr.Lastname,"")) as moderator_name,sfd.json_data,ea.check_in_attendee,(select GROUP_CONCAT(ac.category_name) as category from attendee_agenda_relation aar left join agenda_categories ac on ac.Id=aar.agenda_category_id where aar.event_id='.$id.' and ac.event_id='.$id.' and aar.attendee_id=u.Id) as category_name,ea.check_in_time,ea.check_out_time',false);
        //$this->db->select('u.*,r.Id as Rid,u.Id as uid,u.Active,an.Id as note_id,ea.extra_column,ea.views_id,ug.group_name,uv.view_name,concat(IFNULL(umr.Firstname,"")," ",IFNULL(umr.Lastname,"")) as moderator_name,ac.category_name,sfd.json_data,ea.check_in_attendee,ea.check_in_time,ea.check_out_time',false);
        $this->db->from('user u');
        $this->db->join('event_attendee ea','ea.Attendee_id=u.Id and ea.Event_id='.$id,'left');
        $this->db->join('user_views uv','ea.views_id=uv.view_id','left');
        $this->db->join('user_group ug','ea.group_id=ug.id','left');
        //$this->db->join('attendee_agenda_relation aar','aar.attendee_id=u.Id and aar.event_id='.$id,'left');
        //$this->db->join('agenda_categories ac','ac.Id=aar.agenda_category_id','left');
        $this->db->join('signup_form_data sfd','sfd.user_id=u.Id','left');
        $this->db->join('moderator_relation mr','mr.user_id=u.Id','left');
        $this->db->join('user umr','umr.Id=mr.moderator_id','left');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('attendee_notes an','an.User_id=u.Id','left');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id) 
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->join('country c','c.id = u.Country','left');
        $this->db->where('r.Id',4);
        if($keyword!='')
        {
            $where = "((u.Email like '%".$keyword."%' OR u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%' OR u.Title like '%".$keyword."%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";
            $this->db->where($where);
        }
        $this->db->order_by('u.Id','desc');
        $this->db->group_by('u.Id');
        if($limit>0)
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function get_attendee_count($id = null,$keyword='') 
    {
        $orid = $this->data['user']->Id;
        $this->db->select('u.Id',false);
        $this->db->from('user u');
        $this->db->join('event_attendee ea','ea.Attendee_id=u.Id and ea.Event_id='.$id,'left');
        $this->db->join('user_views uv','ea.views_id=uv.view_id','left');
        $this->db->join('user_group ug','ea.group_id=ug.id','left');
        $this->db->join('signup_form_data sfd','sfd.user_id=u.Id','left');
        $this->db->join('moderator_relation mr','mr.user_id=u.Id','left');
        $this->db->join('user umr','umr.Id=mr.moderator_id','left');
        $this->db->join('relation_event_user ru','ru.User_id = u.Id');
        $this->db->join('attendee_notes an','an.User_id=u.Id','left');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id) 
        {
            $this->db->where('ru.Event_id', $id);
        }
        if($keyword!='')
        {
            $where = "((u.Email like '%".$keyword."%' OR u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%' OR u.Title like '%".$keyword."%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";
            $this->db->where($where);
        }
        $this->db->join('country c','c.id = u.Country','left');
        $this->db->where('r.Id',4);
        $this->db->order_by('u.Id','desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $count = $query->num_rows();
        return $count;
    }
    public function add_attendee_categorie($cdata) //Monday 26 March 2018 06:08:00 PM IST
    {   
        $cdata['created_date']=date('Y-m-d H:i:s');
        $this->db->insert('attendee_category',$cdata);
        return $this->db->insert_id();
    }
    public function update_attendee_categorie($cdata,$cid) //Monday 26 March 2018 06:08:05 PM IST
    {
      $this->db->where('id',$cid);
      $this->db->update('attendee_category',$cdata);
    }
    public function get_attendee_categories_by_event($eid,$cid=NULL) //Monday 26 March 2018 06:08:10 PM IST
    {
      $this->db->select('*')->from('attendee_category');
      if(!empty($cid))
      {
        $this->db->where('id',$cid);
      } 
      $this->db->where('menu_id','2');
      $this->db->where('event_id',$eid);
      $res=$this->db->get()->result_array();
      return $res;
    }
    public function delete_attendee_categorie($cid,$event_id='') //Monday 26 March 2018 06:08:15 PM IST
    {
        $this->db->where('id',$cid);
        $this->db->delete('attendee_category');
        if($event_id!='')
        {
            $a_cat = $this->db->select('*')->from('attendee_category')->where('event_id',$event_id)->order_by('updated_date','DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('id',$a_cat['id'])->update('attendee_category',$update);
        }
    }
    public function assign_attendee_categories($insert_data,$event_id)
    {
        $keywords =  explode(',',$insert_data['keyword']);
        foreach ($keywords as $key => $value)
        {
            $insert['keyword'] = $value;
            $insert['user_id'] = $insert_data['user_id'];
            $insert['event_id'] = $event_id;
            $res = $this->db->where($insert)->get('attendee_keywords')->row_array();
            if(empty($res))
            {
                $this->db->insert('attendee_keywords',$insert);
            }
        }
    }

    public function get_booked_meetings($event_id)
    {
          $this->db->select('concat(u.Firstname," ",u.Lastname) as sender,
                             concat(u1.Firstname," ",u1.Lastname) as reciever,
                             eam.location,eam.date,eam.time,eam.status,eam.Id',false);
          $this->db->from('exhibitor_attendee_meeting eam');
          $this->db->join('user u','eam.attendee_id=u.Id');
          $this->db->join('user u1','eam.recever_attendee_id=u1.Id');
          $this->db->where('eam.event_id',$event_id);
          $this->db->order_by('eam.date');
          $this->db->order_by('eam.time');
          $res=$this->db->get()->result_array(); 
          //echo $this->db->last_query(); exit();
          return $res;
    }
    public function delete_meeting($event_id,$id)
    {   
        $this->db->where('Id',$id);
        $this->db->where('event_id',$event_id);
        $this->db->delete('exhibitor_attendee_meeting');
        //echo $this->db->last_query(); exit();
    }
    public function checkEventDateFormat($event_id)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Id',$event_id);
        $query = $this->db->get();  
        $res=$query->result_array();
        return $res;
    } 
    public function SaveRequest($data)
    {
        $this->db->insert('exhibitor_attendee_meeting',$data);
    }
    public function get_attendee_id_by_group_id($event_id,$group_ids)
    {   
        $this->db->select('Attendee_id');
        $this->db->where('event_id',$event_id);
        $this->db->where_in('group_id',$group_ids);
        $res = array_column($this->db->get('event_attendee')->result_array(),'Attendee_id');
        return $res;
    }
    public function addFilter($data)
    {
        $this->db->where($data);
        $res = $this->db->get('attendee_filter_relation')->row_array();
        if(!$res)
        {
            $this->db->insert('attendee_filter_relation',$data);
        }
    }
    public function getFilters($event_id)
    {   
        $this->db->select('group_concat(distinct aec.value) as keywords,c.column_name,ac.category,af.id,af.label');
        $this->db->join('custom_column c','c.column_id = af.custom_column');
        $this->db->join('attendee_extra_column aec','aec.key1 = c.column_name','left');
        $this->db->join('attendee_category ac','ac.id = af.category_id');
        $this->db->where('af.event_id',$event_id);
        $this->db->group_by('af.id');
        $res = $this->db->get('attendee_filter_relation af')->result_array();
        return $res;
    }
    public function getFilter($event_id,$id)
    {
        $this->db->where('event_id',$event_id);
        $this->db->where('id',$id);
        $res = $this->db->get('attendee_filter_relation')->row_array();
        return $res;
    }
    public function updateFilter($data,$filter_id)
    {
        $this->db->where('id',$filter_id);
        $this->db->update('attendee_filter_relation',$data);
    }
    public function deleteFilter($event_id,$id)
    {
        $this->db->where('event_id',$event_id);
        $this->db->where('id',$id);
        $this->db->delete('attendee_filter_relation');
    }
    public function getCustomColumn($event_id)
    {
        $this->db->where('event_id',$event_id);
        $res = $this->db->get('custom_column')->result_array();
        return $res;
    }
    public function getRegisterdUserIds($event_id)
    {   
        $this->db->where('(user_id IN (select User_id FROM `relation_event_user` WHERE `Event_id` = "'.$event_id.'" AND `Role_id` = "4"))');
        $res = array_column($this->db->get('users_agenda')->result_array(),'user_id');
        return $res;
    }
}

?>
