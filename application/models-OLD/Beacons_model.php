<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Beacons_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }
     
     public function add_trigger($data)
     {
          //$data['created_at']=date('Y-m-d H:i:s');
          $this->db->insert('Triggers',$data);
          return $this->db->insert_id();
     }

     public function update_trigger($id,$triggerid,$data)
     {
          $this->db->where('id',$triggerid);
          $this->db->where('event_id',$id);
          $this->db->update('Triggers',$data);
     }

     public function get_triggers($id)
     {
     
          $this->db->protect_identifiers=false;
          $this->db->select('t.*,GROUP_CONCAT(b.beacon_name ORDER BY b.Id) as Becons');
          $this->db->from('Triggers t');
          $this->db->join("Beacons b","FIND_IN_SET(b.Id, t.beacon_id)");
          $this->db->where('t.event_id', $id);
          $this->db->group_by('t.Id');
          
          $query = $this->db->get();
          $res = $query->result_array();
          
          return $res;
     }

     public function get_trigger($id)
     {
     
          $this->db->select('t.*');
          $this->db->from('Triggers t');
          $this->db->where('t.Id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          if(!$res)
          return redirect('Forbidden');
          return $res;
     }


     public function edit_trigger($data)
     {

     }

     public function get_all_beacons_by_id($id)
     {
          $this->db->select('*');
          $this->db->from('Beacons');
          $this->db->where('event_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

     public function add_heat_map($data)
     {
          $this->db->insert('beacon_heatmap',$data);
          return $this->db->insert_id();
     }

     public function edit_heat_map($data)
     {

     }

     public function delete_trigger($id,$cid)
     {
          $this->db->where('Id', $id);
          $this->db->delete('Triggers');
          $this->db->last_query();
     }

     public function delete_heatmap($id)
     {    
          $this->db->where('Id', $id);
          $this->db->delete('beacon_heatmap');
          $this->db->last_query();
     }

     public function delete_beacon($id,$cid)
     {
          $this->db->where('Id', $id);
          $this->db->delete('Beacons');
          $this->db->last_query();
     }

     public function update_beacon($id,$data)
     {
          $this->db->where('Id',$id);
          $this->db->update('Beacons',$data);
     }

     public function get_heatmaps($event_id)
     {
          $this->db->select('bh.*,b.*,bh.Id as beacon_heatmap_id');
          $this->db->from('beacon_heatmap bh');
          $this->db->join('Beacons b','bh.beacons_id=b.Id','left');
          $this->db->where('bh.event_id',$event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          //echo "<prev>";print_r($res);die;
          return $res;
     }

     public function get_heatmap($id,$mid)
     {
          $this->db->select('b.*');
          $this->db->from('beacon_heatmap b');
          $this->db->where('b.event_id', $id);
          $this->db->where('b.Id',$mid);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

     public function save_heatmap_coords($data)
     {
          $this->db->insert('heatmap_maping',$data);
          return $this->db->insert_id();
     }

     public function delete_heatmap_coords($id)
     {
          $this->db->where('Id', $id);
          $this->db->delete('heatmap_maping');
          $this->db->last_query();
     }

     public function get_heatmap_mapping_data($id,$mid)
     {
          $this->db->select('h.*,b.beacon_name');
          $this->db->from('heatmap_maping h');
          $this->db->join('Beacons b','h.beacons_id=b.Id','left');
          $this->db->where('b.event_id',$id);
          $this->db->where('h.heatmap_id', $mid);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;

     }

     public function get_heat_movements($heatmap_id)
     {
          $this->db->select('h.*,bm.number_of_hits');
          $this->db->from('heatmap_maping h');
          $this->db->join('beacon_movements bm','h.beacons_id=bm.beacon_id','left');
          $this->db->where('h.heatmap_id', $heatmap_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }

     public function show_heat_movements($event_id)
     {
          $this->db->select('bm.number_of_hits,b.beacon_name,b.Id');
          $this->db->from('beacon_movements bm');
          $this->db->join('Beacons b','bm.beacon_id = b.Id');
          $this->db->where('b.event_id',$event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;

     }

}

?>