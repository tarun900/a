<?php
class lead_representative_model extends CI_Model
{
	function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function get_edit_attendee_informaion_event_by_user_id($eid,$uid)
    {
        $this->db->select('u.*,reu.Role_id,ea.extra_column,ea.check_in_attendee,ea.views_id,ea.group_id')->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id And reu.Event_id='.$eid);
        $this->db->join('event_attendee ea','u.Id=ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('reu.Role_id','4');
        $this->db->where('u.Id',$uid);
        $this->db->group_by('u.Id');
        return $this->db->get()->row_array();
    }
    public function get_all_custom_column_data($eid)
	{
		$this->db->select('*')->from('custom_column');
		$this->db->where('event_id',$eid);
		$que=$this->db->get();
		$res=$que->result_array();
		return $res;
	}
	public function get_all_exibitor_user_questions($eid,$ex_uid,$uid)
	{   
        $this->db->protect_identifiers=false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id','left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->group_by('eq.q_id');
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $res2 = $this->db->get()->result_array();
        foreach ($res2 as $key => $value) 
        {
            if($value['skip_logic']=='1')
            {
                $this->db->select('`option`,redirectexibitorquestion_id')->from('exibitor_question_skiplogic');
                $this->db->where('exibitor_question_id',$value['q_id']);
                $res=$this->db->get()->result_array();
                $option=array_column($res,'option');
                $redirectquestion_id=array_column($res,'redirectexibitorquestion_id');
                $finalarray=array_combine($option, $redirectquestion_id);
                $res2[$key]['redirectids']=json_encode($finalarray);
            }
            $question_array=$this->get_survey_array($eid,$ex_uid,$uid);
            $dependent_ids=$question_array['dependent_ids'];
            $independent_ids=array_values($question_array['independent_ids']);
            if(!in_array($value['q_id'],$independent_ids))
            {
                $redirectkey=$this->get_inarrayval($value['q_id'],$dependent_ids,$independent_ids);
                $rekey=array_search($redirectkey,$independent_ids);
                $redirectid=$independent_ids[$rekey+1];
            }
            else
            {
                $rekey=array_search($value['q_id'],$independent_ids);
                $redirectid=$independent_ids[$rekey+1];
            }
            $res2[$key]['redirectid']=$redirectid;
        }
        return $res2;
	}
	public function get_inarrayval($qid,$dearr,$indearr)
    { 
        $qkey=$dearr[$qid];
        if(in_array($qkey, $indearr))
        {
          return $qkey;
        }
        return $this->get_inarrayval($qkey,$dearr,$indearr); 
    }
	public function get_survey_array($eid,$ex_uid,$uid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('eqs.*')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $dependentquestion = $this->db->get()->result_array();
        $idsarr=array_unique(array_filter(array_column($dependentquestion,"redirectexibitorquestion_id")));
        $masterqarr=array_filter(array_column($dependentquestion,"exibitor_question_id"));
        $dependent_ids=array_filter(array_column($dependentquestion,"redirectexibitorquestion_id"));
        $dependentquestionarr=array_combine($dependent_ids, $masterqarr);

        $this->db->protect_identifiers=false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id','left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        if(count($idsarr) > 0)
        {
            $this->db->where_not_in('eq.q_id',$idsarr);
        }
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $independentquestion = $this->db->get()->result_array();
        $independent_ids=array_unique(array_filter(array_column($independentquestion,"q_id")));
        $res2['dependent_ids']=$dependentquestionarr;
        $res2['independent_ids']=$independent_ids;
        return $res2;
    }
    public function save_scan_lead_data($lead_data,$eid,$ex_uid,$uid)
    {
        $this->db->select('*')->from('exibitor_lead');
        $this->db->where('event_id',$eid);
        $this->db->where('exibitor_user_id',$ex_uid);
        $this->db->where('lead_user_id',$uid);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('event_id',$eid);
            $this->db->where('exibitor_user_id',$ex_uid);
            $this->db->where('lead_user_id',$uid);
            $this->db->update('exibitor_lead',$lead_data);
        }
        else
        {
            $lead_data['event_id']=$eid;
            $lead_data['exibitor_user_id']=$ex_uid;
            $lead_data['lead_user_id']=$uid;
            $lead_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('exibitor_lead',$lead_data);
        }
    }
    public function add_question_answer($question_data,$exqid,$uid,$eid)
    {
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('exibitor_questionid',$exqid);
        $this->db->where('user_id',$uid);
        $this->db->where('event_id',$eid);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('exibitor_questionid',$exqid);
            $this->db->where('user_id',$uid);
            $this->db->where('event_id',$eid);
            $this->db->update('exibitor_question_answer',$question_data);
        }
        else
        {
            $question_data['user_id'] = $uid;
            $question_data['event_id'] = $eid;
            $question_data['exibitor_questionid'] = $exqid;
            $question_data['answer_date']=date('Y-m-d H:i:s');
            $this->db->insert('exibitor_question_answer',$question_data);
        }
    }
    public function get_exibitor_by_representative_id_in_event($eid,$resp_uid)
    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id',$eid);
        $this->db->where('reps_user_id',$resp_uid);
        $res=$this->db->get()->row_array();
        return $res;
    }
    public function get_all_my_lead_by_exibitor_user($eid,$ex_uid)
    {
        $this->db->select('u.*,reu.Role_id,reu.Organisor_id,el.firstname as Firstname,el.lastname as Lastname,el.email as Email,el.title as Title,el.company_name as Company_name,el.custom_column_data')->from('user u');
        $this->db->join('exibitor_lead el','el.lead_user_id=u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$eid);
        $this->db->where('el.event_id',$eid);
        $this->db->where('el.exibitor_user_id',$ex_uid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_my_representative_by_exibitor_user($eid,$ex_uid)
    {
        $this->db->select('u.*,reu.Role_id,reu.Organisor_id')->from('user u');
        $this->db->join('exhibitor_representatives er','er.reps_user_id=u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$eid);
        $this->db->where('er.event_id',$eid);
        $this->db->where('er.exibitor_user_id',$ex_uid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_event_all_attendee_from_assing_resp($eid)
    {
        $this->db->select('u.*')->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('reu.Role_id','4');
        $this->db->where('u.Id NOT IN (select reps_user_id from exhibitor_representatives where event_id='.$eid.')');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function add_new_representative($eid,$ex_uid,$resp_uid)
    {
        $res=$this->get_exibitor_by_representative_id_in_event($eid,$resp_uid);
        if(empty($res))
        {
            $resp_data['event_id']=$eid;
            $resp_data['exibitor_user_id']=$ex_uid;
            $resp_data['reps_user_id']=$resp_uid;
            $resp_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_representatives',$resp_data);
        }
    }
    public function get_total_representative_by_exibitor_user($eid,$ex_uid)
    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id',$eid);
        $this->db->where('exibitor_user_id',$ex_uid);
        $res=$this->db->get()->result_array();
        return count($res);
    }
    public function get_exibitor_user_premission($eid,$ex_uid)
    {
        $this->db->select('*')->from('exibitor_premission');
        $this->db->where('event_id',$eid);
        $this->db->where('user_id',$ex_uid);
        $res=$this->db->get()->row_array();
        return $res;
    }
    public function remove_my_representative($eid,$resp_uid)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('reps_user_id',$resp_uid);
        $this->db->delete('exhibitor_representatives');
    }
    public function get_all_exibitor_user_questions_for_export($eid,$ex_uid)
    {
        $this->db->protect_identifiers=false;
        $this->db->select('eq.*')->from('exibitor_question eq');
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_user_question_answer($eid,$uid,$qid)
    {
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('event_id',$eid);
        $this->db->where('user_id',$uid);
        $this->db->where('exibitor_questionid',$qid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_lead_role($event_id)
    {
        $res = $this->db->where('event_id',$event_id)->where('role_type','1')->get('role')->result_array();
        return $res;
    }

    public function addRole($create_data)
    {
        $this->db->insert('role',$create_data);
    }

    public function updateRole($update_data,$where)
    {
        $this->db->where($where);
        $this->db->update('role',$update_data);
    }
    public function deleteRole($where)
    {
        $this->db->where($where);
        $this->db->delete('role');
    }

    public function add($postarr,$eid,$Organisor_id,$current_user_id,$current_user_role)
    {
        $userdata=$this->db->select('*')->from('user')->where('Email',$postarr['Email'])->get()->row_array();
        if(count($userdata) > 0)
        {   
            $return = true;
            $rel_data['User_id']=$userdata['Id'];
            $check_org = $this->db->where($rel_data)->get('relation_event_user')->row_array();
            if($check_org['Role_id'] == '3')
            {
                return false;
            }
            $rel_data['Event_id']=$eid;
            $rel_data['Organisor_id']=$Organisor_id;
            $relation=$this->db->where($rel_data)->get('relation_event_user')->row_array();
            if(count($relation) < 1)
            {
                $rel_data['Role_id']=$current_user_role;
                $this->db->insert('relation_event_user',$rel_data);
            }
            elseif($relation['Role_id'] != '3')
            {   unset($rel_data['Event_id']);

                $this->db->where($relation);
                $rel_data['Role_id']=$current_user_role;
                $this->db->update('relation_event_user',$rel_data);
            }
            elseif($relation['Role_id'] == '3')
            {
                return false;
            }


            $rep_data['event_id']=$eid;
            $rep_data['user_id']=$current_user_id;
            $rep_data['rep_id']=$userdata['Id'];
            $relation=$this->db->select('*')->from('user_representatives')->where($rep_data)->get()->row_array();
            if(count($relation) < 1)
            {
                $this->db->insert('user_representatives',$rep_data);
            }
            $this->db->where('Id',$userdata['Id']);
            $this->db->update('user',$postarr);
        }
        else
        {
            $postarr['Active']='1';
            $postarr['Organisor_id']=$Organisor_id;
            $postarr['Created_date']=date('Y-m-d H:i:s');
            $this->db->insert('user',$postarr);
            $user_id=$this->db->insert_id();
            $return = true;
            $rel_data['Event_id']=$eid;
            $rel_data['User_id']=$user_id;
            $relation=$this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->row_array();
            if(count($relation) < 1)
            {
                $rel_data['Organisor_id']=$Organisor_id;
                $rel_data['Role_id']=$current_user_role;
                $this->db->insert('relation_event_user',$rel_data);
            }

            $rep_data['event_id']=$eid;
            $rep_data['user_id']=$current_user_id;
            $rep_data['rep_id']=$user_id;
            $relation=$this->db->select('*')->from('user_representatives')->where($rep_data)->get()->row_array();
            if(count($relation) < 1)
            {
                $this->db->insert('user_representatives',$rep_data);
            }
        }
        return $return;
    }

    public function getLeadUsers($event_id,$user_id)
    {
        return $this->db->select('*')->from('user')
            ->join('user_representatives','user_representatives.user_id = user.Id AND user_representatives.rep_id = user.Id')
            ->where('user_representatives.event_id',$event_id)
            ->where('user_representatives.user_id',$user_id)
            ->get()->result_array();
    }

    public function delete_rep($event_id,$user_id,$loggedinuserid,$rid)
    {
        $where['user_id'] = $loggedinuserid;
        $where['rep_id'] = $user_id;
        $where['event_id'] = $event_id;
        $this->db->where($where);
        $this->db->delete('user_representatives');

        unset($where);

        $where['User_id'] = $user_id;
        $where['Role_id'] = $rid;
        $where['Event_id'] = $event_id;
        $this->db->where($where);
        $this->db->delete('relation_event_user');
    }
}