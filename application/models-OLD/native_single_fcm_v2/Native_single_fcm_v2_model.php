<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Native_single_fcm_v2_model extends CI_Model 
{

	public $variable;
	public function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
		
	}
   	public function getPrivateEquityExclusiveDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1344);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getPensionBridgeAnnualDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1296);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getNapecDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',772);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getHFTPAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',55497);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',55497);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function HFTPSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 55497);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getBathandWestShowDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',978);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getIOTAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',77463);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',77463);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function IOTSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 77463);
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getNACFBDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1041);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    public function getUHGAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',20152);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',20152);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function UHGSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 20152);
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}
	public function getIdahoRealtorsDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1172);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getMuveoAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',50736);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',50736);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function MuveosearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 50736);
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}
	
	public function getCityWeekDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1124);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getAICEDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1361);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getVenusDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1383);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getEuroviaDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1203);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }


    public function getSourceMediaAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',318677);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',318677);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function SourceMediasearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 318677);
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getConnectechDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',870);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getEpsonDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',875);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getiAmericas2018DefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1278);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getMIT2018DefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1067);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getGlobalRegTechDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1367);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getBigDataTorontoDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1285);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getSIAMAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',287975);
		//$this->db->where('(e.secure_key IS NOT NULL OR e.Event_type = 3)');

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',287975);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function SIAMsearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 287975);
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}
	
    public function getRetailBulletinPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
	 // $this->db->where_in('e.Event_type',);
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',45783);
		$this->db->where('(e.secure_key IS NOT NULL OR e.Event_type = 3)');

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		//$this->db->where('e.Event_type','3');
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',45783);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function getRetailBulletinMasterEvent()
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1515);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		return $res;
	}

	public function getClosingTheGapDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1270);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getPodcastMovementDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1381);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getUHGLatestDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1169);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }


    public function getFIMEDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1580);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getSmartAirportsDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1543);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getHowardhughesDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1573);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getDynamiteAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',323823);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',323823);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function DynamiteSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 323823);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getFoodMattersDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1420);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getConcordiaAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',15663);
	    $this->db->where('e.Id !=','1689');
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',15663);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function ConcordiaSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 15663);
	    $this->db->where('e.Id !=','1689');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}
	public function getConcordiaMasterEvent()
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		/*$this->db->where('e.Event_type','3');
		$this->db->where('e.Status','1');*/
		$this->db->where('e.id',1689);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		return $res;
	}
	public function getCompassionateFriendsDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1425);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }


    public function NCRealtorDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1362);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }


	public function getNCRealtorAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',311376);
	    $this->db->where('e.Id !=','1362');
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',311376);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}
	public function NCRealtorSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 311376);
	    $this->db->where('e.Id !=','1689');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getPurposeBuiltCommunitiesDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1114);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getPPMADefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1394);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

	public function getTricordAllPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',10331);
	    $this->db->where('e.Id !=','1362');
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',10331);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}
	public function TricordSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 10331);
	    $this->db->where('e.Id !=','1689');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}

	public function getWarriorDefaultEvent()
    {
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1185);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    
    public function getAASADefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1761);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    
    public function getDCACDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1660);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getVitafoodsDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1397);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getFinancialServicesDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1741);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getAlignHRDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1509);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getFranklinCoveyDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1341);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    
    public function getPCAWaterproofingDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1738);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

     public function getCRForumLisbonDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1594);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getIPExpoEuropeDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1682);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getBTSConferenceDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1821);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getOTRFilmFestDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1532);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getGemConferenceDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1683);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getLogisticsDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1323);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getClarionDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1825);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    
    public function getShareFair2018DefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1816);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }

    public function getIAAArkansasDefaultEvent()
    {
	    $this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->where('e.id',1856);
		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res=$oqu->result_array();
      	return $res;
    }
    public function getParaxelPublicEvents($gcm_id)
	{
		$date = date('Y-m-d H:i:s',strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('e.Status','1');
	    $this->db->where('e.Organisor_id',382025);

		$this->db->order_by("e.Start_date","desc");
		$oqu=$this->db->get();
		$res1=$oqu->result_array();
		foreach ($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.id","left");
		$this->db->join("user_event_relation ur","ur.event_id=e.id");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->where('ur.gcm_id',$gcm_id);
	    $this->db->where('e.Organisor_id',382025);
		$this->db->where('e.Status','1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if(!empty($ids))
			$this->db->where_not_in('e.id',$ids);
		$this->db->order_by("e.Start_date","desc");
      	$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");

		$this->db->group_by("e.id");
		$oqu=$this->db->get();
		$res2=$oqu->result_array();

		$res = array_merge($res1,$res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach ($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;

		}
		return $return;
	}

	public function ParaxelSearchEvent($event_name)
	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen',FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
		$this->db->join("user u","u.Id=e.Organisor_id","left");
		$this->db->like('e.Event_name',$event_name);
		$this->db->where('e.Organisor_id', 382025);
		//$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu=$this->db->get();
		$res=$oqu->result_array();
		$return = [];
		foreach ($res as $key => $value)
		{	
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)
				$return[] = $value;
		}
		return $return;
	}
}
