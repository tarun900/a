<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_groups($event_id,$menu_id=NULL)
	{
		$this->db->select('*,CASE WHEN group_image IS NULL THEN "" ELSE group_image END as group_image',FALSE);
		$this->db->where('event_id',$event_id);
		if(!empty($menu_id))
			$this->db->where('menu_id',$menu_id);
		$res = $this->db->get('modules_group')->result_array();
		return $res;
	}
	public function get_super_groups($event_id,$menu_id=NULL)
	{
		$this->db->select('*,CASE WHEN group_image IS NULL THEN "" ELSE group_image END as group_image',FALSE);
		$this->db->where('event_id',$event_id);
		if(!empty($menu_id))
			$this->db->where('menu_id',$menu_id);
		$res = $this->db->get('modules_super_group')->result_array();
		return $res;
	}
	public function group_relation($event_id)
	{	
		$this->db->select('mr.*');
		$this->db->join('modules_group gr','gr.module_group_id = mr.group_id');
		$this->db->where('gr.event_id',$event_id);
		$res = $this->db->get('modules_group_relation mr')->result_array();
		return $res;
	}
	public function super_group_relation($event_id)
	{	
		$this->db->select('mr.*');
		$this->db->join('modules_super_group gr','gr.id = mr.super_group_id');
		$this->db->where('gr.event_id',$event_id);
		$res = $this->db->get('super_group_relation mr')->result_array();
		return $res;
	}
}
