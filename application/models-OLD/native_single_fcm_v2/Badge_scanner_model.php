<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Badge_scanner_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function share_cotact($event_id,$user_id,$code)
	{	
		$this->db->select('u.Id');
		$this->db->join('relation_event_user reu','reu.User_id = u.Id');
		$this->db->where('reu.Role_id','4');
		$this->db->where('u.Email',$code);
		$user = $this->db->get('user u')->row_array();
		if($user)
		{
			$data['from_id'] = $user_id;
			$data['to_id'] = $user['Id'];
			$data['event_id'] = $event_id;
			$data['contact_type'] = '0';
			$share_cotact = $this->db->where($data)->get('attendee_share_contact')->row_array();
			$data['approval_status'] = '1';
			if(!empty($share_cotact))
			{
				$res['status'] = false;
				$res['msg'] = "You have already saved this contact.";
			}
			else
			{
				$this->db->insert('attendee_share_contact',$data);
				$res['status'] = true;
				$res['msg'] = "Contact Saved Successfully.";
			}
		}
		else
		{
			$res['status'] = false;
			$res['msg'] = "No User found for this code.";
		}
		return $res;
	}
	public function get_user_id_by_code($code)
	{
		$this->db->select('u.Id');
		$this->db->join('relation_event_user reu','reu.User_id = u.Id');
		$this->db->where('reu.Role_id','4');
		$this->db->where('u.Email',$code);
		$user = $this->db->get('user u')->row_array();
		return $user['Id'];
	}
	public function save_exhi_lead($data)//podcast lead scan
	{
		$res = $this->db->where($data)->get('podcast_exhi_lead')->row_array();
		if(empty($res))
		{
			$this->db->insert('podcast_exhi_lead',$data);
		}
	}
	public function getEmailFromCode($user_id)
	{
		$this->db->where('Id',$user_id);
		$res = $this->db->get('user')->row_array();
		return $res['Email'];
	}
}