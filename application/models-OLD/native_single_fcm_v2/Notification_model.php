<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Notification_model extends CI_Model
{
	public function getGeoNoti($event_id,$user_id)
    {	
    	$this->db->select('n.Id,n.title,n.content,COALESCE(n.moduleslink,"") as moduleslink,
    						COALESCE(n.custommoduleslink,"") as custommoduleslink,n.lat,n.long,n.radius,n.address,n.event_id');
    	$this->db->join('user_module_notification un','un.notification_id = n.Id','left');
    	$this->db->where("FIND_IN_SET('$user_id',n.user_ids) != 0",NULL,FALSE);
        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0 OR FIND_IN_SET('$user_id',un.user_id) IS NULL)",NULL,FALSE);
    	$this->db->where('n.event_id',$event_id);
    	$this->db->where('n.notification_type','3');
    	$res = $this->db->get('notification n')->result_array();
    	return $res;
    }

    public function updateUserNoti($user_id,$noti_id)
    {
    	$user_noti = $this->db->where('notification_id',$noti_id)->get('user_module_notification')->row_array();
    	if(empty($user_noti))
    	{	
    		$insert['notification_id'] = $noti_id;
    		$insert['user_id'] = $user_id;
    		$this->db->insert('user_module_notification',$insert);
    	}
    	else
    	{
    		$user_ids = explode(',',$user_noti['user_id']);
    		$user_ids[] = $user_id;
    		$update['user_id'] = implode(',',array_unique(array_filter($user_ids)));
    		$this->db->where('notification_id',$noti_id);
    		$this->db->update('user_module_notification',$update);
    	}
    }
    public function getNoti($id)
    {
        $this->db->where('Id',$id);
        $res = $this->db->get('notification')->row_array();
        return $res;
	}   
	public function updateNotificationLog($update,$where)
    {
        $this->db->where($where);
        $this->db->update('notification_logs',$update);
    }    
}