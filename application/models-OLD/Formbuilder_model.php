<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class formbuilder_model extends CI_Model
{
     function __construct()
     {
          parent::__construct();
     }
    public function get_form_list($intEventId=null,$intFormId=null)
    {
        $this->db->select('*');
        $this->db->from('forms');
        $this->db->where('event_id',$intEventId);
        if(!empty($intFormId))
        {
            $this->db->where('id',$intFormId);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($intFormId) && !$res)
          return redirect('Forbidden');
        return $res;
    }

    public function get_event_from_data($eid)
    {
        $this->db->select('*');
        $this->db->from('form_data');
        $this->db->where('event_id',$eid);
        $query=$this->db->get();
        $res=$query->result_array();
        return $res;
    }
    public function get_singup_form_list($eid)
    {
        $this->db->select('*');
        $this->db->from('signup_form');
        $this->db->where('event_id',$eid);
        $query=$this->db->get();
        $res=$query->result_array();
        return $res;
    }
    public function get_singup_form_data_userwise($uid)
    {
        $this->db->select('*');
        $this->db->from('signup_form_data');
        $this->db->where('user_id',$uid);
        $query=$this->db->get();
        $res=$query->result_array();
        return $res;
    }
    
    public function get_evnt_form_list($intEventId=null)
    {
       $query =  $this->db->query("SELECT fom.*,GROUP_CONCAT(DISTINCT(m.menuname)) AS std_menu,GROUP_CONCAT(DISTINCT(cm.Menu_name)) AS cms_menu
                                FROM forms fom
                                LEFT JOIN menu m ON FIND_IN_SET(m.id,fom.m_id)
                                LEFT JOIN cms cm ON FIND_IN_SET(cm.Id,fom.cms_id) 
                         WHERE fom.event_id = '".$intEventId."' GROUP BY fom.id
                         ORDER BY fom.id,m.menuname");
       $res = $query->result_array();
       return $res;
    }
    public function add_form($data=array())
    {
      $data['created_date']=date('Y-m-d H:i:s');
      $this->db->insert('forms',$data);
      $intLastId = $this->db->insert_id();
      return $intLastId;
    }
    public function add_signup_form($data=array(),$eid)
    {

          $this->db->select('id')->from('signup_form');
          $this->db->where('event_id', $eid);
          $query = $this->db->get();
          $res = $query->result_array();
          $cnt = count($res);
          if ($cnt == 0)
          {

               $this->db->insert('signup_form', $data);
          }
          else
          {
               $this->db->where("event_id", $eid);
               $this->db->update('signup_form', $data);
          }
    }
    
    public function update_form($data=array())
    {
        $arrUpdate['json_data'] = $data['json_data']; 
        $this->db->where('id', $data['id']);
        $this->db->where('event_id', $data['event_id']);
        $this->db->update('forms', $arrUpdate);
        return true;
    }
    public function delete_form($intFormId,$intEventId)
    {
        $this->db->where('id', $intFormId);
        $this->db->where('event_id', $intEventId);
        $this->db->delete('forms');
        return true; 
    }
    public function update_form_module($data=array())
    {
        $arrUpdate['m_id'] = $data['m_id'];
        $arrUpdate['cms_id'] = $data['cms_id'];
        $arrUpdate['frm_posi'] = $data['frm_posi'];
        $this->db->where('id', $data['id']);
        $this->db->where('event_id', $data['event_id']);
        $this->db->update('forms', $arrUpdate);
        return true;
    }
    public function getFormData($intEventId,$intFormId)
    {
      return $this->db->select('fdt.*,CONCAT(tu.Firstname," ", tu.Lastname) as tuname',false)->from('form_data fdt')->join('user tu','fdt.user_id=tu.Id','left')->where('fdt.event_id',$intEventId)->where('fdt.f_id',$intFormId)->get()->result_array();
    }
}
?>