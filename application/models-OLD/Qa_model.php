<?php
class Qa_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function get_qa_session_list_by_event($id,$sid=NULL)
    {
        $this->db->select('qs.*,u.Firstname,u.Lastname')->from('qa_session qs');
        $this->db->join('user u','u.Id=qs.Moderator_speaker_id','left');
        $this->db->where('qs.Event_id',$id);
        if(!empty($sid))
        {
            $this->db->where('qs.Id',$sid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($sid) && !$res)
            return redirect('Forbidden');
        return $res;
    }
        
    public function add_session($data)
    {
        if($data['session_array']['Event_id'] != NULL)
        $array_session['Event_id'] = $data['session_array']['Event_id'];

        if($data['session_array']['Session_name'] != NULL)
        $array_session['Session_name'] = $data['session_array']['Session_name'];
	
		if($data['session_array']['Session_description'] != NULL)
        $array_session['Session_description'] = $data['session_array']['Session_description'];
	
		if($data['session_array']['Moderator_speaker_id'] != NULL)
        $array_session['Moderator_speaker_id'] = $data['session_array']['Moderator_speaker_id'];

        if($data['session_array']['start_time'] != NULL)
            $array_session['start_time'] = $data['session_array']['start_time'];

        if($data['session_array']['end_time'] != NULL)
            $array_session['end_time'] = $data['session_array']['end_time'];

        if($data['session_array']['session_date'] != NULL)
            $array_session['session_date'] = $data['session_array']['session_date'];

        if($data['session_array']['is_closed_qa'] != NULL)
            $array_session['is_closed_qa'] = $data['session_array']['is_closed_qa'];

        $array_session['created_date']=date('Y-m-d H:i:s');
        $this->db->insert('qa_session',$array_session);
        /*if($data['session_array']['Event_id'] == '1511')
        {*/
                //echo "<pre";
                //print_r($array_session); exit();
               //echo $this->db->last_query(); exit()l;
        //}
        $session_id = $this->db->insert_id();
        return $session_id;
    }

    public function update_session($data)
    {   
		if($data['session_array']['Event_id'] != NULL)
        $array_session['Event_id'] = $data['session_array']['Event_id'];

        if($data['session_array']['Session_name'] != NULL)
        $array_session['Session_name'] = $data['session_array']['Session_name'];
	
		if($data['session_array']['Session_description'] != NULL)
        $array_session['Session_description'] = $data['session_array']['Session_description'];
	
		if($data['session_array']['Moderator_speaker_id'] != NULL)
        $array_session['Moderator_speaker_id'] = $data['session_array']['Moderator_speaker_id'];

        if($data['session_array']['start_time'] != NULL)
            $array_session['start_time'] = $data['session_array']['start_time'];

        if($data['session_array']['end_time'] != NULL)
            $array_session['end_time'] = $data['session_array']['end_time'];

        if($data['session_array']['session_date'] != NULL)
            $array_session['session_date'] = $data['session_array']['session_date'];

        if($data['session_array']['is_closed_qa'] != NULL)
            $array_session['is_closed_qa'] = $data['session_array']['is_closed_qa'];

        $this->db->where('Id', $data['session_array']['Id']);
        $this->db->update('qa_session', $array_session);
    }   
    public function delete_session($id,$event_id='')
    {
        $this->db->where('Id', $id);
        $this->db->delete('qa_session');

        if($event_id!='')
        {
           $a_cat = $this->db->select('*')->from('qa_session')->where('Event_id',$event_id)->order_by('updated_date','DESC')->get()->row_array();
           $update['updated_date'] = date('Y-m-d H:i:s');
           $this->db->where('Id',$a_cat['Id'])->update('qa_session',$update);
        }
    }
	public function get_all_speaker_or_moderator_by_event_id($id)
    {
        $this->db->select('u.Id,u.Company_name,u.Salutation,u.Firstname,u.Lastname,u.Title,u.Email,u.Logo,u.is_moderator')->from('user u');
        $this->db->join('relation_event_user reu','u.Id=reu.User_id','left');
        $this->db->where('reu.Event_id',$id);
        $this->db->where('reu.Role_id',7);
        $res = $this->db->get()->result_array();
        return $res;
	}
    public function get_qa_session_by_event_frontend($eid,$sid)
    {
        $this->db->select('qs.*,u.Firstname,u.Lastname')->from('qa_session qs');
        $this->db->join('user u','u.Id=qs.Moderator_speaker_id','left');
        $this->db->where('qs.Event_id',$eid);
        if(!empty($sid))
        {
            $this->db->where('qs.Id',$sid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_qa_voites_by_modetor($voites,$msgid)
    {
        $this->db->select('*')->from('qa_message_votes');
        $this->db->where('message_id',$msgid);
        $res=$this->db->get()->result_array();
        if(count($res) > 0)
        {
            $this->db->where('message_id',$msgid);
            $this->db->update('qa_message_votes',array('votes'=>$voites));
        }
        else
        {
            $this->db->insert('qa_message_votes',array('votes'=>$voites,'message_id'=>$msgid));   
        }
    }
    public function get_all_qa_session_question_by_qa_session_id($eid,$qsid)
    {   
        $this->db->protect_idenifiers = false;
         $this->db->select('sm.Id as message_id,
                           sm.Message,
                           sm.Sender_id,
                           sm.Time,
                           qmv.users_id,
                           qmv.device_id,
                           qmv.votes,
                           u.Logo,
                           CASE WHEN sm.Sender_id IS NULL THEN "Anonymous" ELSE concat(u.Firstname," ",u.Lastname) END as user_name,
                           u.Title,
                           u.Company_name,
                           u.Email,
                           u.Firstname,
                           u.Lastname',false)->from('speaker_msg sm');
        $this->db->join('qa_message_votes qmv','sm.Id=qmv.message_id','left');
        $this->db->join('user u','sm.Sender_id=u.Id','left');
        $this->db->where('sm.Event_id',$eid);
        $this->db->where('sm.qasession_id',$qsid);
        $this->db->where('sm.qa_approved','1');
        $this->db->where('sm.hide_qa','0');
        $this->db->order_by('u.Id','desc');
        $this->db->order_by('sm.Id','desc');
        $res=$this->db->get()->result_array();
        return $res;
        // $this->db->select('sm.Id as message_id,sm.Message,sm.Sender_id,sm.Time,qmv.users_id,qmv.device_id,qmv.votes,u.Logo,CASE WHEN sm.Sender_id IS NULL THEN "Anonymous" ELSE u.Firstname AND as user_name')->from('speaker_msg sm');
    }
    public function get_all_qa_session_question_for_show_on_screen($eid,$qsid)
    {   
        $this->db->protect_idenifiers = false;
        $this->db->select('sm.Id as message_id,sm.Message,sm.Sender_id,sm.Time,qmv.users_id,qmv.device_id,qmv.votes,u.Logo,CASE WHEN sm.Sender_id IS NULL THEN "Anonymous" ELSE concat(u.Firstname," ",u.Lastname) END as user_name,CASE WHEN sm.Sender_id IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN sm.Sender_id IS NULL THEN "" ELSE u.Company_name END as Company_name',false)->from('speaker_msg sm');
        $this->db->join('qa_message_votes qmv','sm.Id=qmv.message_id','left');
        $this->db->join('user u','sm.Sender_id=u.Id','left');
        $this->db->where('sm.Event_id',$eid);
        $this->db->where('sm.qasession_id',$qsid);
        $this->db->where('sm.qa_approved','1');
        $this->db->where('sm.show_on_web','1');
        $this->db->limit('1');
        $this->db->order_by('sm.Id','desc');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function save_setting($data,$event_id)
    {
        $res = $this->db->where('event_id',$event_id)->get('event_settings')->row_array();
        if(empty($res))
        {
            $data['event_id'] = $event_id;
            $this->db->insert('event_settings',$data);
        }   
        else
        {   
            $this->db->where($res);
            $this->db->update('event_settings',$data);
        }
    }
    public function getQaSettingShowOnScreen($event_id)
    {
        $res = $this->db->where('event_id',$event_id)->get('event_settings')->row_array();
        return $res['show_on_screen_qa']?:'0';
    }
}        
?>