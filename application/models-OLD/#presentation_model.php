<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Presentation_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function add_presentation($data)
    {       

        if(!empty($data['presentation_array']['Images']))
        {
            foreach($data['presentation_array']['Images'] as $k=>$v)
            {
                $Images[] = $v;
            }
            
        }
        $array_presentation['Images'] = json_encode($Images);

        if(!empty($data['presentation_array']['Image_lock']))
        {
            foreach($data['presentation_array']['Image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }
            
        }
        $array_presentation['Image_lock'] = json_encode($Image_lock);

        if($data['presentation_array']['Organisor_id'] != NULL)
        $array_presentation['Organisor_id'] = $data['presentation_array']['Organisor_id'];

        if($data['presentation_array']['Event_id'] != NULL)
        $array_presentation['Event_id'] = $data['presentation_array']['Event_id'];

        if($data['presentation_array']['Start_date'] != NULL)
        $array_presentation['Start_date'] = $data['presentation_array']['Start_date'];

        if($data['presentation_array']['Start_time'] != NULL)
        $array_presentation['Start_time'] = $data['presentation_array']['Start_time'];

        if($data['presentation_array']['End_date'] != NULL)
        $array_presentation['End_date'] = $data['presentation_array']['End_date'];

        if($data['presentation_array']['End_time'] != NULL)
        $array_presentation['End_time'] = $data['presentation_array']['End_time'];

        if($data['presentation_array']['Status'] != NULL)
        $array_presentation['Status'] = $data['presentation_array']['Status'];

        if($data['presentation_array']['Heading'] != NULL)
        $array_presentation['Heading'] = $data['presentation_array']['Heading'];

        if($data['presentation_array']['Thumbnail_status'] != NULL)
        $array_presentation['Thumbnail_status'] = $data['presentation_array']['Thumbnail_status'];

        if($data['presentation_array']['Auto_slide_status'] != NULL)
        $array_presentation['Auto_slide_status'] = $data['presentation_array']['Auto_slide_status'];

        if($data['presentation_array']['Address_map'] != NULL)
        $array_presentation['Address_map'] = $data['presentation_array']['Address_map'];

        if($data['presentation_array']['Types'] != NULL)
        $array_presentation['Types'] = $data['presentation_array']['Types'];

        if($data['presentation_array']['presentation_file_type'] != NULL)
            $array_presentation['presentation_file_type']=$data['presentation_array']['presentation_file_type'];

        $this->db->insert('presentation',$array_presentation);
        $presentation_id = $this->db->insert_id();
        return $presentation_id;
   
    }

    public function get_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('*');
        $this->db->from('presentation');
        if($logged_in_user_id)
        {
            //$this->db->where('Organisor_id',$logged_in_user_id);
        }
        $this->db->where('Event_id',$id);
        $this->db->where('Id',$this->uri->segment(4));
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }


    public function get_all_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('p.*,e.Event_name');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        if($logged_in_user_id)
        {
            //$this->db->where('p.Organisor_id',$logged_in_user_id);
        }
        $this->db->where('p.Event_id',$id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }

    public function get_speaker_presentation_list($id=null)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $this->db->select('p.*,e.Event_name');
        $this->db->from('presentation p');
        $this->db->join('event e', 'e.Id = p.Event_id');
        if($logged_in_user_id)
        {
            $this->db->where('p.Organisor_id',$logged_in_user_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }

    public function update_presentation($data)
    {           

        $Speakers = $this->speaker_model->get_speaker_list();
        $this->data['Speakers'] = $Speakers;

        $images=array();
        if(!empty($data['presentation_array']['old_images']))
        {
            foreach($data['presentation_array']['old_images'] as $k=>$v)
            {
                $images[] = $v;
            }  
            $array_presentation['Images'] = json_encode($images);              
        }
        if(!empty($data['presentation_array']['Images']))
        {
            foreach($data['presentation_array']['Images'] as $k=>$v)
            {
                $images[] = $v;
            }
            $array_presentation['Images'] = json_encode($images);
        }

        $Image_lock=array();
        if(!empty($data['presentation_array']['old_image_lock']))
        {
            foreach($data['presentation_array']['old_image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }  
            $array_presentation['Image_lock'] = json_encode($Image_lock);              
        }
        if(!empty($data['presentation_array']['Image_lock']))
        {
            foreach($data['presentation_array']['Image_lock'] as $a=>$b)
            {
                $Image_lock[] = $b;
            }
            $array_presentation['Image_lock'] = json_encode($Image_lock); 
        }
    

        if($data['presentation_array']['Start_date'] != NULL)
        $array_presentation['Start_date'] = $data['presentation_array']['Start_date'];

        if($data['presentation_array']['Start_time'] != NULL)
        $array_presentation['Start_time'] = $data['presentation_array']['Start_time'];

        if($data['presentation_array']['End_date'] != NULL)
        $array_presentation['End_date'] = $data['presentation_array']['End_date'];

        if($data['presentation_array']['End_time'] != NULL)
        $array_presentation['End_time'] = $data['presentation_array']['End_time'];

        if($data['presentation_array']['Status'] != NULL)
        $array_presentation['Status'] = $data['presentation_array']['Status'];

        if($data['presentation_array']['Heading'] != NULL)
        $array_presentation['Heading'] = $data['presentation_array']['Heading'];

        if($data['presentation_array']['Auto_slide_status'] != NULL)
        $array_presentation['Auto_slide_status'] = $data['presentation_array']['Auto_slide_status'];

        if($data['presentation_array']['Thumbnail_status'] != NULL)
        $array_presentation['Thumbnail_status'] = $data['presentation_array']['Thumbnail_status'];

        if($data['presentation_array']['Address_map'] != NULL)
        {
            $array_presentation['Address_map'] = $data['presentation_array']['Address_map'];
        }
        else
        {
            $array_presentation['Address_map'] = NULL;
        }

        if($data['presentation_array']['Types'] != NULL)
        $array_presentation['Types'] = $data['presentation_array']['Types'];
        
        if(empty($data['presentation_array']['Image_current']))
        {
            $array_presentation['Image_current'] = NULL;
        }
        else {
            $array_presentation['Image_current'] = $data['presentation_array']['Image_current'];
        }

        if($data['presentation_array']['presentation_file_type'] != NULL)
            $array_presentation['presentation_file_type']=$data['presentation_array']['presentation_file_type'];
        

        $this->db->where('Id', $this->uri->segment(4));
        $this->db->update('presentation', $array_presentation);
        //echo $this->db->last_query(); exit();
    }

    public function get_permission_list()
    {
        $orid = $this->data['user']->Id;
        if($this->data['user']->Role_name == 'User')
        {
            $this->db->select('*');
            $this->db->from('user');
            if($orid)
            {
                $this->db->where('user.Id',$orid);
            }
            $query = $this->db->get();
            $res = $query->result();
            return $res;
        }
    }

    public function delete_presentation($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('presentation');
        $str = $this->db->last_query();
    }

    public function get_menu_name($id)
    {

        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('menu_Setting');
        $this->db->where('Event_id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
        
?>