<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('j'))
{
	function j($data = '')
	{	
		echo "<pre>";print_r($data);exit();
	}
}
 
if (! function_exists('jp'))
{
	function jp()
	{	
		echo "<pre> Parmas:";print_r($_POST);exit;
	}
}

if (! function_exists('lq'))
{
	function lq()
	{	
		$ci=& get_instance();
		$ci->load->database(); 
		echo "<pre> ".$ci->db->last_query();exit;
	}
}

if (! function_exists('dd'))
{
	function dd($query=0)
	{		
		if(!empty($_POST['test']) || !empty($_GET['test']))
		{
			if($query == '1')
			{
				$ci=& get_instance();
				$ci->load->database(); 
				echo "<pre> ".$ci->db->last_query();exit;
			}
			else
			{
					echo "<pre>";print_r($query);exit();
			}
		}
	}
}

if(! function_exists('log_que'))
{
	function log_que($sql,$time,$table_name) {
		if((preg_match('/INSERT/',$sql) || preg_match('/UPDATE/',$sql) || preg_match('/DELETE/',$sql)) AND (!preg_match('/rss_feed/',$sql)) AND (!preg_match('/ci_sessions/',$sql))) 
		{	
			$table_name = str_replace('`','',$table_name);
	        $filepath = APPPATH . 'logs/Query-log-' . date('Y-m-d') . '.txt';   
            $handle = fopen($filepath, "a+");
            $CI =& get_instance();
            $CI->load->library('session');
            $user = $CI->session->userdata('current_user');
            $user_id = $user[0]->Id;
            if(isset($_POST['user_id']) && $user_id == '')
            {
            	$user_id = $_POST['user_id'];
            }
            $event_id = $CI->uri->segment(3);
            if((isset($_POST['event_id'])||isset($_POST['Event_id'])) && !filter_var($event_id, FILTER_VALIDATE_INT))
            {
            	$event_id = $_POST['event_id'];
            }
            $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            if($CI->input->ip_address() != '0.0.0.0')
            fwrite($handle, $sql."\n\nURL: ".$url."\nExecution Time: ".$time."\nUser Id: ".$user_id."\nEvent Id:".$event_id."\nIp: ".$CI->input->ip_address()."\nDate: ".gmdate("Y-m-d H:i:s ")." \n------------------------------------------------------------------------------------------\n\n");   
            fclose($handle);
            if($CI->uri->segment(1) != 'apiv2' && $CI->uri->segment(1) != 'apiv3' && $CI->uri->segment(1) != 'cronjob' && $CI->uri->segment(1) != 'apiv4' && $CI->uri->segment(2) != 'add_duplicate_event')
            {
                $no_update = '1';
						
				if($table_name == 'relation_event_user')
				{	
					if(preg_match('/INSERT/',$sql))
					{	
						if(preg_match("/'6', '".$event_id."'/",$sql))
						{
							$table_name = 'exibitor';
						}
						elseif(preg_match("/'7'\)/",$sql))
						{
							$table_name = 'speaker';
						}
					}
					elseif(preg_match('/DELETE/',$sql))
					{
						if(preg_match('/`Role_id` =  \'6\'/',$sql))
						{
							$table_name = 'exibitor';
						}
						if(preg_match('/`Role_id` =  \'7\'/',$sql))
						{
							$table_name = 'speaker';
						}
					}
				}
				if($table_name == 'user' && preg_match('/UPDATE/',$sql) && $CI->uri->segment(1) == 'speaker')
				{	
					$table_name = 'speaker';
				}
				$where['event_id'] = ($event_id)?:$user[0]->Event_id;
				switch ($table_name)
				{
					case 'exibitor':
					case 'exhibitor_category':
					case 'event_countries':
					case 'exhi_category_group':
						$where['module_name'] = 'exhibitor';
						break;
					case 'sponsors':
					case 'sponsors_type':
						$where['module_name'] = 'sponsor';
						break;
					case 'agenda':
					case 'agenda_categories':
					case 'session_types':
						$where['module_name'] = 'agenda';
						break;
					case 'modules_group':
					case 'modules_super_group':
						$where['module_name'] = 'group';
						break;
					case 'cms':
						$where['module_name'] = 'cms';
						break;
					case 'map':
						$where['module_name'] = 'map';
						break;
					case 'qa_session':
						$where['module_name'] = 'qa';
						break;
					case 'speaker':
						$where['module_name'] = 'speaker';
						break;
					default:
						$no_update = '0';
					break;
				}
				$CI->dbj = $CI->load->database('dbj', TRUE);
				if($no_update == '1')
				{	
					if($CI->dbj->where($where)->get('modules_update_log')->row())
					{	
						$data['updated_date'] = date('Y-m-d H:i:s');
						$CI->dbj->where($where)->update('modules_update_log',$data);
					}
					else
					{	
						$data = $where;
						$data['updated_date'] = date('Y-m-d H:i:s');
						$CI->dbj->insert('modules_update_log',$data);
					}
				}   
            }
    	}

    	$files = glob(APPPATH . 'logs/'."*");
		$now   = time();

		foreach ($files as $file) 
		{
		    if (is_file($file)) 
		    {
		      if (($now - filemtime($file) >=  3600*24*30) && $file!='index.html') 
		      {		
		      	unlink($file);
		      }
		    }
		}
	}
}
// Friday 11 May 2018 12:46:33 PM IST 
if(! function_exists('get_domain'))
{
	function get_domain($url)
    {
      $pieces = parse_url($url);
      $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }
      return false;
    }
}

if(!function_exists('show_errors'))
{
	function  show_errors()
	{
		error_reporting(E_ALL^E_NOTICE);
	    ini_set('display_errors','1');
	}
}

if(!function_exists('console_log'))
{
	function console_log($data){
	  echo '<script>';
	  echo 'console.log('. json_encode( $data ) .')';
	  echo '</script>';
	}
}
if(!function_exists('updateModuleDate'))
{	
    function updateModuleDate($event_id,$module)
    {  	
    	if(!empty($event_id) && !empty($module))
    	{
	    	$CI =& get_instance();
	        $where['event_id'] = $event_id;
	        $where['module_name'] = $module;
	       	$CI->dbj = $CI->load->database('dbj', TRUE);
			if($CI->dbj->where($where)->get('modules_update_log')->row())
			{	
				$data['updated_date'] = date('Y-m-d H:i:s');
				$CI->dbj->where($where)->update('modules_update_log',$data);
			}
			else
			{	
				$data = $where;
				$data['updated_date'] = date('Y-m-d H:i:s');
				$CI->dbj->insert('modules_update_log',$data);
			}
    	}
	}
}
 
if (! function_exists('filterArray'))
{
	function filterArray($array, $key, $value)
	{	
		foreach($array as $subKey => $subArray)
		{
			if(is_array($value)){
				if(!in_array($subArray[$key],$value))
				{
					unset($array[$subKey]);
				}
			}	
			else{
	          	if($subArray[$key] != $value){
	               unset($array[$subKey]);
	          	}
			}	
     	}
    	return $array;
	}
}

if (! function_exists('validate_date'))
{
	function validate_date($date)
	{
		if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
	  		return true;
		else
	  		return false;
	}
}

if (! function_exists('validate_time'))
{
	function validate_time($time)
	{	
		if(preg_match('/^(?:[01][0-9]|2[0-3]):[0-5][0-9]$/',$time))
		    return true;
		else
		  	return false;
	}
}

if (! function_exists('clean'))
{
	function clean($string)
	{
		$string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}
}

if (! function_exists('ld'))
{
	function ld($data = '')
	{	
		if($_SERVER['REMOTE_ADDR'] == '180.211.96.226')
		{
			echo "<pre>";print_r($data);exit();
		}
	}
}

if (! function_exists('j_curl'))
{
	function j_curl($url,$header)
	{	
		$curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;exit;
        } else {
         return $response;
        }
	}
}

if (! function_exists('array_column_1')) {
	function array_column_1($array,$column_name)
	{
	    $data = array_map(function($element) use($column_name){return $element[$column_name];}, $array);
	    return $data;
	}
}

if(!function_exists('sec2read'))
{
	function sec2read($str_time)
	{
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		$seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		$hours = floor($seconds / 3600);
		$minutes = floor($seconds % 3600 / 60);
		$seconds = $seconds % 60;
		if($hours>0)
		{
		   return sprintf(" %d Hours %02d Minutes %02d Seconds", $hours, $minutes, $seconds);
		}
		else if($minutes>0)
		{
		  return sprintf("%02d Minutes %02d Seconds", $minutes, $seconds);
		}
		else
		{
		  return sprintf("%02d Seconds",  $seconds);
		}
	}
}

if(!function_exists('sec2int'))
{
	function sec2int($str_time)
	{	
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		$seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		return $seconds;
	}
}

if(!function_exists('array_iunique'))
{
	function array_iunique( $array ) {
	    return array_intersect_key(
	        $array,
	        array_unique(array_map( "strtolower", $array))
	    );
	}
}

if(!function_exists('get_api_reponse'))
{
	function get_api_reponse($url,$post)
	{	
		$url = base_url().$url;
		$data = json_decode(file_get_contents($url.'?'.http_build_query($post)),true);
		return $data;
	}
}