<?php
class category_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_category_list($id = null)

    {
        $this->db->select('*');
        $this->db->from('category c');
        if ($id)
        {
            $this->db->where('c.Id', $id);
        }
        $query = $this->db->get();
        $res = $query->result();
        $cat_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $cat_res[$i]['Id'] = $res[$i]->Id;
            $cat_res[$i]['Name'] = $res[$i]->Name;
            $cat_res[$i]['Description'] = $res[$i]->Description;
            $cat_res[$i]['Created_date'] = $res[$i]->Created_date;
            $cat_res[$i]['Event'] = array();
            $this->db->select('*');
            $this->db->from('event p');
            $this->db->join('event_category pc', 'pc.Event_id = p.Id');
            $this->db->where('pc.Category_id', $res[$i]->Id);
            $event_query = $this->db->get();
            $event_res = $event_query->result();
            for ($j = 0; $j < count($event_res); $j++)
            {
                $cat_res[$i]['Event'][$j]['Id'] = $event_res[$j]->Event_id;
                $cat_res[$i]['Event'][$j]['Event_name'] = $event_res[$j]->Event_name;
                $cat_res[$i]['Event'][$j]['Common_name'] = $event_res[$j]->Common_name;
                $cat_res[$i]['Event'][$j]['Description'] = $event_res[$j]->Description;
                $cat_res[$i]['Event'][$j]['Images'] = $event_res[$j]->Images;
                $cat_res[$i]['Event'][$j]['Created_date'] = $event_res[$j]->Created_date;
            }
        }
        return $cat_res;
    }
    public function add_category($data=NULL)

    {
        $category = array(
            'Name' => $data['Category_name'],
            'Description' => $data['Description'],
        );
        $this->db->insert('category', $category);
        $insert_id = $this->db->insert_id();
        if (!empty($data['event']))
        {
            for ($i = 0; $i < count($data['event']); $i++)
            {
                $event = array(
                    'Event_id' => $data['event'][$i],
                    'Category_id' => $insert_id
                );
                $this->db->insert('event_category', $event);
            }
        }
    }
    public function update_category($data=NULL, $id=NULL)

    {
        $category = array(
            'Name' => $data['Category_name'],
            'Description' => $data['Description'],
        );
        $this->db->where('Id', $id);
        $this->db->update('category', $category);
        $this->db->where('Category_id', $id);
        $this->db->delete('event_category');
        for ($i = 0; $i < count($data['event']); $i++)
        {
            $event = array(
                'Event_id' => $data['event'][$i],
                'Category_id' => $id
            );
            $this->db->insert('event_category', $event);
        }
    }
    public function delete_category($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('category');
    }
    public function checkcategory($category=NULL, $id = null)

    {
        $this->db->select('*');
        $this->db->from('category c');
        if ($id)
        {
            $this->db->where('c.Id !=', $id);
        }
        $this->db->where('c.Name', $category);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
?>