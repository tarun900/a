<?php
class Photo_model extends CI_Model

{
  function __construct()
  {
    parent::__construct();
  }
  public function getAllPublicFeedsByEvent($event_id=NULL, $page_no=NULL, $limit=NULL, $user_id=NULL)

  {
    $user_id = ($user_id) ? $user_id : '0';
    $page_no = (!empty($page_no)) ? $page_no : 1;
    $start = ($page_no - 1) * $limit;
    $this->db->select('CASE WHEN fi.Id IS NULL THEN "" ELSE fi.Id END as feed_id,CASE WHEN fi.Sender_id IS NULL THEN "" ELSE fi.Sender_id END as Sender_id,CASE WHEN fi.image IS NULL THEN "" ELSE fi.image END as image,CASE WHEN fi.Time IS NULL THEN "" ELSE fi.Time END as Time,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,count(fl.post_id) as total_likes,sum(CASE WHEN fl.user_id = ' . "$user_id" . ' THEN 1 ELSE 0 END) as is_like', FALSE);
    $this->db->from('feed_img fi');
    $this->db->join('user u', 'u.Id=fi.Sender_id');
    $this->db->join('feed_like fl', 'fl.post_id=fi.Id', 'left');
    $where = "fi.Event_id = '" . $event_id . "' AND  fi.Parent = '0' ";
    $this->db->where($where);
    $this->db->order_by('Time', 'DESC');
    $this->db->group_by('fi.Id');
    $this->db->limit($limit, $start);
    $query1 = $this->db->get();
    $res1 = $query1->result_array();
    foreach($res1 as $key => $value)
    {
      $res1[$key]['time_stamp'] = $this->time_cal($value['Time']);
      if (!empty($value['image']))
      {
        $res1[$key]['image'] = json_decode($value['image']);
      }
      else
      {
        $res1[$key]['image'] = array(
          [0] => ""
        );
      }
      $this->db->select('CONCAT((CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END )," ",(CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END)) as user_name,fc.id as comment_id,fc.comment,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,fc.image,fc.Time,fc.user_id', FALSE);
      $this->db->from('feed_comment fc');
      $this->db->join('user u', 'u.Id=fc.user_id');
      $this->db->where('fc.msg_id', $value['feed_id']);
      $query_comment = $this->db->get();
      $res_comment = $query_comment->result_array();
      $comment_count = count($res_comment);
      if (!empty($res_comment))
      {
        foreach($res_comment as $key1 => $value1)
        {
          $res_comment[$key1]['time_stamp'] = $this->time_cal($value1['Time']);
          if (!empty($value1['image']))
          {
            $image_arr = json_decode($value1['image']);
            $res_comment[$key1]['image'] = $image_arr[0];
          }
          else
          {
            $res_comment[$key1]['image'] = "";
          }
        }
        $res1[$key]['comment'] = $res_comment;
      }
      $res1[$key]['comment'] = $res_comment;
      $res1[$key]['total_comments'] = $comment_count;
    }
    return $res1;
  }
  public function saveFeed($feedData=NULL)

  {
    $this->db->insert("feed_img", $feedData);
    $feed_id = $this->db->insert_id();
    return $feed_id;
  }
  public function delete_feed($id=NULL)

  {
    $this->db->where('Id', $id);
    $this->db->delete('feed_img');
    $this->db->where('Parent', $id);
    $this->db->delete('feed_img');
    $this->db->where('msg_id', $id);
    $this->db->delete('feed_comment');
    $this->db->where('post_id', $id);
    $this->db->delete('feed_like');
  }
  public function getFeedDetails($feed_id=NULL)

  {
    $this->db->select('image');
    $this->db->from('feed_img');
    $this->db->where("Id", $feed_id);
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
  }
  public function updateFeedImage($feed_id=NULL, $data=NULL)

  {
    $this->db->where("Id", $feed_id);
    $this->db->update("feed_img", $data);
  }
  public function getPageCountFeedsByEvent($Event_id=NULL, $limit=NULL)

  {
    $this->db->select('fi.Id');
    $this->db->from('feed_img fi');
    $where = "fi.Event_id = '" . $Event_id . "' 
                    AND  fi.Parent = '0' ";
    $this->db->where($where);
    $rows = $this->db->get()->num_rows();
    $total = ceil($rows / $limit);
    return $total;
  }
  public function make_comment($comment=NULL)

  {
    $this->db->insert("feed_comment", $comment);
    $comment_id = $this->db->insert_id();
    return true;
  }
  public function delete_comment($comment_id=NULL)

  {
    $this->db->where('id', $comment_id);
    $this->db->delete('feed_comment');
  }
  public function like_post($data=NULL)

  {
    $this->db->insert("feed_like", $data);
    $feed_id = $this->db->insert_id();
    return $feed_id;
  }
  public function dislike_post($user_id=NULL, $post_id=NULL)

  {
    $this->db->where('user_id', $user_id);
    $this->db->where('post_id', $post_id);
    $this->db->delete('feed_like');
    return true;
  }
  public function time_cal($ago_time=NULL)

  {
    $cur_time = time();
    $time_ago = strtotime($ago_time);
    $time_elapsed = $cur_time - $time_ago;
    $seconds = $time_elapsed;
    $minutes = round($time_elapsed / 60);
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400);
    $weeks = round($time_elapsed / 604800);
    $months = round($time_elapsed / 2600640);
    $years = round($time_elapsed / 31207680);
    $time_status;
    if ($seconds <= 60)
    {
      $time_status = $seconds . " seconds ago";
    }
    // Minutes
    else if ($minutes <= 60)
    {
      if ($minutes == 1)
      {
        $time_status = "one minute ago";
      }
      else
      {
        $time_status = $minutes . " minutes ago";
      }
    }
    // Hours
    else if ($hours <= 24)
    {
      if ($hours == 1)
      {
        $time_status = "an hour ago";
      }
      else
      {
        $time_status = $hours . " hours ago";
      }
    }
    // Days
    else if ($days <= 7)
    {
      if ($days == 1)
      {
        $time_status = "yesterday";
      }
      else
      {
        $time_status = $days . " days ago";
      }
    }
    // Weeks
    else if ($weeks <= 4.3)
    {
      if ($weeks == 1)
      {
        $time_status = "a week ago";
      }
      else
      {
        $time_status = $weeks . " weeks ago";
      }
    }
    // Months
    else if ($months <= 12)
    {
      if ($months == 1)
      {
        $time_status = "a month ago";
      }
      else
      {
        $time_status = $months . " months ago";
      }
    }
    // Years
    else
    {
      if ($years == 1)
      {
        $time_status = "one year ago";
      }
      else
      {
        $time_status = $years . " years ago";
      }
    }
    return $time_status;
  }
  public function getImages($event_id=NULL, $message_id=NULL)

  {
    $this->db->select("CASE WHEN image is NULL THEN '' ELSE image END as image ", false);
    $this->db->from('feed_img');
    $this->db->where('Id', $message_id);
    $this->db->where('Event_id', $event_id);
    $rows = $this->db->get()->row();
    return $rows->image;
  }
}
?>