<?php
class note_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getNotesListByEventId($event_id=NULL, $user_id=NULL)

    {
        $this->db->select('CASE WHEN n.Id IS NULL THEN "" ELSE n.Id END as Id,CASE WHEN n.Heading IS NULL THEN "" ELSE n.Heading END as Heading,CASE WHEN n.Description IS NULL THEN "" ELSE n.Description END as Description,CASE WHEN n.Created_at IS NULL THEN "" ELSE n.Created_at END as Created_at,CASE WHEN n.Event_id IS NULL THEN "" ELSE n.Event_id END as Event_id,CASE WHEN n.User_id IS NULL THEN "" ELSE n.User_id END as User_id,CASE WHEN n.Organisor_id IS NULL THEN "" ELSE n.Organisor_id END as Organisor_id', false);
        $this->db->from('notes n');
        $this->db->where('n.Event_id', $event_id);
        $this->db->where('n.User_id', $user_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function getOrganisorIdByUserId($user_id=NULL, $event_id=NULL)

    {
        $this->db->select('Organisor_id');
        $this->db->from('relation_event_user');
        $this->db->where('Event_id', $event_id);
        $this->db->where('User_id', $user_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        $Organisor_id = $res2[0]['Organisor_id'];
        return $Organisor_id;
    }
    public function add_note($add_arr=NULL)

    {
        $this->db->insert("notes", $add_arr);
        return $this->db->insert_id();
    }
    public function update_note($note_id=NULL, $update_data=NULL)

    {
        $this->db->where("Id", $note_id);
        $this->db->update("notes", $update_data);
    }
    public function checkEventDateFormat($event_id=NULL)

    {
        $this->db->select('date_format');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getNotesListByEventIdNew($event_id=NULL, $user_id=NULL)

    {
        $this->db->select('CASE WHEN n.Id IS NULL THEN "" ELSE n.Id END as Id,
                           CASE WHEN n.Heading IS NULL THEN "" ELSE n.Heading END as Heading,
                           CASE WHEN n.Description IS NULL THEN "" ELSE n.Description END as Description,
                           CASE WHEN n.Created_at IS NULL THEN "" ELSE n.Created_at END as Created_at,
                           CASE WHEN n.Updated_at IS NULL THEN "" ELSE n.Updated_at END as Updated_at,
                           CASE WHEN n.Event_id IS NULL THEN "" ELSE n.Event_id END as Event_id,
                           CASE WHEN n.User_id IS NULL THEN "" ELSE n.User_id END as User_id,
                           CASE WHEN n.Organisor_id IS NULL THEN "" ELSE n.Organisor_id END as Organisor_id,
                           CASE WHEN n.Menu_id IS NULL THEN "" ELSE n.Menu_id END AS Menu_id,
                           CASE WHEN n.Module_id IS NULL THEN 0 ELSE n.Module_id END AS Module_id,
                           CASE WHEN n.is_cms IS NULL THEN 0 ELSE n.is_cms END AS is_cms,
                           CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname', false);
        $this->db->from('notes n');
        $this->db->join('menu m', 'm.id = n.Menu_id', 'left');
        $this->db->join('event_menu em', 'em.menu_id=n.Menu_id and em.event_id=' . $event_id . '', 'left');
        $this->db->where('n.Event_id', $event_id);
        $this->db->where('n.User_id', $user_id);
        $this->db->order_by('Updated_at', 'DESC');
        $this->db->order_by('Created_at', 'DESC');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        $date_format = $this->checkEventDateFormat($event_id);
        $time_format = $this->getTimeFormat($event_id);
        $d_format = ($date_format['date_format'] == '0') ? "d/m/Y" : "m/d/Y";
        $t_format = ($time_format == '0') ? "h:i A" : "H:i";
        foreach($res2 as $key => $value)
        {
            if (!empty($value['Menu_id']) && !empty($value['Module_id']))
            {
                switch ($value['Menu_id'])
                {
                case '1': //AGENDA
                    $res = $this->db->select('Heading')->where('Id', $value['Module_id'])->get('agenda')->row_array();
                    $res2[$key]['title'] = $res['Heading'];
                    break;

                case '2': //ATTENDEE
                    $res = $this->db->select('CONCAT(Firstname," ",(CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END)) as user_name', false)->where('Id', $value['Module_id'])->get('user')->row_array();
                    $res2[$key]['title'] = $res['user_name'];
                    break;

                case '3': //EXHIBITOR REMAIN
                    $res = $this->db->select('Heading')->where('Id', $value['Module_id'])->get('exibitor')->row_array();
                    $res2[$key]['title'] = $res['Heading'];
                    break;

                case '6': //NOTES
                    $res = $this->db->select('Heading')->where('Id', $value['Module_id'])->get('notes')->row_array();
                    $res2[$key]['title'] = $res['Heading'];
                    break;

                case '7': //SPEAKER
                    $res = $this->db->select('CONCAT(Firstname," ",(CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END)) as user_name', false)->where('Id', $value['Module_id'])->get('user')->row_array();
                    $res2[$key]['title'] = $res['user_name'];
                    break;

                case '9': //Presentations
                    $res = $this->db->select('Heading')->where('Id', $value['Module_id'])->get('presentation')->row_array();
                    $res2[$key]['title'] = $res['Heading'];
                    break;

                case '10': //Map
                    $res = $this->db->select('Map_title')->where('Id', $value['Module_id'])->get('map')->row_array();
                    $res2[$key]['title'] = $res['Map_title'];
                    break;

                case '12': //Private Message
                    $res = $this->db->select('CONCAT(Firstname," ",(CASE WHEN Lastname IS NULL THEN "" ELSE Lastname END)) as user_name', false)->where('Id', $value['Module_id'])->get('user')->row_array();
                    $res2[$key]['title'] = $res['user_name'];
                    break;

                case '15': //Surveys
                    $res = $this->db->select('survey_name')->where('survey_id', $value['Module_id'])->get('survey_category')->row_array();
                    $res2[$key]['title'] = $res['survey_name'];
                    break;

                case '16': //Documents
                    $res = $this->db->select('title')->where('id', $value['Module_id'])->get('documents')->row_array();
                    $res2[$key]['title'] = $res['title'];
                    break;

                case '20': //Fundraising
                    $res = $this->db->select('name')->where('product_id', $value['Module_id'])->get('product')->row_array();
                    $res2[$key]['title'] = $res['name'];
                    break;

                case '21': //Custom modules
                    $res = $this->db->select('Menu_name')->where('Id', $value['Module_id'])->get('cms')->row_array();
                    $res2[$key]['title'] = $res['Menu_name'];
                    break;

                case '22': //Silent Auction
                    $res = $this->db->select('name')->where('product_id', $value['Module_id'])->get('product')->row_array();
                    $res2[$key]['title'] = $res['name'];
                    break;

                case '23': //Live Auction
                    $res = $this->db->select('name')->where('product_id', $value['Module_id'])->get('product')->row_array();
                    $res2[$key]['title'] = $res['name'];
                    break;

                case '24': //Buy Now
                    $res = $this->db->select('name')->where('product_id', $value['Module_id'])->get('product')->row_array();
                    $res2[$key]['title'] = $res['name'];
                    break;

                case '25': //Buy Now
                    $res = $this->db->select('name')->where('product_id', $value['Module_id'])->get('product')->row_array();
                    $res2[$key]['title'] = $res['name'];
                    break;

                case '43': //Sponsors
                    $res = $this->db->select('Sponsors_name')->where('Id', $value['Module_id'])->get('sponsors')->row_array();
                    $res2[$key]['title'] = $res['Sponsors_name'];
                    break;

                case '44': //Twitter
                    $res = $this->db->select('hashtags')->where('Id', $value['Module_id'])->get('event_hashtags')->row_array();
                    $res2[$key]['title'] = $res['hashtags'];
                    break;

                case '48': //Virtual Supermarket
                    $res = $this->db->select('Heading')->where('Id', $value['Module_id'])->get('exibitor')->row_array();
                    $res2[$key]['title'] = $res['Heading'];
                    break;

                case '50': //Q&A
                    $res = $this->db->select('Session_name')->where('Id', $value['Module_id'])->get('qa_session')->row_array();
                    $res2[$key]['title'] = $res['Session_name'];
                    break;

                default:
                    $res2[$key]['title'] = "";
                    break;
                }
            }
            if ($value['is_cms'] == '1')
            {
                $this->db->select('CASE WHEN em.title IS NULL THEN c.Menu_name ELSE em.title END as Menu_name', false);
                $this->db->where('c.Id', $value['Module_id']);
                $this->db->from('cms c');
                $this->db->join('event_menu em', 'em.cms_id=c.Id and em.event_id=' . $event_id . '', 'left');
                $res = $this->db->get()->row_array();
                $res2[$key]['title'] = $res['Menu_name'];
            }
            if (!empty($res2[$key]['title']))
            {
                $res2[$key]['title'] = $value['menuname'] . ": " . $res2[$key]['title'];
            }
            else
            {
                if (!empty($value['Module_id']))
                {
                    $res2[$key]['title'] = $value['Module_id'];
                }
                else
                {
                    $res2[$key]['title'] = ($value['menuname']) ? $value['menuname'] : "";
                }
            }
            $res2[$key]['is_custom_title'] = (!empty($value['Menu_id'])) ? '0' : '1';
            $res2[$key]['menuname'] = ($value['menuname']) ? $value['menuname'] : "";
            if (!empty($value['Updated_at']))
            {
                $a = strtotime($value['Updated_at']);
                $time = "Updated: " . date($t_format, $a) . " " . date($d_format, $a);
            }
            else
            {
                $a = strtotime($value['Created_at']);
                $time = "Created: " . date($t_format, $a) . " " . date($d_format, $a);
            }
            $res2[$key]['Created_at'] = $time;
            $res2[$key]['time'] = $time;
        }
        return $res2;
    }
    public function get_event_timezone($event_id=NULL)

    {
        $res = $this->db->select('Event_show_time_zone')->where('Id', $event_id)->get('event')->row_array();
        return $res;
    }
    public function getTimeFormat($eid=NULL)

    {
        $this->db->select('format_time');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get('fundraising_setting');
        $res = $query->row_array();
        return $res['format_time'];
    }
    public function delete_note($note_id=NULL, $update_data=NULL)

    {
        $this->db->where("Id", $note_id);
        $this->db->delete("notes");
    }
}
?>