<?php
class qr_code_model extends CI_Model

{
   function __construct()
   {
      parent::__construct();
   }
   public function getQrCodeOfLoggedInAttendee($event_id=NULL, $attendee_id=NULL)

   {
      $this->db->select('Id as session_id,Session_name,Event_id');
      $this->db->from('qa_session');
      $this->db->where('Event_id', $event_id);
      $this->db->get();
      return $this->db->result_array();
   }
   public function getQrCodeOfLoggedInAttendeeNew($event_id=NULL, $attendee_id=NULL)

   {
      $this->db->select('Id as session_id,Session_name,Event_id');
      // $this->db->select('Id,Session_name,Event_id');
      $this->db->from('qa_session');
      $this->db->where('Event_id', $event_id);
      $query = $this->db->get();
      $res = $query->result();
      return $res;
   }
   public function getAgendaByUserId($user_id=NULL, $event_id=NULL)

   {
      $this->db->select('aci.agenda_id,');
      $this->db->from('attendee_agenda_relation ar');
      $this->db->where('ar.attendee_id', $user_id);
      $this->db->where('ar.event_id', $event_id);
      $this->db->join('agenda_category_relation aci', 'aci.category_id = ar.agenda_category_id');
      $query = $this->db->get();
      $res = $query->result_array();
      $agenda_array = array();
      foreach($res as $key => $value)
      {
         $agenda_array[] = $value['agenda_id'];
      }
      return $agenda_array;
   }
}
?>