<?php
class social_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getSocialListByEventId($event_id=NULL)

    {
        $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,CASE WHEN s.facebook_url IS NULL THEN "" ELSE s.facebook_url END as facebook_url,CASE WHEN s.twitter_url IS NULL THEN "" ELSE s.twitter_url END as twitter_url,CASE WHEN s.instagram_url IS NULL THEN "" ELSE s.instagram_url END as instagram_url,CASE WHEN s.linkedin_url IS NULL THEN "" ELSE s.linkedin_url END as linkedin_url,CASE WHEN s.pinterest_url IS NULL THEN "" ELSE s.pinterest_url END as pinterest_url,CASE WHEN s.youtube_url IS NULL THEN "" ELSE s.youtube_url END as youtube_url', false);
        $this->db->from('social s');
        $this->db->where('s.Event_id', $event_id);
        $this->db->limit(1);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;
    }
    public function getTwitterKeywords($event_id=NULL)

    {
        $this->db->select('hashtags');
        $this->db->from('event_hashtags');
        $this->db->where('event_id', $event_id);
        $query2 = $this->db->get();
        $res2 = $query2->row();
        return $res2->hashtags;
    }
    public function getInstaToken($event_id=NULL)

    {
        $this->db->select('insta_access_token');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result->insta_access_token;
    }
    public function getFacbookProfileId($event_id=NULL)

    {
        $this->db->select('facebook_page_name');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result->facebook_page_name;
    }
    public function getTwitterKeywordsNew($event_id=NULL)

    {
        $this->db->select('hashtags');
        $this->db->from('event_hashtags');
        $this->db->where('event_id', $event_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        foreach($res2 as $key => $value)
        {
            $data[$key] = $value['hashtags'];
        }
        return $data;
    }
}
?>