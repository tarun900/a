<?php
class presentation_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function getAllPresentationByTimeEvent($event_id= NULL, $id_arr = NULL, $lang_id = NULL)

    {
        $this->db->_protect_identifiers = false;
        $this->db->select('p.*,CASE WHEN p.presentation_file_type IS NULL THEN "" ELSE p.presentation_file_type END as presentation_file_type,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,e.Event_name,(CASE WHEN elmc1.title IS NULL THEN CASE WHEN m.map_Title IS NULL THEN "" ELSE m.map_Title END ELSE elmc1.title END) as map_Title,(CASE WHEN elmc.title IS NULL THEN p.Heading ELSE elmc.title END) as Heading', false);
        $this->db->from('presentation p');
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="9" and elmc.modules_id=p.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->join('event e', 'e.Id = p.Event_id');
        $this->db->join('map m', 'm.Id = p.Address_map', 'left');
        $this->db->join('event_lang_modules_content elmc1', 'elmc1.menu_id="10" and elmc1.modules_id=m.Id and elmc1.lang_id="' . $lang_id . '" and elmc1.event_id=' . $event_id, 'left');
        if (!empty($id_arr))
        {
            $this->db->where_in('p.Id', $id_arr);
        }
        $this->db->where('e.Id', $event_id);
        $this->db->where("p.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('p.Start_date asc');
        $this->db->order_by('p.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        $prev = "";
        $j = 0;
        $k = 0;
        for ($i = 0; $i < count($res); $i++)
        {
            if ($res[$i]->Start_date != $prev)
            {
                $k = 0;
                $myarr[$j]["date"] = $res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Id'] = $res[$i]->Id;
                $myarr[$j]["data"][$k]['Heading'] = $res[$i]->Heading;
                $myarr[$j]["data"][$k]['images'] = "";
                if ($res[$i]->Images != 'NULL')
                {
                    $images_js_decode = json_decode($res[$i]->Images);
                    $myarr[$j]["data"][$k]['images'] = $images_js_decode[0];
                }
                $myarr[$j]["data"][$k]['presentation_file_type'] = $res[$i]->presentation_file_type;
                $myarr[$j]["data"][$k]['Start_date'] = $res[$i]->Start_date;
                $myarr[$j]["data"][$k]['map_Title'] = $res[$i]->map_Title;
                $myarr[$j]["data"][$k]['Start_time'] = $res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date'] = $res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time'] = $res[$i]->End_time;
                $myarr[$j]["data"][$k]['Event_id'] = $res[$i]->Event_id;
                $j++;
            }
            else
            {
                $myarr[$j - 1]["data"][$k]['Id'] = $res[$i]->Id;
                $myarr[$j - 1]["data"][$k]['Heading'] = $res[$i]->Heading;
                $myarr[$j - 1]["data"][$k]['presentation_file_type'] = $res[$i]->presentation_file_type;
                $myarr[$j - 1]["data"][$k]['images'] = "";
                if ($res[$i]->Images != 'NULL')
                {
                    $images_js_decode = json_decode($res[$i]->Images);
                    $myarr[$j - 1]["data"][$k]['images'] = $images_js_decode[0];
                }
                $myarr[$j - 1]["data"][$k]['Start_date'] = $res[$i]->Start_date;
                $myarr[$j - 1]["data"][$k]['map_Title'] = $res[$i]->map_Title;
                $myarr[$j - 1]["data"][$k]['Start_time'] = $res[$i]->Start_time;
                $myarr[$j - 1]["data"][$k]['End_date'] = $res[$i]->End_date;
                $myarr[$j - 1]["data"][$k]['End_time'] = $res[$i]->End_time;
                $myarr[$j - 1]["data"][$k]['Event_id'] = $res[$i]->Event_id;
            }
            $k++;
            $prev = $res[$i]->Start_date;
        }
        return $myarr;
    }
    public function getTimeFormat($eid)

    {
        $this->db->select('format_time')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getAllPresentationByTypeEvent($event_id = null, $id = null, $id_arr = null, $lang_id = NULL)

    {
        $this->db->_protect_identifiers = false;
        $id = $this->uri->segment(4);
        $this->db->select('p.*,CASE WHEN p.presentation_file_type IS NULL THEN "" ELSE p.presentation_file_type END as presentation_file_type,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,(CASE WHEN elmc1.title IS NULL THEN CASE WHEN m.map_Title IS NULL THEN "" ELSE m.map_Title END ELSE elmc1.title END) as map_Title,(CASE WHEN elmc.title IS NULL THEN p.Heading ELSE elmc.title END) as Heading', false);
        $this->db->from('presentation p');
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="9" and elmc.modules_id=p.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->join('map m', 'm.Id = p.Address_map', 'left');
        $this->db->join('event_lang_modules_content elmc1', 'elmc1.menu_id="10" and elmc1.modules_id=m.Id and elmc1.lang_id="' . $lang_id . '" and elmc1.event_id=' . $event_id, 'left');
        if (!empty($id_arr))
        {
            $this->db->where_in('p.Id', $id_arr);
        }
        $this->db->where('p.Event_id', $event_id);
        $this->db->where("p.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('p.Start_date asc');
        $this->db->order_by('p.Start_time asc');
        $this->db->where('p.Types', $id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev = "";
        $j = 0;
        $k = 0;
        for ($i = 0; $i < count($res); $i++)
        {
            if ($res[$i]->Types != $prev)
            {
                $k = 0;
                if ($res1[$i]->Types == 'Other')
                {
                    $myarr[$j]["Types"] = $res[$i]->Types;
                }
                $myarr[$j]["Types"] = $res[$i]->Types;
                $myarr[$j]["data"][$k]['Id'] = $res[$i]->Id;
                $myarr[$j]["data"][$k]['presentation_file_type'] = $res[$i]->presentation_file_type;
                $myarr[$j]["data"][$k]['images'] = "";
                if ($res[$i]->Images != 'NULL')
                {
                    $images_js_decode = json_decode($res[$i]->Images);
                    $myarr[$j]["data"][$k]['images'] = $images_js_decode[0];
                }
                $myarr[$j]["data"][$k]['Types'] = $res[$i]->Types;
                $myarr[$j]["data"][$k]['map_Title'] = $res[$i]->map_Title;
                $myarr[$j]["data"][$k]['Heading'] = $res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date'] = $res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time'] = $res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date'] = $res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time'] = $res[$i]->End_time;
                $myarr[$j]["data"][$k]['Event_id'] = $res[$i]->Event_id;
                $j++;
            }
            else
            {
                $myarr[$j - 1]["data"][$k]['Id'] = $res[$i]->Id;
                $myarr[$j - 1]["data"][$k]['Types'] = $res[$i]->Types;
                $myarr[$j - 1]["data"][$k]['Heading'] = $res[$i]->Heading;
                $myarr[$j - 1]["data"][$k]['presentation_file_type'] = $res[$i]->presentation_file_type;
                $myarr[$j - 1]["data"][$k]['images'] = "";
                if ($res[$i]->Images != 'NULL')
                {
                    $images_js_decode = json_decode($res[$i]->Images);
                    $myarr[$j - 1]["data"][$k]['images'] = $images_js_decode[0];
                }
                $myarr[$j - 1]["data"][$k]['map_Title'] = $res[$i]->map_Title;
                $myarr[$j - 1]["data"][$k]['Start_date'] = $res[$i]->Start_date;
                $myarr[$j - 1]["data"][$k]['Start_time'] = $res[$i]->Start_time;
                $myarr[$j - 1]["data"][$k]['End_date'] = $res[$i]->End_date;
                $myarr[$j - 1]["data"][$k]['End_time'] = $res[$i]->End_time;
                $myarr[$j - 1]["data"][$k]['Event_id'] = $res[$i]->Event_id;
            }
            $k++;
            $prev = $res[$i]->Types;
        }
        return $myarr;
    }
    /*public function getPresentationById($event_id,$presentation_id)
    {
    $this->db->select('CASE WHEN p.Id IS NULL THEN "" ELSE p.Id END as Id,CASE WHEN p.End_date IS NULL THEN "" ELSE p.End_date END as End_date,CASE WHEN p.End_time IS NULL THEN "" ELSE p.End_time END as End_time,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,CASE WHEN p.Status IS NULL THEN "" ELSE p.Status END as Status,CASE WHEN p.Thumbnail_status IS NULL THEN "" ELSE p.Thumbnail_status END as Thumbnail_status,CASE WHEN p.Auto_slide_status IS NULL THEN "" ELSE p.Auto_slide_status END as Auto_slide_status,CASE WHEN p.Image_lock IS NULL THEN "" ELSE p.Image_lock END as Image_lock,user_permissions',false);
    $this->db->from('presentation p');
    $this->db->where('p.Event_id',$event_id);
    $this->db->where('p.Id',$presentation_id);
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
    }
    public function getPresentationImagesById($event_id,$presentation_id)
    {
    $this->db->select('CASE WHEN p.Id IS NULL THEN "" ELSE p.Id END as Id,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,CASE WHEN p.Image_lock IS NULL THEN "" ELSE p.Image_lock END as Image_lock',false);
    $this->db->from('presentation p');
    $this->db->where('p.Event_id',$event_id);
    $this->db->where('p.Id',$presentation_id);
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
    }
    public function getSurvayData($where)
    {
    return $this->db->select('*')->from('survey')->where($where)->get()->row_array();
    }
    public function lockUnlockSlides($presentation_id,$lock_data)
    {
    $this->db->select('*')->from('presentation_tool pt');
    $this->db->where('presentation_id',$presentation_id);
    $qu=$this->db->get();
    $res=$qu->result_array();
    if(count($res) > 0)
    {
    $this->db->where('presentation_id',$presentation_id);
    $this->db->update('presentation_tool',$lock_data);
    }
    else
    {
    $data['presentation_id']=$presentation_id;
    $this->db->insert('presentation_tool',$lock_data);
    }
    }
    public function get_survey_and_by_user_id($sid,$uid=NULL)
    {
    $this->db->select('*');
    $this->db->from('poll_survey');
    $this->db->where('Question_id',$sid);
    if(!empty($uid)){
    $this->db->where('User_id',$uid);
    }
    $que=$this->db->get();
    $res=$que->result_array();
    return $res;
    }
    public function get_edit_survey_in_presentation($id,$sid)
    {
    $this->db->select('*');
    $this->db->from('survey');
    $this->db->where('Event_id',$id);
    $this->db->where('Id',$sid);
    $query = $this->db->get();
    $res = $query->row_array();
    return $res;
    }
    public function get_survey_answer_chart_by_survry_ids($qids)
    {
    $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
    $this->db->from('poll_survey p');
    $this->db->join('user u', 'u.id = p.User_id', 'left');
    $this->db->join('survey s', 's.Id = p.Question_id', 'left');
    $this->db->where('s.Id', $qids);
    $query = $this->db->get();
    $query_res = $query->result_array();
    return $query_res;
    }
    public function save_ans_in_presentation($ans)
    {
    $res=$this->get_survey_and_by_user_id($ans['Question_id'],$ans['User_id']);
    if(count($res) > 0)
    {
    $this->db->where('Question_id',$ans['Question_id']);
    unset($ans['Question_id']);
    $this->db->where('User_id',$ans['User_id']);
    unset($ans['User_id']);
    $this->db->update('poll_survey',$ans);
    }
    else
    {
    $this->db->insert('poll_survey',$ans);
    }
    }
    public function getPresentationTool($presentation_id)
    {
    $this->db->select("*");
    $this->db->from('presentation_tool');
    $this->db->where('presentation_id', $presentation_id);
    $query = $this->db->get();
    $query_res = $query->row_array();
    return $query_res;
    }*/
    public function getPresentationById($event_id= NULL, $presentation_id= NULL)

    {
        $this->db->select('CASE WHEN p.Id IS NULL THEN "" ELSE p.Id END as Id,CASE WHEN p.End_date IS NULL THEN "" ELSE p.End_date END as End_date,CASE WHEN p.End_time IS NULL THEN "" ELSE p.End_time END as End_time,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,CASE WHEN p.Status IS NULL THEN "" ELSE p.Status END as Status,CASE WHEN p.Thumbnail_status IS NULL THEN "" ELSE p.Thumbnail_status END as Thumbnail_status,CASE WHEN p.Auto_slide_status IS NULL THEN "" ELSE p.Auto_slide_status END as Auto_slide_status,CASE WHEN p.Image_lock IS NULL THEN "" ELSE p.Image_lock END as Image_lock,CASE WHEN p.user_permissions IS NULL THEN "" ELSE p.user_permissions END as user_permissions', false);
        $this->db->from('presentation p');
        $this->db->where('p.Event_id', $event_id);
        $this->db->where('p.Id', $presentation_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getPresentationImagesById($event_id= NULL, $presentation_id= NULL)

    {
        $this->db->select('CASE WHEN p.Id IS NULL THEN "" ELSE p.Id END as Id,CASE WHEN p.Images IS NULL THEN "" ELSE p.Images END as Images,CASE WHEN p.Image_lock IS NULL THEN "" ELSE p.Image_lock END as Image_lock', false);
        $this->db->from('presentation p');
        $this->db->where('p.Event_id', $event_id);
        $this->db->where('p.Id', $presentation_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getSurvayData($where= NULL)

    {
        return $this->db->select('*')->from('survey')->where($where)->get()->row_array();
    }
    public function lockUnlockSlides($presentation_id= NULL, $lock_data= NULL)

    {
        $this->db->select('*')->from('presentation_tool pt');
        $this->db->where('presentation_id', $presentation_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $this->db->where('presentation_id', $presentation_id);
            $this->db->update('presentation_tool', $lock_data);
        }
        else
        {
            $lock_data['presentation_id'] = $presentation_id;
            $this->db->insert('presentation_tool', $lock_data);
        }
    }
    public function get_survey_and_by_user_id($sid= NULL, $uid = NULL)

    {
        $this->db->select('*');
        $this->db->from('poll_survey');
        $this->db->where('Question_id', $sid);
        if (!empty($uid))
        {
            $this->db->where('User_id', $uid);
        }
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_edit_survey_in_presentation($id= NULL, $sid= NULL)

    {
        $this->db->select('*');
        $this->db->from('survey');
        $this->db->where('Event_id', $id);
        $this->db->where('Id', $sid);
        $query = $this->db->get();
        $res = $query->row_array();
        return $res;
    }
    public function get_survey_answer_chart_by_survry_ids($qids= NULL)

    {
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->where('s.Id', $qids);
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function save_ans_in_presentation($ans= NULL)

    {
        $res = $this->get_survey_and_by_user_id($ans['Question_id'], $ans['User_id']);
        if (count($res) > 0)
        {
            $this->db->where('Question_id', $ans['Question_id']);
            unset($ans['Question_id']);
            $this->db->where('User_id', $ans['User_id']);
            unset($ans['User_id']);
            $this->db->update('poll_survey', $ans);
        }
        else
        {
            $this->db->insert('poll_survey', $ans);
        }
    }
    public function getPresentationTool($presentation_id= NULL)

    {
        $this->db->select("*");
        $this->db->from('presentation_tool');
        $this->db->where('presentation_id', $presentation_id);
        $query = $this->db->get();
        $query_res = $query->row_array();
        return $query_res;
    }
    public function updatePresentationTool($presentation_id= NULL, $is_bar_chart= NULL)

    {
        $data['chart_type'] = $is_bar_chart;
        $this->db->where('presentation_id', $presentation_id);
        $this->db->update('presentation_tool', $data);
    }
    public function getUrlData($event_id= NULL, $is_bar_chart= NULL)

    {
        $this->db->select('e.Subdomain,u.acc_name');
        $this->db->from('event e');
        $this->db->join('user u', 'u.Id = e.Organisor_id');
        $this->db->where('e.Id', $event_id);
        $data = $this->db->get()->row_array();
        // showbarchartdata
        if ($is_bar_chart == "1")
        {
            return $data['acc_name'] . '/' . $data['Subdomain'] . '/showbarchartdata/';
        }
        else
        {
            return $data['acc_name'] . '/' . $data['Subdomain'] . '/showchartdata/';
        }
    }
    public function getEventId($presentation_id= NULL)

    {
        $data = $this->db->select('Event_id')->from('presentation')->where('Id', $presentation_id)->get()->row_array();
        return $data['Event_id'];
    }
    public function saveToDB($gcm_id= NULL)

    {
        $data['name'] = $gcm_id;
        $this->db->insert('temp_socket', $data);
    }
}
?>