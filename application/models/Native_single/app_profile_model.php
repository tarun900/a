<?php
class app_profile_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getStateListByCountry($id=NULL)

    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('country_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function update_user_profile($user_id=NULL, $data=NULL)

    {
        $this->db->where("Id", $user_id);
        $this->db->update("user", $data);
    }
    public function make_thumbnail($user_id=NULL, $image_name=NULL)

    {
        try
        {
            $destination = "./assets/user_files/thumbnail/" . $image_name;
            // copy("assets/user_files/".$image_name, $destination);
            $config['image_library'] = 'gd2';
            $config['source_image'] = $destination;
            $config['file_name'] = $image_name;
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 100;
            $config['height'] = 100;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
    public function getAllUsersLogo()

    {
        $this->db->select('Logo');
        $this->db->from('user');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_extra_column($user_id=NULL, $event_id=NULL)

    {
        $this->db->select('json_data');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get('signup_form');
        $res = $query->row_array();
        $res = json_decode($res['json_data'], true);
        $data1 = array_column_1($res['fields'], 'title');
        $this->db->select('json_data');
        $this->db->where('user_id', $user_id);
        // $this->db->where('event_id',$event_id);
        $query = $this->db->get('signup_form_data');
        $res1 = $query->row_array();
        $res1 = json_decode($res1['json_data'], true);
        $array1 = array_intersect($data1, array_keys($res1));
        if (!empty($res1))
        {
            foreach($res['fields'] as $key => $value)
            {
                if (in_array($value['title'], $array1))
                {
                    $data[] = $value;
                }
            }
            foreach($data as $key => $value)
            {
                foreach($res1 as $key1 => $value1)
                {
                    if ($key1 == $value['title'])
                    {
                        if ($value['type'] == 'element-multiple-choice')
                        {
                            foreach($value['choices'] as $k => $v)
                            {
                                if (in_array($v['value'], $value1)) $data[$key]['choices'][$k]['checked'] = true;
                                else $data[$key]['choices'][$k]['checked'] = false;
                            }
                        }
                        elseif ($value['type'] == 'element-checkboxes' || $value['type'] == 'element-dropdown')
                        {
                            foreach($value['choices'] as $k => $v)
                            {
                                if ($v['value'] == $value1) $data[$key]['choices'][$k]['checked'] = true;
                                else $data[$key]['choices'][$k]['checked'] = false;
                            }
                        }
                        $data[$key]['user_value'] = $value1;
                    }
                }
            }
        }
        else
        {
            $data = $res['fields'];
        }
        return $data;
    }
    public function save_extra_column($user_id=NULL, $data=NULL)

    {
        $this->db->where("user_id", $user_id);
        $this->db->update("signup_form_data", $data);
        return true;
    }
    public function get_social_login_status($event_id=NULL)

    {
        $this->db->select('CASE WHEN facebook_login IS NULL THEN 0 ELSE facebook_login END as is_facebook,
                           CASE WHEN linkedin_login_enabled IS NULL THEN 0 ELSE linkedin_login_enabled END as is_linkedin', false);
        $this->db->where('Event_id', $event_id);
        $this->db->from('fundraising_setting');
        $query = $this->db->get();
        $res = $query->row_array();
        return $res;
    }
}
?>