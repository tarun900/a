<?php
class beacon_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function insert_beacon($insert=NULL, $where=NULL)

    {
        $this->db->select('*');
        $this->db->from('Beacons b');
        $this->db->where($where);
        $count = $this->db->get()->num_rows();
        if ($count) return "Beacon already saved";
        else
        {
            if ($this->db->insert("Beacons", $insert))
            {
                return "Beacon saved successfully";
            }
        }
    }
    public function edit_beacon($id=NULL, $data=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->update("Beacons", $data);
        return "Beacon Rename Successfully";
    }
    public function delete_beacon($id=NULL)

    {
        $this->db->delete("Beacons", "Id = $id");
        return "Beacon Deleted Successfully";
    }
    public function get_all_beacon($event_id=NULL, $organizer_id=NULL)

    {
        $this->db->select('CASE WHEN Id IS NULL THEN "" ELSE Id END as Id,
                           CASE WHEN beacon_name IS NULL THEN "" ELSE beacon_name END as beacon_name, 
                           CASE WHEN UDID IS NULL THEN "" ELSE UDID END as UDID,
                           CASE WHEN major IS NULL THEN "" ELSE major END as major,
                           CASE WHEN minor IS NULL THEN "" ELSE minor END as minor,
                           CASE WHEN nameSpaceId IS NULL THEN "" ELSE nameSpaceId END nameSpaceId,
                           CASE WHEN InstanceId IS NULL THEN "" ELSE InstanceId END as InstanceId', false);
        $this->db->from('Beacons');
        $this->db->where('organizer_id', $organizer_id);
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_notification($where=NULL)

    {
        $this->db->_protect_identifiers = false;
        $this->db->select('t.message,t.trigger_name,b.Id,t.modules');
        $this->db->from('Triggers t');
        $this->db->join("Beacons b", "FIND_IN_SET(b.Id, t.beacon_id)");
        $this->db->where($where);
        $this->db->group_by('t.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_users($event_id=NULL, $User_id=NULL)

    {
        $this->db->select('u.Id,u.gcm_id,u.device,u.Organisor_id');
        $this->db->from('relation_event_user r');
        $this->db->join('user u', 'u.Id = r.User_id');
        $this->db->where('r.Event_id', $event_id);
        $this->db->where('r.User_id', $User_id);
        $this->db->where('r.Role_id', '4');
        $query = $this->db->get();
        $res = $query->row_array();
        return $res;
    }
    public function add_beacon_movement($insert=NULL)

    {
        $this->db->select('*');
        $this->db->from('beacon_movements');
        $this->db->where('beacon_id', $insert['beacon_id']);
        $query = $this->db->get();
        if ($res = $query->result_array())
        {
            $res[0]['user_id'].= "," . $insert['user_id'];
            $this->db->where('id', $res[0]['Id']);
            $this->db->set('user_id', $res[0]['user_id']);
            $this->db->set('number_of_hits', 'number_of_hits+1', false);
            $this->db->update('beacon_movements', $data);
        }
        else
        {
            $insert['number_of_hits'] = 1;
            if ($this->db->insert("beacon_movements", $insert))
            {
                return true;
            }
        }
    }
    public function check_beacon_movement($user_id=NULL, $beacon_id=NULL)

    {
        $this->db->_protect_identifiers = false;
        $this->db->select('*');
        $this->db->from('beacon_movements bm');
        $this->db->where("FIND_IN_SET($user_id,bm.user_id) != ", 0);
        $this->db->where('beacon_id', $beacon_id);
        $query = $this->db->get();
        if ($res = $query->result_array())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function get_beacon_id($data=NULL)

    {
        $this->db->select('Id');
        $this->db->from('Beacons');
        $this->db->where("UDID", $data['UDID']);
        if (!empty($data['nameSpaceId']))
        {
            $this->db->where("nameSpaceId", $data['nameSpaceId']);
            $this->db->where("InstanceId", $data['InstanceId']);
        }
        else
        {
            $this->db->where("major", $data['major']);
            $this->db->where("minor", $data['minor']);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['Id'];
    }
}
?>