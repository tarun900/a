<?php
class Developmentuser_model extends CI_Model

{
    function __construct()
    {
        $this->db1 = $this->load->database('db1', TRUE);
        parent::__construct();
    }
    public function getusersByEventid($event_id)

    {
        $this->db->select('*');
        $this->db->from('developement_users');
        $this->db->where('event_id', $event_id);
        $this->db->where('is_deleted', '0');
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function getAllRoles($event_id)

    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('event_id', $event_id);
        $this->db->or_where_in('id', array(
            '3',
            '4',
            '5',
            '6',
            '7',
            '73',
            '95'
        ));
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function assignRoles($id, $assignroles)

    {
        $data = array(
            'role_id' => $assignroles
        );
        $this->db->where('id', $id);
        $this->db->update('developement_users', $data);
    }
    public function softdeleteUser($id)

    {
        $date = date('Y-m-d H:i:s');
        $data = array(
            'fname' => '',
            'lname' => '',
            'email' => '',
            'role_id' => '',
            'event_id' => '0',
            'is_deleted' => '1',
            'created_at ' => '0000-00-00 00:00:00',
            'updated_at' => $date
        );
        $this->db->where('id', $id);
        $this->db->update('developement_users', $data);
    }
    public function insert_user($user_data)

    {
        $this->db->set($user_data);
        $this->db->insert('developement_users');
    }
    public function check_email($email)

    {
        $this->db->select('*');
        $this->db->from('developement_users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function check_email_InCsv($email)

    {
        $this->db->select('*');
        $this->db->from('developement_users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function getUserByid($id)

    {
        $this->db->select('*');
        $this->db->from('developement_users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function check_email_forEdit($email, $id)

    {
        $this->db->select('*');
        $this->db->from('developement_users');
        $this->db->where('email', $email);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function update_user($id, $user_data)

    {
        // $this->db->set($usedata);
        $this->db->where('id', $id);
        $this->db->update('developement_users', $user_data);
    }
    public function getDataForPermisionListing($event_id)

    {
        $this->db->select('*, development_permision.id AS dpid');
        $this->db->from('development_permision');
        $this->db->join('role', 'role.id = development_permision.role_id', 'left');
        $this->db->join('menu', 'menu.id = development_permision.menu_id', 'left');
        $this->db->where('development_permision.event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function deletePermision($id)

    {
        $this->db->where('id', $id);
        $this->db->delete('development_permision');
        $rows = $this->db->affected_rows();
        if ($rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function getAllMenu($event_id)

    {
        $this->db->select('*');
        $this->db->from('menu');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    //     public function multiple_insert_permision($event_id,$menu_id,$role_id,$functionality_id)
    // {
    //      $data = array();
    //      $functionality_ids=implode(",",$functionality_id);
    //    for ($i=0; $i <count($role_id) ; $i++) {
    //       $newarray=array(
    //           'event_id' => $event_id ,
    //           'role_id' => $role_id[$i] ,
    //           'menu_id' => $menu_id,
    //           'functionality_id'=>$functionality_ids
    //        );
    //       array_push($data,$newarray);
    //    }
    //     $this->db->insert_batch('development_permision', $data);
    // }
    public function multiple_insert_permision($event_id, $menu_id, $role_id)

    {
        $data = array();
        // $functionality_ids=implode(",",$functionality_id);
        $functionality_ids = '';
        for ($i = 0; $i < count($role_id); $i++)
        {
            $newarray = array(
                'event_id' => $event_id,
                'role_id' => $role_id[$i],
                'menu_id' => $menu_id,
                'functionality_id' => $functionality_ids
            );
            array_push($data, $newarray);
        }
        $this->db->insert_batch('development_permision', $data);
    }
    public function getpermisionByid($id)

    {
        $this->db->select('*');
        $this->db->from('development_permision');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function multiple_update_permision($event_id, $menu_id, $role_id, $id, $functionality_id)

    {
        $functionality_ids = implode(",", $functionality_id);
        $date = date('Y-m-d H:i:s');
        $permision_data = array(
            'event_id' => $event_id,
            'menu_id' => $menu_id,
            'role_id' => $role_id,
            'functionality_id' => $functionality_ids,
            'updated_at' => $date
        );
        $this->db->where('id', $id);
        $this->db->update('development_permision', $permision_data);
    }
    public function getDataForFunctinalityListing($event_id)

    {
        $this->db->select('*, development_functionality_master.id AS dfmid');
        $this->db->from('development_functionality_master');
        $this->db->join('menu', 'menu.id = development_functionality_master.menu_id', 'left');
        $this->db->where('development_functionality_master.event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function deletefunctionality($id)

    {
        $this->db->where('id', $id);
        $this->db->delete('development_functionality_master');
        $rows = $this->db->affected_rows();
        if ($rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function insert_funtionality($event_id, $menu_id, $functionality_name)

    {
        $data = array(
            'event_id' => $event_id,
            'menu_id' => $menu_id,
            'functinality_name' => $functionality_name
        );
        $this->db->set($data);
        $this->db->insert('development_functionality_master');
    }
    public function getfunctionalityByid($id)

    {
        $this->db->select('*');
        $this->db->from('development_functionality_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function multiple_update_functionality($event_id, $menu_id, $functionality_name, $id)

    {
        $date = date('Y-m-d H:i:s');
        $data = array(
            'event_id' => $event_id,
            'menu_id' => $menu_id,
            'functinality_name' => $functionality_name,
            'updated_at' => $date
        );
        $this->db->where('id', $id);
        $this->db->update('development_functionality_master', $data);
    }
    public function getfunctionalityBymenuid($menu_id, $event_id)

    {
        $this->db->select('*');
        $this->db->from('development_functionality_master');
        $this->db->where('menu_id', $menu_id);
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function getAllFunctionality($event_id)

    {
        $this->db->select('*');
        $this->db->from('development_functionality_master');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
    public function get_admin_stripe_setting()

    {
        $this->db1->select('*')->from('stripe_settings');
        $this->db1->where('id', '1');
        $qu = $this->db1->get();
        $res = $qu->result_array();
        return $res;
    }
    public function getfundraising_enabled($eid)

    {
        $this->db->select('*')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_all_event_name()

    {
        $this->db->select('Subdomain');
        $this->db->from('event');
        $query = $this->db->get();
        $res = $query->result_array();
        for ($i = 0; $i < count($res); $i++)
        {
            $res[$i] = $res[$i]['Subdomain'];
        }
        return $res;
    }
    public function getnotificationsetting($eid)

    {
        $this->db->select('email_display,pushnoti_display,facebook_login,linkedin_login_enabled,format_time')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getTimeFormat($eid)

    {
        $this->db->select('format_time')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_facebook_login($format_time, $fackbook_login, $linkedin_login_enabled, $eid)

    {
        $this->db->select('Event_id')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            $fackbook_login_setting = array(
                'facebook_login' => $fackbook_login,
                'linkedin_login_enabled' => $linkedin_login_enabled,
                'format_time' => $format_time
            );
            $this->db->where('Event_id', $eid);
            $this->db->update('fundraising_setting', $fackbook_login_setting);
        }
        else
        {
            $fackbook_login_setting = array(
                'facebook_login' => $fackbook_login,
                'Event_id' => $eid,
                'linkedin_login_enabled' => $linkedin_login_enabled,
                'format_time' => $format_time
            );
            $this->db->insert('fundraising_setting', $fackbook_login_setting);
        }
    }
    public function getraisedsetting($eid)

    {
        $this->db->select('facebook_login');
        $this->db->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_paypal_settings($id)

    {
        $this->db->select('*')->from('paypal_settings');
        $this->db->where("organisor_id", $id);
        $qry = $this->db->get();
        $res = $qry->result_array();
        return $res;
    }
    public function update_paypal_settings($orid, $data)

    {
        $this->db->where("organisor_id", $orid);
        $this->db->update("paypal_settings", $data);
    }
    public function add_paypal_settings($orid, $data)

    {
        $data['organisor_id'] = $orid;
        $this->db->insert("paypal_settings", $data);
    }
    public function delete_logo($id)

    {
        $data['Logo_images'] = '';
        $this->db->where("Id", $id);
        $this->db->update("temp_event", $data);
        $this->db->update("event", $data);
    }
    public function get_menu_list($roleid, $eventid)

    {
        $this->db->select('*');
        $this->db->from('role_permission r');
        $this->db->where('r.Role_id', $roleid);
        $this->db->where('r.event_id', $eventid);
        $this->db->order_by("r.Menu_id", "asc");
        $this->db->join('menu m', 'm.id=r.Menu_id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_view_hit($userid, $current_date, $menu_id, $event_id)

    {
        $this->db->select('menu_hit');
        $this->db->from('users_leader_board');
        $this->db->where('event_id', $event_id);
        $this->db->where("user_id", $userid);
        $this->db->where("date", $current_date);
        $this->db->where("menu_id", $menu_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $menu_hit = $res[0]['menu_hit'];
        if ($menu_hit == '')
        {
            $data['menu_hit'] = 1;
            $data['user_id'] = $userid;
            $data['date'] = $current_date;
            $data['menu_id'] = $menu_id;
            $data['event_id'] = $event_id;
            $this->db->insert("users_leader_board", $data);
        }
        else
        {
            $data['menu_hit'] = $menu_hit + 1;
            $this->db->where('event_id', $event_id);
            $this->db->where("user_id", $userid);
            $this->db->where("date", $current_date);
            $this->db->where("menu_id", $menu_id);
            $this->db->update("users_leader_board", $data);
        }
    }
    public function delete_view_hit($userid, $current_date, $menu_id)

    {
        $this->db->select('menu_hit');
        $this->db->from('users_leader_board');
        $this->db->where("user_id", $userid);
        $this->db->where("date", $current_date);
        $this->db->where("menu_id", $menu_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $menu_hit = $res[0]['menu_hit'];
        if ($menu_hit == '')
        {
            $data['menu_hit'] = 1;
            $data['user_id'] = $userid;
            $data['date'] = $current_date;
            $data['menu_id'] = $menu_id;
            if ($userid1 != '')
            {
                $this->db->insert("users_leader_board", $data);
            }
        }
        else
        {
            $data['menu_hit'] = $menu_hit - 1;
            $this->db->where("user_id", $userid);
            $this->db->where("date", $current_date);
            $this->db->where("menu_id", $menu_id);
            $this->db->update("users_leader_board", $data);
        }
    }
    public function checkkey($key, $eid)

    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('secure_key', $key);
        $this->db->where('Id !=', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_menu_id($menuname = null)

    {
        $this->db->select('id');
        $this->db->from('menu');
        $this->db->where("menuname", $menuname);
        $query = $this->db->get();
        $res = $query->result_array();
        $id = $res[0]['id'];
        return $id;
    }
    public function get_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.Start_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->where("e.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        // $this->db->where('e.Status','1');
        $this->db->order_by('Created_date', DESC);
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Checkbox_values'] = $res[$i]->checkbox_values;
            $this->db->select('*')->from('hub_event');
            $this->db->where('event_id', $res[$i]->Id);
            $hquery = $this->db->get();
            $hres = $hquery->result_array();
            if (count($hres) > 0)
            {
                $event_res[$i]['is_add_hub'] = '1';
            }
            else
            {
                $event_res[$i]['is_add_hub'] = '0';
            }
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else if ($this->data['user']->Role_name == 'Attendee')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else
        {
            $event_res_val = $event_res;
        }
        return $event_res_val;
    }
    public function get_module_event($id = null)

    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $qry = $this->db->get();
        $res = $qry->result_array();
        return $res;
    }
    public function get_total_invited_attendee_event_list()

    {
    }
    public function get_archive_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('Created_date', DESC);
        // $this->db->where('e.Status','1');
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Checkbox_values'] = $res[$i]->checkbox_values;
            $this->db->select('*')->from('hub_event');
            $this->db->where('event_id', $res[$i]->Id);
            $hquery = $this->db->get();
            $hres = $hquery->result_array();
            if (count($hres) > 0)
            {
                $event_res[$i]['is_add_hub'] = '1';
            }
            else
            {
                $event_res[$i]['is_add_hub'] = '0';
            }
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else if ($this->data['user']->Role_name == 'Attendee')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else
        {
            $event_res_val = $event_res;
        }
        return $event_res_val;
    }
    public function get_user_archive_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id', 'left');
        $this->db->where('u.Email', $user[0]->Email);
        $this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->where('e.Status', '1');
        $this->db->group_by('e.Id');
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
        }
        return $event_res;
    }
    public function get_user_feature_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id', 'left');
        $this->db->where('u.Email', $user[0]->Email);
        $this->db->where("e.Start_date > DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->where('e.Status', '1');
        $this->db->group_by('e.Id');
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Checkbox_values'] = $res[$i]->checkbox_values;
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else if ($this->data['user']->Role_name == 'Attendee')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else
        {
            $event_res_val = $event_res;
        }
        return $event_res_val;
    }
    public function get_feature_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.Start_date > DATE_FORMAT(NOW(),'%Y-%m-%d')");
        // $this->db->where('e.Status','1');
        $this->db->order_by('Created_date', DESC);
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Checkbox_values'] = $res[$i]->checkbox_values;
            $this->db->select('*')->from('hub_event');
            $this->db->where('event_id', $res[$i]->Id);
            $hquery = $this->db->get();
            $hres = $hquery->result_array();
            if (count($hres) > 0)
            {
                $event_res[$i]['is_add_hub'] = '1';
            }
            else
            {
                $event_res[$i]['is_add_hub'] = '0';
            }
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else if ($this->data['user']->Role_name == 'Attendee')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else
        {
            $event_res_val = $event_res;
        }
        return $event_res_val;
    }
    public function view_event_by_id($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->where('e.id', $id);
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id && $user[0]->Role_name == 'Client')
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else if ($this->data['user']->Role_name == 'Attendee')
        {
            $event_res_val = array_merge(array() , $event_res);
        }
        else
        {
            $event_res_val = $event_res;
        }
        return $event_res_val;
    }
    public function get_latest_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select("e.*,count(ai.Id) as total_invited_att");
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.organisor_id and u.Login_date IS NOT NULL', 'left');
        $this->db->join("attendee_invitation ai", "ai.Event_id=e.Id", "left");
        $this->db->where("e.End_date >= CURRENT_DATE()");
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->group_by('e.Id');
        $this->db->order_by('e.Start_date desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result();
        foreach($res as $key => $value)
        {
            $eventid = $value->Id;
            $this->db->select("u.Login_date,u.Id,ru.Event_id,count(u.Id) as total_login");
            $this->db->from('user u');
            $this->db->join('relation_event_user ru', 'ru.User_id=u.Id and u.Login_date IS NOT NULL', 'right');
            $this->db->where("ru.Event_id", $eventid);
            $this->db->group_by('ru.Event_id');
            $query = $this->db->get();
            $res1 = $query->result_array();
            $res[$key]->total_login = $res1[0]['total_login'];
        }
        return $res;
    }
    public function get_attendee_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id', 'left');
        $this->db->where('u.Email', $user[0]->Email);
        $this->db->group_by('e.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_admin_event($id = null)

    {
        $this->db->select('e.*');
        $this->db->from('event e');
        if ($id)
        {
            $this->db->where('e.Id', $id);
        }
        $query = $this->db->get();
        $res = $query->result();
        $org_email = $this->getOrgEmail($res[0]->Organisor_id);
        $iseventfreetrial = $this->check_event_free_trial_account_by_org_email($org_email);
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Logo_images'] = $res[$i]->Logo_images;
            $event_res[$i]['Background_img'] = $res[$i]->Background_img;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['icon_set_type'] = $res[$i]->icon_set_type;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['menu_background_color'] = $res[$i]->menu_background_color;
            $event_res[$i]['menu_hover_background_color'] = $res[$i]->menu_hover_background_color;
            $event_res[$i]['menu_text_color'] = $res[$i]->menu_text_color;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Beacon_feature'] = $res[$i]->Beacon_feature;
            $event_res[$i]['UUID'] = $res[$i]->UUID;
            $event_res[$i]['only_notify_login_user'] = $res[$i]->only_notify_login_user;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Event_time'] = $res[$i]->Event_time;
            $event_res[$i]['secure_key'] = $res[$i]->secure_key;
            $event_res[$i]['social_media'] = $res[$i]->social_media;
            $event_res[$i]['Event_time_option'] = $res[$i]->Event_time_option;
            $event_res[$i]['Event_time_zone'] = $res[$i]->Event_time_zone;
            $event_res[$i]['show_session_by_time'] = $res[$i]->show_session_by_time;
            $event_res[$i]['key_people_sort_by'] = $res[$i]->key_people_sort_by;
            $event_res[$i]['allow_msg_keypeople_to_attendee'] = $res[$i]->allow_msg_keypeople_to_attendee;
            $event_res[$i]['hide_request_meeting'] = $res[$i]->hide_request_meeting;
            $event_res[$i]['attendee_hide_request_meeting'] = $res[$i]->attendee_hide_request_meeting;
            $event_res[$i]['allow_meeting_exibitor_to_attendee'] = $res[$i]->allow_meeting_exibitor_to_attendee;
            $event_res[$i]['allow_msg_user_to_exhibitor'] = $res[$i]->allow_msg_user_to_exhibitor;
            $event_res[$i]['on_pending_agenda'] = $res[$i]->on_pending_agenda;
            $event_res[$i]['show_agenda_place_left_column'] = $res[$i]->show_agenda_place_left_column;
            $event_res[$i]['show_agenda_speaker_column'] = $res[$i]->show_agenda_speaker_column;
            $event_res[$i]['show_agenda_location_column'] = $res[$i]->show_agenda_location_column;
            $event_res[$i]['show_attendee_menu'] = $res[$i]->show_attendee_menu;
            $event_res[$i]['date_format'] = $res[$i]->date_format;
            $event_res[$i]['Event_show_time_zone'] = $res[$i]->Event_show_time_zone;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['checkbox_values'] = $res[$i]->checkbox_values;
            $event_res[$i]['hub_menu_show'] = $res[$i]->hub_menu_show;
            $event_res[$i]['insta_access_token'] = $res[$i]->insta_access_token;
            $event_res[$i]['authorized_email'] = $res[$i]->authorized_email;
            $event_res[$i]['facebook_page_name'] = $res[$i]->facebook_page_name;
            $event_res[$i]['hub_menu_title'] = $res[$i]->hub_menu_title;
            $event_res[$i]['Img_view'] = $res[$i]->Img_view;
            $event_res[$i]['photo_filter_image'] = $res[$i]->photo_filter_image;
            $event_res[$i]['allow_export_lead_data'] = $res[$i]->allow_export_lead_data;
            $event_res[$i]['allow_custom_survey_lead'] = $res[$i]->allow_custom_survey_lead;
            $event_res[$i]['hide_survey'] = $res[$i]->hide_survey;
            $event_res[$i]['allow_show_all_agenda'] = $res[$i]->allow_show_all_agenda;
            $event_res[$i]['agenda_sort'] = $res[$i]->agenda_sort;
            $event_res[$i]['show_login_screen'] = $res[$i]->show_login_screen;
            $event_res[$i]['menu_sidebar'] = $this->geteventmenu($res[$i]->Id);
            $event_res[$i]['show_slider'] = $res[$i]->show_slider;
            $event_res[$i]['access_key'] = $res[$i]->access_key;
            $event_res[$i]['api_sync'] = $res[$i]->api_sync;
            if (count($iseventfreetrial) > 0)
            {
                $event_res[$i]['iseventfreetrial'] = '1';
            }
            else
            {
                $event_res[$i]['iseventfreetrial'] = '0';
            }
            $event_res[$i]['o_screen'] = $res[$i]->o_screen;
            $event_res[$i]['banner_url'] = $res[$i]->banner_url;
            $event_res[$i]['meeting_time_slot'] = $res[$i]->meeting_time_slot;
            $event_res[$i]['meeting_start_time'] = $res[$i]->meeting_start_time;
            $event_res[$i]['meeting_end_time'] = $res[$i]->meeting_end_time;
            $event_res[$i]['allow_meeting_attendee_to_speaker'] = $res[$i]->allow_meeting_attendee_to_speaker;
            $event_res[$i]['adv_show_sticky'] = $res[$i]->adv_show_sticky;
            $event_res[$i]['show_topbar_exhi'] = $res[$i]->show_topbar_exhi;
            $event_res[$i]['no_of_login_attempts'] = $res[$i]->no_of_login_attempts;
            $event_res[$i]['link_auto_attendee'] = $res[$i]->link_auto_attendee;
            $event_res[$i]['max_meeting'] = $res[$i]->max_meeting;
            $event_res[$i]['hide_user_identity'] = $res[$i]->hide_user_identity;
            $event_res[$i]['show_reminder_button'] = $res[$i]->show_reminder_button;
            $event_res[$i]['enable_block_button'] = $res[$i]->enable_block_button;
            $event_res[$i]['enable_hide_my_identity'] = $res[$i]->enable_hide_my_identity;
        }
        $eventmodule = $this->geteventmodulues($id);
        $module = json_decode($eventmodule[0]['module_list']);
        foreach($event_res[0]['menu_sidebar'] as $key => $value)
        {
            if (!in_array($value->id, $module))
            {
                unset($event_res[0]['menu_sidebar'][$key]);
            }
        }
        return $event_res;
    }
    public function add_admin_event($data)

    {
        $arrAppNotiTemplate = $this->db->get("default_app_notification_template")->result_array();
        $arrEmailTemplate = $this->db->get("email_notification_templates")->result_array();
        $arrpush = $this->db->get("default_app_push_template")->result_array();
        if (!empty($data['event_array']['Images']))
        {
            $array_event['Images'] = json_encode($data['event_array']['Images']);
        }
        if (!empty($data['event_array']['Logo_images']))
        {
            $array_event['Logo_images'] = $data['event_array']['Logo_images'];
        }
        if (!empty($data['event_array']['fun_logo_images']))
        {
            $array_event['fun_logo_images'] = $data['event_array']['fun_logo_images'];
        }
        if ($data['event_array']['Background_img'] != null)
        {
            $array_event['Background_img'] = $data['event_array']['Background_img'];
        }
        if ($data['event_array']['menu_background_color'] != Null) $array_event['menu_background_color'] = $data['event_array']['menu_background_color'];
        if ($data['event_array']['menu_text_color'] != Null) $array_event['menu_text_color'] = $data['event_array']['menu_text_color'];
        if ($data['event_array']['menu_hover_background_color'] != Null) $array_event['menu_hover_background_color'] = $data['event_array']['menu_hover_background_color'];
        if ($data['event_array']['Subdomain'] != NULL) $array_event['Subdomain'] = $data['event_array']['Subdomain'];
        if ($data['event_array']['Event_name'] != NULL) $array_event['Event_name'] = $data['event_array']['Event_name'];
        if ($data['event_array']['End_date'] != NULL) $array_event['End_date'] = $data['event_array']['End_date'];
        if ($data['event_array']['Start_date'] != NULL) $array_event['Start_date'] = $data['event_array']['Start_date'];
        if ($data['event_array']['Status'] != NULL) $array_event['Status'] = $data['event_array']['Status'];
        if ($data['event_array']['Event_type'] != NULL) $array_event['Event_type'] = $data['event_array']['Event_type'];
        if ($data['event_array']['Event_time'] != NULL) $array_event['Event_time'] = $data['event_array']['Event_time'];
        if ($data['event_array']['Event_time_option'] != NULL) $array_event['Event_time_option'] = $data['event_array']['Event_time_option'];
        if ($data['event_array']['Description'] != NULL) $array_event['Description'] = $data['event_array']['Description'];
        if ($data['event_array']['Background_color'] != NULL) $array_event['Background_color'] = $data['event_array']['Background_color'];
        if ($data['event_array']['Top_background_color'] != NULL) $array_event['Top_background_color'] = $data['event_array']['Top_background_color'];
        if ($data['event_array']['Top_text_color'] != NULL) $array_event['Top_text_color'] = $data['event_array']['Top_text_color'];
        if ($data['event_array']['Icon_text_color'] != NULL) $array_event['Icon_text_color'] = $data['event_array']['Icon_text_color'];
        if ($data['event_array']['icon_set_type'] != NULL) $array_event['icon_set_type'] = $data['event_array']['icon_set_type'];
        if ($data['event_array']['Footer_background_color'] != NULL) $array_event['Footer_background_color'] = $data['event_array']['Footer_background_color'];
        if ($data['event_array']['Contact_us'] != NULL) $array_event['Contact_us'] = $data['event_array']['Contact_us'];
        if ($data['event_array']['Organisor_id'] != NULL) $array_event['Organisor_id'] = $data['event_array']['Organisor_id'];
        if (!empty($data['event_array']['secure_key']))
        {
            $array_event['secure_key'] = $data['event_array']['secure_key'];
        }
        // pen
        $array_event['event_token'] = sha1('1234567890' . rand() . rand(1, 4));
        // pen
        $this->db->insert('event', $array_event);
        $event_id = $this->db->insert_id();
        $lang_data['lang_name'] = "English";
        $lang_data['lang_icon'] = "United States.png";
        $lang_data['event_id'] = $event_id;
        $lang_data['lang_default'] = '1';
        $lang_data['created_date'] = date('Y-m-d H:i:s');
        $this->db->insert('event_language', $lang_data);
        if ($data['event_array']['fun_image'] != null)
        {
            $slideshow['image'] = $data['event_array']['fun_image'];
            $slideshow['event_id'] = $event_id;
            $this->db->insert('slideshow', $slideshow);
        }
        $fun_setting = "insert";
        if ($data['event_array']['stripe_show'] == '1')
        {
            if ($data['event_array']['slide_image'] != "")
            {
                $slideimg = $data['event_array']['slide_image'];
                $simg = explode(",", $slideimg);
                for ($i = 0; $i < count($simg); $i++)
                {
                    $sdata['image'] = "assets/images/slideshow/" . $simg[$i];
                    $sdata['event_id'] = $event_id;
                    $this->db->insert('slideshow', $sdata);
                }
            }
            if ($data['event_array']['Description'] != "")
            {
                $fundata['content'] = $data['event_array']['Description'];
            }
            if ($data['event_array']['Fundraising_target'] != "")
            {
                $fundata['Fundraising_target'] = $data['event_array']['Fundraising_target'];
            }
            if ($data['event_array']['event_video_link'] != "")
            {
                $fundata['event_video_link'] = $data['event_array']['event_video_link'];
            }
            if ($data['event_array']['target_raisedsofar_display'] != "")
            {
                $fundata['target_raisedsofar_display'] = $data['event_array']['target_raisedsofar_display'];
            }
            if ($data['event_array']['bids_donations_display'] != "")
            {
                $fundata['bids_donations_display'] = $data['event_array']['bids_donations_display'];
            }
            $fun_setting;
            if (!empty($fundata))
            {
                $fundata['Event_id'] = $event_id;
                $this->db->insert("fundraising_setting", $fundata);
                $fun_setting = "update";
            }
            else
            {
                $fun_setting = "insert";
            }
            if ($data['event_array']['currency'] != "")
            {
                $cdata['event_id'] = $event_id;
                $cdata['currency'] = $data['event_array']['currency'];
                $this->db->insert('fundraising_currency', $cdata);
            }
        }
        if ($event_id != '' && !empty($arrAppNotiTemplate))
        {
            foreach($arrAppNotiTemplate as $intKey => $strValue):
                $arrInsert = array();
                $arrInsert['Slug'] = $strValue['Slug'];
                $arrInsert['Subject'] = $strValue['Subject'];
                $arrInsert['From'] = $strValue['From'];
                $arrInsert['Content'] = $strValue['Content'];
                $arrInsert['event_id'] = $event_id;
                $this->db->insert('app_notification_template', $arrInsert);
            endforeach;
        }
        if ($event_id != '' && !empty($arrEmailTemplate))
        {
            foreach($arrEmailTemplate as $intKey => $strValue):
                $arrInsert = array();
                $arrInsert['Slug'] = $strValue['Slug'];
                $arrInsert['Subject'] = $strValue['Subject'];
                $arrInsert['From'] = $strValue['From'];
                $arrInsert['Content'] = $strValue['Content'];
                $arrInsert['event_id'] = $event_id;
                $this->db->insert('event_email_templates', $arrInsert);
            endforeach;
        }
        if ($event_id != '' && !empty($arrpush))
        {
            foreach($arrpush as $intKey => $strValue):
                $arrInsert = array();
                $arrInsert['Slug'] = $strValue['Slug'];
                $arrInsert['Content'] = $strValue['Content'];
                $arrInsert['event_id'] = $event_id;
                $this->db->insert('event_app_push_template', $arrInsert);
            endforeach;
        }
        $module = $this->geteventmodulues($event_id);
        $modulelist = json_decode($module[0]['module_list']);
        if (in_array("20", $modulelist))
        {
            if ($fun_setting == "insert")
            {
                $this->db->insert("fundraising_setting", array(
                    "fundraising_enbled" => "1",
                    "Event_id" => $event_id
                ));
            }
            else
            {
                $this->db->where('Id', $event_id);
                $this->db->update('fundraising_setting', array(
                    "fundraising_enbled" => "1"
                ));
            }
        }
        $arr['checkbox_values'] = "," . implode(",", $modulelist) . ",42";
        // pen
        $arr['event_token'] = sha1('56689598595' . rand() . rand(1, 4));
        // pen
        $this->db->where('Id', $event_id);
        $this->db->update('event', $arr);
        $view_data['view_name'] = "All Active Modules";
        $view_data['event_id'] = $event_id;
        $view_data['view_type'] = '1';
        $view_data['created_date'] = date('Y-m-d H:i:s');
        $this->db->insert('user_views', $view_data);
        return $event_id;
    }
    public function get_permission_list()

    {
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'Client')
        {
            $this->db->select('*');
            $this->db->from('event');
            if ($orid)
            {
                $this->db->where('event.Organisor_id', $orid);
            }
            $query = $this->db->get();
            $res = $query->result();
            return $res;
        }
    }
    public function update_admin_event($data)

    {
        if (!empty($data['event_array']['Images']))
        {
            $array_event['Images'] = json_encode($data['event_array']['Images']);
        }
        if (!empty($data['event_array']['Logo_images']))
        {
            $array_event['fun_logo_images'] = $data['event_array']['fun_logo_images'];
            $array_event['Logo_images'] = $data['event_array']['Logo_images'];
        }
        if (!empty($data['event_array']['Background_img']))
        {
            $background_img[] = $data['event_array']['Background_img'];
            $array_event['Background_img'] = json_encode($background_img);
        }
        if ($data['event_array']['social_media'] != NULL) $array_event['social_media'] = $data['event_array']['social_media'];
        $array_event['no_of_login_attempts'] = $data['event_array']['no_of_login_attempts'];
        if ($data['event_array']['menu_background_color'] != Null) $array_event['menu_background_color'] = $data['event_array']['menu_background_color'];
        if ($data['event_array']['menu_text_color'] != Null) $array_event['menu_text_color'] = $data['event_array']['menu_text_color'];
        if ($data['event_array']['menu_hover_background_color'] != Null) $array_event['menu_hover_background_color'] = $data['event_array']['menu_hover_background_color'];
        if ($data['event_array']['hub_menu_show'] != Null) $array_event['hub_menu_show'] = $data['event_array']['hub_menu_show'];
        if ($data['event_array']['hub_menu_title'] != Null) $array_event['hub_menu_title'] = $data['event_array']['hub_menu_title'];
        if ($data['event_array']['insta_access_token'] != NULL) $array_event['insta_access_token'] = $data['event_array']['insta_access_token'];
        if ($data['event_array']['facebook_page_name'] != NULL) $array_event['facebook_page_name'] = $data['event_array']['facebook_page_name'];
        if ($data['event_array']['allow_msg_keypeople_to_attendee'] != NULL) $array_event['allow_msg_keypeople_to_attendee'] = $data['event_array']['allow_msg_keypeople_to_attendee'];
        if ($data['event_array']['hide_request_meeting'] != NULL) $array_event['hide_request_meeting'] = $data['event_array']['hide_request_meeting'];
        if ($data['event_array']['attendee_hide_request_meeting'] != NULL) $array_event['attendee_hide_request_meeting'] = $data['event_array']['attendee_hide_request_meeting'];
        if (array_key_exists('allow_msg_user_to_exhibitor', $data['event_array'])) $array_event['allow_msg_user_to_exhibitor'] = $data['event_array']['allow_msg_user_to_exhibitor'];
        if ($data['event_array']['key_people_sort_by'] != NULL) $array_event['key_people_sort_by'] = $data['event_array']['key_people_sort_by'];
        if ($data['event_array']['on_pending_agenda'] != NULL) $array_event['on_pending_agenda'] = $data['event_array']['on_pending_agenda'];
        if ($data['event_array']['show_agenda_place_left_column'] != NULL) $array_event['show_agenda_place_left_column'] = $data['event_array']['show_agenda_place_left_column'];
        if ($data['event_array']['show_agenda_speaker_column'] != NULL) $array_event['show_agenda_speaker_column'] = $data['event_array']['show_agenda_speaker_column'];
        if ($data['event_array']['show_agenda_location_column'] != NULL) $array_event['show_agenda_location_column'] = $data['event_array']['show_agenda_location_column'];
        if ($data['event_array']['launch_your_app'] != NULL) $array_event['launch_your_app'] = $data['event_array']['launch_your_app'];
        if ($data['event_array']['islaunchapp'] != NULL) $array_event['islaunchapp'] = $data['event_array']['islaunchapp'];
        if ($data['event_array']['show_attendee_menu'] != NULL) $array_event['show_attendee_menu'] = $data['event_array']['show_attendee_menu'];
        if ($data['event_array']['show_session_by_time'] != NULL) $array_event['show_session_by_time'] = $data['event_array']['show_session_by_time'];
        if (!empty($data['event_array']['Event_time_zone'])) $array_event['Event_time_zone'] = $data['event_array']['Event_time_zone'];
        if (!empty($data['event_array']['Event_show_time_zone'])) $array_event['Event_show_time_zone'] = $data['event_array']['Event_show_time_zone'];
        if ($data['event_array']['Subdomain'] != NULL) $array_event['Subdomain'] = $data['event_array']['Subdomain'];
        if ($data['event_array']['Event_name'] != NULL) $array_event['Event_name'] = $data['event_array']['Event_name'];
        if ($data['event_array']['Status'] != NULL) $array_event['Status'] = $data['event_array']['Status'];
        if ($data['event_array']['Event_type'] != NULL) $array_event['Event_type'] = $data['event_array']['Event_type'];
        if ($data['event_array']['Event_time'] != NULL) $array_event['Event_time'] = $data['event_array']['Event_time'];
        if ($data['event_array']['Event_time_option'] != NULL) $array_event['Event_time_option'] = $data['event_array']['Event_time_option'];
        if ($data['event_array']['Start_date'] != NULL) $array_event['Start_date'] = $data['event_array']['Start_date'];
        /*if ($data['event_array']['Start_time'] != NULL)
        $array_event['Start_time'] = $data['event_array']['Start_time'];*/
        if ($data['event_array']['End_date'] != NULL) $array_event['End_date'] = $data['event_array']['End_date'];
        if ($data['event_array']['Description'] != NULL) $array_event['Description'] = $data['event_array']['Description'];
        if ($data['event_array']['Background_color'] != NULL) $array_event['Background_color'] = $data['event_array']['Background_color'];
        if ($data['event_array']['Top_background_color'] != NULL) $array_event['Top_background_color'] = $data['event_array']['Top_background_color'];
        if ($data['event_array']['Top_text_color'] != NULL) $array_event['Top_text_color'] = $data['event_array']['Top_text_color'];
        if ($data['event_array']['Icon_text_color'] != NULL) $array_event['Icon_text_color'] = $data['event_array']['Icon_text_color'];
        if ($data['event_array']['Footer_background_color'] != NULL) $array_event['Footer_background_color'] = $data['event_array']['Footer_background_color'];
        if ($data['event_array']['Contact_us'] != NULL) $array_event['Contact_us'] = $data['event_array']['Contact_us'];
        if ($data['event_array']['checkbox_values'] != NULL) $array_event['checkbox_values'] = $data['event_array']['checkbox_values'];
        if ($data['event_array']['img_view'] != NULL) $array_event['img_view'] = $data['event_array']['img_view'];
        if ($data['event_array']['stripe_show'] == '1')
        {
            if ($data['event_array']['slide_image'] != "")
            {
                $slideimg = $data['event_array']['slide_image'];
                $simg = explode(",", $slideimg);
                for ($i = 0; $i < count($simg); $i++)
                {
                    $sdata['image'] = "assets/images/slideshow/" . $simg[$i];
                    $sdata['event_id'] = $data['event_array']['Id'];
                    $this->db->insert('slideshow', $sdata);
                }
            }
            if ($data['event_array']['Description'] != "")
            {
                $fundata['content'] = $data['event_array']['Description'];
            }
            if ($data['event_array']['Fundraising_target'] != "")
            {
                $fundata['Fundraising_target'] = $data['event_array']['Fundraising_target'];
            }
            if ($data['event_array']['event_video_link'] != "")
            {
                $fundata['event_video_link'] = $data['event_array']['event_video_link'];
            }
            if ($data['event_array']['target_raisedsofar_display'] != "")
            {
                $fundata['target_raisedsofar_display'] = $data['event_array']['target_raisedsofar_display'];
            }
            if ($data['event_array']['bids_donations_display'] != "")
            {
                $fundata['bids_donations_display'] = $data['event_array']['bids_donations_display'];
            }
            if (!empty($fundata))
            {
                $this->db->where('Event_id', $data['event_array']['Id']);
                $this->db->update("fundraising_setting", $fundata);
            }
            if ($data['event_array']['currency'] != "")
            {
                $this->db->where('event_id', $data['event_array']['Id']);
                $cdata['currency'] = $data['event_array']['currency'];
                $this->db->update('fundraising_currency', $cdata);
            }
        }
        if ($data['event_array']['fun_image'] != null)
        {
            $slideshow['image'] = $data['event_array']['fun_image'];
            $slideshow['event_id'] = $data['event_array']['Id'];
            $this->db->insert('slideshow', $slideshow);
        }
        if (array_key_exists('secure_key', $data['event_array']))
        {
            $array_event['secure_key'] = $data['event_array']['secure_key'];
        }
        if (array_key_exists('authorized_email', $data['event_array']))
        {
            $array_event['authorized_email'] = $data['event_array']['authorized_email'];
        }
        if (array_key_exists('date_format', $data['event_array']))
        {
            $array_event['date_format'] = $data['event_array']['date_format'];
        }
        if (array_key_exists('UUID', $data['event_array']))
        {
            $array_event['UUID'] = $data['event_array']['UUID'];
        }
        if (array_key_exists('only_notify_login_user', $data['event_array']))
        {
            $array_event['only_notify_login_user'] = $data['event_array']['only_notify_login_user'];
        }
        if (array_key_exists('Beacon_feature', $data['event_array']))
        {
            $array_event['Beacon_feature'] = $data['event_array']['Beacon_feature'];
        }
        if (array_key_exists('allow_export_lead_data', $data['event_array']))
        {
            $array_event['allow_export_lead_data'] = $data['event_array']['allow_export_lead_data'];
        }
        if (array_key_exists('allow_custom_survey_lead', $data['event_array']))
        {
            $array_event['allow_custom_survey_lead'] = $data['event_array']['allow_custom_survey_lead'];
        }
        if (array_key_exists('allow_meeting_exibitor_to_attendee', $data['event_array']))
        {
            $array_event['allow_meeting_exibitor_to_attendee'] = $data['event_array']['allow_meeting_exibitor_to_attendee'];
        }
        if (array_key_exists('photo_filter_image', $data['event_array']))
        {
            $array_event['photo_filter_image'] = $data['event_array']['photo_filter_image'];
        }
        if (array_key_exists('allow_show_all_agenda', $data['event_array']))
        {
            $array_event['allow_show_all_agenda'] = $data['event_array']['allow_show_all_agenda'];
        }
        if (array_key_exists('agenda_sort', $data['event_array']))
        {
            $array_event['agenda_sort'] = $data['event_array']['agenda_sort'];
        }
        if (array_key_exists('meeting_time_slot', $data['event_array']))
        {
            $array_event['meeting_time_slot'] = $data['event_array']['meeting_time_slot'];
        }
        if (array_key_exists('meeting_start_time', $data['event_array']))
        {
            $array_event['meeting_start_time'] = $data['event_array']['meeting_start_time'];
        }
        if (array_key_exists('meeting_end_time', $data['event_array']))
        {
            $array_event['meeting_end_time'] = $data['event_array']['meeting_end_time'];
        }
        if (array_key_exists('allow_meeting_attendee_to_speaker', $data['event_array']))
        {
            $array_event['allow_meeting_attendee_to_speaker'] = $data['event_array']['allow_meeting_attendee_to_speaker'];
        }
        if (array_key_exists('show_topbar_exhi', $data['event_array']))
        {
            $array_event['show_topbar_exhi'] = $data['event_array']['show_topbar_exhi'];
        }
        if (array_key_exists('show_login_screen', $data['event_array']))
        {
            $array_event['show_login_screen'] = $data['event_array']['show_login_screen'];
        }
        if (array_key_exists('link_auto_attendee', $data['event_array']))
        {
            $array_event['link_auto_attendee'] = $data['event_array']['link_auto_attendee'];
        }
        if (array_key_exists('max_meeting', $data['event_array']))
        {
            $array_event['max_meeting'] = $data['event_array']['max_meeting'];
        }
        if (array_key_exists('hide_user_identity', $data['event_array']))
        {
            $array_event['hide_user_identity'] = $data['event_array']['hide_user_identity'];
        }
        if (array_key_exists('show_reminder_button', $data['event_array']))
        {
            $array_event['show_reminder_button'] = $data['event_array']['show_reminder_button'];
        }
        if (array_key_exists('enable_block_button', $data['event_array']))
        {
            $array_event['enable_block_button'] = $data['event_array']['enable_block_button'];
        }
        if (array_key_exists('enable_hide_my_identity', $data['event_array']))
        {
            $array_event['enable_hide_my_identity'] = $data['event_array']['enable_hide_my_identity'];
        }
        $this->db->where('Id', $data['event_array']['Id']);
        $this->db->update('event', $array_event);
        // echo $this->db->last_query(); exit();
        // $this->db->update('temp_event', $array_event);
    }
    public function delete_event($id)

    {
        $this->db->where_in('Id', $id);
        $this->db->delete('event');
    }
    public function checksubdomain($subdomain, $id = null)

    {
        $this->db->select('s.Subdomain as ssubdomain');
        $this->db->from('event s');
        if ($Subdomain)
        {
            $this->db->where('s.Subdomain !=', $Subdomain);
        }
        $this->db->where('s.Subdomain', $subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function eventallocation_graph($id = NULL)

    {
        $orid = $this->data['user']->Id;
        $this->db->select("Start_date AS Month,COUNT(DISTINCT Id) AS total_event");
        $this->db->from('event e');
        if ($orid)
        {
            $this->db->where('e.organisor_id', $orid);
        }
        $this->db->group_by('Month');
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_total_menu($subdomain)

    {
        $this->db->select("*");
        $this->db->from('menu m');
        $query = $this->db->get();
        $query_res = $query->result_array();
        $eventmodule = $this->geteventmodulues($subdomain);
        $module = json_decode($eventmodule[0]['module_list']);
        foreach($query_res as $key => $value)
        {
            if (!in_array($value['id'], $module))
            {
                unset($query_res[$key]);
            }
        }
        return $query_res;
    }
    public function get_survey_answer($id = NULL, $Question_id = NULL)

    {
        $Question_id = $this->uri->segment(4);
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->where('p.Question_id', $Question_id);
        $query = $this->db->get();
        $query_res = $query->result_array();
        // if (!$query_res && $Question_id) return redirect('Forbidden');
        return $query_res;
    }
    public function get_survey_users($id)

    {
        $this->db->select("u.Firstname,u.Lastname,u.Email,u.Company_name,u.Id");
        $this->db->from('user u');
        $this->db->join('relation_event_user re', 're.User_id = u.Id');
        $this->db->join('poll_survey ps', 'ps.User_id = u.Id');
        $this->db->where('re.Event_id', $id);
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_survey_answer_export($id = NULL, $Question_id = NULL)

    {
        $this->db->select("u.Firstname,u.Lastname,u.Email,u.Company_name,s.Question,p.answer as panswer,su.json_data");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'right');
        $this->db->join('signup_form_data su', 'su.user_id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->where('p.Question_id', $Question_id);
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_userwise_survey_answer_export($user_id)

    {
        $this->db->select("s.Question,p.Answer");
        $this->db->from('poll_survey p');
        $this->db->join('survey s', 's.Id = p.Question_id', '');
        $this->db->where('p.user_id', $user_id);
        $this->db->group_by('p.Question_id');
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_survey_answer_chart($id = NULL, $Question_id = NULL, $sid)

    {
        $this->db->select("p.answer as panswer,p.User_id as puserid,p.Question_id as pquestion_id,u.*,s.*");
        $this->db->from('poll_survey p');
        $this->db->join('user u', 'u.id = p.User_id', 'left');
        $this->db->join('survey s', 's.Id = p.Question_id', 'left');
        $this->db->join('survey_category_relation scr', 's.Id=scr.Question_id');
        $this->db->where('scr.survey_category_id', $sid);
        $this->db->where('s.Event_id', $id);
        $query = $this->db->get();
        $query_res = $query->result_array();
        return $query_res;
    }
    public function get_event_template_by_id_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id = e.Organisor_id', 'left');
        $this->db->where('e.Id', $id);
        $this->db->where('e.Organisor_id', $orid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function geteventmodulues($event_id)

    {
        $this->db->select('*');
        $this->db->from('event e');
        $this->db->where('e.Id', $event_id);
        $this->db->join('user u', 'u.Id=e.Organisor_id');
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('ou.Email', $res[0]['Email']);
        $this->db1->join('subscription_billing_cycle sbc', 'sbc.id=ou.subscriptiontype');
        $this->db1->join('products p', 'FIND_IN_SET(CAST(p.id AS CHAR),sbc.product_id)');
        $query = $this->db1->get();
        if ($query) $res1 = $query->result_array();
        else $res1 = array();
        $module;
        if (count($res1) > 0)
        {
            for ($i = 0; $i < count($res1); $i++)
            {
                $arr[] = json_decode($res1[$i]['module_list']);
            }
            $output = array();
            foreach($arr as $key => $data)
            {
                $output = array_merge($output, $data);
            }
            $uarray = array_keys(array_flip($output));
            $module = json_encode($uarray);
            // echo $module;die;
        }
        $res1[0]['module_list'] = $module;
        return $res1;
        /*$this->db->select('*');
        $this->db->from('event e');
        $this->db->join('user u','u.Id=e.Organisor_id');
        $this->db->join('amber_ghtar.organizer_user ou','ou.Email=u.Email');
        $this->db->join('amber_ghtar.subscription_billing_cycle sbc','sbc.id=ou.subscriptiontype');
        $this->db->join('amber_ghtar.products p','FIND_IN_SET(CAST(p.id AS CHAR),sbc.product_id)');
        $this->db->where('e.Id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        echo "<pre>";
        print_r($res);
        exit();
        return $res;*/
    }
    public function geteventmenu($eventid, $menu_id = null, $is_feture = null, $isvisible = "1")

    {
        $this->db->select('m.id,m.pagetitle,m.type,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,em.is_feture_product,em.show_in_front', FALSE);
        $this->db->from('menu m');
        $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id="' . $eventid . '"', 'left');
        if ($isvisible == "1")
        {
            $this->db->where('m.isvisible', '1');
        }
        if ($menu_id != null)
        {
            $this->db->where('m.id', $menu_id);
        }
        if ($is_feture != null)
        {
            $this->db->where('em.is_feture_product', '1');
        }
        $this->db->order_by("m.type");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function geteventcmsmenu($eventid, $cms_id = null, $is_feture = null, $acc_name = null)

    {
        $user = $this->session->userdata('current_user');
        if ($user[0]->Id != "" && $user[0]->Rid == '4')
        {
            $this->db->select('*')->from('event_attendee ea');
            $this->db->join('user_views uv', 'uv.view_id=ea.views_id', 'right');
            $this->db->where('ea.Event_id', $eventid);
            $this->db->where('ea.Attendee_id', $user[0]->Id);
            $vqu = $this->db->get();
            $vres = $vqu->result_array();
            if (!empty($vres[0]['view_modules']))
            {
                $activemodules = explode(",", $vres[0]['view_modules']);
            }
            if ($vres[0]['view_type'] == '1' && empty($vres[0]['view_modules']))
            {
                $activemodules = array(
                    '21'
                );
            }
        }
        else
        {
            $activemodules = array(
                '21'
            );
        }
        $this->db->select('s.Subdomain as ssubdomain,s.checkbox_values');
        $this->db->from('event s');
        $this->db->where('s.Id', $eventid);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        if (empty($acc_name))
        {
            $acc_name = $this->session->userdata('acc_name');
        }
        $this->db->select('c.Id,c.Menu_name,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,em.title as menuname,CONCAT("Cms/' . $acc_name . '/' . $res1[0]['ssubdomain'] . '/View/",c.Id) as menuurl,em.is_feture_product', FALSE);
        $this->db->from('cms c');
        $this->db->join('event_menu em', 'em.cms_id=c.Id and em.event_id="' . $eventid . '"', 'left');
        $this->db->where('c.show_in_app', '1');
        if ($cms_id != null)
        {
            $this->db->where('c.Id', $cms_id);
            $this->db->where('c.Event_id', $eventid);
        }
        if ($is_feture != null)
        {
            $this->db->where('em.is_feture_product', '1');
        }
        $query = $this->db->get();
        $res = $query->result();
        if (!$res && $cms_id != null) return redirect('Forbidden');
        if (in_array('21', $activemodules) && in_array('21', array_filter(explode(",", $res1[0]['checkbox_values']))))
        {
            return $res;
        }
        else
        {
            return array();
        }
    }
    public function geteventcmsmenu_fecture($eventid, $cms_id = null, $is_feture = null)

    {
        $this->db->select('c.Id,c.Menu_name,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,c.Menu_name as menuname,CONCAT("Cms/demo/View/",c.Id) as menuurl,em.is_feture_product', FALSE);
        $this->db->from('cms c');
        $this->db->join('event_menu em', 'em.cms_id=c.Id and em.event_id="' . $eventid . '"', 'left');
        if ($cms_id != null)
        {
            $this->db->where('c.Id !=', $cms_id);
        }
        if ($is_feture != null)
        {
            $this->db->where('em.is_feture_product', '1');
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function geteventmenu_rolemanagement($eventid, $menu_id = null, $is_feture = null, $isvisible = "1")

    {
        $this->db->select('m.id,m.pagetitle,em.img,em.img_view, CASE WHEN em.is_feture_product IS NULL THEN "0" ELSE em.is_feture_product END as is_feture_products,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname,m.menuurl,em.is_feture_product', FALSE);
        $this->db->from('menu m');
        $this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id="' . $eventid . '"', 'left');
        if ($menu_id != null)
        {
            $this->db->where('m.id', $menu_id);
        }
        if ($is_feture != null)
        {
            $this->db->where('em.is_feture_product', '1');
        }
        $ignore = array(
            18,
            19,
            22,
            23,
            24,
            25
        );
        $this->db->where_not_in('m.id', $ignore);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function eventaddrelation($eventid, $userid, $organizerid, $roleid)

    {
        $array_relation_user_attendee['Event_id'] = $eventid;
        $array_relation_user_attendee['User_id'] = $userid;
        $array_relation_user_attendee['Organisor_id'] = $organizerid;
        $array_relation_user_attendee['Role_id'] = $roleid;
        $this->db->insert('relation_event_user', $array_relation_user_attendee);
    }
    public function get_user_latest_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id', 'left');
        $this->db->where('u.Email', $user[0]->Email);
        $this->db->where('ru.User_id', $user[0]->Id);
        $this->db->where("e.Start_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->where("e.End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->group_by('e.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Status'] = $res[$i]->Status;
            $event_res[$i]['Event_type'] = $res[$i]->Event_type;
            $event_res[$i]['Subdomain'] = $res[$i]->Subdomain;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Start_date'] = $res[$i]->Start_date;
            // $event_res[$i]['Start_time'] = $res[$i]->Start_time;
            $event_res[$i]['End_date'] = $res[$i]->End_date;
            $event_res[$i]['Organisor_id'] = $res[$i]->Organisor_id;
            $event_res[$i]['Background_color'] = $res[$i]->Background_color;
            $event_res[$i]['Top_background_color'] = $res[$i]->Top_background_color;
            $event_res[$i]['Top_text_color'] = $res[$i]->Top_text_color;
            $event_res[$i]['Icon_text_color'] = $res[$i]->Icon_text_color;
            $event_res[$i]['Footer_background_color'] = $res[$i]->Footer_background_color;
            $event_res[$i]['Contact_us'] = $res[$i]->Contact_us;
            $event_res[$i]['Checkbox_values'] = $res[$i]->checkbox_values;
        }
        return $event_res;
    }
    public function get_total_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function get_total_public_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.event_type = '1'");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function get_total_private_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.event_type = '0'");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function get_total_active_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.status = '1'");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function get_total_inactive_event_list($id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('e.*');
        $this->db->from('event e');
        $this->db->join('user u', 'u.id=e.Organisor_id', 'left');
        if ($user[0]->Id)
        {
            $this->db->where('e.Organisor_id', $user[0]->Id);
        }
        $this->db->where("e.status = '0'");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function getUserRole($eventid)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('ru.Role_id,r.Name');
        $this->db->from('relation_event_user ru');
        $this->db->join('role r', 'r.Id=ru.Role_id');
        $this->db->where('ru.User_id', $user[0]->Id);
        $this->db->where('(ru.Event_id=' . $eventid, NULL, FALSE);
        $this->db->or_where('(ru.Event_id is NULL and ru.Role_id=3))', NULL, FALSE);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function getEventUser($intEventId)

    {
        $this->db->select('usr.*');
        $this->db->from('user usr');
        $this->db->join("relation_event_user reu", "reu.User_id=usr.Id");
        $this->db->where("reu.Event_id", $intEventId);
        $this->db->where("reu.Role_id != ", 3);
        $this->db->where("reu.Role_id != ", 5);
        $this->db->group_by('usr.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function front_email_template($slug = NULL, $intEventId = NULL)

    {
        $this->db->select('*');
        $this->db->where('event_id', $intEventId);
        if ($slug != '')
        {
            $this->db->where('Slug', $slug);
        }
        $this->db->from('event_email_templates');
        $et_query = $this->db->get();
        $et_query_res = $et_query->result_array();
        return $et_query_res;
    }
    public function setwinnerproduct()

    {
        $this->db->select('product_id');
        $this->db->from('product_user_bids');
        $this->db->where('win_status', '1');
        $this->db->group_by('product_id');
        $qry1 = $this->db->get();
        $res1 = $qry1->result_array();
        for ($i = 0; $i < count($res1); $i++)
        {
            $proid[$i] = $res1[$i]['product_id'];
        }
        /*$this->db->select('p.name,u.Firstname,u.Lastname,p.product_id,max(pub.bid_amt) as max_bid,pub.user_id,p.auctionType,p.auctionStartDateTime,p.auctionEndDateTime,pub.win_status,p.event_id,u.Email,u.Id ruid');
        $this->db->from('product p');
        $this->db->join('product_user_bids pub','pub.product_id=p.product_id');
        $this->db->join('user u','u.Id=pub.user_id');
        $this->db->where('p.auctionType',1);
        $this->db->where('p.auctionEndDateTime <',date("Y-m-d H:i:s"));
        $this->db->where_not_in('p.product_id',$proid);
        $this->db->group_by('p.product_id');
        $this->db->order_by('pub.id','DESC');
        $qry2=$this->db->get();
        $res2=$qry2->result_array();*/
        $this->db->select('p.name,u.Firstname,u.Lastname,p.product_id,max(pub.bid_amt) as max_bid,pub.user_id,p.auctionType,p.auctionStartDateTime,p.auctionEndDateTime,pub.win_status,p.event_id,u.Email,u.Id ruid,u.gcm_id,u.device,pub.event_id');
        $this->db->from('product p');
        $this->db->join('product_user_bids pub', 'pub.product_id=p.product_id');
        $this->db->join('user u', 'u.Id=pub.user_id');
        $this->db->where('p.auctionType', 2);
        $this->db->where('p.reserveBidVisible', '0');
        $this->db->where_not_in('p.product_id', $proid);
        $this->db->group_by('p.product_id');
        $this->db->order_by('pub.id', 'DESC');
        $qry3 = $this->db->get();
        $res = $qry3->result_array();
        // $res=array_merge($res2,$res3);
        $this->load->library('email');
        $slug = "Congratulations - you won the auction!";
        for ($i = 0; $i < count($res); $i++)
        {
            $this->db->select('pub.user_id,u.Firstname,u.Lastname,u.Email')->from('product_user_bids pub');
            $this->db->where('product_id', $res[$i]['product_id']);
            $this->db->where('bid_amt', $res[$i]['max_bid']);
            $this->db->join('user u', 'u.Id=pub.user_id');
            $qyu = $this->db->get();
            $resu = $qyu->result_array();
            $uid = $resu[0]['user_id'];
            if ($res[$i]['product_id'] != "" && $res[$i]['user_id'] != "" && $res[$i]['max_bid'] != "")
            {
                $this->db->where('product_id', $res[$i]['product_id']);
                $this->db->where('user_id', $uid);
                $this->db->where('bid_amt', $res[$i]['max_bid']);
                $this->db->update('product_user_bids', array(
                    'win_status' => '1'
                ));
            }
            if ($this->db->affected_rows())
            {
                $this->db->select('*');
                $this->db->from('product');
                $this->db->where('product_id', $res[$i]['product_id']);
                $query = $this->db->get();
                $result = $query->result_array();
                if ($result[0]['auctionType'] == '1')
                {
                    $data1['product_type'] = '1';
                }
                else
                {
                    $data1['product_type'] = '2';
                }
                $data1['time'] = date('Y-m-d H:i:s');
                $data1['product_id'] = $res[$i]['product_id'];
                $data1['quantity'] = '1';
                $data1['user_id'] = $uid;
                $data1['price'] = $res[$i]['max_bid'];
                $this->db->insert('cart', $data1);
                $em_template = $this->front_email_template($slug, $res[$i]['event_id']);
                $msg = $em_template[0]['Content'];
                $patterns = array();
                $patterns[0] = '/{{firstname}}/';
                $patterns[1] = '/{{itemname}}/';
                $patterns[2] = '/{{bidamount}}/';
                $patterns[3] = '/{{administrator}}/';
                $patterns[4] = '/{{companyname}}/';
                $replacements = array();
                $name = $resu[0]['Firstname'] . "" . $resu[0]['Lastname'];
                $replacements[0] = $name;
                $replacements[1] = $res[$i]['name'];
                $replacements[2] = $res[$i]['max_bid'];
                $replacements[3] = 'Administrator';
                $replacements[4] = 'Allintheloop';
                $msg = preg_replace($patterns, $replacements, $msg);
                $this->email->from($em_template[0]['From'], 'All In The Loop');
                $this->email->to($resu[0]['Email']);
                $this->email->subject($em_template[0]['Subject']);
                $this->email->message(html_entity_decode($msg));
                $this->email->send();
                $msg = "";
            }
        }
        return $res;
    }
    public function savestripe_data($data)

    {
        $this->db->select('*')->from('stripe_payment_info');
        $this->db->where('user_id', $data['user_id']);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $this->db->where('user_id', $data['user_id']);
            unset($data['user_id']);
            $this->db->update('stripe_payment_info', $data);
        }
        else
        {
            $this->db->insert('stripe_payment_info', $data);
        }
    }
    public function save_stripe_data($data)

    {
        $this->db->select('*')->from('stripe_settings');
        $this->db->where('organisor_id', $data['organisor_id']);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $this->db->where('organisor_id', $data['organisor_id']);
            unset($data['organisor_id']);
            $this->db->update('stripe_settings', $data);
        }
        else
        {
            $this->db->insert('stripe_settings', $data);
        }
    }
    public function get_subscription_type($email)

    {
        $this->db1->select('*')->from('organizer_user');
        $this->db1->where('Email', $email);
        $qu = $this->db1->get();
        $res = $qu->result_array();
        return $res;
    }
    public function check_first_event_by_organisor($oid)

    {
        $this->db->select('*')->from('event');
        $this->db->where('Organisor_id', $oid);
        $equ = $this->db->get();
        $eres = $equ->result_array();
        return $eres;
    }
    public function get_all_fun_setting($eid)

    {
        $this->db->select('*')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $fqu = $this->db->get();
        $fs = $fqu->result_array();
        $res['fundraising_setting'] = $fs[0];
        $this->db->select('*')->from('slideshow');
        $this->db->where('event_id', $eid);
        $squ = $this->db->get();
        $res['slideshow'] = $squ->result_array();
        $this->db->select('*')->from('fundraising_currency');
        $this->db->where('event_id', $eid);
        $cqu = $this->db->get();
        $cd = $cqu->result_array();
        $res['fundraising_currency'] = $cd[0];
        // echo "<pre>";print_r($res);die;
        return $res;
    }
    public function delete_slide($ID)

    {
        $result = $this->db->get_where("slideshow", array(
            'id' => $ID
        ))->result();
        unlink("./fundraising/" . $result[0]->image);
        $this->db->delete("slideshow", array(
            'id' => $ID
        ));
    }
    public function get_stripe_show($oeid)

    {
        $this->db1->select('*')->from('organizer_user');
        $this->db1->where('Email', $oeid);
        $oqu = $this->db1->get();
        $res = $oqu->result_array();
        return $res;
    }
    public function get_role_by_user_and_event($roleid, $event)

    {
        $this->db->select('*')->from('role_permission r');
        $this->db->where('r.Role_id', $roleid);
        $this->db->where('r.Event_id', $event);
        $this->db->where('r.Menu_id', '20');
        $equ = $this->db->get();
        $res = $equ->result_array();
        if (count($res) > 0)
        {
            return '1';
        }
        else
        {
            return '0';
        }
    }
    public function addhubevent($data)

    {
        $this->db->select('*')->from('hub_event');
        $this->db->where('event_id', $data['event_id']);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            $this->db->where('event_id', $data['event_id']);
            unset($data['event_id']);
            $this->db->update('hub_event', $data);
        }
        else
        {
            $this->db->insert('hub_event', $data);
        }
    }
    public function addhubsetting($hub)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*')->from('hub_setting');
        $this->db->where('organizer_id', $user[0]->Id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $this->db->where('organizer_id', $user[0]->Id);
            $this->db->update('hub_setting', $hub);
        }
        else
        {
            $hub['organizer_id'] = $user[0]->Id;
            $this->db->insert('hub_setting', $hub);
        }
    }
    public function get_hub_event($org_id = null)

    {
        if (empty($org_id))
        {
            $user = $this->session->userdata('current_user');
            $org_id = $user[0]->Organisor_id;
        }
        $this->db->select('*')->from('hub_setting');
        $this->db->where('organizer_id', $org_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_hub_event_menu_setting($id)

    {
        $this->db->select('he.Id as menusettingid,CASE WHEN he.title IS NULL THEN e.Event_name ELSE he.title END as title,CASE WHEN he.is_feture_product IS NULL THEN "0" ELSE he.is_feture_product END as is_feture_products,he.hub_home_tab_image,he.img_view,he.show_photos,he.show_publicmsg,he.show_attendee,e.*', false)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->where('he.event_id', $id);
        $hquery = $this->db->get();
        $hres = $hquery->result_array();
        return $hres;
    }
    public function get_hub_event_menu_setting_organizerid($org_id = null)

    {
        $this->db->select('e.Id as eventid,e.Subdomain,he.Id as menusettingid,CASE WHEN he.title IS NULL THEN e.Event_name ELSE he.title END as title,CASE WHEN he.is_feture_product IS NULL THEN "0" ELSE he.is_feture_product END as is_feture_products,he.hub_home_tab_image,he.img_view,e.*,u.acc_name,u.Id', false)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->join('user u', 'u.Id=e.Organisor_id');
        if (empty($org_id))
        {
            $user = $this->session->userdata('current_user');
            $org_id = $user[0]->Organisor_id;
        }
        $this->db->where('e.Organisor_id', $org_id);
        $this->db->where('he.is_feture_product', '1');
        $hquery = $this->db->get();
        $hres = $hquery->result_array();
        return $hres;
    }
    public function deletefromhub($heid)

    {
        $this->db->delete('hub_event', array(
            'Id' => $heid
        ));
    }
    public function get_hub_active_by_organizer($oid)

    {
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('ou.Email', $this->getOrgEmail($oid));
        $haquery = $this->db1->get();
        $hadata = $haquery->result_array();
        return $hadata[0]['hub_active'];
    }
    public function get_attendee_by_hub_event($org_id)

    {
        $this->db->select('e.Id as eventid,e.Id as eventid,e.Subdomain,he.Id as hub_event_id,u.Id as Id,u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('hub_event he', 'he.event_id=e.Id', 'right');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Organisor_id', $org_id);
        $where = "FIND_IN_SET(2,`checkbox_values`) > 0";
        $this->db->where($where);
        $this->db->where('he.show_attendee', '1');
        $this->db->where('ru.Role_id', 4);
        $this->db->where('r.Id', 4);
        $this->db->order_by('u.Lastname');
        $qu = $this->db->get();
        $res = $qu->result();
        $attendees = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $prev = "";
            $fc = strtoupper($res[$i]->Lastname);
            $al = substr($fc, 0, 1);
            if ($prev == "" || $res[$i]->Lastname != $prev)
            {
                $prev = $res[$i]->Start_date;
                $attendees[$al][$i]['Id'] = $res[$i]->Id;
                $attendees[$al][$i]['Firstname'] = $res[$i]->Firstname;
                $attendees[$al][$i]['Lastname'] = $res[$i]->Lastname;
                $attendees[$al][$i]['Company_name'] = $res[$i]->Company_name;
                $attendees[$al][$i]['Title'] = $res[$i]->Title;
                $attendees[$al][$i]['Email'] = $res[$i]->Email;
                $attendees[$al][$i]['Logo'] = $res[$i]->Logo;
                $attendees[$al][$i]['Website_url'] = $res[$i]->Website_url;
                $attendees[$al][$i]['Facebook_url'] = $res[$i]->Facebook_url;
                $attendees[$al][$i]['Twitter_url'] = $res[$i]->Twitter_url;
                $attendees[$al][$i]['Linkedin_url'] = $res[$i]->Linkedin_url;
            }
        }
        return $attendees;
    }
    public function get_attendee_by_hub_event_by_user_id($org_id, $uid)

    {
        $this->db->select('e.Id as eventid,e.Subdomain,he.Id as hub_event_id,u.Id as Id,u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('hub_event he', 'he.event_id=e.Id', 'right');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Organisor_id', $org_id);
        $this->db->where('u.Id', $uid);
        $this->db->where('ru.Role_id', 4);
        $this->db->where('r.Id', 4);
        $this->db->order_by('u.Lastname');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_user_by_hub_event_by_user_id($org_id, $uid)

    {
        $this->db->select('e.Id as eventid,e.Subdomain,he.Id as hub_event_id,u.Id as Id,u.*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('event e');
        $this->db->join('hub_event he', 'he.event_id=e.Id', 'right');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Organisor_id', $org_id);
        $this->db->where('u.Id', $uid);
        $this->db->order_by('u.Lastname');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_organizer_by_acc_name($acc_name)

    {
        $this->db->select('*')->from('user u');
        $this->db->where('u.acc_name', $acc_name);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function event_in_hub_by_event_id($eid)

    {
        $this->db->select('*')->from('hub_event');
        $this->db->where('event_id', $eid);
        $this->db->where('is_feture_product', '1');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_all_time_zone()

    {
        $this->db->select('*')->from('timezone');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_timezone_desc_and_type($offset)

    {
        $this->db->select('*')->from('timezone');
        $this->db->where('GMT', $offset);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_gmt_offset_short_name($name)

    {
        $this->db->select('*')->from('timezone');
        $this->db->where('Name', $name);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_queue_mail_list_by_event($eid)

    {
        $this->db->select('*')->from('queue_mail');
        $this->db->where('event_id', $eid);
        $this->db->order_by("queue_id", "asc");
        $this->db->limit(30);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_queue_mail_event_id()

    {
        $this->db->select('event_id')->from('queue_mail');
        $this->db->group_by('event_id');
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function delete_in_queue_mail_list($qid)

    {
        $this->db->where('queue_id', $qid);
        $this->db->delete('queue_mail');
    }
    public function category($id)

    {
        $this->db->select('pc.product_category_id,pc.category,b.*')->from('product_category pc');
        $this->db->where('pc.event_id', $id);
        $this->db->join("banner b", "b.category_id=pc.product_category_id", "left");
        $this->db->order_by("pc.product_category_id", 'asc');
        $this->db->group_by("pc.product_category_id");
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function getAccname_event_id($id = '')

    {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu', 'reu.User_id = tu.Id', 'left');
        $this->db->where("reu.Event_id", $id);
        $this->db->where("reu.Role_id", 3);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function update_survey($id, $data)

    {
        $update_data['hide_survey'] = $data;
        $this->db->where('Id', $id);
        $this->db->update('event', $update_data);
    }
    public function update_admin_event_checkbox_values($eid, $checkbox)

    {
        $this->db->where('Id', $eid);
        $this->db->update('event', $checkbox);
    }
    public function add_left_hand_menu_title($event_menu)

    {
        $this->db->select('*')->from('event_menu');
        $this->db->where('menu_id', $event_menu['menu_id']);
        $this->db->where('event_id', $event_menu['event_id']);
        $que = $this->db->get();
        $res = $que->result_array();
        if (count($res) > 0)
        {
            $this->db->where('menu_id', $event_menu['menu_id']);
            $this->db->where('event_id', $event_menu['event_id']);
            unset($event_menu['menu_id']);
            unset($event_menu['event_id']);
            $this->db->update('event_menu', $event_menu);
        }
        else
        {
            $this->db->insert('event_menu', $event_menu);
        }
    }
    public function get_all_advertising_count($eid)

    {
        $this->db->select('count(*) as totaladverts')->from('advertising');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totaladverts'];
    }
    public function get_all_maps_count($eid)

    {
        $this->db->select('count(*) as totalmap')->from('map');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalmap'];
    }
    public function get_all_social_link_count($eid)

    {
        $this->db->select('count(*) as totallink')->from('social');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totallink'];
    }
    public function get_all_presentation_count($eid)

    {
        $this->db->select('count(*) as totalpresentation')->from('presentation');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalpresentation'];
    }
    public function get_all_documents_count($eid)

    {
        $this->db->select('count(d.id) as totaldocument');
        $this->db->from('documents d');
        $this->db->join('documents d1', 'd.parent=d1.id', 'left');
        $this->db->where('d.Event_id', $eid);
        $this->db->where('d.doc_type', '1');
        $this->db->order_by('d.id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totaldocument'];
    }
    public function get_all_notes_count($eid)

    {
        $this->db->select('count(*) as totalnotes')->from('notes');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalnotes'];
    }
    public function get_all_private_messages_count($eid)

    {
        $this->db->select('count(*) as totalprivatemsg')->from('speaker_msg');
        $this->db->where('Event_id', $eid);
        $this->db->where('parent', '');
        $this->db->where('ispublic', '0');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalprivatemsg'];
    }
    public function get_all_photos_count($eid)

    {
        $this->db->select('count(*) as totalphotos')->from('feed_img ');
        $this->db->where('Event_id', $eid);
        $this->db->where('parent', '');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalphotos'];
    }
    public function get_all_public_messages_count($eid)

    {
        $this->db->select('count(*) as totalpublicmsg')->from('speaker_msg');
        $this->db->where('Event_id', $eid);
        $this->db->where('parent', '');
        $this->db->where('ispublic', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalpublicmsg'];
    }
    public function get_all_surveys_count($eid)

    {
        $this->db->select('count(*) as totalsurvey')->from('survey');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalsurvey'];
    }
    public function get_all_twitter_hashtags_count($eid)

    {
        $this->db->select('count(*) as totalhashtags')->from('event_hashtags');
        $this->db->where('event_id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res[0]['totalhashtags'];
    }
    public function get_all_form_builder_count($eid)

    {
        $this->db->select('count(*) as totalforms')->from('forms');
        $this->db->where('event_id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res[0]['totalforms'];
    }
    public function get_all_agenda_category_count($eid)

    {
        $this->db->select('count(*) as totalcat')->from('agenda_categories');
        $this->db->where('event_id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res[0]['totalcat'];
    }
    public function get_all_exhibitors_count($eid)

    {
        $this->db->select('count(user.Id) as totalexhibitors');
        $this->db->from('user');
        $this->db->join('relation_event_user', 'user.Id = relation_event_user.User_id');
        $this->db->join('role r', 'relation_event_user.Role_id = r.Id');
        $this->db->join('exibitor ex', 'ex.user_id = user.Id', 'left');
        $this->db->where('relation_event_user.Event_id', $eid);
        $this->db->where('r.Id', 6);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalexhibitors'];
    }
    public function get_all_sponsors_count($eid)

    {
        $this->db->select('count(e.Id) as totalsponse');
        $this->db->from('sponsors e');
        $this->db->join('user u', 'e.user_id=u.id', 'left');
        $this->db->where('e.Event_id', $eid);
        $qry = $this->db->get();
        $res = $qry->result_array();
        return $res[0]['totalsponse'];
    }
    public function get_all_attendee_count($eid)

    {
        $orid = $this->data['user']->Id;
        $this->db->select("count(DISTINCT(u.Id)) as totalattende", false);
        $this->db->from('user u');
        $this->db->join('event_attendee ea', 'ea.Attendee_id=u.Id and ea.Event_id=' . $eid, 'left');
        $this->db->join('user_views uv', 'ea.views_id=uv.view_id', 'left');
        $this->db->join('attendee_agenda_relation aar', 'aar.attendee_id=u.Id and aar.event_id=' . $eid, 'left');
        $this->db->join('agenda_categories ac', 'ac.Id=aar.agenda_category_id', 'left');
        $this->db->join('signup_form_data sfd', 'sfd.user_id=u.Id', 'left');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('attendee_notes an', 'an.User_id=u.Id', 'left');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if (!empty($eid))
        {
            $this->db->where('ru.Event_id', $eid);
        }
        $this->db->where('r.Id', 4);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalattende'];
    }
    public function get_all_speakers_count($eid)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('count(u.Id) as totalspeaker');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id', 7);
        if ($eid)
        {
            $this->db->where('ru.Event_id', $eid);
        }
        $this->db->where('r.Id !=', 3);
        $this->db->where('r.Id !=', 4);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['totalspeaker'];
    }
    public function SaveAppdata($eventdata, $eid)

    {
        $this->db->where('Id', $eid);
        $this->db->update('event', $eventdata);
    }
    public function get_home_screen_tabs($id)

    {
        $this->db->select('*')->from('event_menu');
        $this->db->where('event_id', $id);
        $this->db->where('menu_id !=', '');
        $query = $this->db->get();
        $res = $query->result_array();
        $customarray = array();
        foreach($res as $key => $value)
        {
            $ckey = $value['menu_id'];
            $customarray[$ckey] = $value;
        }
        return $customarray;
    }
    public function update_home_screen_tab_modules($event_menu)

    {
        $this->db->select('*')->from('event_menu');
        $this->db->where('menu_id', $event_menu['menu_id']);
        $this->db->where('event_id', $event_menu['event_id']);
        $que = $this->db->get();
        $res = $que->result_array();
        if (count($res) > 0)
        {
            $menu_data['is_feture_product'] = $event_menu['is_feture_product'];
            $this->db->where('menu_id', $event_menu['menu_id']);
            $this->db->where('event_id', $event_menu['event_id']);
            $this->db->update('event_menu', $menu_data);
        }
        else
        {
            $this->db->insert('event_menu', $event_menu);
        }
    }
    public function get_menu_data_by_menu_id($menu_id)

    {
        $this->db->select('*')->from('menu');
        $this->db->where('id', $menu_id);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_event_data_by_event_id($eid)

    {
        $this->db->select('*')->from('event');
        $this->db->where('Id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res[0];
    }
    public function get_all_advertising_data($eid)

    {
        $this->db->select('Organisor_id,Event_id,H_images,Google_header_adsense,Google_footer_adsense,F_images,Advertisement_name,Header_link,Footer_link,Menu_id,Cms_id')->from('advertising');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_map_data($eid)

    {
        $this->db->select('Map_title,Event_id,Map_desc,satellite_view,place,lat_long,zoom_level,Address,Images,Organizer_id,include_map,area,map_u_id')->from('map');
        $this->db->where('Event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_and_save_all_document_data($old_eid, $new_eid)

    {
        $parent_arr = array();
        $this->db->select('*')->from('documents');
        $this->db->where('Event_id', $old_eid);
        $this->db->order_by('parent');
        $que = $this->db->get();
        $res = $que->result_array();
        foreach($res as $key => $value)
        {
            $doc_data = $value;
            $doc_data['Event_id'] = $new_eid;
            $old_document_id = $doc_data['id'];
            unset($doc_data['id']);
            if (!empty($doc_data['parent']))
            {
                $doc_data['parent'] = $parent_arr[$doc_data['parent']];
            }
            $this->db->insert('documents', $doc_data);
            $parent_arr[$old_document_id] = $this->db->insert_id();
            $new_document_id = $this->db->insert_id();
            $this->db->select('*')->from('document_files');
            $this->db->where('document_id', $value['id']);
            $dque = $this->db->get();
            $dres = $dque->result_array();
            foreach($dres as $akey => $avalue)
            {
                $df_data = $avalue;
                unset($df_data['id']);
                $df_data['document_id'] = $new_document_id;
                $this->db->insert('document_files', $df_data);
            }
        }
        return $res;
    }
    public function get_all_social_data($eid)

    {
        $this->db->select('facebook_url,twitter_url,instagram_url,linkedin_url,pinterest_url,youtube_url,Event_id,Organisor_id')->from('social');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_cms_data($eid)

    {
        $this->db->select('Organisor_id,Event_id,Description,Created_date,Menu_name,Images,Logo_images,Img_view,Background_color,fecture_module,Status,show_in_hub')->from('cms');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_twitter_feed_data($eid)

    {
        $this->db->select('hashtags,event_id,status,date')->from('event_hashtags');
        $this->db->where('event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_relation_data($eid, $rid)

    {
        $this->db->select('Event_id,User_id,Organisor_id,Role_id')->from('relation_event_user');
        $this->db->where('Event_id', $eid);
        $this->db->where('Role_id', $rid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_event_attendee_data($eid)

    {
        $this->db->select('Event_id,Attendee_id,extra_column,views_id')->from('event_attendee');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_default_view_for_attendee($eid)

    {
        $this->db->select('*')->from('user_views');
        $this->db->where('event_id', $eid);
        $this->db->where('view_type', '1');
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_custom_column_data($eid)

    {
        $this->db->select('column_name,event_id,crequire')->from('custom_column');
        $this->db->where('event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_attendee_notes_data($eid)

    {
        $this->db->select('Heading,Description,Event_id,User_id,Created_at')->from('attendee_notes');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_attendee_share_contact($eid)

    {
        $this->db->select('event_id,from_id,to_id,approval_status')->from('attendee_share_contact');
        $this->db->where('event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_attendee_invitation_data($eid)

    {
        $this->db->select('firstname,lastname,Emailid,Event_id,Status,role_status,link_code,Company_name,Title,isUsedLink,extra_column')->from('attendee_invitation');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_attendee_temp_invitation_data($eid)

    {
        $this->db->select('firstname,lastname,Emailid,Event_id,Company_name,Title,extra_column,skip_to_invite')->from('temp_attendee_invitation');
        $this->db->where('Event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_exibitor_data($eid, $et_id = NULL)

    {
        $this->db->select('Organisor_id,Event_id,Heading,main_contact_name,Short_desc,Description,website_url,facebook_url,twitter_url,linkedin_url,phone_number1,HQ_phone_number,email_address,main_email_address,company_logo,Images,user_id,stand_number,et_id')->from('exibitor');
        $this->db->where('Event_id', $eid);
        if (empty($et_id))
        {
            $this->db->where('et_id IS NULL');
        }
        else
        {
            $this->db->where('et_id', $et_id);
        }
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_exibitor_type_data_by_event($eid)

    {
        $this->db->select('*')->from('exhibitor_type');
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function add_exibitor_type_data($etdata, $nid)

    {
        unset($etdata['type_id']);
        $etdata['event_id'] = $nid;
        $etdata['created_date'] = date('Y-m-d H:i:s');
        $etdata['type_ucode'] = substr(uniqid() , 0, 5);
        $this->db->insert('exhibitor_type', $etdata);
        return $this->db->insert_id();
    }
    public function get_all_sponsors_data($eid, $st_id = NULL)

    {
        $this->db->select('Organisor_id,Event_id,Sponsors_name,Company_name,Description,website_url,facebook_url,twitter_url,linkedin_url,company_logo,Images,st_id')->from('sponsors');
        $this->db->where('Event_id', $eid);
        if (empty($st_id))
        {
            $this->db->where('st_id IS NULL');
        }
        else
        {
            $this->db->where('st_id', $st_id);
        }
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_sponsors_type_data($eid)

    {
        $this->db->select('*')->from('sponsors_type');
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function add_sponsors_type_data($stdata, $nid)

    {
        unset($stdata['type_id']);
        $stdata['event_id'] = $nid;
        $stdata['created_date'] = date('Y-m-d H:i:s');
        $stdata['type_ucode'] = substr(uniqid() , 0, 5);
        $this->db->insert('sponsors_type', $stdata);
        return $this->db->insert_id();
    }
    public function get_all_fun_setting_data($old_eid, $new_eid)

    {
        $this->db->select('Fundraising_target,bg_color,font_color,content,Event_id,raisedsetting,raised_display,raised_price,checkout_content,email_display,pushnoti_display,facebook_login,linkedin_login_enabled,signup,updatedate,email,fundraising_enbled,target_raisedsofar_display,event_video_link,bids_donations_display,product_color,instant_donate_enbled,instant_donate_backgroundcolor,instant_donate_color,instant_donate_name,fun_donation_first,fun_donation_second,format_time,fundraising_donation_enbled,fundraising_donate_backgroundcolor,fundraising_donate_color,fundraising_donate_name')->from('fundraising_setting');
        $this->db->where('Event_id', $old_eid);
        $que = $this->db->get();
        $res = $que->result_array();
        if (count($res) > 0)
        {
            $this->db->select('*')->from('fundraising_setting');
            $this->db->where('Event_id', $new_eid);
            $newque = $this->db->get();
            $newres = $newque->result_array();
            $fun_data = $res[0];
            if (count($newres) > 0)
            {
                unset($fun_data['Event_id']);
                $this->db->where('Event_id', $new_eid);
                $this->db->update('fundraising_setting', $fun_data);
            }
            else
            {
                $fun_data['Event_id'] = $new_eid;
                $this->db->insert('fundraising_setting', $fun_data);
            }
        }
        return $res;
    }
    public function get_all_fun_slideshow_data($eid)

    {
        $this->db->select('image,event_id')->from('slideshow');
        $this->db->where('event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_fun_currency_data($old_eid, $new_eid)

    {
        $this->db->select('currency,event_id')->from('fundraising_currency');
        $this->db->where('event_id', $old_eid);
        $que = $this->db->get();
        $res = $que->result_array();
        if (count($res) > 0)
        {
            $this->db->select('currency,event_id')->from('fundraising_currency');
            $this->db->where('event_id', $new_eid);
            $newque = $this->db->get();
            $newres = $newque->result_array();
            $cu_data = $res[0];
            if (count($newres) > 0)
            {
                unset($cu_data['event_id']);
                $this->db->where('event_id', $new_eid);
                $this->db->update('fundraising_currency', $cu_data);
            }
            else
            {
                $cu_data['event_id'] = $new_eid;
                $this->db->insert('fundraising_currency', $cu_data);
            }
        }
        return $res;
    }
    public function get_all_home_screen_tabs_data($id)

    {
        $this->db->select('menu_id,cms_id,event_id,title,img,is_feture_product,img_view,Redirect_url,Background_color')->from('event_menu');
        $this->db->where('event_id', $id);
        $this->db->where('menu_id !=', '');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_survey_category_data($old_eid)

    {
        $this->db->select('*')->from('survey_category');
        $this->db->where('event_id', $old_eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function duplicate_all_suvery_category_data($survey, $old_eid, $new_eid, $org_id)

    {
        $old_survey_id = $survey['survey_id'];
        unset($survey['survey_id']);
        $survey['event_id'] = $new_eid;
        $this->db->insert('survey_category', $survey);
        $new_survey_id = $this->db->insert_id();
        $this->db->select('s.*')->from('survey s');
        $this->db->join('survey_category_relation scr', 'scr.Question_id=s.Id');
        $this->db->where('scr.survey_category_id', $old_survey_id);
        $this->db->where('s.Event_id', $old_eid);
        $old_question = $this->db->get()->result_array();
        $old_question_ids = array_column_1($old_question, 'Id');
        $question_ids = array();
        foreach($old_question as $key => $value)
        {
            $question = $value;
            unset($question['Id']);
            $question['Organisor_id'] = $org_id;
            $question['Event_id'] = $new_eid;
            $question['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('survey', $question);
            $Question_id = $this->db->insert_id();
            $question_ids[$value['Id']] = $Question_id;
            $rel_data['survey_category_id'] = $new_survey_id;
            $rel_data['Question_id'] = $Question_id;
            $this->db->insert('survey_category_relation', $rel_data);
        }
        $this->db->select('welcome_data,thanku_data')->from('survey_data');
        $this->db->where('Event_id', $old_eid);
        $this->db->where('surveys_id', $old_survey_id);
        $survey_data = $this->db->get()->row_array;
        $survey_data['Event_id'] = $new_eid;
        $survey_data['surveys_id'] = $new_survey_id;
        $this->db->insert('survey_data', $survey_data);
        $this->db->select('*')->from('survey_skiplogic');
        $where = "(question_id IN ('" . implode(",", $old_question_ids) . "') OR redirectquestion_id IN ('" . implode(",", $old_question_ids) . "'))";
        $this->db->where($where);
        $skiplogic_data = $this->db->get()->result_array();
        foreach($skiplogic_data as $key => $value)
        {
            $skip_data['question_id'] = $question_ids[$value['question_id']];
            $skip_data['option'] = $value['option'];
            $skip_data['redirectquestion_id'] = $question_ids[$value['redirectquestion_id']];
            $this->db->insert('survey_skiplogic', $skip_data);
        }
    }
    public function get_all_survey_data($id)

    {
        $this->db->select('welcome_data,thanku_data,Event_id')->from('survey_data');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_survey_Question_data($id)

    {
        $this->db->select('Event_id,Organisor_id,Question,Question_type,Option,Answer')->from('survey');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_Presentation_data($id)

    {
        $this->db->select('Organisor_id,Event_id,Start_date,Start_time,End_date,End_time,Status,Heading,Thumbnail_status,Auto_slide_status,Types,Address_map,Images,Image_lock,Image_current,presentation_file_type,user_permissions')->from('presentation');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_survey_question_id($sid, $new_eid)

    {
        $this->db->select('*')->from('survey');
        $this->db->where('Id', $sid);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*')->from('survey');
        $this->db->where('Event_id', $new_eid);
        $this->db->where('Question', $res[0]['Question']);
        $new_query = $this->db->get();
        $new_res = $new_query->result_array();
        return $new_res;
    }
    public function save_all_agenda($old_eid, $new_eid, $link_missing, $copymodules, $new_org_id = NULL)

    {
        $this->db->select('*')->from('agenda_categories');
        $this->db->where('event_id', $old_eid);
        $query = $this->db->get();
        $res = $query->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('*')->from('agenda_category_relation');
            $this->db->where('category_id', $value['Id']);
            $rquery = $this->db->get();
            $rres = $rquery->result_array();
            $new_cat['event_id'] = $new_eid;
            $new_cat['category_name'] = $value['category_name'];
            $new_cat['category_image'] = $value['category_image'];
            $new_cat['created_date'] = date('Y-m-d H:i:s');
            $new_cat['show_check_in_time'] = $value['show_check_in_time'];
            $this->db->insert('agenda_categories', $new_cat);
            $cat_id = $this->db->insert_id();
            foreach($rres as $key1 => $value1)
            {
                $this->db->select('*')->from('agenda');
                $this->db->where('Id', $value1['agenda_id']);
                $aquery = $this->db->get();
                $ares = $aquery->result_array();
                $new_agenda = $ares[0];
                if ($link_missing == '0')
                {
                    if (!empty($new_agenda['Speaker_id']))
                    {
                        $speakers = array_filter(explode(",", $new_agenda['Speaker_id']));
                        foreach($speakers as $skey => $svalue)
                        {
                            $this->db->select('*')->from('relation_event_user');
                            $this->db->where('Event_id', $new_eid);
                            $this->db->where('User_id', $svalue);
                            $squ = $this->db->get();
                            $res = $squ->result_array();
                            if (count($res) < 1)
                            {
                                $resl['Event_id'] = $new_eid;
                                $resl['User_id'] = $svalue;
                                $resl['Organisor_id'] = $new_org_id;
                                $resl['Role_id'] = 7;
                                $this->db->insert('relation_event_user', $resl);
                            }
                        }
                    }
                    if (!empty($new_agenda['document_id']))
                    {
                        $this->db->select('*')->from('documents');
                        $this->db->where('Event_id', $old_eid);
                        $this->db->where('id', $new_agenda['document_id']);
                        $dqu = $this->db->get();
                        $dres = $dqu->result_array();
                        if (count($dres) > 0)
                        {
                            $old_doc = $dres[0];
                            unset($old_doc['id']);
                            $old_doc['Event_id'] = $new_eid;
                            $this->db->select('*')->from('documents');
                            $this->db->where($old_doc);
                            $ndqu = $this->db->get();
                            $ndres = $ndqu->result_array();
                            if (count($ndres) > 0)
                            {
                                $new_agenda['document_id'] = $ndres[0]['id'];
                            }
                            else
                            {
                                $old_doc = $dres[0];
                                unset($old_doc['id']);
                                $old_doc['Event_id'] = $new_eid;
                                $this->db->insert('documents', $old_doc);
                                $doc_id = $this->db->insert_id();
                                $new_agenda['document_id'] = $doc_id;
                                $this->db->select('*')->from('document_files');
                                $this->db->where('document_id', $new_agenda['document_id']);
                                $qu = $this->db->get();
                                $res = $qu->result_array();
                                $newdocfiles = $res[0];
                                unset($newdocfiles['id']);
                                $newdocfiles['document_id'] = $doc_id;
                                $this->db->insert('document_files', $newdocfiles);
                            }
                        }
                    }
                    if (!empty($new_agenda['presentation_id']))
                    {
                        $this->db->select('*')->from('presentation');
                        $this->db->where('Event_id', $old_eid);
                        $this->db->where('Id', $new_agenda['presentation_id']);
                        $pqu = $this->db->get();
                        $pres = $pqu->result_array();
                        if (count($pres) > 0)
                        {
                            $new_pre = $pres[0];
                            unset($new_pre['Id']);
                            $new_pre['Event_id'] = $new_eid;
                            $this->db->select('*')->from('presentation');
                            $this->db->where($new_pre);
                            $npqu = $this->db->get();
                            $npres = $npqu->result_array();
                            if (count($npres) > 0)
                            {
                                $new_agenda['presentation_id'] = $npres[0]['Id'];
                            }
                            else
                            {
                                $this->db->insert('presentation', $new_pre);
                                $new_agenda['presentation_id'] = $this->db->insert_id();
                            }
                        }
                    }
                    if (!empty($new_agenda['Address_map']))
                    {
                        $this->db->select('*')->from('map');
                        $this->db->where('Event_id', $old_eid);
                        $this->db->where('Id', $new_agenda['Address_map']);
                        $mqu = $this->db->get();
                        $mres = $mqu->result_array();
                        if (count($mres) > 0)
                        {
                            $new_map = $mres[0];
                            unset($new_map['Id']);
                            $new_map['Event_id'] = $new_eid;
                            $this->db->select('*')->from('map')->where('Event_id', $new_map['Event_id'])->where('map_u_id', $new_map['map_u_id']);
                            $nmres = $this->db->get()->result_array();
                            if (count($nmres) > 0)
                            {
                                $new_agenda['Address_map'] = $nmres[0]['Id'];
                            }
                            else
                            {
                                $this->db->insert('map', $new_map);
                                $new_agenda['Address_map'] = $this->db->insert_id();
                            }
                        }
                    }
                }
                else
                {
                    $new_agenda['Speaker_id'] = "";
                    $new_agenda['document_id'] = "";
                    $new_agenda['presentation_id'] = "";
                    $new_agenda['Address_map'] = "";
                }
                unset($new_agenda['Id']);
                $new_agenda['Organisor_id'] = $new_org_id;
                $new_agenda['Event_id'] = $new_eid;
                $this->db->insert('agenda', $new_agenda);
                $rel['agenda_id'] = $this->db->insert_id();
                $rel['category_id'] = $cat_id;
                $this->db->insert('agenda_category_relation', $rel);
                if (in_array(2, $copymodules))
                {
                    $this->db->select('*')->from('attendee_agenda_relation');
                    $this->db->where('agenda_category_id', $value['Id']);
                    $aarqu = $this->db->get();
                    $aares = $aarqu->result_array();
                    $aar['agenda_category_id'] = $cat_id;
                    $aar['event_id'] = $new_eid;
                    foreach($aares as $key => $value)
                    {
                        $aar['attendee_id'] = $value['attendee_id'];
                        $this->db->insert('attendee_agenda_relation', $aar);
                    }
                }
            }
        }
    }
    public function get_qa_session_data($eid)

    {
        $this->db->select('Event_id,Session_name,Session_description,Moderator_speaker_id,start_time,end_time,session_date,created_date')->from('qa_session');
        $this->db->where('Event_id', $eid);
        return $this->db->get()->result_array();
    }
    public function get_all_gamification_header_data($eid)

    {
        return $this->db->select('header,event_id')->from('game_header')->where('event_id', $eid)->get()->result_array();
    }
    public function get_all_gamification_rank_color_data($eid)

    {
        return $this->db->select('rank_no,color,event_id')->from('game_rank_color')->where('event_id', $eid)->get()->result_array();
    }
    public function get_all_gamification_rank_point_data($eid)

    {
        return $this->db->select('rank_id,point,event_id')->from('game_rank_point')->where('event_id', $eid)->get()->result_array();
    }
    public function save_data_databasetables($tablename, $data)

    {
        if (!empty($tablename) && count($data) > 0)
        {
            $this->db->insert_batch($tablename, $data);
        }
    }
    public function updateevent_all_data($data, $eid)

    {
        $this->db->where('Id', $eid);
        $this->db->update('event', $data);
    }
    public function get_user_custom_clounm($uid, $eid)

    {
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id', $eid);
        $this->db->where('Attendee_id', $uid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_user_all_moderator($eid, $uid)

    {
        $this->db->select('*')->from('moderator_relation');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $uid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_speaker_id_by_moderator_id($eid, $mid)

    {
        $this->db->select('*')->from('moderator_relation');
        $this->db->where('event_id', $eid);
        $this->db->where('moderator_id', $mid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_user_assign_agenda_id($cid)

    {
        $this->db->select('group_concat(agenda_id) as agenda_ids')->from('agenda_category_relation');
        $this->db->where('category_id', $cid);
        $que = $this->db->get();
        $res = $que->result_array();
        return explode(",", $res[0]['agenda_ids']);
    }
    public function save_lead_user_data($data)

    {
        $this->db1->select('*')->from('visitor_lead');
        $this->db1->where('user_email', $data['user_email']);
        $res = $this->db1->get()->result_array();
        if (count($res) < 1)
        {
            $this->db1->insert('visitor_lead', $data);
        }
    }
    public function get_user_detail_by_user_id($uid)

    {
        $this->db->select('*')->from('user');
        $this->db->where('Id', $uid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function getAllUsersGCM_id_by_event_id($event_id)

    {
        return $this->db->select('u.*')->from('relation_event_user reu')->join('user u', 'u.Id = reu.User_id')->where('reu.Event_id', $event_id)->get()->result_array();
    }
    public function check_event_free_trial_account_by_org_email($email)

    {
        $this->db1->select('*')->from('organizer_user');
        $this->db1->where('Email', $email);
        $this->db1->where('subscriptiontype', '23');
        $qu = $this->db1->get();
        $res = $qu->result_array();
        return $res;
    }
    public function check_muliti_event_account_by_org_email($email)

    {
        $this->db1->select('*')->from('organizer_user');
        $this->db1->where('Email', $email);
        $this->db1->where('subscriptiontype', '19');
        $qu = $this->db1->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_multievent_by_org_id($oid)

    {
        $this->db->select('*')->from('event');
        $this->db->where('Organisor_id', $oid);
        $this->db->where('launch_your_app', '2');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_all_flage_data_by_event($eid)

    {
        $this->db->select('*')->from('Progress_flage_data');
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function save_complate_flage_data($fdata, $eid)

    {
        $res = $this->get_all_flage_data_by_event($eid);
        if (count($res) > 0)
        {
            $this->db->where('event_id', $eid);
            $this->db->update('Progress_flage_data', $fdata);
        }
        else
        {
            $fdata['event_id'] = $eid;
            $this->db->insert('Progress_flage_data', $fdata);
        }
    }
    public function get_splash_screen_data($oid)

    {
        $this->db->select('*')->from('splash_screen_data');
        $this->db->where('organisor_id', $oid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function save_splash_screen_data($sp_data)

    {
        $res = $this->get_splash_screen_data($sp_data['organisor_id']);
        if (count($res) > 0)
        {
            $oid = $sp_data['organisor_id'];
            unset($sp_data['organisor_id']);
            $this->db->where('organisor_id', $oid);
            $this->db->update('splash_screen_data', $sp_data);
        }
        else
        {
            $this->db->insert('splash_screen_data', $sp_data);
        }
    }
    public function change_subscription_type_by_org_email($org_email, $sub_type)

    {
        $this->db1->select('*')->from('subscription_billing_cycle');
        $this->db1->where('id', $sub_type);
        $subres = $this->db1->get()->result_array();
        if (count($subres) > 0)
        {
            $date = date("Y-m-d", strtotime("today"));
            if ($subres[0]['year'] != "")
            {
                $date = date("Y-m-d", strtotime($date . "+" . $subres[0]['year'] . " Year"));
            }
            if ($subres[0]['month'] != "")
            {
                $date = date("Y-m-d", strtotime($date . "+" . $subres[0]['month'] . " months"));
            }
            if ($subres[0]['day'] != "")
            {
                $date = date("Y-m-d", strtotime($date . "+" . $subres[0]['day'] . " days"));
            }
            $org_data['Expiry_date'] = $date;
            $org_data['subscriptiontype'] = $sub_type;
            $this->db1->where('Email', $org_email);
            $this->db1->update('organizer_user', $org_data);
        }
    }
    public function get_organisor_scret_key_by_organisor_id($org_id)

    {
        $this->db->select('*')->from('stripe_settings');
        $this->db->where('organisor_id', $org_id);
        $oqu = $this->db->get();
        $ores = $oqu->result_array();
        return $ores;
    }
    public function get_allow_contact_me_option_by_user_id($aid, $eid)

    {
        $this->db->select('*')->from('event_attendee');
        $this->db->where('Event_id', $eid);
        $this->db->where('Attendee_id', $aid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function edit_force_login_status($mids, $eid)

    {
        $this->db->where('event_id', $eid)->update('event_menu', array(
            'is_force_login' => '0'
        ));
        foreach($mids as $key => $value)
        {
            $res = $this->db->select('*')->from('event_menu')->where('event_id', $eid)->where('menu_id', $value)->get()->result_array();
            if (count($res) > 0)
            {
                $this->db->where('event_id', $eid)->where('menu_id', $value)->update('event_menu', array(
                    'is_force_login' => '1'
                ));
            }
            else
            {
                $menures = $this->db->select('*')->from('menu')->where('Id', $value)->get()->row_array();
                $eventmenudata['menu_id'] = $value;
                $eventmenudata['event_id'] = $eid;
                $eventmenudata['title'] = $menures['menuname'];
                $eventmenudata['is_feture_product'] = '0';
                $eventmenudata['is_force_login'] = '1';
                $this->db->insert('event_menu', $eventmenudata);
            }
        }
    }
    public function get_all_menu_id_by_froce_login($eid)

    {
        return $this->db->select('group_concat(menu_id) as menuid', false)->from('event_menu')->where('event_id', $eid)->where('is_force_login', '1')->get()->row_array();
    }
    public function get_force_login_enabled_by_menu_id($mid, $eid)

    {
        return $this->db->select('*')->from('event_menu')->where('event_id', $eid)->where('menu_id', $mid)->get()->row_array();
    }
    public function add_advert_hit($event_id, $advert_id, $user_id, $date)

    {
        $this->db->select('*')->from('user_click_board');
        $this->db->where('event_id', $event_id);
        $this->db->where('advert_id', $advert_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('date', $date);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $adata['click_hit'] = $res[0]['click_hit'] + 1;
            $this->db->where('event_id', $event_id);
            $this->db->where('advert_id', $advert_id);
            $this->db->where('user_id', $user_id);
            $this->db->where('date', $date);
            $this->db->update('user_click_board', $adata);
        }
        else
        {
            $adata['click_hit'] = 1;
            $adata['event_id'] = $event_id;
            $adata['advert_id'] = $advert_id;
            $adata['user_id'] = $user_id;
            $adata['date'] = $date;
            $this->db->insert('user_click_board', $adata);
        }
    }
    public function add_visit_profile_hit($event_id, $sid, $user_id)

    {
        $date = date('Y-m-d');
        $this->db->select('*')->from('user_click_board');
        $this->db->where('event_id', $event_id);
        $this->db->where('sponsor_id', $sid);
        $this->db->where('user_id', $user_id);
        $this->db->where('date', $date);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $adata['click_hit'] = $res[0]['click_hit'] + 1;
            $this->db->where('event_id', $event_id);
            $this->db->where('sponsor_id', $sid);
            $this->db->where('user_id', $user_id);
            $this->db->where('date', $date);
            $this->db->update('user_click_board', $adata);
        }
        else
        {
            $adata['click_hit'] = 1;
            $adata['event_id'] = $event_id;
            $adata['sponsor_id'] = $sid;
            $adata['user_id'] = $user_id;
            $adata['date'] = $date;
            $this->db->insert('user_click_board', $adata);
        }
    }
    public function add_exhibitor_visit_profile_hit($event_id, $eid, $user_id)

    {
        $date = date('Y-m-d');
        $this->db->select('*')->from('user_click_board');
        $this->db->where('event_id', $event_id);
        $this->db->where('exhibitor_id', $eid);
        $this->db->where('user_id', $user_id);
        $this->db->where('date', $date);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            $adata['click_hit'] = $res[0]['click_hit'] + 1;
            $this->db->where('event_id', $event_id);
            $this->db->where('exhibitor_id', $eid);
            $this->db->where('user_id', $user_id);
            $this->db->where('date', $date);
            $this->db->update('user_click_board', $adata);
        }
        else
        {
            $adata['click_hit'] = 1;
            $adata['event_id'] = $event_id;
            $adata['exhibitor_id'] = $eid;
            $adata['user_id'] = $user_id;
            $adata['date'] = $date;
            $this->db->insert('user_click_board', $adata);
        }
    }
    public function change_done_stripe_payment_status($email, $arr)

    {
        $this->db1->where('Email', $email);
        $this->db1->update('organizer_user', $arr);
    }
    public function save_banner_image_and_content_by_event($imgdata)

    {
        $this->db->insert_batch('event_banner_image', $imgdata);
    }
    public function get_all_banner_list_by_event($event_id, $cid = NULL, $menulist = NULL, $allow_show_all_agenda = 0)

    {
        $this->db->select('*')->from('event_banner_image');
        $this->db->where('event_id', $event_id);
        $this->db->where('image_type', '0');
        $res = $this->db->get()->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('ebc.*,m.pagetitle,m.menuname')->from('event_banner_coord ebc');
            $this->db->join('menu m', 'ebc.menuid = m.Id and ebc.menuid!=""', 'left');
            $this->db->where('ebc.banner_id', $value['id']);
            if (!empty($cid) && $allow_show_all_agenda != '1')
            {
                if (in_array('1', $menulist))
                {
                    $this->db->where('(ebc.agenda_id IN ("' . implode(",", array_filter($cid)) . '") OR ebc.agenda_id IS NULL)');
                }
                else
                {
                    $this->db->where('ebc.agenda_id IS NULL');
                }
            }
            $coords = $this->db->get()->result_array();
            $res[$key]['coords'] = $coords;
        }
        return $res;
    }
    public function get_all_active_icon_list_by_event_id($eid, $cid = NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*')->from('event');
        $this->db->where('Id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $eventmodule = explode(",", $res[0]['checkbox_values']);
        unset($eventmodule[array_search(8, $eventmodule) ]);
        unset($eventmodule[array_search(54, $eventmodule) ]);
        if ($user[0]->Id != "" && $user[0]->Rid == '4')
        {
            $this->db->select('*')->from('event_attendee ea');
            $this->db->join('user_views uv', 'uv.view_id=ea.views_id', 'right');
            $this->db->where('ea.Event_id', $eid);
            $this->db->where('ea.Attendee_id', $user[0]->Id);
            $vqu = $this->db->get();
            $vres = $vqu->result_array();
            if (!empty($vres[0]['view_modules']))
            {
                $check = explode(',', $res[0]['checkbox_values']);
                $activemodules = explode(",", $vres[0]['view_modules']);
                $eventmodule = array_intersect($check, $activemodules);
            }
        }
        $this->db->protect_identifiers = false;
        $this->db->select('em.id,em.menu_id,em.cms_id,em.agenda_id,em.event_id,CASE WHEN em.title IS NULL THEN CASE WHEN m.menuname IS NOT NULL THEN m.menuname ELSE c.Menu_name end ELSE em.title END as title,em.img,em.is_feture_product,em.img_view,em.Redirect_url,em.Background_color,em.sort_order,em.redirect_link,m.pagetitle', false)->from('event_menu em');
        $this->db->join('menu m', 'm.Id=em.menu_id AND em.menu_id IS NOT NULL', 'left');
        $this->db->join('cms c', 'c.Id=em.cms_id AND em.cms_id IS NOT NULL', 'left');
        $this->db->where('em.event_id', $eid);
        $this->db->where('em.is_feture_product', '1');
        if ($user[0]->Id != "" && $user[0]->Rid == '4' && count($vres) > 0 && $vres[0]['view_type'] == '0')
        {
            $this->db->where_in('c.Id', explode(",", $vres[0]['view_custom_modules']));
        }
        if (count(array_filter($eventmodule)) > 0)
        {
            $where = "(m.Id IN (" . implode(",", array_filter($eventmodule)) . ") or m.Id IS NULL)";
        }
        else
        {
            $where = "(m.Id IS NULL)";
        }
        if (!empty($cid) && $res[0]['allow_show_all_agenda'] != '1')
        {
            if (in_array('1', $eventmodule))
            {
                $this->db->where('(em.agenda_id IN ("' . implode(",", array_filter($cid)) . '") OR em.agenda_id IS NULL)');
            }
            else
            {
                $this->db->where('em.agenda_id IS NULL');
            }
        }
        $this->db->where($where);
        $this->db->order_by('em.sort_order=0,em.sort_order', 'ASC', false);
        $res = $this->db->get()->result_array();
        if ($user[0]->Rid != '6')
        {
            if ($user[0]->Rid == '4')
            {
                $this->db->select('*')->from('exhibitor_representatives');
                $this->db->where('event_id', $eid);
                $this->db->where('reps_user_id', $user[0]->Id);
                $att_pre = $this->db->get()->result_array();
                if (count($att_pre) <= 0)
                {
                    $menukey = array_search(53, array_column_1($res, 'menu_id'));
                    if ($menukey != "")
                    {
                        unset($res[$menukey]);
                    }
                }
            }
            else
            {
                $menukey = array_search(53, array_column_1($res, 'menu_id'));
                if ($menukey != "")
                {
                    unset($res[$menukey]);
                }
            }
        }
        return $res;
    }
    public function get_all_in_active_icon_list_by_event_id($eid)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*')->from('event');
        $this->db->where('Id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $eventmodule = explode(",", $res[0]['checkbox_values']);
        unset($eventmodule[array_search(8, $eventmodule) ]);
        $this->db->protect_identifiers = false;
        $this->db->select('em.id,em.menu_id,em.cms_id,em.event_id,CASE WHEN em.title IS NULL THEN case when m.menuname IS NOT NULL THEN m.menuname ELSE c.Menu_name end ELSE em.title END as title,em.img,em.is_feture_product,em.img_view,em.Redirect_url,em.Background_color,em.sort_order,em.redirect_link,m.pagetitle', false)->from('event_menu em');
        $this->db->join('menu m', 'm.Id=em.menu_id AND em.menu_id IS NOT NULL', 'left');
        $this->db->join('cms c', 'c.Id=em.cms_id AND em.cms_id IS NOT NULL', 'left');
        $this->db->where('em.event_id', $eid);
        $this->db->where('em.is_feture_product', '0');
        if (count(array_filter($eventmodule)) > 0)
        {
            $where = "(m.Id IN (" . implode(",", array_filter($eventmodule)) . ") or m.Id IS NULL)";
        }
        else
        {
            $where = "(m.Id IS NULL)";
        }
        $this->db->where($where);
        $this->db->order_by('em.sort_order=0,em.sort_order', 'ASC', false);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_all_homescreenicon_by_event_id($eid)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*')->from('event');
        $this->db->where('Id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $eventmodule = explode(",", $res[0]['checkbox_values']);
        unset($eventmodule[array_search(8, $eventmodule) ]);
        $this->db->protect_identifiers = false;
        $this->db->select('em.id,em.menu_id,em.cms_id,em.event_id,CASE WHEN em.title IS NULL THEN case when m.menuname IS NOT NULL THEN m.menuname ELSE c.Menu_name end ELSE em.title END as title,em.img,em.is_feture_product,em.img_view,em.Redirect_url,em.Background_color,em.sort_order,em.redirect_link,m.pagetitle', false)->from('event_menu em');
        $this->db->join('menu m', 'm.Id=em.menu_id AND em.menu_id IS NOT NULL', 'left');
        $this->db->join('cms c', 'c.Id=em.cms_id AND em.cms_id IS NOT NULL', 'left');
        $this->db->where('em.event_id', $eid);
        if (count(array_filter($eventmodule)) > 0)
        {
            $where = "(m.Id IN (" . implode(",", array_filter($eventmodule)) . ") or m.Id IS NULL)";
        }
        else
        {
            $where = "(m.Id IS NULL)";
        }
        $this->db->where($where);
        $this->db->order_by('em.sort_order=0,em.sort_order', 'ASC', false);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function update_active_icon_order($mid, $order_no, $is_feture_product)

    {
        $this->db->where('id', $mid);
        $this->db->update('event_menu', array(
            'sort_order' => $order_no,
            'is_feture_product' => $is_feture_product
        ));
    }
    public function add_custom_modules_link($eid, $mid, $url)

    {
        $this->db->where('event_id', $eid);
        $this->db->where('id', $mid);
        $this->db->update('event_menu', array(
            'menu_id' => NULL,
            'cms_id' => NULL,
            'is_feture_product' => '1',
            'redirect_link' => $url
        ));
    }
    public function delete_event_menu_exists_by_menu_or_cms_id($eid, $mid = NULL, $cid = NULL)

    {
        $this->db->where('event_id', $eid);
        if (!empty($mid))
        {
            $this->db->where('menu_id', $mid);
        }
        if (!empty($cid))
        {
            $this->db->where('cms_id', $cid);
        }
        $this->db->delete('event_menu');
    }
    public function update_event_menu_modules($eid, $mid, $menu_id = NULL, $cid = NULL)

    {
        $this->db->where('event_id', $eid);
        $this->db->where('id', $mid);
        $this->db->update('event_menu', array(
            'menu_id' => $menu_id,
            'cms_id' => $cid,
            'is_feture_product' => '1'
        ));
    }
    public function save_banner_image_in_event($data)

    {
        $this->db->insert('event_banner_image', $data);
        return $this->db->insert_id();
    }
    public function get_banner_details_by_banner_id($bid)

    {
        $this->db->select('*')->from('event_banner_image');
        $this->db->where('id', $bid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function update_banner_image_in_event($data, $bid)

    {
        $this->db->where('id', $bid);
        $this->db->update('event_banner_image', $data);
    }
    public function save_banner_image_area_coords($area_data)

    {
        $this->db->insert('event_banner_coord', $area_data);
        return $this->db->insert_id();
    }
    public function delete_banner_image_area($aid)

    {
        $this->db->where('id', $aid);
        $this->db->delete('event_banner_coord');
    }
    public function delete_Advance_banner($bid)

    {
        $this->db->where('id', $bid);
        $this->db->delete('event_banner_image');
    }
    public function get_all_active_cms_menu_by_event($eid, $cids)

    {
        if (!empty($eid))
        {
            $this->db->select('(case when em.title IS NULL then c.Menu_name else em.title end) as Menuname,c.*', false)->from('cms c');
            $this->db->join('event_menu em', 'em.cms_id=c.Id and em.event_id=' . $eid, 'left');
            $this->db->where('c.Event_id', $eid);
            if (count($cids) > 0)
            {
                $this->db->where_not_in('c.Id', $cids);
            }
            $res = $this->db->get()->result_array();
            return $res;
        }
    }
    public function get_active_menu_list_by_event($eid, $mids)

    {
        $this->db->select('*')->from('menu');
        if (count(array_filter($mids)) > 0)
        {
            $this->db->where_in('id', $mids);
        }
        $this->db->where_not_in('id', array(
            5,
            8,
            21,
            26,
            27,
            28,
            40,
            42
        ));
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function save_lead_data($lead_data)

    {
        $this->db1->insert('lead', $lead_data);
        return $this->db1->insert_id();
    }
    public function check_demo_app_creted($email)

    {
        $this->db1->select('*')->from('organizer_user');
        $this->db1->where('Email', $email);
        $res = $this->db1->get()->result_array();
        return $res;
    }
    public function get_copy_event_data_by_event_id($eid)

    {
        return $this->db->select('*')->from('event')->where('Id', $eid)->get()->row_array();
    }
    public function update_auto_creted_event_status($soid)

    {
        $this->db1->where('Id', $soid);
        $this->db1->update('organizer_user', array(
            'demo_app_created' => '1'
        ));
    }
    public function save_onboarding_settings($data, $event_id)

    {
        $this->db->select('*');
        $this->db->from('onboarding_settings');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!count($res))
        {
            $data['event_id'] = $event_id;
            $data['change_on_tap'] = ($data['change_on_tap']) ? '1' : '0';
            $data['change_screen_on_seconds'] = ($data['change_screen_on_seconds']) ? '1' : '0';
            $this->db->insert("onboarding_settings", $data);
        }
        else
        {
            $data['change_on_tap'] = ($data['change_on_tap']) ? '1' : '0';
            $data['change_screen_on_seconds'] = ($data['change_screen_on_seconds']) ? '1' : '0';
            $this->db->where('event_id', $event_id);
            $this->db->update("onboarding_settings", $data);
        }
    }
    public function get_onboarding_settings($eid)

    {
        $this->db->select('*');
        $this->db->where('event_id', $eid);
        $q = $this->db->get('onboarding_settings');
        $res = $q->row_array();
        return $res;
    }
    public function get_cms_menu($eid)

    {
        $this->db->select('c.Id as id,CASE WHEN em.title IS NULL THEN c.Menu_name ELSE em.title END as menuname', FALSE);
        $this->db->from('cms c');
        $this->db->join('event_menu em', 'c.Id=em.cms_id', 'left');
        $this->db->where('c.Event_id', $eid);
        $this->db->where('c.status', '1');
        $this->db->order_by("c.Id", "desc");
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function sendEmailToAttendees($event_id, $message, $email, $subject = "Notification")

    {
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'localhost';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'invite@allintheloop.com';
        $config['smtp_pass'] = 'xHi$&h9M)x9m';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $event_data = $this->db->select('Event_name')->from('event')->where('Id', $event_id)->get()->row_array();
        $event_name = $event_data['Event_name'];
        $this->email->from("invite@allintheloop.com", ucfirst($event_name));
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message(html_entity_decode($message));
        $result = $this->email->send();
    }
    public function get_lock_agenda_status($org_id)

    {
        $email = $this->getOrgEmail($org_id);
        $this->db1->select('lock_agenda')->from('organizer_user');
        $this->db1->where('Email', $email);
        $res = $this->db1->get()->row_array();
        return $res['lock_agenda'];
    }
    public function get_all_left_hand_menu_image_list_by_event($event_id)

    {
        $this->db->select('*')->from('event_banner_image');
        $this->db->where('event_id', $event_id);
        $this->db->where('image_type', '1');
        $res = $this->db->get()->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('ebc.*,m.pagetitle,m.menuname')->from('event_banner_coord ebc');
            $this->db->join('menu m', 'ebc.menuid = m.Id and ebc.menuid!=""', 'left');
            $this->db->where('ebc.banner_id', $value['id']);
            $coords = $this->db->get()->result_array();
            $res[$key]['coords'] = $coords;
        }
        return $res;
    }
    public function get_agenda_category_menu_data($id, $cid)

    {
        $this->db->select('em.*,(CASE WHEN em.title IS NULL THEN ac.category_name ELSE em.title END) as title')->from('agenda_categories ac');
        $this->db->join('event_menu em', 'em.agenda_id=ac.Id', 'left');
        $this->db->where('ac.event_id', $id);
        $this->db->where('ac.Id ', $cid);
        return $this->db->get()->row_array();
    }
    public function get_all_representatives($id, $user_id)

    {
        return $this->db->select('*')->from('user')->join('user_representatives', 'user_representatives.rep_id = user.Id')->where('user_representatives.event_id', $id)->where('user_representatives.user_id', $user_id)->get()->result_array();
    }
    public function save_event_settings($event_id, $data)

    {
        $event_settings = $this->db->where('event_id', $event_id)->get('event_settings')->row_array();
        if (!empty($event_settings))
        {
            $this->db->where('event_id', $event_id);
            $this->db->update('event_settings', $data);
        }
        else
        {
            $data['event_id'] = $event_id;
            $this->db->insert('event_settings', $data);
        }
    }
    public function get_event_settings($event_id)

    {
        return $this->db->where('event_id', $event_id)->get('event_settings')->row_array();
    }
    public function update_access_key_event($event_id, $data)

    {
        $this->db->where('id', $event_id);
        $this->db->update('event', $data);
    }
    public function check_access_key_event($event_id, $access_key)

    {
        $this->db->select('id', 'access_key');
        $this->db->where_not_in('id', [$event_id]);
        $this->db->where('access_key', $access_key);
        $res = $this->db->get('event')->result_array();
        return $res;
    }
    public function get_access_key_event($access_key)

    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('access_key', $access_key);
        $query = $this->db->get()->row_array();
        return $query;
    }
    public function addAttendeeUserOpenApi($data)

    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    public function addRelationEventUser($data)

    {
        $this->db->insert('relation_event_user', $data);
        return $this->db->insert_id();
    }
    public function checkEmailUserTable($email)

    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $email);
        $query = $this->db->get()->row_array();
        return $query;
    }
    public function updateAttendeeUserOpenApi($user_id, $data)

    {
        $this->db->where('id', $user_id);
        $this->db->update('user', $data);
    }
    public function getUserFromEmail($email)

    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $email);
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function get_event_accesskey_id($id, $access_key)

    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('id', $id);
        // $this->db->where('access_key', $access_key);
        $query = $this->db->get()->row_array();
        return $query;
    }
    public function checkUserEmail($email)

    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $email);
        $query = $this->db->get()->row_array();
        return $query;
    }
    public function getCSVEventImport($import_type)

    {
        $this->db->select('*');
        $this->db->where('import_type', $import_type);
        $res = $this->db->get('event_csv_import')->result_array();
        return $res;
    }
    public function saveCacheKey($event_id, $CacheKey)

    {
        $this->db->select('*')->from('event_cache_keys');
        $this->db->where('event_id', $event_id);
        $this->db->where('cache_key', $CacheKey);
        $result = $this->db->get()->row_array();
        if (empty($result))
        {
            $data['event_id'] = $event_id;
            $data['cache_key'] = $CacheKey;
            $data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('event_cache_keys', $data);
            return $this->db->insert_id();
        }
    }
    public function getEventBetweenCurrentDate($eventid)

    {
        $this->db->select('Id,Start_date,End_date');
        $this->db->from('event');
        $this->db->where('Id', $eventid);
        $this->db->where("Start_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->where("End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $query = $this->db->get();
        $res = $query->row();
        return $res;
    }
    public function getEventCacheKeys($eventid)

    {
        $this->db->select('*');
        $this->db->where('event_id', $eventid);
        $res = $this->db->get('event_cache_keys')->result_array();
        return $res;
    }
    public function deleteEventCacheKeys($ids)

    {
        $this->db->where_in('id', $ids);
        $this->db->delete('event_cache_keys');
    }
    public function saveEventCSVImport($data)

    {
        $this->db->select('*')->from('event_csv_import');
        $this->db->where('event_id', $data['event_id']);
        $this->db->where('import_type', $data['import_type']);
        $result = $this->db->get()->row_array();
        if (empty($result))
        {
            $data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('event_csv_import', $data);
            return $this->db->insert_id();
        }
        else
        {
            $data['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where("id", $result['id']);
            $this->db->update("event_csv_import", $data);
            return true;
        }
    }
    public function saveCSVData($csvpath, $eventid, $Organisor_id)

    {
        $csv_mapping = $this->db->where('event_id', $eventid)->where('import_type', 'attendees')->get('event_csv_import')->row_array() ['csv_mapping'];
        $csv_mapping = json_decode($csv_mapping, true);
        $str = "SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "' ";
        foreach($csv_mapping as $key => $value)
        {
            $str.= " , $key = @var$value";
        }
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvpath . "' 
                          INTO TABLE attendee_tmp 
                          FIELDS TERMINATED BY ',' 
                          OPTIONALLY ENCLOSED BY '\"' 
                          IGNORE 1 LINES (@var0,@var1,@var2,@var3,@var4,@var5,@var6) $str");
        return true;
    }
    public function saveJSONData($jsonpath, $eventid, $Organisor_id)

    {
        $jsonString = file_get_contents($jsonpath);
        $jsonDecoded = json_decode($jsonString, true);
        $csvFileName = $_SERVER['DOCUMENT_ROOT'] . '/assets/test/' . $eventid . '-attendees-' . $Organisor_id . uniqid() . '.csv';
        $fp = fopen($csvFileName, 'w');
        $attendee['Company_name'] = 'Company_name';
        $attendee['Firstname'] = 'Firstname';
        $attendee['Lastname'] = 'Lastname';
        $attendee['Email'] = 'Email';
        $attendee['Password'] = 'Password';
        $attendee['Salutation'] = 'Salutation';
        $attendee['Title'] = 'Title';
        fputcsv($fp, $attendee);
        foreach($jsonDecoded as $row)
        {
            fputcsv($fp, $row);
        }
        fclose($fp);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvFileName . "' 
               INTO TABLE attendee_tmp 
               FIELDS TERMINATED BY ','
               OPTIONALLY ENCLOSED BY '\"'
               IGNORE 1 LINES
               SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "'");
    }
    public function saveJSONDataExhi($jsonpath, $eventid, $Organisor_id)

    {
        $jsonString = file_get_contents($jsonpath);
        $jsonDecoded = json_decode($jsonString, true);
        $csvFileName = $_SERVER['DOCUMENT_ROOT'] . '/assets/test/' . $eventid . '-Exhi-' . $Organisor_id . uniqid() . '.csv';
        $fp = fopen($csvFileName, 'w');
        foreach($jsonDecoded as $row)
        {
            fputcsv($fp, $row);
        }
        fclose($fp);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvFileName . "' 
               INTO TABLE exhi_tmp 
               FIELDS TERMINATED BY ','
               OPTIONALLY ENCLOSED BY '\"'
               SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "'");
    }
    public function getAttendeeTMPtable()

    {
        $this->db->select('*')->from('attendee_tmp');
        $this->db->limit(300);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function deleteAttendeeTMPtable($id)

    {
        $this->db->delete('attendee_tmp', array(
            'id' => $id
        ));
    }
    public function saveCSVDataExhi($csvpath, $eventid, $Organisor_id)

    {
        $csv_mapping = $this->db->where('event_id', $eventid)->where('import_type', 'exhibitors')->get('event_csv_import')->row_array() ['csv_mapping'];
        $csv_mapping = json_decode($csv_mapping, true);
        $str = "SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "' ";
        foreach($csv_mapping as $key => $value)
        {
            $key = str_replace('_', '', $key);
            $key = ($key == 'Heading') ? 'Firstname' : $key;
            $str.= " , $key = @var$value";
        }
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvpath . "' 
                          INTO TABLE exhi_tmp 
                          FIELDS TERMINATED BY ',' 
                          OPTIONALLY ENCLOSED BY '\"' 
                          IGNORE 1 LINES (@var0,@var1,@var2,@var3,@var4,@var5,@var6,@var7,@var8,@var9,@var10,@var11,@var12,@var13,@var14,@var15,@var16,@var17,@var18) $str");
        return true;
    }
    public function saveCSVDataAgenda($csvpath, $eventid, $Organisor_id)

    {
        $csv_mapping = $this->db->where('event_id', $eventid)->where('import_type', 'agenda')->get('event_csv_import')->row_array() ['csv_mapping'];
        $csv_mapping = json_decode($csv_mapping, true);
        $str = "SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "' ";
        $i = 0;
        foreach($csv_mapping as $key => $value)
        {
            $str.= " , $key = @var$value";
            $str2[] = "@var" . $i;
            $i++;
        }
        $str2 = implode(',', $str2);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvpath . "' 
                          INTO TABLE agenda_tmp 
                          FIELDS TERMINATED BY ',' 
                          OPTIONALLY ENCLOSED BY '\"' 
                          IGNORE 1 LINES ($str2) $str");
        return true;
    }
    public function saveJSONDataAgenda($jsonpath, $eventid, $Organisor_id)

    {
        $jsonString = file_get_contents($jsonpath);
        $jsonDecoded = json_decode($jsonString, true);
        $csvFileName = $_SERVER['DOCUMENT_ROOT'] . '/assets/test/' . $eventid . '-agenda-' . $Organisor_id . uniqid() . '.csv';
        $fp = fopen($csvFileName, 'w');
        foreach($jsonDecoded as $row)
        {
            fputcsv($fp, $row);
        }
        fclose($fp);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvFileName . "' 
               INTO TABLE agenda_tmp 
               FIELDS TERMINATED BY ','
               OPTIONALLY ENCLOSED BY '\"'
               SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "'");
    }
    public function saveCSVDataSpeaker($csvpath, $eventid, $Organisor_id)

    {
        $csv_mapping = $this->db->where('event_id', $eventid)->where('import_type', 'speaker')->get('event_csv_import')->row_array() ['csv_mapping'];
        $csv_mapping = json_decode($csv_mapping, true);
        $str = "SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "' ";
        $i = 0;
        foreach($csv_mapping as $key => $value)
        {
            $str.= " , $key = @var$value";
            $str2[] = "@var" . $i;
            $i++;
        }
        $str2 = implode(',', $str2);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvpath . "' 
                          INTO TABLE speaker_tmp 
                          FIELDS TERMINATED BY ',' 
                          OPTIONALLY ENCLOSED BY '\"' 
                          IGNORE 1 LINES ($str2) $str");
        return true;
    }
    public function saveJSONDataSpeaker($jsonpath, $eventid, $Organisor_id)

    {
        $jsonString = file_get_contents($jsonpath);
        $jsonDecoded = json_decode($jsonString, true);
        $csvFileName = $_SERVER['DOCUMENT_ROOT'] . '/assets/test/' . $eventid . '-speaker-' . $Organisor_id . uniqid() . '.csv';
        $fp = fopen($csvFileName, 'w');
        foreach($jsonDecoded as $row)
        {
            fputcsv($fp, $row);
        }
        fclose($fp);
        $this->db->query("LOAD DATA LOCAL INFILE '" . $csvFileName . "' 
               INTO TABLE speaker_tmp 
               FIELDS TERMINATED BY ','
               OPTIONALLY ENCLOSED BY '\"'
               SET event_id = '" . $eventid . "' , organisor_id = '" . $Organisor_id . "'");
    }
}
?>