<?php
class Qr_scanner_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_qr_scanner_list($id = null)

    {
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Name', 'Qr_scanner');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_qr_scanner($array_add=NULL)

    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Name', 'Qr_scanner');
        $query = $this->db->get();
        $result = $query->result_array();
        $array_add['Role_id'] = $result[0]['Id'];
        $array_add['Password'] = md5($array_add['Password']);
        $this->db->insert('user', $array_add);
        $this->session->set_flashdata('qr_scanner_data', 'Added');
        redirect('qr_scanner');
    }
    public function delete_qr_scanner($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('user');
        $str = $this->db->last_query();
    }
    public function get_recent_qr_scanner_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Name', 'Qr_scanner');
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
?>