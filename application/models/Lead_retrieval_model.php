<?php
class lead_retrieval_model extends CI_Model

{
    function __construct()
    {
        $this->db1 = $this->load->database('db1', TRUE);
        parent::__construct();
    }
    public function get_edit_attendee_informaion_event_by_user_id($eid=NULL, $uid=NULL)

    {
        $this->db->select('u.*,reu.Role_id,ea.extra_column,ea.check_in_attendee,ea.views_id,ea.group_id')->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id And reu.Event_id=' . $eid);
        $this->db->join('event_attendee ea', 'u.Id=ea.Attendee_id and ea.Event_id=' . $eid, 'left');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        $this->db->where('u.Id', $uid);
        $this->db->group_by('u.Id');
        return $this->db->get()->row_array();
    }
    public function get_all_custom_column_data($eid=NULL)

    {
        $this->db->select('*')->from('custom_column');
        $this->db->where('event_id', $eid);
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_event_oraganiser($eid=NULL)

    {
        $res = $this->db->where('Id', $eid)->get('event')->row_array();
        return $res['Organisor_id'];
    }
    public function get_all_exibitor_user_questions($eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $org_id = $this->get_event_oraganiser($eid);
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id', 'left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id', $eid);
        if ($user[0]->ask_survey == '1')
        {
            $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $org_id . ')');
        }
        else
        {
            $this->db->where('eq.exibitor_user_id', $ex_uid);
        }
        $this->db->group_by('eq.q_id');
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $res2 = $this->db->get()->result_array();
        foreach($res2 as $key => $value)
        {
            if ($value['skip_logic'] == '1')
            {
                $this->db->select('`option`,redirectexibitorquestion_id')->from('exibitor_question_skiplogic');
                $this->db->where('exibitor_question_id', $value['q_id']);
                $res = $this->db->get()->result_array();
                $option = array_column_1($res, 'option');
                $redirectquestion_id = array_column_1($res, 'redirectexibitorquestion_id');
                $finalarray = array_combine($option, $redirectquestion_id);
                $res2[$key]['redirectids'] = json_encode($finalarray);
            }
            $question_array = $this->get_survey_array($eid, $ex_uid, $uid);
            $dependent_ids = $question_array['dependent_ids'];
            $independent_ids = array_values($question_array['independent_ids']);
            if (!in_array($value['q_id'], $independent_ids))
            {
                $redirectkey = $this->get_inarrayval($value['q_id'], $dependent_ids, $independent_ids);
                $rekey = array_search($redirectkey, $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            else
            {
                $rekey = array_search($value['q_id'], $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            $res2[$key]['redirectid'] = $redirectid;
        }
        return $res2;
    }
    public function get_inarrayval($qid=NULL, $dearr=NULL, $indearr=NULL)

    {
        $qkey = $dearr[$qid];
        if (in_array($qkey, $indearr))
        {
            return $qkey;
        }
        return $this->get_inarrayval($qkey, $dearr, $indearr);
    }
    public function get_survey_array($eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $org_id = $this->get_event_oraganiser($eid);
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('eqs.*')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id', $eid);
        if ($user[0]->ask_survey == '1')
        {
            $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $org_id . ')');
        }
        else
        {
            $this->db->where('eq.exibitor_user_id', $ex_uid);
        }
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $dependentquestion = $this->db->get()->result_array();
        $idsarr = array_unique(array_filter(array_column_1($dependentquestion, "redirectexibitorquestion_id")));
        $masterqarr = array_filter(array_column_1($dependentquestion, "exibitor_question_id"));
        $dependent_ids = array_filter(array_column_1($dependentquestion, "redirectexibitorquestion_id"));
        $dependentquestionarr = array_combine($dependent_ids, $masterqarr);
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id', 'left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        if (count($idsarr) > 0)
        {
            $this->db->where_not_in('eq.q_id', $idsarr);
        }
        $this->db->where('eq.event_id', $eid);
        if ($user[0]->ask_survey == '1')
        {
            $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $org_id . ')');
        }
        else
        {
            $this->db->where('eq.exibitor_user_id', $ex_uid);
        }
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $independentquestion = $this->db->get()->result_array();
        $independent_ids = array_unique(array_filter(array_column_1($independentquestion, "q_id")));
        $res2['dependent_ids'] = $dependentquestionarr;
        $res2['independent_ids'] = $independent_ids;
        return $res2;
    }
    public function save_scan_lead_data($lead_data=NULL, $eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $this->db->select('*')->from('exibitor_lead');
        $this->db->where('event_id', $eid);
        $this->db->where('exibitor_user_id', $ex_uid);
        $this->db->where('lead_user_id', $uid);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where('event_id', $eid);
            $this->db->where('exibitor_user_id', $ex_uid);
            $this->db->where('lead_user_id', $uid);
            $this->db->update('exibitor_lead', $lead_data);
        }
        else
        {
            $lead_data['event_id'] = $eid;
            $lead_data['exibitor_user_id'] = $ex_uid;
            $lead_data['lead_user_id'] = $uid;
            $lead_data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('exibitor_lead', $lead_data);
        }
    }
    public function add_question_answer($question_data=NULL, $exqid=NULL, $uid=NULL, $eid=NULL)

    {
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('exibitor_questionid', $exqid);
        $this->db->where('user_id', $uid);
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where('exibitor_questionid', $exqid);
            $this->db->where('user_id', $uid);
            $this->db->where('event_id', $eid);
            $this->db->update('exibitor_question_answer', $question_data);
        }
        else
        {
            $question_data['user_id'] = $uid;
            $question_data['event_id'] = $eid;
            $question_data['exibitor_questionid'] = $exqid;
            $question_data['answer_date'] = date('Y-m-d H:i:s');
            $this->db->insert('exibitor_question_answer', $question_data);
        }
    }
    public function get_exibitor_by_representative_id_in_event($eid=NULL, $resp_uid=NULL)

    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id', $eid);
        $this->db->where('reps_user_id', $resp_uid);
        $res = $this->db->get()->row_array();
        return $res;
    }
    public function get_all_my_lead_by_exibitor_user($eid=NULL, $ex_uid=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('u.*,reu.Role_id,
                           reu.Organisor_id,
                           el.firstname as Firstname,
                           el.lastname as Lastname,
                           el.email as Email,
                           el.title as Title,
                           el.company_name as Company_name,
                           el.custom_column_data,
                           el.salutation,
                           el.country,
                           el.mobile,
                           el.badgeNumber,
                           concat(u1.firstname," ", u1.Lastname) as lead_rep,
                           el.created_date')->from('user u');
        $this->db->join('exibitor_lead el', 'el.lead_user_id=u.Id');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
        $this->db->join('user u1', 'u1.Id = el.rep_id', 'left');
        $this->db->where('el.event_id', $eid);
        $this->db->where('(el.exibitor_user_id=' . $ex_uid . ' or el.rep_id=' . $ex_uid . ')');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_all_my_lead_by_exibitor_user_test($eid=NULL, $ex_uid=NULL)

    {
        if ($eid == '1378')
        {
            $this->db->protect_identifiers = false;
            $this->db->select('u.*,reu.Role_id,
                               reu.Organisor_id,
                               el.firstname as Firstname,
                               el.lastname as Lastname,
                               el.email as Email,
                               el.title as Title,
                               el.company_name as Company_name,
                               el.custom_column_data,
                               el.salutation,
                               el.country,
                               el.mobile,
                               el.badgeNumber,
                               concat(u1.firstname," ", u1.Lastname) as lead_rep,
                               el.created_date,
                               (select group_concat(exibitor_questionid SEPARATOR "~~") from exibitor_question_answer eqa where eqa.user_id = u.Id and (exhi_id =  ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')) as exibitor_ques,
                               (select group_concat(Answer SEPARATOR "~~") from exibitor_question_answer eqa where eqa.user_id = u.Id and (exhi_id =  ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')) as exibitor_ans,
                               tl.a1,tl.a2,tl.state,tl.zip
                               ')->from('user u');
            $this->db->join('exibitor_lead el', 'el.lead_user_id=u.Id');
            $this->db->join('tmp_dig_lead tl', 'tl.lead_user_id = el.lead_user_id');
            $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
            $this->db->join('user u1', 'u1.Id = el.rep_id', 'left');
            $this->db->where('el.event_id', $eid);
            $this->db->where('(el.exibitor_user_id=' . $ex_uid . ' or el.rep_id=' . $ex_uid . ')');
            $res = $this->db->get()->result_array();
        }
        else
        {
            $this->db->protect_identifiers = false;
            $this->db->select('u.*,reu.Role_id,
                               reu.Organisor_id,
                               el.firstname as Firstname,
                               el.lastname as Lastname,
                               el.email as Email,
                               el.title as Title,
                               el.company_name as Company_name,
                               el.custom_column_data,
                               el.salutation,
                               el.country,
                               el.mobile,
                               el.badgeNumber,
                               concat(u1.firstname," ", u1.Lastname) as lead_rep,
                               el.created_date,
                               (select group_concat(exibitor_questionid SEPARATOR "~~") from exibitor_question_answer eqa where eqa.user_id = u.Id and (exhi_id =  ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')) as exibitor_ques,
                               (select group_concat(Answer SEPARATOR "~~") from exibitor_question_answer eqa where eqa.user_id = u.Id and (exhi_id =  ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')) as exibitor_ans
                               ')->from('user u');
            $this->db->join('exibitor_lead el', 'el.lead_user_id=u.Id');
            $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
            $this->db->join('user u1', 'u1.Id = el.rep_id', 'left');
            $this->db->where('el.event_id', $eid);
            $this->db->where('(el.exibitor_user_id=' . $ex_uid . ' or el.rep_id=' . $ex_uid . ')');
            $res = $this->db->get()->result_array();
        }
        return $res;
    }
    public function get_all_my_representative_by_exibitor_user($eid=NULL, $ex_uid=NULL)

    {
        $this->db->select('u.*,reu.Role_id,reu.Organisor_id')->from('user u');
        $this->db->join('exhibitor_representatives er', 'er.reps_user_id=u.Id');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
        $this->db->where('er.event_id', $eid);
        $this->db->where('er.exibitor_user_id', $ex_uid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_event_all_attendee_from_assing_resp($eid=NULL)

    {
        $this->db->select('u.*')->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        $this->db->where('u.Id NOT IN (select reps_user_id from exhibitor_representatives where event_id=' . $eid . ')');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function add_new_representative($eid=NULL, $ex_uid=NULL, $resp_uid=NULL)

    {
        $res = $this->get_exibitor_by_representative_id_in_event($eid, $resp_uid);
        if (empty($res))
        {
            $resp_data['event_id'] = $eid;
            $resp_data['exibitor_user_id'] = $ex_uid;
            $resp_data['reps_user_id'] = $resp_uid;
            $resp_data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_representatives', $resp_data);
        }
    }
    public function get_total_representative_by_exibitor_user($eid=NULL, $ex_uid=NULL)

    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id', $eid);
        $this->db->where('exibitor_user_id', $ex_uid);
        $res = $this->db->get()->result_array();
        return count($res);
    }
    public function get_exibitor_user_premission($eid=NULL, $ex_uid=NULL)

    {
        $this->db->select('*')->from('exibitor_premission');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $ex_uid);
        $res = $this->db->get()->row_array();
        return $res;
    }
    public function remove_my_representative($eid=NULL, $resp_uid=NULL)

    {
        $this->db->where('event_id', $eid);
        $this->db->where('reps_user_id', $resp_uid);
        $this->db->delete('exhibitor_representatives');
    }
    public function get_all_exibitor_user_questions_for_export($eid=NULL, $ex_uid=NULL)

    {
        $org_id = $this->get_event_oraganiser($eid);
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*')->from('exibitor_question eq');
        $this->db->where('eq.event_id', $eid);
        if ($user[0]->ask_survey == '1')
        {
            $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $org_id . ')');
        }
        else
        {
            $this->db->where('eq.exibitor_user_id', $ex_uid);
        }
        // $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_user_question_answer($eid=NULL, $uid=NULL, $qid=NULL, $user_id=NULL)

    {
        $rep_data = $this->get_lead_user_by_rep($eid, $user_id);
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $uid);
        $this->db->where('exhi_id', $rep_data['exibitor_user_id']);
        $this->db->where('exibitor_questionid', $qid);
        $res = $this->db->get()->result_array();
        if (count($res) == 0 && isset($rep_data['rep_id']))
        {
            $this->db->select('*')->from('exibitor_question_answer');
            $this->db->where('event_id', $eid);
            $this->db->where('user_id', $uid);
            $this->db->where('rep_id', $rep_data['rep_id']);
            $this->db->where('exibitor_questionid', $qid);
            $res = $this->db->get()->result_array();
        }
        return $res;
    }
    public function get_user_question_answer_all($eid=NULL, $uid=NULL, $user_id=NULL)

    {
        $rep_data = $this->get_lead_user_by_rep($eid, $user_id);
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $uid);
        $this->db->where('exhi_id', $rep_data['exibitor_user_id']);
        // $this->db->where('exibitor_questionid',$qid);
        $res = $this->db->get()->result_array();
        if (count($res) == 0 && isset($rep_data['rep_id']))
        {
            $this->db->select('*')->from('exibitor_question_answer');
            $this->db->where('event_id', $eid);
            $this->db->where('user_id', $uid);
            $this->db->where('rep_id', $rep_data['rep_id']);
            // $this->db->where('exibitor_questionid',$qid);
            $res = $this->db->get()->result_array();
        }
        foreach($res as $key => $value)
        {
            $tmp_key = $value['exibitor_questionid'];
            $tmp_res[$tmp_key] = $value['Answer'];
        }
        return $tmp_res;
    }
    public function get_lead_role($event_id=NULL)

    {
        $res = $this->db->where('event_id', $event_id)->where('role_type', '1')->get('role')->result_array();
        return $res;
    }
    public function addRole($create_data=NULL, $where=NULL)

    {
        $count = $this->db->select()->from('role')->where($where)->get()->num_rows();
        if ($count)
        {
            $result['success'] = false;
            $result['msg'] = $create_data['Name'] . " already exists.";
        }
        else
        {
            $this->db->insert('role', $create_data);
            $result['success'] = true;
        }
        return $result;
    }
    public function updateRole($update_data=NULL, $where=NULL)

    {
        $this->db->where($where);
        $this->db->update('role', $update_data);
    }
    public function deleteLeadUser($where=NULL)

    {
        $this->db->where($where);
        $this->db->delete('relation_event_user');
    }
    public function updateLeadUser($where=NULL, $update=NULL)

    {
        $this->db->where($where);
        $this->db->update('user', $update);
    }
    public function updateLeadUserRole($where=NULL, $update=NULL)

    {
        $this->db->where($where);
        $this->db->update('relation_event_user', $update);
    }
    public function deleteRole($where=NULL)

    {
        $this->db->where($where);
        $this->db->delete('role');
    }
    public function add($postarr=NULL, $eid=NULL, $Organisor_id=NULL, $role_name=NULL)

    {
        $role = $this->db->where('name', $role_name)->where('event_id', $eid)->get('role')->row_array();
        if ($role)
        {
            $userdata = $this->db->select('*')->from('user')->where('Email', $postarr['Email'])->get()->row_array();
            if (count($userdata) > 0)
            {
                $rel_data['User_id'] = $userdata['Id'];
                $check_org = $this->db->where($rel_data)->get('relation_event_user')->row_array();
                if ($check_org['Role_id'] == '3')
                {
                    $return['result'] = false;
                    $return['msg'] = "Oraganise can not be assign as Lead user";
                    return $return;
                }
                $rel_data['Event_id'] = $eid;
                $rel_data['Organisor_id'] = $Organisor_id;
                $relation = $this->db->where($rel_data)->get('relation_event_user')->row_array();
                $event_roles = ['4', '5', '6', '7'];
                if (count($relation) < 1)
                {
                    $rel_data['Role_id'] = $role['Id'];
                    $this->db->insert('relation_event_user', $rel_data);
                    $return['result'] = true;
                }
                elseif (!in_array($relation['Role_id'], $event_roles))
                {
                    $return['result'] = false;
                    $return['msg'] = "This user is already assigned as Lead User Or Representative.";
                    return $return;
                }
                elseif ($relation['Role_id'] != '3')
                {
                    unset($rel_data['Event_id']);
                    $this->db->where($relation);
                    $rel_data['Role_id'] = $role['Id'];
                    $this->db->update('relation_event_user', $rel_data);
                    $return['result'] = true;
                }
                elseif ($relation['Role_id'] == '3')
                {
                    $return['result'] = false;
                    $return['msg'] = "Oraganise can not be assign as Lead user";
                    return $return;
                }
                $this->db->where($userdata);
                $update['no_of_reps'] = $postarr['no_of_reps'];
                $update['Title'] = $postarr['Title'];
                $update['Company_name'] = $postarr['Company_name'];
                $new_pass = $this->generate_password();
                $update['lead_user_password_link'] = base_url() . "login/reset_password/" . $new_pass . "/lead";
                $this->db->update('user', $update);
                $this->load->model('Login_model');
                $this->Login_model->insertUserAttempts($userdata['Id'], 'reset_password', $new_pass);
            }
            else
            {
                $new_pass = $this->generate_password();
                $postarr['lead_user_password_link'] = base_url() . "login/reset_password/" . $new_pass . "/lead";
                $postarr['Active'] = '1';
                $postarr['Organisor_id'] = $Organisor_id;
                $postarr['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user', $postarr);
                $user_id = $this->db->insert_id();
                $this->load->model('Login_model');
                $this->Login_model->insertUserAttempts($user_id, 'reset_password', $new_pass);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $user_id;
                $relation = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->row_array();
                if (count($relation) < 1)
                {
                    $rel_data['Organisor_id'] = $Organisor_id;
                    $rel_data['Role_id'] = $role['Id'];
                    $this->db->insert('relation_event_user', $rel_data);
                }
                $return['result'] = true;
            }
            return $return;
        }
        else
        {
            $return['result'] = false;
            $return['msg'] = 'Error: Role - ' . $role_name . ' is not found.';
            return $return;
        }
    }
    public function generate_password()

    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 9; $i++)
        {
            $randomString.= $characters[rand(0, strlen($characters) - 1) ];
        }
        return $randomString;
    }
    public function getLeadUsers($id=NULL)

    {
        $res = $this->db->select('*')->from('user')->join('relation_event_user', 'relation_event_user.User_id = user.Id')->join('role', 'role.Id = relation_event_user.Role_id')->where('relation_event_user.Event_id', $id)->where('role.event_id', $id)->where('role.role_type', '1')->where('user.Id NOT IN (select rep_id from user_representatives where event_id=' . $id . ')')->get()->result_array();
        return $res;
    }
    public function get_lead_user_by_rep($event_id=NULL, $user_id=NULL)

    {
        $lead_user = array_column_1($this->getLeadUsers($event_id) , 'User_id');
        if (in_array($user_id, $lead_user))
        {
            $data['exibitor_user_id'] = $user_id;
        }
        else
        {
            $res = $this->db->where('rep_id', $user_id)->get('user_representatives')->row_array();
            $data['exibitor_user_id'] = $res['user_id'];
            $data['rep_id'] = $user_id;
        }
        return $data;
    }
}