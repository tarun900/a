<?php
class Badge_scanner_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    public function save_badge_scan_settings($data=NULL,$event_id=NULL)
    {   
        $badge_data['event_id'] = $event_id;
        $res=$this->db->select('*')->from('event_settings')->where($badge_data)->get()->result_array();

       if(!empty($res))
        {
            $this->db->where($badge_data);
            $this->db->update('event_settings',$data);
        }
        else
        {   
            $data['event_id'] = $event_id;
            $this->db->insert('event_settings',$data);
        }
        
    }
    public function get_save_badge_scan_settings($eid=NULL)
    {
        $res=$this->db->select('exibitor_scan_attendee_reward,attendee_scan_swap_contact')->from('event_settings')->where('event_id',$eid)->get()->result_array();
        return $res;
    }
}        
?>