<?php
class Map_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_eventinfo($id = null)

    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getmapdata_by_mapid($mid=NULL)

    {
        $this->db->select('*');
        $this->db->from('map');
        $this->db->where('Id', $mid);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function delete_region($reg_id=NULL)

    {
        $this->db->where('id', $reg_id);
        $this->db->delete('map_image_mapping');
    }
    public function add_map($data=NULL)

    {
        if (!empty($data['map_array']['Images']))
        {
            $images[] = $data['map_array']['Images'];
            $array_map['Images'] = json_encode($images);
        }
        if ($data['map_array']['Organizer_id'] != NULL) $array_map['Organizer_id'] = $data['map_array']['Organizer_id'];
        if ($data['map_array']['Event_id'] != NULL) $array_map['Event_id'] = $data['map_array']['Event_id'];
        if ($data['map_array']['Map_title'] != NULL) $array_map['Map_title'] = $data['map_array']['Map_title'];
        if ($data['map_array']['place'] != NULL) $array_map['place'] = $data['map_array']['place'];
        if ($data['map_array']['lat_long'] != NULL) $array_map['lat_long'] = $data['map_array']['lat_long'];
        if ($data['map_array']['zoom_level'] != NULL) $array_map['zoom_level'] = $data['map_array']['zoom_level'];
        if ($data['map_array']['Map_desc'] != NULL) $array_map['Map_desc'] = $data['map_array']['Map_desc'];
        if ($data['map_array']['Address'] != NULL) $array_map['Address'] = $data['map_array']['Address'];
        if ($data['map_array']['satellite_view'] != NULL) $array_map['satellite_view'] = $data['map_array']['satellite_view'];
        if ($data['map_array']['include_map'] != NULL) $array_map['include_map'] = $data['map_array']['include_map'];
        if ($data['map_array']['area'] != NULL) $array_map['area'] = $data['map_array']['area'];
        $array_map['map_u_id'] = substr(sha1(uniqid()) , 0, 5);
        $this->db->insert('map', $array_map);
        $map_id = $this->db->insert_id();
        return $map_id;
    }
    public function saveCoords($data=NULL)

    {
        $this->db->insert('map_image_mapping', $data);
        $id = $this->db->insert_id();
        return $id;
    }
    public function get_map_image_data_list($map_id=NULL)

    {
        $this->db->select('m.*,u.Firstname,u.Lastname,u.Email,u.Id,e.*,e.Id as exid');
        $this->db->from('map_image_mapping m');
        $this->db->join('user u', 'm.user_id=u.Id', 'left');
        $this->db->join('exibitor e', 'e.user_id=u.Id', 'left');
        $this->db->where('m.map_id', $map_id);
        $query = $this->db->get();
        $res = $query->result_array();
        foreach($res as $key => $value)
        {
            if (!empty($value['session_id']))
            {
                $session_id = explode(",", $value['session_id']);
                foreach($session_id as $skey => $svalue)
                {
                    $this->db->select('Id as sid,Heading as session_heading,Start_date,Start_time')->from('agenda');
                    $this->db->where('Id', $svalue);
                    $squery = $this->db->get();
                    $sres = $squery->result_array();
                    $res[$key]['session'][$svalue] = $sres[0];
                }
            }
        }
        return $res;
    }
    public function get_maps($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('map m');
        $this->db->where('m.Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_map_list($id = null)

    {
        $mid = $this->uri->segment(4);
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('map m');
        if ($orid)
        {
            // $this->db->where('m.Organizer_id',$orid);
        }
        $this->db->where('m.Event_id', $id);
        $this->db->where('m.Id', $mid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res) redirect('Forbidden');
        return $res;
    }
    public function getarea($id=NULL, $mid=NULL)

    {
        $this->db->select('*');
        $this->db->from('map');
        $this->db->where('Id', $mid);
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function update_map($data=NULL, $map_id=NULL)

    {
        if (!empty($data['map_array']['Images']))
        {
            $images[] = $data['map_array']['Images'];
            $array_map['Images'] = json_encode($images);
        }
        if ($data['map_array']['Organizer_id'] != NULL)
        {
            $array_map['Organizer_id'] = $data['map_array']['Organizer_id'];
        }
        if ($data['map_array']['Map_title'] != NULL)
        {
            $array_map['Map_title'] = $data['map_array']['Map_title'];
        }
        if ($data['map_array']['Map_desc'] != NULL)
        {
            $array_map['Map_desc'] = $data['map_array']['Map_desc'];
        }
        if ($data['map_array']['place'] != NULL)
        {
            $array_map['place'] = $data['map_array']['place'];
        }
        if ($data['map_array']['lat_long'] != NULL)
        {
            $array_map['lat_long'] = $data['map_array']['lat_long'];
        }
        if ($data['map_array']['zoom_level'] != NULL)
        {
            $array_map['zoom_level'] = $data['map_array']['zoom_level'];
        }
        if ($data['map_array']['Event_id'] != NULL)
        {
            $array_map['Event_id'] = $data['map_array']['Event_id'];
        }
        if ($data['map_array']['Address'] != NULL)
        {
            $array_map['Address'] = $data['map_array']['Address'];
        }
        if ($data['map_array']['satellite_view'] != NULL) $array_map['satellite_view'] = $data['map_array']['satellite_view'];
        if ($data['map_array']['include_map'] != NULL) $array_map['include_map'] = $data['map_array']['include_map'];
        $array_map['area'] = $data['map_array']['area'];
        $this->db->where('Id', $map_id);
        $this->db->update('map', $array_map);
        $event_id = $this->db->insert_id();
        return $event_id;
    }
    public function delete_map($id=NULL, $Event_id = '')

    {
        $this->db->where('Id', $id);
        $this->db->delete('map');
        $str = $this->db->last_query();
        if ($Event_id != '')
        {
            $a_cat = $this->db->select('*')->from('map')->where('Event_id', $Event_id)->order_by('updated_date', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('Id', $a_cat['Id'])->update('map', $update);
        }
    }
    public function checktitle($map_title=NULL, $id = null, $eid=NULL, $rid=NULL)

    {
        $this->db->select('Id,Map_title');
        $this->db->from('map');
        if ($Map_title)
        {
            $this->db->where('Map_title !=', $Map_title);
        }
        $this->db->where('Map_title', $map_title);
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            if ($rid == $res[0]['Id'])
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    public function change_dwg_files_status_by_map($mid=NULL)

    {
        $this->db->select('check_dwg_files')->from('map');
        $this->db->where('Id', $mid);
        $query = $this->db->get();
        $res = $query->result_array();
        $mapdata['check_dwg_files'] = $res[0]['check_dwg_files'] == '1' ? '0' : '1';
        $this->db->where('Id', $mid);
        $this->db->update('map', $mapdata);
        return $this->db->affected_rows();
    }
    public function get_all_meeting_location_list($event_id=NULL)

    {
        $this->db->select('*')->from('meeting_location');
        $this->db->where('event_id', $event_id);
        $this->db->where('location_id NOT IN(select location_id from map_image_mapping where location_id IS NOT NULL)');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
}
?>