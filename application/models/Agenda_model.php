<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Agenda_model extends CI_Model

{
     function __construct()
     {
          parent::__construct();
     }
     public function getAllpresentationByEventId($event_id=NULL)

     {
          $this->db->select('Id,Heading');
          $this->db->from('presentation');
          $this->db->where('Event_id', $event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function getAllDocumentonByEventId($event_id=NULL)

     {
          $this->db->select('doc.id,doc.title');
          $this->db->from('documents doc');
          $this->db->join('document_files df', 'df.document_id = doc.Id');
          $this->db->where('Event_id', $event_id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function getforms($eid=NULL, $mid=NULL)

     {
          $query = $this->db->query("select id,`json_data`,frm_posi from `forms` where FIND_IN_SET('$mid',m_id) and event_id='$eid' ");
          $res = $query->result_array();
          return $res;
     }
     public function getCmsforms($eid=NULL, $mid=NULL)

     {
          $query = $this->db->query("select id,`json_data`,frm_posi from `forms` where FIND_IN_SET('$mid',cms_id) and event_id='$eid' ");
          $res = $query->result_array();
          return $res;
     }
     public function formsinsert(array $formdata)

     {
          // print_r($formdata);die;
          $this->db->insert('form_data', $formdata);
     }
     public function check_auth($title=NULL, $roleid=NULL, $rolename=NULL, $eventid=NULL)

     {
          // echo $title;die;
          if ($rolename == 'Client')
          {
               return 1;
          }
          else
          {
               $this->db->select('id');
               $this->db->from('menu');
               $this->db->where('pagetitle', $title);
               $query = $this->db->get();
               // echo $this->db->last_query(); exit();
               $res = $query->result_array();
               $menuid = $res[0]['id'];
               $this->db->select('Menu_id');
               $this->db->from('role_permission');
               $this->db->where('Role_id', $roleid);
               $this->db->where('Menu_id', $menuid);
               $this->db->where('Event_id', $eventid);
               $query1 = $this->db->get();
               $res1 = $query1->result_array();
               if (count($res1) >= 1)
               {
                    return 1;
               }
               else
               {
                    return 0;
               }
          }
     }
     public function check_access($user_id=NULL, $event_id=NULL)

     {
          $this->db->select('');
          $this->db->from('relation_event_user');
          $this->db->where('Event_id', $event_id);
          $this->db->where('User_id', $user_id);
          $query = $this->db->get();
          $res = $query->result_array();
          $cnt = count($res);
          return $cnt;
     }
     public function update_users_agenda($id=NULL, $data=NULL)

     {
          $this->db->where('user_id', $id);
          $this->db->update("users_agenda", $data);
          $this->db->select('*');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          if ($res[0]['pending_agenda_id'] == '' && $res[0]['agenda_id'] == '' && $res[0]['check_in_agenda_id'] == '')
          {
               $this->db->where('user_id', $id);
               $this->db->delete('users_agenda');
          }
     }
     public function add_users_agenda($data=NULL)

     {
          $this->db->insert("users_agenda", $data);
     }
     public function exit_user_agenda($id=NULL)

     {
          $this->db->select('');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          $cnt = count($res);
          return $cnt;
     }
     public function get_agenda_list_user_wise($id=NULL)

     {
          $this->db->select('');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_all_agenda_by_user($agend_id_arr=NULL)

     {
          $this->db->select('');
          $this->db->from('agenda a');
          $this->db->where_in('a.Id', $agend_id_arr);
          $this->db->join("map m", "m.Id=a.Address_map");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function add_agenda_category($data=NULL, $eventid=NULL)

     {
          $data['event_id'] = $eventid;
          $this->db->insert('agenda_categories', $data);
          return $this->db->insert_id();
     }
     public function update_agenda_category($data=NULL, $cid=NULL)

     {
          $this->db->where('Id', $cid);
          $this->db->update('agenda_categories', $data);
          return $cid;
     }
     public function delete_agenda_category_by_id($cid=NULL, $eventid = '')

     {
          $this->db->where('Id', $cid);
          $this->db->delete('agenda_categories');
          $a_cat = $this->db->select('*')->from('agenda_categories')->where('event_id', $eventid)->order_by('updated_date', 'DESC')->get()->row_array();
          $update['updated_date'] = date('Y-m-d H:i:s');
          $this->db->where('Id', $a_cat['Id'])->update('agenda_categories', $update);
     }
     public function add_existing_agenda($relation_data=NULL)

     {
          $this->db->insert('agenda_category_relation', $relation_data);
     }
     public function add_agenda($data=NULL, $Event_id=NULL, $cid = null)

     {
          $Speakers = $this->Speaker_model->get_speaker_list();
          $this->data['Speakers'] = $Speakers;
          $array_agenda['show_places_remaining'] = $data['agenda_array']['show_places_remaining'];
          $array_agenda['show_checking_in'] = $data['agenda_array']['show_checking_in'];
          $array_agenda['allow_clashing'] = $data['agenda_array']['allow_clashing'];
          $array_agenda['show_rating'] = $data['agenda_array']['show_rating'];
          $array_agenda['allow_comments'] = $data['agenda_array']['allow_comments'];
          if ($data['agenda_array']['Organisor_id'] != NULL) $array_agenda['Organisor_id'] = $data['agenda_array']['Organisor_id'];
          if ($data['agenda_array']['Event_id'] != NULL) $array_agenda['Event_id'] = $data['agenda_array']['Event_id'];
          if ($data['agenda_array']['Start_date'] != NULL) $array_agenda['Start_date'] = $data['agenda_array']['Start_date'];
          if ($data['agenda_array']['Start_time'] != NULL) $array_agenda['Start_time'] = $data['agenda_array']['Start_time'];
          if ($data['agenda_array']['End_date'] != NULL) $array_agenda['End_date'] = $data['agenda_array']['End_date'];
          if ($data['agenda_array']['checking_datetime'] != NULL) $array_agenda['checking_datetime'] = $data['agenda_array']['checking_datetime'];
          if ($data['agenda_array']['End_time'] != NULL) $array_agenda['End_time'] = $data['agenda_array']['End_time'];
          if ($data['agenda_array']['Heading'] != NULL) $array_agenda['Heading'] = $data['agenda_array']['Heading'];
          if ($data['agenda_array']['Agenda_status'] != NULL) $array_agenda['Agenda_status'] = $data['agenda_array']['Agenda_status'];
          if ($data['agenda_array']['Maximum_People'] != NULL) $array_agenda['Maximum_People'] = $data['agenda_array']['Maximum_People'];
          if ($data['agenda_array']['custom_speaker_name'] != NULL) $array_agenda['custom_speaker_name'] = $data['agenda_array']['custom_speaker_name'];
          if ($data['agenda_array']['custom_location'] != NULL) $array_agenda['custom_location'] = $data['agenda_array']['custom_location'];
          if ($data['agenda_array']['Speaker_id'] != NULL) $array_agenda['Speaker_id'] = $data['agenda_array']['Speaker_id'];
          if ($data['agenda_array']['Types'] != NULL) $array_agenda['Types'] = $data['agenda_array']['Types'];
          if ($data['agenda_array']['short_desc'] != NULL) $array_agenda['short_desc'] = $data['agenda_array']['short_desc'];
          if ($data['agenda_array']['description'] != NULL) $array_agenda['description'] = $data['agenda_array']['description'];
          if ($data['agenda_array']['other_types'] != NULL) $array_agenda['other_types'] = $data['agenda_array']['other_types'];
          if ($data['agenda_array']['Address_map'] != NULL) $array_agenda['Address_map'] = $data['agenda_array']['Address_map'];
          if ($data['agenda_array']['document_id'] != NULL) $array_agenda['document_id'] = $data['agenda_array']['document_id'];
          if ($data['agenda_array']['presentation_id'] != NULL) $array_agenda['presentation_id'] = $data['agenda_array']['presentation_id'];
          if (array_key_exists('qasession_id', $data['agenda_array']))
          {
               $array_agenda['qasession_id'] = $data['agenda_array']['qasession_id'];
          }
          if (array_key_exists('session_image', $data['agenda_array']))
          {
               $array_agenda['session_image'] = $data['agenda_array']['session_image'];
          }
          if (array_key_exists('survey_id', $data['agenda_array']))
          {
               $array_agenda['survey_id'] = $data['agenda_array']['survey_id'];
          }
          if ($data['agenda_array']['sort_order'] != NULL)
          {
               $array_agenda['sort_order'] = $data['agenda_array']['sort_order'];
          }
          $array_agenda['session_tracks'] = $data['agenda_array']['session_type'];
          $array_agenda['created_date'] = date('Y-m-d H:i:s');
          $array_agenda['agenda_code'] = substr(sha1(uniqid()) , 0, 6);
          $this->db->insert('agenda', $array_agenda);
          $agenda_id = $this->db->insert_id();
          if (!empty($cid))
          {
               $relation_data['agenda_id'] = $agenda_id;
               $relation_data['category_id'] = $cid;
               $this->add_existing_agenda($relation_data);
          }
          $arr = explode(',', $array_agenda['Speaker_id']);
          $i = 0;
          foreach($arr as $key => $spea_id)
          {
               // if ($data['array_speakers_events']['Speech_id'] != NULL)
               // $array_speakers_events['Speech_id'] = $data['agenda_speakers_events']['Speech_id'];
               $array_speakers_events[$i]['Agenda_id'] = $agenda_id;
               $array_speakers_events[$i]['Event_id'] = $Event_id;
               $array_speakers_events[$i]['Speaker_id'] = $spea_id;
               $i++;
          }
          $this->db->where('Agenda_id', $agenda_id);
          $this->db->insert_batch('speakers_agenda', $array_speakers_events);
          return $agenda_id;
     }
     public function get_all_agenda_list($id = null, $cid=NULL)

     {
          $total_permission = $this->Agenda_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;
          $orid = $this->data['user']->Id;
          $this->db->select('a.*,group_concat(rt.ticket_name) as ticket_type,st.type_name as Types');
          $this->db->from('agenda a');
          $this->db->join('session_types st', 'st.type_id=a.Types', 'left');
          $this->db->join('agenda_category_relation acr', 'acr.agenda_id=a.Id', 'right');
          $this->db->join('agenda_ticket at', 'a.Id=at.agenda_id', 'left');
          $this->db->join('reg_tickets rt', 'at.ticket_id=rt.id', 'left');
          $this->db->where('acr.category_id', $cid);
          $this->db->where('a.Event_id', $id);
          $this->db->order_by("a.Start_date", "asc");
          $this->db->group_by('a.Id');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_all_agenda_list_for_add_existing($id=NULL)

     {
          $total_permission = $this->Agenda_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;
          $orid = $this->data['user']->Id;
          $this->db->select('a.*');
          $this->db->from('agenda a');
          $this->db->where('Event_id', $id);
          $this->db->order_by("Start_date", "asc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_all_agenda_category_list($id=NULL)

     {
          $this->db->select('ac.category_name,ac.show_check_in_time,ac.category_image,ac.created_date,ac.event_id,count(acr.agenda_id) as total_agenda,ac.Id as cid,acr.Id as category_relation_id,acr.agenda_id,acr.category_id,ac.categorie_type')->from('agenda_categories ac');
          $this->db->join('agenda_category_relation acr', 'acr.category_id=ac.Id', 'left');
          $this->db->where('ac.event_id', $id);
          $this->db->group_by('ac.Id');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_agenda_category_by_name($cnm=NULL, $eventid=NULL, $cid=NULL)

     {
          $this->db->select('*')->from('agenda_categories');
          $this->db->where('category_name', $cnm);
          $this->db->where('event_id', $eventid);
          if (!empty($cid))
          {
               $this->db->where('Id !=', $cid);
          }
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res;
     }
     public function get_agenda_category_by_id($cid=NULL)

     {
          $this->db->select('*')->from('agenda_categories');
          $this->db->where('Id', $cid);
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res;
     }
     public function get_agenda_list($id = null)

     {
          $total_permission = $this->Agenda_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;
          $aid = $this->uri->segment(4);
          $orid = $this->data['user']->Id;
          $this->db->select('a.Id as aid,a.*,m.Id as Address_map_id,m.*,u.*,pre.Id as presentation_id,pre.Heading as pHeading,doc.id as document_id,doc.title as doc_title,sn.*');
          $this->db->from('agenda a');
          $this->db->join('user u', 'u.Id = a.speaker_id', 'left');
          $this->db->join('map m', 'm.Id = a.Address_map', 'left');
          $this->db->join('presentation pre', 'pre.Id = a.presentation_id', 'left');
          $this->db->join('documents doc', 'doc.id = a.document_id', 'left');
          $this->db->join('session_notification sn', 'sn.agenda_id = a.Id', 'left');
          $this->db->where('a.Event_id', $id);
          $this->db->where('a.Id', $aid);
          $query = $this->db->get();
          $res = $query->result_array();
          if (!$res && $this->uri->segment(2) == 'edit') return redirect('Forbidden');
          return $res;
     }
     public function update_agenda($data=NULL, $agenda_id=NULL, $Event_id=NULL)

     {
          $Speakers = $this->Speaker_model->get_speaker_list();
          $this->data['Speakers'] = $Speakers;
          $array_agenda['show_places_remaining'] = $data['agenda_array']['show_places_remaining'];
          $array_agenda['show_checking_in'] = $data['agenda_array']['show_checking_in'];
          $array_agenda['allow_clashing'] = $data['agenda_array']['allow_clashing'];
          $array_agenda['allow_comments'] = $data['agenda_array']['allow_comments'];
          $array_agenda['show_rating'] = $data['agenda_array']['show_rating'];
          if ($data['agenda_array']['Datetime'] != NULL) $array_agenda['Datetime'] = $data['agenda_array']['Datetime'];
          if ($data['agenda_array']['Start_date'] != NULL) $array_agenda['Start_date'] = $data['agenda_array']['Start_date'];
          if ($data['agenda_array']['Start_time'] != NULL) $array_agenda['Start_time'] = $data['agenda_array']['Start_time'];
          if ($data['agenda_array']['End_date'] != NULL) $array_agenda['End_date'] = $data['agenda_array']['End_date'];
          if ($data['agenda_array']['End_time'] != NULL) $array_agenda['End_time'] = $data['agenda_array']['End_time'];
          if ($data['agenda_array']['checking_datetime'] != NULL) $array_agenda['checking_datetime'] = $data['agenda_array']['checking_datetime'];
          if ($data['agenda_array']['Heading'] != NULL) $array_agenda['Heading'] = $data['agenda_array']['Heading'];
          if ($data['agenda_array']['Types'] != NULL) $array_agenda['Types'] = $data['agenda_array']['Types'];
          if ($data['agenda_array']['other_types'] != NULL) $array_agenda['other_types'] = $data['agenda_array']['other_types'];
          if ($data['agenda_array']['short_desc'] != NULL) $array_agenda['short_desc'] = $data['agenda_array']['short_desc'];
          if ($data['agenda_array']['description'] != NULL) $array_agenda['description'] = $data['agenda_array']['description'];
          if ($data['agenda_array']['Agenda_status'] != NULL) $array_agenda['Agenda_status'] = $data['agenda_array']['Agenda_status'];
          $array_agenda['Maximum_People'] = $data['agenda_array']['Maximum_People'];
          $array_agenda['custom_speaker_name'] = $data['agenda_array']['custom_speaker_name'];
          $array_agenda['custom_location'] = $data['agenda_array']['custom_location'];
          $array_agenda['Speaker_id'] = $data['agenda_array']['Speaker_id'];
          if ($data['agenda_array']['presentation_id'] == "New")
          {
               $array_agenda['presentation_id'] = NULL;
          }
          else if ($data['agenda_array']['presentation_id'] == "")
          {
               $array_agenda['presentation_id'] = NULL;
          }
          else
          {
               $array_agenda['presentation_id'] = $data['agenda_array']['presentation_id'];;
          }
          if ($data['agenda_array']['document_id'] == "New")
          {
               $array_agenda['document_id'] = NULL;
          }
          else if ($data['agenda_array']['document_id'] == "")
          {
               $array_agenda['document_id'] = NULL;
          }
          else
          {
               $array_agenda['document_id'] = $data['agenda_array']['document_id'];
          }
          /* echo "<pre>";
          print_r($array_agenda);die;*/
          if ($data['agenda_array']['Address_map'] == "")
          {
               $array_agenda['Address_map'] = NULL;
          }
          else
          {
               $array_agenda['Address_map'] = $data['agenda_array']['Address_map'];
          }
          if (array_key_exists('qasession_id', $data['agenda_array']))
          {
               $array_agenda['qasession_id'] = $data['agenda_array']['qasession_id'];
          }
          if (array_key_exists('session_image', $data['agenda_array']))
          {
               $array_agenda['session_image'] = $data['agenda_array']['session_image'];
          }
          if (array_key_exists('survey_id', $data['agenda_array']))
          {
               $array_agenda['survey_id'] = $data['agenda_array']['survey_id'];
          }
          if ($data['agenda_array']['sort_order'] != NULL)
          {
               $array_agenda['sort_order'] = $data['agenda_array']['sort_order'];
          }
          $array_agenda['session_tracks'] = $data['agenda_array']['session_tracks'];
          $this->db->where('Id', $data['agenda_array']['Id']);
          $this->db->update('agenda', $array_agenda);
          $arr = explode(',', $array_agenda['Speaker_id']);
          $i = 0;
          foreach($arr as $key => $spea_id)
          {
               // if ($data['array_speakers_events']['Speech_id'] != NULL)
               // $array_speakers_events['Speech_id'] = $data['agenda_speakers_events']['Speech_id'];
               $array_speakers_events[$i]['Agenda_id'] = $agenda_id;
               $array_speakers_events[$i]['Event_id'] = $Event_id;
               $array_speakers_events[$i]['Speaker_id'] = $spea_id;
               $i++;
          }
          $this->db->where('Agenda_id', $agenda_id);
          $this->db->delete('speakers_agenda');
          $this->db->insert_batch('speakers_agenda', $array_speakers_events);
     }
     public function get_permission_list()

     {
          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $this->db->select('*');
               $this->db->from('user');
               if ($orid)
               {
                    $this->db->where('user.Id', $orid);
               }
               $query = $this->db->get();
               $res = $query->result();
               return $res;
          }
     }
     public function delete_agenda($id=NULL, $cid=NULL, $Event_id = '')

     {
          $this->db->where('Id', $id);
          $this->db->delete('agenda');
          $str = $this->db->last_query();
          if ($Event_id != '')
          {
               $a_cat = $this->db->select('*')->from('agenda')->where('Event_id', $Event_id)->order_by('updated_date', 'DESC')->get()->row_array();
               $update['updated_date'] = date('Y-m-d H:i:s');
               $this->db->where('Id', $a_cat['Id'])->update('agenda', $update);
          }
          $this->db->where('category_id', $cid);
          $this->db->where('agenda_id', $id);
          $this->db->delete('agenda_category_relation');
     }
     public function get_menu_name($id=NULL)

     {
          $orid = $this->data['user']->Id;
          $this->db->select('*');
          $this->db->from('menu_Setting');
          $this->db->where('Event_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_how_many_time_save_agenda_by_id($aid = null, $user_id = null)

     {
          $this->db->select('*')->from('users_agenda');
          if (!empty($aid))
          {
               $where = "FIND_IN_SET($aid,`agenda_id`) > 0";
               $this->db->where($where);
          }
          if (!empty($user_id))
          {
               $this->db->where('user_id', $user_id);
          }
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res;
     }
     public function get_overlapping_agenda($data=NULL, $user_id=NULL, $on_pending=NULL)

     {
          $this->db->select('*')->from('users_agenda ua');
          $this->db->where('ua.user_id', $user_id);
          $qu = $this->db->get();
          $res = $qu->result_array();
          if (count($res) > 0)
          {
               if ($on_pending == '1')
               {
                    $agenda_id = explode(",", $res[0]['pending_agenda_id']);
               }
               else
               {
                    $agenda_id = explode(",", $res[0]['agenda_id']);
               }
               foreach($agenda_id as $key => $value)
               {
                    if (!empty($value))
                    {
                         $this->db->select('*')->from('agenda');
                         $dt = $data[0]['Start_date'] . ' ' . $data[0]['Start_time'];
                         $et = $data[0]['End_date'] . ' ' . $data[0]['End_time'];
                         $wherecon = "(('" . $dt . "' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)) OR ('" . $et . "' BETWEEN concat(Start_date,' ',Start_time) AND concat(End_date,' ',End_time)))";
                         // $wherecon="End_date = ".$data[0]['Start_date']." AND End_date= ".$data[0]['End_date'];
                         // "Start_date",$data[0]['Start_date']
                         $this->db->where($wherecon);
                         // $this->db->where("(('".$data[0]['Start_time']."' BETWEEN `Start_time` AND `End_time`) OR (Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                         // $this->db->or_where("('".$data[0]['End_time']."' BETWEEN `Start_time` AND `End_time`) OR (End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."'))");
                         $this->db->where('allow_clashing', '0');
                         $this->db->where('Id !=', $data[0]['Id']);
                         $this->db->where('Id', $value);
                         $qu1 = $this->db->get();
                         $res1 = $qu1->result_array();
                         if (count($res1) > 0)
                         {
                              $return = false;
                              break;
                         }
                         else
                         {
                              $return = true;
                         }
                    }
                    else
                    {
                         $return = true;
                    }
               }
          }
          else
          {
               $return = true;
          }
          return $return;
     }
     public function user_agenda_check_in_process($aid=NULL, $user_id=NULL)

     {
          $cnt = $this->exit_user_agenda($user_id);
          if ($cnt > 0)
          {
               $this->db->select('*')->from('users_agenda');
               $where = "FIND_IN_SET($aid,`check_in_agenda_id`) > 0";
               $this->db->where($where);
               $this->db->where('user_id', $user_id);
               $qu = $this->db->get();
               $res = $qu->result_array();
               $checkin_id = explode(",", $res[0]['check_in_agenda_id']);
               if (count($res) > 0)
               {
                    unset($checkin_id[array_search($aid, $checkin_id) ]);
                    $chekc = implode(",", $checkin_id);
                    $this->db->where('user_id', $user_id);
                    $this->db->update('users_agenda', array(
                         'check_in_agenda_id' => $chekc
                    ));
                    $this->db->where('user_id', $user_id);
                    $this->db->where('agenda_id', $aid);
                    $this->db->delete('user_check_in');
               }
               else
               {
                    $this->db->select('*')->from('users_agenda');
                    $this->db->where('user_id', $user_id);
                    $qu = $this->db->get();
                    $res = $qu->result_array();
                    $checkin_id = explode(",", $res[0]['check_in_agenda_id']);
                    $checkin_id[count($checkin_id) ] = $aid;
                    $chekc = implode(",", $checkin_id);
                    $this->db->where('user_id', $user_id);
                    $this->db->update('users_agenda', array(
                         'check_in_agenda_id' => $chekc
                    ));
                    $chek_date['user_id'] = $user_id;
                    $chek_date['agenda_id'] = $aid;
                    $chek_date['date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user_check_in', $chek_date);
               }
          }
          else
          {
               $this->db->insert('users_agenda', array(
                    'user_id' => $user_id,
                    'check_in_agenda_id' => ',' . $aid
               ));
          }
     }
     public function save_user_rating($sid=NULL, $uid=NULL, $rating=NULL)

     {
          $this->db->select('*')->from('user_session_rating');
          $this->db->where('session_id', $sid);
          $this->db->where('user_id', $uid);
          $qu = $this->db->get();
          $res = $qu->result_array();
          if (count($res) > 0)
          {
               $this->db->where('session_id', $sid);
               $this->db->where('user_id', $uid);
               $this->db->update('user_session_rating', array(
                    'rating' => $rating,
                    'date' => date('Y-m-d H:i:s')
               ));
          }
          else
          {
               $rating_array['session_id'] = $sid;
               $rating_array['user_id'] = $uid;
               $rating_array['rating'] = $rating;
               $rating_array['date'] = date('Y-m-d H:i:s');
               $this->db->insert('user_session_rating', $rating_array);
          }
     }
     public function get_save_rating($sid=NULL)

     {
          $login_user = $this->session->userdata('current_user');
          $uid = $login_user[0]->Id;
          $this->db->select('*')->from('user_session_rating');
          $this->db->where('session_id', $sid);
          $this->db->where('user_id', $uid);
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res;
     }
     public function get_prev_session_rating($aid=NULL, $cdate=NULL)

     {
          $user = $this->session->userdata('current_user');
          $user_id = $user[0]->Id;
          $savesession = $this->get_agenda_list_user_wise($user_id);
          $sid = array_filter(explode(",", $savesession[0]['agenda_id']));
          $akey = array_search($aid, $sid);
          foreach($sid as $key => $value)
          {
               if ($akey > 0)
               {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id', $value);
                    $savesession = $this->db->get();
                    $sres = $savesession->result_array();
                    $End_date = date("Y-m-d H:i:s", strtotime($sres[0]['End_date'] . ' ' . $sres[0]['End_time']));
                    if ($cdate > $End_date)
                    {
                         if ($value != $aid)
                         {
                              $this->db->select('*')->from('user_session_rating');
                              $this->db->where('user_id', $user_id);
                              $this->db->where('session_id', $value);
                              $qu = $this->db->get();
                              $rating = $qu->result_array();
                              if (count($rating) <= 0)
                              {
                                   $this->db->select('*')->from('agenda');
                                   $this->db->where('Id', $value);
                                   $psession = $this->db->get();
                                   $pres = $psession->result_array();
                                   if (count($pres) > 0)
                                   {
                                        break;
                                   }
                              }
                         }
                         else
                         {
                              unset($sid[array_search($aid, $sid) ]);
                         }
                    }
                    else
                    {
                         $rating[0]['Heading'] = "Session Not End";
                    }
               }
               else
               {
                    $rating[0]['Heading'] = "First Session";
               }
          }
          if (count($sid) > 0)
          {
               return $rating;
          }
          else
          {
               $rating[0]['Heading'] = "First Session";
               return $rating;
          }
     }
     public function get_prev_session($aid=NULL, $cdate=NULL)

     {
          $user = $this->session->userdata('current_user');
          $user_id = $user[0]->Id;
          $savesession = $this->get_agenda_list_user_wise($user_id);
          $sid = array_filter(explode(",", $savesession[0]['agenda_id']));
          $akey = array_search($aid, $sid);
          foreach($sid as $key => $value)
          {
               if ($akey > 0)
               {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id', $value);
                    $savesession = $this->db->get();
                    $sres = $savesession->result_array();
                    $End_date = date("Y-m-d H:i:s", strtotime($sres[0]['End_date'] . ' ' . $sres[0]['End_time']));
                    if ($cdate > $End_date)
                    {
                         if ($value != $aid)
                         {
                              $this->db->select('*')->from('user_session_rating');
                              $this->db->where('user_id', $user_id);
                              $this->db->where('session_id', $value);
                              $qu = $this->db->get();
                              $rating = $qu->result_array();
                              if (count($rating) <= 0)
                              {
                                   $this->db->select('*')->from('agenda');
                                   $this->db->where('Id', $value);
                                   $psession = $this->db->get();
                                   $pres = $psession->result_array();
                                   if (count($pres) > 0)
                                   {
                                        break;
                                   }
                              }
                         }
                    }
                    else
                    {
                         $pres[0]['Heading'] = "Session Not End";
                    }
               }
               else
               {
                    $pres[0]['Heading'] = "First Session";
               }
          }
          return $pres;
     }
     public function remove_from_save_my_agenda($agenda_id=NULL, $user_id=NULL)

     {
          $this->db->select('*')->from('users_agenda');
          $this->db->where('user_id', $user_id);
          $qu = $this->db->get();
          $res = $qu->result_array();
          $said = explode(",", $res[0]['agenda_id']);
          unset($said[array_search($agenda_id, $said) ]);
          $save_agenda['agenda_id'] = implode(",", $said);
          $this->db->where('user_id', $user_id);
          $this->db->update('users_agenda', $save_agenda);
     }
     public function book_all_pending_agenda($user_id=NULL, $time_format=NULL)

     {
          $msg = array();
          $this->db->select('*')->from('users_agenda');
          $this->db->where('user_id', $user_id);
          $qu = $this->db->get();
          $res = $qu->result_array();
          $paid = array_filter(explode(",", $res[0]['pending_agenda_id']));
          $pending_id = $paid;
          $agenda_id = array_filter(explode(",", $res[0]['agenda_id']));
          $said = $agenda_id;
          if (count($agenda_id) > 0)
          {
               foreach($paid as $pkey => $pvalue)
               {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id', $pvalue);
                    $dqu = $this->db->get();
                    $data = $dqu->result_array();
                    if (!in_array($pvalue, $agenda_id))
                    {
                         foreach($agenda_id as $key => $value)
                         {
                              if ($data[0]['allow_clashing'] == '0')
                              {
                                   $this->db->select('*')->from('agenda');
                                   $this->db->where('Start_date', $data[0]['Start_date']);
                                   $this->db->where("(Start_time BETWEEN '" . $data[0]['Start_time'] . "' AND '" . $data[0]['End_time'] . "')");
                                   $this->db->where("(End_time BETWEEN '" . $data[0]['Start_time'] . "' AND '" . $data[0]['End_time'] . "')");
                                   $this->db->where('allow_clashing', '0');
                                   $this->db->where('Id', $value);
                                   $qu1 = $this->db->get();
                                   $res1 = $qu1->result_array();
                              }
                              if (count($res1) > 0)
                              {
                                   $msg[$pkey]['Heading'] = $data[0]['Heading'];
                                   $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                                   $msg[$pkey]['book'] = '0';
                                   $msg[$pkey]['resion'] = "Session Clash";
                                   $return = false;
                                   break;
                              }
                              else
                              {
                                   if (!empty($data[0]['Maximum_People']))
                                   {
                                        $this->db->select('*')->from('users_agenda');
                                        $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                                        $this->db->where($where);
                                        $tqu = $this->db->get();
                                        $total = $tqu->result_array();
                                        $placeleft = $data[0]['Maximum_People'] - count($total);
                                   }
                                   else
                                   {
                                        $placeleft = 1;
                                   }
                                   if ($placeleft > 0)
                                   {
                                        $return = true;
                                   }
                                   else
                                   {
                                        $msg[$pkey]['Heading'] = $data[0]['Heading'];
                                        $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                                        $msg[$pkey]['book'] = '0';
                                        $msg[$pkey]['resion'] = "No Spaces Left";
                                        $return = false;
                                        break;
                                   }
                              }
                         }
                         if ($return == "true")
                         {
                              array_push($said, $pvalue);
                              unset($pending_id[array_search($pvalue, $pending_id) ]);
                              $msg[$pkey]['Heading'] = $data[0]['Heading'];
                              $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                              $msg[$pkey]['book'] = '1';
                              $msg[$pkey]['resion'] = "";
                         }
                    }
                    else
                    {
                         $msg[$pkey]['Heading'] = $data[0]['Heading'];
                         $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                         $msg[$pkey]['book'] = '0';
                         $msg[$pkey]['resion'] = "Already Save Session";
                    }
               }
               $ua['agenda_id'] = implode(",", $said);
               $ua['pending_agenda_id'] = implode(",", $pending_id);
          }
          else
          {
               foreach($paid as $pkey => $pvalue)
               {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id', $pvalue);
                    $dqu = $this->db->get();
                    $data = $dqu->result_array();
                    if (!empty($data[0]['Maximum_People']))
                    {
                         $this->db->select('*')->from('users_agenda');
                         $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                         $this->db->where($where);
                         $tqu = $this->db->get();
                         $total = $tqu->result_array();
                         $placeleft = $data[0]['Maximum_People'] - count($total);
                    }
                    else
                    {
                         $placeleft = 1;
                    }
                    if ($placeleft > 0)
                    {
                         $return = true;
                    }
                    else
                    {
                         $msg[$pkey]['Heading'] = $data[0]['Heading'];
                         $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                         $msg[$pkey]['book'] = '0';
                         $msg[$pkey]['resion'] = "No Spaces Left";
                         $return = false;
                    }
                    if ($return == "true")
                    {
                         array_push($said, $pvalue);
                         unset($pending_id[array_search($pvalue, $pending_id) ]);
                         $msg[$pkey]['Heading'] = $data[0]['Heading'];
                         $msg[$pkey]['Start_time'] = $data[0]['Start_time'];
                         $msg[$pkey]['book'] = '1';
                         $msg[$pkey]['resion'] = "";
                    }
               }
               $ua['agenda_id'] = implode(",", $said);
               $ua['pending_agenda_id'] = implode(",", $pending_id);
          }
          $this->db->where('user_id', $user_id);
          $this->db->update('users_agenda', $ua);
          foreach($msg as $key => $value)
          {
               if ($time_format[0]['format_time'] == 0)
               {
                    $msg[$key]['Start_time'] = date("h:i A", strtotime($value['Start_time']));
               }
               else
               {
                    $msg[$key]['Start_time'] = date("H:i", strtotime($value['Start_time']));
               }
          }
          return $msg;
     }
     public function show_check_in_time_by_category($id=NULL, $cid=NULL)

     {
          $res = $this->get_agenda_category_by_id($cid);
          if ($res[0]['show_check_in_time'] == '1')
          {
               $a['show_check_in_time'] = '0';
          }
          else
          {
               $a['show_check_in_time'] = '1';
          }
          $this->db->where('Id', $cid);
          $this->db->where('event_id', $id);
          $this->db->update('agenda_categories', $a);
          return $this->db->affected_rows();
     }
     public function add_session_types($eid=NULL, $type=NULL)

     {
          $type_data['type_name'] = $type;
          $type_data['event_id'] = $eid;
          $type_data['created_date'] = date('Y-m-d H:i:s');
          $this->db->insert('session_types', $type_data);
          $type_id = $this->db->insert_id();
          return $type_id;
     }
     public function update_session_types($type_id=NULL, $tnm=NULL)

     {
          $this->db->where('type_id', $type_id);
          $this->db->update('session_types', array(
               'type_name' => $tnm
          ));
     }
     public function remove_session_type($type_id=NULL, $event_id = '')

     {
          $this->db->where('type_id', $type_id);
          $this->db->delete('session_types');
          if ($event_id != '')
          {
               $a_cat = $this->db->select('*')->from('session_types')->where('event_id', $event_id)->order_by('updated_date', 'DESC')->get()->row_array();
               $update['updated_date'] = date('Y-m-d H:i:s');
               $this->db->where('type_id', $a_cat['type_id'])->update('session_types', $update);
          }
     }
     public function check_exists_type_name($eid=NULL, $tnm=NULL, $tid = NULL)

     {
          $this->db->select('*')->from('session_types');
          $this->db->where('event_id', $eid);
          $this->db->where('type_name', $tnm);
          if (!empty($tid))
          {
               $this->db->where('type_id !=', $tid);
          }
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function get_session_type($eid=NULL)

     {
          $this->db->protect_identifiers = false;
          $this->db->select('*')->from('session_types');
          $this->db->where('event_id', $eid);
          $this->db->order_by('order_no=0,order_no', NULL, false);
          $tres = $this->db->get()->result_array();
          return $tres;
     }
     public function get_speaker_id_by_email($email=NULL, $eid=NULL)

     {
          $this->db->select('u.Id')->from('user u');
          $this->db->join('relation_event_user reu', 'u.Id=reu.User_id AND reu.Event_id=' . $eid, 'left');
          $this->db->where('Email', $email);
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res[0]['Id'];
     }
     public function get_map_id_by_map_code($code=NULL, $eid=NULL)

     {
          $this->db->select('Id')->from('map');
          $this->db->where('Event_id', $eid);
          $this->db->where('map_u_id', $code);
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res[0]['Id'];
     }
     public function check_overlapping_meeting_with_agenda($uid=NULL, $eid=NULL, $date=NULL, $time=NULL)

     {
          error_reporting(E_ALL);
          $Start_time = $time;
          $End_time = date('h:i', strtotime("+15 minutes", strtotime($time)));
          $this->db->select('*')->from('users_agenda ua');
          $this->db->where('ua.user_id', $uid);
          $qu = $this->db->get();
          $res = $qu->result_array();
          if (count($res) > 0)
          {
               $agenda_id = explode(",", $res[0]['agenda_id']);
               foreach($agenda_id as $key => $value)
               {
                    if (!empty($value))
                    {
                         $this->db->select('*,concat_ws(" ",Start_date,Start_time) AS starttime,concat_ws(" ",End_date,End_time) as endtime', false)->from('agenda');
                         $datetime = $date . ' ' . $time;
                         $this->db->where('Id', $value);
                         $this->db->having("'" . $datetime . "' BETWEEN starttime and endtime ", NULL, false);
                         $qu1 = $this->db->get();
                         $res1 = $qu1->result_array();
                         if (count($res1) > 0)
                         {
                              $return = $res1;
                              break;
                         }
                         else
                         {
                              $return = array();
                         }
                    }
                    else
                    {
                         $return = array();
                    }
               }
          }
          else
          {
               $return = array();
          }
          return $return;
     }
     public function add_metting_with_exibitor($metting=NULL)

     {
          $metting['created_datetime'] = date('Y-m-d H:i:s');
          $this->db->insert('exhibitor_attendee_meeting', $metting);
          return $this->db->insert_id();
     }
     public function get_all_pending_metting_by_exibitor($eid=NULL)

     {
          $user = $this->session->userdata('current_user');
          $this->db->select('*')->from('exibitor e');
          $this->db->where('e.Event_id', $eid);
          $this->db->where('e.user_id', $user[0]->Id);
          $qu = $this->db->get();
          $eres = $qu->result_array();
          $this->db->select('eam.*,case when u.Lastname IS NULL Then u1.Lastname ELSE u.Lastname end as Lastname,case when u.Firstname IS NULL Then u1.Firstname ELSE u.Firstname end as Firstname,case when u.Logo IS NULL Then u1.Logo ELSE u.Logo end as Logo', false)->from('exhibitor_attendee_meeting eam');
          $this->db->join('user u', 'eam.attendee_id=u.Id ANd eam.attendee_id IS NOT NULL', 'left');
          $this->db->join('user u1', 'eam.recever_attendee_id=u1.Id AND eam.recever_attendee_id IS NOT NULL', 'left');
          $this->db->where('eam.event_id', $eid);
          $this->db->where(' (eam.exhibiotor_id="' . $eres[0]['Id'] . '" OR eam.sender_exhibitor_id="' . $user[0]->Id . '") ', NULL, false);
          $this->db->where('eam.status !=', '2');
          $qu = $this->db->get();
          $res = $qu->result();
          $attendees = array();
          for ($i = 0; $i < count($res); $i++)
          {
               $prev = "";
               $fc = strtoupper($res[$i]->Lastname);
               $al = substr($fc, 0, 1);
               if ($prev == "" || $res[$i]->Lastname != $prev)
               {
                    $prev = $res[$i]->Lastname;
                    $attendees[$al][$i]['metting_id'] = $res[$i]->Id;
                    $attendees[$al][$i]['Firstname'] = $res[$i]->Firstname;
                    $attendees[$al][$i]['Lastname'] = $res[$i]->Lastname;
                    $attendees[$al][$i]['date'] = $res[$i]->date;
                    $attendees[$al][$i]['time'] = $res[$i]->time;
                    $attendees[$al][$i]['status'] = $res[$i]->status;
                    $attendees[$al][$i]['Logo'] = $res[$i]->Logo;
                    $attendees[$al][$i]['attendee_id'] = $res[$i]->attendee_id;
                    $attendees[$al][$i]['exhibiotor_id'] = $res[$i]->exhibiotor_id;
                    $attendees[$al][$i]['exhibiotor_user_id'] = $user[0]->Id;
                    $attendees[$al][$i]['stand_number'] = $eres[0]['stand_number'];
                    $attendees[$al][$i]['sender_exhibitor_id'] = $res[$i]->sender_exhibitor_id;
                    $attendees[$al][$i]['Event_id'] = $eres[0]['Event_id'];
                    $attendees[$al][$i]['Organisor_id'] = $eres[0]['Organisor_id'];
               }
          }
          return $attendees;
     }
     public function change_metting_status_by_metting_id($mid=NULL, $status=NULL)

     {
          $metting_data['status'] = $status;
          $this->db->where('Id', $mid);
          $this->db->update('exhibitor_attendee_meeting', $metting_data);
          $this->db->select('*')->from('exhibitor_attendee_meeting');
          $this->db->where('Id', $mid);
          $qu = $this->db->get();
          $eres = $qu->result_array();
          return $eres;
     }
     public function get_all_metting_in_attendee_id_by_datetime($eid=NULL)

     {
          $this->db->protect_identifiers = false;
          $user = $this->session->userdata('current_user');
          if ($user[0]->Rid == '4')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL)', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,case when concat(u.Firstname," ",u.Lastname) IS NULL then e.Heading ELSE concat(u.Firstname," ",u.Lastname) end as Heading,eam.location as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.recever_attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'eam.attendee_id=u.Id And eam.attendee_id IS NOT NULL', 'left');
               $this->db->join('exibitor e', 'eam.sender_exhibitor_id=e.user_id AND e.Event_id=' . $eid, 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          if ($user[0]->Rid == '6')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.sender_exhibitor_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.sender_exhibitor_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL)', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,concat(u.Lastname," ",u.Firstname) as Heading,u.Company_name,u.Logo,e.Heading as exhibitor_Heading,e.stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id', 'left');
               $this->db->where('e.Event_id', $eid);
               $this->db->where('e.user_id', $user[0]->Id);
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          $metting = array();
          for ($i = 0; $i < count($res); $i++)
          {
               $timedate = $res[$i]->date;
               $prev = "";
               if ($prev == "" || $timedate != $prev)
               {
                    $prev = $timedate;
                    $metting[$timedate][$i]['metting_id'] = $res[$i]->Id;
                    $metting[$timedate][$i]['exhibiotor_id'] = $res[$i]->exhibiotor_id;
                    $metting[$timedate][$i]['attendee_id'] = $res[$i]->attendee_id;
                    $metting[$timedate][$i]['event_id'] = $res[$i]->event_id;
                    $metting[$timedate][$i]['date'] = $res[$i]->date;
                    $metting[$timedate][$i]['time'] = $res[$i]->time;
                    $metting[$timedate][$i]['status'] = $res[$i]->status;
                    $metting[$timedate][$i]['Firstname'] = $res[$i]->Firstname;
                    $metting[$timedate][$i]['Lastname'] = $res[$i]->Lastname;
                    $metting[$timedate][$i]['Logo'] = $res[$i]->Logo;
                    $metting[$timedate][$i]['Heading'] = $res[$i]->Heading;
                    $metting[$timedate][$i]['stand_number'] = $res[$i]->stand_number;
                    $metting[$timedate][$i]['Company_name'] = $res[$i]->Company_name;
               }
          }
          return $metting;
     }
     public function get_all_metting_in_attendee_id_by_type($eid=NULL)

     {
          $user = $this->session->userdata('current_user');
          if ($user[0]->Rid == '4')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL)', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,case when concat(u.Firstname," ",u.Lastname) IS NULL then e.Heading ELSE concat(u.Firstname," ",u.Lastname) end as Heading,eam.location as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.recever_attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'eam.attendee_id=u.Id And eam.attendee_id IS NOT NULL', 'left');
               $this->db->join('exibitor e', 'eam.sender_exhibitor_id=e.user_id AND e.Event_id=' . $eid, 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          if ($user[0]->Rid == '6')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.sender_exhibitor_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.sender_exhibitor_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,concat(u.Lastname," ",u.Firstname) as Heading,u.Company_name,u.Logo,e.Heading as exhibitor_Heading,e.stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id', 'left');
               $this->db->where('e.Event_id', $eid);
               $this->db->where('e.user_id', $user[0]->Id);
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          $metting = array();
          for ($i = 0; $i < count($res); $i++)
          {
               $prev = "";
               if ($prev == "" || $timedate != $prev)
               {
                    $prev = 'Meeting';
                    $metting['Meeting'][$i]['metting_id'] = $res[$i]->Id;
                    $metting['Meeting'][$i]['exhibiotor_id'] = $res[$i]->exhibiotor_id;
                    $metting['Meeting'][$i]['attendee_id'] = $res[$i]->attendee_id;
                    $metting['Meeting'][$i]['event_id'] = $res[$i]->event_id;
                    $metting['Meeting'][$i]['date'] = $res[$i]->date;
                    $metting['Meeting'][$i]['time'] = $res[$i]->time;
                    $metting['Meeting'][$i]['status'] = $res[$i]->status;
                    $metting['Meeting'][$i]['Firstname'] = $res[$i]->Firstname;
                    $metting['Meeting'][$i]['Lastname'] = $res[$i]->Lastname;
                    $metting['Meeting'][$i]['Logo'] = $res[$i]->Logo;
                    $metting['Meeting'][$i]['Heading'] = $res[$i]->Heading;
                    $metting['Meeting'][$i]['stand_number'] = $res[$i]->stand_number;
                    $metting['Meeting'][$i]['Company_name'] = $res[$i]->Company_name;
               }
          }
          return $metting;
     }
     public function check_metting_availability($eid=NULL)

     {
          $user = $this->session->userdata('current_user');
          if ($user[0]->Rid == '4')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL)', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,case when concat(u.Firstname," ",u.Lastname) IS NULL then e.Heading ELSE concat(u.Firstname," ",u.Lastname) end as Heading,eam.location as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.recever_attendee_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'eam.attendee_id=u.Id And eam.attendee_id IS NOT NULL', 'left');
               $this->db->join('exibitor e', 'eam.sender_exhibitor_id=e.user_id AND e.Event_id=' . $eid, 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          if ($user[0]->Rid == '6')
          {
               $this->db->select('eam.*,u.Lastname,u.Firstname,u.Logo,(CASE WHEN e.Heading IS NULL THEN concat(u2.Firstname, " ", u2.Lastname) ELSE e.Heading END) as Heading,(CASE WHEN e.stand_number IS NULL THEN eam.location ELSE e.stand_number END) as stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.sender_exhibitor_id', $user[0]->Id);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.sender_exhibitor_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id AND eam.exhibiotor_id IS NOT NULL', 'left');
               $this->db->join('user u2', '(u2.Id=eam.recever_attendee_id AND eam.recever_attendee_id IS NOT NULL OR u2.Id=eam.recipient_attendee_id AND eam.recipient_attendee_id IS NOT NULL)', 'left');
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res1 = $qu->result();
               $this->db->select('eam.*,concat(u.Lastname," ",u.Firstname) as Heading,u.Company_name,u.Logo,e.Heading as exhibitor_Heading,e.stand_number')->from('exhibitor_attendee_meeting eam');
               $this->db->where('eam.event_id', $eid);
               $this->db->where('eam.status !=', '2');
               $this->db->join('user u', 'u.Id=eam.attendee_id', 'right');
               $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id', 'left');
               $this->db->where('e.Event_id', $eid);
               $this->db->where('e.user_id', $user[0]->Id);
               $this->db->group_by('eam.Id');
               $this->db->order_by('eam.date asc');
               $qu = $this->db->get();
               $res2 = $qu->result();
               $res = array_merge($res1, $res2);
          }
          return $res;
     }
     public function make_primary_agenda_categories($eid=NULL, $acid=NULL)

     {
          $this->db->where('event_id', $eid);
          $this->db->update('agenda_categories', array(
               'categorie_type' => '0'
          ));
          $this->db->where('Id', $acid);
          $this->db->where('event_id', $eid);
          $this->db->update('agenda_categories', array(
               'categorie_type' => '1'
          ));
     }
     public function get_meeting_data_by_meeting_id($mid=NULL)

     {
          $this->db->select('eam.*,e.Heading,e.stand_number,e.main_contact_name,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email')->from('exhibitor_attendee_meeting eam');
          $this->db->join('exibitor e', 'e.Id=eam.exhibiotor_id', 'left');
          $this->db->join('user u', 'u.Id=eam.attendee_id', 'left');
          $this->db->where('eam.Id', $mid);
          $eres = $this->db->get()->result_array();
          return $eres;
     }
     public function update_datetime_in_meeting_by_meeting_id($m_db=NULL, $mid=NULL)

     {
          $this->db->where('Id', $mid);
          $this->db->update('exhibitor_attendee_meeting', $m_db);
     }
     public function agenda_id_listby_session_id($sid=NULL)

     {
          $this->db->select('agenda_id,category_id')->from('agenda_category_relation');
          $this->db->where('category_id', $sid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function get_all_agenda_in_map($id=NULL)

     {
          $this->db->select('a.*')->from('agenda a');
          $this->db->join('agenda_category_relation acr', 'a.Id=acr.agenda_id', 'right');
          $this->db->where('a.Event_id', $id);
          $this->db->order_by("a.Start_date", "asc");
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_all_pending_metting_by_attendee($eid=NULL)

     {
          $user = $this->session->userdata('current_user');
          $this->db->select('eam.*,case when eam.recever_attendee_id=' . $user[0]->Id . ' THEN case when concat(u.Firstname," ",u.Lastname) IS NULL then e.Heading else concat(u.Firstname," ",u.Lastname) end ELSE case when concat(u1.Firstname," ",u1.Lastname) IS NULL then e1.Heading else concat(u1.Firstname," ",u1.Lastname) end ENd as name ,Case when eam.recever_attendee_id=' . $user[0]->Id . ' Then u.Logo ELSE u1.Logo END as Logo,Case when eam.recever_attendee_id=' . $user[0]->Id . ' THEN e.Heading ELSE e1.Heading ENd as Heading,CASE When eam.recever_attendee_id=' . $user[0]->Id . ' THEN e.company_logo ELSE e1.company_logo end as company_logo,case when eam.recever_attendee_id=' . $user[0]->Id . ' THEN e.stand_number ELSE e1.stand_number END as stand_number,case when eam.recever_attendee_id=' . $user[0]->Id . ' Then u.Lastname ELSE u1.Lastname end as Lastname,case when eam.recever_attendee_id=' . $user[0]->Id . ' Then u.Firstname ELSE u1.Firstname end as Firstname', false)->from('exhibitor_attendee_meeting eam');
          $this->db->where('eam.event_id', $eid);
          $this->db->where('eam.recever_attendee_id IS NOT NULL');
          $this->db->where(' (eam.attendee_id="' . $user[0]->Id . '" OR eam.recever_attendee_id="' . $user[0]->Id . '") ', NULL, false);
          // $this->db->where('eam.recever_attendee_id',$user[0]->Id);
          $this->db->where('eam.status !=', '2');
          $this->db->join('user u', 'eam.attendee_id=u.Id AND eam.attendee_id IS NOT NULL', 'left');
          $this->db->join('exibitor e', 'eam.sender_exhibitor_id=e.user_id AND e.Event_id=' . $eid, 'left');
          $this->db->join('user u1', 'eam.recever_attendee_id=u1.Id AND eam.recever_attendee_id IS NOT NULL', 'left');
          $this->db->join('exibitor e1', 'eam.exhibiotor_id=e1.user_id AND e.Event_id=' . $eid, 'left');
          $qu = $this->db->get();
          $res = $qu->result();
          $attendees = array();
          for ($i = 0; $i < count($res); $i++)
          {
               $prev = "";
               $fc = strtoupper($res[$i]->Lastname);
               $al = substr($fc, 0, 1);
               if ($prev == "" || $res[$i]->Lastname != $prev)
               {
                    $prev = $res[$i]->Lastname;
                    $attendees[$al][$i]['metting_id'] = $res[$i]->Id;
                    $attendees[$al][$i]['name'] = $res[$i]->name;
                    $attendees[$al][$i]['Firstname'] = $res[$i]->Firstname;
                    $attendees[$al][$i]['Lastname'] = $res[$i]->Lastname;
                    $attendees[$al][$i]['date'] = $res[$i]->date;
                    $attendees[$al][$i]['time'] = $res[$i]->time;
                    $attendees[$al][$i]['status'] = $res[$i]->status;
                    $attendees[$al][$i]['Logo'] = $res[$i]->Logo;
                    $attendees[$al][$i]['attendee_id'] = $res[$i]->attendee_id;
                    $attendees[$al][$i]['recever_attendee_id'] = $res[$i]->recever_attendee_id;
               }
          }
          return $attendees;
     }
     public function get_all_pending_metting_by_moderator($eid=NULL)

     {
          $user = $this->session->userdata('current_user');
          $this->db->select('eam.*,eam.id as metting_id,case when concat(u.Firstname," ",u.Lastname) IS NULL then e.Heading else concat(u.Firstname," ",u.Lastname) end as name ,u.Logo,e.Heading,e.company_logo,e.stand_number,concat(IFNULL(u1.Firstname,"")," ",IFNULL(u1.Lastname,"")) as recipient_name', false)->from('exhibitor_attendee_meeting eam');
          $this->db->where('eam.event_id', $eid);
          $this->db->where('eam.moderator_id IS NOT NULL');
          $this->db->where('eam.moderator_id', $user[0]->Id);
          $this->db->join('user u', 'eam.attendee_id=u.Id AND eam.attendee_id IS NOT NULL', 'left');
          $this->db->join('exibitor e', 'eam.sender_exhibitor_id=e.user_id AND e.Event_id=' . $eid, 'left');
          $this->db->join('user u1', 'eam.recipient_attendee_id = u1.Id and eam.recipient_attendee_id Is NOT NULL', 'left');
          $qu = $this->db->get();
          $res = $qu->result_array();
          return $res;
     }
     public function add_suggest_new_datetime($eid=NULL, $date=NULL, $mid=NULL, $aid=NULL, $ex_uid = NULL, $raid = NULL)

     {
          $suggest_meeting_data['event_id'] = $eid;
          $suggest_meeting_data['date_time'] = $date;
          $suggest_meeting_data['meeting_id'] = $mid;
          $suggest_meeting_data['attendee_id'] = $aid;
          if (!empty($ex_uid)) $suggest_meeting_data['exhibitor_user_id'] = $ex_uid;
          if (!empty($raid)) $suggest_meeting_data['recever_user_id'] = $raid;
          $this->db->insert('suggest_meeting', $suggest_meeting_data);
     }
     public function delete_suggest_meeting_date($eid=NULL, $aid=NULL, $exid = NULL, $raid = NULL)

     {
          $this->db->where('event_id', $eid);
          $this->db->where('attendee_id', $aid);
          if (!empty($exid))
          {
               $this->db->where('exhibitor_user_id', $exid);
          }
          if (!empty($raid))
          {
               $this->db->where('recever_user_id', $raid);
          }
          $this->db->delete('suggest_meeting');
     }
     public function save_user_agenda_comment($eid=NULL, $uid=NULL, $aid=NULL, $comment=NULL)

     {
          $res = $this->get_agenda_comment_by_agenda_id_and_user_id($eid, $uid, $aid);
          if (count($res) > 0)
          {
               $this->db->where('event_id', $eid);
               $this->db->where('user_id', $uid);
               $this->db->where('agenda_id', $aid);
               $this->db->update('agenda_comments', array(
                    "comments" => $comment
               ));
               return $res[0]['Id'];
          }
          else
          {
               $cdata['event_id'] = $eid;
               $cdata['user_id'] = $uid;
               $cdata['agenda_id'] = $aid;
               $cdata['comments'] = $comment;
               $cdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('agenda_comments', $cdata);
               return $this->db->insert_id();
          }
     }
     public function get_all_qasession_by_event($eid=NULL)

     {
          $this->db->select('*')->from('qa_session');
          $this->db->where('Event_id', $eid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function get_all_agenda_by_speaker_id($eid=NULL, $sid=NULL)

     {
          $this->db->select('*')->from('agenda');
          $this->db->where('Event_id', $eid);
          $where = "FIND_IN_SET($sid,`Speaker_id`) > 0";
          $this->db->where($where);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function get_agenda_comment_by_agenda_id_and_user_id($eid=NULL, $uid=NULL, $aid=NULL)

     {
          $this->db->select('*')->from('agenda_comments');
          $this->db->where('event_id', $eid);
          $this->db->where('user_id', $uid);
          $this->db->where('agenda_id', $aid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function add_session_notification($notiarr=NULL)

     {
          $this->db->insert('session_notification', $notiarr);
     }
     public function update_session_notification($notiarr=NULL, $event_id=NULL, $agenda_id=NULL)

     {
          $this->db->where('event_id', $event_id);
          $this->db->where('agenda_id', $agenda_id);
          $this->db->update('session_notification', $notiarr);
     }
     public function delete_agenda_images($eid=NULL, $agenda_id=NULL)

     {
          $udata['session_image'] = NULL;
          $this->db->where('Event_id', $eid);
          $this->db->where('Id', $agenda_id);
          $this->db->update('agenda', $udata);
     }
     public function send_recipient_meeting_by_moderator($mid=NULL, $raid=NULL)

     {
          $updata['recever_attendee_id'] = $raid;
          $this->db->where('Id', $mid);
          $this->db->update('exhibitor_attendee_meeting', $updata);
     }
     public function update_metting_location($mid=NULL, $location=NULL)

     {
          $updata['location'] = $location;
          $this->db->where('Id', $mid);
          $this->db->update('exhibitor_attendee_meeting', $updata);
     }
     public function get_user_agenda($uid=NULL)

     {
          $this->db->select('*');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $uid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function save_useragenda($savedata=NULL, $uid=NULL)

     {
          $res = $this->get_user_agenda($uid);
          if (count($res) > 0)
          {
               $this->db->where('user_id', $uid);
               $this->db->update('users_agenda', $savedata);
          }
          else
          {
               $savedata['user_id'] = $uid;
               $this->db->insert('users_agenda', $savedata);
          }
     }
     public function assign_ticket_to_agenda($aid=NULL, $ticket=NULL)

     {
          $update_array['agenda_id'] = $aid;
          $update_array['ticket_id'] = $ticket;
          $res = $this->db->select('*')->from('agenda_ticket')->where($update_array)->get()->result_array();
          if (count($res) > 0)
          {
               $this->db->where($update_array);
               $this->db->update('agenda_ticket', $update_array);
          }
          else
          {
               $this->db->insert('agenda_ticket', $update_array);
          }
     }
     public function get_all_surveys_category_by_event($eid=NULL)

     {
          $res = $this->db->select('*')->from('survey_category')->where('event_id', $eid)->get()->result_array();
          return $res;
     }
     public function save_sesssion_by_user($uid=NULL, $sid=NULL)

     {
          $this->db->select('*')->from('user_save_session');
          $this->db->where('user_id', $uid);
          $this->db->where('session_id', $sid);
          $res = $this->db->get()->result_array();
          if (count($res) > 0)
          {
               $this->db->where('user_id', $uid);
               $this->db->where('session_id', $sid);
               $this->db->delete('user_save_session');
          }
          else
          {
               $ssd['user_id'] = $uid;
               $ssd['session_id'] = $sid;
               $ssd['save_datetime'] = date('Y-m-d H:i:s');
          }
     }
     public function get_export_session_rating_users($eid=NULL)

     {
          $this->db->select('u.Firstname,u.Lastname,u.Email,u.Company_name,u.Title,a.Heading,a.Start_time,a.End_time,a.Start_date,a.End_date,usr.rating')->from('agenda a');
          $this->db->join('user_session_rating usr', 'a.Id=usr.session_id', 'left');
          $this->db->join('user u', 'u.Id=usr.user_id');
          $this->db->where('a.Event_id', $eid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function get_export_session_comment_users($eid=NULL)

     {
          $this->db->select('u.Firstname,u.Lastname,u.Email,u.Company_name,u.Title,a.Heading,a.Start_time,a.End_time,a.Start_date,a.End_date,ac.comments')->from('agenda a');
          $this->db->join('agenda_comments ac', 'a.Id=ac.agenda_id', 'left');
          $this->db->join('user u', 'u.Id=ac.user_id');
          $this->db->where('a.Event_id', $eid);
          $res = $this->db->get()->result_array();
          return $res;
     }
     public function save_session_type_order_number($type_id=NULL, $ono=NULL)

     {
          $this->db->where('type_id', $type_id);
          $this->db->update('session_types', array(
               'order_no' => $ono
          ));
     }
     public function get_reg_tickets($event_id=NULL)

     {
          $res = $this->db->where('event_id', $event_id)->get('reg_tickets')->result_array();
          return $res;
     }
     public function check_meeting_location_clash_with_other($data=NULL, $sender_user_id=NULL, $reciver_user_id=NULL)

     {
          $num = $this->db->select('*')->from('exhibitor_attendee_meeting')->where('event_id', $data['event_id'])->where('location', $data['location'])->where('date', $data['date'])->where('time', $data['time'])->where('status !=', '2')->get()->num_rows();
          if (!empty($num))
          {
               $res['msg'] = "This meeting location is not available at the specified time. Please try a different time or a different Meeting Location.";
               $res['status'] = false;
          }
          else
          {
               $count = $this->db->select('*')->from('exhibitor_attendee_meeting')->where('status !=', '2')->where('event_id', $data['event_id'])->where(' (sender_exhibitor_id=' . $sender_user_id . ' OR attendee_id=' . $sender_user_id . ') ', NULL, false)->where('date', $data['date'])->where('time', $data['time'])->get()->num_rows();
               if (!empty($count))
               {
                    $res['msg'] = "You have already set meeting with this time and date.";
                    $res['status'] = false;
               }
               else
               {
                    $recevercount = $this->db->select('*')->from('exhibitor_attendee_meeting')->where('status !=', '2')->where('event_id', $data['event_id'])->where('recever_attendee_id', $reciver_user_id)->where('date', $data['date'])->where('time', $data['time'])->get()->num_rows();
                    if (!empty($recevercount))
                    {
                         $user = $this->db->where('Id', $reciver_user_id)->get('user')->row_array();
                         $res['msg'] = $user['Firstname'] . ' ' . $user['Lastname'] . ' is unavailable at this time, please choose another time.';
                         $res['status'] = false;
                    }
                    else
                    {
                         $res['status'] = true;
                    }
               }
          }
          return $res;
     }
     /*public function get_agenda_and_comments($agenda_id)
     {
     $res = $this->db->where('Id',$agenda_id)->get('agenda')->row_array();
     $res['session_comments'] = $this->db->where('agenda_id',$agenda_id)
     ->get('agenda_comments')
     ->result_array();
     return $res;
     }*/
     public function get_agenda_and_comments($agenda_id=NULL, $event_id=NULL)

     {
          $this->db->where('Id', $agenda_id);
          if ($event_id != null) $this->db->where('Event_id', $event_id);
          $res = $this->db->get('agenda')->row_array();
          if (!$res) return redirect('Forbidden');
          $res['session_comments'] = $this->db->where('agenda_id', $agenda_id)->get('agenda_comments')->result_array();
          return $res;
     }
     public function get_all_sortorder($event_id=NULL, $agenda_id=NULL)

     {
          $this->db->select('sort_order');
          $this->db->from('agenda');
          if ($agenda_id != '')
          {
               $this->db->where('Id !=', $agenda_id);
          }
          $this->db->where('Event_id', $event_id);
          $this->db->where('sort_order !=', 0);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function downloadDeepLink($event_id=NULL, $cid=NULL)

     {
          $org_id = $this->db->where('Id', $event_id)->get('event')->row_array() ['Organisor_id'];
          $version_code = $this->db->where('org_id', $org_id)->get('version_code')->row_array();
          if (!empty($version_code['ios_app_id']) || !empty($version_code['android_app_id'])) $host = strtolower($version_code['app']);
          else $host = 'allintheloop';
          $this->load->dbutil();
          // $this->db->select('a.Heading as Session Name,a.agenda_code as Session Code,concat("https://www.allintheloop.net/apiv4/deeplink?h=allintheloop&node_main=MQ==&node_sub=",TO_BASE64(a.Id),"&nevent=",TO_BASE64(a.Event_id)) as DeepLink');
          //
          $this->db->select('a.Heading as Session Name,a.agenda_code as Session Code,concat("' . $host . '://m.allintheloop.com?node_main=MQ==&node_sub=",TO_BASE64(a.Id),"&nevent=",TO_BASE64(a.Event_id)) as DeepLink');
          $this->db->join('agenda_category_relation acr', 'acr.agenda_id=a.Id', 'right');
          $this->db->where('acr.category_id', $cid);
          $this->db->where('a.Event_id', $event_id);
          $this->db->order_by("a.Start_date", "asc");
          $this->db->group_by('a.Id');
          $res = $this->db->get('agenda a');
          $backup = $this->dbutil->csv_from_result($res);
          $this->load->helper('file');
          write_file('agenda_deeplink.csv', $backup);
          $this->load->helper('download');
          force_download('agenda_deeplink.csv', $backup);
     }
}
?>