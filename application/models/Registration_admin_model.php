<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Registration_admin_model extends CI_Model

{
    public function __construct()

    {
        parent::__construct();
    }
    public function get_registration_screen_data($eid=NULL)

    {
        $this->db->select('*')->from('registration_screen_data');
        $this->db->where('event_id', $eid);
        $rqu = $this->db->get();
        $res = $rqu->row_array();
        return $res;
    }
    public function save_registration_screen_data($rs_data=NULL)

    {
        $rsdata = $this->get_registration_screen_data($rs_data['event_id']);
        if (count($rsdata) > 0)
        {
            $this->db->where('event_id', $rs_data['event_id']);
            unset($rs_data['event_id']);
            $this->db->update('registration_screen_data', $rs_data);
        }
        else
        {
            $this->db->insert('registration_screen_data', $rs_data);
        }
    }
    public function save_stage($data=NULL)

    {
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->db->insert('reg_stages', $data);
    }
    public function get_stages($event_id=NULL)

    {
        $res = $this->db->select()->where('event_id', $event_id)->order_by('sort_order')->get('reg_stages')->result_array();
        return $res;
    }
    public function delete_stage($where=NULL)

    {
        $this->db->where($where)->delete('reg_stages');
    }
    public function get_stage($id=NULL, $event_id=NULL)

    {
        $res = $this->db->select()->where('id', $id)->where('event_id', $event_id)->get('reg_stages')->row_array();
        if (!$res && $this->uri->segment(2) == 'edit_stage') return redirect('Forbidden');
        $this->db->select();
        $this->db->where('stage_id', $id);
        $this->db->select('rq.*,rs.stage_name,CASE WHEN rsk.id IS NULL THEN 0 ELSE 1 END as skip_logic', false);
        $this->db->where('rq.event_id', $event_id);
        $this->db->join('reg_stages rs', 'rs.id = rq.stage_id', 'left');
        $this->db->join('reg_skiplogic rsk', 'rsk.que_id = rq.id', 'left');
        $this->db->group_by('rq.id');
        $que = $this->db->get('reg_que rq')->result_array();
        $res['stage_que'] = $que;
        $res['stage_que_id'] = array_column_1($que, 'id');
        return $res;
    }
    public function update_stage($where=NULL, $update=NULL)

    {
        $res = $this->db->where($where)->update('reg_stages', $update);
    }
    public function add_question($data=NULL)

    {
        $this->db->insert('reg_que', $data);
        $custom_column = $this->db->select('*')->from('custom_column')->where('column_name', $data['question'])->where('event_id', $data['event_id'])->get()->row_array();
        if (empty($custom_column))
        {
            $custom_column['column_name'] = $data['question'];
            $custom_column['event_id'] = $data['event_id'];
            $custom_column['crequire'] = '0';
            $this->db->insert('custom_column', $custom_column);
        }
    }
    public function update_custom_column($eid=NULL, $old_val=NULL, $new_val=NULL)

    {
        $custom_column = $this->db->select('*')->from('custom_column')->where('column_name', $old_val)->where('event_id', $eid)->get()->row_array();
        $custom_column_data['column_name'] = $new_val;
        if (!empty($custom_column))
        {
            $this->db->where('column_name', $old_val)->where('event_id', $eid)->update('custom_column', $custom_column_data);
        }
        else
        {
            $custom_column_data['event_id'] = $eid;
            $custom_column_data['crequire'] = '0';
            $this->db->insert('custom_column', $custom_column_data);
        }
    }
    public function get_questions($event_id=NULL)

    {
        $this->db->select('rq.*,rs.stage_name,rd.code,rd.price,rd.status');
        $this->db->where('rq.event_id', $event_id);
        $this->db->join('reg_stages rs', 'rs.id = rq.stage_id', 'left');
        $this->db->join('reg_discount rd', 'rd.que_id = rq.id', 'left');
        $res = $this->db->get('reg_que rq')->result_array();
        return $res;
    }
    public function get_questions_for_add($event_id=NULL)

    {
        $this->db->select();
        $this->db->where('event_id', $event_id);
        $this->db->where('stage_id IS NULL');
        $res = $this->db->get('reg_que')->result_array();
        return $res;
    }
    public function delete_question($where=NULL)

    {
        $this->db->where($where)->delete('reg_que');
    }
    /*public function get_question($id)
    {
    $res = $this->db->select()->where('id',$id)->get('reg_que')->row_array();
    return $res;
    }*/
    public function get_question($id=NULL, $event_id = null)

    {
        $this->db->select()->where('id', $id);
        if ($event_id != null) $this->db->where('event_id', $event_id);
        $res = $this->db->get('reg_que')->row_array();
        if (!$res && $this->uri->segment(2) == 'edit_question' && $event_id != null) return redirect('Forbidden');
        return $res;
    }
    public function update_question($data=NULL, $id=NULL)

    {
        $this->db->where('id', $id)->update('reg_que', $data);
    }
    public function add_question_to_stage($where=NULL, $update=NULL)

    {
        $this->db->where($where)->update('reg_que', $update);
    }
    public function add_skip_logic($data=NULL)

    {
        $this->db->select();
        $this->db->where('que_id', $data['que_id']);
        $this->db->where('option', $data['option']);
        $res = $this->db->get('reg_skiplogic')->row_array();
        if (!empty($res))
        {
            $update['redriect_stage_id'] = $data['redriect_stage_id'];
            $where['que_id'] = $data['que_id'];
            $where['option'] = $data['option'];
            $this->db->where($where)->update('reg_skiplogic', $update);
        }
        else
        {
            $this->db->insert('reg_skiplogic', $data);
        }
    }
    public function get_skip_logic($id=NULL)

    {
        $this->db->select();
        $this->db->where('que_id', $id);
        $res = $this->db->get('reg_skiplogic')->result_array();
        return $res;
    }
    public function get_stage_for_redirect_list($event_id=NULL, $id=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('rs.*');
        $this->db->from('reg_stages rs');
        $this->db->where('rs.event_id', $event_id);
        $this->db->where('rs.id != ', $id);
        $this->db->where('id NOT IN (select rq.stage_id from reg_skiplogic rsl join reg_que rq on rq.id= rsl.que_id where rsl.redriect_stage_id=' . $id . ' and rsl.event_id=' . $event_id . ')');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function update_screen_data($where=NULL, $update=NULL)

    {
        $res = $this->db->where($where)->get('registration_screen_data')->row_array();
        if (!empty($res))
        {
            $this->db->where($where);
            $this->db->update('registration_screen_data', $update);
        }
        else
        {
            $update['event_id'] = $where['event_id'];
            $this->db->insert('registration_screen_data', $update);
        }
    }
    public function add_discount($where = '', $update=NULL)

    {
        // echo "<pre>"; print_r($update);
        if ($update['code_type'] == '0')
        {
            $res = $this->db->where($where)->get('reg_discount')->row_array();
            if (empty($res))
            {
                $update['que_id'] = $where['que_id'];
                // echo "<pre>"; print_r($update); exit();
                $this->db->insert('reg_discount', $update);
            }
            else
            {
                $this->db->where($where);
                $this->db->update('reg_discount', $update);
            }
        }
        else
        {
            $where['event_id'] = $update['event_id'];
            $where['code_type'] = '1';
            $res = $this->db->where($where)->get('reg_discount')->row_array();
            if (empty($res))
            {
                $update['que_id'] = NULL;
                $this->db->insert('reg_discount', $update);
            }
            else
            {
                $this->db->where($where);
                $this->db->update('reg_discount', $update);
            }
        }
    }
    public function get_all_disc($event_id=NULL)

    {
        $this->db->select();
        $this->db->where('event_id', $event_id);
        $this->db->where('code_type', '1');
        $res = $this->db->get('reg_discount')->row_array();
        return $res;
    }
    public function get_badge_data($event_id=NULL)

    {
        $this->db->select();
        $this->db->where('event_id', $event_id);
        $res = $this->db->get('badges_values')->row_array();
        return $res;
    }
    public function save_badge_data($where=NULL, $update=NULL)

    {
        $res = $this->db->select()->where($where)->get('badges_values')->row_array();
        if (empty($res))
        {
            $update['event_id'] = $where['event_id'];
            $this->db->insert('badges_values', $update);
        }
        else
        {
            $this->db->where($where)->update('badges_values', $update);
        }
    }
    public function get_tickets($id=NULL)

    {
        $res = $this->db->select()->where('event_id', $id)->get('reg_tickets')->result_array();
        return $res;
    }
    public function save_ticket($data=NULL)

    {
        $this->db->insert('reg_tickets', $data);
    }
    public function delete_ticket($where=NULL)

    {
        $this->db->delete('reg_tickets', $where);
    }
    public function update_ticket($where=NULL, $update=NULL)

    {
        $this->db->where($where)->update('reg_tickets', $update);
    }
    public function update_option_in_skiplogic($qid=NULL, $old_val=NULL, $new_val=NULL)

    {
        $this->db->where('que_id', $qid);
        $this->db->where('option', $old_val);
        $this->db->update('reg_skiplogic', array(
            'option' => $new_val
        ));
    }
    public function delete_skip_login_option($qid=NULL, $old_val=NULL)

    {
        $this->db->where('que_id', $qid);
        $this->db->where('option', $old_val);
        $this->db->delete('reg_skiplogic');
    }
    public function reg_show_in_front($event_id=NULL, $flag=NULL)

    {
        $res = $this->db->where('menu_id', '55')->where('event_id', $event_id)->get('event_menu')->row_array();
        if (!empty($res))
        {
            $update['show_in_front'] = $flag;
            $this->db->where('id', $res['id']);
            $this->db->update('event_menu', $update);
        }
        else
        {
            $insert['menu_id'] = '55';
            $insert['title'] = 'Registration';
            $insert['event_id'] = $event_id;
            $insert['show_in_front'] = $flag;
            $this->db->insert('event_menu', $insert);
        }
    }
}