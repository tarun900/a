<?php
class Role_management_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function insert_permssion($data=NULL)

    {
        $this->db->insert('role_permission', $data);
    }
    public function get_edit_event($eid=NULL)

    {
        $query = "select Id,Event_name from event where Id=$eid";
        $res = $this->db->query($query);
        return $res->result_array();
    }
    public function get_all_event()

    {
        $query = "select Id,Event_name from event";
        $res = $this->db->query($query);
        return $res->result_array();
    }
    public function add_role($array_add=NULL, $per_arr=NULL, $eventid=NULL)

    {
        $this->db->insert('role', $array_add);
        $roleid = $this->db->insert_id();
        foreach($per_arr['Menu_id'] as $key => $value)
        {
            $arr = array();
            $arr['Role_id'] = $roleid;
            $arr['Menu_id'] = $value;
            $arr['event_id'] = $eventid;
            $this->db->insert('role_permission', $arr);
        }
        $this->session->set_flashdata('user_data', 'Added');
    }
    public function get_last_roleid()

    {
        $query = "select Id from role order by Id DESC limit 1";
        $res = $this->db->query($query);
        $id = $res->result_array();
        return $id;
    }
    public function delete_permssion($role_id=NULL, $menu_id=NULL)

    {
        $this->db->where('Role_id', $role_id);
        $this->db->where('Menu_id', $menu_id);
        $this->db->delete('role_permission');
    }
    public function update_permssion($data=NULL, $role_id=NULL, $menu_id=NULL)

    {
        $this->db->where('Role_id', $role_id);
        $this->db->where('Menu_id', $menu_id);
        $this->db->update('role_permission', $data);
    }
    public function get_role_list($id=NULL)

    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('id IN (select Role_id from role_permission where event_id=' . $id . ')', NULL, FALSE);
        $this->db->where('Id !=', 3);
        $this->db->where('Id !=', 4);
        $this->db->where('Id !=', 5);
        $this->db->where('Id !=', 6);
        $this->db->where('Id !=', 7);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_login_role($roleid=NULL)

    {
        $qry = $this->db->query("select menuname from menu where id in (select Menu_id from role_permission where Role_id=$roleid)");
        $res = $qry->result_array();
        return $res;
    }
    /*public function get_role_byid($id)
    {
    $qry = $this->db->query("select * from role where Id=$id");
    $res = $qry->result_array();
    return $res;
    }*/
    public function get_role_byid($id=NULL, $event_id = null)

    {
        $this->db->select('*')->from('role r');
        if ($event_id != null)
        {
            $this->db->join('role_permission rp', 'rp.Role_id = r.Id');
            $this->db->where('rp.event_id', $event_id);
        }
        $this->db->where('r.Id', $id);
        $res = $this->db->get()->result_array();
        if (!$res && $this->uri->segment(2) == 'edit' && $event_id != null) return redirect('Forbidden');
        return $res;
    }
    public function update_admin_role($data=NULL)

    {
        $array_role1 = array();
        if ($data['role_array']['Name'] != NULL) $array_role1['Name'] = $data['role_array']['Name'];
        if ($data['role_array']['status'] != NULL) $array_role1['status'] = $data['role_array']['status'];
        if ($data['role_array']['Active'] != NULL) $array_role1['Active'] = $data['role_array']['Active'];
        $this->db->where('Id', $data['role_array']['Id']);
        $this->db->update('role', $array_role1);
    }
    public function get_role_menu($roleid=NULL, $event_id=NULL)

    {
        $this->db->select('Menu_id');
        $this->db->from('role_permission rp');
        $this->db->where('rp.Role_id', $roleid);
        $this->db->where('rp.event_id', $event_id);
        $this->db->order_by('rp.Menu_id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_role_menu_by_id($roleid=NULL)

    {
        $roleid = $this->data['user']->Role_id;
        $this->db->select('Menu_id');
        $this->db->from('role_permission rp');
        $this->db->where('rp.Role_id', $roleid);
        $this->db->order_by('rp.Menu_id');
        $query = $this->db->get();
        $res = $query->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('*');
            $this->db->from('menu m');
            $this->db->where('m.Id', $value['Menu_id']);
            $query = $this->db->get();
            $res = $query->result_array();
            return $res;
        }
        exit;
    }
    public function delete_role($id=NULL)

    {
        $this->db->where_in('Id', $id);
        $this->db->delete('role');
    }
    public function delete_role1()

    {
        $this->db->select('GROUP_CONCAT(User_id) AS userid');
        $this->db->from('relation_event_user');
        $this->db->where('Role_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        $data['is_deleted'] = '1';
        $this->db->where('Id', $id);
        $this->db->update('role', $data);
        echo "<pre>";
        print_r($res);
        die;
        /*
        $this->db->where_in('Id', $id);
        $this->db->delete('role');
        $this->db->where_in('Role_id', $id);
        $this->db->delete('role_permission');
        $this->db->select('User_id');
        $this->db->from('relation_event_user');
        $this->db->where('Role_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->where_in('Role_id', $id);
        $this->db->delete('relation_event_user');*/
    }
    public function insertdefultrole($eventid=NULL, $roleid=NULL, $Menu_id=NULL)

    {
        $arr['Role_id'] = $roleid;
        $arr['Menu_id'] = $Menu_id;
        $arr['event_id'] = $eventid;
        $this->db->insert('role_permission', $arr);
    }
    public function checkrole($role=NULL, $id = null, $eventid = null)

    {
        /*
        select * from role_permission
        where Role_id in (SELECT id FROM `role`
        where Name='Manager' and id!=29)
        and event_id=112 ORDER BY `Id` DESC
        */
        $this->db->select('*');
        $this->db->from('role_permission rp');
        $this->db->where('rp.event_id', $eventid);
        $string = "";
        if (!empty($id))
        {
            $string = "and id!=" . $id;
        }
        $this->db->where('rp.Role_id IN (SELECT id FROM `role` where Name="' . $role . '" ' . $string . ')', NULL, FALSE);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
?>