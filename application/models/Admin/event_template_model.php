<?php
class Event_template_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function get_event_template_list($id=null)
    {
         echo $id;
         exit;

        $user = $this->session->userdata('current_user');
        /*for ($i = 0; $i < count($user); $i++) 
        { */
            $this->db->select('e.*');
            $this->db->from('event e');
            $this->db->join('user u', 'u.id=e.Organisor_id','left');
            //$user[0]->Id;
            if($user[0]->Id)
            {
                $this->db->where('e.Organisor_id',$user[0]->Id);
            }
            //$this->db->where("e.End_date >= CURRENT_DATE()");
       /* }*/

        $query = $this->db->get();
        $res = $query->result();    

        $event_res  = array();
        for($i=0;$i<count($res);$i++)
            {
                $event_res[$i]['Id']=$res[$i]->Id;
                $event_res[$i]['Event_name']=$res[$i]->Event_name;
                $event_res[$i]['Status']=$res[$i]->Status;
                $event_res[$i]['Event_type']=$res[$i]->Event_type;
                $event_res[$i]['Event_time']=$res[$i]->Event_time;
                $event_res[$i]['Subdomain']=$res[$i]->Subdomain;
                $event_res[$i]['Description']=$res[$i]->Description;
                $event_res[$i]['Images']=$res[$i]->Images;
                $event_res[$i]['Created_date']=$res[$i]->Created_date;
                $event_res[$i]['Start_date']=$res[$i]->Start_date;
                $event_res[$i]['End_date']=$res[$i]->End_date;
                $event_res[$i]['Organisor_id']=$res[$i]->Organisor_id;
                $event_res[$i]['Address']=$res[$i]->Address;
                $event_res[$i]['Lattitude']=$res[$i]->Lattitude;
                $event_res[$i]['Lattitude']=$res[$i]->Lattitude;
                $event_res[$i]['Background_color']=$res[$i]->Background_color;
                $event_res[$i]['Contact_us']=$res[$i]->Contact_us;
 
            }
            if($this->data['user']->Role_name == 'Client')
            {
                  $event_res_val=  array_merge(array(),$event_res); 
            }
            else if($this->data['user']->Role_name == 'Attendee')
            {
                  $event_res_val=  array_merge(array(),$event_res); 
            }
            else
            {
                $event_res_val = $event_res;
            }
           return $event_res_val;
    }

    public function get_event_template_by_id_list($Subdomain=null)
    {
        $user = $this->session->userdata('current_user');
        /*for ($i = 0; $i < count($user); $i++) 
        { */

            $this->db->select('e.*');
            $this->db->from('event e');
            /*$this->db->where('e.Id',$id);*/
            $this->db->where('e.Subdomain',$Subdomain);
            $this->db->join('user u', 'u.id=e.Organisor_id','left');
            if($user[0]->Id)
            {
                $this->db->where('e.Organisor_id',$user[0]->Id);
            }
            //$this->db->where("e.End_date >= CURRENT_DATE()");

        /*}*/
            $query = $this->db->get();
            $res = $query->result();    
                
        $event_res = array();
        for($i=0;$i<count($res);$i++)
        {
            $event_res[$i]['Id']=$res[$i]->Id;
            $event_res[$i]['Event_name']=$res[$i]->Event_name;
            $event_res[$i]['Description']=$res[$i]->Description;
            $event_res[$i]['Subdomain']=$res[$i]->Subdomain;
            $event_res[$i]['Address']=$res[$i]->Address;
            $event_res[$i]['Lattitude']=$res[$i]->Lattitude;
            $event_res[$i]['Longitude']=$res[$i]->Longitude;
            $event_res[$i]['Images']=$res[$i]->Images;
            $event_res[$i]['Background_color']=$res[$i]->Background_color;
            $event_res[$i]['Start_date']=$res[$i]->Start_date;
            $event_res[$i]['End_date']=$res[$i]->End_date;
            $event_res[$i]['Contact_us']=$res[$i]->Contact_us;
            $event_res[$i]['Created_date']=$res[$i]->Created_date;
            $event_res[$i]['Status']=$res[$i]->Status;
            $event_res[$i]['Event_type']=$res[$i]->Event_type;
            $event_res[$i]['Event_time']=$res[$i]->Event_time;
        }
       return $event_res;
    }

}
        
?>