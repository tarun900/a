<?php
class Exibitor_page_model extends CI_Model

{
     function __construct()
     {
          parent::__construct();
     }
     public function check_auth($title=NULL, $roleid=NULL, $rolename=NULL, $eventid=NULL)

     {
          if ($rolename == 'Client')
          {
               return 1;
          }
          else
          {
               $this->db->select('id');
               $this->db->from('menu');
               $this->db->where('pagetitle', $title);
               $query = $this->db->get();
               $res = $query->result_array();
               $menuid = $res[0]['id'];
               $this->db->select('Menu_id');
               $this->db->from('role_permission');
               $this->db->where('Role_id', $roleid);
               $this->db->where('Menu_id', $menuid);
               $this->db->where('Event_id', $eventid);
               $query1 = $this->db->get();
               $res1 = $query1->result_array();
               if (count($res1) >= 1)
               {
                    return 1;
               }
               else
               {
                    return 0;
               }
          }
     }
     public function add_exibitor($data=NULL)

     {
          if (!empty($data['exibitor_array']['company_logo']))
          {
               $company_logo[] = $data['exibitor_array']['company_logo'];
          }
          $array_exibitor['company_logo'] = json_encode($company_logo);
          if (!empty($data['exibitor_array']['Images']))
          {
               foreach($data['exibitor_array']['Images'] as $k => $v)
               {
                    $Images[] = $v;
               }
          }
          $array_exibitor['Images'] = json_encode($Images);
          if ($data['exibitor_array']['Organisor_id'] != NULL) $array_exibitor['Organisor_id'] = $data['exibitor_array']['Organisor_id'];
          if ($data['exibitor_array']['Event_id'] != NULL) $array_exibitor['Event_id'] = $data['exibitor_array']['Event_id'];
          if ($data['exibitor_array']['Heading'] != NULL) $array_exibitor['Heading'] = $data['exibitor_array']['Heading'];
          if ($data['exibitor_array']['Short_desc'] != NULL) $array_exibitor['Short_desc'] = $data['exibitor_array']['Short_desc'];
          if ($data['exibitor_array']['Status'] != NULL) $array_exibitor['Status'] = $data['exibitor_array']['Status'];
          if ($data['exibitor_array']['website_url'] != NULL) $array_exibitor['website_url'] = $data['exibitor_array']['website_url'];
          if ($data['exibitor_array']['facebook_url'] != NULL) $array_exibitor['facebook_url'] = $data['exibitor_array']['facebook_url'];
          if ($data['exibitor_array']['twitter_url'] != NULL) $array_exibitor['twitter_url'] = $data['exibitor_array']['twitter_url'];
          if ($data['exibitor_array']['linkedin_url'] != NULL) $array_exibitor['linkedin_url'] = $data['exibitor_array']['linkedin_url'];
          if ($data['exibitor_array']['phone_number1'] != NULL) $array_exibitor['phone_number1'] = $data['exibitor_array']['phone_number1'];
          if ($data['exibitor_array']['phone_number2'] != NULL) $array_exibitor['phone_number2'] = $data['exibitor_array']['phone_number2'];
          if ($data['exibitor_array']['email_address'] != NULL) $array_exibitor['email_address'] = $data['exibitor_array']['email_address'];
          $this->db->insert('exibitor', $array_exibitor);
     }
     public function get_exibitor_list($id = null)

     {
          $total_permission = $this->exibitor_page_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;
          $orid = $this->data['user']->Id;
          $orid = $this->data['user']->Id;
          $this->db->select('e.*');
          $this->db->from('exibitor e');
          $this->db->where('e.Event_id', $id);
          $this->db->where('e.User_id', $orid);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function get_exibitor_by_id($id = null, $eid = null)

     {
          $total_permission = $this->exibitor_page_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
          $Organisor_id = $total_permission[0]->Organisor_id;
          $orid = $this->data['user']->Id;
          $this->db->select('e.*');
          $this->db->from('exibitor e');
          $this->db->where('e.Event_id', $id);
          $this->db->where('e.Id', $eid);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     public function update_exibitor($data=NULL)

     {
          if (!empty($data['exibitor_array']['company_logo']))
          {
               $company_logo[] = $data['exibitor_array']['company_logo'];
               $array_exibitor['company_logo'] = json_encode($company_logo);
          }
          $images = array();
          if (!empty($data['exibitor_array']['Images']))
          {
               foreach($data['exibitor_array']['Images'] as $k => $v)
               {
                    $images[] = $v;
               }
          }
          $array_exibitor['Images'] = json_encode($images);
          if (!empty($data['exibitor_array']['old_images']))
          {
               foreach($data['exibitor_array']['old_images'] as $k => $v)
               {
                    $images[] = $v;
               }
          }
          $array_exibitor['Images'] = json_encode($images);
          if ($data['exibitor_array']['Heading'] != NULL) $array_exibitor['Heading'] = $data['exibitor_array']['Heading'];
          if ($data['exibitor_array']['Short_desc'] != NULL) $array_exibitor['Short_desc'] = $data['exibitor_array']['Short_desc'];
          if ($data['exibitor_array']['Status'] != NULL) $array_exibitor['Status'] = $data['exibitor_array']['Status'];
          if ($data['exibitor_array']['website_url'] != NULL) $array_exibitor['website_url'] = $data['exibitor_array']['website_url'];
          if ($data['exibitor_array']['facebook_url'] != NULL) $array_exibitor['facebook_url'] = $data['exibitor_array']['facebook_url'];
          if ($data['exibitor_array']['twitter_url'] != NULL) $array_exibitor['twitter_url'] = $data['exibitor_array']['twitter_url'];
          if ($data['exibitor_array']['linkedin_url'] != NULL) $array_exibitor['linkedin_url'] = $data['exibitor_array']['linkedin_url'];
          if ($data['exibitor_array']['phone_number1'] != NULL) $array_exibitor['phone_number1'] = $data['exibitor_array']['phone_number1'];
          if ($data['exibitor_array']['phone_number2'] != NULL) $array_exibitor['phone_number2'] = $data['exibitor_array']['phone_number2'];
          if ($data['exibitor_array']['email_address'] != NULL) $array_exibitor['email_address'] = $data['exibitor_array']['email_address'];
          $this->db->where('Id', $data['exibitor_array']['Id']);
          $this->db->update('exibitor', $array_exibitor);
     }
     public function get_permission_list()

     {
          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $this->db->select('*');
               $this->db->from('user');
               if ($orid)
               {
                    $this->db->where('user.Id', $orid);
               }
               $query = $this->db->get();
               $res = $query->result();
               return $res;
          }
     }
     public function delete_exibitor($id=NULL, $Event_id=NULL)

     {
          $this->db->where('Id', $id);
          $this->db->delete('exibitor');
          $str = $this->db->last_query();
     }
     public function get_menu_name($id=NULL)

     {
          $orid = $this->data['user']->Id;
          $this->db->select('*');
          $this->db->from('menu_Setting');
          $this->db->where('Event_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
}
?>