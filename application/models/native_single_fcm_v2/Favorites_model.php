<?php
class Favorites_model extends CI_Model

{
  function __construct()
  {
    $this->db1 = $this->load->database('db1', TRUE);
    parent::__construct();
  }
  public function addOrRemoveFavorites($favorites_data=NULL)

  {
    $count = $this->db->select('*')->from('my_favorites')->where($favorites_data)->get()->num_rows();
    if ($count)
    {
      $this->db->where($favorites_data);
      $this->db->delete('my_favorites');
      $return = 0;
    }
    else
    {
      $favorites_data['datetime'] = date('Y-m-d H:i:s');
      $this->db->insert('my_favorites', $favorites_data);
      $return = 1;
    }
    return $return;
  }
  public function getAllFavorites($where=NULL)

  {
    $array = [];
    $this->db->select('m.Id as modules_id,(case WHEN em.title IS NULL THEN m.menuname ELSE em.title end) as menuname,(case when fmo.color IS NULL THEN "#5381ce" else fmo.color end) as color,fmo.position')->from('menu m');
    $this->db->join('event_menu em', 'm.Id=em.menu_id', 'left');
    $this->db->join('favorites_modules_order fmo', 'm.Id=fmo.modules_id AND fmo.event_id=' . $where['mf.event_id'], 'left');
    $this->db->where('em.event_id', $where['mf.event_id']);
    $this->db->where_in('m.Id', array(
      '2',
      '3',
      '7',
      '43'
    ));
    $this->db->order_by('fmo.position');
    $res = $this->db->get()->result_array();
    foreach($res as $key => $value)
    {
      $this->db->select('mf.Id,mf.datetime,mf.module_id,mf.module_type,mf.user_id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.stand_number IS NULL THEN "" END as stand_number,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as ex_logo,CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name,CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo1', false)->from('my_favorites mf');
      $this->db->join('user u', 'u.Id=mf.module_id AND (mf.module_type="2" OR mf.module_type="7")', 'left');
      $this->db->join('exibitor e', 'e.Id=mf.module_id AND mf.module_type="3"', 'left');
      $this->db->join('sponsors s', 's.Id=mf.module_id AND mf.module_type="43"', 'left');
      $this->db->where($where);
      $this->db->where('mf.module_type', $value['modules_id']);
      $data = $this->db->get()->result_array();
      // echo $this->db->last_query(); exit();
      if (!empty($data))
      {
        $final['type'] = $value['menuname'];
        $final['color'] = $value['color'];
        $final['data'] = $data;
        $array[] = $final;
      }
    }
    foreach($array as $key1 => $value1)
    {
      foreach($value1['data'] as $key => $value)
      {
        array_walk($value, function (&$item)
        {
          $item = strval($item);
        });
        $array[$key1]['data'][$key]['Id'] = $value['Id'];
        $array[$key1]['data'][$key]['module_id'] = $value['module_id'];
        $array[$key1]['data'][$key]['module_type'] = $value['module_type'];
        switch ($value['module_type'])
        {
        case '3':
          $cmpy_logo_decode = json_decode($value['ex_logo']);
          $array[$key1]['data'][$key]['user_name'] = ($value['Heading']) ? $value['Heading'] : '';
          $array[$key1]['data'][$key]['logo'] = ($cmpy_logo_decode) ? $cmpy_logo_decode[0] : '';
          $array[$key1]['data'][$key]['extra'] = ($value['stand_number']) ? $value['stand_number'] : '';
          break;

        case '43':
          $cmpy_logo_decode = json_decode($value['company_logo1']);
          $array[$key1]['data'][$key]['user_name'] = ($value['Sponsors_name']) ? $value['Sponsors_name'] : '';
          $array[$key1]['data'][$key]['logo'] = ($cmpy_logo_decode) ? $cmpy_logo_decode[0] : '';
          $array[$key1]['data'][$key]['extra'] = ($value['Company_name']) ? $value['Company_name'] : '';
          break;

        default:
          $name = $value['Firstname'] . " " . $value['Lastname'];
          $array[$key1]['data'][$key]['user_name'] = ($name) ? $name : '';
          $array[$key1]['data'][$key]['logo'] = ($value['Logo']) ? $value['Logo'] : '';
          $array[$key1]['data'][$key]['extra'] = ($value['Company_name']) ? $value['Company_name'] : '';
          break;
        }
        // $temp[] = $return_data;
      }
      /* $fav['type'] = $value1['type'];
      $fav['color'] = $value1['color'];
      $fav['data'] = $temp;
      $favotitrs[] = $fav;*/
    }
    return $array;
  }
  public function getFavoritesType($menu_id=NULL)

  {
    switch ($menu_id)
    {
    case '2':
      $return = "Attendees";
      break;

    case '3':
      $return = "Exhibitors";
      break;

    case '43':
      $return = "Sponsors";
      break;

    case '7':
      $return = "Speaker";
      break;

    default:
      $return = "";
      break;
    }
    return $return;
  }
}