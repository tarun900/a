<?php
class App_login_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->db1 = $this->load->database('db1', TRUE);
    }
    public function fb_signup($email=NULL, $facebook_id=NULL, $event_id=NULL, $firstname=NULL, $facebook_logo=NULL, $device=NULL)

    {
        $this->db->select('u.*,CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as User_id,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id', FALSE);
        $this->db->from('user u');
        $this->db->where('u.Email', $email);
        // $this->db->where('u.facebook_id',$facebook_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $data = $res[0];
        if (!empty($res))
        {
            $up['Logo'] = $facebook_logo;
            $up['facebook_id'] = $facebook_id;
            $up['Login_date'] = date('Y-m-d h:i:s');
            $this->db->where('Email', $email);
            // $this->db->where('facebook_id',$facebook_id);
            $this->db->update('user', $up);
            $event_arr["Event_id"] = $event_id;
            $event_arr["User_id"] = $data['Id'];
            $event_arr["Role_id"] = 4;
            // $event_arr["Organisor_id"]=$data['Organisor_id'];
            $this->db->select('Organisor_id')->from('event e');
            $this->db->where('e.id', $event_id);
            $qu = $this->db->get();
            $res = $qu->result_array();
            $Organisor_id = $res[0]['Organisor_id'];
            $event_arr["Organisor_id"] = $Organisor_id;
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id', $event_id);
            $this->db->where('User_id', $data['Id']);
            $this->db->where('Organisor_id', $data['Organisor_id']);
            $qu = $this->db->get();
            $res = $qu->row();
            if (!count($res))
            {
                $this->db->insert("relation_event_user", $event_arr);
            }
            $data1["Rid"] = $res->Role_id;
            $data1 = $this->getUserDetailsId($data['Id'],$event_id);
            array_walk($data1, function (&$item)
            {
                $item = strval($item);
            });
            $return['status'] = "0";
            $return['data'] = $data1;
            return $return;
        }
        else
        {
            $this->db->select('*')->from('event e');
            $this->db->where('e.id', $event_id);
            $qu = $this->db->get();
            $res = $qu->result_array();
            $Organisor_id = $res[0]['Organisor_id'];
            $data['Organisor_id'] = $Organisor_id;
            $data['Active'] = '1';
            $data['token'] = sha1($email . $facebook_id . $event_id);
            $data['Email'] = $email;
            $data['facebook_id'] = $facebook_id;
            $data['Logo'] = $facebook_logo;
            $data['Firstname'] = $firstname;
            $data['device'] = $device;
            $data['token'] = sha1($email . $password . $event_id);
            $this->db->insert("user", $data);
            $user_id = $this->db->insert_id();
            $event_arr["Event_id"] = $event_id;
            $event_arr["User_id"] = $user_id;
            $event_arr["Role_id"] = 4;
            $event_arr["Organisor_id"] = $Organisor_id;
            $count = $this->db->select('*')->from('relation_event_user')->where($event_arr)->get()->num_rows();
            if ($count == 0) $this->db->insert("relation_event_user", $event_arr);
            $data = $this->getUserDetailsId($user_id,$event_id);
            // $data["Rid"]=4;
            $return['status'] = "1";
            $return['data'] = $data;
            return $return;
        }
    }
    public function getAllCountryList()

    {
        $this->db->select('*')->from('country');
        $this->db->order_by("country_name");
        $qu = $this->db->get();
        $res = $qu->result_array();
        $i = 2;
        $j = 0;
        $arr;
        foreach($res as $key => $value)
        {
            if ($value['id'] == 223 || $value['id'] == 225)
            {
                $arr[$j] = $value;
                $j++;
            }
            else
            {
                $arr[$i] = $value;
                $i++;
            }
        }
        ksort($arr);
        return $arr;
    }
    public function checkEmailAlreadyByEvent($email=NULL, $event_id=NULL)

    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs", "rs.User_id=u.Id");
        $this->db->where('u.Email', $email);
        $this->db->where('rs.Event_id', $event_id);
        $this->db->where(array(
            'u.facebook_id' => ''
        ));
        $qu = $this->db->get();
        $res = $qu->result_array();
        $cnt = count($res);
        return $cnt;
    }
    public function getCountry($country_id=NULL)

    {
        $this->db->select('c.country_name')->from('country c');
        $this->db->where('c.id', $country_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (!empty($res))
        {
            $country = $res[0]['country_name'];
            return $country;
        }
        else
        {
            return "";
        }
    }
    public function getState($state_id=NULL)

    {
        $this->db->select('s.state_name')->from('state s');
        $this->db->where('s.id', $state_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (!empty($res))
        {
            $state = $res[0]['state_name'];
            return $state;
        }
        else
        {
            return "";
        }
    }
    public function checkEmailAlreadyByEvent1($email=NULL, $event_id=NULL)

    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs", "rs.User_id=u.Id");
        $this->db->where('u.Email', $email);
        $this->db->where('rs.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $cnt = count($res);
        return $cnt;
    }
    public function checkUniqueNoAlreadyByEvent($unique_no=NULL, $event_id=NULL)

    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs", "rs.User_id=u.Id");
        $this->db->where('u.Unique_no', $unique_no);
        $this->db->where('rs.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $cnt = count($res);
        return $cnt;
    }
    public function check_email_and_unique_no_by_event($email=NULL, $unique_no=NULL, $event_id=NULL)

    {
        $this->db->select('u.Id')->from('user u');
        $this->db->join("relation_event_user rs", "rs.User_id=u.Id");
        $this->db->where('u.Email', $email);
        $this->db->where('u.Unique_no', $unique_no);
        $this->db->where('rs.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $cnt = count($res);
        return $cnt;
    }
    public function getUserDetailsId($user_id=NULL, $event_id=NULL)

    {
        $this->db->select('u.*,
                    CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,
                    CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,
                    CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,
                    CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,
                    CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                    CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,
                    CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                    CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                    CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                    CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,
                    CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,
                    CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,
                    CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,
                    CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,
                    CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                    CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,
                    CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,
                    CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,
                    CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,
                    CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,
                    CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,
                    CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id', FALSE);
        $this->db->from('user u');
        $this->db->where('u.Id', $user_id);
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('ru.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id', $res[0]['Id']);
        $links = $this->db->get();
        $links = $links->row_array();
        $res[0]['linkedin_url'] = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
        $res[0]['facebook_url'] = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
        $res[0]['twitter_url'] = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
        $res[0]['Website_url'] = ($links['Website_url']) ? $links['Website_url'] : '';
        $res[0]['Password'] = '';
        $res = $res[0];
        array_walk($res, function (&$item)
        {
            $item = strval($item);
        });
        return $res;
    }
    public function check_app_login($email=NULL, $password=NULL, $event_id=NULL)

    {
        $this->db->select('u.*,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id,ru.Organisor_id as Organisor_id', FALSE);
        $this->db->from('user u');
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('u.Email', $email);
        if ($event_id != '447') $this->db->where('u.Password', md5($password));
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->where('ru.Event_id', $event_id);
        $this->db->where('ru.Event_id IS NOT NULL');
        $this->db->limit(1);
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        $res = $query->result();
        // print_r($res);exit;
        if (!empty($res))
        {
            unset($res[0]->Speaker_desc);
            if ($event_id == '447' && ($res[0]->Rid == '6' || $res[0]->Rid == '4'))
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $pass = $rc4->decrypt($res[0]->Password);
                if ($pass != $password) $res = array();
            }
            $res[0]->Event_id = $event_id;
        }
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id', $res[0]->Organisor_id);
        $qu = $this->db->get();
        $organizeruser = $qu->result_array();
        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id', $res[0]->Id);
        $links = $this->db->get();
        $links = $links->row_array();
        $res[0]->linkedin_url = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
        $res[0]->facebook_url = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
        $res[0]->twitter_url = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
        $res[0]->Website_url = ($links['Website_url']) ? $links['Website_url'] : '';
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('Email', $organizeruser[0]['Email']);
        $this->db1->where('Expiry_date >=', date("Y-m-d"));
        $this->db1->join('subscription_billing_cycle sbc', 'ou.subscriptiontype=sbc.id');
        $query = $this->db1->get();
        $res1 = $query->result_array();
        if (count($res1) == 0 || $res1[0]['status'] == 0)
        {
            if (count($res) == 0)
            {
                $res = $res1;
            }
            else
            {
                $res = $res1;
                $res = "inactive";
            }
        }
        // check lead representative disable account
        $data = $this->db->select('*')->from('user_representatives')->where('event_id', $event_id)->where('rep_id', $res[0]->Id)->get()->row_array();
        if ($data && $data['status'] == '0')
        {
            $res = "inactive1";
        }
        return $res;
    }
    public function get_additionalFormData($user_id=NULL)

    {
        $this->db->select('json_data');
        $this->db->from('signup_form_data');
        $this->db->where('user_id', $user_id);
        $qu = $this->db->get();
        $data = $qu->result_array();
        return $data;
    }
    public function check_token_with_event($token=NULL, $event_id=NULL, $type=NULL)

    {
        if ($event_id == '634')
        {
            $data[] = true;
            $data[] = '123';
            return $data;
        }
        $token = ($token == NULL) ? '' : $token;
        $this->db->select('e.*,u.Id as user_id');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->where('ru.Event_id', $event_id);
        $this->db->limit(1);
        /*if($type==1 || $type==2)
        {
        if($token=="")
        {
        $token="NULL";
        }
        $this->db->where('u.token', $token);
        }*/
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function check_token($token=NULL)

    {
        $this->db->select('u.*,role.Name as Role_name,u.Id as Id,role.Id as Rid,mobile_data.token as _token');
        $this->db->from('mobile_data');
        $this->db->where('token', $token);
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->join('user as u', 'u.Id=mobile_data.uid');
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function check_email_with_event($token=NULL, $email=NULL)

    {
        $this->db->select('e.Id,e.Start_date,e.End_date,e.Subdomain,coalesce(e.description," ") as description1,e.Created_date,e.Event_name,e.Organisor_id,e.Img_view,e.Status,e.Event_type,e.Event_time,e.Event_time_option,e.checkbox_values,coalesce(e.fun_header_status," ") as fun_header_status1,coalesce(e.Background_img," ") as Background_img1,u.Id as user_id, coalesce(Logo_images," ") as Logo_images1, coalesce(fun_logo_images," ") as fun_logo_images1, coalesce(Contact_us," ") as Contact_us1, coalesce(Icon_text_color," ") as Icon_text_color1, coalesce(fun_top_background_color," ") as fun_top_background_color1, coalesce(fun_top_text_color," ") as fun_top_text_color1, coalesce(fun_footer_background_color," ") as fun_footer_background_color1, coalesce(fun_block_background_color," ") as fun_block_background_color1, coalesce(fun_block_text_color," ") as fun_block_text_color1, coalesce(theme_color," ") as theme_color1, coalesce(theme_color," ") as theme_color1', FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        // $this->db->join('mobile_data m', 'm.uid = u.Id');
        $this->db->where('u.token', $token);
        $this->db->where('u.email', $email);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function check_event_with_id($event_id=NULL, $user_id = 0)

    {
        $this->db->select('e.*,CASE WHEN e.Description IS NULL THEN "" ELSE e.Description END as Description,
            CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
            CASE WHEN e.Logo_images IS NULL THEN "" ELSE e.Logo_images END as Logo_images,
            CASE WHEN e.Background_img IS NULL THEN "" ELSE e.Background_img END as Background_img,
            CASE WHEN e.Background_color IS NULL THEN "#FFFFFF" ELSE e.Background_color END as Background_color,
            CASE WHEN e.Top_background_color IS NULL THEN "#FFFFFF" ELSE e.Top_background_color END as Top_background_color,
            CASE WHEN e.Top_text_color IS NULL THEN "#FFFFFF" ELSE e.Top_text_color END as Top_text_color,
            CASE WHEN e.Icon_text_color IS NULL THEN "#FFFFFF" ELSE e.Icon_text_color END as Icon_text_color,
            CASE WHEN e.Footer_background_color IS NULL THEN "#FFFFFF" ELSE e.Footer_background_color END as Footer_background_color,
            CASE WHEN e.fun_logo_images IS NULL THEN "" ELSE e.fun_logo_images END as fun_logo_images,
            CASE WHEN e.fun_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_background_color END as fun_background_color,
            CASE WHEN e.fun_top_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_background_color END as fun_top_background_color,
            CASE WHEN e.fun_top_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_text_color END as fun_top_text_color,
            CASE WHEN e.fun_footer_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_footer_background_color END as fun_footer_background_color,
            CASE WHEN e.fun_block_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_background_color END as fun_block_background_color,
            CASE WHEN e.fun_block_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_text_color END as fun_block_text_color,
            CASE WHEN e.theme_color IS NULL THEN "#FFFFFF" ELSE e.theme_color END as theme_color,
            CASE WHEN e.Contact_us IS NULL THEN "" ELSE e.Contact_us END as Contact_us,
            CASE WHEN e.checkbox_values IS NULL THEN "" ELSE e.checkbox_values END as checkbox_values,
            CASE WHEN e.fun_header_status IS NULL THEN "" ELSE e.fun_header_status END as fun_header_status,
            CASE WHEN e.secure_key IS NULL THEN "" ELSE e.secure_key END as secure_key,
            CASE WHEN e.Beacon_feature IS NULL THEN "" ELSE e.Beacon_feature END as Beacon_feature,
            ru.Role_id,CASE WHEN e.UUID IS NULL THEN "" ELSE e.UUID END as UUID,
            CASE WHEN e.photo_filter_image IS NULL THEN "" ELSE e.photo_filter_image END as photo_filter_image,
            CASE WHEN e.agenda_sort IS NULL THEN "" ELSE e.agenda_sort END as agenda_sort,
            CASE WHEN e.activity_share_icon_setting IS NULL THEN "" ELSE e.activity_share_icon_setting END as activity_share_icon_setting,
            e.show_slider', FALSE);
        $this->db->from('event e');
        $this->db->where('e.Id', $event_id);
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        if ($user_id != 0)
        {
            $this->db->where('ru.User_id', $user_id);
        }
        $this->db->group_by("e.Id");
        $query = $this->db->get();
        $res = $query->result_array();
        if ($res[0]['Role_id'] == '4')
        {
            $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user_id)->get()->row_array();
            $extra = json_decode($extra['extra_column'], true);
            foreach($extra as $key => $value)
            {
                $keyword = "{" . str_replace(' ', '', $key) . "}";
                if (stripos(strip_tags($res[0]['Description'], $keyword)) !== false)
                {
                    $res[0]['Description'] = str_ireplace($keyword, $value, $res[0]['Description']);
                }
            }
        }
        return $res;
    }
    public function check_user_email($data=NULL)

    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $data['email']);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    public function get_site_setting()

    {
        $this->db->select('*');
        $this->db->from('settings');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getFormBuillderDataByEvent($event_id=NULL)

    {
        $this->db->select('*');
        $this->db->from('signup_form');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function signup($email=NULL, $firstname=NULL, $last_name=NULL, $password=NULL, $country_id=NULL, $title=NULL, $company_name=NULL, $event_id=NULL, $org_id=NULL, $device=NULL)

    {
        $where['Email'] = $email;
        $data['Firstname'] = $firstname;
        $data['Lastname'] = $last_name;
        $data['Email'] = $email;
        if ($event_id == '447')
        {
            $this->load->library('RC4');
            $rc4_obj = new RC4();
            $data['Password'] = $rc4_obj->encrypt($password);
        }
        else
        {
            $data['Password'] = ($password != '') ? md5($password) : '';
        }
        $data['Country'] = $country_id;
        $data['Organisor_id '] = $org_id;
        $data['Active'] = '1';
        $data['device'] = $device;
        $data['version_code_id'] = $this->input->post('version_code_id');
        if ($title != '') $data['Title'] = $title;
        if ($company_name != '') $data['Company_name'] = $company_name;
        $data['token'] = sha1($email . $password . $event_id);
        $user = $this->db->select('*')->from('user')->where($where)->get()->row_array();
        if (!empty($user))
        {
            return $user['Id'];
        }
        $this->db->insert("user", $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    }
    public function update_token($user_id=NULL, $data=NULL)

    {
        $this->db->where("Id", $user_id);
        $this->db->update("user", $data);
    }
    public function getOrganizerByEvent($event_id=NULL)

    {
        $this->db->select('Organisor_id');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $id = $res[0]['Organisor_id'];
        return $id;
    }
    public function setupUserWithEvent($user_id=NULL, $event_id=NULL, $org_id=NULL, $role = 4)

    {
        $data['Event_id'] = $event_id;
        $data['User_id'] = $user_id;
        $data['Organisor_id'] = $org_id;
        $data['Role_id'] = $role;
        $count = $this->db->select('*')->from('relation_event_user')->where($data)->get()->num_rows();
        if ($count == 0) $this->db->insert("relation_event_user", $data);
        $view = $this->db->select('view_id')->from('user_views')->where('event_id', $event_id)->where('view_type', '1')->get()->row_array();
        $event_data['Event_id'] = $event_id;
        $event_data['Attendee_id'] = $user_id;
        $event_data['views_id'] = $view['view_id'];
        $event_attendee = $this->db->where($event_data)->get('event_attendee')->row_array();
        if (empty($event_attendee))
        {
            $this->db->insert("event_attendee", $event_data);
        }
    }
    public function addFormBuilderData($user_id=NULL, $form_data=NULL)

    {
        $data['user_id'] = $user_id;
        $data['json_data'] = $form_data;
        $this->db->insert("signup_form_data", $data);
    }
    /* public function updateGCMID($user_id,$gcm_id)
    {
    $data['gcm_id']=$gcm_id;
    $this->db->select('u.Id');
    $this->db->from('user u');
    $this->db->where('u.gcm_id',$gcm_id);
    $query = $this->db->get();
    $res = $query->result_array();
    $ids = array_map(function ($ar) { return $ar['Id']; }, $res);
    $str=implode(',', $ids);
    if(count($res)>0)
    {
    $sql="update user set gcm_id='' WHERE Id IN ($str);";
    $query = $this->db->query($sql);
    }
    $this->db->where("Id",$user_id);
    $this->db->update("user",$data);
    $user_details=$this->getUserDetailsId($user_id);
    return $user_details;
    }*/
    public function updateGCMID($user_id=NULL, $gcm_id=NULL, $device=NULL, $event_id=NULL, $is_aitl = '0')

    {
        $user = $this->db->select('*')->from('user')->where('Id', $user_id)->get()->row_array();
        $user_details = $this->getUserDetailsId($user_id,$event_id);
        if ($user['gcm_id'] != $gcm_id)
        {
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'updateGCMID';
                $extra['message_id'] = $user['Id'];
                if ($user['version_code_id'] == '')
                {
                    if ($user['device'] == 'Iphone')
                    {
                        $extra['title'] = "";
                        $extra['event'] = "";
                        include ('application/libraries/FcmV2.php');

                        $obj = new Fcm();
                        $result = $obj->send(1, $user['gcm_id'], "You have successfully logged out", $extra, $user['device'], $event_id);
                    }
                    else
                    {
                        include ('application/libraries/NativeGcm.php');

                        $obj = new Gcm($event_id);
                        $message['title'] = "Test";
                        $message['message'] = "test";
                        $message['vibrate'] = 1;
                        $message['sound'] = 0;
                        $result = $obj->send_notification($user['gcm_id'], $message, $extra, $user['device']);
                    }
                }
                else
                {
                    $extra['title'] = '';
                    $extra['message'] = 'You are successfully loogged out.';
                    $extra['event'] = '';
                    include 'application/libraries/Gcmr.php';

                    $obj = new Gcmr($event_id);
                    $msg['title'] = "";
                    $msg['message'] = "You are successfully loogged out.";
                    $result[] = $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
        }
        $data['gcm_id'] = $gcm_id;
        $data['device'] = $device;
        $data['is_aitl'] = $is_aitl;
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->where('u.gcm_id', $gcm_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $ids = array_map(function ($ar)
        {
            return $ar['Id'];
        }
        , $res);
        $str = implode(',', $ids);
        if (count($res) > 0)
        {
            $sql = "update user set gcm_id='' WHERE Id IN ($str);";
            $query = $this->db->query($sql);
        }
        $this->db->where("Id", $user_id);
        $this->db->update("user", $data);
        $user_details = $this->getUserDetailsId($user_id,$event_id);
        return $user_details;
    }
    public function linkedInSignup($email=NULL, $facebook_id=NULL, $event_id=NULL, $firstname=NULL, $last_nam=NULLe, $img=NULL, $device=NULL, $company_name=NULL, $title=NULL)

    {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu', 'reu.User_id = tu.Id', 'left');
        $this->db->where("reu.Event_id", $event_id);
        $this->db->where("reu.Role_id", 3);
        $query = $this->db->get();
        $res = $query->result_array();
        $accname = $res[0]['acc_name'];
        $this->db->select('Id,Organisor_id');
        $this->db->from('event');
        $this->db->where('Id', $event_id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $eventid = $res1[0]['Id'];
        $Organisor_id = $res1[0]['Organisor_id'];
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Email', $email);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        if (count($res1) == 0)
        {
            $data['Email'] = $email;
            $data['Firstname'] = $firstname;
            $data['Lastname'] = $last_name;
            $data['acc_name'] = $accname;
            $data['Logo'] = $img;
            $data['Company_name'] = $company_name;
            $data['device'] = $device;
            $data['Title'] = $title;
            $data['attendee_flag'] = '0';
            $data['token'] = sha1($email . $password . $event_id);
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $auto_password = substr(str_shuffle($chars) , 0, 6);
            $data['Password'] = md5($auto_password);
            $data['Active'] = '1';
            $data['Organisor_id'] = $Organisor_id;
            $this->db->insert("user", $data);
            $user_id = $this->db->insert_id();
            $relation_event_user['Event_id'] = $eventid;
            $relation_event_user['User_id'] = $user_id;
            $relation_event_user['Role_id'] = 4;
            $relation_event_user['Organisor_id'] = $Organisor_id;
            // send mail to first time facebook user login
            $this->load->library('email');
            /*$config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "";
            $config['smtp_pass'] = "";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "text/html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);*/
            $this->email->from('info@allintheloop.com', 'AllInTheLoop');
            $this->email->subject('AllInTheLoop Auto-generated Password');
            $this->email->to($email);
            $this->email->set_mailtype("html");
            $message = "Hello " . $firstname . " " . $last_name . "," . "<br />" . "Your Auto-generated AllInTheLoop password is " . $auto_password;
            $this->email->message($message);
            $this->email->send();
            $count = $this->db->select('*')->from('relation_event_user')->where($relation_event_user)->get()->num_rows();
            if ($count == 0) $this->db->insert("relation_event_user  ", $relation_event_user);
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid');
            $this->db->from('user');
            $this->db->where('user.Id', $user_id);
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id', 'left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->limit(1);
            $query1 = $this->db->get();
            $result_user = $query1->row_array();
            array_walk($result_user, function (&$item)
            {
                $item = strval($item);
            });
            // return $result_user;
            $return['status'] = "1";
        }
        else
        {
            $up['Logo'] = $img;
            $this->db->where('Email', $email);
            $this->db->update('user', $up);
            $user_id = $res1[0]['Id'];
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id', $eventid);
            $this->db->where('User_id', $user_id);
            $this->db->where('Organisor_id', $Organisor_id);
            $qu = $this->db->get();
            $res = $qu->result_array();
            if (count($res) == 0)
            {
                $rel['Event_id'] = $eventid;
                $rel['User_id'] = $user_id;
                $rel['Organisor_id'] = $Organisor_id;
                $rel['Role_id'] = 4;
                $this->db->insert('relation_event_user', $rel);
            }
            $this->db->select('*,role.Name as Role_name,user.Id as Id,role.Id as Rid');
            $this->db->from('user');
            $this->db->where('user.Id', $user_id);
            $this->db->join('relation_event_user ru', 'ru.User_id = user.Id', 'left');
            $this->db->join('role', 'role.Id = ru.Role_id');
            $this->db->limit(1);
            $query1 = $this->db->get();
            $result_user = $query1->row_array();
            $return['status'] = "0";
        }
        $return['data'] = $this->getUserDetailsId($user_id,$eventid);
        return $return;
    }
    public function saveSocialLinks($user_id=NULL, $social=NULL)

    {
        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id', $user_id);
        $count = $this->db->get()->num_rows();
        if ($count)
        {
            $this->db->where('User_id', $user_id);
            $this->db->update('user_social_links', $social);
        }
        else
        {
            $social['user_id'] = $user_id;
            $this->db->insert("user_social_links", $social);
        }
    }
    public function getEventId($user_id=NULL)

    {
        $data = $this->db->select('Event_id')->from('relation_event_user')->where('User_id', $user_id)->get()->row_array();
        return $data['Event_id'];
    }
    public function change_pas($email=NULL, $password=NULL)

    {
        $this->db->where('Email', $email);
        $this->db->update('user', ['password' => $password]);
    }
    public function getUserRole($email=NULL, $event_id=NULL)

    {
        $this->db->select('ru.Role_id')->from('relation_event_user ru');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->where('ru.Event_id', $event_id);
        $this->db->where('u.Email', $email);
        $data = $this->db->get()->row_array();
        return $data['Role_id'];
    }
    public function getEventName($event_id=NULL)

    {
        $data = $this->db->select('Event_name')->from('event')->where('Id', $event_id)->get()->row_array();
        return ($data['Event_name']) ? $data['Event_name'] : '';
    }
    public function checkAuthorizedLogin($event_id=NULL, $email=NULL)

    {
        $event = $this->db->select('authorized_email')->from('event')->where('Id', $event_id)->get()->row_array();
        $user = $this->db->select('*')->from('authorized_user')->where('Event_id', $event_id)->where('Email', $email)->get()->row_array();
        $data = $this->getUserByEmail($email, $event_id);
        if (!empty($data))
        {
            // check lead representative disable account
            $data_r = $this->db->select('*')->from('user_representatives')->where('event_id', $event_id)->where('rep_id', $data['Id'])->get()->row_array();
            if ($data_r && $data_r['status'] == '0')
            {
                $return['status'] = false;
                $return['register'] = '0';
                $return['rep_user'] = '0';
                $return['data'] = new stdclass;
            }
            else
            {
                $user_data['token'] = sha1($email . $event_id);
                $user_data['version_code_id'] = $this->input->post('version_code_id');
                $user_data['Login_date'] = date('Y-m-d H:i:s');
                $this->db->where('Id', $data['Id']);
                $this->db->update('user', $user_data);
                $data['token'] = $user_data['token'];
                $return['status'] = true;
                $return['register'] = '0';
                $return['data'] = $data;
                if (count($user))
                {
                    $this->db->where('authorized_id', $user['authorized_id']);
                    $this->db->delete('authorized_user');
                }
            }
        }
        elseif (count($user))
        {
            $org_id = $this->getOrganizerByEvent($event_id);
            $user_id = $this->signup($email, $user['firstname'], $user['lastname'], "", "", $user['title'], $user['company_name'], $event_id, $org_id, "");
            $this->setupUserWithEvent($user_id, $event_id, $org_id, ($user['email_type'] == '0') ? 4 : 6);
            if ($user['agenda_id'] != NULL && $user['email_type'] == "0")
            {
                $agenda_relation['attendee_id'] = $user_id;
                $agenda_relation['agenda_category_id'] = $user['agenda_id'];
                $agenda_relation['event_id'] = $event_id;
                $this->assignAgenda($agenda_relation);
            }
            elseif ($user['agenda_id'] == NULL && $user['email_type'] == "0")
            {
                $agenda_categories = $this->db->select('*')->from('agenda_categories')->where('event_id', $event_id)->get()->result_array();
                if (count($agenda_categories) == 1)
                {
                    $agenda_relation['attendee_id'] = $user_id;
                    $agenda_relation['agenda_category_id'] = $agenda_categories[0]['Id'];
                    $agenda_relation['event_id'] = $event_id;
                    $this->assignAgenda($agenda_relation);
                }
                else
                {
                    $agenda_categories = $this->db->select('*')->from('agenda_categories')->where('event_id', $event_id)->where('categorie_type', '1')->get()->row_array();
                    if (count($agenda_categories) > 0)
                    {
                        $agenda_relation['attendee_id'] = $user_id;
                        $agenda_relation['agenda_category_id'] = $agenda_categories['Id'];
                        $agenda_relation['event_id'] = $event_id;
                        $this->assignAgenda($agenda_relation);
                    }
                }
            }
            if ($user['check_in'] == "1")
            {
                $event_attendee['check_in_attendee'] = '1';
                if ($user['extra_column'] != NULL) $event_attendee['extra_column'] = $user['extra_column'];
                // test
                $where['Event_id'] = $event_id;
                $where['Attendee_id'] = $user_id;
                $this->db->where($where);
                $this->db->update('event_attendee', $event_attendee);
            }
            if ($user['views_id'] != NULL)
            {
                $event_attendee['views_id'] = $user['views_id'];
                $where['Event_id'] = $event_id;
                $where['Attendee_id'] = $user_id;
                $this->db->where($where);
                $this->db->update('event_attendee', $event_attendee);
            }
            $data = $this->getUserByEmail($email, $event_id);
            $this->db->where('authorized_id', $user['Id']);
            $this->db->delete('authorized_user');
            $return['status'] = true;
            $return['register'] = '1';
            $return['data'] = $data;
        }
        elseif ($event['authorized_email'] == "1")
        {
            $return['status'] = false;
            $return['register'] = '0';
            $return['data'] = new stdclass;
        }
        else
        {
            $org_id = $this->getOrganizerByEvent($event_id);
            $user_id = $this->signup($email, "", "", "", "", "", "", $event_id, $org_id, "");
            $this->setupUserWithEvent($user_id, $event_id, $org_id);
            $agenda_categories = $this->db->select('*')->from('agenda_categories')->where('event_id', $event_id)->get()->result_array();
            if (count($agenda_categories) == 1)
            {
                $agenda_relation['attendee_id'] = $user_id;
                $agenda_relation['agenda_category_id'] = $agenda_categories[0]['Id'];
                $agenda_relation['event_id'] = $event_id;
                $this->assignAgenda($agenda_relation);
            }
            else
            {
                $agenda_categories = $this->db->select('*')->from('agenda_categories')->where('event_id', $event_id)->where('categorie_type', '1')->get()->row_array();
                if (count($agenda_categories) > 0)
                {
                    $agenda_relation['attendee_id'] = $user_id;
                    $agenda_relation['agenda_category_id'] = $agenda_categories['Id'];
                    $agenda_relation['event_id'] = $event_id;
                    $this->assignAgenda($agenda_relation);
                }
            }
            $data = $this->getUserByEmail($email, $event_id);
            $return['status'] = true;
            $return['register'] = '1';
            $return['data'] = $data;
        }
        return $return;
    }
    public function assignAgenda($agenda_relation=NULL)

    {
        $this->db->insert('attendee_agenda_relation', $agenda_relation);
    }
    public function getUserByEmail($email=NULL, $event_id=NULL)

    {
        $this->db->select('u.*,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id', FALSE);
        $this->db->from('user u');
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('u.Email', $email);
        $this->db->where('ru.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id', $res[0]['Id']);
        $links = $this->db->get();
        $links = $links->row_array();
        if (!empty($res))
        {
            $res[0]['linkedin_url'] = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
            $res[0]['facebook_url'] = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
            $res[0]['twitter_url'] = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
            $res[0]['Website_url'] = ($links['Website_url']) ? $links['Website_url'] : '';
            $res = $res[0];
            array_walk($res, function (&$item)
            {
                $item = strval($item);
            });
        }
        return $res;
    }
    public function getEventData($event_id=NULL, $user_id = 0)

    {
        $this->db->select('e.*,CASE WHEN e.Description IS NULL THEN "" ELSE e.Description END as Description,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.Logo_images IS NULL THEN "" ELSE e.Logo_images END as Logo_images,CASE WHEN e.Background_img IS NULL THEN "" ELSE e.Background_img END as Background_img,CASE WHEN e.Background_color IS NULL THEN "#FFFFFF" ELSE e.Background_color END as Background_color,CASE WHEN e.Top_background_color IS NULL THEN "#FFFFFF" ELSE e.Top_background_color END as Top_background_color,CASE WHEN e.Top_text_color IS NULL THEN "#FFFFFF" ELSE e.Top_text_color END as Top_text_color,CASE WHEN e.Icon_text_color IS NULL THEN "#FFFFFF" ELSE e.Icon_text_color END as Icon_text_color,CASE WHEN e.Footer_background_color IS NULL THEN "#FFFFFF" ELSE e.Footer_background_color END as Footer_background_color,CASE WHEN e.fun_logo_images IS NULL THEN "" ELSE e.fun_logo_images END as fun_logo_images,CASE WHEN e.fun_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_background_color END as fun_background_color,CASE WHEN e.fun_top_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_background_color END as fun_top_background_color,CASE WHEN e.fun_top_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_text_color END as fun_top_text_color,CASE WHEN e.fun_footer_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_footer_background_color END as fun_footer_background_color,CASE WHEN e.fun_block_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_background_color END as fun_block_background_color,CASE WHEN e.fun_block_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_text_color END as fun_block_text_color,CASE WHEN e.theme_color IS NULL THEN "#FFFFFF" ELSE e.theme_color END as theme_color,CASE WHEN e.Contact_us IS NULL THEN "" ELSE e.Contact_us END as Contact_us,CASE WHEN e.checkbox_values IS NULL THEN "" ELSE e.checkbox_values END as checkbox_values,CASE WHEN e.fun_header_status IS NULL THEN "" ELSE e.fun_header_status END as fun_header_status,CASE WHEN e.secure_key IS NULL THEN "" ELSE e.secure_key END as secure_key,ru.Role_id', FALSE);
        $this->db->from('event e');
        $this->db->where('e.Id', $event_id);
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        if ($user_id != 0)
        {
            $this->db->where('ru.User_id', $user_id);
        }
        $this->db->group_by("e.Id");
        $query = $this->db->get();
        $res = $query->result_array();
        if ($res[0]['Role_id'] == '4')
        {
            $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user_id)->get()->row_array();
            $extra = json_decode($extra['extra_column'], true);
            foreach($extra as $key => $value)
            {
                $keyword = "{" . str_replace(' ', '', $key) . "}";
                if (stripos(strip_tags($res[0]['Description'], $keyword)) !== false)
                {
                    $res[0]['Description'] = str_ireplace($keyword, $value, $res[0]['Description']);
                }
            }
        }
        return $res;
    }
    public function check_event_with_id1($event_id=NULL, $user_id = 0)

    {
        $this->db->select('
                    CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as Id,
                    CASE WHEN e.Event_name IS NULL THEN "" ELSE e.Event_name END as Event_name,
                    CASE WHEN e.Start_date IS NULL THEN "" ELSE e.Start_date END as Start_date,
                    CASE WHEN e.End_date IS NULL THEN "" ELSE e.End_date END as End_date,
                    CASE WHEN e.Top_background_color IS NULL THEN "#FFFFFF" ELSE e.Top_background_color END as Top_background_color,
                    CASE WHEN e.Top_text_color IS NULL THEN "#FFFFFF" ELSE e.Top_text_color END as Top_text_color,
                    CASE WHEN e.Icon_text_color IS NULL THEN "#FFFFFF" ELSE e.Icon_text_color END as Icon_text_color,
                    CASE WHEN e.menu_background_color IS NULL THEN "" ELSE e.menu_background_color END as menu_background_color,
                    CASE WHEN e.menu_text_color IS NULL THEN "" ELSE e.menu_text_color END as menu_text_color,
                    CASE WHEN e.menu_hover_background_color IS NULL THEN "" ELSE e.menu_hover_background_color END as menu_hover_background_color,
                    CASE WHEN e.Footer_background_color IS NULL THEN "#FFFFFF" ELSE e.Footer_background_color END as Footer_background_color,
                    CASE WHEN e.fun_header_status IS NULL THEN "" ELSE e.fun_header_status END as fun_header_status,
                    CASE WHEN e.fun_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_background_color END as fun_background_color,
                    CASE WHEN e.Background_color IS NULL THEN "#FFFFFF" ELSE e.Background_color END as Background_color,
                    CASE WHEN e.fun_top_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_background_color END as fun_top_background_color,
                    CASE WHEN e.fun_top_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_top_text_color END as fun_top_text_color,
                    CASE WHEN e.fun_footer_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_footer_background_color END as fun_footer_background_color,
                    CASE WHEN e.fun_block_background_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_background_color END as fun_block_background_color,
                    CASE WHEN e.fun_block_text_color IS NULL THEN "#FFFFFF" ELSE e.fun_block_text_color END as fun_block_text_color,
                    CASE WHEN e.allow_msg_keypeople_to_attendee IS NULL THEN "" ELSE e.allow_msg_keypeople_to_attendee END as allow_msg_keypeople_to_attendee,
                    CASE WHEN e.hide_request_meeting IS NULL THEN "" ELSE e.hide_request_meeting END as hide_request_meeting,
                    CASE WHEN e.theme_color IS NULL THEN "#FFFFFF" ELSE e.theme_color END as theme_color,
                    CASE WHEN e.fun_logo_images IS NULL THEN "" ELSE e.fun_logo_images END as fun_logo_images,
                    CASE WHEN e.show_agenda_place_left_column IS NULL THEN "" ELSE e.show_agenda_place_left_column END as show_agenda_place_left_column,
                    CASE WHEN e.show_agenda_speaker_column IS NULL THEN "" ELSE e.show_agenda_speaker_column END as show_agenda_speaker_column,
                    CASE WHEN e.show_agenda_location_column IS NULL THEN "" ELSE e.show_agenda_location_column END as show_agenda_location_column,
                    CASE WHEN e.Description IS NULL THEN "" ELSE e.Description END as Description,
                    CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,
                    CASE WHEN e.Background_img IS NULL THEN "" ELSE e.Background_img END as Background_img1,
                    CASE WHEN e.Logo_images IS NULL THEN "" ELSE e.Logo_images END as Logo_images', FALSE);
        $this->db->from('event e');
        $this->db->where('e.Id', $event_id);
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        if ($user_id != 0)
        {
            $this->db->where('ru.User_id', $user_id);
        }
        $this->db->group_by("e.Id");
        $query = $this->db->get();
        $res = $query->result_array();
        if ($res[0]['Role_id'] == '4')
        {
            $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user_id)->get()->row_array();
            $extra = json_decode($extra['extra_column'], true);
            foreach($extra as $key => $value)
            {
                $keyword = "{" . str_replace(' ', '', $key) . "}";
                if (stripos(strip_tags($res[0]['Description'], $keyword)) !== false)
                {
                    $res[0]['Description'] = str_ireplace($keyword, $value, $res[0]['Description']);
                }
            }
        }
        return $res;
    }
    public function get_user_role($event_id=NULL, $user_id=NULL)

    {
        $this->db->join('relation_event_user reu', 'reu.Role_id = r.Id');
        $this->db->where('reu.User_id', $user_id);
        $this->db->where('reu.Event_id', $event_id);
        return $this->db->get('role r')->row_array();
    }
    public function banner_images_and_coords($event_id=NULL, $user_id=NULL)

    {
        $this->db->select('*');
        $this->db->from('event_banner_image');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $result = $query->result_array();
        $res['images'] = $result;
        $user_role_info = $this->get_user_role($event_id, $user_id);
        $event_settings = $this->db->where('event_id', $event_id)->get('event_settings')->row_array();
        foreach($res['images'] as $key => $value)
        {
            $this->load->model('Event_model');
            $allow_show_all_agenda = $this->db->select('allow_show_all_agenda')->where('Id', $event_id)->get('event')->row_array();
            $allow_show_all_agenda = $allow_show_all_agenda['allow_show_all_agenda'];
            if ($allow_show_all_agenda == '0')
            {
                $agenda_ids = $this->Event_model->get_agenda_category_id_by_user($event_id, $user_id);
                $cid = array_column_1($agenda_ids, 'agenda_category_id');
                if (empty($cid))
                {
                    $primary_agenda = $this->Event_model->get_primary_category_id($eventid);
                    $cid = array_column_1($primary_agenda, 'agenda_category_id');
                }
            }
            list($width, $height) = getimagesize(str_replace('https://', 'http://', base_url()) . "assets/user_files/" . $value['Image']);
            $res['images'][$key]['width'] = ($width) ? $width : '';
            $res['images'][$key]['height'] = ($height) ? $height : '';
            $this->db->select('CASE WHEN eb.id IS NULL THEN "" ELSE eb.id END as id,
                               CASE WHEN eb.banner_id IS NULL THEN "" ELSE eb.banner_id END as banner_id,
                               CASE WHEN eb.coords IS NULL THEN "" ELSE eb.coords END as coords,
                               CASE WHEN eb.redirect_url IS NULL THEN "" ELSE eb.redirect_url END as redirect_url,
                               CASE WHEN eb.menuid IS NULL THEN "" ELSE eb.menuid END as menuid,
                               CASE WHEN eb.cmsid IS NULL THEN "" ELSE eb.cmsid END as cmsid,
                               CASE WHEN eb.agenda_id IS NULL THEN "" ELSE eb.agenda_id END as agenda_id,
                               CASE WHEN eb.group_id IS NULL THEN "" ELSE eb.group_id END as group_id,
                               CASE WHEN eb.exhi_id IS NULL THEN "" ELSE eb.exhi_id END as exhi_id,
                               CASE WHEN eb.super_group_id IS NULL THEN "" ELSE eb.super_group_id END as super_group_id,
                               CASE WHEN eb.exhi_sub_cat_id IS NULL THEN "" ELSE eb.exhi_sub_cat_id END as exhi_sub_cat_id,
                                CASE WHEN eb.all_exhi_sub_cat IS NULL THEN "" ELSE eb.all_exhi_sub_cat END as all_exhi_sub_cat,
                               CASE WHEN em.is_force_login IS NULL THEN "0" ELSE em.is_force_login END as is_force_login,
                               eb.myagenda', FALSE);
            $this->db->from('event_banner_coord eb');
            $this->db->join('event_menu em', 'eb.menuid=em.menu_id and em.event_id=' . $event_id . ' and eb.menuid IS NOT NULL or eb.cmsid=em.cms_id and em.event_id=' . $event_id . ' and eb.cmsid IS NOT NULL or eb.menuid IS NULL and eb.cmsid IS NULL', 'left', false);
            $this->db->join('event_banner_image ebi', 'ebi.id = eb.banner_id', 'left', false);
            $this->db->where('eb.banner_id', $value['id']);
            $this->db->where('ebi.event_id', $event_id);
            if ($user_role_info['role_type'] != '1') $this->db->where('(eb.menuid != "53" OR eb.menuid IS NULL)');
            if ($user_role_info['Role_id'] == '4' && ($event_settings['attendee_scan_swap_contact'] == '0' || empty($event_settings))) $this->db->where('(eb.menuid != "57" OR eb.menuid IS NULL)');
            elseif ($user_role_info['Role_id'] == '6' && ($event_settings['exibitor_scan_attendee_reward'] == '0' || empty($event_settings))) $this->db->where('(eb.menuid != "57" OR eb.menuid IS NULL)');
            $this->db->group_by('eb.id');
            $query = $this->db->get();
            $result = $query->result_array();
            foreach($result as $k => $v)
            {
                $result[$k]['is_contains_data'] = '0';
                if ($allow_show_all_agenda == '0')
                {
                    if (!empty($v['agenda_id']) && !in_array($v['agenda_id'], $cid))
                    {
                        unset($result[$k]);
                    }
                    /*if(!empty($v['group_id']))
                    {
                    $m_group_relation = $this->db->where('menu_id','1')->where('group_id',$v['group_id'])->get('modules_group_relation')->result_array();
                    if(!empty($m_group_relation))
                    {
                    foreach ($m_group_relation as $k1 => $v1)
                    {
                    if(!in_array($v1['module_id'],$cid))
                    {
                    unset($result[$k]);
                    }
                    }
                    }
                    }*/
                }
                elseif (!empty($v['agenda_id']))
                {
                    $result[$k]['menuid'] = '1';
                }
                if (!empty($v['group_id']))
                {
                    $m_group_relation = $this->db->where('module_group_id', $v['group_id'])->get('modules_group')->row_array();
                    $result[$k]['menuid'] = $m_group_relation['menu_id'];
                    $data_count = $this->db->select('*')->from('modules_group_relation')->where('group_id', $m_group_relation['module_group_id'])->get()->num_rows();
                    $result[$k]['is_contains_data'] = ($data_count) ? '1' : '0';
                }
                if (!empty($v['exhi_id']))
                {
                    $result[$k]['menuid'] = '3';
                }
                if (!empty($v['super_group_id']))
                {
                    $result[$k]['menuid'] = '21';
                }
                if (!empty($v['exhi_sub_cat_id']))
                {
                    $exhibitor_category = $this->db->where('id', $v['exhi_sub_cat_id'])->where('event_id', $event_id)->get('exhibitor_category')->row_array();
                    $result[$k]['menuid'] = '3';
                    $result[$k]['keyword'] = $exhibitor_category['categorie_keywords'];
                }
                elseif (!empty($result[$k]))
                {
                    $result[$k]['keyword'] = "";
                }
                if (!empty($v['all_exhi_sub_cat']))
                {
                    $result[$k]['menuid'] = '3';
                }
            }
            $res['images'][$key]['details'] = array_values($result);
        }
        // j($res);
        return $res;
    }
    public function check_custom_view($eventid=NULL, $user_id=NULL)

    {
        $this->db->select('*')->from('event_attendee ea');
        $this->db->join('user_views uv', 'uv.view_id=ea.views_id', 'right');
        $this->db->where('ea.Event_id', $eventid);
        $this->db->where('ea.Attendee_id', $user_id);
        $vqu = $this->db->get();
        $vres = $vqu->result_array();
        if (!empty($vres[0]['view_modules']))
        {
            $activemodules = explode(",", $vres[0]['view_modules']);
        }
        if ($vres[0]['view_type'] == '1' && empty($vres[0]['view_modules']))
        {
            $activemodules = array(
                '21'
            );
        }
        $res = $this->db->select('checkbox_values')->where('Id', $eventid)->get('event')->row_array();
        $res = explode(',', $res['checkbox_values']);
        if (!in_array('21', $res))
        {
            $activemodules = array_diff($activemodules, ['21']);
        }
        return $activemodules;
    }
    public function add_server_data_50($data=NULL)

    {
        $this->db->insert('records_load_50', $data);
        return true;
    }
    public function get_endtime_of_last_rec_50($event_id=NULL)

    {
        $this->db->select('end_time');
        if (!empty($event_id))
        {
            $this->db->where('event_id', $event_id);
        }
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $res = $this->db->get('records_load_50')->row_array();
        return $res['end_time'];
    }
    public function add_server_data_100($data=NULL)

    {
        $this->db->insert('records_load_100', $data);
        return true;
    }
    public function get_endtime_of_last_rec_100()

    {
        $this->db->select('end_time');
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $res = $this->db->get('records_load_100')->row_array();
        return $res['end_time'];
    }
    public function add_server_data_200($data=NULL)

    {
        $this->db->insert('records_load_200', $data);
        return true;
    }
    public function get_endtime_of_last_rec_200()

    {
        $this->db->select('end_time');
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $res = $this->db->get('records_load_200')->row_array();
        return $res['end_time'];
    }
    public function add_server_data_500($data=NULL)

    {
        $this->db->insert('records_load_500', $data);
        return true;
    }
    public function get_endtime_of_last_rec_500()

    {
        $this->db->select('end_time');
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $res = $this->db->get('records_load_500')->row_array();
        return $res['end_time'];
    }
    public function get_exhi_info($stand_number=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('CASE WHEN Heading IS NULL THEN "" ELSE Heading END as Heading,
                           CASE WHEN Id IS NULL THEN "" ELSE Id END as Id,
                           CASE WHEN user_id IS NULL THEN "" ELSE user_id END as user_id,
                           CASE WHEN company_logo IS NULL THEN "" ELSE company_logo END as company_logo,Description', false);
        // $this->db->where('stand_number',$stand_number);
        $this->db->where("FIND_IN_SET('" . $stand_number . "',stand_number) != ", 0);
        $res = $this->db->get('exibitor')->row_array();
        return $res;
    }
    public function check_active_view($eventid=NULL, $user_id=NULL)

    {
        $this->db->select('*')->from('event_attendee ea');
        $this->db->join('user_views uv', 'uv.view_id=ea.views_id', 'right');
        $this->db->where('ea.Event_id', $eventid);
        $this->db->where('ea.Attendee_id', $user_id);
        $vqu = $this->db->get();
        $vres = $vqu->result_array();
        if (!empty($vres[0]['view_modules']))
        {
            $activemodules = explode(",", $vres[0]['view_modules']);
        }
        if ($vres[0]['view_type'] == '1' && empty($vres[0]['view_modules']))
        {
            $activemodules = array(
                '21'
            );
        }
        $res = $this->db->select('checkbox_values')->where('Id', $eventid)->get('event')->row_array();
        $res = explode(',', $res['checkbox_values']);
        if (!in_array('21', $res))
        {
            $activemodules = array_diff($activemodules, ['21']);
        }
        return $activemodules;
    }
    public function delete_user($user_id=NULL)

    {
        $this->db->where('Id', $user_id);
        $this->db->delete('user');
    }
    public function get_default_lang_label($eid=NULL, $lang_id = NULL)

    {
        if (empty($lang_id))
        {
            $this->db->select('lang_id')->from('event_language');
            $this->db->where('lang_default', '1');
            $this->db->where('event_id', $eid);
            $defaultlang = $this->db->get()->row_array();
            $lang_id = $defaultlang['lang_id'];
        }
        $this->db->protect_identifiers = false;
        $this->db->select("(CASE WHEN el.lang_id IS NULL THEN '' ELSE el.lang_id END) as lang_id,
                           (CASE WHEN el.lang_name IS NULL THEN 'English' ELSE el.lang_name END) as lang_name,
                           (CASE WHEN el.lang_icon IS NULL THEN 'United States.png' ELSE el.lang_icon END) as lang_icon,
                           (CASE WHEN el.lang_code IS NULL THEN 'EN' ELSE el.lang_code END) as lang_code,
                           concat(IFNULL(concat(dl.menu_id,'','__'),
                           concat(dl.custom_feature_name,'','__')), dl.label_key) as arr_keys,
                           CASE WHEN ell.label_values IS NULL THEN dl.label_value ELSE ell.label_values END AS label_vales", false)->from('default_label dl');
        $this->db->join('event_language_label ell', "ell.label_key=dl.default_label_id AND ell.lang_id='" . $lang_id . "' AND ell.event_id=" . $eid, 'left');
        $this->db->join('event_language el', 'el.lang_id="' . $lang_id . '" AND el.event_id=' . $eid, 'left');
        $res = $this->db->get()->result_array();
        $finel_array = array_combine(array_column_1($res, 'arr_keys') , array_column_1($res, 'label_vales'));
        $finel_array['lang_id'] = $res[0]['lang_id'];
        $finel_array['lang_name'] = $res[0]['lang_name'];
        $finel_array['lang_icon'] = $res[0]['lang_icon'];
        $finel_array['lang_code'] = $res[0]['lang_code'];
        $event_settings = $this->get_event_settings($eid);
        if (!empty($event_settings) && $event_settings['show_stand_no'] == '0')
        {
            $finel_array['3__stand'] = '';
        }
        else if (!empty($event_settings) && $event_settings['show_stand_no'] == '1')
        {
            $finel_array['3__stand'] = $event_settings['stand_no_label'];
        }
        return $finel_array;
    }
    public function get_event_settings($event_id=NULL)

    {
        return $this->db->where('event_id', $event_id)->get('event_settings')->row_array();
    }
    public function get_default_lang_label_for_signup($eid=NULL, $lang_id = NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select("concat(ell.custom_modules_name,'__', ell.custom_label_key) AS arr_keys,
                           ell.label_values AS label_vales", false)->from('event_language_label ell');
        $this->db->where('ell.custom_modules_name', 'sign_up_process_form');
        $this->db->where('ell.lang_id', $lang_id);
        $this->db->where('ell.event_id', $eid);
        $res = $this->db->get()->result_array();
        $finel_array = array_combine(array_column_1($res, 'arr_keys') , array_column_1($res, 'label_vales'));
        return $finel_array;
    }
    public function get_event_lang_list($event_id=NULL)

    {
        $res = $this->db->select('lang_id,lang_name,lang_icon,lang_default,lang_code')->where('event_id', $event_id)->get('event_language')->result_array();
        return $res;
    }
    public function getAdvertise($event_id=NULL)

    {
        $data = $this->db->select('*')->from('advertising')->where('Event_id', $event_id)->get()->result_array();
        $n['header'] = [];
        $n['footer'] = [];
        $n['show_all'] = '0';
        $n['show_sticky'] = '0';
        foreach($data as $key => $value)
        {
            $h = json_decode($value['H_images'], true);
            $f = json_decode($value['F_images'], true);
            $h_images['image'] = ($h) ? $h[0] : '';
            $h_images['url'] = ($value['Header_link']) ? $value['Header_link'] : '';
            $f_images['image'] = ($f) ? $f[0] : '';
            $f_images['url'] = ($value['Footer_link']) ? $value['Footer_link'] : '';
            $n['header'][] = $h_images;
            $n['footer'][] = $f_images;
            $n['show_all'] = $value['show_all'];
            $n['show_sticky'] = $value['show_sticky'];
        }
        return $n;
    }
    public function getUpdatedLogs($event_id=NULL)

    {
        return $this->db->select('*')->from('modules_update_log')->where('event_id', $event_id)->get()->result_array();
    }
    public function getLatestUpdatedDate($table=NULL, $column=NULL, $event_id=NULL)

    {
        if ($table == 'user')
        {
            $data = $this->db->select('u.updated_date')->from('user u')->join('exibitor e', 'e.user_id = u.Id')->where('e.Event_id', $event_id)->order_by('updated_date', 'DESC')->limit(1)->get()->row_array();
        }
        else
        {
            $data = $this->db->select('updated_date')->from($table)->where($column, $event_id)->order_by('updated_date', 'DESC')->limit(1)->get()->row_array();
        }
        return $data['updated_date'];
    }
    public function getFavouritedExhibitors($event_id=NULL, $user_id=NULL)

    {
        return $this->db->select('e.Id as exhibitor_page_id, e.user_id as exhibitor_user_id')->from('my_favorites mf')->join('exibitor e', 'e.Id = mf.module_id')->where('mf.event_id', $event_id)->where('mf.user_id', $user_id)->where('module_type', '3')->get()->result_array();
    }
    public function getTimeFormat($eid=NULL)

    {
        $this->db->select('format_time')->from('fundraising_setting');
        $this->db->where('Event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_all_users_agenda($user_id=NULL)

    {
        $this->db->where('user_id', $user_id);
        $res = $this->db->get('users_agenda')->row_array();
        if (!empty($res['agenda_id'])) $data['agenda_id'] = array_values(array_filter(explode(',', $res['agenda_id'])));
        else $data['agenda_id'] = [];
        if (!empty($res['check_in_agenda_id'])) $data['check_in_agenda_id'] = array_values(array_filter(explode(',', $res['check_in_agenda_id'])));
        else $data['check_in_agenda_id'] = [];
        if (!empty($res['pending_agenda_id'])) $data['pending_agenda_id'] = array_values(array_filter(explode(',', $res['pending_agenda_id'])));
        else $data['pending_agenda_id'] = [];
        return $data;
    }
    public function get_attendee_agenda($event_id=NULL, $user_id=NULL)

    {
        $this->db->select('group_concat(agenda_category_id) as category_id')->where('event_id', $event_id)->where('attendee_id', $user_id);
        $res = $this->db->get('attendee_agenda_relation')->row_array();
        $res = !empty($res['category_id']) ? explode(',', $res['category_id']) : [];
        return $res;
    }
    public function get_modules_relation($event_id=NULL, $menu_id=NULL)

    {
        $this->db->where('mg.event_id', $event_id);
        $this->db->where('mg.menu_id', $menu_id);
        $this->db->join('modules_group_relation mgr', 'mgr.group_id = mg.module_group_id');
        $res = $this->db->get('modules_group mg')->result_array();
        return array_column_1($res, 'module_id');
    }
    public function get_modules_group($event_id=NULL, $menu_id=NULL)

    {
        $this->db->select('*,CASE WHEN group_image IS NULL THEN "" ELSE group_image END as group_image,"0" as is_force_login', false);
        $this->db->where('event_id', $event_id);
        $this->db->where('menu_id', $menu_id);
        $res = $this->db->get('modules_group')->result_array();
        if ($menu_id == '21')
        {
            $this->db->where('sg.event_id', $event_id);
            $this->db->where('sg.menu_id', $menu_id);
            $this->db->join('super_group_relation sgr', 'sgr.super_group_id = sg.id');
            $cms_group = $this->db->get('modules_super_group sg')->result_array();
            $cms_group = array_column_1($cms_group, 'child_group_id');
            foreach($res as $key => $value)
            {
                if (in_array($value['module_group_id'], $cms_group))
                {
                    unset($res[$key]);
                }
            }
        }
        foreach($res as $key => $value)
        {
            $data_count = $this->db->select('*')->from('modules_group_relation')->where('group_id', $value['module_group_id'])->get()->num_rows();
            $res[$key]['is_contains_data'] = ($data_count) ? '1' : '0';
        }
        return array_values($res);
    }
    public function get_modules_super_group($event_id=NULL, $menu_id=NULL)

    {
        $this->db->select('*,CASE WHEN group_image IS NULL THEN "" ELSE group_image END as group_image,"0" as is_force_login', false);
        $this->db->where('event_id', $event_id);
        $this->db->where('menu_id', $menu_id);
        $res = $this->db->get('modules_super_group')->result_array();
        return $res;
    }
    public function get_exhi_info_arab($stand_number=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('CASE WHEN Heading IS NULL THEN "" ELSE Heading END as Heading,
                           CASE WHEN Id IS NULL THEN "" ELSE Id END as Id,
                           CASE WHEN user_id IS NULL THEN "" ELSE user_id END as user_id,
                           CASE WHEN company_logo IS NULL THEN "" ELSE company_logo END as company_logo,Description', false);
        // $this->db->where('stand_number',$stand_number);
        $this->db->where("FIND_IN_SET('" . $stand_number . "',stand_number) != ", 0);
        $this->db->where('Event_id', '634');
        $res = $this->db->get('exibitor')->row_array();
        return $res;
    }
    public function get_exhi_info_map($stand_number=NULL, $event_id=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
                           CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as Id,
                           CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as user_id,
                           CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,em.map_id', false);
        // $this->db->where('stand_number',$stand_number);
        $this->db->join('exhi_map_id em', 'em.exhi_id = e.Id', 'left');
        $this->db->where('FIND_IN_SET("' . $stand_number . '",e.stand_number) != ', 0);
        $this->db->where('e.Event_id', $event_id);
        $res = $this->db->get('exibitor e')->row_array();
        // lq();
        return $res;
    }
    public function check_token_with_event_test($token=NULL, $event_id=NULL, $type=NULL)

    {
        /*if($event_id == '634')
        {
        $data[] = true;
        $data[] = '123';
        return $data;
        }*/
        $token = ($token == NULL) ? '' : $token;
        $this->db->select('e.*,u.Id as user_id');
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->where('ru.Event_id', $event_id);
        /*if($type==1 || $type==2)
        {
        if($token=="")
        {
        $token="NULL";
        }
        $this->db->where('u.token', $token);
        }*/
        $this->db->where('u.token', $token);
        $query = $this->db->limit(1)->get();
        $res = $query->result_array();
        return $res;
    }
    public function CheckDomain()

    {
        $domain = $this->db->get('uhg_allowed_domain')->result_array();
        $domain = array_column_1($domain, 'domain');
        return $domain;
    }
    public function getEventsByOrg($org_id=NULL)

    {
        $this->db->select('Id');
        $this->db->where('Organisor_id', $org_id);
        $res = $this->db->get('event')->result_array();
        $res = array_column_1($res, 'Id');
        return $res;
    }
    public function signup_v2($email=NULL, $firstname=NULL, $last_name=NULL, $password=NULL, $country_id=NULL, $title=NULL, $company_name=NULL, $event_id=NULL, $org_id=NULL, $device=NULL)

    {
        $where['Email'] = $email;
        $data['Firstname'] = $firstname;
        $data['Lastname'] = $last_name;
        $data['Email'] = $email;
        if ($event_id == '447')
        {
            $this->load->library('RC4');
            $rc4_obj = new RC4();
            $data['Password'] = $rc4_obj->encrypt($password);
        }
        else
        {
            $data['Password'] = ($password != '') ? $password : '';
        }
        $data['Country'] = $country_id;
        $data['Organisor_id '] = $org_id;
        $data['Active'] = '1';
        $data['device'] = $device;
        if ($title != '') $data['Title'] = $title;
        if ($company_name != '') $data['Company_name'] = $company_name;
        $data['token'] = sha1($email . $password . $event_id . rand());
        $user = $this->db->select('*')->from('user')->where($where)->get()->row_array();
        if (!empty($user))
        {
            return $user['Id'];
        }
        $this->db->insert("user", $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    }
    public function check_app_login_v2($email=NULL, $password=NULL, $event_id=NULL)

    {
        $this->db->select('u.*,CASE WHEN u.Street IS NULL THEN "" ELSE u.Street END as Street,CASE WHEN u.Suburb IS NULL THEN "" ELSE u.Suburb END as Suburb,CASE WHEN u.State IS NULL THEN "" ELSE u.State END as State,CASE WHEN u.acc_name IS NULL THEN "" ELSE u.acc_name END as acc_name,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END as Salutation,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Password  IS NULL THEN "" ELSE u.Password END as Password,CASE WHEN u.Country IS NULL THEN "" ELSE u.Country END as Country,CASE WHEN u.Postcode IS NULL THEN "" ELSE u.Postcode END as Postcode,CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END as Mobile,CASE WHEN u.Phone_business IS NULL THEN "" ELSE u.Phone_business END as Phone_business,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Login_date IS NULL THEN "" ELSE u.Login_date END as Login_date,CASE WHEN u.Organisor_id IS NULL THEN "" ELSE u.Organisor_id END as Organisor_id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Speaker_message_status IS NULL THEN "0" ELSE u.Speaker_message_status END as Speaker_message_status,CASE WHEN u.attendee_flag IS NULL THEN "0" ELSE u.attendee_flag END as attendee_flag,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.gcm_id IS NULL THEN "" ELSE u.gcm_id END as gcm_id,CASE WHEN u.facebook_id IS NULL THEN "" ELSE u.facebook_id END as facebook_id,role.Name as Role_name,role.Id as Rid,ru.Event_id,ru.User_id,ru.Organisor_id as Organisor_id', FALSE);
        $this->db->from('user u');
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        $this->db->where('u.Email', $email);
        if ($event_id != '447') $this->db->where('u.Password', $password);
        $this->db->where('u.Active', '1');
        $this->db->where('role.Active', '1');
        $this->db->where('ru.Event_id', $event_id);
        $this->db->where('ru.Event_id IS NOT NULL');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result();
        if (!empty($res))
        {
            unset($res[0]->Speaker_desc);
            if ($event_id == '447' && ($res[0]->Rid == '6' || $res[0]->Rid == '4'))
            {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $pass = $rc4->decrypt($res[0]->Password);
                if ($pass != $password) $res = array();
            }
        }
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('Id', $res[0]->Organisor_id);
        $qu = $this->db->get();
        $organizeruser = $qu->result_array();
        $this->db->select('*');
        $this->db->from('user_social_links');
        $this->db->where('User_id', $res[0]->Id);
        $links = $this->db->get();
        $links = $links->row_array();
        $res[0]->linkedin_url = ($links['Linkedin_url']) ? $links['Linkedin_url'] : '';
        $res[0]->facebook_url = ($links['Facebook_url']) ? $links['Facebook_url'] : '';
        $res[0]->twitter_url = ($links['Twitter_url']) ? $links['Twitter_url'] : '';
        $res[0]->Website_url = ($links['Website_url']) ? $links['Website_url'] : '';
        $this->db1->select('*');
        $this->db1->from('organizer_user ou');
        $this->db1->where('Email', $organizeruser[0]['Email']);
        $this->db1->where('Expiry_date >=', date("Y-m-d"));
        $this->db1->join('subscription_billing_cycle sbc', 'ou.subscriptiontype=sbc.id');
        $query = $this->db1->get();
        $res1 = $query->result_array();
        if (count($res1) == 0 || $res1[0]['status'] == 0)
        {
            if (count($res) == 0)
            {
                $res = $res1;
            }
            else
            {
                $res = $res1;
                $res = "inactive";
            }
        }
        // check lead representative disable account
        $data = $this->db->select('*')->from('user_representatives')->where('event_id', $event_id)->where('rep_id', $res[0]->Id)->get()->row_array();
        if ($data && $data['status'] == '0')
        {
            $res = "inactive1";
        }
        return $res;
    }
    public function getUser($email=NULL)

    {
        return $this->db->where('Email', $email)->from('user')->get()->row_array();
    }
    public function getUserAttempts($user_id=NULL, $key_name=NULL)

    {
        $where['user_id'] = $user_id;
        $where['key_name'] = $key_name;
        return $this->db->select('*')->from('user_attempts')->where($where)->order_by('id', 'desc')->limit(1)->get()->row_array();
    }
    public function updateUserAttempts($id=NULL, $date=NULL, $attempts=NULL)

    {
        $c_date = date('Y-m-d');
        if ($c_date != date('Y-m-d', strtotime($date)))
        {
            $update['created_date'] = $c_date;
            $update['value'] = 1;
        }
        else
        {
            $update['value'] = $attempts;
        }
        $this->db->where('id', $id);
        $this->db->update('user_attempts', $update);
    }
    public function insertUserAttempts($user_id=NULL, $keyname=NULL, $value = 1)

    {
        date_default_timezone_set('UTC');
        $data['user_id'] = $user_id;
        $data['key_name'] = $keyname;
        $data['value'] = $value;
        $data['created_date'] = date('Y-m-d H:i:s');
        $data['updated_date'] = date('Y-m-d H:i:s');
        $this->db->insert('user_attempts', $data);
    }
    public function getAccname($id = '')

    {
        $this->db->select("tu.*");
        $this->db->from('user tu');
        $this->db->join('relation_event_user reu', 'reu.User_id = tu.Id', 'left');
        $this->db->where("reu.Event_id", $id);
        $this->db->where("reu.Role_id", 3);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_event_id_byaccess_id($aid=NULL)

    {
        $this->db->select('Id')->from('event');
        $this->db->where('secure_key', $aid);
        $or = $this->db->get();
        $res = $or->result_array();
        return $res[0]['Id'];
    }
    public function getLatestSpeakerUpdateDate($event_id=NULL)

    {
        $this->db->select('u.updated_date');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
        $this->db->where('reu.Role_id', '7');
        $this->db->where('reu.Event_id', $event_id);
        $this->db->order_by('u.updated_date', 'desc');
        $updated_date = $this->db->get('user u')->row_array() ['updated_date'];
        return $updated_date;
    }
    public function get_login_screen_settings($event_id=NULL)

    {
        return $this->db->where('event_id', $event_id)->get('event_settings')->row_array();
    }
    public function getAttendeeCategoryStatus($event_id=NULL)

    {
        $this->db->where('event_id', $event_id);
        $res = $this->db->get('attendee_category')->row_array();
        return $res ? '1' : '0';
    }

    public function getAssignedNewsEvent($event_id,$user_id)
    {   
        $this->db->where('event_id IN (select Event_id from relation_event_user where User_id = "'.$user_id.'" and Role_id = "4")');
        $this->db->where('news_app','1');
        $res = $this->db->get('event_settings')->result_array();
        return $res ? '1' : '0';
    }

    public function saveUserLoginEvent($event_id,$user_id)
    {
        $login_event = $this->db->where('user_id',$user_id)->get('user_login_event')->row_array();
        if($login_event)
        {
            if($login_event['event_id'] != $event_id)
            {   
                $update['event_id'] = $event_id;
                $this->db->where($login_event);
                $this->db->update('user_login_event',$update);
            }
        }
        else
        {
            $insert['event_id'] = $event_id;
            $insert['user_id'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('user_login_event',$insert);
        }
    }
    public function getAttendeeGoal($event_id,$user_id)
    {   
        $this->db->where('Attendee_id',$user_id);
        $this->db->where('Event_id',$event_id);
        $res = $this->db->get('event_attendee')->row_array()['goal'] ?: '';
        return $res;
    }
}
?>