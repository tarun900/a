<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Native_single_fcm_v3_model extends CI_Model

{
	public $variable;

	public function __construct()

	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function getL3techDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1768);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getLearn2019DefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1756);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getDefaultEvent($event_id=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen,e.enable_register_button', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', $event_id);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getVenturefestAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 383336);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 383336);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function VenturefestSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 383336);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function getRiskUsaAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 381895);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 381895);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function RiskUsaSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 381895);
		// $this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function getWaterskUsaAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 410111);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 410111);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function WatersUsaSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 410111);
		// $this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function getCrugatherAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 353837);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 353837);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function CrugatherSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 353837);
		// $this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function InfoProEventsAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 410111);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 410111);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function InfoProEventsSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 410111);
		// $this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}

	public function RethinkAllPublicEvents($gcm_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen,e.enable_register_button', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id', 479096);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id', 479096);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function MultiSearchEvent($event_name=NULL,$org_id=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen,e.enable_register_button', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', $org_id);
		// $this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['default_lang']['1__sort_by_track'] = 'Sort by Stream';
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function MultiAllPublicEvents($gcm_id=NULL,$org_id=NULL)

	{
		$date = date('Y-m-d H:i:s', strtotime("-1 week"));
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen,e.enable_register_button', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.Organisor_id',$org_id);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res1 = $oqu->result_array();
		foreach($res1 as $key => $value)
		{
			$ids[] = $value['event_id'];
		}
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.id", "left");
		$this->db->join("user_event_relation ur", "ur.event_id=e.id");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->where('ur.gcm_id', $gcm_id);
		$this->db->where('e.Organisor_id',$org_id);
		$this->db->where('e.Status', '1');
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		if (!empty($ids)) $this->db->where_not_in('e.id', $ids);
		$this->db->order_by("e.Start_date", "desc");
		$this->db->where("e.End_date < DATE_FORMAT(NOW(),'%Y-%m-%d')");
		$this->db->group_by("e.id");
		$oqu = $this->db->get();
		$res2 = $oqu->result_array();
		$res = array_merge($res1, $res2);
		$res = array_unique($res, SORT_REGULAR);
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('app_login_model');
			$value['default_lang'] = $this->app_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->app_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
}