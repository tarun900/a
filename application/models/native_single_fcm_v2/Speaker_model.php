<?php
class Speaker_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }

    public function getSpeakersListByEventId($event_id= NULL, $user_id= NULL)
    {
        $event = $this->db->select('*')->from('event')->where('Id', $event_id)->get()->row_array();
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id', 'right');
        $this->db->join('user u', 'u.Id = ru.User_id', false);
        $this->db->join('role r', 'ru.Role_id = r.Id', false);
        $this->db->where('e.Id', $event_id);
        $this->db->where('u.key_people', '0');
        $this->db->where('u.is_moderator', '0');
        $this->db->where('e.allow_msg_keypeople_to_attendee', '1');
        $this->db->where('r.Id =', 7);
        if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
        else $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        // $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();
        $speakers = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $speakers[$i]['Id'] = $res[$i]->Id;
            $speakers[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name'] = $res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0, 60);
            $speakers[$i]['Title'] = (strlen($title) >= 60) ? $subtitle . "..." : $title;
            $speakers[$i]['Speaker_desc'] = $res[$i]->Speaker_desc;
            $speakers[$i]['Email'] = $res[$i]->Email;
            $speakers[$i]['Logo'] = $res[$i]->Logo;
            $speakers[$i]['Heading'] = "";
            $count = $this->db->select('*')->from('my_favorites')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '7')->where('module_id', $res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites'] = ($count) ? '1' : '0';
        }
        return $speakers;
    }

    public function getSpeakersSortByEvent($event_id= NULL)
    {
        $event = $this->db->select('key_people_sort_by')->from('event')->where('Id', $event_id)->get()->row_array();
        return $event['key_people_sort_by'];
    }

    public function getSpeakerDocument($speaker_id= NULL, $event_id= NULL, $lang_id = NULL)
    {
        $document_id = $this->db->select('document_id')->from('user')->where('Id', $speaker_id)->get()->row_array();
        $doc_ids = array_filter(explode(",", $document_id['document_id']));
        if (count($doc_ids) > 0)
        {
            $this->db->protect_identifiers = false;
            $this->db->select('d.id,(CASE WHEN elmc.title IS NULL THEN case when d.title="" then "" ELSE d.title end ELSE elmc.title END) as title,(case when df.document_file="" then "" ELSE concat("' . base_url() . 'assets/user_documents","/",df.document_file) end) as document_file', false)->from('documents d');
            $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="16" and elmc.modules_id=d.id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
            $this->db->join('document_files df', 'd.id=df.document_id', 'left');
            $this->db->where('d.Event_id', $event_id);
            $this->db->where_in('d.id', $doc_ids);
            return $this->db->get()->result();
        }
        else
        {
            return [];
        }
    }

    public function getSpeakersListByEvent($event_id= NULL, $user_id= NULL, $lang_id = NULL)
    {
        $this->db->protect_identifiers = false;
        $event = $this->db->select('*')->from('event')->where('Id', $event_id)->get()->row_array();
        // (CASE WHEN elmc.content IS NULL THEN CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END ELSE elmc.content END) as Speaker_desc
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        "" as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id', 'right');
        $this->db->join('user u', 'u.Id = ru.User_id', false);
        $this->db->join('role r', 'ru.Role_id = r.Id', false);
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->where('e.Id', $event_id);
        $this->db->where('u.key_people', '0');
        $this->db->where('u.is_moderator', '0');
        // $this->db->where('e.allow_msg_keypeople_to_attendee','1');
        $this->db->where('r.Id =', 7);
        if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
        else $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        // $this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();
        $speakers = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $speakers[$i]['Id'] = $res[$i]->Id;
            $speakers[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name'] = $res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0, 60);
            // $speakers[$i]['Title']=(strlen($title) >= 60 )  ? $subtitle."..." : $title;
            $speakers[$i]['Title'] = $title;
            // $speakers[$i]['Speaker_desc']=$res[$i]->Speaker_desc;
            $speakers[$i]['Speaker_desc'] = "";
            $speakers[$i]['Email'] = $res[$i]->Email;
            $speakers[$i]['Logo'] = $res[$i]->Logo;
            $speakers[$i]['Heading'] = "";
            $count = $this->db->select('*')->from('my_favorites')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '7')->where('module_id', $res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites'] = ($count) ? '1' : '0';
        }
        return $speakers;
    }

    public function getSpeakerDetails($speaker_id= NULL, $event_id= NULL, $user_id= NULL, $lang_id = NULL)
    {
        $this->db->protect_identifiers = false;
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                            CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                            CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                            CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                            (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                            CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                            (CASE WHEN elmc.content IS NULL THEN CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END ELSE elmc.content END) as Speaker_desc,
                            CASE WHEN usl.Facebook_url IS NULL THEN "" ELSE usl.Facebook_url END as Facebook_url,
                            CASE WHEN usl.Twitter_url IS NULL THEN "" ELSE usl.Twitter_url END as Twitter_url,
                            CASE WHEN usl.Instagram_url IS NULL THEN "" ELSE usl.Instagram_url END as Instagram_url,
                            CASE WHEN usl.Youtube_url IS NULL THEN "" ELSE usl.Youtube_url END as Youtube_url,
                            CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url', FALSE);
        $this->db->from('user u');
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->join('user_social_links usl', 'u.Id = usl.User_id', 'left');
        $this->db->where('u.Id', $speaker_id);
        $query = $this->db->get();
        $res = $query->result();
        $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user_id)->get()->row_array();
        $extra = json_decode($extra['extra_column'], true);
        foreach($extra as $key => $value)
        {
            $keyword = "{" . str_replace(' ', '', $key) . "}";
            if (stripos(strip_tags($res[0]->Speaker_desc, $keyword)) !== false)
            {
                $res[0]->Speaker_desc = str_ireplace($keyword, $value, $res[0]->Speaker_desc);
            }
        }
        $res[0]->is_favorited = $this->isFavorited($event_id,$user_id,$speaker_id);    
        return $res;
    }

    public function getUserId($token)
    {
        $data = $this->db->select('Id')->from('user')->where('token', $token)->limit(1)->get()->row_array();
        return $data['Id'];
    }

    public function getConnectedAgenda($speaker_id= NULL, $event_id= NULL, $lang_id = NULL)
    {
        $this->db->protect_identifiers = false;
        $data = $this->db->select('(CASE WHEN elmc.title IS NULL THEN a.Heading ELSE elmc.title END) as Heading,(CASE WHEN elmc.type IS NULL THEN st.type_name ELSE elmc.type END) as Types,a.Id,a.Start_date,a.Start_time,a.custom_location,e.date_format')->from('agenda a')->join('session_types st', 'st.type_id=a.Types')->join('agenda_category_relation ac', 'ac.agenda_id = a.Id')->join('event e', 'e.Id = a.Event_id')->join('event_lang_modules_content elmc', 'elmc.menu_id="1" and elmc.modules_id=a.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left')->where("FIND_IN_SET('$speaker_id',a.Speaker_id) != 0", NULL, FALSE)->where('a.Event_id', $event_id)->group_by('a.Id')->get()->result_array();
        $time_format = $this->db->where('Event_id', $event_id)->get('fundraising_setting')->row_array();
        foreach($data as $key => $value)
        {
            if ($value['date_format'] == '0')
            {
                $data[$key]['Start_date'] = date('d/m/Y', strtotime($value['Start_date']));
            }
            else
            {
                $data[$key]['Start_date'] = date('m/d/Y', strtotime($value['Start_date']));
            }
            if ($time_format['format_time'] == '0')
            {
                $data[$key]['Start_time_new'] = date('h:i A', strtotime($value['Start_time']));
            }
            else
            {
                $data[$key]['Start_time_new'] = date('H:i', strtotime($value['Start_time']));
            }
            $data[$key]['Start_time'] = date('H:i', strtotime($value['Start_time']));
            $data[$key]['timestamp'] = strtotime($value['Start_date'].' '.$value['Start_time']);
        }
        function sortByOrder($a, $b) {
            return $a['timestamp'] - $b['timestamp'];
        }
        usort($data, 'sortByOrder');
        return $data;
    }

    public function getSpeakerDetailsNew($speaker_id= NULL, $event_id= NULL, $user_id= NULL)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,
                           CASE WHEN usl.Facebook_url IS NULL THEN "" ELSE usl.Facebook_url END as Facebook_url,
                           CASE WHEN usl.Twitter_url IS NULL THEN "" ELSE usl.Twitter_url END as Twitter_url,
                           CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url,
                           CASE WHEN usl.Youtube_url IS NULL THEN "" ELSE usl.Youtube_url END as Youtube_url,
                           CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url', FALSE);
        $this->db->from('user u');
        $this->db->join('user_social_links usl', 'u.Id = usl.User_id', 'left');
        $this->db->where('u.Id', $speaker_id);
        $query = $this->db->get();
        $res = $query->result();
        $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user_id)->get()->row_array();
        $extra = json_decode($extra['extra_column'], true);
        foreach($extra as $key => $value)
        {
            $keyword = "{" . str_replace(' ', '', $key) . "}";
            if (stripos(strip_tags($res[0]->Speaker_desc, $keyword)) !== false)
            {
                $res[0]->Speaker_desc = str_ireplace($keyword, $value, $res[0]->Speaker_desc);
            }
        }
        return $res;
    }

    public function get_favorited_speakers($event_id= NULL, $user_id= NULL)
    {
        return $this->db->select('module_id as speaker_id')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '7')->get('my_favorites')->result_array();
    }

    public function getSpeakerListOffline($Event_id= NULL, $user_id= NULL)#1
    {   
        $this->db->protect_identifiers = false;
        $event_id = $Event_id;
        $event = $this->db->select('*')->from('event')->where('Id', $event_id)->get()->row_array();

        /*$this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        "" as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id', 'right');
        $this->db->join('user u', 'u.Id = ru.User_id', false);
        $this->db->join('role r', 'ru.Role_id = r.Id', false);
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->where('e.Id', $event_id);
        $this->db->where('u.key_people', '0');
        $this->db->where('u.is_moderator', '0');
        $this->db->where('r.Id =', 7);
        if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
        else $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result();
        $speakers = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $speakers[$i]['Id'] = $res[$i]->Id;
            $speakers[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name'] = $res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0, 60);
            // $speakers[$i]['Title']=(strlen($title) >= 60 )  ? $subtitle."..." : $title;
            $speakers[$i]['Title'] = $title;
            // $speakers[$i]['Speaker_desc']=$res[$i]->Speaker_desc;
            $speakers[$i]['Speaker_desc'] = "";
            $speakers[$i]['Email'] = $res[$i]->Email;
            $speakers[$i]['Logo'] = $res[$i]->Logo;
            $speakers[$i]['Heading'] = "";
            $count = $this->db->select('*')->from('my_favorites')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '7')->where('module_id', $res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites'] = ($count) ? '1' : '0';
        }
        return $speakers;*/
        $types = $this->db->select('*')->from('speaker_type')->where('event_id', $Event_id)->order_by('type_position')->get()->result_array();

        $speaker = array();

        foreach($types as $key => $value)
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        "" as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', false);
            $this->db->from('event e');
            $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id', 'right');
            $this->db->join('user u', 'u.Id = ru.User_id', false);
            $this->db->join('speaker_profile sp','sp.user_id = u.Id');
            $this->db->join('role r', 'ru.Role_id = r.Id', false);
            $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
            $this->db->where('e.Id', $event_id);
            $this->db->where('u.key_people', '0');
            $this->db->where('u.is_moderator', '0');
            $this->db->where('r.Id =', 7);
            $this->db->where('sp.event_id',$event_id);
            $this->db->where('sp.type',$value['type_id']);
            if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
            else $this->db->order_by('u.Firstname');
            $this->db->group_by('u.Id');
            $query = $this->db->get();
            $sp_data = $query->result_array();
            if ($sp_data)
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['type_id'] = $value['type_id'];
                $results['type_position'] = $value['type_position'];
                $results['data'] = $sp_data;
                $speaker[] = $results;
            }
        }
        $sprids = $this->db->where('type IS NOT NULL')->where('event_id',$event_id)->get('speaker_profile')->result_array();
        $sprids = array_column_1($sprids,'user_id');
        
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                        CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                        CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                        (CASE WHEN elmc.title IS NULL THEN CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END ELSE elmc.title END) as Title,
                        CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                        "" as Speaker_desc,
                        CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                        CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id', 'right');
        $this->db->join('user u', 'u.Id = ru.User_id', false);
        $this->db->join('role r', 'ru.Role_id = r.Id', false);
        $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id="7" and elmc.modules_id=u.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
        $this->db->where('e.Id', $event_id);
        if(!empty($sprids))
        $this->db->where_not_in('u.Id', $sprids);
        $this->db->where('u.key_people', '0');
        $this->db->where('u.is_moderator', '0');
        $this->db->where('r.Id =', 7);
        if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
        else $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $sp1_data = $query->result_array();
        if ($sp1_data)
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['type_id'] = '';
            $results['type_position'] = '99999';
            $results['data'] = $sp1_data;
            $speaker[] = $results;
        }
        $this->load->library('image_lib');
        foreach($speaker as $key => $value)
        {
            foreach($value['data'] as $key1 => $value1)
            {
                $speaker[$key]['data'][$key1]['Firstname'] = trim(ucfirst($value1['Firstname']));
                $speaker[$key]['data'][$key1]['Lastname'] = trim(ucfirst($value1['Lastname']));
                $speaker[$key]['data'][$key1]['Company_name'] = ucfirst($value1['Company_name']);
                $title = $value1['Title'];
                $subtitle = substr($title, 0, 60);
                $speaker[$key]['data'][$key1]['Title'] = $title;
                $count = $this->db->select('*')->from('my_favorites')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '7')->where('module_id', $res[$i]->Id)->get()->num_rows();
                $speaker[$key]['data'][$key1]['is_favorites'] = ($count) ? '1' : '0';

                $new_name = "new_" . $value1['Logo'];
                $extension_pos = strrpos($new_name, '.');
                $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/thumb/" . $new_name;
                if (!empty($value1['Logo']) && !file_exists($destination_url))
                {
                    $source_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/" . $value1['Logo'];
                    $info = getimagesize($source_url);
                    $new_name = "new_" . $value1['Logo'];
                    $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/thumb/" . $new_name;
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $source_url;
                    $config['new_image'] = $destination_url;
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = '110';
                    $config['height'] = '110';
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $extension_pos = strrpos($new_name, '.');
                    $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                    if(file_exists($destination_url))
                    {
                         $speaker[$key]['data'][$key1]['Logo'] = 'thumb/' . $new_name;
                    }
                }
                elseif (file_exists($destination_url))
                {
                    $speaker[$key]['data'][$key1]['Logo'] = 'thumb/' . $new_name;
                }
            }
        }
        return $speaker;
    }
    public function isFavorited($event_id=0,$user_id=0,$speaker_id=0)
    {
        $where['event_id'] = $event_id;
        $where['user_id'] = $user_id;
        $where['module_id'] = $speaker_id;
        $where['module_type'] = '7';
        $res = $this->db->where($where)->get('my_favorites')->num_rows();
        return $res;
    }
}
?>