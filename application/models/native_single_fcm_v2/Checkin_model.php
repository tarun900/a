<?php
class Checkin_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getAttendeeListByEventId($Event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Id', $Event_id);
        $this->db->where('r.Id', 4);
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
        $attendees = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $attendees[$i]['Id'] = $res[$i]->Id;
            $attendees[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name'] = $res[$i]->Company_name;
            $attendees[$i]['Title'] = $res[$i]->Title;
            $attendees[$i]['Email'] = $res[$i]->Email;
            $attendees[$i]['Logo'] = $res[$i]->Logo;
            $is_checked_in = $this->db->select('*')->from('event_attendee')->where('Event_id', $Event_id)->where('Attendee_id', $res[$i]->Id)->get()->row_array();
            $attendees[$i]['is_checked_in'] = (!empty($is_checked_in)) ? $is_checked_in['check_in_attendee'] : '0';
        }
        return $attendees;
    }
    public function getAttendeeDetails($attendee_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('user u');
        $this->db->where('u.Id', $attendee_id);
        $res = $this->db->get()->row_array();
        return $res;
    }
    public function getBadgeLogo($event_id=NULL)

    {
        $this->db->select('CASE WHEN badges_logo_images IS NULL THEN "" ELSE badges_logo_images END as badges_logo_images', FALSE);
        $this->db->from('badges_values');
        $this->db->where('event_id', $event_id);
        $res = $this->db->get()->result_array();
        // print_r($res); exit();
        return $res[0]['badges_logo_images'];
    }
    public function saveCheckIn($data=NULL)

    {
        $result = $this->db->select('*')->from('event_attendee')->where($data)->get()->row_array();
        if (count($result))
        {
            if ($result['check_in_attendee'] == '0')
            {
                $update['check_in_time'] = date('Y-m-d H:i:s');
                $update['check_out_time'] = NULL;
            }
            elseif ($result['check_in_attendee'] == '1')
            {
                $update['check_out_time'] = date('Y-m-d H:i:s');
            }
            $flag = ($result['check_in_attendee'] == '0') ? 1 : 0;
            $this->db->where($data);
            $update['check_in_attendee'] = ($result['check_in_attendee'] == '0') ? '1' : '0';
            $this->db->update('event_attendee', $update);
        }
        else
        {
            $flag = 1;
            $data['check_in_attendee'] = '1';
            $data['check_in_time'] = date('Y-m-d H:i:s');
            $this->db->insert('event_attendee', $data);
        }
        return $flag;
    }
    public function getCustomInfo($where=NULL)

    {
        $custom_coulmns = $this->db->select('*')->from('custom_column')->where('event_id', $where['Event_id'])->order_by('column_id')->get()->result_array();
        $extra_info = $this->db->select('*')->from('event_attendee')->where($where)->get()->row_array();
        $extra_value = json_decode($extra_info['extra_column'], TRUE);
        foreach($custom_coulmns as $key => $value)
        {
            $val = $extra_value[$value['column_name']];
            $data['key'] = $value['column_name'];
            $data['value'] = ($val) ? $val : '';
            $result[] = $data;
        }
        return $result;
    }
    public function saveAttendeeDetails($personal_info=NULL, $attendee_id=NULL)

    {
        $this->db->where('Id', $attendee_id);
        $this->db->update('user', $personal_info);
    }
    public function saveCustomInfo($custom_info=NULL, $where=NULL)

    {
        $this->db->where($where);
        $this->db->update('event_attendee', $custom_info);
    }
    public function add_checkin($data=NULL)

    {
        $this->db->select('*');
        $this->db->where('email', $data['email']);
        $this->db->where('event_id', $data['event_id']);
        $query = $this->db->get('check_in');
        if ($query->num_rows())
        {
            $res = $query->result_array();
            if ($res[0]['check_out_time'] != NULL && $res[0]['check_in_time'] != NULL)
            {
                $data['check_out_time'] = NULL;
                $data['check_in_time'] = date('Y-m-d H:i:s');
                $this->db->where('event_id', $data['event_id']);
                $this->db->where('email', $data['email']);
                $this->db->update('check_in', $data);
                return "User Checkin";
            }
            else
            {
                $data['check_out_time'] = date('Y-m-d H:i:s');
                $this->db->where('event_id', $data['event_id']);
                $this->db->where('email', $data['email']);
                $this->db->update('check_in', $data);
                return "User Checkout";
            }
        }
        else
        {
            $data['check_in_time'] = date('Y-m-d H:i:s');
            $this->db->insert('check_in', $data);
            return "User Checkin";
        }
    }
    public function getAttendeeDetailsCheckin($email=NULL, $Event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Id', $Event_id);
        $this->db->where('u.Email', $email);
        $this->db->where('r.Id', 4);
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
        $attendees = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $attendees[$i]['Id'] = $res[$i]->Id;
            $attendees[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name'] = $res[$i]->Company_name;
            $attendees[$i]['Title'] = $res[$i]->Title;
            $attendees[$i]['Email'] = $res[$i]->Email;
            $attendees[$i]['Logo'] = $res[$i]->Logo;
            $is_checked_in = $this->db->select('*')->from('event_attendee')->where('Event_id', $Event_id)->where('Attendee_id', $res[$i]->Id)->get()->row_array();
            $attendees[$i]['is_checked_in'] = (!empty($is_checked_in)) ? $is_checked_in['check_in_attendee'] : '0';
        }
        return $attendees;
    }
    public function get_checkedin_attendee_count($Event_id=NULL)

    {
        $this->db->select('count(check_in_attendee) as count');
        $this->db->where('Event_id', $Event_id);
        $this->db->where('check_in_attendee', '1');
        $query = $this->db->get('event_attendee');
        $res = $query->row_array();
        return $res['count'];
    }
    public function get_checkedin_attendee_list($Event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('user u');
        $this->db->join('relation_event_user reu', 'u.Id=reu.User_id');
        $this->db->join('event_attendee ea', 'u.Id=ea.Attendee_id');
        $this->db->join('role r', 'reu.Role_id = r.Id');
        $this->db->where('reu.Event_id', $Event_id);
        $this->db->where('r.Id', 4);
        $this->db->where('ea.check_in_attendee', '1');
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
        $attendees = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $attendees[$i]['Id'] = $res[$i]->Id;
            $attendees[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name'] = $res[$i]->Company_name;
            $attendees[$i]['Title'] = $res[$i]->Title;
            $attendees[$i]['Email'] = $res[$i]->Email;
            $attendees[$i]['Logo'] = $res[$i]->Logo;
            $is_checked_in = $this->db->select('*')->from('event_attendee')->where('Event_id', $Event_id)->where('Attendee_id', $res[$i]->Id)->get()->row_array();
            $attendees[$i]['is_checked_in'] = (!empty($is_checked_in)) ? $is_checked_in['check_in_attendee'] : '0';
        }
        return $attendees;
    }
    public function get_user_info($User_id=NULL, $Event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('user u');
        $this->db->join('relation_event_user reu', 'u.Id=reu.User_id');
        $this->db->where('reu.Event_id', $Event_id);
        $this->db->where('u.Id', $User_id);
        $this->db->where('reu.Role_id', 4);
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->row_array();
        return $res;
    }
    public function get_badges($Event_id=NULL)

    {
        $this->db->select('*', FALSE);
        $this->db->from('badges_values');
        $this->db->where('event_id', $Event_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getAttendeeDetailsCheckin_new($u_id=NULL, $Event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
        $this->db->join('user u', 'u.Id = ru.User_id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('e.Id', $Event_id);
        $this->db->where('r.Id', 4);
        $this->db->where('u.Id', $u_id);
        // start - for email QR codes - rashmi
        $this->db->or_where('u.Email', $u_id);
        // end - for email QR codes - rashmi
        $this->db->order_by('u.Lastname');
        $query = $this->db->get();
        $res = $query->result();
        $attendees = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $attendees[$i]['Id'] = $res[$i]->Id;
            $attendees[$i]['Firstname'] = ucfirst($res[$i]->Firstname);
            $attendees[$i]['Lastname'] = ucfirst($res[$i]->Lastname);
            $attendees[$i]['Company_name'] = $res[$i]->Company_name;
            $attendees[$i]['Title'] = $res[$i]->Title;
            $attendees[$i]['Email'] = $res[$i]->Email;
            $attendees[$i]['Logo'] = $res[$i]->Logo;
            $is_checked_in = $this->db->select('*')->from('event_attendee')->where('Event_id', $Event_id)->where('Attendee_id', $res[$i]->Id)->get()->row_array();
            $attendees[$i]['is_checked_in'] = (!empty($is_checked_in)) ? $is_checked_in['check_in_attendee'] : '0';
        }
        return $attendees;
    }
}
?>