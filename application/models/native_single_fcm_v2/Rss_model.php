<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// include('../application/libraries/Rssparser.php');
class Rss_model extends CI_Model

{
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', true);
		$this->db->protect_identifiers = 'false';
	}
	public function update_rss()

	{
		$this->load->library('Rssparser');
		$rss = $this->rssparser->set_feed_url('http://www.bondsloans.com/news/rss')->set_cache_life(0)->getFeed();
		$last_date = $this->db->where('organizer_id', '40171')->order_by('date', 'desc')->get('rss_feed')->row_array();
		foreach($rss as $key => $value)
		{
			$value['organizer_id'] = '40171';
			if ($last_date['date'] < $value['date'])
			{
				$this->db->insert('rss_feed', $value);
			}
		}
		$rss_category = array(
			'33',
			'34',
			'35',
			'36',
			'37',
			'38',
			'39',
			'40',
			'41',
			'42',
			'43',
			'44',
			'45',
			'46',
			'47',
			'48',
			'49',
			'50',
			'51',
			'52',
			'53',
			'54',
			'55',
			'56',
			'57',
			'60',
			'62',
			'63',
			'64',
			'65',
			'66',
			'67',
			'68',
			'69',
			'71'
		);
		foreach($rss_category as $key => $value)
		{
			$rss = $this->rssparser->set_feed_url('http://www.bondsloans.com/news/rss/' . $value)->set_cache_life(0)->getFeed();
			foreach($rss as $k1 => $v1)
			{
				$tmp_rss = $this->db->where('link', $v1['link'])->get('rss_feed')->row_array();
				$v1['organizer_id'] = '40171';
				if (!empty($tmp_rss))
				{
					$this->db->where('id', $tmp_rss['id']);
					$this->db->update('rss_feed', $v1);
				}
				else
				{
					$this->db->insert('rss_feed', $v1);
				}
			}
		}
		echo "RSS FEEDS Updated Successfully";
	}
	public function get_top_news()

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('organizer_id', '40171');
		$this->db->order_by('DATE_FORMAT(`date`,"%Y-%m-%d %H:%i:%s") desc', NULL, FALSE);
		$this->db->limit(15);
		$res = $this->db->get('rss_feed')->result_array();
		return $res;
	}
	public function get_my_news($page_no=NULL, $keyword=NULL)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('organizer_id', '40171');
		if (!empty($keyword)) $this->db->where_in('tag', $keyword);
		$this->db->order_by('DATE_FORMAT(`date`,"%Y-%m-%d %H:%i:%s") desc', NULL, FALSE);
		$res = $this->db->get('rss_feed')->result_array();
		return $res;
	}
	public function get_rss_tags()

	{
		$res = $this->db->select('id,tag')->where('organizer_id', '40171')->get('rss_tag')->result_array();
		return $res;
	}
	public function get_news_details($news_id=NULL, $event_id=NULL, $user_id=NULL)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN content IS NULL THEN "" ELSE content END AS content,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('id', $news_id);
		$res = $this->db->get('rss_feed rf')->row_array();
		$res['is_like'] = $this->check_like($event_id, $user_id, $news_id);
		$res['comments'] = $this->getComments($news_id, $user_id);
		$res['content'] = str_replace('src="', 'src="http://www.bondsloans.com', $res['content']);
		return $res;
	}
	public function check_like($event_id=NULL, $user_id=NULL, $news_id=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->where('module_type', 'rss')->where('user_id', $user_id)->where('module_primary_id', $news_id)->get('activity_like')->row_array();
		return ($res['like']) ? : '0';
	}
	public function like_news($news_id=NULL, $event_id=NULL, $user_id=NULL, $like=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->where('module_type', 'rss')->where('user_id', $user_id)->where('module_primary_id', $news_id)->get('activity_like')->row_array();
		if (!empty($res))
		{
			$update['like'] = $like;
			$this->db->where('like_id', $res['like_id'])->update('activity_like', $update);
		}
		else
		{
			$insert['module_type'] = 'rss';
			$insert['module_primary_id'] = $news_id;
			$insert['user_id'] = $user_id;
			$insert['event_id'] = $event_id;
			$insert['datetime'] = date('Y-m-d H:i:s');
			$insert['like'] = $like;
			$this->db->insert('activity_like', $insert);
		}
	}
	public function make_comment($data=NULL)

	{
		$this->db->insert('activity_comment', $data);
		return $this->db->insert_id();
	}
	public function getCommentDetails($message_id=NULL)

	{
		$this->db->select('comment_image');
		$this->db->from('activity_comment');
		$this->db->where("comment_id", $message_id);
		$query = $this->db->get();
		$res = $query->row();
		return $res;
	}
	public function updateCommentImage($message_id=NULL, $update_arr=NULL)

	{
		$this->db->where("comment_id", $message_id);
		$this->db->update("activity_comment", $update_arr);
	}
	public function getComments($id=NULL, $user_id = 0)

	{
		$this->db->select("ac.comment_id,
        	               ac.comment,
        	               concat(usr.Firstname,' ',usr.Lastname) as name,
        	               CASE WHEN usr.Logo IS NULL THEN '' ELSE usr.Logo END as logo,
        	               ac.datetime,
        	               comment_image,
        	               ac.user_id", false);
		$this->db->from('activity_comment ac');
		$this->db->join('user usr', 'ac.user_id = usr.Id');
		$this->db->where('ac.module_primary_id', $id);
		$this->db->where('ac.module_type', 'rss');
		$this->db->order_by('ac.datetime DESC', NULL, FALSE);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $key => $value)
		{
			$result[$key]['show_delete'] = ($user_id == $value['user_id']) ? '1' : '0';
			$result[$key]['datetime'] = $this->get_timeago(strtotime($value['datetime']));
			$result[$key]['comment_image'] = (!empty($value['comment_image']) && $value['comment_image'] != 'false') ? json_decode($value['comment_image']) : [];
		}
		return $result;
	}
	public function get_timeago($ptime=NULL)

	{
		$estimate_time = time() - $ptime;
		if ($estimate_time < 1)
		{
			return '1 second ago';
		}
		$condition = array(
			12 * 30 * 24 * 60 * 60 => 'year',
			30 * 24 * 60 * 60 => 'month',
			24 * 60 * 60 => 'day',
			60 * 60 => 'hour',
			60 => 'minute',
			1 => 'second'
		);
		foreach($condition as $secs => $str)
		{
			$d = $estimate_time / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
			}
		}
	}
	public function getorganizer_id($event_id=NULL)

	{
		$this->db->select('Organisor_id');
		$this->db->from('relation_event_user');
		$this->db->where('Event_id', $event_id);
		$this->db->where('Role_id', '3');
		$query = $this->db->get();
		$res = $query->result();
		return $res;
	}
	public function update_rss_global()

	{
		$this->load->library('rssparser');
		$rss = $this->rssparser->set_feed_url('https://www.theretailbulletin.com/rss/app.php')->set_cache_life(0)->getFeed();
		$last_date = $this->db->where('organizer_id', '45783')->order_by('date', 'desc')->get('rss_feed')->row_array();
		foreach($rss as $key => $value)
		{
			$value['organizer_id'] = '45783';
			if ($last_date['date'] < $value['date'])
			{
				$this->db->insert('rss_feed', $value);
			}
		}
		$rss = $this->rssparser->set_feed_url('https://www.theretailbulletin.com/rss/app.php?' . $value)->set_cache_life(0)->getFeed();
		foreach($rss as $k1 => $v1)
		{
			$tmp_rss = $this->db->where('link', $v1['link'])->get('rss_feed')->row_array();
			$v1['organizer_id'] = '45783';
			if (!empty($tmp_rss))
			{
				$this->db->where('id', $tmp_rss['id']);
				$this->db->update('rss_feed', $v1);
			}
			else
			{
				$this->db->insert('rss_feed', $v1);
			}
		}
		require 'HttpRequest.php';

		function getData($i)
		{
			$r = new HttpRequest("get", "https://www.concordia.net/wp-json/wp/v2/posts?per_page=100&page=" . $i);
			if ($r->getError())
			{
				return 0;
			}
			else
			{
				$data = json_decode($r->getResponse());
				return $data;
			}
		}
		$j = 0;
		for ($i = 1; $i < 50; $i++)
		{
			$tmpdata = getData($i);
			if ($tmpdata == 0)
			{
				foreach($data as $key => $value)
				{
					foreach($value as $key1 => $value1)
					{
						$last_date = $this->db->where('organizer_id', '15663')->order_by('date', 'desc')->get('rss_feed')->row();
						if (strtotime($value1->date) > strtotime($last_date->date))
						{
							$insert[$j]['title'] = $value1->title->rendered;
							$insert[$j]['content'] = $value1->content->rendered;
							$insert[$j]['link'] = $value1->link;
							$insert[$j]['date'] = date('Y-m-d H:i:s', strtotime($value1->date));
							$insert[$j]['organizer_id'] = '15663';
							$featuredmedia = json_decode(json_encode($value1->_links) , true);
							$r = new HttpRequest("get", $featuredmedia['wp:featuredmedia'][0]['href']);
							if ($r->getError())
							{
								$insert[$j]['image'] = '';
							}
							else
							{
								$featuredmedia = json_decode($r->getResponse());
								$insert[$j]['image'] = $featuredmedia->source_url;
							}
							$j++;
						}
					}
					if (!empty($insert)) $this->db->insert_batch('rss_feed', $insert);
					j($insert);
				}
			}
			else
			{
				$data[$i] = $tmpdata;
			}
		}
		echo "RSS FEEDS Updated Successfully";
	}
	public function get_top_news_global($organizer_id=NULL, $no_limit = 0)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('organizer_id', $organizer_id);
		$this->db->order_by('DATE_FORMAT(`date`,"%Y-%m-%d %H:%i:%s") desc', NULL, FALSE);
		$this->db->limit(15);
		$this->db->group_by('link');
		$res = $this->db->get('rss_feed')->result_array();
		return $res;
	}
	public function get_top_news_page($organizer_id=NULL)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('organizer_id', $organizer_id);
		$this->db->order_by('DATE_FORMAT(`date`,"%Y-%m-%d %H:%i:%s") desc');
		$this->db->group_by('link');
		$res = $this->db->get('rss_feed')->result_array();
		return $res;
	}
	public function get_my_news_global($page_no=NULL, $keyword=NULL, $orid=NULL)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('organizer_id', $orid);
		if (!empty($keyword)) $this->db->where_in('tag', $keyword);
		$this->db->order_by('DATE_FORMAT(`date`,"%Y-%m-%d %H:%i:%s") desc', NULL, FALSE);
		$this->db->group_by('link');
		$res = $this->db->get('rss_feed')->result_array();
		return $res;
	}
	public function get_rss_tags_global($orid=NULL)

	{
		$res = $this->db->select('id,tag')->where('organizer_id', $orid)->get('rss_tag')->result_array();
		return $res;
	}
	public function get_news_details_global($news_id=NULL, $event_id=NULL, $user_id=NULL)

	{
		$this->db->select('id,
			               CASE WHEN title IS NULL THEN "" ELSE title END AS title,
			               CASE WHEN `desc` IS NULL THEN "" ELSE `desc` END AS `desc`,
			               CASE WHEN link IS NULL THEN "" ELSE link END AS link,
			               CASE WHEN content IS NULL THEN "" ELSE content END AS content,
			               CASE WHEN tag IS NULL THEN "" ELSE tag END AS tag,
			               CASE WHEN image IS NULL THEN "" ELSE image END AS image,
			               CASE WHEN date IS NULL THEN "" ELSE concat("Published ",DATE_FORMAT(`date`,"%d %M %Y %H:%i")) END AS date,
			               CASE WHEN source IS NULL THEN "" ELSE source END AS source', FALSE);
		$this->db->where('id', $news_id);
		$res = $this->db->get('rss_feed rf')->row_array();
		$res['is_like'] = $this->check_like($event_id, $user_id, $news_id);
		$res['comments'] = $this->getComments($news_id, $user_id);
		$res['content'] = str_replace('src="', 'src="http://www.bondsloans.com', $res['content']);
		return $res;
	}
	public function check_like_global($event_id=NULL, $user_id=NULL, $news_id=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->where('module_type', 'rss')->where('user_id', $user_id)->where('module_primary_id', $news_id)->get('activity_like')->row_array();
		return ($res['like']) ? : '0';
	}
	public function like_news_global($news_id=NULL, $event_id=NULL, $user_id=NULL, $like=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->where('module_type', 'rss')->where('user_id', $user_id)->where('module_primary_id', $news_id)->get('activity_like')->row_array();
		if (!empty($res))
		{
			$update['like'] = $like;
			$this->db->where('like_id', $res['like_id'])->update('activity_like', $update);
		}
		else
		{
			$insert['module_type'] = 'rss';
			$insert['module_primary_id'] = $news_id;
			$insert['user_id'] = $user_id;
			$insert['event_id'] = $event_id;
			$insert['datetime'] = date('Y-m-d H:i:s');
			$insert['like'] = $like;
			$this->db->insert('activity_like', $insert);
		}
	}
	public function make_comment_global($data=NULL)

	{
		$this->db->insert('activity_comment', $data);
		return $this->db->insert_id();
	}
	public function getCommentDetails_global($message_id=NULL)

	{
		$this->db->select('comment_image');
		$this->db->from('activity_comment');
		$this->db->where("comment_id", $message_id);
		$query = $this->db->get();
		$res = $query->row();
		return $res;
	}
	public function updateCommentImage_global($message_id=NULL, $update_arr=NULL)

	{
		$this->db->where("comment_id", $message_id);
		$this->db->update("activity_comment", $update_arr);
	}
	public function getComments_global($id=NULL, $user_id = 0)

	{
		$this->db->select("ac.comment_id,
        	               ac.comment,
        	               concat(usr.Firstname,' ',usr.Lastname) as name,
        	               CASE WHEN usr.Logo IS NULL THEN '' ELSE usr.Logo END as logo,
        	               ac.datetime,
        	               comment_image,
        	               ac.user_id", false);
		$this->db->from('activity_comment ac');
		$this->db->join('user usr', 'ac.user_id = usr.Id');
		$this->db->where('ac.module_primary_id', $id);
		$this->db->where('ac.module_type', 'rss');
		$this->db->order_by('ac.datetime DESC', NULL, FALSE);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $key => $value)
		{
			$result[$key]['show_delete'] = ($user_id == $value['user_id']) ? '1' : '0';
			$result[$key]['datetime'] = $this->get_timeago(strtotime($value['datetime']));
			$result[$key]['comment_image'] = (!empty($value['comment_image']) && $value['comment_image'] != 'false') ? json_decode($value['comment_image']) : [];
		}
		return $result;
	}
	public function get_timeago_global($ptime=NULL)

	{
		$estimate_time = time() - $ptime;
		if ($estimate_time < 1)
		{
			return '1 second ago';
		}
		$condition = array(
			12 * 30 * 24 * 60 * 60 => 'year',
			30 * 24 * 60 * 60 => 'month',
			24 * 60 * 60 => 'day',
			60 * 60 => 'hour',
			60 => 'minute',
			1 => 'second'
		);
		foreach($condition as $secs => $str)
		{
			$d = $estimate_time / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
			}
		}
	}
}