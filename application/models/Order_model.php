<?php
class order_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_order_requested($id = NULL)

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name, u.Firstname as client_name,u.Company_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if ($this->data['user']->Role_name == "Client")
        {
            $this->db->where('a.CLient_id', $this->data['user']->Id);
        }
        if ($id == NULL)
        {
            $this->db->where('a.Approve', '0');
        }
        $this->db->order_by('a.Id desc');
        $order_query = $this->db->get();
        // echo $this->db->last_query();die;
        $order_query_res = $order_query->result_array();
        return $order_query_res;
    }
    public function get_approveorder_requested()

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name,u.Firstname as client_name,u.Company_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('(a.Approve="1"', NULL, FALSE);
        $this->db->or_where('a.Approve="2")', NULL, FALSE);
        $this->db->order_by('a.Id desc');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        return $order_query_res;
    }
    public function get_order_approved_or_cancelled($id = NULL)

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if (!empty($id)) $this->db->where('a.CLient_id', $id);
        else $this->db->where('a.CLient_id', $this->data['user']->Id);
        $this->db->where('(a.Approve="1"', NULL, FALSE);
        $this->db->or_where('a.Approve="2")', NULL, FALSE);
        $this->db->order_by('a.Id desc');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        return $order_query_res;
    }
    public function get_order_approved()

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name,case a.Allocated_qty when NULL then a.Qty Else a.Allocated_qty End as Qty', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if ($this->data['user']->Role_name == "Client")
        {
            $this->db->where('a.CLient_id', $this->data['user']->Id);
        }
        $this->db->where('a.Approve', '1');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        return $order_query_res;
    }
    public function insert_order($data=NULL)

    {
        $this->db->insert('allocation', $data);
        return true;
    }
    public function get_ordered_event_details($data=NULL)

    {
        $this->db->select('*,SUM(pq.qty) as sum,ps.Id as size_id, pq.Id as qty_id');
        $this->db->from('event_qty pq');
        $this->db->join('event p', 'p.Id = pq.Event_id');
        $this->db->join('event_size ps', 'ps.Id = pq.Size_id');
        $this->db->where('ps.Id', $data['size_id']);
        $this->db->where('MONTH(pq.Month)', $data['month']);
        $this->db->group_by('MONTH(pq.Month)');
        $query = $this->db->get();
        $res = $query->first_row();
        return $res;
    }
    public function check_order($id=NULL)

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('a.Approve', '0');
        $this->db->where('a.Id', $id);
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        if (count($order_query_res) > 0) return true;
        else return false;
    }
    public function complate_order($id=NULL)

    {
        $this->db->select('a.*, ps.Size, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('a.Id', $id);
        $this->db->where('(a.Approve = "1" OR a.Approve = "2")');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        if (count($order_query_res) > 0) return true;
        else return false;
    }
    public function get_order_details($id=NULL)

    {
        $this->db->select('a.*, ps.Size, p.Id as Event_id, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('a.Approve', '0');
        $this->db->where('a.Id', $id);
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        $event = $this->Event_model->get_admin_event($order_query_res[0]['Event_id']);
        unset($event[0]['Size']);
        unset($event[0]['Category']);
        unset($event[0]['Attendee_Qty_Count']);
        $supllier = count($event[0]['Attendee']);
        for ($i = 0; $i < $supllier; $i++)
        {
            if ($event[0]['Attendee'][$i]['Id'] != $order_query_res[0]['Event_size_id']) unset($event[0]['Attendee'][$i]);
            else if ($event[0]['Attendee'][$i]['Month'] != date('M-Y', strtotime($order_query_res[0]['Month']))) unset($event[0]['Attendee'][$i]);
        }
        $event[0]['Attendee'] = array_values($event[0]['Attendee']);
        $client = $this->client_model->get_client_list($order_query_res[0]['Client_id']);
        $order_detail = array();
        $order_detail['Event'] = $event;
        $order_detail['Order'] = $order_query_res;
        $order_detail['Client'] = $client;
        return $order_detail;
    }
    public function get_order($id=NULL)

    {
        $this->db->select('a.*, ps.Size, p.Id as Event_id, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('a.Id', $id);
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        $event = $this->Event_model->get_admin_event($order_query_res[0]['Event_id']);
        unset($event[0]['Size']);
        unset($event[0]['Category']);
        unset($event[0]['Attendee_Qty_Count']);
        $supllier = count($event[0]['Attendee']);
        for ($i = 0; $i < $supllier; $i++)
        {
            if ($event[0]['Attendee'][$i]['Id'] != $order_query_res[0]['Event_size_id']) unset($event[0]['Attendee'][$i]);
            else if ($event[0]['Attendee'][$i]['Month'] != date('M-Y', strtotime($order_query_res[0]['Month']))) unset($event[0]['Attendee'][$i]);
        }
        $event[0]['Attendee'] = array_values($event[0]['Attendee']);
        $client = $this->client_model->get_client_list($order_query_res[0]['Client_id']);
        $order_detail = array();
        $order_detail['Event'] = $event;
        $order_detail['Order'] = $order_query_res;
        $order_detail['Client'] = $client;
        return $order_detail;
    }
    public function attendee_partial_allocation($data=NULL)

    {
        $update_allocation = array(
            'Allocated_qty' => $data['qty'],
            'Client_approve' => '0',
        );
        $this->db->where('Id', $data['aid']);
        $this->db->update('allocation', $update_allocation);
    }
    public function attendee_allocation($data=NULL)

    {
        for ($i = 0; $i < count($data); $i++)
        {
            $insert_data = array(
                'Allocation_id' => $data[$i][0],
                'Attendee_id' => $data[$i][1],
                'Qty' => $data[$i][2]
            );
            $this->db->insert('allocation_attendee', $insert_data);
        }
        $update_allocation = array(
            'Approve' => '1',
        );
        $this->db->where('Id', $data[0][0]);
        $this->db->update('allocation', $update_allocation);
        for ($i = 0; $i < count($data); $i++)
        {
            $this->db->select('*');
            $this->db->from('event_qty a');
            $this->db->where('Id', $data[$i][3]);
            $event_qty_id = $this->db->get();
            $event_qty_id_res = $event_qty_id->result_array();
            $update_qty = array(
                'Qty' => $event_qty_id_res[0]['Qty'] - $data[$i][2]
            );
            $this->db->where('Id', $data[$i][3]);
            $this->db->update('event_qty', $update_qty);
        }
    }
    public function get_approved_order_details($id=NULL)

    {
        $this->db->select('a.*, ps.Size, p.Id as Event_id, case p.Event_name when NULL then p.Event_name End as event_name, case u.Firstname when NULL then u.Company_name Else u.Firstname End as client_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        $this->db->where('a.Id', $id);
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        $event = $this->Event_model->get_admin_event($order_query_res[0]['Event_id']);
        unset($event[0]['Size']);
        unset($event[0]['Category']);
        unset($event[0]['Attendee_Qty_Count']);
        $supllier = count($event[0]['Attendee']);
        for ($i = 0; $i < $supllier; $i++)
        {
            if ($event[0]['Attendee'][$i]['Id'] != $order_query_res[0]['Event_size_id']) unset($event[0]['Attendee'][$i]);
            else if ($event[0]['Attendee'][$i]['Month'] != date('M-Y', strtotime($order_query_res[0]['Month']))) unset($event[0]['Attendee'][$i]);
        }
        $event[0]['Attendee'] = array_values($event[0]['Attendee']);
        $client = $this->client_model->get_client_list($order_query_res[0]['Client_id']);
        $order_detail = array();
        $order_detail['Event'] = $event;
        $order_detail['Order'] = $order_query_res;
        $order_detail['Client'] = $client;
        return $order_detail;
    }
    public function get_attendee_order($id=NULL)

    {
        $this->db->select('ps.Size,asupp.Qty as Qty,case p.Event_name when null then p.Event_name end as event_name,a.Month', FALSE);
        $this->db->from('allocation_attendee asupp');
        $this->db->join('allocation a', 'a.Id = asupp.Allocation_id');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->where('asupp.Attendee_id', $id);
        $this->db->order_by('a.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    function cancel_order($id=NULL)
    {
        $this->db->where('Id', $id);
        $this->db->update('allocation', array(
            'Approve' => '2'
        ));
    }
    function approve_order($id=NULL)
    {
        $this->db->where('Id', $id);
        $this->db->update('allocation', array(
            'Client_approve' => '1'
        ));
    }
    function getallocationorderinfo($id=NULL)
    {
        $this->db->select('Event_size_id,Client_id,Qty,Allocated_qty,Approve,Client_approve', FALSE);
        $this->db->from('allocation');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_approved_or_cancelled_order($id = NULL)

    {
        $this->db->select('a.id as allocation_id,p.id,a.Approve,p.Event_name,case p.Event_name when NULL then p.Event_name End as event_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if (!empty($id)) $this->db->where('a.CLient_id', $id);
        else $this->db->where('a.CLient_id', $this->data['user']->Id);
        $this->db->where('(a.Approve="1"', NULL, FALSE);
        $this->db->or_where('a.Approve="2")', NULL, FALSE);
        $this->db->group_by('event_name');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        $resultdata = array();
        foreach($order_query_res as $key => $eventname)
        {
            $resultdata[$key]['event_id'] = $eventname['id'];
            $resultdata[$key]['Event_name'] = $eventname['Event_name'];
            $resultdata[$key]['Common_name'] = $eventname['Common_name'];
            $this->db->select('MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
            $this->db->from('allocation a');
            $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
            $this->db->join('event p', 'p.Id = ps.Event_id');
            $this->db->join('user u', 'u.Id = a.Client_id');
            if (!empty($id)) $this->db->where('a.CLient_id', $id);
            else $this->db->where('a.CLient_id', $this->data['user']->Id);
            $this->db->where('(a.Approve="1"', NULL, FALSE);
            $this->db->or_where('a.Approve="2")', NULL, FALSE);
            $this->db->where('p.id', $eventname['id']);
            $this->db->group_by('monthyr');
            $month_query = $this->db->get();
            $month_query_res = $month_query->result_array();
            $resultdata[$key]['Month'] = array();
            foreach($month_query_res as $mkey => $monthvalue)
            {
                $monthyear = date('M', strtotime($monthvalue['year'] . '-' . $monthvalue['month'] . '-01')) . "-" . $monthvalue['year'];
                $this->db->select('a.*,ps.Size,MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
                $this->db->from('allocation a');
                $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
                $this->db->join('event p', 'p.Id = ps.Event_id');
                $this->db->join('user u', 'u.Id = a.Client_id');
                if (!empty($id)) $this->db->where('a.Client_id', $id);
                else $this->db->where('a.Client_id', $this->data['user']->Id);
                $this->db->where('(a.Approve="1"', NULL, FALSE);
                $this->db->or_where('a.Approve="2")', NULL, FALSE);
                $this->db->where('p.id', $eventname['id']);
                $this->db->where('CONCAT(MONTH(a.Month),"-",YEAR(a.Month))', $monthvalue['monthyr']);
                $this->db->group_by('a.Event_size_id');
                $size_query = $this->db->get();
                $size_query_res = $size_query->result_array();
                $resultdata[$key]['Month'][$monthyear]['Size'] = array();
                foreach($size_query_res as $skey => $svalues)
                {
                    $this->db->select('a.*,a.id as allocation_id,p.id,a.Approve,ps.Size,MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
                    $this->db->from('allocation a');
                    $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
                    $this->db->join('event p', 'p.Id = ps.Event_id');
                    $this->db->join('user u', 'u.Id = a.Client_id');
                    if (!empty($id)) $this->db->where('a.Client_id', $id);
                    else $this->db->where('a.Client_id', $this->data['user']->Id);
                    $this->db->where('(a.Approve="1"', NULL, FALSE);
                    $this->db->or_where('a.Approve="2")', NULL, FALSE);
                    $this->db->where('p.id', $eventname['id']);
                    $this->db->where('ps.id', $svalues['Event_size_id']);
                    $this->db->where('CONCAT(MONTH(a.Month),"-",YEAR(a.Month))', $monthvalue['monthyr']);
                    $allocation_query = $this->db->get();
                    $allocation_query_res = $allocation_query->result_array();
                    foreach($allocation_query_res as $allkey => $allvalue)
                    {
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Qty'] = $allvalue['Qty'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Allocated_qty'] = $allvalue['Allocated_qty'] = NULL ? NULL : $svalues['Allocated_qty'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Approve'] = $allvalue['Approve'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['allocation_id'] = $allvalue['allocation_id'];
                    }
                }
            }
        }
        return $resultdata;
    }
    public function get_attendeeevent_allocation_info($id = null)

    {
        $this->db->select('*');
        $this->db->from('event p');
        $query = $this->db->get();
        $res = $query->result();
        $event_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $event_res[$i]['Id'] = $res[$i]->Id;
            $event_res[$i]['Event_name'] = $res[$i]->Event_name;
            $event_res[$i]['Common_name'] = $res[$i]->Common_name;
            $event_res[$i]['Description'] = $res[$i]->Description;
            $event_res[$i]['Images'] = $res[$i]->Images;
            $event_res[$i]['Created_date'] = $res[$i]->Created_date;
            $event_res[$i]['Month'] = array();
            $this->db->protect_identifiers = false;
            $this->db->select('pq.Attendee_id,pq.Event_id,pq.Size_id,pq.Qty,sum(pq.Qty),MONTH(pq.Month) as month,YEAR(pq.Month) as year,CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month)) as monthyr', FALSE);
            $this->db->from('event_qty pq');
            $this->db->where('pq.event_id', $res[$i]->Id);
            $this->db->group_by('CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month))');
            $this->db->order_by('monthyr');
            $month_query = $this->db->get();
            $month_res = $month_query->result_array();
            $totalmonthremainqty = 0;
            $totaleventremainqty = 0;
            foreach($month_res as $key => $value)
            {
                $monthyear = date('M', strtotime($value['year'] . '-' . $value['month'] . '-01')) . "-" . $value['year'];
                $event_res[$i]['Month'][$monthyear]['Size'] = array();
                $this->db->protect_identifiers = false;
                $this->db->select('ps.*,CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month)) as monthyr', FALSE);
                $this->db->from('event_qty pq');
                $this->db->join('event_size ps', 'ps.id=pq.Size_id', 'left');
                $this->db->where('pq.event_id', $res[$i]->Id);
                $this->db->where('CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month))', $value['monthyr']);
                $this->db->group_by('pq.Size_id');
                $size_query = $this->db->get();
                $size_res = $size_query->result_array();
                $totalqty = 0;
                $totalallocatedqty = 0;
                $totalremainqty = 0;
                $totalmonthremainqty = 0;
                foreach($size_res as $skey => $svalues)
                {
                    $this->db->protect_identifiers = false;
                    $this->db->select('pq.Attendee_id,case u.Firstname when NULL then u.Company_name Else u.Firstname End as attendeename,sum(pq.Qty) as remainqty,CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month)) as monthyr', FALSE);
                    $this->db->from('event_qty pq');
                    $this->db->join('event_size ps', 'ps.id=pq.Size_id', 'left');
                    $this->db->join('user u', 'u.id=pq.Attendee_id', 'left');
                    $this->db->where('pq.event_id', $res[$i]->Id);
                    $this->db->where('pq.Attendee_id', $id);
                    $this->db->where('CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month))', $value['monthyr']);
                    $this->db->where('pq.Size_id', $svalues['Id']);
                    $this->db->group_by('pq.Attendee_id');
                    $attendee_query = $this->db->get();
                    $attendee_res = $attendee_query->result_array();
                    $this->db->protect_identifiers = false;
                    $this->db->select('CASE WHEN a.Approve = "1" and a.Client_approve=1 THEN a.Allocated_qty WHEN a.Approve = "1" THEN a.Qty else 0 END AS "AllocatedQty"', FALSE);
                    $this->db->from('event_qty pq');
                    $this->db->join('event_size ps', 'ps.id=pq.Size_id', 'left');
                    $this->db->join('user u', 'u.id=pq.Attendee_id', 'left');
                    $this->db->join('allocation a', 'a.Event_size_id=pq.Size_id', 'left');
                    $this->db->where('pq.event_id', $res[$i]->Id);
                    if ($this->data['user']->Role_name == 'Attendee')
                    {
                        $this->db->where('pq.Attendee_id', $this->data['user']->Id);
                    }
                    $this->db->where('CONCAT(MONTH(pq.Month),"-",YEAR(pq.Month))', $value['monthyr']);
                    $this->db->where('pq.Size_id', $svalues['Id']);
                    $this->db->where('a.Approve', "1");
                    $this->db->group_by('pq.Attendee_id');
                    $attendeeallocated_query = $this->db->get();
                    $attendeeallocated_res = $attendeeallocated_query->result_array();
                    $totalallocatedqty = 0;
                    $totalqty = 0;
                    $totalremainqty = 0;
                    foreach($attendee_res as $supkey => $supvalues)
                    {
                        $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']][$supkey]['AttendeeId'] = $supvalues['Attendee_id'];
                        $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']][$supkey]['AttendeeName'] = $supvalues['attendeename'];
                        $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']][$supkey]['Remainqty'] = $supvalues['remainqty'];
                        $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']][$supkey]['Allocatedqty'] = $attendeeallocated_res[$supkey]['AllocatedQty'];
                        $totalallocatedqty = $totalallocatedqty + $attendeeallocated_res[$supkey]['AllocatedQty'];
                        $totalqty = $totalqty + $supvalues['remainqty'] + $attendeeallocated_res[$supkey]['AllocatedQty'];
                        $totalremainqty = $totalremainqty + $supvalues['remainqty'];
                    }
                    $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']]['TotalAllocatedQty'] = $totalallocatedqty;
                    $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']]['TotalQty'] = $totalqty;
                    $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']]['TotalRemainQty'] = $totalremainqty;
                    $event_res[$i]['Month'][$monthyear]['Size'][$svalues['Size']]['Plnt_size_id'] = $svalues['Id'];
                    $totalmonthremainqty = $totalmonthremainqty + $totalremainqty;
                }
                $totaleventremainqty = $totaleventremainqty + $totalmonthremainqty;
                $event_res[$i]['Month'][$monthyear]['MonthTotalRemaintQty'] = $totalmonthremainqty;
            }
            $event_res[$i]['EventRemaintQty'] = $totaleventremainqty;
        }
        return $event_res;
    }
    public function get_client_approved_or_cancelled_order($id = NULL)

    {
        $this->db->select('a.id as allocation_id,p.id,a.Approve,p.Event_name,case p.Event_name when NULL then p.Event_name End as event_name', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if (!empty($id)) $this->db->where('a.Client_id', $id);
        else $this->db->where('a.Client_id', $this->data['user']->Id);
        $this->db->where('(a.Approve="1"', NULL, FALSE);
        $this->db->or_where('a.Approve="2")', NULL, FALSE);
        $this->db->group_by('event_name');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        $resultdata = array();
        foreach($order_query_res as $key => $eventname)
        {
            $resultdata[$key]['event_id'] = $eventname['id'];
            $resultdata[$key]['Event_name'] = $eventname['Event_name'];
            $this->db->select('MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
            $this->db->from('allocation a');
            $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
            $this->db->join('event p', 'p.Id = ps.Event_id');
            $this->db->join('user u', 'u.Id = a.Client_id');
            if (!empty($id)) $this->db->where('a.Client_id', $id);
            else $this->db->where('a.Client_id', $this->data['user']->Id);
            $this->db->where('(a.Approve="1"', NULL, FALSE);
            $this->db->or_where('a.Approve="2")', NULL, FALSE);
            $this->db->where('p.id', $eventname['id']);
            $this->db->group_by('monthyr');
            $month_query = $this->db->get();
            $month_query_res = $month_query->result_array();
            $resultdata[$key]['Month'] = array();
            foreach($month_query_res as $mkey => $monthvalue)
            {
                $monthyear = date('M', strtotime($monthvalue['year'] . '-' . $monthvalue['month'] . '-01')) . "-" . $monthvalue['year'];
                $this->db->select('a.*,ps.Size,MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
                $this->db->from('allocation a');
                $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
                $this->db->join('event p', 'p.Id = ps.Event_id');
                $this->db->join('user u', 'u.Id = a.Client_id');
                if (!empty($id)) $this->db->where('a.Client_id', $id);
                else $this->db->where('a.Client_id', $this->data['user']->Id);
                $this->db->where('(a.Approve="1"', NULL, FALSE);
                $this->db->or_where('a.Approve="2")', NULL, FALSE);
                $this->db->where('p.id', $eventname['id']);
                $this->db->where('CONCAT(MONTH(a.Month),"-",YEAR(a.Month))', $monthvalue['monthyr']);
                $size_query = $this->db->get();
                $size_query_res = $size_query->result_array();
                $resultdata[$key]['Month'][$monthyear]['Size'] = array();
                foreach($size_query_res as $skey => $svalues)
                {
                    $this->db->select('a.*,a.id as allocation_id,p.id,a.Approve,ps.Size,MONTH(a.Month) as month,YEAR(a.Month) as year,CONCAT(MONTH(a.Month),"-",YEAR(a.Month)) as monthyr', FALSE);
                    $this->db->from('allocation a');
                    $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
                    $this->db->join('event p', 'p.Id = ps.Event_id');
                    $this->db->join('user u', 'u.Id = a.Client_id');
                    if (!empty($id)) $this->db->where('a.Client_id', $id);
                    else $this->db->where('a.Client_id', $this->data['user']->Id);
                    $this->db->where('(a.Approve="1"', NULL, FALSE);
                    $this->db->or_where('a.Approve="2")', NULL, FALSE);
                    $this->db->where('p.id', $eventname['id']);
                    $this->db->where('ps.id', $svalues['Event_size_id']);
                    $this->db->where('CONCAT(MONTH(a.Month),"-",YEAR(a.Month))', $monthvalue['monthyr']);
                    $allocation_query = $this->db->get();
                    $allocation_query_res = $allocation_query->result_array();
                    foreach($allocation_query_res as $allkey => $allvalue)
                    {
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Qty'] = $allvalue['Qty'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Allocated_qty'] = $allvalue['Allocated_qty'] = NULL ? NULL : $svalues['Allocated_qty'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['Approve'] = $allvalue['Approve'];
                        $resultdata[$key]['Month'][$monthyear]['Size'][$svalues['Size']][$allkey]['allocation_id'] = $allvalue['allocation_id'];
                    }
                }
            }
        }
        return $resultdata;
    }
    public function get_monst_common_order_approved_or_cancelled($id = NULL)

    {
        $this->db->select('p.id,case p.Event_name when NULL then p.Event_name End as event_name,count(p.id) as eventcount', FALSE);
        $this->db->from('allocation a');
        $this->db->join('event_size ps', 'ps.Id = a.Event_size_id');
        $this->db->join('event p', 'p.Id = ps.Event_id');
        $this->db->join('user u', 'u.Id = a.Client_id');
        if (!empty($id)) $this->db->where('a.Client_id', $id);
        else $this->db->where('a.Client_id', $this->data['user']->Id);
        $this->db->where('(a.Approve="1"', NULL, FALSE);
        $this->db->or_where('a.Approve="2")', NULL, FALSE);
        $this->db->group_by('p.id');
        $this->db->order_by('eventcount desc');
        $order_query = $this->db->get();
        $order_query_res = $order_query->result_array();
        return $order_query_res;
    }
    public function checkordercount()

    {
        $this->db->select('count(a.id) as openorder', FALSE);
        $this->db->from('allocation a');
        $this->db->where('a.Approve', '0');
        $openorder_query = $this->db->get();
        $openorder_query_res = $openorder_query->result_array();
        $this->db->select('count(a.id) as completeorder', FALSE);
        $this->db->from('allocation a');
        $this->db->where('a.Approve', '1');
        $completeorder_query = $this->db->get();
        $completeorder_query_res = $completeorder_query->result_array();
        $this->db->protect_identifiers = false;
        $this->db->select('count(a.id) as pendingorder', FALSE);
        $this->db->from('allocation a');
        $this->db->where('a.Approve', '0');
        $this->db->where('(a.Client_approve="0"', NULL, FALSE);
        $this->db->or_where('a.Client_approve="1")', NULL, FALSE);
        $pendingorder_query = $this->db->get();
        $pendingorder_query_res = $pendingorder_query->result_array();
        $arraydata = array();
        $arraydata['openorder'] = $openorder_query_res[0]['openorder'];
        $arraydata['completeorder'] = $completeorder_query_res[0]['completeorder'];
        $arraydata['pendingorder'] = $pendingorder_query_res[0]['pendingorder'];
        $this->db->select('count(a.id) as totalorder', FALSE);
        $this->db->from('allocation a');
        $total_query = $this->db->get();
        $totalorder_res = $total_query->result_array();
        $arraydata['openorderpercent'] = ceil($arraydata['openorder'] * 100 / $totalorder_res[0]['totalorder']);
        $arraydata['completeorderpercent'] = ceil($arraydata['completeorder'] * 100 / $totalorder_res[0]['totalorder']);
        $arraydata['pendingorderpercent'] = ceil($arraydata['pendingorder'] * 100 / $totalorder_res[0]['totalorder']);
        return $arraydata;
    }
}
?>
