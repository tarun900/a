<?php
class Matchmaking_model extends CI_Model

{
	public function __construct()

	{
		parent::__construct();
	}
	public function getModuleName($event_id=NULL)

	{
		$menu_id = array(2,3,7,43,1);
		$this->db->select('m.id,CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname', FALSE);
		$this->db->from('menu m');
		$this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id=' . $event_id . '', 'left');
		$this->db->join('event e', 'e.Id=em.event_id', 'left');
		$this->db->where_in('m.id', $menu_id);
		$res = $this->db->get()->result_array();
		return $res;
	}
	public function getModuleSetting($event_id=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->get('matchmaking_modules')->row_array();
		$data['menuid_2'] = $res['attendee'];
		$data['menuid_3'] = $res['exhibitor'];
		$data['menuid_7'] = $res['speaker'];
		$data['menuid_43'] = $res['sponsor'];
		$data['menuid_1'] = $res['agenda'];
		return $data;
	}
	public function saveModuleSetting($data=NULL, $event_id=NULL)

	{
		$this->db->where('event_id', $event_id);
		$res = $this->db->get('matchmaking_modules')->row_array();
		if (!empty($res))
		{
			$this->db->where('event_id', $event_id);
			$this->db->update('matchmaking_modules', $data);
		}
		else
		{
			$this->db->insert('matchmaking_modules', $data);
		}
	}
	public function addRule($data=NULL)

	{
		$this->db->insert('matchmaking_rules', $data);
	}
	public function getRule($event_id=NULL)

	{
		$this->db->where('event_id', $event_id);
		$res = $this->db->get('matchmaking_rules')->result_array();
		return $res;
	}
	public function deleteRule($id=NULL)

	{
		$this->db->where('id', $id)->delete('matchmaking_rules');
	}
	public function editRule($id=NULL)

	{
		$res = $this->db->where('id', $id)->get('matchmaking_rules')->row_array();
		return $res;
	}
	public function updateRule($id=NULL, $data=NULL)

	{
		$this->db->where('id', $id);
		$this->db->update('matchmaking_rules', $data);
	}
	public function getCustomColumn($id=NULL)

	{
		$this->db->where('event_id', $id);
		$res = $this->db->get('custom_column')->result_array();
		return $res;
	}
	public function getAgenda($event_id)
	{
		$this->db->where('event_id', $event_id);
		$agenda = $this->db->get('agenda_categories')->result_array();
		return $agenda;
	}
}