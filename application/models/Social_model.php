<?php
class Social_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_social_list($id = null, $sid = null)

    {
        $this->db->select('*');
        $this->db->from('social');
        if ($sid != "")
        {
            $this->db->where('Id', $sid);
        }
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'edit') redirect('Forbidden');
        return $res;
    }
    public function add_social($data=NULL)

    {
        if ($data['social_array']['Organisor_id'] != NULL) $array_social['Organisor_id'] = $data['social_array']['Organisor_id'];
        if ($data['social_array']['Event_id'] != NULL) $array_social['Event_id'] = $data['social_array']['Event_id'];
        if ($data['social_array']['facebook_url'] != NULL) $array_social['facebook_url'] = $data['social_array']['facebook_url'];
        if ($data['social_array']['twitter_url'] != NULL) $array_social['twitter_url'] = $data['social_array']['twitter_url'];
        if ($data['social_array']['instagram_url'] != NULL) $array_social['instagram_url'] = $data['social_array']['instagram_url'];
        if ($data['social_array']['linkedin_url'] != NULL) $array_social['linkedin_url'] = $data['social_array']['linkedin_url'];
        if ($data['social_array']['pinterest_url'] != NULL) $array_social['pinterest_url'] = $data['social_array']['pinterest_url'];
        if ($data['social_array']['youtube_url'] != NULL) $array_social['youtube_url'] = $data['social_array']['youtube_url'];
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $user_soc['Event_id'] = $data['social_array']['Event_id'];
        $user_soc['User_id'] = $logged_in_user_id;
        $user_soc['Website_url'] = $data['social_array']['instagram_url'];
        $user_soc['Facebook_url'] = $data['social_array']['facebook_url'];
        $user_soc['Linkedin_url'] = $data['social_array']['linkedin_url'];
        $user_soc['Twitter_url'] = $data['social_array']['twitter_url'];
        $this->db->insert('social', $array_social);
        $social_id = $this->db->insert_id();
        return $social_id;
    }
    public function update_social($data=NULL)

    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        if ($data['social_array']['facebook_url'] != NULL) $array_social['facebook_url'] = $data['social_array']['facebook_url'];
        if ($data['social_array']['twitter_url'] != NULL) $array_social['twitter_url'] = $data['social_array']['twitter_url'];
        if ($data['social_array']['instagram_url'] != NULL) $array_social['instagram_url'] = $data['social_array']['instagram_url'];
        if ($data['social_array']['linkedin_url'] != NULL) $array_social['linkedin_url'] = $data['social_array']['linkedin_url'];
        if ($data['social_array']['pinterest_url'] != NULL) $array_social['pinterest_url'] = $data['social_array']['pinterest_url'];
        if ($data['social_array']['youtube_url'] != NULL) $array_social['youtube_url'] = $data['social_array']['youtube_url'];
        $array_social['Event_id'] = $this->uri->segment(3);
        $array_social['Organisor_id'] = $orid;
        $this->db->where('Id', $this->uri->segment(4));
        $this->db->update('social', $array_social);
    }
    public function delete_social($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('social');
        $str = $this->db->last_query();
    }
    public function add_social_links($data=NULL)

    {
        if ($data['social_array']['User_id'] != NULL) $array_social['User_id'] = $data['social_array']['User_id'];
        if ($data['social_array']['Event_id'] != NULL) $array_social['Event_id'] = $data['social_array']['Event_id'];
        if ($data['social_array']['website_url'] != NULL) $array_social['website_url'] = $data['social_array']['website_url'];
        if ($data['social_array']['facebook_url'] != NULL) $array_social['facebook_url'] = $data['social_array']['facebook_url'];
        if ($data['social_array']['twitter_url'] != NULL) $array_social['twitter_url'] = $data['social_array']['twitter_url'];
        if ($data['social_array']['linkedin_url'] != NULL) $array_social['linkedin_url'] = $data['social_array']['linkedin_url'];
        $this->db->insert('user_social_links', $array_social);
        $social_id = $this->db->insert_id();
        return $social_id;
    }
    public function update_social_links($data=NULL)

    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        if ($data['social_array']['website_url'] != NULL) $array_social['website_url'] = $data['social_array']['website_url'];
        if ($data['social_array']['facebook_url'] != NULL) $array_social['facebook_url'] = $data['social_array']['facebook_url'];
        if ($data['social_array']['twitter_url'] != NULL) $array_social['twitter_url'] = $data['social_array']['twitter_url'];
        if ($data['social_array']['linkedin_url'] != NULL) $array_social['linkedin_url'] = $data['social_array']['linkedin_url'];
        $array_social['User_id'] = $orid;
        $this->db->where('Id', $this->uri->segment(3));
        $this->db->update('user_social_links', $array_social);
    }
    public function delete_social_links($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('user_social_links');
        $str = $this->db->last_query();
    }
    public function get_event_id()

    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('*');
        $this->db->from('user');
        if ($orid)
        {
            $this->db->where('Id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_social_links_list()

    {
        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Event_id');
        $this->db->from('relation_event_user');
        $this->db->where('Id', $orid);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*');
        $this->db->from('user_social_links');
        if ($orid)
        {
            $this->db->where('User_id', $orid);
        }
        $this->db->where('Event_id', $res[0]['Event_id']);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        return $res1;
    }
}
?>