<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class appnotitemplate_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function getAppTemplate($intEventId = null, $intTempId = null)

    {
        $this->db->select('*');
        $this->db->from('app_notification_template');
        $this->db->where('event_id', $intEventId);
        if (!empty($intTempId))
        {
            $this->db->where('id', $intTempId);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getAppPushTemplate($intEventId = null, $intTempId = NUll)

    {
        $this->db->select('*');
        $this->db->from('event_app_push_template');
        $this->db->where('event_id', $intEventId);
        if (!empty($intTempId))
        {
            $this->db->where('id', $intTempId);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        $res = $query->result_array();
        if (!$res) return redirect('Forbidden');
        return $res;
    }
    public function update_template($arrUpdate = array())
    {
        $arrUpdate['Subject'] = $this->input->post('Subject');
        $arrUpdate['From'] = $this->input->post('From');
        $arrUpdate['Content'] = $this->input->post('Content');
        $this->db->where('Id', $arrUpdate['id']);
        $this->db->where('event_id', $arrUpdate['event_id']);
        $this->db->update('app_notification_template', $arrUpdate);
        return true;
    }
    public function update_pushtemplate($arrUpdate = array())
    {
        $arrUpdate['Content'] = $this->input->post('Content');
        $this->db->where('Id', $arrUpdate['id']);
        $this->db->where('event_id', $arrUpdate['event_id']);
        $this->db->update('event_app_push_template', $arrUpdate);
        return true;
    }
    public function active_email_notify($arr=NULL, $event_id=NULL, $nid=NULL)

    {
        $this->db->where('Id', $nid);
        $this->db->where('event_id', $event_id);
        $this->db->update('event_app_push_template', $arr);
    }
}
?>