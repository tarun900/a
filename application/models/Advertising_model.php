<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Advertising_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_login_role($roleid=NULL)

    {
        $qry = $this->db->query("select * from menu where id in (select Menu_id from role_permission where Role_id=$roleid)");
        $res = $qry->result_array();
        return $res;
    }
    public function add_advertising($data=NULL)

    {
        if (!empty($data['advertising_array']['H_images']))
        {
            $H_images[] = $data['advertising_array']['H_images'];
            $array_advertising['H_images'] = json_encode($H_images);
        }
        if (!empty($data['advertising_array']['F_images']))
        {
            $F_images[] = $data['advertising_array']['F_images'];
            $array_advertising['F_images'] = json_encode($F_images);
        }
        if ($data['advertising_array']['Advertisement_name'] != NULL) $array_advertising['Advertisement_name'] = $data['advertising_array']['Advertisement_name'];
        if ($data['advertising_array']['Menu_id'] != NULL) $array_advertising['Menu_id'] = $data['advertising_array']['Menu_id'];
        if ($data['advertising_array']['Cms_id'] != NULL) $array_advertising['Cms_id'] = $data['advertising_array']['Cms_id'];
        if ($data['advertising_array']['Google_header_adsense'] != NULL) $array_advertising['Google_header_adsense'] = $data['advertising_array']['Google_header_adsense'];
        if ($data['advertising_array']['Google_footer_adsense'] != NULL) $array_advertising['Google_footer_adsense'] = $data['advertising_array']['Google_footer_adsense'];
        if ($data['advertising_array']['Footer_link'] != NULL) $array_advertising['Footer_link'] = $data['advertising_array']['Footer_link'];
        if ($data['advertising_array']['Header_link'] != NULL) $array_advertising['Header_link'] = $data['advertising_array']['Header_link'];
        if ($data['advertising_array']['Organisor_id'] != NULL) $array_advertising['Organisor_id'] = $data['advertising_array']['Organisor_id'];
        if ($data['advertising_array']['Event_id'] != NULL) $array_advertising['Event_id'] = $data['advertising_array']['Event_id'];
        if ($data['advertising_array']['show_all'] != NULL) $array_advertising['show_all'] = $data['advertising_array']['show_all'];
        if ($data['advertising_array']['show_sticky'] != NULL) $array_advertising['show_sticky'] = $data['advertising_array']['show_sticky'];
        $this->db->insert('advertising', $array_advertising);
        $advertising_id = $this->db->insert_id();
        return $advertising_id;
    }
    public function get_advertising_list($id = null)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_advertising_list_by_id($id = null, $aid = null)

    {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $this->db->select('*');
        $this->db->from('advertising');
        $this->db->where('Event_id', $id);
        $this->db->where('Id', $aid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res) redirect('Forbidden');
        return $res;
    }
    public function update_advertising($data=NULL)

    {
        if ($data['advertising_array']['Advertisement_name'] != NULL) $array_advertising['Advertisement_name'] = $data['advertising_array']['Advertisement_name'];
        if ($data['advertising_array']['Menu_id'] != NULL) $array_advertising['Menu_id'] = $data['advertising_array']['Menu_id'];
        if (!empty($data['advertising_array']['Cms_id']))
        {
            $array_advertising['Cms_id'] = $data['advertising_array']['Cms_id'];
        }
        else
        {
            $array_advertising['Cms_id'] = NULL;
        }
        $array_advertising['H_images'] = $data['advertising_array']['H_images'];
        $array_advertising['F_images'] = $data['advertising_array']['F_images'];
        $array_advertising['Google_header_adsense'] = $data['advertising_array']['Google_header_adsense'];
        $array_advertising['Google_footer_adsense'] = $data['advertising_array']['Google_footer_adsense'];
        $array_advertising['Header_link'] = $data['advertising_array']['Header_link'];
        $array_advertising['Footer_link'] = $data['advertising_array']['Footer_link'];
        $array_advertising['show_sticky'] = $data['advertising_array']['show_sticky'];
        $array_advertising['show_all'] = $data['advertising_array']['show_all'];
        $this->db->where('Id', $data['Id']);
        $this->db->update('advertising', $array_advertising);
        // echo $this->db->last_query(); exit;
    }
    public function checkmenusection($subdomain = null, $id = null)

    {
        $this->db->select('a.Menu_id as menuid ');
        $this->db->from('advertising a');
        $this->db->where_in('a.Menu_id', $subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function checkcmssection($subdomain = null, $id = null)

    {
        $this->db->select('a.Cms_id as cmsid');
        $this->db->from('advertising a');
        $this->db->where_in('a.Cms_id', $subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function delete_advertising($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('advertising');
        $str = $this->db->last_query();
    }
    public function get_menu_name($id=NULL)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('menu_Setting');
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function saveSettings($update=NULL, $where=NULL)

    {
        $this->db->where($where);
        $this->db->update('event', $update);
        /*$this->db->where($where);
        $this->db->update('advertising',$update);*/
    }
}
?>