<?php
class Notifications_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
        $this->db1 = $this->load->database('db1', TRUE);
        $this->load->library('email');
    }
    public function get_notifications_list($id = null, $userid = null, $limit = 'All', $sarchtext = null)

    {
        if ($id != NULL && $userid != NULL)
        {
            $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,Date_Format(n.Created_date,"%d %b %Y, %r") as Created_date', FALSE);
        }
        else
        {
            $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,n.Created_date,nu.Read_unread,nu.Seen_unseen', FALSE);
        }
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        $this->db->join('user u', 'u.id =nu.User_id', 'Left');
        $this->db->join('user uc', 'uc.id =n.Created_by', 'Left');
        if ($id)
        {
            $this->db->where('nu.Notification_id', $id);
        }
        if ($userid)
        {
            $this->db->where('nu.User_id', $userid);
            $this->db->where('nu.Softdelete', '0');
        }
        if ($sarchtext != null)
        {
            $this->db->like('(case u.Firstname when NULL then u.Company_name Else u.Firstname End)', $sarchtext);
            $this->db->or_like('n.Summary', $sarchtext);
        }
        // $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');
        if ($limit != 'All')
        {
            $this->db->limit($limit, 0);
        }
        $query = $this->db->get();
        if ($id != NULL && $userid != NULL)
        {
            $res = $query->first_row();
        }
        else
        {
            $res = $query->result_array();
        }
        return $res;
    }
    public function get_private_msg_notification($receiver_id=NULL, $eid=NULL, $acc_name=NULL, $Subdomain=NULL, $email=NULL, $email_display=NULL)

    {
        $this->db->select("sm.Message,sm.Id as msg_id,u.Id,concat(u.Firstname,' ',u.Lastname) as Name", FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.sender_id');
        $this->db->where('sm.Receiver_id', $receiver_id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sm.is_send', '0');
        $query = $this->db->get();
        $res = $query->result_array();
        $update_data['is_send'] = '1';
        $arr = array();
        foreach($res as $key => $value)
        {
            $this->db->where('Id', $value['msg_id']);
            $this->db->update("speaker_msg", $update_data);
            $arr[] = $value;
        }
        $this->db->select("sm.Id as msg_id,u.Id,concat(u.Firstname,' ',u.Lastname) as Name,sc.comment as Message,sc.*,sc.id as com_id", FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.receiver_id');
        $this->db->join('speaker_comment sc', 'sc.msg_id=sm.Id');
        $this->db->where('sm.sender_id', $receiver_id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sc.is_send', '0');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $update_data1['is_send'] = '1';
        foreach($res1 as $key => $value)
        {
            $this->db->where('id', $value['com_id']);
            $this->db->update("speaker_comment", $update_data1);
            $arr[] = $value;
        }
        if (count($arr) > 0)
        {
            $this->db->select('*')->from('event_app_push_template');
            $this->db->where('event_id', $eid);
            $this->db->where('Slug', 'Message');
            $noti = $this->db->get()->result_array();
            $url = base_url() . 'Messages/' . $acc_name . '/' . $Subdomain . '/privatemsg';
            $arr1[0]['title'] = $noti[0]['Slug'];
            $arr1[0]['Message'] = "<a href='" . $url . "'>" . $noti[0]['Content'] . "</a>";
            if ($noti[0]['send_email'] == '1' && $email_display == '1')
            {
                $sub = !empty($noti[0]['email_subject']) ? $noti[0]['email_subject'] : $noti[0]['Slug'];
                $msg = !empty(strip_tags($noti[0]['email_content'])) ? $noti[0]['email_content'] : $noti[0]['Content'];
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com', 'All In The Loop');
                $this->email->to($email);
                $this->email->subject($sub);
                $this->email->message(html_entity_decode($msg));
                $this->email->send();
            }
        }
        return $arr1;
    }
    public function updatemsgtoread($notifiyid=NULL, $userid=NULL)

    {
        $category = array(
            'Read_unread' => 'read'
        );
        $this->db->where('Notification_id', $notifiyid);
        $this->db->where('User_id', $userid);
        $data = $this->db->update('notifications_users', $category);
        return $data;
    }
    public function userdata($rolename=NULL)

    {
        $this->db->select('u.id,case u.Firstname when NULL then u.Company_name Else u.Firstname End as name ', FALSE);
        $this->db->from('role r');
        $this->db->join('user u', 'u.role_id=r.id', 'Left');
        $this->db->where('r.Name', $rolename);
        $this->db->where('u.Active', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_notification($data=NULL, $id = null)

    {
        if ($id != NULL)
        {
            $this->db->where('Id', $id);
            $this->db->update('notifications', $data); // update the record
        }
        else
        {
            $this->db->insert('notifications', $data);
        }
        return $this->db->insert_id();
    }
    public function add_usernotification($data=NULL, $notificationid = null, $userid = null)

    {
        $id = $this->db->insert('notifications_users', $data);
        return $id;
    }
    public function deletenotification($id=NULL, $userid=NULL, $flag = 0)

    {
        $this->db->where('Notification_id', $id);
        $this->db->where('User_id', $userid);
        $this->db->delete('notifications_users');
        $res = $this->getcurrentuserwithnotification($id);
        if (empty($res) && $flag == 0)
        {
            $this->db->where('Id', $id);
            $this->db->delete('notifications');
        }
        return true;
    }
    public function get_notifications($id = null)

    {
        $this->db->select('n.*,n.Id as Notification_id,nu.*,nu.Id as notification_user_id ', FALSE);
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        if ($id)
        {
            $this->db->where('nu.Notification_id', $id);
        }
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        $res = $query->result_array();
        $finalarr = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $finalarr['Id'] = $res[$i]['Notification_id'];
            $finalarr['Title'] = $res[$i]['Title'];
            $finalarr['Summary'] = $res[$i]['Summary'];
            $finalarr['Content'] = $res[$i]['Content'];
            $finalarr['Notification_by'] = $res[$i]['Notification_by'];
            $finalarr['Notification_by'] = $res[$i]['Notification_by'];
            $finalarr['Schedule'] = $res[$i]['Schedule'];
            $finalarr['Schedual_date'] = $res[$i]['Schedual_date'];
            $finalarr['Schedule_for'] = $res[$i]['Schedule_for'];
            $finalarr['User_type'] = $res[$i]['User_type'];
            $finalarr['CheckdUSer'][$i] = $res[$i]['User_id'];
        }
        return $finalarr;
    }
    public function getcurrentuserwithnotification($id=NULL)

    {
        $this->db->select('User_id', FALSE);
        $this->db->from('notifications_users');
        $this->db->where('Notification_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function getcountandseencount($id = null, $userid = null)

    {
        $this->db->select('count(n.id) as msg_unseencount', FALSE);
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        if ($id)
        {
            $this->db->where('nu.Notification_id', $id);
        }
        if ($userid)
        {
            $this->db->where('nu.User_id', $userid);
        }
        $this->db->where('nu.Seen_unseen', 'unseen');
        $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        $res = $query->first_row();
        $this->db->select('count(n.id) as msg_unreadcount', FALSE);
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        if ($id)
        {
            $this->db->where('nu.Notification_id', $id);
        }
        if ($userid)
        {
            $this->db->where('nu.User_id', $userid);
        }
        $this->db->where('nu.Read_unread', 'unread');
        $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        $res1 = $query->first_row();
        $arrcount = array();
        $arrcount['msg_unseencount'] = $res->msg_unseencount;
        $arrcount['msg_unreadcount'] = $res1->msg_unreadcount;
        return $arrcount;
    }
    public function timeAgo($ts=NULL)

    {
        if (!ctype_digit($ts)) $ts = strtotime($ts);
        $diff = time() - $ts;
        if ($diff == 0) return 'now';
        elseif ($diff > 0)
        {
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0)
            {
                if ($diff < 60) return 'just now';
                if ($diff < 120) return '1 min';
                if ($diff < 3600) return floor($diff / 60) . ' mins';
                if ($diff < 7200) return '1 hr';
                if ($diff < 86400) return floor($diff / 3600) . ' hrs';
            }
            if ($day_diff == 1) return 'Yesterday';
            if ($day_diff < 7) return $day_diff . ' ds';
            if ($day_diff < 31) return ceil($day_diff / 7) . ' ws';
            if ($day_diff < 60) return 'last month';
            return date('F Y', $ts);
        }
        else
        {
            $diff = abs($diff);
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0)
            {
                if ($diff < 120) return 'in a minute';
                if ($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
                if ($diff < 7200) return 'in an hour';
                if ($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
            }
            if ($day_diff == 1) return 'Tomorrow';
            if ($day_diff < 4) return date('l', $ts);
            if ($day_diff < 7 + (7 - date('w'))) return 'next week';
            if (ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
            if (date('n', $ts) == date('n') + 1) return 'next month';
            return date('F Y', $ts);
        }
    }
    public function unseenmsg($userid=NULL)

    {
        $data['Seen_unseen'] = "seen";
        $this->db->where('User_id', $userid);
        $this->db->update('notifications_users', $data); // update the record
        return true;
    }
    public function softdeletenotification($id=NULL, $userid=NULL)

    {
        $data['Softdelete'] = "1";
        $this->db->where('User_id', $userid);
        $this->db->where('Notification_id', $id);
        $this->db->update('notifications_users', $data); // update the record
        return true;
    }
    public function get_notifications_searchlist($id = null, $userid = null, $sarchtext = null)

    {
        $this->db->select('n.id,n.Title,n.Summary,n.content,n.Notification_by,n.Schedule,case uc.Firstname when NULL then uc.Company_name Else uc.Firstname End as Fromname,case u.Firstname when NULL then u.Company_name Else u.Firstname End as Toname,u.Logo,u.id as Userid,u.Email as Toemail,uc.Email as Fromemail,n.Created_date,nu.Read_unread,nu.Seen_unseen', FALSE);
        $this->db->from('notifications n');
        $this->db->join('notifications_users nu', 'nu.Notification_id=n.Id', 'Left');
        $this->db->join('user u', 'u.id =nu.User_id', 'Left');
        $this->db->join('user uc', 'uc.id =n.Created_by', 'Left');
        if ($id)
        {
            $this->db->where('nu.Notification_id', $id);
        }
        if ($userid)
        {
            $this->db->where('nu.User_id', $userid);
            $this->db->where('nu.Softdelete', '0');
        }
        if ($sarchtext != null)
        {
            $this->db->where('((case u.Firstname when NULL then u.Company_name Else u.Firstname End) like "%' . $sarchtext . '%" or n.Summary like "%' . $sarchtext . '%")');
        }
        // $this->db->where('n.Schedule', '0');
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        if ($id != NULL && $userid != NULL)
        {
            $res = $query->first_row();
        }
        else
        {
            $res = $query->result_array();
        }
        foreach($res as $key => $value)
        {
            if ($key == "Created_date")
            {
                if (date('Y-m-d') == date('Y-m-d', strtotime($value['Created_date'])))
                {
                    $res[$key]['Created_date'] = date('H:s A', strtotime($value['Created_date']));
                }
                else
                {
                    $res[$key]['Created_date'] = date('d M y', strtotime($value['Created_date']));
                }
            }
            if ($key == "Title")
            {
                if (strlen($value['Title']) > 35)
                {
                    $res[$key]['Title'] = substr($value['Title'], 0, 35) . '...';
                }
                else
                {
                    $res[$key]['Title'] = $value['Title'];
                }
            }
            if ($key == "Summary")
            {
                if (strlen($value['Summary']) > 73)
                {
                    $res[$key]['Summary'] = substr($value['Summary'], 0, 73) . '...';
                }
                else
                {
                    $res[$key]['Summary'] = $value['Summary'];
                }
            }
        }
        return $res;
    }
    public function getautometednotificationlist($limit=NULL)

    {
        $this->db->select('an.To_id,an.Link,case tou.Firstname when NULL then tou.Company_name Else tou.Firstname End as Toname,case fru.Firstname when NULL then fru.Company_name Else fru.Firstname End as Frname,an.From_id,an.Event_id,an.Type_id,an.Message,an.Created_date as Createddate,p.Event_name,p1.Event_name as eventname, ps.*', FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou', 'tou.id=an.To_id', 'left');
        $this->db->join('user fru', 'fru.id=an.From_id', 'left');
        $this->db->join('event p', 'p.id=an.Event_id', 'left');
        $this->db->join('event_size ps', 'ps.id=an.Event_size_id', 'left');
        $this->db->join('event p1', 'p1.id=ps.Event_id', 'left');
        $this->db->where('an.To_id', $this->data['user']->Id);
        $this->db->order_by('an.id desc');
        if ($limit != "All")
        {
            $this->db->limit($limit, 0);
        }
        $notify_query = $this->db->get();
        $notify_res['notification_res'] = $notify_query->result_array();
        $this->db->select('count(an.id) as notifycnt', FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou', 'tou.id=an.To_id', 'left');
        $this->db->join('user fru', 'fru.id=an.From_id', 'left');
        $this->db->join('event p', 'p.id=an.Event_id', 'left');
        $this->db->join('event_size ps', 'ps.id=an.Event_size_id', 'left');
        $this->db->where('an.To_id', $this->data['user']->Id);
        $this->db->where('an.Status', '0');
        $notify_query = $this->db->get();
        $datacnt = $notify_query->result_array();
        $notify_res['notification_res_cnt'] = $datacnt[0]['notifycnt'];
        $this->db->select('count(an.id) as notifytcnt', FALSE);
        $this->db->from('auto_notify an');
        $this->db->join('user tou', 'tou.id=an.To_id', 'left');
        $this->db->join('user fru', 'fru.id=an.From_id', 'left');
        $this->db->join('event p', 'p.id=an.Event_id', 'left');
        $this->db->join('event_size ps', 'ps.id=an.Event_size_id', 'left');
        $this->db->where('an.To_id', $this->data['user']->Id);
        $notify_query = $this->db->get();
        $datacnt = $notify_query->result_array();
        $notify_res['notification_res_tcnt'] = $datacnt[0]['notifytcnt'];
        foreach($notify_res['notification_res'] as $key => $value)
        {
            $notify_res['notification_res'][$key]['Createddate'] = $this->notifications_model->timeAgo($value['Createddate']);
            if ($value['Type_id'] == '0' || $value['Type_id'] == '1')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $replacements = array();
                $replacements[0] = $value['Event_name'];
                $msg = $this->replacemsg($value['Message'], $patterns, $replacements);
                $notify_res['notification_res'][$key]['Message'] = $msg;
                // $notify_res['notification_res'][$key]['Link']=base_url().'event/detail/'.$value['Event_id'];
            }
            else if ($value['Type_id'] == '9')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $replacements = array();
                $replacements[0] = $value['eventname'];
                $msg = $this->replacemsg($value['Message'], $patterns, $replacements);
                $notify_res['notification_res'][$key]['Message'] = $msg;
                // $notify_res['notification_res'][$key]['Link']=base_url().'order';
            }
            else if ($value['Type_id'] == '2')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{attendee}}/';
                $replacements = array();
                $replacements[0] = $value['Event_name'];
                $replacements[1] = $value['Frname'];
                $msg = $this->replacemsg($value['Message'], $patterns, $replacements);
                $notify_res['notification_res'][$key]['Message'] = $msg;
                // $notify_res['notification_res'][$key]['Link']=base_url().'event/detail/'.$value['Event_id'];
            }
            else if ($value['Type_id'] == '3' || $value['Type_id'] == '5' || $value['Type_id'] == '8')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{client}}/';
                $replacements = array();
                $replacements[0] = $value['eventname'];
                $replacements[1] = $value['Frname'];
                $msg = $this->replacemsg($value['Message'], $patterns, $replacements);
                $notify_res['notification_res'][$key]['Message'] = $msg;
                // $notify_res['notification_res'][$key]['Link']=base_url().'order';
            }
            else if ($value['Type_id'] == '4' || $value['Type_id'] == '6' || $value['Type_id'] == '7')
            {
                $patterns = array();
                $patterns[0] = '/{{event}}/';
                $patterns[1] = '/{{client}}/';
                $patterns[2] = '/{{size}}/';
                $replacements = array();
                $replacements[0] = $value['eventname'];
                $replacements[1] = $value['Toname'];
                $replacements[2] = $value['Size'];
                $msg = $this->replacemsg($value['Message'], $patterns, $replacements);
                $notify_res['notification_res'][$key]['Message'] = $msg;
            }
        }
        return $notify_res;
    }
    function replacemsg($messag=NULLe, $patterns=NULL, $replacements=NULL)
    {
        return preg_replace($patterns, $replacements, $message);
    }
    public function seenautonotify()

    {
        $category = array(
            'Status' => '1'
        );
        $this->db->where('To_id', $this->data['user']->Id);
        $this->db->update('auto_notify', $category);
        return true;
    }
    public function get_push_notification($event_id=NULL, $user_id=NULL, $acc_name=NULL, $Subdomain=NULL, $timezone = null, $email=NULL, $email_display=NULL)

    {
        date_default_timezone_set("UTC");
        $cdate = date('Y-m-d H:i:s');
        if (!empty($timezone))
        {
            if (strpos($timezone, "-") == true)
            {
                $arr = explode("-", $timezone);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) - $intNew);
            }
            if (strpos($timezone, "+") == true)
            {
                $arr = explode("+", $timezone);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) + $intNew);
            }
        }
        $this->db->select('n.Id,n.title,n.content,n.datetime,n.event_id,n.moduleslink,n.custommoduleslink,n.notification_type,un.id, un.user_id as visited_user_id,m.pagetitle')->from('notification n');
        $this->db->join('user_module_notification un', 'un.notification_id=n.id', 'left');
        $this->db->join('menu m', 'n.moduleslink=m.Id', 'left');
        $this->db->where("FIND_IN_SET('$user_id',n.user_ids) != 0", NULL, FALSE);
        $this->db->where("(FIND_IN_SET('$user_id',un.user_id) = 0", NULL, FALSE);
        $this->db->or_where("FIND_IN_SET('$user_id',un.user_id) IS NULL)", NULL, FALSE);
        $this->db->where('n.event_id', $event_id);
        $this->db->where("(n.datetime IS NULL", NULL, FALSE);
        $this->db->or_where("n.datetime <= '" . $cdate . "')", NULL, FALSE);
        $notify_query = $this->db->get()->result_array();
        foreach($notify_query as $key => $value)
        {
            if ($value['notification_type'] == '2' && $email_display == '1')
            {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com', 'All In The Loop');
                $this->email->to($email);
                $this->email->subject($value['title']);
                $this->email->message(html_entity_decode($value['content']));
                $this->email->send();
            }
            if (!empty($value['moduleslink']))
            {
                if (!in_array($value['moduleslink'], array(
                    22,
                    23,
                    24,
                    25
                )))
                {
                    if ($value['moduleslink'] == 12)
                    {
                        $url = base_url() . 'Messages/' . $acc_name . "/" . $Subdomain . '/privatemsg';
                    }
                    else if ($value['moduleslink'] == 13)
                    {
                        $url = base_url() . 'Messages/' . $acc_name . "/" . $Subdomain . '/publicmsg';
                    }
                    else if ($value['moduleslink'] == 42)
                    {
                        $url = base_url() . 'fundraising' . '/' . $acc_name . "/" . $Subdomain . '/home/fundraising_donation';
                    }
                    else if ($value['moduleslink'] == 27)
                    {
                        $url = base_url() . 'app/' . $acc_name . '/' . $Subdomain;
                    }
                    else
                    {
                        $url = base_url() . $value['pagetitle'] . '/' . '' . $acc_name . "/" . $Subdomain;
                    }
                }
                else
                {
                    $url = base_url() . 'fundraising' . '/' . $acc_name . "/" . $Subdomain . '/home/get_auction_pr';
                    if ($value['moduleslink'] == 22)
                    {
                        $url = $url . '/1';
                    }
                    else if ($value['moduleslink'] == 23)
                    {
                        $url = $url . '/2';
                    }
                    else if ($value['moduleslink'] == 24)
                    {
                        $url = $url . '/3';
                    }
                    else if ($value['moduleslink'] == 25)
                    {
                        $url = $url . '/4';
                    }
                }
                $notify_query[$key]['content'] = "<a href='" . $url . "'>" . $value['content'] . "</a>";
            }
            elseif (!empty($value['custommoduleslink']))
            {
                $url = base_url() . 'Cms/' . $acc_name . "/" . $Subdomain . "/View/" . $value['custommoduleslink'];
                $notify_query[$key]['content'] = "<a href='" . $url . "'>" . $value['content'] . "</a>";
            }
            if (empty($value['id']))
            {
                $data = array(
                    'notification_id' => $value['Id'],
                    'user_id' => $user_id
                );
                $this->db->insert('user_module_notification', $data);
            }
            else
            {
                if (empty($value['visited_user_id']))
                {
                    $str = $value['visited_user_id'];
                }
                else
                {
                    $str = $value['visited_user_id'] . ',' . $user_id;
                }
                $update_data = array(
                    'user_id' => $str
                );
                $this->db->where('id', $value['id']);
                $this->db->update('user_module_notification', $update_data);
            }
        }
        return $notify_query;
    }
    public function get_request_metting_by_event_user($uid=NULL, $rid=NULL, $eid=NULL, $acc_name=NULL, $Subdomain=NULL, $email=NULL, $email_display=NULL)

    {
        if ($rid == '6')
        {
            $this->db->select('*')->from('exibitor');
            $this->db->where('user_id', $uid);
            $this->db->where('Event_id', $eid);
            $ex_user = $this->db->get()->result_array();
            $this->db->select('*')->from('exhibitor_attendee_meeting eam');
            $this->db->where('eam.exhibiotor_id', $ex_user[0]['Id']);
            $this->db->where('eam.event_id', $eid);
            $this->db->where('eam.is_read', '0');
            $res = $this->db->get()->result_array();
            $url = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
        }
        else if ($rid == '4')
        {
            $this->db->select('*')->from('exhibitor_attendee_meeting eam');
            $this->db->where('eam.recever_attendee_id', $uid);
            $this->db->where('eam.event_id', $eid);
            $this->db->where('eam.is_read', '0');
            $res = $this->db->get()->result_array();
            $url = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
        }
        else if ($rid == '7')
        {
            $this->db->select('*')->from('exhibitor_attendee_meeting eam');
            $this->db->where('eam.moderator_id', $uid);
            $this->db->where('eam.event_id', $eid);
            $this->db->where('eam.is_read', '0');
            $res = $this->db->get()->result_array();
            $url = base_url() . 'Speakers/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
        }
        else
        {
            $res = array();
        }
        $arr1 = array();
        if (count($res) > 0)
        {
            $this->db->select('*')->from('event_app_push_template');
            $this->db->where('event_id', $eid);
            $this->db->where('Slug', 'Meeting');
            $noti = $this->db->get()->result_array();
            $arr1[0]['title'] = $noti[0]['Slug'];
            $arr1[0]['Message'] = "<a href='" . $url . "'>" . $noti[0]['Content'] . "</a>";
            if ($noti[0]['send_email'] == '1' && $email_display == '1')
            {
                $sub = !empty($noti[0]['email_subject']) ? $noti[0]['email_subject'] : $noti[0]['Slug'];
                $msg = !empty(strip_tags($noti[0]['email_content'])) ? $noti[0]['email_content'] : $noti[0]['Content'];
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com', 'All In The Loop');
                $this->email->to($email);
                $this->email->subject($sub);
                $this->email->message(html_entity_decode($msg));
                $this->email->send();
            }
            $data_status['is_read'] = '1';
            foreach($res as $key => $value)
            {
                $this->db->where('Id', $value['Id']);
                $this->db->update('exhibitor_attendee_meeting', $data_status);
            }
        }
        return $arr1;
    }
    public function get_auction_outbid_notify($eid=NULL, $uid=NULL, $acc_name=NULL, $Subdomain=NULL)

    {
        $this->db->select('p.product_id,p.auctionType,p.name,concat(u.Firstname," ",u.Lastname) as username,bn.*', False);
        $this->db->from('product p');
        $this->db->join('user u', 'u.Id=p.userid', 'left');
        $this->db->join('bids_notification bn', 'bn.product_id=p.product_id');
        $this->db->where('bn.is_sent', '0');
        $this->db->where('bn.msg_status', '0');
        $this->db->where('bn.user_id', $uid);
        $this->db->where('p.event_id', $eid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            $url = base_url() . 'fundraising/' . $acc_name . '/' . $Subdomain . '/product/details/' . $res[0]['product_id'] . '/' . $res[0]['auctionType'];
            $this->db->select('*')->from('event_app_push_template');
            $this->db->where('event_id', $eid);
            $this->db->where('Slug', 'Outbid Alert');
            $noti = $this->db->get()->result_array();
            $arr1[0]['title'] = $noti[0]['Slug'];
            $arr1[0]['Message'] = "<a href='" . $url . "'>" . $noti[0]['Content'] . "</a>";
        }
        foreach($res as $key => $value)
        {
            $this->db->where("notification_id", $value['notification_id']);
            $this->db->delete("bids_notification");
        }
        return $arr1;
    }
    public function get_session_notification($event_id=NULL, $user_id=NULL, $acc_name=NULL, $Subdomain=NULL, $timezone = null)

    {
        date_default_timezone_set("UTC");
        $cdate = date('Y-m-d H:i:s');
        if (!empty($timezone))
        {
            if (strpos($timezone, "-") == true)
            {
                $arr = explode("-", $timezone);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) - $intNew);
            }
            if (strpos($timezone, "+") == true)
            {
                $arr = explode("+", $timezone);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) + $intNew);
            }
        }
        $this->db->select('sn.*')->from('session_notification sn');
        $this->db->join("users_agenda ua", "FIND_IN_SET(sn.agenda_id,(TRIM(BOTH ',' FROM ua.agenda_id)))");
        $this->db->where('sn.event_id', $event_id);
        $this->db->where('ua.user_id', $user_id);
        $this->db->where("sn.notify_datetime <= '" . $cdate . "'", NULL, FALSE);
        $this->db->where("(FIND_IN_SET('$user_id',sn.send_user_ids) = 0", NULL, FALSE);
        $this->db->or_where('sn.send_user_ids IS NULL)', NULL, FALSE);
        $res = $this->db->get()->result_array();
        $respons = array();
        if (count($res) > 0)
        {
            foreach($res as $key => $value)
            {
                $url = base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/View_agenda/' . $value['agenda_id'];
                $respons[$key]['title'] = $value['title'];
                $respons[$key]['messages'] = "<a href='" . $url . "'>" . $value['messages'] . "</a>";
            }
            $update_val = $value['send_user_ids'] . ',' . $user_id;
            $this->db->where('notify_id', $value['notify_id']);
            $this->db->update('session_notification', array(
                'send_user_ids' => $update_val
            ));
        }
        return $respons;
    }
}
?>