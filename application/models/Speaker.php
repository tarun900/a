<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Speaker extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Speaker';
        $this->data['smalltitle'] = 'Speaker Details';
        $this->data['page_edit_title'] = 'edit';
        $this->data['breadcrumb'] = 'Speaker';
        parent::__construct($this->data);
        $this->load->model('agenda_model');
        $this->load->model('Event_model');
        $eventid = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            // $eventid = $user[0]->Event_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Speaker")
            {
                $title = "Speakers";
            }
            $cnt = $this->agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('7', $menu_list))
        {
            $this->load->model('speaker_model');
            // $this->load->model('agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Profile_model');
            $this->load->model('event_template_model');
            $this->load->library('session');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('Forbidden');
        }
    }
    public function index($id=NULL)

    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $speakers = $this->speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
        }
        // if ($this->data['user']->Role_name == 'Client')
        // {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event_id = $user[0]->event_id;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        if ($event_id > 0)
        {
            $speakers = $this->speaker_model->get_speaker_list($event_id);
            $this->data['speakers'] = $speakers;
            $speaker_list = $this->speaker_model->get_all_speakers($event_id);
            $this->data['speaker_list'] = $speaker_list;
        }
        else
        {
            $speakers = $this->speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
            $speaker_list = $this->speaker_model->get_all_speakers($id);
            $this->data['speaker_list'] = $speaker_list;
        }
        $menudata = $this->Event_model->geteventmenu($id, 7);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'speaker/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/js', true);
        $this->template->render();
    }
    public function edit($id = '0')

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['speakers'] = $this->speaker_model->get_speaker_list($id);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    public function add($id=NULL)

    {
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_port'] = "587";
        $config['smtp_user'] = "tarun@xhtmljunkies.com";
        $config['smtp_pass'] = "5LJoghWsNAIqtJWm_Ulb6A";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "text/html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
            {
                $tempname = explode('.', $_FILES['userfile']['name']);
                $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                $_POST['Logo'] = $_FILES['userfile']['name'];
                if ($this->data['user']->Role_name == 'Speaker')
                {
                    $this->data['user']->Logo = $_FILES['userfile']['name'];
                }
                $config['upload_path'] = './assets/user_files';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width'] = '2048';
                $config['max_height'] = '2048';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload())
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                else
                {
                    $data = array(
                        'upload_data' => $this->upload->data()
                    );
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval" && $k != "Speaker_mail")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $array_add['Organisor_id'] = $logged_in_user_id;
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $notification = $this->Setting_model->getnotificationsetting($Event_id);
            if ($notification[0]['email_display'] == 1)
            {
                $new_pass = $this->input->post('password');
                if ($em_template['Subject'] == "")
                {
                    $subject = "WELCOME_MSG";
                }
                else
                {
                    $subject = $em_template['Subject'];
                }
                $strFrom = EVENT_EMAIL;
                if (strip_tags($this->input->post('Speaker_mail')) == "")
                {
                    $slug = "Add New speaker";
                    $em_template = $this->Setting_model->front_email_template($slug, $Event_id);
                    // echo "fcbvg";print_r($em_template);die;
                    $em_template = $em_template[0];
                    $name = "<a href='" . base_url() . "login/index/new' target='_blank'>" . $this->input->post('Firstname') . ' ' . $this->input->post('Lastname') . "</a>";
                    $msg1 = $em_template['Content'];
                    $patterns = array();
                    $patterns[0] = '/{{FirstNameLastName}}/';
                    $patterns[1] = '/{{AppName}}/';
                    $patterns[2] = '/{{App URL}}/';
                    $patterns[3] = '/{{EmailAddress}}/';
                    $patterns[4] = '/{{Password}}/';
                    $patterns[5] = '/{{Administrator}}/';
                    $replacements = array();
                    $replacements[0] = $name;
                    $replacements[1] = $event[0]['Subdomain'];
                    $replacements[2] = base_url();
                    $replacements[3] = $this->input->post('email');
                    $replacements[4] = $new_pass;
                    $replacements[5] = 'Administrator';
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    // $msg .= "Email: " . $this->input->post('email') . "<br/>";
                    // $msg .= "Password: " . $new_pass."<br/>";
                    $msg.= $msg1;
                    if (strip_tags($em_template['Content']) == "")
                    {
                        $msg = "Dear " . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . ",<br/>";
                        $msg.= "A speaker profile has been created for you for " . $event[0]['Subdomain'] . "<br/>";
                        $msg.= "Please login via " . base_url() . " with the following credentials." . "<br/>";
                        $msg.= $this->input->post('email') . " " . $new_pass;
                        ".Once your login please click on your name on the top right of the screen and press My Profile to edit your profile<br/>";
                        $msg.= "many thanks <br/>Administrator";
                    }
                }
                else
                {
                    $msg = "<p>Dear <a href='" . base_url() . "login/index/new' target='_blank'>" . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . "</a>, </p>";
                    $msg.= "<p>A speaker profile has been created for you for " . $event[0]['Subdomain'] . ". <br/>";
                    $msg.= "Please login via " . base_url() . " with the following credentials. </p>";
                    $msg.= "Email:" . $this->input->post('email') . "<br/>";
                    $msg.= "Password:" . $new_pass . "<br/>";
                    $msg.= "<p>Once your login please click on your name on the top right of the screen and press My <br/>Profile to edit your profile.</p>";
                    $msg.= "<p>" . $_POST['Speaker_mail'] . "</p>";
                    $msg.= "Many thanks, <br/>";
                    $msg.= "Administrator";
                }
                if ($em_template['From'] != "")
                {
                    $strFrom = $em_template['From'];
                }
                // echo $msg;die;
                $this->email->from($strFrom, 'Event Management');
                $this->email->to($this->input->post('email'));
                $this->email->subject($subject); //'Speaker Account'
                $this->email->set_mailtype("html");
                $this->email->message(html_entity_decode($msg));
                if ($this->email->send())
                {
                    echo $this->email->print_debugger();
                    echo "success";
                    die;
                }
                else
                {
                    echo $this->email->print_debugger();
                    die;
                }
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $speaker_id = $this->speaker_model->add_speaker($array_add);
            $this->session->set_flashdata('speaker_data', 'Added');
            // redirect("speaker/index/" . $id);
            echo '<script>window.location.href="' . base_url() . 'speaker/index/' . $id . '"</script>';
        }
        /* if ($this->data['user']->Role_name == 'Client')
        { */
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $agenda_list = $this->agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        // echo'<pre>'; print_r($agenda_list).'test'; exit;
        // }
        // $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        // $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    public function exist($id=NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $speaker_list = $this->speaker_model->get_all_speakers($id);
        $this->data['speaker_list'] = $speaker_list;
        $this->data['Event_id'] = $Event_id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['speaker_array']['Event_id'] = $this->input->post('Event_id');
            $data['speaker_array']['User_id'] = $this->input->post('User_id');
            $data['speaker_array']['Role_id'] = $this->input->post('rolename');
            $speaker_id = $this->speaker_model->add_exist_speaker($data);
            $this->session->set_flashdata('speaker_data', 'Added');
            echo '<script>window.location.href="' . base_url() . 'speaker/index/' . $Event_id . '"</script>';
        }
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/exist', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    public function add_speakers($id=NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $dataval = "";
        if ($this->input->post())
        {
            if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
            {
                $tempname = explode('.', $_FILES['userfile']['name']);
                $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                $_POST['Logo'] = $_FILES['userfile']['name'];
                if ($this->data['user']->Role_name == 'Speaker')
                {
                    $this->data['user']->Logo = $_FILES['userfile']['name'];
                }
                $config['upload_path'] = './assets/user_files';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width'] = '2048';
                $config['max_height'] = '2048';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload())
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                else
                {
                    $data = array(
                        'upload_data' => $this->upload->data()
                    );
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $notification = $this->Setting_model->getnotificationsetting($Event_id);
            if ($notification[0]['email_display'] == 1)
            {
                $slug = "WELCOME_MSG";
                $em_template = $this->Setting_model->front_email_template($slug, $Event_id);
                $em_template = $em_template[0];
                $new_pass = $this->input->post('password');
                $name = ($this->input->post('Firstname') == NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
                $msg = $em_template['Content'];
                $patterns = array();
                $patterns[0] = '/{{name}}/';
                $patterns[1] = '/{{password}}/';
                $patterns[2] = '/{{email}}/';
                $replacements = array();
                $replacements[0] = $name;
                $replacements[1] = $new_pass;
                $replacements[2] = $this->input->post('Email');
                $msg = preg_replace($patterns, $replacements, $msg);
                $msg.= "Email: " . $this->input->post('Email') . "<br/>";
                $msg.= "Password: " . $new_pass;
                if ($em_template['Subject'] == "")
                {
                    $subject = "WELCOME_MSG";
                }
                else
                {
                    $subject = $em_template['Subject'];
                }
                $strFrom = EVENT_EMAIL;
                if ($em_template['From'] != "")
                {
                    $strFrom = $em_template['From'];
                }
                $this->email->from($strFrom, 'Event Management');
                $this->email->to($this->input->post('email'));
                $this->email->subject($subject); //'Speaker Account'
                $this->email->set_mailtype("html");
                $this->email->message(html_entity_decode($msg));
                $this->email->send();
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $speaker_id = $this->speaker_model->add_speakers($array_add);
            $speaker_list = $this->speaker_model->get_speaker_list($id);
            foreach($speaker_list as $keys => $values)
            {
                $dataval.= '<option value="' . $values['uid'] . '">' . $values["Firstname"] . ' ' . $values["Lastname"] . '</option>';
            }
            $dataval.= "<option value='New'>Add New Speaker</option>";
        }
        echo $dataval;
        exit;
    }
    public function delete($Event_id=NULL, $id=NULL)

    {
        // $Event_id = $this->uri->segment(4);
        $speaker = $this->speaker_model->delete_speaker($Event_id, $id);
        $this->session->set_flashdata('speaker_data', 'Deleted');
        // redirect('speaker/index/' . $Event_id);
        echo '<script>window.location.href="' . base_url() . 'speaker/index/' . $Event_id . '"</script>';
    }
    public function getnewstate()

    {
        $id_country = $this->input->post('id', TRUE);
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $statedata = $this->Profile_model->getstate($id_country);
        $output = null;
        foreach($statedata as $key => $value)
        {
            $output.= "<option value='" . $value['Id'] . "'>" . $value['state_name'] . "</option>";
        }
        echo $output;
    }
    public function checkemail($Event_id = NULL)

    {
        if ($this->input->post())
        {
            $client = $this->speaker_model->checkemail($this->input->post('email') , $this->input->post('idval') , $this->input->post('event_id'));
            if ($client)
            {
                echo "error###Email already exist. Please choose another email.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }
}