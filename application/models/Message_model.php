<?php
class Message_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_senderid($msg_id=NULL)

    {
        $this->db->select('Sender_id');
        $this->db->from('speaker_msg');
        $this->db->where("Id", $msg_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $sender_id = $res[0]['Sender_id'];
        return $sender_id;
    }
    public function get_speaker_value($Subdomain = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain', $Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*');
        $this->db->from('speaker_msg s');
        $this->db->join('user u', 'u.Id=s.receiver_id', 'left');
        $this->db->where('s.Sender_id', $user[0]->Id);
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }
    public function add_msg_hit($user_id=NULL, $current_date=NULL, $receiver_id=NULL, $event_id=NULL)

    {
        $this->db->select('*');
        $this->db->from('users_leader_board');
        $this->db->where("event_id", $event_id);
        $this->db->where("user_id", $user_id);
        $this->db->where("date", $current_date);
        $this->db->where("received_id", $receiver_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $msg_hit = $res[0]['msg_hit'];
        if ($msg_hit == '')
        {
            $data['msg_hit'] = 1;
            $data['event_id'] = $event_id;
            $data['user_id'] = $user_id;
            $data['date'] = $current_date;
            $data['received_id'] = $receiver_id;
            $this->db->insert("users_leader_board", $data);
        }
        else
        {
            $data['msg_hit'] = $msg_hit + 1;
            $this->db->where("user_id", $user_id);
            $this->db->where("event_id", $event_id);
            $this->db->where("date", $current_date);
            $this->db->where("received_id", $receiver_id);
            $this->db->update("users_leader_board", $data);
        }
    }
    public function add_comment_hit($user_id=NULL, $current_date=NULL, $commenter_id=NULL, $event_id=NULL)

    {
        $this->db->select('*');
        $this->db->from('users_leader_board');
        $this->db->where("event_id", $event_id);
        $this->db->where("user_id", $user_id);
        $this->db->where("date", $current_date);
        $this->db->where("comment_id", $commenter_id);
        $query = $this->db->get();
        $res = $query->result_array();
        $comment_hit = $res[0]['comment_hit'];
        if ($comment_hit == '')
        {
            $data['comment_hit'] = 1;
            $data['event_id'] = $event_id;
            $data['user_id'] = $user_id;
            $data['date'] = $current_date;
            $data['comment_id'] = $commenter_id;
            $this->db->insert("users_leader_board", $data);
        }
        else
        {
            $data['comment_hit'] = $comment_hit + 1;
            $this->db->where("event_id", $event_id);
            $this->db->where("user_id", $user_id);
            $this->db->where("date", $current_date);
            $this->db->where("comment_id", $commenter_id);
            $this->db->update("users_leader_board", $data);
        }
    }
    public function get_chat_users($Subdomain = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('u.Firstname as Sendername,u1.Firstname as Recivername,sm.*');
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->where('(sm.Sender_id=' . $user[0]->Id, NULL, FALSE);
        $this->db->or_where('sm.Receiver_id = ' . $user[0]->Id . ')', NULL, FALSE);
        $this->db->group_by('sm.Sender_id');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        return $res;
    }
    public function send_speaker_message($data=NULL)

    {
        $data['msg_creator_id'] = $data['Sender_id'];
        $this->db->insert('speaker_msg', $data);
        $msgid = $this->db->insert_id();
        $user = $this->session->userdata('current_user');
        foreach(json_decode($data['image']) as $imgkey => $imgvalus)
        {
            $dataimages = array(
                "Message" => "",
                "Sender_id" => $data['Sender_id'],
                "Receiver_id" => $data['Receiver_id'],
                "Parent" => $msgid,
                "Event_id" => $data['Event_id'],
                "Time" => $data['Time'],
                "image" => json_encode(array(
                    $imgvalus
                )) ,
                "ispublic" => $data['ispublic'],
                "msg_creator_id" => $data['Sender_id'],
                "msg_type" => $data['msg_type'],
                "qasession_id" => $data['qasession_id']
            );
            $this->db->insert('speaker_msg', $dataimages);
        }
        if ($data['ispublic'] == '0')
        {
            $data1 = array(
                'msg_id' => $msgid,
                'resiver_id' => $data['Receiver_id'],
                'sender_id' => $user[0]->Id,
                'event_id' => $data['Event_id'],
                'isread' => '1',
                'type' => '0',
            );
            $this->db->insert('msg_notifiy', $data1);
        }
        else
        {
            $res = $this->notifypublic($data['Event_id']);
            foreach($res as $key => $values)
            {
                $data1 = array(
                    'msg_id' => $msgid,
                    'resiver_id' => $values,
                    'sender_id' => $user[0]->Id,
                    'event_id' => $data['Event_id'],
                    'isread' => '1',
                    'type' => '0',
                );
                $this->db->insert('msg_notifiy', $data1);
            }
        }
    }
    public function send_grp_speaker_message($data=NULL)

    {
        $i = 0;
        $msgid;
        $user = $this->session->userdata('current_user');
        if (!empty($data['Receiver_id']))
        {
            foreach($data['Receiver_id'] as $key => $value)
            {
                $data1['Receiver_id'] = $value;
                $data1['Message'] = $data['Message'];
                $data1['Sender_id'] = $data['Sender_id'];
                $data1['Parent'] = $data['Parent'];
                $data1['Event_id'] = $data['Event_id'];
                $data1['Time'] = $data['Time'];
                $data1['image'] = $data['image'];
                $data1['ispublic'] = $data['ispublic'];
                $data1['priority_no'] = $data['priority_no'];
                if (!empty($data['msg_creator_id']))
                {
                    $data1['msg_creator_id'] = $data['msg_creator_id'];
                }
                else
                {
                    $data1['msg_creator_id'] = $data['Sender_id'];
                }
                $data1['org_msg_receiver_id'] = $data['org_msg_receiver_id'][$key];
                $data1['qasession_id'] = NULL;
                $this->db->insert('speaker_msg', $data1);
                $msgid[$i] = $this->db->insert_id();
                $i++;
            }
            foreach(json_decode($data['image']) as $imgkey => $imgvalus)
            {
                $i = 0;
                if (!empty($data['msg_creator_id']))
                {
                    $data['msg_creator_id'] = $data['msg_creator_id'];
                }
                else
                {
                    $data['msg_creator_id'] = $data['Sender_id'];
                }
                foreach($data['Receiver_id'] as $key => $value)
                {
                    $dataimages = array(
                        "Message" => "",
                        "Sender_id" => $data['Sender_id'],
                        "Receiver_id" => $value,
                        "Parent" => $msgid[$i],
                        "Event_id" => $data['Event_id'],
                        "Time" => $data['Time'],
                        "image" => json_encode(array(
                            $imgvalus
                        )) ,
                        "ispublic" => $data['ispublic'],
                        "priority_no" => $data['priority_no'],
                        "msg_creator_id" => $data['msg_creator_id'],
                        "org_msg_receiver_id" => $data['org_msg_receiver_id'][$key],
                        "qasession_id" => NULL
                    );
                    $this->db->insert('speaker_msg', $dataimages);
                    $i++;
                }
            }
        }
        else
        {
            if (!empty($data['msg_creator_id']))
            {
                $data['msg_creator_id'] = $data['msg_creator_id'];
            }
            else
            {
                $data['msg_creator_id'] = $data['Sender_id'];
            }
            $this->db->insert('speaker_msg', $data);
            $msgid = $this->db->insert_id();
            foreach(json_decode($data['image']) as $imgkey => $imgvalus)
            {
                $dataimages = array(
                    "Message" => "",
                    "Sender_id" => $data['Sender_id'],
                    "Receiver_id" => $data['Receiver_id'],
                    "Parent" => $msgid,
                    "Event_id" => $data['Event_id'],
                    "Time" => $data['Time'],
                    "image" => json_encode(array(
                        $imgvalus
                    )) ,
                    "ispublic" => $data['ispublic'],
                    "priority_no" => $data['priority_no'],
                    "msg_creator_id" => $data['msg_creator_id'],
                    "qasession_id" => NULL
                );
                $this->db->insert('speaker_msg', $dataimages);
            }
        }
        if ($data['ispublic'] == '0')
        {
            $i = 0;
            foreach($data['Receiver_id'] as $key => $value)
            {
                $data1 = array(
                    'msg_id' => $msgid[$i],
                    'resiver_id' => $value,
                    'sender_id' => $data['Sender_id'],
                    'event_id' => $data['Event_id'],
                    'isread' => '1',
                    'type' => '0',
                );
                $this->db->insert('msg_notifiy', $data1);
                $i++;
            }
        }
        else
        {
            $res = $this->notifypublic($data['Event_id']);
            foreach($res as $key => $values)
            {
                $data1 = array(
                    'msg_id' => $msgid,
                    'resiver_id' => $values,
                    'sender_id' => $user[0]->Id,
                    'event_id' => $data['Event_id'],
                    'isread' => '1',
                    'type' => '0',
                );
                $this->db->insert('msg_notifiy', $data1);
            }
        }
    }
    public function delete_message($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('speaker_msg');
        $this->db->where('msg_id', $id);
        $this->db->delete('speaker_comment');
        $str = $this->db->last_query();
    }
    public function delete_comment($id=NULL)

    {
        $this->db->where('id', $id);
        $this->db->delete('speaker_comment');
        $str = $this->db->last_query();
    }
    public function view_hangouts($id=NULL, $toid=NULL, $msgid = null, $start = 0, $end = 5)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('(sm.Sender_id=' . $user[0]->Id, NULL, FALSE);
        $this->db->or_where('sm.Receiver_id = ' . $user[0]->Id . ')', NULL, FALSE);
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Sender_id', $toid);
        if (!empty($msgid))
        {
            $this->db->where('sm.Id', $msgid);
        }
        $this->db->order_by('sm.id ', 'desc');
        // $this->db->where('sm.ispublic','0');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $this->db->select('u.Firstname as Sendername,u1.Firstname as Recivername,sm.*,u.Logo as Senderlogo,u1.Logo as Reciverlogo');
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or (sm.Receiver_id IS NULL and `ispublic`="1")');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('(sm.Sender_id=' . $toid, NULL, FALSE);
        $this->db->or_where('(sm.Receiver_id = ' . $toid . ' or sm.Receiver_id IS NULL))', NULL, FALSE);
        $this->db->where('sm.Sender_id', $user[0]->Id);
        $this->db->where('sm.Event_id', $id);
        if (!empty($msgid))
        {
            $this->db->where('sm.Id', $msgid);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        // $this->db->where('sm.ispublic','0');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        //        echo $this->db->last_query();
        //        exit;
        foreach($res as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res[$key]['comment'] = $res_comment;
        }
        if (!empty($res1))
        {
            $mainarray = array_merge($res1, $res);
        }
        else
        {
            $mainarray = array_merge($res1, $res);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        //        echo "<pre>";
        //        print_r($airports);
        $totalcount = count($airports);
        $recordarray = array();
        for ($i = $start; $i < $end; $i++)
        {
            if ($totalcount >= $i)
            {
                if (!empty($airports[$i]))
                {
                    $recordarray[$i] = $airports[$i];
                }
            }
        }
        return $recordarray;
    }
    function record_sort($records=NULL, $field=NULL, $reverse = false)
    {
        $hash = array();
        foreach($records as $record)
        {
            $hash[$record[$field]] = $record;
        }
        ($reverse) ? krsort($hash) : ksort($hash);
        $records = array();
        foreach($hash as $record)
        {
            $records[] = $record;
        }
        return $records;
    }
    public function view_hangouts_public_msg($id=NULL, $start = 0, $end = 5, $msg_id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or sm.Receiver_id  IS NULL');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        // $this->db->where('sm.Sender_id',$user[0]->Id);
        $this->db->where('sm.ispublic', '1');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        /*$this->db->protect_identifiers=false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile',FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u','u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru','ru.User_id =sm.Sender_id and ru.Event_id='.$id.'');
        $this->db->join('role r','r.Id =ru.Role_id');
        $this->db->join('user u1','u1.Id=sm.Receiver_id or (sm.Receiver_id IS NULL and `ispublic`="1")');
        $this->db->join('relation_event_user ru1','ru1.User_id =sm.Receiver_id and ru1.Event_id='.$id.'','left');
        $this->db->join('role r1','r1.Id =ru1.Role_id','left');
        $this->db->where('sm.ispublic','1');
        $this->db->where('sm.Event_id',$id);
        $this->db->where('sm.Parent','0');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach ($res as $key=>$value)
        {
        $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time',FALSE);
        $this->db->from('speaker_comment sc');
        $this->db->join('user u','u.Id=sc.user_id');
        $this->db->where('sc.msg_id',$value['Id']);
        $query_comment = $this->db->get();
        $res_comment = $query_comment->result_array();
        $res[$key]['comment']=$res_comment;
        }
        if($res1 !='')
        {
        $mainarray= array_merge($res1,$res);
        }
        else
        {
        $mainarray=array_merge($res1,$res);
        }*/
        if ($res1 != '')
        {
            $mainarray = array_merge($res1);
        }
        else
        {
            $mainarray = array_merge($res1);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        $totalcount = count($airports);
        $recordarray = array();
        for ($i = $start; $i < $end; $i++)
        {
            if ($totalcount >= $i)
            {
                if (!empty($airports[$i]))
                {
                    $recordarray[$i] = $airports[$i];
                }
            }
        }
        return $recordarray;
    }
    public function view_hangouts_private_msg($id=NULL, $start = 0, $end = 5, $msg_id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Sender_id', $user[0]->Id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('(sm.Sender_id=' . $user[0]->Id, NULL, FALSE);
        $this->db->or_where('sm.Receiver_id = ' . $user[0]->Id . ')', NULL, FALSE);
        //        $this->db->where('sm.Sender_id',$user[0]->Id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            // echo '<pre>'; print_r($res_comment); exit();
            $res[$key]['comment'] = $res_comment;
        }
        if (!empty($res1))
        {
            $mainarray = array_merge($res1, $res);
        }
        else
        {
            $mainarray = array_merge($res1, $res);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        $totalcount = count($airports);
        $recordarray = array();
        for ($i = $start; $i < $end; $i++)
        {
            if ($totalcount >= $i)
            {
                if (!empty($airports[$i]))
                {
                    $recordarray[$i] = $airports[$i];
                }
            }
        }
        /*       echo "<pre>";
        print_r($recordarray);
        exit;*/
        return $recordarray;
    }
    public function view_hangouts_private_msg_speaker($id=NULL, $msg_id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('mn.isread,(case when concat(u.Firstname," ",u.Lastname) Is NULL then "Anonymous" else concat(u.Firstname," ",u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Title as SenderTitle,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,sm.priority_no,spn.Priority_no as p_no,concat(oru.Firstname," ",oru.Lastname) as msg_creator_name,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id', 'left');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '', 'left');
        $this->db->join('role r', 'r.Id =ru.Role_id', 'left');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id', 'left');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->join('msg_notifiy mn', 'mn.msg_id =sm.Id', 'left');
        $this->db->join('speaker_priority_no spn', 'spn.msg_id=sm.Id', 'left');
        $this->db->join('user oru', 'oru.Id=sm.msg_creator_id', 'left');
        $this->db->where('mn.type', '0');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('mn.event_id', $id);
        $this->db->where('sm.Receiver_id', $user[0]->Id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id asc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            if (!empty($value['msg_creator_id']))
            {
                $res1[$key]['Sender_id'] = $value['msg_creator_id'];
                $res1[$key]['Sendername'] = $value['msg_creator_name'];
            }
            $this->db->select('mn.resiver_id as forward_id,u.Firstname as forward_name');
            $this->db->from('msg_notifiy mn');
            $this->db->join('user u', 'u.Id=mn.resiver_id', 'left');
            $this->db->where('mn.sender_id', $user[0]->Id);
            $this->db->where('mn.event_id', $id);
            $this->db->where('mn.msg_id', $value['Id']);
            $query_for = $this->db->get();
            $for = $query_for->result_array();
            $res1[$key]['forward_id'] = $for[0]['forward_id'];
            $res1[$key]['forward_name'] = $for[0]['forward_name'];
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        /*$this->db->protect_identifiers=false;
        $this->db->select('mn.isread,(case when concat(u.Firstname," ",u.Lastname) Is NULL then "Anonymous" else concat(u.Firstname," ",u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Title as SenderTitle,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,sm.priority_no,spn.Priority_no as p_no,concat(oru.Firstname," ",oru.Lastname) as msg_creator_name,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id',FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u','u.Id=sm.Sender_id','left');
        $this->db->join('relation_event_user ru','ru.User_id =sm.Sender_id and ru.Event_id='.$id.'','left');
        $this->db->join('role r','r.Id =ru.Role_id','left');
        $this->db->join('user u1','u1.Id=sm.Receiver_id','left');
        $this->db->join('user u3','u3.Id=sm.org_msg_receiver_id','left');
        $this->db->join('relation_event_user ru1','ru1.User_id =sm.Receiver_id and ru1.Event_id='.$id.'','left');
        $this->db->join('role r1','r1.Id =ru1.Role_id','left');
        $this->db->join('msg_notifiy mn','mn.msg_id =sm.Id','left');
        $this->db->join('speaker_priority_no spn','spn.msg_id=sm.Id','left');
        $this->db->join('user oru','oru.Id=sm.msg_creator_id','left');
        $this->db->where('mn.type','0');
        $this->db->where('(sm.Receiver_id='.$user[0]->Id,NULL,FALSE);
        $this->db->or_where('sm.Receiver_id = '.$user[0]->Id.')',NULL,FALSE);
        $this->db->where('sm.ispublic','0');
        $this->db->where('sm.Event_id',$id);
        $this->db->where('sm.Parent','0');
        if($msg_id!="")
        {
        $this->db->where('sm.id',$msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id asc');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach ($res as $key=>$value)
        {
        if(!empty($value['msg_creator_id']))
        {
        $res[$key]['Sender_id']=$value['msg_creator_id'];
        $res[$key]['Sendername']=$value['msg_creator_name'];
        }
        $this->db->select('mn.resiver_id as forward_id,u.Firstname as forward_name');
        $this->db->from('msg_notifiy mn');
        $this->db->join('user u','u.Id=mn.resiver_id','left');
        $this->db->where('mn.sender_id',$user[0]->Id);
        $this->db->where('mn.event_id',$id);
        $this->db->where('mn.msg_id',$value['Id']);
        $query_for=$this->db->get();
        $for=$query_for->result_array();
        $res[$key]['forward_id']=$for[0]['forward_id'];
        $res[$key]['forward_name']=$for[0]['forward_name'];
        $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time',FALSE);
        $this->db->from('speaker_comment sc');
        $this->db->join('user u','u.Id=sc.user_id');
        $this->db->where('sc.msg_id',$value['Id']);
        $query_comment = $this->db->get();
        $res_comment = $query_comment->result_array();
        $res[$key]['comment']=$res_comment;
        }
        if(!empty($res1))
        {
        $mainarray= array_merge($res1,$res);
        }
        else
        {
        $mainarray=array_merge($res1,$res);
        }*/
        $airports = $this->record_sort($res1, "Id", true);
        return $airports;
    }
    // $qry = mysql_query("select * from speaker_msg where sender_id='$sid' and rec_id='$receiver_id' or sen_id='$receiver_id' and rec_id='$sid' ;"));
    public function view_hangouts_private_msg1($id=NULL, $start = 0, $end = 5, $msg_id = null, $uid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $where = "sm.Event_id = '" . $id . "' AND (sm.Sender_id = '" . $user[0]->Id . "' OR  sm.Sender_id = '" . $uid . "')  
                    AND (sm.Receiver_id = '" . $user[0]->Id . "' OR  sm.Receiver_id = '" . $uid . "')  
                    AND (u1.Id = '" . $user[0]->Id . "' OR  u1.Id = '" . $uid . "') 
                    AND sm.ispublic = '0' AND  sm.Parent = '0' ";
        if ($msg_id != "")
        {
            $where.= " AND sm.id = '" . $msg_id . "' ";
        }
        $this->db->where($where);
        /*this->db->where('sm.Event_id',$id);
        $this->db->where('sm.Sender_id',$user[0]->Id);
        $this->db->or_where('sm.Sender_id',$uid);
        $this->db->where('sm.Receiver_id',$uid);
        $this->db->or_where('sm.Receiver_id',$user[0]->Id);
        $this->db->where('u1.Id',$user[0]->Id);
        $this->db->or_where('u1.Id',$uid);
        $this->db->where('sm.ispublic','0');
        $this->db->where('sm.Parent','0');
        if($msg_id!="")
        {
        $this->db->where('sm.id',$msg_id);
        }*/
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        /* echo '<pre>'; print_r($res1); exit();*/
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $this->db->protect_identifiers = false;
        $this->db->select('u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        /* $this->db->where('(sm.Sender_id='.$user[0]->Id,NULL,FALSE);
        $this->db->or_where('sm.Sender_id',$uid);
        $this->db->where('sm.Receiver_id = '.$uid.')',NULL,FALSE);
        $this->db->or_where('sm.Receiver_id',$user[0]->Id);
        //        $this->db->where('sm.Sender_id',$user[0]->Id);
        $this->db->where('sm.ispublic','0');
        $this->db->where('sm.Event_id',$id);
        $this->db->where('sm.Parent','0');
        if($msg_id!="")
        {
        $this->db->where('sm.id',$msg_id);
        }*/
        $where1 = "sm.Event_id = '" . $id . "' AND (sm.Sender_id = '" . $user[0]->Id . "' OR  sm.Sender_id = '" . $uid . "')  
                    AND (sm.Receiver_id = '" . $user[0]->Id . "' OR  sm.Receiver_id = '" . $uid . "')  
                    AND (u1.Id = '" . $user[0]->Id . "' OR  u1.Id = '" . $uid . "') 
                    AND sm.ispublic = '0' AND  sm.Parent = '0' ";
        if ($msg_id != "")
        {
            $where.= " AND sm.id = '" . $msg_id . "' ";
        }
        $this->db->where($where1);
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            // echo '<pre>'; print_r($res_comment); exit();
            $res[$key]['comment'] = $res_comment;
        }
        if (!empty($res1))
        {
            $mainarray = array_merge($res1, $res);
        }
        else
        {
            $mainarray = array_merge($res1, $res);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        $totalcount = count($airports);
        $recordarray = array();
        for ($i = $start; $i < $end; $i++)
        {
            if ($totalcount >= $i)
            {
                if (!empty($airports[$i]))
                {
                    $recordarray[$i] = $airports[$i];
                }
            }
        }
        return $recordarray;
    }
    public function get_s_id($id=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('*');
        $this->db->from('speaker_msg');
        $this->db->where('Sender_id', $user[0]->Id);
        $this->db->where('Receiver_id', $this->uri->segment(3));
        $this->db->or_where('Sender_id', $this->uri->segment(3));
        $this->db->where('Receiver_id', $user[0]->Id);
        $query = $this->db->get();
        $res = $query->result_array();
        // echo '<pre>'; print_r($res); exit();
        return $res;
    }
    public function add_comment($eventid=NULL, $array_add=NULL)

    {
        $this->db->insert('speaker_comment', $array_add);
        $insertid = $this->db->insert_id();
        $user = $this->session->userdata('current_user');
        $alluserdata = $this->commentalleventuser($eventid, $array_add['msg_id']);
        foreach($alluserdata as $keya => $value)
        {
            $data1 = array(
                'msg_id' => $array_add['msg_id'],
                'resiver_id' => $value,
                'sender_id' => $user[0]->Id,
                'event_id' => $eventid,
                'isread' => '1',
                'type' => '1',
            );
            $this->db->insert('msg_notifiy', $data1);
        }
        return $insertid;
    }
    public function msg_notify($eventid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('mn.id,sm.Message,mn.issent,sm.ispublic,u.Firstname,u.Lastname,u.Logo,u1.Firstname as Senderfname,u1.Lastname as Senderlname,u1.Logo as Senderlogo,mn.type');
        $this->db->from('msg_notifiy mn');
        $this->db->join('speaker_msg sm', 'sm.Id=mn.msg_id');
        $this->db->join('user u', 'u.id=mn.resiver_id', 'left');
        $this->db->join('user u1', 'u1.id=mn.sender_id', 'left');
        $this->db->where('mn.resiver_id', $user[0]->Id);
        $this->db->where('mn.event_id', $eventid);
        $this->db->where('mn.isread', '1');
        $this->db->where(" (sm.Receiver_id='" . $user[0]->Id . "' OR sm.Receiver_id IS NULL) ", NULL, false);
        $query = $this->db->get();
        $res = $query->result_array();
        $listdata = array();
        foreach($res as $mkey => $mvalue)
        {
            if ($mvalue['type'] == 0)
            {
                $listdata[$mkey]['id'] = $mvalue['id'];
                if (strlen($mvalue['Message']) > 25)
                {
                    // $listdata[$mkey]['Message']=substr($mvalue['Message'], 0,25)."...";
                    $listdata[$mkey]['Message'] = $mvalue['Message'];
                }
                else
                {
                    $listdata[$mkey]['Message'] = $mvalue['Message'];
                }
                $listdata[$mkey]['Firstname'] = $mvalue['Senderfname'];
                $listdata[$mkey]['Lastname'] = $mvalue['Senderlname'];
                $listdata[$mkey]['Ispublic'] = $mvalue['ispublic'];
                $listdata[$mkey]['Issent'] = $mvalue['issent'];
                if ($mvalue['Senderlogo'] != "")
                {
                    $listdata[$mkey]['Logo'] = base_url() . 'assets/user_files/' . $mvalue['Senderlogo'];
                }
                else
                {
                    $listdata[$mkey]['Logo'] = base_url() . "assets/images/anonymous.jpg";
                }
            }
            else
            {
                $listdata[$mkey]['id'] = $mvalue['id'];
                $listdata[$mkey]['Message'] = "commented on your message";
                $listdata[$mkey]['Firstname'] = $mvalue['Senderfname'];
                $listdata[$mkey]['Lastname'] = $mvalue['Senderlname'];
                $listdata[$mkey]['Ispublic'] = $mvalue['ispublic'];
                $listdata[$mkey]['Issent'] = $mvalue['issent'];
                if ($mvalue['Senderlogo'] != "")
                {
                    $listdata[$mkey]['Logo'] = base_url() . 'assets/user_files/' . $mvalue['Senderlogo'];
                }
                else
                {
                    $listdata[$mkey]['Logo'] = base_url() . "assets/images/anonymous.jpg";
                }
            }
            $update_allocation = array(
                'issent' => '0'
            );
            $this->db->where('id', $mvalue['id']);
            $this->db->update('msg_notifiy', $update_allocation);
        }
        return $listdata;
    }
    public function msg_notify_autoload($eventid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('mn.id,sm.Message,mn.issent,sm.ispublic,u.Firstname,u.Lastname,u.Logo,u1.Firstname as Senderfname,u1.Lastname as Senderlname,u1.Logo as Senderlogo,mn.type');
        $this->db->from('msg_notifiy mn');
        $this->db->join('speaker_msg sm', 'sm.Id=mn.msg_id');
        $this->db->join('user u', 'u.id=mn.resiver_id', 'left');
        $this->db->join('user u1', 'u1.id=mn.sender_id', 'left');
        $this->db->where('mn.resiver_id', $user[0]->Id);
        $this->db->where('mn.event_id', $eventid);
        $this->db->where('mn.isread', '1');
        $this->db->where(" (sm.Receiver_id = '" . $user[0]->Id . "' OR sm.Receiver_id IS NULL) ", NULL, false);
        $query = $this->db->get();
        $res = $query->result_array();
        $listdata = array();
        foreach($res as $mkey => $mvalue)
        {
            if ($mvalue['type'] == 0)
            {
                $listdata[$mkey]['id'] = $mvalue['id'];
                if (strlen($mvalue['Message']) > 25)
                {
                    $listdata[$mkey]['Message'] = $mvalue['Message'];
                }
                else
                {
                    $listdata[$mkey]['Message'] = $mvalue['Message'];
                }
                $listdata[$mkey]['Firstname'] = $mvalue['Senderfname'];
                $listdata[$mkey]['Lastname'] = $mvalue['Senderlname'];
                $listdata[$mkey]['Ispublic'] = $mvalue['ispublic'];
                $listdata[$mkey]['Issent'] = $mvalue['issent'];
                if ($mvalue['Senderlogo'] != "")
                {
                    $listdata[$mkey]['Logo'] = base_url() . 'assets/user_files/' . $mvalue['Senderlogo'];
                }
                else
                {
                    $listdata[$mkey]['Logo'] = base_url() . "assets/images/anonymous.jpg";
                }
            }
            else
            {
                $listdata[$mkey]['id'] = $mvalue['id'];
                $listdata[$mkey]['Message'] = "replied on your message";
                $listdata[$mkey]['Firstname'] = $mvalue['Senderfname'];
                $listdata[$mkey]['Lastname'] = $mvalue['Senderlname'];
                $listdata[$mkey]['Ispublic'] = $mvalue['ispublic'];
                $listdata[$mkey]['Issent'] = $mvalue['issent'];
                if ($mvalue['Senderlogo'] != "")
                {
                    $listdata[$mkey]['Logo'] = base_url() . '/assets/user_files/' . $mvalue['Senderlogo'];
                }
                else
                {
                    $listdata[$mkey]['Logo'] = base_url() . "/assets/images/anonymous.jpg";
                }
            }
            $update_allocation = array(
                'issent' => '0'
            );
            $this->db->where('id', $mvalue['id']);
            $this->db->update('msg_notifiy', $update_allocation);
        }
        return $listdata;
    }
    public function msg_notify_update($eventid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $update_allocation = array(
            'isread' => '0'
        );
        $this->db->where('event_id', $eventid);
        $this->db->where('resiver_id', $user[0]->Id);
        $this->db->update('msg_notifiy', $update_allocation);
        return true;
    }
    public function msg_read_update($eventid=NULL, $mid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $update_allocation = array(
            'isread' => '0'
        );
        $this->db->where('event_id', $eventid);
        $this->db->where('resiver_id', $user[0]->Id);
        $this->db->where('msg_id', $mid);
        $this->db->update('msg_notifiy', $update_allocation);
        return $this->db->affected_rows();
    }
    public function bid_msg_notify_autoload($eventid=NULL, $user_id=NULL)

    {
        $this->db->select('p.product_id,p.name,concat(u.Firstname," ",u.Lastname) as username,bn.*', False);
        $this->db->from('product p');
        $this->db->join('user u', 'u.Id=p.userid', 'left');
        $this->db->join('bids_notification bn', 'bn.product_id=p.product_id');
        $this->db->where('bn.is_sent', '0');
        $this->db->where('bn.msg_status', '1');
        $this->db->where('bn.user_id', $user_id);
        $this->db->where('p.event_id', $eventid);
        $query = $this->db->get();
        $res = $query->result_array();
        foreach($res as $key => $value)
        {
            $this->db->where("notification_id", $value['notification_id']);
            $data['is_sent'] = '1';
            $this->db->update("bids_notification", $data);
        }
        return $res;
    }
    public function commentalleventuser($eventid=NULL, $msgid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('Sender_id as User_id');
        $this->db->from('speaker_msg sm');
        $this->db->where('sm.Id', $msgid);
        $this->db->where('sm.Sender_id !=', $user[0]->Id);
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('Receiver_id as User_id');
        $this->db->from('speaker_msg sm');
        $this->db->where('sm.Id', $msgid);
        $this->db->where('sm.Receiver_id !=', $user[0]->Id);
        $query = $this->db->get();
        $res2 = $query->result_array();
        if (!empty($res) && !empty($res2))
        {
            $this->db->select('group_concat(distinct(user_id)) as User_id');
            $this->db->from('speaker_comment sc');
            $this->db->where('sc.msg_id', $msgid);
            $this->db->where('(sc.user_id !=' . $res[0]['User_id'], NULL, FALSE);
            $this->db->or_where('sc.user_id !=' . $res1[0]['User_id'] . ')', NULL, FALSE);
            $query = $this->db->get();
            $res1 = $query->result_array();
        }
        else if (!empty($res))
        {
            $this->db->select('group_concat(distinct(user_id)) as User_id');
            $this->db->from('speaker_comment sc');
            $this->db->where('sc.msg_id', $msgid);
            $this->db->where('sc.user_id !=', $res[0]['User_id']);
            $query = $this->db->get();
            $res1 = $query->result_array();
        }
        else if (!empty($res2))
        {
            $this->db->select('group_concat(distinct(user_id)) as User_id');
            $this->db->from('speaker_comment sc');
            $this->db->where('sc.msg_id', $msgid);
            $this->db->where('sc.user_id !=', $res2[0]['User_id']);
            $query = $this->db->get();
            $res1 = $query->result_array();
        }
        if (!empty($res1))
        {
            if (!empty($res) && !empty($res2))
            {
                $sringuser = $res2[0]['User_id'] . "," . $res[0]['User_id'] . "," . $res1[0]['User_id'];
            }
            else if (!empty($res))
            {
                $sringuser = $res[0]['User_id'] . "," . $res1[0]['User_id'];
            }
            else if (!empty($res2))
            {
                $sringuser = $res2[0]['User_id'] . "," . $res1[0]['User_id'];
            }
            else
            {
                $sringuser = $res1[0]['User_id'];
            }
        }
        else
        {
            $sringuser = $res[0]['User_id'];
        }
        if ($sringuser != "")
        {
            $finalarray = explode(',', $sringuser);
            $finalarray = array_unique($finalarray);
            $pos = array_search($user[0]->Id, $finalarray);
            unset($finalarray[$pos]);
        }
        else
        {
            $finalarray = array();
        }
        return $finalarray;
    }
    public function notifypublic($eventid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->select('group_concat(ru.User_id) as User_id');
        $this->db->from('relation_event_user ru');
        $this->db->where('ru.Event_id', $eventid);
        $this->db->where('ru.User_id !=', $user[0]->Id);
        $query = $this->db->get();
        $res1 = $query->result_array();
        $finalarray = array();
        if ($res1[0]['User_id'] != "")
        {
            $finalarray = explode(',', $res1[0]['User_id']);
            $finalarray = array_unique($finalarray);
        }
        return $finalarray;
    }
    public function savepriorityno($data=NULL)

    {
        $no = $data['Priority_no'];
        unset($data['Priority_no']);
        $query = $this->db->get_where('speaker_priority_no', $data);
        $res = $query->result_array();
        if (count($res) > 0)
        {
            $this->db->where($data);
            $this->db->update('speaker_priority_no', array(
                'Priority_no' => $no
            ));
        }
        else
        {
            $data['Priority_no'] = $no;
            $this->db->insert('speaker_priority_no', $data);
        }
    }
    public function send_grp_speaker_message_public_reply($data=NULL)

    {
        $data['msg_creator_id'] = $data['msg_creator_id'];
        $this->db->insert('speaker_msg', $data);
        $msgid = $this->db->insert_id();
        foreach(json_decode($data['image']) as $imgkey => $imgvalus)
        {
            $dataimages = array(
                "Message" => "",
                "Sender_id" => $data['Sender_id'],
                "Receiver_id" => $data['Receiver_id'],
                "Parent" => $msgid,
                "Event_id" => $data['Event_id'],
                "Time" => $data['Time'],
                "image" => json_encode(array(
                    $imgvalus
                )) ,
                "ispublic" => $data['ispublic'],
                "msg_creator_id" => $data['msg_creator_id']
            );
            $this->db->insert('speaker_msg', $dataimages);
        }
        $res = $this->notifypublic($data['Event_id']);
        foreach($res as $key => $values)
        {
            $data1 = array(
                'msg_id' => $msgid,
                'resiver_id' => $values,
                'sender_id' => $data['Sender_id'],
                'event_id' => $data['Event_id'],
                'isread' => '1',
                'type' => '0',
            );
            $this->db->insert('msg_notifiy', $data1);
        }
        return $msgid;
    }
    public function forwardmsg($data=NULL)

    {
        $this->db->insert('msg_notifiy', $data);
    }
    public function view_hangouts_public_msg_by_organizer($org_id=NULL, $start = 0, $msg_id = null)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->join('speaker_msg sm', 'sm.Event_id=he.event_id', 'left');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or sm.Receiver_id  IS NULL');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('e.Organisor_id', $org_id);
        $where = "FIND_IN_SET(13,`checkbox_values`) > 0";
        $this->db->where($where);
        $this->db->where('he.show_publicmsg', '1');
        $this->db->where('sm.ispublic', '1');
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $this->db->protect_identifiers = false;
        $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->join('speaker_msg sm', 'sm.Event_id=he.event_id', 'left');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id or (sm.Receiver_id IS NULL and `ispublic`="1")');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->where('e.Organisor_id', $org_id);
        $where = "FIND_IN_SET(13,`checkbox_values`) > 0";
        $this->db->where($where);
        $this->db->where('he.show_publicmsg', '1');
        $this->db->where('sm.ispublic', '1');
        $this->db->where('sm.Parent', '0');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            if (!empty($res_comment))
            {
                $res[$key]['comment'] = $res_comment;
            }
        }
        if (!empty($res1))
        {
            $mainarray = array_merge($res1, $res);
        }
        else
        {
            $mainarray = array_merge($res1, $res);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        $airports = array_slice($airports, $start, 5);
        return $airports;
    }
    public function view_hangouts_private_msg1_by_organizer_hub($org_id=NULL, $start = 0, $msg_id = null, $uid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->join('speaker_msg sm', 'sm.Event_id=he.event_id', 'left');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $where = "(sm.Sender_id = '" . $user[0]->Id . "' OR  sm.Sender_id = '" . $uid . "')  
      AND (sm.Receiver_id = '" . $user[0]->Id . "' OR  sm.Receiver_id = '" . $uid . "')  
      AND (u1.Id = '" . $user[0]->Id . "' OR  u1.Id = '" . $uid . "') 
      AND sm.ispublic = '0' AND  sm.Parent = '0' ";
        if ($msg_id != "")
        {
            $where.= " AND sm.id = '" . $msg_id . "' ";
        }
        $this->db->where('e.Organisor_id', $org_id);
        $this->db->where($where);
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $this->db->protect_identifiers = false;
        $this->db->select('e.Id as eventid,e.Subdomain,u.Firstname as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile', FALSE)->from('hub_event he');
        $this->db->join('event e', 'e.Id=he.event_id', 'left');
        $this->db->join('speaker_msg sm', 'sm.Event_id=he.event_id', 'left');
        $this->db->join('user u', 'u.Id=sm.Sender_id');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=he.event_id');
        $this->db->join('role r', 'r.Id =ru.Role_id');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=he.event_id', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $where1 = "(sm.Sender_id = '" . $user[0]->Id . "' OR  sm.Sender_id = '" . $uid . "')  
        AND (sm.Receiver_id = '" . $user[0]->Id . "' OR  sm.Receiver_id = '" . $uid . "')  
        AND (u1.Id = '" . $user[0]->Id . "' OR  u1.Id = '" . $uid . "') 
        AND sm.ispublic = '0' AND  sm.Parent = '0' ";
        if ($msg_id != "")
        {
            $where.= " AND sm.id = '" . $msg_id . "' ";
        }
        $this->db->where('e.Organisor_id', $org_id);
        $this->db->where($where1);
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id desc');
        $query1 = $this->db->get();
        $res = $query1->result_array();
        foreach($res as $key => $value)
        {
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res[$key]['comment'] = $res_comment;
        }
        if (!empty($res1))
        {
            $mainarray = array_merge($res1, $res);
        }
        else
        {
            $mainarray = array_merge($res1, $res);
        }
        $airports = $this->record_sort($mainarray, "Id", true);
        $airports = array_slice($airports, $start, 5);
        return $airports;
    }
    public function view_hangouts_private_msg_moderator_panel($id=NULL, $msg_id = null, $qaid = NULL)

    {
        $user = $this->session->userdata('current_user');
        $this->db->protect_identifiers = false;
        $this->db->select('qmv.votes,mn.isread,(case when concat(u.Firstname," ",u.Lastname) Is NULL then "Anonymous" else concat(u.Firstname," ",u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo,r.Name as SenderRole,r1.Name as ReciverRole,u.Company_name as SenderCompnayname,u.Title as SenderTitle,u.Street as SenderStreet,u.Suburb as SenderSuburb,u.State as SenderState,u.Country as SenderCountry,u.Postcode as SenderPostcode,u.Mobile as SenderMobile,u.Phone_business as SenderPhone_business,u.Isprofile as SenderIsprofile,u1.Company_name as ReciverCompnayname,u1.Street as ReciverStreet,u1.Suburb as ReciverSuburb,u1.State as ReciverState,u1.Country as ReciverCountry,u1.Postcode as ReciverPostcode,u1.Mobile as ReciverMobile,u1.Phone_business as ReciverPhone_business,u1.Isprofile as ReciverIsprofile,sm.priority_no,spn.Priority_no as p_no,concat(oru.Firstname," ",oru.Lastname) as msg_creator_name,u3.Firstname as org_Firstname,u3.Lastname as org_Lastname,u3.Id as org_receiver_id', FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u', 'u.Id=sm.Sender_id', 'left');
        $this->db->join('relation_event_user ru', 'ru.User_id =sm.Sender_id and ru.Event_id=' . $id . '', 'left');
        $this->db->join('role r', 'r.Id =ru.Role_id', 'left');
        $this->db->join('user u1', 'u1.Id=sm.Receiver_id', 'left');
        $this->db->join('user u3', 'u3.Id=sm.org_msg_receiver_id', 'left');
        $this->db->join('relation_event_user ru1', 'ru1.User_id =sm.Receiver_id and ru1.Event_id=' . $id . '', 'left');
        $this->db->join('role r1', 'r1.Id =ru1.Role_id', 'left');
        $this->db->join('msg_notifiy mn', 'mn.msg_id =sm.Id', 'left');
        $this->db->join('speaker_priority_no spn', 'spn.msg_id=sm.Id', 'left');
        $this->db->join('user oru', 'oru.Id=sm.msg_creator_id', 'left');
        $this->db->join('qa_message_votes qmv', 'qmv.message_id=sm.Id', 'left');
        $this->db->where('mn.type', '0');
        $this->db->where('sm.Event_id', $id);
        $this->db->where('sm.Receiver_id', $user[0]->Id);
        $this->db->where('sm.ispublic', '0');
        $this->db->where('sm.Parent', '0');
        if ($msg_id != "")
        {
            $this->db->where('sm.id', $msg_id);
        }
        if (!empty($qaid))
        {
            $this->db->where('sm.qasession_id', $qaid);
        }
        $this->db->group_by('sm.id');
        $this->db->order_by('sm.id asc');
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach($res1 as $key => $value)
        {
            $this->db->select('mn.resiver_id as forward_id,u.Firstname as forward_name');
            $this->db->from('msg_notifiy mn');
            $this->db->join('user u', 'u.Id=mn.resiver_id', 'left');
            $this->db->where('mn.sender_id', $user[0]->Id);
            $this->db->where('mn.event_id', $id);
            $this->db->where('mn.msg_id', $value['Id']);
            $query_for = $this->db->get();
            $for = $query_for->result_array();
            $res1[$key]['forward_id'] = $for[0]['forward_id'];
            $res1[$key]['forward_name'] = $for[0]['forward_name'];
            $this->db->select('sc.id,CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.user_id,sc.Time', FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u', 'u.Id=sc.user_id');
            $this->db->where('sc.msg_id', $value['Id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            $res1[$key]['comment'] = $res_comment;
        }
        $airports = $this->record_sort($res1, "Id", true);
        return $airports;
    }
    public function delete_qa_question_by_messages_id($mid=NULL)

    {
        $this->db->where('Id', $mid);
        $this->db->delete('speaker_msg');
    }
    public function update_modrate_msg($mid=NULL, $mcontent=NULL)

    {
        $this->db->where('Id', $mid);
        $this->db->update('speaker_msg', array(
            'Message' => $mcontent
        ));
    }
    public function getAllMeetingRequestModerator($event_id=NULL,$user_id=NULL,$id=NULL)
    {
        $this->db->select('eam.date,
                                      eam.time,
                                      eam.status,
                                      eam.Id as request_id,
                                      CASE WHEN eam.location IS NULL THEN "" ELSE eam.location
                                       END as location,
                                      CASE WHEN eam.exhibiotor_id IS NULL THEN eam.attendee_id ELSE eam.attendee_id END as sender_id,
                                      CASE WHEN eam.recipient_attendee_id IS NULL THEN "" ELSE eam.recipient_attendee_id END as receiver_id',false)
        ->from('exhibitor_attendee_meeting eam')
        ->where('eam.moderator_id',$user_id)
        ->where('eam.event_id',$event_id)
        ->where('eam.recipient_attendee_id !=','NULL');
        if($id)
        $this->db->where('eam.Id',$id);
        $data1 =   $this->db->order_by('eam.Id','DESC')->get()->result_array();
        
        $format_time = $this->db->select('format_time')->where('Event_id',$event_id)->get('fundraising_setting')->row_array()['format_time'];
        $format['format'] = ($format_time == '0') ? 'h:i A' : 'H:i:s';

        foreach ($data1 as $key => $value)
        {
            $tmp = $this->getAttendeeDetails($value['sender_id']);
            $data1[$key]['sender_name'] = $tmp[0]->Firstname." ".$tmp[0]->Lastname;
            $tmp = $this->getAttendeeDetails($value['receiver_id']);
            $data1[$key]['reciver_name'] = $tmp[0]->Firstname." ".$tmp[0]->Lastname;
            $data1[$key]['time_new'] = date($format['format'],strtotime($value['time']));
        }
        return $data1;
    }
    public function getAttendeeDetails($attendee_id=NULL,$event_id=NULL)
    {   
        if($event_id == '1397')
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Biography,ac.value',FALSE);
            $this->db->join('attendee_extra_column ac','ac.user_id = u.Id and ac.key1="Country" and ac.event_id='.$event_id);  
        }
        else
        {
            $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,
                           CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Biography',FALSE);
        }
        $this->db->where('u.Id',$attendee_id);
        $query = $this->db->get('user u');
        $res = $query->result();
        foreach ($res as $key => $value)
        {
            $res[$key]->Biography = strip_tags($value->Biography);
            if(!empty($value->value))
            {
                $res[$key]->Company_name .=', '.$value->value;
            }
        }  
        return $res;
    }
}
?>