<?php
class twitter_feed_model extends CI_Model

{
    function __construct()
    {
        $this->db1 = $this->load->database('db1', TRUE);
        parent::__construct();
    }
    public function get_hashtags_list_by_event_id($eid=NULL)

    {
        $this->db->select('*')->from('event_hashtags');
        $this->db->where('event_id', $eid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function checkhashtagsvalid($eid=NULL, $hashtags=NULL, $hashtag_id=NULL)

    {
        $this->db->select('*')->from('event_hashtags');
        $this->db->where('event_id', $eid);
        $this->db->where('hashtags', $hashtags);
        if (!empty($hashtag_id))
        {
            $this->db->where('Id !=', $hashtag_id);
        }
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) > 0)
        {
            return FLASE;
        }
        else
        {
            return TRUE;
        }
    }
    public function add_hashtags($hash=NULL)

    {
        $this->db->insert('event_hashtags', $hash);
    }
    public function getHashtagsById($id=NULL, $event_id = null)

    {
        $this->db->select('*')->from('event_hashtags');
        $this->db->where('Id', $id);
        if ($event_id != null) $this->db->where('event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (!$res) return redirect('Forbidden');
        return $res;
    }
    public function update_hashtags($hid=NULL, $hash=NULL)

    {
        $this->db->where('Id', $hid);
        $this->db->update('event_hashtags', $hash);
    }
    public function deleteHashtags($hid=NULL)

    {
        $this->db->where('Id', $hid);
        $this->db->delete('event_hashtags');
    }
}
?>