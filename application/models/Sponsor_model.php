<?php
class Sponsor_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_sponsor_list($id = null)

    {
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Name', 'Sponsor');
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_sponsor($array_add=NULL)

    {
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Name', 'Sponsor');
        $query = $this->db->get();
        $result = $query->result_array();
        $array_add['Role_id'] = $result[0]['Id'];
        $array_add['Password'] = md5($array_add['Password']);
        $this->db->insert('user', $array_add);
        $this->session->set_flashdata('sponsor_data', 'Added');
        redirect('sponsor');
    }
    public function delete_sponsor($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('user');
        $str = $this->db->last_query();
    }
    public function get_recent_sponsor_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Name', 'Sponsor');
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
?>