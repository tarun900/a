<?php
class Visitor_Event_model extends CI_Model

{
	function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function check_acc_name_valid($acc_name=NULL)

	{
		$this->db1->select('*')->from('organizer_user');
		$this->db1->where('acc_name', $acc_name);
		$qu = $this->db1->get();
		$res = $qu->result_array();
		return $res;
	}
	public function check_org_email_valid($email=NULL)

	{
		$this->db1->select('*')->from('organizer_user');
		$this->db1->where('Email', $email);
		$qu = $this->db1->get();
		$res = $qu->result_array();
		return $res;
	}
	public function check_user_email_valid($email=NULL)

	{
		$this->db->select('*')->from('user');
		$this->db->where('Email', $email);
		$qu = $this->db->get();
		$res = $qu->result_array();
		return $res;
	}
	public function create_super_admin_org($data=NULL)

	{
		$this->db1->insert('organizer_user', $data);
		return $this->db1->insert_id();
	}
	public function create_user_org($data=NULL)

	{
		$this->db->insert('user', $data);
		$org_id = $this->db->insert_id();
		$rel_event['User_id'] = $org_id;
		$rel_event['Organisor_id'] = $org_id;
		$rel_event['Role_id'] = 3;
		$this->db->insert('relation_event_user', $rel_event);
		return $org_id;
	}
	public function add_lead_data($leaddata=NULL)

	{
		$leaddata['created_date'] = date('Y-m-d H:i:s');
		$this->db1->insert('visitor_lead', $leaddata);
	}
	public function get_country_name_by_id($cid=NULL)

	{
		$this->db->select('*')->from('country');
		$this->db->where('id', $cid);
		$res = $this->db->get()->result_array();
		return $res;
	}
}
?>