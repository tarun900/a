<?php
class Qa_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_qa_session_list_by_event($id=NULL, $sid = NULL)

    {
        $this->db->select('qs.*,u.Firstname,u.Lastname')->from('qa_session qs');
        $this->db->join('user u', 'u.Id=qs.Moderator_speaker_id', 'left');
        $this->db->where('qs.Event_id', $id);
        if (!empty($sid))
        {
            $this->db->where('qs.Id', $sid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_session($data=NULL)

    {
        if ($data['session_array']['Event_id'] != NULL) $array_session['Event_id'] = $data['session_array']['Event_id'];
        if ($data['session_array']['Session_name'] != NULL) $array_session['Session_name'] = $data['session_array']['Session_name'];
        if ($data['session_array']['Session_description'] != NULL) $array_session['Session_description'] = $data['session_array']['Session_description'];
        if ($data['session_array']['Moderator_speaker_id'] != NULL) $array_session['Moderator_speaker_id'] = $data['session_array']['Moderator_speaker_id'];
        if ($data['session_array']['start_time'] != NULL) $array_session['start_time'] = $data['session_array']['start_time'];
        if ($data['session_array']['end_time'] != NULL) $array_session['end_time'] = $data['session_array']['end_time'];
        if ($data['session_array']['session_date'] != NULL) $array_session['session_date'] = $data['session_array']['session_date'];
        $array_session['created_date'] = date('Y-m-d H:i:s');
        $this->db->insert('qa_session', $array_session);
        $session_id = $this->db->insert_id();
        return $session_id;
    }
    public function update_session($data=NULL)

    {
        if ($data['session_array']['Event_id'] != NULL) $array_session['Event_id'] = $data['session_array']['Event_id'];
        if ($data['session_array']['Session_name'] != NULL) $array_session['Session_name'] = $data['session_array']['Session_name'];
        if ($data['session_array']['Session_description'] != NULL) $array_session['Session_description'] = $data['session_array']['Session_description'];
        if ($data['session_array']['Moderator_speaker_id'] != NULL) $array_session['Moderator_speaker_id'] = $data['session_array']['Moderator_speaker_id'];
        if ($data['session_array']['start_time'] != NULL) $array_session['start_time'] = $data['session_array']['start_time'];
        if ($data['session_array']['end_time'] != NULL) $array_session['end_time'] = $data['session_array']['end_time'];
        if ($data['session_array']['session_date'] != NULL) $array_session['session_date'] = $data['session_array']['session_date'];
        $this->db->where('Id', $data['session_array']['Id']);
        $this->db->update('qa_session', $array_session);
    }
    public function delete_session($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('qa_session');
    }
    public function get_all_speaker_or_moderator_by_event_id($id=NULL)

    {
        $this->db->select('u.Id,u.Company_name,u.Salutation,u.Firstname,u.Lastname,u.Title,u.Email,u.Logo,u.is_moderator')->from('user u');
        $this->db->join('relation_event_user reu', 'u.Id=reu.User_id', 'left');
        $this->db->where('reu.Event_id', $id);
        $this->db->where('reu.Role_id', 7);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_qa_session_by_event_frontend($eid=NULL, $sid=NULL)

    {
        $this->db->select('qs.*,u.Firstname,u.Lastname')->from('qa_session qs');
        $this->db->join('user u', 'u.Id=qs.Moderator_speaker_id', 'left');
        $this->db->where('qs.Event_id', $eid);
        if (!empty($sid))
        {
            $this->db->where('qs.Id', $sid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_qa_voites_by_modetor($voites=NULL, $msgid=NULL)

    {
        $this->db->select('*')->from('qa_message_votes');
        $this->db->where('message_id', $msgid);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where('message_id', $msgid);
            $this->db->update('qa_message_votes', array(
                'votes' => $voites
            ));
        }
        else
        {
            $this->db->insert('qa_message_votes', array(
                'votes' => $voites,
                'message_id' => $msgid
            ));
        }
    }
}
?>