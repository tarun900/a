<?php
class subscription_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_subscription_list($id = null)

    {
        $this->db->select('*');
        $this->db->from('subscription s');
        if ($id)
        {
            $this->db->where('s.Id', $id);
        }
        $query = $this->db->get();
        $res = $query->result();
        $sub_res = array();
        for ($i = 0; $i < count($res); $i++)
        {
            $sub_res[$i]['Id'] = $res[$i]->id;
            $sub_res[$i]['Name'] = $res[$i]->Name;
            $sub_res[$i]['Description'] = $res[$i]->Description;
            $sub_res[$i]['Created_date'] = $res[$i]->Created_date;
            $sub_res[$i]['Event'] = array();
            $this->db->select('*');
            $this->db->from('event p');
            $this->db->join('event_subscription ps', 'ps.Event_id = p.Id');
            $this->db->where('ps.Subscription_id', $res[$i]->Id);
            $event_query = $this->db->get();
            $event_res = $event_query->result();
            for ($j = 0; $j < count($event_res); $j++)
            {
                $sub_res[$i]['Event'][$j]['Id'] = $event_res[$j]->Event_id;
                $sub_res[$i]['Event'][$j]['Event_name'] = $event_res[$j]->Event_name;
                $sub_res[$i]['Event'][$j]['Common_name'] = $event_res[$j]->Common_name;
                $sub_res[$i]['Event'][$j]['Description'] = $event_res[$j]->Description;
                $sub_res[$i]['Event'][$j]['Images'] = $event_res[$j]->Images;
                $sub_res[$i]['Event'][$j]['Created_date'] = $event_res[$j]->Created_date;
            }
        }
        return $sub_res;
    }
    public function add_subscription($data=NULL)

    {
        $subscription = array(
            'Name' => $data['Subscription_name'],
            'Description' => $data['Description'],
        );
        $this->db->insert('subscription', $subscription);
        $insert_id = $this->db->insert_id();
        if (!empty($data['event']))
        {
            for ($i = 0; $i < count($data['event']); $i++)
            {
                $event = array(
                    'Event_id' => $data['event'][$i],
                    'Subscription_id' => $insert_id
                );
                $this->db->insert('event_subscription', $event);
            }
        }
    }
    public function update_subscription($data=NULL, $id=NULL)

    {
        $subscription = array(
            'Name' => $data['Subscription_name'],
            'Description' => $data['Description'],
        );
        $this->db->where('Id', $id);
        $this->db->update('subscription', $subscription);
        $this->db->where('Subscription_id', $id);
        $this->db->delete('event_subscription');
        for ($i = 0; $i < count($data['event']); $i++)
        {
            $event = array(
                'Event_id' => $data['event'][$i],
                'Subscription_id' => $id
            );
            $this->db->insert('event_subscription', $event);
        }
    }
    public function delete_subscription($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('subscription');
    }
    public function checksubscription($subscription=NULL, $id = null)

    {
        $this->db->select('*');
        $this->db->from('subscription s');
        if ($id)
        {
            $this->db->where('s.Id !=', $id);
        }
        $this->db->where('s.Name', $subscription);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
?>