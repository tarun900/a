<?php
class Speaker_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_all_speakers($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('u.Email');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($orid)
        {
            $this->db->where('u.Organisor_id', $orid);
        }
        if ($id != NULL)
        {
            $this->db->where('ru.Event_id =', $id);
        }
        $this->db->where('r.Id', 7);
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        $string = array();
        if (!empty($res))
        {
            foreach($res as $key => $value)
            {
                $string[] = $value['Email'];
            }
        }
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        if ($id != NULL)
        {
            $this->db->where('ru.Event_id !=', $id);
        }
        if ($orid)
        {
            $this->db->where('u.Organisor_id', $orid);
        }
        if (!empty($string))
        {
            $this->db->where_not_in('u.Email', $string);
        }
        $this->db->where('r.Id', 7);
        $this->db->where('u.Id !=', $orid);
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res1 = $query->result_array();
        return $res1;
    }
    public function get_speaker_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id', 7);
        if ($id)
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->where('r.Id !=', 3);
        $this->db->where('r.Id !=', 4);
        $this->db->where('u.is_moderator', '0');
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_moderator_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->join('role r', 'ru.Role_id = r.Id');
        $this->db->where('r.Id', 7);
        if ($id)
        {
            $this->db->where('ru.Event_id', $id);
        }
        $this->db->where('r.Id !=', 3);
        $this->db->where('r.Id !=', 4);
        $this->db->where('u.is_moderator', '1');
        $this->db->order_by('u.Id desc');
        $this->db->group_by('u.Id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    /*public function get_in_status($id)
    {
    $this->db->select('*');
    $this->db->from('speaker_invitation');
    $this->db->where('user_id',$id);
    $query = $this->db->get();
    $result = $query->result_array();
    $status=$result[0]['status'];
    if($status=='')
    {
    $status=0;
    }
    return $status;
    }*/
    public function add_speaker($array_add=NULL)

    {
        $orid = $this->data['user']->Id;
        $Event_id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id', 7);
        $query = $this->db->get();
        $result = $query->result_array();
        if ($array_add['Facebook_url'] != "")
        {
            $social['Facebook_url'] = $array_add['Facebook_url'];
        }
        unset($array_add['Facebook_url']);
        if ($array_add['Twitter_url'] != "")
        {
            $social['Twitter_url'] = $array_add['Twitter_url'];
        }
        unset($array_add['Twitter_url']);
        if ($array_add['Linkdin_url'] != "")
        {
            $social['Linkedin_url'] = $array_add['Linkdin_url'];
        }
        unset($array_add['Linkdin_url']);
        if ($array_add['Youtube_url'] != "")
        {
            $social['Youtube_url'] = $array_add['Youtube_url'];
        }
        unset($array_add['Youtube_url']);
        if ($array_add['Instagram_url'] != "")
        {
            $social['Instagram_url'] = $array_add['Instagram_url'];
        }
        unset($array_add['Instagram_url']);
        $array_add['Password'] = md5($array_add['Password']);
        $array_add['Active'] = '1';
        $array_add['Organisor_id'] = $orid;
        $speaker_type = $array_add['Speaker_type'];
        unset($array_add['Speaker_type']);
        if (empty($array_add['Email']))
        {
            $array_add['Email'] = str_replace(' ', '', $array_add['Firstname'] . '-' . $array_add['Lastname'] . '-' . uniqid() . '@venturiapps.com');
        }
        $this->db->insert('user', $array_add);
        $speaker_id = $this->db->insert_id();
        $array_relation_add['User_id'] = $this->db->insert_id();
        $array_relation_add['Event_id'] = $Event_id;
        $array_relation_add['Organisor_id'] = $orid;
        $array_relation_add['Role_id'] = $result[0]['Id'];
        $this->db->insert('relation_event_user', $array_relation_add);
        if (!empty($speaker_type)):
            $insert['event_id'] = $Event_id;
            $insert['user_id'] = $speaker_id;
            $insert['type'] = $speaker_type;
            $this->db->insert('speaker_profile', $insert);
        endif;
        if (!empty($social))
        {
            $social['User_id'] = $speaker_id;
            $this->db->insert('user_social_links', $social);
        }
        return $speaker_id;
    }
    public function edit_speaker($array_add=NULL, $event_id = NULL)

    {
        $user_id = $array_add['Id'];
        unset($array_add['Id']);
        $social_id = $array_add['Social_id'];
        unset($array_add['Social_id']);
        unset($array_add['Email_update']);
        $social['Facebook_url'] = $array_add['Facebook_url'];
        unset($array_add['Facebook_url']);
        $social['Twitter_url'] = $array_add['Twitter_url'];
        unset($array_add['Twitter_url']);
        $social['Linkedin_url'] = $array_add['Linkdin_url'];
        unset($array_add['Linkdin_url']);
        $social['Youtube_url'] = $array_add['Youtube_url'];
        unset($array_add['Youtube_url']);
        $social['Instagram_url'] = $array_add['Instagram_url'];
        unset($array_add['Instagram_url']);
        if ($array_add['Password_update'] != "")
        {
            $array_add['Password'] = $array_add['Password_update'];
        }
        unset($array_add['Password_update']);
        unset($array_add['Password_again_update']);
        $speaker_type = $array_add['Speaker_type'];
        unset($array_add['Speaker_type']);
        $this->db->where('Id', $social_id);
        $this->db->update('user_social_links', $social);
        $this->db->where('Id', $user_id);
        $this->db->update('user', $array_add);
        $this->db->where('user_id', $user_id);
        $this->db->where('event_id', $event_id);
        $res = $this->db->get('speaker_profile')->row_array();
        if (empty($res))
        {
            $insert['event_id'] = $event_id;
            $insert['user_id'] = $user_id;
            $insert['type'] = $speaker_type ? : NULL;
            $this->db->insert('speaker_profile', $insert);
        }
        else
        {
            $update['type'] = $speaker_type ? : NULL;
            $this->db->where($res);
            $this->db->update('speaker_profile', $update);
        }
    }
    public function add_speakers($array_add=NULL)

    {
        $orid = $this->data['user']->Id;
        $Event_id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('role');
        $this->db->where('Id', 7);
        $query = $this->db->get();
        $result = $query->result_array();
        if ($Event_id == '447' && $array_add['is_moderator'] != '1')
        {
            $this->load->library('RC4');
            $rc4 = new RC4();
            $array_add['Password'] = $rc4->encrypt($array_add['Password']);
        }
        else
        {
            $array_add['Password'] = md5($array_add['Password']);
        }
        $array_add['Active'] = '1';
        $array_add['Organisor_id'] = $orid;
        if (empty($array_add['Email']))
        {
            $array_add['Email'] = str_replace(' ', '', $array_add['Firstname'] . '-' . $array_add['Lastname'] . '-' . uniqid() . '@venturiapps.com');
        }
        $this->db->insert('user', $array_add);
        $speaker_id = $this->db->insert_id();
        /* $array_speakers_events['Speaker_id'] = $this->db->insert_id();
        $array_speakers_events['Event_id'] = $Event_id;
        $array_speakers_events['Agenda_id'] = $array_add['Agenda_id'];
        $this->db->insert('speakers_agenda', $array_speakers_events);*/
        $array_relation_add['User_id'] = $speaker_id;
        $array_relation_add['Event_id'] = $Event_id;
        $array_relation_add['Role_id'] = $result[0]['Id'];
        $array_relation_add['Organisor_id'] = $orid;
        $this->db->insert('relation_event_user', $array_relation_add);
        return $speaker_id;
    }
    public function add_exist_speaker($data=NULL)

    {
        $user = $this->session->userdata('current_user');
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        if ($data['speaker_array']['User_id'] != NULL) $array_speaker['User_id'] = $data['speaker_array']['User_id'];
        if ($data['speaker_array']['Event_id'] != NULL) $array_speaker['Event_id'] = $data['speaker_array']['Event_id'];
        if ($data['speaker_array']['Role_id'] != NULL) $array_speaker['Role_id'] = $data['speaker_array']['Role_id'];
        if ($Organisor_id != NULL) $array_speaker['Organisor_id'] = $Organisor_id;
        $this->db->insert('relation_event_user', $array_speaker);
        $speaker_id = $this->db->insert_id();
        return $speaker_id;
    }
    public function delete_speaker($Event_id=NULL, $id=NULL)

    {
        $this->db->where('User_id', $id);
        $this->db->where('Event_id', $Event_id);
        $this->db->delete('relation_event_user');
        $str = $this->db->last_query();
        $this->db->select('u.*');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
        $this->db->where('reu.Event_id', $Event_id);
        $this->db->where('reu.Role_id', '7');
        $this->db->order_by('updated_date', 'desc')->limit('1');
        $res = $this->db->get('user u')->row_array();
        if (!empty($res))
        {
            $update['updated_date'] = date('Y-m-d H:i:s', strtotime($res['updated_date']) + '10 sec');
            $this->db->where($res);
            $this->db->update('user', $update);
        }
    }
    public function get_recent_speaker_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Id', 7);
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_speaker_event()

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('speakers_agenda s');
        $this->db->join('agenda a', 's.Speaker_id = a.Speaker_id');
        if ($orid)
        {
            $this->db->where('s.Speaker_id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_agenda_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('speakers_agenda s');
        $this->db->join('agenda a', 's.Agenda_id = a.Id');
        if ($orid)
        {
            $this->db->where('s.Speaker_id', $orid);
        }
        $this->db->where('s.Agenda_id', $orid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function checkemail($email=NULL, $id = null, $eventid = null)

    {
        $this->db->select('u.Id as uid,ru.*,u.*');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        if ($id != NULL)
        {
            $this->db->where('u.Id !=', $id);
        }
        $this->db->where('u.Email', $email);
        $this->db->where('ru.Event_id', $eventid);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /*public function get_speaker_data($id)
    {
    $this->db->select('*,us.Id as social_id,u.Id as Id');
    $this->db->from('user u');
    $this->db->where('u.Id',$id);
    $this->db->join('user_social_links us','us.User_id = u.Id','left');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
    }*/
    public function get_speaker_data($id=NULL, $event_id = null)

    {
        $this->db->select('*,us.Id as social_id,u.Id as Id,sp.type as speaker_type');
        $this->db->from('user u');
        if ($event_id != null)
        {
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            $this->db->join('speaker_profile sp', 'sp.user_id = u.Id', 'left');
            $this->db->where('(sp.event_id = ' . $event_id . ' OR sp.event_id IS NULL)');
            $this->db->where('reu.Event_id', $event_id);
            $this->db->where('reu.Role_id', '7');
        }
        $this->db->where('u.Id', $id);
        $this->db->join('user_social_links us', 'us.User_id = u.Id', 'left');
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'edit_speaker') return redirect('Forbidden');
        return $res;
    }
    /*public function get_moderator_data($id,$eid)
    {
    $this->db->select('*,us.Id as social_id,u.Id as Id');
    $this->db->from('user u');
    $this->db->where('u.Id',$id);
    $this->db->join('user_social_links us','us.User_id = u.Id','left');
    $query = $this->db->get();
    $res = $query->result_array();
    return $res;
    }*/
    public function get_moderator_data($id=NULL, $eid=NULL)

    {
        $this->db->select('*,us.Id as social_id,u.Id as Id');
        $this->db->from('user u');
        if ($eid != null)
        {
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            $this->db->where('reu.Event_id', $eid);
            $this->db->where('reu.Role_id', '7');
        }
        $this->db->where('u.Id', $id);
        $this->db->where('u.is_moderator', '1');
        $this->db->join('user_social_links us', 'us.User_id = u.Id', 'left');
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'edit_moderator') return redirect('Forbidden');
        return $res;
    }
    public function update_keypeople($id=NULL, $data=NULL)

    {
        $this->db->where('Id', $id);
        $update_data['key_people'] = $data;
        $this->db->update("user", $update_data);
    }
    public function add_csv_speaker($id=NULL, $speaker_data=NULL, $user_link=NULL)

    {
        $this->db->insert('user', $speaker_data);
        $user_id = $this->db->insert_id();
        $array_relation_add['User_id'] = $user_id;
        $array_relation_add['Event_id'] = $id;
        $array_relation_add['Role_id'] = '7';
        $array_relation_add['Organisor_id'] = $speaker_data['Organisor_id'];
        $this->db->insert('relation_event_user', $array_relation_add);
        $user_link['User_id'] = $user_id;
        $this->db->insert('user_social_links', $user_link);
        return $user_id;
    }
    public function add_moderators_relation($mod_rel=NULL)

    {
        $this->db->select('*')->from('moderator_relation');
        $this->db->where('user_id', $mod_rel['user_id']);
        $this->db->where('moderator_id', $mod_rel['moderator_id']);
        $this->db->where('event_id', $mod_rel['event_id']);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (count($res) < 1)
        {
            $this->db->insert('moderator_relation', $mod_rel);
        }
    }
    public function get_speaker_list_by_moderator($id=NULL, $eventid=NULL)

    {
        $this->db->select('*')->from('moderator_relation');
        $this->db->where('moderator_id', $id);
        $this->db->where('event_id', $eventid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_speaker_type_list($id=NULL)

    {
        $this->db->select('*')->from('speaker_type');
        $this->db->where('event_id', $id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function get_edit_speaker_type($id=NULL, $tid=NULL)

    {
        $this->db->select('*')->from('speaker_type');
        $this->db->where('event_id', $id);
        $this->db->where('type_id', $tid);
        $qu = $this->db->get();
        $res = $qu->result_array();
        if (!$res && $this->uri->segment(2) == 'speaker_type_edit') return redirect('Forbidden');
        return $res;
    }
    public function save_speaker_type($typedata=NULL)

    {
        $typedata['type_ucode'] = substr(sha1(uniqid()) , 0, 5);
        $this->db->insert('speaker_type', $typedata);
        return $this->db->insert_id();
    }
    public function remove_speaker_type($id=NULL, $tid=NULL)

    {
        $this->db->where('event_id', $id);
        $this->db->where('type_id', $tid);
        $this->db->delete('speaker_type');
        $st_data['st_id'] = NULL;
        $this->db->where('st_id', $tid);
        $this->db->update('sponsors', $st_data);
        if ($id != '')
        {
            $a_cat = $this->db->select('*')->from('speaker_type')->where('event_id', $id)->order_by('type_id', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s', strtotime('+1 seconds' . $a_cat['updated_date']));
            $this->db->where('type_id', $a_cat['type_id'])->update('speaker_type', $update);
        }
    }
    public function update_speaker_type($tdata=NULL, $eid=NULL, $tid=NULL)

    {
        $this->db->where('event_id', $eid);
        $this->db->where('type_id', $tid);
        $this->db->update('speaker_type', $tdata);
    }
    public function assign_spear_type($eid=NULL, $speaker_type=NULL, $speaker_id=NULL)

    {
        $data['event_id'] = $eid;
        $type_id = $this->db->where($data)->where('type_name', $speaker_type)->get('speaker_type')->row_array() ['type_id'];
        if (!empty($type_id))
        {
            $data['user_id'] = $speaker_id;
            if ($this->db->where($data)->get('speaker_profile')->row_array())
            {
                $update['type'] = $type_id;
                $this->db->where($data)->update('speaker_profile', $update);
            }
            else
            {
                $data['type'] = $type_id;
                $this->db->insert('speaker_profile', $data);
            }
        }
    }
    public function add_speaker_categorie($cdata=NULL) //Monday 26 March 2018 06:08:00 PM IST

    {
        $cdata['created_date'] = date('Y-m-d H:i:s');
        $this->db->insert('attendee_category', $cdata);
        return $this->db->insert_id();
    }
    public function update_speaker_categorie($cdata=NULL, $cid=NULL) //Monday 26 March 2018 06:08:05 PM IST

    {
        $this->db->where('id', $cid);
        $this->db->update('attendee_category', $cdata);
    }
    public function get_speaker_categories_by_event($eid=NULL, $cid = NULL) //Monday 26 March 2018 06:08:10 PM IST

    {
        $this->db->select('*')->from('attendee_category');
        if (!empty($cid))
        {
            $this->db->where('id', $cid);
        }
        $this->db->where('menu_id', '7');
        $this->db->where('event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function delete_speaker_categorie($cid=NULL, $event_id = '') //Monday 26 March 2018 06:08:15 PM IST

    {
        $this->db->where('id', $cid);
        $this->db->delete('attendee_category');
        if ($event_id != '')
        {
            $a_cat = $this->db->select('*')->from('attendee_category')->where('event_id', $event_id)->order_by('updated_date', 'DESC')->get()->row_array();
            $update['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $a_cat['id'])->update('attendee_category', $update);
        }
    }
    public function assign_speaker_categories($insert_data=NULL, $event_id=NULL)

    {
        $keywords = explode(',', $insert_data['keyword']);
        foreach($keywords as $key => $value)
        {
            $insert['keyword'] = $value;
            $insert['user_id'] = $insert_data['user_id'];
            $insert['event_id'] = $event_id;
            $res = $this->db->where($insert)->get('attendee_keywords')->row_array();
            if (empty($res))
            {
                $this->db->insert('attendee_keywords', $insert);
            }
        }
    }
    public function downloadDeepLinks($id=NULL)

    {
        $org_id = $this->db->where('Id', $id)->get('event')->row_array() ['Organisor_id'];
        $version_code = $this->db->where('org_id', $org_id)->get('version_code')->row_array();
        if (!empty($version_code['ios_app_id']) || !empty($version_code['android_app_id'])) $host = strtolower($version_code['app']);
        else $host = 'allintheloop';
        $this->load->dbutil();
        // $this->db->select('CONCAT_WS(" ",u.Firstname,u.Lastname) as Speaker,concat("https://www.allintheloop.net/apiv4/deeplink?h=allintheloop&node_main=Nw==&node_sub=",TO_BASE64(u.Id),"&nevent=",TO_BASE64(reu.Event_id)) as Deeplink',false);
        // allintheloop://m.allintheloop.com?node_main=MQ==&node_sub=MzU0NjI=
        $this->db->select('CONCAT_WS(" ",u.Firstname,u.Lastname) as Speaker,concat("' . $host . '://m.allintheloop.com?node_main=Nw==&node_sub=",TO_BASE64(u.Id),"&nevent=",TO_BASE64(reu.Event_id)) as Deeplink', false);
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
        $this->db->where('reu.Event_id', $id);
        $this->db->where('reu.Role_id', '7');
        $res = $this->db->get('user u');
        $backup = $this->dbutil->csv_from_result($res);
        $this->load->helper('file');
        write_file('speaker_deeplinks.csv', $backup);
        $this->load->helper('download');
        force_download('speaker_deeplinks.csv', $backup);
    }
}
?>