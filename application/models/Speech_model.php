<?php
class Speech_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_speech_list($id = null)

    {
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Event_id', $id);
        }
        $this->db->where('r.Id', 7);
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        $res = $query->result_array();
        return $res;
    }
    public function add_speech($data=NULL)

    {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        if (!empty($data['speech_array']['Documents']))
        {
            foreach($data['speech_array']['Documents'] as $k => $v)
            {
                $Documents[] = $v;
            }
        }
        $array_speech['Documents'] = json_encode($Documents);
        if ($data['speech_array']['Speaker_id'] != NULL) $array_speech['Speaker_id'] = $data['speech_array']['Speaker_id'];
        if ($data['speech_array']['Speech_text'] != NULL) $array_speech['Speech_text'] = $data['speech_array']['Speech_text'];
        $this->db->insert('speech', $array_speech);
        $speech_id = $this->db->insert_id();
        $array_speakers_agenda['Event_id'] = $data['speech_array']['Event_id'];
        $array_speakers_agenda['Agenda_id'] = $data['speech_array']['Agenda_id'];
        $array_speakers_agenda['Speaker_id'] = $logged_in_user_id;
        $array_speakers_agenda['Speech_id'] = $this->db->insert_id();
        $this->db->insert('speakers_agenda', $array_speakers_agenda);
        return $speech_id;
    }
    public function get_speech_event($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('speakers_agenda s');
        $this->db->join('agenda a', 'a.Speaker_id = s.Speaker_id');
        if ($orid)
        {
            $this->db->where('s.Speaker_id', $orid);
        }
        $this->db->order_by('s.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function update_speech_event($data=NULL)

    {
        $orid = $this->data['user']->Id;
        $documents = array();
        if (!empty($data['speech_array']['Documents']))
        {
            foreach($data['speech_array']['Documents'] as $k => $v)
            {
                $documents[] = $v;
            }
        }
        $array_speech['Documents'] = json_encode($documents);
        if (!empty($data['speech_array']['old_documents']))
        {
            foreach($data['speech_array']['old_documents'] as $k => $v)
            {
                $documents[] = $v;
            }
        }
        $array_speech['Documents'] = json_encode($documents);
        if ($data['speech_array']['Speaker_id'] != NULL) $array_speech['Speaker_id'] = $orid;
        if ($data['speech_array']['Speech_text'] != NULL) $array_speech['Speech_text'] = $data['speech_array']['Speech_text'];
        $this->db->where('Id', $data['speech_array']['Id']);
        $this->db->update('speech', $array_speech);
        // echo $speech_array->Event_id; exit;
        // print_r($data);
        // echo $data['speech_array']['Event_id']; exit;
        // echo $this->db->last_query(); exit;
        $array_speakers_agenda['Event_id'] = $data['speech_array']['Event_id'];
        $array_speakers_agenda['Agenda_id'] = $speech_array['Agenda_id'];
        $array_speakers_agenda['Speaker_id'] = $orid;
        $this->db->where('Speech_id', $this->uri->segment(3));
        $this->db->update('speakers_agenda', $array_speakers_agenda);
        // echo $this->db->last_query(); exit();
    }
    public function delete_speech($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('speech');
        $str = $this->db->last_query();
    }
    public function get_recent_speech_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*,r.Id as Rid,u.Id as uid,u.Active');
        $this->db->from('user u');
        $this->db->join('role r', 'u.Role_id = r.Id');
        if ($id)
        {
            $this->db->where('u.Id', $id);
        }
        $this->db->where('r.Id', 7);
        $this->db->order_by('u.Id desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_speaker_event($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('event e', 'e.Id = u.Event_id');
        if ($orid)
        {
            $this->db->where('u.Id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_speaker_agenda($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('event e', 'e.Id = u.Event_id');
        if ($orid)
        {
            $this->db->where('u.Id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        $orid = $this->data['user']->Id;
        $this->db->select('a.*');
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->where('e.Id', $res[0]['Id']);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_speaker_speech($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('event e', 'e.Id = u.Event_id');
        if ($orid)
        {
            $this->db->where('u.Id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('s.id as sid,s.*,e.Id,e.Start_date,e.End_date');
        $this->db->from('speech s');
        $this->db->join('speakers_agenda s1', 's1.Speaker_id = s.Speaker_id');
        $this->db->join('event e', 'e.Id = s1.Event_id');
        $this->db->where('s.Speaker_id', $orid);
        $this->db->group_by('s.Id');
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }
    public function get_speaker_speech_by_id($id = null)

    {
        $orid = $this->data['user']->Id;
        $speech_id = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('event e', 'e.Id = u.Event_id');
        if ($orid)
        {
            $this->db->where('u.Id', $orid);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('s.id as sid,s.*,e.Id,e.Start_date,e.End_date,s1.*');
        $this->db->from('speech s');
        $this->db->join('speakers_agenda s1', 's1.Speaker_id = s.Speaker_id');
        $this->db->join('event e', 'e.Id = s1.Event_id');
        $this->db->where('s.Id', $speech_id);
        $this->db->group_by('s.Id');
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        $res = $query->result_array();
        return $res;
    }
}
?>