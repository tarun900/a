<?php
class Notes_admin_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_notes_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('notes');
        if ($orid)
        {
            // $this->db->where('Organisor_id',$orid);
        }
        $this->db->where('Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_notes($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('notes');
        if ($orid)
        {
            // $this->db->where('Organisor_id',$orid);
        }
        $this->db->where('Event_id', $id);
        $this->db->where('Id', $this->uri->segment(4));
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function add_notes($data=NULL)

    {
        if ($data['notes_array']['User_id'] != NULL) $array_notes['User_id'] = $data['notes_array']['User_id'];
        if ($data['notes_array']['Organisor_id'] != NULL) $array_notes['Organisor_id'] = $data['notes_array']['Organisor_id'];
        if ($data['notes_array']['Event_id'] != NULL) $array_notes['Event_id'] = $data['notes_array']['Event_id'];
        if ($data['notes_array']['Heading'] != NULL) $array_notes['Heading'] = $data['notes_array']['Heading'];
        if ($data['notes_array']['Description'] != NULL) $array_notes['Description'] = $data['notes_array']['Description'];
        if ($data['notes_array']['Created_at'] != NULL) $array_notes['Created_at'] = $data['notes_array']['Created_at'];
        $this->db->insert('notes', $array_notes);
        $notes_id = $this->db->insert_id();
        return $notes_id;
    }
    public function update_notes($data=NULL)

    {
        $orid = $this->data['user']->Id;
        if ($data['notes_array']['Heading'] != NULL) $array_notes['Heading'] = $data['notes_array']['Heading'];
        if ($data['notes_array']['Description'] != NULL) $array_notes['Description'] = $data['notes_array']['Description'];
        $array_notes['Event_id'] = $this->uri->segment(3);
        $array_notes['Organisor_id'] = $orid;
        $array_notes['User_id'] = $orid;
        $this->db->where('Id', $this->uri->segment(4));
        $this->db->update('notes', $array_notes);
    }
    public function delete_notes($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('notes');
        $str = $this->db->last_query();
    }
}
?>