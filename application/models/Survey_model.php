<?php
class Survey_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_survey_list($id = null, $sid=NULL)

    {
        $orid = $this->data['org'];
        $this->db->select('s.*');
        $this->db->from('survey s');
        $this->db->join('survey_category_relation scr', 's.Id=scr.Question_id');
        $this->db->where('scr.survey_category_id', $sid);
        $this->db->where('s.Event_id', $id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_survey_list_in_presentation($id=NULL)

    {
        $this->db->select('s.*');
        $this->db->from('survey s');
        $this->db->where('s.Event_id', $id);
        $this->db->where('s.Question_type', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_survey_screens($id = null, $sid=NULL)

    {
        $this->db->select('*');
        $this->db->from('survey_data');
        $this->db->where('Event_id', $id);
        $this->db->where('surveys_id', $sid);
        $query = $this->db->get();
        $res = $query->result_array();
        // if(!$res && $this->uri->segment(2)=='question_index')
        // echo "eee"; exit();
        // return redirect('Forbidden');
        return $res;
    }
    public function add_welcome_screen($arr=NULL)

    {
        $this->db->select('Event_id')->from('survey_data');
        $this->db->where('Event_id', $arr['Event_id']);
        $this->db->where('surveys_id', $arr['surveys_id']);
        $query = $this->db->get();
        $res = $query->result_array();
        if (count($res) > 0)
        {
            $this->db->where('Event_id', $arr['Event_id']);
            $this->db->where('surveys_id', $arr['surveys_id']);
            $this->db->update('survey_data', $arr);
        }
        else
        {
            $this->db->insert('survey_data', $arr);
        }
    }
    public function update_screen($id=NULL, $sid=NULL, $data=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->where('surveys_id', $sid);
        $this->db->update('survey_data', $data);
    }
    public function delete_welcome_screen($id=NULL)

    {
        $arr['welcome_data'] = "";
        $this->db->where('Id', $id);
        $this->db->update('survey_data', $arr);
    }
    public function delete_thanks_screen($id=NULL)

    {
        $arr['thanku_data'] = "";
        $this->db->where('Id', $id);
        $this->db->update('survey_data', $arr);
    }
    public function get_edit_survey_list($id = null)

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('survey');
        if ($orid)
        {
            // $this->db->where('Organisor_id',$orid);
        }
        $this->db->where('Event_id', $id);
        $this->db->where('Id', $this->uri->segment(4));
        $query = $this->db->get();
        $res = $query->result_array();
        if (!$res && $this->uri->segment(2) == 'edit') return redirect('Forbidden');
        return $res;
    }
    public function add_survey($data=NULL, $sid=NULL)

    {
        if ($data['survey_array']['Organisor_id'] != NULL) $array_survey['Organisor_id'] = $data['survey_array']['Organisor_id'];
        if ($data['survey_array']['Event_id'] != NULL) $array_survey['Event_id'] = $data['survey_array']['Event_id'];
        if ($data['survey_array']['Question'] != NULL) $array_survey['Question'] = $data['survey_array']['Question'];
        if ($data['survey_array']['Question_type'] != NULL) $array_survey['Question_type'] = $data['survey_array']['Question_type'];
        if ($data['survey_array']['Option'] != NULL) $array_survey['Option'] = $data['survey_array']['Option'];
        if ($data['survey_array']['Answer'] != NULL) $array_survey['Answer'] = $data['survey_array']['Answer'];
        $array_survey['created_date'] = date('Y-m-d H:i:s');
        $this->db->insert('survey', $array_survey);
        $social_id = $this->db->insert_id();
        $rel_data['survey_category_id'] = $sid;
        $rel_data['Question_id'] = $social_id;
        $this->db->insert('survey_category_relation', $rel_data);
        return $social_id;
    }
    public function update_survey($data=NULL)

    {
        $orid = $this->data['user']->Id;
        if ($data['survey_array']['Question'] != NULL) $array_survey['Question'] = $data['survey_array']['Question'];
        if ($data['survey_array']['Question_type'] != NULL) $array_survey['Question_type'] = $data['survey_array']['Question_type'];
        if ($data['survey_array']['Option'] != NULL) $array_survey['Option'] = $data['survey_array']['Option'];
        if ($data['survey_array']['Answer'] != NULL) $array_survey['Answer'] = $data['survey_array']['Answer'];
        $array_survey['Event_id'] = $this->uri->segment(3);
        $array_survey['Organisor_id'] = $orid;
        $this->db->where('Id', $this->uri->segment(4));
        $this->db->update('survey', $array_survey);
    }
    public function delete_survey($id=NULL)

    {
        $this->db->where('Id', $id);
        $this->db->delete('survey');
        $str = $this->db->last_query();
    }
    public function change_question_hide_status_by_question_id($eid=NULL, $qid=NULL)

    {
        $this->db->select('*')->from('survey');
        $this->db->where('Event_id', $eid);
        $this->db->where('Id', $qid);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $qdata['show_question'] = $res[0]['show_question'] == '0' ? '1' : '0';
            $this->db->where('Event_id', $eid);
            $this->db->where('Id', $qid);
            $this->db->update('survey', $qdata);
            return $this->db->affected_rows();
        }
    }
    public function get_surveys_category_by_name($snm=NULL, $eventid=NULL, $sid=NULL)

    {
        $this->db->select('*')->from('survey_category');
        $this->db->where('survey_name', $snm);
        $this->db->where('event_id', $eventid);
        if (!empty($sid))
        {
            $this->db->where('survey_id !=', $sid);
        }
        $qu = $this->db->get();
        $res = $qu->result_array();
        return $res;
    }
    public function add_survey_category($data=NULL, $eventid=NULL)

    {
        $data['event_id'] = $eventid;
        $this->db->insert('survey_category', $data);
        return $this->db->insert_id();
    }
    public function update_survey_category($data=NULL, $sid=NULL)

    {
        $this->db->where('survey_id', $sid);
        $this->db->update('survey_category', $data);
        return $sid;
    }
    public function delete_survey_category_by_id($sid=NULL)

    {
        $this->db->where('survey_id', $sid);
        $this->db->delete('survey_category');
    }
    public function get_all_surveys_category_list_by_event($eid=NULL)

    {
        $this->db->select('sc.*,count(scr.Question_id) as total_question')->from('survey_category sc');
        $this->db->join('survey_category_relation scr', 'sc.survey_id=scr.survey_category_id', 'left');
        $this->db->where('sc.event_id', $eid);
        $this->db->group_by('sc.survey_id');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_edit_survey_category_by_id($eid=NULL, $sid=NULL)

    {
        $this->db->select('sc.*,count(scr.Question_id) as total_question')->from('survey_category sc');
        $this->db->join('survey_category_relation scr', 'sc.survey_id=scr.survey_category_id', 'left');
        $this->db->where('sc.event_id', $eid);
        $this->db->where('sc.survey_id', $sid);
        $this->db->group_by('sc.survey_id');
        $res = $this->db->get()->result_array();
        // echo $this->db->last_query(); exit();
        // if($res && $this->uri->segment(2)=='question_index')
        // return redirect('Forbidden');
        return $res;
    }
    public function change_surveys_hide_status_by_surveys_id($eid=NULL, $sid=NULL)

    {
        $this->db->select('*')->from('survey_category');
        $this->db->where('event_id', $eid);
        $this->db->where('survey_id', $sid);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $qdata['show_survey'] = $res[0]['show_survey'] == '0' ? '1' : '0';
            $this->db->where('event_id', $eid);
            $this->db->where('survey_id', $sid);
            $this->db->update('survey_category', $qdata);
            return $this->db->affected_rows();
        }
    }
    public function get_exporting_surveys_by_event($eid=NULL, $sid=NULL)

    {
        $this->db->select('s.Question,sc.survey_name,ps.Answer,u.Company_name,u.Firstname,u.Lastname,u.Title,u.Email,ea.extra_column')->from('survey s');
        $this->db->join('survey_category_relation scr', 'scr.Question_id=s.Id');
        $this->db->join('survey_category sc', 'sc.survey_id=scr.survey_category_id');
        $this->db->join('poll_survey ps', 'ps.Question_id=s.Id');
        $this->db->join('user u', 'u.Id=ps.User_id');
        $this->db->join('event_attendee ea', 'u.Id=ea.Attendee_id and ea.Event_id=' . $eid, 'left');
        if (!empty($sid))
        {
            $this->db->where('sc.survey_id', $sid);
        }
        $this->db->group_by('s.Id');
        $this->db->group_by('u.Id');
        $this->db->where('s.Event_id', $eid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function add_clone_entry($event_id=NULL, $survey_id=NULL, $count=NULL)

    {
        for ($i = 1; $i <= $count; $i++)
        {
            $category = $this->db->where('survey_id', $survey_id)->get('survey_category')->row_array();
            unset($category['survey_id']);
            $this->db->insert('survey_category', $category);
            $new_survey = $this->db->insert_id();
            $this->db->select('s.*')->from('survey s');
            $this->db->join('survey_category_relation scr', 's.Id=scr.Question_id');
            $this->db->where('scr.survey_category_id', $survey_id);
            $this->db->where('s.Event_id', $event_id);
            $Questions = $this->db->get()->result_array();
            foreach($Questions as $qkey => $oldQuestion)
            {
                $newQuestion = $oldQuestion;
                unset($newQuestion['Id']);
                $this->db->insert('survey', $newQuestion);
                $rel_data['Question_id'] = $this->db->insert_id();
                unset($newQuestion);
                $rel_data['survey_category_id'] = $new_survey;
                $this->db->insert('survey_category_relation', $rel_data);
                unset($rel_data);
            }
            unset($new_survey);
        }
    }
}
?>