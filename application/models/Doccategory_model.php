<?php

class Doccategory_model extends CI_Model
{

     function __construct()
     {
          parent::__construct();
     }

     /*public function get_doccategory_list($id = null,$evnetid=null,$isactive=null)
     {
          $this->db->select('*');
          $this->db->from('doccategory dc');
          
          if($id!=NULL)
          {
               $this->db->where('id', $id);
          }
          
          if($evnetid!=NULL)
          {
               $this->db->where('Event_id', $evnetid);
          }
          
          if($isactive!=NULL)
          {
               $this->db->where('status', $isactive);
          }
          
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }*/

     /*public function add_doccategory($array_add)
     {
          $this->db->insert('doccategory', $array_add);
          $this->session->set_flashdata('user_data', 'Added');
          return 1;
     }*/
     
    /* public function edit_doccategory($id,$array_add)
     {
          $this->db->where('Id',$id);
          $this->db->update('doccategory',$array_add);
          return 1;
     }*/

    /* public function delete_doccategory($id)
     {
          $this->db->where('id', $id);
          $this->db->delete('doccategory');
          $str = $this->db->last_query();
     }*/
     
     public function get_unique_doccategory_list($eventid=NULL, $id=null)
     {
          $this->db->select('d.*');
          $this->db->from('documents d');
          if($eventid!=NULL)
          {
               $this->db->where('d.Event_id', $eventid);
          }
          $this->db->group_by('d.id');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     
     public function get_doccategory_documents_list($eventid=NULL, $docid=null,$isparent=null)
     {
          $this->db->select('d.*');
          $this->db->from('documents d');
          if($isparent===0)
          {
               $this->db->where('d.parent', '0');
          }
          else
          {
               if($docid!=NULL)
               {    
                    $this->db->where('d.parent',$docid);
               }
          }          
          
          if($eventid!=NULL)
          {
               $this->db->where('d.Event_id', $eventid);
          }
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
     }
     
    

     public function getalldocumentfilenames($eventid=NULL,$docid=NULL)
     {
          
          $this->db->select('*');
          $this->db->from('documents d');
          $this->db->where('parent', $docid);
          $this->db->where('Event_id', $eventid);
          $this->db->where('d.doc_type', '1');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
          
     }
     /*public function getcoverimage($eventid,$docid)
     {
          
          if($docid==0)
          {
               $this->db->select('coverimages');
               $this->db->from('document_coverimge dc');
               $this->db->where('dc.Event_id', $eventid);
               $query = $this->db->get();
               $res = $query->result_array();
          }
          else
          {
               $this->db->select('coverimages');
               $this->db->from('documents d');
               $this->db->where('d.id', $docid);
               $this->db->where('d.Event_id', $eventid);
               $query = $this->db->get();
               $res = $query->result_array();
          }
          
         
          if(!empty($res))
          {
               $coverimage=$res[0]['coverimages'];               
          }
          else
          {
               $coverimage='';
          }
          
          return $coverimage;
     }*/
     
     /*public function uploadcover($eventid,$coverimage)
     {
          
          $array_add=array(
              "Event_id" =>$eventid,
              "coverimages"=>$coverimage
          );
          
          $coverimage=$this->getcoverimage($eventid,0);
          
          if(empty($coverimage))
          {
               $this->db->insert('document_coverimge', $array_add);
               $this->session->set_flashdata('user_data', 'Added');
          }
          else
          {
               $this->db->where('Event_id', $eventid);
               $this->db->update('document_coverimge', $array_add);
          }
          return 1;
     }*/
     
     /*public function deletecover($eventid)
     {    
          $this->db->where('Event_id', $eventid);
          $this->db->delete('document_coverimge');
          return 1;
     }*/
     
}

?>