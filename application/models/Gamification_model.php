<?php
class Gamification_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_all_rank_type()

    {
        $res = $this->db->get('game_rank')->result_array();
        return $res;
    }
    public function save_game_rank_point($rankpoint=NULL, $rank_id=NULL, $eid=NULL)

    {
        $rank_data['rank_id'] = $rank_id;
        $rank_data['event_id'] = $eid;
        $res = $this->db->select('*')->from('game_rank_point')->where($rank_data)->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where($rank_data);
            $this->db->update('game_rank_point', array(
                'point' => $rankpoint
            ));
        }
        else
        {
            $rank_data['point'] = $rankpoint;
            $this->db->insert('game_rank_point', $rank_data);
        }
    }
    public function get_all_rank_point_by_event($eid=NULL)

    {
        $res = $this->db->select('*')->from('game_rank_point')->where('event_id', $eid)->get()->result_array();
        return $res;
    }
    public function save_game_rank_color($rankcolor=NULL, $rank_no=NULL, $eid=NULL)

    {
        $rank_data['rank_no'] = $rank_no;
        $rank_data['event_id'] = $eid;
        $res = $this->db->select('*')->from('game_rank_color')->where($rank_data)->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where($rank_data);
            $this->db->update('game_rank_color', array(
                'color' => $rankcolor
            ));
        }
        else
        {
            $rank_data['color'] = $rankcolor;
            $this->db->insert('game_rank_color', $rank_data);
        }
    }
    public function get_all_rank_color_by_event($eid=NULL)

    {
        $res = $this->db->select('*')->from('game_rank_color')->where('event_id', $eid)->get()->result_array();
        return $res;
    }
    public function get_game_header($eid=NULL)

    {
        $res = $this->db->select('*')->from('game_header')->where('event_id', $eid)->get()->row_array();
        return $res['header'];
    }
    public function save_game_header($header=NULL, $eid=NULL)

    {
        $data['header'] = $header;
        $data['event_id'] = $eid;
        $res = $this->db->select('*')->from('game_header')->where('event_id', $eid)->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where('event_id', $eid);
            $this->db->update('game_header', array(
                'header' => $header
            ));
        }
        else
        {
            $this->db->insert('game_header', $data);
        }
    }
    public function get_leaderboard($event_id=NULL)

    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sender_name,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,
                           CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END as logo,SUM(gu.points) as total,ru.Role_id', FALSE);
        $this->db->from('user u');
        $this->db->join('game_users_point gu', 'gu.user_id = u.Id');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id');
        $this->db->where('gu.event_id', $event_id);
        $this->db->where('ru.Event_id', $event_id);
        $this->db->group_by('u.Id');
        $this->db->order_by('total', 'desc');
        $this->db->limit('5');
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('*');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get('game_rank_color');
        $color = $query->result_array();
        foreach($res as $key => $value)
        {
            $res[$key]['color'] = $color[$key]['color'];
            $res[$key]['rank'] = $color[$key]['rank_no'];
            if ($key > 0)
            {
                if ($value['total'] == $res[$key - 1]['total'])
                {
                    $res[$key]['rank'] = $res[$key - 1]['rank'];
                }
            }
            if ($value['Role_id'] == '6')
            {
                $tmp = $this->db->select('id')->from('exibitor')->where('user_id', $value['id'])->where('Event_id', $event_id)->get()->row_array();
                $res[$key]['id'] = $tmp['id'];
            }
        }
        $data['top_five'] = $res;
        $this->db->select('header');
        $this->db->where('event_id', $event_id);
        $header = $this->db->get('game_header')->row_array();
        $data['header'] = $header['header'];
        $data['image']['L'] = 'http://www.allintheloop.net/assets/user_files/Star.png';
        $data['image']['R'] = 'http://www.allintheloop.net/assets/user_files/Star-R.png';
        $this->db->select('gr.point,g.rank');
        $this->db->from('game_rank_point gr');
        $this->db->join('game_rank g', 'g.id = gr.rank_id');
        $this->db->where('event_id', $event_id);
        $this->db->where('gr.point !=', '0');
        $info = $this->db->get()->result_array();
        foreach($info as $key => $value)
        {
            $info[$key]['point'].= ($value['point'] == 1) ? " Point" : " Points";
        }
        $data['info'] = $info;
        return $data;
    }
}
?>