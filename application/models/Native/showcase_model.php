<?php

class showcase_model extends CI_Model
{

     function __construct()
     {
          $this->db1 = $this->load->database('db1', TRUE);
          parent::__construct();
     }
    public function getDefaultEvent()
    {
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      /*$this->db->where('e.Event_type','3');
      $this->db->where('e.Status','1');*/
      $this->db->where('e.id',426);
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }
     
}

?>