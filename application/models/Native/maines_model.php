<?php
class Maines_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->db1 = $this->load->database('db1', TRUE);
      
    }
    public function getDefaultEvent()
    {
      $this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled',FALSE);
      $this->db->from('event e');
      $this->db->join("fundraising_setting fs","fs.Event_id=e.Id","left");
      /*$this->db->where('e.Event_type','3');
      $this->db->where('e.Status','1');*/
      $this->db->where('e.id',446);
      $this->db->order_by("e.Start_date","desc");
      $oqu=$this->db->get();
      $res=$oqu->result_array();
      return $res;
    }

    public function getExhibitorList($Event_id=null,$page_no)
    {
        $exibitors = [];
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        foreach ($types as $key => $value) {
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->where('e.Event_id',$Event_id);
            $this->db->where('e.et_id',$value['type_id']);
            $this->db->order_by('e.Heading');

            $query = $this->db->get();
            $data_result = $query->result_array(); 
            if(!empty($data_result))
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['data'] = $data_result;
                $exibitors[] = $results; 
            }
        }

        //$this->db->select('*');
        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
        $this->db->from('exibitor e');
        $this->db->where('e.Event_id',$Event_id);
        $this->db->where('e.et_id IS NULL');
        $this->db->order_by('e.Heading');
        $query = $this->db->get();
        $data_result1 = $query->result_array();

        if(!empty($data_result1))
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['data'] = $data_result1; 
            $exibitors[] = $results; 
        }
        if(!empty($exibitors))
        {
            foreach ($exibitors as $key => $value) {
                foreach ($value['data'] as $key1 => $value) {
                    $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                    $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                    $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                    
                    $images_decode = json_decode($value['Images']);
                    $cmpy_logo_decode = json_decode($value['company_logo']);
                    if(empty($images_decode[0]))
                    {
                        $images_decode[0]="";
                    }
                    if(empty($cmpy_logo_decode[0]))
                    {
                        $cmpy_logo_decode[0]="";
                    }
                    $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                    
                    $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                    $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                    $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                    $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                    $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                    $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                    $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                    $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
                }
            }
        }
        return $exibitors;
    }
    public function getAllMeetingRequest1($event_id,$user_id)
    {
        $data = $this->db->select('e.Id')->from('exibitor e')->join('user u','u.Id = e.user_id')->where('u.token',$user_id)->where('e.event_id',$event_id)->get()->row_array();

        return $this->db->select('u.Firstname,u.Lastname,eam.date,eam.time,eam.status,eam.Id as request_id,eam.exhibiotor_id')->from('exhibitor_attendee_meeting eam')->join('user u','u.Id = eam.attendee_id')->where('eam.exhibiotor_id',$data['Id'])->where('eam.event_id',$event_id)->where('eam.status != ','2')->order_by('eam.Id','DESC')->get()->result_array();
    }
    public function getSerachableRecords($Event_id,$keyword)
    {
        $exibitors = [];
        $types = $this->db->select('*')->from('exhibitor_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        foreach ($types as $key => $value) {
            $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
            $this->db->from('exibitor e');
            $this->db->where('e.Event_id',$Event_id);
            $this->db->where('e.et_id',$value['type_id']);
            $where = "(Short_desc like '%$keyword%' or Heading like '%$keyword%')";
            $this->db->where($where);
            $this->db->order_by('e.Heading');

            $query = $this->db->get();
            $data_result = $query->result_array(); 
            if(!empty($data_result))
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['data'] = $data_result;
                $exibitors[] = $results; 
            }
        }

        //$this->db->select('*');
        $this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,CASE WHEN e.Short_desc IS NULL THEN "" ELSE e.Short_desc END as Short_desc,CASE WHEN e.Images IS NULL THEN "" ELSE e.Images END as Images,CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,CASE WHEN e.website_url IS NULL THEN "" ELSE e.website_url END as website_url,CASE WHEN e.facebook_url IS NULL THEN "" ELSE e.facebook_url END as facebook_url,CASE WHEN e.twitter_url IS NULL THEN "" ELSE e.twitter_url END as twitter_url,CASE WHEN e.linkedin_url IS NULL THEN "" ELSE e.linkedin_url END as linkedin_url,CASE WHEN e.phone_number1 IS NULL THEN "" ELSE e.phone_number1 END as phone_number1,CASE WHEN e.email_address IS NULL THEN "" ELSE e.email_address END as email_address,
            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id',FALSE);
        $this->db->from('exibitor e');
        $this->db->where('e.Event_id',$Event_id);
        $where = "(Short_desc like '%$keyword%' or Heading like '%$keyword%')";
        $this->db->where($where);
        $this->db->where('e.et_id IS NULL');
        $this->db->order_by('e.Heading');
        $query = $this->db->get();
        $data_result1 = $query->result_array();

        if(!empty($data_result1))
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['data'] = $data_result1; 
            $exibitors[] = $results; 
        }
       
        foreach ($exibitors as $key => $value) {
            foreach ($value['data'] as $key1 => $value) {
                $exibitors[$key]['data'][$key1]['exhibitor_id']=$value['exhibitor_id'];
                $exibitors[$key]['data'][$key1]['exhibitor_page_id']=$value['exhibitor_page_id'];
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Heading']=ucfirst($value['Heading']);
                $exibitors[$key]['data'][$key1]['Short_desc']=$value['Short_desc'];
                
                $images_decode = json_decode($value['Images']);
                $cmpy_logo_decode = json_decode($value['company_logo']);
                if(empty($images_decode[0]))
                {
                    $images_decode[0]="";
                }
                if(empty($cmpy_logo_decode[0]))
                {
                    $cmpy_logo_decode[0]="";
                }
                $exibitors[$key]['data'][$key1]['Images']=$images_decode[0];
                
                $exibitors[$key]['data'][$key1]['stand_number']=$value['stand_number'];
                $exibitors[$key]['data'][$key1]['company_logo']= $cmpy_logo_decode[0];
                $exibitors[$key]['data'][$key1]['website_url']=$value['website_url'];
                $exibitors[$key]['data'][$key1]['facebook_url']=$value['facebook_url'];
                $exibitors[$key]['data'][$key1]['twitter_url']=$value['twitter_url'];
                $exibitors[$key]['data'][$key1]['linkedin_url']=$value['linkedin_url'];
                $exibitors[$key]['data'][$key1]['phone_number1']=$value['phone_number1'];
                $exibitors[$key]['data'][$key1]['email_address']=$value['email_address'];
            }
        }
        return $exibitors;
    }  
}
?>