<?php
class lead_model extends CI_Model
{
	function __construct()
	{
		$this->db1 = $this->load->database('db1', TRUE);
		parent::__construct();
	}
	public function get_scan_info($eid,$uid)
    {   
        $lead = $this->db->select()->where('lead_user_id',$uid)->where('event_id',$eid)->get('exibitor_lead')->row_array();

        if(empty($lead))
        {
            $this->db->select('u.Id,
                               CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                               CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                               CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END AS Email,
                               CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                               CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                               reu.Role_id,
                               CASE WHEN ea.extra_column IS NULL THEN "" ELSE ea.extra_column END AS extra_column',false)
                               ->from('user u');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id And reu.Event_id='.$eid);
            $this->db->join('event_attendee ea','u.Id=ea.Attendee_id and ea.Event_id='.$eid,'left');
            $this->db->where('reu.Event_id',$eid);
            $this->db->where('reu.Role_id','4');
            $this->db->where('u.Id',$uid);
            $this->db->group_by('u.Id');
            $res = $this->db->get()->row_array();
            $res['extra_column'] = !empty($res['extra_column']) ?  json_decode($res['extra_column']) : [];
            $i = 0;
            foreach ($res['extra_column'] as $key => $value)
            {
                $tmp[$i]['Que'] = $key;
                $tmp[$i]['Ans'] = $value;           
                $i++;
            }
            $res['extra_column'] = ($tmp) ? $tmp : [];
            if(empty($res['Id']))
                unset($res['extra_column']);
        return $res;
        }
        else
        {   
            $tmp['message'] = "Lead is already scanned";
            return $tmp;
        }
            
    }
    public function get_custom_column($eid)
	{
		$this->db->select('*')->from('custom_column');
		$this->db->where('event_id',$eid);
		$que=$this->db->get();
		$res=$que->result_array();
		return $res;
	}
	public function get_all_exibitor_user_questions($eid,$ex_uid,$uid)
	{   
        $this->db->_protect_identifiers=false;
        $this->db->select('eq.*,
                           case when eq.commentbox_label_text is null then "" else eq.commentbox_label_text end as commentbox_label_text,
                          (case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic',false)
                        ->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id','left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->group_by('eq.q_id');
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $res2 = $this->db->get()->result_array();
        foreach ($res2 as $key => $value) 
        {
            if($value['skip_logic']=='1')
            {
                $this->db->select('`option`,redirectexibitorquestion_id')->from('exibitor_question_skiplogic');
                $this->db->where('exibitor_question_id',$value['q_id']);
                $res=$this->db->get()->result_array();
                foreach ($res as $k2 => $v2)
                {
                    $res[$k2]['option'] = str_replace('_',' ',$v2['option']);
                }
                $option=array_column_1($res,'option');
                $redirectquestion_id=array_column_1($res,'redirectexibitorquestion_id');
                $finalarray=array_combine($option, $redirectquestion_id);
                $res2[$key]['redirectids']=$finalarray;
                $i = 0 ;
                foreach ($finalarray as $key1 => $value1)
                {
                    $tmp[$i]['option'] = $key1;
                    $tmp[$i]['redirect_id'] = $value1; 
                    $i++;
                }
                $res2[$key]['a_redirectids'] = $tmp;
                unset($tmp);
            }
            else
            {
                $res2[$key]['redirectids'] = new stdClass();
                $res2[$key]['a_redirectids'] = [];
            }
            $res2[$key]['Option'] = ($value['Option']) ? json_decode($value['Option']) : [];
            $question_array=$this->get_survey_array($eid,$ex_uid,$uid);
            $dependent_ids=$question_array['dependent_ids'];
            $independent_ids=array_values($question_array['independent_ids']);
            if(!in_array($value['q_id'],$independent_ids))
            {
                $redirectkey=$this->get_inarrayval($value['q_id'],$dependent_ids,$independent_ids);
                $rekey=array_search($redirectkey,$independent_ids);
                $redirectid=$independent_ids[$rekey+1];
            }
            else
            {
                $rekey=array_search($value['q_id'],$independent_ids);
                $redirectid=$independent_ids[$rekey+1];
            }
            $res2[$key]['redirectid']=($redirectid) ? $redirectid:"";
        }
        return $res2;
	}
	public function get_inarrayval($qid,$dearr,$indearr)
    { 
        $qkey=$dearr[$qid];
        if(in_array($qkey, $indearr))
        {
          return $qkey;
        }
        return $this->get_inarrayval($qkey,$dearr,$indearr); 
    }
	public function get_survey_array($eid,$ex_uid,$uid)
    {
        $this->db->_protect_identifiers=false;
        $this->db->select('eqs.*')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $dependentquestion = $this->db->get()->result_array();
        $idsarr=array_unique(array_filter(array_column_1($dependentquestion,"redirectexibitorquestion_id")));
        $masterqarr=array_filter(array_column_1($dependentquestion,"exibitor_question_id"));
        $dependent_ids=array_filter(array_column_1($dependentquestion,"redirectexibitorquestion_id"));
        $dependentquestionarr=array_combine($dependent_ids, $masterqarr);

        $this->db->_protect_identifiers=false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs','eq.q_id=eqs.exibitor_question_id','left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid','left');
        $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        if(count($idsarr) > 0)
        {
            $this->db->where_not_in('eq.q_id',$idsarr);
        }
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id');
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $independentquestion = $this->db->get()->result_array();
        $independent_ids=array_unique(array_filter(array_column_1($independentquestion,"q_id")));
        $res2['dependent_ids']=$dependentquestionarr;
        $res2['independent_ids']=$independent_ids;
        foreach($res2 as $key => $value)
        {
            $res2[$key]['redirectids'] = json_decode($value['redirectids'],true);
        }
        return $res2;
    }
    public function save_scan_lead($lead_data,$eid,$ex_uid,$uid)
    {   
        $lead = $this->db->select()->where('lead_user_id',$uid)->where('event_id',$eid)->get('exibitor_lead')->row_array();
        if(empty($lead))
        {
            $this->db->select('*')->from('exibitor_lead');
            $this->db->where('event_id',$eid);
            $this->db->where('exibitor_user_id',$ex_uid);
            $this->db->where('lead_user_id',$uid);
            $res=$this->db->get()->result_array();
            if(count($res) > 0)
            {
                $this->db->where('event_id',$eid);
                $this->db->where('exibitor_user_id',$ex_uid);
                $this->db->where('lead_user_id',$uid);
                $this->db->update('exibitor_lead',$lead_data);
            }
            else
            {
                $lead_data['event_id']=$eid;
                $lead_data['exibitor_user_id']=$ex_uid;
                $lead_data['lead_user_id']=$uid;
                $lead_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('exibitor_lead',$lead_data);
            }
            return true;
        }
        else
        {
            return false;
        }    
    }
    public function add_question_answer($survey_data)
    {
        $this->db->insert_batch("exibitor_question_answer",$survey_data);
    }
    public function get_exhibitor_user_by_representative_id($resp_user_id,$event_id)
    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('reps_user_id',$resp_user_id);
        $this->db->where('event_id',$event_id);
        $res = $this->db->get()->row_array();
        return $res['exibitor_user_id'];
    }
    public function get_all_my_lead_by_exibitor_user($eid,$ex_uid,$where)
    {   

        $this->db->select('u.Id,reu.Role_id,reu.Organisor_id,el.firstname as Firstname,el.lastname as Lastname,el.email as Email,el.title as Title,el.company_name as Company_name')->from('user u');
        $this->db->join('exibitor_lead el','el.lead_user_id=u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$eid);
        $this->db->where('el.event_id',$eid);
        $this->db->where('el.exibitor_user_id',$ex_uid);
        if(!empty($where))
            $this->db->where($where);
        $this->db->order_by('el.firstname');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_all_my_representative_by_exibitor_user($eid,$ex_uid,$where)
    {
        $this->db->select('u.Id,
                          CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END AS logo,
                          CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                          CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                          CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                          CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                          reu.Role_id,
                          reu.Organisor_id',false)->from('user u');
        $this->db->join('exhibitor_representatives er','er.reps_user_id=u.Id');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id and reu.Event_id='.$eid);
        $this->db->where('er.event_id',$eid);
        $this->db->where('er.exibitor_user_id',$ex_uid);
        if(!empty($where))
            $this->db->where($where);
        $this->db->order_by('u.Firstname');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_event_all_attendee_from_assing_resp($eid,$where)
    {
        $this->db->select('u.Id,
                          CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END AS logo,
                          CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                          CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                          CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                          CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name',false)
                 ->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id=u.Id');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('reu.Role_id','4');
        if(!empty($where))
            $this->db->where($where);
        $this->db->where('u.Id NOT IN (select reps_user_id from exhibitor_representatives where event_id='.$eid.')');
        $this->db->where('u.Firstname != ""');
        $this->db->order_by('u.Firstname');
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function add_new_representative($eid,$ex_uid,$resp_uid)
    {
        $res=$this->get_exibitor_by_representative_id_in_event($eid,$resp_uid);
        if(empty($res))
        {
            $resp_data['event_id']=$eid;
            $resp_data['exibitor_user_id']=$ex_uid;
            $resp_data['reps_user_id']=$resp_uid;
            $resp_data['created_date']=date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_representatives',$resp_data);
        }
        return true;
    }
    public function get_total_representative_by_exibitor_user($eid,$ex_uid)
    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id',$eid);
        $this->db->where('exibitor_user_id',$ex_uid);
        $res=$this->db->get()->result_array();
        return count($res);
    }
    public function get_exibitor_user_premission($eid,$ex_uid)
    {
        $this->db->select('CASE WHEN maximum_Reps IS NULL THEN "" ELSE maximum_Reps end as maximum_Reps',false)->from('exibitor_premission');
        $this->db->where('event_id',$eid);
        $this->db->where('user_id',$ex_uid);
        $res=$this->db->get()->row_array();
        return (!empty($res['maximum_Reps'])) ? $res['maximum_Reps'] : '';
    }
    public function remove_my_representative($eid,$resp_uid)
    {
        $this->db->where('event_id',$eid);
        $this->db->where('reps_user_id',$resp_uid);
        $this->db->delete('exhibitor_representatives');
    }
    public function get_all_exibitor_user_questions_for_export($eid,$ex_uid)
    {
        $this->db->_protect_identifiers=false;
        $this->db->select('eq.*')->from('exibitor_question eq');
        $this->db->where('eq.event_id',$eid);
        $this->db->where('eq.exibitor_user_id',$ex_uid);
        $this->db->order_by('eq.sort_order=0,eq.sort_order');
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_user_question_answer($eid,$uid,$qid)
    {
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('event_id',$eid);
        $this->db->where('user_id',$uid);
        $this->db->where('exibitor_questionid',$qid);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_exibitor_by_representative_id_in_event($eid,$resp_uid)
    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id',$eid);
        $this->db->where('reps_user_id',$resp_uid);
        $res=$this->db->get()->row_array();
        return $res;
    }
    public function get_user_by_email($event_id,$email)
    {
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id And reu.Event_id='.$event_id);
        $this->db->where('u.Email',$email);
        $res = $this->db->get()->row_array();
        return $res['Id'];
    }
    public function get_event_name($event_id)
    {
        $this->db->select('Event_name');
        $this->db->where('Id',$event_id);
        $res = $this->db->get('event')->row_array();
        return ucfirst($res['Event_name']);
    }
    public function get_offline_scan_data($eid)
    {   
        $lead_users = $this->db->select()->where('event_id',$eid)->get('exibitor_lead')->result_array();
        $lead_users = array_column_1($lead_users, 'lead_user_id');
           
        $this->db->select('u.Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END AS Email,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                           reu.Role_id,
                           CASE WHEN ea.extra_column IS NULL THEN "" ELSE ea.extra_column END AS extra_column',false)
                           ->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id And reu.Event_id='.$eid);
        $this->db->join('event_attendee ea','u.Id=ea.Attendee_id and ea.Event_id='.$eid,'left');
        $this->db->where('reu.Event_id',$eid);
        $this->db->where('reu.Role_id','4');
        $this->db->where_not_in('u.Id',$lead_users);
        $this->db->group_by('u.Id');
        $res = $this->db->get()->result_array();
        foreach ($res as $key => $value)
        {
            $res[$key]['extra_column'] = !empty($value['extra_column']) ?  json_decode($value['extra_column']) : [];
            $i = 0;
            foreach ($res[$key]['extra_column'] as $k1 => $v1)
            {
                $tmp[$i]['Que'] = $k1;
                $tmp[$i]['Ans'] = $v1;           
                $i++;
            }
            $res[$key]['extra_column'] = ($tmp) ? $tmp : [];
            unset($tmp);
            if(empty($res[$key]['Id']))
                unset($res[$key]['extra_column']);
        }
       return $res;
    }
    public function get_user_by_id($event_id,$id)
    {
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id And reu.Event_id='.$event_id);
        $this->db->where('u.Id',$id);
        $res = $this->db->get()->row_array();
        return $res['Id'];
    }
}