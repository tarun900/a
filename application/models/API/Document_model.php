<?php
class document_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        // $this->db2 = $this->load->database('db1', TRUE);
    }
    public function getFoldersByEventId($event_id)
    {
       
        $this->db->select('CASE WHEN d.id IS NULL THEN "" ELSE d.id END as doc_id,CASE WHEN d.doc_type IS NULL THEN "" ELSE d.doc_type END as doc_type,CASE WHEN d.title IS NULL THEN "" ELSE d.title END as title',false);
        $this->db->from('documents d');
        $this->db->where('d.Event_id',$event_id);
        $this->db->where('d.doc_type','0');
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;

    }
    public function getFilesByEventId($event_id,$doc_id=NULL)
    {
       
        $this->db->select('CASE WHEN d.id IS NULL THEN "" ELSE d.id END as doc_id,CASE WHEN d.doc_type IS NULL THEN "" ELSE d.doc_type END as doc_type,CASE WHEN d.title IS NULL THEN "" ELSE d.title END as title,CASE WHEN df.document_file IS NULL THEN "" ELSE df.document_file END as document_file',false);
        $this->db->from('documents d');
        $this->db->join("document_files df","df.document_id=d.id");
        $this->db->where('d.Event_id',$event_id);
        $this->db->where('d.doc_type','1');
        if($doc_id!='')
        {
            $this->db->where('d.parent',$doc_id);
        }
        else
        {
            $this->db->where('d.parent',0);
        }
        
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        return $res2;

    }
 }
        
?>
