<?php
class note_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);
    }
    public function getNotesListByEventId($event_id,$user_id)
    {
       
        $this->db->select('CASE WHEN n.Id IS NULL THEN "" ELSE n.Id END as Id,CASE WHEN n.Heading IS NULL THEN "" ELSE n.Heading END as Heading,CASE WHEN n.Description IS NULL THEN "" ELSE n.Description END as Description,CASE WHEN n.Created_at IS NULL THEN "" ELSE n.Created_at END as Created_at,CASE WHEN n.Event_id IS NULL THEN "" ELSE n.Event_id END as Event_id,CASE WHEN n.User_id IS NULL THEN "" ELSE n.User_id END as User_id,CASE WHEN n.Organisor_id IS NULL THEN "" ELSE n.Organisor_id END as Organisor_id',false);
        $this->db->from('notes n');
        $this->db->where('n.Event_id',$event_id);
        $this->db->where('n.User_id',$user_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        
        return $res2;

    }
    public function getOrganisorIdByUserId($user_id,$event_id)
    {
        $this->db->select('Organisor_id');
        $this->db->from('relation_event_user');
        $this->db->where('Event_id',$event_id);
        $this->db->where('User_id',$user_id);
        $query2 = $this->db->get();
        $res2 = $query2->result_array();
        $Organisor_id=$res2[0]['Organisor_id'];
        return $Organisor_id;
    }
    public function add_note($add_arr)
    {
        $this->db->insert("notes",$add_arr);
    }
    public function update_note($note_id,$update_data)
    {
        $this->db->where("Id",$note_id);
        $this->db->update("notes",$update_data);
    }
 }
        
?>
