<?php
class Speaker_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }

    public function getSpeakersListByEventId($event_id,$user_id)
    {
        $event = $this->db->select('*')->from('event')->where('Id',$event_id)->get()->row_array();
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END as Email,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo',false);
        $this->db->from('event e');
        $this->db->join('relation_event_user ru','ru.Event_id = e.Id','right');
        $this->db->join('user u', 'u.Id = ru.User_id',false);
        $this->db->join('role r','ru.Role_id = r.Id',false);
        $this->db->where('e.Id',$event_id);
        $this->db->where('u.key_people','0');
        $this->db->where('u.is_moderator','0');
        $this->db->where('r.Id =',7);
        if($event['key_people_sort_by'] == '1')
            $this->db->order_by('u.Lastname');
        else
            $this->db->order_by('u.Firstname');
        $this->db->group_by('u.Id');
        //$this->db->order_by('u.Firstname');
        $query = $this->db->get();
        $res = $query->result();  
        $speakers = array();
        for($i=0; $i<count($res); $i++)
        {
            $speakers[$i]['Id']=$res[$i]->Id;
            $speakers[$i]['Firstname']=ucfirst($res[$i]->Firstname);
            $speakers[$i]['Lastname']=ucfirst($res[$i]->Lastname);
            $speakers[$i]['Company_name']=$res[$i]->Company_name;
            $title = $res[$i]->Title;
            $subtitle = substr($title, 0 ,60);
            $speakers[$i]['Title']=(strlen($title) >= 60 )  ? $subtitle."..." : $title;
            $speakers[$i]['Speaker_desc']=$res[$i]->Speaker_desc;
            $speakers[$i]['Email']=$res[$i]->Email;
            $speakers[$i]['Logo']=$res[$i]->Logo;
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$event_id)->where('user_id',$user_id)->where('module_type','7')->where('module_id',$res[$i]->Id)->get()->num_rows();
            $speakers[$i]['is_favorites']=($count) ? '1' : '0';
        }
        return $speakers;
    }
    public function getSpeakerDetails($speaker_id,$event_id,$user_id)
    {
        $this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,CASE WHEN u.Speaker_desc IS NULL THEN "" ELSE u.Speaker_desc END as Speaker_desc,CASE WHEN usl.Facebook_url IS NULL THEN "" ELSE usl.Facebook_url END as Facebook_url,CASE WHEN usl.Twitter_url IS NULL THEN "" ELSE usl.Twitter_url END as Twitter_url,CASE WHEN usl.Linkedin_url IS NULL THEN "" ELSE usl.Linkedin_url END as Linkedin_url',FALSE);
        $this->db->from('user u');
        $this->db->join('user_social_links usl', 'u.Id = usl.User_id','left');
        $this->db->where('u.Id',$speaker_id);
        $query = $this->db->get();
        $res = $query->result();  
        $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get()->row_array();
           $extra=json_decode($extra['extra_column'],true);
           foreach ($extra as $key => $value) {
              $keyword="{".str_replace(' ', '', $key)."}";
              if(stripos(strip_tags($res[0]->Speaker_desc,$keyword)) !== false)
              {
                $res[0]->Speaker_desc=str_ireplace($keyword, $value,$res[0]->Speaker_desc);
              }
            }
        return $res;
    }
    public function getUserId($token)
    {
        $data = $this->db->select('Id')->from('user')->where('token',$token)->get()->row_array();
        return $data['Id'];
    }
  }
        
?>
