<?php
class Survay_model extends CI_Model{
    function __construct()
    {
            parent::__construct();
    }
    public function get_survey_screens($id=null)
    {
        $this->db->select('*,CASE WHEN welcome_data IS NULL THEN "" ELSE welcome_data END as welcome_data,CASE WHEN thanku_data IS NULL THEN "" ELSE thanku_data END as thanku_data',false);
        $this->db->from('survey_data');
        $this->db->where('Event_id',$id);
        $query = $this->db->get();
        $res = $query->result_array();
        if(empty($res))
        {
            $res[0]['Id'] = '';
            $res[0]['welcome_data'] = '';
            $res[0]['thanku_data'] = '';
            $res[0]['Event_id'] = $id;
        }
        return $res;
    }
    public function get_survey($event_id=null,$user_id=null)
    {
        $this->db->select('CASE WHEN s.id IS NULL THEN "" ELSE s.id END as Question_id,CASE WHEN s.Question IS NULL THEN "" ELSE s.Question END as Question,CASE WHEN s.Question_type IS NULL THEN "" ELSE s.Question_type END as Question_type,CASE WHEN s.Option IS NULL THEN "" ELSE s.Option END as option1',false);
        $this->db->from('survey s');
        if($user_id!='')
        {
           //$this->db->join('poll_survey p', 's.Id = p.Question_id');
            $this->db->where('s.id not IN (SELECT ps.Question_id  FROM `poll_survey` ps join survey s on s.id=ps.Question_id WHERE `User_id` = '.$user_id.' and Event_id='.$event_id.' group by ps.Question_id ORDER BY `User_id` DESC )', NULL, FALSE);
            $this->db->where('s.Event_id',$event_id);
        }
        else
        {   

            $this->db->where('s.Event_id',$event_id);
        }
        $this->db->where('s.show_question','1');
        $this->db->group_by('s.id');

        $query2 = $this->db->get();
        $res2 = $query2->result_array();
      
        if(!empty($res2))
        {

            foreach ($res2 as $key => $value) 
            {
               
                $fin_arr[$key]['Question_id']=$value['Question_id'];
                $fin_arr[$key]['Question']=$value['Question'];
                $fin_arr[$key]['Question_type']=$value['Question_type'];
                $fin_arr[$key]['Option']=json_decode($value['option1']);
            }
        }
        return $fin_arr;
    }
    public function saveSurvey($survey_data)
    {
        $this->db->insert_batch("poll_survey",$survey_data);
        return ($this->db->affected_rows() > 0) ? 1 : 0;
    }
  }
        
?>