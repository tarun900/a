<?php
class app_profile_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
        //$this->db2 = $this->load->database('db1', TRUE);

    }
    public function getStateListByCountry($id)
    {
          $this->db->select('*');
          $this->db->from('state');
          $this->db->where('country_id', $id);
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }
    public function update_user_profile($user_id,$data)
    {
        $this->db->where("Id",$user_id);
        $this->db->update("user",$data);
    }
    public function make_thumbnail($user_id,$image_name)
    {
      try {
          
          $destination = "./assets/user_files/thumbnail/".$image_name;

         // copy("assets/user_files/".$image_name, $destination);

          $config['image_library']    = 'gd2';
          $config['source_image']     = $destination;
          $config['file_name']        = $image_name;
          $config['create_thumb']     = FALSE;
          $config['maintain_ratio']   = TRUE;
          $config['width']            = 100;
          $config['height']           = 100;

          $this->load->library('image_lib');
          $this->image_lib->initialize($config);
          $this->image_lib->resize();

          return true;
      } catch (Exception $e) {
        return false;
      }
      
    }   
    public function getAllUsersLogo()
    {
          $this->db->select('Logo');
          $this->db->from('user');
          $query = $this->db->get();
          $res = $query->result_array();
          return $res;
    }
}
        
?>
