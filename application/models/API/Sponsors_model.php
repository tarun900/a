<?php
class Sponsors_model extends CI_Model{
    function __construct()
    {   
        parent::__construct();

    }

   
    /*public function getSponsorsListByEventId($Event_id=null)
    {
        $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name',FALSE);
        $this->db->from('sponsors s');
        $this->db->where('s.Event_id',$Event_id);
        $this->db->order_by('s.Sponsors_name');
        $query = $this->db->get();
        $res = $query->result(); 

        $sponsors = array();
        for($i=0; $i<count($res); $i++)
        {
           
            $sponsors[$i]['Id']=$res[$i]->Id;
            $sponsors[$i]['Sponsors_name']=ucfirst($res[$i]->Sponsors_name);
            $sponsors[$i]['Company_name']=ucfirst($res[$i]->Company_name);
            $cmpy_logo_decode = json_decode($res[$i]->company_logo);
            if(empty($cmpy_logo_decode[0]))
            {
                $cmpy_logo_decode[0]="";
            }
            $sponsors[$i]['company_logo']=$cmpy_logo_decode[0];
        }
        return $sponsors; 
    } */
    public function getSponsorsListByEventId($Event_id=null,$user_id)
    {
        $types = $this->db->select('*')->from('sponsors_type')->where('event_id',$Event_id)->order_by('type_position')->get()->result_array();
        $sposers = array();
        foreach ($types as $key => $value) {
            $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name',FALSE);
            $this->db->from('sponsors s');
            $this->db->where('s.Event_id',$Event_id);
            $this->db->where('s.st_id',$value['type_id']);
            $this->db->order_by('s.Sponsors_name');
            $sp_data = $this->db->get()->result_array();
            if($sp_data)
            {
                $results['type'] = $value['type_name'];
                $results['bg_color'] = $value['type_color'];
                $results['data'] = $sp_data; 
                $sposers[] = $results; 
            }
        }
        
        $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name',FALSE);
        $this->db->from('sponsors s');
        $this->db->where('s.Event_id',$Event_id);
        $this->db->where('s.st_id IS NULL');
        $this->db->order_by('s.Sponsors_name');
        $sp1_data = $this->db->get()->result_array();
        if($sp1_data)
        {
            $results['type'] = '';
            $results['bg_color'] = '';
            $results['data'] = $sp1_data; 
            $sposers[] = $results; 
        }
         
        foreach ($sposers as $key => $value) {
          foreach ($value['data'] as $key1 => $value1) {
            $sposers[$key]['data'][$key1]['Sponsors_name']=ucfirst($value1['Sponsors_name']);
            $sposers[$key]['data'][$key1]['Company_name']=ucfirst($value1['Company_name']);
            $cmpy_logo_decode = json_decode($value1['company_logo']);
            if(empty($cmpy_logo_decode[0]))
            {
                $cmpy_logo_decode[0]="";
            }
            $sposers[$key]['data'][$key1]['company_logo']=$cmpy_logo_decode[0];
            $count = $this->db->select('*')->from('my_favorites')->where('event_id',$Event_id)->where('user_id',$user_id)->where('module_type','43')->where('module_id',$value1['Id'])->get()->num_rows();
            $sposers[$key]['data'][$key1]['is_favorites']=($count) ? '1' : '0';
          }
        }
      return $sposers; 
    }  
    public function getSponsorsDetails($id,$event_id,$token)
    {
        $this->db->select('CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                           CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name,
                           CASE WHEN s.Description IS NULL THEN "" ELSE s.Description END as Description,
                           CASE WHEN s.website_url IS NULL THEN "" ELSE s.website_url END as website_url,
                           CASE WHEN s.facebook_url IS NULL THEN "" ELSE s.facebook_url END as facebook_url,
                           CASE WHEN s.twitter_url IS NULL THEN "" ELSE s.twitter_url END as twitter_url,
                           CASE WHEN s.linkedin_url IS NULL THEN "" ELSE s.linkedin_url END as linkedin_url,
                           CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                           CASE WHEN s.Images IS NULL THEN "" ELSE s.Images END as Images',FALSE);
        $this->db->from('sponsors s');
        $this->db->where('s.Id',$id);
        $query = $this->db->get();
        $res = $query->result_array();  
        if(!empty($res[0]))
        {
            $token = ($token == NULL) ? '' : $token;
            $user = $this->db->select('*')->from('user')->where('token',$token)->get()->row_array();
            $extra =  $this->db->select('*')->from('event_attendee')->where('Event_id',$event_id)->where('Attendee_id',$user['Id'])->get()->row_array();
            $extra=json_decode($extra['extra_column'],true);

            foreach ($extra as $key => $value) {
              $keyword="{".str_replace(' ', '', $key)."}";
              if(stripos(strip_tags($res[0]['Description'],$keyword)) !== false)
              {
                $res[0]['Description']=str_ireplace($keyword, $value,$res[0]['Description']);
              }
            }
        }
        return $res;
    }
    public function getUserId($token)
    {
        $data = $this->db->select('Id')->from('user')->where('token',$token)->get()->row_array();
        return $data['Id'];
    } 
}
        
?>
