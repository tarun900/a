<?php

class activity_model extends CI_Model
{

   function __construct()
   {
        $this->db1 = $this->load->database('db1', TRUE);
        parent::__construct();
   }
     
   public function getPublicMessageFeeds($event_id,$user_id=0)
   {
        $this->db->select("sm.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,sm.Message,sm.Time,(select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = sm.Id AND module_type = 'public') as like_count, (select activity_like.like from activity_like where module_primary_id = sm.Id AND user_id = '$user_id' AND module_type = 'public') as is_like",false);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u','u.Id=sm.Sender_id');
        $this->db->where('sm.Event_id',$event_id);
        $this->db->where('sm.ispublic','1');
        $this->db->order_by('sm.Time','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        
        foreach ($result as $key => $value) 
        {
          $result[$key]['rating'] = '';
          $message = $value['Message'];
          $result[$key]['Message'] = (strlen($message) > 25) ? substr($message, 0,25)."..." : $message;
          $result[$key]['title']   =  "posted a new update:";
          $result[$key]['navigation_title'] = "READ MORE";
          $result[$key]['type'] = "public";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;
        }
   
        return $result;
   } 

   public function getPhotoFeeds($event_id,$user_id=0)
   {
        $this->db->select("fi.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,fi.Message,fi.Time,(select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = fi.Id AND module_type = 'photo') as like_count, (select activity_like.like from activity_like where module_primary_id = fi.Id AND user_id = '$user_id' AND module_type = 'photo') as is_like",false);
        $this->db->from('feed_img fi');
        $this->db->join('user u','u.Id=fi.Sender_id');
        $this->db->where('fi.Event_id',$event_id);
        $this->db->where('fi.ispublic','1');
        $this->db->where('fi.Parent',0);
        $this->db->order_by('fi.Time','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['rating'] = '';
          $result[$key]['Message'] = "";
          $result[$key]['title'] =  "shared a photo";
          $result[$key]['navigation_title'] = "VIEW PHOTO";
          $result[$key]['type'] = "photo";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;

        }
        return $result;
   }

   public function getCheckInFeeds($event_id,$user_id=0)
   {
        $this->db->select("uc.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,a.Heading as Message,uc.date as Time,(select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = uc.Id AND module_type = 'check_in') as like_count, (select activity_like.like from activity_like where module_primary_id = uc.id AND user_id = '$user_id' AND module_type = 'check_in') as is_like",false);
        $this->db->from('user_check_in uc');
        $this->db->join('user u','u.Id=uc.user_id');
        $this->db->join('agenda a','a.Id=uc.agenda_id');
        $this->db->where('a.Event_id',$event_id);
        $this->db->order_by('uc.date','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['rating'] = '';
          $result[$key]['Message'] = $value['Message'];
          $result[$key]['title'] =  "Checked In to a session:";
          $result[$key]['navigation_title'] = "";
          $result[$key]['type'] = "check_in";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;
        }
        return $result;
   }

   public function getRatingFeeds($event_id,$user_id=0)
   {
        $this->db->select("uc.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,a.Heading as Message,uc.date as Time,uc.rating,(select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = uc.Id AND module_type = 'rating') as like_count, (select activity_like.like from activity_like where module_primary_id = uc.Id AND user_id = '$user_id' AND module_type = 'rating') as is_like",false);
        $this->db->from('user_session_rating uc');
        $this->db->join('user u','u.Id=uc.user_id');
        $this->db->join('agenda a','a.Id=uc.session_id');
        $this->db->where('a.Event_id',$event_id);
        $this->db->order_by('uc.date','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['Message'] = $value['Message'];
          $result[$key]['title'] =  "rated a session:";
          $result[$key]['navigation_title'] = "";
          $result[$key]['type'] = "rating";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;
        }

        return $result;
   }

   public function getUserFeeds($event_id,$user_id=0)
   {
        $this->db->select("u.Id as id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,u.updated_date as Time,
          (select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = u.Id AND module_type='update_profile') as like_count, 
          (select activity_like.like from activity_like where module_primary_id = u.Id AND user_id = '$user_id' AND module_type='update_profile') as is_like
          ",false);
        $this->db->from('user u');
        $this->db->join('relation_event_user ru','ru.User_id=u.Id');
        $this->db->where('ru.Event_id',$event_id);
        $this->db->where('u.updated_date is NOT NULL', NULL, FALSE);
        $this->db->order_by('u.updated_date','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['rating'] = '';
          $result[$key]['Message'] = '';
          $result[$key]['title'] =  "Updated profile picture";
          $result[$key]['navigation_title'] = "";
          $result[$key]['type'] = "update_profile";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;
          }
       
        return $result;
   }
   public function getActivityFeeds($event_id,$user_id=0)
   {
        $this->db->select("a.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN '' ELSE u.Logo END as Logo,a.message as Message,a.time as Time,(select count(CASE WHEN 'like' = '1' THEN 1 ELSE 0 END)  from activity_like where module_primary_id = a.Id AND module_type='activity') as like_count, (select activity_like.like from activity_like where module_primary_id = a.id AND user_id = '$user_id' AND module_type='activity') as is_like",false);
        $this->db->from('activity a');
        $this->db->join('user u','u.Id=a.sender_id');
        $this->db->where('a.event_id',$event_id);
        $this->db->order_by('a.time','DESC');
        $query = $this->db->get();
        $result =  $query->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['rating'] = '';
          $message = $value['Message'];
          $result[$key]['Message'] = (strlen($message) > 25) ? substr($message, 0,25)."..." : $message;
          $result[$key]['title'] =  "posted a new Update:";
          $result[$key]['navigation_title'] = "READ MORE";
          $result[$key]['type'] = "activity";
          $result[$key]['is_like'] = ($value['is_like']) ? $value['is_like'] : '0' ;
        }

        return $result;
   }
   public function getFeedDetails($id)
   {
        $this->db->select('a.id,u.Firstname,u.Lastname,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo,a.message,a.image,a.time as Time',false);
        $this->db->from('activity a');
        $this->db->join('user u','u.Id=a.sender_id');
        $this->db->where('a.id',$id);
        $query = $this->db->get();
        $result =  $query->row();
        if($result->image=="")
        {
          $result->image = "[]";
        }
        $result->Time = $this->get_timeago(strtotime($result->Time));
        return $result;
   }
   public function savePublicPost($message_data)
   {
        $this->db->insert("activity",$message_data);
        return $this->db->insert_id();
   }
   public function getMessageDetails($message_id)
   {
        $this->db->select('image');
        $this->db->from('activity');
        $this->db->where("id",$message_id);
        $query=$this->db->get();
        $res=$query->row();
        return $res;
   }
   public function getCommentDetails($message_id)
   {
        $this->db->select('comment_image');
        $this->db->from('activity_comment');
        $this->db->where("comment_id",$message_id);
        $query=$this->db->get();
        $res=$query->row();
        return $res;
   }
   
   public function updateMessageImage($message_id,$update_arr)
   {
      $this->db->where("id",$message_id);
      $this->db->update("activity",$update_arr);
   }
   public function updateCommentImage($message_id,$update_arr)
   {
      $this->db->where("comment_id",$message_id);
      $this->db->update("activity_comment",$update_arr);
   }
   public function getImages($event_id,$activity_id){
        $this->db->select('image');
        $this->db->from('activity');
        $this->db->where('id',$activity_id);
        $this->db->where('event_id',$event_id);
        $rows = $this->db->get()->row();
        return $rows->image;
   }
   public function get_timeago($ptime)
   {
      $estimate_time = time() - $ptime;
      
      if ($estimate_time < 1)
      {
           return '1 second ago';
      }

      $condition = array(
              12 * 30 * 24 * 60 * 60 => 'year',
              30 * 24 * 60 * 60 => 'month',
              24 * 60 * 60 => 'day',
              60 * 60 => 'hour',
              60 => 'minute',
              1 => 'second'
      );
      foreach ($condition as $secs => $str)
      {
         $d = $estimate_time / $secs;

         if ($d >= 1)
         {
              $r = round($d);
              return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
         }
      }
    }
    public function getFeedLike($data)
    {
        $this->db->select('like');
        $this->db->from('activity_like');
        $this->db->where($data);
        $rows = $this->db->get()->row();
        return $rows->like;
    }
    public function updateFeedLike($update_data,$where)
    {
      $this->db->where($where);
      $this->db->update('activity_like',$update_data);
    }
    public function makeFeedLike($data)
    {
      $this->db->insert('activity_like',$data);
    }
    public function makeFeedComment($data)
    {
      $this->db->insert('activity_comment',$data);
      return $this->db->insert_id();
    }
    public function getComments($id,$type,$user_id=0)
    {
      $this->db->select("ac.comment_id,ac.comment,concat(usr.Firstname,' ',usr.Lastname) as name,CASE WHEN usr.Logo IS NULL THEN '' ELSE usr.Logo END as logo,ac.datetime,comment_image,ac.user_id",false);
      $this->db->from('activity_comment ac');
      $this->db->join('user usr','ac.user_id = usr.Id');
      $this->db->where('ac.module_primary_id',$id);
      $this->db->where('ac.module_type',$type);
      $this->db->order_by('ac.datetime DESC');
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $key => $value) {
        $result[$key]['show_delete'] = ($user_id == $value['user_id']) ? TRUE : FALSE ;
        $result[$key]['datetime'] = $this->get_timeago(strtotime($value['datetime']));
        $img = json_decode($value['comment_image']);
        $result[$key]['comment_image'] = ($img) ? $img : [];
      }
      return $result;
    }
    public function getCommentImages($activity_id)
    {
        $this->db->select('comment_image');
        $this->db->from('activity_comment');
        $this->db->where('comment_id',$activity_id);
        $rows = $this->db->get()->row();
        return $rows->comment_image;
    }
    public function deleteComment($comment_id)
    {
      $this->db->where('comment_id',$comment_id);
      $this->db->delete('activity_comment');
    }
}

?>
    
    