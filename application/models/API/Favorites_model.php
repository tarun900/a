<?php

class favorites_model extends CI_Model
{

   function __construct()
   {
      $this->db1 = $this->load->database('db1', TRUE);
      parent::__construct();
   }

   public function addOrRemoveFavorites($favorites_data)
   {
      $count = $this->db->select('*')->from('my_favorites')->where($favorites_data)->get()->num_rows();

      if($count)
      {
        $this->db->where($favorites_data);
        $this->db->delete('my_favorites');
        $return = 0;
      }
      else
      {
        $favorites_data['datetime'] = date('Y-m-d H:i:s');
        $this->db->insert('my_favorites',$favorites_data);
        $return = 1;
      }
      return $return;
   }

   public function getAllFavorites($where)
   {
      $data = $this->db->select('*')
      ->from('my_favorites')
      ->where($where)
      ->get()->result_array();

      foreach ($data as $key => $value) 
      {
        $return_data['Id'] = $value['Id'];
        $return_data['module_id'] = $value['module_id'];
        $return_data['module_type'] = $value['module_type'];

        switch ($value['module_type']) 
        {
          case '3':
              $ex = $this->db->select('Heading,company_logo,stand_number')
              ->from('exibitor')->where('Id',$value['module_id'])->get()->row_array();
              $cmpy_logo_decode = json_decode($ex['company_logo']);
              $return_data['user_name'] = ($ex['Heading']) ? $ex['Heading'] : '';
              $return_data['logo'] = ($cmpy_logo_decode) ? $cmpy_logo_decode[0] : '';
              $return_data['extra'] = ($ex['stand_number']) ? $ex['stand_number'] : '';
            break;

          case '43':
              $ex = $this->db->select('Sponsors_name,company_logo,Company_name')
              ->from('sponsors')->where('Id',$value['module_id'])->get()->row_array();
              $cmpy_logo_decode = json_decode($ex['company_logo']);
              $return_data['user_name'] = ($ex['Sponsors_name']) ? $ex['Sponsors_name'] : '';
              $return_data['logo'] = ($cmpy_logo_decode) ? $cmpy_logo_decode[0] : '';
              $return_data['extra'] = ($ex['Company_name']) ? $ex['Company_name'] : '';
            break;
         
          default:
              $ex = $this->db->select('Firstname,Lastname,Logo,Company_name')
              ->from('user')->where('Id',$value['module_id'])->get()->row_array();
              $name = $ex['Firstname'] . " " . $ex['Lastname'];
              $return_data['user_name'] = ($name) ? $name : '';
              $return_data['logo'] = ($ex['Logo']) ? $ex['Logo'] : '';
              $return_data['extra'] = ($ex['Company_name']) ? $ex['Company_name'] : '';
            break;
        }

        $return[] = $return_data;
      }
      return $return;
   }

}
    
    