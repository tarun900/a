<?php
class My_contact_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function get_contact_user_list($event_id = null, $userid=NULL)

    {
        $this->db->select('u.Id as uid,u.Firstname,u.Lastname,u.Logo');
        $this->db->from('user u');
        $this->db->join('relation_event_user ru', 'ru.User_id = u.Id', 'left');
        $this->db->join('product_user_bids p', 'p.User_id = u.Id', 'left');
        $this->db->join('role r', 'ru.Role_id = r.Id', 'left');
        if ($event_id)
        {
            $this->db->where('ru.Event_id', $event_id);
        }
        $this->db->where('u.Id !=', $userid);
        $this->db->where('(r.Name ="Attendee"', NULL, FALSE);
        $this->db->or_where('r.Name= "Speaker")', NULL, FALSE);
        $this->db->group_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        $this->db->select('uc.Sharedto_id');
        $this->db->from('user_mycontact uc');
        $this->db->where('uc.Sharedwith_id', $userid);
        $this->db->where('uc.Event_id', $event_id);
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        $shareduser = array();
        foreach($res1 as $keyshared => $valueshered)
        {
            $shareduser[] = $valueshered['Sharedto_id'];
        }
        $contactd = array();
        foreach($res as $userkey => $userdata)
        {
            if (!in_array($userdata['uid'], $shareduser))
            {
                $contactd[] = $userdata;
            }
        }
        return $contactd;
    }
    public function addcontact($data=NULL)

    {
        $this->db->insert('user_mycontact', $data);
    }
    public function updatecontact($data=NULL, $id=NULL)

    {
        $this->db->where('id', $id);
        $this->db->update('user_mycontact', $data);
        return true;
    }
    public function getcontanctdetails($event_id=NULL, $userid=NULL, $isfriend = null)

    {
        $this->db->select('uc.id as uc_id,u.Id as uid,u.Firstname,u.Lastname,u.Company_name,uc.Sharedto_id,u.Logo,u1.Id as uid1,u1.Firstname as Firstname1,u1.Lastname as Lastname1,u1.Company_name as Company_name1,uc.Sharedwith_id ,u1.Logo as Logo1,uc.ismyfriend');
        $this->db->from('user_mycontact uc');
        $this->db->join('user u', 'uc.Sharedto_id = u.Id', 'left');
        $this->db->join('user u1', 'uc.Sharedwith_id = u1.Id', 'left');
        if ($userid)
        {
            $this->db->where('(uc.Sharedwith_id=' . $userid, NULL, FALSE);
            $this->db->or_where('uc.Sharedto_id=' . $userid . ')', NULL, FALSE);
        }
        $this->db->where('uc.Event_id', $event_id);
        if ($isfriend != NULL)
        {
            $this->db->where('uc.ismyfriend', $isfriend);
        }
        $query1 = $this->db->get();
        $res = $query1->result_array();
        $sharecontact = array();
        for ($i = 0; $i < count($res); $i++)
        {
            if ($res[$i]['uid'] != $userid)
            {
                $prev = "";
                $fc = strtoupper($res[$i]['Firstname']);
                $al = substr($fc, 0, 1);
                if ($prev == "" || $res[$i]['Firstname'] != $prev)
                {
                    $prev = $res[$i]->Start_date;
                    $sharecontact[$al][$i]['Id'] = $res[$i]['uid'];
                    $sharecontact[$al][$i]['Uc_id'] = $res[$i]['uc_id'];
                    $sharecontact[$al][$i]['Firstname'] = $res[$i]['Firstname'];
                    $sharecontact[$al][$i]['Lastname'] = $res[$i]['Lastname'];
                    $sharecontact[$al][$i]['Company_name'] = $res[$i]['Company_name'];
                    $sharecontact[$al][$i]['Logo'] = $res[$i]['Logo'];
                    $sharecontact[$al][$i]['Ismyfriend'] = $res[$i]['ismyfriend'];
                }
            }
            else if ($res[$i]['uid1'] != $userid)
            {
                $prev = "";
                $fc = strtoupper($res[$i]['Firstname1']);
                $al = substr($fc, 0, 1);
                if ($prev == "" || $res[$i]['Firstname1'] != $prev)
                {
                    $prev = $res[$i]->Start_date;
                    $sharecontact[$al][$i]['Id'] = $res[$i]['uid1'];
                    $sharecontact[$al][$i]['Uc_id'] = $res[$i]['uc_id'];
                    $sharecontact[$al][$i]['Firstname'] = $res[$i]['Firstname1'];
                    $sharecontact[$al][$i]['Lastname'] = $res[$i]['Lastname1'];
                    $sharecontact[$al][$i]['Company_name'] = $res[$i]['Company_name1'];
                    $sharecontact[$al][$i]['Logo'] = $res[$i]['Logo1'];
                    $sharecontact[$al][$i]['Ismyfriend'] = $res[$i]['ismyfriend'];
                }
            }
        }
        return $sharecontact;
    }
}
?>