<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Photo_filter_model extends CI_Model

{
    public function addFilter($data=NULL)

    {
        $this->db->insert('photo_filter_image', $data);
    }
    public function deleteFilter($id=NULL, $event_id=NULL)

    {
        $this->deleteFilterImage($event_id, $id);
        $this->db->where('id', $id)->delete('photo_filter_image');
    }
    public function deleteFilterImage($event_id=NULL, $id=NULL)

    {
        $res = $this->db->where('id', $id)->get('photo_filter_image')->row_array();
        unlink("./assets/photo_filter/" . $event_id . "/" . $res['image']);
    }
    public function getFilters($id=NULL)

    {
        $this->db->where('event_id', $id);
        $res = $this->db->get('photo_filter_image')->result_array();
        return $res;
    }
    public function getPhotosTaken($id=NULL)

    {
        return $this->db->select('p.*,u.Firstname,u.Lastname')->from('photo_filter_uploads p')->join('user u', 'u.Id = p.user_id')->where('event_id', $id)->get()->result_array();
    }
}
/* End of file Photo_filter.php */
/* Location: .//C/Users/nteam/AppData/Local/Temp/fz3temp-2/Photo_filter.php */