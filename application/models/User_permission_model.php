<?php
class User_permission_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }
    public function insert_permssion($data=NULL)

    {
        $this->db->insert('role_permission', $data);
    }
    public function delete_permssion($role_id=NULL, $menu_id=NULL)

    {
        $this->db->where('Role_id', $role_id);
        $this->db->where('Menu_id', $menu_id);
        $this->db->delete('role_permission');
    }
    public function update_permssion($data=NULL, $role_id=NULL, $menu_id=NULL)

    {
        $this->db->where('Role_id', $role_id);
        $this->db->where('Menu_id', $menu_id);
        $this->db->update('role_permission', $data);
    }
    public function get_permissions()

    {
        $orid = $this->data['user']->Id;
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('role_permission r', 'u.Role_id = r.Role_id');
        if ($user[0]->Id)
        {
            $this->db->where('u.Id', $user[0]->Id);
        }
        $this->db->where('u.Role_id', $this->uri->segment(3));
        $this->db->order_by('u.Id desc');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_role_menu($roleid=NULL)

    {
        $this->db->select('Menu_id');
        $this->db->from('role_permission rp');
        $this->db->where('rp.Role_id', $roleid);
        $this->db->order_by('rp.Menu_id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function get_role_menu_by_id($roleid=NULL)

    {
        $roleid = $this->data['user']->Role_id;
        $this->db->select('Menu_id');
        $this->db->from('role_permission rp');
        $this->db->where('rp.Role_id', $roleid);
        $this->db->order_by('rp.Menu_id');
        $query = $this->db->get();
        $res = $query->result_array();
        foreach($res as $key => $value)
        {
            // print_r($value);
            $this->db->select('*');
            $this->db->from('menu m');
            $this->db->where('m.Id', $value['Menu_id']);
            $query = $this->db->get();
            // echo $this->db->last_query(); exit;
            $res = $query->result_array();
            return $res;
        }
        exit;
    }
}
?>