<?php
class Photofilter_model extends CI_Model{
    function __construct()
    {
            parent::__construct();

    }
    public function getFiltersByEvent($event_id=NULL)
    {
    	return $this->db->select('*')->from('photo_filter_image')->where('event_id',$event_id)->get()->result_array();
	}

	public function saveImageInfo($data=NULL)
	{
		return $this->db->insert('photo_filter_uploads',$data);
	}

	public function getPhotosByUser($event_id=NULL,$user_id=NULL)
	{
		return $this->db->select('*')->from('photo_filter_uploads')->where('event_id',$event_id)->where('user_id',$user_id)->get()->result_array();
	}
}