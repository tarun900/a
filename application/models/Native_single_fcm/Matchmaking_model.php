<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Matchmaking_model extends CI_Model

{
	public function __construct()

	{
		parent::__construct();
	}
	public function getModules($event_id=NULL)

	{
		$res = $this->db->where('event_id', $event_id)->get('matchmaking_modules')->row_array();
		return $res;
	}
	public function getAttendees($event_id=NULL, $user_id=NULL, $role_id=NULL, $keyword=NULL)

	{
		$rules = $this->getRules($event_id, $role_id, 2);
		$user = $this->getUser($event_id, $user_id, $role_id);
		foreach($rules as $key => $value)
		{
			$apply_rule = 0;
			switch ($value['field_if'])
			{
			case 'Email':
				if (strpos($user['Email'], $value['contains']) !== false):
					$apply_rule = 1;
				endif;
				break;

			case 'Keywords':
				foreach($user['keywords'] as $k => $v):
					if (strpos($v, $value['contains']) !== false):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;

			default:
				foreach($user['extra_column'] as $k => $v):
					if ($v['key1'] == $value['field_if'] && $v['value'] == $value['contains']):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;
			}
			if ($apply_rule == '1')
			{
				switch ($value['field_then'])
				{
				case 'Email':
					$where[] = '(u.Email LIKE "%' . $value['contains_that'] . '%")';
					break;

				case 'Keywords':
					$where[] = '(u.Id in (select user_id from attendee_keywords where keyword LIKE "%' . $value['contains_that'] . '%" and event_id = ' . $event_id . '))';
					break;

				default:
					$where[] = '(u.Id in (select user_id from attendee_extra_column where key1 = "' . $value['field_then'] . '" and value LIKE "%' . $value['contains_that'] . '%" and event_id = "' . $event_id . '"))';
					break;
				}
			}
		}
		if (!empty($where))
		{
			$where = '(' . implode(' OR ', $where) . ')';
			$this->db->_protect_identifiers = false;
			$this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
	            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
			$this->db->from('event e');
			$this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
			$this->db->join('user u', 'u.Id = ru.User_id');
			$this->db->join('role r', 'ru.Role_id = r.Id');
			$this->db->where('e.Id', $event_id);
			$this->db->where('r.Id', 4);
			$this->db->where('u.Firstname !=""');
			$this->db->where('u.Id not in (select Attendee_id from event_attendee where Event_id=' . $event_id . ' and hide_identity="1")');
			$this->db->where($where);
			if (!empty($keyword)) $this->db->where($keyword);
			$this->db->order_by("concat(u.Firstname,' ',u.Lastname)");
			$this->db->group_by('u.Id');
			$res = $this->db->get()->result_array();
		}
		return $res ? : [];
	}
	public function getExhibitor($event_id=NULL, $user_id=NULL, $role_id=NULL, $keyword=NULL)

	{
		$rules = $this->getRules($event_id, $role_id, 3);
		$user = $this->getUser($event_id, $user_id, $role_id);
		foreach($rules as $key => $value)
		{
			$apply_rule = 0;
			switch ($value['field_if'])
			{
			case 'Email':
				if (strpos($user['Email'], $value['contains']) !== false):
					$apply_rule = 1;
				endif;
				break;

			case 'Keywords':
				foreach($user['keywords'] as $k => $v):
					if (strpos($v, $value['contains']) !== false):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;

			default:
				foreach($user['extra_column'] as $k => $v):
					if ($v['key1'] == $value['field_if'] && strpos($v['value'], $value['contains']) !== false):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;
			}
			if ($apply_rule == '1')
			{
				switch ($value['field_then'])
				{
				case 'Email':
					$where[] = '(u.Email LIKE "%' . $value['contains_that'] . '%")';
					break;

				case 'Keywords':
					$where[] = '(FIND_IN_SET("' . $value['contains_that'] . '",e.Short_desc))';
					break;

				default:
					$where[] = '(u.Id in (select user_id from attendee_extra_column where key1 = "' . $value['field_then'] . '" and value LIKE "%' . $value['contains_that'] . '%" and event_id = "' . $event_id . '"))';
					break;
				}
			}
		}
		if (!empty($where))
		{
			$where = '(' . implode(' OR ', $where) . ')';
			$this->db->select('CASE WHEN e.Id IS NULL THEN "" ELSE e.Id END as exhibitor_page_id,
	                            CASE WHEN e.Heading IS NULL THEN "" ELSE e.Heading END as Heading,
	                            CASE WHEN e.company_logo IS NULL THEN "" ELSE e.company_logo END as company_logo,
	                            CASE WHEN e.stand_number IS NULL THEN "" ELSE e.stand_number END as stand_number,
	                            CASE WHEN e.user_id IS NULL THEN "" ELSE e.user_id END as exhibitor_id', FALSE);
			$this->db->from('exibitor e');
			$this->db->join('user u', 'u.Id = e.user_id');
			$this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
			$this->db->join('exhibitor_type et', 'e.et_id=et.type_id', 'left');
			$this->db->where('e.Event_id', $event_id);
			$this->db->where($where);
			if (!empty($keyword)) $this->db->where($keyword);
			$this->db->order_by('et.type_position');
			$this->db->order_by('e.Heading');
			$this->db->group_by('u.Id');
			$res = $this->db->get()->result_array();
			foreach($res as $key => $value)
			{
				if ($value['company_logo'] == 'null') $res[$key]['company_logo'] = "";
				elseif (!empty($value['company_logo'])) $res[$key]['company_logo'] = json_decode($value['company_logo'], true) [0];
			}
		}
		return $res ? : [];
	}
	public function getSpeaker($event_id=NULL, $user_id=NULL, $role_id=NULL, $keyword=NULL)

	{
		$rules = $this->getRules($event_id, $role_id, 7);
		$user = $this->getUser($event_id, $user_id, $role_id);
		foreach($rules as $key => $value)
		{
			$apply_rule = 0;
			switch ($value['field_if'])
			{
			case 'Email':
				if (strpos($user['Email'], $value['contains']) !== false):
					$apply_rule = 1;
				endif;
				break;

			case 'Keywords':
				foreach($user['keywords'] as $k => $v):
					if (strpos($v, $value['contains']) !== false):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;

			default:
				foreach($user['extra_column'] as $k => $v):
					if ($v['key1'] == $value['field_if'] && $v['value'] == $value['contains']):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;
			}
			if ($apply_rule == '1')
			{
				switch ($value['field_then'])
				{
				case 'Email':
					$where[] = '(u.Email LIKE "%' . $value['contains_that'] . '%")';
					break;

				case 'Keywords':
					$where[] = '(u.Id in (select user_id from attendee_keywords where keyword LIKE "%' . $value['contains_that'] . '%" and event_id = ' . $event_id . '))';
					break;

				default:
					$where[] = '(u.Id in (select user_id from attendee_extra_column where key1 = "' . $value['field_then'] . '" and value LIKE "%' . $value['contains_that'] . '%" and event_id = "' . $event_id . '"))';
					break;
				}
			}
		}
		if (!empty($where))
		{
			$event = $this->db->select('*')->from('event')->where('Id', $event_id)->get()->row_array();
			$where = '(' . implode(' OR ', $where) . ')';
			/*$sprids = $this->db->where('event_id',$event_id)->get('speaker_profile')->result_array();
			$sprids = array_column_1($sprids,'user_id');*/
			$this->db->_protect_identifiers = false;
			$this->db->select('CASE WHEN u.Id IS NULL THEN "" ELSE u.Id END as Id,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Firstname,CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END as Lastname,CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END as Company_name
	            ,CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END as Title,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Logo', FALSE);
			$this->db->from('event e');
			$this->db->join('relation_event_user ru', 'ru.Event_id = e.Id');
			$this->db->join('user u', 'u.Id = ru.User_id');
			$this->db->join('role r', 'ru.Role_id = r.Id');
			$this->db->where('e.Id', $event_id);
			$this->db->where('r.Id', 7);
			$this->db->where('u.Firstname !=""');
			$this->db->where('u.key_people', '0');
			$this->db->where('u.is_moderator', '0');
			$this->db->where($where);
			if (!empty($keyword)) $this->db->where($keyword);
			/*if(!empty($sprids))
			$this->db->where_not_in('u.Id',$sprids);*/
			if ($event['key_people_sort_by'] == '1') $this->db->order_by('u.Lastname');
			else $this->db->order_by('u.Firstname');
			$this->db->group_by('u.Id');
			$res = $this->db->get()->result_array();
		}
		return $res ? : [];
	}
	public function getSponsor($event_id=NULL, $user_id=NULL, $role_id=NULL, $keyword=NULL)

	{
		$rules = $this->getRules($event_id, $role_id, 43);
		$user = $this->getUser($event_id, $user_id, $role_id);
		foreach($rules as $key => $value)
		{
			$apply_rule = 0;
			switch ($value['field_if'])
			{
			case 'Email':
				if (strpos($user['Email'], $value['contains']) !== false):
					$apply_rule = 1;
				endif;
				break;

			case 'Keywords':
				foreach($user['keywords'] as $k => $v):
					if (strpos($v, $value['contains']) !== false):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;

			default:
				foreach($user['extra_column'] as $k => $v):
					if ($v['key1'] == $value['field_if'] && $v['value'] == $value['contains']):
						$apply_rule = 1;
						break;
					endif;
				endforeach;
				break;
			}
			if ($apply_rule == '1')
			{
				switch ($value['field_then'])
				{
				case 'Keywords':
					$where[] = '(s.Id in (select user_id from sponsors_keywords where keyword LIKE "%' . $value['contains_that'] . '%" and event_id = ' . $event_id . '))';
					break;
				}
			}
		}
		if (!empty($where))
		{
			$where = '(' . implode(' OR ', $where) . ')';
			$this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,
                             CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                             CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                             CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name', FALSE);
			$this->db->from('sponsors s');
			$this->db->where('s.Event_id', $event_id);
			$this->db->where($where);
			if (!empty($keyword)) $this->db->where($keyword);
			$this->db->order_by('s.Sponsors_name');
			$res = $this->db->get()->result_array();
			foreach($res as $key => $value)
			{
				$res[$key]['company_logo'] = json_decode($value['company_logo'], true) [0] ? : '';
			}
		}
		return $res ? : [];
	}
	public function getRules($event_id=NULL, $role_id=NULL, $menu_id_then=NULL)

	{
		$menu_id = $this->getMenuId($role_id);
		$this->db->where('event_id', $event_id);
		$this->db->where('profile_type_if', $menu_id);
		$this->db->where('profile_type_then', $menu_id_then);
		$res = $this->db->get('matchmaking_rules')->result_array();
		return $res;
	}
	public function getMenuId($role_id=NULL)

	{
		switch ($role_id)
		{
		case '4':
			return 2;
			break;

		case '6':
			return 3;
			break;

		case '7':
			return 7;
			break;
		}
	}
	public function getUser($event_id=NULL, $user_id=NULL, $role_id=NULL)

	{
		$res = $this->db->where('Id', $user_id)->get('user')->row_array();
		if ($role_id == '6')
		{
			$keywords = explode(',', $this->db->where('Event_id', $event_id)->where('user_id', $user_id)->get('exibitor')->row_array() ['Short_desc']);
		}
		else
		{
			$keywords = $this->db->where('event_id', $event_id)->where('user_id', $user_id)->get('attendee_keywords')->result_array();
			$keywords = array_column_1($keywords, 'keyword');
			$res['extra_column'] = $this->db->where('event_id', $event_id)->where('user_id', $user_id)->get('attendee_extra_column')->result_array();
		}
		$res['keywords'] = $keywords;
		return $res;
	}
	public function getModuleName($event_id=NULL, $menu_id=NULL)

	{
		// $menu_id = array(2,3,7,43);
		$this->db->select('m.id,
				    	   CASE WHEN em.title IS NULL THEN m.menuname ELSE em.title END as menuname', FALSE);
		$this->db->join('event_menu em', 'em.menu_id=m.id and em.event_id=' . $event_id . '', 'left');
		$this->db->join('event e', 'e.Id=em.event_id', 'left');
		$this->db->where_in('m.id', $menu_id);
		$res = $this->db->get('menu m')->result_array();
		return $res;
	}
	public function markAsVisited($data=NULL)

	{
		unset($data['lang_id']);
		$this->db->insert('matchmaking_visited', $data);
	}
}
/* End of file matchmaking_model.php */
/* Location: ./application/models/matchmaking_model.php */