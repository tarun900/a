<?php
$num_columns	= 15;
$can_delete	= $this->auth->has_permission('Orders.Content.Delete');
$can_view		= $this->auth->has_permission('Orders.Content.View');
$can_edit		= $this->auth->has_permission('Orders.Content.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

/*if($_SERVER['SERVER_NAME']=='localhost')
{
    $mag_base = "http://localhost/open_ordermgmt/trunk/magento/";
}
else
{
    //$mag_base = 'http://webcluesglobal.com/qa/ordermanage/magento/';
	$mag_base = 'http://fmv.cc/sm_papa';
}*/
//$mag_base = 'http://fmv.cc/sm_papa';
$mag_base = 'http://sayitloud.in/';

if ($can_delete) {
    $num_columns++;
}
?>

<?php if (!isset($ajax)) : ?>
<div class="tabbable">
    <ul id="myTab" class="nav nav-tabs">
        <?php $active_class_counter = 0; ?>
        <?php if (has_permission('Orders.Content.NewOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example1" data-toggle="tab">
                New Orders <span class="badge badge-danger"><?php echo count($total_pending_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.ConfirmOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example2" data-toggle="tab">
                Confirmed Orders <span class="badge badge-purple"><?php echo count($total_confirmed_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.PackOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example3" data-toggle="tab">
                Pack Orders <span class="badge badge-warning"><?php echo count($total_packed_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.DispatchOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example4" data-toggle="tab">
                Dispatch Orders <span class="badge badge-primary"><?php echo count($total_dispatched_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.PickOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example5" data-toggle="tab">
                Picked Orders <span class="badge badge-success"><?php echo count($total_picked_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.DeliverOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example6" data-toggle="tab">
                Delivered Orders <span class="badge badge-default"><?php echo count($total_delivered_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.ReturnOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example7" data-toggle="tab">
                Return Orders <span class="badge badge-danger"><?php echo count($total_return_order);?></span>
            </a>
        </li>
        <?php endif;?>
        <?php if (has_permission('Orders.Content.PartialDispatchOrders')):?>
        <li <?php if($active_class_counter == 0){ ?> class="active" <?php $active_class_counter = 1; } ?>>
            <a href="#myTab_example8" data-toggle="tab">
                Partial Dispatch Orders <span class="badge badge-danger"><?php echo count($total_partial_order);?></span>
            </a>
        </li>
        <?php endif;?>
    </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="myTab_example1">
                    <div class='admin-box'>
                        <div style="margin-bottom: 10px;width: auto; float: left;margin-right: 10px" id="import_div">
                            <?php echo form_open_multipart($this->uri->uri_string().'/import/', array('name'=>'frm_import')); ?>
                            <input type="hidden" id="csrf" name="ci_csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <input type="file" name="file_xls" accept=".csv" class="btn btn-info" style="float: left;margin-right: 5px" />
                            <input type="submit" id="btn_import" name="btn_import" class='btn btn-info' title='Import Records' value="Import Records" style="float: left;margin-right: 5px">
                            <?php echo form_close();?>
                        </div>
                        <div style="float: left;display: none;"><a class="btn btn-warning" id='ci_to_mag' title="Sync To Magento">Sync To Magento</a></div>
                        <div style="float: left;margin-left: 0px;margin-bottom: 10px;" id="invoice_div">
                            <form style="float: left; margin: 5px;" method="POST" target="_blank" action="<?php echo base_url().'admin/content/orders/gridinvoce';?>">
                                <div class="hdnEl"></div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                                <input type="submit" id="btn_invoice" name="btn_invoice" class='btn btn-info' title='View Invoice' value="View Invoice">
                            </form>
                            <form id="change_order_status_form" style="float: left; margin: 5px;" method="POST" action="<?php echo base_url().'admin/content/orders/updateOrderStatus';?>">
                                <div class="hdnEl"></div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                                <input type="hidden" name="order_status" id="order_status" value="dispatched" />
                                <input type="submit" id="btn_update_order_status" name="btn_update_order_status" class='btn btn-info' title='Move to Dispatch' value="Move to Dispatch">
                            </form>
                        </div>
                        
                        <?php if($this->session->flashdata('import_summary')):?>
                            <div style="padding: 7px;width: 470px;height: 34px;float:right;" class="alert-success">
                                <label>Total Records</label>
                                <span>
                                    <?php echo $this->session->flashdata('import_summary')['total_records'].' , ';?>
                                </span>

                                <label>Updated Records</label>
                                <span>
                                    <?php echo $this->session->flashdata('import_summary')['inserted_records'].' , ';?>
                                </span>

                                <label>Skipped Records</label>
                                <span>
                                    <?php echo $this->session->flashdata('import_summary')['skipped_records'];?>
                                </span>
                            </div>
                        <?php endif ?>
                        <?php
                            $attributes = array(
                                'name' => 'admin_listing_form',
                                'id' => 'admin_listing_form',
                                'class'=>'form-inline'
                            );
                            echo form_open($this->uri->uri_string().'/index', $attributes);
                        ?>
                        <div id='ajax_loader'></div>
                        <div class='grid-filters' style="clear:both">
                            <input type="hidden" value="" name="sortby" id="sortby" class="reset-input">
                            <input type="hidden" value="" name="order" id="order" class="reset-input">
                            <input type="hidden" value="" name="action" id="action" class="reset-input">
                            <table>
                                <tr>
                                    <?php /*
                                    <td>
                                            <select name='category' id="category_dropdown" class='category-dropdown reset-dropdown form-control input-small' ><option value='all'>All(Status)</option><option value='canceled'>Canceled</option><option value='closed'>Closed</option><option value='complete'>Complete</option><option value='fraud'>Suspected Fraud</option><option value='holded'>On Hold</option><option value='payment_review'>Payment Review</option><option value='paypal_canceled_reversal'>PayPal Canceled Reversal</option><option value='paypal_reversed'>PayPal Reversed</option><option value='pending'>Pending</option><option value='pending_payment'>Pending Payment</option><option value='pending_paypal'>Pending PayPal</option><option value='processing'>Processing</option></select>&nbsp;
                                    </td>
                                     */ ?>
                                
                                    
                                    <input type="hidden" name="category" id="category_dropdown" value="" /> <!-- value="pending" -->
                                    <td><input type='text' class='search-field reset-input form-control' rel_id='serach_filed1' name='search[increment_id]' />&nbsp;</td><td><select class='search-field-dropdown reset-dropdown form-control input-small ' rel='serach_filed1'><option value='increment_id'>Order #</option><option value='BillingAddress'>Bill To Name</option><option value='ShippingAddress'>Ship To Name</option><option value='telephone'>Phone</option><option value='customer_email'>Email</option></select>&nbsp;</td>
                                    <td></td>
                                    <td><input class="search-field  form-control FromDatepicker" value="" id="txtFrom" type="text" name="between[created_at][from]" placeholder="From Date"  />&nbsp;</td>
                                    <td><input class="search-field  form-control ToDatepicker" id="txtTo" type="text" name="between[created_at][to]"   placeholder="To Date" />&nbsp;</td>
                                    <td>
                                        <label class="checkbox-inline">
                                                <input type="checkbox" name="searchAll" value="1" class="grey">
                                                Search into all order status
                                        </label>
                                        &nbsp;
                                    </td>
                                    <td><button type='button' class='btn submit-filters' title='Find' data-original-title=''>Find</button>&nbsp;</td>
                                    <td><button type='button' class='btn reset-filters' title='Reset' data-original-title=''>Reset</button>&nbsp;</td>
                                    
                                    <?php /*<td>
                                        <input type="submit" id="btn_export_1" name="btn_export" class='btn btn-info' title='Export All Pending Order' onclick="return pending_order_select();" value="Export All Pending Order">&nbsp;
                                    </td>*/?>
                                </tr>
                                <tr class="exp_tr">
    <td colspan="8" style="padding:5px 5px 0px 0px;">
                                        <input type="submit" id="btn_export" name="btn_export" class='btn btn-info' title='Export Records' value="Export Records">&nbsp;
                                         <input type="submit" id="btn_packing_list_export" name="btn_packing_list_export" class='btn btn-info' title='Export Records' value="Export Packing List">&nbsp;
                                    </td>
                                    
</tr>
                            </table>
                        </div>
                        <script type="text/javascript">
                            jQuery(function(){                                
                                
                                jQuery(".checkboxes").live('click',function(){
                                 if(jQuery(this).prop("checked")==true)
                                   { 
                                     if(jQuery("#"+jQuery(this).val()).length == 0)
                                     {
                                       jQuery("<input>").attr({
                                           type:'hidden',
                                           id:jQuery(this).val(),
                                           value:jQuery(this).val(),
                                           name:'arrOrders[]',
                                           class:"hdnOd hide_checkbox_val "+jQuery(this).val()
                                       }).appendTo(".hdnEl");
                                     }
                                   }
                                   else
                                   {
                                       var strId = jQuery(this).val();
                                       jQuery(".hdnEl ."+strId).remove();
                                   }
                                });
                            });
                            function pending_order_select()
                            {
                                document.getElementById('category_dropdown').value='pending';
                                return true;
                            }
                        </script>    
                        <div id='table_content'>
                <?php endif; ?>
                <table class='table table-striped table-hover dataTable no-footer order_table'>
                <thead>
                <?php if ($has_records) : ?>
                <!-- <tfoot> -->
                    <tr style="border-top: 1px solid #ccc">
                        <td colspan='<?php echo $num_columns; ?>'>
                            <?php
                            if (isset($pagination)) {
                                echo $pagination;
                            }
                            ?>
                        </td>
                    </tr>
                <!-- </tfoot> -->
                <?php endif; ?>
                <tr>
                    <th class="checkbox_td">&nbsp;</th>
                    <th width="10%">Order#<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="increment_id" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="increment_id" title="Desc" effect="tooltip"></i>
                    </th>
                    <th width="60%">Order Summary<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="increment_id" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="increment_id" title="Desc" effect="tooltip"></i>
                    </th>
                    <th width="20%">Order Status<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="status" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="status" title="Desc" effect="tooltip"></i>
                    </th>
                    <th width="20%">Order Date<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="created_at" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="created_at" title="Desc" effect="tooltip"></i>
                    </th>
                    <th width="20%">Quantity & Price<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="grand_total" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="grand_total" title="Desc" effect="tooltip"></i>
                    </th>
                    <th width="20%">Buyer Details<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="BillingAddress" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="BillingAddress" title="Desc" effect="tooltip"></i>
                    </th>
                    <?php /* <th>Order Status
                        <i class="icon-arrow-up sort" rel="asc" for="status" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="status" title="Desc" effect="tooltip"></i>
                    </th> */ ?>
                    <th width="30%">Order Type<br/>
                        <i class="icon-arrow-up sort" rel="asc" for="payment_info" title="Asc" effect="tooltip"></i>
                        <i class="icon-arrow-down sort" rel="desc" for="payment_info" title="Desc" effect="tooltip"></i>
                    </th>
                </tr>
                </thead>

                <tbody>
                <?php
                    if ($has_records) :
                        foreach ($records as $record) :

                        $my_data =array();
                    ?>
                            <tr>
                                <td class="checkbox_td"><input class="checkboxes" type="checkbox" id="<?php echo 'checkbox_'.$record->entity_id;?>" value="<?php echo $record->entity_id;?>" /></td>
                                <td>
                                    <?php if ($can_view) : ?>
                                        <?php echo anchor(SITE_AREA .'/content/orders/view/'.$record->entity_id, '<i title="View" effect="tooltip" >'.$record->increment_id.'</i>') ?>&nbsp;&nbsp;
                                    <?php else : ?>
                                        <?php echo $record->increment_id;?>
                                    <?php endif;?>
                                </td>
                                <td>
                                    <?php 
                                    foreach($record->order_meta as $order_meta_key => $order_meta_value)
                                    {
                                       
                                        if($order_meta_value['product_type']=='simple' && empty($order_meta_value['parent_item_id']))
                                        {
                                            $my_data[] = array(
                                                'name' => $order_meta_value['name'],
                                                'item_id' => $order_meta_value['item_id'],
                                                'sku' => $order_meta_value['sku'],
                                                'img' => $order_meta_value['image'],
                                                'price' => $order_meta_value['price'],
                                                'qty_ordered' => $order_meta_value['qty_ordered']

                                            );
                                        }
                                        else
                                        {
                                            foreach($record->order_meta as $order_meta_sub_key => $order_meta_sub_value)
                                            {
                                                if(($order_meta_value['product_type']=='configurable' && empty($order_meta_value['parent_item_id'])) && ($order_meta_sub_value['product_type'] == 'simple' && $order_meta_sub_value['parent_item_id'] == $order_meta_value['item_id']))
                                                {
                                                    $my_data[] = array(
                                                        'name' => $order_meta_value['name'],
                                                        'item_id' => $order_meta_value['item_id'],
                                                        'sku' => $order_meta_sub_value['sku'],
                                                        'img' => $order_meta_sub_value['image'],
                                                        'price' => $order_meta_value['price'],
                                                        'qty_ordered' => $order_meta_value['qty_ordered']

                                                    );
                                                }
                                            }
                                        }
                                    }

                                    foreach($my_data as $my_key => $my_val)
                                    {
                                        ?>
                                        <table border="0" style="height: 150px" width="100%" class="inner_table">
                                            <tr>
                                                <td rowspan="6">
                                                    <?php if($my_val['img']!='no_selection'):?>
                                                        <img src="<?php echo $mag_base.'/media/catalog/product/'.$my_val['img'];?>" width="100" alt="Image">
                                                    <?php else: ?>
                                                        No Image
                                                    <?php endif ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Name</b></td>
                                                <td><?php echo $my_val['name'];?></td>
                                            </tr>
                                            <tr>
                                                <td><b>SKU</b></td>
                                                <td><?php echo $my_val['sku'];?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Price</b></td>
                                                <td>&#8377;<?php echo number_format((float)$my_val['price'], 2, '.', '');?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Qty</b></td>
                                                <td><?php echo number_format($my_val['qty_ordered']);?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><b>Order Date</b></td>
                                                <td><?php echo date_format(new DateTime($record->created_at),'M j, Y g:i:s A');?></td>
                                            </tr>
                                        </table>
                                    <?php 
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        /*if(trim($record->order_history[0]['status'])=='man123'):
                                            echo "<b>Partial Dispatched</b>";
                                        else:
                                            echo '<b>'.ucfirst($record->order_history[0]['status']).'</b>';
                                        endif;*/
                                        if(trim($record->order_history[0]['status'])=='man123'):
                                            echo "<b>Partial Dispatched</b>";
                                        else:
                                            echo '<b>'.ucfirst($record->status).'</b>';
                                        endif;
                                    //echo '(<b>'.ucfirst($record->state).'</b>)';
                                    ?>
                                    <?php //echo '</br>on '.date_format(new DateTime($record->created_at),'l, jS M Y');?>
                                </td>
                                <td>
                                    <?php echo date_format(new DateTime($record->created_at),'l, jS M Y'); ?>
                                </td>
                                <td>
                                        <table border="0" style="height: 200px" width="100%">
                                            <tr>
                                                <td><b>Sub Total</b></td>
                                                <td>&#8377;<?php echo number_format((float)$record->subtotal, 2, '.', ''); ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Shipping</b></td>
                                                <td>&#8377;<?php echo number_format((float)$record->shipping_amount, 2, '.', ''); ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Total</b></td>
                                                <td>&#8377;<?php echo number_format((float)$record->grand_total, 2, '.', ''); ?></td>
                                            </tr>
                                        </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <?php /*<td>
                                                <b>Billing Info</b>
                                                <?php
                                                    echo $record->billing_address[0]['firstname'].' '.$record->billing_address[0]['middlename'].' '.$record->billing_address[0]['lastname'];
                                                    echo "<br/>".$record->billing_address[0]['street'];
                                                    echo "<br/>".$record->billing_address[0]['city'].', '.$record->billing_address[0]['region'].', '.$record->billing_address[0]['postcode'];
                                                    echo "<br/>".$record->billing_address[0]['country_id'];
                                                    echo "<br/> T: ".$record->billing_address[0]['telephone'];
                                                ?>
                                            </td> */?>
                                            <td>
                                                <b>Shipping Info</b>
                                                <?php
                                                    echo $record->shipping_address[0]['firstname'].' '.$record->shipping_address[0]['middlename'].' '.$record->shipping_address[0]['lastname'];
                                                    echo "<br/>".$record->shipping_address[0]['street'];
                                                    echo "<br/>".$record->shipping_address[0]['city'].', '.$record->shipping_address[0]['region'].', '.$record->shipping_address[0]['postcode'];
                                                    echo "<br/>".$record->shipping_address[0]['country_id'];
                                                    echo "<br/> T: ".$record->shipping_address[0]['telephone'];
                                                ?>
                                            </td>
                                        </tr>
                                        <?php /*?><tr>
                                            <td>
                                                <b>Shipping Info</b>
                                                <?php
                                                    echo $record->shipping_address[0]['firstname'].' '.$record->shipping_address[0]['middlename'].' '.$record->shipping_address[0]['lastname'];
                                                    echo "<br/>".$record->shipping_address[0]['street'];
                                                    echo "<br/>".$record->shipping_address[0]['city'].', '.$record->shipping_address[0]['region'].', '.$record->shipping_address[0]['postcode'];
                                                    echo "<br/>".$record->shipping_address[0]['country_id'];
                                                    echo "<br/> T: ".$record->shipping_address[0]['telephone'];
                                                ?>
                                            </td>
                                        </tr><?php */?>
                                    </table>
                                </td>
                                <?php /* <td>
                                    <?php 
                                            if(trim($record->order_history[0]['status'])=='man123'):
                                                echo "Partial Dispatched";
                                            else:
                                                echo ucfirst($record->order_history[0]['status']);
                                            endif;
                                        ?>
                                </td> */ ?>    
                                <td align="center"><?php echo ($record->payment_info=="Cash On Delivery")?"COD":"NONCOD";?></td>
                                
                            </tr>
                <?php endforeach;
                    else:
                ?>
                        <tr>
                            <td colspan='<?php echo $num_columns; ?>'><?php echo lang('orders_records_empty'); ?></td>
                        </tr>
                <?php endif; ?>
                </tbody>
            </table>
                
        <?php if (!isset($ajax)) : ?>
            <?php echo form_close(); ?>
            </div>
    </div>
            </div>
        </div>
</div>
<?php endif; ?>

<script>
    $('#ci_to_mag').click(function() {
        var csrf = '<?php echo $this->security->get_csrf_hash();?>';
        $.ajax({
            url: '<?php echo site_url('admin/content/orders/sync_to'); ?>',
            type: 'POST',
            data: {
                ci_csrf_token: csrf
            },
            dataType: 'html',
            success: function(data) {
                console.log(data);
                return false;
                window.location.reload();
            }
        });
    });
    
    //if($('#category_dropdown').val()=='pending' || $('#category_dropdown').val()=='confirmed')
    if($('#category_dropdown').val()=='' || $('#category_dropdown').val()=='confirmed')
    {
        $('#invoice_div').css('display','none');
        $('#import_div').css('display','block');
        //$('.checkbox_td').css('display','none');
        $('.checkbox_td').hide();
    }
    
    if($('#category_dropdown').val()=='packed' || $('#category_dropdown').val()=='dispatched' || $('#category_dropdown').val()=='picked' || $('#category_dropdown').val()=='delivered' || $('#category_dropdown').val()=='return')
    {
        $('#invoice_div').css('display','block');
        $('#import_div').css('display','none');
        //$('.checkbox_td').css('display','block');
        $('.checkbox_td').show();
        
        if($('#category_dropdown').val()=='packed')
        {
            $('#change_order_status_form').css('display','block');
            $('#order_status').val('dispatched');
            $('#btn_update_order_status').val('Move to Dispatch');
            $('#btn_update_order_status').attr('title','Move to Dispatch');
            $('#btn_packing_list_export').val('Export Packing List');
        }
        else if($('#category_dropdown').val()=='dispatched')
        {
            $('#change_order_status_form').css('display','block');
            $('#order_status').val('picked');
            $('#btn_update_order_status').val('Move to Picked');
            $('#btn_update_order_status').attr('title','Move to Picked');
            $('#btn_packing_list_export').val('Export Dispatched List');
        }
        else if($('#category_dropdown').val()=='picked')
        {
            $('#change_order_status_form').css('display','block');
            $('#order_status').val('delivered');
            $('#btn_update_order_status').val('Move to Delivered');
            $('#btn_update_order_status').attr('title','Move to Delivered');
            $('#btn_packing_list_export').val('Export Picked List');
        }
        else if($('#category_dropdown').val()=='delivered')
        {
            $('#change_order_status_form').css('display','block');
            $('#order_status').val('return');
            $('#btn_update_order_status').val('Move to Return');
            $('#btn_update_order_status').attr('title','Move to Return');
            $('#btn_packing_list_export').val('Export Delivered List');
        }
        else
        { 
            $('#btn_packing_list_export').val('Export Return List');
            $('#change_order_status_form').css('display','none');
        }
    }
    else
    {
        $('#btn_packing_list_export').val('Export Partial Dispatch List');
        $('#change_order_status_form').css('display','none');
    }
    
    if($('#category_dropdown').val()=='confirmed')
    {
        $('#btn_packing_list_export').val('Export Confirmed List');
    }
    //else if($('#category_dropdown').val()=='pending')
    else if($('#category_dropdown').val()=='')
    {
        $('#btn_packing_list_export').val('Export New Order List');
    }
    
    $('.hide_checkbox_val').each(function(index){
        var hidden_val = $(this).val();
        $('#checkbox_'+hidden_val).prop( "checked", true );
    })
       
</script>
<style>
    .switch{ display: table-cell !important;}
</style>    
