<?php
class Sponsors_model extends CI_Model

{
  function __construct()
  {
    parent::__construct();
  }
  public function getSponsorsListByEventId($Event_id = null, $user_id=NULL)

  {
    $types = $this->db->select('*')->from('sponsors_type')->where('event_id', $Event_id)->order_by('type_position')->get()->result_array();
    $sposers = array();
    foreach($types as $key => $value)
    {
      $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,
                             CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                             CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                             CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name', FALSE);
      $this->db->from('sponsors s');
      $this->db->where('s.Event_id', $Event_id);
      $this->db->where('s.st_id', $value['type_id']);
      $this->db->order_by('s.Sponsors_name', NULL, FALSE);
      $sp_data = $this->db->get()->result_array();
      if ($sp_data)
      {
        $results['type'] = $value['type_name'];
        $results['bg_color'] = $value['type_color'];
        $results['data'] = $sp_data;
        $sposers[] = $results;
      }
    }
    $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,
                         CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                         CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                         CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name', FALSE);
    $this->db->from('sponsors s');
    $this->db->where('s.Event_id', $Event_id);
    $this->db->where('s.st_id IS NULL');
    $this->db->order_by('s.Sponsors_name', NULL, FALSE);
    $sp1_data = $this->db->get()->result_array();
    if ($sp1_data)
    {
      $results['type'] = '';
      $results['bg_color'] = '';
      $results['data'] = $sp1_data;
      $sposers[] = $results;
    }
    foreach($sposers as $key => $value)
    {
      foreach($value['data'] as $key1 => $value1)
      {
        $sposers[$key]['data'][$key1]['Sponsors_name'] = ucfirst($value1['Sponsors_name']);
        $sposers[$key]['data'][$key1]['Company_name'] = ucfirst($value1['Company_name']);
        $cmpy_logo_decode = json_decode($value1['company_logo']);
        if (empty($cmpy_logo_decode[0]))
        {
          $cmpy_logo_decode[0] = "";
        }
        $sposers[$key]['data'][$key1]['company_logo'] = $cmpy_logo_decode[0];
        $count = $this->db->select('*')->from('my_favorites')->where('event_id', $Event_id)->where('user_id', $user_id)->where('module_type', '43')->where('module_id', $value1['Id'])->get()->num_rows();
        $sposers[$key]['data'][$key1]['is_favorites'] = ($count) ? '1' : '0';
      }
    }
    return $sposers;
  }
  public function getSponsorsDetails($id=NULL, $event_id=NULL, $token=NULL, $lang_id=NULL)

  {
    $this->db->protect_identifiers = false;
    $token = ($token == NULL) ? '' : $token;
    $this->db->select('CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                           CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name,
                          (CASE WHEN elmc.content IS NULL THEN  CASE WHEN s.Description IS NULL THEN "" ELSE s.Description END ELSE elmc.content END) as Description,
                           CASE WHEN s.website_url IS NULL THEN "" ELSE s.website_url END as website_url,
                           CASE WHEN s.facebook_url IS NULL THEN "" ELSE s.facebook_url END as facebook_url,
                           CASE WHEN s.twitter_url IS NULL THEN "" ELSE s.twitter_url END as twitter_url,
                           CASE WHEN s.linkedin_url IS NULL THEN "" ELSE s.linkedin_url END as linkedin_url,
                           CASE WHEN s.instagram_url IS NULL THEN "" ELSE s.instagram_url END as instagram_url,
                           CASE WHEN s.youtube_url IS NULL THEN "" ELSE s.youtube_url END as youtube_url,
                           CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                           CASE WHEN s.Images IS NULL THEN "" ELSE s.Images END as Images,
                           CASE WHEN st.type_name IS NULL THEN "" ELSE st.type_name END as type,
                           CASE WHEN st.type_color IS NULL THEN "" ELSE st.type_color END as type_color', FALSE);
    $this->db->from('sponsors s');
    $this->db->join('event_lang_modules_content elmc', 'elmc.menu_id=43 and elmc.modules_id=s.Id and elmc.lang_id="' . $lang_id . '" and elmc.event_id=' . $event_id, 'left');
    $this->db->join('sponsors_type st', 'st.type_id = s.st_id', 'left');
    $this->db->where('s.Id', $id);
    $query = $this->db->get();
    $res = $query->result_array();
    if (!empty($res[0]))
    {
      $user = $this->db->select('*')->from('user')->where('token', $token)->get()->row_array();
      $extra = $this->db->select('*')->from('event_attendee')->where('Event_id', $event_id)->where('Attendee_id', $user['Id'])->get()->row_array();
      $extra = json_decode($extra['extra_column'], true);
      foreach($extra as $key => $value)
      {
        $keyword = "{" . str_replace(' ', '', $key) . "}";
        if (stripos(strip_tags($res[0]['Description'], $keyword)) !== false)
        {
          $res[0]['Description'] = str_ireplace($keyword, $value, $res[0]['Description']);
        }
      }
    }
    return $res;
  }
  public function getUserId($token=NULL)

  {
    $data = $this->db->select('Id')->from('user')->where('token', $token)->get()->row_array();
    return $data['Id'];
  }
  public function getSponsorsListByEventId_offline($Event_id = null, $user_id=NULL)

  {
    $types = $this->db->select('*')->from('sponsors_type')->where('event_id', $Event_id)->order_by('type_position')->get()->result_array();
    $sposers = array();
    foreach($types as $key => $value)
    {
      $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,
                             CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                             CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                             CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name', FALSE);
      $this->db->from('sponsors s');
      $this->db->where('s.Event_id', $Event_id);
      $this->db->where('s.st_id', $value['type_id']);
      $this->db->order_by('s.Sponsors_name', NULL, FALSE);
      $sp_data = $this->db->get()->result_array();
      if ($sp_data)
      {
        $results['type'] = $value['type_name'];
        $results['bg_color'] = $value['type_color'];
        $results['type_id'] = $value['type_id'];
        $results['type_position'] = $value['type_position'];
        $results['data'] = $sp_data;
        $sposers[] = $results;
      }
    }
    $this->db->select('CASE WHEN s.Id IS NULL THEN "" ELSE s.Id END as Id,
                         CASE WHEN s.Sponsors_name IS NULL THEN "" ELSE s.Sponsors_name END as Sponsors_name,
                         CASE WHEN s.company_logo IS NULL THEN "" ELSE s.company_logo END as company_logo,
                         CASE WHEN s.Company_name IS NULL THEN "" ELSE s.Company_name END as Company_name', FALSE);
    $this->db->from('sponsors s');
    $this->db->where('s.Event_id', $Event_id);
    $this->db->where('s.st_id IS NULL');
    $this->db->order_by('s.Sponsors_name', NULL, FALSE);
    $sp1_data = $this->db->get()->result_array();
    if ($sp1_data)
    {
      $results['type'] = '';
      $results['bg_color'] = '';
      $results['type_id'] = '';
      $results['type_position'] = '9999';
      $results['data'] = $sp1_data;
      $sposers[] = $results;
    }
    foreach($sposers as $key => $value)
    {
      foreach($value['data'] as $key1 => $value1)
      {
        $sposers[$key]['data'][$key1]['Sponsors_name'] = ucfirst($value1['Sponsors_name']);
        $sposers[$key]['data'][$key1]['Company_name'] = ucfirst($value1['Company_name']);
        $cmpy_logo_decode = json_decode($value1['company_logo']);
        if (empty($cmpy_logo_decode[0]))
        {
          $cmpy_logo_decode[0] = "";
        }
        $sposers[$key]['data'][$key1]['company_logo'] = $cmpy_logo_decode[0];
        $count = $this->db->select('*')->from('my_favorites')->where('event_id', $Event_id)->where('user_id', $user_id)->where('module_type', '43')->where('module_id', $value1['Id'])->get()->num_rows();
        $sposers[$key]['data'][$key1]['is_favorites'] = ($count) ? '1' : '0';
      }
    }
    return $sposers;
  }
  public function get_favorited_sponser($event_id=NULL, $user_id=NULL)

  {
    return $this->db->select('module_id as sponser_id')->where('event_id', $event_id)->where('user_id', $user_id)->where('module_type', '43')->get('my_favorites')->result_array();
  }
}
?>