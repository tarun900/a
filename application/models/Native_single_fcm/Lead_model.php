<?php
class Lead_model extends CI_Model

{
    function __construct()
    {
        $this->db1 = $this->load->database('db1', TRUE);
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
    }
    public function get_scan_info($eid=NULL, $uid=NULL, $user_id=NULL, $flag=NULL)

    {
        if (($eid == '634' && $flag == '0') || ($eid == '1012' && $flag == '0'))
        {
            $user_info = $this->db->where('Unique_no', $uid)->get('user')->row_array();
            if (empty($user_info))
            {
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($eid);
                $insert['Unique_no'] = $uid;
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $this->db->insert('user', $insert);
                $uid = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($uid, $eid, $org_id, '4');
            }
            else
            {
                $uid = $user_info['Id'];
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($eid);
                $wdata['Event_id'] = $eid;
                $wdata['User_id'] = $uid;
                $wdata['Organisor_id'] = $org_id;
                $wdata['Role_id'] = 4;
                $count = $this->db->select('*')->from('relation_event_user')->where($wdata)->get()->num_rows();
                if ($count == 0) $this->db->insert("relation_event_user", $wdata);
            }
        }
        // events with Unique no as Barcode
        if ($eid == '1378' || $eid == '1512' || $eid == '1609' || $eid == '1580' || $eid == '1804' || $eid == '1850')
        {
            $this->db->select('u.Id');
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            $this->db->where('u.Unique_no', $uid);
            $this->db->where('reu.Event_id', $eid);
            $this->db->where('reu.Role_id', '4');
            $user_info = $this->db->get('user u')->row_array();
            if (empty($user_info))
            {
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($eid);
                $insert['Unique_no'] = $uid;
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $this->db->insert('user', $insert);
                $uid = $this->db->insert_id();
                $rel_insert['Role_id'] = '4';
                $rel_insert['Event_id'] = $eid;
                $rel_insert['User_id'] = $uid;
                $rel_insert['Organisor_id'] = $org_id;
                $this->db->insert('relation_event_user', $rel_insert);
            }
            else
            {
                $uid = $user_info['Id'];
            }
        }
        elseif ($eid == '1511' || $eid == '1270')
        {
            $this->db->select('u.Id,u.Email');
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            if ($flag) $this->db->where('u.Email', $uid);
            else $this->db->where('u.Id', $uid);
            $this->db->where('reu.Event_id', $eid);
            $user_info = $this->db->get('user u')->row_array();
            if (empty($user_info))
            {
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($eid);
                if ($flag) $insert['Email'] = $uid;
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $insert['is_from_api'] = '1';
                $this->db->insert('user', $insert);
                $uid = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($uid, $eid, $org_id, '4');
                $uid = $insert['Email'];
            }
            else
            {
                $uid = $flag ? $user_info['Email'] : $user_info['Id'];
            }
        }
        if ($flag) $lead = $this->db->where('email', $uid)->where('(exibitor_user_id = ' . $user_id . ' or rep_id = ' . $user_id . ')')->where('event_id', $eid)->get('exibitor_lead')->row_array();
        else $lead = $this->db->where('lead_user_id', $uid)->where('(exibitor_user_id = ' . $user_id . ' or rep_id = ' . $user_id . ')')->where('event_id', $eid)->get('exibitor_lead')->row_array();
        $this->db->select('u.Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END AS Email,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                           CASE WHEN u.Salutation IS NULL THEN "" ELSE u.Salutation END AS salutation,
                           CASE WHEN u.Unique_no IS NULL THEN "" ELSE u.Unique_no END AS badgeNumber,
                           CASE WHEN u.Mobile IS NULL THEN "" ELSE u.Mobile END AS mobile,
                           CASE WHEN c.country_name IS NULL THEN "" ELSE c.country_name END AS country,
                           reu.Role_id,
                           CASE WHEN ea.extra_column IS NULL THEN "" ELSE ea.extra_column END AS extra_column', false)->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id And reu.Event_id=' . $eid);
        $this->db->join('event_attendee ea', 'u.Id=ea.Attendee_id and ea.Event_id=' . $eid, 'left');
        $this->db->join('country c', 'c.id = u.Country', 'left');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        if ($flag) $this->db->where('u.Email', $uid);
        else $this->db->where('u.Id', $uid);
        $this->db->group_by('u.Id');
        $res = $this->db->get()->row_array();
        $res['extra_column'] = !empty($res['extra_column']) ? json_decode($res['extra_column']) : [];
        $i = 0;
        foreach($res['extra_column'] as $key => $value)
        {
            $tmp[$i]['Que'] = $key;
            $tmp[$i]['Ans'] = $value;
            $i++;
        }
        $res['extra_column'] = ($tmp) ? $tmp : [];
        if (empty($res['Id'])) unset($res['extra_column']);
        if (empty($lead))
        {
            return $res;
        }
        elseif (($lead['exibitor_user_id'] == $user_id || $lead['rep_id'] == $user_id))
        {
            $tmp['message'] = "You have already scanned this badge - To edit the lead please visit My Leads and tap on the Lead you would like to edit.";
            return $tmp;
        }
        else
        {
            return $res;
        }
    }
    public function get_custom_column($eid=NULL)

    {
        $this->db->select('*')->from('custom_column');
        $this->db->where('event_id', $eid);
        if ($eid == '634' || $eid == '1012')
        {
            $this->db->where_in('column_id', '(1240,1246,1255)');
        }
        $que = $this->db->get();
        $res = $que->result_array();
        return $res;
    }
    public function get_all_exibitor_user_questions($eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $event = $this->db->select('*')->from('event')->where('Id', $eid)->get()->row_array();
        $user_rep = $this->db->select('*')->from('role')->join('relation_event_user', 'relation_event_user.Role_id = role.Id')->where('User_id', $ex_uid)->where('relation_event_user.Event_id', $eid)->get()->row_array();
        $rep = $this->db->where('event_id', $eid)->where('rep_id', $ex_uid)->get('user_representatives')->row_array();
        $ex_uid = ($rep['user_id']) ? : $ex_uid;
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*,
                           case when eq.commentbox_label_text is null then "" else eq.commentbox_label_text end as commentbox_label_text,
                          (case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic', false)->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id', 'left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id', $eid);
        if ($user_rep['ask_survey'] == '1') $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $event['Organisor_id'] . ')');
        else $this->db->where('eq.exibitor_user_id', $ex_uid);
        $this->db->group_by('eq.q_id');
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id', NULL, FALSE);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $res2 = $this->db->get()->result_array();
        foreach($res2 as $key => $value)
        {
            if ($value['skip_logic'] == '1')
            {
                $this->db->select('`option`,redirectexibitorquestion_id')->from('exibitor_question_skiplogic');
                $this->db->where('exibitor_question_id', $value['q_id']);
                $res = $this->db->get()->result_array();
                foreach($res as $k2 => $v2)
                {
                    $res[$k2]['option'] = str_replace('_', ' ', $v2['option']);
                }
                $option = array_column_1($res, 'option');
                $redirectquestion_id = array_column_1($res, 'redirectexibitorquestion_id');
                $finalarray = array_combine($option, $redirectquestion_id);
                $res2[$key]['redirectids'] = $finalarray;
                $i = 0;
                foreach($finalarray as $key1 => $value1)
                {
                    $tmp[$i]['option'] = $key1;
                    $tmp[$i]['redirect_id'] = $value1;
                    $i++;
                }
                $res2[$key]['a_redirectids'] = $tmp;
                unset($tmp);
            }
            else
            {
                $res2[$key]['redirectids'] = new stdClass();
                $res2[$key]['a_redirectids'] = [];
            }
            $res2[$key]['Option'] = ($value['Option']) ? json_decode($value['Option']) : [];
            $question_array = $this->get_survey_array($eid, $ex_uid, $uid);
            $dependent_ids = $question_array['dependent_ids'];
            $independent_ids = array_values($question_array['independent_ids']);
            if (!in_array($value['q_id'], $independent_ids))
            {
                $redirectkey = $this->get_inarrayval($value['q_id'], $dependent_ids, $independent_ids);
                $rekey = array_search($redirectkey, $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            else
            {
                $rekey = array_search($value['q_id'], $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            $res2[$key]['redirectid'] = ($redirectid) ? $redirectid : "";
            $res2[$key]['survey_que_ans'] = [];
        }
        return $res2;
    }
    public function get_all_exibitor_user_questions_answer($eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*,
                           case when eq.commentbox_label_text is null then "" else eq.commentbox_label_text end as commentbox_label_text,
                          (case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic', false)->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id', 'left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        $this->db->where('eq.event_id', $eid);
        $this->db->group_by('eq.q_id');
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id', NULL, FALSE);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $res2 = $this->db->get()->result_array();
        foreach($res2 as $key => $value)
        {
            if ($value['skip_logic'] == '1')
            {
                $this->db->select('`option`,redirectexibitorquestion_id')->from('exibitor_question_skiplogic');
                $this->db->where('exibitor_question_id', $value['q_id']);
                $res = $this->db->get()->result_array();
                foreach($res as $k2 => $v2)
                {
                    $res[$k2]['option'] = str_replace('_', ' ', $v2['option']);
                }
                $option = array_column_1($res, 'option');
                $redirectquestion_id = array_column_1($res, 'redirectexibitorquestion_id');
                $finalarray = array_combine($option, $redirectquestion_id);
                $res2[$key]['redirectids'] = $finalarray;
                $i = 0;
                foreach($finalarray as $key1 => $value1)
                {
                    $tmp[$i]['option'] = $key1;
                    $tmp[$i]['redirect_id'] = $value1;
                    $i++;
                }
                $res2[$key]['a_redirectids'] = $tmp;
                unset($tmp);
            }
            else
            {
                $res2[$key]['redirectids'] = new stdClass();
                $res2[$key]['a_redirectids'] = [];
            }
            $res2[$key]['Option'] = ($value['Option']) ? json_decode($value['Option']) : [];
            $question_array = $this->get_survey_array($eid, $ex_uid, $uid);
            $dependent_ids = $question_array['dependent_ids'];
            $independent_ids = array_values($question_array['independent_ids']);
            if (!in_array($value['q_id'], $independent_ids))
            {
                $redirectkey = $this->get_inarrayval($value['q_id'], $dependent_ids, $independent_ids);
                $rekey = array_search($redirectkey, $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            else
            {
                $rekey = array_search($value['q_id'], $independent_ids);
                $redirectid = $independent_ids[$rekey + 1];
            }
            $res2[$key]['redirectid'] = ($redirectid) ? $redirectid : "";
            $que_ans = $this->db->select('exibitor_question_answer.Answer')->from('exibitor_question_answer')->join('user', 'user.Id = exibitor_question_answer.user_id')->where('exibitor_question_answer.exibitor_questionid', $value['q_id'])->where('exibitor_question_answer.event_id', $eid)->where('exibitor_question_answer.user_id', $uid)->where('(exibitor_question_answer.exhi_id = ' . $ex_uid . ' OR exibitor_question_answer.rep_id = ' . $ex_uid . ')')->where('exibitor_question_answer.Answer != ""')->get()->row_array();
            if ($value['Question_type'] == '2') $res2[$key]['survey_que_ans'] = ($que_ans['Answer']) ? explode(',', $que_ans['Answer']) : [];
            elseif (($que_ans['Answer']) != '') $res2[$key]['survey_que_ans'][] = $que_ans['Answer'];
            else $res2[$key]['survey_que_ans'] = [];
        }
        return $res2;
    }
    public function get_inarrayval($qid=NULL, $dearr=NULL, $indearr=NULL)

    {
        $qkey = $dearr[$qid];
        if (in_array($qkey, $indearr))
        {
            return $qkey;
        }
        return $this->get_inarrayval($qkey, $dearr, $indearr);
    }
    public function get_survey_array($eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $event = $this->db->select('*')->from('event')->where('Id', $eid)->get()->row_array();
        $user_rep = $this->db->select('*')->from('role')->join('relation_event_user', 'relation_event_user.Role_id = role.Id')->where('User_id', $ex_uid)->where('relation_event_user.Event_id', $eid)->get()->row_array();
        $this->db->protect_identifiers = false;
        $this->db->select('eqs.*')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        $this->db->where('eq.event_id', $eid);
        if ($user_rep['ask_survey'] == '1') $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $event['Organisor_id'] . ')');
        else $this->db->where('eq.exibitor_user_id', $ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id', NULL, FALSE);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $dependentquestion = $this->db->get()->result_array();
        $idsarr = array_unique(array_filter(array_column_1($dependentquestion, "redirectexibitorquestion_id")));
        $masterqarr = array_filter(array_column_1($dependentquestion, "exibitor_question_id"));
        $dependent_ids = array_filter(array_column_1($dependentquestion, "redirectexibitorquestion_id"));
        $dependentquestionarr = array_combine($dependent_ids, $masterqarr);
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*,(case when eqs.logic_id IS NULL Then "0" else "1" End) as skip_logic')->from('exibitor_question eq');
        $this->db->join('exibitor_question_skiplogic eqs', 'eq.q_id=eqs.exibitor_question_id', 'left');
        $this->db->join('exibitor_question_answer eqa', 'eq.q_id = eqa.exibitor_questionid', 'left');
        // $this->db->where('eq.q_id not IN (SELECT eqa.exibitor_questionid  FROM `exibitor_question_answer` as eqa inner join exibitor_question as eq on eq.q_id=eqa.exibitor_questionid WHERE eqa.user_id = '.$uid.' and eqa.event_id = '.$eid.' group by eq.q_id ORDER BY eqa.user_id DESC )', NULL, FALSE);
        if (count($idsarr) > 0)
        {
            $this->db->where_not_in('eq.q_id', $idsarr);
        }
        $this->db->where('eq.event_id', $eid);
        if ($user_rep['ask_survey'] == '1') $this->db->where('(eq.exibitor_user_id = ' . $ex_uid . ' OR eq.exibitor_user_id = ' . $event['Organisor_id'] . ')');
        else $this->db->where('eq.exibitor_user_id', $ex_uid);
        $this->db->order_by('eqs.logic_id IS NULL,eqs.logic_id', NULL, FALSE);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $independentquestion = $this->db->get()->result_array();
        $independent_ids = array_unique(array_filter(array_column_1($independentquestion, "q_id")));
        $res2['dependent_ids'] = $dependentquestionarr;
        $res2['independent_ids'] = $independent_ids;
        foreach($res2 as $key => $value)
        {
            $res2[$key]['redirectids'] = json_decode($value['redirectids'], true);
        }
        return $res2;
    }
    public function save_scan_lead($lead_data=NULL, $eid=NULL, $ex_uid=NULL, $uid=NULL, $created_date = NULL, $test_lead = NULL)

    {
        if ($test_lead)
        {
            $lead_data['exibitor_user_id'] = $ex_uid;
            $lead_data['event_id'] = $eid;
            $lead_data['lead_user_id'] = $uid;
            $lead_data['created_date'] = !empty($created_date) ? $created_date : date('Y-m-d H:i:s');
            $this->db->insert('exibitor_lead_test', $lead_data);
            return true;
        }
        else
        {
            $lead = $this->db->where('lead_user_id', $uid)->where('event_id', $eid)->where('(exibitor_user_id = ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')')->get('exibitor_lead')->row_array();
            $rep = $this->db->select('*')->from('user_representatives')->where('rep_id', $ex_uid)->where('event_id', $eid)->get()->row_array();
            if ($rep)
            {
                $lead_data['rep_id'] = $ex_uid;
                $lead_data['exibitor_user_id'] = $rep['user_id'];
                $ex_uid = $rep['user_id'];
            }
            else
            {
                $lead_data['exibitor_user_id'] = $ex_uid;
            }
            if (($eid == '634' && empty($lead_data['email'])) || ($eid == '1012' && empty($lead_data['email'])) || ($eid == '1378') || ($eid == '1512') || ($eid == '1609') || ($eid == '1580') || ($eid == '1804') || ($eid == '1850'))
            {
                $user_email = $this->db->where('Id', $uid)->get('user')->row_array();
                $lead_data['email'] = $user_email['Email'];
                if ($eid == '1850')
                {
                    $lead_data['email'] = strstr($user_email['Email'], '#', true);
                }
            }
            elseif (empty($lead_data['email']))
            {
                $user_email = $this->db->where('Id', $uid)->get('user')->row_array();
                $lead_data['email'] = $user_email['Email'];
            }
            $org_user = $this->db->where('Id', $uid)->get('user')->row_array();
            $lead_data['firstname'] = !empty($lead_data['firstname']) ? $lead_data['firstname'] : $org_user['Firstname'];
            $lead_data['lastname'] = !empty($lead_data['lastname']) ? $lead_data['lastname'] : $org_user['Lastname'];
            $lead_data['email'] = !empty($lead_data['email']) ? $lead_data['email'] : $org_user['Email'];
            $lead_data['title'] = !empty($lead_data['title']) ? $lead_data['title'] : $org_user['Title'];
            $lead_data['company_name'] = !empty($lead_data['company_name']) ? $lead_data['company_name'] : $org_user['Company_name'];
            $lead_data['salutation'] = !empty($lead_data['salutation']) ? $lead_data['salutation'] : $org_user['Salutation'];
            $tmp_country = $this->db->where('id', $org_user['Country'])->get('country')->row_array();
            $lead_data['country'] = !empty($lead_data['country']) ? $lead_data['country'] : $tmp_country['country_name'];
            $lead_data['mobile'] = !empty($lead_data['mobile']) ? $lead_data['mobile'] : $org_user['Mobile'];
            $lead_data['badgeNumber'] = !empty($lead_data['badgeNumber']) ? $lead_data['badgeNumber'] : $org_user['Unique_no'];
            if ($org_user)
            {
                $update_user['Firstname'] = !empty($org_user['Firstname']) ? $org_user['Firstname'] : $lead_data['firstname'];
                $update_user['Lastname'] = !empty($org_user['Lastname']) ? $org_user['Lastname'] : $lead_data['lastname'];
                $update_user['Email'] = !empty($org_user['Email']) ? $org_user['Email'] : $lead_data['email'];
                $update_user['Title'] = !empty($org_user['Title']) ? $org_user['Title'] : $lead_data['title'];
                $update_user['Company_name'] = !empty($org_user['Company_name']) ? $org_user['Company_name'] : $lead_data['company_name'];
                $update_user['Salutation'] = !empty($org_user['Salutation']) ? $org_user['Salutation'] : $lead_data['salutation'];
                $update_user['Mobile'] = !empty($org_user['Mobile']) ? $org_user['Mobile'] : $lead_data['mobile'];
                $tmp_country = $this->db->where('country_name', $lead_data['country'])->get('country')->row_array();
                $update_user['Country'] = !empty($org_user['Country']) ? $org_user['Country'] : $tmp_country['id'];
                $this->db->where('Id', $org_user['Id']);
                $this->db->update('user', $update_user);
            }
            if (empty($lead))
            {
                $lead_data['event_id'] = $eid;
                $lead_data['lead_user_id'] = $uid;
                $lead_data['created_date'] = !empty($created_date) ? $created_date : date('Y-m-d H:i:s');
                $this->db->insert('exibitor_lead', $lead_data);
                return true;
            }
            else
            {
                $this->db->where('exibitor_lead_id', $lead['exibitor_lead_id']);
                $this->db->update('exibitor_lead', $lead_data);
                return true;
            }
        }
    }
    public function add_question_answer($survey_data=NULL)

    {
        $this->db->insert_batch("exibitor_question_answer", $survey_data);
    }
    public function get_exhibitor_user_by_representative_id($resp_user_id=NULL, $event_id=NULL)

    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('reps_user_id', $resp_user_id);
        $this->db->where('event_id', $event_id);
        $res = $this->db->get()->row_array();
        return $res['exibitor_user_id'];
    }
    public function get_all_my_lead_by_exibitor_user($eid=NULL, $ex_uid=NULL, $where = "", $flag=NULL)

    {
        $this->db->select('u.Id,
                           reu.Role_id,
                           reu.Organisor_id,
                           CASE WHEN el.firstname IS NULL THEN "" ELSE el.firstname END as Firstname,
                           CASE WHEN el.lastname IS NULL THEN "" ELSE el.lastname END as Lastname,
                           CASE WHEN el.email IS NULL THEN "" ELSE el.email END as Email,
                           CASE WHEN el.title IS NULL THEN "" ELSE el.title END as Title,
                           CASE WHEN el.company_name IS NULL THEN "" ELSE el.company_name END as Company_name,
                           el.badgeNumber as badgeNumber
                           ', false)->from('user u');
        $this->db->join('exibitor_lead el', 'el.lead_user_id=u.Id');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
        $this->db->where('el.event_id', $eid);
        if ($flag != 1) $this->db->where('el.exibitor_user_id', $ex_uid);
        else $this->db->where('el.rep_id', $ex_uid);
        if (!empty($where)) $this->db->where($where);
        $this->db->order_by('el.firstname', NULL, FALSE);
        $res = $this->db->get()->result_array();
        // lq();
        return $res;
    }
    public function get_exhibitor_user_by_representative_id_with_que_ans($exhibitor_user_id=NULL, $event_id=NULL)

    {
        $this->db->select('u.Id,reu.Role_id,reu.Organisor_id,el.firstname as Firstname,el.lastname as Lastname,el.email as Email,el.title as Title,el.company_name as Company_name')->from('user u');
        $this->db->join('exibitor_lead el', 'el.lead_user_id=u.Id');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
        $this->db->where('el.event_id', $eid);
        $this->db->where('el.exibitor_user_id', $ex_uid);
        if (!empty($where)) $this->db->where($where);
        $this->db->order_by('el.firstname', NULL, FALSE);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_all_my_representative_by_exibitor_user($eid=NULL, $ex_uid=NULL, $where=NULL)

    {
        $this->db->select('u.Id,
                          CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END AS logo,
                          CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                          CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                          CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                          CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                          reu.Role_id,
                          reu.Organisor_id', false)->from('user u');
        $this->db->join('exhibitor_representatives er', 'er.reps_user_id=u.Id');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id and reu.Event_id=' . $eid);
        $this->db->where('er.event_id', $eid);
        $this->db->where('er.exibitor_user_id', $ex_uid);
        if (!empty($where)) $this->db->where($where);
        $this->db->order_by('u.Firstname', NULL, FALSE);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_event_all_attendee_from_assing_resp($eid=NULL, $where=NULL)

    {
        $this->db->select('u.Id,
                          CASE WHEN u.logo IS NULL THEN "" ELSE u.logo END AS logo,
                          CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                          CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                          CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                          CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name', false)->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id=u.Id');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        if (!empty($where)) $this->db->where($where);
        $this->db->where('u.Id NOT IN (select reps_user_id from exhibitor_representatives where event_id=' . $eid . ')');
        $this->db->where('u.Firstname != ""');
        $this->db->order_by('u.Firstname', NULL, FALSE);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function add_new_representative($eid=NULL, $ex_uid=NULL, $resp_uid=NULL)

    {
        $res = $this->get_exibitor_by_representative_id_in_event($eid, $resp_uid);
        if (empty($res))
        {
            $resp_data['event_id'] = $eid;
            $resp_data['exibitor_user_id'] = $ex_uid;
            $resp_data['reps_user_id'] = $resp_uid;
            $resp_data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_representatives', $resp_data);
        }
        return true;
    }
    public function get_total_representative_by_exibitor_user($eid=NULL, $ex_uid=NULL)

    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id', $eid);
        $this->db->where('exibitor_user_id', $ex_uid);
        $res = $this->db->get()->result_array();
        return count($res);
    }
    public function get_exibitor_user_premission($eid=NULL, $ex_uid=NULL)

    {
        $this->db->select('CASE WHEN maximum_Reps IS NULL THEN "" ELSE maximum_Reps end as maximum_Reps', false)->from('exibitor_premission');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $ex_uid);
        $res = $this->db->get()->row_array();
        return (!empty($res['maximum_Reps'])) ? $res['maximum_Reps'] : '';
    }
    public function remove_my_representative($eid=NULL, $resp_uid=NULL)

    {
        $this->db->where('event_id', $eid);
        $this->db->where('reps_user_id', $resp_uid);
        $this->db->delete('exhibitor_representatives');
    }
    public function get_all_exibitor_user_questions_for_export($eid=NULL, $ex_uid=NULL)

    {
        $this->db->protect_identifiers = false;
        $this->db->select('eq.*')->from('exibitor_question eq');
        $this->db->where('eq.event_id', $eid);
        $this->db->where('eq.exibitor_user_id', $ex_uid);
        $this->db->order_by('eq.sort_order=0,eq.sort_order', NULL, FALSE);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_user_question_answer($eid=NULL, $uid=NULL, $qid=NULL, $exhi_id=NULL)

    {
        $this->db->select('*')->from('exibitor_question_answer');
        $this->db->where('event_id', $eid);
        $this->db->where('user_id', $uid);
        $this->db->where('(exhi_id = ' . $exhi_id . ' OR rep_id = ' . $exhi_id . ')');
        $this->db->where('exibitor_questionid', $qid);
        $res = $this->db->get()->result_array();
        return $res;
    }
    public function get_exibitor_by_representative_id_in_event($eid=NULL, $resp_uid=NULL)

    {
        $this->db->select('*')->from('exhibitor_representatives');
        $this->db->where('event_id', $eid);
        $this->db->where('reps_user_id', $resp_uid);
        $res = $this->db->get()->row_array();
        return $res;
    }
    public function get_user_by_email($event_id=NULL, $email=NULL)

    {
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id And reu.Event_id=' . $event_id);
        $this->db->where('u.Email', $email);
        $res = $this->db->get()->row_array();
        return $res['Id'];
    }
    public function get_event_name($event_id=NULL)

    {
        $this->db->select('Event_name');
        $this->db->where('Id', $event_id);
        $res = $this->db->get('event')->row_array();
        return ucfirst($res['Event_name']);
    }
    public function get_offline_scan_data($eid=NULL)

    {
        $lead_users = $this->db->select()->where('event_id', $eid)->get('exibitor_lead')->result_array();
        $lead_users = array_column_1($lead_users, 'lead_user_id');
        $this->db->select('u.Id,
                           CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END AS Firstname,
                           CASE WHEN u.Lastname IS NULL THEN "" ELSE u.Lastname END AS Lastname,
                           CASE WHEN u.Email IS NULL THEN "" ELSE u.Email END AS Email,
                           CASE WHEN u.Company_name IS NULL THEN "" ELSE u.Company_name END AS Company_name,
                           CASE WHEN u.Title IS NULL THEN "" ELSE u.Title END AS Title,
                           reu.Role_id,
                           CASE WHEN ea.extra_column IS NULL THEN "" ELSE ea.extra_column END AS extra_column', false)->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id And reu.Event_id=' . $eid);
        $this->db->join('event_attendee ea', 'u.Id=ea.Attendee_id and ea.Event_id=' . $eid, 'left');
        $this->db->where('reu.Event_id', $eid);
        $this->db->where('reu.Role_id', '4');
        $this->db->where_not_in('u.Id', $lead_users);
        $this->db->group_by('u.Id');
        $res = $this->db->get()->result_array();
        foreach($res as $key => $value)
        {
            $res[$key]['extra_column'] = !empty($value['extra_column']) ? json_decode($value['extra_column']) : [];
            $i = 0;
            foreach($res[$key]['extra_column'] as $k1 => $v1)
            {
                $tmp[$i]['Que'] = $k1;
                $tmp[$i]['Ans'] = $v1;
                $i++;
            }
            $res[$key]['extra_column'] = ($tmp) ? $tmp : [];
            unset($tmp);
            if (empty($res[$key]['Id'])) unset($res[$key]['extra_column']);
        }
        return $res;
    }
    public function get_user_by_id($event_id=NULL, $where=NULL, $lead_data=NULL)

    {
        if ($event_id == '634' || $event_id == '1012')
        {
            $uid = $this->db->where('u1.Unique_no', $where)->get('user u1')->row_array();
            if (empty($uid))
            {
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                $insert['Unique_no'] = $where;
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $insert['Firstname'] = $lead_data['firstname'];
                $insert['Lastname'] = $lead_data['lastname'];
                $insert['Email'] = $lead_data['email'];
                $insert['Company_name'] = $lead_data['company_name'];
                $insert['Title'] = $lead_data['title'];
                $insert['Salutation'] = $lead_data['salutation'];
                $country_id = $this->db->where('country_name', $lead_data['country'])->get('country')->row_array() ['id'];
                $insert['Country'] = ($country_id) ? : $lead_data['country'];
                $insert['Mobile'] = $lead_data['mobile'];
                $this->db->insert('user', $insert);
                $uid1 = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($uid1, $event_id, $org_id, '4');
                $uid = $this->db->where('Id', $uid1)->get('user u1')->row_array();
                $tmp[1] = $uid['Id'];
            }
            else
            {
                $tmp[1] = $uid['Id'];
            }
        }
        if ($event_id == '1378' || $event_id == '1512' || $event_id == '1609' || $event_id == '1580' || $event_id == '1804' || $event_id == '1850')
        {
            $this->db->select('u.Id');
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            $this->db->where('u.Unique_no', $where);
            $this->db->where('reu.Event_id', $event_id);
            $user_info = $this->db->get('user u')->row_array();
            $tmp[1] = $user_info['Id'];
            if (empty($user_info))
            {
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                $insert['Unique_no'] = $where;
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $this->db->insert('user', $insert);
                $tmp[1] = $this->db->insert_id();
                $rel_insert['Role_id'] = '4';
                $rel_insert['Event_id'] = $event_id;
                $rel_insert['User_id'] = $tmp[1];
                $rel_insert['Organisor_id'] = $org_id;
                $this->db->insert('relation_event_user', $rel_insert);
            }
            else
            {
                $tmp[1] = $user_info['Id'];
            }
        }
        elseif ($event_id == '1511' || $event_id == '1270')
        {
            $this->db->select('u.Id,u.Email');
            $this->db->join('relation_event_user reu', 'reu.User_id = u.Id');
            if (strpos($where, '@') !== false)
            {
                $this->db->where('u.Email', $where);
            }
            else
            {
                $where1 = explode('-', $where);
                $this->db->where('u.Id', $where1[1]);
            }
            $this->db->where('reu.Event_id', $event_id);
            $user_info = $this->db->get('user u')->row_array();
            if (empty($user_info))
            {
                $this->load->model('native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                if (strpos($where, '@') !== false)
                {
                    $insert['Email'] = $where;
                }
                $insert['Created_date'] = date('Y-m-d H:i:s');
                $insert['Active'] = '1';
                $insert['Organisor_id'] = $org_id;
                $this->db->insert('user', $insert);
                $tmp[1] = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($tmp[1], $event_id, $org_id, '4');
            }
        }
        $this->db->protect_identifiers = false;
        $this->db->select('u.Id');
        $this->db->from('user u');
        $this->db->join('relation_event_user reu', 'reu.User_id = u.Id And reu.Event_id=' . $event_id);
        if (strpos($where, '@') !== false)
        {
            $this->db->where('u.Email', $where);
        }
        else
        {
            $is_unique_no = array(
                '634',
                '1012',
                '1378',
                '1512',
                '1609',
                '1580',
                '1804',
                '1850'
            );
            if (!in_array($event_id, $is_unique_no))
            {
                $tmp = explode('-', $where);
            }
            $this->db->where('u.Id', $tmp[1]);
        }
        $res = $this->db->get()->row_array();
        return $res['Id'];
    }
    public function getLeadDetails($event_id=NULL, $user_id=NULL, $ex_uid=NULL)

    {
        $res = $this->db->select('el.*,
            CASE WHEN el.firstname IS NULL THEN "" ELSE el.firstname END as firstname,
            CASE WHEN el.lastname IS NULL THEN "" ELSE el.lastname END as lastname,
            CASE WHEN el.email IS NULL THEN "" ELSE el.email END as email,
            CASE WHEN el.title IS NULL THEN "" ELSE el.title END as title,
            CASE WHEN el.company_name IS NULL THEN "" ELSE el.company_name END as company_name,
            CASE WHEN el.salutation IS NULL THEN "" ELSE el.salutation END as salutation,
            CASE WHEN el.mobile IS NULL THEN "" ELSE el.mobile END as mobile,
            CASE WHEN el.country IS NULL THEN "" ELSE el.country END as country,
            CASE WHEN u.Unique_no IS NULL THEN "" ELSE u.Unique_no END AS Unique_no', false)->where('(el.exibitor_user_id = ' . $ex_uid . ' or el.rep_id = ' . $ex_uid . ')')->where('el.event_id', $event_id)->where('el.lead_user_id', $user_id)->join('user u', 'el.lead_user_id = u.Id')->get('exibitor_lead el')->row_array();
        return $res;
    }
    public function getUserDetailsId($user_id=NULL, $event_id=NULL)

    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->where('u.Id', $user_id);
        $this->db->join('relation_event_user as ru', 'u.Id=ru.User_id');
        $this->db->join('role', 'role.Id = ru.Role_id');
        if ($event_id != "") $this->db->where('ru.Event_id', $event_id);
        $qu = $this->db->get();
        $res = $qu->result_array();
        $res = $res[0];
        array_walk($res, function (&$item)
        {
            $item = strval($item);
        });
        return $res;
    }
    public function update_scan_lead($lead_data=NULL, $eid=NULL, $ex_uid=NULL, $uid=NULL)

    {
        $lead = $this->db->where('lead_user_id', $uid)->where('(exibitor_user_id = ' . $ex_uid . ' or rep_id = ' . $ex_uid . ')')->where('event_id', $eid)->get('exibitor_lead')->row_array();
        $rep = $this->db->select('*')->from('user_representatives')->where('rep_id', $ex_uid)->where('event_id', $eid)->get()->row_array();
        if ($rep)
        {
            $lead_data['rep_id'] = $ex_uid;
            $lead_data['exibitor_user_id'] = $rep['user_id'];
            $ex_uid = $rep['user_id'];
        }
        if ($lead)
        {
            $this->db->where('event_id', $eid);
            $this->db->where('exibitor_user_id', $ex_uid);
            $this->db->where('lead_user_id', $uid);
            $this->db->update('exibitor_lead', $lead_data);
            return true;
        }
        else
        {
            return false;
        }
    }
    public function add_update_question_answer($survey_data=NULL, $where=NULL)

    {
        $count = $this->db->select()->from('exibitor_question_answer')->where($where)->get()->num_rows();
        if ($count)
        {
            $this->db->where($where);
            $this->db->update('exibitor_question_answer', $survey_data);
        }
        else $this->db->insert('exibitor_question_answer', $survey_data);
    }
    public function reset_questions_answer($where=NULL)

    {
        $this->db->where($where);
        $this->db->delete('exibitor_question_answer');
    }
    public function get_lead_rep($event_id=NULL, $user_id=NULL)

    {
        $rep = $this->db->where('event_id', $event_id)->where('rep_id', $user_id)->get('user_representatives')->row_array();
        if ($rep)
        {
            $data['rep_id'] = $user_id;
            $data['exibitor_user_id'] = $rep['user_id'];
        }
        else
        {
            $data['exibitor_user_id'] = $user_id;
        }
        return $data;
    }
}