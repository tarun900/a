<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Native_single_fcm_model extends CI_Model

{
	public $variable;

	public function __construct()

	{
		parent::__construct();
	}
	public function getGlidewellDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 795);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getNHSDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 578);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getSIALMEDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 539);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getCABSATDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1020);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getONTDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 803);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getWPBDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1078);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function ApexSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 14241);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function ComicconSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 32372);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function getGamesforumEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.Status', '1');
		$this->db->where('e.id', 806);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function KnectSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 17181);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			/*$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)*/
			$return[] = $value;
		}
		return $return;
	}
	public function getArabHelthDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 634);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getMedlabDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1012);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getFHADefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 585);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getBondsAndLoansMasterEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		/*$this->db->where('e.Event_type','3');
		$this->db->where('e.Status','1');*/
		$this->db->where('e.id', 1007);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getBondsAndLoansDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 881);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getConcreteExpoDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1070);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getConnect2bDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1095);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getMjBakerDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1017);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getAvangridDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 723);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function CoburnsSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 1582);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email', $value['Email'])->get()->row_array();
			if ($org['subscriptiontype'] != 23) $return[] = $value;
		}
		return $return;
	}
	public function getGreyDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1064);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getLTLondonDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1029);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getLTFranceDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1069);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getDerivConeDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1137);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getFixIncome2018DefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1136);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function IMEASearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 33148);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			/*$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)*/
			$return[] = $value;
		}
		return $return;
	}
	public function UCASSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 49069);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			/*$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)*/
			$return[] = $value;
		}
		return $return;
	}
	public function AICDSearchEvent($event_name=NULL)

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,u.Email,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->join("user u", "u.Id=e.Organisor_id", "left");
		$this->db->like('e.Event_name', $event_name);
		$this->db->where('e.Organisor_id', 53794);
		$this->db->where('(e.secure_key IS NULL OR e.Event_type = 3)');
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$return = [];
		foreach($res as $key => $value)
		{
			$this->load->model('App_login_model');
			$value['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
			$value['lang_list'] = $this->App_login_model->get_event_lang_list($value['event_id']);
			/*$org = $this->db1->select('subscriptiontype')->from('organizer_user')->where('Email',$value['Email'])->get()->row_array();
			if($org['subscriptiontype'] != 23)*/
			$return[] = $value;
		}
		return $return;
	}
	public function getAICDMemberDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1148);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getAICDShow2018DefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1058);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getErsteTalentDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1019);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getEuromedicomAMWCDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 799);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$res[0]['show_activity_feed_on_home'] = '1';
		return $res;
	}
	public function getEuromedicomFACEDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1044);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$res[0]['show_activity_feed_on_home'] = '1';
		return $res;
	}
	public function getEuromedicomAMECLiveVISAGEDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1045);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		$res[0]['show_activity_feed_on_home'] = '1';
		return $res;
	}
	public function getArlarDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1141);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getIPExpoDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 992);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getUCExpoDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1107);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getCityScapeAbuDhabiDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1112);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getAEI2018DefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 750);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getBBGADefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1032);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getBathAndWestDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 978);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getTSAMLondonDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1254);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getTSAMTorontoDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1298);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getTSAMNewYorkDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1299);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getTSAMHongKongDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1300);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getTSAMBostonDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1301);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getAhicDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 993);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
	public function getAviationMediaDefaultEvent()

	{
		$this->db->select('e.event_token,e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled,e.show_login_screen', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		$this->db->where('e.id', 1239);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
}