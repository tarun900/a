<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dims_model extends CI_Model

{
	public $variable;

	public function __construct()

	{
		parent::__construct();
	}
	public function getDimsEvent()

	{
		$this->db->select('e.id as event_id,e.Event_type,e.Subdomain,e.Event_name,e.Logo_images,CASE WHEN fs.facebook_login IS NULL THEN "0" ELSE fs.facebook_login END as facebook_login,CASE WHEN fs.fundraising_enbled IS NULL THEN "0" ELSE fs.fundraising_enbled END as fundraising_enbled,CASE WHEN fs.linkedin_login_enabled IS NULL THEN "0" ELSE fs.linkedin_login_enabled END as linkedin_login_enabled', FALSE);
		$this->db->from('event e');
		$this->db->join("fundraising_setting fs", "fs.Event_id=e.Id", "left");
		/*$this->db->where('e.Event_type','3');
		$this->db->where('e.Status','1');*/
		// $this->db->where('e.id',384);
		$this->db->where('e.Organisor_id', 28281);
		$this->db->order_by("e.Start_date", "desc");
		$oqu = $this->db->get();
		$res = $oqu->result_array();
		return $res;
	}
}
/* End of file Dims_model.php */
/* Location: ./application/models/Dims_model.php */