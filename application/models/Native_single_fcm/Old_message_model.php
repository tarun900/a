<?php
class Message_model extends CI_Model{
    function __construct()
    {
            parent::__construct();

    }
    public function view_private_msg_coversation($user_id,$att_id,$Event_id,$page_no,$limit)
    {
        $page_no = (!empty($page_no))?$page_no:1;
        $start=($page_no-1)*$limit;
        $this->db->select('CASE WHEN sm.id IS NULL THEN "" ELSE sm.id END as message_id,CASE WHEN sm.image IS NULL THEN "" ELSE sm.image END as image,CASE WHEN sm.message IS NULL THEN "" ELSE sm.message END as message,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u.Firstname IS NULL THEN "" ELSE u.Firstname END as Sendername,CASE WHEN u1.Firstname IS NULL THEN "" ELSE u1.Firstname END as Recivername,CASE WHEN u.Logo IS NULL THEN "" ELSE u.Logo END as Senderlogo,CASE WHEN u1.Logo IS NULL THEN "" ELSE u1.Logo END as Reciverlogo,CASE WHEN sm.Time IS NULL THEN "" ELSE sm.Time END as Time',FALSE);
        $this->db->from('speaker_msg sm');
        $this->db->join('user u','u.Id=sm.Sender_id');
        $this->db->join('user u1','u1.Id=sm.Receiver_id');
        $where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Sender_id = '".$att_id."')  
                    AND (sm.Receiver_id = '".$user_id."' OR  sm.Receiver_id = '".$att_id."')  
                    AND (u1.Id = '".$user_id."' OR  u1.Id = '".$att_id."') 
                    AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
       
        $this->db->where($where);
        $this->db->group_by('sm.id');
        $this->db->limit($limit,$start); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array();
        foreach ($res1 as $key=>$value)
        {
            
            $res1[$key]['time_stamp']=$this->time_cal($value['Time']);
            if(!empty($value['image']))
            {
                $res1[$key]['image']=json_decode($value['image']);
            }
            else
            {
                $res1[$key]['image']="";
            }
           
            $this->db->select('CONCAT(u.Firstname," ",u.Lastname) as user_name,sc.comment,u.Logo,sc.image,sc.Time',FALSE);
            $this->db->from('speaker_comment sc');
            $this->db->join('user u','u.Id=sc.user_id');
            $this->db->where('sc.msg_id',$value['message_id']);
            $query_comment = $this->db->get();
            $res_comment = $query_comment->result_array();
            if(!empty($res_comment))
            {
                foreach ($res_comment as $key1 => $value1) 
                {
                    $res_comment[$key1]['time_stamp']=$this->time_cal($value1['Time']);
                    if(!empty($value1['image']))
                    {
                        $res_comment[$key1]['image']=json_decode($value1['image']);
                    }
                    else
                    {
                        $res_comment[$key1]['image']="";
                    }
                }
                $res1[$key]['comment']=$res_comment;
            }
            $res1[$key]['comment']=$res_comment;
        }
        return $res1;
    }
    public function saveMessage($data)
    {
        $this->db->insert("speaker_msg",$data);
        $message_id=$this->db->insert_id();
        if($message_id!='')
        {
            $data1 = array(
                  'msg_id' => $message_id,
                  'resiver_id' => $data['Receiver_id'],
                  'sender_id' => $data['Sender_id'],
                  'event_id' => $data['Event_id'],
                  'isread' => '1',
                  'type' => '0',
            );
            $this->db->insert('msg_notifiy', $data1);
            $msg_notifiy_id=$this->db->insert_id();
            if($msg_notifiy_id!='')
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }

    }
    public function add_msg_hit($user_id,$current_date,$receiver_id)
    {

         $this->db->select('*');
         $this->db->from('users_leader_board');
         $this->db->where("user_id",$user_id);
         $this->db->where("date",$current_date);
         $this->db->where("received_id",$receiver_id);
         $query=$this->db->get();
         $res=$query->result_array();
         $msg_hit=$res[0]['msg_hit'];
        
         if($msg_hit=='')
         {
            $data['msg_hit']=1;
            $data['user_id']=$user_id;
            $data['date']=$current_date;
            $data['received_id']=$receiver_id;
            $this->db->insert("users_leader_board",$data);
         }
         else
         {
            $data['msg_hit']=$msg_hit+1;
            $this->db->where("user_id",$user_id);
            $this->db->where("date",$current_date);
            $this->db->where("received_id",$receiver_id);
            $this->db->update("users_leader_board",$data);
         }
    }
    public function add_comment_hit($user_id,$current_date,$commenter_id)
    {

         $this->db->select('*');
         $this->db->from('users_leader_board');
         $this->db->where("user_id",$user_id);
         $this->db->where("date",$current_date);
         $this->db->where("comment_id",$commenter_id);
         $query=$this->db->get();
         $res=$query->result_array();
         $comment_hit=$res[0]['comment_hit'];
        
         if($comment_hit=='')
         {
            $data['comment_hit']=1;
            $data['user_id']=$user_id;
            $data['date']=$current_date;
            $data['comment_id']=$commenter_id;
            $this->db->insert("users_leader_board",$data);
         }
         else
         {
            $data['comment_hit']=$comment_hit+1;
            $this->db->where("user_id",$user_id);
            $this->db->where("date",$current_date);
            $this->db->where("comment_id",$commenter_id);
            $this->db->update("users_leader_board",$data);
         }
    }
    public function total_pages($user_id,$att_id,$Event_id,$limit)
    {
        $this->db->select('sm.id');
        $this->db->from('speaker_msg sm');
        $where = "sm.Event_id = '".$Event_id."' AND (sm.Sender_id = '".$user_id."' OR  sm.Sender_id = '".$att_id."')  
                    AND (sm.Receiver_id = '".$user_id."' OR  sm.Receiver_id = '".$att_id."')  
                    AND sm.ispublic = '0' AND  sm.Parent = '0' " ;
       
        $this->db->where($where);
        $this->db->group_by('sm.id');
        $rows = $this->db->get()->num_rows();
        $total=ceil($rows/$limit);
        return $total;
    }
    public function time_cal($ago_time)
    {
            $cur_time = time();
            $time_ago=strtotime($ago_time);
            $time_elapsed = $cur_time - $time_ago;
            $seconds = $time_elapsed;
            $minutes = round($time_elapsed / 60);
            $hours = round($time_elapsed / 3600);
            $days = round($time_elapsed / 86400);
            $weeks = round($time_elapsed / 604800);
            $months = round($time_elapsed / 2600640);
            $years = round($time_elapsed / 31207680);
            $time_status;
            if ($seconds <= 60)
            {
                $time_status=$seconds." seconds ago";
            }
             //Minutes
            else if ($minutes <= 60)
            {
                  if ($minutes == 1)
                  {
                        $time_status="one minute ago";
                  }
                  else
                  {
                        $time_status=$minutes." minutes ago";
                  }
             }
             //Hours
             else if ($hours <= 24)
             {
                  if ($hours == 1)
                  {
                        $time_status="an hour ago";
                  }
                  else
                  {
                        $time_status=$hours." hours ago";
                  }
             }
             //Days
             else if ($days <= 7)
             {
                  if ($days == 1)
                  {
                        $time_status="yesterday";
                  }
                  else
                  {
                        $time_status=$days." days ago";
                  }
             }
             //Weeks
             else if ($weeks <= 4.3)
             {
                  if ($weeks == 1)
                  {
                        $time_status="a week ago";
                  }
                  else
                  {
                        $time_status=$weeks." weeks ago";
                  }
             }
             //Months
             else if ($months <= 12)
             {
                  if ($months == 1)
                  {
                        $time_status= "a month ago";
                  }
                  else
                  {
                        $time_status= $months." months ago";
                  }
             }
             //Years
             else
             {
                  if ($years == 1)
                  {
                        $time_status= "one year ago";
                  }
                  else
                  {
                        $time_status=$years." years ago";
                  }
            }
            return $time_status;
    }
    
}        
?>