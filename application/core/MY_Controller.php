<?php class MY_Controller extends CI_Controller 
{ 

    // public function MY_Controller()
    function __construct()
    {
    	parent::__construct();       
		$this->data = array();
    } 
    public function __destruct() {
	    $this->db->close();
	}
} 

class FrontendController extends CI_Controller 
{
	function __construct($data) 
	{
		parent::__construct();
		$this->no_cache();
		$this->load->model('Event_model');
		$this->data = array();
        
		$user = $this->session->userdata('current_user');  
		$acc_name=$this->session->userdata('acc_name');
		$check_user = $this->db->where('Id',$user[0]->Id)->where('Email',$user[0]->Email)->get('user')->row_array();
		if (!$user || !$check_user) 
   		{	
   			$this->session->sess_destroy();
			redirect('Login');
		}
		else 
		{
			$this->data['user'] = $user[0];				
		}   
    
		$this->data['url'] = $this->router->class.'/'.$this->router->method;
        $my_current_role = $user[0]->Role_name;
        $my_current_role_a = $this->config->item('Role');
        $my_current_role=$my_current_role_a[$my_current_role];

        $logged_in_user_id = $user[0]->Organisor_id;
        //$session_time = $this->profile_model->get_session_time($logged_in_user_id);
        //$this->data['session_time'] = $session_time;

		$this->data['pagetitle'] = $data['pagetitle'];
        $this->data['page_edit_title'] = $data['page_edit_title'];
		$this->data['smalltitle'] = $data['smalltitle'];
		$this->data['breadcrumb'] = $data['breadcrumb'];
		$scret_key = $this->Event_model->get_screct_key();
        $this->data['scret_key'] = $scret_key;
        $this->data['iseventfreetrial']=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
         ///////////////Site Setting///////////////////////
        $site_setting = $this->Login_model->get_site_setting();
        $site_setting = $site_setting[0];
        $this->data['site_title'] = $site_setting['Site_title'];
        $this->data['site_logo'] = $site_setting['Site_logo'];
        $this->data['reserved_right'] = $site_setting['Reserved_right'];
                
        $news_event = $this->Event_model->getNewsMasterEvent();
        $this->data['news_event'] = $news_event[0];
        if($news_event[0]['Id'] == $this->uri->segment(3))
		{	
			if($this->uri->segment(1) == "Attendee_admin")
			{
			   $this->data['pagetitle'] = 'Your Users';
			   $this->data['smalltitle'] = '';
			   $this->data['page_edit_title'] = 'edit';
			   $this->data['breadcrumb'] = 'Your Users';
			}
			   $this->data['is_news_event'] = '1';
		}

		$this->template->set_template('home');
		$this->template->write('pagetitle', $data['pagetitle']);
	    $this->template->write('page_edit_title', $data['page_edit_title']);    
		$this->template->write_view('header', 'common/header', $this->data , true);
		$this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
		$this->template->write_view('right_sidebar', 'common/right_sidebar', $this->data , true);
		$this->template->write_view('inner_toolbar', 'common/inner_toolbar', $this->data , true);
		$this->template->write_view('breadcrumb', 'common/breadcrumb', $this->data , true);
		$this->template->write_view('footer', 'common/footer', $this->data , true);		
    }

     private function no_cache()
     {
       
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        
    }

}
