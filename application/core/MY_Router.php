<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Router.php";

class MY_Router extends MX_Router {
	function __contruct() {
        parent::MX_Router();
    }

    function _validate_request($segments) {
        $tmp  = explode('/',$_SERVER['REQUEST_URI']);
		if($segments[0] == 'native_single_fcm' || $segments[0] == 'native_single_fcm_v2' || $segments[0] == 'native')
		{
			$segments['1'] = ucfirst($segments['1']);
		}
		else if($segments[0] != 'assets')
		{	
			$segments['0'] = ucfirst($segments['0']);
		}
		return parent::_validate_request($segments);
    }

}