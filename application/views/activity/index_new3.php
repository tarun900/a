<?php //echo urlencode($facebook_feeds['paging']['next']);  exit;?>
<?php
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
if (empty($user))
{
    $url = base_url() . 'activity/' . $acc_name . '/' . $Subdomain;
}
function get_timeago($ptime)
{
    $estimate_time = time() - $ptime;
    if ($estimate_time < 1)
    {
        return '1 second ago';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    foreach($condition as $secs => $str)
    {
        $d = $estimate_time / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
        }
    }
} ?>

    <!-- Material Design fonts -->

<link rel="stylesheet" href="<?php
echo base_url(); ?>assets/css/bootstrap-social.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<!-- Bootstrap Material Design -->
<link rel="stylesheet" href="<?php
echo base_url(); ?>assets/css/cards.css">
<link rel="stylesheet" href="<?php
echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style type="text/css" media="screen">

.message_container{

    border:none !important;
    box-shadow: none !important;
}

.msg_message p{
    word-wrap:break-word;
    font-size:24px;

}

.message_container .msg_date{
    padding-left: 70px !important;
   font-size:24px;

}

.message_container .msg_fromname{
    margin-left: 10px;
}

.message_container .message_img img{
    max-width:132%;
    border-radius:59%;
    height:70px;
    width:70px;
}


#messages_all, .message_container .msg_fromname a{ 
   font-size:24px;

}

.topbar{
    display: none;
}
.card-columns {


@include media-breakpoint-only(lg) {
    column-count: 4;
}
@include media-breakpoint-only(xl) {
    column-count: 5;
}
/*padding-left: 18px;
padding-right: 18px;*/
margin:18px;
}
.card-img-top {
width: 100%;
}
.img-circle {
border-radius: 50%;
margin: 5px;
float: left;
}
.fa {
    float:right;
}
.fa-facebook-square {
        color:#3b5998;
}
.fa-twitter-square {
    color:#00aced;
}
.fa-linkedin-square {
        color:#005983;
}
.icon-gplus{
    color:#dd4b39;
}
#DIV_1 {
height: 68px;
position: relative;
border-width: 0px 0px 1px;
border-color: rgb(51, 51, 51) rgb(51, 51, 51) rgb(223, 224, 229);
border-style: none none solid;
padding: 15px 35px 15px 65px;
}/*#DIV_1*/

#IMG_2 {
    left: 15px;
    position: absolute;
    width: 40px;
    border-width: 1px;
    border-color: rgb(223, 224, 229);
    border-style: solid;
}/*#IMG_2*/

#SPAN_4 {
    color: rgb(65, 67, 83);
    display: block;
}/*#SPAN_4*/

#SPAN_5 {
    color: rgb(175, 180, 189);
    font: normal normal normal normal 12.6px / 18.9px seravek-web, sans-serif;
}
.likes-comments{
    float: right;
    letter-spacing: 5px;
    color: rgb(175, 180, 189);
    font: normal normal normal normal 14.6px / 18.9px seravek-web, sans-serif;
    margin: 2px;
}
.flag {
    position: relative;
    top: -22px;
    right: -19px;
}
.btn {
    margin-right: auto;
}
</style>

<div class="container">
<!-- HEADING -->
<div class="row" style="margin: 10px">
    <div class="col-md-12">
        <center>
            <!-- <h3>Social Media Wall</h3> -->
            <?php
if (count($socialwall_heading) > 0)
{
    echo $socialwall_heading[0]['socialwall_heading'];
}
?>
        </center>

        <span class="glyphicon glyphicon-move" onclick="toggleFullScreen(document.body)" class="pull-right" style="font-size:30px;margin-top:-16px;margin-left:98%;"></span>
         
    </div>
</div>

<div class="row">
    <div class="col-md-12" id="test">
        <div class="card-columns">

           <!-- Dynamic loop here write -->

           <div class="tab-pane active fade in" id="all">
                <div id="messages_all"> 


                    <?php
                    foreach($activity_data as $key => $value)
                    {
                        $alldivid = uniqid() . md5(rand()); ?>

                         <div class="card" data-sort="<?php echo strtotime($value['Time']); ?>">

                          <div class="message_container internal_message" data-sort="<?php
                        echo strtotime($value['Time']); ?>">
                                                 <?php
                        if (count(json_decode($value['image'], TRUE)) > 0)
                        {
                            $img = json_decode($value['image'], TRUE);
                            $img1 = 1;
                            foreach($img as $ikey => $ivalue)
                            {?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $alldivid; ?>" href="<?php echo base_url() . 'assets/user_files/' . $ivalue; ?>">
                                        <?php
                                        if ($img1 == '1')
                                        {?>
                                            <img src="<?php echo base_url() . 'assets/user_files/' . $ivalue; ?>">
                                            <?php
                                            $img1++;
                                        }?>
                                    </a>
                                </div>     
                            <?php
                            }
                        }?>
                        <div class="msg_message">
                            <?php
                            if (!empty($value['agenda_id']))
                            {
                                $agenda_url = base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/View_agenda/' . $value['agenda_id'];?>
                                <div class="msg_date"><?php echo $value['agenda_time'] ?></div>
                            <?php
                            }
                            else
                            {
                                if (!empty($value['Message']))
                                {?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['Message'], 0, 300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['Message']; ?></p>
                                    </div>
                                <?php
                                 }
                            }?>
                        </div>
                        <div class="msg_main_body">
                            <div class="message_img">
                                <?php if (!empty($value['Logo']) && file_exists('./assets/user_files/' . $value['Logo'])) { ?>
                                        <img src="<?php echo base_url() . 'assets/user_files/' . $value['Logo']; ?>">
                                <?php } else { ?>
                                        <img src="<?php echo base_url() . '/assets/images/anonymous.jpg'; ?>">
                                <?php } ?>
                            </div>
                            <div class="msg_type_images pull-right">
                                <img width="40" height="40" src="<?php echo base_url() . 'assets/images/activity_internal_img.png'; ?>">
                            </div>
                            <div class="msg_fromname">
                                <?php
                                    if ($value['Role_id'] == '4')
                                    {
                                        $pro_url = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/View/' . $value['user_id'];
                                    }
                                    elseif ($value['Role_id'] == '6')
                                    {
                                        $pro_url = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/View/' . $value['user_id'];
                                    }
                                    elseif ($value['Role_id'] == '7')
                                    {
                                        $pro_url = base_url() . 'Speakers/' . $acc_name . '/' . $Subdomain . '/View/' . $value['user_id'];
                                    }
                                    else
                                    {
                                        $pro_url = '#';
                                    }
                                ?>
                                <a href="<?php echo $pro_url?>" class="tooltip_data"><?php echo ucfirst($value['Firstname']) . ' ' . $value['Lastname']; ?>
                                </a>
                            </div>
                           
                            <div class="msg_date"><?php echo get_timeago(strtotime($value['Time'])); ?></div>
                                <?php
                                if ($value['activity_no'] == '4')
                                { ?>
                                    <div class="activity_session_rating">
                                    <?php
                                    for ($i = 1; $i <= 5; $i++)
                                    {
                                        if ($i <= $value['rating'])
                                        {
                                            $img = "star-on.png";
                                        }
                                        else
                                        {
                                            $img = "star-off.png";
                                        } ?>
                                        <img src="<?php echo base_url() . 'assets/images/' . $img; ?>">
                                        <?php } ?>     
                                    </div>
                                <?php
                                }?> 
                            </div>  
                            </div>


                            </div> <!-- card new -->

                            <?php } ?> 

                            <?php
                            foreach($facebook_feeds['data'] as $key => $value)
                            {
                                $allfbdivid = uniqid() . md5(rand()); ?>

                                <div class="card" mycard_id_2="<?php echo $value['id'];?>" data-sort="<?php echo strtotime($value['created_time']); ?>">

                                <div class="message_container facebook_message" data-sort="<?php echo strtotime($value['created_time']); ?>">
                                <?php if (!empty($value['full_picture'])) { ?>
                                    <div class="msg_photo">
                                        <a class="colorbox_<?php echo $allfbdivid; ?>" href="<?php echo $value['full_picture']; ?>">
                                            <img src="<?php echo $value['full_picture']; ?>">
                                        </a>
                                        <?php
                                        if (array_key_exists('subattachments', $value['attachments']['data'][0]))
                                        {
                                            $medialoop = $value['attachments']['data'][0]['subattachments']['data'];
                                        }
                                        else
                                        {
                                            $medialoop = $value['attachments']['data'];
                                        }
                                        if (count($medialoop) > 1)
                                        {
                                            foreach($medialoop as $ikey => $ivalue)
                                            {
                                                if ($ivalue['media']['image']['src'] != $value['full_picture'])
                                                { ?>
                                                    <a class="colorbox_<?php echo $allfbdivid; ?>" href="<?php echo $ivalue['media']['image']['src']; ?>"></a>
                                                <?php 
                                                }
                                            }
                                        }?>
                                    </div>
                                <?php } ?>
                                <div class="msg_message">
                                    <?php
                                        if (!empty($value['message']))
                                        { ?>
                                            <p class="short_msg_show">
                                                <?php echo substr($value['message'], 0, 300); ?>
                                            </p>
                                        <?php
                                        } ?>
                                </div>
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <img src="https://graph.facebook.com/<?php echo $arrPermission['fbpage_name']; ?>/picture?type=large">
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url() . 'assets/images/facebook-logo.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data">
                                            <?php echo ucfirst($value['from']['name']); ?>
                                        </a>
                                    </div>
                                    <div class="msg_date">
                                        <?php echo get_timeago(strtotime($value['created_time'])); ?>
                                    </div>
                                </div>
                            </div>

                             </div> <!-- card new -->

                            <?php } ?>
                            <?php
                            foreach($tweets['statuses'] as $key => $value)
                            {
                                $alltwitterdivid = uniqid() . md5(rand()); ?>

                                <div class="card" mycard_id="<?php echo $value['id'];?>" data-sort="<?php echo strtotime($value['created_at']); ?>">

                                <div class="message_container twitter_message" data-sort="<?php echo strtotime($value['created_at']); ?>"><?php
                                if (!empty($value['entities']['media'][0]['media_url']))
                                {?>
                                    <div class="msg_photo">
                                        <a class="colorbox_<?php echo $alltwitterdivid; ?>" href="<?php
                                    echo $value['entities']['media'][0]['media_url']; ?>">
                                        <img src="<?php
                                    echo $value['entities']['media'][0]['media_url']; ?>">
                        </a>
                                                            </div>     
                                                            <?php
                                } ?>

                                                            <div class="msg_message">
                                                                <?php
                                if (!empty($value['text']))
                                { ?>
                                                                <p class="short_msg_show">
                                                                    <?php
                                    echo substr($value['text'], 0, 300); ?>
                                                                </p>
                                                                <div class="fully_msg_show" style="display:none;">
                                                                    <p><?php
                                    echo $value['text']; ?></p>
                                                                </div>
                                                                <?php
                                    if (strlen($value['text']) > 300)
                                    { ?>
                                                               <!--  <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a> -->
                                                                <?php
                                    }
                                } ?>
                                                            </div>
                                                            

                                                            <div class="msg_main_body">
                                                                <div class="message_img">
                                                                    <img src="<?php
                                echo $value['user']['profile_image_url']; ?>">
                                                                </div>
                                                                <div class="msg_type_images pull-right" style="border-radius: 0px;">
                                                                    <img width="40" height="40" src="<?php
                                echo base_url() . 'assets/images/activity_twitter.png'; ?>">
                                                                </div>
                                                                <div class="msg_fromname">
                                                                    <a href="#" class="tooltip_data">
                                                                        <?php
                                echo ucfirst($value['user']['screen_name']); ?>
                                                                    </a>
                                                                </div>
                                                                <div class="msg_date">
                                                                    <?php
                                echo get_timeago(strtotime($value['created_at'])); ?>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>


                                         </div> <!-- card new -->

                                                        <?php
                            } 
                            ?>
                        </div>
                        
                            
                    </div>


        </div>

         <div id="facebook_next_urls_all" style="display: none;">
                            <?php echo urlencode($facebook_feeds['paging']['next']); ?>
                        </div>
                        <div id="twitter_next_result_all" style="display: none;">
                            <?php echo urlencode($tweets['search_metadata']['next_results']); ?>
        </div>
        <!-- <center>
            
                <a onclick="loadmore_all();" class="btn loadmore panel-green">Load More</a>
            
        </center> -->
    </div>
</div>

</div> <!-- container -->





<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>

<script src="<?php
echo base_url(); ?>assets/js/tether.min.js"></script>

<script src="https://cdn.rawgit.com/FezVrasta/bootstrap-material-design/dist/dist/bootstrap-material-design.iife.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php
echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>

<script>
    // $('body').bootstrapMaterialDesign();
    // $('.card').sort(function (a, b) {
    //   return $(b).find('.card-block').data('sort') - $(a).find('.card-block').data('sort');
    //     }).each(function (_, card) {
    //   $(card).parent().append(card);
    // });

 $('#messages_all').find('.card').sort(function (a, b) {

              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_all) {

              $(message_container_all).parent().append(message_container_all);
            });




</script>
<!-- <script type="text/javascript">
    var page = 1;
    var next_page = '';

    window.onload = function() {
    loadMoreData(1);
    };
    function loadMoreData(page){
      $.ajax(
            {
                url: 'http://localhost/social-feeds/load_more.php',
                type: "post",
                data: next_page,
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                if(data == ""){
                    $('.ajax-load').html("No more records found");
                    return;
                }
                $('.ajax-load').hide();
                $(".card-columns").append(data);
                next_page =  next_page_new;
                $('.card').sort(function (a, b) {
                  return $(b).find('.card-block').data('sort') - $(a).find('.card-block').data('sort');
                    }).each(function (_, card) {
                  $(card).parent().append(card);
                });
                // loadMoreData(1);
            });
    }
function get_share_post_popup(diskey)
{
    $('#share_popup_twitter_btn').attr('href',$.trim($('#twitter_share_href_data_div_'+diskey).html()));
    $('#share_popup_linkdin_btn').attr('href',$.trim($('#linkdin_share_href_data_div_'+diskey).html()));
    $('#share_popup_facebook_btn').attr('href',$.trim($('#facebook_share_href_data_div_'+diskey).html()));
    $('#share_social_post_popup').modal('show');
}
</script>
 -->





<script type="text/javascript">
    
    
    $(document).ready(function(){



    $(".sb-toggle-left").trigger("click");
   
    setInterval(function(){ loadmore_all(); }, 10000);   

    // $('#messages_all').find('.message_container').sort(function (a, b) {

        $('#messages_all').find('.card').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_all) {

              $(message_container_all).parent().append(message_container_all);


            });

      
}); 


 function loadmore_all()
{

  
    $('#loader_logo_all').show();
        jQuery.ajax({
        url: "<?php
echo base_url(); ?>activity/<?php
echo $acc_name . '/' . $Subdomain . '/all_loadmore_feed_socialwall'; ?>/"+$('#messages_all > .card > .internal_message').length,
        type:'post',
        data:'facebook_next_url='+jQuery('#facebook_next_urls_all').html()+"&twitter_next_result="+jQuery('#twitter_next_result_all').html(),
        async: true,
        success: function(result)
        {
            var data=jQuery.parseJSON(result);
            

            jQuery('#facebook_next_urls_all').html(data.all_facebook_next_url);
            jQuery('#twitter_next_result_all').html(data.all_twitter_next_result);
            if ($.trim(data.all_html)!="")
            {
                jQuery("#messages_all").append(data.all_html);


            }
            else
            {
                jQuery("#loadmore_all").css('display', 'none');
            }
            // $('#messages_all').find('.message_container').sort(function (a, b) {
             $('#messages_all').find('.card').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_all) {
              
              $(message_container_all).parent().append(message_container_all);
            });


              $(".card").each(function(){
                   var cardlength= $(this).attr('mycard_id');
                   if(cardlength>1){
                        $('div[mycard_id="'+cardlength+'"]:not(:first)').remove();
                      }

                    
                });


              $(".card").each(function(){
                   var cardlength= $(this).attr('mycard_id_2');
                   
                       
                    var numItems = $('div[mycard_id_2="'+cardlength+'"').length;
          
                    if(numItems>2){
                        $('div[mycard_id_2="'+cardlength+'"]:not(:first)').remove();
                      }

                    
                });
             
              
              // var mycardid=$('.card').attr();
              // var cardlength=$('div[mycard_id="value"]').length;
              // if(cardlength>1){
              //   $('div[mycard_id="value"]:not(:first)').remove();
              // }

             // $('div[comment_id="value"]:not(:first)').remove();

            // $(".twitter_message:not(:first)").remove();
            $('#loader_logo_all').hide();
        }
    });
}



</script>


<!-- Toggle a Full Screen -->
<script type="text/javascript">
function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
</script>


<!-- for mobile view -->
<script type="text/javascript">
   $(document).ready(function(){
    var fourth_segment="<?php echo $this->uri->segment(4);?>";

    if(fourth_segment=='socialwall'){
     
       setTimeout(function(){ $("body").removeClass("sidebar-mobile-open"); }, 1000);
       
     }
   });
</script>