<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newactivity.css?'.time(); ?>">
<?php $user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
if(empty($user))
{
   $url=base_url().'Activity/'.$acc_name.'/'.$Subdomain;
}
function get_timeago($ptime)
{
    $estimate_time = time() - $ptime;
    if ($estimate_time < 1)
    {
        return '1 second ago';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    foreach ($condition as $secs => $str)
    {
        $d = $estimate_time / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }
} ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white" id="exibitor">
        	<div class="panel-body" style="padding: 0px;">
        		<div class="tabbable">
                <div class="row">
                    <div class="col-md-9 col-sm-9">
                        <ul id="myTab2" class="nav nav-tabs">
                            <li class="active">
                                <a href="#all" data-toggle="tab">
                                    <img width="30" height="30" src="<?php echo base_url().'assets/images/activity_all.png'; ?>">
                                    <h4 style="text-align: center;">All</h4>
                                </a>    
                            </li>
                            <li>
                                <a href="#internal" data-toggle="tab">
                                    <img width="30" height="30" src="<?php echo base_url().'assets/images/activity_internal_img.png'; ?>">
                                    <h4 style="text-align: center;">internal</h4>
                                </a>    
                            </li>
                            <li>
                                <a href="#social" data-toggle="tab">
                                    <img width="30" height="30" src="<?php echo base_url().'assets/images/activity_social.png'; ?>">
                                    <h4 style="text-align: center;">Social</h4>
                                </a>    
                            </li>
                            <li>
                                <a href="#alerts" data-toggle="tab">
                                    <img width="30" height="30" src="<?php echo base_url().'assets/images/activity_alert.png'; ?>">
                                    <h4 style="text-align: center;">Alerts</h4>
                                </a>    
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <a href="javascript:void(0);" onclick="refresh_all_feed();" class="btnref">
                            <img width="30" height="30" src="<?php echo base_url().'assets/images/refreshlogo.png'; ?>" id="refresh_logo_images">
                        </a>
        		    </div>
                
                    </div>
                </div>
                <form id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/'; ?>upload_imag' style="clear:both">
                    <div class="facebok-container">
                        <div class="facebok-main">
                            <div class="facebok-head">
                                <ul>
                                    <li class="facebok-head-status">
                                        <span>Message</span>
                                    </li>
                                    <li class="facebok-head-photo"> 
                                        <span> Add Photo </span>
                                        <div id='imageloadstatus' style='display:none'>
                                            <img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/>
                                        </div>
                                        <div id='imageloadbutton'>
                                            <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="facebok-middle">
                                <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text" placeholder="Post an Update....." id="Message" name="Message"></textarea>
                            </div>
                            <div class="fb-upload-img-box">
                                <ul id='preview'></ul>
                                <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                            </div>
                            <div class="facebok-footer clearfix">
                                <ul class="footer-left">
                                    <li class="camera-icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                                </ul>
                                <ul class="footer-right">
                                    <li class="submit-button-box">
                                        <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                            Publish Post <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>    
                    </div>
                </form>
        		<div class="tab-content">
                    <div class="tab-pane active fade in" id="all">
                        <div id="messages_all">
                            <?php //echo "<pre>"; print_r($activity_data);exit; ?>
                            <?php foreach ($activity_data as $key => $value) { $alldivid=uniqid().md5(rand());   ?>
                            <div class="message_container internal_message" data-sort="<?php echo strtotime($value['Time']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
                                        <?php }else{ ?>
                                        <img src="<?php echo base_url().'/assets/images/anonymous.jpg'; ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/activity_internal_img.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <?php
                                        if($value['Role_id'] == '4')
                                        {
                                            $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        elseif($value['Role_id'] == '6')
                                        {
                                            $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        elseif ($value['Role_id'] == '7')
                                        {
                                            $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        else
                                        {
                                            $pro_url = '#';
                                        } 
                                        ?>
                                        <a href="<?=$pro_url?>" class="tooltip_data"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></a>
                                    </div>
                                    <div class="msg_toname activity_msg">
                                        <?php echo ucfirst($value['activity']); ?>
                                    </div>
                                    <div class="msg_user_companyname"><?php echo $value['Title'].' at '.$value['Company_name']; ?></div>
                                    <div class="msg_date"><?php echo get_timeago(strtotime($value['Time'])); ?></div>
                                    <?php if($value['activity_no']=='4'){ ?>
                                    <div class="activity_session_rating">
                                        <?php for ($i=1;$i<=5;$i++) { 
                                            if($i<=$value['rating'])
                                            {
                                                $img="star-on.png";
                                            }
                                            else
                                            {
                                                $img="star-off.png";
                                            } ?>
                                            <img src="<?php echo base_url().'assets/images/'.$img; ?>">
                                        <?php } ?>     
                                    </div>
                                    <?php } ?> 
                                </div>
                                <div class="msg_message">
                                    <?php
                                    if(!empty($value['agenda_id']))
                                    { 
                                    $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
                                    ?>
                                        <h5><a href="<?=$agenda_url?>" class="msg_toname activity_msg"><?=$value['Message']?></a></h5>
                                        <div class="msg_date"><?=$value['agenda_time']?></div>
                                    <?php
                                    }
                                    else
                                    {
                                    if(!empty($value['Message'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['Message'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['Message']; ?></p>
                                    </div>
                                    <?php if(strlen($value['Message'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } } ?>
                                </div>
                                <?php if(count(json_decode($value['image'],TRUE)) > 0){ $img=json_decode($value['image'],TRUE); $img1=1; foreach ($img as $ikey => $ivalue) {  ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $alldivid; ?>" href="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php if($img1=='1'){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php $img1++; } ?>
                                    </a>
                                </div>     
                                <?php } } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;float: left;">
                                        <p class="count_like count_like_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count']=='' ? 0 : $value['like_count']; ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count'] > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                    <span style="text-align:left;">
                                        <p class="comment_count comment_count_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']); ?>
                                        </p>
                                        <p class="comment_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']) > 1 ? 'Comments' : 'Comment';  ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="like-dislike-btn col-sm-3 col-xs-3">
                                        <div class='like blue_like <?php echo 'dislike_'.$value['id'];?>' <?php if($value['is_like']!='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)'  class="like_btn blue_btn" onclick="unlike('<?php echo $value['id'];?>','<?php echo $value['type'];?>')" >
                                                <img width="40" height="40" src="<?php echo base_url().'assets/images/like new.png'; ?>">
                                            </a>
                                        </div>
                                        <div class='like <?php echo 'like_'.$value['id'];?>' <?php if($value['is_like']=='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)' class='like_btn'  onclick="like('<?php echo $value['id'];?>','<?php echo $value['type']; ?>')" >
                                                <img width="40" height="40" src="<?php echo base_url().'assets/images/not liked.png'; ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="show_add_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href='javascript: void(0);' onclick="showcommentbox('<?php echo $alldivid; ?>');" class='comment_btn'>
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/comment new.png'; ?>">
                                        </a>
                                    </div> 
                                    <div class="show_all_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="getcomment_popup('<?php echo $value['id']; ?>');">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/view new.png'; ?>">
                                        </a>
                                    </div>
                                    <div class="share_post_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                        </a>
                                        <div class="twitter_share_href_data_div" style="display: none;">https://twitter.com/home?status=“<?php echo $value['Message']; ?>” <?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>
                                        </div>
                                        <div class="linkdin_share_href_data_div" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>&title=<?php echo ucfirst($value['activity']); ?>&summary=<?php echo $value['Message']; if(!empty($img[0])){ ?>&source=<?php echo base_url().'assets/user_files/'.$img[0]; }else{ ?>&source=LinkedIn;<?php } ?>
                                        </div>
                                    </div> 
                                    <div class='comment_panel clearfix' id='slidepanel<?php echo $alldivid; ?>' style="display:none;">
                                        <form method='post' name='commentform_<?php echo $alldivid; ?>' id="commentform_<?php echo $alldivid; ?>" enctype='multipart/form-data' action="<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag'  ?>">
                                            <div class='comment_message_img'>
                                                <?php if ($user[0]->Logo!="")
                                                {
                                                    echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                                }
                                                else
                                                {
                                                    echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                } ?>
                                            </div>
                                            <textarea class='user_comment_input' id='comment_text_<?php echo $alldivid; ?>' placeholder='Comment' name='comment'></textarea>
                                            <div class='photo_view_icon'>
                                            <?php echo "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $alldivid . "\").click();' >&nbsp;</div>";
                                                 ?>
                                                 <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto<?php echo $alldivid; ?>' onchange='comment_photo("<?php echo $alldivid; ?>")' multiple></div>
                                                 <?php  
                                                  echo "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
                                                  echo "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
                                                  echo "<ul id='cpreview".$alldivid."' class='cpreview clearfix'></ul>";
                                                  echo "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
                                            ?>
                                                <input type='button' value='Comment'  class='comment_submit' onclick='addcomment("<?php echo $value['id']; ?>","<?php echo $alldivid ?>");'/>
                                        </form>
                                        <div class='comment_data comment_content_<?php echo $value['id']; ?>' style='display: none;'>
                                             <?php if (!empty($value['comments']))
                                             {
                                                  $i = 0;
                                                  $flag="false";
                                                  foreach ($value['comments'] as $ckey => $cval) 
                                                  {   
                                                       echo "<div class='comment_container comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
                                                       if ($cval['Logo'] != "")
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                       }
                                                       else
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                       }
                                                       echo "</div>
                                                       <div class='comment_wrapper'>        
                                                       <div class='comment_username'>
                                                            " . ucfirst($cval['name']) . "
                                                       </div>
                                                       <div class='comment_text'>
                                                            " . $cval['comment'] . "
                                                       </div>
                                                       <div class='comment_text' style='float:right;padding-left:13px;'>";
                                                       echo $cval['datetime'];
                                                       echo"</div></div>";
                                                       if ($cval['comment_image'] != "")
                                                       {
                                                            $image_comment = json_decode($cval['comment_image']);
                                                            echo "<div class='msg_photo photos_feed'>";
                                                            foreach ($image_comment as $ikey => $ivalue) {
                                                            echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
                                                            if($ikey=='0'){                                                            echo "view Photo";
                                                            }
                                                            echo "</a>";
                                                            }
                                                            echo "</div>";
                                                       }
                                                       if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
                                                       {
                                                            ?>
                                                            <button class='comment_section_btn' type='button' title='' onclick='removecomment("<?php echo $cval['comment_id']; ?>","<?php echo $value['id']; ?>",this)'>&nbsp;</button>
                                                            <?php 
                                                       }
                                                       echo "</div>";
                                                  } 
                                             } 
                                             ?>
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                            <?php } ?>
                            <?php foreach ($facebook_feeds['data'] as $key => $value) { $allfbdivid=uniqid().md5(rand()); ?>
                            <div class="message_container facebook_message" data-sort="<?php echo strtotime($value['created_time']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <img src="https://graph.facebook.com/<?php echo $arrPermission['fbpage_name']; ?>/picture?type=large">
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/facebook-logo.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data">
                                            <?php echo ucfirst($value['from']['name']); ?>
                                        </a>
                                    </div>
                                    <div class="msg_date">
                                        <?php echo get_timeago(strtotime($value['created_time'])); ?>
                                    </div>
                                </div>
                                <div class="msg_message">
                                    <?php if(!empty($value['message'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['message'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['message']; ?></p>
                                    </div>
                                    <?php if(strlen($value['message'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } ?>
                                </div>
                                <?php if(!empty($value['full_picture'])){ ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $allfbdivid; ?>" href="<?php echo $value['full_picture']; ?>">
                                        <img src="<?php echo $value['full_picture']; ?>">
                                    </a>
                                    <?php
                                    if(array_key_exists('subattachments',$value['attachments']['data'][0]))
                                    {

                                        $medialoop=$value['attachments']['data'][0]['subattachments']['data'];
                                    }
                                    else
                                    {
                                        $medialoop=$value['attachments']['data'];
                                    } if(count($medialoop)>1){ 
                                    foreach ($medialoop as $ikey => $ivalue) { if($ivalue['media']['image']['src']!=$value['full_picture']){ ?>
                                    <a class="colorbox_<?php echo $allfbdivid; ?>" href="<?php echo $ivalue['media']['image']['src']; ?>">
                                        
                                    </a>
                                    <?php } } } ?>
                                </div>     
                                <?php } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;float: left;">
                                        <p class="counnt_like" style='float:left;'>
                                            <?php echo count($value['likes']['data']) ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['likes']['data']) > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                    <span style="text-align:left;">
                                        <p class="counnt_comment" style='float:left;'>
                                            <?php echo count($value['comments']['data']) ?>
                                        </p>
                                        <p class="comment_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']['data']) > 1 ? 'Comments' : 'Comment'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="share_post_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                        </a>
                                        <?php
                                        $f_post_id = explode('_',$value['id']);
                                        ?>
                                        <div class="facebook_share_href_data_div" style="display: none;">
                                            https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                        <div class="twitter_share_href_data_div" style="display: none;">https://twitter.com/home?status=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                        <div class="linkdin_share_href_data_div" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                    </div>                                  
                                    <div class="show_all_comment_popup_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="getcomment_popup('<?php echo $value['id']; ?>');">
                                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/view new.png'; ?>">
                                        </a>
                                    </div>
                                </div>
                                <div class='comment_panel clearfix' id='slidepanel<?php echo $allfbdivid; ?>' style="display:none;">
                                    <div class='comment_data comment_content_<?php echo $value['id']; ?>' style='display: none;'>
                                        <?php if (!empty($value['comments']['data'])){
                                            $i = 0;
                                            $flag="false";
                                            foreach ($value['comments']['data'] as $ckey => $cval) 
                                            {   
                                                echo "<div class='comment_container comment_container_".$cval['id']."'><div class='comment_message_img'>";
                                                echo "<img src='https://graph.facebook.com/".$cval['from']['id']."/picture'>";
                                                echo "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['from']['name']) . "</div><div class='comment_text'>" . $cval['message'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
                                                echo get_timeago(strtotime($cval['created_time']));
                                                echo"</div></div>";
                                                echo "</div>";
                                            } 
                                        } ?>
                                    </div>
                                </div> 
                            </div>
                            <?php } ?>
                            <?php foreach ($tweets['statuses'] as $key => $value) { $alltwitterdivid=uniqid().md5(rand()); ?>
                            <div class="message_container twitter_message" data-sort="<?php echo strtotime($value['created_at']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <img src="<?php echo $value['user']['profile_image_url']; ?>">
                                    </div>
                                    <div class="msg_type_images pull-right" style="border-radius: 0px;">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/activity_twitter.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data">
                                            <?php echo ucfirst($value['user']['screen_name']); ?>
                                        </a>
                                    </div>
                                    <div class="msg_date">
                                        <?php echo get_timeago(strtotime($value['created_at'])); ?>
                                    </div>
                                </div>
                                <div class="msg_message">
                                    <?php if(!empty($value['text'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['text'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['text']; ?></p>
                                    </div>
                                    <?php if(strlen($value['text'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } ?>
                                </div>
                                <?php  if(!empty($value['entities']['media'][0]['media_url'])){ ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $alltwitterdivid; ?>" href="<?php echo $value['entities']['media'][0]['media_url']; ?>">
                                        <img src="<?php echo $value['entities']['media'][0]['media_url']; ?>">
                                    </a>
                                </div>     
                                <?php } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;">
                                        <p class="counnt_like" style='float:left;'>
                                            <?php echo $value['favorite_count']; ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['favorite_count'] > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="share_post_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                        </a>
                                        <div class="facebook_share_href_data_div" style="display: none;">
                                            https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?>
                                        </div>
                                        <div class="twitter_share_href_data_div" style="display: none;">https://twitter.com/home?status=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?>
                                        </div>
                                        <div class="linkdin_share_href_data_div" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?></div>
                                    </div>                                  
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div id="facebook_next_urls_all" style="display: none;">
                            <?php echo urlencode($facebook_feeds['paging']['next']); ?>
                        </div>
                        <div id="twitter_next_result_all" style="display: none;">
                            <?php echo urlencode($tweets['search_metadata']['next_results']); ?>
                        </div>
                        <div id="loadmore_all" class="load_btn" <?php if(empty($tweets['search_metadata']['next_results']) || empty($facebook_feeds['paging']['next']) || $internal_loadmore_show!='1'){ ?> style="display:none;" <?php } ?>>
                            <a href="javascript: void(0);" onclick="loadmore_all();" class="loadmore panel-green"> 
                                Load More <i class="fa fa-spinner fa-spin" id="loader_logo_all" style="display:none;"></i>
                            </a>
                        </div>
                    </div>
        			<div class="tab-pane fade in" id="internal">
        				<div id="messages_internal">
                            <?php foreach ($activity_data as $key => $value) { $internaldivid=uniqid().md5(rand());  ?>
                            <div class="message_container internal_message" data-sort="<?php echo strtotime($value['Time']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
                                        <?php }else{ ?>
                                        <img src="<?php echo base_url().'/assets/images/anonymous.jpg'; ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/activity_internal_img.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                    <?php
                                        if($value['Role_id'] == '4')
                                        {
                                            $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        elseif($value['Role_id'] == '6')
                                        {
                                            $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        elseif ($value['Role_id'] == '7')
                                        {
                                            $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
                                        }
                                        else
                                        {
                                            $pro_url = '#';
                                        } 
                                        ?>
                                        <a href="<?=$pro_url?>" class="tooltip_data"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></a>
                                    </div>
                                    <div class="msg_toname activity_msg">
                                        <?php echo ucfirst($value['activity']); ?>
                                    </div>
                                    <div class="msg_user_companyname"><?php echo $value['Title'].' at '.$value['Company_name']; ?></div>
                                    <div class="msg_date"><?php echo get_timeago(strtotime($value['Time'])); ?></div>
                                    <?php if($value['activity_no']=='4'){ ?>
                                    <div class="activity_session_rating">
                                        <?php for ($i=1;$i<=5;$i++) { 
                                            if($i<=$value['rating'])
                                            {
                                                $img="star-on.png";
                                            }
                                            else
                                            {
                                                $img="star-off.png";
                                            } ?>
                                            <img src="<?php echo base_url().'assets/images/'.$img; ?>">
                                        <?php } ?>     
                                    </div>
                                    <?php } ?> 
                                </div>
                                <div class="msg_message">
                                    <?php
                                    if(!empty($value['agenda_id']))
                                    { 
                                    $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
                                    ?>
                                        <h5><a href="<?=$agenda_url?>" class="msg_toname activity_msg"><?=$value['Message']?></a></h5>
                                        <div class="msg_date"><?=$value['agenda_time']?></div>
                                    <?php
                                    }
                                    else
                                    {
                                    if(!empty($value['Message'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['Message'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['Message']; ?></p>
                                    </div>
                                    <?php if(strlen($value['Message'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } } ?>
                                </div>
                                <?php  if(count(json_decode($value['image'],TRUE)) > 0){ $img=json_decode($value['image'],TRUE); $img1=1; foreach ($img as $ikey => $ivalue) {  ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $internaldivid; ?>" href="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php if($img1=='1'){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php $img1++; } ?>
                                    </a>
                                </div>     
                                <?php } } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;float: left;">
                                        <p class="count_like count_like_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count']=='' ? 0 : $value['like_count']; ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count'] > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                    <span style="text-align:left;">
                                        <p class="comment_count comment_count_<?php echo $value['id'];?>" style='float:left;'>  <?php echo count($value['comments']); ?>
                                        </p>
                                        <p class="comment_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']) > 1 ? 'Comments' : 'Comment'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="like-dislike-btn col-sm-3 col-xs-3">
                                        <div class='like blue_like <?php echo 'dislike_'.$value['id'];?>' <?php if($value['is_like']!='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)'  class="like_btn blue_btn" onclick="unlike('<?php echo $value['id'];?>','<?php echo $value['type'];?>')" >
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/like new.png'; ?>">
                                            <!-- <i style="font-size: 40px" class='fa fa-thumbs-o-up'></i>  -->
                                            </a>
                                        </div>
                                        <div class='like <?php echo 'like_'.$value['id'];?>' <?php if($value['is_like']=='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)' class='like_btn'  onclick="like('<?php echo $value['id'];?>','<?php echo $value['type']; ?>')" >
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/not liked.png'; ?>">
                                            <!-- <i style="font-size: 40px" class='fa fa-thumbs-o-up'></i>  -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="show_add_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href='javascript: void(0);' onclick="showcommentbox('<?php echo $internaldivid; ?>');" class='comment_btn'>
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/comment new.png'; ?>">
                                            <!-- <i class="flaticon-commenting" aria-hidden="true"></i> -->
                                        </a>
                                    </div> 
                                    <div class="show_all_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="getcomment_popup('<?php echo $value['id']; ?>');">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/view new.png'; ?>">
                                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                                        </a>
                                    </div>
                                    <div class="share_post_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                           <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                        </a>
                                        <div class="twitter_share_href_data_div_<?php echo $internaldivid; ?>" style="display: none;">https://twitter.com/home?status=“<?php echo $value['Message']; ?>” <?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>
                                        </div>
                                        <div class="linkdin_share_href_data_div_<?php echo $internaldivid; ?>" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>&title=<?php echo ucfirst($value['activity']); ?>&summary=<?php echo $value['Message']; if(!empty($img[0])){ ?>&source=<?php echo base_url().'assets/user_files/'.$img[0]; }else{ ?>&source=LinkedIn;<?php } ?>
                                        </div>
                                    </div> 
                                    <div class='comment_panel clearfix' id='slidepanel<?php echo $internaldivid; ?>' style="display:none;">
                                        <form method='post' name='commentform_<?php echo $internaldivid; ?>' id="commentform_<?php echo $internaldivid; ?>" enctype='multipart/form-data' action="<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag'  ?>">
                                            <div class='comment_message_img'>
                                                <?php if ($user[0]->Logo!="")
                                                {
                                                    echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                                }
                                                else
                                                {
                                                    echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                } ?>
                                            </div>
                                            <textarea class='user_comment_input' id='comment_text_<?php echo $internaldivid; ?>' placeholder='Comment' name='comment'></textarea>
                                            <div class='photo_view_icon'>
                                            <?php echo "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $internaldivid . "\").click();' >&nbsp;</div>";
                                                 ?>
                                                 <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto<?php echo $internaldivid; ?>' onchange='comment_photo("<?php echo $internaldivid; ?>")' multiple></div>
                                                 <?php  
                                                  echo "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
                                                  echo "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
                                                  echo "<ul id='cpreview".$internaldivid."' class='cpreview clearfix'></ul>";
                                                  echo "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
                                            ?>
                                                <input type='button' value='Comment'  class='comment_submit' onclick='addcomment("<?php echo $value['id']; ?>","<?php echo $internaldivid; ?>");'/>
                                        </form>
                                        <div class='comment_data comment_content_<?php echo $value['id']; ?>' style='display: none;'>
                                             <?php if (!empty($value['comments']))
                                             {
                                                  $i = 0;
                                                  $flag="false";
                                                  foreach ($value['comments'] as $ckey => $cval) 
                                                  {   
                                                       echo "<div class='comment_container comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
                                                       if ($cval['Logo'] != "")
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                       }
                                                       else
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                       }
                                                       echo "</div>
                                                       <div class='comment_wrapper'>        
                                                       <div class='comment_username'>
                                                            " . ucfirst($cval['name']) . "
                                                       </div>
                                                       <div class='comment_text'>
                                                            " . $cval['comment'] . "
                                                       </div>
                                                       <div class='comment_text' style='float:right;padding-left:13px;'>";
                                                       echo $cval['datetime'];
                                                       echo"</div></div>";
                                                       if ($cval['comment_image'] != "")
                                                       {
                                                            $image_comment = json_decode($cval['comment_image']);
                                                            echo "<div class='msg_photo photos_feed'>";
                                                            foreach ($image_comment as $ikey => $ivalue) {
                                                            echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
                                                            if($ikey=='0'){                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
                                                            }
                                                            echo "</a>";
                                                            }
                                                            echo "</div>";
                                                       }
                                                       if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
                                                       {
                                                            ?>
                                                            <button class='comment_section_btn' type='button' title='' onclick='removecomment("<?php echo $cval['comment_id']; ?>","<?php echo $value['id']; ?>",this)'>&nbsp;</button>
                                                            <?php 
                                                       }
                                                       echo "</div>";
                                                  } 
                                             } 
                                             ?>
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if($internal_loadmore_show=='1'){ ?>
                        <div id="loadmore_internal" class="load_btn">
                            <a href="javascript: void(0);" onclick="loadmore_internal();" class="loadmore panel-green"> 
                                Load More <i class="fa fa-spinner fa-spin" id="loader_logo_internal" style="display:none;"></i>
                            </a>
                        </div>
                        <?php } ?>
        			</div>
        			<div class="tab-pane fade in" id="social">
                        <div id="messages_social">
            			    <?php foreach ($facebook_feeds['data'] as $key => $value) { $socialfbdivid=uniqid().md5(rand()); ?>
                            <div class="message_container facebook_message" data-sort="<?php echo strtotime($value['created_time']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <img src="https://graph.facebook.com/<?php echo $arrPermission['fbpage_name']; ?>/picture?type=large">
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/facebook-logo.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data">
                                            <?php echo ucfirst($value['from']['name']); ?>
                                        </a>
                                    </div>
                                    <div class="msg_date">
                                        <?php echo get_timeago(strtotime($value['created_time'])); ?>
                                    </div>
                                </div>
                                <div class="msg_message">
                                    <?php if(!empty($value['message'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['message'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['message']; ?></p>
                                    </div>
                                    <?php if(strlen($value['message'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } ?>
                                </div>
                                <?php if(!empty($value['full_picture'])){ ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $socialfbdivid; ?>" href="<?php echo $value['full_picture']; ?>">
                                        <img src="<?php echo $value['full_picture']; ?>">
                                    </a>
                                    <?php
                                    if(array_key_exists('subattachments',$value['attachments']['data'][0]))
                                    {

                                        $medialoop=$value['attachments']['data'][0]['subattachments']['data'];
                                    }
                                    else
                                    {
                                        $medialoop=$value['attachments']['data'];
                                    } if(count($medialoop)>1){ 
                                    foreach ($medialoop as $ikey => $ivalue) { if($ivalue['media']['image']['src']!=$value['full_picture']){ ?>
                                    <a class="colorbox_<?php echo $socialfbdivid; ?>" href="<?php echo $ivalue['media']['image']['src']; ?>">
                                        
                                    </a>
                                    <?php } } } ?>
                                </div>     
                                <?php } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;float: left;">
                                        <p class="counnt_like" style='float:left;'>
                                            <?php echo count($value['likes']['data']) ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['likes']['data']) > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                    <span style="text-align:left;">
                                        <p class="counnt_comment" style='float:left;'>
                                            <?php echo count($value['comments']['data']) ?>
                                        </p>
                                        <p class="comment_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']['data']) > 1 ? 'Comments' : 'Comment'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="share_post_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                        </a>
                                        <?php
                                        $f_post_id = explode('_',$value['id']);
                                        ?>
                                        <div class="facebook_share_href_data_div_<?php echo $socialfbdivid; ?>" style="display: none;">
                                        	https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                        <div class="twitter_share_href_data_div_<?php echo $socialfbdivid; ?>" style="display: none;">https://twitter.com/home?status=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                        <div class="linkdin_share_href_data_div_<?php echo $socialfbdivid; ?>" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=https://www.facebook.com/<?=$arrPermission['fbpage_name']?>/posts/<?=$f_post_id['1']?>
                                        </div>
                                    </div>                                  
                                    <div class="show_all_comment_popup_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="getcomment_popup('<?php echo $value['id']; ?>');">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/comment new.png'; ?>">
                                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                                        </a>
                                    </div>
                                </div>
                                <div class='comment_panel clearfix' id='slidepanel<?php echo $socialfbdivid; ?>' style="display:none;">
                                    <div class='comment_data comment_content_<?php echo $value['id']; ?>' style='display: none;'>
                                        <?php if (!empty($value['comments']['data'])){
                                            $i = 0;
                                            $flag="false";
                                            foreach ($value['comments']['data'] as $ckey => $cval) 
                                            {   
                                                echo "<div class='comment_container comment_container_".$cval['id']."'><div class='comment_message_img'>";
                                                echo "<img src='https://graph.facebook.com/".$cval['from']['id']."/picture'>";
                                                echo "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['from']['name']) . "</div><div class='comment_text'>" . $cval['message'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
                                                echo get_timeago(strtotime($cval['created_time']));
                                                echo"</div></div>";
                                                if ($cval['comment_image'] != "")
                                                {
                                                    $image_comment = json_decode($cval['comment_image']);
                                                    echo "<div class='msg_photo photos_feed'>";
                                                    foreach ($image_comment as $ikey => $ivalue) {
                                                        echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
                                                        if($ikey=='0'){                                              echo "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
                                                        }
                                                        echo "</a>";
                                                    }
                                                    echo "</div>";
                                                }
                                                echo "</div>";
                                            } 
                                        } ?>
                                    </div>
                                </div> 
                            </div>
                            <?php } ?>
            				<?php foreach ($tweets['statuses'] as $key => $value) { $socialtwitterdivid=uniqid().md5(rand()); ?>
                            <div class="message_container twitter_message" data-sort="<?php echo strtotime($value['created_at']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <img src="<?php echo $value['user']['profile_image_url']; ?>">
                                    </div>
                                    <div class="msg_type_images pull-right" style="border-radius: 0px;">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/activity_twitter.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data">
                                            <?php echo ucfirst($value['user']['screen_name']); ?>
                                        </a>
                                    </div>
                                    <div class="msg_date">
                                        <?php echo get_timeago(strtotime($value['created_at'])); ?>
                                    </div>
                                </div>
                                <div class="msg_message">
                                    <?php if(!empty($value['text'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['text'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['text']; ?></p>
                                    </div>
                                    <?php if(strlen($value['text'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } ?>
                                </div>
                                <?php  if(!empty($value['entities']['media'][0]['media_url'])){ ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $socialtwitterdivid; ?>" href="<?php echo $value['entities']['media'][0]['media_url']; ?>">
                                        <img src="<?php echo $value['entities']['media'][0]['media_url']; ?>">
                                    </a>
                                </div>     
                                <?php } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;">
                                        <p class="counnt_like" style='float:left;'>
                                            <?php echo $value['favorite_count']; ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['favorite_count'] > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="share_post_btn col-sm-3 col-xs-3 pull-right">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                        </a>
                                        <div class="facebook_share_href_data_div_<?php echo $socialtwitterdivid; ?>" style="display: none;">
                                        	https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?>
                                        </div>
                                        <div class="twitter_share_href_data_div_<?php echo $socialtwitterdivid; ?>" style="display: none;">https://twitter.com/home?status=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?>
                                        </div>
                                        <div id="linkdin_share_href_data_div_<?php echo $socialtwitterdivid; ?>" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=https://twitter.com/<?=$value['user']['screen_name']?>/status/<?=$value['id_str']?></div>
                                    </div>                                  
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div id="facebook_next_urls_social" style="display: none;">
                            <?php echo urlencode($facebook_feeds['paging']['next']); ?>
                        </div>
                        <div id="twitter_next_urls_social" style="display: none;">
                            <?php echo urlencode($tweets['search_metadata']['next_results']); ?>
                        </div>
                        <div id="loadmore_social" class="load_btn" <?php if(empty($tweets['search_metadata']['next_results']) || empty($facebook_feeds['paging']['next'])){ ?> style="display:none;" <?php } ?>>
                            <a href="javascript: void(0);" onclick="loadmore_social();" class="loadmore panel-green"> 
                                Load More <i class="fa fa-spinner fa-spin" id="loader_logo_social" style="display:none;"></i>
                            </a>
                        </div>
        			</div>
        			<div class="tab-pane fade in" id="alerts">
        			    <div id="messages_alerts">
                            <?php foreach ($orgactivity_data as $key => $value) { $alertdivid=uniqid().md5(rand()); ?>
                            <div class="message_container alerts_message" data-sort="<?php echo strtotime($value['Time']); ?>">
                                <div class="msg_main_body">
                                    <div class="message_img">
                                        <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
                                        <?php }else{ ?>
                                        <img src="<?php echo base_url().'/assets/images/anonymous.jpg'; ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="msg_type_images pull-right">
                                        <img width="40" height="40" src="<?php echo base_url().'assets/images/activity_alert.png'; ?>">
                                    </div>
                                    <div class="msg_fromname">
                                        <a href="#" class="tooltip_data"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></a>
                                    </div>
                                    <div class="msg_toname activity_msg">
                                        <?php echo ucfirst($value['activity']); ?>
                                    </div>
                                    <div class="msg_user_companyname"><?php echo $value['Title'].' at '.$value['Company_name']; ?></div>
                                    <div class="msg_date"><?php echo get_timeago(strtotime($value['Time'])); ?></div>
                                    <?php if($value['activity_no']=='4'){ ?>
                                    <div class="activity_session_rating">
                                        <?php for ($i=1;$i<=5;$i++) { 
                                            if($i<=$value['rating'])
                                            {
                                                $img="star-on.png";
                                            }
                                            else
                                            {
                                                $img="star-off.png";
                                            } ?>
                                            <img src="<?php echo base_url().'assets/images/'.$img; ?>">
                                        <?php } ?>     
                                    </div>
                                    <?php } ?> 
                                </div>
                                <div class="msg_message">
                                    <?php
                                    if(!empty($value['agenda_id']))
                                    { 
                                    $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
                                    ?>
                                        <h5><a href="<?=$agenda_url?>" class="msg_toname activity_msg"><?=$value['Message']?></a></h5>
                                        <div class="msg_date"><?=$value['agenda_time']?></div>
                                    <?php
                                    }
                                    else
                                    {
                                     if(!empty($value['Message'])){ ?>
                                    <p class="short_msg_show">
                                        <?php echo substr($value['Message'],0,300); ?>
                                    </p>
                                    <div class="fully_msg_show" style="display:none;">
                                        <p><?php echo $value['Message']; ?></p>
                                    </div>
                                    <?php if(strlen($value['Message'])>300){ ?>
                                    <a href="javascript:void(0);" onclick='readmore(this);'>Read More</a>
                                    <?php } } } ?>
                                </div>
                                <?php if(count(json_decode($value['image'],TRUE)) > 0){ $img=json_decode($value['image'],TRUE); $img1=1; foreach ($img as $ikey => $ivalue) {  ?>
                                <div class="msg_photo">
                                    <a class="colorbox_<?php echo $alertdivid; ?>" href="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php if($img1=='1'){ ?>
                                        <img src="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                    <?php $img1++; } ?>
                                    </a>
                                </div>     
                                <?php } } ?>
                                <div class="like_user_totale">
                                    <span style="text-align:left;float: left;">
                                        <p class="count_like count_like_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count']=='' ? 0 : $value['like_count']; ?>
                                        </p>
                                        <p class="like_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo $value['like_count'] > 1 ? 'Likes' : 'Like'; ?>
                                        </p>    
                                    </span>
                                    <span style="text-align:left;">
                                        <p class="comment_count comment_count_<?php echo $value['id'];?>" style='float:left;'>  <?php echo count($value['comments']); ?>
                                        </p>
                                        <p class="comment_text_<?php echo $value['id'];?>" style='float:left;'>
                                            <?php echo count($value['comments']) > 1 ? 'Comments' : 'Comment'; ?>
                                        </p>    
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 activity_feed_btn">
                                    <div class="like-dislike-btn col-sm-3 col-xs-3">
                                        <div class='like blue_like <?php echo 'dislike_'.$value['id'];?>' <?php if($value['is_like']!='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)'  class="like_btn blue_btn" onclick="unlike('<?php echo $value['id'];?>','<?php echo $value['type'];?>')" >
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/like new.png'; ?>">
                                            <!-- <i style="font-size: 40px" class='fa fa-thumbs-o-up'></i>  -->
                                            </a>
                                        </div>
                                        <div class='like <?php echo 'like_'.$value['id'];?>' <?php if($value['is_like']=='1'){ ?> style="display:none;" <?php } ?>>
                                            <a href='javascript:void(0)' class='like_btn'  onclick="like('<?php echo $value['id'];?>','<?php echo $value['type']; ?>')" >
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/not liked.png'; ?>">
                                            <!-- <i style="font-size: 40px" class='fa fa-thumbs-o-up'></i>  -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="show_add_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href='javascript: void(0);' onclick="showcommentbox('<?php echo $alertdivid; ?>');" class='comment_btn'>
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/comment new.png'; ?>">
                                            <!-- <i class="flaticon-commenting" aria-hidden="true"></i> -->
                                        </a>
                                    </div> 
                                    <div class="show_all_comment_popup_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="getcomment_popup('<?php echo $value['id']; ?>');">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/view new.png'; ?>">
                                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> -->
                                        </a>
                                    </div>
                                    <div class="share_post_btn col-sm-3 col-xs-3">
                                        <a href="javascript:void(0);" onclick="get_share_post_popup(this);">
                                            <img width="40" height="40" src="<?php echo base_url().'assets/images/share new.png'; ?>">
                                            <!-- <i class="fa fa-share-square-o" aria-hidden="true"></i> -->
                                        </a>
                                        <div class="twitter_share_href_data_div_<?php echo $alertdivid; ?>" style="display: none;">https://twitter.com/home?status=“<?php echo $value['Message']; ?>” <?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>
                                        </div>
                                        <div class="linkdin_share_href_data_div_<?php echo $alertdivid; ?>" style="display: none;">https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url() ?>Activity/<?php echo $acc_name.'/'.$Subdomain; ?>&title=<?php echo ucfirst($value['activity']); ?>&summary=<?php echo $value['Message']; if(!empty($img[0])){ ?>&source=<?php echo base_url().'assets/user_files/'.$img[0]; }else{ ?>&source=LinkedIn;<?php } ?>
                                        </div>
                                    </div> 
                                    <div class='comment_panel clearfix' id='slidepanel<?php echo $alertdivid; ?>' style="display:none;">
                                        <form method='post' name='commentform_<?php echo $alertdivid; ?>' id="commentform_<?php echo $alertdivid; ?>" enctype='multipart/form-data' action="<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag'  ?>">
                                            <div class='comment_message_img'>
                                                <?php if ($user[0]->Logo!="")
                                                {
                                                    echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                                                }
                                                else
                                                {
                                                    echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                } ?>
                                            </div>
                                            <textarea class='user_comment_input' id='comment_text_<?php echo $alertdivid; ?>' placeholder='Comment' name='comment'></textarea>
                                            <div class='photo_view_icon'>
                                            <?php echo "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $alertdivid . "\").click();' >&nbsp;</div>";
                                                  ?>
                                                  <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto<?php echo $alertdivid; ?>' onchange='comment_photo("<?php echo $alertdivid; ?>")' multiple></div>
                                                  <?php 
                                                  echo "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
                                                  echo "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
                                                  echo "<ul id='cpreview".$alertdivid."' class='cpreview clearfix'></ul>";
                                                  echo "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
                                            ?>
                                                <input type='button' value='Comment'  class='comment_submit' onclick='addcomment("<?php echo $value['id']; ?>","<?php echo $alertdivid; ?>");' />
                                        </form>
                                        <div class='comment_data comment_content_<?php echo $value['id']; ?>' style='display: none;'>
                                             <?php if (!empty($value['comments']))
                                             {
                                                  $i = 0;
                                                  $flag="false";
                                                  foreach ($value['comments'] as $ckey => $cval) 
                                                  {   
                                                       echo "<div class='comment_container comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
                                                       if ($cval['Logo'] != "")
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                                       }
                                                       else
                                                       {
                                                            echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                                       }
                                                       echo "</div>
                                                       <div class='comment_wrapper'>        
                                                       <div class='comment_username'>
                                                            " . ucfirst($cval['name']) . "
                                                       </div>
                                                       <div class='comment_text'>
                                                            " . $cval['comment'] . "
                                                       </div>
                                                       <div class='comment_text' style='float:right;padding-left:13px;'>";
                                                       echo $cval['datetime'];
                                                       echo"</div></div>";
                                                       if ($cval['comment_image'] != "")
                                                       {
                                                            $image_comment = json_decode($cval['comment_image']);
                                                            echo "<div class='msg_photo photos_feed'>";
                                                            foreach ($image_comment as $ikey => $ivalue) {
                                                            echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
                                                            if($ikey=='0'){                                                            echo "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
                                                            }
                                                            echo "</a>";
                                                            }
                                                            echo "</div>";
                                                       }
                                                       if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
                                                       {
                                                            ?>
                                                            <button class='comment_section_btn' type='button' title='' onclick='removecomment("<?php echo $cval['comment_id']; ?>","<?php echo $value['id']; ?>",this)'>&nbsp;</button>
                                                            <?php 
                                                       }
                                                       echo "</div>";
                                                  } 
                                             } 
                                             ?>
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if($alerts_loadmore_show=='1'){ ?>
                        <div id="loadmore_alerts" class="load_btn">
                            <a href="javascript: void(0);" onclick="loadmore_alerts();" class="loadmore panel-green"> 
                                Load More <i class="fa fa-spinner fa-spin" id="loader_logo_alerts" style="display:none;"></i>
                            </a>
                        </div>
                        <?php } ?>
        			</div>
        		</div>
        	</div>
        </div>	
	</div>
</div>
<div class="modal fade" id="show_comment_popup" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Comments</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="main_comment_div_in_popup">
                    
                </div>
                <h3 style="display: none;text-align: center;" id="no_comments_msg">No comments have been made yet.</h3>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="share_social_post_popup" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share this post</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                	<a class="" href="" target="_blank" class="twi-bg" id="share_popup_facebook_btn" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="" href="" target="_blank" class="twi-bg" id="share_popup_twitter_btn" onclick="return !window.open(this.href, 'Twitter', 'width=640,height=580')"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a class="" href="" target="_blank" id="share_popup_linkdin_btn" onclick="return !window.open(this.href, 'Twitter', 'width=640,height=580')"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(window).load(function(){
    $('#messages_social').find('.message_container').sort(function (a, b) {
      return $(b).data('sort') - $(a).data('sort');
        }).each(function (_, message_container_social) {
      $(message_container_social).parent().append(message_container_social);
    });
    $('#messages_all').find('.message_container').sort(function (a, b) {
      return $(b).data('sort') - $(a).data('sort');
        }).each(function (_, message_container_all) {
      $(message_container_all).parent().append(message_container_all);
    });  
});
function showcommentbox(id)
{
    $('#slidepanel'+id).slideToggle('slow');
}
function comment_photo(id)
{
    var user="<?php echo !empty($user) ? '1' : '0'; ?>";
    if(user==1)
    {
        jQuery("#commentform_"+id).ajaxForm({target: '#cpreview' + id,
            beforeSubmit: function() {
                jQuery('#camera_icon_comment').hide();
                jQuery("#imageloadstatus").show();
                jQuery("#imageloadbutton").hide();
            },
            success: function() {
                jQuery("#imageloadstatus").hide();
                jQuery("#imageloadbutton").show();
                jQuery(".comment_section_btn").click(function(){
                    jQuery(".addmore_photo").css("display", "none");
                    jQuery(this).parent().remove();
                    jQuery("#preview li").each(function() {
                        jQuery(".addmore_photo").css("display", "block");
                    });
                    jQuery('#camera_icon_comment').show();
                });
            },
            error: function() {
                jQuery("#imageloadstatus").hide();
                jQuery("#imageloadbutton").show();
            }
        }).submit();
        return false;
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }
}
function addcomment(id,divid)
{
    var user="<?php echo !empty($user) ? '1' : '0'; ?>";
    if(user==1)
    {
        var str = jQuery("#commentform_"+divid).serialize();
        var values = jQuery("input[name='cmphto[]']");
        if (jQuery.trim(jQuery("#comment_text_"+divid).val()) != "" || values.val() != "")
        {
            jQuery.ajax({
                url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name."/".$Subdomain; ?>/commentaddpublic/",
                data: str+"&key_post="+id,
                type: "POST",
                async: true,
                success: function(result)
                {
                    var comm=parseInt(jQuery('.comment_count_'+id).html())+1;
                    jQuery('.comment_count_'+id).html(comm);
                    if(comm > 1)
                    {
                        jQuery('.comment_text_'+id).html('Comments');
                    }
                    else
                    {
                        jQuery('.comment_text_'+id).html('Comment');
                    }
                    jQuery(".comment_content_"+id).html(result);
                    jQuery("#comment_text_"+divid).val('');
                    jQuery("#cpreview"+divid).html('');
                    jQuery('#camera_icon_comment').show();
                }
            });
        }
        else
        {
            alert("Please write something or photo to add comment.");
            jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
            return false;
        }
        return false;
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }
}
function removecomment(id,post_id,e)
{
    var user="<?php echo !empty($user) ? '1' : '0'; ?>";
    if(user==1)
    {
        jQuery.ajax({
            url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name."/".$Subdomain; ?>/delete_comment/" + id,
            type: "POST",
            async: true,
            success: function(result)
            {
                var comm=parseInt(jQuery('.comment_count_'+post_id).html())-1;
                jQuery('.comment_count_'+post_id).html(comm);
                if(comm > 1)
                {
                    jQuery('.comment_text_'+post_id).html('Comments');
                }
                else
                {
                    jQuery('.comment_text_'+post_id).html('Comment');
                }
                jQuery('.comment_container_'+id).remove();
                jQuery(e).parent().remove();
                if(jQuery(e).parent().parent().find('.comment_container  div').length > 0)
                {
                    $('#main_comment_div_in_popup').show();
                    $('#no_comments_msg').hide();
                    $('#show_comment_popup').modal('show');
                    $('#main_comment_div_in_popup').html(jQuery(e).parent().parent().html());
                }
                else
                {
                    $('#show_comment_popup').modal('show');
                    $('#main_comment_div_in_popup').hide();
                    $('#no_comments_msg').show();
                }
            }
        });
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }
}
function like(post_id,post_type)
{
    var user_id="<?php echo $user[0]->Id; ?>";
    if(user_id!=""){
        $.ajax({
            url:"<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/Save_user_like_data/' ?>",
            type:"POST",
            data:"module_primary_id="+post_id+"&module_type="+post_type+"&user_id="+user_id,
            success:function(result)
            {
                $('.count_like_'+post_id).html(result);
                if(parseInt(result) > 1)
                {
                    jQuery('.like_text_'+post_id).html('Likes');
                }
                else
                {
                    jQuery('.like_text_'+post_id).html('Like');
                }
                $('.like_'+post_id).hide();
                $('.dislike_'+post_id).show();
            }
        });
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }
}
function unlike(post_id,post_type)
{
    var user_id="<?php echo $user[0]->Id; ?>";
    if(user_id!=""){
        $.ajax({
            url:"<?php echo base_url().'Activity/'.$acc_name.'/'.$Subdomain.'/Save_user_like_data' ?>",
            type:"POST",
            data:"module_primary_id="+post_id+"&module_type="+post_type+"&user_id="+user_id,
            success:function(result)
            {
                $('.count_like_'+post_id).html(result);
                if(parseInt(result) > 1)
                {
                    jQuery('.like_text_'+post_id).html('Likes');
                }
                else
                {
                    jQuery('.like_text_'+post_id).html('Like');
                }
                $('.like_'+post_id).show();
                $('.dislike_'+post_id).hide();
            }
        });
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }     
} 
function readmore(elem)
{
    var text=$.trim($(elem).text());
    if(text=="Read More")
    {
        $(elem).parent().find('.short_msg_show').hide();
        $(elem).parent().find('.fully_msg_show').show();
        $(elem).html("Read Less");
    }
    else
    {
        $(elem).parent().find('.short_msg_show').show();
        $(elem).parent().find('.fully_msg_show').hide();
        $(elem).html("Read More");
    }
}
function getcomment_popup(post_id)
{
    if($('.comment_content_'+post_id).find('.comment_container  div').length > 0)
    {
        $('#main_comment_div_in_popup').show();
        $('#no_comments_msg').hide();
        $('#show_comment_popup').modal('show');
        $('#main_comment_div_in_popup').html($('.comment_content_'+post_id).html());
    }
    else
    {
        $('#show_comment_popup').modal('show');
        $('#main_comment_div_in_popup').hide();
        $('#no_comments_msg').show();
    }
}
function get_share_post_popup(elem)
{
    $('#share_popup_twitter_btn').attr('href',$.trim($(elem).parent().find('.twitter_share_href_data_div').html()));
    $('#share_popup_linkdin_btn').attr('href',$.trim($(elem).parent().find('.linkdin_share_href_data_div').html()));
    $('#share_popup_facebook_btn').attr('href',$.trim($(elem).parent().find('.facebook_share_href_data_div').html()));
    $('#share_social_post_popup').modal('show');
}
function loadmore_all()
{
    $('#loader_logo_all').show();
        jQuery.ajax({
        url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/all_loadmore_feed'; ?>/"+$('#messages_all > .internal_message').length,
        type:'post',
        data:'facebook_next_url='+jQuery('#facebook_next_urls_all').html()+"&twitter_next_result="+jQuery('#twitter_next_result_all').html(),
        async: true,
        success: function(result)
        {
            var data=jQuery.parseJSON(result);
            jQuery('#facebook_next_urls_all').html(data.all_facebook_next_url);
            jQuery('#twitter_next_result_all').html(data.all_twitter_next_result);
            if ($.trim(data.all_html)!="")
            {
                jQuery("#messages_all").append(data.all_html);
            }
            else
            {
                jQuery("#loadmore_all").css('display', 'none');
            }
            $('#messages_all').find('.message_container').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_all) {
              $(message_container_all).parent().append(message_container_all);
            });
            $('#loader_logo_all').hide();
        }
    });
}
function loadmore_internal()
{
    $('#loader_logo_internal').show();
        jQuery.ajax({
        url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/internal_loadmore_feed'; ?>/"+$('#messages_internal > div').length,
        async: true,
        success: function(result)
        {
            if ($.trim(result)!="No Feed")
            {
                jQuery("#messages_internal").append(result);
            }
            else
            {
                jQuery("#loadmore_internal").css('display', 'none');
            }
            $('#loader_logo_internal').hide();
        }
    });
}
function loadmore_social()
{
    $('#loader_logo_social').show();
        jQuery.ajax({
        url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/social_loadmore_feed'; ?>/"+$('#messages_social > div').length,
        type:'post',
        data:'facebook_next_url='+jQuery('#facebook_next_urls_social').html()+"&twitter_next_result="+jQuery('#twitter_next_result_social').html(),
        async: true,
        success: function(result)
        {
            var data=jQuery.parseJSON(result);
            jQuery('#facebook_next_urls_social').html(data.social_facebook_next_url);
            jQuery('#twitter_next_result_social').html(data.social_twitter_next_result);
            if ($.trim(data.social_html)!="")
            {
                jQuery("#messages_social").append(data.social_html);
            }
            else
            {
                jQuery("#loadmore_social").css('display', 'none');
            }
            $('#messages_social').find('.message_container').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_social) {
              $(message_container_social).parent().append(message_container_social);
            });
            $('#loader_logo_social').hide();
        }
    });
}
function loadmore_alerts()
{
    $('#loader_logo_alerts').show();
        jQuery.ajax({
        url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/alerts_loadmore_feed'; ?>/"+$('#messages_alerts > div').length,
        async: true,
        success: function(result)
        {
            if ($.trim(result)!="No Feed")
            {
                jQuery("#messages_alerts").append(result);
            }
            else
            {
                jQuery("#loadmore_alerts").css('display', 'none');
            }
            $('#loader_logo_alerts').hide();
        }
    });
}
function refresh_all_feed()
{
    $('#refresh_logo_images').attr('style','animation: spin 2s linear infinite;');
    jQuery.ajax({
        url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/refresh_all_feed'; ?>",
        async: true,
        error:function(error)
        {
            $('#refresh_loader_logo').removeAttr('style');
        },
        success: function(result)
        {
            var data=jQuery.parseJSON(result);
            jQuery('#messages_all').html(data.all_html);
            jQuery('#facebook_next_urls_all').html(data.all_facebook_next_url);
            jQuery('#twitter_next_result_all').html(data.all_twitter_next_result);
            if($.trim(data.all_loadmore_show)=='1')
            {
                jQuery("#loadmore_all").show();
            }
            else
            {
                jQuery("#loadmore_all").hide();   
            }
            jQuery("#messages_internal").html(data.internal_html);
            if($.trim(data.internal_loadmore_show)=='1')
            {
                jQuery("#loadmore_internal").show();
            }
            else
            {
                jQuery("#loadmore_internal").hide();   
            }
            jQuery('#messages_social').html(data.social_html);
            jQuery('#facebook_next_urls_social').html(data.social_facebook_next_url);
            jQuery('#twitter_next_urls_social').html(data.social_twitter_next_result);
            if($.trim(data.social_loadmore_show)=='1')
            {
                jQuery("#loadmore_social").show();
            }
            else
            {
                jQuery("#loadmore_social").hide();   
            }
            jQuery("#messages_alerts").html(data.alerts_html);
            if($.trim(data.alerts_loadmore_show)=='1')
            {
                jQuery("#loadmore_alerts").show();
            }
            else
            {
                jQuery("#loadmore_alerts").hide();   
            }
            $('#messages_social').find('.message_container').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_social) {
              $(message_container_social).parent().append(message_container_social);
            });
            $('#messages_all').find('.message_container').sort(function (a, b) {
              return $(b).data('sort') - $(a).data('sort');
                }).each(function (_, message_container_all) {
              $(message_container_all).parent().append(message_container_all);
            }); 
            $('#refresh_logo_images').removeAttr('style');
        }
    });
}
function sendmessage()
{
    var user="<?php echo !empty($user) ? '1' : '0'; ?>";
    if(user==1)
    {
        jQuery("#sendbtn").html('Sending <i class="fa fa-refresh fa-spin"></i>');
        jQuery("#sendbtn").attr('disabled','disabled');
        var str = jQuery("#imageform").serialize();
        var flag=false;
        if(jQuery("#Message").val() != "")
        {
            flag=true;
        }
        if (flag==false)
        {
            alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
            jQuery("#sendbtn").html('Publish Post <i class="fa fa-arrow-circle-right"></i>');
            jQuery("#sendbtn").removeAttr('disabled');
            return false;
        }

        jQuery.ajax({
            url: "<?php echo base_url(); ?>Activity/<?php echo $acc_name.'/'.$Subdomain.'/addupdate_feed'; ?>/",
            data: str,
            type: "POST",
            async: true,
            success: function(result)
            {
                refresh_all_feed();    
                jQuery(".addmore_photo").css('display', 'none');      
                jQuery("#sendbtn").html('Publish Post <i class="fa fa-arrow-circle-right"></i>');
                jQuery("#sendbtn").removeAttr('disabled');
                jQuery('#Message').val(''); 
                jQuery("#preview").html('');
            }
        });
    }
    else
    {
        var url="<?php echo $url.'?action=1'; ?>";
        window.location.href=url;
    }
}
$('#myTab2 a').click(function(){
    var attrhref=$.trim($(this).attr('href'));
    if(attrhref=="#all" || attrhref=="#internal")
    {
        jQuery('#imageform').slideDown();
    }
    else
    {
        jQuery('#imageform').slideUp();
    }
});
jQuery.noConflict();
jQuery(document).ready(function() {
    jQuery(document).on('mouseover', '.msg_photo', function() {
        var my_class = $(this).find('a').attr('class');
        $(this).find('a').colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
    });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        jQuery("#photoimg").removeAttr('multiple');
    }
    else
    {
        jQuery("#photoimg").attr('multiple');
    }
    FormElements.init();
});
</script>  

