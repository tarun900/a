<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
     <div class="col-sm-12">
          <!-- start: TEXT FIELDS PANEL -->
          <div class="panel panel-white">
		  
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#document_list" data-toggle="tab">
                                Add Folder
                            </a>
                        </li>
                    </ul>
                </div>


               <div class="panel-body" style="padding: 0px;">
                  <div class="tab-content">
                    <div class="tab-pane fade active in" id="document_list">
                        <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
                           <div class="row">
                                <?php if ($this->session->flashdata('site_setting_data') == "Updated")
                                { ?>
                                     <div class="errorHandler alert alert-success no-display" style="display: block;">
                                          <i class="fa fa-remove-sign"></i> Updated Successfully.
                                     </div>
                                <?php } ?>
                                <div class="col-md-6">
                                     <div class="form-group">
                                          <label class="control-label">Title<span class="symbol required"></span>
                                          </label>
                                          <input type="text" placeholder="Title" class="form-control required" id="title" name="title">
                                          <input type="hidden" name="doc_type" value="0">
                                          <input type="hidden" name="type" value="0">
                                     </div>

                                     <div class="form-group">
                                          <label for="form-field-select-1">Title Status </label>
                                          <select id="title_status" class="form-control required" name="title_status">
                                               <option value="1">Enable</option>                                                                                
                                               <option value="0">Disable</option>
                                          </select>   
                                     </div>

                                     <div class="form-group">
                                          <label for="form-field-select-1">Parent folder</label>
                                          <select id="parent" class="form-control" name="parent">
                                               <option value="0">Main</option>  
                                               <?php
                                                    foreach ($docfolder as $key=>$value)
                                                    {
                                               ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                                               <?php
                                                    }
                                               ?>
                                          </select>                                                                
                                     </div>
                                     <div class="form-group">
                                          <label>Upload Folder Icon Image</label>
                                          <div class="fileupload fileupload-new required" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="userfile">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>
                                     </div>

                                     <div class="form-group">
                                          <label>Upload Background Image</label>
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="coverimages">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>
                                     </div>
                                </div>
                                
                                <div class="col-md-12">
                                     <div id="option_container">
                                               <div class="form-group well height-137">
                                                    <label for="form-field-1" class="col-md-3">
                                                        File 
                                                    </label>
                                                    <label for="form-field-select-1" class="col-md-3">
                                                         File View
                                                    </label>
                                                    <label for="form-field-select-1" class="col-md-3">
                                                         File Type
                                                    </label>
                                                    <label for="form-field-select-1" class="col-md-3">
                                                         Custom Icon <span class="symbol required"></span>
                                                    </label>
                                                    <div style="height: 65px;">
                                                       <div data-provides="fileupload" class="fileupload fileupload-new col-md-3">
                                                             <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                                  <input type="file" name="document[]">
                                                             </span>
                                                             <span class="fileupload-preview"></span>
                                                             <a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
                                                                  &times;
                                                             </a>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                             <select id="form-field-select-1" class="form-control" name="document_view[]">
                                                                  <option value="0">Square</option>                                                                                
                                                                  <option value="1">Round</option>
                                                             </select>   
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                             <select id="doctype" class="form-control" onchange="changedoctype(this);" name="type[]" style="margin-bottom: 7px;">
                                                                  <option value="0">File</option>                                                                                
                                                                  <option value="1">Url</option>
                                                             </select>  
                                                             <div class="form-group" id="visible_link" style="display: none;">
                                                                  <input type="text" placeholder="Link" class="form-control" id="link" name="link[]" > 
                                                             </div>
                                                        </div>
                                                        
                                                        <div data-provides="fileupload" class="fileupload fileupload-new col-md-2 required">
                                                             <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                                  <input type="file" name="customeicon[]" class="required">
                                                             </span>
                                                             <span class="fileupload-preview"></span>
                                                             <a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
                                                                  &times;
                                                             </a>
                                                        </div>
                                                         <div class="form-group col-md-1">  
                                                             <a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a>
                                                         </div>
                                                    </div>
                                           </div>
                                      </div>
                                     <div class="form-group add_more" id="display_addmore" style="float: none !important;">
                                          <label for="form-field-1">
                                              Add More 
                                          </label>
                                          <div>
                                               <a class="btn btn-blue" href="javascript: void(0);" onclick="addmoreoption()" ><i class="fa fa-plus fa fa-white"></i></a>
                                          </div>
                                      </div>
                                </div>
                                <div class="col-md-6">     
                                     <div class="row">
                                          <div class="col-md-4">
                                               <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                          </div>
                                     </div>
                                </div>          
                           </div>
                      </form>
                    </div>
					
                  </div>
               </div>
          </div>
          <!-- end: TEXT FIELDS PANEL -->
     </div>
</div>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
     function addmoreoption()
     {
          var html='<div class="form-group well height-137"><label for="form-field-1" class="col-md-3">File <span class="symbol"></span></label><label for="form-field-select-1" class="col-md-3">File View</label><label for="form-field-select-1" class="col-md-3">File Type</label><label for="form-field-select-1" class="col-md-3">Custom Icon</label><div style="height: 65px;"><div data-provides="fileupload" class="fileupload fileupload-new col-md-3"><span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="document[]"></span><span class="fileupload-preview"></span><a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">&times;</a></div><div class="form-group col-md-3"><select id="form-field-select-1" class="form-control" name="document_view[]"><option value="0">Square</option><option value="1">Round</option></select></div><div class="form-group col-md-3"><select id="doctype" class="form-control" onchange="changedoctype(this);" name="type[]" style="margin-bottom: 7px;"><option value="0">File</option><option value="1">Url</option></select><div class="form-group" id="visible_link" style="display: none;"><input type="text" placeholder="Link" class="form-control" id="link" name="link[]" ></div></div><div data-provides="fileupload" class="fileupload fileupload-new col-md-2 required"><span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="customeicon[]" ></span><span class="fileupload-preview"></span><a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">&times;</a></div><div class="form-group col-md-1"><a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a></div></div></div>';
          
          jQuery("#option_container").append(html);
     }
     
     function removeoption(e)
     {
          if(jQuery("#option_container div.well").length>1)
          {
               jQuery(e).parent().parent().parent().remove();
          }
     }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
    
//    jQuery(".doctype").on('change', function() {
     function changedoctype(e)
     {
          if(jQuery(e).val()==1)
          {
               jQuery(e).parent().parent().css('height','108px');
               jQuery(e).parent().find('#link').addClass('required');
               jQuery(e).parent().find('#link').val('');
               jQuery(e).parent().find('#visible_link').css('display','block');
               jQuery(e).parent().find('#link').css('display','block');
          }
          else
          {
               jQuery(e).parent().find('#visible_link').css('display','none');
               jQuery(e).parent().find('#link').css('display','none');
               jQuery(e).parent().find('#link').removeClass('required');
               jQuery(e).parent().find('#link').val('');
               jQuery(e).parent().parent().css('height','65px');
          }
     }
     
    jQuery('#parent').change(function(){
          if(jQuery(this).val()==0)
          {
               jQuery('#category_content').css('display','block');
               jQuery('#category_id').addClass("required");
               
          }
          else
          {
               jQuery('#category_content').css('display','none');
               jQuery('#category_id').removeClass("required");
          }
     });
</script>