<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">

                <!-- <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>document/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Folder</a> -->
                <a style="top: 7px;right: 5px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Document/adddoc/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> File</a>
                
                <?php if ($this->session->flashdata('document_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Document <?php echo $this->session->flashdata('document_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('folder_data')) { ?>
                  <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('folder_data'); ?>
                  </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#document_list" data-toggle="tab">
                              File List
                            </a>
                        </li>
                        <li class="">
                          <a href="#folder_list" data-toggle="tab">
                            Folder List
                          </a>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="document_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Folder Name</th>
                                        <th>Type</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($Document);$i++) {  ?>
                                     <tr>
                                        <td>
                                          <a href="<?php echo base_url() ?>Document/editdoc/<?php echo $Document[$i]['Event_id'].'/'.$Document[$i]['id']; ?>"><?php echo ucfirst($Document[$i]['doctitle']); ?></a>
                                        </td>
                                        <td>
                                          <?php if(!empty($Document[$i]['parent_name'])) { ?>
                                          <span class="label label-sm label-success"><?php echo ucfirst($Document[$i]['parent_name']); ?></span>
                                          <?php } else { ?>
                                          <span class="label label-sm label-info">No Folder Available</span>
                                          <?php } ?>
                                        </td>
                                        <td>
                                            <span class="label label-sm
                                                <?php if($Document[$i]['type']=='1') {  ?> 
                                                     label-warning
                                                <?php  }  else  { ?> 
                                                     label-inverse
                                                <?php } ?>">
                                                <?php 
                                                if($Document[$i]['type']=='1')
                                                { 
                                                   echo "Video";
                                                }
                                                else 
                                                { 
                                                    echo "Document";
                                                } 
                                                ?>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Document/editdoc/<?php echo $Document[$i]['Event_id'].'/'.$Document[$i]['id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>         
                                            <a href="javascript:;" onclick="delete_document(<?php echo $Document[$i]['id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="folder_list">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Folder Name</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($folder_list as $key => $value) { ?> 
                            <tr>
                              <td><?php echo $key+1; ?></td>
                              <td><?php echo ucfirst($value['title']); ?></td>
                              <td>
                                <a href="javascript:void(0);" onclick="update_folder(<?php echo $value['id']; ?>,'<?php echo $value['title']; ?>','<?php echo $value['docicon']; ?>');" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="javascript:void(0);" onclick="delete_folder(<?php echo $value['id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                              </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>  
                      </div>  
                    </div>
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                             <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                             <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
                     
                    <div class="tab-pane fade" id="doc_coverimage">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Document/uploadcover/<?php echo $event_id; ?>" enctype="multipart/form-data">
                                <label>
                                   Image Upload
                               </label>
                               <div class="fileupload fileupload-new" data-provides="fileupload">
                               <div class="fileupload-new thumbnail">
                                   <?php if($coverimages != '') {   ?>  
                                       <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $coverimages; ?>" alt="">
                                       <?php  } else { ?>
                                       <img src="<?php echo base_url(); ?>assets/images/dcoument_defult.png" alt="">
                                       <?php  }  ?>
                               </div>
                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                   <?php if($coverimages != '') { ?>
                                       <div class="user-edit-image-buttons">
                                           <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_image();">
                                             <i class="fa fa-times"></i> Delete Image
                                           </a>
                                       </div>
                                   <?php }  ?>
                                   <div class="user-edit-image-buttons">
                                       <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                          <input type="file" name="converimage">
                                       </span>
                                       <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                           <i class="fa fa-times"></i> Remove
                                       </a>
                                   </div>
                               </div>
                                 <div class="form-group">
                                   <button type="submit" class="btn btn-primary">
                                        Upload
                                   </button>
                                 </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit_folder_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Folder</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form id="folder_name_form" enctype="multipart/form-data" action="<?php echo base_url().'Document/savefoldername/'.$this->uri->segment(3); ?>" method="post">
            <input type="hidden" name="folder_id" id="folder_id" value="">
            <div class="form-group">
              <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Folder Name</label>
              <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">  
                <input type="text" name="folder_name" id="folder_name" class="form-control required" placeholder="">
              </div>  
            </div>
            <div class="form-group">
              <label class="control-label col-sm-12 col-md-12 col-xs-12 col-lg-12">Upload Folder Icon Image</label>
              <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                <div id="fileupload_main_div" class="fileupload fileupload-new required" data-provides="fileupload">
                  <div class="fileupload-new thumbnail"></div>
                  <div class="fileupload-preview fileupload-exists thumbnail" id="exists_image_previews"></div>
                  <div class="user-edit-image-buttons">
                    <span class="btn btn-azure btn-file">
                      <span class="fileupload-new">
                        <i class="fa fa-picture"></i> Select image
                      </span>
                      <span class="fileupload-exists">
                        <i class="fa fa-picture"></i> Change
                      </span>
                      <input type="file" name="folder_icon_images" id="folder_icon_images">
                    </span>
                    <a href="#" class="btn btn-red" style="display: none;" id="remove_folder_icon_btn">
                      <i class="fa fa-times"></i> Delete Folder icon
                    </a>
                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                      <i class="fa fa-times"></i> Remove
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
              <button type="submit" id="save_folder_name_btn" class="btn btn-green btn-block">Save</button>
            </div>  
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
function delete_document(id)
{   
  if(confirm("Are you sure to delete this?"))
  {
    window.location.href ="<?php echo base_url(); ?>Document/delete/"+<?php echo $this->uri->segment(3); ?>+"/"+id
  }
}
function delete_user(id)
{
  if(confirm("Are you sure to delete this?"))
  {
    window.location.href ="<?php echo base_url(); ?>Doccategory/delete/"+id+"/<?php echo $event_id; ?>";
  }
}
function delete_image()
{
  if(confirm("Are you sure to delete this image?"))
  {
    window.location.href='<?php echo base_url(); ?>Document/deletecover/<?php echo $event_id; ?>';
  }
}
$('#remove_folder_icon_btn').click(function(){
  if(confirm("Are you sure to delete this image?"))
  {
    window.location.href='<?php echo base_url(); ?>Document/delete_folder_icon/<?php echo $event_id.'/'; ?>'+$('#folder_id').val();
  }
});
function update_folder(fid,fnm,finm)
{
  $('#folder_id').val(fid);
  $('#folder_name').val(fnm);
  if(finm!="")
  {
    $('#fileupload_main_div').removeClass('fileupload-new').addClass('fileupload-exists');
    var imgurl="<?php echo base_url().'assets/user_documents/' ?>"+finm;
    $('#exists_image_previews').html("<img src='"+imgurl+"'/>");
    $('#remove_folder_icon_btn').show();
  }
  else
  {
    $('#fileupload_main_div').removeClass('fileupload-exists').addClass('fileupload-new');
    $('#exists_image_previews').html('');
    $('#remove_folder_icon_btn').hide();
  }
  $('#edit_folder_modal').modal('show');
}
function delete_folder(fid)
{
  if(confirm("Are you sure to delete this?"))
  {
    window.location.href ="<?php echo base_url(); ?>Document/delete_folder/<?php echo $event_id; ?>/"+fid;
  }
}
$(document).ready(function() {
  $('#folder_name_form').validate({
    errorElement: "label",
    errorClass: 'help-block',  
    rules: 
    {
      folder_name:{
        required: true,
      }
    },
    highlight: function (element) 
    {
      $(element).closest('.help-block').removeClass('valid');
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    }
  });
});    
</script>
<!-- end: PAGE CONTENT-->