<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>


<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Session</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>

            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                 
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Start Date <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group" style="padding-left: 1.7%;">
                                <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                <input type="text" name="Start_date" id="Start_date" contenteditable="false" value="<?php echo $agenda_list[0]['Start_date']; ?>" class="form-control">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Start Time <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-5">
                            <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker" contenteditable="false">
                                <input type="text" id="Start_time" name="Start_time" class="form-control time-picker" value="<?php echo $agenda_list[0]['Start_time']; ?>">
                                <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            End Date <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group" style="padding-left: 1.7%;">
                                <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                <input type="text" name="End_date" id="End_date" contenteditable="false" value="<?php echo $agenda_list[0]['End_date']; ?>" class="form-control">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            End Time <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-5">
                            <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker" contenteditable="false">
                                <input type="text" id="End_time" name="End_time" class="form-control time-picker" value="<?php echo $agenda_list[0]['End_time']; ?>">
                                <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Session Heading <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Session Heading" id="Heading" value="<?php echo $agenda_list[0]['Heading'] ?>" name="Heading" class="form-control name_group required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Session Type <span class="symbol required"></span>
                        </label>
                        <div class="col-md-9" id="select_box_type">
                            <select id="cmd_select_box_type" class="form-control" name="Types">
                                <option value=''>Select Type</option>
                                <option value="Introduction" <?php if($agenda_list[0]['Types'] == 'Introduction') { ?> selected <?php } ?>>Introduction</option>
                                <option value="Break" <?php if($agenda_list[0]['Types'] == 'Break') { ?> selected <?php } ?>>Break</option>
                                <option value="Networking" <?php if($agenda_list[0]['Types'] == 'Networking') { ?> selected <?php } ?>>Networking</option>
                                <option value="Main Session" <?php if($agenda_list[0]['Types'] == 'Main Session') { ?> selected <?php } ?>>Main Session</option>
                                <option value="Breakout Session" <?php if($agenda_list[0]['Types'] == 'Breakout Session') { ?> selected <?php } ?>>Breakout Session</option>
                                <option value="Focus Group" <?php if($agenda_list[0]['Types'] == 'Focus Group') { ?> selected <?php } ?>>Focus Group</option>
                                <option value="Other" <?php if($agenda_list[0]['Types'] == 'Other') { ?> selected <?php } ?>>Other</option>
                            </select>
                             <?php if($agenda_list[0]['Types'] == 'Other') { ?>
                             <input type="text" value="<?php echo $agenda_list[0]['other_types']; ?>" placeholder="Session Type" id="other_types" name="other_types" class="form-control name_group" style="display:block;">
                             <?php } else {  ?>
                             <input type="text" placeholder="Session Type" id="other_types" name="other_types" class="form-control name_group" style="display:none;">
                             <?php } ?>
                         </div>
                    </div>  

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Short Description 
                        </label>
                        <div class="col-md-9">
                            <textarea maxlength="100" id="form-field-23" class="form-control limited" name="short_desc"><?php echo $agenda_list[0]['short_desc']; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Description 
                        </label>
                        <div class="col-md-9">
                           <textarea class="summernote form-control" name="description"><?php echo $agenda_list[0]['description']; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-select-4">
                            Select Location 
                        </label>
                        <div class="col-md-9">
                            <select id="form-field-select-4" class="form-control" name="Address_map">
                                <?php 
                                    echo"<option value=''>Select Location</option>";
                                    foreach ($map_list as $k => $v) { ?>
                                    <option value="<?php echo $v['Id']; ?>" <?php if($agenda_list[0]['Address_map'] == $v['Id']) { ?> selected <?php } ?>><?php echo $v["Map_title"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="areadiv" style="display:none;">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Area
                                    </label>
                                  <div class="col-sm-9">
                                     <input id="area" name="area" value="" class="form-control" type="text" placeholder="Area"/>
                                  </div>
                               </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Session Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="1" <?php if ($agenda_list[0]['Agenda_status'] == '1') echo ' checked="checked"'; ?> name="Agenda_status">
                                Active
                            </label>
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="0" <?php if ($agenda_list[0]['Agenda_status'] == '0') echo ' checked="checked"'; ?> name="Agenda_status">
                                Inactive
                            </label>    
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-select-4">
                            Speakers
                        </label>
                        <div class="col-md-9">
                            <select id="form-field-select-4" onClick="openpop();" class="form-control old_speakers" name="Speaker_id[]" multiple="true">
                                <?php 
                                    $agenda_speaker=explode(',',$agenda_list[0]['Speaker_id']);

                                    echo "<option>Select Speakers</option>";
                                    foreach ($speakers as $key => $value) 
                                    { 
                                    ?>
                                        <option value="<?php echo $value['uid']; ?>" <?php if(in_array($value['uid'], $agenda_speaker)) { ?> selected <?php } ?>><?php echo $value["Firstname"].' '.$value["Lastname"]; ?></option>
                                    <?php
                                    }
                                    echo"<option style='background-color: #ccc !important;' value='New'>Add New Speaker</option>";
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-select-4">
                           Presentation Slides
                        </label>
                        <div class="col-md-9">
                            <select id="form-field-select-4" onchange="newpre();" class="form-control new_pre"class="form-control" name="presentation_id">
                                <?php 
                                    echo"<option value=''>Select Presentation Slides</option>";
                                    foreach ($presentation_list as $k => $v) { ?>
                                    <option value="<?php echo $v['Id']; ?>" <?php if($agenda_list[0]['presentation_id'] == $v['Id']) { ?> selected <?php } ?>><?php echo $v["Heading"]; ?></option>
                                <?php } 
                                echo"<option style='background-color: #ccc !important;' value='New'>Add New Presentation</option>";

                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-select-4">
                           Related Document
                        </label>
                        <div class="col-md-9">
                            <select id="form-field-select-4" onchange="newdoc();" class="form-control new_doc" name="doc_id">
                                <?php 
                                    echo"<option value=''>Select Related Document</option>";
                                    foreach ($doc_list as $k => $v) { ?>
                                    <option value="<?php echo $v['id']; ?>" <?php if($agenda_list[0]['document_id'] == $v['id']) { ?> selected <?php } ?>><?php echo $v["title"]; ?></option>
                                <?php } 
                                echo"<option style='background-color: #ccc !important;' value='New'>Add New Documents</option>";
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<div id="full_popup">
    <div id="blanket" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a>
        <form role="form" method="post" class="ajax-form" id="form2">
        <div class="row">
            <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
            <div class="errorHandler alert alert-success no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> Updated Successfully.
            </div>
            <?php } ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">First Name </span>
                    </label>
                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name </span>
                    </label>
                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
                </div>
                <div class="form-group">
                    <label class="control-label">Email </span>
                    </label>
                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
                </div>
                <div class="form-group">
                        <label class="control-label">Password </label>
                        <input type="password" placeholder="Password" class="form-control required" id="password" name="password">
                </div>
                <div class="col-md-3">
                        <button id="comment" class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>                     
        </div>
        </form>
    </div> 
</div>


<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
.close_popup
{
    position: absolute;
    top: -10px;
    right: -10px;
}
#popUpDiv {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}

.fileupload-new.thumbnail img
{
 height: 140px !important;
}

</style>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">

$('#comment').click(function() {
    var form_data = {
        Firstname : $('#Firstname').val(),
        Lastname : $('#Lastname').val(),
        Email : $('#email').val(),
        Password : $('#password').val()
    };

    if($('#Firstname').val() != '' && $('#Lastname').val() && $('#email').val() != '' && $('#password').val() != '')
    {
        $.ajax({
            url: "<?php echo base_url().'speaker/add_speakers/'.$this->uri->segment(3); ?>",
            type: 'POST',
            data: form_data,
            success: function(data){
                    $('#full_popup').css('display','none');
                    $('.old_speakers').html(data); 
                    $('#Firstname').val('');
                    $('#Lastname').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('.old_speakers').on('change',function(){
                        if($('.old_speakers').val()=='New')
                {
                    $("#full_popup").show();
                    popup('popUpDiv');
                }else{
                    $("#full_popup").hide();
                }
                    });
               }
        });
    }
});


function openpop()
{
    if($(".old_speakers").val()=='New')
    {
       //popup('popUpDiv');
       popup('popUpDiv');
    }
}
function newdoc()
{
    if($(".new_doc").val()=='New')
    {
        var url="<?php echo base_url();?>document/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}
function newpre()
{
    if($(".new_pre").val()=='New')
    {
        var url="<?php echo base_url();?>presentation/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}

</script>

<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
             $("#cmd_select_box_type").change(function()
            {
                if($(this).val()=="Other")
                {
                     $('#other_types').css('display','block');
                }
                else
                {
                     $('#other_types').css('display','none');
                }
          });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } ?>
<!-- end: PAGE CONTENT-->