<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#agenda_list" data-toggle="tab">
                                Session Comments
                            </a>
                        </li>					 
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="agenda_list">
                        <div class="row" style="margin: 10px;">
                            <h3><?=$agenda_data['Heading']?></h3>
                        </div>
                        <div class="table-responsive custom_checkbox_table">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_11">
                                <thead>
                                    <tr>
                                        <th>Comments</th>
                                        <th>Comments Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($agenda_data['session_comments'] as $key => $value):?>
                                        <tr>
                                            <td><?=$value['comments']?></td>
                                            <td><?=$value['created_date']?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
         $("#sample_11").DataTable({
             bStateSave: true,
            bDestroy: true,
            fnStateSave: function(settings,data) {
                  localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
                },
            fnStateLoad: function(settings) {
                return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
                }
        })
    })
   
     $('#agenda_list').dataTable( {
        "sDom": "<'row-fluid'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?php echo base_url().'Exhibitor/ajax_manage/'.$this->uri->segment(3); ?>"
    });
</script>
<!-- end: PAGE CONTENT-->