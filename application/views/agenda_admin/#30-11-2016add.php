<?php $acc_name=$this->session->userdata('acc_name'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/agenda_js/csspopup.js"></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="panel-body" style="padding: 0px;">

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#agenda_list" data-toggle="tab">
                                Add Session
                            </a>
                        </li>
                        <li class="">
                            <a href="#existing_agenda_add" data-toggle="tab">
                                Duplicate Another Session
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="agenda_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <!--<link rel="stylesheet" type="text/css" media="screen" href="https://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">-->
                                <div class="form-group">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Start Date <span class="symbol required" id="showdatetime_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="input-group" style="padding-left: 1.7%;">
                                            <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                            <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                            <input type="text" name="Start_date" id="Start_date" contenteditable="false" class="form-control name_group required">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                        </div>
                                    </div>
                                    <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                                </div>
                                <div class="form-group">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Start Time <span class="symbol required" id="showdatetime_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker">
                                            <input type="text" id="Start_time" name="Start_time" class="form-control time-picker">
                                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                                </div>

                                <div class="form-group">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        End Date <span class="symbol required" id="showdate_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="input-group" style="padding-left: 1.7%;">
                                            <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                            <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                            <input type="text" name="End_date" id="End_date" contenteditable="false" class="form-control">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                        </div>
                                    </div>
                                    <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                                </div>

                                <div class="form-group">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        End Time <span class="symbol required" id="showtime_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker">
                                            <input type="text" id="End_time" name="End_time" class="form-control time-picker">
                                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Session Heading <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="Session Heading" id="Heading" name="Heading" class="form-control name_group required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Session Type <span class="symbol required"></span>
                                    </label>
                                    <div class="col-md-9" id="select_box_type">
                                        <select id="cmd_select_box_type" onchange="openpopforsessiontype();" class="form-control" name="Types">
                                            <option value="">Select Type</option>
                                            <option value="Introduction">Introduction</option>
                                            <option value="Break">Break</option>
                                            <option value="Networking">Networking</option>
                                            <option value="Main Session">Main Session</option>
                                            <option value="Breakout Session">Breakout Session</option>
                                            <option value="Focus Group">Focus Group</option>
                                            <option value="Other">Other</option>
                                            <option style="background-color: #ccc !important;" value="New">Add a New Session Type</option>
                                        </select>
                                         <input type="text" placeholder="Session Type" id="other_types" name="other_types" class="form-control name_group" style="display:none;">
                                     </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Short Description 
                                    </label>
                                    <div class="col-md-9">
                                        <textarea maxlength="100" id="form-field-23" class="form-control limited" name="short_desc"></textarea>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Description 
                                    </label>
                                    <div class="col-md-9">
                                       <textarea class="summernote form-control" name="description"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Select Location 
                                    </label>
                                    <div class="col-md-9">
                                        <select id="form-field-select-4" class="form-control" name="Address_map">
                                            <?php 
                                                echo"<option value=''>Select Location</option>";
                                                foreach ($map_list as $key => $value) 
                                                {
                                                    echo'<option value="'.$value['Id'].'">'.$value["Map_title"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="areadiv" style="display:none;">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Area
                                    </label>
                                  <div class="col-sm-9">
                                     <input id="area" name="area" value="" class="form-control" type="text" placeholder="Area"/>
                                  </div>
                               </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Session Status <span class="symbol required"></label>
                                    <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1" checked="checked" name="Agenda_status">
                                            Active
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0" name="Agenda_status">
                                            Inactive
                                        </label>    
                                    </div>
                                </div>
                    
                                 <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Speakers
                                    </label>
                                    <div class="col-md-9">
                                    
                                        <select id="form-field-select-4" onchange="openpop();" class="form-control old_speakers" name="Speaker_id[]" multiple="true">
                                            <?php 
                                                echo"<option value=''>Select Speakers</option>";
                                                foreach ($speakers as $key => $value) 
                                                {
                                                    echo'<option value="'.$value['uid'].'">'.$value["Firstname"].' '.$value["Lastname"].'</option>';
                                                }
                                                echo"<option style='background-color: #ccc !important;' value='New'>Add New Speaker</option>";
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Presentation Slides
                                    </label>
                                    <div class="col-md-9">
                                    
                                        <select id="form-field-select-4"  onchange="newpre();" class="form-control new_pre" name="presentation_id">
                                            <?php 
                                                echo"<option value=''>Select Presentation Slides</option>";
                                                foreach ($presentation_list as $key => $value) 
                                                {
                                                    echo'<option value="'.$value['Id'].'">'.$value["Heading"].'</option>';
                                                }
                                                echo"<option style='background-color: #ccc !important;' value='New'>Add New Presentation</option>";
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Related Document
                                    </label>
                                    <div class="col-md-9">
                                        
                                        <select id="form-field-select-4"  onchange="newdoc();" class="form-control new_doc" name="doc_id">
                                            <?php 
                                                echo"<option value=''>Select Related Document</option>";
                                                foreach ($doc_list as $key => $value) 
                                                {
                                                    echo'<option value="'.$value['id'].'">'.$value["title"].'</option>';
                                                }
                                                echo"<option style='background-color: #ccc !important;' value='New'>Add New Documents</option>";
                                              
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Maximum People
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" name="Maximum_People" id="Maximum_People" class="form-control name_group"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Show number of places remaining
                                    </label>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="show_places_remaining" id="show_places_remaining" class="" value="1"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Activate Checking In
                                    </label>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="show_checking_in" id="show_checking_in" class="" value="1"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Time to allow Check In
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="checking_datetime" id="checking_datetime" contenteditable="false" class="form-control datetimepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Activate Session Ratings
                                    </label>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="show_rating" id="show_rating" class="" value="1"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Speaker Name
                                    </label>
                                  <div class="col-sm-9">
                                     <input id="custom_speaker_name" name="custom_speaker_name" value="" class="form-control" type="text" placeholder="Enter Speaker Name"/>
                                  </div>
                               </div>
                               <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Location
                                    </label>
                                  <div class="col-sm-9">
                                     <input id="custom_location" name="custom_location" value="" class="form-control" type="text" placeholder="Enter Location"/>
                                  </div>
                               </div>
                               <div class="form-group">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Allow Clashing
                                    </label>
                                    <div class="col-md-1">
                                        <input type="checkbox" name="allow_clashing" id="allow_clashing" class="" value="1"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                    </label>
                                    <div class="col-md-4">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <div class="tab-pane fade" id="existing_agenda_add">
                        <form role="form" name="existing_form" method="post" class="form-horizontal" id="existing_form" action="<?php echo base_url().'agenda_admin/add_existing_agenda/'.$this->uri->segment(3).'/'.$this->uri->segment(4); ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3" style="margin-bottom:20px;">
                                        <label>Select Existing Session<span class="symbol required"></span></label>
                                    </div>
                                    <div class="col-md-8" style="margin-bottom:20px;">
                                        <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="main_select" name="agenda_id">
                                                <option value="">Select Session</option>
                                                <?php foreach ($agenda_list as $key => $value) { ?>
                                                    <option value="<?php echo $value['Id']; ?>"><?php echo $value['Heading']; ?></option>
                                                <?php }  ?>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                            </div>
                                        </div>
                                    </div>      
                                </div>                  
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<div id="full_popup1">
    <div id="blanket1" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv1" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv1&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a><form>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Session Type</span>
                </label>
                <input type="text" placeholder="Session Type" class="form-control required" id="Session_type_testbox" name="Session_type_testbox">
                <span id="Session_type_testbox_error" style="color:red;display:none;" for="Session_type_testbox" class="help-block">This field is required.</span>
            </div>
            <div class="col-md-3">
                <button id="session_type_btn" class="btn btn-green btn-block" type="button">Add</button>
            </div>
        </div>
    </div>
    </div>
</div> 
<div id="full_popup">
    <div id="blanket" style="display: none; height: 2000px;"></div>
    <div id="popUpDiv" style="display: none; top: 191px; left: 505.5px;">
        <a href="javascript:void(0)" onclick="popup(&#39;popUpDiv&#39;)"><img class="close_popup" src="<?php echo base_url(); ?>/assets/images/close_popup.png"></a>
        <form role="form" method="post" class="ajax-form" id="form2">
        <div class="row">
            <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
            <div class="errorHandler alert alert-success no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> Updated Successfully.
            </div>
            <?php } ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">First Name </span>
                    </label>
                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
                    <span id="firstnamespan" style="color:red;display:none;" for="Firstname" class="help-block">This field is required.</span>
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name </span>
                    </label>
                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
                    <span id="Lastnamespan" style="color:red;display:none;" for="Lastname" class="help-block">This field is required.</span>
                </div>
                <div class="form-group">
                    <label class="control-label">Email </span>
                    </label>
                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
                    <span id="emailspan" style="color:red;display:none;" for="email" class="help-block">This field is required.</span>
                </div>
                <div class="form-group">
                        <label class="control-label">Password </label>
                        <input type="password" placeholder="Password" class="form-control required" id="password" name="password">
                        <span id="passwordspan" style="color:red;display:none;" for="password" class="help-block">This field is required.</span>
                </div>
                <div class="col-md-3">
                        <button id="comment" class="btn btn-green btn-block" type="button">Add <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>                     
        </div>
        </form>
    </div> 
</div>

<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
.close_popup
{
    position: absolute;
    top: -10px;
    right: -10px;
}
#popUpDiv {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}
#popUpDiv .form-group label{color:#333;}
#popUpDiv .col-md-3{margin: 0; padding: 0;}
#blanket1 {
   background-color:#111;
   opacity: 0.65;
   position:absolute;
   z-index: 10;
   top:0px;
   left:0px;
   width:100%;
   height: 100% !important;
}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}
#popUpDiv1 {
    position:fixed;
    background-color:#BDBDBD;
    width:500px;
    height:auto;
    z-index: 9002;
    padding: 15px;
    left: 42% !important;
    top: 25% !important;
    border: 5px solid #333;
}
#popUpDiv1 .form-group label{color:#333;}
#popUpDiv1 .col-md-3{margin: 0; padding: 0;}
</style>

<script type="text/javascript">
$('#comment').click(function() {
    var form_data = {
        Firstname : $('#Firstname').val(),
        Lastname : $('#Lastname').val(),
        Email : $('#email').val(),
        Password : $('#password').val()
    };

    if($('#Firstname').val() != '' && $('#Lastname').val() && $('#email').val() != '' && $('#password').val() != '')
    {
        $.ajax({
            url: "<?php echo base_url().'speaker/add_speakers/'.$this->uri->segment(3); ?>",
            type: 'POST',
            data: form_data,
            success: function(data){
                    $('#full_popup').css('display','none');
                    $('.old_speakers').html(data); 
                    $('#Firstname').val('');
                    $('#Lastname').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('.old_speakers').on('change',function(){
                        if($('.old_speakers').val()=='New')
                {
                    $("#full_popup").show();
                    popup('popUpDiv');
                    $("#blanket").show();
                    $("#popUpDiv").show();
                }else{
                    $("#full_popup").hide();
                }
                    });
               }
        });
    }
    else
    {
        $('#firstnamespan').show();
        $('#Lastnamespan').show();
        $('#emailspan').show();
        $('#passwordspan').show();
    }
});
function openpopforsessiontype()
{
  if($("#cmd_select_box_type").val()=='New')
  {
     popup('popUpDiv1');
  }
}
$('#session_type_btn').click(function(){
    var sessval=$.trim($('#Session_type_testbox').val());
    if(sessval!=""){
        $('#cmd_select_box_type').append("<option selected='selected' value='" + sessval+ "'>" + sessval + "</option>");
        popup('popUpDiv1');
    }
    else
    {
        $('#Session_type_testbox_error').show();
    }    

});
function openpop()
{
    if($(".old_speakers").val()=='New')
    {
    
       popup('popUpDiv');
    }
}
function newdoc()
{
    if($(".new_doc").val()=='New')
    {
        var url="<?php echo base_url();?>document/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}
function newpre()
{
    if($(".new_pre").val()=='New')
    {
        var url="<?php echo base_url();?>presentation_admin/index/<?php echo $this->uri->segment(3);?>";
        window.open(
 url,
  '_blank' // <- This is what makes it open in a new window.
);
        
    }
}

</script>

<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).ready(function(){
        $('.datetimepicker').datetimepicker({
            formatTime:'H:i',
            formatDate:'m/d/Y',
            step:5
        });
    });
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
            
          $("#cmd_select_box_type").change(function()
          {
                if($(this).val()=="Other")
                {
                     $('#other_types').css('display','block');
                }
                else
                {
                     $('#other_types').css('display','none');
                }
          });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $ti = $("#Start_date").val();
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } ?>
<!-- end: PAGE CONTENT-->