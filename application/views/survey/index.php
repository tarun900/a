<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery.datetimepicker.css"/>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
            <div class="panel-body" style="padding: 0px;">
                <?php if(!empty($sid)){ ?>
                <a style="top: 7px;" class="btn btn-primary list_page_btn survey_btn pull-right"  href="<?php echo base_url(); ?>Survey/exprot_surveys/<?php echo $this->uri->segment(3).'/'.$sid; ?>"> Export Survey </a>
                <a style="top: 7px;margin-right: 12%;" class="btn btn-primary list_page_btn survey_btn" href="<?php echo base_url(); ?>Survey/add/<?php echo $this->uri->segment(3).'/'.$sid; ?>"><i class="fa fa-plus"></i> Add new Question</a>
                 <?php if($survey_screens[0]['welcome_data']=='') { ?>
                <a style="top: 7px;display: none;" class="btn btn-primary list_page_btn welcome_btn"  href="<?php echo base_url(); ?>Survey/add_welcome_screen/<?php echo $this->uri->segment(3).'/'.$sid; ?>"><i class="fa fa-plus"></i>Add Welcome </a>
                <?php } ?>
                <?php if($survey_screens[0]['thanku_data']=='') { ?>
                 <a style="top: 7px;display: none;" class="btn btn-primary list_page_btn thank_btn"  href="<?php echo base_url(); ?>Survey/add_thanks_screen/<?php echo $this->uri->segment(3).'/'.$sid; ?>"><i class="fa fa-plus"></i>Add Thank you</a>
                 <?php } ?>
                <?php } if($this->session->flashdata('survey_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Survey <?php echo $this->session->flashdata('survey_data'); ?> Successfully.
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('survey_wl_screen_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Survey Welcome Screen <?php echo $this->session->flashdata('survey_wl_screen_data'); ?> Successfully.
                </div>
                <?php } ?>
                 <?php if($this->session->flashdata('survey_thk_screen_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Survey Thankyou Screen <?php echo $this->session->flashdata('survey_thk_screen_data'); ?> Successfully.
                </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#survey_list" data-toggle="tab">
                                Questions List
                            </a>
                        </li>

                        <li class="">
                            <a href="#welcome_screen" data-toggle="tab">
                                Welcome Screen
                            </a>
                        </li>

                        <li class="">
                            <a href="#thank_you_screen" data-toggle="tab">
                                Thank You Screen
                            </a>
                        </li>
                        <!-- <li class="">
                            <a href="#users_list" data-toggle="tab">
                                Users List
                            </a>
                        </li> -->
                        <li class="">
                            <a href="#r_date" data-toggle="tab">
                               Release Time
                            </a>
                        </li>
                        <li class="">
                            <a href="#survey_chart" data-toggle="tab">
                                Responses
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="survey_list">
                        <div class="row agenda_row">
                            <div id="show_survey_error_msg" class="alert alert-danger" <?php if (!$this->session->flashdata('survey_error')) { ?> style="display:none;" <?php } ?>>
                                <?php echo $this->session->flashdata('survey_error'); ?>
                            </div>
                            <?php if ($this->session->flashdata('survey_success')) { ?>
                                <div class="errorHandler alert alert-success no-display" style="display: block;">
                                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('survey_success'); ?>
                                </div>
                            <?php } ?>
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url().'Survey/add_survey/'.$this->uri->segment(3); ?>">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="form-field-1" style="text-align:left;padding-top: 1.2%;">Surveys Name</label>
                                    <div class="col-md-4">
                                        <input type="hidden" name="survey_id" id="survey_id" value="<?php echo $sid; ?>">
                                        <input type="text" name="survey_name" onblur="checksurveycategoryname();" id="survey_name" value="<?php echo $survey_category[0]['survey_name']; ?>" class="form-control" placeholder="Surveys Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-green" name="save_survey" id="save_survey">Save <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Hide</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($Surveys);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() ?>Survey/edit/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id'].'/'.$sid; ?>"><?php echo $Surveys[$i]['Question']; ?></a></td>
                                        <td>
                                            <input type="checkbox" <?php if($Surveys[$i]['show_question']=='0'){ ?> checked="checked" <?php } ?> onchange='hidequestion("<?php echo $Surveys[$i]['Id']; ?>");' name="show_question" value="<?php echo $Surveys[$i]['Id']; ?>" id="show_question_<?php echo $Surveys[$i]['Id'];?>" class="">
                                           <label for="show_question_<?php echo $Surveys[$i]['Id'];?>"></label>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Survey/edit/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id'].'/'.$sid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url() ?>Survey/view/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id'].'/'.$sid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="View"><i class="fa fa-share"></i></a>
                                            <a href="javascript:;" onclick="delete_survey(<?php echo $Surveys[$i]['Id']; ?>,<?php echo $sid ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <a href="<?php echo base_url(); ?>Survey/export/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id']; ?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export
                                            </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="welcome_screen">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($survey_screens[0]['welcome_data'])) { ?>
                                    <tr>
                                        <td>Welcome Screen Content</td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Survey/welcome_edit_screen/<?php echo $survey_screens[0]['Event_id'].'/'.$survey_screens[0]['Id'].'/'.$sid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_welcome(<?php echo $survey_screens[0]['Id']; ?>,<?php echo $sid ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="thank_you_screen">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>Content</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($survey_screens[0]['thanku_data'])) { ?>
                                    <tr>
                                        <td>Thank you Screen Content</td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Survey/thanku_edit_screen/<?php echo $survey_screens[0]['Event_id'].'/'.$survey_screens[0]['Id'].'/'.$sid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_thankyou(<?php echo $survey_screens[0]['Id']; ?>,<?php echo $sid ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="r_date">
                        <form action="<?php echo base_url().'Survey/add_relese_date/'.$this->uri->segment(3); ?>" method="post">
                            <div class="row">
                                <div class="form-group">
                                     <input type="hidden" name="survey_id" id="survey_id" value="<?php echo $sid; ?>">
                                     <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Release Time <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="relese_datetime" id="relese_datetime" value="<?php echo $survey_category[0]['relese_datetime'] ?>" class="form-control datetimepicker">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-green" name="save_survey" id="save_survey">Save <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade in" id="users_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($survey_user);$i++) { ?>
                                    <tr>
                                        <td><?php echo $survey_user[$i]['Firstname']." ".$survey_user[$i]['Lastname']; ?></td>
                                        <td><?php echo $survey_user[$i]['Email']; ?></td>
                                        <td>
                                            
                                            <a href="<?php echo base_url(); ?>Survey/user_export/<?php echo $this->uri->segment(3).'/'.$survey_user[$i]['Id']; ?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">
                                            Export
                                            </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <style type="text/css">
                    </style>
                    <div class="tab-pane fade" id="survey_chart">
                   
                    <?php 

                        $final_Array = array();

                        foreach ($survey_answer_chart as $key => $value) 
                        {
                            $final_Array[$value['Question']][$value['panswer']][] = array(
                                                                    'id' => $value['puserid'],
                                                                    'Firstname' => $value['Firstname'],
                                                                    'Lastname' => $value['Lastname'],
                                                                    'Email' => $value['Email']
                                                                );              

                        }

                        foreach($final_Array as $key => $value)
                        {
                            foreach ($value as $i => $j) 
                            {
                                $final_Array[$key][$i] = sizeof($j);
                            }

                        }
                        $arryvalues=array();
                        foreach($final_Array as $key => $item) { ?>
                            
                            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                               <div id='piechart<?php echo $key; ?>'></div>

                                <script type="text/javascript">
                                    google.load("visualization", "1", {packages:["corechart"]});
                                    google.setOnLoadCallback(drawChart);
                                      function drawChart() {                                       

                                        var data = google.visualization.arrayToDataTable
                                        ([
                                              ['Options', 'No. of users'],  
                                              <?php foreach ($item as $a => $b) { ?>
                                    
                                             ['<?php echo $a; ?>', <?php echo $b; ?>],
                                            
                                             <?php } ?>
                                        ]);

                                        var options = {
                                          title: '<?php echo 'Question: '. $key; ?>','width':900,'height':500,
                                          pieSliceTextStyle: 
                                          {
                                            color: 'black',
                                          },
                                          legend: 'right',
                                        };

                                        var chart = new 
                                        google.visualization.PieChart(document.getElementById('piechart<?php echo $key;?>'));
                                        chart.draw(data, options);
                                      }
                                </script>
                                <?php
                         }
                         ?>
                    </div>
                    </script>
                    <div id="show_respons"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
    $('#save_survey').click(function(){
        if($.trim($('#survey_name').val())!="")
        {
            $('#show_survey_error_msg').html('');
            $('#show_survey_error_msg').hide();
            $('#save_survey').attr('type','submit');
        }
        else
        {
            $('#show_survey_error_msg').html("Plase Enter Surveys Name");
            $('#show_survey_error_msg').show();
        }
    });
    function checksurveycategoryname()
    {
        if($.trim($('#survey_name').val())!="")
        {
            $('#show_survey_error_msg').html('');
            $('#show_survey_error_msg').hide();
            $('#save_survey').removeAttr('disabled');
            $.ajax({
                url:"<?php echo base_url().'Survey/check_survey_cateory/'.$this->uri->segment(3); ?>",
                data:"survey_name="+$.trim($('#survey_name').val())+"&sid="+$.trim($('#survey_id').val()),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#show_survey_error_msg').html(values[1]);
                        $('#show_survey_error_msg').show();
                        $('#save_survey').attr('disabled','disabled');
                    }
                    else
                    {
                        $('#show_survey_error_msg').html(values[1]);
                        $('#show_survey_error_msg').hide();
                        $('#save_survey').removeAttr('disabled');
                    }
                }
            });
        }
        else
        {
            $('#show_survey_error_msg').html("Plase Enter Surveys Name");
            $('#show_survey_error_msg').show();
            $('#save_survey').attr('disabled','disabled');
        }
    }
    function hidequestion(qid)
    {
        $.ajax({
            url:"<?php echo base_url(); ?>Survey/change_hide_question_status/<?php echo $this->uri->segment(3); ?>/"+qid,
            success:function(result)
            {
              if($.trim(result)>0)
              {
                var shortCutFunction ='success';
                if($("#show_question_"+qid).is(':checked'))
                {
                  var title = 'Hide Successfully';
                  var msg = 'Question Hide Successfully.';
                }
                else
                {
                  var title = 'Show Successfully';
                  var msg = 'Question Show Successfully.'; 
                }
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-top-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
              }
            }
        });
    }
    function delete_survey(id,sid)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Survey/delete/"+<?php echo $test; ?>+"/"+id+"/"+sid
        }
    }
    function delete_welcome(id,sid)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Survey/delete_welcome/"+<?php echo $test; ?>+"/"+id+"/"+sid;
        }
    }
    function delete_thankyou(id,sid)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Survey/delete_thankyou/"+<?php echo $test; ?>+"/"+id+"/"+sid;
        }
    }
    function delete_surveyresponse(id,Event_id)
    {   
        <?php $test = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Survey/deleteresponse/"+<?php echo $test; ?>+"/"+id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.css">
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script>
    jQuery("#myTab2 li a").click(function(){
          var strTab = jQuery(this).prop("href");
          var arrTab = strTab.split("#");
          
          if(arrTab[1]=='survey_list')
          {
              jQuery(".survey_btn").show();
              jQuery(".welcome_btn").hide();
              jQuery(".thank_btn").hide();
          }
          else if(arrTab[1]=='welcome_screen')
          {
              jQuery(".welcome_btn").show();
              jQuery(".survey_btn").hide();
              jQuery(".thank_btn").hide();
          }
          else if(arrTab[1]=='thank_you_screen')
          {
              jQuery(".welcome_btn").hide();
              jQuery(".survey_btn").hide();
              jQuery(".thank_btn").show();
          }
          else
          {
              jQuery(".welcome_btn").hide();
              jQuery(".survey_btn").hide();
              jQuery(".thank_btn").hide();
          }
        });   
    jQuery(document).ready(function() {
        //Main.init();
        //SVExamples.init();
        Charts.init();
    });
</script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#session_date').datepicker({
        weekStart: 1,
        startDate: '-0d',
        endDate: '<?php echo date('m/d/Y',strtotime($event['End_date'])); ?>', 
        autoclose: true
    });
});
$('.datetimepicker').datetimepicker({
    format:'Y-m-d H:i',
    step:5
});
</script>
