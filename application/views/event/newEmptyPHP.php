<!-- Add Accordien -->
                <div class="col-md-7 col-lg-12">
                    <?php 
                        foreach ($Event as $key=>$eventname)
                        {                                
                    ?>
                    <div class="panel panel-dark">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $eventname['Botanical_name']."/".$eventname['Common_name']; ?></h4>
<!--                            <span class="label pull-right"></span>-->
                        </div>
                        <div class="panel-body no-padding">
                            <div class="partition-green padding-15 text-center">
<!--                                <h4 class="no-margin">Monthly Statistics</h4>
                                <span class="text-light">based on the major browsers</span>-->
                            </div>
                            
                            <div id="<?php echo $eventname['Id']; ?>" class="panel-group accordion accordion-white no-margin">
                                <div class="panel no-radius">                                   
                                        <?php
                                            foreach ($eventname['Month'] as $mkey=>$mvalue)
                                            {
                                            ?>
                                             <div class="panel-heading">
                                                <h4 class="panel-title monthname">
                                                <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-parent="#<?php echo $eventname['Id']; ?>" data-toggle="collapse" class="accordion-toggle padding-15 months">
                                                    <i class="icon-arrow"></i>
                                                    <?php echo $mkey; ?> <!--<span class="label label-danger pull-right">3</span>-->
                                                </a></h4>
                                            </div>
                                    <div class="panel-collapse collapse in" id="<?php echo "collapseOne".$eventname['Id'].$mkey; ?>">
                                        <div class="panel-body no-padding partition-light-grey">
                                            <?php                                                 
                                                foreach ($mvalue['Size'] as $skey=>$svalue)
                                                {                                                    
                                            ?>
                                            <div class="panel-heading">
                                                <h4 class="panel-title sizename">
                                                    <a href="#<?php echo "collapseOne".$eventname['Id'].$mkey.$skey; ?>" data-parent="#<?php echo "collapseOne".$eventname['Id'].$mkey; ?>" data-toggle="collapse" class="accordion-toggle padding-15 sizes">
                                                        <i class="icon-arrow"></i>
                                                        <?php echo $skey; ?> <!--<span class="label label-danger pull-right">3</span>-->
                                                    </a></h4>
                                            </div>
                                            <div class="panel-collapse collapse in innercontains" id="<?php echo "collapseOne".$eventname['Id'].$mkey.$skey; ?>">
                                                <div class="panel-body no-padding partition-light-grey">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <th class="center">#</th>
                                                                <th>Attendee Name</th>
                                                                <th class="center">Quantity</th>
                                                            </tr>
                                                            <?php
                                                            for($i=0;$i<count($svalue);$i++)
                                                            {
                                                                if(isset($svalue[$i]))
                                                                { 
                                                                    if($svalue[$i]['Remainqty']>0)
                                                                    {
                                                            ?>
                                                            <tr>
                                                                <td class="center"><?php echo $i+1; ?></td>
                                                                <td><?php echo $svalue[$i]['AttendeeName']; ?></td>
                                                                <td class="center"><?php echo $svalue[$i]['Remainqty']; ?></td>
                                                            </tr>
                                                            <?php 
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php
                                                
                                                }
                                            ?>
                                        </div>
                                    </div>
                                            <?php
                                            }
                                            ?>                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>                
                <!-- End Add Accordien -->