<?php $acc_name=$this->session->userdata('acc_name');?>
<style type="text/css"> 
.customcol-md-4 {
    width: 83.3333% !important;
}
.customcol-md-4 .file_btn input  {     
    z-index: 5;     
    background: #fcf8e3;     
    margin-left: 0;     
    position: relative;     
    cursor: pointer;     
    width: 100%;     
    border: 1px solid #DDD;     
    border-top: 0;     
    border-radius: 0px;     
    text-align: left;     
    height: 200px; 
} 
.customcol-md-4 .file_btn  {     
    padding: 5px 0px;     
    width: 100%; 
} 
.customcol-md-4 .image-upload-title  {     
    margin-bottom: -5px;     
    padding: 10px;     
    text-align: center;     
    background: #337AB7;     
    color: #fff; 
} 
#c-submit {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: #337ab7 none repeat scroll 0 0;
    border-color: -moz-use-text-color #2e6da4 #2e6da4;
    border-image: none;
    border-style: none solid solid;
    border-width: 0 1px 1px;
    color: #fff;
    padding: 10px;
    width: 100%;
}
.overview .titles {
    background: #333 none repeat scroll 0 0;
    color: #fff;
    font-weight: 300;
    padding: 10px 0 10px 10px;
    position: relative;
    text-align: left;
}
.overview .titles i {
    cursor: pointer;
    float: right;
    font-size: 20px;
    margin-left: 0;
    margin-top: -2px;
    padding: 0 5px;
    transition: background-color 0.8s ease 0s;
}
.fa {
    display: inline-block;
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    line-height: 1;
}
#wizard .col-sm-6{
padding-left:0px; }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<div class="row" id="edit">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php }  ?>
            <div class="panel-body" style="padding: 0px;">
            <?php if($user->Role_name=='Client'){ ?>
                <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div id="wizard" class="swMain">
                        <ul style="margin-top:15px;">
                            <li>
                                <a href="#step-1">
                                    <div class="stepNumber">
                                        1
                                    </div>
                                    <span class="stepDesc"> 
                                    <small>Define your app</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <div class="stepNumber">
                                        2
                                    </div>
                                    <span class="stepDesc">
                                    <small>Add Your Branding</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <div class="stepNumber">
                                        3
                                    </div>
                                    <span class="stepDesc">
                                    <small>Add Homescreen Text</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <div class="stepNumber">
                                        4
                                    </div>
                                    <span class="stepDesc">
                                    <small>Create your color scheme</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <div class="stepNumber">
                                        5
                                    </div>
                                    <span class="stepDesc">
                                    <small>Add Commerce Details</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-6">
                                    <div class="stepNumber">
                                        6
                                    </div>
                                    <span class="stepDesc"> 
                                        <small>Finish</small>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="progress progress-xs transparent-black no-radius active">
                            <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                              <!--  <span class="sr-only"> 0% Complete (success) </span>-->
                            </div>
                        </div>
                        <div id="step-1">
                            <h3 class="StepTitle">The Basics</h3>
                            <span class="control-label">Laying the foundations of your app is the most important first step. Follow the instructions below to soft-launch your app so it is ready to be customized and launched!</span>
                            <div class="col-sm-6">
                                <h3 class="StepTitle">Enter Your App Link</h3>
                                <span><p>This is the unique extension your app users will use to access your app. Once you have chosen your Unique URL you cannot change it without speaking with the Support Team - so choose a suitable extension you would like your users to see and you would like to share.</p>
                                <p><a href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>"><?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?></a></p></span>
                                <h3 class="StepTitle">Give your App a Name</h3>
                                <p>Save your app with a suitable name. This name will appear when your app is shared across social media channels and underneath your logo in the app. For example: 25th Annual Event</p>
                                <div class="form-group">
                                    <div class="col-sm-12" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Event_name']; ?>" placeholder="Enter Your app name" id="Event_name" name="Event_name" class="form-control name_group">
                                        <input type="hidden" name="id" id="Id" value="<?php echo $event['Id']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="padding-left: 0px;">
                                    <div class="col-sm-6" style="padding-left: 0px;">
                                        <button id="first_step_btn" class="btn btn-theme_green next-step btn-block">
                                            Next Step >>>
                                        </button>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="btn btn-yellow btn-block" type="button" onclick="preview('result_iframe_first');" value="Preview">      
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="float:left;">
                                <div id="viewport" class="iphone">
                                    <iframe src="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>" id="result_iframe_first"></iframe>
                                </div>
                            </div>
                        </div>
                        <div id="step-2">
                            <div class="row">
                                <div class="col-sm-4">
                                    <button class="btn btn-theme_green next-step btn-block">
                                        Skip This Step
                                    </button>
                                </div>
                            </div>
                            <h3 class="StepTitle">Add your branding</h3>
                            <span class="control-label">Here you can your logo and app banner to customize your app with your own branding. Please use JPG, JPEG and PNG files to get the best results. These can be changed at any time!</span>
                            <div class="col-sm-6">
                                <h3 class="StepTitle">Add your logo</h3>
                                <span>We recommend adding a logo with a width of at least 500 pixels to ensure it appears nice and crisp.</span>
                                <div class="col-sm-9" style="margin-top:15px;">
                                    <?php $logo_images_array = $event['Logo_images']; ?>
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail" id="logo_previews_image"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                <div id="sele">
                                                <span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span>
                                                </div><div id="chn">
                                                <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span></div>
                                                <input type="file" onchange="imagecrop();" name="logo_images" id="logo_images">
                                                </span>
                                                <div id="mybtn1">
                                                    <a href="#" id="mybtn" class="btn fileupload-exists btn-red mybtn" >
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                                </div>
                                           </div>
                                      </div>
                                       <?php if($logo_images_array != '') { ?>
                                        <div class="user-edit-image-buttons">
                                            <!--onclick="return confirm('Are you sure?')">-->
                                            <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_logo();">
                                              <i class="fa fa-times"></i> Delete Image
                                            </a>
                                        </div>
                                    <?php }  ?>
                                    <div class="col-sm-3 fileupload-new thumbnail center">
                                        <img id="logo_image_response" alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $logo_images_array; ?>">
                                        <input type="hidden" name="old_logo_images[]" value="<?php echo $logo_images_array; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3 custom-class">
                                    <input type="hidden" name="logo_image_tetbox" value="" id="logo_image_tetbox">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/logo_device.png">
                                </div>
                                 <div class="banner" style="clear:both;float: left; width: 75%;">
                                    <h3 class="StepTitle">Add your slide show images</h3>
                                    <p>We recommend using an image with a larger width than 1500 pixels.</p>
                                    <div class="row">   
                                        <div class="customcol-md-4" style="padding-left:15px;">
                                            <div class="col-md-12">
                                                <div class="row" id="image-upload-display">
                                                <?php foreach ($fun_setting['slideshow'] as $key => $slide) { ?>
                                                    <div class="col-sm-6 col-md-6 col-lg-4 banner-wrap">
                                                        <div class="overview widget" style="margin-top:0;height:auto;border-bottom-color:#888">
                                                            <div class="titles">
                                                                <i class="fa fa-times delete-slide1" slide-id="<?php echo $slide['id'] ?>" style="color:#d43f3a"></i>
                                                                &nbsp
                                                            </div>
                                                            <img src="<?php echo base_url().'fundraising/'.$slide['image']; ?>" style="width:100%;height:100px;">
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                </div>  
                                            </div>
                                            <div style="clear:both">
                                            </div>
                                            <div class="col-md-12"  style="padding-left:0px;"> 
                                                <div id="image-error-alert" style="background:#f2dede;padding:0px;color:#a94442;">
                                                </div> 
                                                <div class="image-upload-title">   &nbsp Drag and drop images &nbsp 
                                                    <i class="fa image-progress"></i> </div>     
                                                    <div class="file_btn"><input type="file" name="txtfiles[]" multiple id="txfile"/> 
                                                </div>
                                                <button onclick="slideshowupload();" type="button" id="c-submit">
                                                    <i class="fa fa-upload"></i> &nbsp; upload
                                                </button>
                                            </div>                    
                                        </div>                   
                                    </div>  
                                </div>
                                <div class="col-sm-3 custom-class">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/banner_device.png">
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-top:5px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <button class="btn btn-green back-step btn-block">
                                            <<< Back
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button id="second_step_btn" class="btn btn-theme_green next-step btn-block">
                                            Next Step >>>
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="btn btn-yellow btn-block" type="button" onclick="preview('result_iframe_forth');" value="Preview">      
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="viewport" class="iphone">
                                    <iframe src="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>" id="result_iframe_forth"></iframe>
                                </div>
                            </div>
                        </div>
                        <div id="step-3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <button id="step-thard-skip" class="btn btn-theme_green next-step btn-block">
                                        Skip This Step
                                    </button>
                                </div>
                            </div>
                            <h3 class="StepTitle">Add homescreen text (optional)</h3>
                            <p>Add some text about your app. This will show just below your banner image. Try adding text that explains the app in more detail or maybe add text about your organization. The text box below allows for different text colors, images and weblinks so you can be creative! You can change this at any time.</p>
                            <div class="col-sm-7" style="padding-left:0px;margin-bottom: 2%;">
                                <textarea  id="Description" class="click2edit form-control" name="Description" style="width: 100%;height: 300px;"><?php echo $event['Description']; ?></textarea>
                            
                            <div class="col-sm-12" style="padding-left:0px;">
                                <h3 class="StepTitle">Add an Image(optional)</h3>
                                <div class="col-sm-12" style="padding-left: 0px;">
                                    <div style="padding-left: 0px;" class="col-sm-5">
                                    <?php $images_array = json_decode($event['Images']); ?>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" id="banner_previews_image"></div>
                                            <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file">
                                                <div id="bannersele">
                                                <span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span></div><div id="bannerchn">
                                                <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span></div>
                                                <input type="file" onchange="bannercrop();" name="images" id="images">
                                                </span>
                                                <div id="bannermybtn1">
                                                    <a href="#" id="bannerbtn" class="btn fileupload-exists btn-red">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                                    <button style="display:none;" id="cropbannerbtnshow" type="button" class="btn btn-info" data-toggle="modal" data-target="#bannerimage">Crop Banners</button>
                                                </div>
                                            </div>
                                        </div>
                                           <?php if($images_array != '') { ?>
                                        <div class="user-edit-image-buttons">
                                                <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_image();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div>
                                        <?php }  ?>

                                        <?php for($i=0;$i<count($images_array);$i++) { ?>
                                            <div class="col-sm-3 fileupload-new thumbnail center">
                                                <img id="image_response" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                                <input type="hidden" id="old_images" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                              </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-2">
                                    <h4>Or</h4></div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <div class="col-sm-9" style="padding-right:0px;">
                                                <input type="hidden" name="banner_image_tetbox" value="" id="banner_image_tetbox">
                                                <input type="text" placeholder="Enter an Image URL" class="form-control" id="image_url" name="image_url">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" style="padding-left:0px;">
                                <h3 class="StepTitle">Add A Video(optional)</h3>
                                <div class="form-group">
                                    <div class="col-sm-6" style="padding-right:0px;">
                                        <input type="text" placeholder="Enter Youtube or Vimeo Link" class="form-control" id="video_url" value="<?php echo $fun_setting['fundraising_setting']['event_video_link']; ?>" name="video_url">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-5" style="float:left;">
                                <div id="viewport" class="iphone">
                                    <iframe src="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>" id="result_iframe_second"></iframe>
                                </div>
                            </div>

                             
                            <div class="form-group" style="clear:both;">
                                <div class="col-sm-2">
                                    <button class="btn btn-green back-step btn-block">
                                        <<< Back
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <button id="three_step_btn" class="btn btn-theme_green next-step btn-block">
                                        Next Step >>>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-yellow btn-block" type="button" onclick="preview('result_iframe_second');" value="Preview">      
                                </div>
                            </div>
                            
                        </div>
                        <div id="step-4">  
                            <div class="row">
                                <div class="col-sm-4">
                                    <button id="forth_step_skip" class="btn btn-theme_green next-step btn-block">
                                        Skip This Step
                                    </button>
                                </div>
                            </div> 
                            <h3 class="StepTitle">Create your color scheme</h3>
                            <p>Add your brand colors to your app to customize it further for your branding. Click on the box to select your colors from the color choose or paste in your color hex code into the box. You can change this at any time.</p>
                            <div class="col-sm-6">
                                 <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Top bar background Color </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="<?php echo $event['Top_background_color']; ?>" placeholder="Top bar background color" id="Top_background_color" name="Top_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                    <div class="col-sm-2 custom-class-color">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/topbar_device.png">
                                </div>
                                  
                                </div>

                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Top bar text Color </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="<?php echo $event['Top_text_color']; ?>" placeholder="Top bar text color" id="Top_text_color" name="Top_text_color" class="color {hash:true} form-control name_group">
                                    </div>
                                    <div class="col-sm-2 custom-class-color">
                                   <img alt="" src="<?php echo base_url(); ?>assets/images/topbar_texcolor_device.png">
                                </div>
                                </div>

                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Footer background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="<?php echo $event['Footer_background_color']; ?>" placeholder="Footer background color" id="Footer_background_color" name="Footer_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span>Theme Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="<?php echo $event['theme_color']; ?>" placeholder="Theme Color" id="theme_color" name="theme_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" value="<?php echo $event['menu_background_color']; ?>" placeholder="Menu Background color" id="menu_background_color" name="menu_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Hover Background Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" value="<?php echo $event['menu_hover_background_color']; ?>" placeholder="Menu Hover Background color" id="menu_hover_background_color" name="menu_hover_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <span> Menu Text Color</span>
                                    </div>
                                    <div class="col-sm-6">
                                       <input type="text" value="<?php echo $event['menu_text_color']; ?>" placeholder="Menu Text color" id="menu_text_color" name="menu_text_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;padding-left:0px;">
                                    <div class="col-sm-4" style="padding-left:0px;">
                                        <button class="btn btn-green back-step btn-block">
                                            <<< Back
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button id="foure_step_btn" class="btn btn-theme_green next-step btn-block">Next Step >>></button>
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="btn btn-yellow btn-block" type="button" onclick="preview('result_iframe_three');" value="Preview">      
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="float:left;">
                                <div id="viewport" class="iphone">
                                    <iframe src="" id="result_iframe_three"></iframe>
                                </div>
                            </div>     
                        </div>
                        <div id="step-5">
                            <h3 class="StepTitle">
                                Add Commerce Details
                            </h3>
                            <p>Have you can add details about your commerce or fundraising.</p>
                            <div class="form-group" style="margin-top:15px;">
                                <div class="col-sm-12" style="padding-left:0px;margin-bottom: 2%;">
                                    <div class="col-sm-2">
                                        <label>Select Currency</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="event_currncy"
                                        id="event_currncy" class="form-control name_group">
                                            <option <?php if($fun_setting['fundraising_currency']['currency']=="gbp#fa-gbp"){ ?> selected="selected" <?php } ?> value="gbp#fa-gbp">GBP</option>
                                            <option <?php if($fun_setting['fundraising_currency']['currency']=="usd#fa-usd"){ ?> selected="selected" <?php } ?>  value="usd#fa-usd">USD</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                             
                            <div class="form-group">
                                <div class="col-sm-12" style="padding-left:0px;margin-bottom: 2%;">
                                    <div class="col-sm-2">
                                        <label>Target (optional)</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" value="<?php echo $fun_setting['fundraising_setting']['Fundraising_target']; ?>" name="fun_target" id="fun_target" class="form-control name_group">
                                    </div>
                                </div>
                            </div>                             
                            <div class="form-group">
                                <div class="col-sm-12" style="padding-left:0px;margin-bottom: 2%;">
                                    <div class="col-sm-2">
                                        <label>Show Target</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="fun_target_show" id="fun_target_show" class="form-control name_group">
                                            <option <?php if($fun_setting['fundraising_setting']['target_raisedsofar_display']=='1'){?> selected="selected" <?php } ?> value="1">Yes</option>
                                            <option <?php if($fun_setting['fundraising_setting']['target_raisedsofar_display']=='0'){?> selected="selected" <?php } ?> value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="padding-left:0px;margin-bottom: 2%;">
                                    <div class="col-sm-2">
                                        <label>Show Most Recent Bids & Donations</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="bids_donations_show" class="form-control name_group">
                                            <option <?php if($fun_setting['fundraising_setting']['bids_donations_display']=='1'){?> selected="selected" <?php } ?> value="1">Yes</option>
                                            <option <?php if($fun_setting['fundraising_setting']['bids_donations_display']=='0'){?> selected="selected" <?php } ?> value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="clear:both;">
                                <div class="col-sm-2">
                                    <button class="btn btn-green back-step btn-block"><<< Back
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <button id="editform_submit" class="btn btn-theme_green next-step btn-block">
                                        Finish
                                    </button>
                                </div>                     
                            </div>                         
                        </div>
                        <div id="step-6" style="margin-bottom:15px;padding-left:30px;">
                            <div id="image-error-alert" style="display:none;" class="alert alert-danger"></div>
                            <div id="edit-success-alert" style="display:none;" class="alert alert-success"></div>
                            <div class="col-sm-4" style="padding-left:0px;margin-right:63%;margin-bottom:15px;">
                                <a href="<?php echo base_url().'Event/access_setting/'.$event['Id']; ?>" class="btn btn-green btn-block">
                                    Continue
                                </a>
                            </div>
                            <h3 class="StepTitle">You have finished defining and setting up your app!</h3>
                            <p>Well done! You are now ready to add your content.</p>
                            <p>Start by opening the various modules you have access to in the left hand menu.</p>
                            <p>Below you can see our Guides Portal. Here you will find step by step instructions showing you how to customize each feature with video walkthroughs.</p>
                            <iframe id="guide_iframe" src="" style="width:90%;height:800px;position: relative;float: left;margin-bottom:10px;"></iframe>
                        </div>
                    </div>
                </form>
            <?php }else { ?>
                    <div id="viewport" class="iphone">
                        <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:40%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                    </div>
            <?php } ?>    
            </div>   
        </div>
    </div>
</div>
<!-- <a style="margin-right:10px ;margin-top:1px;margin-bottom:15px" class="display_notification" data-toggle="modal" data-target="#add-slides"><i class="fa fa-plus "></i></a> -->
<div class='modal fade no-display' id='add-slides' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
            </div>
            <div class='modal-body' style="padding:0px;" id="co">
            
            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
<div class='modal fade no-display' id='bannerimage' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class="modal-header">
                    <button aria-hidden="true" aria-hidden="true" data-dismiss="modal" class="close" type="button">
                        ×
                    </button>
                </div>
                <div class='modal-body' id="show_banner_div">

                </div>
                <div class='modal-footer'>
                </div>
            </div>
        </div>
    </div>
<div class='modal fade no-display' id='displayimage' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header">
                <button aria-hidden="true" id="cancel"  data-dismiss="modal" class="close" type="button">
                    ×
                </button>
            </div>
            <div class='modal-body' id="show_image_div"></div>
            <div class='modal-footer'></div>
        </div>
    </div>
</div>
<style type="text/css">
    #map
    {
        margin-bottom: 20px;
        height: 250px;
        background-color: #fff !important;
    }
    .gm-style
    {
        left: 17.5% !important;
        width: 74% !important;
    }
    .fileupload-new.thumbnail
    {
      height:auto;
    }
</style>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript">
  $("#mybtn").click(function(){
    $('#logo_previews_image').hide();
    $('.fileupload-new').show();
    $("#chn").hide();
     $("#sele").show();
    $("#mybtn1").hide();
});
  $("#bannerbtn").click(function(){
    $('#banner_previews_image').hide();
    $('.fileupload-new').show();
    $("#bannerchn").hide();
     $("#bannersele").show();
    $("#bannermybtn1").hide();
});
  $('#three_step_btn').click(function(){
     $("#result_iframe_three").attr("src","<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>");
     setTimeout(function(){
        $('#step-5').hide();
        $('#step-6').hide();
    },1000);
  });
  $('#step-thard-skip').click(function(){
    setTimeout(function(){
        $('#step-5').hide();
        $('#step-6').hide();
    },1000);
  });
$('#foure_step_btn').click(function(){
    setTimeout(function(){
        $('#step-5').show();
        $('#step-4').hide();
        $('#step-6').hide();
    },1000);
});
$('#forth_step_skip').click(function(){
    setTimeout(function(){
        $('#step-5').show();
        $('#step-4').hide();
        $('#step-6').hide();
    },1000);
});
  $('#second_step_btn').click(function(){
    $("#result_iframe_second").attr("src","<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>");
  });
    $('#editform_submit').click(function(){
        $('#step-5').hide();
        $('#step-6').show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'Event/editsubmit/'.$event['Id']; ?>",
            data: formData,
            processData: false,
            contentType: false,
            success: function(result){
                var data=$.parseJSON(result);
                if(data.logo_image!=null)
                {
                    $('#logo_image_response').attr('src',data.logo_image);
                    $('#logo_images').val('');
                }
                if(data.image!=null)
                {
                    $('#image_response').attr('src',data.image);
                    $('#images').val('');
                }
                if(data.back_image!=null)
                {
                    $('#back_image_response').attr('src',data.back_image);
                    $('#background_img').val('');
                }
                var values=data.msg.split('###');
                $('#image-error-alert').html('');
                if(values[0]=="error")
                {
                    $('#image-error-alert').show();
                    $('#image-error-alert').html(values[1]);
                }
                else
                {
                    $("#guide_iframe").attr("src","https://www.allintheloop.com/guide-cms.html");
                    $('#edit-success-alert').show();
                    $('#edit-success-alert').html(values[1]);
                }
            }
        });
    });
  function show_div(divid)
  {
    if("select_image_div"==divid)
    {
        $('#'+divid).slideDown('slow');
        $('#background_color_div').slideUp('slow');
    }
    else
    {
        $('#'+divid).slideDown('slow'); 
        $('#select_image_div').slideUp('slow');
    }
  }
  $('.delete-slide1').click(function() {
        var button  = $(this);
        var ID      = $(this).attr('slide-id');
        if(confirm("Are you sure to delete this image?"))
        {
            $.ajax({
                url: "<?php echo base_url(); ?>Event/delete_slide/"+ID,
                cache: true,
                beforeSend: function(){
                    button.removeClass('fa-times');
                    button.addClass('fa-spinner');
                },
                success: function(html){
                    button.parent('.titles').parent('.widget').parent('.banner-wrap').hide('slow');
                    $('#result').html(html);
                },
                error: function(){
                    button.removeClass('fa-spinner');
                    button.addClass('fa-warning');
                    setTimeout(function(){
                        button.removeClass('fa-warning');
                        button.addClass('fa-times');
                    }, 1000);
                }
            });
        }
    });
  function slideshowupload(){
    $('.image-progress').addClass('fa-spinner');
    var formData = new FormData($('form')[0]);
    $.ajax({
        type: "POST",
        url:"<?php echo base_url().'Event/slideshowupload'; ?>",
        data: formData,
        processData: false,
        contentType: false,
        success: function(result){
            $('.image-progress').removeClass('fa-spinner');
            $('.image-progress').addClass('fa-check');
            setTimeout(function(){$('.image-progress').removeClass('fa-check');}, 2500);
            $('#image-upload-display').append(result);
            //alert(result);
        }
    });
  }
  function delete_slide_image(divid,imgnm){
    $rval=removeValue($('#select_slide_image').val(),imgnm)
    $('#select_slide_image').val($rval);
   $(divid).remove();
  }

  function removeValue(list, value) {
  return list.replace(new RegExp(",?" + value + ",?"), function(match) {
      var first_comma = match.charAt(0) === ',',
          second_comma;

      if (first_comma &&
          (second_comma = match.charAt(match.length - 1) === ',')) {
        return ',';
      }
      return '';
    });
}
      function delete_backgroundimg()
      {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_backgroundimg/<?php echo $event['Id'];?>";
        }
      }
   function delete_image()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_banner/<?php echo $event['Id'];?>";
        }
    }
    function preview(iframeid)
    {
        $(".se-pre-con").show();
        var formData = new FormData($('form')[0]);
         $.ajax({
            url : "<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/preview'; ?>",
            type: "POST",  
            async: true,
            processData: false,
            contentType: false,
            success : function(data)
            {
                $("#"+iframeid).attr("src","<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/preview/'; ?>");
                $(".se-pre-con").hide();
            }
        });
    }
    function bannercrop()
    {
        $('#cropbannerbtnshow').show();
        $('#cropbannerbtnshow').html('Uploading Banner');
        $('#banner_previews_image').show();
        $("#bannerchn").show();
        $("#bannersele").hide();
        $("#bannermybtn1").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url : "<?php echo base_url().'Event/bannerupload/'.$event['Id']; ?>",
            data:formData,
            type: "POST",  
            async: true,
            processData: false,
            contentType: false,
            success : function(data)
            {
                //$('#bannerimage').modal({show:'true'});
                $('#show_banner_div').html(data);
                $('#cropbannerbtnshow').html('Crop Banner');  
            }
        });
    }
    function imagecrop()
    {
        $('#logo_previews_image').show();
        $("#chn").show();
        $("#sele").hide();
        $("#mybtn1").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url : "<?php echo base_url().'Event/imageupload/'.$event['Id']; ?>",
            data:formData,
            type: "POST",  
            async: true,
            processData: false,
            contentType: false,
            success : function(data)
            {
                $('#displayimage').modal({show:'true'});
                $('#show_image_div').html(data);    
            }
        });   
    }
    function closepoppu(){
        $('#displayimage').modal({show:'false'});
        $('#displayimage').data('modal', null);
    }
    function image_crop_save()
    {
        var coor ="x1="+$('#x1').val()+"&y1="+$('#y1').val()+"&x2="+$('#x2').val()+"&y2="+$('#y2').val()+"&w="+$('#w').val()+"&h="+$('#h').val()+"&organalimage="+$('#organalimage').val();
        if($('#x1').val()!="" && $('#y1').val()!="" && $('#x2').val()!="" && $('#y2').val()!="" && $('#w').val()!="" && $('#h').val()!=""){
        $.ajax({
            url : "<?php echo base_url().'Event/savecropimage/'.$event['Id']; ?>",
            data: coor,
            type: "POST",  
            async: true,
            success : function(data)
            {
                var ht='<img src="'+data+'" alert="no image"/>';
                if($.trim($('#logo_or_banner').val())==0)
                {
                    $('#logo_previews_image').html(ht);
                    $('#logo_image_tetbox').val($.trim($('#organalimage').val()));
                    $('#show_image_div').html('');
                }
                else
                {
                    $('#banner_previews_image').html(ht);
                    $('#banner_image_tetbox').val($.trim($('#organalimage').val()));
                    $('#show_banner_div').html('');
                }
                $('#displayimage').attr('class','modal fade');
                $('#displayimage').attr('style','display:none;');
                $('body').removeAttr('class');
                $('.modal-backdrop').removeClass('fade in');
                $('.modal-backdrop').hide();
                $('#displayimage').data('modal', null);
                $('#bannerimage').data('modal', null);
            }
        });
        }
        else{
            if($.trim($('#logo_or_banner').val())==0)
            {
                $('#logo_image_tetbox').val('');
                $('#displayimage').data('modal', null);
                $('#displayimage').attr('class','modal fade');
                $('#displayimage').attr('style','display:none;');
            }
            else
            {
                $('#banner_image_tetbox').val('');
                $('#bannerimage').data('modal', null);
                $('#bannerimage').attr('class','modal fade');
                $('#bannerimage').attr('style','display:none;');
            }
            $('body').removeAttr('class');
            $('.modal-backdrop').removeClass('fade in');
            $('.modal-backdrop').hide();
            

        }  
    }
    function delete_logo()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_logo/<?php echo $event['Id'];?>";
        }
    }
        
    function checksubdomain()
    {      
            var sendflag="";
            $.ajax({
            url : '<?php echo base_url(); ?>Event/checksubdomain',
            data :'Subdomain='+$("#Subdomain").val()+'&idval='+$('#idval').val(),
            type: "POST",  
            async: false,
            success : function(data)
            {
                var values=data.split('###');
                if(values[0]=="error")
                {   
                    $('#Subdomain').parent().removeClass('has-success').addClass('has-error');
                    $('#Subdomain').parent().find('.control-label span').removeClass('ok').addClass('required');
                    $('#Subdomain').parent().find('.help-block').removeClass('valid').html(values[1]);
                    sendflag=false;
                }
                else
                {
                    $('#Subdomain').parent().removeClass('has-error').addClass('has-success');
                    $('#Subdomain').parent().find('.control-label span').removeClass('required').addClass('ok');
                    $('#Subdomain').parent().find('.help-block').addClass('valid').html(''); 
                    sendflag=true;
                }
            }
        });
        return sendflag;
    }
        var file = $("#images");
    var validation_file = false;
    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
    });

   
/*function progressHandlingFunction(e)
{
    if (e.lengthComputable) {
            // $('progress').attr({value:e.loaded,max:e.total});
            $('.progress-bar').css('width', (e.loaded / e.total)*100+"%");
            $('.progress-bar').html((e.loaded / e.total)*100 + "%");
        }
    }
    // validate the upload file
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {
                var value = $(this).val(),
                file = value.toLowerCase(),
                extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
    file.checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('#image-error-alert').html('');
            $('#file').css('background-color', '1px solid green');
            validation_file = true;
            var formData = new FormData($('form')[0]);
            $.ajax({
                url : '<?php echo base_url(); ?>app/<?php echo $acc_name."/".$event['Subdomain'];?>/preview',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                    }
                    return myXhr;
                },
                beforeSend: function(){
                    $('#file').attr('disabled', 'disabled');
                },
                error: function(){
                    console.log(formData);
                    $('#image-error-alert').html('<p>&nbsp</p> &nbsp <i class="fa fa-warning"></i>&nbsp an error occured. <p>&nbsp</p>');
                    setTimeout(function(){
                        $('#image-error-alert').html('');
                    }, 3000);
                    $('#file').removeAttr('disabled');
                    $('.image-progress').removeClass('fa-spinner');
                    $('.image-progress').addClass('fa-warning');
                    setTimeout(function(){$('.image-progress').removeClass('fa-warning');}, 2500);
                },
                success: function(html){
                    $('#file').removeAttr('disabled');
                    $('.image-progress').removeClass('fa-spinner');
                    $('.image-progress').addClass('fa-check');
                    setTimeout(function(){$('.image-progress').removeClass('fa-check');}, 2500);
                    $('#image-upload-display').append(html);
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        },
        error: function() {
            validation_file = false;
            $('#image-error-alert').html('<p>&nbsp</p> &nbsp <i class="fa fa-warning"></i>&nbsp Wrong file type <p>&nbsp</p>');
        }
    });*/
</script>


<style>
#wizard h3{
    font-size: 35px;
  }
  #wizard p,#wizard span{
    font-size: 15px;
  }
  .cropit-image-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 5px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 250px;
    height: 250px;
    cursor: move;
  }
  .cropit-image-background {
    opacity: .2;
    cursor: auto;
  }
  .image-size-label {
    margin-top: 10px;
  }
  input {
    /* Use relative position to prevent from being covered by image background */
    position: relative;
    /*z-index: 10;*/
    display: block;
  }
  .export {
    margin-top: 10px;
  }
  #wizard h3
  {
    color: #00b8c2;
    font-weight: bold;
  }

  #wizard #step-4 span
  {
    color: #00b8c2;
    font-weight: bold;
  }

  /*#wizard .progress-bar.partition-green.step-bar
  {
    background: #00b8c2;
  }*/

  #wizard .progress.progress-xs.transparent-black.no-radius.active.content
  {
    width: 95%;
    margin: 0 auto;
  }

  #wizard #step-1
  {
    padding:20px;
  }
  #wizard #step-2
  {
    padding:20px;
  }
  #wizard #step-3
  {
    padding:20px;
  }
  #wizard #step-4
  {
    padding:20px;
  }
  #wizard #step-5
  {
    padding:20px;
  }
  
</style>
<!-- end: PAGE CONTENT-->