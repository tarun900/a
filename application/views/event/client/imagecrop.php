<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Jcrop/css/jquery.Jcrop.min.css">
<div class="row">
    <div class="col-md-12"> 
        <div class="panel panel-white">
            <div class="panel-body crop-image"> 
                <img id="target" src="<?php echo $link; ?>" alt="[Jcrop Example]"/>                         
                <form id="coords" class="coords" method="post" action="">
                    <div class="inline-labels" style="display: none;">
                        <input type="hidden" value="<?php echo $imgname; ?>" id="organalimage" name="organalimage">
                        <input type="hidden" value="<?php echo $logo_or_banner; ?>" id="logo_or_banner" name="logo_or_banner">
                        <label>
                            X1
                            <input type="text" size="4" id="x1" name="x1" />
                        </label>
                        <label>
                            Y1
                            <input type="text" size="4" id="y1" name="y1" />
                        </label>
                        <label>
                            X2
                            <input type="text" size="4" id="x2" name="x2" />
                        </label>
                        <label>
                            Y2
                            <input type="text" size="4" id="y2" name="y2" />
                        </label>
                        <label>
                            W
                            <input type="text" size="4" id="w" name="w" />
                        </label>
                        <label>
                            H
                            <input type="text" size="4" id="h" name="h" />
                        </label>
                    </div>
                    <input type="button" name="sub" onclick="image_crop_save()"  class="close" data-dismiss="modal" id="image_crop" value="crop">
                </form>
             </div>
        </div> 
    </div>
</div>
<style type="text/css">
    .jcrop-keymgr{
        display: none !important;
    }
</style>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="<?php  echo base_url();?>assets/plugins/Jcrop/js/jquery.Jcrop.min.js"></script>
<script src="<?php  echo base_url();?>assets/plugins/Jcrop/js/jquery.color.js"></script>
<script src="<?php  echo base_url();?>assets/js/form-image-cropping.js"></script>
<script>
    jQuery(document).ready(function() {
        ImageCropping.init();
    });
</script>
<style type="text/css">
    .jcrop-holder
    {
        max-width: 100% !important;
    }
    .jcrop-holder img
    {
        max-width: 100% !important;
    }
</style>