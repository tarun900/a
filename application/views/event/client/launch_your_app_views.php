<?php 
if (!empty($_SERVER['HTTP_CLIENT_IP']))      
{      
  $ip=$_SERVER['HTTP_CLIENT_IP'];    
}    
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))       
{      
  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];    
}    
else    
{   
   $ip = $_SERVER['REMOTE_ADDR'];    
}

$iptolocation = 'http://api.hostip.info/country.php?ip=' . $ip;
$query = file_get_contents($iptolocation);

if($query) 
{
  if($query == 'GB')
  {
    $currency="gbp";
  }
  else
  {
    $currency="usd";
  }
}
else
{
  $currency="usd";
}
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/launchyourappviewpage.css?<?php echo time(); ?>">
<?php if(count($iseventfreetrial) > 0){ ?>
<form enctype="multipart/form-data" method="post" id="launch_your_app_form" name="launch_your_app_form" action="<?php echo base_url().'event/make_launch_stripe_payment/'.$event_id; ?>">
<h4 style="text-align: center;">Launch Your App</h4>
<div class="row" id="launch_your_app_popup_first_page" style="height: 500px;">
  <div class="col-sm-12">
    <div class="col-sm-6 text-center">
      <div class="package_name_price">
      <h4 style="text-align: center;">Standard</h4>
      <p>(Launched on the All In The Loop App)</p>
      <?php
      /*if (!empty($_SERVER['HTTP_CLIENT_IP']))      
      {      
        $ip=$_SERVER['HTTP_CLIENT_IP'];    
      }    
      elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))       
      {      
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];    
      }    
      else    
      {   
         $ip = $_SERVER['REMOTE_ADDR'];    
      }
      $iptolocation = 'http://api.hostip.info/country.php?ip=' . $ip;
      $query = file_get_contents($iptolocation);
      if($query !='') 
      {
        if($query == 'GB')
        {
          $currency="gbp";
          echo '<span><i class="fa fa-'.$currency.'"></i> 1,500 Plus VAT</span>';
        }
        else
        {
        $currency="usd";
        echo '<span><i class="fa fa-'.$currency.'"></i> 2,000</span>';
        }
      }
      else
      {
        $currency="usd";
      }*/

      $currency="usd";
      echo '<span><i class="fa fa-'.$currency.'"></i> 999 </span>';

      ?>
      
      </div>
      <div class="col-sm-12 package_content" style="min-height: 188px;">
        <p>Update Instantly</p>
        <p>Active for 12 Months</p>
        <p>Unique App Web Link</p>
        <p>Users download the All In The Loop App and search for your App using the Secure Key set in Access Settings</p>
      </div> 
      <div class="col-sm-12 text-center">
        <a class="btn btn-green btn-block" id="standard_launch_your_app_btn" style="width: 150px;margin: 0 auto;" href="javascript:void(0);">Buy Now</a>
      </div>  
    </div>    
    <div class="col-sm-6 text-center">
      <div class="package_name_price">
        <h4 style="text-align: center;">Enterprise</h4>
        <p>(Launched in the App Store and Play Store)</p>
        <!-- <span><i class="fa fa-<?php echo $currency; ?>"></i> 1,199</span> -->
      </div>
      <div class="col-sm-12 package_content" style="min-height: 188px;margin-top: 12%;">
        <p>Update Instantly</p>
        <p>Active for 12 months</p>
        <p>Unique Web App Link</p>
        <p>Branded Standalone App</p>
        <p>Available on the App Store</p>
        <p>Available on the Play Store</p>
      </div> 
      <div class="col-sm-12 text-center">
        <!-- <a class="btn btn-green btn-block" id="plus_launch_your_app_btn" style="width: 150px;margin: 0 auto;" href="javascript:void(0);">Speak to the team</a> -->
        <a class="btn btn-green btn-block" style="width: 150px;margin: 0 auto;" href="http://allintheloop.com/product-tour-event-apps.html" target="_blank">Speak to the team</a>
      </div>
    </div>    
    <!-- <div class="col-sm-4 text-center">
      <div class="package_name_price">
        <h4 style="text-align: center;">Multi-Event App</h4>
        <p>(Launched in the App Store and Play Store)</p>
        <span><i class="fa fa-<?php echo $currency; ?>"></i> 1,999</span>
      </div>
      <div class="col-sm-12 package_content">
        <p>Update Instantly</p>
        <p>Active for 12 months</p>
        <p>Unique App Web Link</p>
        <p>Branded Multi-Event App</p>
        <p>Available on the App Store</p>
        <p>Available on the Play Store</p>
      </div>
      <div class="col-sm-12 text-center">
        <a class="btn btn-green btn-block" id="multi_event_app_launch_your_app_btn" style="width: 150px; margin: 0 auto;" href="javascript:void(0);">Speak to the team</a>
      </div>
    </div> -->    
  </div>  
</div>
<input type="hidden" id="stripeToken" name="stripeToken" value="">
<input type="hidden" id="payment_price" name="payment_price" value="">
<input type="hidden" id="which_launch_your_app_btn_click" name="which_launch_your_app_btn_click" value="">
</form>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>
<?php $key=$admin_scret_key[0]['public_key']; 

if (!empty($_SERVER['HTTP_CLIENT_IP']))      
{      
  $ip=$_SERVER['HTTP_CLIENT_IP'];    
}    
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))       
{      
  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];    
}    
else    
{   
   $ip = $_SERVER['REMOTE_ADDR'];    
}
$iptolocation = 'http://api.hostip.info/country.php?ip=' . $ip;
$query = file_get_contents($iptolocation);

//if($query == 'IN') { $currency1="gbp"; } else { $currency1="usd"; }
$currency1="usd";

//$key="pk_test_oxTGjphCde1zLtOwDSxcjjwr"; ?> 
<script type="text/javascript">
$(window).load(function(){
  $('.summernote').summernote({
    height:'200px',
  });
});
var handler = StripeCheckout.configure({
  key: '<?php echo $key; ?>',
  image: '<?php echo base_url(); ?>assets/images/stripe_logo.png?<?php echo time(); ?>',
  locale: 'auto',
  token: function(token) {
    jQuery('#stripeToken').val(token.id);
    jQuery('#launch_your_app_form').submit();
  }
});
$('#plus_launch_your_app_btn').click(function(){
  $('#which_launch_your_app_btn_click').val(1);
  $('#payment_price').val('1199');
  handler.open({
    name: 'Allintheloop.com',
    description: 'All In The Loop Software',
    amount: "<?php echo 1199*100; ?>",
    currency:"<?php echo $currency; ?>",
    billingAddress:"true"
  });
  e.preventDefault();
});
$('#multi_event_app_launch_your_app_btn').click(function(){
  $('#which_launch_your_app_btn_click').val(2);
  $('#payment_price').val('1999');
  handler.open({
    name: 'Allintheloop.com',
    description: 'All In The Loop Software',
    amount: "<?php echo 1999*100; ?>",
    currency:"<?php echo $currency; ?>",
    billingAddress:"true"
  });
  e.preventDefault();
});
$('#standard_launch_your_app_btn').click(function(){
  $('#payment_price').val('999');
  handler.open({
    name: 'Allintheloop.com',
    description: 'All In The Loop Software',
    amount: "<?php echo 999*100; ?>",
    currency:"<?php echo $currency1; ?>",
    billingAddress:"true"
  });
  e.preventDefault();
});
</script>
<?php }else{ ?>
<script src="https://www.allintheloop.net/demo/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<div id="launch_your_app_form">
  <h4>Speak to the team</h4>
  <div class="row">
    <h2 style="text-align: center;"></h2>
  </div>
</div>
<?php } ?>