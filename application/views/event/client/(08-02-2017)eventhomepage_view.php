<?php $acc_name=$this->session->userdata('acc_name'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/newcmseventpage.css">
<div class="col-sm-12">
	<div class="panel panel-body">
		<h4 style="color:#5381ce;">App Summary</h4>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="tabbable tabs-left">
						<ul id="visitor_event_second_screen_tab" class="nav nav-tabs">
							<li class="active">
								<a href="#app_content_data_section" data-toggle="tab">
									App Content
								</a>
							</li>
							<li class="">
								<a href="#interactive_content_data_section" data-toggle="tab">
									Interactive Features
								</a>
							</li>
							<li class="">
								<a href="#event_content_data_section" data-toggle="tab">
									Event Features
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="app_content_data_section">
								<div class="col-sm-12">
									<div class="row">
										<div class="tabbing-content-row">
											<div class="icon_img_modules">
												<img src="<?php echo base_url().'assets/css/images/icontwostep/visitor advertising icon.png'; ?>" />
											</div>
											<div class="content-info">
												<label class="control-label" id="label_5">Advertising</label>
												<p>Show advert banners within your App.</p>
											</div>
				                            <div class="edit-labeltxt">
				                             <?php echo $advertising_count.' Adverts'; ?>
				                            </div>
			                            </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor maps icon.png'; ?>" /></div>
                                        	<div class="content-info">
												<label class="control-label" id="label_10">Maps</label>
												<p>Include Google Maps and Interactive Maps</p>
											</div>
					                        <div class="edit-labeltxt">
					                        <?php echo $maps_count.' Maps'; ?>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor social icon.png'; ?>" /></div>
                                          	<div class="content-info">
												<label class="control-label" id="label_17">Social</label>
												<p>Include your social media links</p>
											</div>
				                            <div class="edit-labeltxt">
				                            <?php echo $social_count.' Social Links'; ?>
				                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor presentations icon.png'; ?>" /></div>
                                        <div class="content-info">
				                        <label class="control-label" id="label_9">Presentations</label>
                                        <p>Add Powerpoint presentations</p>
                                        </div>
			                            <div class="edit-labeltxt">
			                            <?php echo $presentation_count.' Presentations'; ?>
			                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor documents icon.png'; ?>" /></div>
                                        <div class="content-info">
				                        <label class="control-label" id="label_16">Documents</label>
                                        <p>Share multiple documents</p>
                                        </div>
			                            <div class="edit-labeltxt">
			                            <?php echo $document_count.' Documents'; ?>
			                            </div>
                                        </div>
                					</div>
								</div>	
							</div>
							<div class="tab-pane fade in" id="interactive_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    	<div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor notes icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        <label class="control-label" id="label_6">Notes</label>
                                        <p>Allow Users to make notes.</p>
                                        </div>
			                            <div class="edit-labeltxt">
			                            <?php echo $notes_count.' Notes'; ?>
			                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor private messaging icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        	<label class="control-label" id="label_12">Private Messaging</label>
                                        <p>Allow Users to message privately.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $private_messages_count.' Private Messaging'; ?>
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor surveys icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        	<label class="control-label" id="label_15">Surveys</label>
                                        <p>Include polls and surveys.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $surveys_count.' Surveys'; ?>
			                            </div>
			                            </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor twitter feed icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_44">Twitter Feed</label><p>Include a Twitter Feed.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $hashtags_count.' Twitter Hashtags'; ?>
			                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor instagram icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_46">Instagram Feed</label><p>Include an Instagram Feed.</p></div>
			                            <div class="edit-labeltxt">
			                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor photos icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_11">Photos</label><p>Allow Users to share photos on a photo feed.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $photos_count.' Photos'; ?>
			                            </div>
                                        </div>
                                        </div>
                					
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor public messaging icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_13">Public Messaging</label><p>Allow Users share messages on a public message feed.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $public_messages_count.' Public Messaging'; ?>
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor form builder icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_26">Form Builder</label><p>Include contact forms to generate leads.</p></div>
				                        
			                            <div class="edit-labeltxt">
			                            <?php echo $formbuilder_count.' Forms'; ?>
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor activity icon.png'; ?>" /></div>
                                         <div class="content-info">  <label class="control-label" id="label_45">Activity Feed</label><p>Show a full activity feed of all Users.</p></div>
				                      
			                            <div class="edit-labeltxt">
			                            </div>
			                           
                                        </div>
                					</div>
                                    </div>
								</div>
						
							<div class="tab-pane fade in" id="event_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor agenda icon.png'; ?>" /></div>
			                            <div class="content-info"><label class="control-label" id="label_1">Agenda</label><p>Add multiple Agendas and sessions.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $session_count.' Sessions'; ?>
			                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor exhibitors icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_3">Exhibitors</label><p>Manage and include an Exhibitor Directory.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $exhibitors_count.' Exhibitors'; ?>
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor sponsors icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_43">Sponsors</label><p>Manage and include a Sponsor Directory.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $sponsors_count.' Sponsors'; ?>
			                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor attendees icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        	<label class="control-label" id="label_2">Attendees</label>
                                        	<p>Manage and include an Attendee Directory.</p></div>
				                            <div class="edit-labeltxt">
				                            <?php echo $attendee_count.' Attendees'; ?>
				                            </div>
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor speakers icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_7">Speakers</label><p>Manage and include a Speaker Directory.</p></div>
			                            <div class="edit-labeltxt">
			                            <?php echo $speakers_count.' Speakers'; ?>
			                            </div>
			                            </div>
                					</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div id="viewport" class="iphone">
                <iframe src="<?php echo base_url().'app/'.$acc_name.'/'.$event['Subdomain']; ?>" id="result_iframe_first"></iframe>
            </div>
		</div>
	</div>
</div>
