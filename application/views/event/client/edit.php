<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row" id="edit">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                     <?php if($user->Role_name=='Client'){ ?>
                        <li class="active">
                            <a href="#edit_events" data-toggle="tab">
                                Edit Your App
                            </a>
                        </li>
                    <?php } ?>
                         <li class="<?php if($user->Role_name!='Client'){ echo 'active'; } ?>">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                     <?php if($user->Role_name=='Client'){ ?>
                        <div class="tab-pane fade active in" id="edit_events">
                            <?php if($this->session->flashdata('event_data')) { ?>
                                <div class="errorHandler alert alert-success no-display" style="display: block;">
                                    <i class="fa fa-remove-sign"></i> App <?php echo $this->session->flashdata('event_data'); ?> Successfully.
                                </div>
                            <?php } ?>
                            <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Unique URL 
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-4" style="padding:0px;">
                                            <span><?php echo base_url(); ?>App/<?php echo $arrAct[0]['acc_name']; ?>/<?php echo $event['Subdomain']; ?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Event Name <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Event_name']; ?>" placeholder="Event Name" id="Event_name" name="Event_name" class="form-control name_group">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Start Date <span class="symbol required" id="showdatetime_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="input-group" style="padding-left: 1.7%;">
                                            <input type="text" name="Start_date" id="startDate" contenteditable="false" class="form-control" value="<?php echo $event['Start_date']; ?>">
                                             <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                       End Date <span class="symbol required" id="showdatetime_remark"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="input-group" style="padding-left: 1.7%;">
                                            <input type="text" name="End_date" id="endDate" contenteditable="false" class="form-control" value="<?php echo $event['End_date']; ?>">
                                            <input type="hidden" name="Organisor_id" id="Organisor_id" value="<?php echo $event['Organisor_id']; ?>">
                                             <input type="hidden" name="id" id="Id" value="<?php echo $event['Id']; ?>">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Status</label>
                                    <div class="col-sm-9">
                                        <label class="radio-inline hover">
                                            <input type="radio" class="purple" value="1" <?php if ($event['Status'] == '1') echo ' checked="checked"'; ?> name="Status">
                                            Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0"  <?php if ($event['Status'] == '0') echo ' checked="checked"'; ?> name="Status">
                                            Inactive
                                        </label>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Type</label>
                                    <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1" <?php if ($event['Event_type'] == '1') echo ' checked="checked"'; ?> name="Event_type">
                                            Private
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="2" <?php if ($event['Event_type'] == '2') echo ' checked="checked"'; ?> name="Event_type">
                                            Public
                                        </label> 
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="3" <?php if ($event['Event_type'] == '3') echo ' checked="checked"'; ?> name="Event_type">
                                            Public open
                                        </label>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Countdown Timer<!--Time--></label>
                                    <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1" <?php if ($event['Event_time'] == '1') echo ' checked="checked"'; ?> name="Event_time">
                                            Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="0" checked="checked" <?php if ($event['Event_time'] == '0') echo ' checked="checked"'; ?> name="Event_time">
                                            Inactive
                                        </label>    
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">App Time Option<!--Event Time Option--></label>
                                    <div class="col-sm-9">
                                       <?php $today = date('Y-m-d'); ?>
                                        <label class="radio-inline">
                                            <input type="radio" id="activeid" class="purple" <?php if ($event['Start_date'] < $today) echo ' disabled=""'; ?> value="1" <?php if ($event['Event_time_option'] == '1') echo ' checked="checked"'; ?> name="Event_time_option">
                                            App Start Time<!--Event Start Time-->
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" id="deactiveid" class="purple" <?php if ($event['End_date'] < $today) echo ' disabled=""'; ?> value="0" <?php if ($event['Event_time_option'] == '0') echo ' checked="checked"'; ?> name="Event_time_option">
                                            App End Time<!--Event End Time-->
                                        </label>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">Home Screen Text<!--Description--> </label>
                                    <div class="col-sm-9">
                                        <textarea  id="Description" class="click2edit form-control" name="Description" style="width: 100%;margin-top: 2%;height: 300px;"><?php echo $event['Description']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">Banner Images</label>
                                    <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:2000px &amp; height:2000px)</em>
                                    <div class="col-sm-9">
                                        <?php $images_array = json_decode($event['Images']); ?>
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="images" id="images">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>
                                          <?php if($images_array != '') { ?>
                                            <div class="user-edit-image-buttons">
                                                <!--onclick="return confirm('Are you sure?')">-->
                                                <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_image();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div>
                                        <?php }  ?>
                                        <?php for($i=0;$i<count($images_array);$i++) { ?>
                                            <div class="col-sm-3 fileupload-new thumbnail center">
                                                <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                                <input type="hidden" id="old_images" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                              </div>
                                        <?php } ?>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">Logo Image</label>
                                    <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:2000px &amp; height:2000px)</em>
                                    <div class="col-sm-9">
                                        <?php $logo_images_array = json_decode($event['Logo_images']); ?>

                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="logo_images" id="logo_images">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>
                                           <?php if($logo_images_array != '') { ?>
                                            <div class="user-edit-image-buttons">
                                                <!--onclick="return confirm('Are you sure?')">-->
                                                <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_logo();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div>
                                        <?php }  ?>
                                        <?php  for($i=0;$i<count($logo_images_array);$i++) { ?>
                                            <div class="col-sm-3 fileupload-new thumbnail center">
                                                <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $logo_images_array[$i]; ?>">
                                                <input type="hidden" name="old_logo_images[]" value="<?php echo $logo_images_array[$i]; ?>">
                                            </div>
                                        <?php } ?>                                                                                              
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1"></label>
                                     <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" class="radiobackground" checked="checked"  value="image" id="background_image" name="background">
                                            Background Image
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="radiobackground"  value="color" id="background_color" name="background">
                                            Background Color
                                        </label>    
                                    </div>
                                </div>

                                <div class="form-group" id="background_image_div">
                                    <label class="col-sm-2" for="form-field-1">Background Images</label>
                                    <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:2000px &amp; height:2000px)</em>
                                    <div class="col-sm-9">

                                        <?php $background_img_array = json_decode($event['Background_img']); ?>
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                               <div class="fileupload-new thumbnail"></div>
                                               <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                               <div class="user-edit-image-buttons">
                                                    <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                         <input type="file" name="background_img" id="background_img">
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                         <i class="fa fa-times"></i> Remove
                                                    </a>
                                               </div>
                                          </div>
                                          <?php if($background_img_array != '') { ?>
                                            <div class="user-edit-image-buttons">
                                                <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_backgroundimg();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div>
                                        <?php }  ?>

                                        <?php for($i=0;$i<count($background_img_array);$i++) { ?>
                                            <div class="col-sm-3 fileupload-new thumbnail center">
                                                <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $background_img_array[$i]; ?>">
                                                <input type="hidden" id="old_background_img" name="old_background_img[]" value="<?php echo $background_img_array[$i]; ?>">
                                              </div>
                                        <?php } ?>    
                                    </div>
                                </div>

                                <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                                <div class="form-group" id="background_color_div" style="display:none">
                                    <label class="col-sm-2" for="form-field-1">
                                        Background Color 
                                    </label>
                                    <div class="col-sm-4" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Background_color']; ?>" placeholder="Background color" id="Background_color" name="Background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Top bar background Color 
                                    </label>
                                    <div class="col-sm-4" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Top_background_color']; ?>" placeholder="Top bar background color" id="Top_background_color" name="Top_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Top bar text Color 
                                    </label>
                                    <div class="col-sm-4" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Top_text_color']; ?>" placeholder="Top bar text color" id="Top_text_color" name="Top_text_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Footer background Color 
                                    </label>
                                    <div class="col-sm-4" style="padding-right:0px;">
                                        <input type="text" value="<?php echo $event['Footer_background_color']; ?>" placeholder="Footer background color" id="Footer_background_color" name="Footer_background_color" class="color {hash:true} form-control name_group">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1"></label>
                                    <div class="col-md-4">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                            Update <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                         <!--<button data-toggle="modal" data-target="#add-slides" id="preview1" class="btn btn-yellow btn-block" type="button" onclick="preview();">
                                            Preview <i class="fa fa-arrow-circle-right"></i>
                                        </button>-->
                                    </div>
                                </div>

                            </form>

                        </div>
                        <?php } ?>
                        <div class="tab-pane <?php if($user->Role_name!='Client'){ echo 'active'; } ?> fade in" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                            <div id="viewport" class="iphone">
                                    <iframe id="" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                            </div>
                            <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                            <div id="viewport_images" class="iphone-l" style="display:none;">
                                   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #map
    {
        margin-bottom: 20px;
        height: 250px;
        background-color: #fff !important;
    }
    .gm-style
    {
        left: 17.5% !important;
        width: 74% !important;
    }
    .fileupload-new.thumbnail
    {
      height:auto;
    }
</style>
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript">
  $('input.radiobackground').click(function() {
            if($(this).val().trim()=="image")
            {
                 $('#background_color_div').slideUp('slow');
                $('#background_image_div').slideDown('slow');
            }else{
                $('#background_image_div').slideUp('slow');
                $('#background_color_div').slideDown('slow');
            }
        });
      function delete_backgroundimg()
      {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_backgroundimg/<?php echo $event['Id'];?>";
        }
      }
   function delete_image()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_banner/<?php echo $event['Id'];?>";
        }
    }
    function preview()
    {
        $(".se-pre-con").show();
         //$('.display_notification').click();
         var Background_color=$("#Background_color").val();
         var Top_background_color=$("#Top_background_color").val();
         var Top_text_color=$("#Top_text_color").val();
         var Footer_background_color=$("#Footer_background_color").val();
         var Start_date=$("#startDate").val();
         var End_date=$("#endDate").val();
         var Event_name=$("#Event_name").val();
         var images=$("#images").val();
         var Organisor_id=$("#Organisor_id").val();
         var Id=$("#Id").val();
     
         var preimg=$('#old_images').val(); 
         var Description=$("#Description").val();
        
       
         $.ajax({
            url : '<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event['Subdomain'];?>/preview',
            data :'Background_color='+Background_color+'&Top_background_color='+Top_background_color+'&Top_text_color='+Top_text_color+'&Footer_background_color='+Footer_background_color+'&Description='+Description+'&Event_name='+Event_name+'&Start_date='+Start_date+'&End_date='+End_date+'&Organisor_id='+Organisor_id+'&Id='+Id+'&Images='+images+'&preimg='+preimg,
            type: "POST",  
            async: true,
            success : function(data)
            {
                
                $(".se-pre-con").hide();
                $("#co").html(data);
            }
        });
     
    }
    function delete_logo()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href="<?php echo base_url(); ?>Event/delete_logo/<?php echo $event['Id'];?>";
        }
    }
        
    function checksubdomain()
    {      
            var sendflag="";
            $.ajax({
            url : '<?php echo base_url(); ?>Event/checksubdomain',
            data :'Subdomain='+$("#Subdomain").val()+'&idval='+$('#idval').val(),
            type: "POST",  
            async: false,
            success : function(data)
            {
                var values=data.split('###');
                if(values[0]=="error")
                {   
                    $('#Subdomain').parent().removeClass('has-success').addClass('has-error');
                    $('#Subdomain').parent().find('.control-label span').removeClass('ok').addClass('required');
                    $('#Subdomain').parent().find('.help-block').removeClass('valid').html(values[1]);
                    sendflag=false;
                }
                else
                {
                    $('#Subdomain').parent().removeClass('has-error').addClass('has-success');
                    $('#Subdomain').parent().find('.control-label span').removeClass('required').addClass('ok');
                    $('#Subdomain').parent().find('.help-block').addClass('valid').html(''); 
                    sendflag=true;
                }
            }
        });
        return sendflag;
    }
        var file = $("#images");
    var validation_file = false;
    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
    });

   
function progressHandlingFunction(e)
{
    if (e.lengthComputable) {
            // $('progress').attr({value:e.loaded,max:e.total});
            $('.progress-bar').css('width', (e.loaded / e.total)*100+"%");
            $('.progress-bar').html((e.loaded / e.total)*100 + "%");
        }
    }
    // validate the upload file
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {
                var value = $(this).val(),
                file = value.toLowerCase(),
                extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
    file.checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            $('#image-error-alert').html('');
            $('#file').css('background-color', '1px solid green');
            validation_file = true;
            var formData = new FormData($('form')[0]);
            $.ajax({
                url : '<?php echo base_url(); ?>App/<?php echo $acc_name."/".$event['Subdomain'];?>/preview',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                    }
                    return myXhr;
                },
                beforeSend: function(){
                    $('#file').attr('disabled', 'disabled');
                },
                error: function(){
                    console.log(formData);
                    $('#image-error-alert').html('<p>&nbsp</p> &nbsp <i class="fa fa-warning"></i>&nbsp an error occured. <p>&nbsp</p>');
                    setTimeout(function(){
                        $('#image-error-alert').html('');
                    }, 3000);
                    $('#file').removeAttr('disabled');
                    $('.image-progress').removeClass('fa-spinner');
                    $('.image-progress').addClass('fa-warning');
                    setTimeout(function(){$('.image-progress').removeClass('fa-warning');}, 2500);
                },
                success: function(html){
                    $('#file').removeAttr('disabled');
                    $('.image-progress').removeClass('fa-spinner');
                    $('.image-progress').addClass('fa-check');
                    setTimeout(function(){$('.image-progress').removeClass('fa-check');}, 2500);
                    $('#image-upload-display').append(html);
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        },
        error: function() {
            validation_file = false;
            $('#image-error-alert').html('<p>&nbsp</p> &nbsp <i class="fa fa-warning"></i>&nbsp Wrong file type <p>&nbsp</p>');
        }
    });
</script>


<style>
  .cropit-image-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 5px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 250px;
    height: 250px;
    cursor: move;
  }
  .cropit-image-background {
    opacity: .2;
    cursor: auto;
  }
  .image-size-label {
    margin-top: 10px;
  }
  input {
    /* Use relative position to prevent from being covered by image background */
    position: relative;
    /*z-index: 10;*/
    display: block;
  }
  .export {
    margin-top: 10px;
  }
</style>




<!-- end: PAGE CONTENT-->