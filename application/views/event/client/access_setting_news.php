<?php $image=json_decode($event['o_screen'],true); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#screen_content').summernote({
        height:300,
    });
    });
</script>
<script>
  $(function() {
    $('#sortable').sortable({ 
    axis: "x",
    revert: true,
    scroll: true,
    cursor: "move",
    placeholder: "placeholder",
    forcePlaceholderSize: true,
    update: function (event, ui) {
        var data = {};
        $('#sortable').children().each(function() {
            data[$(this).index() + 1] = $(this).attr('name');
        });
        $.ajax({
            url: "<?php echo base_url().'Event/save_sort_order/'.$event['Id']; ?>",
            type: 'post',
            data: {data:data},
            });
        }
    });
    $( "#sortable" ).disableSelection();
});
</script>

<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row" id="edit">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success no-display" style="display: block;">
                <button class="close" data-dismiss="alert"> × </button>
                <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#app_settings" data-toggle="tab">
                            App Settings
                        </a>
                    </li>
                    <li>
                        <a href="#login_screen" data-toggle="tab">
                            Login Screen
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="app_settings">
                    <div class="panel-body" style="padding: 0px;">
                        <form role="form" method="post" class="form-horizontal" id="form" action="">
                            <div class="col-sm-6" style="width:50%;padding-left: 0px;">
                                <div class="panel-heading">
                                    <h2 class="title">Privacy Settings</h2>
                                </div>
                                <!-- <div class="col-sm-12">    
                                    <p>All In The Loop has three different settings. Choose your setting and press save to activate it.</p>
                                    <p>NOTE: For anyone to interact with the app they must Sign Up/Log In</p>
                                </div> -->
                                <div class="col-sm-12" id="open_to_everyone_div">
                                    <div>
                                     <input type="radio" value="3" <?php if ($event['Event_type'] == '3') echo ' checked="checked"'; ?> name="Event_type" id="radio01">
                                    <label for="radio01">
                                    <h4>Open To Everyone</h4>
                                    <p style="padding-left: 0px;">Anyone can access the content in your app but must sign up in order to interact, for example; send messages or purchase products.</p></label>
                                       
                                    </div>
                                </div>
                                <div class="col-sm-12" id="requires_login_div">
                                    <div>
                                        <input type="radio" value="2" <?php if ($event['Event_type'] == '2') echo ' checked="checked"'; ?> name="Event_type" id="radio02">
                                        <label for="radio02">
                                            <h4>Requires Login</h4>
                                            <!-- <p style="padding-left: 0px;">Anyone can access your app once they sign up on the sign up form on your app. You can personalize the sign-up form by clicking here.</p> -->
                                            <p style="padding-left: 0px;">Anyone can access your app once they sign up on the sign up form on your app.</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12" id="private_div">
                                    <div>
                                     <input type="radio" value="1" <?php if ($event['Event_type'] == '1') echo ' checked="checked"'; ?> name="Event_type" id="radio03">
                                    <label for="radio03">
                                    <h4>Private</h4>
                                    <p style="padding-left: 0px;">Only pre-defined users who you invite in the Attendees module can access the app Only pre-defined app users can acc.</p></label>
                                       
                                    </div>
                                </div>
                                <div class="col-sm-12" id="authorized_div">
                                    <div>
                                        <input type="radio" value="4" <?php if ($event['Event_type'] == '4') echo ' checked="checked"'; ?> name="Event_type" id="radio04">
                                        <label for="radio04">
                                            <h4>Authorized Emails</h4>
                                            <p style="padding-left: 0px;">Only email addresses that you add to the Attendees module can gain access to the App – No registration or password is required.</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="title">Social Login Options</h2>
                                    <div class="col-sm-12">
                                        <h4>
                                            <label class="radio-inline hover" style="padding-left: 0px;">
                                                <span style="padding-right:5px;">Allow Users to Login with Facebook</span>
                                                 <input type="checkbox" value="1"  id="facebook_login" name="facebook_login" <?php if($notificationsetting[0]['facebook_login']=='1') { echo 'checked'; } ?> >
                                            </label>
                                        </h4>
                                    </div>
                                    <div class="col-sm-12">
                                        <h4>
                                            <label class="radio-inline hover" style="padding-left: 0px;">
                                                <span style="padding-right:5px;">Allow Users to Login with LinkedIn</span>
                                                 <input type="checkbox" value="1"  id="linkedin_login_enabled" name="linkedin_login_enabled" <?php if($notificationsetting[0]['linkedin_login_enabled']=='1') { echo 'checked'; } ?> >
                                            </label>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="float:left;width:50%">
                                <div class="panel-heading">
                                    <h2 class="title">Time Settings</h2>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-12">
                                        <label class="radio-inline hover">
                                            <input type="radio" class="purple" value="0" <?php if ($notificationsetting[0]['format_time'] == '0') echo ' checked="checked"'; ?> name="format_time">
                                            12 Hour Time
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1"  <?php if ($notificationsetting[0]['format_time'] == '1') echo ' checked="checked"'; ?> name="format_time">
                                             24 Hour Time
                                        </label>    
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h4>Time Zone</h4>
                                </div> 
                                <div class="col-sm-12">
                                    <select name="Event_time_zone" class="form-control" id="Event_time_zone">
                                        <option value="">Select Time Zone</option>
                                        <?php foreach ($timezone as $key => $value) { ?>
                                        <option value="<?php echo $value['Name'] ?>" <?php if($event['Event_time_zone']==$value['Name']){ ?>
                                          selected="selected" <?php } ?>><?php echo $value['Name']." (".$value['Desc'].")";  ?></option>
                                       <?php } ?>
                                    </select>
                                </div> 
                                <div class="col-sm-12">
                                    <h4>Time Zone Label</h4>
                                </div> 
                                <div class="col-sm-12">
                                    <?php $arr=array("UTC","UTC-12","UTC-11","UTC-10","UTC-9","UTC-8","UTC-7","UTC-6","UTC-5","UTC-4","UTC-3","UTC-2","UTC-1","UTC+12","UTC+11","UTC+10","UTC+9","UTC+8","UTC+7","UTC+6","UTC+5","UTC+4","UTC+3","UTC+2","UTC+1"); ?>
                                    <select name="Event_show_time_zone" onchange="show_time(this);" class="form-control" id="Event_show_time_zone">
                                        <?php foreach ($arr as $key => $value){ ?>
                                        <option value="<?php echo $value; ?>" <?php if($event['Event_show_time_zone']==$value){ ?>selected="selected" <?php } ?> ><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label style="padding-left:15px;">Your App Time</label>
                                <div class="col-sm-12">
                                    <?php 
                                    date_default_timezone_set("UTC");
                                    if(strpos($event['Event_show_time_zone'],"-")==true)
                                      { 
                                          $arr=explode("-",$event['Event_show_time_zone']);
                                          $intoffset=$arr[1]*3600;
                                          $intNew = abs($intoffset);
                                          $StartDateTime = date('Y-m-d H:i',strtotime(date('Y-m-d H:i'))-$intNew);
                                      }
                                      if(strpos($event['Event_show_time_zone'],"+")==true)
                                      {
                                          $arr=explode("+",$event['Event_show_time_zone']);
                                          $intoffset=$arr[1]*3600;
                                          $intNew = abs($intoffset);
                                          $StartDateTime = date('Y-m-d H:i',strtotime(date('Y-m-d H:i'))+$intNew);
                                      }
                                      if(empty($StartDateTime))
                                      {
                                        $StartDateTime=date("Y-m-d H:i",strtotime(date('Y-m-d H:i')));
                                      }
                                    ?>
                                    <input type="text" value="<?php  echo $StartDateTime; ?>" placeholder="" id="currnt_date" name="currnt_date" class="form-control name_group" disabled>
                                </div>
                                <h4 class="title" style="margin-top: 10%;margin-left: 15px;">Date Format</h4>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-12">
                                        <label class="radio-inline hover">
                                            <input type="radio" class="purple" value="0" <?php if ($event['date_format'] == '0') echo ' checked="checked"'; ?> name="date_format">
                                            Day/Month/Year
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="purple" value="1"  <?php if ($event['date_format'] == '1') echo ' checked="checked"'; ?> name="date_format">
                                            Month/Day/Year
                                        </label>    
                                    </div>
                                </div>
                                <?php if($is_hub_created=='1' && $hub_active=='1'){ ?>
                                <div class="col-sm-12">
                                    <h4>
                                        <label class="radio-inline hover" style="padding-left: 0px;">
                                            <span style="padding-right:5px;">Publish in Hub</span>
                                             <input type="checkbox" value="1" onchange="showtextbox();" <?php if ($event['hub_menu_show'] == '1') echo ' checked="checked"'; ?>  id="hub_btn" name="hub_btn">
                                        </label>
                                    </h4>
                                </div>
                                <div id="hub_title_textbox_div" class="form-group col-sm-12" <?php if ($event['hub_menu_show'] == '0'){ ?> style="display: none;" <?php } ?>>
                                    <label class="col-sm-4" for="form-field-1">
                                        Hub Button Title
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="input-group" style="width: 100%;">
                                            <input type="text" name="hub_btn_title" id="hub_btn_title" class="form-control" value="<?php echo $event['hub_menu_title']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                
                                <!-- <div class="panel-heading"> -->
                                    <h2 class="title" style="color: #5381ce;">Authorized Emails Options</h2>
                                <!-- </div> -->
                                <div class="col-sm-12">
                                    <h4>
                                        <label class="radio-inline hover">
                                            <input type="radio" value="1" class="" name="authorized_email" <?php if ($event['authorized_email'] == '1') echo ' checked="checked"'; ?>>
                                            Only allow Authorized Emails
                                        </label>
                                    </h4>
                                </div>
                                <div class="col-sm-12">
                                    <h4>
                                        <label class="radio-inline hover">
                                            <input type="radio" value="0" class="" name="authorized_email" <?php if ($event['authorized_email'] == '0') echo ' checked="checked"'; ?>>
                                            Allow any Email
                                        </label>
                                    </h4>
                                </div>
                                <div class="col-sm-12">
                                    <h4>
                                        <label class="radio-inline hover">
                                            <input id="show_login_screen" type="checkbox" class="" name="show_login_screen" <?php if ($event['show_login_screen'] == '1') echo ' checked="checked"'; ?>>
                                            Show Login Screen
                                        </label>
                                    </h4>
                                </div>
                                <?php if(count($iseventfreetrial) > 0){?>
                                <div class="col-sm-12" style="margin-bottom:20px;margin-top:20px;">
                                    <div class="col-md-9" style="padding-left:0px;">
                                        <button class="btn btn-theme_green btn-block" type="button" data-toggle="modal" data-target="#event_freetrial_messages">
                                            Update Your Access Settings
                                        </button>
                                    </div>
                                </div> 
                                <div id="event_freetrial_messages" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Launch your App to change the Privacy Settings</h4>
                                      </div>
                                      <div class="modal-body">
                                        <h4>You cannot change the Privacy Setting until you have launched your App. When you are ready please click Launch Your App on the bottom left of the screen and then you will be able to change your Privacy Settings.</h4>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <?php }else{ ?>
                                <div class="col-sm-12" style="margin-bottom:20px;margin-top:20px;">
                                    <div class="col-md-9" style="padding-left:0px;">
                                        <button class="btn btn-theme_green btn-block" type="submit">
                                            Update Your Access Settings
                                        </button>
                                    </div>
                                </div> 
                                <?php } ?> 
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="login_screen">
                    <div class="panel-body" style="padding: 0px;">
                        <form role="form" method="post" class="form-horizontal" id="form" action="<?=base_url().'Event/save_login_screen/'.$this->uri->segment(3)?>" enctype="multipart/form-data">
                            <div class="col-sm-12" style="padding-left: 0px;">
                                <div class="panel-heading">
                                    <h2 class="title">Login Screen</h2>
                                    <h4>Please upload image you would like to show on login screen here</h4>
                                </div>
                                <div class="col-sm-12">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                                        <div class="fileupload-new thumbnail" id="uploded_files_preview">
                                            <?php if (!empty($login_screen['login_screen_image'])) { ?>
                                            <img src="<?php echo base_url(); ?>assets/login_screen/<?php echo $this->uri->segment(3)."/".$login_screen['login_screen_image']; ?>" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div"></div>
                                        <?php if (!empty($login_screen['login_screen_image'])) { ?>
                                        <div class="user-edit-image-buttons">
                                            <a href="javascript:void(0);" class="btn btn-red fileupload-new" id="delete_img_fun" data-dismiss="fileupload" >
                                            <i class="fa fa-times"></i> Delete Image
                                            </a>
                                        </div>
                                        <?php } ?>
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file">
                                            <span class="fileupload-new">
                                            <i class="fa fa-picture"></i> Select image
                                            </span>
                                            <span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                            <input type="file" id="banner_Images" name="banner_Images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                            <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Please add in the text you would like to show on the login screen</h4>
                                    <textarea id="screen_content" name="screen_content"><?php echo $login_screen['login_screen_text']; ?></textarea>
                                </div><br/><br/>
                                <div class="col-sm-12">
                                    <div class="col-sm-3" style="padding: 0px;margin:0px;">
                                        <button class="btn btn-yellow btn-block" type="submit">
                                        Save <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>
<div id="addlanguage_popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add A New Language</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="add_language_form" action="<?php echo base_url().'Event/add_new_language/'.$event['Id']; ?>" method="post">
                        <div class="form-group">
                            <label class="control-label col-sm-12">
                                Language Name
                            </label>
                            <div class="col-sm-12">
                                <input type="text" name="lang_name" id="lang_name" placeholder="Enter Language Name" class="form-control required" autocomplete="off">
                                <span for="lang_name" id="lang_name_error" style="display: none;" class="help-block">This field is required.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12">
                                Language Icon
                            </label>
                            <div class="col-sm-12">
                                <select class="selectpicker form-control required" name="language_icon" data-live-search="true" id="language_icon">
                                    <option value="">Select Language Icon</option>
                                    <?php foreach ($countrylist as $key => $value) { ?>
                                    <option value="<?php echo $value['country_name'].'.png'; ?>" data-content="<span><img src='<?php echo base_url().'images/countryflags/'.$value['country_name'].'.png'; ?>'> <?php echo  $value['country_name']; ?> </span>"></option>
                                    <?php } ?>
                                </select>
                                <span for="language_icon" id="language_icon_error" style="display: none;" class="help-block">This field is required.</span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="button" id="save_add_language_btn" class="btn btn-green btn-block">
                                Save
                            </button>
                        </div>
                    </form>
                </div>    
            </div>
        </div>
    </div>
</div>
<div id="editlanguage_popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Language</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="edit_language_form" action="<?php echo base_url().'Event/edit_language/'.$event['Id']; ?>" method="post">
                        <input type="hidden" name="lang_id" id="lang_id" value=""> 
                        <div class="form-group">
                            <label class="control-label col-sm-12">
                                Language Name
                            </label>
                            <div class="col-sm-12">
                                <input type="text" name="lang_name" id="language_name" placeholder="Enter Language Name" class="form-control required" autocomplete="off">
                                <span for="language_name" id="language_name_error" style="display: none;" class="help-block">This field is required.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-12">
                                Language Icon
                            </label>
                            <div class="col-sm-12">
                                <select class="selectpicker form-control required" name="lang_icon" data-live-search="true" id="lang_icon">
                                    <option value="">Select Language Icon</option>
                                    <?php foreach ($countrylist as $key => $value) { ?>
                                    <option value="<?php echo $value['country_name'].'.png'; ?>" data-content="<span><img src='<?php echo base_url().'images/countryflags/'.$value['country_name'].'.png'; ?>'> <?php echo  $value['country_name']; ?> </span>"></option>
                                    <?php } ?>
                                </select>
                                <span for="lang_icon" id="lang_icon_error" style="display: none;" class="help-block">This field is required.</span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="button" style="margin-top: 15%;" id="save_edit_language_btn" class="btn btn-green btn-block">
                                Save
                            </button>
                        </div>
                    </form>
                </div>    
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function show_time(e)
{
    $.ajax({
        url:"<?php echo base_url().'Event/show_your_app_time/'.$this->uri->segment(3); ?>",
        data:{strtime: e.value,currnt_date:$('#currnt_date').val()},
        dataType: "json",
        type:"post",
        complete:function (result)
        {
            $('#currnt_date').val(result.responseText);
        }
    });
}
function showtextbox()
{
    if($("#hub_btn").is(':checked'))
    {
        $('#hub_title_textbox_div').show();
        if($.trim($('#hub_btn_title').val())=="")
        {
            //$('#hub_btn_title').val('Back To Hub');   
        }
    }
    else
    {
        $('#hub_title_textbox_div').hide();
    }
}
</script>
<!-- O screnn Wednesday 28 June 2017 03:25:12 PM IST !-->
<script id="uploadheaderimages_template_div" type="text/x-jQuery-tmpl">
    <li class="item-1" id='${divid}'>
        <div data-provides="fileupload" class="fileupload fileupload-new" >
            <span class="btn btn-file btn-light-grey">
                <i class="fa fa-folder-open-o"></i> 
                <span class="fileupload-new">Add Image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" name="header_image['${divid}']" onchange="geaderimageupload(this,'${divid}');">
            </span>
            <div class="file_upload_div">
                <a data-dismiss="fileupload" class="close-image fileupload-exists float-none" href="#" onclick="delete_banner_image(this,'${divid}','');">
                    &times;
                </a>
            </div>
        </div>
    </li>
</script>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#add_extra_images_button').click(function(){
    addbtnhide();
    if($('#header_images_contener .fileupload').length <= 4)
    {
        var dividarr=[{"divid":Number(totalbannerupload)+1}];
        $("#sortable").append($('#uploadheaderimages_template_div').tmpl(dividarr));
        totalbannerupload++;
    }
    else
    {
        alert('You can maximum 5 upload');
    }
});
var totalbannerupload="<?php echo count($image)==0 ? '1' : count($image); ?>";
function delete_banner_image(elem,imgname,key)
{ 
    addbtnhide();
    if(imgname!="")
    {
        $.ajax({
            url:"<?php echo base_url().'Event/delete_o_screen/'.$event['Id'] ?>",
            type:'post',
            data:'imagename='+imgname,
        });
        var del = '#'+key;
        $(del).remove();
    }
}
</script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image');
var banneritemno;
function addbtnhide()
{
  var total = $("#sortable li").length;
  if(total >= "5")
  {
    $('.extra-greay').hide();
  }
  else
  {
    $('.extra-greay').show();
  }
}
function geaderimageupload(elem,headerimagenumber)
{
    addbtnhide(); 
    banneritemno=headerimagenumber;
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
    var files = elem.files;
    var file;
    if (files && files.length) {
        file = files[0];
        if (/^image\/\w+$/.test(file.type)) {
            if($.trim(file.type)=="image/gif")
            {
                var divid="banner_item_div_"+banneritemno;
                var del = '#'+banneritemno;
                var formData = new FormData($('#eventedit_form')[0]);
                formData.append('gifimage',file);
                $.ajax({
                    url:"<?php echo base_url().'Event/save_o_screen_upload/'.$event['Id']; ?>",
                    type:'post',
                    data:formData,
                    dataType: 'html',
                    processData: false,
                    contentType: false,
                    beforeSend: function(){
                        $(del).empty();
                        $(del).append('<i class="fa fa-spinner fa-spin"></i>');
                        $('#empty').empty();
                        $('#empty').append('<i class="fa fa-spinner fa-spin"></i>');
                    },
                    success:function(data){
                        $(del).remove();
                        $('#empty').remove();
                        $('#sortable').append(data);
                        $('#test').empty();
                        $bannerimage.cropper('destroy');
                        $('#headermodelscopetool').data('bs.modal', null);
                    }
                });
            }
            else
            {
                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                }
                uploadedImageURL = URL.createObjectURL(file);
                $bannerimage.attr('src', uploadedImageURL);
                $('#add_extra_image_button_div').show();
                $('#headermodelscopetool').modal('show');
            }
        } else {
            window.alert('Please choose an image file.');
        }
    }
}
$(document).ready(function(){
    addbtnhide();
    var croppable = false;
    var $button = $('#upload_result_btn_header_crop');
    $button.on('click', function () {
        var croppedCanvas;
        if (!croppable) {
          return;
        }
        croppedCanvas = $bannerimage.cropper("getCroppedCanvas",{ 'width': 1242, 'height': 2208});
        var divid="banner_item_div_"+banneritemno;
        var textname="header_images_crope_text["+banneritemno+"]";
        $('#test').append('<input type="hidden" name="'+textname+'" value="'+croppedCanvas.toDataURL()+'">');
        var formData = new FormData($('#eventedit_form')[0]);
        var del = '#'+banneritemno;
        addbtnhide();
        $.ajax({
            url:"<?php echo base_url().'Event/save_o_screen_upload/'.$event['Id']; ?>",
            type:'post',
            data:formData,
            dataType: 'html',
            processData: false,
            contentType: false,
            beforeSend: function(){
                $(del).empty();
                $(del).append('<i class="fa fa-spinner fa-spin"></i>');
                $('#empty').empty();
                $('#empty').append('<i class="fa fa-spinner fa-spin"></i>');
            },
            success:function(data){
                $(del).remove();
                $('#empty').remove();
                $('#sortable').append(data);
                $('#test').empty();
                $bannerimage.cropper('destroy');
                $('#headermodelscopetool').data('bs.modal', null);
            }
        });
        $("#header_image").val('');
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        $bannerimage.cropper({
            aspectRatio: 9 / 16,
            built: function () {
              croppable = true;
            }
        });
    }).on('hidden.bs.modal',function(){
        $bannerimage.cropper('destroy');
    });
});
$('#save_edit_language_btn').click(function(){
    if($.trim($('#language_name').val())!="" && $.trim($('#lang_icon').val())!="")
    {
        $.ajax({
            url:"<?php echo base_url().'Event/check_language_name/'.$event['Id']; ?>",
            type:'post',
            data:"lang_name="+$.trim($('#language_name').val())+"&lang_id="+$.trim($('#lang_id').val()),
            success:function(data){
                var result=data.split('###');
                if($.trim(result[0])=="Success")
                {
                    $('#lang_icon_error').hide;
                    $('#lang_icon_error').parent().parent().addClass('has-success').removeClass('has-error');
                    $('#edit_language_form').submit();
                }
                else 
                {
                    $('#language_name_error').html(result[1]);
                    $('#language_name_error').parent().parent().addClass('has-error').removeClass('has-success');
                    $('#language_name_error').show();
                    $('#language_name').focus();
                }
            }
        });
        $('#language_name_error').hide;
        $('#language_name_error').parent().parent().addClass('has-success').removeClass('has-error');
    }
    else
    {
        if($.trim($('#language_name').val())=="")
        {
            $('#language_name_error').html('This field is required.');
            $('#language_name_error').parent().parent().addClass('has-error').removeClass('has-success');
            $('#language_name_error').show();
            $('#language_name').focus();
        }
        else
        {
            $('#language_name_error').hide;
            $('#language_name_error').parent().parent().addClass('has-success').removeClass('has-error');
        }
        if($.trim($('#lang_icon').val())=="")
        {
            $('#lang_icon_error').html('This field is required.');
            $('#lang_icon_error').parent().parent().addClass('has-error').removeClass('has-success');
            $('#lang_icon_error').show();
            $('#lang_icon').focus();
        }
        else
        {
            $('#lang_icon_error').hide;
            $('#lang_icon_error').parent().parent().addClass('has-success').removeClass('has-error');
        }
    }
});
function edit_lang(lang_arr)
{
    $('#edit_language_form').trigger('reset');
    $('#language_name_error').parent().parent().removeClass('has-error').removeClass('has-success');
    $('#language_name_error').hide();
    $('#language_name').focus();
    $('#lang_icon_error').parent().parent().removeClass('has-error').removeClass('has-success');
    $('#lang_icon_error').hide();
    if(lang_arr.lang_name=="English")
    {
        $('#language_name').attr('readonly', 'true');
    }
    else
    {
        $('#language_name').removeAttr('readonly');
    }
    $('#language_name').val(lang_arr.lang_name);
    $('#lang_id').val(lang_arr.lang_id);
    $('#lang_icon').val(lang_arr.lang_icon);
    $('#lang_icon').selectpicker('refresh');
    $('#editlanguage_popup').modal('show');
}
function delete_lang(lang_id)
{
    if(confirm("Are you sure to delete this Language?"))
    {
        window.location.href ="<?php echo base_url().'Event/delete_language/'.$event['Id'].'/' ?>"+lang_id;
    } 
}
$('#add_new_lang_btn').click(function(){
    $('#add_language_form').trigger('reset');
    $('#lang_name_error').parent().parent().removeClass('has-error').removeClass('has-success');
    $('#lang_name_error').hide();
    $('#lang_name').focus();
    $('#language_icon_error').parent().parent().removeClass('has-error').removeClass('has-success');
    $('#language_icon_error').hide();
    $('#addlanguage_popup').modal('show');
});
$('#save_add_language_btn').click(function(){
    if($.trim($('#lang_name').val())!="" && $.trim($('#language_icon').val())!="")
    {
        $.ajax({
            url:"<?php echo base_url().'Event/check_language_name/'.$event['Id']; ?>",
            type:'post',
            data:"lang_name="+$.trim($('#lang_name').val()),
            success:function(data){
                var result=data.split('###');
                if($.trim(result[0])=="Success")
                {
                    $('#language_icon_error').hide;
                    $('#language_icon_error').parent().parent().addClass('has-success').removeClass('has-error');
                    $('#add_language_form').submit();
                }
                else 
                {
                    $('#lang_name_error').html(result[1]);
                    $('#lang_name_error').parent().parent().addClass('has-error').removeClass('has-success');
                    $('#lang_name_error').show();
                    $('#lang_name').focus();
                }
            }
        });
        $('#lang_name_error').hide;
        $('#lang_name_error').parent().parent().addClass('has-success').removeClass('has-error');
    }
    else
    {
        if($.trim($('#lang_name').val())=="")
        {
            $('#lang_name_error').html('This field is required.');
            $('#lang_name_error').parent().parent().addClass('has-error').removeClass('has-success');
            $('#lang_name_error').show();
            $('#lang_name').focus();
        }
        else
        {
            $('#lang_name_error').hide;
            $('#lang_name_error').parent().parent().addClass('has-success').removeClass('has-error');
        }
        if($.trim($('#language_icon').val())=="")
        {
            $('#language_icon_error').html('This field is required.');
            $('#language_icon_error').parent().parent().addClass('has-error').removeClass('has-success');
            $('#language_icon_error').show();
            $('#language_icon_error').focus();
        }
        else
        {
            $('#language_icon_error').hide;
            $('#language_icon_error').parent().parent().addClass('has-success').removeClass('has-error');
        }
    }
});
$('#save_primary_language_btn').click(function(){
    $.ajax({
        url:"<?php echo base_url().'Event/make_primary_language/'.$event['Id']; ?>",
        type:'post',
        data:"lang_id="+$.trim($('#primary_language').val()),
        success:function(resultdata)
        {
            window.location.reload();
        }
    });
});
function show_category(elem) 
{
    if($(elem).val()=='1' || $(elem).val()=='15')
    {
        $('#search_by_category_and_modules_btn').attr('disabled','disabled');
        $('#search_by_category_and_modules_btn').html("Searching <i class='fa fa-spinner fa-spin'></i>");
        $.ajax({
            url:"<?php echo base_url().'Event/get_modules_category/'.$event['Id']; ?>",
            type:'post',
            data:"menu_id="+$.trim($(elem).val()),
            success:function(resultdata)
            {
                $('#modules_category').html(resultdata);
                $('#search_by_category_and_modules_btn').removeAttr('disabled');
                $('#search_by_category_and_modules_btn').html('Search');
                $('#category_label').show();
                $('#modules_category').parent().show();
            }
        });
    }
    else
    {
        $('#category_label').hide();
        $('#modules_category').parent().hide();
    }
}
$('#search_by_category_and_modules_btn').click(function(){
    $('#search_by_category_and_modules_btn').attr('disabled','disabled');
    $('#search_by_category_and_modules_btn').html("Searching <i class='fa fa-spinner fa-spin'></i>");
    $.ajax({
        url:"<?php echo base_url().'Event/get_modiles_content_data/'.$event['Id']; ?>",
        type:'post',
        data:'menu_id='+$('#modules_name').val()+'&module_category='+$('#modules_category').val(),
        error:function(errordata) {
            $('#search_by_category_and_modules_btn').removeAttr('disabled');
            $('#search_by_category_and_modules_btn').html('Search');
        },
        success:function(resultdata) {
            $('#show_modules_name_div').html($('#modules_name :selected').text());
            $('#content_menu_id').val($('#modules_name :selected').val());
            $('#main_div_contaner_all_table').html(resultdata);
            $('#search_by_category_and_modules_btn').removeAttr('disabled');
            $('#search_by_category_and_modules_btn').html('Search');
        }
    });
});
</script>
<div id="o-boarding">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="<?php echo base_url().'Event/save_onboarding_settings/'.$event['Id']; ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Onboarding Screens</h4>
                    </div>
                    <div class="modal-body">
                        <div class="checkbox">
                            <label>
                                <p>Change onboarding screens when an App user taps the screen </p>
                                <input type="checkbox" name="change_on_tap" <?=($o_settings['change_on_tap']) ? 'checked' : '' ?>/> 
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <p>Change onboarding screens automatically after a specific time time period</p>
                                <input type="checkbox" name="change_screen_on_seconds" <?=($o_settings['change_screen_on_seconds']) ? 'checked' : '' ?>/> 
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Number of seconds</label>
                            <select class="form-control" name="seconds">
                            <?php 
                                $i = 1;
                                while ($i <= 10) 
                                    {   
                                        if($o_settings['seconds'] == $i)
                                        {
                                            echo "<option selected value='$i'>$i</option>";
                                        }
                                        else
                                        {
                                            echo "<option value='$i'>$i</option>";
                                        }                      
                                        $i++;
                                    }
                                ?>    
                            </select>
                        </div>
                        <div class="radio">
                            <p>How you want to show screen :</p>
                            <label class="rdmargin">
                                <span>Once:</span>
                                <input type="radio" name="show_once" value="1" <?=($o_settings['show_once'] == '1') ? 'checked' : '' ?>>
                            </label>
                            <label class="rdmargin"> 
                                <span>Always:</span>
                                <input type="radio" name="show_once" value="0" <?=($o_settings['show_once'] == '0') ? 'checked' : '' ?>>
                            </label>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Save changes"/></form>
                    </div>
                </form>  
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        /*pen*/
        $('#language_name,#lang_name').keydown(function (e) {
              if (e.shiftKey || e.ctrlKey || e.altKey) {
                  e.preventDefault();
              } else {
                  var key = e.keyCode;
                  if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                      e.preventDefault();
                  }
              }
          });
        /*pen*/
        if($('input:radio[name=authorized_email]:checked').val() == '1')
        {   
            $('#show_login_screen').prop('checked', true);
            $('#show_login_screen').attr('onclick','return false;');
        }
        $('input:radio[name=authorized_email]').change(function() {
            if (this.value == '1')
            {
               $('#show_login_screen').prop('checked', true);
               $('#show_login_screen').attr('onclick','return false;');
            }
            else if (this.value == '0')
            {   
               if (!$('#show_login_screen').is(":checked"))
               {
                    $('#show_login_screen').prop('checked', false);
               } 
               $('#show_login_screen').removeAttr('onclick');
            }
        });
    });
</script>
<style type="text/css" media="screen">
.panel-heading .title{
    color: #5381ce;
} 
.col-sm-12 h4{
    font-weight: bold;
} 
#add_language_form .bootstrap-select.form-control:not([class*="span"]) {
    width: 100%;
    height: auto;
}
#multi_language_settings tbody td input.form-control, #multi_language_content tbody td input.form-control{
    width: auto;
}
#multi_language_settings .table-responsive , #multi_language_content .table-responsive {
    margin: 15px 0;
}
#multi_language_settings h2 {
    margin: 0px 0 30px;
}
#multi_language_content textarea.form-control {
    min-height: 120px;
}
</style>
<script type="text/javascript">
$(document).on('click','#delete_img_fun',function(e){
    e.preventDefault();
    if (confirm("Are you sure to delete this?"))
    {
        window.location.href ="<?php echo base_url().'Event/save_login_screen/'.$this->uri->segment(3).'/true'; ?>";
    }
    else 
    {
        return false;
    }

});
$('#save_access_key_btn').click(function(){
    if($('#access_key').val() == '')
    {
        $("#error_access_key_empty").css("display", "block");
        $("#error_access_key").css("display", "none");
    }
    else
    {
        $.ajax({
            url:"<?php echo base_url().'Event/save_access_key/'.$event['Id']; ?>",
            type:'post',
            data:"access_key="+$.trim($('#access_key').val())+"&"+"api_sync="+$('#api_sync').is(":checked"),
            success:function(resultdata)
            {
                var result=resultdata.split('###');
                if($.trim(result[0])=="Success")
                {
                    window.location.reload();
                }
                else 
                {
                    $("#error_access_key_empty").css("display", "none");
                    $("#error_access_key").css("display", "block");
                }
            }
        });
    }
});

</script>