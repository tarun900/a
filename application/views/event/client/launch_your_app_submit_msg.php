<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/launchyourappviewpage.css?<?php echo time(); ?>">
<div id="launch_your_app_form" class="launch_your_app_form_messages_div">
	<h4>Launch Your App</h4>
	<div class="col-sm-12">
		<?php if($status=="succeeded"){ ?>
		<h2>Congratulations! Your App is being launched!</h2>
		<h4>Your App will be available to download from the App Store and Play Store in approximately 7 days. We will let you know by email when they are available to download.</h4>
		<?php }else{ ?>
		<h2>Something Went Wrong</h2>
		<h4>Please Refresh Your page And Try Again.</h4>
		<?php } ?>
	</div>
</div>