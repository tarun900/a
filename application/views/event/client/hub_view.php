<?php $acc_name=$this->session->userdata('acc_name'); 
$user = $this->session->userdata('current_user'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
        	<div class="tabbable">
    			<ul id="myTab2" class="nav nav-tabs">
    				<li class="active">
                        <a href="#menu_setting" data-toggle="tab">
                            Menu Setting
                        </a>
                	</li>
                	<li>
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View Hub
                        </a>
                	</li>
    			</ul>
        	</div>
        	<div class="panel-body">
        		<div class="tab-content">
        			<?php if(!empty($this->session->flashdata('error'))){ ?>
        				<div class="alert alert-danger">
        					<?php echo $this->session->flashdata('error'); ?>
        				</div>
        			<?php } ?>
        			<?php if(!empty($this->session->flashdata('msg'))){ ?>
        				<div class="alert alert-success">
        					<?php echo $this->session->flashdata('msg'); ?>
        				</div>
        			<?php } ?>
        			<div class="tab-pane fade active in" id="menu_setting">
        				<div class="row padding-15">
	                        <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Event/showhubmenusetting/<?php echo $event_id; ?>" enctype="multipart/form-data">
	                            <div class="col-md-12 alert alert-info">
	                                <h5 class="space15">Menu Title & Image</h5>
	                                <div class="form-group">
	                                    <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="50" placeholder="Title" value="<?php echo $title; ?>">
	                                </div>
	                                <div class="form-group">
	                                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
	                                    <input type="hidden" name="Id" id="Id" value="<?php echo $Id; ?>">
	                                </div>

	                                <div class="form-group">
	                                   <div class="fileupload fileupload-new" data-provides="fileupload">
	                                        <div class="fileupload-new thumbnail"></div>
	                                        <div class="fileupload-preview fileupload-exists thumbnail"></div>
	                                        <div class="user-edit-image-buttons">
	                                             <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
	                                                  <input type="file" name="Images">
	                                             </span>
	                                             <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
	                                                  <i class="fa fa-times"></i> Remove
	                                             </a>
	                                        </div>
	                                   </div>
	                                </div>
	                                <div class="form-group">
	                                    <!--<label class="checkbox-inline">
	                                        <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php //if($is_feture_product=="1") { ?> checked="checked" <?php //} ?>>
	                                        Create Home Tab
	                                    </label>
	                                    <span class="help-block" for="is_feture_product" style="display:none;">This field is required.</span>-->
	                                    <label class="checkbox-inline">
	                                        <input type="checkbox" value="1" class="square-orange" name="show_photos" id="show_photos" <?php if($show_photos=="1") { ?> checked="checked" <?php } ?>>
	                                        Photos Show In Hub
	                                    </label><br>
	                                    <label class="checkbox-inline">
	                                        <input type="checkbox" value="1" class="square-orange" name="show_publicmsg" id="show_publicmsg" <?php if($show_publicmsg=="1") { ?> checked="checked" <?php } ?>>
	                                        Public Messages Show In Hub
	                                    </label><br>
	                                    <label class="checkbox-inline">
	                                        <input type="checkbox" value="1" class="square-orange" name="show_attendee" id="show_attendee" <?php if($show_attendee=="1") { ?> checked="checked" <?php } ?>>
	                                        Attendee Show In Hub
	                                    </label>
	                                    <?php if($img !='') { ?>
	                                    <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
	                                    <?php } ?>
	                                </div>
	                                <input type="hidden" value="1" name="is_feture_product">
	                                <div class="form-group">
	                                    <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
	                                    <div class="col-sm-5">
	                                        <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
	                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
	                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
	                                        </select>
	                                    </div>
	                                </div>
	                                <br/><br/><br/><br/>
	                                <button type="submit" class="btn btn-primary">
	                                    Submit
	                                </button>
	                                <?php if(!empty($menusettingid)){ ?>
	                                	<button type="button" onclick="deletefromhub();" id="btn_remove_hub" style="border-radius: 15px;" class="btn btn-red">
	                                    	Remove from Hubs
	                                	</button>
	                                <?php } ?>
	                            </div>
	                        </form>
                    	</div>
        			</div>
        			<div class="tab-pane fade" id="view_events">
        				<div id="viewport" class="iphone">
                            <iframe id="displayframe123" name="displayframe123" height="480" width="320" src="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"></iframe>
	                    </div>
	                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
	                    <div id="viewport_images" class="iphone-l" style="display:none;">
	                           <a target="_blank" href="<?php echo base_url(); ?>Hub/<?php echo $acc_name; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
	                    </div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function deletefromhub() {
		if(confirm("Are you sure Delete From Hub?"))
        {
            window.location.href="<?php echo base_url().'Event/detelefromhub/'.$event_id.'/'.$menusettingid; ?>";
        }
	}
</script> 
