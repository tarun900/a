<!-- start: PAGE CONTENT -->
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
</script>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add Your <span class="text-bold">App</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                   
                   <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Unique URL <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-4" style="padding-right: 0px;">
                            <input type="text" placeholder="Unique URL" class="form-control" id="Subdomain" name="Subdomain" onblur="checksubdomain();">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            App Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-4" style="padding-right: 0px;">
                            <input type="text" placeholder="Event Name" id="Event_name" name="Event_name" class="form-control name_group">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Start Date <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-4">
                            <div class="input-group" style="padding-left: 1%;">
                                <input type="text" name="Start_date" id="startDate" contenteditable="false" class="form-control">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            End Date <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-4">
                            <div class="input-group" style="padding-left: 1%;">
                                <input type="text" name="End_date" id="endDate" contenteditable="false" class="form-control">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            </div>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Status <span class="symbol required"></label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="1" checked="checked" name="Status">
                                Active
                            </label>

                            <label class="radio-inline">
                                <input type="radio" class="purple" value="0" name="Status">
                                Inactive
                            </label>    
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Type <span class="symbol required"></label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="1" checked="checked" name="Event_type">
                                Private
                            </label>

                            <label class="radio-inline">
                                <input type="radio" class="purple" value="2" name="Event_type">
                                Public
                            </label> 

                            <label class="radio-inline">
                                <input type="radio" class="purple" value="3" name="Event_type">
                                Public open
                            </label> 
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Time <span class="symbol required"></label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="1" checked="checked" name="Event_time">
                                Active
                            </label>

                            <label class="radio-inline">
                                <input type="radio" class="purple" value="0" name="Event_time">
                                Inactive
                            </label> 
                        </div>
                    </div> -->
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<!-- end: PAGE CONTENT-->