<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Event</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                   
                   <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Subdomain <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="col-sm-4" style="padding:0px;">
                                <input type="text" placeholder="Subdomain" class="form-control" id="Subdomain" name="Subdomain" onblur="checksubdomain();">
                            </div>
                            <div class="col-sm-5" style="padding:0px;">
                                <span class="help-inline col-sm-12"> <i class="fa fa-info-circle"></i> <?php echo ".".$_SERVER['HTTP_HOST']; ?> </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
							Event Name <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
							<input type="text" placeholder="Event Name" id="Event_title" name="Event_title" class="form-control name_group">
						</div>
					</div>

					<div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Start Date <span class="symbol required" id="showdatetime_remark"></span>
                        </label>
                        <div class="col-sm-9 input-group" style="padding-left: 1.7%;">
                            <input type="text" name="Start_date" id="Start_date" data-date-format="yyyy-mm-dd hh:mm:ss" data-date-viewmode="years" class="form-control date-picker">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        </div>
                        <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                    </div>

    
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                                Background Color 
                        </label>
                       <div class="col-sm-9">
                            <input type="text" value="" placeholder="Background Color" id="Background_color" name="Background_color" class="color {hash:true} form-control name_group">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->