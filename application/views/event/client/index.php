<script src="https://code.jquery.com/jquery-1.8.1.min.js"></script>
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('speaker_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_templates_data'); ?> Successfully.
                </div>
                <?php } ?>
                 <div class="col-md-2 pull-right">
                    <a style="top: 7px;right:41%;" class="btn btn-green list_page_btn" href="<?php echo base_url(); ?>Event/add"><i class="fa fa-plus"></i> Create New App</a>                
                    <a style="top: 7px;" class="btn btn-green list_page_btn" href="javascript: window.history.go(-2)">Back</a>
                 </div>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="">
                            <a href="#archive_events" data-toggle="tab">
                                Archive Events
                            </a>
                        </li>
                        <li class="active">
                            <a href="#latest_events" data-toggle="tab">
                                Active Events
                            </a>
                        </li>
                        <li class="">
                            <a href="#feature_events" data-toggle="tab">
                                Future Events
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="archive_events">
                            <?php  if($Archive_event[0]['Id'] !='') { ?>

                            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
                            <span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>

                            <?php  }else { } ?>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>
                                            </th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0;$i<count($Archive_event);$i++) { ?>
                                        <tr>
                                            <td>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" name="event_name[]" value="<?php echo $Archive_event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                                    </label>
                                                </div>
                                            </td>
                                            <td><?php echo $i+1; ?></td>
                                            <td><?php echo $Archive_event[$i]['Event_name']; ?> <span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo base_url(); ?>Event/edit/<?php echo $Archive_event[$i]['Id']; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>App/<?php echo $Archive_event[$i]['Subdomain']; ?>">View</a></span></td>
                                            <td><?php echo $Archive_event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Archive_event[$i]['End_date']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                               </table>
                            </div>
                        </div>
                        <div class="tab-pane fade active in" id="latest_events">
                            <?php if($Event[0]['Id'] !='') { ?>

                            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
                            <span class="label btn-blue pull-right margin-left-10 tooltips" onclick="check_edit_event()" data-placement="top" data-original-title="Edit Event"><i class="fa fa-edit"></i></span>
                            <span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>

                            <?php  }else { } ?>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Fundraising</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0;$i<count($Event);$i++) {
                                            //$str=  $login_user[0]->Email.'###'.$login_user[0]->Password;
                                            /*$encrypt_method = "AES-256-CBC";
                                            $secret_key = $login_user[0]->Email;
                                            $secret_iv = $login_user[0]->Password;
                                            $key = hash('sha256', $secret_key);
                                            $iv = substr(hash('sha256', $secret_iv), 0, 16);
                                            $output = openssl_encrypt($str, $encrypt_method, $key, 0, $iv);
                                            $output = urlencode(base64_encode($output));*/
                                            //$output = urlencode(base64_encode($str));
                                            
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" name="event_name[]" value="<?php echo $Event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                                    </label>
                                                </div>
                                            </td>
                                            <td><?php echo $i+1; ?></td>
                                            <td><a href="<?php echo base_url(); ?>Event/edit/<?php echo $Event[$i]['Id']; ?>"><?php echo $Event[$i]['Event_name']; ?></a><span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo base_url(); ?>Event/edit/<?php echo $Event[$i]['Id']; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>App/<?php echo $Event[$i]['Subdomain']; ?>">View</a></span></td>
                                            <td>
                                                <?php 
                                                $fundraising_val = explode(',', $Event[$i]['Checkbox_values']);
                                                if(in_array(20, $fundraising_val))
                                                {
                                                    //echo '<a href="'.base_url().'fundraising/'.$Event[$i]['Subdomain'].'/panel/?k='.$output.'" target="_blank" >Fundraising Panel</a>';
                                                    echo '<a href="'.base_url().'fundraising/'.$Event[$i]['Subdomain'].'/panel" target="_blank" >Fundraising Panel</a>';
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $Event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Event[$i]['End_date']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="feature_events">
                            <?php  if($Feature_event[0]['Id'] !='') { ?>

                            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
                            <span class="label btn-blue pull-right margin-left-10 tooltips" onclick="check_edit_event()" data-placement="top" data-original-title="Edit Event"><i class="fa fa-edit"></i></span>
                            <span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>

                            <?php  }else { } ?>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Fundraising</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php for($i=0;$i<count($Feature_event);$i++) { 
                                            //$str=  $login_user[0]->Email.'###'.$login_user[0]->Password;
                                            /*$encrypt_method = "AES-256-CBC";
                                            $secret_key = $login_user[0]->Email;
                                            $secret_iv = $login_user[0]->Password;
                                            $key = hash('sha256', $secret_key);
                                            $iv = substr(hash('sha256', $secret_iv), 0, 16);
                                            $output = openssl_encrypt($str, $encrypt_method, $key, 0, $iv);
                                            $output = urlencode(base64_encode($output));*/
                                            //$output = urlencode(base64_encode($str));
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" name="event_name[]" value="<?php echo $Feature_event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                                    </label>
                                                </div>
                                            </td>
                                            <td><?php echo $i+1; ?></td>
                                            <td><a href="<?php echo base_url(); ?>Event/edit/<?php echo $Feature_event[$i]['Id']; ?>"><?php echo $Feature_event[$i]['Event_name']; ?></a><span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo base_url(); ?>Event/edit/<?php echo $Feature_event[$i]['Id']; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>App/<?php echo $Feature_event[$i]['Subdomain']; ?>">View</a></span></td>
                                            <td>
                                                <?php 
                                                $fundraising_val = explode(',', $Feature_event[$i]['Checkbox_values']);
                                                if(in_array(20, $fundraising_val))
                                                {
                                                    //echo '<a href="'.base_url().'fundraising/'.$Feature_event[$i]['Subdomain'].'/panel/?k='.$output.'" target="_blank" >Fundraising Panel</a>';
                                                    echo '<a href="'.base_url().'fundraising/'.$Feature_event[$i]['Subdomain'].'/panel" target="_blank" >Fundraising Panel</a>';
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $Feature_event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Feature_event[$i]['End_date']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->


<script type="text/javascript">
    /*function delete_event(id)
    {
        if(confirm("Are you sure to delete this?"))
        {            
            window.location.href ="<?php echo base_url(); ?>event/delete/"+id;
        }
    }*/
    /*function delete_checked_values(id)
    {
        if(confirm("Are you sure to delete these Events?"))
        {            
            window.location.href ="<?php echo base_url(); ?>event/delete/"+id +'';
        }
    }*/
    function edit_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/edit/" + id;
    }
    function display_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/detail/" + id;
    }
    function check_edit_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            edit_event($('input[type=checkbox]:checked').val());
        }
    }
    function check_display_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            display_event($('input[type=checkbox]:checked').val());
        }
    }
    
    function check_delete_events()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else
        {
            if (confirm('Are you sure you want to delete this?')) 
            {
                event.preventDefault();

                $.each($('input[name="event_name[]"]:checked'), function() {  
                    $.ajax({
                      type: "POST",
                      url: '<?php echo base_url(); ?>Event/delete',
                      data: 
                        { selected: $(this).val() },
                      success: function(data){              
                        setTimeout(function () {
                                        
                            window.location.href = window.location.href;
                         
                        }, 1000);

                        //$('#ajax_message').show().html('Successfully deleted.');
                      },              
                    });           

                });
            }
        }
    }

</script>