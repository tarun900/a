<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js"></script>
<script>
	jQuery(document).ready(function() {
		FormElements.init();
          FormValidator.init();
	});
        
        
        $(document).on("click", (".remove_size"), function() {
            if(confirm("Are you sure to delete this?"))
                {
                    var forthis = $(this);
                    var cur_id = $(this).parent().parent().find('.size_id').val();
                        $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>Event/delete_size",
                        data: 'id='+cur_id,
                        success: function(result){
                            forthis.closest('.form-group').slideUp("slow", function() { $(this).remove(); } )
                           }
                        });
                }
        });
        
        function add_size()
        {   
            var adddiv = '<div class="form-group"><label class="col-sm-2 control-label" for="form-field-1"></label><div class="col-sm-3"><input type="text" placeholder="Size" id="Size" name="Size[]" class="form-control"></div><div class="col-sm-2 control-label" for="form-field-1"><a href="javascript:;" class="btn btn-danger btn-xs  pull-left remove_size"><i class="fa fa-minus"></i> Remove Size</a></div></div>';
            $("#size_div").after(adddiv);
        }

        $(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });

</script>


<script>
    function checksubdomain()
    {      
        var sendflag="";
        $.ajax({
        url : '<?php echo base_url(); ?>Event/checksubdomain',
        data :'Subdomain='+$("#Subdomain").val()+'&idval='+$('#idval').val(),
        type: "POST",  
        async: false,
        success : function(data)
        {
            var values=data.split('###');
            if(values[0]=="error")
            {   
                $('#Subdomain').parent().removeClass('has-success').addClass('has-error');
                $('#Subdomain').parent().find('.control-label span').removeClass('ok').addClass('required');
                $('#Subdomain').parent().find('.help-block').removeClass('valid').html(values[1]);
                sendflag=false;
            }
            else
            {
                $('#Subdomain').parent().removeClass('has-error').addClass('has-success');
                $('#Subdomain').parent().find('.control-label span').removeClass('required').addClass('ok');
                $('#Subdomain').parent().find('.help-block').addClass('valid').html(''); 
                sendflag=true;
            }
        }
    });
    return sendflag;
    }
</script>
