<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Event</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
                            Event Name <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
                            <input type="text" value="<?php echo $event['Event_name']; ?>" placeholder="Event Name" id="Event_name" name="Event_name" class="form-control name_group">
						</div>
					</div>
                    <div class="form-group" style="display:none;">
						<label class="col-sm-2 control-label" for="form-field-1">Common Name <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">
							<input type="text" value="<?php echo $event['Common_name']; ?>" placeholder="Common Name" id="Common_name" name="Common_name" class="form-control name_group">
						</div>
					</div>
                    <div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">Description</label>
						<div class="col-sm-9">
							<textarea placeholder="Description"  id="Description" name="Description" class="form-control"><?php echo $event['Description']; ?></textarea>
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">Category</label>
                            <div class="col-sm-9">
                                <select multiple="multiple" id="form-field-select-4" class="form-control search-select" name="Category[]">
                                    <?php
                                    for($i=0;$i<count($event['Category']);$i++){ $cid[] = $event['Category'][$i]['Id']; }
                                    for($i=0;$i<count($all_Category);$i++){ 
                                        ?>
                                        <option value="<?php echo $all_Category[$i]['Id']; ?>" <?php if(count($event['Category']) != 0){if(in_array($all_Category[$i]['Id'], $cid)){ ?> selected="selected" <?php }} ?>><?php echo $all_Category[$i]['Name'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-2 control-label" for="form-field-1">Images</label>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($event['Images']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" name="images[]" multiple>
                                </span>
                            </div>
                            <?php
                            for($i=0;$i<count($images_array);$i++)
                            {
                            ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" src="<?php echo base_url(); ?>assets/timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>&h=150&w=209"><a class="btn btn-red remove_image" href="javascript:;" style=""><i class="fa fa-times fa fa-white"></i></a>
                                    <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php
                            }
                            ?>                                                                                                
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1"></label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->