<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Event</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
                    
			<div class="panel-body">
                <?php if($this->session->flashdata('event_qty')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_qty'); ?> Successfully.
                </div>
                <?php } ?>
                <form role="form" method="post" class="form-horizontal" id="mine-form-number" action="" enctype="multipart/form-data">                     
					<div class="form-group">
						<label class="col-sm-2 control-label" for="form-field-1">
							Event: <span class="symbol required"></span>
						</label>
						<div class="col-sm-9">                  
	                        <select id="form-field-select-3" class="form-control search-select event" name="Event">
	                                <option value="">-- Select Event --</option>
	                            <?php foreach($Event as $k=>$v) { ?>
	                                <option value="<?php echo $v['Id']; ?>"><?php if(@$v['Event_name'] != NULL && @$v['Event_name'] != '') {echo $v['Event_name'];}else{ echo $v['Common_name'];}  ?></option>
	                            <?php } ?>
	                        </select>
						</div>
					</div>
                    <div class="event-select"></div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->