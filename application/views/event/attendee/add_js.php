<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>

<link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
<script src="<?php echo base_url(); ?>assets/js/jquery.mtz.monthpicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		FormElements.init();
                FormValidator.init();
	});
       
        $('.qty').change(function(){
             
            var errorgot = 0;
            var intRegex = /^\d+$/;
              if(intRegex.test($(this).val()) && $(this).val() != '') {
                    $(this).parent().parent().removeClass('has-error');
                    $(this).parent().find('.help-block').remove();
              }
              else
              {
                    $(this).parent().parent().removeClass('has-success').addClass('has-error');
                    $(this).parent().find('.control-label span').removeClass('valid');
                    $(this).parent().find('.help-block').remove();
                    if($(this).val() != '')
                    {
                        $(this).after('<span class="help-block">Please enter a valid number.</span>');
                    }
                    else
                    {
                        $(this).after('<span class="help-block">This field is required.</span>');
                    }
                    errorgot++;
              }
                if(errorgot>0)
                {
                    e.preventDefault();
                }
           
        });
        
        
       
       $("#mine-form-number").submit(function(e){
       	  var fill_error = 0;
           $('.months-qty').each(function(value){               
               $(this).find('input[type=text]').each(function(){
                   if($(this).val()!='')
                   {
                       fill_error++;
                   }
               });
           if(fill_error > 0)
           {
            if($(this).find('.add-qty').val() != null && $(this).find('.add-qty').val() != '')
             {
                var errorgot = 0;
                var intRegex = /^\d+$/;
                if(intRegex.test($(this).find('.add-qty').val())) {
                    $(this).removeClass('has-error');
                    $(this).find('.help-block').remove();
                }
                else
                {
                    $(this).removeClass('has-success').addClass('has-error');
                    $(this).find('.control-label span').removeClass('valid');
                    $(this).find('.help-block').remove();
                    $(this).find('.add-qty').after('<span class="help-block">Please enter a valid number.</span>');
                    errorgot++;
                }
                if(errorgot>0)
                {
                    e.preventDefault();
                }
              }
              
              $(this).find('.alert').parent().remove();
           }           
           });
           
           /*if(fill_error == 0)
           {
           		 $(this).find('.qty_empty_error').parent('.form-group').addClass('has-error');
                $(this).find('.qty_empty_error').after('<span class="help-block">Please enter a valid Qty.</span>');
                e.preventDefault();
           }*/
        });
       
        $("#mine-form").submit(function(e){
           $('.qty').each(function(value){ 
            var errorgot = 0;
            var intRegex = /^\d+$/;
              if(intRegex.test($(this).val()) && $(this).val() != '') {
                    $(this).parent().parent().removeClass('has-error');
                    $(this).parent().find('.help-block').remove();
              }
              else
              {
                    $(this).parent().parent().removeClass('has-success').addClass('has-error');
                    $(this).parent().find('.control-label span').removeClass('valid');
                    $(this).parent().find('.help-block').remove();
                    if($(this).val() != '')
                    {
                        $(this).after('<span class="help-block">Please enter a valid number.</span>');
                    }
                    else
                    {
                        $(this).after('<span class="help-block">This field is required.</span>');
                    }
                    errorgot++;
              }
                if(errorgot>0)
                {
                    e.preventDefault();
                }
           });
        });
       
        $('.event').change(function() {            
            var id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>Event/details",
                dataType: "json",
                data: { id: id},
                success: function(result)
                {
                    if(id != null && id != '')
                    {
                        $(".event-select").html($("#eventsuppldetail").tmpl(result));
                    }   
                    else
                    {
                        $(".event-select").html("");
                    }
                    options = {
                        pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                        selectedYear: <?php echo date('Y'); ?>,
                        startYear: <?php echo date('Y'); ?>,
                        finalYear: <?php echo (date('Y') + 5); ?>,
                        monthNames: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                    };
                    $('.month').monthpicker(options).on('monthpicker-show', function (e, year) {                        
                        var curr_this = $(this);
                        var curr_val = $(this).val();
                        $(curr_this).parent().find(".month").text("");
                        $(curr_this).parent().find(".month").val("");
                        if(curr_val == null || curr_val == '')
                        {
                            var not_allow_month = new Array();
                            for ( var i = 0; i < <?php echo date('m'); ?>; i++ ) {
                                        not_allow_month[i] = i;
                            }
                            $(this).monthpicker('disableMonths', not_allow_month);
                        }
                        else
                        {
                            $(this).monthpicker('disableMonths', []);
                            var not_allow_month = new Array();
                            var selected_year = curr_val.split('-')[0];                            
                            if (selected_year === '<?php echo date('Y'); ?>') {
                                for(var i=0;i<<?php echo date('m'); ?>;i++){
                                    not_allow_month[i] = i;
                                }
                                $(this).monthpicker('disableMonths', not_allow_month);
                            }
                        }
                    }).on('monthpicker-click-month', function (e, month) {
                        var size_id = $(this).parent().parent().find('.size_id').val();
                        check_qty($(this).val(),id,size_id);                        
                    }).on('monthpicker-change-year', function (e, year) {
                        $(this).monthpicker('disableMonths', []);
                        var not_allow_month = new Array();
                        if (year === '<?php echo date('Y'); ?>') {
                            for ( var i = 0; i < <?php echo date('m'); ?>; i++ ) {
                                not_allow_month[i] = i;
                            }
                            $(this).monthpicker('disableMonths', not_allow_month);
                        }
                    });
                }
              });
        });
                
        function check_qty(selected_date,event_id,size_id)
        {
            var selected_year = selected_date.split('-')[0];
            var selected_month = selected_date.split('-')[1];
            $.ajax({
               type: 'POST',
               url: "<?php echo base_url();?>Event/check_availablity",
               dataType: 'json',
               data: {selected_month:selected_month, selected_year:selected_year,event_id:event_id,size_id:size_id},
               success: function (result) {
                        if(result == "there")
                        {
                            $(".month").text("");
                            $(".month").val("");
                            alert("Entry of this month is already there, Please update");                            
                        }
                    }
            });
        }

</script>

<script id="eventsuppldetail" type="text/x-jQuery-tmpl">
    {{if Event_name}}
        <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                        Botanical Name:
                </label>
                <div class="col-sm-9">
                        ${Event_name}
                </div>
        </div>
    {{/if}}
    {{if Common_name}}
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                    Common Name:
            </label>
            <div class="col-sm-9">
                    ${Common_name}
            </div>
        </div>
    {{/if}}
    {{if Description}}
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">
                    Description:
            </label>
            <div class="col-sm-9">
                    ${Description}
            </div>
        </div>
    {{/if}}

	<div class="form-group months-qty">
		<label class="col-sm-2 control-label" for="form-field-1">&nbsp;</label>
		{{each(prop, val) Month}}
			<div class="col-sm-2">${val}</div>		
		{{/each}}
	</div>
        
    {{each(prop, val) Size}}
        <div class="form-group months-qty">
            <label class="col-sm-2 control-label" for="form-field-1">
                    <b>Size</b> ${val.Size}:
            </label>
           
            {{each(prop1, val1) Month}}
            	<div class="col-sm-2">               	
                	<input type="text" placeholder="qty" name="size[qty][${prop}][]" class="add-qty col-sm-12">
                	<input type="hidden" name="size[month][${prop}][]" value="${val1}" >
                	<input type="hidden" class="size_id" name="size[id][${prop}][]" value="${val.Id}">
            	</div>
				{{/each}}
        </div>
    {{/each}}
       
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-8 qty_empty_error"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1">

            </label>
            <div class="col-md-4">
                <button class="btn btn-yellow btn-block" type="submit">
                    Submit <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>    
</script>