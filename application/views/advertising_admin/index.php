<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php //if($user->Role_name=='Client'){ ?>
                <a style="top:7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Advertising_admin/add/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Create Advert </a>
                <?php //} ?>

                <?php if($this->session->flashdata('advertising_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Advertisement <?php echo $this->session->flashdata('advertising_data'); ?> Successfully.
                </div>
                <?php } ?>
                
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#advertising_list" data-toggle="tab">
                                Advert Banners
                            </a>
                        </li>
                         <li class="">
                            <a href="#settings" data-toggle="tab">
                                Settings
                            </a>
                        </li>
						<li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="advertising_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width " id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Advert Name</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($advertising_list);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() ?>Advertising_admin/edit/<?php echo $advertising_list[$i]['Event_id']; ?>/<?php echo $advertising_list[$i]['Id']; ?>"><?php echo $advertising_list[$i]['Advertisement_name']; ?></a></td>
                                       <td>
                                            <a href="<?php echo base_url() ?>Advertising_admin/edit/<?php echo $advertising_list[$i]['Event_id']; ?>/<?php echo $advertising_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_advertising('<?php echo $advertising_list[$i]['Event_id']; ?>','<?php echo $advertising_list[$i]['Id']; ?>')" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                        <div class="tab-pane fade " id="settings">
                            <div class="row">
                                <div class="col-md-12">
                                <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Advertising_admin/settings/<?php echo $advertising_list[0]['Event_id']; ?>" enctype="multipart/form-data">
                                    
                                    <div class="form-group">
                                        <!-- <div class="col-md-12">
                                            <h4>
                                            Show in All Modules 
                                            <label class="">
                                                <input  type="checkbox" class="" <?php echo  ($advertising_list[0]['show_all'] == '1') ? 'checked' : ''?> value="1"  name="show_all">
                                            </label>
                                            </h4>
                                        </div> -->
                                        <br>
                                        <div class="col-md-12">
                                            <h4>
                                           Make Advert Sticky 

                                            <label class="">
                                                <input  <?php echo  ($event['adv_show_sticky'] == '1') ? 'checked' : ''?> type="checkbox" class="" value="1"  name="show_sticky">
                                                
                                            </label>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                             <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                                </div>
                            </div>
                        </div>
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
            $("#advert").datatable();
        });
</script>

<script>
    function delete_advertising(Event_id,id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Advertising_admin/delete_advertising/" + Event_id + "/" + id;
        }
    }
</script>
