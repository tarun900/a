<?php $acc_name=$this->session->userdata('acc_name');?>
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />
<!-- start: PAGE CONTENT -->
<div class="row advertising">
    <div class="col-sm-12">
        <div class="panel panel-white">
             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#advertise_list" data-toggle="tab">
                            Edit Advertising
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
					
                </ul>
            </div>

 
            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="advertise_list">
                        <div class="advertisement" style="background:#eee;padding:15px;">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Advertising_admin/edit/<?php echo $advertising_by_id[0]['Event_id']; ?>/<?php echo $advertising_by_id[0]['Id']; ?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Advertisement Name <span class="symbol required"></span></label>
                                <div class="col-sm-10">
                                   <input type="text" name="Advertisement_name" class="form-control required" placeholder="Place you Advertisement Name here." value="<?php echo $advertising_by_id[0]['Advertisement_name']; ?>">
                                </div>
                            </div>
                            <div style="clear:both;"></div><br/>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Choose Option </label>
                                <div class="col-sm-10">
                                     <!-- <label><input type="radio" <?php if (!empty($advertising_by_id[0]['Google_header_adsense'])) echo ' checked="checked"'; ?> name="colorCheckbox" value="red" id="myradio" style=" left: 0;position: relative;"> Google Adsense</label> -->
                                  <label><input type="radio" <?php if (!empty($advertising_by_id[0]['H_images']) || !empty($advertising_by_id[0]['F_images'])) echo ' checked="checked"'; ?> name="colorCheckbox" value="green" id="myradiobu" style=" left: 0;position: relative;"> Header footer Images</label>

                                </div>
                            </div>

                            <div style="clear:both;"></div><br/>
                            <div class="form-group red box" <?php if (empty(!$advertising_by_id[0]['Google_header_adsense'])){ ?> style="display:block;" <?php } ?>>
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Google Adsense Header</label>
                                <div class="col-sm-10">
                                    <textarea style="width: 100%;margin-top: 2%;height: 300px;" id="summernote" name="Google_header_adsense"><?php echo $advertising_by_id[0]['Google_header_adsense']; ?></textarea>
                                </div><br/>
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Google Adsense Footer </label>
                                <div class="col-sm-10">
                                    <textarea style="width: 100%;margin-top: 2%;height: 300px;" id="summernote" name="Google_footer_adsense"><?php echo $advertising_by_id[0]['Google_footer_adsense']; ?></textarea>
                                </div>
                            </div>
                            <div style="clear:both;"></div>

                            <div class="green box" <?php if (!empty($advertising_by_id[0]['H_images'])){ ?>
                            style="display:block;" <?php } ?>>
                            
                            <div class="form-group">
                                <label style="padding-left: 0px;" class="col-sm-2" for="form-field-1">Header Images</label>
                                <div class="modal fade" id="h_image_image_cropping_tool_models" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Footer Images</h4>
                                        </div>
                                        <div class="modal-body">
                                            <section>
                                                <div class="demo-wrap" style="display:none;">
                                                    <div class="container">
                                                        <div class="grid">
                                                            <div class="col-1-2">
                                                                <div id="vanilla-demo"></div>
                                                            </div>
                                                            <div class="col-1-2">
                                                                <strong>Vanilla Example</strong>
                                                                <div class="actions">
                                                                    <button class="vanilla-result">Result</button>
                                                                    <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                                    <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="h_image_preview_crop_tool"></div>
                                                    <div class="col-sm-3 crop-btn">
                                                        <button class="btn btn-green btn-block" id="h_image_crop_data" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>         
                                            </section>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                   <?php $h_img_array = json_decode($advertising_by_id[0]['H_images']); ?>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail"></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" id="h_image_crop_tool_preview"></div>
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                 <input type="file" name="H_images" id="H_images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_h_image_data">
                                                 <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                        <input type="hidden" name="h_image_crop_data_textbox" id="h_image_crop_data_textbox"> 
                                    </div>
                                    <?php for($i=0;$i<count($h_img_array);$i++) { ?>
                                    <div class="col-sm-3 fileupload-new thumbnail center" style="height: auto;">
                                          <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $h_img_array[$i]; ?>">
                                          <input type="hidden" name="old_h_images[]" value="<?php echo $h_img_array[$i]; ?>">
                                          <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_h_image" id="delete_h_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form').submit();">
                                            <i class="fa fa-times"></i> Remove
                                          </label>
                                    </div>
                                    <?php } ?> 
                                </div>
                            </div>
                            <div style="clear:both;"></div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Header Link <span class="symbol required"></span></label>
                                <div class="col-sm-10">
                                   <input type="text" class="form-control" id="Header_link"  placeholder="Place you header redirecting link here.For Example https://www.headerlink.com" value="<?php if($advertising_by_id[0]['Header_link']=="" || $advertising_by_id[0]['Header_link']=="https://") { echo ""; } else { echo $advertising_by_id[0]['Header_link']; } ?>">
                                </div>
                            </div>
                            <div style="clear:both;"></div><br/>

                            <div class="form-group">
                                <label style="padding-left: 0px;" class="col-sm-2" for="form-field-1">Footer Images</label>
                               <div class="modal fade" id="f_image_image_cropping_tool_models" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Footer Images</h4>
                                        </div>
                                        <div class="modal-body">
                                            <section>
                                                <div class="demo-wrap" style="display:none;">
                                                    <div class="container">
                                                        <div class="grid">
                                                            <div class="col-1-2">
                                                                <div id="vanilla-demo"></div>
                                                            </div>
                                                            <div class="col-1-2">
                                                                <strong>Vanilla Example</strong>
                                                                <div class="actions">
                                                                    <button class="vanilla-result">Result</button>
                                                                    <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                                    <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="f_images_crop_tool_previews"></div>
                                                    <div class="col-sm-3 crop-btn">
                                                        <button class="btn btn-green btn-block" id="f_image_crop_data" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>         
                                            </section>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                               <div class="col-sm-9">
                                   <?php $f_img_array = json_decode($advertising_by_id[0]['F_images']); ?>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail"></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" id="f_image_crop_previews"></div>
                                        <div class="user-edit-image-buttons">
                                            <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                 <input type="file" name="F_images" id="F_images">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_f_image_data">
                                                 <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                        <input type="hidden" name="f_image_crop_data_textbox" id="f_image_crop_data_textbox"> 
                                    </div>
                                    <?php for($i=0;$i<count($f_img_array);$i++) { ?>
                                    <div class="col-sm-3 fileupload-new thumbnail center" style="height: auto;">
                                          <img alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $f_img_array[$i]; ?>">
                                          <input type="hidden" name="old_f_images[]" value="<?php echo $f_img_array[$i]; ?>">
                                          <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_f_image" id="delete_f_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form').submit();">
                                            <i class="fa fa-times"></i> Remove
                                          </label>
                                    </div>
                                    <?php } ?> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Footer Link <span class="symbol required"></span></label>
                                <div class="col-sm-10">
                                   <input type="text" class="form-control" id="Footer_link"   placeholder="Place you footer redirecting link here.For Example https://www.footerlink.com" value="<?php if($advertising_by_id[0]['Footer_link']=="" || $advertising_by_id[0]['Footer_link']=="https://") { echo ""; } else { echo $advertising_by_id[0]['Footer_link']; } ?>">
                                </div>
                            </div>
                            <div style="clear:both;"></div><br/>

                            </div>

                            <?php 
                                $string_cms_id = $advertising_by_id[0]['Cms_id'];
                                $Cms_id =  explode(',', $string_cms_id); 

                                $string_menu_id = $advertising_by_id[0]['Menu_id'];
                                $Menu_id =  explode(',', $string_menu_id); 
                             ?>
                            <div style="clear:both;"></div><br/>
                            <?php $modu_id=array('26','8','18','5','28','20','27','12','13','14'); ?>
                            <div class="form-group" id="toggle_standard_modules" style="<?php echo ($advertising_by_id[0]['show_all'] == '1') ? 'display: none' : '' ?>">
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Standard modules to show advert<span class="symbol required"></span></label>
                                <div class="col-sm-10">
                                    <select multiple="multiple" style="margin-top:-8px;" class="select2-container select2-container-multi form-control menu-section-select search-select required" name="Menu_id[]" id="std_modules">
                                        <option <?php echo in_array('0', $Menu_id) ? 'selected="selected"' : ''; ?> id="menu0" value="0">Home</option>

                                        <?php foreach ($menu_toal_data as $key => $value) { 
                                             if(!in_array($value['id'],$modu_id))
                                           { 
                                            ?>
                                        <option <?php echo in_array($value['id'], $Menu_id) ? 'selected="selected"' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div style="clear:both;"></div><br/>

                            <div class="form-group" >
                                <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Custom modules to show advert</label>
                                <div class="col-sm-10">
                                    <select multiple="multiple" id="s2id_form-field-select-4" class="select2-container select2-container-multi form-control search-select" name="Cms_id[]">
                                        <?php foreach ($cms_list as $key => $value) { ?>
                                        <option <?php echo in_array($value['Id'], $Cms_id) ? 'selected="selected"' : ''; ?> value="<?php echo $value['Id']; ?>"><?php echo $value['Menu_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                             <div style="clear:both;"></div>
                            <br/><br/>
                            <!-- <div class="form-group" >
                                <label class="control-label col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Show advert sticky</label>
                                <div class="col-sm-10">
                                    <label class="">
                                        <input type="checkbox" class="" <?php echo ($advertising_by_id[0]['show_sticky'] == '1') ? 'checked' : '' ?> value="1"  name="show_sticky">
                                    </label>
                                </div>
                            </div> -->

                             <div style="clear:both;"></div>
                            <br/><br/>
                            <div class="form-group" >
                                <label class="control-label col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Show in all modules</label>
                                <div class="col-sm-10">
                                    <label class="">
                                        <input type="checkbox" id="show_all" class="" <?php echo ($advertising_by_id[0]['show_all'] == '1') ? 'checked' : '' ?> value="1"  name="show_all">
                                    </label>
                                </div>
                            </div>
                           
                            <button type="submit" class="btn btn-primary">
                                    Update 
                            </button>
                            </form>
                        </div>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
				
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<style type="text/css">
    #select2-result-label-7
    {
        display: none;
    }
</style>
<!-- end: PAGE CONTENT-->
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js?<?php echo time(); ?>"></script>
<script>
    Demo.init();
</script>
<script type="text/javascript">
$("#show_all").on('change',function(){
    if($(this).prop('checked'))
    {   
        $("#toggle_standard_modules").hide();
        $("#std_modules").removeClass("required");
    }
    else
    {
        $("#toggle_standard_modules").show();
        $("#std_modules").addClass("required");
    }
});
if($("#show_all").prop('checked'))
{   
    $("#toggle_standard_modules").hide();
    $("#std_modules").removeClass("required");
}
else
{
    $("#toggle_standard_modules").show();
    $("#toggle_standard_modules").addClass("required");
}

$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
$('#remove_f_image_data').click(function(){
    $('#f_image_crop_data_textbox').val('');
});
$('#remove_h_image_data').click(function(){
    $('#h_image_crop_data_textbox').val('');
});
</script>


<script type="text/javascript">
    $(document).ready(function(){
        document.getElementById("Header_link").setAttribute("name", "Header_link");
        document.getElementById("Footer_link").setAttribute("name", "Footer_link");
        /*if($('#myradio').val()=='red')
        {
            $('.green').show();
            $('.red').hide();
            $('#myradiobu').prop('checked',true);
        }
        else
        {
            $('.red').show();
            $('.green').hide();
            $('.red').prop('checked',true);
        }*/
        $('input[type="radio"]').click(function(){
            
            if($(this).attr("value")=="red"){
                $(".red").show();
                $(".green").hide();
                document.getElementById("Header_link").setAttribute("name", "Header");
                document.getElementById("Footer_link").setAttribute("name", "Footer");
            }
            if($(this).attr("value")=="green")
            {
                $(".green").show();
                $(".red").hide();
                document.getElementById("Header_link").setAttribute("name", "Header_link");
                document.getElementById("Footer_link").setAttribute("name", "Footer_link");
            }
        });
        if($('#myradiobu').prop('checked') == true)
        {
            $(".green").show();
            $(".red").hide();
            document.getElementById("Header_link").setAttribute("name", "Header_link");
            document.getElementById("Footer_link").setAttribute("name", "Footer_link");
        }
        else
        {
            $(".green").hide();
        }
    });
</script>

<style type="text/css">
    .box{
        display: none;
    }
</style>