<!doctype html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html>
<!--<html manifest="example.appcache">-->
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>
		<?php
			if($this->uri->segment(3) == '1511'):
				j($event_templates);
			endif;
			for($i=0;$i<count($event_templates);$i++) 
            { 
                echo $event_templates[$i]['Event_name'];
            } 
	    ?>
        </title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0 minimal-ui">
		<meta name="apple-mobile-web-app-capable" content="yes minimal-ui">
		<meta name="apple-mobile-web-app-status-bar-style" content="black minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<meta name="mobile-web-app-capable" content="yes">

		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
		
		<!-- end: META -->
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<?php echo $css; ?>
        <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/coustom.css?<?php echo time(); ?>">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-f.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
		<!--<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

		<script type="text/javascript">
		/*$(window).load(function() 
		{
			$(".se-pre-con").fadeOut("slow");
		});*/
		</script>

		<style type="text/css">

			.no-js #loader { display: none;  }
			.js #loader { display: block; position: absolute; left: 100px; top: 0; }
			.se-pre-con {
				position: fixed;
				left: 0px;
				top: 0px;
				width: 100%;
				height: 100%;
				z-index: 9999;
				background: url("<?php echo base_url(); ?>assets/images/Preloader_41.gif") center no-repeat #fff;
			}
		</style>
		<!-- end: CORE CSS -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
    <body style="background:#eee;<?php
      if($this->uri->segment(1)=="Exhibitors" && $event_templates[0]['show_topbar_exhi'] == '0')
      {?>
      	overflow:scroll !important;overflow-y:hidden; 
      <?php }
      else if($this->uri->segment(1)=="Presentation" && $this->uri->segment(3)=="View_presentation")
      {
           ?> overflow:hidden; <?php
           
      } ?>" <?php
      if(($this->uri->segment(1)=="Presentation" && $this->uri->segment(3)=="View_presentation") || ($this->uri->segment(1)=="Exhibitors" && $event_templates[0]['show_topbar_exhi'] == '0'))
      {
           ?>
           class="sidebar-close presentation_body"
           <?php
      } ?>>
         <!--  <div class="se-pre-con"></div> -->
         <audio id="chatAudio">
              <source src="<?php echo base_url(); ?>assets/images/notify.ogg" type="audio/ogg">
              <source src="<?php echo base_url(); ?>assets/images/notify.mp3" type="audio/mpeg">
              <source src="<?php echo base_url(); ?>assets/images/notify.wav" type="audio/wav">
         </audio>
         <!-- start: SLIDING BAR (SB) -->
		<div id="slidingbar-area">
		</div>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: PAGESLIDE LEFT -->
			<a class="closedbar inner hidden-sm hidden-xs" href="#">
			</a>
			<nav id="pageslide-left" class="pageslide inner">
				<?php echo $sidebar; ?>
			</nav>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<?php echo $header; ?>					
				
						<!-- end: BREADCRUMB -->
						<?php echo $content; ?>
						<div class="subviews">
							<div class="subviews-container"></div>
						</div>
					</div>
                         <?php echo $footer; ?>
				</div>
				<!-- end: PAGE -->
                    
			</div>
            
			
			<!-- end: MAIN CONTAINER -->
		</div>
		
		<!--[if gte IE 9]><!-->
		<script src="https://code.jquery.com/jquery-1.8.3.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>
		<!--<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>-->
		<!--<![endif]-->
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>		
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/velocity/jquery.velocity.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/pages-gallery.js"></script>
		<!--<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/pages-timeline.js"></script>-->
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jquery.appear/jquery.appear.js"></script>
		<!-- <script src="<?php echo base_url(); ?>assets/js/pages-user-profile.js"></script> -->
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->		
		<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<!-- start: CORE JAVASCRIPTS  -->
		<script src="<?php echo base_url(); ?>assets/js/index.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main-f.js"></script>

		<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script> 
		<!--<script src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>-->
		<script src="<?php echo base_url() ?>assets/js/image_slider/owl.carousel.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script> 
		<script src="<?php echo base_url(); ?>assets/js/form-wizard.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.colorbox.js"></script>   
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>  
        <script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/subview.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/pages-timeline.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <?php echo $js; ?>
		
		<!--<script src="<?php echo base_url(); ?>assets/image_map/jquery.rwdImageMaps.min.js"></script>-->
		<!--<script src="<?php echo base_url(); ?>assets/image_map/assets/jquery.rwdImageMaps.min.js"></script>-->
		<!--<script>
		jQuery(document).ready(function(e) {
			jQuery('img[usemap]').rwdImageMaps();
			
			jQuery('area').on('click', function() {
				alert($(this).attr('alt') + ' clicked');
			});
		});
		</script>-->
                
		<script>
		    jQuery(document).ready(function() {
		    	<?php if($this->uri->segment(3) != 'View_presentation' && $this->uri->segment(4) != 'View_presentation' && $this->uri->segment(1) != 'Surveys') { ?>
		    	// SVExamples.init();
		    	<?php } ?>
		    	
		        Main.init();
		        SVExamples.init();
                Timeline.init();
		        //ComingSoon.init(); 
		        //PagesUserProfile.init();
		        FormWizard.init();
		        <?php if($this->uri->segment(1)=="Exhibitors" && $event_templates[0]['show_topbar_exhi'] == '0'):?>
                jQuery('body').addClass('sidebar-close');
                jQuery('#section_msg').attr('style','display:none');
                console.log('he');
                jQuery('.agenda_content').attr('style','margin: 0 auto !important;padding: 0 5px;');  
            	<?php endif; ?>
		    });
		</script>
		<!-- end: CORE JAVASCRIPTS  -->
	</body>
	<!-- end: BODY -->
</html>