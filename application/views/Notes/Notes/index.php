<?php 
    $k = array_rand($advertisement_images);
    $advertisement_images = $advertisement_images[$k];
    $acc_name = $this->session->userdata('acc_name');
?>

<div class="row margin-left-10">
    <?php  $user = $this->session->userdata('current_user');if(!empty($user)) { ?>
<?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2):
                  continue;
              endif;
          ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/formbuilder/formvalidation.js"></script>
<script type="text/javascript">
  $(document).ready(function() 
  {
       FormValidator.init();

  });  
</script>
        <div class="col-sm-6 col-sm-offset-3">
        <?php
          $json_data = false;
          $formid="form";
          if($intKey==0)
          {
            $formid="form";  
          }
          else
          {
            $intKey+=1;
            $formid=$formid."".$intKey;
          }
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Notes/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
        <?php endforeach;  
        endif;  
        ?>
    <?php } else 
      {
        ?>
        
            <div class="errorHandler alert alert-info center" style="display: block;margin: 3% 10%;">
                <i class="fa fa-remove-sign"></i> 
                 Login or Sign Up to proceed. To sign up or login tap the Sign Up button on the top right of the screen.
             </div>
        
    <?php  }
      ?>
</div>

<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('6', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
   
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
       <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>


  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>


<div class="agenda_content notes-content">
    <div class="panel panel-white">
        <div class="panel-body">
          <?php if(!empty($notes_list)) { ?>
          <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width dataTable" id="sample_1">
                    <thead>
                        <tr>
                            <th>Heading</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;$i<count($notes_list);$i++) { ?>
                        <tr>
                            <td><?php echo $notes_list[$i]['Heading']; ?></td>
                            <td><?php echo $notes_list[$i]['Created_at']; ?></td>
                           <td>
                                <a href="<?php echo base_url() ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/edit/<?php echo $notes_list[$i]['Id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php } else { ?>
          <div class="tab-content">
           <!-- <span>No Notes available for this event.</span> -->
           <div class="col-sm-2" style="margin-bottom: 15px;">
            <a href="<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/add" class="btn btn-green btn-block">Add Notes</a>
           </div>
          </div>
        <?php } ?>
         
    </div>
</div>
<script type="text/javascript">
  function add_advertise_hit()
  {
     
      $.ajax({
                url : '<?php echo base_url().Agenda."/".$Subdomain ?>/add_advertise_hit',
                type: "POST",  
                async: false,
                success : function(data1)
                {

                }
             });
  }
</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" >
    jQuery(document).ready(function()
     {
       //Main.init();
     });
</script>
