<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
<?php $curr=$this->input->post('url');
$acc_name = $this->session->userdata('acc_name');
?>
<div id="boxes">
  <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window"> Add Notes
  <button type="button" class="close" data-dismiss="modal" id="Close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div id="lorem">
      <div class="panel-body notes_popup">
            <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url(); ?>Notes/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/add" enctype="multipart/form-data">
                <div id="notes_edit">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="form-field-1">
                            Heading <span class="required">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Heading" id="Heading" name="Heading" value="" class="form-control name_group" required>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-sm-2" for="form-field-1">
                            Description <span class="required">*</span>
                        </label>
                        <div class="col-sm-9">
                            <textarea style="width:100%;height:200px;" placeholder="Description" id="summernote" name="Description" class="summernote" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="hidden" id="Created_at" value="<?php echo date('Y-m-d H:i:s'); ?>" name="Created_at" class="form-control name_group">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-green btn-block" type="submit">
                                Add <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="url1" value="<?php echo $curr;?>">
            </form>
        </div> 
    </div>
  </div>
  <div style="width: 1478px; font-size: 32pt; color:white; height: 602px; display: none; opacity: 0.8;" id="mask"></div>
</div>
<script src="<?php echo base_url(); ?>assets/js/main1.js"></script>
<script type="text/javascript">
    $(function() {

       $('#Close').click(function() {
            /*$.ajax({
                    url: "<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>/notes_close/"+<?php echo $id;?>,
                    data: 'flag='+<?php echo $close_flag?>,
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                        var values=result.split('###');
                        if($.trim(values[0])=="sucess")
                        {*/
                            window.history.go(-1);
                            //window.location.href="<?php echo base_url(); ?>Notes/<?php echo $acc_name.'/'.$event_templates[0]['Subdomain']; ?>";
                        /*}
                    }
    
               });*/


          });
       }); 
</script>


