<?php 

// echo "<pre>"; print_r($socialwall_heading); exit();

$acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#activity_feed" data-toggle="tab">
                                Options
                            </a>
                        </li>
                        <li>
                            <a href="#social_feed" data-toggle="tab">
                                Social Feed
                            </a>
                        </li>
                        <li>
                            <a href="#internal_feed" data-toggle="tab">
                                Internal Feed
                            </a>
                        </li>

                        <li>
                            <a href="#sponsored_post">
                                Sponsored Posts
                            </a>
                        </li>
                        <li>
                            <a href="#survey" data-toggle="tab">
                                Survey
                            </a>
                        </li>
                        
                        <li>
                            <a href="#settings" data-toggle="tab">
                                Settings
                            </a>
                        </li>

                        <li>
                            <a href="#socialwall" data-toggle="tab">
                                Social Wall
                            </a>
                        </li>
                        <?php if($user->Role_name=='Client' || $user->Role_name=='Admin'){ ?>
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <?php } ?>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                        <?php if ($event_id == '1511') { ?>
                        <li class="">
                            <a id="repull1" href="#repull" data-toggle="tab">
                                Repull
                            </a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane active fade in" id="activity_feed">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Activity_admin/save_permission/<?php echo $event_id; ?>" enctype="multipart/form-data">
                                <div class="col-md-12">
                                    <h4 class="space15 center">Modules</h4>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="grey selectall" <?php if(!in_array('0',$arrPermission) && count($arrPermission) > 0){ ?> checked="checked" <?php } ?>>
                                                All  
                                            </label>
                                        </div>
                                    </div>    
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="public" <?php if($arrPermission['public']==1): echo 'checked="checked"';endif; ?> value="1" class="grey foocheck">
                                                Show Public Messages  
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="comments" value="1" <?php if($arrPermission['comments']==1): echo 'checked="checked"';endif; ?> class="grey foocheck">
                                                Show Comments  
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="photo" <?php if($arrPermission['photo']==1): echo 'checked="checked"';endif; ?> value="1" class="grey foocheck">
                                                Show Photos 
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="show_session" <?php if($arrPermission['show_session']==1): echo 'checked="checked"';endif; ?> value="1" class="grey foocheck">
                                                Show Session Saves  
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="check_in" value="1"  <?php if($arrPermission['check_in']==1): echo 'checked="checked"';endif; ?> class="grey foocheck">
                                                Show Session Check Ins  
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="rating" value="1" class="grey foocheck" <?php if($arrPermission['rating']==1): echo 'checked="checked"';endif; ?>>
                                                Show Session Ratings
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="update_profile" value="1" class="grey foocheck" <?php if($arrPermission['update_profile']==1): echo 'checked="checked"';endif; ?>>
                                                Show Profile Updates
                                            </label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="auction_bids" value="1" class="grey foocheck" <?php if($arrPermission['auction_bids']==1): echo 'checked="checked"';endif; ?>>
                                                Show Auction Bids
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        <div class="col-md-4">
                                            <button class="btn btn-primary" type="submit">
                                                Submit 
                                            </button>
                                        </div>
                                        <div class="col-md-8">
                                            <p>
                                                &nbsp;
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="social_feed">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Activity_admin/save_social_feed/<?php echo $event_id; ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1">
                                        Twitter Hashtags
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $arrPermission['hashtag']; ?>" style="width: 80%;margin-bottom: 10px;" placeholder="Enter Hashtags" id="hashtags" name="hashtags" class="form-control name_group required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1">
                                        FaceBook Page Name
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $arrPermission['fbpage_name']; ?>" style="width: 80%;margin-bottom: 10px;" placeholder="Enter FaceBook Page Name" id="fbpage_name" name="fbpage_name" class="form-control name_group required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1">
                                        FaceBook Access Token
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $arrPermission['fb_access_token']; ?>" style="width: 80%;margin-bottom: 10px;" placeholder="Enter FaceBook Access Token" id="fb_access_token" name="fb_access_token" class="form-control name_group required">
                                    </div>
                                </div>

                                 

                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1"></label>
                                    <div class="col-sm-2">
                                        <input type="submit" class="btn btn-green btn-block" value="Save" name="save_hasetag" id="save_hasetag"> 
                                    </div>
                                </div>    
                            </form>
                        </div>  
                    </div>
                    <div class="tab-pane fade in" id="internal_feed">
                        <div class="table-responsive custom_checkbox_table" style="overflow-x:auto;">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Activity</th>
                                        <th>Activity Content</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($activity_data as $key => $activity) { ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td>
                                            <?php echo ucfirst($activity['Firstname'])." ".$activity['Lastname'];  ?>
                                        </td>
                                        <td>
                                            <?php echo ucfirst($activity['activity']);  ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($activity['activity_no']=='2')
                                            {
                                                if(count(json_decode($activity['image'],TRUE)) > 0)
                                                {
                                                    $img=json_decode($activity['image'],TRUE);
                                                    $img1=1; 
                                                    foreach ($img as $ikey => $ivalue) { ?>
                                                        <div class="msg_photo">
                                                            <a class="colorbox_<?php echo $key; ?>" href="<?php echo base_url().'assets/user_files/'.$ivalue; ?>">
                                                                <?php if($img1=='1'){ ?>
                                                                View Photos
                                                                <?php } $img1++; ?>
                                                            </a>
                                                        </div> 
                                                    <?php 
                                                    } 
                                                } 
                                            }
                                            else if($activity['activity_no']=='4')
                                            {
                                                for ($i=1;$i<=5;$i++) { 
                                                    if($i<=$activity['rating'])
                                                    {
                                                        $img="star-on.png";
                                                    }
                                                    else
                                                    {
                                                        $img="star-off.png";
                                                    } ?>
                                                    <img src="<?php echo base_url().'assets/images/'.$img; ?>">
                                                <?php 
                                                } 
                                                echo " On This Session ".$activity['Message'];
                                            }
                                            else if($activity['activity_no']=='5')
                                            {
                                                ?>
                                                <div class="msg_photo">
                                                    <a class="colorbox_<?php echo $key; ?>" href="<?php echo base_url().'assets/user_files/'.$activity['Logo']; ?>">
                                                    View Profile Picture
                                                    </a>
                                                </div> 
                                                <?php   
                                            }
                                            else
                                            {
                                                echo $activity['Message'];
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php if($activity['activity_no']=='1' || $activity['activity_no']=='2' || $activity['activity_no']=='6' || $activity['activity_no']=='9' || $activity['activity_no']=='10'){ ?>
                                            <a href="javascript:;" onclick="delete_activity(<?php echo $activity['id']; ?>,<?php echo $activity['activity_no']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="sponsored_post">
                        <div class="table-responsive custom_checkbox_table" style="overflow-x:auto;">
                            <a href="<?php echo base_url(); ?>Activity_admin/addSponsoredPost/<?=$this->uri->segment(3)?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">Add Sponsored Post</a>
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Advert</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($advert as $key => $value):?>
                                        <tr>
                                            <td><?=++$i;?></td>
                                            <td><?=$value['title']?></td>
                                            <td><img src="<?=base_url()?>assets/activity_advert/<?=$this->uri->segment(3).'/'.$value['image']?>" style="height: 40px;"></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Activity_admin/editSponsoredPost/<?php echo $value['event_id'].'/'.$value['id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_advert(<?=$value['id']?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="survey">

                        <div class="table-responsive custom_checkbox_table" style="overflow-x:auto;">
                            <a href="<?php echo base_url(); ?>Activity_admin/add/<?=$this->uri->segment(3)?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;">Add Survey</a>
                            <a href="<?php echo base_url(); ?>Activity_admin/export/<?=$this->uri->segment(3)?>" class="btn btn-primary  pull-right " style="margin-bottom:5px;margin-right:5px;">Export Survey</a>
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Question</th>
                                        <th>Hide</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($Surveys);$i++) { ?>
                                    <tr>
                                        <td><?=++$j;?></td>
                                        <td><a href="<?php echo base_url() ?>Activity_admin/edit/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id'].'/'.$sid; ?>"><?php echo $Surveys[$i]['Question']; ?></a></td>
                                        <td>
                                            <input type="checkbox" <?php if($Surveys[$i]['show_question']=='0'){ ?> checked="checked" <?php } ?> onchange='hidequestion("<?php echo $Surveys[$i]['Id']; ?>");' name="show_question" value="<?php echo $Surveys[$i]['Id']; ?>" id="show_question_<?php echo $Surveys[$i]['Id'];?>" class="">
                                           <label for="show_question_<?php echo $Surveys[$i]['Id'];?>"></label>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Activity_admin/edit/<?php echo $Surveys[$i]['Event_id'].'/'.$Surveys[$i]['Id'].'/'.$sid; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_survey(<?=$Surveys[$i]['Id']?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="settings">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Activity_admin/activity_share_icon_setting/<?php echo $event_id; ?>">
                                <div class="col-md-12">
                                    <div class="col-md-12" style="padding-left: 0px;">
                                        <label class="checkbox-inline">
                                        <input type="checkbox" name="activity_share_icon_setting" value="1" class="grey foocheck" <?php if($activity_share_icon_setting['activity_share_icon_setting']==1): echo 'checked="checked"'; endif; ?>>
                                               Show Activity Icons and Share Button
                                            </label>
                                    </div>    
                                    
                                    <div class="row">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        <div class="col-md-4">
                                            <button class="btn btn-primary" type="submit">
                                                Submit 
                                            </button>
                                        </div>
                                        <div class="col-md-8">
                                            <p>
                                                &nbsp;
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="tab-pane fade in" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                        </div>
                    </div>

                    <div class="tab-pane fade in" id="repull"> 
                        <input type="hidden" id="repull_event_id" name="repull_event_id" value="<?php echo $event_id;?>">
                        <input type="hidden" id="page_no" name="page_no" value="1">
                        <input type="hidden" id="lang_id" name="lang_id" value="1200">
                        <input type="hidden" id="user_id" name="user_id" value="0">
                        <?php 
                            $checked_repull = '';
                            if ($socialwall_heading[0]['activity_sync'] == 1) {
                                $checked_repull = 'checked';
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="button" class="btn btn-primary" id="repull_button" name="repull_button" value="Sync Now">
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" id="auto_pull" name="auto_pull" <?php echo $checked_repull;?> >
                                <label><strong> Auto Sync</strong></label>
                            </div>
                            
                        </div>

                    </div>



                    <div class="tab-pane fade in" id="socialwall">
                        <div class="row padding-15">

                            <!-- Copy social wall URl start -->
                        <center>
                         <a class="btn btn-green" href="javascript:void(0);" data-toggle="modal" data-target="#show_adn_copy_app_link_model2" id="show_my_web_app_link_btn_sliderbar2" style="margin:10px 10px;width: 225px;color:white !important;">Show My Social Wall Link</a>
                        </center>
                        
                         <div class="modal fade" id="show_adn_copy_app_link_model2" role="dialog">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Social Wall Link</h4>
                              </div>
                              <div class="modal-body">
                                <p id="show_app_link_p_tag2" style="word-wrap: break-word;text-align: center;"><?php echo base_url().'activity/'.$acc_name.'/'.$event['Subdomain'].'/socialwall' ?></p>
                                <a href="javascript:void(0);" id="copy_my_web_link_btn2" class="btn btn-green btn-block">Click to Copy Social wall Url</a>
                              </div>
                            </div>
                          </div>
                        </div>

                            <!-- Copy social wall URl end -->

                           <!-- header text start -->


                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Activity_admin/save_social_wall/<?php echo $event_id; ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1">
                                        Header Text
                                    </label>
                                    <div class="col-sm-10">
                                        <textarea class="summernote" name="socialwall_heading">
                                            <?php 
                                              if(count($socialwall_heading)>0){
                                                echo $socialwall_heading[0]['socialwall_heading'];
                                              } 
      
                                            ?>
                                        </textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="padding-left: 0px;" for="form-field-1"></label>
                                    <div class="col-sm-2">
                                        <input type="submit" class="btn btn-green btn-block" value="Save" name="save_hasetag" id="save_hasetag"> 
                                    </div>
                                </div>    
                            </form>
                        </div>  
                    </div>  <!-- social wall tab end -->
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
    $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
});
$.noConflict();
$(document).ready(function(){
    $('input.selectall').on('ifChecked ifUnchecked', function(event) {
        if (event.type == 'ifChecked') {
            $('input.foocheck').iCheck('check');
        } else {
            $('input.foocheck').iCheck('uncheck');
        }
    });
});
$(document).on('mouseover', '.msg_photo', function() {
    var my_class = $(this).find('a').attr('class');
    $(this).find('a').colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
});
function delete_activity(id,ac_no)
{
    if(confirm('Are You Sure Delete This Activity?'))
    {
        window.location.href="<?php echo base_url().'Activity_admin/delete_activityfeed/'.$event['Id'].'/' ?>"+id+"/"+ac_no;
    }
}
</script>



<!-- FOr Social Wall URL Copy to clip-board -->

<script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>

<script>
$('#show_my_web_app_link_btn_sliderbar2').click(function(){
  $('#copy_my_web_link_btn2').html('Copy My Social Wall Link');
});         
$('#copy_my_web_link_btn2').click(function(){
  if(copyToClipboard(document.getElementById("show_app_link_p_tag2")))
  {
    $('#copy_my_web_link_btn2').html('Copied Your Link');
  }
  else
  {
    $('#copy_my_web_link_btn2').html('Copy My Social wall Link');
  }
});
    function hidequestion(qid)
    {
        $.ajax({
            url:"<?php echo base_url(); ?>Activity_admin/chageQuestionStatus/<?php echo $this->uri->segment(3); ?>/"+qid,
            success:function(result)
            {
              if($.trim(result)>0)
              {
                var shortCutFunction ='success';
                if($("#show_question_"+qid).is(':checked'))
                {
                  var title = 'Hide Successfully';
                  var msg = 'Question Hide Successfully.';
                }
                else
                {
                  var title = 'Show Successfully';
                  var msg = 'Question Show Successfully.'; 
                }
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-top-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
              }
            }
        });
    }
    function delete_survey(id)
    {   
        <?php $event_id = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Activity_admin/delete/"+<?php echo $event_id; ?>+"/"+id;
        }
    }
    function delete_advert(id)
    {   
        <?php $event_id = $this->uri->segment(3); ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Activity_admin/deleteAdvert/"+<?php echo $event_id; ?>+"/"+id;
        }
    }
</script>
<script type="text/javascript">
    $('#repull_button').click(function() {
        var event_id = $('#repull_event_id').val(); 
        var page_no = $('#page_no').val(); 
        var lang_id = $('#lang_id').val(); 
        var user_id = $('#user_id').val(); 
        $.ajax({
            type: "POST",
                url: "<?php echo base_url();?>/native_single_fcm_v2/activityV2/get_all_v2",
                data: "event_id="+ event_id + "&page_no=" + page_no + "&lang_id=" + lang_id + "&user_id=" + user_id,
            success: function(data) {
               alert('Data Pull Successfully...!');
            }
        });
    });
    $("#auto_pull").change(function() {
        var event_id = $('#repull_event_id').val();
        if(this.checked){
            var status = 1;
        }else{
            var status = 0;
        }
        $.ajax({
            type: "POST",
                url: "<?php echo base_url();?>Activity_admin/update_repull_status/"+<?php echo $event_id;?>,
                data: "event_id="+ event_id + "&status=" + status,
                success: function(data) {
                   alert('Auto Sync Update Successfully...!');
                }
            });
    });
   
    
</script>