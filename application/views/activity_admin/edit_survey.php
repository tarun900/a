<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

             <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#survey_list" data-toggle="tab">
                            Edit Survey
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
					
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
              <div class="tab-content">
                <div class="tab-pane fade active in" id="survey_list">
                    <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                      <div class="form-group">
                          <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                              Question <span class="symbol required"></span>
                          </label>
                          <div class="col-sm-9">
                              <input type="text" placeholder="Question" id="Question" name="Question" class="form-control name_group required" value="<?=$survey['Question']?>">
                          </div>
                      </div>
                     
                    <div id="Question_option"> 
                      <div class="form-group">
                          <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                              Add More 
                          </label>
                          <div class="col-sm-9">
                               <a class="btn btn-blue" href="javascript: void(0);" onclick="addmoreoption()" ><i class="fa fa-plus fa fa-white"></i></a>
                          </div>
                      </div>
                    
                    <?php $Option=  json_decode($survey['Option']); ?>
                        <div id="option_container">
                             <?php if(!empty($Option)){$i=1;
                                  foreach ($Option as $key=>$val){?>
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                          Option <span class="symbol required"></span>
                                      </label>
                                      <div class="col-sm-9">
                                           <input type="text" style="margin-bottom:15px;" placeholder="Option" id="Option<?php echo $i; ?>" name="Option[<?php echo $i-1; ?>]" class="form-control required name_group" value="<?php echo $val; ?>">
                                          <a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a>
                                      </div>
                                  </div>
                                  <?php $i++;}}?>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Publish Results when Submitted</label>
                      <div class="col-sm-9">
                        <input type="checkbox" name="publish_result" <?=($survey['publish_result']) ? 'checked="checked"': ''?> style="left: 15px;margin-top: 1.5%;" id="publish_result" onchange="show_date()">
                      </div>
                    </div>

                    <div class="form-group" id="publish_date" style="display: none;">
                          <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                              Publish Date <span class="symbol required"></span>
                          </label>
                          <div class="col-sm-9">
                                <input type="text" name="publish_date" id="e_date" contenteditable="false" class="form-control" value="<?=date('m/d/Y',strtotime($survey['publish_date']))?>">
                          </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Hide Survey</label>
                      <div class="col-sm-9">
                        <input type="checkbox" name="show_question" style="left: 15px;margin-top: 1.5%;" <?=($survey['show_question'] == '0') ? 'checked="checked"': ''?>>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
			
			<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
				<div id="viewport" class="iphone">
						<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
				</div>
				<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
				<div id="viewport_images" class="iphone-l" style="display:none;">
					   <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
				</div>
			</div>
				
        </div>
    </div>
</div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    show_date();
    $('#e_date').datepicker({
        weekStart: 1,
        startDate: '-0d',
        autoclose: true
        });
    });
      var count=0;
     function addmoreoption()
     {    count++;
          var id="Option"+count;
          var name="Option["+count+"]";
          var html='<div class="form-group"><label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Option <span class="symbol required"></span></label><div class="col-sm-9"><input type="text" style="margin-bottom:15px;" placeholder="Option" id="'+id+'" name="'+name+'" class="form-control required name_group"><a class="btn btn-red" href="javascript: void(0);" onclick="removeoption(this)" ><i class="fa fa-times fa fa-white"></i></a></div></div>';
          jQuery("#option_container").append(html);
     }
     function removeoption(e)
     {
          if(jQuery("#option_container div.form-group").length>1)
          {
               jQuery(e).parent().parent().remove();
          }
     }

    function show_date()
    {
        if($("#publish_result").is(':checked'))
        {
            $("#publish_date").slideUp("slow");
            $('#e_date').val('');
        }
        else
        {
            $("#publish_date").slideDown("slow");  
        }
    }
</script>