<?php
//$Sid = $this->uri->segment(3);
$user = $this->session->userdata('current_user');
$lid = $user[0]->Id;

echo '<div style="background: #fff;padding-top: 20px;">
      <div id="messages" style="overflow-y: scroll;height: 400px;width: 99%;padding: 10px;">';
echo '<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with ' . $view_chats1[0]['Sendername'] . '<br/></span>';
foreach ($view_chats1 as $key => $value) {
    echo '<strong>From :</strong>' . $value['Sendername'];
    echo '<br/>';
    echo '<strong>To :</strong>' . $value['Recivername'];
    echo '<br/>';
    echo $value['Message'];
    echo '<br/>';
    echo timeAgo(strtotime($value['Time']));
    echo '<br/>';
    echo '<br/>';
}
?>
<div style="padding-left:5%;">
<?php
if (!empty($message)) {
    foreach ($message as $key => $value) {
        echo '<span style="background:#ccc;padding:5px;width:auto;border-radius:5px;margin-bottom:5px;">' . $value['Message'] . '</span></br/></br/>';
    }
}
?>
</div>
</div>
<form style="margin-top: 20px; margin-bottom: 20px;width: 50%;margin-left: 1%;" role="form" id="form" class="form-horizontal" novalidate="novalidate" action="" method="post">
    <?php
    $user = $this->session->userdata('current_user');
    if (!empty($user)) {
        foreach ($user as $val) {
            echo'<input type="hidden" name="Speaker_id" id="" value="">';
        }
    }
    ?>
    <input type="hidden" name="User_id" id="" value="">
    <select name="ispublic">
        <option value="0">Is Private</option>
        <option value="1">Is Public</option>
    </select>
    <textarea style="height: 150px;margin-bottom: 20px;" class="col-md-12 col-lg-12 col-sm-12" id="Message" name="Message" value="" placeholder="Type your message here..."></textarea><br/>
    <input style="margin-bottom: 10px;" class="btn btn-danger" type="button" name="submit" onclick="sendmessage();" value="Send Message">
</form>
</div>
    <?php ?>

<script type="text/javascript">
    function sendmessage()
    {
        var str = $("#form").serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>Message/chats1/<?php echo $Sid; ?>",
                        data: str,
                        type: "POST",
                        async: true,
                        success: function(result)
                        {
                            $('#Message').val('');
                            $("#messages").html();
                            $("#messages").html(result);
                        }

                    });
                }
</script>
<?php

function timeAgo($time_ago) {
//    echo $time_ago;
//    echo "<br>";
//    echo time();
//    exit;
    $cur_time = time();
    $time_elapsed = $cur_time - $time_ago;
    
    $seconds = $time_elapsed;
    $minutes = round($time_elapsed / 60);
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400);
    $weeks = round($time_elapsed / 604800);
    $months = round($time_elapsed / 2600640);
    $years = round($time_elapsed / 31207680);
    // Seconds
    if ($seconds <= 60) {
        echo "$seconds seconds ago";
    }
    //Minutes
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            echo "one minute ago";
        } else {
            echo "$minutes minutes ago";
        }
    }
    //Hours
    else if ($hours <= 24) {
        if ($hours == 1) {
            echo "an hour ago";
        } else {
            echo "$hours hours ago";
        }
    }
    //Days
    else if ($days <= 7) {
        if ($days == 1) {
            echo "yesterday";
        } else {
            echo "$days days ago";
        }
    }
    //Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            echo "a week ago";
        } else {
            echo "$weeks weeks ago";
        }
    }
    //Months
    else if ($months <= 12) {
        if ($months == 1) {
            echo "a month ago";
        } else {
            echo "$months months ago";
        }
    }
    //Years
    else {
        if ($years == 1) {
            echo "one year ago";
        } else {
            echo "$years years ago";
        }
    }
}
?>