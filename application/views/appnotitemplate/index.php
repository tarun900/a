<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php if($this->session->flashdata('appnoti_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> App Notification Template <?php echo $this->session->flashdata('appnoti'); ?> Successfully.
                </div>
                <?php } ?>
                
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#form_list" data-toggle="tab">
                                Email Template
                            </a>
                        </li>
                        <li>
                            <a href="#push_list" data-toggle="tab">
                                Push Notification
                            </a>
                        </li>
                         <?php if($user->Role_name=='Client'){ ?>
                        <!--<li class="">
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Setting
                            </a>
                        </li>-->
                         <?php } ?>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                        
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="form_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="35%">Name</th>
                                        <th width="40%">Subject</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($arrTemplate);$i++) {
                                        ?>
                                    <tr>
                                        <td width="5%"><?php echo $i+1; ?></td>
                                        <td width="35%"><?php echo stripslashes($arrTemplate[$i]['Slug']);?></td>
                                        <td width="40%"><?php echo stripslashes($arrTemplate[$i]['Subject']);?></td>
                                        <td width="20%">
                                            <a href="<?php echo base_url() ?>Appnotitemplate/edit/<?php echo $arrTemplate[$i]['event_id']; ?>/<?php echo $arrTemplate[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade  in" id="push_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="35%">Name</th>
                                        <th width="40%">Content</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($arrPushTemplate);$i++) {
                                        ?>
                                    <tr>
                                        <td width="5%"><?php echo $i+1; ?></td>
                                        <td width="35%"><?php echo stripslashes($arrPushTemplate[$i]['Slug']);?></td>
                                        <td width="40%"><?php echo stripslashes($arrPushTemplate[$i]['Content']);?></td>
                                        <td width="20%">
                                            <a href="<?php echo base_url() ?>Appnotitemplate/pushedit/<?php echo $arrPushTemplate[$i]['event_id']; ?>/<?php echo $arrPushTemplate[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!--<div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Feature Menu Title</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="img_view" id="img_view" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="Redirect_url" id="Redirect_url" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>-->

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                    <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                    </div>
                        <!-- <div class="demo-controls" style="width:30%;">
                            <div class="device-list" id="size-select">
                                <a class="device-list phone current" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Phone']); return false;">Phone</a>
                                <a class="device-list tablet" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Tablet']); return false;">Tablet</a>
                                <a class="device-list laptop" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Laptop']); return false;">Laptop</a>
                            </div>
                        </div> -->
                        <!-- <div class="light intro">
                            <div class="row">
                                <section id="demo">
                                    <figure id="devicePreview" class="phone">
                                        <iframe src="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>" scrolling="auto" frameborder="0"></iframe>
                                        <a class="laptopLink" target="_blank" href="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>">
                                            <img class="laptopImage hide" style="left: 145px;top: 46px;position: absolute;width: 67.3%;height: 383px;" src="<?php echo base_url(); ?>images/event_dummy.jpg">
                                        </a>
                                    </figure>
                                </section>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script>
    function delete_form(id,Event_id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Formbuilder/delete_form/" + id + "/" + Event_id;
        }
    }
</script>
