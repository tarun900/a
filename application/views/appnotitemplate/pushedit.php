<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Notification</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
                    
			<div class="panel-body">
                            <?php if($this->session->flashdata('appnoti_data')){ ?>
                            <div class="errorHandler alert alert-success no-display" style="display: block;">
                                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('appnoti_data'); ?> Successfully.
                            </div>
                            <?php } ?>
                            <form role="form" method="post" class="form-horizontal" id="mine-form" action="<?php echo base_url();?>Appnotitemplate/pushedit/<?php echo $arrTemplate[0]['event_id'];?>/<?php echo $arrTemplate[0]['Id'];?>" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                                Title : 
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" name="Subject" disabled="" value="<?php echo $arrTemplate[0]['Slug'];?>">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                            <label class="col-sm-2 control-label" for="form-field-1">
                                                    Content: <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea name="Content" class="ckeditor form-control" cols="10" rows="10"><?php echo $arrTemplate[0]['Content'];?></textarea>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="form-field-1">
                                        </label>
                                        <div class="col-md-4">
                                            <button class="btn btn-yellow btn-block" type="submit">
                                                Submit <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
				</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<!-- end: PAGE CONTENT-->