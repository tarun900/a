<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js"></script>
<script>
	jQuery(document).ready(function() {
		FormElements.init();
                FormValidator.init();
                $('#Schedual_date').datepicker({
                	date: null,
                        restrictDateSelection: false
                });
                
                jQuery('#show_email_error').css('display','none');
                <?php
                    if(!empty($Notifications) && !empty($Notifications['CheckdUSer']))
                    {
                       
                        ?>
                                displayedituser('<?php echo json_encode($Notifications['CheckdUSer']); ?>');
                                <?php
                    }
                    
                    if(!empty($Notifications) && $Notifications['Schedule']==1)
                    {
                        $scheduledateY=date('Y',  strtotime($Notifications['Schedual_date']));
                        $scheduledatem=date('m',  strtotime($Notifications['Schedual_date']))-1;
                        $scheduledated=date('d',  strtotime($Notifications['Schedual_date']));
                        
                        ?>
                                showsheduledate();
                                jQuery('#Schedual_date').datepicker('setDate', new Date(<?php echo $scheduledateY.",".$scheduledatem.",".$scheduledated; ?>));
                                jQuery('#Schedual_date').datepicker('update');
                        <?php
                    }
                ?>
	}); 
        
        function displayuser(showall)
        {
           if($("#User_type").val()!="")
           {
            $.ajax({
                 url : '<?php echo base_url(); ?>Notifications/displayuser',
                 data :'type='+$("#User_type").val(),
                 type: "POST",           
                 success : function(data)
                    {
                        var values=data.split('###');
                        if(values[0]=="error")
                        {   
                            $("#userdata").html('');
                            $("#userdata").html(values[1]);
                        }
                        else
                        {
                            var obj = jQuery.parseJSON(values[1]);
                            //var data='<label class="control-label"><em>(select at least one)</em> <span class="symbol required"></span></label><div>';
                            var data='<label for="form-field-select-4">Sent To</label><select multiple="multiple" name="services[]" id="service1" class="form-control search-select">';
                            if(showall)
                            {
                                data+='<option value="All">All</option>';
                            }
                            $.each( obj, function( key, value ) { 
                                var checkboxtrue="";                                
                                //data+='<div class="checkbox"><label><input type="checkbox" class="grey" '+checkboxtrue+' value="'+value.id+'" name="services[]" id="service1">'+value.name+'</label></div></div>';
                                data+='<option value="'+value.id+'" '+checkboxtrue+'>'+value.name+'</option>';
                            });
                            
                            data+='</select></div>';
                            
                            $("#userdata").html('');
                            $("#userdata").html(data);
                            $(".search-select").select2({
                                    placeholder: "Select",
                                    allowClear: false
                            });
                        }
                    }
                });
            }
            else
            {
                $("#userdata").html('');
            }
        }
        
        function showsheduledate()
        {            
            if($("#Schedule").val()=='1')
            {
                $("#showdate").show();
                $("#schedulefor").show();
            }
            else
            {
                $("#showdate").hide();
                $("#schedulefor").hide();
            }
            
            $("#Schedual_date").val('');
        }
        
        function displayedituser(datvalue)
        {
           
           if($("#User_type").val()!="")
           {
            $.ajax({
                 url : '<?php echo base_url(); ?>Notifications/displayuser',
                 data :'type='+$("#User_type").val(),
                 type: "POST",           
                 success : function(data)
                    {
                        var values=data.split('###');
                        if(values[0]=="error")
                        {   
                            $("#userdata").html('');
                            $("#userdata").html(values[1]);
                        }
                        else
                        {
                            var obj = jQuery.parseJSON(values[1]);
                            var data='<label for="form-field-select-4">Sent To</label><select multiple="multiple" name="services[]" id="service1" class="form-control search-select">';
                            var checkboxtrue="";
                            $.each( obj, function( key, value ) { 
                                if(datvalue.indexOf(value.id)<0)
                                {
                                    checkboxtrue="";
                                }
                                else
                                {
                                    checkboxtrue="selected";
                                }
                                data+='<option value="'+value.id+'" '+checkboxtrue+'>'+value.name+'</option>';
                            });
                            
                            data+='</select></div>';

                            $("#userdata").html('');
                            $("#userdata").html(data);
                            $(".search-select").select2({
                                    placeholder: "Select",
                                    allowClear: false
                            });
                        }
                    }
                });
            }
            else
            {
                $("#userdata").html('');
            }
        }        
</script>