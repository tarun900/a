<?php  
$arr=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#indiviual_attendee" data-toggle="tab">
                                CSV Attendee
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>                  
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="alert alert-info"><?php echo $nomatch.' Column Are Not Match'; ?></div>
                    <div class="tab-pane fade active in" id="indiviual_attendee">
                        <div id="cms_page_list" class="table-responsive" style="overflow-x:scroll; ">
                        <table class="table table-striped table-bordered table-hover table-full-width dataTable" id="sample_1">
                            <thead>
                                <tr>
                                    <?php foreach ($keysdata as $key => $value) {
                                        $arr=array_column_1($customkey,'column_name');
                                        $fixedc=array('Emailaddress','firstname','lastname','Company','Title');
                                        $carr=array_merge($arr,$fixedc);
                                      if(!in_array($value,$carr)){  ?>
                                    <th style="color:red;"><?php echo $value; ?></th>
                                    <?php }else{ ?>
                                    <th><?php echo $value; ?></th>
                                    <?php } } ?>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($csvdatatest as $key => $value) { ?>
                                <tr>
                                    <?php foreach ($keysdata as $kkey => $kvalue) {?>
                                    <td><?php echo $value[$kvalue]; ?></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                            </label>
                            <div class="col-md-3">
                                <a href="<?php echo base_url().'Attendee_admin/add/'.$this->uri->segment(3); ?>" class="btn btn-theme_green btn-block">
                                    Next
                                </a>
                            </div>
                        </div>
                    </div>   
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="border:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Column Name</h4>
        </div>
        <div class="modal-body">
            <form method="post" action="<?php echo base_url().'Attendee_admin/savecoumnname/'.$this->uri->segment(3); ?>">
                <input type="Hidden" placeholder="Column Id" id="column_id" name="column_id" class="form-control" value="">
                <input type="text" placeholder="Column Name" id="column_name" name="column_name" class="form-control">
                <div class="alert alert-danger" style="display:none;color: red;" id="column_name_error">This field required.</div>
                <button class="btn btn-green btn-block" type="button" id="column_save_btn">Save</button>
            </form>
        </div>
         <div class="modal-footer" style="border:none;display: none;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function updateheader(cid,cnm)
    {
        $('#column_id').val(cid);
        $('#column_name').val(cnm);
        $("#myModal").modal({backdrop: true});
    }
    $('#column_save_btn').click(function(){
        var id=$.trim($('#column_id').val());
        var name=$.trim($('#column_name').val());
        if(name=="")
        {
            $('#column_name_error').show();
        }
        else
        {
            $('#column_save_btn').attr('type','submit');
        }
    });
</script>