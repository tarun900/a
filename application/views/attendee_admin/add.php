<?php  $arr=$this->session->userdata('current_user');  $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="">
                            <a href="#indiviual_attendee" data-toggle="tab">
                                Invite Individual Attendee
                            </a>
                        </li>
                        <li class="active">
                            <a href="#multiple_attendee" data-toggle="tab">
                                Invite Multiple Attendees
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
                                      
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade" id="indiviual_attendee">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Send another email
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea style="height:200px;" name="msg" id="content" class="ckeditor form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Assign Agenda</label>
                                <div class="col-sm-9">
                                        <?php if(count($category_list)==1){ ?>
                                            <input type="hidden" name="session_id" id="session_id" value="<?php echo $category_list[0]['cid']; ?>"/>
                                            <input disabled="disabled" type="text" class="col-md-6 form-control required" value="<?php  echo $category_list[0]['category_name']; ?>" name="cat_name"/>    
                                        <?php }else{ ?>    
                                        <select class="col-md-12 form-control" id="session_id" name="session_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                        <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Invite Attendees Individually <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                       <input type="text" placeholder="Attendee email address" class="form-control required" id="email_address" name="email_address" onchange=""><br/>
                                       <input type="text" placeholder="Attendee First name" class="form-control required" id="first_name" name="first_name"><br/>
                                       <input type="text" placeholder="Attendee Last name" class="form-control required" id="last_name" name="last_name">
                                      <!--<textarea class="form-control limited" maxlength="90" placeholder="Attendee email address, First name, Last name" name="bulk_data" id="form-field-23"></textarea>-->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Send Link
                                </label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="1" id="send_link" name="send_link">
                                            <span>Send Home Screen Link</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="0" id="send_link" name="send_link">
                                            <span>Send Registration Screen Link</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Subject <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Subject" class="form-control required" id="subject1" name="subject1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control" id="sent_name" name="sent_name">
                                    </div>
                                </div>
                            </div>
                          
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade active in" id="multiple_attendee">
                        <form role="form" method="post" class="form-horizontal" id="form2" action="<?php echo  base_url().'Attendee_admin/savecsvdata/'.$this->uri->segment(3);?>" enctype="multipart/form-data">
                            <h4>Upload the email addresses you would like to invite to your app.</h4>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Short instructions
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-4" style="padding:0px;">
                                        <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Attendee_admin/download_csv/<?php echo $this->uri->segment(3); ?>">Download CSV</a>
                                        <br/><br/><small>Download Example CSV of multiple invite attendees</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Upload CSV <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-4" style="padding:0px;">
                                      <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                          <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                  <input type="file" name="csv" id="csv">
                                          </span><br/><small>Browse CSV file only</small>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-3">
                                    <button class="btn btn-theme_green btn-block" type="submit">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function setEmailTemplate()
    {
        var Slug=$("#template").val();
        $.ajax({
            url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
            data :'Slug='+Slug,
            type: "POST",  
            async: false,
            success : function(data)
            {
                $("#content").code('');
                $("#content").code(data);
            }
        });
    }
    function setEmailTemplate1()
    {
        var Slug=$("#template").val();
        $.ajax({
            url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
            data :'Slug='+Slug,
            type: "POST",  
            async: false,
            success : function(data)
            {
                $("#content1").code('');
                $("#content1").code(data);
            }
        });
    }
</script>