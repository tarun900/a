<!-- start: PAGE CONTENT -->   
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Upload Attendee <span class="text-bold"> Session </span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('upload_success_data')){ ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('upload_success_data'); ?> 
                    </div>
                <?php } ?>

                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Upload CSV <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <div class="col-sm-4" style="padding:0px;">
                              <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <span class="btn btn-file btn-light-grey">
                                    <i class="fa fa-folder-open-o"></i> 
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="csv_files" id="csv_files" class="required">
                                </span>
                                <br/>
                                <small>Browse CSV file only</small>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-3">
                            <button class="btn btn-theme_green btn-block" name="submit_save" type="submit">
                                Add
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>