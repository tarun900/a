    <?php $user=$this->session->userdata('current_user');  
$acc_name=$this->session->userdata('acc_name'); ?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#screen_content').summernote({
    height:300,
  });
});
</script>
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />  
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white attend-register">
        	<div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Registration Screen</span></h4>
           </div>
           <div class="panel-body">
                <form action="" role="form" method="post" class="form-horizontal" id="form" novalidate enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Banner Images 
                        </label>
                        <div class="modal fade" id="companybannermodelscopetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Company Banner Image</h4>
                                </div>
                                <div class="modal-body">
                                    <section>
                                        <div class="demo-wrap" style="display:none;">
                                            <div class="container">
                                                <div class="grid">
                                                    <div class="col-1-2">
                                                        <div id="vanilla-demo"></div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <strong>Vanilla Example</strong>
                                                        <div class="actions">
                                                            <button class="vanilla-result">Result</button>
                                                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="preview_company_banner_crop_tool"></div>
                                            <div class="col-sm-3 crop-btn">
                                                <button class="btn btn-green btn-block" id="company_banner_upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>         
                                    </section>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                    <input type="file" id="banner_Images" name="banner_Images">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                            <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">
                            Background Images 
                        </label>
                        <div class="col-sm-9">
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <div class="fileupload-preview fileupload-exists thumbnail" id="backgroungimageuploadpreview">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                    <input type="file" name="background_image">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                             Description
                        </label>
                        <div class="col-sm-9">
                            <textarea id="screen_content" name="screen_content"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            iOS Link
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="ios Link" id="ios_link" name="ios_link" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Android Link
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Android Link" id="android_link" name="android_link" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Show App Links
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="show_app_link" name="show_app_link"  value="1">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Show Footer
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="show_footer" checked="checked" name="show_footer"  value="1">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Add QR Code for Badges
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="send_pdf" name="send_pdf" value="1" checked="checked">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Send Qr code
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="send_qr_code" name="send_qr_code" value="1" checked="checked">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Show Attendee Type
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="show_attendee_type" name="show_attendee_type" value="1" checked="checked">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Show Pay By Invoice
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="show_pay_by_invoice" name="show_pay_by_invoice" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Show Coupon Code
                        </label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="show_coupon_code" name="show_coupon_code" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Send Notification Email when a new user registers
                        </label>
                        <div class="col-sm-8">
                            <input type="checkbox" id="email_notify_admin" name="email_notify_admin" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Payment Type
                        </label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input type="radio" id="payment_type" name="payment_type" value="1"> 
                                    <span>Authorize.net</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="payment_type" name="payment_type" value="0" checked="checked"> 
                                    <span>ebizcharge.com</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="payment_type_2" name="payment_type" value="2"> 
                                    <span>Stripe</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Coupon Code 
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Enter Coupon Code" id="coupon_code" name="coupon_code" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Number of Forms 
                        </label>
                        <div class="col-sm-9">
                            <select name="number_forms" class="form-control">
                                <?php for ($i=1;$i<=15;$i++) { ?>
                                <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-2">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Save <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-yellow btn-block" onclick="preview_registration();" type="button" data-toggle="modal" data-target="#registration_screen_Preview_model">
                                Preview Registration Screen
                            </button>
                        </div>
                    </div>
                </form>
                <div class="advertisement" style="background:#eee;padding:15px;">
                    <div id="formBuilder"></div>
                </div>
            </div>    
        </div>
    </div>
</div>
<div class="modal fade" id="registration_screen_Preview_model" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Preview Registration Screen</h4>
            </div>
            <div class="modal-body" id="resgistrationscreenbackimagepreviw">
                <section>
                    <div class="row banner-img" style="display: none;">
                        <img width="100%" src="" id="preview_model_image_tag">
                    </div>
                    <div class="row">
                        <h1 style="text-align: center;"><?php echo ucfirst($event['Event_name']); ?></h1>
                    </div>
                    <div class="row" style="display: none;">
                        <div id="preview_model_html_content">
                            
                        </div>
                    </div>
                    <div id="app_logos_div" class="app-logo" style="display: none;">
                        <div class="row ios-icn" style="display: none;">
                            <a id="ios_link_in_models" target="_blank" href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>">
                                <img src="<?php echo base_url().'assets/images/ios icon.png'; ?>">
                            </a>
                        </div>
                        <div class="row android-icn" style="display: none;">
                            <a id="android_link_in_models" target="_blank" href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>">
                                <img src="<?php echo base_url().'assets/images/android icon.png'; ?>">
                            </a>
                        </div>
                        <div class="row web-icn">
                            <a target="_blank" href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain']; ?>">
                                <img src="<?php echo base_url().'assets/images/web icon.png'; ?>">
                            </a>
                        </div>
                    </div>
                    <div class="attendee-footer">
                        <p>This App was made on</p>
                        <p><img src="<?php echo base_url(); ?>assets/images/aitl-logo1.png" alt="aitl-logo"/></p>
                        <a href="https://www.allintheloop.com"><p>www.allintheloop.com</p></a>
                    </div>
                </section>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js"></script>
<script>
    Demo.init();
</script>
<script type="text/javascript">
$('#remove_company_banner_data').click(function(){
    $('#company_banner_crop_data_textbox').val('');
});
function preview_registration()
{
    if($('#banner_preview_image_div').find('img').attr('src'))
    {
        $('#preview_model_image_tag').parent().show();
        $('#preview_model_image_tag').attr('src',$('#banner_preview_image_div').find('img').attr('src'));
    }
    else
    {
        $('#preview_model_image_tag').parent().hide();
        $('#preview_model_image_tag').attr('src','');
    }

    if($('#backgroungimageuploadpreview').find('img').attr('src'))
    {
        
        $('#resgistrationscreenbackimagepreviw').attr('style','background:transparent url("'+$('#backgroungimageuploadpreview').find('img').attr('src')+'") no-repeat scroll 0 0');
    }
    else
    {
        $('#resgistrationscreenbackimagepreviw').attr('style','');
    }
    if($('#screen_content').code()!="")
    {
        $('#preview_model_html_content').parent().show();
        $('#preview_model_html_content').html($('#screen_content').code());
    }
    else
    {
        $('#preview_model_html_content').parent().hide();
        $('#preview_model_html_content').html('');   
    }

    if($('#ios_link').val()!="")
    {
        $('#ios_link_in_models').parent().show();
        $('#ios_link_in_models').attr('href',$('#ios_link').val());
    }
    else
    {
        $('#ios_link_in_models').parent().hide();
        $('#ios_link_in_models').attr('href','');
    }

    if($('#android_link').val()!="")
    {
        $('#android_link_in_models').parent().show();
        $('#android_link_in_models').attr('href',$('#android_link').val());
    }
    else
    {
        $('#android_link_in_models').parent().hide();
        $('#android_link_in_models').attr('href','');
    }
    if($('#show_app_link').prop('checked'))
    {
        $('#app_logos_div').show();
    }
    else
    {
        $('#app_logos_div').hide();   
    }
    if($('#show_footer').prop('checked'))
    {
        $('.attendee-footer').show();
    }
    else
    {
        $('.attendee-footer').hide();   
    }
}
</script>