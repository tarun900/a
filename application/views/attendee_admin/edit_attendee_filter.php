<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />    
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css"> 
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Edit <span class="text-bold">Attendee Filter</span></h4>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="categorie_name">
              Custom Column <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <select class="form-control" name="custom_column" id="custom_column" onchange="change_label()">
                <option>Select Custom Column</option>
                <?php foreach ($custom_column as $key => $value): ?>
                    <option value="<?=$value['column_id']?>" <?=$value['column_id'] == $filter['custom_column'] ? 'selected=selected' : '' ?>><?=$value['column_name']?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Attendee Category <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <select class="form-control" name="attendee_categories">
                <option value="">Select Attendee Categories</option>
                <?php foreach ($attendee_categories as $key => $value): ?>
                    <option value="<?=$value['id']?>" <?=$value['id'] == $filter['category_id'] ? 'selected=selected' : '' ?>><?=$value['category']?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Display Text <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <input type="text" placeholder="Please Select Custom Column" id="display-text" class="form-control" name="label" value="<?=$filter['label']?>">
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Key Words <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <textarea id="Short_desc" class="form-control required tags" name="tags"><?=$filter['tags']?></textarea>     
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Submit <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
  <?php 
  if(empty($filter['label']))
  {?>
    change_label();
  <?php }?>
  function change_label()
  { 
      var tmp = $("#custom_column option:selected").text();
      $('#display-text').val(tmp);
  }
  $(document).ready(function() {
  $('#Short_desc').tagsInput({
    maxTags: 6,
    allowSpaces: false,
    removeWithBackspace : true
  });});
</script>