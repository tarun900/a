<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
<?php $arr=$this->session->userdata('current_user'); $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Attendee_admin/add/<?php echo $Event_id; ?>"><i class="fa fa-plus"></i> Invite Attendees</a>
                <?php if (count($skipcsvdata)>0){ ?>        
                <a style="top: 7px;margin-right:12%;" class="btn btn-primary list_page_btn" href="<?php echo base_url().'Attendee_admin/send_invitation/'.$Event_id; ?>">Invite By Email</a>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_view_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('attendee_view_data'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('attendee_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Attendee <?php echo $this->session->flashdata('attendee_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('add_without_invite')) { ?>
                    <div class="errorHandler alert alert-info no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('add_without_invite'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('org_limit_attendee')) { ?>
                    <div class="errorHandler alert alert-warning no-display" style="display: block; background-color: #FFA500 !important;">
                        <i class="fa fa-remove-sign"></i>  <?php echo $this->session->flashdata('org_limit_attendee'); ?> 
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('formbuild_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Form Builder <?php echo $this->session->flashdata('formbuild_data'); ?> Successfully.
                    </div>
                <?php } ?>
              
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#attendee_dropdown_link_div" data-close-others="true">
                               <span id="Attendees_textshow"> Attendees </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="attendee_dropdown_link_div" class="dropdown-menu dropdown-info"> 
                                <li class="active">
                                    <a href="#attendees_list" data-toggle="tab">
                                        Attendees List
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#add_attendee" data-toggle="tab">
                                        Add Attendee Without Inviting
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#invite_attendees" data-toggle="tab">
                                        Invited Attendees
                                    </a>
                                </li>
                                <?php if($event['Event_type']=='4'){ ?>
                                <li class="">
                                    <a href="#authorized_emails" data-toggle="tab">
                                        Authorized Emails
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a href="#singup" data-toggle="tab">
                                Sign Up Process
                            </a>
                        </li>
                        <?php if ($user->Role_name == 'Client') { ?>
                        <!-- <li class="dropdown">
                            <a data-toggle="dropdown" href="#Registration_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Registration_textshow"> Registration </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Registration_dropdown_link_div" class="dropdown-menu dropdown-default"> 
                                <li>
                                    <a href="#registration_screen" data-toggle="tab">Registration Screen</a>
                                </li>
                                <li>
                                    <a href="#badges_values" data-toggle="tab">Badges Values</a>
                                </li>
                                <li>
                                    <a href="#singup" data-toggle="tab">
                                        Sign Up Process
                                    </a>
                                </li>
                                <li>
                                    <a href="#payment_processor" data-toggle="tab">
                                        Payment Processor
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <?php } ?>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Viewing_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Viewing_textshow"> Viewing </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Viewing_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <?php if ($user->Role_name == 'Client') { ?> 
                                <li>
                                    <a href="#merge_fields" data-toggle="tab">Merge Fields</a>
                                </li>
                                <li>
                                    <a href="#viewer_permissions" data-toggle="tab">Viewer Permissions</a>
                                </li>
                                <?php } ?>
                                <li class="">
                                    <a href="#show_directory" data-toggle="tab">
                                        Show Directory
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#allow_metting" data-toggle="tab">
                                        Meetings
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#metting_location" data-toggle="tab">
                                        Meetings Locations
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#Groups_dropdown_link_div" class="dropdown-toggle" data-close-others="true">
                               <span id="Viewing_textshow">Groups</span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="Groups_dropdown_link_div" class="dropdown-menu dropdown-default">
                                <li class="">
                                    <a href="#groups" data-toggle="tab">
                                        Groups
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#groups_per" data-toggle="tab">
                                        Messaging Permissions
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#advertising" data-toggle="tab">
                                Menu Name and Icon
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>        
                    </ul>

                    <div class="tab-content">
                    
                        <div class="tab-pane fade active in at_tabpan" id="attendees_list">
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;">

                        </div>
                        <form action="<?php echo base_url().'Attendee_admin/assign_agenda_category/'.$this->uri->segment(3); ?>" method="post">    
                            <div class="row attendee_row" style="text-align: center;">
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New Agenda To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="agenda_category_id" name="agenda_category_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New View To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="attendee_view_id" name="attendee_view_id">
                                            <option value="">Select View</option>
                                            <?php foreach ($view_data as $key => $value) { ?>
                                                <option value="<?php echo $value['view_id']; ?>"><?php echo $value['view_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                    </div>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <button class="btn btn-green btn-block" id="assign_agenda_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Selected Attendees Type</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="attendee_type" name="attendee_type">
                                                <option value="">Select Registration Type</option>
                                                <?php $form=json_decode($registration_data[0]['registration_forms'],true);
                                                $field=$form['fields'][5]['choices'];
                                                foreach ($field as $key => $value) { ?>
                                                <option value="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Selected Attendees Group</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="user_group" name="user_group" onchange="get_typepopup(this);">
                                                <option value="">Select Attendee Group</option>
                                                <?php foreach($user_group as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>"><?php echo $value['group_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    <button class="btn btn-success btn-block" id="send_mail_pdf_btn" type="button" disabled="disabled">Send Badges</button>
                                </div>
                                <div class="col-sm-3 pull-right">
                                    <button class="btn btn-success btn-block" id="send_mail_btn" type="button" disabled="disabled">Send Email Invite</button>
                                </div>
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'Attendee_admin/download_data_in_csv/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="download_csv">Download Your Attendee List</a> 
                                </div>
                                <div class="col-sm-3 pull-right">
                                <a class="btn btn-success btn-block" id="add_new_column_btn" data-toggle="modal" data-target="#add_cloumn_models">Add a New Column</a></div>
                            </div>
                            <div class="row" style="margin: 10px 0px;">
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'Attendee_admin/add_mass_attendee/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="download_csv">Mass Upload</a> 
                                </div>
                            </div>
                            <div id="attendee_list_table" class="table-responsive custom_checkbox_table" style="overflow-x:scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_123">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox-table">
                                                <label>
                                                    <input name="selectall_checkbox" type="checkbox" class="flat-grey selectall checkbox_select_all_box">
                                                </label>
                                            </div>
                                        </th>
                                        <th>Name</th>
                                        <?php if($Event_id == '1012' || $Event_id == '634'): ?>
                                        <th>Unique No.</th>
                                        <?php endif;?>
                                        <?php if($Event_id == '1012'): ?>
                                        <th>Barcode List</th>
                                        <?php endif; ?>
                                        <td>Email</td>
                                        <th>Agenda</th>
                                        <th>View</th>
                                        <th>Status</th>
                                        <th>Registered</th>
                                        <th>Edit or Delete</th>
                                        <th>Salutation</th>
                                        <th>Title</th>
                                        <th>Street</th>
                                        <th>Suburb</th>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th>Mobile</th>
                                        <th>Phone Business</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($Attendees); $i++) { ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="attendees_id[]" value="<?php echo $Attendees[$i]['uid']; ?>" id="attendees_id_<?php echo $Attendees[$i]['uid'];?>"   class="foocheck">
                                                <label for="attendees_id_<?php echo $Attendees[$i]['uid'];?>"></label>    
                                            </td>
                                            <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $Attendees[$i]['uid'].'/'.$Event_id; ?>"><?php echo $Attendees[$i]['Firstname'].' '.$Attendees[$i]['Lastname']; ?></a></td>
                                            <?php if($Event_id == '1012' || $Event_id == '634'): ?>
                                            <td><?php echo $Attendees[$i]['Unique_no']; ?></td>
                                            <?php endif; ?>
                                            <?php if($Event_id == '1012'): ?>
                                            <td><?php echo $Attendees[$i]['barcodes']; ?></td>
                                            <?php endif; ?>
                                            <td><?php echo $Attendees[$i]['Email']; ?></td>
                                            <td><?php echo ucfirst($Attendees[$i]['category_name']); ?></td>
                                            <td><?php echo $Attendees[$i]['view_name']; ?></td>
                                            
                                            <td>
                                                <span class="label label-sm label-success">Registered</span>
                                            </td>
                                            <td>
                                                <span class="label label-sm label-success">Yes</span>
                                            </td>
                                            <td>
                                                    <a href="<?php echo base_url(); ?>Profile/update/<?php echo $Attendees[$i]['uid'].'/'.$Event_id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                                    <a href="javascript:;" onclick="delete_attendee(<?php echo $Attendees[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                                    <a href="javascript:void(0);" onclick="show_save_session('<?php echo $Attendees[$i]['uid']; ?>');" class="btn btn-xs btn-green tooltops" data-original-title="User Sessions">User Sessions</a>
                                                </td>
                                            <td><?php echo $Attendees[$i]['Salutation'];?></td>
                                            <td><?php echo $Attendees[$i]['Title'];?></td>
                                            <td><?php echo $Attendees[$i]['Street'];?></td>
                                            <td><?php echo $Attendees[$i]['Suburb'];?></td>
                                            <td><?php echo $Attendees[$i]['State'];?></td>
                                            <td><?php echo $Attendees[$i]['Country'];?></td>
                                            <td><?php echo $Attendees[$i]['Mobile'];?></td>
                                            <td><?php echo $Attendees[$i]['Phone_business'];?></td>
                                        </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                            <div class="pagination pull-left"> <?php echo "Showing ".($start+1)." to ".($start+count($Attendees))." of ".$total_rows." entries"; ?> </div>
                            <h1>testaitl</h1>
                            <div class="pull-right">
                                <?php if (isset($links)) { ?>
                                    <?php echo $links ?>
                                <?php } ?>
                            </div>
                            
                            </div>
                            </form>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="authorized_emails">
                            <div id="authassign_agenda_error_div" class="alert alert-danger" style="display: none;"></div>
                            <div class="row attendee_row" style="text-align: center;">
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New Agenda To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <select class="col-md-6 form-control required" id="authagenda_category_id" name="authagenda_category_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row attendee_row" style="text-align: center;"> 
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New View To Selected Attendees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select class="col-md-6 form-control required" id="auth_user_view_id" name="auth_user_view_id">
                                                <option value="">Select View</option>
                                                <?php foreach ($view_data as $key => $value) { ?>
                                                    <option value="<?php echo $value['view_id']; ?>"><?php echo $value['view_name']; ?></option>
                                                <?php }  ?>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <button class="btn btn-green btn-block" id="authorized_assign_agenda_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                <a class="btn btn-success btn-block" id="add_new_column_btn" data-toggle="modal" data-target="#add_cloumn_models">Add Column</a></div>
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'Attendee_admin/add_authorized_email/'.$Event_id; ?>" class="btn btn-green btn-block">Add Authorized Emails</a>
                                </div>
                            </div>
                            <div class="table-responsive" id="authorized_user_listng">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_5">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input name="selectall_checkbox_authuser" type="checkbox" class="flat-grey selectall">
                                                    </label>
                                                </div>
                                            </th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Agenda</th>
                                            <th>Views</th>
                                            <th>Check In</th>
                                            <th>Action</th>
                                            <?php foreach ($keysdata as $key => $value) { ?>
                                            <th id="custom_colunm_authorized">
                                                <span onclick='updateheader("<?php echo $value['column_id']; ?>","<?php echo $value['column_name']; ?>","<?php echo $value['crequire']; ?>");'><?php echo ucfirst($value['column_name']); ?>
                                                </span>
                                            </th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($auth_user_list as $key => $value) { ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="auth_user_id[]" value="<?php echo $value['authorized_id']; ?>" id="auth_user_id<?php echo $value['authorized_id'];?>"   class="foocheck">
                                                <label for="auth_user_id<?php echo $value['authorized_id'];?>"></label>
                                            </td>
                                            <td><?php $key++;echo $key; ?></td>
                                            <td><?php echo ucfirst($value['firstname']).' '.$value['lastname']; ?></td>
                                            <td><?php echo $value['Email']; ?></td>
                                            <td><?php echo $value['category_name']; ?></td>
                                            <td><?php echo $value['view_name']; ?></td>
                                            <td>
                                                <input type="checkbox" name="aauth_user_check_id" value="<?php echo $value['authorized_id']; ?>" id="aauth_user_check_id<?php echo $value['authorized_id'];?>" <?php if($value['check_in']=='1'){ ?> checked="checked" <?php } ?>>
                                                <label for="aauth_user_check_id<?php echo $value['authorized_id'];?>"></label>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'Attendee_admin/edit_authorized_user/'.$Event_id.'/'.$value['authorized_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_auth_user(<?php echo $value['authorized_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                            <?php for($j=0;$j<count($keysdata);$j++){
                                            $custom=json_decode($value['extra_column'],TRUE);
                                            $keynm=$keysdata[$j]['column_name']; ?>
                                            <td onclick='getuseredit("<?php echo $value['authorized_id']; ?>","4");'><?php echo $custom[$keynm]; ?></td>
                                            <?php } ?>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>    
                        </div>
						<div class="tab-pane fade at_tabpan" id="registration_screen">
                            <?php if(count($registration_data) <= 0){ ?>
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    <a href="<?php echo base_url().'Attendee_admin/add_registration_screen/'.$this->uri->segment(3); ?>" class="btn btn-green btn-block" id="create_registration_screen">Create Registration Screen</a> 
                                </div>
                            </div>
                            <?php } ?>
                            <div class="table-responsive" style="overflow-x:scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Subject</th>
                                            <th>Registration Screen Url</th>
                                            <?php if(count($category_list) <= 1){ ?>
                                            <th>Multi Registration Forms Url</th>
                                            <?php } ?>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($registration_data as $key => $value) {?>
                                        <tr>
                                            <td>
                                                <?php echo $key+1; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'Attendee_admin/edit_registration_screen/'.$Event_id.'/'.$value['screen_id']; ?>"><?php echo "Registration Screen"; ?></a>
                                            </td>
                                            <td><a href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/attendee_registration_screen' ?>" target="_blank"><?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/attendee_registration_screen' ?></a></td>
                                            <?php if(count($category_list) <= 1){ ?>
                                            <td><a href="<?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/show_multi_registration' ?>" target="_blank"><?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'].'/show_multi_registration' ?></a></td>
                                            <?php } ?>
                                            <td>
                                                <a data-original-title="Edit" data-placement="top" class="label btn-blue tooltips" href="<?php echo base_url().'Attendee_admin/edit_registration_screen/'.$Event_id.'/'.$value['screen_id']; ?>"><i class="fa fa-edit"></i></a>
                                                <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_registration_screen("<?php echo $value['screen_id'] ?>");' href="javascript:;"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="badges_values">
                            <form class="" id="badges_form" name="badges_form" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>Attendee_admin/save_badges_vales/<?php echo $event_id; ?>">
                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-2" for="form-field-1" style="padding-left: 0px;">
                                            Badges Logo Images
                                        </label>
                                        <div class="col-sm-9">
                                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                            <div class="fileupload-preview fileupload-exists thumbnail" id="backgroungimageuploadpreview">
                                            </div>
                                                <?php if (!empty($badges_data[0]['badges_logo_images'])) { ?>
                                                <div class="fileupload-new thumbnail" id="backgroungimageuploadpreview">
                                                <img src="<?php echo base_url(); ?>assets/badges_files/<?php echo $badges_data[0]['badges_logo_images']; ?>" alt="">
                                                </div>
                                                <?php } ?>
                                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                    <input type="file" name="badges_logo_images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                    <i class="fa fa-times"></i> Remove
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Sender Name <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sender_name" class="form-control required" value="<?php echo $badges_data[0]['sender_name']; ?>" id="sender_name" placeholder="Please Enter Email Sender Name">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Subject <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="subject" class="form-control required" value="<?php echo $badges_data[0]['subject']; ?>" id="subject" placeholder="Please Enter Email Subject">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Email Body <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <textarea type="text" name="email_body" class="form-control summernote" id="email_body" placeholder="Please Enter Email Content"><?php echo $badges_data[0]['email_body']; ?></textarea>
                                        </div>
                                    </div> 
                                </div>
                                <?php   $form=json_decode($registration_data[0]['registration_forms'],true);
                                        $field=$form['fields'][5]['choices'];
                                        $type_color=json_decode($badges_data[0]['attendee_type_color'],true);
                                        $cutom_color=array('0'=>'#000000','1'=>'#FF2B1C');

                                foreach ($field as $key => $value) { ?>
                                <div class="col-sm-12" style="margin-bottom:10px;">   
                                    <div class="form-group">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> <?php echo $value['value']; ?> Color <span class="symbol required"></span> </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="<?php echo $value['value']; ?>" value="<?php echo !empty($type_color[$value['value']]) ? $type_color[$value['value']] : $cutom_color[$key]; ?>" id="<?php echo $value['value']; ?>" class="color {hash:true} form-control name_group">
                                        </div>
                                    </div> 
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Send Code
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" id="code_type_0" <?php if($badges_data[0]['code_type']!='1'){ ?> checked="checked" <?php } ?> name="code_type" value="0"> 
                                                <span>QR Code</span>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" <?php if($badges_data[0]['code_type']=='1'){ ?> checked="checked" <?php } ?> id="code_type_1" name="code_type" value="1"> 
                                                <span>BarCode</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>                
                                <div class="row">
                                    <div class="col-sm-3" style="margin-top:15px;margin-left: 193px;">
                                        <input  type="submit" class="btn btn-theme_green btn-block" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
						<div class="tab-pane fade at_tabpan" id="add_attendee">
                            <div class="panel-body">
							<form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Attendee_admin/add_attendee_without_invite/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
								<div class="row">
								  <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
								  <div class="errorHandler alert alert-success no-display" style="display: block;">
								  <i class="fa fa-remove-sign"></i> Add Successfully.
								  </div>
								  <?php } ?>
								  <div class="col-md-6">

									<div class="form-group">
									   <label class="control-label">First Name <span class="symbol required"></span></span>
									   </label>
										<input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
									</div>

									<div class="form-group">
									   <label class="control-label">Last Name <span class="symbol required"></span> </span>
									   </label>
										<input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
									</div>
									
									<div class="form-group">
									  <label class="control-label">Email <span class="symbol required"></span></label>
									  <input type="email" placeholder="Email" class="form-control required" id="email_attendee" name="email_attendee" onblur="checkemail_by_attendee();">
									  <input type="hidden" class="form-control" id="event_id" name="event_id" value="<?php echo $this->uri->segment(3); ?>">
									</div>

									<div class="row">
									   <div class="form-group col-md-6">
											<label class="control-label">Password <span class="symbol required"></span></label>
											<input type="password" class="form-control" id="password" name="password">
									   </div>
									   <div class="form-group col-md-6" style="margin-left: 7%;">
											 <label class="control-label">Confirm Password <span class="symbol required"></span></label>
											 <input type="password" class="form-control" id="password_again" name="password_again">
									   </div>
									</div>
									  <div class="row" style="margin-left:-30px;">
										  <div class="col-md-4">
											 <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
										  </div>
									  </div>
								  </div>
								</div>
							</form>
                            </div>
						</div>
                        <div class="tab-pane fade at_tabpan" id="invite_attendees">
                            <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" class="flat-grey selectall">
                                                    </label>
                                                </div>
                                                </th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < count($agenda_invited_list); $i++) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        if ($agenda_invited_list[$i]['Status'] == "0") {
                                                        ?>
                                                        <div class="checkbox-table">
                                                            <label>
                                                                <input type="checkbox" name="invites[]"  value="<?php echo $agenda_invited_list[$i]['Emailid']; ?>" class="flat-grey foocheck">
                                                            </label>
                                                        </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <!--<td><?php echo $i + 1; ?></td>-->
                                                    <td><?php echo $agenda_invited_list[$i]['Emailid']; ?></td>
                                                    <td><?php echo $agenda_invited_list[$i]['firstname']." ".$agenda_invited_list[$i]['lastname']; ?></td>
                                                    <td>
                                                        <span class="label label-sm 
                                                            <?php
                                                            if ($agenda_invited_list[$i]['Status'] == '1') {
                                                                ?> 
                                                                      label-success 
                                                                      <?php
                                                                  } else {
                                                                      ?> label-danger 
                                                                  <?php }
                                                                  ?>">
                                                                      <?php
                                                                      if ($agenda_invited_list[$i]['Status'] == '1') {
                                                                          echo "Register";
                                                                      } else {
                                                                          echo "Not Register";
                                                                      }
                                                            ?>
                                                            </span>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group" style="margin-top: 3%;">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Send another email</strong>
                                    </label>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <textarea name="msg" style="height: 200px;" class="summernote form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Send Link</strong>
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="col-sm-2" style="padding:0px;">
                                            <input type="checkbox" value="1" checked="checked" class="form-control" id="send_link" name="send_link">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Subject</strong>  <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <input type="text" placeholder="Subject" class="form-control required" id="subject" name="subject">
                                        </div>
                                    </div>
                                </div>
                                 <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    <strong>Sender Name</strong>
                                </label>
                                <div class="col-sm-12">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control" id="sent_name" name="sent_name">
                                    </div>
                                </div>
                            </div>

							<div class="form-group">
								<!--<label class="col-sm-2 control-label" for="form-field-1">
								</label>-->
								<div class="col-md-3">
									<button class="btn btn-yellow btn-block" type="submit">
										Submit <i class="fa fa-arrow-circle-right"></i>
									</button>
								</div>
							</div>
							
                            </form>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="show_directory">
                            <form method="post" action="<?php echo base_url().'Attendee_admin/saveshowateendee/'.$this->uri->segment(3); ?>">
                                <div class="row">
                                    <label class="col-sm-2" for="form-field-select-4">
                                        Show Directory
                                    </label>
                                    <div class="col-md-1">
                                        <input type="checkbox" <?php if($event['show_attendee_menu']=='1'){ ?> checked="checked" <?php } ?> name="show_attendee_menu" id="show_attendee_menu" class="" value="1"> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"  style="margin:15px 0px;">
                                        <button name="show_attendee_menu_btn" class="btn btn-yellow btn-block" value="show_submit" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="add_hub_attendees">          
                      <div class="row">
                        <form method="post" action="<?php echo base_url().'Attendee_admin/add_hub_attendees/'.$this->uri->segment(3); ?>">
                        <div class="form-group">
                            <label class="col-sm-3" for="form-field-1">
                                <strong>Select Hub Attendees Group</strong>
                            </label>
                            <div class="col-sm-5" style="padding:0px;">
                                <select class="form-control" name="attendee_group_id" id="attendee_group_id">
                                    <option value="">Select Attendees Group</option>
                                    <?php foreach ($attendee_group as $key => $value) { ?>
                                        <option value="<?php echo $value['group_id']; ?>"><?php echo $value['group_name']; ?></option>
                                    <?php  } ?>
                                </select>
                            </div>
                        </div><div class="row"></div>
                        <div class="form-group">
                        <div class="col-md-3">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                        </div>
                        </form>
                        </div>
                      </div>        
                        <div class="tab-pane fade at_tabpan" id="singup">
                           <div class="row advertising">
                                <div class="col-sm-12">
                                    <div class="panel panel-white">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Add <span class="text-bold">Form</span></h4>

                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-close" href="#">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <?php if(!empty($arrSingupForm)){ ?>
                                         <a style="top: 7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Attendee_admin/delete_form/<?php echo $Event_id; ?>"><i class="fa fa-plus"></i> Delete</a>
                                         <?php } ?>
                                        <div class="panel-body">
                                            <div class="advertisement" style="background:#eee;padding:15px;">
                                                <?php $Event_id = $Event_id; ?>
                                                <div id="formBuilder"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="payment_processor">
                            <div class="row">
                                <form class="" id="payment_processor_form" name="payment_processor" enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>Attendee_admin/save_payment_processor/<?php echo $event_id; ?>">
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Secret Key <span class="symbol required"></span> </label>
                                            <div class="col-sm-8">
                                                <input type="text" name="stripe_secret_key" class="form-control required" value="<?php echo $registration_data[0]['stripe_secret_key']; ?>" id="stripe_secret_key" placeholder="Please Enter Secret Key">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Public Key <span class="symbol required"></span> </label>
                                            <div class="col-sm-8">
                                                <input type="text" name="stripe_public_key" class="form-control required" value="<?php echo $registration_data[0]['stripe_public_key']; ?>" id="stripe_public_key" placeholder="Please Enter Public Key">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Price <span class="symbol required"></span> </label>
                                            <div class="col-sm-8">
                                                <input type="text" name="stripe_payment_price" class="form-control required" value="<?php echo $registration_data[0]['stripe_payment_price']; ?>" id="stripe_payment_price" placeholder="Please Enter Payment Price">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom:10px;">   
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"> Payment Currency 
                                            </label>
                                            <div class="col-sm-10">
                                                <label class="radio">
                                                    <input type="radio" name="stripe_payment_currency" value="GBP" <?php if($registration_data[0]['stripe_payment_currency']=='GBP'){ ?> checked="checked" <?php } ?>>
                                                    <span style="margin-left: 2%;"> <i class="fa fa-gbp"></i> GBP </span>
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="stripe_payment_currency" <?php if($registration_data[0]['stripe_payment_currency']!='GBP'){ ?> checked="checked" <?php } ?> value="USD">
                                                    <span style="margin-left: 2%;"> <i class="fa fa-usd"></i> USD </span>
                                                </label>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1"></label>
                                        <div class="col-sm-3">
                                            <input  type="submit" class="btn btn-theme_green btn-block" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
						<div class="tab-pane fade at_tabpan" id="viewer_permissions">
                            <div class="row">
                                <div class="col-sm-3 pull-right" style="margin-bottom: 10px;">
                                    <a href="<?php echo base_url().'Attendee_admin/add_view_role/'.$Event_id ?>" class="btn btn-green btn-block">Add View</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($view_data as $key => $value) {?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td>
                                            <a <?php if($value['view_type']=='0'){ ?> href="<?php echo base_url().'Attendee_admin/edit_view_role/'.$Event_id.'/'.$value['view_id']; ?>" <?php } else{ ?> href="javascript:void(0);" <?php } ?>>
                                            <?php echo $value['view_name']; ?>
                                            </a>
                                            </td>
                                            <td>
                                            <?php if($value['view_type']=='0'){ ?>
                                            <a data-original-title="Edit" data-placement="top" class="label btn-blue tooltips" href="<?php echo base_url().'Attendee_admin/edit_view_role/'.$Event_id.'/'.$value['view_id']; ?>"><i class="fa fa-edit"></i></a>
                                            <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_view("<?php echo $value['view_id'] ?>");' href="javascript:;"><i class="fa fa-times fa fa-white"></i></a>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="merge_fields">
                            <h2>Merge Fields</h2>
                            <h5>Use the Merge Fields below in HTML content in your App to show personalised information to each Attendee. The Merge Field will stream information entered into the Attendee Columns in Attendee List.<h5>
                            <?php
                            if(count($keysdata) > 0){
                            foreach ($keysdata as $key => $value) { ?>
                            <h5><?php echo $value['column_name']." = {".str_replace(" ",'', $value['column_name'])."}"; ?></h5>
                            <?php } }else{ ?>
                            <h2>No Custom Column Available For this App.</h2>
                            <?php } ?>
                        </div>
						<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
							<div id="viewport" class="iphone">
									<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
							</div>
							<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
							<div id="viewport_images" class="iphone-l" style="display:none;">
								   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
							</div>
						</div>
						<div class="tab-pane fade" id="allow_metting">
                            <div class="row padding-15">
                                <form class="" method="post" action="<?php echo base_url().'Attendee_admin/saveallow_setting_for_attendee/'.$this->uri->segment(3); ?>">
                                    <div id="all_meeting_div_check" class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="1" class="square-orange" name="attendee_hide_request_meeting" <?php if($event['attendee_hide_request_meeting']=='1') { ?> checked="checked" <?php } ?>>
                                                    <span style="font-weight: bold;"> Allow Meeting Requests </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select class="form-control" name="meeting_time_slot">
                                                    <option value="5" <?=($event['meeting_time_slot'] == '5')? 'selected' : ''?>">5 minutes</option>
                                                    <option value="10" <?=($event['meeting_time_slot'] == '10')?'selected' : ''?>>10 minutes</option>
                                                    <option value="15" <?=($event['meeting_time_slot'] == '15')?'selected' : ''?>>15 minutes</option>
                                                    <option value="20" <?=($event['meeting_time_slot'] == '20')?'selected' : ''?>>20 minutes</option>
                                                    <option value="25" <?=($event['meeting_time_slot'] == '25')?'selected' : ''?>>25 minutes</option>
                                                    <option value="30" <?=($event['meeting_time_slot'] == '30')?'selected' : ''?>>30 minutes</option>
                                                </select>
                                                <span style="font-weight: bold;"> Meeting Time Slot Length </span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select class="form-control" name="meeting_start_time">
                                                   <?php
                                                   $time=range(strtotime(date('07:00')),strtotime("22:00"),5*60);
                                                    foreach ($time as $key => $value)
                                                    {
                                                       if((date('H:i',$value)) == "00:00")
                                                           continue;
                                                       $selected = ($event['meeting_start_time'] == date('H:i',$value))? ' selected' : '';    
                                                       echo "<option value=".date('H:i',$value)." ".$selected.">".date('H:i',$value)."</option>";
                                                    }
                                                   ?>
                                                </select>
                                                <span style="font-weight: bold;"> Meeting Start Time</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select class="form-control" name="meeting_end_time">
                                                    <?php
                                                    foreach ($time as $key => $value)
                                                    {
                                                       if((date('H:i',$value)) == "00:00")
                                                           continue;
                                                       $selected = ($event['meeting_end_time'] == date('H:i',$value))? ' selected' : '';    
                                                       echo "<option value=".date('H:i',$value)." ".$selected.">".date('H:i',$value)."</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <span style="font-weight: bold;"> Meeting End Time</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-primary">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                    <div id="all_modaration_div" <?php if($event['attendee_hide_request_meeting']=='0') { ?> style="display:none;" <?php } ?> class="row padding-15">
                                        <h2> Moderation </h2> 
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_7">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Assigned Moderator</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($Attendees as $key => $value) { ?>
                                                    <tr>
                                                        <td><?php echo $key+1; ?></td>
                                                        <td><a href="javascript:void(0);" onclick='get_assign_moderator_popup("<?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?>",<?php echo $value['uid']; ?>);'><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></a></td>
                                                        <td><?php echo $value['Email']; ?></td>
                                                        <td><?php echo ucfirst($value['moderator_name']); ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>    
                                    </div>    
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="metting_location">
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    <a class="btn btn-green btn-block" href="javascript:void(0);" data-toggle="modal" data-target="#add_meeting_location_modales">Add Meeting Locations</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_8">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Meeting Location</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($meeting_locations as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td>
                                                <a href="javascript:void(0);" onclick='get_edit_location_models("<?php echo $value['location_id']; ?>","<?php echo $value['location']; ?>")'><?php echo ucfirst($value['location']); ?></a>
                                            </td>
                                            <td>
                                                <a data-original-title="Edit" data-placement="top" class="label btn-blue tooltips" href="javascript:void(0);" onclick='get_edit_location_models("<?php echo $value['location_id']; ?>","<?php echo $value['location']; ?>")'><i class="fa fa-edit"></i></a>

                                                <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_meeting_location("<?php echo $value['location_id']; ?>")' href="javascript:;"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade at_tabpan" id="advertising">
                            <div class="row padding-15">
                                <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                    <div class="col-md-12 alert alert-info">
                                        <h5 class="space15">Menu Title & Image</h5>
                                        <div class="form-group">
                                            <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        </div>
                                        <div class="form-group">
                                             <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail"></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div class="user-edit-image-buttons">
                                                   <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                        <input type="file" name="Images[]">
                                                   </span>
                                                   <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                        <i class="fa fa-times"></i> Remove
                                                   </a>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                                Create Home Tab
                                            </label>
                                            <?php if($img !='') { ?>
                                            <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                            </label>
                                            <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                            <?php } ?>
                                        </div>
                            
                                        <div class="form-group image_view:">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                            <div class="col-sm-5">
                                                <select id="img_view" class="form-control" name="img_view">
                                                        <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                        <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                                </select>
                                            </div>
                                        </div>
                                         <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                       
                                    </div>
                                </form>
                            </div>
                        </div>
                                                <div class="tab-pane fade at_tabpan" id="groups">
                            <div class="row pull-right">
                                <a style="width: 150px;" href="<?php echo base_url().'Attendee_admin/add_group/'.$event_id; ?>" class="btn btn-green btn-block"><i class="fa fa-plus"></i> Add New Group</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_6">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Groups Name</th>
                                            <th>Created Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($user_group as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td><?php echo ucfirst($value['group_name']); ?></td>
                                            <td><?php echo $value['created_date']; ?></td>
                                            <td>
                                            <a class="btn btn-xs btn-green showmodel" data-toggle="modal" data-id="<?= $value['id']; ?>" data-bname="<?= $value['group_name']; ?>" data-sel_val=<?= json_encode(explode(',',$value['permitted_group']));?>><i class="fa fa-edit"></i></a>
                                            <a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-red tooltips fa-del-icon" onclick='delete_group("<?php echo $value['id'] ?>");' href="javascript:;"><i class="fa fa-times fa fa-white"></i></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                        <div class="tab-pane fade at_tabpan" id="groups_per">
                            <h4>Manage your Groups’ messaging permissions below. In the field next to a Group name choose which other Group that Group is allowed to Message using the App. Once you set this up all Groups will have messaging blocked between groups unless specified below.</h4>
                            <br><br>
                            <form action="<?php echo base_url(); ?>Attendee_admin/add_group_permisson/<?php echo $event_id ?>" method="POST"  role="form" id="form" novalidate="novalidate">
                                <input type="hidden" name="group_ids" value="<?=implode(',',(array_column_1($user_group,'id')))?>">
                                <?php foreach($user_group as $key => $value) { 
                                    $selected = explode(',',$value['permitted_group']);
                                    ?>
                                <div class="row attendee_row" style="text-align: center;"> 
                                    <div class="col-md-4 agenda_assign_label">
                                        <label><?=$value['group_name']?></label>
                                    </div>
                                    <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="<?=$value['id']?>[]" multiple="multiple">
                                                <?php foreach ($user_group as $key1 => $value1) {?>
                                                <option id="menu<?php echo $key1; ?>" <?=(in_array($value1['id'],$selected)) ? 'selected': ''; ?> value="<?php echo $value1['id']; ?>"><?php echo $value1['group_name']; ?></option>
                                                <?php }?>
                                                </select>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="row attendee_row">
                                    <button style="width: 150px;<?=(empty($user_group)? 'display:none':'')?>" class="btn btn-green" id="" type="submit">Assign</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>

<div class="modal fade" id="attendee_note_models" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notes</h4>
        </div>
        <div class="modal-body">
            <div id="notes_form_field" style="display:none;">
            <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Attendee_admin/save_attendee_notes/'.$this->uri->segment(3); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Heading <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Heading" id="Heading" name="Heading" class="form-control name_group">
                        <div class="alert alert-danger" style="display:none;" id="note_heading_error">This field required.</div>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Description <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-9">
                        <textarea id="Description" name="Description" class="summernote"></textarea>
                        <div class="alert alert-danger" style="display:none;" id="note_description_error">This field required.</div>
                    </div>
                </div>
                <input type="hidden" value="" name="User_id" id="User_id">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                    </label>
                    <div class="col-md-4">
                        <button id="save_attendee_notes" class="btn btn-yellow btn-block" type="button">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                </div>
            </form>
            </div>
            <div id="model_msg"><h3>Please wait while loading notes data..</h3></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="add_cloumn_models" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="border:none;">
          <button type="button" class="close" data-dismiss="modal" onclick="clecerdata();">&times;</button>
          <h4 class="modal-title">Save Column</h4>
        </div>
        <div class="modal-body">
        <form action="<?php echo base_url().'Attendee_admin/savecoumnname/'.$this->uri->segment(3); ?>" method="post">
            <div class="form-group">
                 <input type="Hidden" placeholder="Column Id" id="column_id" name="column_id" class="form-control" value="">
                <input type="text" name="column_name" id="column_name" class="form-control" placeholder="Column Name">
                <span id="column_name_text_box_errror_msg" style="color:red;display:none;">Plase Enter Column Name</span>
            </div>
            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox" id="crequire" name="crequire" value="1" class="grey">
                        Required after invite
                </label>
            </div>
            <button type="button" id="column_btn_save" class="btn btn-green btn-block">Save</button>
            <button class="btn btn-red btn-block" type="submit" value="Delete" name="delete_btn" id="column_delete_btn" style="display:none;">Delete</button>
        </form>    
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="extra_info_edit_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Extra Information</h4>
        </div>
        <div class="modal-body">
            <div id="load_extra_info_field" style="display: none;">
                <form action="<?php echo base_url().'Attendee_admin/extra_info_save/'.$this->uri->segment(3); ?>" method="post">
                    <input type="hidden" name="extra_user_id" id="extra_user_id" value=""> 
                    <input type="hidden" name="user_type" id="user_type" value="">
                    <?php foreach ($keysdata as $key => $value) { ?>
                    <div class="form-group">
                        <label><?php echo $value['column_name']; ?></label>
                        <input type="text" name="<?php echo $value['column_id']; ?>" id="text_<?php echo $value['column_id']; ?>" class="form-control" placeholder="">
                    </div>
                    <?php } ?>
                    <button type="submit" id="extra_info_btn_save" class="btn btn-green btn-block">Save</button>
                </form> 
            </div>
          <div id="model_extrainfo_msg">Please wait while loading notes data..</div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="show_save_session_model" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Sessions</h4>
        </div>
        <div class="modal-body">
            <div class="row" id="Show_user_save_session_list" style="display: none;padding: 0 10px;max-height: 496px;overflow-y: auto;">
            </div>
            <div id="model_usersavesession_msg"><h4>Please wait while loading Session data..</h4></div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="assign_attendee_moderator_popup" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign a Meeting Moderator to <span id="attendee_name_assign_moderator"></span></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <form id="assign_moderator_form" name="assign_moderator_form" role="form" method="post" action="<?php echo base_url().'Attendee_admin/assign_moderator_to_attendee/'.$this->uri->segment(3); ?>">
                    <div class="form-group">
                        <input type="hidden" name="user_id" id="attendee_user_id" value="">
                        <div class="col-sm-9 moderator_selectbox">
                            <select id="moderator_user_id" class="select2-container select2-container-multi form-control search-select menu-section-select required" name="moderator_user_id">
                            <option value="">Select Moderator</option>    
                            <?php foreach ($moderator as $key => $value) { ?>
                            <option value="<?php echo $value['uid']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <button style="width: 150px;" class="btn btn-green" id="assign_moderator_btn" type="submit">Assign</button>
                    </div>
                </form>    
            </div>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="add_meeting_location_modales" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Meeting Locations</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form2" action="<?php echo base_url().'Attendee_admin/save_meeting_locations/'.$this->uri->segment(3); ?>" method="post">
                        <input type="hidden" name="locations_id" id="locations_id" value="">
                        <div class="form-group">
                            <label class="control-label col-sm-12">Meeting Location</label>
                            <div class="col-sm-12">
                                <input type="text" name="locations" id="locations" class="form-control required" placeholder="Enter Meeting Locations" required="required">
                            </div>
                        </div>
                        <div class="col-sm-12" style="margin-top: 15px;">
                            <button type="submit" id="meeting_locations_btn_save" class="btn btn-green btn-block">Save</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Update Group</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Attendee_admin/update_group/<?php echo $this->uri->segment(3); ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="beacon-name" name="group_name">
                <input type="hidden" name="id" id="beacon-id">
              </div>
              <div class="form-group">
                    <select style="margin-top:-8px;" id="Menu_id_1" class="select2-container select2-container-multi form-control search-select menu-section-select" name="permitted_group[]" multiple="multiple">
                    <?php foreach ($user_group as $key1 => $value1) { ?>
                    <option id="menu<?= $value1['id']; ?>" value="<?php echo $value1['id']; ?>"><?php echo $value1['group_name']; ?></option>
                    <?php }?>
                    </select>
              </div>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTab2 > li > ul > li > a').click(function(){
        jQuery(this).parent().parent().parent().first('a').find('span').html($.trim(jQuery(this).text()));
    });
});
</script>
<script>
     jQuery(document).ready(function() {
          TableData.init();
          $('#column_btn_save').click(function(){
            if($.trim($('#column_name').val())=="")
            {
                $('#column_name_text_box_errror_msg').show();
            }
            else
            {
                $('#column_name_text_box_errror_msg').hide();
                $('#column_btn_save').attr('type','saumit');
            }
          });
        $('input.checkbox_select_all_box').on('ifChecked', function(event) {
            $('#send_mail_btn').removeAttr('disabled');
            $('#send_mail_pdf_btn').removeAttr('disabled');
        });
        $('input.checkbox_select_all_box').on('ifUnchecked', function(event) {
            $('#send_mail_btn').attr('disabled','disabled');
            $('#send_mail_pdf_btn').attr('disabled','disabled');
        });
     });
     $(window).load(function() {
        $('#assign_agenda_btn').click(function(){
            var table=$('#sample_1').DataTable();
            var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
            if($('#user_group').val()!="" || ($('#agenda_category_id').val()!="" || $('#attendee_view_id').val()!="") && chkbox_checked>0)
            {
                $('#assign_agenda_error_div').hide();
                var data = table.$('input').serialize();
                console.log(data);
                data=data+"&agenda_category_id="+$('#agenda_category_id').val()+'&attendee_view_id='+$('#attendee_view_id').val();
                data=data+"&user_group="+$('#user_group').val();
                $.ajax({
                    url : "<?php echo base_url().'Attendee_admin/assign_agenda_category/'.$this->uri->segment(3); ?>",
                    data:data,
                    type: "POST",
                    success : function(data)
                    {
                        var values=data.split('###');
                        if($.trim(values[0])=="success")
                        {
                            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                            if(regex .test(values[1]))
                            {
                                window.location.href=values[1];   
                            }
                        }
                        else
                        {
                            $('#assign_agenda_error_div').html(values[1]);
                            $('#assign_agenda_error_div').slideDown('slow');
                            setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
                        }    
                    }
                });
            }
            else
            {
                $('#assign_agenda_error_div').html('Plase Select Attendees or Agenda or Group');
                $('#assign_agenda_error_div').slideDown();
                setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
            }
        });
        $('#authorized_assign_agenda_btn').click(function(){
            var table=$('#sample_5').DataTable();
            var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
            if(($('#authagenda_category_id').val()!="" || $('#auth_user_view_id').val()!="") && chkbox_checked>0)
            {
                $('#authassign_agenda_error_div').hide();
                var data = table.$('input').serialize();
                console.log(data);
                data=data+"&agenda_category_id="+$('#authagenda_category_id').val()+"&views_id="+$('#auth_user_view_id').val();
                $.ajax({
                    url : "<?php echo base_url().'Attendee_admin/Authorized_assign_agenda_category/'.$this->uri->segment(3); ?>",
                    data:data,
                    type: "POST",
                    success : function(data)
                    {
                        var values=data.split('###');
                        if($.trim(values[0])=="success")
                        {
                            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                            if(regex .test(values[1]))
                            {
                                window.location.href=values[1];   
                            }
                        }
                        else
                        {
                            $('#authassign_agenda_error_div').html(values[1]);
                            $('#authassign_agenda_error_div').slideDown('slow');
                            setInterval(function(){ $('#authassign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
                        }    
                    }
                });
            }
            else
            {
                $('#authassign_agenda_error_div').html('Plase Select Authorized Email or Agenda');
                $('#authassign_agenda_error_div').slideDown();
                setInterval(function(){ $('#authassign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
            }
        });
        $('#send_mail_btn').click(function(){
            var table=$('#sample_1').DataTable();
            var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
            if(chkbox_checked>0)
            {
                var data = table.$('input').serialize(); 
                $.ajax({
                    url : "<?php echo base_url().'Attendee_admin/save_attendee_ids/'.$this->uri->segment(3); ?>",
                    data:data,
                    type: "POST",
                    success : function(result)
                    {
                        window.location.href="<?php echo base_url().'Attendee_admin/send_email_msg_sceen/'.$this->uri->segment(3); ?>";   
                    }
                });  
            }
        });
        $('#send_mail_pdf_btn').click(function(){
            var table=$('#sample_1').DataTable();
            var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
            if(chkbox_checked>0 && $.trim($('#attendee_type').val())!="")
            {
                var data = table.$('input').serialize(); 
                $.ajax({
                    url : "<?php echo base_url().'Attendee_admin/send_attndee_registration_mailpdf/'.$this->uri->segment(3); ?>",
                    data:data+'&attendee_type='+$('#attendee_type').val(),
                    type: "POST",
                    success : function(result)
                    {
                        window.location.href="<?php echo base_url().'Attendee_admin/index/'.$this->uri->segment(3); ?>";   
                    }
                });  
            }
            else
            {
                $('#assign_agenda_error_div').html("Please Select Attendee Type.");
                $('#assign_agenda_error_div').slideDown();
                setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
            }
        });
   });
    $('input[name="attendees_id[]"]').click(function(){
        var table=$('#sample_1').DataTable();
        var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
        if(chkbox_checked > 0)
        {
            $('#send_mail_btn').removeAttr('disabled');
            $('#send_mail_pdf_btn').removeAttr('disabled');
        }
        else
        {
            $('#send_mail_btn').attr('disabled','disabled');
            $('#send_mail_pdf_btn').attr('disabled','disabled');
        }
    });
    $('input[name="attendees_check_id"]').click(function(){
        $.ajax({
            url : "<?php echo base_url().'Attendee_admin/save_check_in_attendee/'.$this->uri->segment(3); ?>",
            data:"attendee_id="+$(this).val(),
            type: "POST",
            success : function(data)
            {
                var shortCutFunction ='success';
                var title = 'Success';
                var msg = 'Check in Save Successfully'; 
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-top-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
            }
        });
    });
    $('input[name="aauth_user_check_id"]').click(function(){
        $.ajax({
            url : "<?php echo base_url().'Attendee_admin/save_check_in_authorized_user/'.$this->uri->segment(3); ?>",
            data:"authorized_id="+$(this).val(),
            type: "POST",
            success : function(data)
            {
                var shortCutFunction ='success';
                var title = 'Success';
                var msg = 'Check in Save Successfully'; 
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-top-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
            }
        });
    });
</script>
<script type="text/javascript">
     
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    function delete_attendee(id,Event_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete/"+<?php echo $test; ?>+"/"+id
        }
    }
    function delete_auth_user(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_authorized_user/"+<?php echo $this->uri->segment(3); ?>+"/"+id
        }
    }
</script>
<style type="text/css">
    #select2-result-label-7
    {
        display: none;
    }
</style>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    $(document).ready(function(){

        $("#save").addClass("btn btn-primary");
        $('#google').prop('checked',true);
        $('.green').show();
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="red"){
                $(".red").toggle();
                $(".green").hide();
            }
            if($(this).attr("value")=="green")
            {
                $(".green").toggle();
                $(".red").hide();
            }
        });
        jQuery('#save_attendee_notes').click(function(e){
            if($.trim($('#Heading').val())=="" || $.trim($('#Description').code())=="")
            {
                if($.trim($('#Heading').val())==""){
                    $('#note_heading_error').slideDown('slow');
                }
                else
                {
                    $('#note_heading_error').slideUp('slow');
                }
               if($.trim($('#Description').code())=="")
                {
                    $('#note_description_error').slideDown('slow');
                }
                else
                {
                    $('#note_description_error').slideUp('slow');
                }
            }
            else
            {
                $('#note_heading_error').slideUp('slow');
                $('#note_description_error').slideUp('slow');
                $('#save_attendee_notes').attr('type','submit');            
            }
        });
    });
    function showuserid(pid)
    {
        $('#Heading').val('');
        $('#Description').code('');
        $('#User_id').val(pid);
        $('#model_msg').hide();
        $('#notes_form_field').show();
    }
    function shownotedata(nid,uid)
    {
        $.ajax({
            url:"<?php echo base_url().'Attendee_admin/getnotesdata/'.$this->uri->segment(3); ?>",
            data:"notes_id="+nid+"&User_id="+uid,
            type:"post",
            success:function(result){
                var result= jQuery.parseJSON(result);
                $('#Heading').val(result.Heading);
                $('#Description').code(result.Description);
                $('#User_id').val(result.User_id);
                $('#model_msg').hide();
                $('#notes_form_field').show();
            }
        });
    }
    function getuseredit(uid,user_type)
    {
        $('#extra_info_edit_model').modal();
        $('#extra_user_id').val(uid);
        $('#user_type').val(user_type);
        $.ajax({
            url:"<?php echo base_url().'Attendee_admin/getuserextra_info/'.$this->uri->segment(3).'/'; ?>"+uid,
            data:"user_type="+user_type,
            type:'post',
            success:function(result){
                var result= jQuery.parseJSON(result);
                $.each(result, function( index, value ) {
                    $('#text_'+index).val(value);
                });
                $('#model_extrainfo_msg').hide();
                $('#load_extra_info_field').show();
            }
        });
    }
    function show_save_session(uid)
    {
        $('#Show_user_save_session_list').hide();
        $('#model_usersavesession_msg').show();
        $('#Show_user_save_session_list').html('');
        $('#show_save_session_model').modal('show');
        $.ajax({
            url:"<?php echo base_url().'Attendee_admin/get_usersave_session/'.$this->uri->segment(3).'/'; ?>"+uid,
            data:"user_type="+user_type,
            type:'post',
            success:function(result){
                var result= jQuery.parseJSON(result);
                if(result.length > 0)
                {
                    $.each(result, function( index, value ) {
                        var string="<div class='col-sm-12' style='border:1px solid #eee;margin-bottom:10px;'><h3>"+value.Heading+"</h3><div class='col-sm-6'>Start : "+value.Start_date+" "+value.Start_time+"</div><div class='col-sm-6'>End : "+value.End_date+" "+value.End_time+"</div></div>";
                        $('#Show_user_save_session_list').append(string);
                    });
                }
                else
                {
                    $('#Show_user_save_session_list').html("<h2 style='text-align:center;'>No Session Save</h2>");
                }
                $('#model_usersavesession_msg').hide();
                $('#Show_user_save_session_list').show();
            }
        });
    }
    function updateheader(cid,cnm,intRequire)
    {
        $('#column_id').val(cid);
        $('#column_name').val(cnm);
        $('#column_delete_btn').show();
        if(intRequire==1)
        {
            $('#crequire').iCheck('check');
        }
        else
        {
            $('#crequire').iCheck('uncheck');
        }
        $("#add_cloumn_models").modal({backdrop: true});
    }
    function clecerdata()
    {
        $('#column_id').val('');
        $('#column_name').val('');
        $('#column_delete_btn').hide();
    }
    function delete_invited_attendee(iid)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_invited_attendee/"+<?php echo $test; ?>+"/"+iid
        }
    }
    function delete_inactive_attendee(temp_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_inactive_attendee/"+<?php echo $test; ?>+"/"+temp_id
        }
    }
    function delete_view(vid)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_view/"+<?php echo $test; ?>+"/"+vid
        }
    }
    function delete_group(vid)
    {
        <?php $event_id = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_group/"+<?php echo $event_id; ?>+"/"+vid
        }
    }
    function delete_registration_screen(rsid)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_registration_screen/"+<?php echo $this->uri->segment(3); ?>+"/"+rsid
        }   
    }
    function checkemail_by_attendee()
        {
            var sendflag="";
                $.ajax({
                url : '<?php echo base_url().'Attendee_admin/checkemail_by_attendee/'.$this->uri->segment(3); ?>',
                data :'email='+$("#email_attendee").val()+'&idval='+$('#idval').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#email_attendee').parent().removeClass('has-success').addClass('has-error');
                        $('#email_attendee').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#email_attendee').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        $('#email_attendee').parent().removeClass('has-error').addClass('has-success');
                        $('#email_attendee').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#email_attendee').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
        function get_assign_moderator_popup(name,uid)
        {
            $('#attendee_name_assign_moderator').html(name);
            $('#assign_attendee_moderator_popup').modal('show');
            $('#attendee_user_id').val(uid);
        }
        function get_edit_location_models(lid,lname)
        {
            $('#locations_id').val(lid);
            $('#locations').val(lname);
            $('#add_meeting_location_modales').modal('show');
        }
        function delete_meeting_location(lid)
        {
            if (confirm("Are you sure to delete this?"))
            {
                window.location.href ="<?php echo base_url(); ?>Attendee_admin/delete_meetinglocations/"+<?php echo $this->uri->segment(3); ?>+"/"+lid
            }    
        }
</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#badges_form").validate({
        errorElement: "label",
        errorClass: 'help-block',  
        rules: 
        {
            badges_title:{
                required: true,
                minlength: 6,
            },
            badges_sub_title:{
                required: true,
                minlength: 3,
            },
            badges_date:{
                required: true,
                minlength: 4,
            },
            sender_name:{
                required:true,
            },
            subject:{
                required:true,
            }
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
    $('#assign_moderator_form').validate({
        errorElement: "label",
        errorClass: 'help-block',  
        rules: 
        {
            moderator_user_id:{
                required: true,
            }
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
    $('#payment_processor_form').validate({
        errorElement:"span",
        errorClass: 'help-block',
        rules:{
            stripe_secret_key:{
                required:true,
            },
            stripe_public_key:{
                required:true,
            },
            stripe_payment_price:{
                required:true,
                number:true
            },
            stripe_payment_currency:{
                required:true,
            },
        },
        highlight: function (element) 
        {
          $(element).closest('.help-block').removeClass('valid');
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
        unhighlight: function (element) {
          $(element).closest('.form-group').removeClass('has-error');
        }
    });
});    
</script>
<style type="text/css">
#custom_colunm
{
    width: 20% !important;
}
.box{
    display: none;
}
#assign_moderator_form .select2-container .select2-choice{
width:350px;
margin:0 auto 20px;
}
#assign_attendee_moderator_popup .modal-content{
    overflow:hidden;
}
#assign_attendee_moderator_popup .modal-header{
    background: #007ce9 none repeat scroll 0 0;
    color:#fff;
}
#assign_attendee_moderator_popup .modal-header .close{
    color:#fff;opacity:1;
}
.col-sm-9.moderator_selectbox {
  display: block;
  float: none;
  margin: 0 auto;
}
</style>
<!-- end: PAGE CONTENT-->
<script>
  $(document).ready(function(){
       $(".showmodel").click(function()
       {        
            $("#Menu_id_1").select2('destroy').select2();
            var id = $(this).data('id');
            $("#beacon-name").val($(this).data('bname'));
            $("#beacon-id").val(id);
            <?php
            $html ="";
            foreach ($user_group as $key1 => $value1)
            {                  
                $html .= '<option id=menu'.$value1['id'].' value='.$value1['id'].'>'.$value1['group_name'].'</option>';
            }   
            ?>
            $("#Menu_id_1").html("<?php echo $html;?>");
            var selected = $(this).data('sel_val');
            $("#Menu_id_1").select2("val",selected);
            $('#exampleModal').modal('show');
       });
       $("#sample_123").DataTable({
        "bPaginate": false,
        "bInfo": false,
       })
});
</script>
