<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/attendee/form-validation.js?<?php echo time(); ?>"></script>
  


<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-core-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-full-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-helpers.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/tabs.jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/customformbuilder.js?<?php echo time(); ?>"></script>

<script>
	jQuery(document).ready(function() {
		TableData.init();
	    FormElements.init();
	    FormValidator.init();
	});
</script>


<script type="text/javascript">
$("#formBuilder").formBuilder({
   load_url: '<?php echo base_url(); ?>Attendee_admin/get_Registration_form_data/<?php echo $event['Id'];?>',
   save_url: '<?php echo base_url(); ?>Attendee_admin/save_registration_frombulder/<?php echo $event['Id']; ?>', 
   onSaveForm: function() {
      alert('Registration Screen Form Save SuccessFully.');
   }
});
</script> 