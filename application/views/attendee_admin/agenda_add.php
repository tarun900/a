<?php  $arr=$this->session->userdata('current_user');  $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding:0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="">
                            <a href="#indiviual_attendee" data-toggle="tab">
                                Invite Individual Attendee
                            </a>
                        </li>
                        <li class="active">
                            <a href="#multiple_attendee" data-toggle="tab">
                                Invite Multiple Attendees
                            </a>
                        </li>  
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>                  
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade in" id="indiviual_attendee">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Inviation Message
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea name="msg" id="content" style="height: 200px;" class="ckeditor form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Assign Agenda</label>
                                <div class="col-sm-9">
                                        <?php if(count($category_list)==1){ ?>
                                            <input type="hidden" name="session_id" id="session_id" value="<?php echo $category_list[0]['cid']; ?>"/>
                                            <input disabled="disabled" type="text" class="col-md-6 form-control required" value="<?php  echo $category_list[0]['category_name']; ?>" name="cat_name"/>    
                                        <?php }else{ ?>    
                                        <select class="col-md-12 form-control" id="session_id" name="session_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                        <?php } ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Invite Attendees Individually <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                       <input type="text" placeholder="Attendee email address" class="form-control required" id="email_address" name="email_address" onchange=""><br/>
                                       <input type="text" placeholder="Attendee First name" class="form-control required" id="first_name" name="first_name"><br/>
                                       <input type="text" placeholder="Attendee Last name" class="form-control required" id="last_name" name="last_name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Send Link
                                </label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="1" id="send_link" name="send_link">
                                            <span>Send Home Screen Link</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="0" id="send_link" name="send_link">
                                            <span>Send Registration Screen Link</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Subject <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Subject" class="form-control required" id="subject1" name="subject1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control" id="sent_name" name="sent_name">
                                    </div>
                                </div>
                            </div>
                           <!-- <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Email <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="email" value="<?php echo  $org_email; ?>" placeholder="Sender Email Address" class="form-control required" id="sent_from" name="sent_from">
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade active in" id="multiple_attendee">
                        <div id="assign_agenda_error_div" class="alert alert-danger" style="display: none;">

                        </div>
                        <form action="" method="post">    
                            <div class="row attendee_row" style="text-align: center;">
                                <div class="col-md-4 agenda_assign_label">
                                    <label>Assign New Agenda To Selected Attedees</label>
                                </div>
                                <div class="col-md-4 agenda_assign_select" style="padding-left: 0;">
                                    <div class="form-group">
                                    <div class="col-md-12">
                                        <?php if(count($category_list)==1){ ?>
                                            <input type="hidden" name="agenda_category_id" id="agenda_category_id" value="<?php echo $category_list[0]['cid']; ?>"/>
                                            <input disabled="disabled" type="text" class="col-md-6 form-control required" value="<?php  echo $category_list[0]['category_name']; ?>" name="cat_name"/>    
                                        <?php }else{ ?>    
                                        <select class="col-md-6 form-control required" id="agenda_category_id" name="agenda_category_id">
                                            <option value="">Select Agenda</option>
                                            <?php foreach ($category_list as $key => $value) { ?>
                                                <option value="<?php echo $value['cid']; ?>"><?php echo $value['category_name']; ?></option>
                                            <?php }  ?>
                                        </select>
                                        <?php } ?>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-green btn-block" id="assign_agenda_btn" type="button">Assign <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                       
                            <div id="attendee_list_table" class="table-responsive" style="overflow-x:scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" class="flat-grey selectall" >
                                                    </label>
                                                </div>
                                            </th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Company Name</th>
                                            <th>Title</th>
                                            <th>Agenda</th>
                                            <?php foreach ($keysdata as $key => $value) {?>
                                            <th ondblclick='updateheader("<?php echo $value['column_id']; ?>","<?php echo $value['column_name']; ?>");'><?php echo $value['column_name']; ?></th>
                                            <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($csvdata as $key => $value) { ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="attendees_id[]" value="<?php echo $value['Emailid']; ?>" id="attendees_id_<?php echo $value['Emailid'];?>"   class="foocheck">
                                                <label for="attendees_id_<?php echo $value['Emailid'];?>"></label>    
                                            </td>
                                            <td><?php echo $value['firstname']; ?></td>
                                            <td><?php echo $value['lastname']; ?></td>
                                            <td><?php echo $value['Emailid']; ?></td>
                                            <td><?php echo $value['Company_name'];?></td>
                                            <td><?php echo $value['Title']; ?></td>
                                            <td><?php echo ucfirst($value['category_name']); ?></td>
                                            <?php for($j=0;$j<count($keysdata);$j++){
                                                $custom=json_decode($value['extra_column'],TRUE);
                                                $keynm=$keysdata[$j]['column_name']; ?>
                                                <td><?php echo $custom[$keynm]; ?></td>
                                            <?php } ?>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <form action="<?php echo base_url().'Attendee_admin/skip_to_invite_attendee/'.$this->uri->segment(3); ?>" method="post">
                            <div class="form-group" style="margin-top: 15px;">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="1" class="square-orange" id="skip_to_invite" name="skip_to_invite">
                                    <span style="font-weight: bold;">Skip Invite</span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="margin-top:2%;">
                                    <button type="submit" class="btn btn-theme_green btn-block">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="border:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Column Name</h4>
        </div>
        <div class="modal-body">
            <form method="post" action="<?php echo base_url().'Attendee_admin/savecoumnname/'.$this->uri->segment(3); ?>">
                <input type="Hidden" placeholder="Column Id" id="column_id" name="column_id" class="form-control" value="">
                <input type="text" placeholder="Column Name" id="column_name" name="column_name" class="form-control">
                <div class="alert alert-danger" style="display:none;color: red;" id="column_name_error">This field required.</div>
                <button class="btn btn-green btn-block" type="button" value="Update" name="update_btn" id="column_save_btn" style="margin-top:10px;">Save</button>
                <button class="btn btn-red btn-block" type="submit" value="Delete" name="delete_btn" id="column_delete_btn">Delete</button>
            </form>
        </div>
         <div class="modal-footer" style="border:none;display: none;">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> 
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function updateheader(cid,cnm)
    {
        $('#column_id').val(cid);
        $('#column_name').val(cnm);
        $("#myModal").modal({backdrop: true});
    }
    $('#column_save_btn').click(function(){
        var id=$.trim($('#column_id').val());
        var name=$.trim($('#column_name').val());
        if(name=="")
        {
            $('#column_name_error').show();
        }
        else
        {
            $('#column_save_btn').attr('type','submit');
        }
    });
    function setEmailTemplate()
    {
            var Slug=$("#template").val();
           
             $.ajax({
                url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
                data :'Slug='+Slug,
                type: "POST",  
                async: false,
                success : function(data)
                {
                    $("#content").code('');
                    $("#content").code(data);
                }
            });
    }
    function setEmailTemplate1()
    {
            var Slug=$("#template").val();
           
             $.ajax({
                url : '<?php echo base_url(); ?>Attendee_admin/getEmailTemplate/<?php echo $this->uri->segment(3); ?>',
                data :'Slug='+Slug,
                type: "POST",  
                async: false,
                success : function(data)
                {
                    $("#content1").code('');
                    $("#content1").code(data);
                }
            });
    }
</script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script>
    $(window).load(function() {
        $('#assign_agenda_btn').click(function(){
            var table=$('#sample_1').DataTable();
            var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
            if($('#agenda_category_id').val()!="" && chkbox_checked>0)
            {
                $('#assign_agenda_error_div').hide();
                var data = table.$('input').serialize();
                data=data+"&agenda_category_id="+$('#agenda_category_id').val();
                $.ajax({
                    url : "<?php echo base_url().'Attendee_admin/assign_agenda_temp_attendee/'.$this->uri->segment(3); ?>",
                    data:data,
                    type: "POST",
                    success : function(data)
                    {
                        var values=data.split('###');
                        if($.trim(values[0])=="success")
                        {
                            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                            if(regex .test(values[1]))
                            {
                                window.location.href=values[1];   
                            }
                        }
                        else
                        {
                            $('#assign_agenda_error_div').html(values[1]);
                            $('#assign_agenda_error_div').slideDown('slow');
                            setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
                        }    
                    }
                });
            }
            else
            {
                $('#assign_agenda_error_div').html('Plase Select Attendees or Agenda');
                $('#assign_agenda_error_div').slideDown();
                setInterval(function(){ $('#assign_agenda_error_div').slideUp(1500,"linear"); }, 3000);
            }
        });
   });
</script>