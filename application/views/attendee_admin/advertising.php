<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertising extends FrontendController {
    function __construct() {

        $this->data['pagetitle'] = 'Advertising';
        $this->data['smalltitle'] = 'Advertising Details';
        $this->data['breadcrumb'] = 'Event advertising';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('advertising_model');
        $this->load->model('event_model');
        $this->load->model('setting_model');
        $this->load->model('speaker_model');
        $this->load->model('profile_model');
        $this->load->model('cms_model');
        $this->load->library('upload');
    }

    public function index($id)           
    {     
        if($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->advertising_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            
        }
        if($this->data['user']->Role_name == 'Client')
        {
            $event = $this->event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $advertising_list = $this->advertising_model->get_advertising_list($id);
            $this->data['advertising_list'] = $advertising_list;
        }
        
        $menudata=$this->event_model->geteventmenu($id,5);
        $menu_toal_data=$this->event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        
        $this->template->write_view('css', 'advertising/css', $this->data, true);
        $this->template->write_view('content', 'advertising/index', $this->data, true);

        if($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        }
        
        $this->template->write_view('js', 'advertising/js', true);
        $this->template->render();
    }

    public function add($id)
    {

        $Event_id = $id;
        //echo $Event_id; exit;
        $user = $this->session->userdata('current_user');
        $Organisor_id = $user[0]->Id;

        $menu_toal_data=$this->event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $cms_list = $this->cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;

        $event_templates = $this->event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;

        $advertising_list = $this->advertising_model->get_advertising_list($id);
        $this->data['advertising_list'] = $advertising_list;
      
        if($this->data['user']->Role_name == 'Client')
        {          

            if($this->input->post())
            {   
           
                if(!empty($_FILES['H_images']['name'][0]))
                {
                    foreach ($_FILES['H_images']['name'] as $a=>$b)
                    {
                        $b= str_replace(' ', '', $b);
                        
                        if(file_exists("./assets/user_files/".$b))
                            $H_images[] = strtotime(date("Y-m-d H:i:s")).'_'.$b;
                        else
                            $H_images[] = $b;
                    }
                    $this->upload->initialize(array(
                        "file_name"     => $H_images,
                        "upload_path"   => "./assets/user_files",
                        "allowed_types" => '*',
                        "max_size" => '1000000',
                        "max_width"  => '3000',
                        "max_height"  => '3000'
                    ));
                    
                    if (!$this->upload->do_multi_upload("H_images")) 
                    {    
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error',"For Header Images, ".$error['error']);
                        //return false;
                    }
                }

                if (!empty($_FILES['F_images']['name'][0])) 
                {
                    foreach ($_FILES['F_images']['name'] as $c => $d) 
                    {
                        $d = str_replace(' ', '', $d);

                        if (file_exists("./assets/user_files/" . $d))
                            $F_images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $d;
                        else
                            $F_images[] = $d;
                    }

                    $this->upload->initialize(array(
                        "file_name" => $F_images,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => '*',
                        "max_size" => '1000000',
                        "max_width" => '3000',
                        "max_height" => '3000'
                    ));

                    if (!$this->upload->do_multi_upload("F_images")) 
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error', "For Footer Images, " . $error['error']);
                        //return false;
                    }

                }
            
                $data['advertising_array']['H_images'] = $H_images;
                $data['advertising_array']['F_images'] = $F_images;
                $Menu_id = $this->input->post('Menu_id');
                $data['advertising_array']['Menu_id'] = implode(',', $Menu_id);
                $Cms_id = $this->input->post('Cms_id');
                $data['advertising_array']['Cms_id'] = implode(',', $Cms_id);
                $data['advertising_array']['Header_link'] = $this->input->post('Header_link');
                $data['advertising_array']['Footer_link'] = $this->input->post('Footer_link');
                $data['advertising_array']['Advertisement_name'] = $this->input->post('Advertisement_name');
                $data['advertising_array']['Organisor_id'] = $Organisor_id;
                $data['advertising_array']['Event_id'] = $Event_id;

                $advertising_id = $this->advertising_model->add_advertising($data);
                $this->session->set_flashdata('advertising_data', 'Added');
                redirect("advertising/index/".$id);
            } 

            $this->template->write_view('css', 'advertising/add_css', $this->data , true);
            $this->template->write_view('content', 'advertising/add', $this->data , true);

            if($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
            }
            $this->template->write_view('js', 'advertising/add_js', $this->data , true);
            $this->template->render();
        }
    }

    public function edit($id=null,$aid=null)
    {        

        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $menu_toal_data=$this->event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $cms_list = $this->cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;

        if($this->data['user']->Role_name == 'Client')
        { 

            $event = $this->event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $advertising_by_id = $this->advertising_model->get_advertising_list_by_id($id,$aid);
            $this->data['advertising_by_id'] = $advertising_by_id;

            if($this->data['page_edit_title'] = 'edit')
            {
                if($id == NULL || $id == '')
                {
                    redirect('event');
                }
                if($this->input->post())
                {   
                    if(!empty($_FILES['H_images']['name'][0]))
                    {
                        foreach ($_FILES['H_images']['name'] as $k=>$v)
                        {
                            $v= str_replace(' ', '', $v);
                            
                            if(file_exists("./assets/user_files".$v))
                                $H_images[] = strtotime(date("Y-m-d H:i:s")).'_'.$v;
                            else
                                $H_images[] = $v;
                        }
                        $this->upload->initialize(array(
                            "file_name"     => $H_images,
                            "upload_path"   => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '1000000',
                            "max_width"  => '3000',
                            "max_height"  => '3000'
                        ));
                        
                        if (!$this->upload->do_multi_upload("H_images")) {    
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('error',"For Header Images, ".$error['error']);
                            //return false;
                        }
                    }

                    if(!empty($_FILES['F_images']['name'][0]))
                    {
                        foreach ($_FILES['F_images']['name'] as $a=>$b)
                        {
                            $b= str_replace(' ', '', $b);
                            
                            if(file_exists("./assets/user_files".$b))
                                $F_images[] = strtotime(date("Y-m-d H:i:s")).'_'.$b;
                            else
                                $F_images[] = $b;
                        }
                        $this->upload->initialize(array(
                            "file_name"     => $F_images,
                            "upload_path"   => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '1000000',
                            "max_width"  => '3000',
                            "max_height"  => '3000'
                        ));
                        
                        if (!$this->upload->do_multi_upload("F_images")) {    
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('error',"For Footer Images, ".$error['error']);
                            //return false;
                        }
                    }
             
                    $data['advertising_array']['Organisor_id'] = $logged_in_user_id;
                    $data['advertising_array']['Event_id'] = $Event_id;
                    $data['advertising_array']['H_images'] = $H_images;
                    $data['advertising_array']['old_h_images'] = $this->input->post('old_h_images');
                    $data['advertising_array']['F_images'] = $F_images;
                    $data['advertising_array']['old_f_images'] = $this->input->post('old_f_images');
                    $Menu_id = $this->input->post('Menu_id');
                    $data['advertising_array']['Menu_id'] = implode(',', $Menu_id);
                    $Cms_id = $this->input->post('Cms_id');
                    $data['advertising_array']['Cms_id'] = implode(',', $Cms_id);
                    $data['advertising_array']['Header_link'] = $this->input->post('Header_link');
                    $data['advertising_array']['Footer_link'] = $this->input->post('Footer_link');
                    $data['advertising_array']['Advertisement_name'] = $this->input->post('Advertisement_name');

                    $this->advertising_model->update_advertising($data);
                    $this->session->set_flashdata('advertising_data', 'Updated');
                    redirect("advertising/index/".$id);
                }
               
                $this->session->set_userdata($data);
                $this->template->write_view('css', 'advertising/add_css', $this->data , true);
                $this->template->write_view('content', 'advertising/edit', $this->data , true);

                if($this->data['user']->Role_name == 'User')
                {
                    $total_permission = $this->advertising_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
                }
                else
                {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
                }

                $this->template->write_view('js', 'advertising/add_js', $this->data , true);
                $this->template->render();
            }        
        }
    }

    public function delete_advertising($id,$Event_id)
    {

        if($this->data['user']->Role_name == 'Client')
        {
            $event = $this->event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $advertising_list = $this->advertising_model->get_advertising_list($id);
            $this->data['advertising_list'] = $advertising_list[0];
        }
        
        $advertising = $this->advertising_model->delete_advertising($id);
        $this->session->set_flashdata('advertising_data', 'Deleted');
        redirect("advertising/index/".$Event_id);
    }
}