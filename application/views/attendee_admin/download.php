<?php
{codecitation style="brush: PHP;"}
header('Content-Type: application/csv'); //Outputting the file as a csv file
header('Content-Disposition: attachment; filename=test.csv'); //Defining the name of the file and suggesting the browser to offer a 'Save to disk ' option
header('Pragma: no-cache');

echo readfile('example.csv'); //Reading the contents of the file
{/codecitation}

?>