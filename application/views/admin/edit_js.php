<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/form-elements.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js?<?php echo time(); ?>"></script>



<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-core-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-full-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-helpers.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/tabs.jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/formBuilder.jquery.js?<?php echo time(); ?>"></script>
<script>
	jQuery(document).ready(function() {
		FormElements.init();
        FormValidator.init();
	});
        
        function checkemail()
        {      
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Client/checkemail',
                data :'email='+$("#email").val()+'&idval='+$('#idval').val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#email').parent().removeClass('has-success').addClass('has-error');
                        $('#email').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#email').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        $('#email').parent().removeClass('has-error').addClass('has-success');
                        $('#email').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#email').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
</script>
<script type="text/javascript">
    


     $("#formBuilder").formBuilder({
       load_url: '<?php echo base_url(); ?>Attendee_admin/getJsonData/<?php echo $event['Id'];?>',
       save_url: '<?php echo base_url(); ?>Attendee_admin/singup/<?php echo $event['Id'];?>', 
       onSaveForm: function() {
          // redirect to demo page
          window.location.href = '<?php echo base_url(); ?>Attendee_admin/index/<?php echo $event['Id'];?>';
        }
    });
</script>    