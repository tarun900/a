<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-body" style="padding: 0px;">
				<div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="">
                            <a href="#archive_events" data-toggle="tab">
                                Archive Events
                            </a>
                        </li>
                        <li class="active">
                            <a href="#latest_events" data-toggle="tab">
                                Active Events
                            </a>
                        </li>
                        <li class="">
                            <a href="#feature_events" data-toggle="tab">
                                Future Events
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="archive_events">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Event Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0;$i<count($Archive_event);$i++) { ?>
                                        <tr>
                                            <td><?php echo $i+1; ?></td>
                                            <td><a target="_blank" href="<?php echo base_url().'Events/'.$Archive_event[$i]['Subdomain']; ?>"><?php echo $Archive_event[$i]['Event_name']; ?></a></td>
                                            <td><?php echo $Archive_event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Archive_event[$i]['End_date']; ?></td>
                                        	<td>
												<?php 

													if($Archive_event[$i]['Event_type'] =='1')
													{
														echo 'Public';
													}
													else
													{
														echo 'Private';
													}
												?>
											</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                               </table>
                            </div>
                        </div>

                        <div class="tab-pane fade active in" id="latest_events">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Event Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0;$i<count($Event);$i++) { ?>
                                        <tr>
                                            <td><?php echo $i+1; ?></td>
                                            <td><a target="_blank" href="<?php echo base_url().'Events/'.$Event[$i]['Subdomain']; ?>"><?php echo $Event[$i]['Event_name']; ?></a></td>
                                            <td><?php echo $Event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Event[$i]['End_date']; ?></td>
                                        	<td>
												<?php 

													if($Event[$i]['Event_type'] =='1')
													{
														echo 'Public';
													}
													else
													{
														echo 'Private';
													}
												?>
											</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="feature_events">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Event Type</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php for($i=0;$i<count($Feature_event);$i++) { ?>
                                        <tr>
                                            <td><?php echo $i+1; ?></td>
                                            <td><a target="_blank" href="<?php echo base_url().'Events/'.$Feature_event[$i]['Subdomain']; ?>"><?php echo $Feature_event[$i]['Event_name']; ?></a></td>
                                            <td><?php echo $Feature_event[$i]['Start_date']; ?></td>
                                            <td><?php echo $Feature_event[$i]['End_date']; ?></td>
                                        	<td>
												<?php 

													if($Feature_event[$i]['Event_type'] =='1')
													{
														echo 'Public';
													}
													else
													{
														echo 'Private';
													}
												?>
											</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>