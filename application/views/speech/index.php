<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Speeches <span class="text-bold">List</span></h4>
                <?php if($user->Role_name=='Speaker'){ ?>
                <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Speech/add"><i class="fa fa-plus"></i> Add Speech</a>
                <?php } ?>
                <div class="panel-tools"></div>
            </div>
            <div class="panel-body">
                <?php if($this->session->flashdata('speech_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('speaker_data'); ?> Successfully.
                </div>
                <?php } ?>
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Speech Title</th>
                            <th>Event Start Date</th>
                            <th>Event End Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;$i<count($speech_events);$i++) { ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $speech_text = substr($speech_events[$i]['Speech_text'], 0, 50); ?></td>
                            <td><?php echo $speech_events[$i]['Start_date']; ?></td>
                            <td><?php echo $speech_events[$i]['End_date']; ?></td>
                            <td>
                                <?php if($user->Role_name=='Speaker'){ ?>
                                <a href="<?php echo base_url(); ?>Speech/edit/<?php echo $speech_events[$i]['sid']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-edit"></i></a>
                                <?php } ?>
                                <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                <a href="javascript:;" onclick="delete_speech(<?php echo $speech_events[$i]['Speech_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>

<script type="text/javascript">
    function delete_speech(Speech_id)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Speech/delete/"+Speech_id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->