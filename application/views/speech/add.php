<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Speech</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
					<div class="row">
                        <?php if($this->session->flashdata('speech_data') == "Updated"){ ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Added Successfully.
                        </div>
                        <?php } ?>
							<div class="form-group">
								<label class="col-sm-2" for="form-field-1">Speech<span class="symbol required"></span>
								</label>
                                <div class="col-sm-9">
                                    <textarea name="Speech_text" id="Speech_text" placeholder="Speech" class="summernote"></textarea>
							    </div>
                            </div>
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Documents</label>
                                <div class="col-sm-9">
                                    <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                        <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                <input type="file" id="Documents" name="Documents[]" multiple>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Events Name</label>
                                <div class="col-sm-5">
                                    <select id="form-field-select-4 Event_id" class="form-control" name="Event_id">
                                        <?php 
                                            echo"<option value=''>Select Event Name</option>";
                                            foreach ($speaker_events as $key => $value) 
                                            {
                                                echo'<option value="'.$value['Event_id'].'">'.$value['Event_name'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Agenda Name</label>
                                <div class="col-sm-5">
                                    <select id="form-field-select-4 Event_id" class="form-control" name="Agenda_id">
                                        <?php 
                                            echo"<option value=''>Select Agenda Name</option>";
                                            foreach ($speaker_agenda as $key => $value) 
                                            {
                                                echo'<option value="'.$value['Id'].'">'.$value['Heading'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php 
                                $user = $this->session->userdata('current_user');
                                $logged_in_user_id = $user[0]->Id;
                            ?>
                            <input type="hidden" id="Speaker_id" name="Speaker_id" value="<?php echo $logged_in_user_id; ?>">
                            <div class="row">
                                <div class="col-md-4">
                                        <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>				
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
    .form-horizontal .form-group
    {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
</style>
<!-- end: PAGE CONTENT-->