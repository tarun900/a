<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-body" style="padding: 0px;">
                <a href="<?php echo base_url().'My_favorites_admin/Export_favorites/'.$event_id; ?>" class="btn btn-green list_page_btn">
                    Export
                </a>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#my_favorites_option" data-toggle="tab">
                                Modules order
                            </a>
                        </li>
                        <li>
                            <a href="#menu_setting" data-toggle="tab">
                                Menu Setting
                            </a>
                        </li>
                        <li>
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="my_favorites_option">
                        <div class="row">
                            <form class="" id="form_modules_order" name="form_modules_order" method="POST" action="<?php echo base_url().'My_favorites_admin/save_modules_order/'.$event_id ?>">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Attendees Position in Directory <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select name="attendee_position" class="form-control required">
                                                    <option value="">Select Position</option>
                                                    <option value="1" <?php echo $modules_arr[0]['position']=='1' ? 'selected="selected"' : ''; ?>>1st</option>
                                                    <option value="2" <?php echo $modules_arr[0]['position']=='2' ? 'selected="selected"' : ''; ?>>2nd</option>
                                                    <option value="3" <?php echo $modules_arr[0]['position']=='3' ? 'selected="selected"' : ''; ?>>3rd</option>
                                                    <option value="4" <?php echo $modules_arr[0]['position']=='4' ? 'selected="selected"' : ''; ?>>4th</option>
                                                </select>    
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Banner Color <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12 color-pick">
                                                <input type="text" class="color {hash:true} form-control required" value="<?php echo $modules_arr[0]['color']; ?>" name="attendees_color">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Exhibitors Position in Directory <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select name="exhibitors_position" class="form-control required">
                                                    <option value="">Select Position</option>
                                                    <option value="1" <?php echo $modules_arr[1]['position']=='1' ? 'selected="selected"' : ''; ?>>1st</option>
                                                    <option value="2" <?php echo $modules_arr[1]['position']=='2' ? 'selected="selected"' : ''; ?>>2nd</option>
                                                    <option value="3" <?php echo $modules_arr[1]['position']=='3' ? 'selected="selected"' : ''; ?>>3rd</option>
                                                    <option value="4" <?php echo $modules_arr[1]['position']=='4' ? 'selected="selected"' : ''; ?>>4th</option>
                                                </select>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Banner Color <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12 color-pick">
                                                <input type="text" class="color {hash:true} form-control required" value="<?php echo $modules_arr[1]['color']; ?>" name="exhibitors_color">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Speakers Position in Directory <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select name="speakers_position" class="form-control required">
                                                    <option value="">Select Position</option>
                                                    <option value="1" <?php echo $modules_arr[2]['position']=='1' ? 'selected="selected"' : ''; ?>>1st</option>
                                                    <option value="2" <?php echo $modules_arr[2]['position']=='2' ? 'selected="selected"' : ''; ?>>2nd</option>
                                                    <option value="3" <?php echo $modules_arr[2]['position']=='3' ? 'selected="selected"' : ''; ?>>3rd</option>
                                                    <option value="4" <?php echo $modules_arr[2]['position']=='4' ? 'selected="selected"' : ''; ?>>4th</option>
                                                </select>    
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Banner Color <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12 color-pick">
                                                <input type="text" class="color {hash:true} form-control required" value="<?php echo $modules_arr[2]['color']; ?>" name="speakers_color">
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Sponsors Position in Directory <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select name="sponsors_position" class="form-control required">
                                                    <option value="">Select Position</option>
                                                    <option value="1" <?php echo $modules_arr[3]['position']=='1' ? 'selected="selected"' : ''; ?>>1st</option>
                                                    <option value="2" <?php echo $modules_arr[3]['position']=='2' ? 'selected="selected"' : ''; ?>>2nd</option>
                                                    <option value="3" <?php echo $modules_arr[3]['position']=='3' ? 'selected="selected"' : ''; ?>>3rd</option>
                                                    <option value="4" <?php echo $modules_arr[3]['position']=='4' ? 'selected="selected"' : ''; ?>>4th</option>
                                                </select>    
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                            Banner Color <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-12 color-pick">
                                                <input type="text" class="color {hash:true} form-control required" value="<?php echo $modules_arr[3]['color']; ?>" name="sponsors_color">
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-sm-12" style="margin-top:10px;margin-left: 15px;">
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-green btn-block">Submit</button>
                                    </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                            <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form').submit();">
                                            <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
    $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
});
$("#form_modules_order").validate({
errorElement: "span",
errorClass: 'help-block',  
rules: {
  attendee_position:{
    required:true
  },
  attendees_color:{
    required:true
  },
  exhibitors_position:{
    required:true
  },
  exhibitors_color:{
    required:true
  },
  speakers_position:{
    required:true
  },
  speakers_color:{
    required:true
  },
  sponsors_position:{
    required:true
  },
  sponsors_color:{
    required:true
  }
},
highlight: function (element) 
{
  $(element).closest('.help-block').removeClass('valid');
  $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
},
unhighlight: function (element) {
  $(element).closest('.form-group').removeClass('has-error');
  $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
}
});
</script>