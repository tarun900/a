<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <style>
      #heatmapContainer {
     background-image: url(<?php echo base_url(); ?>assets/user_files/beacons/<?php echo $heatmap_data[0]['image']; ?>);
     background-size: contain;
     background-repeat: no-repeat;
      }
      #heatmapContainer img{
    vertical-align: top;
    width: auto; 
    opacity: 0;  
      }
    </style>
    <?php
    $j = 0;

    foreach ($heat_movements as $key => $value) {
       $heat[$j]['coords'] = explode(",",$value['coords']);
       $heat[$j]['x'] = round(($heat[$j]['coords'][0] + $heat[$j]['coords'][2]) / 2); 
       $heat[$j]['y'] = round(($heat[$j]['coords'][1] + $heat[$j]['coords'][3]) / 2); 
       $heat[$j]['clicks'] = $value['number_of_hits'];

       $j++;
     }
     function sortByClick($a, $b)
      {
        return $b['clicks'] - $a['clicks'];
      }
      usort($heat, 'sortByClick'); 
     //echo "<pre>";print_r($heat);exit();
    ?>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#map_list" data-toggle="tab">
                             Image Mapping
                        </a>
                    </li>
                </ul>
            </div>


            <div class="panel-body" style="padding: 0px;">
                 <?php if ($this->session->flashdata('map_region_data')) { ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Map Region <?php echo $this->session->flashdata('map_region_data'); ?> Successfully.
                        </div>
                    <?php } ?>

                <div class="tab-content" style="overflow-x: scroll">
                <div width="100%">
                  <div id="heatmapContainerWrapper" style="margin: 0 auto;display: table;">
                    <div id="heatmapContainer">
                       <img src="<?php echo base_url(); ?>assets/user_files/beacons/<?php echo $heatmap_data[0]['image']; ?>" id="image">
                    </div>
                    <div id="heatmapLegend" style="opacity: 0">
                       <h2>Clicks</h2>
                       <span id="min"></span>
                       <span id="max"></span>
                       <img id="gradient" src=""/>
                    </div>
                  </div>
                </div>
                  
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/heatmap.js"></script>
<script>
   window.onload = function() {
     // helper function
     function $(id) {
       return document.getElementById(id);
     };
   
     /*  legend code */
     // we want to display the gradient, so we have to draw it
     var legendCanvas = document.createElement('canvas');
     legendCanvas.width = 100;
     legendCanvas.height = 10;
   
     var legendCtx = legendCanvas.getContext('2d');
     var gradientCfg = {};
   
     function updateLegend(data) {
       // the onExtremaChange callback gives us min, max, and the gradientConfig
       // so we can update the legend
       $('min').innerHTML = data.min;
       $('max').innerHTML = data.max;
       // regenerate gradient image
       if (data.gradient != gradientCfg) {
         gradientCfg = data.gradient;
         var gradient = legendCtx.createLinearGradient(0, 0, 100, 1);
         for (var key in gradientCfg) {
           gradient.addColorStop(key, gradientCfg[key]);
         }
   
         legendCtx.fillStyle = gradient;
         legendCtx.fillRect(0, 0, 100, 10);
         $('gradient').src = legendCanvas.toDataURL();
       }
     };
     /* legend code end */
   
   
     // create a heatmap instance
     var heatmap = h337.create({
       container: document.getElementById('heatmapContainer'),
       maxOpacity: .5,
       radius: 10,
       blur: .75,
       // update the legend whenever there's an extrema change
       onExtremaChange: function onExtremaChange(data) {
         updateLegend(data);
       }
     });
   
     // boundaries for data generation
   /*        var width = (+window.getComputedStyle(document.body).width.replace(/px/,''));
     var height = (+window.getComputedStyle(document.body).height.replace(/px/,''));*/
     var width = (+window.getComputedStyle(document.getElementById('heatmapContainer')).width.replace(/px/,''));
     var height = (+window.getComputedStyle(document.getElementById('heatmapContainer')).height.replace(/px/,''));
      
     //alert(height);  
   
     // generate 1000 datapoints
     var generate = function() {
       // randomly generate extremas
       var extremas = [100 >> 0];
       
       var min = <?php echo $heat[sizeof($heat)-1]['clicks']-1;?>;
       var max = <?php echo $heat[0]['clicks']-1;?>;
       
       var t = [];

      <?php foreach ($heat as $key => $value) {
        $click = ($value['clicks']) ? $value['clicks'] : 0;
        if($click)
        {
          echo "t.push({ x: ".$value['x'].", y:".$value['y'].", value: ".$click.", radius: 50 });";
        }
      }

      ?>  
      console.log(t);
       var init = +new Date;
   
       heatmap.setData({
         min: min,
         max: max,
         data: t
       });
   
       console.log('took ', (+new Date) - init, 'ms');
     };
     generate();
   
   };
</script>



