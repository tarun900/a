<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#map_list" data-toggle="tab">
                             Image Mapping
                        </a>
                    </li>
                </ul>
            </div>


            <div class="panel-body" style="padding: 0px;">
                 <?php if ($this->session->flashdata('map_region_data')) { ?>
                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                            <i class="fa fa-remove-sign"></i> Map Region <?php echo $this->session->flashdata('map_region_data'); ?> Successfully.
                        </div>
                    <?php } ?>

                <div class="tab-content">
                      <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                      <p style="text-align:center;">Click on areas of your Heat Map to mark where your beacons are in relation to the Map</p>
                          <div id="ref">
                                <img  src="<?php echo base_url(); ?>assets/user_files/beacons/<?php echo $heatmap_data[0]['image']; ?>" id="#photo" style="position: relative;" usemap="#mape" class="map1">
                                <map id="mape1" name="mape">
                                <?php foreach ($heatmap_mapping_data as $key => $value) { ?>
                                   <area title="<?php echo $value['beacon_name']; ?>" alt="Mapping"  id="are" shape="rect"  onclick='delete1("<?php echo $value['Id'];?>")' href="javascript:void(0);" 
                                  coords="<?php echo  $value['coords']; ?>">
                                <?php } ?>
                                </map>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

 
  <div class="modal-scrollable" style="z-index: 1060;">
  <div class="modal fade" id="myModal123" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content mapping-popup">
        <div class="modal-body">
        <strong>Please Select Beacons: </strong>
        <select id="user" style="width:73%;margin-bottom: 15px;">
          <?php  
          foreach ($beacon_data as $key => $row) { ?>
             <option value="<?php echo $row['Id']; ?>"><?php echo $row['beacon_name']; ?></option>
          <?php } ?>
        </select> 
        <div class="modal-footer">
         <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">cancel</button>
         <button type="button" id="close"  class="btn btn-default" data-dismiss="modal">Submit</button>
        </div>
      </div>
    </div>
  </div>
  </div>
<div class="modal-backdrop" style="z-index: 1050;display:none;"></div>
<script type="text/javascript">
    function delete1(a)
    {	
      if (confirm("Are you sure to delete this region?"))
      {
        var urlde="<?php echo base_url()."Beacons/delete_heatmap_coords/".$this->uri->segment(3)."/".$this->uri->segment(4)."/";?>"+a;
        setTimeout(function(){document.location.href = urlde;},500);
        return false;
      }  
    }
    $(document).ready(function() 
    {
        $('.map1').mapster({
        selected: true
    });
    var demo;
    $('.map1').imgAreaSelect({
        handles: true,
        autoHide:true,

        hide:true,
        onSelectEnd: function(img,selection){
            $('#myModal123').attr('class','modal fade in');
            $('#myModal123').attr('style','display:block;');
            $('body').attr('class','modal-open page-overflow');
            $('.modal-backdrop').addClass('fade in');
            $('.modal-backdrop').show();
             demo=selection.x1 + ',' + selection.y1+','+selection.x2+','+selection.y2;
        }

    });
    $('#cancel').click(function(){
        $('#myModal123').attr('class','modal fade');
        $('#myModal123').attr('style','display:none;');
        $('body').removeAttr('class');
        $('.modal-backdrop').removeClass('fade in');
        $('.modal-backdrop').hide();
    });
    $('#cancel_agenda_select').click(function(){
        $('#myModal123').attr('class','modal fade');
        $('#myModal123').attr('style','display:none;');
        $('body').removeAttr('class');
        $('.modal-backdrop').removeClass('fade in');
        $('.modal-backdrop').hide();
    });
    $('#close').click(function(){
        var person=$('#user').val();
            $.ajax({
                url:'<?php echo base_url(); ?>Beacons/save_heatmap_coords/<?php echo $this->uri->segment(3)."/".$this->uri->segment(4);?>',
                data:'coor='+demo+'&name='+person,
                method:'post',
                success:function(data)
                {
                    $('#are').mapster('deselect');
                    $('#mape1').append(data);
                    $('.map1').mapster({
                         selected: true
                     });
                },
            });
        $('#myModal123').attr('class','modal fade');
        $('#myModal123').attr('style','display:none;');
        $('body').removeAttr('class');
        $('.modal-backdrop').removeClass('fade in');
        $('.modal-backdrop').hide();
    });
     $('#close_agenda_select').click(function(){
        var session_id=$('#agenda_session').val();
          $.ajax({
              url:'<?php echo base_url(); ?>Map/save_coords_with_session/<?php echo $event_id."/".$map_id;?>',
              data:'coor='+demo+'&session_id='+session_id,
              method:'post',
              success:function(data){              
                $('#are').mapster('deselect');
                $('#mape1').append(data);
                $('.map1').mapster({
                    selected: true
                 });
              },
          });
        $('#myModal123').attr('class','modal fade');
        $('#myModal123').attr('style','display:none;');
        $('body').removeAttr('class');
        $('.modal-backdrop').removeClass('fade in');
        $('.modal-backdrop').hide();
    });
    
    
    });
</script>
<style type="text/css">
.map1 { margin: 0 auto; }
</style>


