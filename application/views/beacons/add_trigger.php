<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#add_notification" data-toggle="tab">
                                Add Trigger
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="add_notification">
                    	<div class="col-sm-10">
                			<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                				<h4 style="color: #5381ce;">New Trigger</h4>
                				<div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Title <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <input type="text" maxlength="20" placeholder="Trigger Title" id="name" name="trigger_name" class="form-control name_group required limited">
	                                </div>
	                            </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Message <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <textarea  maxlength="60" id="Description form-field-23" class="form-control limited" name="message" style="width:100%;height:100px;"><?php  ?></textarea>
	                                </div>
	                            </div>

	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Open button link <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <?php $arrkey=array(5,8,14,18,19,21,26,27,28,29); ?>
	                                    <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="modules">
	                                    <option value="">Select Modules</option>
	                                    <?php foreach ($menu_toal_data as $key => $value) { 
	                                            if(!in_array($value['id'], $arrkey)){
	                                        ?>
	                                    <option id="menu<?php echo $key; ?>" value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
	                                    <?php } } ?>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Connect to Beacon <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                   <select multiple="multiple" style="margin-top:-8px;" id="beacon_id[]" class="select2-container select2-container-multi form-control search-select menu-section-select required" name="beacon_id[]">
		                                    <?php foreach ($beacon_data as $key => $value) { ?>
		                                    	<option value="<?php echo $value['Id']; ?>"> <?php echo $value['beacon_name']; ?> </option>
		                                    <?php } ?>
	                                    </select>
                            		</div>
	                            </div> 

	                            <div class="form-group">
	                                <div class="col-md-2 pull-left">
	                                    <button class="btn btn-green btn-block" type="submit">
	                                        Send 
	                                    </button>
	                                </div>
	                            </div>       
                			</form>
                    	</div>
                    </div>
                </div>    
            </div>
        </div>        
	</div>
</div>