<?php $acc_name=$this->session->userdata('acc_name'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
            <div class="panel-body" style="padding: 0px;">
                <a style="margin-top: 0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Exhibitor_leads/export_my_lead/<?php echo @$event_id; ?>"> Export Leads </a>
                <?php if($this->session->flashdata('lead_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('lead_data'); ?>
                </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#my_leads" data-toggle="tab">
                                My Leads
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="my_leads">
                        <div class="table-responsive" style="overflow-x:scroll;">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FirstName</th>
                                        <th>LastName</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Company Name</th>
                                        <?php foreach ($custom_column as $key => $value) { ?>
                                        <th><?php echo ucfirst($value['column_name']); ?></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (@$my_lead as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo @$key+1; ?></td>
                                        <td><?php echo ucfirst($value['Firstname']); ?></td>
                                        <td><?php echo ucfirst($value['Lastname']); ?></td>
                                        <td><?php echo ucfirst($value['Email']); ?></td>
                                        <td><?php echo ucfirst($value['Title']); ?></td>
                                        <td><?php echo ucfirst($value['Company_name']); ?></td>
                                        <?php $custom_data=json_decode($value['custom_column_data'],true); foreach ($custom_column as $key => $value) { ?>
                                        <td><?php echo $custom_data[$value['column_name']]; ?></td>
                                        <?php } ?>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
function delete_survey(id)
{   
    if(confirm("Are you sure to delete this?"))
    {
        window.location.href="<?php echo base_url().'Exibitor_survey/delete_question/'.@$event_id.'/'; ?>"+id;
    }
}
</script>