<?php $acc_name=$this->session->userdata('acc_name');
$user = $this->session->userdata('current_user');?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/image_slider/owl.carousel.min.css" />
<script type="text/javascript">
var owl;
$(document).ready(function() {
  owl = jQuery("#owlslider_banner_div").owlCarousel({
    items : 1,
    autoplay: true,
    autoHeight:true,
    navigation : false,
    center: true,
    loop:true
  }); 
});
</script>
<?php $image_array = array_filter(json_decode($event_templates[0]['Images'],true)); if (!empty($image_array) || count($banner_list) > 0){  ?>
<div class="slider-banner">
  <div class="row">
    <div class="owl-carousel owl-theme" id="owlslider_banner_div">
      <?php foreach ($image_array as $key => $value) { if(file_exists('./assets/user_files/'.$value)){ ?>
        <img width="100%" height="500" title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>">
      <?php } } ?>
    </div>
    <?php foreach($banner_list as $key => $value): if(!empty($value['Image']) && file_exists('./assets/user_files/'.$value['Image'])){ ?>
      <div class="row image_contener_div">
        <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value['Image']; ?>" class="homescreenmap" usemap="#homescreenmape_<?php echo $value['id']; ?>">
        <map id="homescreenmape_<?php echo $value['id']; ?>" name="homescreenmape_<?php echo $value['id']; ?>">
          <?php 
          foreach ($value['coords'] as $ckey => $cvalue) {
            $area_url=base_url()."App/".$acc_name."/".$Subdomain;
            $area_title="";
            if(!empty($cvalue['menuid']))
            {
              if(!in_array($cvalue['menuid'],array(22,23,24,25)))
              {
                if($cvalue['menuid']==12)
                {
                  $area_url = base_url() .'Messages/'. $acc_name."/".$Subdomain.'/privatemsg';
                  $area_title="Private Messages";
                }
                else if($cvalue['menuid']==13)
                {
                  $area_url = base_url() .'Messages/'.$acc_name."/". $Subdomain.'/publicmsg';
                  $area_title="Public Messages";
                }
                else if($cvalue['menuid']==42)
                {
                  $area_url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/fundraising_donation';
                  $area_title="Fundraising Donation";
                }
                else if($cvalue['menuid'] == 56)
                { 
                  if($user[0]->Rid=='6'){ ?>
                  <li>
                    <?php $area_url =  base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>
                  </li>
                  <?php } ?>
                  <?php if($user[0]->Rid=='4'){ ?>
                  <li>
                    <?php $area_url =  base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>
                  </li>
                  <?php } ?>
                  <?php if($user[0]->Rid=='7' && $user[0]->is_moderator=='1'){ ?>
                  <li>
                    <?php $area_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>
                  </li>
                  <?php } 
                }
                else
                {
                  $area_url = base_url() . ucfirst($cvalue['pagetitle']) . '/' . '' . $acc_name."/".$Subdomain;
                  $area_title=$cvalue['menuname'];
                }
              }
              else
              {
                $area_url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/get_auction_pr';
                $var_target="";
                if($cvalue['menuid']==22)
                {
                  $var_target='target="_blank"';
                  $area_url=$area_url.'/1';
                  $area_title="Silent Auction";
                }
                else if($cvalue['menuid']==23)
                {
                  $var_target='target="_blank"';
                  $area_url=$area_url.'/2';
                  $area_title="Live Auction";
                }                                        
                else if($cvalue['menuid']==24)
                {
                  $var_target='target="_blank"';
                  $area_url=$area_url.'/3';
                  $area_title="Buy Now";
                }
                else if($cvalue['menuid']==25)
                {
                  $var_target='target="_blank"';
                  $area_url=$area_url.'/4';
                  $area_title="Pledge";
                }
              }
            }
            else if(!empty($cvalue['cmsid']))
            {
              $area_url=base_url().'Cms/'.$acc_name.'/'.$Subdomain.'/View/'.$cvalue['cmsid'];
              $area_title="Custom Modules";
            }
            else if(!empty($cvalue['agenda_id']))
            {
              $area_url=base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/view_category_session/'.$cvalue['agenda_id'];
              $area_title="Agenda Caegory";
            }
            else
            {
              $area_url=$cvalue['redirect_url'];
              $area_title="Other Url";
            }
            echo '<area title="'.$area_title.'" alt="'.$area_title.'" shape="rect" href="'.$area_url.'" coords="'.$cvalue['coords'].'">';
          } ?>
        </map>
        <?php if(!empty(strip_tags($value['Content']))){ ?>
        <div class="image_over_content">
          <?php echo $value['Content']; ?>
        </div>
        <?php } ?>
      </div>
    <?php } endforeach ?>
  </div>
</div>
<?php } $description= preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_templates[0]['Description']))); 
if($description!=""){
  $user = $this->session->userdata('current_user');
  if($user[0]->Role_id==4)
  {
    $extra=json_decode($custom[0]['extra_column'],true);
    foreach ($extra as $key => $value) {
      $keyword="{".str_replace(' ', '', $key)."}";
      if(stripos(strip_tags($event_templates[0]['Description'],$keyword)) !== false)
      {
        $event_templates[0]['Description']=str_ireplace($keyword, $value,$event_templates[0]['Description']);
      }
    }
  } ?>
<div class="event-content panel-white">
  <div class="event_description">
    <p style="padding-left: 25px;">
    <?php echo html_entity_decode($event_templates[0]['Description']); ?>
    </p>
  </div>
</div>
<?php } if($event_templates[0]['Background_img']!=""){ ?>
<div class="event-list" style="display:block;<?php $bgimg = json_decode($event_templates[0]['Background_img']); for($i=0;$i<count($bgimg);$i++) { ?>background-image:url('<?php echo base_url()."assets/user_files/".$bgimg[$i]; ?>') <?php } ?>">
<?php } else{ ?><div class="event-list" style="background-color: #FFFFFF;"><?php } ?>
  <div class="row">
    <div class="col-md-12">
      <?php $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
      foreach($active_icon as $key => $value){ 
        $img_view = $value['img_view'];
        if(!empty($value['img']) && file_exists('./assets/user_files/'.$value['img']))
        {
          $img = base_url().'assets/user_files/'.$value['img'];
        }
        else
        {
          if(in_array($value['menu_id'],$iconset))
          {
            $img = base_url()."assets/css/images/icons/icon".$event_templates[0]['icon_set_type']."/".$value['menu_id'].".png";
          }
          else
          {
            $img = base_url()."assets/images/EventApp_Default.jpg";
          }            
        }
        if(!empty($value['menu_id']))
        {
          if(!in_array($value['menu_id'],array(22,23,24,25)))
          {
            if($value['menu_id']==12)
            {
              $url = base_url() .'Messages/'. $acc_name."/".$Subdomain.'/privatemsg';
            }
            else if($value['menu_id']==13)
            {
              $url = base_url() .'Messages/'.$acc_name."/". $Subdomain.'/publicmsg';
            }
            else if($value['menu_id']==42)
            {
              $url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/fundraising_donation';
            }
            else
            {
              $url = base_url() . ucfirst($value['pagetitle']) . '/' . '' . $acc_name."/".$Subdomain;
            }
          }
          else
          {
            $url = base_url() . 'fundraising' . '/'.$acc_name."/". $Subdomain.'/home/get_auction_pr';
            $var_target="";
            if($value['menu_id']==22)
            {
              $var_target='target="_blank"';
              $url=$url.'/1';
            }
            else if($value['menu_id']==23)
            {
              $var_target='target="_blank"';
              $url=$url.'/2';
            }                                        
            else if($value['menu_id']==24)
            {
              $var_target='target="_blank"';
              $url=$url.'/3';
            }
            else if($value['menu_id']==25)
            {
              $var_target='target="_blank"';
              $url=$url.'/4';
            }
          }
        }
        else if(!empty($value['cms_id']))
        {
          if(!empty($value['img']) && file_exists('./assets/user_files/'.$value['img']))
          {
            $img = base_url().'assets/user_files/'.$value['img'];
          }
          else
          {
            $img = base_url()."assets/images/Custom_Module_Icon.png";
          }
          $url=base_url().'Cms/'.$acc_name.'/'.$Subdomain.'/View/'.$value['cms_id'];
        }
        else if(!empty($value['agenda_id']))
        {
          $url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/view_category_session/'.$value['agenda_id'];
        }
        else
        {
          $url=$value['redirect_link'];
        }
       ?>
        <div class="col-md-6 col-lg-4 col-sm-6">
          <a href="<?php echo $url; ?>" style="background:<?php echo !empty($value['Background_color']) ? $value['Background_color'] : $event_templates[0]['Background_color']; ?>" <?php echo $var_target; ?>> 
            <div style="background:url('<?php echo $img ?>') no-repeat center; background-size: 90%;" <?php if($img_view=='0'){ ?> class="event-img" <?php }else{ ?> class="event-round-img" <?php } ?>>
                 &nbsp;
              <div class="event-tile">
                <h3><?php echo $value['title']; ?></h3>
              </div>
            </div>
          </a>
          <div class="event-tile mobiletitle"> 
            <h3><?php echo $value['title']; ?></h3>
          </div>
        </div>  
      <?php } ?>
    </div>
  </div>  
</div>
<style type="text/css">
.event-list a{
  display: inline-block;     
  height: 100%;     
  width: 100%;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
  $('.homescreenmap').mapster({
    selected: true,
    fill: false,
    strokeColor: false,
    fillColor: 'FFFFFF',
    fillOpacity: 0.6,
  });
});
</script>