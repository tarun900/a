<html>
	<head>
		<title>Multi Forms</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/coustom.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-f.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web_responsive.css?<?php echo time(); ?>">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newattendeeregistration.css?'.time(); ?>">
		<style type="text/css">
			.smart-wizard input{
				margin-bottom: 15px;
			}
			.final-buuton button
			{
				margin-bottom: 15px;
			}
			#authorized_payment_box form{
			display:block;
			text-align:center;
			margin:0 auto;
			width:100%;
			}
			#authorized_payment_box .fa-usd{    
				display: inline-block;    
				font-size: 18px;    
				line-height: 23px;    
				padding-left: 10px;
			}
			#authorized_payment_box h2 {  
				display: inline-block;  
				font-size: 20px;  
				margin: 0 0 15px;  
				vertical-align: top;  
				width: auto;
			}
			#authorized_payment_box button.btn-green{    
				clear: both;    
				display: block;
				margin: 0 auto;
			}
			.main-container {
				background:transparent;
			}
			.main-container .col-sm-6.col-sm-offset-3{
				background: #FFF;
			    font-size: 17px;
			}
			#parent_formtitle label{
				color: #000;
			}
			.attend-register .app-logo
			{
				border: 0;
			    box-shadow: none;
			}
		</style>
	</head>
	<body>
		<div class="attend-register" <?php if(!empty($registration_screen[0]['background_image'])){ ?> style="background:transparent url('<?php echo base_url().'assets/user_files/'.$registration_screen[0]['background_image'] ?>')no-repeat scroll 0 0;" <?php } ?>>
			<div class="">
				<?php if(!empty($registration_screen[0]['banner_image'])){ ?>
					<div class="row banner-img">
			            <img width="100%" src="<?php echo base_url().'assets/user_files/'.$registration_screen[0]['banner_image']; ?>" id="preview_model_image_tag">
			        </div>
			        <div class="row">
			        	<h1 style="text-align: center;"><?php echo ucfirst($event_templates[0]['Event_name']); ?></h1>
			        </div>
		        <?php } if(!empty($registration_screen[0]['screen_content'])){ ?>
			        <div class="row">
			            <div id="preview_model_html_content">
			                <?php echo $registration_screen[0]['screen_content']; ?>
			            </div>
			        </div>
		        <?php } if($registration_screen[0]['show_app_link']=='1'){ ?>
		        <div class="app-logo">
		        	<?php if(!empty($registration_screen[0]['ios_link'])){ ?>
			        <div class="row ios-icn">
			            <a id="ios_link_in_models" target="_blank" href="<?php echo $registration_screen[0]['ios_link']; ?>">
			                <img src="<?php echo base_url().'assets/images/ios icon.png'; ?>">
			            </a>
			        </div>
			        <?php } if(!empty($registration_screen[0]['android_link'])){ ?>
			        <div class="row android-icn">
			            <a id="android_link_in_models" target="_blank" href="<?php echo $registration_screen[0]['android_link']; ?>">
			                <img src="<?php echo base_url().'assets/images/android icon.png'; ?>">
			            </a>
			        </div>
			        <?php } ?>
			        <div class="row web-icn">
			            <a target="_blank" href="<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain; ?>">
			                <img src="<?php echo base_url().'assets/images/web icon.png'; ?>">
			            </a>
			        </div>
		        </div>
		        <?php } ?>
			</div>
			<div class="main-wrapper">
				<div class="main-container inner">
					<div class="main-content">
						<div class="container text-center">
							<div class="col-sm-6 col-sm-offset-3 form-registration" style="float: none;display: inline-block;text-align: left;">
								<?php if($this->session->flashdata('registration_screen_error')){ ?>
								<div class="errorHandler alert alert-danger">
									<i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('registration_screen_error'); ?>
								</div>
								<?php } ?>
								<?php if($this->session->flashdata('registration_screen_success')){ ?>
								<div class="errorHandler alert alert-success">
									<i class="fa fa-remove-sign"></i> <h2 style="text-align: center;"><?php echo $this->session->flashdata('registration_screen_success'); ?></h2>
								</div>
								<?php } ?>
								<form role="form" method="post" class="smart-wizard" id="form" action="<?php echo base_url().'Attendee_login/multi_user_save_in_temp/'.$acc_name.'/'.$Subdomain ?>" enctype="multipart/form-data">
								<?php if(!empty($registration_screen[0]['title'])){ ?>
								<h2 style="text-align: center;"><?php echo $registration_screen[0]['title'];unset($registration_screen[0]['title']); ?></h2>
								<?php }if(!empty($registration_screen[0]['description'])){ ?>
								<h3><?php echo $registration_screen[0]['description'];unset($registration_screen[0]['description']); ?></h3>
								<?php } ?>
								<?php $numberofuser=$registration_screen[0]['number_forms']; ?>
								<input type="hidden" name="stripeToken" id="stripeToken" value="">
								<input type="hidden" name="numberuser" id="numberuser" value="1">
								<input type="hidden" value="<?php echo base_url().'Attendee_login/checkmultiuseremail' ?>" name="checkmailurl" id="checkmailurl">
								<input type="hidden" value="<?php echo $event_id; ?>" name="event_id" id="event_id">
									<div id="wizard" class="swMain">
										<ul id="step_anchor_ul" class="anchor11" style="margin-top:15px;">
											<?php for ($i=1; $i <=$numberofuser; $i++) { ?>
		                                    <li>
		                                        <a id="anchor_<?php echo $i; ?>" href="#step-<?php echo $i; ?>">
		                                            <div class="stepNumber">
		                                                <?php echo $i; ?>
		                                            </div>
		                                            <span class="stepDesc"> 
		                                            	<small id="step_user_<?php echo $i; ?>">Attendee <?php echo $i; ?></small>
		                                            </span>
		                                        </a>
		                                    </li>
		                                    <?php } ?>
		                                </ul>
		                                <div style="display: none;" class="progress progress-xs transparent-black no-radius active">
	                                    	<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar"></div>
	                                	</div> 
	                                	<?php if($registration_screen[0]['show_pay_by_invoice']=='1'){ ?>
	                                	<div class="form-group">
	                                		<div class="col-sm-12">
			                                	<div class="checkbox">
													<label>
														<input type="checkbox" value="1" id="pay_by_invoice" name="pay_by_invoice">				<strong>Pay By Invoice</strong>
													</label>
												</div>
											</div>
										</div>	
	                                	<?php } for ($j=1; $j<=$numberofuser; $j++) { ?>   
	                                	<div id="step-<?php echo $j; ?>">
	                                		<input type="hidden" name="temp_user_id_<?php echo $j; ?>" id="temp_user_id_<?php echo $j; ?>" value="">
	                                		<?php 
	                                		$customform=json_decode($registration_screen[0]['registration_forms'],true);
	                                		$json_data = false;
	                                		$textboxname=array('FirstName__'.$j,'LastName__'.$j,'Email__'.$j,'Company_Name__'.$j,'Title__'.$j,'attendee_type__'.$j,'choose_agenda__'.$j);
											$staticfields=5;
											if(count($category_list) <= 1)
											{
												$staticfields=6;
											} 
											foreach ($customform['fields'] as $key => $value) {
												if($key <= $staticfields)
												{
													if($value['type']=='element-single-line-text')
													{ ?>
													<div class="form-group">
														<label class="control-label col-sm-12" for="<?php echo $textboxname[$key]; ?>">
															<?php echo $value['title'] ?> <span class="symbol required"></span>
														</label>
														<div class="col-sm-12">
															<input type="text" name="<?php echo $textboxname[$key]; ?>" id="<?php echo $textboxname[$key]; ?>" class="form-control required">
														</div>	
													</div>
													<?php }else{ if($key=='5'){ if($registration_screen[0]['show_attendee_type']=='1'){ ?>
													<div class="form-group">
														<label class="control-label col-sm-12" for="<?php echo $textboxname[$key]; ?>">
															<?php echo $value['title'] ?> <span class="symbol required"></span>
														</label>
														<div class="col-sm-12">
															<div class="radio">
																<label>
																	<input type="radio" checked="checked" value="<?php echo $value['choices'][0]['title']; ?>" id="attendee_type_0" name="<?php echo $textboxname[$key]; ?>"><?php echo $value['choices'][0]['title']; ?>
																</label>
															</div>
															<div class="radio">
																<label>
																	<input type="radio" value="<?php echo $value['choices'][1]['title']; ?>" id="attendee_type_1" name="<?php echo $textboxname[$key]; ?>"><?php echo $value['choices'][1]['title']; ?>
																</label>
															</div>
														</div>	
													</div>
													<?php } }else{ $sessiontitle=$value['choices'][0]['title'];
													$choose_agenda=$value;
													} } 	
													unset($customform['fields'][$key]);
												}
											} 
											$json_data = json_encode($customform);
											$loader = new custom_formloader($json_data,'__'.$j);
											$loader->render_form();
											unset($loader);
											if($registration_screen[0]['show_coupon_code']=='1'){ ?>
											<div class="form-group">
												<label class="control-label col-sm-12" for="coupon_code__<?php echo $j; ?>">
													Coupon Code 
												</label>
												<div class="col-sm-12">
													<input type="text" name="coupon_code__<?php echo $j; ?>" id="coupon_code__<?php echo $j; ?>" class="form-control">
												</div>	
											</div>
											<?php }
											if(count($choose_agenda) > 0)
											{ if($event_id=='413' || $event_id=='618'){ ?>
											<input type="hidden" name="saveagenda__<?php echo $j; ?>" id="saveagenda__<?php echo $j; ?>" value="1">
											<div class="form-group">
												<label class="control-label col-sm-12" for="<?php echo $textboxname[6]; ?>">
													<?php echo $choose_agenda['title'] ?> 
												</label>
												<div class="col-sm-12">
												<?php foreach ($choose_agenda['choices'] as $vkey => $vvalue) { ?>
												<div class="checkbox">
													<label>
														<input type="checkbox" <?php if($vvalue['checked']=='1'){ $sessiontitle=$vvalue['title']; ?> checked="checked" <?php } ?> value="<?php echo $vvalue['value']; ?>" name="<?php echo $textboxname[6]; ?>[]"><?php echo $vvalue['title']; ?>
													</label>
												</div>
												<?php } ?>
												</div>
											</div>
											<?php }else{ ?>
											<div class="form-group">
												<label class="control-label col-sm-12" for="<?php echo $textboxname[6]; ?>">
													<?php echo $choose_agenda['title'] ?> <span class="symbol required"></span>
												</label>
												<div class="col-sm-12">
												<?php foreach ($choose_agenda['choices'] as $vkey => $vvalue) { ?>
												<div class="radio">
													<label>
														<input type="radio" <?php if($vvalue['checked']=='1'){ $sessiontitle=$vvalue['title']; ?> checked="checked" <?php } ?> value="<?php echo $vvalue['value']; ?>" onchange='get_sessionlist("<?php echo ucfirst($vvalue['title']); ?>","<?php echo $j; ?>");' name="<?php echo $textboxname[6]; ?>"><?php echo $vvalue['title']; ?>
													</label>
												</div>
												<?php } ?>
												</div>
											</div>
											<div class="row">
												<input type="hidden" name="saveagenda__<?php echo $j; ?>" id="saveagenda__<?php echo $j; ?>" value="0">
												<h3 style="text-align: center;" id="main_agenda_category_heading_<?php echo $j; ?>"><?php echo ucfirst($sessiontitle); ?></h3>
												<div id="ticket_agenda_list__<?php echo $j; ?>">
												</div>
											</div>
											<?php } } ?>
	                                		<div class="form-group final-buuton">
	                                			<?php if($j!=1){ ?>
	                                			<div class="col-sm-4 pull-left" style="padding-left:0px;">
	                                                <button data-redirectid="<?php echo $j-1; ?>" class="btn btn-green back-step btn-block">
	                                                    <<< Back
	                                                </button>
	                                            </div>
	                                			<?php } ?>
	                                            <div class="col-sm-4 pull-center">
	                                                <button data-redirectid="<?php echo $j; ?>" type="button" id="payment_btn_<?php echo $j ?>" onclick="getpaymentbox(<?php echo $j; ?>);" class="btn btn-green btn-block" disabled="disabled">
	                                                    Submit
	                                                </button>
	                                            </div>
	                                            <?php if($j!=$numberofuser){ ?>
	                                            <div class="col-sm-4 pull-right">
	                                                <button data-redirectid="<?php echo $j+1; ?>" id="next_btn_<?php echo $j ?>" class="btn btn-green next-step btn-block" disabled="disabled">
	                                                    Add a new Attendee
	                                                </button>
	                                            </div>
	                                            <?php } ?>
	                                        </div>
	                                	</div>
	                                	<?php } ?>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="attendee-footer">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<p>This App was made on</p>
								<p><img src="<?php echo base_url(); ?>assets/images/aitl-logo1.png" alt="aitl-logo"/></p>
								<a href="https://www.allintheloop.com" target="_blank"><p>www.allintheloop.com</p></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<?php if($registration_screen[0]['payment_type']=='1'){ ?>
		<div class="modal fade" id="authorized_payment_box" data-backdrop="static" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header" style="display:none;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4> 
		        </div>
		        <div class="modal-body">
		        	<div class="row">
		        		<!--<form method="post" action="https://test.authorize.net/payment/payment">-->
		        		<form method="post" action="https://accept.authorize.net/payment/payment">
		        			<h2>Amount:<i class="fa fa-usd"></i><?php echo $Price; ?></h2>
				        	<input type="hidden" name="token" value="<?php echo $Paymenttoken; ?>">
				        	<button type="submit" name="paymentbtn" class="btn btn-green">Continue To Pay</button>
				        </form>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<?php } ?>	
		<div class="modal fade" id="error_msg_model" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header" style="display:none;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4> 
		        </div>
		        <div class="modal-body" id="error_msg_body">

		        </div>
		        <div class="modal-footer">
		          <button type="button" style="border:none;color:blue;" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php  echo base_url();?>assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard1.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/form-wizard_multi.js?<?php echo time(); ?>"></script>
		<?php if($registration_screen[0]['payment_type']=='2'){ ?>
		<script src="https://checkout.stripe.com/checkout.js"></script>
		<?php $key=$registration_screen[0]['stripe_public_key']; } ?>
		<script type="text/javascript">
			<?php if($registration_screen[0]['payment_type']=='2'){ ?>
			var handler = StripeCheckout.configure({
			  key: '<?php echo $key; ?>',
			  image: '<?php echo base_url(); ?>assets/images/favicon.png',
			  locale: 'auto',
			  token: function(token) {
			    jQuery('#stripeToken').val(token.id);
			    jQuery('#form').submit();
			  }
			});
			function getstripepopup(onuser)
			{
				var price="<?php echo $registration_screen[0]['stripe_payment_price']; ?>";
				handler.open({
				    name: 'Allintheloop.com',
				    description: 'All In The Loop Software',
				    amount: parseInt(price*onuser)*100,
				    currency:'<?php echo $registration_screen[0]['stripe_payment_currency']; ?>',
				    billingAddress:"true"
				});
				e.preventDefault();
			}
			<?php } ?>
			$(document).ready(function(){
				$('body').addClass('sidebar-close');
				<?php if(!empty($Paymenttoken)){ ?>
				    $('#authorized_payment_box').modal('show');
				<?php } ?>
		        FormWizard.init();
		        $('#form').trigger("reset");
		        <?php for ($k=1; $k<=$numberofuser; $k++) { ?> 
		        get_sessionlist("<?php echo ucfirst($sessiontitle); ?>",<?php echo $k; ?>);
		        <?php } ?>
			});
			function getpaymentbox(onuser)
			{
				var paymenttype="<?php echo $registration_screen[0]['payment_type']; ?>";
				if($('#form').valid())
	        	{
	        		if($.trim($('#event_id').val())=="662")
	        		{
	        			if($.trim($('input[name="choose_agenda__'+onuser+'"]:checked').val())=="Tech on Tap Party Pass - $49")
	            		{
			        		if($('#saveagenda__'+onuser).val()>=1)
			        		{
				        		$('#numberuser').val(onuser);
				        		if(paymenttype=='2' && $('#pay_by_invoice').prop("checked") == false)
			        			{
					        		getstripepopup(onuser);
					        	}
					        	else
					        	{
					        		$('#form').submit();
					        	}
				        	}
				        	else
				        	{
				        		$('#error_msg_body').html('<b>Please Save At Least One Session.</b>');
				    			$('#error_msg_model').modal('show');
				        	}
				        }
				        else
				        {
				        	if(($.trim($('input[name="choose_agenda__'+onuser+'"]:checked').val())!="Foresight Pass (Includes Tech on Tap Party Pass) - $249" && $('#saveagenda__'+onuser).val()>=3) || ($.trim($('input[name="choose_agenda__'+onuser+'"]:checked').val())=="Foresight Pass (Includes Tech on Tap Party Pass) - $249" && $('#saveagenda__'+onuser).val()>=6))
			        		{
				        		$('#numberuser').val(onuser);
				        		if(paymenttype=='2' && $('#pay_by_invoice').prop("checked") == false)
			        			{
					        		getstripepopup(onuser);
					        	}
					        	else
					        	{
					        		$('#form').submit();
					        	}
				        	}
				        	else
				        	{
				        		if($.trim($('input[name="choose_agenda__'+onuser+'"]:checked').val())=="Foresight Pass (Includes Tech on Tap Party Pass) - $249")
				        		{
				        			$('#error_msg_body').html('<b>Please Save At Least 6 Session.</b>');
				        		}
				        		else
				        		{
				        			$('#error_msg_body').html('<b>Please Save At Least 3 Session.</b>');
				        		}
				    			$('#error_msg_model').modal('show');
				        	}
				        }
	        		}
	        		else
	        		{
		        		if($('#saveagenda__'+onuser).val()>=1)
		        		{
			        		$('#numberuser').val(onuser);
			        		if(paymenttype=='2' && $('#pay_by_invoice').prop("checked") == false)
		        			{
				        		getstripepopup(onuser);
				        	}
				        	else
				        	{
				        		$('#form').submit();
				        	}
			        	}
			        	else
			        	{
			        		$('#error_msg_body').html('<b>Please Save At Least One Session.</b>');
			    			$('#error_msg_model').modal('show');
			        	}
		        	}
				}
			}
			function get_sessionlist(category_title,j)
		    {
		    	$.ajax({
		    		url:"<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain.'/get_session_list' ?>",
		    		data:'ticket_type='+category_title+'&numnerforms='+j+'&useremail='+$('#Email__'+j).val(),
		    		type:'post',
		    		success:function(result)
		    		{	
		    			$('#main_agenda_category_heading_'+j).html(category_title);
		    			$('#ticket_agenda_list__'+j).html(result);
		    			$('#saveagenda__'+j).val('0');
		    		}
		    	});
		    }
		    function savesession(elem,aid,Placesleft,formsnumber)
		    {
		    	$('#next_btn_'+formsnumber).attr('disabled','disabled');
		    	$('#payment_btn_'+formsnumber).attr('disabled','disabled');
		    	if(Placesleft > 0)
		    	{
		    		if($('#form').valid())
		    		{
			    		$.ajax({
			    			url:"<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain.'/save_tempusersession/' ?>"+aid,
			    			data:'useremail='+$('#Email__'+formsnumber).val()+"&temp_uid="+$('#temp_user_id_'+formsnumber).val(),
			    			type:'post',
			    			success:function(result)
			    			{
			    				var data=result.split('###');
			    				if($.trim(data[0])=="error")
			    				{
			    					$('#error_msg_body').html('<b>'+data[1]+'</b>');
		    						$('#error_msg_model').modal('show');
			    				}
			    				else
			    				{
			    					$(elem).attr('class',data[1]);
			    					if($.trim(data[1])=="btn btn-success btn-block")
			    					{
			    						var totalsavesession=parseInt($('#saveagenda__'+formsnumber).val())+1;
			    					}
			    					else
			    					{
			    						var totalsavesession=parseInt($('#saveagenda__'+formsnumber).val())-1;
			    					}
			    					$('#saveagenda__'+formsnumber).val(totalsavesession);
			    					$('#temp_user_id_'+formsnumber).val(data[2]);
			    				}
		    					if($.trim($('#event_id').val())=="662")
				        		{
				        			if($.trim($('input[name="choose_agenda__'+formsnumber+'"]:checked').val())=="Tech on Tap Party Pass - $49")
				            		{
						        		if($('#saveagenda__'+formsnumber).val()>=1)
						        		{
						        			$('#next_btn_'+formsnumber).removeAttr('disabled');
		    								$('#payment_btn_'+formsnumber).removeAttr('disabled');
							        	}
							        	else
							        	{
							        		$('#next_btn_'+formsnumber).attr('disabled','disabled');
		    								$('#payment_btn_'+formsnumber).attr('disabled','disabled');
							        	}
							        }
							        else
							        {
							        	if(($.trim($('input[name="choose_agenda__'+formsnumber+'"]:checked').val())!="Foresight Pass (Includes Tech on Tap Party Pass) - $249" && $('#saveagenda__'+formsnumber).val()>=3) || ($.trim($('input[name="choose_agenda__'+formsnumber+'"]:checked').val())=="Foresight Pass (Includes Tech on Tap Party Pass) - $249" && $('#saveagenda__'+formsnumber).val()>=6))
						        		{
						        			$('#next_btn_'+formsnumber).removeAttr('disabled');
		    								$('#payment_btn_'+formsnumber).removeAttr('disabled');
							        	}
							        	else
							        	{
							        		$('#next_btn_'+formsnumber).attr('disabled','disabled');
		    								$('#payment_btn_'+formsnumber).attr('disabled','disabled');
							        	}
							        }
				        		}
				        		else
				        		{
					        		if($('#saveagenda__'+formsnumber).val()>=1)
					        		{
						        		$('#next_btn_'+formsnumber).removeAttr('disabled');
		    							$('#payment_btn_'+formsnumber).removeAttr('disabled');
						        	}
						        	else
						        	{
						        		$('#next_btn_'+formsnumber).attr('disabled','disabled');
		    							$('#payment_btn_'+formsnumber).attr('disabled','disabled');
						        	}
					        	}
			    			},
			    		});
			    	}
			    	else
			    	{
			    		$('#error_msg_body').html('<b>Please Fill Up The Form.</b>');
		    			$('#error_msg_model').modal('show');
			    	}
		    	}
		    	else
		    	{
		    		$('#error_msg_body').html('<b>This session is fully booked – Unfortunately you have not been booked onto this session</b>');
		    		$('#error_msg_model').modal('show');
		    		$('#next_btn_'+formsnumber).removeAttr('disabled');
		    		$('#payment_btn_'+formsnumber).removeAttr('disabled');
		    	}
		    }
		</script>					
	</body>
</html>