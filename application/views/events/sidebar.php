<div class="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
        <a class="navbar-brand" href="#">
            <?php for($i=0;$i<count($event_templates);$i++) { ?>
                    <?php $image_array = json_decode($event_templates[$i]['Images']); ?>
                    <?php if(!empty($image_array))   { ?>
                    <img alt="Logo" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
                    <?php } else{  ?>
                    <img alt="Logo" src="https://www.aal-europe.eu/wp-content/uploads/2013/04/events_medium.jpg">
                    <?php } ?>
            <?php } ?>
        </a>
        <div class="logo-tagline">
            <?php for($i=0;$i<count($event_templates);$i++) 
                { 
                    echo $event_templates[$i]['Event_name'];
                } 
            ?>
        </div>
        <ul class="main-navigation-menu">
            <li class="active open">
                 <?php for($i=0;$i<count($event_templates);$i++) { ?>
                    <a href="<?php echo base_url(); ?>App/<?php echo $event_templates[$i]['Subdomain']; ?>"> <span class="title">Home</span></a>
                <?php } ?>
            </li>
            <?php
                foreach ($menu as $key => $value) 
                {
                    for($i=0;$i<count($event_templates);$i++) 
                    { 
                        $url = base_url().$value['pagetitle'].'/'.''.$event_templates[$i]['Subdomain'];
                        echo '<li><a id="'.$value['pagetitle'].'" href="'.$url.'"><span class="title">'.$value['menuname'].'</span></a></li>';
                    }
               }
            ?>
        </ul>
    </div>
</div>