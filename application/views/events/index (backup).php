<div class="slider-banner">
    <div class="row">
        <?php for($i=0;$i<count($event_templates);$i++) { ?>
        <?php $image_array = json_decode($event_templates[$i]['Images']); ?>
        <?php if(!empty($image_array))   { ?>
        <?php if($event_templates[$i]['Img_view'] =='0') { ?>
          <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
        <?php } else { ?>
        <img title="" style="border-radius:50px;" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
        <?php } } else{ ?>
        <img "title="" alt="" src="https://www.aal-europe.eu/wp-content/uploads/2013/04/events_medium.jpg">
        <?php } } ?>
    </div>
</div>
<div class="event-content">
    <div class="row" style="padding: 10px;font-size: 12px;color: #545454;margin-left:0px;margin-right:0px;">
        <?php for($i=0;$i<count($event_templates);$i++) { ?>
        <p><?php
        $string  = $event_templates[$i]['Description'];
        echo $string;
        //$desc =  substr($string, 0, 2000);  echo $desc; ?></p>
        <?php } ?>
    </div>
</div>
<div class="event-list" style="background:<?php for($i=0;$i<count($event_templates);$i++) { echo $event_templates[$i]['Background_color'];}?>">
    <div class="row">
        <?php foreach ($menu as $key => $value) 
        {
           $img_view = $value['img_view'];
           $img = $value['img'];
           $url = base_url();
           $bimg = $url."assets/user_files/".$img; 
           $dimg = $url."assets/images/defult-menuimg.png";
                for($i=0;$i<count($event_templates);$i++) 
                {
                    $url = base_url().$value['pagetitle'].'/'.''.$event_templates[$i]['Subdomain'];
           ?>
            <?php if($img_view =='0') { ?>
            <div class="col-md-6 col-lg-4 col-sm-6">
                <a href="<?php echo $url; ?>"> 
                    <?php if($img !='') { ?>
                        <div style="background:url(<?php echo $bimg ?>) no-repeat center top; background-size: cover;" class="event-img">
                            <div class="event-tile">
                                <h3><?php echo $value['menuname']; ?></h3>
                            </div>
                        </div>
                    <?php } else { ?>
                    <div style="background:url(<?php echo $dimg ?>) no-repeat center top; background-size: cover;" class="event-img">
                        <div class="event-tile">
                            <h3><?php echo $value['menuname']; ?></h3>
                        </div>
                    </div>
                    <?php } ?>
                </a>
            </div>
            <?php } else { ?>
            <div class="col-md-6 col-lg-4 col-sm-6" style="">
                <a href="<?php echo $url; ?>"> 
                    <?php if($img !='') { ?>
                        <div style="background:url(<?php echo $bimg ?>) no-repeat center top; background-size: cover;" class="event-round-img">
                            &nbsp;
                            <div class="event-tile">
                                <h3><?php echo $value['menuname']; ?></h3>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div style="background:url(<?php echo $dimg ?>) no-repeat center top; background-size: cover;" class="event-round-img">
                            &nbsp;
                            <div class="event-tile">
                                <h3><?php echo $value['menuname']; ?></h3>
                            </div>
                        </div>
                    <?php } ?>
                </a>
            </div>
            <?php } } } ?>
    </div>
</div>
<?php if($event_templates[0][Event_time] =='1') {   ?>
<div class="row" style="margin-left: 0px; padding-left: 0;">
    <div class="event-timer">
        <div class="row">
            <?php for($i=0;$i<count($event_templates);$i++) 
                { 
                    $event = $event_templates[$i]['Start_date'];
                    $end_event = $event_templates[$i]['End_date'];
                    $today = date("Y-m-d");
                    if($event >= $today)
                    {
                       echo '<h3>Event starts in</h3>';
                       echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event >= $today || $end_event > $today)
                    {
                        echo '<h3>Event end in</h3>';
                        echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event < $today)
                    {
                        echo '<h3>Event have end</h3>';
                    }
                    else
                    {

                    }
                }
                ?>
        </div>
    </div>
</div>
<?php } else { } ?>
<!-- end: PAGE --> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/jquery-2.0.3.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility-coming-soon.js"></script>
<script type="text/javascript">
        jQuery("#countdown").countdown({
            <?php for($i=0;$i<count($event_templates);$i++) { 
            $event = $event_templates[$i]['Start_date'];
            $end_event = $event_templates[$i]['End_date'];
            $end_event_date = date('Y/m/d H:i:s', strtotime($end_event));
            $edate = date('Y/m/d H:i:s', strtotime($event));
            $today = date('Y/m/d H:i:s');
            if($edate >= $today)
            {
    ?>
            date : '<?php echo $edate; ?>',
            format: "on"
    <?php }  else
    {?>
            date : '<?php echo $end_event_date; ?>',
            format: "on"

    <?php } }?>
        });
</script>
<script>
   jQuery(document).ready(function() 
   {
        //Main.init();
        ComingSoon.init();
    });
</script>