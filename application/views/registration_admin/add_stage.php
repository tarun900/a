<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: TEXT FIELDS PANEL -->
      <div class="panel panel-white">
         <div class="tabbable">
            <ul id="myTab2" class="nav nav-tabs">
               <li class="active">
                  <a href="#surveylist" data-toggle="tab">
                  Update Stage
                  </a>
               </li>
            </ul>
         </div>
         <div class="panel-body" style="padding: 0px;">
            <div class="tab-content">
               <div class="tab-pane fade active in" id="surveylist">
                  <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                     <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Stage <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-6">
                           <input type="text" placeholder="Stage name" name="stage_name" class="form-control name_group required" value="<?=$stage['stage_name']?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12">
                           <h4>Drop available questions in this Stage to create the Layout and question order. Click the + Sign below to begin.</h4>
                           <h4>You can also add in images and extra text using an HTML editor.</h4>
                        </div>

                        <?php $skiparr=array_filter(array_column_1($stage['stage_que'],'skip_logic'));
                            foreach($stage['stage_que'] as $key => $value) : 
                              $label = ($value['skip_logic'] == '1') ? 'Update Logic' : 'Add Logic'; ?>
                          <div class="col-sm-12 stage_que">
                            <p><span class="stage_question">Question : <?=$value['question']?></span> <?=($value['type'] == '1' && (empty($skiparr) || $value['skip_logic'] == '1')) ? '
                            <a class="btn btn-green showmodel" data-toggle="modal" data-question="'.$value['question'].'" data-id="'.$value['id'].'">'.$label.'</a>' : ''?></p>
                          </div>
                        <?php endforeach;?>
                     </div>
                     <div class="form-group" <?=empty(strip_tags($stage['stage_html'])) ? 'style="display:none"' : ''?>>
                        <div class="col-sm-12 add_stage">
                           <button class="btn btn-block" data-toggle="modal" data-target="#add_html"><?=$stage['stage_html']?></button>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12 add_stage">
                           <button class="btn btn-block" data-toggle="modal" data-target="#add_stage">+</button>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-12 stagesubmitbtn">
                          <button class="btn btn-yellow btn-block" type="submit">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                          </button>
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</form>
<div class="modal fade" id="add_stage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bluebackgroupstyle">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Select Content</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <button class="btn btn-primary form-control" id="" data-toggle="modal" data-target="#add_question" data-dismiss="modal">Insert a Question</button>                    
                </div>
                <div class="form-group">
                    <button class="btn btn-primary form-control" id="insert_html" data-toggle="modal" data-target="#add_html" data-dismiss="modal">Insert HTML text</button>               
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_html" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header bluebackgroupstyle">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="exampleModalLabel">Add HTML</h4>
         </div>
         <div class="modal-body">
            <form role="form" method="post" class="form-horizontal" id="form" action="<?=base_url().'Registration_admin/add_html_to_stage/'.$this->uri->segment(3).'/'.$this->uri->segment(4)?>" enctype="multipart/form-data">
              <div class="form-group">
                  <div class="row" style="margin-right: 0px;margin-left: 0px;">
                  <textarea id="screen_content" name="stage_html"><?=$stage['stage_html']?></textarea>
                  </div>
              </div>
              <input type="submit" class="btn btn-green" value="Submit">
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="add_question" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header bluebackgroupstyle">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Add Question</h4>
         </div>
         <div class="modal-body">
            <form role="form" method="post" class="form-horizontal" id="form" action="<?=base_url().'Registration_admin/add_question_to_stage/'.$this->uri->segment(3).'/'.$this->uri->segment(4)?>" enctype="multipart/form-data">
            <div class="">
               <select class="select2-container select2-container-multi form-control search-select menu-section-select"  multiple="multiple" name="q_id[]">
                  <?php foreach ($questions as $key => $value) : ?>
                        <option value="<?=$value['id']?>" <?=in_array($value['id'],$stage['stage_que_id']) ? 'selected=selected' : ''?> ><?=$value['question']?></option>
                  <?php endforeach; ?>
               </select>
            </div>
            <input type="submit" class="btn btn-green" value="Submit">
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="add_logic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header bluebackgroupstyle">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="Question"></h4>
         </div>
         <div class="modal-body">
            <form role="form" method="post" class="form-horizontal" id="form" action="<?=base_url().'Registration_admin/add_skip_logic/'.$this->uri->segment(3).'/'.$this->uri->segment(4)?>" enctype="multipart/form-data">
            <div id="logic-html" class="col-sm-12">
              
            </div>
            <input type="submit" class="btn btn-green" value="Save Logic">
            </form>
         </div>
      </div>
   </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $('#screen_content').summernote({
       height:300,
          });
   });
  $(document).ready(function(){
       $(".showmodel").click(function()
       {  
             $("#Question").html($(this).data('question'));
             
             var id = $(this).data('id');
             $.ajax({
              method: "POST",
              url: "<?=base_url().'Registration_admin/get_logic_html/'.$this->uri->segment(3).'/'.$this->uri->segment(4)?>",
              data: { id:id },
               success: function (response) {
                $("#logic-html").html(response);
                $('#add_logic').modal('show');
              }
            })
       });
  });
</script>