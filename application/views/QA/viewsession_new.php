<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
$timeformat="H:i"; 
if($time_format[0]['format_time']=='0')
{
  $timeformat="h:i A";
}
$date_format="d/m/Y";
if($event_templates[0]['date_format']=='1')
{
  $date_format="m/d/Y";
} ?>
<style type="text/css">
.message_img { float:none !important; }
.msg_main_body {margin: 0 auto;width: 100%;text-align: center;}
.message_container{text-align:center !important;padding-bottom: 1000px;}
.msg_message {text-align:center !important;}
./*message_container .message_img img { width:120px !important; height:120px !important;margin: 15px !important;}*/
.message_container .message_img img { width:120px !important; height:120px !important;margin-bottom: 40px !important;}
.qa_session_desc,.qa_session_datetime,.msg_fromname,.msg_message {color: <?=$event_templates[0]['Top_text_color']?> !important}
.message_container { border: none !important;border-radius: 0px !important;box-shadow: 0 0 0;}
body{overflow-y: hidden;}


/*******************/
header.topbar.navbar.navbar-inverse.inner {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 1;
}
.message_container {
    border: none !important;
    border-radius: 0px !important;
    box-shadow: 0 0 0;
    height: 100vh;
    position: relative;
    padding: 0;
}
.msg_main_body_wrap {
    position: absolute;
    left: 0;
    right: 0;
    top: 46%; /*50%*/
    transform: translateY(-50%);
    -ms-transform:translateY(-50%);
    -webkit-transform:translateY(-50%);
}

.msg_fromname h1{font-size:70px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 600}
.msg_fromname h2{font-size:50px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 400}
.qa_session_name{font-size:70px;}


@media screen and (max-width: 1400px){
.msg_fromname h1{font-size:55px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 600}
.msg_fromname h2{font-size:40px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 400}
.qa_session_name{font-size:55px;}	
}

@media screen and (max-width: 1200px){
.msg_fromname h1{font-size:45px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 600}
.msg_fromname h2{font-size:35px; color: <?=$event_templates[0]['Top_text_color']?> !important; font-weight: 400}
.qa_session_name{font-size:45px;}	
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<div class="panel panel-white" id="qa_section_msg">
	<center>
		<div class="loader-j">
	    	<img src="https://www.allintheloop.net/demo/assets/user_files/ajax-loader-j.gif">
		</div>
	</center>
	<div class="question_main_container" id="qa_session_question_main_container">
		<?php foreach ($qa_session_question as $key => $value) { ?>
		<div class="message_container col-xs-12 col-sm-12 col-md-12 col-lg-12" id="message_container__<?php echo $value['message_id']; ?>" data-sort="<?php echo $value['votes']?: '0'; ?>">
			<div class="msg_main_body_wrap">
				<div class="msg_main_body">
					<div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
						<a class="sp-full-screen-button1 sp-fade-full-screen1" style="cursor: pointer;margin-right: 10px;" onclick="test();"><i class="fa fa-expand fa-2x" style="margin-top: 10px;"></i></a>
					</div>
					<div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
						<h3 class="qa_session_name" style="color:<?=$event_templates[0]['Top_text_color']?>;font-size: 60px;"><?php echo ucfirst($qa_session[0]["Session_name"]); ?></h3>
					</div>
					<div class="message_img">
						<?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?> 
						<img src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
						<?php }else{ ?>
						<img src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
						<?php } ?>
					</div>
					<div class="msg_fromname">
						<h1><?=ucfirst($value['user_name']); ?></h1>
						<h2><?=ucfirst($value['Title'])?></h2>
		              	<h2><?=ucfirst($value['Company_name'])?></h2>
					</div>
				</div>
				<div class="msg_message" style="font-size: 60px;">
					<?php echo $value['Message']; ?>
				</div>
			</div>
		</div>
		<?php }?>
	</div>
</div>
<div id="sendquestionpopup" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="border: none;">
				<button type="button" class="close" data-dismiss="modal">
					<img width="20" height="20" src="<?php echo base_url().'assets/images/close_red.png'; ?>">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="imageform" method="post" enctype="multipart/form-data" action='' style="">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<textarea style="width: 100%;height: 150px;" id="Message" placeholder="<?php echo $event_templates[0]['default_lang']['50__ask_your_own_question_or_vote_other_to_the_top']; ?>..." name="Message"></textarea>
								<span id="question_error_msg_span" class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<label class="checkbox-inline">
                            		<input type="checkbox" value="1" class="grey" name="anonymous_user" id="anonymous_user">&nbsp;
                            		<span><?php echo $event_templates[0]['default_lang']['50__anonymous']; ?></span>
                        		</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0 auto;display: inline-block;float: none;">
								<a href="javascript:void(0)" onclick="sendmessage()" id="sendbtn" class="btn btn-green btn-block">
									Send <i class="fa fa-arrow-circle-right"></i>
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript">
$(window).load(function(){
    $('body').attr('class',"sidebar-close");
	$('.loader-j').hide();
    $('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
      return $(b).data('sort') - $(a).data('sort');
        }).each(function (_, message_container_question) {
      $(message_container_question).parent().append(message_container_question);
    });  
    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:<?=$event_templates[0]['Top_background_color']?>;");
    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    {
    	$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");
    }*/
});
function opensendmshpopup()
{
	$("#Message").val('');
	$('#anonymous_user').iCheck('uncheck');
	$('#question_error_msg_span').parent().parent().removeClass('has-error').removeClass('has-success');
	$('#question_error_msg_span').hide();
	$('#sendquestionpopup').modal('show');
}
function sendmessage()
{
	$("#sendbtn").html('Sending <i class="fa fa-refresh fa-spin"></i>');
	if($.trim($("#Message").val())=="")
	{
		$('#question_error_msg_span').html("Please write Message...");
		$('#question_error_msg_span').parent().parent().removeClass('has-success').addClass('has-error');
		$('#question_error_msg_span').show();
    	$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
	}
	else
	{
		$('#question_error_msg_span').parent().parent().removeClass('has-error').addClass('has-success');
		$('#question_error_msg_span').hide();
		var str = jQuery("#imageform").serialize();
		$.ajax({
			url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/send_message/'.$sid; ?>",
        	data: str,
        	type: "POST",
        	async: true,
        	success: function(result)
        	{
        		$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
        		$('#Message').val('');
        		$('#anonymous_user').iCheck('uncheck');
        		window.location.reload();
        	}
		});
	}
}
function vote(mid)
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/change_question_vote/'; ?>"+mid,
    	type: "POST",
    	async: true,
    	success: function(result)
    	{
    		$('#vote_img__'+mid).show();
			$('#unvote_img__'+mid).hide();
			var total_vote=parseInt($('#total_vote__'+mid).html());
			$('#total_vote__'+mid).html(total_vote+1);
			$('#message_container__'+mid).data('sort',total_vote+1);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:<?=$event_templates[0]['Top_background_color']?>;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");
    		}*/
    	}
	});
}
function unvote(mid)
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/change_question_vote/'; ?>"+mid,
    	type: "POST",
    	async: true,
    	success: function(result)
    	{
			$('#unvote_img__'+mid).show();
			$('#vote_img__'+mid).hide();
			var total_vote=parseInt($('#total_vote__'+mid).html());
			$('#total_vote__'+mid).html(total_vote-1);
			$('#message_container__'+mid).data('sort',total_vote-1);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:<?=$event_templates[0]['Top_background_color']?>;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");  
    		}*/  
		}
	});     
}
$(document).ready(function() {
    load_qa();
  setInterval(function() {
    load_qa();
  }, 2000);
});

function load_qa()
{
	$.ajax({
		url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/view/'.$this->uri->segment(5); ?>",
    	type: "POST",
    	async: true,
    	beforeSend: function() {
		   //$('.loader-j').show();
		},
    	success: function(result)
    	{	
    		$('.loader-j').hide();
		   	$('#qa_session_question_main_container').empty();
			$('#qa_session_question_main_container').append(result);
			$('#qa_session_question_main_container').find('.message_container').sort(function (a, b) {
		      return $(b).data('sort') - $(a).data('sort');
		        }).each(function (_, message_container_question) {
		      $(message_container_question).parent().append(message_container_question);
		    });
		    $('#qa_session_question_main_container').find('.message_container').attr('style',"background-color:<?=$event_templates[0]['Top_background_color']?>;");
		    /*if($('#qa_session_question_main_container').find('.message_container:first').data('sort') > 0)
    		{
    			$('#qa_session_question_main_container').find('.message_container:first').attr('style',"background-color:<?php echo $event_templates[0]['Top_background_color']; ?>");  
    		}*/
		}
	});     
}
$('.sp-full-screen-button').click(function(){
		if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
	   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
	    if (document.documentElement.requestFullScreen) {  
	      document.documentElement.requestFullScreen();  
	    } else if (document.documentElement.mozRequestFullScreen) {  
	      document.documentElement.mozRequestFullScreen();  
	    } else if (document.documentElement.webkitRequestFullScreen) {  
	      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
	    }
	    $('header').hide();  
	  } else {  
	    if (document.cancelFullScreen) {  
	      document.cancelFullScreen();  
	    } else if (document.mozCancelFullScreen) {  
	      document.mozCancelFullScreen();  
	    } else if (document.webkitCancelFullScreen) {  
	      document.webkitCancelFullScreen();  
	    }
	    $('header').show();  
	  }
	});
function test()
{
	if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
	   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
	    if (document.documentElement.requestFullScreen) {  
	      document.documentElement.requestFullScreen();  
	    } else if (document.documentElement.mozRequestFullScreen) {  
	      document.documentElement.mozRequestFullScreen();  
	    } else if (document.documentElement.webkitRequestFullScreen) {  
	      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
	    }
	    $('header').hide();  
	  } else {  
	    if (document.cancelFullScreen) {  
	      document.cancelFullScreen();  
	    } else if (document.mozCancelFullScreen) {  
	      document.mozCancelFullScreen();  
	    } else if (document.webkitCancelFullScreen) {  
	      document.webkitCancelFullScreen();  
	    }
	    $('header').show();  
	  }
}
</script> 
