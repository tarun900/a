<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
?>
<div class="row margin-left-10">
    <?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
         
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data,base_url().'twitter_feed/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('44', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if($image_array!='') { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id'] ?>")'> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } }  } ?>
 </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
    <div class="event_maintitle" <?php if($this->uri->segment(3) =='thegrayreveal') { ?> style="background:#A29061" <?php } ?>><?php echo "#".$hashtags[0]['hashtags']; ?></div>
    <div class="panel panel-white" id="twitter_feed_design">
    <div id="event_post" style="text-align:center;margin-top:0px;">
<?php
if(count($tweet->statuses)>0){
	foreach ($tweet->statuses as $key => $value) { ?>	
			<div class="event_list" style="padding:0px;">
		    	<ul class="eventbox">
		        	<li>
		            	<div class="post">
		                	<div class="post-left">
		                    	<img src="<?php echo $value->user->profile_image_url; ?>" alt="" title=""/>
		                    </div>
		                    <div class="post-right">
		                    	<div class="post_main">
			                        <span class="post_title">
			                        <?php if(!empty($value->entities->user_mentions[0]->screen_name)){
									$snm=$value->entities->user_mentions[0]->screen_name;
									}else
									{
										$snm=$value->user->screen_name;
									}
									?>
									<a href="<?php echo 'https://www.twitter.com/'.$snm; ?>">
										<?php echo $value->user->name; ?>	
									</a>
								    </span>
		                            <span class="post_date"><?php echo date("jS F, Y h:i:s",strtotime($value->created_at)); ?></span>
		                        </div>
		                        <div class="post_detail">
		                        	<div class="post_desc"><?php echo $value->text; ?></div>
									<div class="post-image">
		                            	<?php if($value->entities->media[0]->media_url){ ?>
										<img src="<?php echo $value->entities->media[0]->media_url; ?>"/>
										<?php } ?>
		                            </div>
		                        </div>
		                        <div class="clear"></div>
		                    </div>
		                      <div class="clear"></div>
		                </div>
		            </li>
		        </ul>
		    </div>
		
		<?php 
	}
}
else
{
	echo "NO Feed For this hashtags.";
}
?></div></div></div>
</div>
<style type="text/css">
#twitter_feed_design { 
  border: 1px solid #e1e8ed;
  border-top-left-radius: 6px !important;
  border-top-right-radius: 6px !important;
  margin: 15px auto;
  padding: 0;
  width: 588px;}
#event_post ul.eventbox {
  padding-left: 0;
}
#event_post .post-right {
  text-align: left;
}
#event_post .event_list {
  padding: 10px !important;
}
#event_post .post_title > a:link, #event_post .post_title a:visited {
  color: #292f33;
  font-size: 14px;
  font-weight: bold;
}
#event_post .post_title > a:hover, #event_post .post_title > a:focus{color:#1b95e0;text-decoration: underline;}
#event_post .post_date {
  color: #8899a6;
  font-size: 13px;
  margin-left: 5px;
}
#event_post .post_desc {
  color: #292f33;
  font-size: 14px;
  margin: 0;
}
</style>