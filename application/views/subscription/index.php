<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">

				<h4 class="panel-title">Subscription <span class="text-bold">List</span></h4>
                                <?php if($user->Role_name=='Administrator' || $user->Role_name=='Client'){ ?>
                                <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Subscription/add"><i class="fa fa-plus"></i> Add Subscription</a>
                                <?php } ?>
                                <div class="panel-tools"></div>
			</div>
			<div class="panel-body">
                                <?php if($this->session->flashdata('subscription_data')){ ?>
                                <div class="errorHandler alert alert-success no-display" style="display: block;">
                                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('subscription_data'); ?> Successfully.
                                </div>
                                <?php } ?>
				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
					<thead>
						<tr>
                            <th>#</th>
							<th>Subscription Name</th>
                            <!-- <th class="hidden-xs" style="width:50% !important;">Event name</th> -->
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						for($i=0;$i<count($Subscription);$i++)
						{
						?>
						<tr>
                            <td><?php echo $i+1; ?></td>
							<td><?php echo $Subscription[$i]['Name']; ?></td>
							<!-- <td class="hidden-xs">
								<?php
									$event_name = ''; 
									//print_r($Subscription[$i]['Event']); exit;
									for($j=0;$j<count($Subscription[$i]['Event']);$j++){ 
										if($event_name=='') $event_name=$Subscription[$i]['Event'][$j]['Event_name'];
										else $event_name .=', '.$Subscription[$i]['Event'][$j]['Event_name'];
									}                     
	                                if(strlen($event_name)>135)
	                                {
	                                    echo substr($event_name, 0, 135).'...';
	                                }
	                                else
	                                {
	                                    echo $event_name; 
	                                }
                              
								?>
							</td> -->
                            <td>
                                <div>
                                    <a href="<?php echo base_url(); ?>Subscription/edit/<?php echo $Subscription[$i]['Id']; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									<!--<a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a>-->
                                    <a href="javascript:;" onclick="delete_cat(<?php echo $Subscription[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                </div>
                            </td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->

<script>
    function delete_cat(id)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Subscription/delete/"+id;
        }
    }
</script>