<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.wallform.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages-gallery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/agenda/form-validation.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main-f.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/subview.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/jquery.imgareaselect/css/imgareaselect-default.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imgareaselect/scripts/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.imgareaselect/scripts/jquery.imagemapster.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script> <script>
    jQuery(document).ready(function()
     { 
        //Main.init();
        //SVExamples.init();
        //Timeline.init();
        FormWizard.init();
        //TableData.init(); 

        //UIModals.init();
     });
</script>


<script type="text/javascript">
  jQuery(document).ready(function() {
          
          var module='<?php echo $this->uri->segment(1);?>';
          var act='<?php echo $this->uri->segment(4);?>';
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("body").addClass("sidebar-close");
          }
          else
          {
               jQuery("body").removeClass("sidebar-close");
          }

          if(module=="Maps" && act=="View")
          {
           
            jQuery("body").addClass("sidebar-close");
          }
          TableData.init();
          jQuery('#photoimg').off('click').on('change', function() {
               jQuery("#imageform").ajaxForm({target: '#preview',
                    beforeSubmit: function() 
                    {
                         jQuery("#imageloadstatus").show();
                         jQuery("#imageloadbutton").hide();
                         jQuery(".addmore_photo").css("display","block");
                    },
                    success: function() 
                    {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();
                         jQuery(".addmore_photo").css("display","block");
                         jQuery(".photo_remove_btn").click(function()
                         {
                              jQuery(".addmore_photo").css("display","none");
                              jQuery(this).parent().parent().remove();
                              jQuery("#preview li").each(function(){
                                   jQuery(".addmore_photo").css("display","block");
                              });
                         });
                    },
                    error: function() 
                    {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();
                         jQuery(".addmore_photo").css("display","none");
                    }}).submit();


          });            
  });     
</script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $(".insta_image_list").fancybox({
          openEffect  : 'none',
          closeEffect : 'none',
		  mouseWheel : false,
        });
    });
</script>