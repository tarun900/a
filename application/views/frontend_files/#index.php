<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">    
                        <?php if ($user->Role_name == 'Client') { ?>
                                <a style="top: -22px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>attendee/add/<?php echo $Attendees[$i]['Event_id']; ?>"><i class="fa fa-plus"></i> Add Attendee</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="panel-tools"></div>
            </div>
            <div class="panel-body" style="margin-top: -6%;padding: 0px;">
                <?php if ($this->session->flashdata('attendee_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Attendee <?php echo $this->session->flashdata('attendee_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#attendees_list" data-toggle="tab">
                                Attendees List
                            </a>
                        </li>
                        <li class="">
                            <a href="#invite_attendees" data-toggle="tab">
                                Invite Attendees List
                            </a>
                        </li>
                        <li class="">
                            <a href="#advertising" data-toggle="tab">
                                Menu Setting
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="attendees_list">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Name</th>
                                            <th class="hidden-xs">Email</th>
                                            <th>Address</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($Attendees); $i++) { ?>
                                            <tr>
                                                <td><?php echo $i + 1; ?></td>
                                                <td><?php echo $Attendees[$i]['Firstname']; ?></td>
                                                <td class="hidden-xs"><?php echo $Attendees[$i]['Email']; ?></td>
                                                <td><?php echo $Attendees[$i]['Street'] . ", " . $Attendees[$i]['Suburb']; ?></td>
                                                <td><?php echo $Attendees[$i]['Mobile']; ?></td>
                                                <td>
                                                    <span class="label label-sm 
                                                    <?php
                                                    if ($Attendees[$i]['Active'] == '1') {
                                                        ?> 
                                                              label-success 
                                                              <?php
                                                          } else {
                                                              ?> label-danger 
                                                          <?php }
                                                          ?>">
                                                              <?php
                                                              if ($Attendees[$i]['Active'] == '1') {
                                                                  echo "Active";
                                                              } else {
                                                                  echo "Inactive";
                                                              }
                                                              ?>
                                                    </span>
                                                </td>
                                                <td>

                                                    <?php if ($user->Role_name == 'Client') { ?>
                                                        <a href="<?php echo base_url(); ?>profile/update/<?php echo $Attendees[$i]['uid']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                                    <?php } ?>
                            <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                                    <a href="javascript:;" onclick="delete_attendee(<?php echo $Attendees[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="invite_attendees">
                                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                    <div class="checkbox-table">
                                                        <label>
                                                            <input type="checkbox" class="flat-grey selectall">
                                                        </label>
                                                    </div>
                                                    </th>
                                                    <th>Sr. No.</th>
                                                    <th class="hidden-xs">Email</th>
                                                    <th>Status</th>
            <!--                                        <th>Action</th>-->
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        for ($i = 0; $i < count($agenda_invited_list); $i++) {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php
                                                                    if ($agenda_invited_list[$i]['Status'] == "0") {
                                                                    ?>
                                                                    <div class="checkbox-table">
                                                                        <label>
                                                                            <input type="checkbox" name="invites[]"  value="<?php echo $agenda_invited_list[$i]['Emailid']; ?>" class="flat-grey foocheck">
                                                                        </label>
                                                                    </div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $i + 1; ?></td>
                                                                <td><?php echo $agenda_invited_list[$i]['Emailid']; ?></td>
                                                                <td>
                                                                    <span class="label label-sm 
                                                                        <?php
                                                                        if ($agenda_invited_list[$i]['Status'] == '1') {
                                                                            ?> 
                                                                                  label-success 
                                                                                  <?php
                                                                              } else {
                                                                                  ?> label-danger 
                                                                              <?php }
                                                                              ?>">
                                                                                  <?php
                                                                                  if ($agenda_invited_list[$i]['Status'] == '1') {
                                                                                      echo "Register";
                                                                                  } else {
                                                                                      echo "Not Register";
                                                                                  }
                                                                        ?>
                                                                        </span>
                                                                </td>
            <!--                                                    <td><span id="delete_selection" name="" class="label btn-red pull-right margin-left-10 tooltips" onClick="" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span></td>-->
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <textarea name="msg" class="summernote" placeholder="Type your message here"></textarea><br/>
                                            <input type="text" name="sent_from" class="col-md-6 form-control required" placeholder="Sender Email Address"><br/><br/><br/>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn btn-yellow btn-block" type="submit">
                                                        Invite Again <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tab-pane fade" id="advertising">
                                        <div class="row padding-15">
                                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>menu/index" enctype="multipart/form-data">
                                    <div class="col-md-12 alert alert-info">
                                        <h5 class="space15">Menu Title & Image</h5>
                                        <div class="form-group">
                                            <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        </div>
                                        <div class="form-group">
                                           <div data-provides="fileupload" class="fileupload fileupload-new">
                                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> 
                                                <?php if($img =='') { ?>
                                                <span class="fileupload-new">Select file</span>
                                                <?php } else { ?>
                                                <span class="fileupload-new">Change</span>
                                                <?php } ?>
                                                    <input type="file" name="Images[]">
                                                </span>
                                                <span class="fileupload-preview"></span>
                                                <a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
                                                        &times;
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                                Create Home Tab
                                            </label>
                                            <?php if($img !='') { ?>
                                            <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                            <?php } ?>
                                        </div>
                            
                                        <div class="form-group">
                                            <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                            <div class="col-sm-5">
                                                <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                        <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                        <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br/><br/><br/><br/>
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script>
    function delete_attendee(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>attendee/delete/" + id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->