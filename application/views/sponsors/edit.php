<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.summernote').summernote({
          height: 300,                 
          minHeight: null,       
          maxHeight: null,
        });
    });
</script>

<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Exhibitor</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                 

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Company Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Exhibitor Heading" id="Heading" value="<?php echo $exibitor_by_id[0]['Heading']; ?>" name="Heading" class="form-control name_group required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Exhibitor Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="1" <?php if ($exibitor_by_id[0]['Status'] == '1') echo ' checked="checked"'; ?> name="Status">
                                Active
                            </label>
                            <label class="radio-inline">
                                <input type="radio" class="purple" value="0" <?php if ($exibitor_by_id[0]['Status'] == '0') echo ' checked="checked"'; ?> name="Status">
                                Inactive
                            </label>    
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Description 
                        </label>
                        <div class="col-sm-9">
                            <textarea id="Short_desc" name="Short_desc" class="summernote"><?php echo $exibitor_by_id[0]['Short_desc'] ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Website URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Website Url (e.g: https://www.yoursite.com)" id="website_url" name="website_url" class="form-control" value="<?php echo $exibitor_by_id[0]['website_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Facebook URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Facebook Url (e.g: https://www.yoursite.com)" id="facebook_url" name="facebook_url" class="form-control" value="<?php echo $exibitor_by_id[0]['facebook_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Twitter URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Twitter Url (e.g: https://www.yoursite.com)" id="twitter_url" name="twitter_url" class="form-control" value="<?php echo $exibitor_by_id[0]['twitter_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>LinkedIn URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="LinkedIn Url (e.g: https://www.yoursite.com)" id="linkedin_url" name="linkedin_url" class="form-control" value="<?php echo $exibitor_by_id[0]['linkedin_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Email Address</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Email Address" id="email_address" name="email_address" class="form-control" value="<?php echo $exibitor_by_id[0]['email_address'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Phone Number1</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Phone Number1" id="phone_number1" name="phone_number1" class="form-control" value="<?php echo $exibitor_by_id[0]['phone_number1'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Phone Number2</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Phone Number2" id="phone_number2" name="phone_number2" class="form-control" value="<?php echo $exibitor_by_id[0]['phone_number2'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Company Logo</label>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($exibitor_by_id[0]['company_logo']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" name="company_logo">
                                </span>
                            </div>
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                    <input type="hidden" name="old_company_logo" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>                                                                                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Banner Images</label>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($exibitor_by_id[0]['Images']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" name="Images[]" multiple>
                                </span>
                            </div>
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>"><a class="btn btn-red remove_image" href="javascript:;" style=""><i class="fa fa-times fa fa-white"></i></a>
                                    <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>                                                                                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>
<!-- end: PAGE CONTENT-->