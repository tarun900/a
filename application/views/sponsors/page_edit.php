<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // $('.summernote').summernote({
        //   height: 300,                 
        //   minHeight: null,       
        //   maxHeight: null,
        // });
        $('#Short_desc').tagsInput({
            maxTags: 6,
            allowSpaces: false,
            removeWithBackspace : true
        });
    });
</script>
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />  
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">    
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>

            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Sponsors</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <?php $select_user_id=$sponsors_by_id[0]['user_id'];?>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                       <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                       Sponsors Name <span class="symbol required"></span>
                       </label>
                       <div class="col-sm-9">
                        <input type="text" placeholder="Sponsors Name" value="<?php echo $sponsors_by_id[0]['Sponsors_name']; ?>" class="form-control required" id="Firstname" name="Sponsors_name">
                       </div>
                    </div>
                    
                     <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                        Sponsor Type
                        </label>
                        <div class="col-sm-9">
                            <select name="sponsors_type" id="sponsors_type" class="form-control">
                                <option value="">Select Sponsor Type</option>
                                <?php foreach ($sponsors_type as $key => $value) { ?>
                                    <option value="<?php echo $value['type_id']; ?>" <?php if($sponsors_by_id[0]['st_id']==$value['type_id']){ ?> selected="selected" <?php } ?>><?php echo $value['type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Company Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Company Name" id="Heading" value="<?php echo $sponsors_by_id[0]['Company_name']; ?>" name="Heading" class="form-control name_group required">
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                             Description
                        </label>
                        <div class="col-sm-9">
                           <textarea id="Description" style="height:150px;" placeholder="Type your Description here" class="summernote form-control" name="Description"><?php echo $sponsors_by_id[0]['Description'] ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Website URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Website Url (e.g: https://www.yoursite.com)" id="website_url" name="website_url" class="form-control" value="<?php echo $sponsors_by_id[0]['website_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Facebook URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Facebook Url (e.g: https://www.yoursite.com)" id="facebook_url" name="facebook_url" class="form-control" value="<?php echo $sponsors_by_id[0]['facebook_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Twitter URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Twitter Url (e.g: https://www.yoursite.com)" id="twitter_url" name="twitter_url" class="form-control" value="<?php echo $sponsors_by_id[0]['twitter_url'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>LinkedIn URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="LinkedIn Url (e.g: https://www.yoursite.com)" id="linkedin_url" name="linkedin_url" class="form-control" value="<?php echo $sponsors_by_id[0]['linkedin_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Youtube URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Youtube Url (e.g: https://www.youtube.com)" id="youtube_url" name="youtube_url" class="form-control" value="<?php echo $sponsors_by_id[0]['youtube_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            <em>Instagram URL</em>
                        </label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <input type="text" placeholder="Instagram Url (e.g: https://www.instagram.com)" id="instagram_url" name="instagram_url" class="form-control" value="<?php echo $sponsors_by_id[0]['instagram_url'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Company Logo</label>
                        <div class="modal fade" id="companylogomodelscopetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Company Logo</h4>
                                </div>
                                <div class="modal-body">
                                    <section>
                                        <div class="demo-wrap" style="display:none;">
                                            <div class="container">
                                                <div class="grid">
                                                    <div class="col-1-2">
                                                        <div id="vanilla-demo"></div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <strong>Vanilla Example</strong>
                                                        <div class="actions">
                                                            <button class="vanilla-result">Result</button>
                                                            <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                                            <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="preview_company_logo_crop_tool"></div>
                                            <div class="col-sm-3 crop-btn">
                                                <button class="btn btn-green btn-block" id="company_logo_upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>         
                                    </section>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($sponsors_by_id[0]['company_logo']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <div class="fileupload-preview fileupload-exists thumbnail" id="company_logo_preview_div">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                    <input type="file" id="company_logo" name="company_logo">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_logo_data">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                            <input type="hidden" name="company_logo_crop_data_textbox" id="company_logo_crop_data_textbox">
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                    <input type="hidden" name="old_company_logo" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>              
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" for="form-field-1">Banner Images</label>
                        <div class="modal fade" id="headermodelscopetool" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Banner Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row"  id="cropping_banner_div">
                                            <div>
                                                <img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <?php $images_array = json_decode($sponsors_by_id[0]['Images']); ?>
                            <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div">
                                </div>
                                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                    <input type="file" id="header_image_sponder" name="banner_Images">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                            <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                            <?php  for($i=0; $i<count($images_array); $i++) {  ?>
                                <div class="col-sm-3 fileupload-new thumbnail center">
                                    <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>"><a class="btn btn-red remove_image" href="javascript:;" style=""><i class="fa fa-times fa fa-white"></i></a>
                                    <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                </div>
                            <?php } ?>        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image_sponder'); 
$(document).ready(function(){
    $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              uploadedImageURL = URL.createObjectURL(file);
              $bannerimage.attr('src', uploadedImageURL);
              $('#company_banner_crop_data_textbox').val('');
              $('#headermodelscopetool').modal('toggle');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
            $('#banner_preview_image_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
            $('#company_banner_crop_data_textbox').val(croppedCanvas.toDataURL());
            $("#header_image_sponder").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    }); 
});
</script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js"></script>
<script>
    Demo.init();
</script>
<script type="text/javascript">
$('#remove_company_logo_data').click(function(){
    $('#company_logo_crop_data_textbox').val('');
});
$('#remove_company_banner_data').click(function(){
    $('#company_banner_crop_data_textbox').val('');
});
</script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>