<?php  $user = $this->session->userdata('current_user'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/slider-pro-master/dist/css/slider-pro.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/presentation.css?<?php echo time(); ?>">
<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">
<style type="text/css">
.sp_chart_as_slider_div {
  height: 70%;
  width: 100%;
}
.chart {
  position: absolute;
  top: 5;
  left: 5;
  width:100%;
  height:60%;
}
#question_show_iframe .main-content
{
  background:#ffffff;
}
#question_show_iframe .panel
{
  box-shadow :none;
}
.option-list {
  display: block;
  margin: 4% auto;
  padding: 0 15px;
  text-align: center;
  width: 100%;
}
.option-list li {
  line-height: 15px;
  margin: 0 0 20px;
  position: relative;
  text-indent: 0;
  padding: 0 20px 0 20px;font-size:40px;text-align:center;
  width:100%;
}
.option-list li label{display:inline-block;vertical-align:middle;width:20px;height:20px;}
ul.option-list li span{display:inline-block;vertical-align:middle;max-width:96%;width:auto !important;}

@media (min-width: 1920px){
	.option-list li {font-size:34px;}
}

@media (min-width: 1400px){
	.option-list li {font-size:24px;}
}

@media (max-width: 767px){
	.option-list li {font-size:16px;}
	ul.option-list li span{max-width:90%;width:auto !important;}
	.option-list{ height:100px;
  overflow:scroll;}
}
.highcharts-credits
{
  display:none;
}
.sp_chart_as_slider_div h3
{
    font-size: 45px;
}

/*---------------5-4-18-------------*/
.slider-pro .sp_survey_as_slider_div{width:68%;}
.slider-pro .sp_survey_as_slider_div .list-group-item-heading{font-size:45px;line-height: 55px !important;}
.slider-pro .sp_survey_as_slider_div .btn.btn-green.btn-block{display: none;}
.sp_survey_as_slider_div span.hdr-span{font-size:24px;}
.icheckbox_line-blue, .iradio_line-blue{font-size:34px;}

</style>
<?php if($presentations[0]['Status'] =='1') {
$user_permissions=explode(",",$presentations[0]['user_permissions']);

if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){
  $class="";
 ?>
<div id="view_help_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Help</h4>
      </div>
      <div class="modal-body">
        <div class="panel">
          <div class="panel panel-white">
            <div class="panel-body">
              <h2 style="text-align: center;">PRESENTER TOOLS</h2>
              <p>As a Presenter you are able to control what slides your attendees  can see on their screens in real-time (live). You can also push out polls, results and view any questions asked all from this presenter platform.</p>
              <h3>Unlocked/Locked</h3>
              <p>When the top button reads “Unlocked” your attendees can scroll through to other presentation slides. Tap this button to change this button to “Locked”. When the presentation is Locked your attendees cannot scroll from slide to slide outside the slide that appears on your  screen. </p>
              <h3>Push</h3>
              <p>When you tap the Push button every attendee will have the slide that appears on your screen appear on theirs automatically.</p>
              <h3>Questions</h3>
              <p>Tap this button to view any questions that have been asked by your attendees using the app. If you have a moderator, these questions may have been moderated.</p>
              <h3>Next</h3>
              <p>Tap Next to show the next slide on all attendees devices.</p>
              <h3>Previous</h3>
              <p>Tap Previous to show the previous slide on all attendees devices.</p>
              <h3>Polls</h3>
              <p>After you push out a poll you can view results by tapping the View Results button. Results will appear in a pie chart. Tap the Push button if you want to show these results on all of your attendees devices.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="view_question_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Questions</h4>
      </div>
      <div class="modal-body">
        <iframe src="" id="question_show_iframe" style="width: 100%;height: 1000px;border: none;"></iframe>
      </div>
    </div>
  </div>
</div>
<?php $class="slider_user_menu_show_div"; }else{
  $class="slider_full_width";
  }?>
  <div style="display:none;">
    <input type="hidden" id="silde_no_text_box" value="0">
    <input type="hidden" value="<?php echo $presetation_tool[0]['push_result']; ?>" id="view_result_id_textbox">
  </div>
  <div id="view_result_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Result</h4>
      </div>
      <div class="modal-body" id="view_result_iframe_div_body">
        <iframe src="" id="chart_show_iframe" style="width:100%;min-height:500px;border:none;" allowfullscreen='true' webkitallowfullscreen='true' mozallowfullscreen='true'></iframe>
        <input type="hidden" id="chart_result_type" name="chart_result_type" value="<?php echo $presetation_tool[0]['chart_type']; ?>">
        <input type="button" value="Push Result" id="push_result_btn" class="btn btn-green btn-block" style="display:none;">
        <input type="button" value="Push Result" id="push_barresult_btn" class="btn btn-green btn-block" style="display:none;">
        <input type="button" value="Full Screen" id="full_screen_result_btn" class="btn btn-green btn-block" style="display:none;">
      </div>
    </div>
  </div>
</div>
<div id="loder_div_show_on_page_load"><img src="<?php echo base_url().'assets/images/loading.gif'; ?>"></div>
<div id="presentation_slider_pro_slider_show" class="slider-pro presenation_slider_wrapper <?php echo $class; ?>" style="display: none;">
<div class="buttons zoom-btn" style="display: none;">
    <button class="zoom-in"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
    <button class="zoom-out"><i class="fa fa-minus-square" aria-hidden="true"></i></button>
  </div> 
	<div class="sp-slides">
		<?php 
		    $array1 = json_decode($presentations[0]['Images']);
        $array2 = json_decode($presentations[0]['Image_lock']);
        $user_permissions=explode(",",$presentations[0]['user_permissions']);
        if(!in_array($user[0]->Id,$user_permissions)){
          if(!empty($presetation_tool[0]['push_result']))
          {
            array_unshift($array1, "chart");
            array_unshift($array2, "1");        
          }
        }
        $final_array = array();
        foreach ($array2 as $key => $value) 
        {
          if($value)
          {
            if(!empty($array1[$key])){
              $final_array[] = $array1[$key];
            }
          }
        }
        foreach ($final_array as $key => $value) {
		?>
		<div class="sp-slide">
      <?php if($value=="chart"){ if(!empty($presetation_tool[0]['push_result']) && count($final_Array_chart)>0){ ?>
        <div class="sp-image">
         <?php $color_arr=array('#109618','#ff9900','#3366cc','#dc3912','#800000','#FF0000','#C14C33','#593027','#96944C','#156A15','#3B2129','#54073E','#5711A7','#093447','#0C3F33'); foreach($final_Array_chart as $ckey => $item) 
              { ?>
        <div class="sp_chart_as_slider_div">      
        <h3 style="text-align: center;"><?php echo $ckey; ?></h3>
        <!-- <div id='chart_wrap' align='center' style='margin:0 auto;'> -->
        <div id="piechart<?php echo $key ?>" class="chart"></div></div>
        <?php $colorindex=0; foreach ($item as $key1 => $value1) { if($colorindex==count($item)-1){$colorta.="'".$color_arr[$colorindex]."'";}else{ $colorta.="'".$color_arr[$colorindex]."',"; } $colorindex++;  } ?>
        <ul style="list-style: none;" class="option-list">
          <?php $dint=0; foreach ($item as $key2 => $value2) { ?>
          <li><label style="border-radius: 100%;background-color:<?php echo $color_arr[$dint]; $dint++;?>">&nbsp;&nbsp;&nbsp;&nbsp;</label>  <span><?php echo $key2; ?></span></li>
          <?php } ?>
        </ul>
        <?php if($presetation_tool[0]['chart_type']=='0'){ ?>
        <script type="text/javascript">
        /*google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart1);
        function drawChart1() {
          var data = google.visualization.arrayToDataTable([['Options', 'No. of users'],  <?php foreach ($item as $a => $b) {?>
            ['<?php echo $a; ?>', <?php echo $b; ?>],
            <?php } ?>
            ]);
            var options = {
            colors:[<?php echo $colorta ?>],
            width:'100%',
            height:'100%',
            align: 'center',    
            pieSliceText: 'percentage',         
            verticalAlign: 'middle',
            pieSliceTextStyle: { color: 'black',},
            fontSize: 20,
            legend:{position:'none',textStyle: {color: 'blue', fontSize: 20}},
            chartArea: {
                    left: "3%",
                    top: "1%",
                    height: "70%",
                    width: "94%"
                }
            };
            var chart = new 
            google.visualization.PieChart(document.getElementById('piechart<?php echo $key;?>'));
            chart.draw(data, options);
        }*/
        $(document).ready(function () {
          Highcharts.chart('piechart<?php echo $key; ?>', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie', 
            },
            animation: false ,
            colors:<?php echo json_encode($color_arr); ?>,
            title: {
              text: '<?php echo 'Question: '. $key; ?>',
              style: {
                display: 'none'
              }
            },
            tooltip: {
              pointFormat: '<b>{point.percentage:.2f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.2f} %',
                    style: {
                      fontSize: '25px',
                      fontWeight: 'bold',
                      color: 'contrast',
                      textOutline: '1px contrast'
                    },
                },
                showInLegend: false,
              }
            },
            series: [{
              name: 'Answer',
              colorByPoint: true,
              data: 
              <?php $arr=array(); foreach ($item as $a => $b) { 
                $arr[]=array('name'=>$a,'y'=>$b);
              }
              echo json_encode($arr);
              ?>
            }]
          });
        });
        </script>
        <?php }else{ ?>
        <script type="text/javascript">
       /* google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart1);
        function drawChart1() {
          var data = google.visualization.arrayToDataTable([['Options', 'No. of users',{ role: 'style' }],  <?php $i=0; foreach ($item as $a => $b) {?>
            ['<?php echo $a; ?>', <?php echo $b; ?>,"<?php echo $color_arr[$i];$i++; ?>"],
            <?php } ?>
            ]);
            var options = {
            colors:[<?php echo $colorta ?>],
            width:'70%',
            height:'90%',
            align: 'center',    
            pieSliceText: 'percentage',         
            verticalAlign: 'middle',
            pieSliceTextStyle: { color: 'black',},
            fontSize: 20,
            legend:{position:'none',textStyle: {color: 'blue', fontSize: 20}},
            chartArea: {
                    left: "20%",
                    top: "1%",
                    height: "70%",
                    width: "70%"
                }
            };
            var chart = new 
            google.visualization.BarChart(document.getElementById('piechart<?php echo $key;?>'));
            chart.draw(data, options);
        }*/
        $(document).ready(function () {
          var data = <?php echo json_encode(array_values($item))?>;
          var dataSum = 0;
          for (var i=0;i < data.length;i++) {
              dataSum += data[i]
          }
          Highcharts.chart('piechart<?php echo $key; ?>', {
            chart: {
              type: 'column'
            },
            title: {
              text: '<?php echo 'Question: '. $key; ?>',
              style: {
                display: 'none'
              }
            },
            tooltip: {
              valueSuffix: ' Users'
            },
            xAxis: {
              type: 'category'
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Total User Answers'
              }
            },
            plotOptions: {
              column: {
                dataLabels: {
                  enabled: true,
                  formatter:function() {
                    var pcnt = (this.y / dataSum) * 100;
                    return Highcharts.numberFormat(pcnt) + '%';
                  },
                  style: {
                      fontSize: '25px',
                      fontWeight: 'bold',
                      color: 'contrast',
                      textOutline: '1px contrast'
                    },
                }
              }
            },
            colors:<?php echo json_encode($color_arr); ?>,
            legend: {
              enabled:false,
            },
            series: [{
              name: 'Answers',
              colorByPoint: true,
              data: 
              <?php $arr=array(); foreach ($item as $a => $b) { 
                $arr[]=array('name'=>$a,'y'=>$b);
              }
              echo json_encode($arr);
              ?>
            }]
          });
        });
        </script>
        <?php } } ?>
        </div>
      <?php } }else{ ?>
			<?php $ext = pathinfo($value, PATHINFO_EXTENSION);
            if(!empty($ext)){ ?>
			<img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-small="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-medium="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-large="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-retina="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>" />
            <?php }else{ ?>
            	<div class="sp-image">
                <div class="sp_survey_as_slider_div">
            		<h4 class="list-group-item-heading"><?php echo $value[0]->Question; ?></h4>
            		<?php if(!empty($value[0]->Answer)){ ?>
                  <span class="hdr-span" id="msg_question_<?php echo $value[0]->Id ?>"> Thank you for your answer.</span>
                  <?php }else{ ?>
                  <span class="hdr-span" id="msg_question_<?php echo $value[0]->Id ?>"> Please select one answer.</span>
                  <?php } ?>
                  <ul class="list">
                  <?php $ans=json_decode($value[0]->Option,TREU);
                    foreach ($ans as $key1 => $value1) { ?>
                      <li>
                        <input type="radio" name="question_option<?php echo $value[0]->Id; ?>" value="<?php echo $value1; ?>" class="required radio-callback" <?php if($value1==$value[0]->Answer){ ?> checked="checked" <?php } ?>>
                        <label for="line-radio-<?php echo $value[0]->Id; ?>"><?php echo $value1; ?></label>
                      </li>
                  <?php } ?>
                  </ul>
                  <?php if(empty($value[0]->Answer)){ ?>
                  <div class="col-sm-12"><button name="next_slied" id="submit_ans_btn_<?php echo $value[0]->Id ?>" class="btn btn-green btn-block" onclick='saveansbyquestion("<?php echo  $value[0]->Id; ?>");' value="Next">Submit</button></div>
                  <?php } ?>
                </div>  
            	</div>
            <?php } ?>  
		</div>
		<?php } } ?>
	</div>
  <?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
  if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){ ?>
  <div class="presenter_left_menu_tool" id="presenter_left_menu_tool_div">
  <ul>
    <li>
    <a href="javascript:void(0);" <?php if(!empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_unlocked"><i class="fa fa-unlock"></i> Unlocked </a>
    <a href="javascript:void(0);" <?php if(empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_locked"><i class="fa fa-lock"></i> Locked </a>
    </li>
    <li><a href="javascript:void(0);" id="link_push"><i class="fa fa-play"></i> Push</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_question_models" id="link_question"><i class="fa fa-question"></i> Questions</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_result_models" id="link_view_result"><i class="fa fa-bar-chart-o"></i> View Pie Chart</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_result_models" class="link_view_bar_chart_result"><i class="fa fa-bar-chart-o"></i> View Bar Chart</a></li>
    <li><a href="javascript:void(0);" id="link_next"><i class="fa fa-step-forward"></i> Next</a></li>
    <li><a href="javascript:void(0);" id="link_prev"><i class="fa fa-step-backward"></i> Previous</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_help_models" id="link_help"><i class="fa fa-life-ring" aria-hidden="true"></i> Help</a></li>
    </ul>
  </div> 
  <?php } ?>
  
  <?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
  if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){ ?>
  <div class="presenter_left_menu_tool_mobile" id="presenter_left_menu_tool_div_mobile" style="display:none;">
  <ul>
    <li>
    <a href="javascript:void(0);" <?php if(!empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_unlocked"><i class="fa fa-unlock"></i> Unlocked </a>
    <a href="javascript:void(0);" <?php if(empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_locked"><i class="fa fa-lock"></i> Locked </a>
    </li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_question_models" id="link_question"><i class="fa fa-question"></i> Questions</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_result_models" id="link_view_result"><i class="fa fa-bar-chart-o"></i> View Pie Chart</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_result_models" class="link_view_bar_chart_result"><i class="fa fa-bar-chart-o"></i> View Bar Chart</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_help_models" id="link_help"><i class="fa fa-life-ring" aria-hidden="true"></i> Help</a></li>
    </ul>
  </div> 
  <?php } ?>
  
  <?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
  if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){ ?>
  <div class="presenter_left_menu_tool_bottom_mobile" id="presenter_left_menu_tool_div_bottom_mobile" style="display:none;">
  <ul>
     <li><a href="javascript:void(0);" id="link_prev"><i class="fa fa-step-backward"></i> Previous</a></li>
	 <li><a href="javascript:void(0);" id="link_push"><i class="fa fa-play"></i> Push</a></li>
	 <li><a href="javascript:void(0);" id="link_next"><i class="fa fa-step-forward"></i> Next</a></li>
    </ul>
  </div> 
  <?php } ?>
 
	<?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
      if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){ ?>
	<div class="sp-thumbnails my-sp-thumbnails">
	<?php foreach ($final_array as $key => $value) { ?>
		<div class="sp-thumbnail">
			<?php if(!is_array($value)){ ?>
      <div class="slider_title_div">Slide</div>  
			<img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>&w=110&h=110"/>
			<?php }else{ ?>
      <div class="slider_title_div">Poll</div>
      <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/images/poll_default_img.png&w=110&h=110"/>
            <?php } ?>
		</div>
	<?php } ?>
	</div>
	<?php } ?>
</div>
<?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
if(in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){ ?>      
<div class="presenter_mode_menu_tool" id="presenter_mode_menu_tool_div">
 <a href="javascript:void(0);" id="preview_mode_btn_link" <?php if(!empty($presetation_tool[0]['push_images'])){ ?> style="display:none;" <?php } ?>><i style="font-size:60px;" class="fa fa-eye"></i> <br/><span>Preview Mode</span></a>
 <a href="javascript:void(0);" id="live_mode_btn_link" <?php if(empty($presetation_tool[0]['push_images'])){ ?> style="display:none;" <?php } ?>><i style="font-size:60px;" class="fa fa-eye"></i> <br/><span>Live Mode</span></a>
</div>
<?php } ?>
<?php } else { ?>
  <h1 style="text-align:center;">Presentation is end.</h1>
<?php } ?>
<?php if($presentations[0]['Thumbnail_status'] == '0' && !in_array($user[0]->Id,$user_permissions)) { ?>
<style type="text/css">
.sp-thumbnails-container
  {
    display: none;
  }
</style>
<?php } ?>
<script type="text/javascript">
 jQuery(document).ready(function()
     {
      $('body').attr('class',"sidebar-close");
     });
</script>
<script id="siderTemplate" type="text/x-jQuery-tmpl">
    <div class="sp-slide">
      {{if img}}
        <img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-small="<?php echo base_url(); ?>assets/user_files/${img}"
            data-medium="<?php echo base_url(); ?>assets/user_files/${img}"
            data-large="<?php echo base_url(); ?>assets/user_files/${img}"
            data-retina="<?php echo base_url(); ?>assets/user_files/${img}" />
      {{else}}
      <div class="sp-image">{{html div}}</div>
      {{/if}}
    </div>
</script>
<script id="siderTemplate1" type="text/x-jQuery-tmpl">
  <div class="sp-thumbnail">
  {{if img}}
    <div class="slider_title_div">Slide</div>
    <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/user_files/${img}&w=110&h=110"/>
  {{else}}
  <div class="slider_title_div">Poll</div>
  <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/images/${div}&w=110&h=110"/>
  {{/if}}  
  </div>
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/test/libs/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/dist/jquery.panzoom.js"></script>
<script>
$(window).load(function(){
  $('#presentation_slider_pro_slider_show').show();
  $('#loder_div_show_on_page_load').hide(); 
  $('.sp-selected').panzoom({
    $zoomIn: $(".zoom-in"),
    $zoomOut: $(".zoom-out"),
    disablePan: true,
  });
  $('.sp-slides').css("overflow","visible");
  $('.sp-slide').each( function( index, listItem ) {
    $(this).css("overflow","visible");
  });
});
</script>