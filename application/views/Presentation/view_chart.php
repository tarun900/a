	<style type="text/css">
		body {
    width:50%;
    margin:10% auto;
    background:#e6e6e6;
}
#chart_wrap {
    border:1px solid gray;
    position: relative;
    padding-bottom: 100%;
    height: 0;
    overflow:hidden;
}
#chart {
    position: absolute;
    top: 0;
    left: 0;
    width:100%;
    height:100%;
}
	</style>
<html>
<head>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>                     
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body>
<div id="chart_wrap"><div id="chart"></div></div>
<p id="canvas_size"></p>
</body>
</html>
<script type="text/javascript">
	google.load("visualization", "1", {
    packages: ["corechart"]
});
google.setOnLoadCallback(initChart);

$(window).on("throttledresize", function (event) {
    initChart();
});

function initChart() {
    var options = {
	    legend: 'bottom',
        width: '100%',
        height: '100%',
        pieSliceText: 'percentage',
        colors: ['#0598d8', '#f97263'],
        chartArea: {
            left: "3%",
            top: "3%",
            height: "94%",
            width: "94%"
        }
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },
    };

    var data = google.visualization.arrayToDataTable([
        ['Gender', 'Overall'],
        ['M', 110],
        ['F', 20]
    ]);
    drawChart(data, options)
}

function drawChart(data, options) {
    var chart = new google.visualization.PieChart(document.getElementById('chart'));
    chart.draw(data, options);
}
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js" charset="UTF-8"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script>
    jQuery(document).ready(function() {
        Charts.init();
    });
</script>
