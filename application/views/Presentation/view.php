<?php  $user = $this->session->userdata('current_user'); ?>
<!-- <meta http-equiv="refresh" content="30" /> -->
<link href="<?php echo base_url(); ?>assets/css/custom-radio-checkbox.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/blue-radio-checkbox.css" rel="stylesheet">
<style type="text/css">
/*body {
    
    height: auto;
    margin:10% auto;
}*/
.sp-image
{
  background-color: white;
}
#chart_wrap {
    width:800px;
    position: relative;
    padding-bottom: 100%;
    /*height: 200px;*/
    overflow:hidden;
}
.chart {
    position: absolute;
    top: 0;
    left: 0;
    width:100%;
    height:100%;
}
  .presenter_left_menu_tool
  {
    float: left;
  }
  .presenter_left_menu_tool ul{
    list-style: none;
  }
  .presenter_left_menu_tool ul li
  {
    border-bottom: 1px solid;
    padding: 10px 0px;
  }
    @media (min-width: 480px) and (max-width: 991px){
    .my_slider_sliderPro.slider_full_width .sp-image{max-width:100% !important;}
  }
  @media screen and (max-width: 991px){
    #chart_wrap {
      width: 500px;
    }
  }
  @media screen and (max-width: 767px){
  .my_slider_sliderPro.slider_full_width #example3 .sp-slides-container{margin-top:0 !important; }
  #chart_wrap {
      width: 100%;
    }
  }
  /*@media screen and (max-width: 600px){
    #chart_wrap {
      width: 300px;
    }
  }*/
  @media screen and (max-width: 479px){
    /*#chart_wrap {
      width: 80% !important;
    }*/
    .my_slider_sliderPro.slider_full_width #example3 .sp-slides-container{margin-top:10px; }
  }
</style>
<?php if($presentations[0]['Status'] =='1') {
$user_permissions=explode(",",$presentations[0]['user_permissions']);
if(in_array($user[0]->Id,$user_permissions)){
  $class="";
 ?>
<div class="presenter_left_menu_tool" id="presenter_left_menu_tool_div">
  <ul>
    <li>
    <a href="javascript:void(0);" <?php if(!empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_unlocked"><i class="fa fa-unlock"></i> Unlocked </a>
    <a href="javascript:void(0);" <?php if(empty($presetation_tool[0]['lock_image'])){ ?> style="display:none;" <?php } ?> id="link_locked"><i class="fa fa-lock"></i> Locked </a>
    </li>
    <li><a href="javascript:void(0);" id="link_push"><i class="fa fa-play"></i> Push</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_question_models" id="link_question"><i class="fa fa-question"></i> Questions</a></li>
    <?php if($view_result_btn=='1'){ ?>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_result_models" id="link_view_result"><i class="fa fa-bar-chart-o"></i> View Result</a></li>
    <?php } ?>
    <li><a href="javascript:void(0);" id="link_next"><i class="fa fa-step-forward"></i> Next</a></li>
    <li><a href="javascript:void(0);" id="link_prev"><i class="fa fa-step-backward"></i> Previous</a></li>
    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#view_help_models" id="link_help">Help</a></li>
  </ul>
</div> 
<div id="view_help_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Help</h4>
      </div>
      <div class="modal-body">
        <div class="panel">
          <div class="panel panel-white">
            <div class="panel-body">
              <h2 style="text-align: center;">PRESENTER TOOLS</h2>
              <p>As a Presenter you are able to control what slides your attendees  can see on their screens in real-time (live). You can also push out polls, results and view any questions asked all from this presenter platform.</p>
              <h3>Unlocked/Locked</h3>
              <p>When the top button reads “Unlocked” your attendees can scroll through to other presentation slides. Tap this button to change this button to “Locked”. When the presentation is Locked your attendees cannot scroll from slide to slide outside the slide that appears on your  screen. </p>
              <h3>Push</h3>
              <p>When you tap the Push button every attendee will have the slide that appears on your screen appear on theirs automatically.</p>
              <h3>Questions</h3>
              <p>Tap this button to view any questions that have been asked by your attendees using the app. If you have a moderator, these questions may have been moderated.</p>
              <h3>Next</h3>
              <p>Tap Next to show the next slide on all attendees devices.</p>
              <h3>Previous</h3>
              <p>Tap Previous to show the previous slide on all attendees devices.</p>
              <h3>Polls</h3>
              <p>After you push out a poll you can view results by tapping the View Results button. Results will appear in a pie chart. Tap the Push button if you want to show these results on all of your attendees devices.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="view_question_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Questions</h4>
      </div>
      <div class="modal-body">
        <iframe src="" id="question_show_iframe" style="width: 100%;height: 1000px;border: none;"></iframe>
      </div>
    </div>
  </div>
</div>
<?php }else{
  $class="slider_full_width";
  }  ?>
  <div style="display:none;"><input type="hidden" id="silde_no_text_box" value="0"></div>
  <div id="view_result_models" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Result</h4>
      </div>
      <div class="modal-body" id="view_result_iframe_div_body">
        <iframe src="" id="chart_show_iframe" style="width:100%;min-height:500px;border:none;" allowfullscreen='true' webkitallowfullscreen='true' mozallowfullscreen='true'></iframe>
        <input type="button" value="Push Result" id="push_result_btn" class="btn btn-green btn-block" style="display:none;">
        <input type="button" value="Full Screen" id="full_screen_result_btn" class="btn btn-green btn-block" style="display:none;">
      </div>
    </div>
  </div>
</div>

<div class="my_slider_sliderPro <?php echo $class; ?>" id="my_slider_silderpro_div">
  <div id="example3" class="slider-pro">
      <div class="sp-slides">
        <?php 
            $array1 = json_decode($presentations[0]['Images']);
            $array2 = json_decode($presentations[0]['Image_lock']);
            $final_array = array();
            foreach ($array2 as $key => $value) 
            {
              if($value)
              {
                if(!empty($array1[$key])){
                  $final_array[] = $array1[$key];
                }
              }
            } ?>
          <?php foreach ($final_array as $key => $value) { ?>
          <div class="sp-slide">
            <?php $ext = pathinfo($value, PATHINFO_EXTENSION);
             if(!empty($ext)){ ?>
            <img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-src="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-small="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-medium="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-large="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>"
              data-retina="<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>" />
              <?php }else{ ?>
                <div class="sp-image">
                  <h4 class="list-group-item-heading"><?php echo $value[0]->Question; ?></h4>
                  <?php if(!empty($value[0]->Answer)){ ?>
                  <span class="hdr-span" id="msg_question_<?php echo $value[0]->Id ?>"> Thank you for your answer.</span>
                  <?php }else{ ?>
                  <span class="hdr-span" id="msg_question_<?php echo $value[0]->Id ?>"> Please select one answer.</span>
                  <?php } ?>
                  <ul class="list">
                  <?php $ans=json_decode($value[0]->Option,TREU);
                    foreach ($ans as $key1 => $value1) { ?>
                      <li>
                        <input type="radio" name="question_option<?php echo $value[0]->Id; ?>" value="<?php echo $value1; ?>" class="required radio-callback" <?php if($value1==$value[0]->Answer){ ?> checked="checked" <?php } ?>>
                        <label for="line-radio-<?php echo $value[0]->Id; ?>"><?php echo $value1; ?></label>
                      </li>
                  <?php } ?>
                  </ul>
                  <?php if(empty($value[0]->Answer)){ ?>
                  <div class="col-sm-12"><button name="next_slied" id="submit_ans_btn_<?php echo $value[0]->Id ?>" class="btn btn-green btn-block" onclick='saveansbyquestion("<?php echo  $value[0]->Id; ?>");' value="Next">Submit</button></div>
                  <?php } ?>
                </div>
                <?php } ?>
          </div>
          <?php } ?>
      </div>
      <?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
      if(in_array($user[0]->Id,$user_permissions)){ ?>
     <div class="buttons zoom-btn">
      <button class="zoom-in">+</button>
      <button class="zoom-out">-</button>
      </div>  
      <?php } ?>   
      <div class="thumbnail_btn"><input type="button" value="Full Screen" class="btn btn-green btn-block" id="full_screen_show" style="display: none;">
      <?php  if(!in_array($user[0]->Id,$user_permissions)){ ?>
       <!--  <input type="button" value="View Result" data-toggle="modal" data-target="#view_result_models" class="btn btn-green btn-block" id="view_result_btn" <?php if(empty($presetation_tool[0]['push_result'])){ ?> style="display:none;" <?php } ?>> -->
      <?php } ?>
      <input type="hidden" value="<?php echo $presetation_tool[0]['push_result']; ?>" id="view_result_id_textbox">
      </div>
      <?php 
      $user_permissions=explode(",",$presentations[0]['user_permissions']); if($presentations[0]['Thumbnail_status'] == 1 || in_array($user[0]->Id,$user_permissions)){ ?>
       <div class="thumbnail_btn" style="display: none;"><input type="button" value="Hide Thumb" id="hide_thumb" ></div>
      <?php }else{ ?>
       <div class="thumbnail_btn" style="display: none;"><input type="button" value="Show Thumb" id="hide_thumb" ></div>
      <?php } ?>
      <?php if(in_array($user[0]->Id,$user_permissions)){ ?>
      <div class="sp-thumbnails my-sp-thumbnails">
        <?php foreach ($final_array as $key => $value) { ?>
          <div class="sp-thumbnail">
           <?php if(!is_array($value)){ ?>  
          <div class="slider_title_div">Slide</div>   
          <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/user_files/<?php echo $value; ?>&w=110&h=110"/>
          <?php }else{ ?>
            <div class="slider_title_div">Poll</div>
            <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/images/poll_default_img.png&w=110&h=110"/>
            <?php } ?>
            </div>
        <?php } ?>
      </div>
      <?php } ?>
  </div>
</div>
<?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
if(in_array($user[0]->Id,$user_permissions)){ ?>      
<div class="presenter_mode_menu_tool" id="presenter_mode_menu_tool_div">
 <a href="javascript:void(0);" id="preview_mode_btn_link" <?php if(!empty($presetation_tool[0]['push_images'])){ ?> style="display:none;" <?php } ?>><i style="font-size:60px;" class="fa fa-eye"></i> <br/><span>Preview Mode</span></a>
 <a href="javascript:void(0);" id="live_mode_btn_link" <?php if(empty($presetation_tool[0]['push_images'])){ ?> style="display:none;" <?php } ?>><i style="font-size:60px;" class="fa fa-eye"></i> <br/><span>Live Mode</span></a>
</div>
<?php } ?>
<?php } else { ?>
  <h1 style="text-align:center;">Presentation is end.</h1>
<?php } ?>
<?php if($presentations[0]['Thumbnail_status'] == '0' && !in_array($user[0]->Id,$user_permissions)) { ?>
<style type="text/css">
.sp-thumbnails-container
  {
    display: none;
  }
</style>
<?php } ?>
<script type="text/javascript">
 jQuery(document).ready(function()
     {
          $('body').attr('class',"sidebar-close");
          jQuery("#hide_thumb").on('click',function() 
          {
              jQuery(".sp-thumbnails-container").toggle();
              if(jQuery("#hide_thumb").val()=='Hide Thumb')
              {
                jQuery("#hide_thumb").val('Show Thumb');
              }
              else
              {
                jQuery("#hide_thumb").val('Hide Thumb');
              }       
          });
     });
</script>

<script id="siderTemplate" type="text/x-jQuery-tmpl">
    <div class="sp-slide">
      {{if img}}
        <img class="sp-image" src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-src="<?php echo base_url(); ?>assets/user_files/${img}"
            data-small="<?php echo base_url(); ?>assets/user_files/${img}"
            data-medium="<?php echo base_url(); ?>assets/user_files/${img}"
            data-large="<?php echo base_url(); ?>assets/user_files/${img}"
            data-retina="<?php echo base_url(); ?>assets/user_files/${img}" />
      {{else}}
      <div class="sp-image">{{html div}}</div>
      {{/if}}
    </div>
</script>
<script id="siderTemplate1" type="text/x-jQuery-tmpl">
  <div class="sp-thumbnail">
  {{if img}}
    <div class="slider_title_div">Slide</div>
    <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/user_files/${img}&w=110&h=110"/>
  {{else}}
  <div class="slider_title_div">Poll</div>
  <img class="" src="<?php echo base_url(); ?>timthumb.php?src=<?php echo base_url(); ?>assets/images/${div}&w=110&h=110"/>
  {{/if}}  
  </div>
</script>
<script type="text/javascript">
  jQuery( document ).ready(function( $ ) {
    var imgheight = $(window).height()-175;
    jQuery("img.sp-image").css("max-height",imgheight);
  });
</script>
<style type="text/css">
.list .checked 
{
  background-color: #333;
}

.list .icheckbox_line-blue:hover 
{
  background-color: #333;
}
</style>
 <?php $user_permissions=explode(",",$presentations[0]['user_permissions']);
      if(in_array($user[0]->Id,$user_permissions)){ ?>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/test/libs/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/dist/jquery.panzoom.js"></script>
<script>
$(window).load(function(){ 
  $('.sp-selected').panzoom({
    $zoomIn: $(".zoom-in"),
    $zoomOut: $(".zoom-out"),
    disablePan: true,
  });
  $('.sp-slides').css("overflow","visible");
  $('.sp-slide').each( function( index, listItem ) {
    $(this).css("overflow","visible");
  });
});
</script>
<?php } ?>

