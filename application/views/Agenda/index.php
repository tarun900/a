<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
$isMobile = preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
                    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
                    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] ); ?>
<div class="row margin-left-10">
    <?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
         
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data,base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('1', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if($image_array!='') { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id'] ?>")'> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } }  } ?>
 </div>
</div>
<?php } ?>
<div class="row" style="text-align: center;"><h3>Main Agenda</h3></div>
<div>
  <div class="input-group search_box_user">
    <i class="fa fa-search search_user_icon"></i>    
    <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
  </div>
</div>
<div id="view-agenda" style="display:none">
    <a style="margin-right:1%;margin-bottom:10px;" class="btn btn-green pull-right" href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/view_user_agenda/<?php echo $acid; ?>">View My Agenda</a>     
</div>
<?php if(!empty($user) && $event_templates[0]['on_pending_agenda']=='1'){ ?>
<div id="pending_agenda">
  <a style="margin-right:1%;margin-bottom:10px;" class="btn btn-yellow pull-right" href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/view_user_pending_agenda">My Pending Agenda</a> 
</div>
<?php } ?>
<div class="agenda_content agenda_content-new">
  <div class="panel panel-white panel-white-new">
    <div class="tabbable panel-green">
      <div id="alldiv">
        <?php if(!empty($agenda)) { ?>
        <ul id="myTab6" class="nav nav-tabs">
          <li class="active"> <a href="#myTab6_example1" id="tab_sort_by_time" data-toggle="tab"> Sort by Time </a> </li>
          <li> <a href="#myTab6_example2" id="tab_sort_by_type" data-toggle="tab"> Sort by Track </a> </li>
        </ul>
        <div class="tab-content">
          <div class="errorHandler alert alert-success no-display" style="display: none;" id="msg">
            <i class="fa fa-remove-sign" id="con"> </i> 
          </div>
          <div class="tab-pane fade in active" id="myTab6_example1">
            <div class="panel-group accordion" id="accordion">
            <?php $couterkey=0; echo ""; foreach ($agenda as $key1 => $value1) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <h5 class="panel-title agenda_title"> <a class="accordion-toggle <?php static $f1=0;if($f1==0) { echo '';$f1++; } else { echo 'collapsed';$f1++;} ?>" data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo $couterkey; ?>"> <i class="icon-arrow"></i>
                  <?php $adate = $key1; $a =  strtotime($adate);
                    if($event_templates[0]['show_session_by_time']=='1'){
                      if($time_format[0]['format_time']=='0')
                      {
                          echo date("h:i A",$a);
                      }
                      else
                      {     
                           echo date("H:i",$a);
                      }
                    }
                    else
                    {
                      if($event_templates[0]['date_format']=='1')
                      {
                        echo date("m/d/Y",$a);
                      }
                      else
                      {
                        echo date("d/m/Y",$a);
                      }
                      //echo date("l, M jS, Y",$a);
                    } ?>
                  </a></h5>
                </div>
                <div id="accordion<?php echo $couterkey; ?>" class="collapse <?php static $f=0;if($f==0) { echo 'in';$f++; } ?> ">
                  <?php $couterkey++;  $checkedval=  explode(',',$user_agenda_list[0]['agenda_id']); sort($checkedval);
                  foreach ($value1 as $key => $value) {  ?>
                  <div class="col-sm-12 agendalistdiv agenda-display-table" style="border-bottom: 2px solid; ">
                    <div class=" col-md-10 col-sm-9 col-xs-7 agenda-table-cell">
                      <div class="pull-left">
                        <h3><a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],50,"<br />",true);?></a></h3>
                        <?php $asdate = $value['Start_time']; $aedate = $value['End_time'];
                        if($time_format[0]['format_time']=='0')
                        {
                          $sdate = date('h:i A', strtotime($asdate));
                          $edate = date('h:i A', strtotime($aedate));
                        }
                        else
                        {     
                         $sdate=date("H:i", strtotime($asdate));
                         $edate = date('H:i', strtotime($aedate));
                        } echo '<span class="time_span">'.$sdate.' - '.$edate.'</span>'; ?>
                        <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ 
                        echo !empty($value['custom_speaker_name']) ? '<span class="custom_speaker_name">'.ucfirst(wordwrap($value['custom_speaker_name'],50,"<br/>",true)).'</span>' : '';  
                        } if($event_templates[0]['show_agenda_location_column']=='1'){
                          echo !empty($value['custom_location']) ? '<span class="custom_location">'.ucfirst(wordwrap($value['custom_location'],50,"<br/>",true)).'</span>' : '' ;
                        } if($event_templates[0]['show_agenda_place_left_column']=='1'){ 
                          echo '<span class="Place_left">'.$value['Place_left'].'</span>';
                        }  ?>
                      </div>
                    </div>
                    <?php if(!empty($value['session_image'])){ ?>
                    <div class="col-md-2 col-sm-3 col-xs-5 agenda-table-cell">
                      <div class="pull-right">
                        <img width="100%" src="<?php echo base_url().'assets/user_files/'.$value['session_image']; ?>">
                      </div>
                    </div>
                    <?php } ?>  
                  </div>
                  <?php } ?>
                </div>
              </div>  
            <?php } ?>
            </div>
          </div>
          <div class="tab-pane fade in" id="myTab6_example2">
            <div class="panel-group accordion" id="accordion_by_type">  
            <?php 
            if($event_templates[0]['Id'] == '776')
            {
              $sort_order = array('Registration','Main Session','Track 1 - FINANCIAL','Track 2 - HR','Track 3 - CLINICAL','Track 4 - QUALITY','Networking');
              foreach ($sort_order as $key)
              {
                $sorted[$key] = $agenda_list[$key];
              }
              $agenda_list = $sorted;
            }
            $typef=count($agenda); foreach ($agenda_list as $key2 => $value2) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <?php $key3 = str_replace(" ", "_", $key2); ?>
                  <h5 class="panel-title"> <a class="accordion-toggle <?php static $m=0;if($m==0) { echo '';$m++; } else { echo 'collapsed';$m++;} ?>" data-toggle="collapse" data-parent="#accordion_by_type" href="#accordion<?php echo $typef; ?>"> <i class="icon-arrow"></i>
                  <?php echo $key2; ?>
                  </a></h5>
                </div>
                <div id="accordion<?php echo $typef; ?>" class="collapse <?php static $n=0;if($n==0) { echo 'in';$n++; } ?>">
                  <?php $typef++; foreach ($value2 as $key => $value) { ?>
                    <div class="col-sm-12 agendalistdiv agenda-display-table" style="border-bottom: 2px solid; ">
                      <div class=" col-md-10 col-sm-9 col-xs-7 agenda-table-cell">
                        <div class="pull-left">
                          <h3><a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],50,"<br/>",true); ?></a></h3>
                          <?php $asdate = $value['Start_time']; $aedate = $value['End_time'];
                          if($time_format[0]['format_time']=='0')
                          {
                            $sdate = date('h:i A', strtotime($asdate));
                            $edate = date('h:i A', strtotime($aedate));
                          }
                          else
                          {
                            $sdate = date("H:i", strtotime($asdate));
                            $edate = date("H:i", strtotime($aedate));
                          } echo '<span class="time_span">'.$sdate.' - '.$edate.'</span>'; ?>
                          <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ 
                          echo !empty($value['custom_speaker_name']) ? '<span class="custom_speaker_name">'.ucfirst(wordwrap($value['custom_speaker_name'],50,"<br/>",true)).'</span>' : '';  
                          } if($event_templates[0]['show_agenda_location_column']=='1'){
                            echo !empty($value['custom_location']) ? '<span class="custom_location">'.ucfirst(wordwrap($value['custom_location'],50,"<br/>",true)).'</span>' : '' ;
                          } if($event_templates[0]['show_agenda_place_left_column']=='1'){ 
                            echo '<span class="Place_left">'.$value['Place_left'].'</span>';
                          }  ?>
                        </div>
                      </div>
                      <?php if(!empty($value['session_image'])){ ?>
                      <div class="col-md-2 col-sm-3 col-xs-5 agenda-table-cell">
                        <div class="pull-right">
                          <img width="100%" src="<?php echo base_url().'assets/user_files/'.$value['session_image']; ?>">
                        </div>
                      </div>
                      <?php } ?>  
                    </div>
                  <?php } ?>
                </div>
              </div>  
            <?php } ?>
            </div>
          </div>  
        </div>    
        <?php } else { ?> 
        <div class="tab-content">
         <span>No Agenda available for this Category.</span> </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {   
  if(location.hash){
    var data=location.hash.split("#accordion");
    var cou="<?php echo count($agenda); ?>";
    if(data[1]>=cou)
    {
      jQuery(".accordion [id^='accordion']").removeClass('in');
      jQuery(".accordion [id^='accordion']").parent().parent().find('.accordion-toggle').addClass('collapsed');
      $('#tab_sort_by_time').parent().removeClass('active');
      $('#tab_sort_by_type').parent().addClass('active');
      $('#myTab6_example1').removeClass('in active');
      $('#myTab6_example2').addClass('in active');
      jQuery(location.hash).addClass('in'); 
      jQuery(location.hash).parent().find('.accordion-toggle').removeClass('collapsed');
    }
  }
});
    jQuery(document).ready(function()
     {
      jQuery('a').click(function(event) {
         if (event.target.hash) {
          window.history.pushState('obj', 'newtitle',"<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+event.target.hash);
        }
      });
       <?php if(count($user_agenda_list)>0 || count($metting)>0) { ?>
         $('#view-agenda').show();
       <?php } ?>
     });
    jQuery("#search_user_content").keyup(function()
    {
      $("#accordion [id^='accordion']").removeClass('in');
      $("#accordion [id^='accordion']").parent().find('.accordion-toggle').removeClass('collapsed');
      jQuery("#accordion h3").each(function( index ) 
      {
        var str=jQuery(this).find('.user_container_name').text();
        if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
        {
          var str1=jQuery.trim(jQuery(this).find('.user_container_name').text().toLowerCase());
          var content=jQuery.trim(jQuery("#search_user_content").val().toLowerCase());
          if(content!=null)
          {
            if(str1.indexOf(content) >= 0)
            {
              jQuery(this).parent().parent().parent().show();
              if(content.length!=0){
                jQuery(this).parent().parent().parent().parent().addClass('in');
                jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').removeClass('collapsed');
              }
              else
              {
                jQuery("#accordion [id^='accordion']").removeClass('in');
                jQuery('#accordion0').addClass('in');
                jQuery('#accordion0').removeAttr('style');
                jQuery("#accordion [id^='accordion']").parent().find('.accordion-toggle').addClass('collapsed');
                jQuery('#accordion0').parent().find('.accordion-toggle').removeClass('collapsed');
                jQuery('#accordion h3').parent().parent().parent().show();
                return false;
              }
            }
            else
            {
              jQuery(this).parent().parent().parent().hide();
              if(jQuery(this).parent().parent().parent().parent().hasClass('in')){}else{
                jQuery(this).parent().parent().parent().parent().removeClass('in');
                jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').addClass('collapsed');
              }
            }
          }
        }
      }); 
      $("#accordion_by_type [id^='accordion']").removeClass('in');
      $("#accordion_by_type [id^='accordion']").parent().find('.accordion-toggle').removeClass('collapsed');
      jQuery("#accordion_by_type h3").each(function( index ) 
      {
        var str=jQuery(this).find('.user_container_name').text();
        if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
        {
          var str1=jQuery.trim(jQuery(this).find('.user_container_name').text().toLowerCase());
          var content=jQuery.trim(jQuery("#search_user_content").val().toLowerCase());
          if(content!=null)
          {
            if(str1.indexOf(content) >= 0)
            {
              jQuery(this).parent().parent().parent().show();
              if(content.length!=0){
                jQuery(this).parent().parent().parent().parent().addClass('in');
                jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').removeClass('collapsed');
              }
              else
              {
                var cou="<?php echo count($agenda); ?>";
                jQuery("#accordion_by_type [id^='accordion']").removeClass('in');
                jQuery('#accordion'+cou).addClass('in');
                jQuery('#accordion'+cou).removeAttr('style');
                jQuery("#accordion_by_type [id^='accordion']").parent().parent().find('.accordion-toggle').addClass('collapsed');
                jQuery('#accordion'+cou).parent().find('.accordion-toggle').removeClass('collapsed');
                jQuery('#accordion_by_type h3').parent().parent().parent().show();
                return false;
              }
            }
            else
            {
              jQuery(this).parent().parent().parent().hide();
              if(jQuery(this).parent().parent().parent().parent().hasClass('in')){}else{
                jQuery(this).parent().parent().parent().parent().removeClass('in');
                jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').addClass('collapsed');
              }
            }
          }
        }
      }); 
    });

 var checkval = $('#checkbox_values').val();
 var allVals = checkval.split(',');
    function updatecheckbox(a)
    {
        var curr_val=$(this).val();
       
        var value_data = curr_val.split('_');
      
       if (jQuery.inArray(value_data[3], allVals) != "-1")
        {
           
            var removeItem = value_data[3];
            allVals = jQuery.grep(allVals, function(value) {
                return value != removeItem;
            });
        }
        else
        {
            allVals.push(value_data[3]);
           
        }
        $('#checkbox_values').val(allVals);
        $("#msg").val('');
        $.ajax({
            url: "<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/user_agenda_update",
                        data: 'checkbox_values=' + $("#alldiv #checkbox_values").val(),
                        type: "POST",
                        async: false,
                        success: function(result)
                        {

                           $('#view-agenda').show();
                          
                           if(value_data[0]=='save')
                           {
                             $("#msg").show();
                             $("#con").html("Session is Successfully Saved");
                             $("#save_btn_time_"+value_data[3]).html("Delete");
                             //$("#delete_btn_time_"+value_data[3]).html("Delete");
                             $("#saved_btn_time_"+value_data[3]).css("display","block");
                             //$("#delete_btn_time_"+value_data[3]).html("Saved");
                             document.getElementById("save_btn_time_"+value_data[3]).id=("delete_btn_time_"+value_data[3]);
                             document.getElementById("delete_btn_time_"+value_data[3]).value=("delete_btn_time_"+value_data[3]);
                             $("#save_btn_type_"+value_data[3]).html("Delete");
                             //$("#delete_btn_type_"+value_data[3]).html("Delete");
                             
                             $("#saved_btn_type_"+value_data[3]).css("display","block");
                             document.getElementById("save_btn_type_"+value_data[3]).id=("delete_btn_type_"+value_data[3]);
                             document.getElementById("delete_btn_type_"+value_data[3]).value=("delete_btn_type_"+value_data[3]);
                           }
                           else if(value_data[0]=='delete')
                           {
                            
                             $("#msg").show();
                             $("#con").html("Session is Successfully Deleted");
                             $("#save_btn_time_"+value_data[3]).html("Save Session");
                             $("#delete_btn_time_"+value_data[3]).html("Save Session");
                             $("#saved_btn_time_"+value_data[3]).css("display","none");
                             document.getElementById("delete_btn_time_"+value_data[3]).id =("save_btn_time_"+value_data[3]);
                             document.getElementById("save_btn_time_"+value_data[3]).value =("save_btn_time_"+value_data[3]);
                             $("#save_btn_type_"+value_data[3]).html("Save Session");
                             $("#delete_btn_type_"+value_data[3]).html("Save Session");
                             $("#saved_btn_type_"+value_data[3]).css("display","none");
                             document.getElementById("delete_btn_type_"+value_data[3]).id=("save_btn_type_"+value_data[3]);
                             document.getElementById("save_btn_type_"+value_data[3]).value=("save_btn_type_"+value_data[3]);
                           }
                         
                           if(result!='')
                           {
                              $('#view-agenda').show();
                             
                           }
                           else
                           {
                              $('#view-agenda').hide();
                           }
                        }
                    });
                    return allVals;
      }
      $(function() {
                     
              $('#alldiv button').click(updatecheckbox);
              updatecheckbox();

      });
             
      
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script id="autonotifyTemplate" type="text/x-jQuery-tmpl">
     ${( $data.symbol = '' ),''}
    
     {{if Ispublic=='1'}}
         ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/publicmsg' ),''}
     {{/if}}
               
     {{if Ispublic=='0'}}
         ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/privatemsg' ),''}
     {{/if}}
               
     <li class="unread">
           <a href="${symbol}" class="unread">
                <div class="container_msg_notify clearfix">
                     <div class="thread-image">
                          <img src="${Logo}" alt="">
                     </div>
                     <div class="thread-content">
                          <span class="author">${Firstname} ${Lastname}</span>
                          <span class="preview">
                          ${Message}</span>
                     </div>
                </div>
           </a>
      </li>
</script>
<style type="text/css">
.agenda-display-table {
  display: table;
  width: 100%;
  padding: 20px 0;
}
.agenda-display-table .agenda-table-cell {
  display: table-cell;
  float: none;
  vertical-align: middle;
}
.agenda-display-table h3 {
  margin-top: 0;
}
</style>