<?php 
$isMobile = preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
                    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
                    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );
$acc_name=$this->session->userdata('acc_name');
$event=$this->uri->segment(3);
$user = $this->session->userdata('current_user');
$agenda_id=  explode(',',$user_agenda_list[0]['agenda_id']);
$pending_agenda_id=explode(',',$user_agenda_list[0]['pending_agenda_id']);
$checkedval=array_merge($agenda_id,$pending_agenda_id);
sort($checkedval);
/*date_default_timezone_set("UTC");
$cdate=date('Y-m-d H:i:s');
if(!empty($agenda_value[0]['checking_datetime'])){
   if(!empty($event_templates[0]['Event_show_time_zone']))
   {
    if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
    { 
      $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
      $intoffset=$arr[1]*3600;
      $intNew = abs($intoffset);
      $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
    }
    if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
    {
      $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
      $intoffset=$arr[1]*3600;
      $intNew = abs($intoffset);
      $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
    }
  }
}*/
if($isMobile==1){
//echo "<pre>";print_r($user_agenda_list);die;
  } ?>
  <div class="errorHandler alert alert-success no-display" style="display: none;" id="msg">
  <i class="fa fa-remove-sign" id="con"> </i> 
  </div>
  <?php if($event_templates[0]['on_pending_agenda']=='1'){ ?>    
  <input type="hidden" name="checkbox_values" value="<?php echo $user_agenda_list[0]['pending_agenda_id']; ?>" id="checkbox_values">
  <?php }else{ ?>
  <input type="hidden" name="checkbox_values" value="<?php echo $user_agenda_list[0]['agenda_id']; ?>" id="checkbox_values">
  <?php } ?>
<div class="agenda_content agenda_content-new agenda_inner">

  <div class="panel panel-white box-effet">
   <div class="agenda_inner_wrap left">
   <div class="col-sm-12" style="padding: 0px;"><h3 class="left"> <?php echo ucfirst($agenda_value[0]['Heading']); ?></h3></div>

    <h4>
    <?php if($event_templates[0]['show_session_by_time']!='1'){ ?>
     Date : <?php $a =  strtotime($agenda_value[0]['Start_date']);
                      //echo date("jS M Y",$a);
                      if($event_templates[0]['date_format']=='1')
                      {
                        echo date("m/d/Y",$a);
                      }
                      else
                      {
                        echo date("d/m/Y",$a);
                      } ?>
                      <br> 
    <?php }  ?>
          Start Time :
     <?php 

                          if($time_format[0]['format_time']=='0')
                          {
                              $sdate = date('h:i A', strtotime($agenda_value[0]['Start_time']))." (".$event_templates[0]['Event_time_zone'].")";
                              $edate = date('h:i A', strtotime($$agenda_value[0]['End_time']))." (".$event_templates[0]['Event_time_zone'].")";
                          }
                          else
                          {
                            $sdate = date('H:i', strtotime($agenda_value[0]['Start_time']))." (".$event_templates[0]['Event_time_zone'].")";
                            $edate = date('H:i', strtotime($$agenda_value[0]['End_time']))." (".$event_templates[0]['Event_time_zone'].")" ;
                          }


    echo $sdate; ?>
     <br> End Time : <?php if($time_format[0]['format_time']=='0')
                          {
                            echo  date('h:i A', strtotime($agenda_value[0]['End_time']))." (".$event_templates[0]['Event_time_zone'].")"; 
                          }
                          else
                          {
                            echo  date('H:i ', strtotime($agenda_value[0]['End_time']))." (".$event_templates[0]['Event_time_zone'].")"; 
                            
                          }
                         if(!empty($agenda_value[0]['custom_speaker_name'])){
                        ?>
      <br> Speaker : <?php echo ucfirst($agenda_value[0]['custom_speaker_name']);  ?>       
        <?php } if(!empty($agenda_value[0]['custom_location'])){ ?>  
        <br> Location : <?php echo ucfirst($agenda_value[0]['custom_location']); } ?>       
                        </h4>
       <?php 

            if(!empty($user))
                {
                if($agenda_value[0]['show_checking_in']=='1'){ 
                $chekin=explode(",",$user_agenda_list[0]['check_in_agenda_id']); 
                if(in_array($agenda_value[0]['Id'],$chekin))
                {
                  $calss="btn btn-success pull-center";
                  ?>
                  <input type="hidden" value="1" id="is_session_checkin"/>
                  <?php 
                }
                else
                {
                  ?>
                  <input type="hidden" value="0" id="is_session_checkin"/>
                  <?php
                  $calss="btn btn-green pull-center"; 
                }
                ?>
                <a <?php if(count($prv_rating)>0 && in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="margin-bottom:10px;color: #ffffff;" <?php }else{ ?> style="display:none;" <?php } ?>  class="<?php echo $calss; ?>" name="Check_In" id="check_btn_<?php echo $agenda_value[0]['Id']; ?>" value="check_btn_<?php echo $agenda_value[0]['Id'] ?>" onclick="updatecheckin();">
                Check In
              </a>
              
                <a id="prev_session_rating_btn" <?php if(count($prv_rating)<=0 && in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="margin-bottom:10px;color: #ffffff;" <?php }else{ ?> style="display:none;" <?php } ?> class="btn btn-green pull-center" data-toggle="modal" data-target="#rating_model">Check In</a>
              <?php }
            if(!in_array($agenda_value[0]['Id'],$checkedval)) { 
                $placesleft=$agenda_value[0]['Maximum_People']-count($totalsave);
                if($placesleft > 0 || empty($agenda_value[0]['Maximum_People'])){ ?>
              <button type="button" style="margin-bottom:10px;" class="btn btn-green pull-center" name="save" id="save_btn_type_<?php echo $agenda_value[0]['Id']; ?>" value="save_btn_type_<?php echo $agenda_value[0]['Id'] ?>" onclick="updatecheckbox()">
              Save this session to your personal agenda 
              </button>
              <?php }else{ ?>
              <a href="<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/view_msg/'.$agenda_value[0]['Id'].'/1'; ?>" style="margin-bottom:10px;color: #ffffff;" class="btn btn-green pull-center">
                Save this session to your personal agenda 
              </a>
              <?php } } } ?>
              <button type="button" <?php  if(!in_array($agenda_value[0]['Id'],$pending_agenda_id)){ ?>style="display:none;" <?php }else{ ?>  style="margin-right:1%;margin-bottom:10px;"<?php } ?> class="btn btn-green" name="delete" id="delete_btn_type_<?php echo $agenda_value[0]['Id']; ?>" value="delete_btn_type_<?php echo $agenda_value[0]['Id'] ?>" onclick="updatecheckbox()">
              Delete
              </button>
                <div class="agenda_inner_wrap">
                  <a type="button" <?php if(in_array($agenda_value[0]['Id'],$agenda_id)){ ?> style="margin-right:1%;margin-bottom:10px;color: #ffffff;" <?php }else{ ?> style="margin-right:1%;margin-bottom:10px;color: #ffffff;display: none;" <?php } ?> class="btn btn-green pull-right" name="remove" id="remove_btn_type_<?php echo $agenda_value[0]['Id']; ?>" value="remove_btn_type_<?php echo $agenda_value[0]['Id'] ?>" onclick="removesaveagenda();">
                  Remove from My Agenda
                  </a>
                </div>
                <?php if(!empty($agenda_value[0]['checking_datetime']) && $category_data[0]['show_check_in_time']=='0'){ ?>
                <div>
                   Check In Time:  <?php if($event_templates[0]['date_format']=='1')
                                          {
                                            echo date("m/d/Y h:i:s",strtotime($agenda_value[0]['checking_datetime']));
                                          }
                                          else
                                          {
                                            echo date("d/m/Y h:i:s",strtotime($agenda_value[0]['checking_datetime']));
                                          } ?>
                </div>
                <?php } if($agenda_value[0]['show_places_remaining']=='1' && !empty($agenda_value[0]['Maximum_People'])){ ?>
                <div id="places_left_div" class="agenda_inner_wrap" style="margin-right:1%;"><h3>Places Left : <?php echo $agenda_value[0]['Maximum_People']-count($totalsave); ?></h3>
                </div>
                <?php } ?>
                <?php if(!empty($agenda_value[0]['qasession_id'])){ ?>
                <a href="<?php echo base_url().'QA/'.$acc_name.'/'.$event.'/view/'.$agenda_value[0]['qasession_id']; ?>" class="btn btn-green btn-clock" style="color: white;">
                Ask a Question
                </a>
                <?php } ?>
   </div>
   <?php if(!empty($agenda_value[0]['session_image'])){ ?>
   <div class="pull-right">
     <img width="200" height="200" style="margin-right: 12px;margin-top: 20px;" src="<?php echo base_url().'assets/user_files/'.$agenda_value[0]['session_image']; ?>">
   </div>
   <?php } ?>
   <?php if($agenda_value[0]['show_rating']=='1'){ ?>
    <div class="clear_row_star_center"></div>
    <div class="row star-center" style="text-align:center;">
      <fieldset id='session_rating_field' class="rating">
          <input class="stars" <?php if($rating_data[0]['rating']=='5'){ ?> checked="checked" <?php } ?> type="radio" id="star5" name="rating" value="5" />
          <label class = "full" for="star5" title="Awesome - 5 stars"></label>
          <input class="stars" <?php if($rating_data[0]['rating']=='4'){ ?> checked="checked" <?php } ?> type="radio" id="star4" name="rating" value="4" />
          <label class = "full" for="star4" title="Pretty good - 4 stars"></label>
          <input class="stars" <?php if($rating_data[0]['rating']=='3'){ ?> checked="checked" <?php } ?> type="radio" id="star3" name="rating" value="3" />
          <label class = "full" for="star3" title="Meh - 3 stars"></label>
          <input class="stars" <?php if($rating_data[0]['rating']=='2'){ ?> checked="checked" <?php } ?> type="radio" id="star2" name="rating" value="2" />
          <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
          <input class="stars" <?php if($rating_data[0]['rating']=='1'){ ?> checked="checked" <?php } ?> type="radio" id="star1" name="rating" value="1" />
          <label class = "full" for="star1" title="Sucks big time - 1 star"></label>
      </fieldset>
      <h3>Rate This Session</h3>
    </div>
    <?php } ?>
   <input type="hidden" name="places_left_counter" id="places_left_counter" value="<?php echo $agenda_value[0]['Maximum_People']-count($totalsave); ?>">
   <?php if($agenda_value[0]['allow_comments']=='1' && count($agenda_comment) <= 0){ ?>
   <div class="col-sm-12" id="comment_box_div">
      <div class="form-group">
        <textarea name="comment_box_textarea" class="form-control name_group required" style="width: 100%;height: 200px;" id="comment_box_textarea" placeholder="Leave your comments here…"></textarea>
        <span for="comment_box_textarea" id="comment_box_textarea_error_span" class="help-block" style="display: none;">This field is required.</span>
      </div> 
      <div class="errorHandler alert alert-danger no-display" style="display: none;" id="error_msg">
      <i class="fa fa-remove-sign" id="con"> </i> 
      </div>
     <div class="col-sm-2" style="padding:15px 0;">
       <a href="javascript:void(0);" id="Comment_submit_button" name="Comment_submit_button" class="btn btn-green btn-block">
       Send <i class="fa fa-arrow-circle-right"></i>
       </a>
     </div>
   </div>
   <div class="errorHandler alert alert-success no-display" style="display: none;" id="success_msg">
      <i class="fa fa-remove-sign" id="con"> </i> 
      </div> 
   <?php } ?>
   
    <div class="agenda_inner_wrap agenda-user-box" style="border-bottom:0px;">
    <?php if(!empty($agenda_value[0]['description'])) { 
      $user = $this->session->userdata('current_user');
      if($user[0]->Role_id==4)
      {
        $extra=json_decode($custom[0]['extra_column'],true);
        foreach ($extra as $key => $value) {
          $keyword="{".str_replace(' ', '', $key)."}";
          if(stripos(strip_tags($agenda_value[0]['description'],$keyword)) !== false)
          {
            $agenda_value[0]['description']=str_ireplace($keyword, $value,$agenda_value[0]['description']);
          }
        }
      }
      ?>
      <?php echo $agenda_value[0]['description']; ?>
    <?php } ?>
   </div>
   
   <div class="agenda-user-box">
   <ul>
  <?php
  $speaker=$agenda_value[0]['speaker'];
  for($i=0;$i<count($speaker);$i++){
   if(!empty($speaker[$i]))
   { 
      ?>
      <li class="agenda_inner_wrap">
      <div class="agenda_inner-img user-files-box"> 
      <?php $url= base_url()."assets/user_files/".$speaker[$i]['Logo'];
      if(file_exists("assets/user_files/".$speaker[$i]['Logo']) && $speaker[$i]['Logo']!="") {  ?>
      <img class="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $speaker[$i]['Logo'];?>" >
        <?php } else { ?>

          <img class="" src="<?php echo base_url(); ?>assets/images/anonymous_speaker.jpg">
        <?php  }  ?>
       </div>
    <?php 
        $a=base_url().'Speakers/'.$acc_name.'/'.$event.'/View/'.$speaker[$i]['Id'];
        if($speaker[$i]['Firstname']!="" && $speaker[$i]['Lastname']!="")
        {
          echo ucfirst($speaker[$i]['Firstname']." ".$speaker[$i]['Lastname']);
          echo "<br>"."<a href='".$a."'>"."View Profile</a>";
    
        }
      ?>
      </li>
<?php } }
 if($agenda_value[0]['presentation_id']!='')
   { ?>
      <li class="agenda_inner_wrap">
      <div class="agenda_inner-img"> <img class="" src="<?php echo base_url(); ?>assets/images/icon/powerpoint-icon.png" alt="" > </div>
    <?php 
        $a=base_url().'Presentation/'.$acc_name.'/'.$event.'/View_presentation/'.$agenda_value[0]['presentation_id'];
       
         
          echo "<br>"."<a href='".$a."'>"."View Polls &amp; Slides</a>";
    
  
      ?>
      </li>
<?php } if(!empty($agenda_value[0]['survey_id'])){ ?>  
  <li class="agenda_inner_wrap">
    <div class="agenda_inner-img" style="background-color: <?php echo $event_templates[0]['Top_background_color']; ?>"> <img class="" src="<?php echo base_url().'demo/assets/images/Survey-Polls_Icon.png'; ?>" alt="" > </div>
    <a href="<?php echo base_url().'Surveys/'.$acc_name.'/'.$event.'/question_index/'.$agenda_value[0]['survey_id']; ?>">
      <span><?php echo $agenda_value[0]['survey_name']; ?></span>
    </a>
  </li>
<?php }
 if($agenda_value[0]['Address_map_id']!='')
   { ?>
      <li class="agenda_inner_wrap">
      <div class="agenda_inner-img"> <img class="" src="<?php echo base_url(); ?>assets/images/icon/map-icon.png" alt="" > </div>
    <?php 
        $a=base_url().'Maps/'.$acc_name.'/'.$event.'/View/'.$agenda_value[0]['Address_map_id'];
       
          echo ucfirst($agenda_value[0]['Map_title']);
          echo "<br>"."<a href='".$a."'>"."View on Map</a>";
    
  
      ?>
      </li>
      <?php } ?>
</ul>
</div>
  
   
   <?php 
 if($agenda_value[0]['document_id']!='')
   { 
      $ext = pathinfo($agenda_value[0]['document_file'], PATHINFO_EXTENSION);
    
                                   if($ext == 'docx' || $ext == 'doc')
                                    {        
                                    $docicon=base_url()."assets/images/icon/docs-file.png";
                                    }
                                    elseif($ext == 'pdf')
                                    {
                                    $docicon=base_url()."assets/images/icon/pdf-icon.png";
                                    }
                                    elseif($ext == 'ppt')
                                    {
                                    $docicon=base_url()."assets/images/icon/ppt-icon.png";
                                    }
                                    elseif($ext == 'odg')
                                    {
                                    $docicon=base_url()."assets/images/icon/ppt-icon.png";
                                    }
                                    elseif($ext == 'txt')
                                    {
                                    $docicon=base_url()."assets/images/icon/txt2.png";
                                    }
                                    else
                                    {
                                    $docicon=base_url()."assets/images/icon/folder-icon.png";
                                    }
                                  
                                  
    
 $a=base_url().'assets/user_documents/'.$agenda_value[0]['document_file'];

    ?>
      <div class="agenda_inner_wrap agenda-inner-folder-box">

     <div class="agenda_inner-img"> <a href="<?php echo $a;?>"><img class="" style="height: auto;" src="<?php echo $docicon;?>" alt="" ></a> </div>
      </div>
<?php } ?>

   <br/><div class="row">
   <?php /*if($agenda_value[0]['Map_title']!=""){ ?>
      <span style="margin-left:1%;margin-bottom:10px;"><i class="fa fa-map-marker"></i> <?php echo $agenda_value[0]['Map_title']; ?></span>
      <?php }*/ ?>
    
      </div>

	   <?php
       /* echo '<div class="map_view">';
        
        if(!empty($agenda_value[0]['Map_address']) && empty($agenda_value[0]['Map_image']))
        {
              echo '<div class="agenda_content notes-content map_view">';
          echo '<div class="panel panel-white">';
          echo '<div class="map_iframe">';
          echo '<iframe src="'.$agenda_value[0]['Map_address'].'" style="width:100%;height:100%;"></iframe>';
          echo '</div>';
          echo '</div>';
              echo '</div>';
        }

        elseif (!empty($agenda_value[0]['Map_image']) && empty($agenda_value[0]['Map_address'])) 
        {
          
          $img = json_decode($agenda_value[0]['Map_image']);
          echo '<div class="map_image">';
          echo '<img style="width:100%;" src="'.base_url().'assets/user_files/'.$img[0].'">';
          echo '</div>';

        }

        elseif (!empty($agenda_value[0]['Map_address']) && !empty($agenda_value[0]['Map_image']) ) 
        {
          echo '<div class="agenda_content notes-content map_view">';
          echo '<div class="panel panel-white box-effect">';
          echo '<div class="map_iframe">';
              echo '<iframe src="'.$agenda_value[0]['Map_address'].'" style="width:100%;height:100%;"></iframe>';
            echo '</div>';
            echo '</div>';
          echo '</div>';
          //echo '</div>';
          $img = json_decode($agenda_value[0]['Map_image']);
          echo '<div class="map_image_iframe box-effect">';
            echo '<img style="width:100%;" src="'.base_url().'assets/user_files/'.$img[0].'">';
          echo '</div>';
          
        }*/
        //echo'</div>';
      ?>
      
 </div>
 </div>
 </div>
 </div>
 <div class="modal fade" id="rating_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="border:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">
          <h3 style="text-align:center;">You must rate:</h3>
          <h3 style="text-align:center;"><?php echo $prev_agenda[0]['Heading']; ?></h3>
          <h4 style="text-align:center;"><?php echo date("h:i A",strtotime($prev_agenda[0]['Start_date'].' '.$prev_agenda[0]['Start_time'])) ?></h4>
          <h3>before  you can Check In  to  this  session</h3>
    <div class="row star-center" style="text-align:center;">
      <fieldset id='prev_rating_session' class="rating">
          <input class="stars" type="radio" id="prev_star5" name="rating" value="5" />
          <label class = "full" for="prev_star5" title="Awesome - 5 stars"></label>
          <input class="stars" type="radio" id="prev_star4" name="rating" value="4" />
          <label class = "full" for="prev_star4" title="Pretty good - 4 stars"></label>
          <input class="stars" type="radio" id="prev_star3" name="rating" value="3" />
          <label class = "full" for="prev_star3" title="Meh - 3 stars"></label>
          <input class="stars" type="radio" id="prev_star2" name="rating" value="2" />
          <label class = "full" for="prev_star2" title="Kinda bad - 2 stars"></label>
          <input class="stars" type="radio" id="prev_star1" name="rating" value="1" />
          <label class = "full" for="prev_star1" title="Sucks big time - 1 star"></label>
      </fieldset>
    </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="error_msg_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="display:none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> 
        </div>
        <div class="modal-body" id="error_msg_body">
          <b>You must Check In before you can rate this session.</b>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" style="border:none;color:blue;" class="btn btn-default" data-dismiss="modal">Close</a>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script>
$(document).ready(function(){
$('header').addClass('ratingpopupshow');
});
var checkval = $('#checkbox_values').val();
 var allVals = checkval.split(',');
    function updatecheckbox()
    {
        var prosess='';
        var curr_val=$(this).val();
        var value_data = curr_val.split('_');
       if (jQuery.inArray(value_data[3], allVals) != "-1")
        {
           
            var removeItem = value_data[3];
            allVals = jQuery.grep(allVals, function(value) {
                return value != removeItem;
            });
            prosess='DELETE';
        }
        else
        {
            allVals.push(value_data[3]);
            prosess='SAVE';
        }
        $('#checkbox_values').val(allVals);
        $("#msg").val('');
        $.ajax({
            url: "<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$event; ?>/user_agenda_update",
                        data: 'checkbox_values=' + $("#checkbox_values").val()+"&agenda_id=<?php echo $agenda_value[0]['Id']; ?>&process="+prosess,
                        type: "POST",
                        async: false,
                        success: function(result)
                        {
                           var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                         if(regex .test(result))
                          {
                            window.location.href=result;
                          }
                          else
                          {
                           if(value_data[0]=='save')
                           {
                            var on_pending="<?php echo $event_templates[0]['on_pending_agenda']; ?>";
                             $("#msg").show();
                             $("#con").html("Session is Successfully Saved");
                            if(on_pending!='1'){
                              $("#remove_btn_type_<?php echo $agenda_value[0]['Id'] ?>").show();
                              var prevsession="<?php echo count($prv_rating); ?>"
                              if(prevsession>0)
                              {
                                $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr("style","margin-bottom:10px;color: #ffffff;");
                                $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").show();
                              }
                              else
                              {
                                $('#prev_session_rating_btn').attr("style","margin-bottom:10px;color: #ffffff;");
                                $("#prev_session_rating_btn").show();
                              }
                            }
                            else
                            {
                              $('#delete_btn_type_'+value_data[3]).show();
                            }
                             // /$("#save_btn_type_"+value_data[3]).html("Delete");
                             $("#save_btn_type_"+value_data[3]).hide();
                             //$("#delete_btn_type_"+value_data[3]).html("Delete");
                             if(on_pending!='1'){
                               var placesleft=parseInt($.trim($('#places_left_counter').val()))-1;
                               $('#places_left_div').html('<h3>Places Left : '+placesleft+'</h3>');
                               $('#places_left_counter').val(placesleft);
                              }
                             $("#saved_btn_type_"+value_data[3]).css("display","none;");
                             document.getElementById("save_btn_type_"+value_data[3]).id=("delete_btn_type_"+value_data[3]);
                             document.getElementById("delete_btn_type_"+value_data[3]).value=("delete_btn_type_"+value_data[3]);
                           }
                           else if(value_data[0]=='delete')
                           {
                             var on_pending="<?php echo $event_templates[0]['on_pending_agenda']; ?>";

                              if(on_pending!='1'){
                                var prevsession="<?php echo count($prv_rating); ?>"
                                if(prevsession>0)
                                {
                                  $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").hide();
                                }
                                else
                                {
                                  $("#prev_session_rating_btn").hide();
                                }
                              }
                             $("#msg").show();
                             $("#con").html("Session is Successfully Deleted");
                             $("#save_btn_type_"+value_data[3]).html("Save Session");
                             $('#delete_btn_type_'+value_data[3]).hide();
                              if(on_pending!='1'){
                             var placesleft=parseInt($.trim($('#places_left_counter').val()))+1;
                               $('#places_left_div').html('<h3>Places Left : '+placesleft+'<h3>');
                               $('#places_left_counter').val(placesleft);
                               $("#delete_btn_type_"+value_data[3]).html("Save Session");
                              }
                             $("#saved_btn_type_"+value_data[3]).css("display","none");
                             document.getElementById("delete_btn_type_"+value_data[3]).id=("save_btn_type_"+value_data[3]);
                             document.getElementById("save_btn_type_"+value_data[3]).value=("save_btn_type_"+value_data[3]);
                           }
                         }
                        }
                    });
                    return allVals;
      }
      $(function() {
        $('button').click(updatecheckbox);
        updatecheckbox();
      });
 function updatecheckin()
  {
    $.ajax({
      url: "<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$event; ?>/user_check_in_update/<?php echo $agenda_value[0]['Id']; ?>",
      success: function(result)
      {
        var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        if(regex .test(result))
        {
          window.location.href=result;
        }
        else
        { if($.trim(result)=="btn btn-success pull-center")
          {
           $('#is_session_checkin').val('1');
          }
          else
          {
            $('#is_session_checkin').val('0');
          }
          $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr('class',result);
        }
      }
    });
  }   
             
       $("#session_rating_field >.stars").click(function () {
        <?php if($event!='dwtcGitecIoTX2017' || $agenda_value[0]['End_date'].' '.$agenda_value[0]['End_time'] <= date('Y-m-d H:i:s')){ ?>
          $.ajax({
            url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/user_rating_save/'.$agenda_value[0]['Id']; ?>",
            type:'post',
            data:"rating="+$(this).val(),
            success:function(result){
              var data=result.split('###');
              if($.trim(data[0])=="error")
              {
                $('#error_msg_body').html('<b>'+data[1]+'</b>');
                  $('#error_msg_model').modal({
                    show: 'false'
                  }); 
              }
              else
              {
                location.reload();
              }
            }
          });
        <?php }else{ ?>
          $('#error_msg_body').html('<b>Please rate after the session is closed.</b>');
          $('#error_msg_model').modal({
             show: 'false'
           }); 
        <?php } ?>
    });  
    $('#prev_rating_session > .stars').click(function(){
      $.ajax({
        url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/user_rating_save/'.$prev_agenda[0]['Id']; ?>",
        type:'post',
        data:"rating="+$(this).val(),
        success:function(result){
          var data=result.split('###');
          if($.trim(data[0])=="error")
          {
            $('#error_msg_body').html('<b>'+data[1]+'</b>');
              $('#error_msg_model').modal({
                show: 'false'
              }); 
          }
          else
          {
            $('#prev_session_rating_btn').hide();
            $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").attr('style','margin-bottom:10px;color: #ffffff;');
            $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").show();
            location.reload();
          }
          $('#rating_model').modal('toggle'); 
        }
      });
    });
    function removesaveagenda()
    {
      $.ajax({
        url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/remove_from_view_my_agenda/'.$agenda_value[0]['Id']; ?>",
        success:function(result){
          if($.trim(result)=="success")
          {
            var prevsession="<?php echo count($prv_rating); ?>"
            if(prevsession>0)
            {
              $("#check_btn_<?php echo $agenda_value[0]['Id']; ?>").hide();
            }
            else
            {
              $("#prev_session_rating_btn").hide();
            }
            $("#msg").show();
            $("#con").html("Remove Session Successfully From View My Agenda");   
            $("#remove_btn_type_<?php echo $agenda_value[0]['Id']; ?>").hide();
          }
        }
      });
    }
    $('#Comment_submit_button').click(function(){
      if($('#comment_box_textarea').val()!="")
      { 
        $('#Comment_submit_button').find('i').attr('class','fa-li fa fa-spinner fa-spin');
        $('#Comment_submit_button').attr('disabled','disabled');
        $.ajax({
          url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$event.'/add_session_user_comment/'.$agenda_value[0]['Id']; ?>",
          type:'post',
          data:"comments="+$('#comment_box_textarea').val(),
          error:function(errormsg){
            $('#error_msg').html("Sothing Went wrong.");
            $('#error_msg').show();
          },
          success:function(result){
            var data=result.split("###");
            if($.trim(data[0])=="Success")
            {
              $('#success_msg').html(data[1]);
              $('#success_msg').show();
              setTimeout(function(){$('#success_msg').hide();}, 5000);
              $('#comment_box_div').remove();
            }
            else
            {
              $('#Comment_submit_button').find('i').attr('class','fa fa-arrow-circle-right');
              $('#Comment_submit_button').removeAttr('disabled');
              $('#error_msg').html(data[1]);
              $('#error_msg').show();
              setTimeout(function(){$('#error_msg').hide();}, 5000);
            }
          }
        });
      }
      else
      {
        $('#comment_box_textarea_error_span').parent().addClass('has-error');
        $('#comment_box_textarea_error_span').show();
        setTimeout(function(){$('#comment_box_textarea_error_span').hide();}, 5000);
      }
    });
    $('#comment_box_textarea').keyup(function(){
      if($('#comment_box_textarea').val()=="")
      {
        $('#comment_box_textarea_error_span').parent().addClass('has-error');
        $('#comment_box_textarea_error_span').show();
        setTimeout(function(){$('#comment_box_textarea_error_span').hide();}, 5000);
      }
      else
      {
        $('#comment_box_textarea_error_span').parent().removeClass('has-error');
        $('#comment_box_textarea_error_span').hide(); 
      }
    });
</script>
<style type="text/css">
  header.ratingpopupshow {   z-index: 0; }
</style>