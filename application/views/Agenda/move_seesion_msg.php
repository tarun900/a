<?php $acc_name=$this->session->userdata('acc_name');
$event=$this->uri->segment(3); ?>
<div class="agenda_content agenda_content-new agenda_inner">
  <div class="panel panel-white box-effet">
		<div class="row" style="margin-left:20px;"><h4>See below for the sessions you have been booked onto</h4></div>
		<div class="table-responsive">
	        <table class="table table-bordered table-hover" id="sample-table-1">
	            <thead>
	              <tr>
	                <th>Name</th>
	                <th>Time</th>
	                <th>Book</th>
	                <th></th>
	              </tr>
	            </thead>
	            <tbody>
	            	<?php foreach ($msg as $key => $value) { if(!empty($value['Heading']) && !empty($value['Start_time'])){ ?>
	            	<tr>
	            		<td><?php echo $value['Heading']; ?></td>
	            		<td><?php echo $value['Start_time']; ?></td>
	            		<td>
	            		<?php $classnm=""; if($value['book']=="1"){ 
	            			$classnm="label label-sm label-success";
	            			$status="Yes";
	            			}else{
	            			$classnm="label label-sm label-danger";	
	            			$status="No";
	            			} ?>
	            			<span class="<?php echo $classnm; ?>"><?php echo ucfirst($status); ?></span> 
	            		</td>
	            		<td><?php echo $value['resion']; ?></td>
	            	</tr>
	            	<?php } } ?>
	            </tbody>
	        </table>
		</div>
		<div class="col-sm-12">
		<a href="<?php echo base_url().'Agenda/'.$acc_name.'/'.$event; ?>" style="margin-bottom:10px;" class="btn btn-green pull-center" >
            Go back to the agenda 
        </a>
        </div>
	</div>
</div>
