<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
$save_session=explode(",",$user_agenda_list[0]['agenda_id']); ?>
<div class="row margin-left-10">
    <?php
          if(!empty($user)):
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
         
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data,base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('1', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if($image_array!='') { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } }  } ?>
 </div>
</div>
<?php } ?>
<div class="row" style="text-align: center;"><h3>Main Agenda</h3></div>
<div>
  <div class="input-group search_box_user">
    <i class="fa fa-search search_user_icon"></i>    
    <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
  </div>
</div>
<div id="view-agenda" style="display:none">
    <a style="margin-right:1%;margin-bottom:10px;" class="btn btn-green pull-right" href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/view_user_agenda/<?php echo $acid; ?>">View My Agenda</a>     
</div>
<?php if(!empty($user) && $event_templates[0]['on_pending_agenda']=='1'){ ?>
<div id="pending_agenda">
  <a style="margin-right:1%;margin-bottom:10px;" class="btn btn-yellow pull-right" href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/view_user_pending_agenda">My Pending Agenda</a> 
</div>
<?php } ?>
<div class="agenda_content agenda_content-new">
  <div class="panel panel-white panel-white-new">
    <div class="tabbable panel-green">
      <div id="alldiv">
        <?php if(!empty($agenda)) { ?>
        <ul id="myTab6" class="nav nav-tabs">
          <li class="active"> <a href="#myTab6_example1" id="tab_sort_by_time" data-toggle="tab"> Sort by Time </a> </li>
          <li> <a href="#myTab6_example2" id="tab_sort_by_type" data-toggle="tab"> Sort by Track </a> </li>
        </ul>
        <div class="tab-content">
          <div class="errorHandler alert alert-success no-display" style="display: none;" id="msg">
            <i class="fa fa-remove-sign" id="con"> </i> 
          </div>
          <div class="tab-pane fade in active main_agenda_index" id="myTab6_example1">
            <div class="panel-group accordion" id="accordion">
              <?php $couterkey=0; echo ""; foreach ($agenda as $key1 => $value1) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <h5 class="panel-title agenda_title"> 
                    <a class="accordion-toggle <?php static $f1=0;if($f1==0) { echo '';$f1++; } else { echo 'collapsed';$f1++;} ?>" data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo $couterkey; ?>"> <i class="icon-arrow"></i>
                    <?php 
                    $adate = $key1;
                    $a =  strtotime($adate);
                    if($event_templates[0]['show_session_by_time']=='1'){
                      if($time_format[0]['format_time']=='0')
                      {
                      echo date("h:i A",$a);
                      }
                      else
                      {     
                      echo date("H:i",$a);
                      }
                    }
                    else
                    {
                      if($event_templates[0]['date_format']=='1')
                      {
                        echo date("m/d/Y",$a);
                      }
                      else
                      {
                        echo date("d/m/Y",$a);
                      }
                      //echo $event_templates[0]['default_lang']['time_and_date__'.strtolower(date("l",$a))].", ".date("M jS, Y",$a);
                    }
                    ?>
                    </a> 
                  </h5>
                </div>
                <div id="accordion<?php echo $couterkey; ?>" class="collapse <?php static $f=0;if($f==0) { echo 'in';$f++; } ?> ">
                  <div class="table-responsive">
                    <?php $couterkey++; ?>
                      <table class="table table-bordered table-hover" id="sample-table-1">
                      <thead>
                        <tr>
                          <th>Session Name</th>
                          <th>Session Track</th>
                          <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                          <th>Session Speaker</th>
                          <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                          <th>Session Location</th>
                          <?php } if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                          <th>Places Left</th>
                          <?php } ?>
                          <th>Session Save</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($value1 as $key => $value) { ?>
                        <tr>
                          <td class="session_name">
                            <a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],15,"<br />",true); ?>
                            </a>
                          </td>
                          <td>
                            <?php echo ucwords($value['Types']); ?>
                          </td>
                          <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                          <td class="user_container_name">
                            <?php echo !empty($value['custom_speaker_name']) ? ucfirst($value['custom_speaker_name']) : '--'; ?>
                          </td>
                          <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                          <td class="user_container_name session_location">
                            <?php echo !empty($value['custom_location']) ? ucfirst($value['custom_location']) : '--' ; ?>
                          </td>
                          <?php }if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                          <td class="session_place_left">
                            <?php echo ($value['Place_left']) ? $value['Place_left'] : '--'; ?>
                          </td>
                          <?php } ?>
                          <td>
                            <?php if(!empty($user)){ ?>
                              <button type="button" class="btn btn-green btn-block Save_Session_btn_<?php echo $value['Id']; ?>" onclick='savesession("<?php echo $value['Id']; ?>");' style="<?php if(in_array($value['Id'],$save_session)){ ?>display: none;<?php } ?>">
                                Save
                              </button>
                              <button type="button" class="btn btn-red btn-block remove_save_session_btn_<?php echo $value['Id']; ?>" onclick='deletesavesession("<?php echo $value['Id']; ?>");' style="<?php if(!in_array($value['Id'],$save_session)){ ?>display: none;<?php } ?>">
                                Remove
                              </button>
                            <?php } ?>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="tab-pane fade in main_agenda_index" id="myTab6_example2">
            <div class="panel-group accordion" id="accordion_by_type">
              <?php $typef=count($agenda);
              foreach ($agenda_list as $key2 => $value2) { ?>
              <div class="panel panel-white">
                <div class="panel-heading">
                  <?php $key3 = str_replace(" ", "_", $key2); ?>
                  <h5 class="panel-title"> 
                    <a class="accordion-toggle <?php static $m=0;if($m==0) { echo '';$m++; } else { echo 'collapsed';$m++;} ?>" data-toggle="collapse" data-parent="#accordion_by_type" href="#accordion<?php echo $typef; ?>"> <i class="icon-arrow"></i>
                    <?php 
                    echo $key2;
                    ?>
                    </a> 
                  </h5>
                </div>
                <div id="accordion<?php echo $typef; ?>" class="collapse <?php static $n=0;if($n==0) { echo 'in';$n++; } ?>">
                  <div class="table-responsive">
                    <?php $typef++; ?>
                    <table class="table table-bordered table-hover" id="sample-table-1">
                      <thead>
                        <tr>
                          <th>Session Name</th>
                          <th>Session Track</th>
                          <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                          <th>Session Speaker</th>
                          <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                          <th>Session Location</th>
                          <?php } if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                          <th>Places Left</th>
                          <?php } ?>
                          <th>Session Save</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($value2 as $key => $value) { ?>
                        <tr>
                          <td class="session_name">
                            <a href="<?php echo base_url(); ?>Agenda/<?php echo $acc_name.'/'.$Subdomain; ?>/View_agenda/<?php echo $value['Id']; ?>" class="user_container_name"><?php echo wordwrap($value['Heading'],15,"<br />",true); ?>
                            </a>
                          </td>
                          <td>
                            <?php echo ucwords($value['Types']); ?>
                          </td>
                          <?php if($event_templates[0]['show_agenda_speaker_column']=='1'){ ?>
                          <td class="user_container_name">
                            <?php echo !empty($value['custom_speaker_name']) ? ucfirst($value['custom_speaker_name']) : '--'; ?>
                          </td>
                          <?php } if($event_templates[0]['show_agenda_location_column']=='1'){ ?>
                          <td class="user_container_name session_location">
                            <?php echo !empty($value['custom_location']) ? ucfirst($value['custom_location']) : '--' ; ?>
                          </td>
                          <?php }if($event_templates[0]['show_agenda_place_left_column']=='1'){ ?>
                          <td class="session_place_left">
                            <?php echo ($value['Place_left']) ? $value['Place_left'] : '--'; ?>
                          </td>
                          <?php } ?>
                          <td>
                            <?php if(!empty($user)){ ?>
                              <button type="button" class="btn btn-green btn-block Save_Session_btn_<?php echo $value['Id']; ?>" onclick='savesession("<?php echo $value['Id']; ?>");' style="<?php if(in_array($value['Id'],$save_session)){ ?>display: none;<?php } ?>">
                                Save
                              </button>
                              <button type="button" class="btn btn-red btn-block remove_save_session_btn_<?php echo $value['Id']; ?>" onclick='deletesavesession("<?php echo $value['Id']; ?>");' style="<?php if(!in_array($value['Id'],$save_session)){ ?>display: none;<?php } ?>">
                                Remove
                              </button>
                            <?php } ?>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table> 
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php } else { ?> 
        <div class="tab-content">
          <span>No Agenda available for this Category.</span> 
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {   
  if(location.hash){
    var data=location.hash.split("#accordion");
    var cou="<?php echo count($agenda); ?>";
     jQuery(".accordion [id^='accordion']").removeClass('in');
      jQuery(".accordion [id^='accordion']").parent().parent().find('.accordion-toggle').addClass('collapsed');
    if(data[1]>=cou)
    {
      $('#tab_sort_by_time').parent().removeClass('active');
      $('#tab_sort_by_type').parent().addClass('active');
      $('#myTab6_example1').removeClass('in active');
      $('#myTab6_example2').addClass('in active');
    }
    jQuery(location.hash).addClass('in'); 
    jQuery(location.hash).parent().find('.accordion-toggle').removeClass('collapsed');
  }
});
jQuery(document).ready(function(){
  jQuery('a').click(function(event) {
    if (event.target.hash) 
    {
      window.history.pushState('obj', 'newtitle',"<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+event.target.hash);
    }
  });
  <?php if(count($user_agenda_list)>0 || count($metting)>0) { ?>
  $('#view-agenda').show();
  <?php } ?>
});
jQuery("#search_user_content").keyup(function(){
  $("#accordion [id^='accordion']").removeClass('in');
  $("#accordion [id^='accordion']").parent().find('.accordion-toggle').removeClass('collapsed');
  jQuery("#accordion  tr").each(function( index ) {
    var str=jQuery(this).find('.user_container_name').text();
    if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
    {
      var str1=jQuery.trim(jQuery(this).find('.user_container_name').text().toLowerCase());
      var content=jQuery.trim(jQuery("#search_user_content").val().toLowerCase());
      if(content!=null)
      {
        if(str1.indexOf(content) >= 0)
        {
          jQuery(this).show();
          if(content.length!=0){
            jQuery(this).parent().parent().parent().parent().addClass('in');
            jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').removeClass('collapsed');
          }
          else
          {
            jQuery("#accordion [id^='accordion']").removeClass('in');
            jQuery('#accordion0').addClass('in');
            jQuery('#accordion0').removeAttr('style');
            jQuery("#accordion [id^='accordion']").parent().find('.accordion-toggle').addClass('collapsed');
            jQuery('#accordion0').parent().find('.accordion-toggle').removeClass('collapsed');
            jQuery('#accordion tr').show();
            return false;
          }
        }
        else
        {
          jQuery(this).hide();
          if(jQuery(this).parent().parent().parent().parent().hasClass('in')){}else{
            jQuery(this).parent().parent().parent().parent().removeClass('in');
            jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').addClass('collapsed');
          }
        }
      }
    }
  }); 
  $("#accordion_by_type [id^='accordion']").removeClass('in');
  $("#accordion_by_type [id^='accordion']").parent().find('.accordion-toggle').removeClass('collapsed');
  jQuery("#accordion_by_type tr").each(function( index ) {
    var str=jQuery(this).find('.user_container_name').text();
    if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
    {
      var str1=jQuery.trim(jQuery(this).find('.user_container_name').text().toLowerCase());
      var content=jQuery.trim(jQuery("#search_user_content").val().toLowerCase());
      if(content!=null)
      {
        if(str1.indexOf(content) >= 0)
        {
          jQuery(this).show();
          if(content.length!=0)
          {
            jQuery(this).parent().parent().parent().parent().addClass('in');
            jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').removeClass('collapsed');
          }
          else
          {
            var cou="<?php echo count($agenda); ?>";
            jQuery("#accordion_by_type [id^='accordion']").removeClass('in');
            jQuery('#accordion'+cou).addClass('in');
            jQuery('#accordion'+cou).removeAttr('style');
            jQuery("#accordion_by_type [id^='accordion']").parent().parent().find('.accordion-toggle').addClass('collapsed');
            jQuery('#accordion'+cou).parent().find('.accordion-toggle').removeClass('collapsed');
            jQuery('#accordion_by_type tr').show();
            return false;
          }
        }
        else
        {
          jQuery(this).hide();
          if(jQuery(this).parent().parent().parent().parent().hasClass('in')){}else{
            jQuery(this).parent().parent().parent().parent().removeClass('in');
            jQuery(this).parent().parent().parent().parent().parent().find('.accordion-toggle').addClass('collapsed');
          }
        }
      }
    }
  }); 
}); 
function savesession(aid)
{
  $.ajax({
    url:"<?php echo base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/direct_save_user_session/'; ?>"+aid,
    error:function(err){
    },
    success:function(data){
      var result=data.split("###");
      if($.trim(result[0])=="Success")
      {
        if($.trim(result[1])=="btn btn-success")
        {
          var msg="Session is Saved Successfully.";
          var title = 'Save Session';
          $('.Save_Session_btn_'+aid).hide();
          $('.remove_save_session_btn_'+aid).show();
        }
        else
        {
          var msg="Session is Deleted Successfully."; 
          var title = 'Remove Session';
          $('.remove_save_session_btn_'+aid).hide();
          $('.Save_Session_btn_'+aid).show();
        }
        var shortCutFunction ='success';
        var $showDuration = 1000;
        var $hideDuration = 10000;
        var $timeOut = 10000;
        var $extendedTimeOut = 10000;
        var $showEasing = 'swing';
        var $hideEasing = 'linear';
        var $showMethod = 'fadeIn';
        var $hideMethod = 'fadeOut';
        toastr.options = {
          closeButton: true,
          debug: false,
          positionClass:'toast-bottom-right',
          onclick: null
        };
        toastr.options.showDuration = $showDuration;
        toastr.options.hideDuration = $hideDuration;
        toastr.options.timeOut = $timeOut;                        
        toastr.options.extendedTimeOut = $extendedTimeOut;
        toastr.options.showEasing = $showEasing;
        toastr.options.hideEasing = $hideEasing;
        toastr.options.showMethod = $showMethod;
        toastr.options.hideMethod = $hideMethod;
        toastr[shortCutFunction](msg, title);
      }
      else
      {
        var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        if(regex .test(result[1]))
        {
          window.location.href=result[1];
        }
      }
    },  
  });
}
function deletesavesession(aid) 
{
  if(confirm("Are you sure you want to Remove this session?"))
  {
    savesession(aid);
  }
}     
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script id="autonotifyTemplate" type="text/x-jQuery-tmpl">
${( $data.symbol = '' ),''}
{{if Ispublic=='1'}}
  ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/publicmsg' ),''}
{{/if}}
{{if Ispublic=='0'}}
  ${($data.symbol = '<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/privatemsg' ),''}
{{/if}}
<li class="unread">
  <a href="${symbol}" class="unread">
    <div class="container_msg_notify clearfix">
      <div class="thread-image"><img src="${Logo}" alt=""></div>
      <div class="thread-content">
        <span class="author">${Firstname} ${Lastname}</span>
        <span class="preview">${Message}</span>
      </div>
    </div>
  </a>
</li>
</script>