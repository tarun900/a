<?php foreach ($exibitors_list as $skey => $svalue) { ?>
<li>
    <a data-url="<?php echo base_url(); ?>Exhibitors/<?php echo $acc_name.'/'.$Subdomain;?>/View/<?php echo $svalue['Id']; ?>" class="showmodel1" onclick="openmodel(<?php echo $svalue['Id']; ?>)" id="a<?php echo $svalue['Id']; ?>">
        <div class="desc">
            <p class="logo-img" style="width: auto;">
                <?php $clogo=json_decode($svalue['company_logo']); $pathexit; if($Subdomain=="gulfoodGulfood2017"){ 
                    $pathexit=$clogo[0];
                    }else{
                      $pathexit=base_url().'assets/user_files/'.$clogo[0];
                    } $ext = pathinfo($pathexit, PATHINFO_EXTENSION);?>
                <?php if(!empty($clogo[0]) && !empty($ext)){ ?>
                <img style="width: auto;max-width: 80px;height: auto;" src="<?php echo filter_var($clogo[0], FILTER_VALIDATE_URL) ? $clogo[0] : base_url().'assets/user_files/'.$clogo[0]; ?>"/>
                <?php }else{ $color = $event_templates[0]['Top_background_color']; ?>
                <span style="background:<?php echo $color; ?>"><?php echo ucfirst(substr($svalue['Heading'], 0, 1)); ?></span>
                <?php } ?>
            </p>
            <h4 class="user_container_name"><?php echo ucfirst($svalue['Heading']); ?></h4>
            <h4 class="user_container_username" style="display:none;"><?php echo trim($svalue['Firstname']).' '.trim($svalue['Lastname']); ?></h4>
            <div class="Exbitor_keyword" style="display: none;">
                <?php echo $svalue['Short_desc']; ?>
            </div>
            <div class="Exbitor_contry" style="display: none;">
                <?php echo $svalue['country_name']; ?>
            </div>
        </div>
        <div class="time"><i class="fa fa-chevron-right"></i></div>
    </a>
    <?php if(!empty($user[0]->Id) && in_array('49',$active_menu) && $user[0]->Rid=='4'){ ?> 
    <div class="myfavorites_content_div">
        <a onclick='my_favorites("<?php echo $svalue['Id']; ?>");'>
        <img width="5%" src="<?php echo base_url().'assets/images/favorites_not_selected.png'; ?>" class="pull-right" id="favorites_not_selected_<?php echo $svalue['Id']; ?>" <?php if(in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
        <img width="5%" src="<?php echo base_url().'assets/images/favorites_selected.png'; ?>" class="pull-right" id="favorites_selected_<?php echo $svalue['Id']; ?>" <?php if(!in_array($svalue['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
        </a>
        <a href="javascript:void(0);" style="display: none;">
        <img width="5%" src="<?php echo base_url().'assets/images/ajax-loader.gif'; ?>" id="loder_icone_<?php echo $svalue['Id']; ?>">
        </a>
    </div>
    <?php } ?>
</li>
<?php } ?>

<?php if(count($exibitors_list) > 0){  ?>
<div class="col-md-12" style="text-align: center;">
<a href="javascript:;" id="load_more" class="btn btn-primary" >
Load More</a>
</div>
<?php } ?>