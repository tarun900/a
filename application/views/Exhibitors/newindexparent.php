<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = ($this->session->userdata('acc_name'))?:$this->uri->segment(2);
$favorites_user=array_filter(array_column_1($my_favorites,'module_id'));
$active_menu=array_filter(explode(",",$event_templates[0]['checkbox_values']));
// j($exhibitor_parent_categories);
?>
<div class="row margin-left-10">
    <?php
        if(!empty($form_data)):
        foreach($form_data as $intKey=>$strval):
            if($strval['frm_posi']==2)
            {
                continue;
            }
        ?>
    <div class="col-sm-6 col-sm-offset-3">
        <?php
            $json_data = false;
            $formid="form";
            
            $temp_form_id=rand(1,9999);
            $json_data = isset($strval) ? $strval['json_data'] : FALSE;
            $loader = new formLoader($json_data, base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
            $loader->render_form();
            unset($loader);
            ?>
    </div>
    <?php include 'validation.php'; ?>
    <script>
        jQuery(document).ready(function() {
            FormValidator<?php echo $temp_form_id; ?>.init();
        });
    </script>
    <?php endforeach;  
        endif;
        ?>
</div>
<?php
    if(!empty($advertisement_images)) { ?>
<div class="main-ads">
    <?php 
        $string_cms_id = $advertisement_images->Cms_id;
        $Cms_id =  explode(',', $string_cms_id); 
        
        $string_menu_id = $advertisement_images['Menu_id'];
        $Menu_id =  explode(',', $string_menu_id); 
        
        if(in_array('3', $Menu_id)) 
        {
        
        $image_array = json_decode($advertisement_images['H_images']);
        $f_url = $advertisement_images['Footer_link'];
        
        if(!empty($image_array)) { ?>
    <div class="hdr-ads">
        <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id']; ?>");'> 
        <button type="button" class="close-custom close" data-dismiss="alert" 
            aria-hidden="true"> &times; </button>
        <?php
            $img_url = base_url().'assets/user_files/'.$image_array[0];
            $size = getimagesize($img_url);
            
            $originalWidth = $size[0];
            $originalHeight = $size[1];
            
             if($originalHeight > '118')
             {
                $first = $originalWidth * 118;
                $width = $first / $originalHeight;
            
                echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
             }
             elseif ($originalHeight < '118') 
             { 
                echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
             }
             else
             {
                 echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
             }
            ?>
        </a>
        <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>
        <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
            aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
        <?php } } }  ?>
    </div>
</div>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newsponsors.css?'.time(); ?>">
<div class="agenda_content">
    <div id="main_exibitor_list">
        <div class="gold-sponsor">
            <div class="gold1">
                <ul class="activities columns activities-new">
                    <?php foreach ($exhibitor_parent_categories as $skey => $svalue) { ?>
                    <li>
                        <a class="activity" href="<?php echo base_url(); ?>Exhibitors/<?php echo $acc_name.'/'.$Subdomain;?>/categorywise/<?php echo $svalue['c_id']; ?>" class="activity" id="a<?php echo $svalue['c_id']; ?>">
                            <div class="desc">
                                <p class="logo-img">
                                    <?php if(!empty($svalue['categorie_icon'])){?>
                                    <img src="<?php echo base_url().'assets/exhibtior_category_icon/'.$svalue['categorie_icon']; ?>" alt="diary-img"/>
                                    <?php }else{ $color = $event_templates[0]['Top_background_color']; ?>
                                    <span style="text-align: center;width:72px;height:72px;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($svalue['category'], 0, 1)); ?></span>
                                    <?php } ?>
                                </p>
                                <h4 class="user_container_name"><?php echo ucfirst($svalue['category']); ?></h4>
                                <h4 class="user_container_username" style="display:none;"><?php echo trim($svalue['Firstname']).' '.trim($svalue['Lastname']); ?></h4>
                                <div class="Exbitor_keyword" style="display: none;">
                                    <?php echo $svalue['Short_desc']; ?>
                                </div>
                            </div>
                            <div class="time"><i class="fa fa-chevron-right"></i></div>
                        </a>
                    </li>
                    <?php } ?>    
                </ul>
            </div>
        </div>
    </div>
</div>
