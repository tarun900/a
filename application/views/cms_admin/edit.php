<?php $acc_name=$this->session->userdata('acc_name');
$cmsdata_edit=$cms_data[0]; ?>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.summernotecmsimages').summernote({
    height: 300,                 
    minHeight: null,       
    maxHeight: null,
    onImageUpload: function(files, editor, welEditable) {
      sendFile(files[0],editor,welEditable);
    }
  });
});
function sendFile(file,editor,welEditable) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
      data: data,
      type: "POST",
      url: "<?php echo base_url().'Cms_admin/saveeditorfiles/'.$this->uri->segment(3); ?>",
      cache: false,
      contentType: false,
      processData: false,
      success: function(url) {
        editor.insertImage(welEditable, url);
      }
  });
}
</script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">    
<div class="row">
  <div class="col-md-12">
      <div class="panel-body">
        <div class="panel-white">
        <div class="tabbable">
          <ul id="myTab2" class="nav nav-tabs">
              <li class="active">
                  <a href="#cms_list" data-toggle="tab">
                      Edit Custom Homescreen
                  </a>
              </li>
              <?php if($user->Role_name=='Client'){ ?>
              <li class="">
                  <a href="#menu_setting" data-toggle="tab">
                      Menu Name and Icon
                  </a>
              </li>
              <?php } ?>
              <li class="">
                  <a id="view_events1" href="#view_events" data-toggle="tab">
                      View App
                  </a>
              </li>
          </ul>
        </div>
          <div class="tab-content">
          <div class="tab-pane fade active in" id="cms_list">
          <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                  <div id="wizard" class="swMain">
                      <ul  style="display:none;margin-top:15px;">
                          <li>
                              <a href="#step-1">
                                  <div class="stepNumber">
                                      1
                                  </div>
                                  <span class="stepDesc"> 
                                  <small>step-1</small></span>
                              </a>
                          </li>
                          <li>
                              <a href="#step-2">
                                  <div class="stepNumber">
                                      2
                                  </div>
                                  <span class="stepDesc">
                                  <small>step-2</small> </span>
                              </a>
                          </li>
                          <li>
                              <a href="#step-3">
                                  <div class="stepNumber">
                                      3
                                  </div>
                                  <span class="stepDesc">
                                  <small>step-3</small> </span>
                              </a>
                          </li>
                          <li>
                              <a href="#step-4">
                                  <div class="stepNumber">
                                      4
                                  </div>
                                  <span class="stepDesc">
                                  <small>step-4</small> </span>
                              </a>
                          </li>
                          <li>
                              <a href="#step-5">
                                  <div class="stepNumber">
                                      5
                                  </div>
                                  <span class="stepDesc"> 
                                  <small>Finish</small></span>
                              </a>
                          </li>
                      </ul>
                      <div class="progress progress-xs transparent-black no-radius active">
                          <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                          </div>
                      </div>
                      <div id="step-1">
                          <h3>Give your custom screen a name</h3>
                          <p>This will show in the left hand menu in your app. if you<br/> change your mind you can change the name later.</p>
                          <div class="form-group">
                              <div class="col-sm-4">
                                  <input type="text" value="<?php echo $cmsdata_edit['Menu_name']; ?>" placeholder="Homescreen Name" id="Event_name" name="Menu_name" class="form-control required name_group">
                                  <input type="hidden" name="cms_id" id="cms_id" value="<?php echo $cms_id; ?>">
                                  <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-3">
                                  <button class="btn btn-theme_green next-step btn-block">
                                      Next Step >>>
                                  </button>
                              </div>
                          </div>
                      </div>
                      <div id="step-2">
                          <h3>Enter your custom screen's body content</h3>
                          <p>Here you can use the editor box below to add formatted text images,videos and links.The formatting tool allows you to place areas in columns and rows,similar to a webpage editor.if you have HTML code you can paste it into the code view of the editor box.</p>
                          <div class="form-group">
                              <div class="col-sm-9">
                                  <textarea id="Description" name="Description" class="summernotecmsimages"><?php echo $cmsdata_edit['Description']; ?></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-3">
                                  <button class="btn btn-green back-step btn-block">
                                      <<< Back
                                  </button>
                              </div>
                              <div class="col-sm-3">
                                  <button class="btn btn-theme_green next-step btn-block">
                                      Next Step >>>
                                  </button>
                              </div>
                          </div>
                      </div>
                      <div id="step-3">
                          <h3>Add your screen's banner image and left hand menu logo</h3>
                          <p>On your screen you can add a banner image and a new logo which only appears on that page.</p>
                          <div class="form-group">
                              <div class="col-sm-2">
                                  <h3>
                                      Add Your custom screen banner image here
                                  </h3>
                              </div>
                              <div class="modal fade" id="headermodelscopetool" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Banner Image</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div>
                                                        <img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
                                                    </div>
                                                    <div class="col-sm-12 text-center">
                                                        <button type="button" class="btn btn-green btn-block" id="upload_result_btn_header_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              <div class="col-sm-4" style="padding-left: 0;">
                                  <?php $images_array = json_decode($cmsdata_edit['Images']); ?>
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail" id="banner_preview_image_div"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" id="header_image_sponder" name="images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_company_banner_data">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div> 
                                    <input type="hidden" name="company_banner_crop_data_textbox" id="company_banner_crop_data_textbox">
                                    <?php if(!empty($images_array[0])){ ?>
                                      <div class="col-sm-3 fileupload-new thumbnail center">
                                        <img height="150" width="209" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>">
                                        <input type="hidden" name="old_images[]" value="<?php echo $images_array[0]; ?>">
                                      </div>
                                      <a href="#" class="btn fileupload-exists btn-red" onclick="delete_images('1');" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Delete
                                      </a>
                                    <?php } ?>  
                              </div>
                              <div class="col-sm-2">
                                  <h3>
                                      Add Your custom screen logo image here
                                  </h3>
                              </div>
                              <div class="modal fade" id="logomodelscopetool" role="dialog">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close close_logo_popup" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Logo</h4>
                                            </div>
                                            <div class="modal-body" id="show_logoupload_div_data">
                                                <div class="row">
                                                    <div id="preview_logo_crop_tool">
                                                        <img class="img-responsive" id="show_crop_img_model" src="" alt="Picture">
                                                    </div>
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-green btn-block" style="width: 120px;margin: 0 auto;" type="button" id="upload_result_btn_crop" data-dismiss="modal">Crop</button>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default close_logo_popup" data-dismiss="modal">Exit crop tool</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              <div class="col-sm-4" style="padding-left: 0;">
                                  <?php $Logo_array = json_decode($cmsdata_edit['Logo_images']); ?>
                                <div class="col-sm-9">
                                     <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail" id="company_logo_preview_div"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" id="logo_image" name="logo_images">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" id="remove_logo_image_data" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div> 
                                      <input type="hidden" name="company_logo_crop_data_textbox" id="company_logo_crop_data_textbox">
                                      <?php if(!empty($Logo_array[0])){ ?>
                                          <div class="col-sm-3 fileupload-new thumbnail center">
                                            <img alt="" height="150" width="209" src="<?php echo base_url(); ?>assets/user_files/<?php echo $Logo_array[0]; ?>">
                                            <input type="hidden" name="old_images[]" value="<?php echo $Logo_array[0]; ?>">
                                          </div>
                                          <a href="#" class="btn fileupload-exists btn-red" onclick="delete_images('0');" data-dismiss="fileupload">
                                            <i class="fa fa-times"></i> Delete
                                          </a>
                                      <?php } ?>    
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-3">
                                  <button class="btn btn-green back-step btn-block">
                                      <<< Back
                                  </button>
                              </div>
                              <div class="col-sm-3">
                                  <button class="btn btn-theme_green next-step btn-block">
                                      Next Step >>>
                                  </button>
                              </div>
                          </div>
                      </div>
                      <div id="step-4">
                          <div class="form-group">
                              <div class="col-sm-6">
                                  <h4>Select the features you would like to show in the footer of your custom screen</h4>
                                  <p>if you have created home tabs in some of your other features you can include those home tabs with links to those features at the bottom of your custom screen.</p>
                                  <p>Choose the features you would like to show in the box below.</p>
                                  <div class="form-group">
                                      <div class="col-sm-10">
                                          <select id="feature_menu" class="select2-container select2-container-multi form-control search-select menu-section-select" multiple="multiple" name="feature_menu[]">
                                              <?php
                                         $menudata=  explode(',', $cmsdata_edit['fecture_module']);
                                         $cmsmenudata=  explode(',', $cmsdata_edit['cms_fecture_module']);
                                         
                                         $key = array_search('20', array_column_1($menu_toal_data, 'id'));
                                          if($key=="")
                                          {
                                              $notshowmenu=array('20','21','26','27','28','22','23','24','25','42');
                                          }
                                          else
                                          {
                                              $notshowmenu=array('20','21','26','27','28');
                                          }
                                         foreach ($menu_toal_data as $key=>$value)
                                         {
                                          if(!in_array($value['id'], $notshowmenu)){
                                           ?>
                                         <option value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'], $menudata)){ ?> selected="selected" <?php } ?> ><?php echo $value['menuname']; ?></option>
                                           <?php }
                                         }
                                         ?>
                                          </select>
                                      </div>
                                  </div>
                                  <?php if($event_id=='447'){ ?>
                                  <div class="form-group"> 
                                    <div class="col-sm-10">
                                      <select class="select2-container form-control search-select menu-section-select" id="exi_page_id" name="exi_page_id"> 
                                        <option value="">Select Exibitor</option>
                                        <?php foreach ($exibitor_page as $key => $value) { ?>
                                        <option value="<?php echo $value['Id']; ?>" <?php if($value['Id']==$cmsdata_edit['exi_page_id']){ ?> selected="seleted" <?php } ?>><?php echo $value['Heading']; ?></option>
                                        <?php } ?>
                                      </select>    
                                    </div>
                                  </div>
                                  <?php } ?>
                              </div>
                              <div class="col-sm-4">
                                  <img alt="" src="<?php echo base_url(); ?>assets/images/image_Yd.png">
                              </div>
                          </div>
                          <div class="form-group" style="margin-top: 40px;">
                              <div class="col-sm-3">
                                  <button class="btn btn-green back-step btn-block">
                                      <<< Back
                                  </button>
                              </div>
                              <div class="col-sm-3">
                                  <button id="cms_form_submit" class="btn btn-theme_green next-step btn-block">
                                      Finish
                                  </button>
                              </div>
                          </div>
                      </div>
                      <div id="step-5">
                          <h3 class="StepTitle">Finish!</h3>
                          <div id="result_div"></div>
                          <p>You have successfully added your custom screen!</p>
                          <p>if you want to edit any part of your screen you can go back to the Custom Screens Module and open it.All your screens are shown in the table.</p>
                          <div class="form-group">
                              <div class="col-sm-3">
                                  <a id="go_back_to_the_main_url_btn" href="<?php echo base_url().'Cms_admin/index/'.$this->uri->segment(3); ?>" class="btn btn-theme_green btn-block" disabled="disabled">
                                      Go back to the main screen
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
            </form>
            </div>
            <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Cms_admin/menusave/<?php echo $event_id.'/'.$this->uri->segment(4); ?>" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title form-field-20" class="form-control required limited" maxlength="20" placeholder="Title" value="<?php echo empty($title) ? $cmsdata_edit['Menu_name'] : $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="cms_id" id="cms_id" value="<?php echo $cms_id; ?>">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id!="" ? $menu_id : 0; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                           <div class="fileupload-new thumbnail"></div>
                                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                           <div class="user-edit-image-buttons">
                                                <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                     <input type="file" name="Images[]">
                                                </span>
                                                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                     <i class="fa fa-times"></i> Remove
                                                </a>
                                           </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                            </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <br/><br/><br/><br/>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
            <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;">
                <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                    </div>
            </div>
        </div>
      </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $logoimage = $("#show_crop_img_model");
var $inputLogoImage = $('#logo_image'); 
var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image_sponder'); 
$(document).ready(function(){
    $inputLogoImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              
              uploadedImageURL = URL.createObjectURL(file);
              $logoimage.attr('src', uploadedImageURL);
              $('#logomodelscopetool').modal('show');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#logomodelscopetool', function () {
        $logoimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_crop');
            $logoimage.cropper({
                aspectRatio: 1,
                built: function () {
                  croppable = true;
                }
            });
            $button.on('click', function () {
                var croppedCanvas;
                if (!croppable) {
                  return;
                }
                croppedCanvas = $logoimage.cropper('getCroppedCanvas');
                $('#company_logo_preview_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
                $('#company_logo_crop_data_textbox').val(croppedCanvas.toDataURL());
                $("#logo_image").val('');
            });
        }).on('hidden.bs.modal','#logomodelscopetool',function(){
             $logoimage.cropper('destroy');
    });
    $inputBannerImage.change(function(){
        var uploadedImageURL;
        var URL = window.URL || window.webkitURL;
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
              if (uploadedImageURL) {
                URL.revokeObjectURL(uploadedImageURL);
              }
              uploadedImageURL = URL.createObjectURL(file);
              $bannerimage.attr('src', uploadedImageURL);
              $('#company_banner_crop_data_textbox').val('');
              $('#headermodelscopetool').modal('toggle');
            } else {
              window.alert('Please choose an image file.');
            }
        }
    });
    $(document).on('shown.bs.modal','#headermodelscopetool' ,function () {
        $bannerimage.cropper('destroy');
        var croppable = false;
        var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
            built: function () {
              croppable = true;
            }
        });
        $button.on('click', function () {
            var croppedCanvas;
            if (!croppable) {
              return;
            }
            croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
            $('#banner_preview_image_div').html("<img src='"+croppedCanvas.toDataURL()+"'>");
            $('#company_banner_crop_data_textbox').val(croppedCanvas.toDataURL());
            $("#header_image_sponder").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    }); 
});
</script>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$('#remove_logo_image_data').click(function(){
  $('#company_logo_crop_data_textbox').val('');
});
$('#remove_company_banner_data').click(function(){
    $('#company_banner_crop_data_textbox').val('');
});
$('#cms_form_submit').click(function(){
    $('#go_back_to_the_main_url_btn').html("Submitting <i class='fa fa-spinner fa-spin'></i>");
    var formData = new FormData($('form')[0]);
    /*formData.append('Description',CKEDITOR.instances['Description'].getData());*/
    formData.append('Description',$('#Description').code());
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'Cms_admin/edit/'.$this->uri->segment(3).'/'.$this->uri->segment(4); ?>",
        data: formData,
        processData: false,
        contentType: false,
        error:function(error){
            $('#result_div').attr('class','alert alert-danger');
            $('#result_div').html('Somthing Went Worng..');
            $('#result_div').show();
            $('#go_back_to_the_main_url_btn').html("Go back to the main screen");
            $('#go_back_to_the_main_url_btn').removeAttr('disabled');
        },
        success: function(result){
            var data=result.split('###');
            if(data[0]=="success")
            {
                $('#result_div').attr('class','alert alert-success');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            else
            {
                $('#result_div').attr('class','alert alert-danger');
                $('#result_div').html(data[1]);
                $('#result_div').show();
            }
            $('#go_back_to_the_main_url_btn').html("Go back to the main screen");
            $('#go_back_to_the_main_url_btn').removeAttr('disabled');
        }
    });
});
function delete_images(image_type)
{
  if(confirm('Are You Sure Delete This Image ?'))
  {
    window.location.href="<?php echo base_url().'Cms_admin/delete_image/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'; ?>"+image_type;
  }
}
</script>
