<?php $acc_name=$this->session->userdata('acc_name');
date_default_timezone_set("UTC");
$cdate=date('Y-m-d H:i:s');
if(!empty($event['Event_show_time_zone']))
{
    if(strpos($event['Event_show_time_zone'],"-")==true)
    { 
        $arr=explode("-",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
    }
    if(strpos($event['Event_show_time_zone'],"+")==true)
    {
        $arr=explode("+",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
    }
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#notification" data-toggle="tab">
                            Notifications
                        </a>
                    </li>
                    <li>
                        <a id="analytics1" href="#analytics" data-toggle="tab">
                            Analytics
                        </a>
                    </li>
                </ul>
            </div>
			<div class="panel-body">
				<?php if ($this->session->flashdata('notification_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Notification <?php echo $this->session->flashdata('notification_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="notification">
                        <div class="row" style="margin-bottom: 25px;">
                            <a class="btn btn-primary list_page_btn schedule_btn" href="<?php echo base_url(); ?>Notification/addNewsNoti/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Notification</a>
                        </div>
                    	<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            <thead>
                                <tr>
									<th>#</th>
									<th>Title</th>
									<th>Created Time</th>
                                    <th>Short Description</th>
									<th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($rss_noti as $key => $value) { ?>
                            	<tr>
                            		<td><?php echo $i++; ?></td>	
                            		<td><?php echo $value['title'] ?></td>	
                                    <td><?php echo $value['created_at'] ?></td> 
                            		<td><?php echo substr($value['content'], 0, 80); ?></td>	
                            		<td><?php if($value['is_survey']=='1'){ ?>
                                        <span class="label label-sm label-success"> Survey </span>
                                         <?php } else {?>
                                        <span class="label label-sm label-danger"> RSS </span> 
                                        <?php } ?>
                                    </td>	
                            		<td>
                                        <a href="<?php echo base_url() ?>Notification/editNewsNoti/<?php echo $value['event_id']; ?>/<?php echo $value['Id'];?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" onclick="delete_notification(<?php echo $value['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>      
                                    </td>	
                            	</tr>
                                <?php } ?>
                            </tbody>
                        </table>    
                    </div>
                    <div class="tab-pane fade" id="analytics"> 
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Total Users</th>
                                    <th>Sent</th>
                                    <th>Failed</th>
                                    <th>Received</th>
                                    <th>Opened</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($noti_analytics as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>    
                                    <td><?php echo $value['title'] ?></td>  
                                    <td><?php 
                                        switch ($value['notification_type']) {
                                            case '1':
                                                echo 'Instant Notifications';
                                                break;
                                            case '2':
                                                echo 'Scheduled Notifications';
                                                break;
                                            case '4':
                                                if($value['is_survey'])
                                                    echo "Survey";
                                                else
                                                    echo "RSS";
                                                break;
                                        }
                                      ?></td> 
                                    <td><?php echo count(explode(',',$value['user_ids'])); ?></td>   
                                    <td><?php echo $value['scount']; ?></td>    
                                    <td><?php echo $value['fcount']; ?></td>
                                    <td><?php echo $value['recount']; ?></td>
                                    <td><?php echo $value['opcount']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url() ?>Notification/analytics/<?php echo $this->uri->segment(3) ?>/<?php echo $value['notification_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Details"><i class="fa fa-eye"></i></a>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>    
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    if(window.location.hash != "") {
        $('a[href="' + window.location.hash + '"]').click()
    }
});
function delete_notification(id,is_geo=0)
{   
    <?php $test = $this->uri->segment(3); ?>
    if(confirm("Are you sure to delete this?"))
    {
        
        window.location.href ="<?php echo base_url(); ?>Notification/delete/"+<?php echo $test; ?>+"/"+id+"/"+is_geo
    }
}
function activeemail(id) {
    $.ajax({
        url:"<?php echo base_url().'Notification/active_mail_notify/'.$event_id.'/' ?>"+id,
        success:function(result) 
        {
            var shortCutFunction ='success';
            if($("#send_email_"+id).is(':checked'))
            {
              var title = 'Active Successfully';
              var msg = 'Email Notification Active Successfully.';
            }
            else
            {
              var title = 'Inactive Successfully';
              var msg = 'Email Notification Inactive Successfully'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
        }    
    });
}
</script>    		