<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#add_notification" data-toggle="tab">
                                Add Notification
                            </a>
                        </li>

                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="add_notification">
                    	<div class="col-sm-10">
                			<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data" onsubmit="return before_submit()">
                				<h4 style="color: #5381ce;">New Notification</h4>
                				<div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Title <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <input type="text" placeholder="Notification Title" id="name" name="title" class="form-control name_group required">
	                                </div>
	                            </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
											Message <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <textarea id="Description form-field-23" class="form-control required" name="Description" style="width:100%;height:100px;"><?php  ?></textarea>
	                                </div>
	                            </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-4" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Send to Attendees Group
										</label>
	                                    <input type="checkbox" id="send_attendee_group" style="margin-top: 9px;" onchange="change()">
                            		</div>
	                            </div>
	                            <div class="form-group" id="user_groups" style="display: none;">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Attendees Group<span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_group" class="select2-container select2-container-multi form-control search-select menu-section-select required" name="user_group[]">
	                                    <?php foreach($user_group as $key => $value):?>
	                                    	<option value="usergroup_<?=$value['id']?>"><?=$value['group_name']?></option>
	                                    <?php endforeach;?>
	                                    </select>
                            		</div>
	                            </div>

	                            <div class="form-group" id="users">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Attendees <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_ids" class="select2-container select2-container-multi form-control search-select menu-section-select required" name="user_ids[]">
	                                    <option value="All">All</option>
	                                    <?php foreach ($attendee_data as $key => $value) { ?>
	                                    <option value="<?php echo $value['Id']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
	                                    <?php } ?>
	                                    </select>
                            		</div>
	                            </div>
	                        	<div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Open button link <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <?php $arrkey=array(5,8,14,18,19,21,26,27,28,29); ?>
	                                    <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id">
	                                    <optgroup label="Select Modules">
	                                    <!-- <option value="">Select Modules</option> -->
	                                    <?php foreach ($menu_toal_data as $key => $value) { 
	                                            if(!in_array($value['id'], $arrkey)){
	                                        ?>
	                                    <option id="menu<?php echo $key; ?>" value="M_<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
	                                    <?php } } ?>
	                                    </optgroup>
	                                    <optgroup label="Select Custom Modules">
	                                    <?php foreach ($cms_menu as $key => $value) { ?>
	                                    <option id="menu<?php echo $key; ?>" value="C_<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
	                                    <?php }  ?>
	                                    </optgroup>
	                                    </select>
	                                </div>
	                            </div>
								<div class="form-group">
											<div class="col-sm-9">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
                                        Expiration Time<span class="symbol required"></span>
                                    </label>
									</div>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="ttl" id="">
											<option value="-1">No Expiration</option>
											<option value="1800">30 Minutes</option>
											<option value="3600">1 hour</option>
											<option value="5400">1.30 Hour</option>
											<option value="7200">2 Hours</option>
											<option value="9000">2.30 Hours</option>
											<option value="10800">3 Hours</option>
										</select>
                                    </div>
                                </div>
	                            <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Send <span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-9">
	                                    <select style="margin-top:-8px;" id="noti_type" class="form-control name_group required" name="noti_type">
	                                    <option value="2"> Scheduled Notification </option>
	                                    <option value="1"> Instant Notifications </option>
	                                    </select>
                            		</div>
	                            </div> 
	                            <div id="notification_option"> 
	                                <div class="form-group">
										<div class="col-sm-9">
											<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;" for="form-field-1">
												Start Date & Time  <span class="symbol required"></span>
											</label>
										</div>
	                                    <div class="col-sm-9">
	                                        <input type="text" class="form-control datetimepicker required" id="inputAuctionStartDateTime" name="datetime" autocomplete="off" />
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-md-2 pull-left">
	                                    <button class="btn btn-green btn-block" type="submit">
	                                        Send 
	                                    </button>
	                                </div>
	                            </div>       
                			</form>
                    	</div>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>    
            </div>
        </div>        
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$('#noti_type').change(function(){
	if($.trim($('#noti_type').val())=='2')
	{
		$('#notification_option').show();
		$('#inputAuctionStartDateTime').addClass('required');
		$('#inputAuctionStartDateTime').attr('name','datetime');
	}
	else
	{
		$('#notification_option').hide();
		$('#inputAuctionStartDateTime').removeClass('required');
		$('#inputAuctionStartDateTime').removeAttr('name');
	}
});
jQuery(document).ready(function($) {
	change();
});
function change()
{
	if($('#send_attendee_group').is(":checked"))
	{
		$('#user_groups').show();
		$('#user_group').addClass('required');
		$('#users').hide();
		$('#user_ids').removeClass('required');
	}
	else
	{
		$('#user_groups').hide();
		$('#user_group').removeClass('required');
		$('#users').show();	
		$('#user_ids').addClass('required');	
	}
}
function before_submit()
{
	if($('#send_attendee_group').is(":checked"))
		$('#user_ids').val('').change();
	else
		$('#user_group').val('').change();
	return true;
}
</script>