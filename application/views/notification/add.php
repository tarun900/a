<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            
            <!--<div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Push Notification</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>-->

            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#add_notification" data-toggle="tab">
                                Add Scheduled Notification
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="add_notification">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                    Title <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Notification Title" id="name" name="title" class="form-control name_group required">
                                </div>
                            </div>

                            <div class="form-group">
                               <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Send copy to Email 
                                </label>
                                <div class="col-sm-9">
                                   <input type="checkbox" name="chkEmailSend" value="1" checked="" class="checkbox" /> 
                                </div>
                            </div>

                            <div id="notification_option"> 
                                <div class="form-group">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                        Start Date & Time  <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control datetimepicker required" id="inputAuctionStartDateTime" name="datetime" />
                                    </div>
                                </div>
                            </div>

                                
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Content <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <textarea  id="Description" name="Description" class="" style="width:100%;height:250px;"><?php  ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                     Module <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                
                                <?php $arrkey=array(5,12,13,18,19,20,22,23,24,25,26,27,28,29); ?>
                                    <select multiple="multiple" style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id[]">
                             <option value="Send All">Send All</option>
                            <?php foreach ($menu_toal_data as $key => $value) { 
                                    if(!in_array($value['id'], $arrkey)){
                                ?>
                            <option id="menu<?php echo $key; ?>" value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
                            <?php } } ?>
                           
                            </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

<script type="text/javascript">
    function closediv()
     {
        if($("#notification_type").val()=='1')
        {
            $("#notification_option").css('display','none');
            $(".btn-block").text('SEND NOW');
        }
        else
        {
            $("#notification_option").css('display','block');
            $(".btn-block").text('Submit');
        }
      }
</script>
