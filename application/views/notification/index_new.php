<?php $acc_name=$this->session->userdata('acc_name');
date_default_timezone_set("UTC");
$cdate=date('Y-m-d H:i:s');
if(!empty($event['Event_show_time_zone']))
{
    if(strpos($event['Event_show_time_zone'],"-")==true)
    { 
        $arr=explode("-",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
    }
    if(strpos($event['Event_show_time_zone'],"+")==true)
    {
        $arr=explode("+",$event['Event_show_time_zone']);
        $intoffset=$arr[1]*3600;
        $intNew = abs($intoffset);
        $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
    }
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#notification" data-toggle="tab">
                            Notifications
                        </a>
                    </li>
                    <li>
                        <a href="#geo_noti" data-toggle="tab">
                            Location Based Notifications
                        </a>
                    </li>
                    <li>
                        <a href="#default_push" data-toggle="tab">
                            Default Push Notifications
                        </a>
                    </li>
                    <!-- <li>
                        <a href="#notification_setting" data-toggle="tab">
                            Notification Settings
                        </a>
                    </li> -->
                    
                        <li>
                        <a id="analytics1" href="#analytics" data-toggle="tab">
                            Analytics
                        </a>
                    </li>
                   
                    <li>
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View App
                        </a>
                    </li>
                </ul>
            </div>
			<div class="panel-body">
				<?php if ($this->session->flashdata('notification_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Notification <?php echo $this->session->flashdata('notification_data'); ?> Successfully.
                    </div>
                <?php } ?>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="notification">
                        <div class="row" style="margin-bottom: 25px;">
                            <a class="btn btn-primary list_page_btn schedule_btn" href="<?php echo base_url(); ?>Notification/addnotification/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Notification</a>
                        </div>
                    	<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            <thead>
                                <tr>
									<th>#</th>
									<th>Title</th>
									<th>Created Time</th>
                                    <th>Scheduled Time</th>
									<th>Short Description</th>
									<th>type</th>
                                    <th>Status</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($arrnotification as $key => $value) { ?>
                            	<tr>
                            		<td><?php echo $i++; ?></td>	
                            		<td><?php echo $value['title'] ?></td>	
                                    <td><?php echo $value['created_at'] ?></td> 
                            		<td><?php echo $value['datetime'] ?></td>	
                            		<td><?php echo substr($value['content'], 0, 80); ?></td>	
                            		<td><?php echo $value['notification_type']=='1' ? 'Instant Notifications' :  'Schedule Notifications'; ?></td>
                                    <td><?php if($value['notification_type']=='1' || $value['datetime'] <= $cdate){ ?><span class="label label-sm label-success"> Sent </span> <?php } ?>
                                    </td>	
                            		<td>
                                        <a href="<?php echo base_url() ?>Notification/editnotification/<?php echo $value['event_id']; ?>/<?php echo $value['Id'];?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" onclick="delete_notification(<?php echo $value['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>      
                                    </td>	
                            	</tr>
                                <?php } ?>
                            </tbody>
                        </table>    
                    </div>
                    <div class="tab-pane fade" id="geo_noti">
                        <div class="row" style="margin-bottom: 25px;">
                            <a class="btn btn-primary list_page_btn schedule_btn" href="<?php echo base_url(); ?>Notification/addGeoNoti/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i>Add Location Based Notification</a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Short Description</th>
                                    <th>Address</th>
                                    <th>Lat</th>
                                    <th>Long</th>
                                    <th>Radius</th>
                                    <th>Created Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($geo_noti as $key => $value) { ?>
                                <tr>
                                    <td><?=$i++?></td>    
                                    <td><?=$value['title']?></td>  
                                    <td><?=substr($value['content'], 0, 80)?></td>   
                                    <td><?=$value['address']?></td> 
                                    <td><?=$value['lat']?></td> 
                                    <td><?=$value['long']?></td> 
                                    <td><?=$value['radius']?></td> 
                                    <td><?=$value['created_at']?></td> 
                                    <td>
                                        <a href="<?php echo base_url() ?>Notification/editGeoNoti/<?php echo $value['event_id']; ?>/<?php echo $value['Id'];?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" onclick="delete_notification(<?php echo $value['Id']; ?>,'1')" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>         
                                    </td>   
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table> 
                    </div>
                    <div class="tab-pane fade" id="default_push">
                        <div class="table-responsive custom_checkbox_table">
                           <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Content</th>
                                        <th>Email Alert</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($arrPushTemplate)){
                                        for($i=0;$i<count($arrPushTemplate);$i++) {?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><?php echo stripslashes($arrPushTemplate[$i]['Slug']);?></td>
                                        <td><?php echo stripslashes($arrPushTemplate[$i]['Content']);?></td>
                                        <td>
                                            <?php $notslugarr=array('POPUP WON','Outbid Alert','HIGHEST BIDDER');
                                            if(!in_array($arrPushTemplate[$i]['Slug'],$notslugarr)){
                                            ?>
                                            <input type="checkbox" onchange='activeemail("<?php echo $arrPushTemplate[$i]['Id']; ?>");' name="send_email" value="<?php echo $arrPushTemplate[$i]['Id']; ?>" id="send_email_<?php echo $arrPushTemplate[$i]['Id'];?>" <?php if($arrPushTemplate[$i]['send_email']=='1'){ ?> checked="checked" <?php } ?>>
                                            <label for="send_email_<?php echo $arrPushTemplate[$i]['Id'];?>"></label>  
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Notification/pushedit/<?php echo $arrPushTemplate[$i]['event_id']; ?>/<?php echo $arrPushTemplate[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                        <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="tab-pane fade" id="notification_setting">
                        <form method="post" action="<?php echo base_url().'Notification/save_only_notify_login_user_status/'.$this->uri->segment(3); ?>">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h4 style="float: left;">Only send notifications to users logged in</h4>
                                    <input style="margin-top: 13px;margin-left: 20px;" type="checkbox" <?php if($event['only_notify_login_user']=='1'){ ?> checked="checked" <?php } ?> name="only_notify_login_user" id="only_notify_login_user" class="" value="1"> 
                                </div>
                            </div>
                            <div class="col-md-3"  style="margin:15px 0px;padding-left: 0px;">
                                <button name="notify_login_user_btn" class="btn btn-yellow btn-block" value="notify_login_user_btn" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </form>
                    </div> -->
                    <div class="tab-pane fade" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Feature Menu Title</h5>
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="img_view" id="img_view" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="Redirect_url" id="Redirect_url" value="0">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="analytics"> 
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Total Users</th>
                                    <th>Sent</th>
                                    <th>Failed</th>
                                    <th>Received</th>
                                    <th>Opened</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach ($noti_analytics as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>    
                                    <td><?php echo $value['title'] ?></td>  
                                    <td><?php 
                                        switch ($value['notification_type']) {
                                            case '1':
                                                echo 'Instant Notifications';
                                                break;
                                            case '2':
                                                echo 'Scheduled Notifications';
                                                break;
                                            case '3':
                                                echo 'Geo Location Notifications';
                                                break;
                                        }
                                      ?></td> 
                                    <td><?php echo count(explode(',',$value['user_ids'])); ?></td>   
                                    <td><?php echo $value['scount']; ?></td>    
                                    <td><?php echo $value['fcount']; ?></td>
                                    <td><?php echo $value['recount']; ?></td>
                                    <td><?php echo $value['opcount']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url() ?>Notification/analytics/<?php echo $this->uri->segment(3) ?>/<?php echo $value['notification_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Details"><i class="fa fa-eye"></i></a>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                      	<div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                      	</div>
                      	<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                      	<div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                      	</div>
                    </div>
                </div>    
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    if(window.location.hash != "") {
        $('a[href="' + window.location.hash + '"]').click()
    }
});
function delete_notification(id,is_geo=0)
{   
    <?php $test = $this->uri->segment(3); ?>
    if(confirm("Are you sure to delete this?"))
    {
        
        window.location.href ="<?php echo base_url(); ?>Notification/delete/"+<?php echo $test; ?>+"/"+id+"/"+is_geo
    }
}
function activeemail(id) {
    $.ajax({
        url:"<?php echo base_url().'Notification/active_mail_notify/'.$event_id.'/' ?>"+id,
        success:function(result) 
        {
            var shortCutFunction ='success';
            if($("#send_email_"+id).is(':checked'))
            {
              var title = 'Active Successfully';
              var msg = 'Email Notification Active Successfully.';
            }
            else
            {
              var title = 'Inactive Successfully';
              var msg = 'Email Notification Inactive Successfully'; 
            }
            var $showDuration = 1000;
            var $hideDuration = 3000;
            var $timeOut = 10000;
            var $extendedTimeOut = 5000;
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass:'toast-top-right',
                onclick: null
            };
            toastr.options.showDuration = $showDuration;
            toastr.options.hideDuration = $hideDuration;
            toastr.options.timeOut = $timeOut;                        
            toastr.options.extendedTimeOut = $extendedTimeOut;
            toastr.options.showEasing = $showEasing;
            toastr.options.hideEasing = $hideEasing;
            toastr.options.showMethod = $showMethod;
            toastr.options.hideMethod = $hideMethod;
            toastr[shortCutFunction](msg, title);
        }    
    });
}
</script>    		