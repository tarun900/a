<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#edit_notification" data-toggle="tab">
                                Edit Notification
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="edit_notification">
                    	<div class="col-sm-9">
                			<form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data" onsubmit="return before_submit()">
                				<h3 style="color: #5381ce;">New Notification</h3>
                				<div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
	                                    Title <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <input type="text" placeholder="Notification Title" id="name" name="title" class="form-control name_group required" value="<?php echo $notification['title']; ?>">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
	                                    Message <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <textarea  id="Description" name="Description" class="form-control" style="width:100%;height:250px;"><?php echo $notification['content']; ?></textarea>
	                                </div>
	                            </div>
	                           <div class="form-group">
									<div class="col-sm-9">
										<label class="control-label col-sm-4" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Send to Attendees Group
										</label>
	                                    <input type="checkbox" id="send_attendee_group" style="margin-top: 9px;" onchange="change()" <?=($notification['user_groups'])?'checked=checked':''?>>
                            		</div>
	                            </div>
                            	<div class="form-group" id="user_groups" style="display: none;">
									<div class="col-sm-9">
										<label class="control-label col-sm-3" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
											Attendees Group<span class="symbol required"></span>
										</label>
									</div>
	                                <div class="col-sm-10">
	                                    <select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_group" class="select2-container select2-container-multi form-control search-select menu-section-select" name="user_group[]">
										<?php
	                                    	$user_groups=explode(",",$notification['user_groups']);
	                                     foreach($user_group as $key => $value):?>
	                                    	<option value="usergroup_<?=$value['id']?>" <?php if(in_array($value['id'],$user_groups)){ ?> selected="selected" <?php } ?>><?=$value['group_name']?></option>
	                                    <?php endforeach;?>
	                                    </select>
                            		</div>
	                            </div>
	                            <div class="form-group" id="users">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Attendees <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                	<?php $user_ids=explode(",",$notification['user_ids']);?>
	                                		  
	                                	<select multiple="multiple" style="margin-top:-8px;height: auto;" id="user_ids" class="select2-container select2-container-multi form-control search-select menu-section-select" name="user_ids[]">
	                                		<?php if($notification['send_to_all'] == '1'):?>
	                                    <option value="All" selected="selected">All</option>
	                                    <?php foreach ($attendee_data as $key => $value) { ?>
	                                    <option value="<?php echo $value['Id']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
	                                    <?php } ?>
	                                    </select>
	                                    <?php else: ?>
                                    	<option value="All">All</option>
	                                    <?php foreach ($attendee_data as $key => $value) { ?>
	                                    <option value="<?php echo $value['Id']; ?>" <?php if(in_array($value['Id'],$user_ids) && !in_array($value['Id'],$groups_user_ids)){ ?> selected="selected" <?php } ?>><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
	                                    <?php } ?>
	                                    </select>
	                                    <?php endif; ?>
                            		</div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Open button link <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <?php $arrkey=array(5,8,14,18,19,21,26,27,28,29); ?>
	                                    <select style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id">
	                                    <optgroup label="Select Modules">
	                                    <?php foreach ($menu_toal_data as $key => $value) { 
	                                            if(!in_array($value['id'], $arrkey)){
	                                        ?>
	                                    <option id="menu<?php echo $key; ?>" value="M_<?php echo $value['id']; ?>" <?php if($value['id']==$notification['moduleslink']){ ?> selected="selected" <?php } ?>><?php echo $value['menuname']; ?></option>
	                                    <?php } } ?>
	                                    </optgroup>
	                                    <optgroup label="Select Custom Modules">
	                                    <?php foreach ($cms_menu as $key => $value) { ?>
	                                    <option id="menu<?php echo $key; ?>" value="C_<?php echo $value['id']; ?>" <?php if($value['id']==$notification['custommoduleslink']){ ?> selected="selected" <?php } ?>><?php echo $value['menuname']; ?></option>
	                                    <?php }  ?>
	                                    </optgroup>
	                                    </select>
	                                </div>
	                            </div>
								<div class="form-group">
											<div class="col-sm-9">
                                    <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;padding-left: 0px;min-height: 34px;" for="form-field-1">
                                        Expiration Time<span class="symbol required"></span>
                                    </label>
									</div>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="ttl" id="">
											<option  <?php if($notification['ttl']=='-1'){ ?> selected="selected" <?php } ?> value="-1">No Expiration</option>
											<option  <?php if($notification['ttl']=='1800'){ ?> selected="selected" <?php } ?> value="1800">30 Minutes</option>
											<option <?php if($notification['ttl']=='3600'){ ?> selected="selected" <?php } ?> value="3600">1 hour</option>
											<option <?php if($notification['ttl']=='5400'){ ?> selected="selected" <?php } ?> value="5400">1.30 Hour</option>
											<option <?php if($notification['ttl']=='7200'){ ?> selected="selected" <?php } ?> value="7200">2 Hours</option>
											<option <?php if($notification['ttl']=='9000'){ ?> selected="selected" <?php } ?> value="9000">2.30 Hours</option>
											<option <?php if($notification['ttl']=='10800'){ ?> selected="selected" <?php } ?> value="10800">3 Hours</option>
										</select>
                                    </div>
                                </div>
	                            <div class="form-group">
	                                <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;padding-left: 18px;min-height: 34px;" for="form-field-1">
	                                    Send <span class="symbol required"></span>
	                                </label>
	                                <div class="col-sm-10">
	                                    <select style="margin-top:-8px;" id="noti_type" class="form-control name_group required" name="noti_type">
	                                    <option value="2" <?php if($notification['notification_type']=='2'){ ?> selected="selected" <?php } ?>> Scheduled Notification </option>
	                                    <option value="1" <?php if($notification['notification_type']=='1'){ ?> selected="selected" <?php } ?>> Instant Notifications </option>
	                                    </select>
                            		</div>
	                            </div> 
	                            <div id="notification_option" <?php if($notification['notification_type']=='1'){ ?> style="display: none;" <?php } ?>> 
	                                <div class="form-group">
	                                    <label class="control-label col-sm-12" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
	                                        Start Date & Time  <span class="symbol required"></span>
	                                    </label>
	                                    <div class="col-sm-10">
	                                    	<?php if($notification['notification_type']=='2'){ ?>
	                                        <input type="text" class="form-control datetimepicker required" id="inputAuctionStartDateTime" name="datetime" value="<?php echo date('Y/m/d H:i',strtotime($notification['datetime'])); ?>"  autocomplete="off" />
	                                        <?php }else{ ?>
	                                        <input type="text" class="form-control datetimepicker" id="inputAuctionStartDateTime" autocomplete="off" />
	                                        <?php } ?>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="col-md-2 pull-left">
	                                    <button class="btn btn-green btn-block" type="submit">
	                                        Send 
	                                    </button>
	                                </div>
	                            </div>      
                			</form>	
                    	</div>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:50%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>    
            </div>
        </div>        
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
$('#noti_type').change(function(){
	if($.trim($('#noti_type').val())=='2')
	{
		$('#notification_option').show();
		$('#inputAuctionStartDateTime').addClass('required');
		$('#inputAuctionStartDateTime').attr('name','datetime');
	}
	else
	{
		$('#notification_option').hide();
		$('#inputAuctionStartDateTime').removeClass('required');
		$('#inputAuctionStartDateTime').removeAttr('name');
	}
});
jQuery(document).ready(function($) {
	change();
});
function change()
{
	if($('#send_attendee_group').is(":checked"))
	{
		$('#user_groups').show();
		$('#user_group').addClass('required');
		$('#users').hide();
		$('#user_ids').removeClass('required');
		// $('#user_ids').val('').change();
	}
	else
	{
		$('#user_groups').hide();
		$('#user_group').removeClass('required');
		// $('#user_group').val('').change();
		$('#users').show();	
		$('#user_ids').addClass('required');	
	}
}
function before_submit()
{
	if($('#send_attendee_group').is(":checked"))
		$('#user_ids').val('').change();
	else
		$('#user_group').val('').change();
	return true;
}
</script>