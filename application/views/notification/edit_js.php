<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-elements.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js"></script>

<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-core-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-full-0.3.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/dust-js/dust-helpers.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/libraries/tabs.jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/formbuilder/formBuilder.jquery.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.datetimepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTables/media/css/DT_bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css"/>

<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        var startDate = '-0d';
        jQuery('.datetimepicker').datetimepicker({
            formatTime:'H:i',
            formatDate:'m/d/Y',
            minDate: '0d',
            step:5
        });
    });
</script>

<script>
	jQuery(document).ready(function() {
		FormElements.init();
        FormValidator.init();
	});
        
        function checkmenusection()
        {      
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Advertising/checkmenusection',
                data :'Menu_id='+$("#Menu_id").val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#Menu_id').parent().removeClass('has-success').addClass('has-error');
                        $('#Menu_id').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#Menu_id').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        $('#Menu_id').parent().removeClass('has-error').addClass('has-success');
                        $('#Menu_id').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#Menu_id').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }

        function checkcmssection()
        {      
                var sendflag="";
                $.ajax({
                url : '<?php echo base_url(); ?>Advertising/checkcmssection',
                data :'Cms_id='+$("#Cms_id").val(),
                type: "POST",  
                async: false,
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#Cms_id').parent().removeClass('has-success').addClass('has-error');
                        $('#Cms_id').parent().find('.control-label span').removeClass('ok').addClass('required');
                        $('#Cms_id').parent().find('.help-block').removeClass('valid').html(values[1]);
                        sendflag=false;
                    }
                    else
                    {
                        $('#Cms_id').parent().removeClass('has-error').addClass('has-success');
                        $('#Cms_id').parent().find('.control-label span').removeClass('required').addClass('ok');
                        $('#Cms_id').parent().find('.help-block').addClass('valid').html(''); 
                        sendflag=true;
                    }
                }
            });
            return sendflag;
        }
        
</script>
<script type="text/javascript">
    $("#formBuilder").formBuilder({
       load_url: '<?php echo base_url(); ?>Formbuilder/getJsonData/<?php echo $event['Id'];?>/<?php echo $arrForm[0]['id'];?>',
       save_url: '<?php echo base_url(); ?>Formbuilder/edit/<?php echo $event['Id'];?>/<?php echo $arrForm[0]['id'];?>', 
       onSaveForm: function() {
          // redirect to demo page
          window.location.href = '<?php echo base_url(); ?>Formbuilder/index/<?php echo $event['Id'];?>';
        }
    });
</script>    