<html>
<head><title>Visitor App</title>
<meta charset="utf-8" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta content="" name="description" />
<meta content="" name="author" />
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=95051990"></script> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/visitor-event.css?<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css">
</head>
<body>
<?php $iconset=array(
	'1'=>array('name'=>'Agenda','image'=>'1.png'),
	'2'=>array('name'=>'Attendees','image'=>'2.png'),
	'3'=>array('name'=>'Exhibitors','image'=>'3.png'),
	'6'=>array('name'=>'Notes','image'=>'6.png'),
	'7'=>array('name'=>'Speakers','image'=>'7.png'),
	'9'=>array('name'=>'Presentations','image'=>'9.png'),
	'10'=>array('name'=>'Maps','image'=>'10.png'),
	'11'=>array('name'=>'Photos','image'=>'11.png'),
	'12'=>array('name'=>'Private Messaging','image'=>'12.png'),
	'13'=>array('name'=>'Public Messaging','image'=>'13.png'),
	'15'=>array('name'=>'Surveys','image'=>'15.png'),
	'16'=>array('name'=>'Documents','image'=>'16.png'),
	'17'=>array('name'=>'Social','image'=>'17.png'),
	'43'=>array('name'=>'Sponsors','image'=>'43.png'),
	'44'=>array('name'=>'Twitter Feed','image'=>'44.png'),
	'45'=>array('name'=>'Activity','image'=>'45.png'),
	'46'=>array('name'=>'Instagram Feed','image'=>'46.png')
	); ?>
<div class=" ">
	<div class="row">
		<div class="col-md-12">
			<div class="visitors-header-block">
				<div class="row">
					<div class="col-sm-2">
						<div class="logoblock">
							<img src="<?php echo base_url().'assets/images/all-in-loop-logo-white.png'; ?>">
						</div>
					</div>
					<div class="col-sm-7">
						<ul class="step_name_ul">
							<li class="active">
								<a href="javascript:void(0);" id="frist_page_screen_link" style="text-decoration: underline !important;">1. Appearance <i class="fa fa-check" id="step_one_li" style="display: none;"></i></a>
							</li>
							<li class="">
								<a href="javascript:void(0);" id="second_page_screen_link">2. Select Features <i class="fa fa-check" id="step_two_li" style="display: none;"></i></a>
							</li>
							<li class="">
								<a href="javascript:void(0);" id="three_page_screen_link">3. Add Content <i class="fa fa-check" id="step_three_li" style="display: none;"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form role="form" id="org_valid_form" enctype="multipart/form-data" method="post" action="<?php echo base_url().'secondvisitorevent/visitor_event_create_account' ?>">
	<section class="col-sm-12" id="app_first_screen_section">
		<div class="left_side_form col-sm-3">
			<h1>1. Appearance</h1>
			<h4>Choose colors, select icons and add images</h4>
			<h3>Name Your App</h3>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="text" name="App_name" id="App_name" class="form-control name_group required">
					<span for="App_name" class="help-block" id="App_name_error_span" style="display: none;">This field is required.</span>
				</div>
			</div>
			<h3>Appearance</h3>
			<div class="form-group color-pick">
				<label for="form-field-1" class="control-label col-sm-12">Pick a Color Scheme : </label>
				<div class="col-sm-12">
					<?php $color_arr=array('#e64d43_#bf3a31','#e47f31_#d05419','#fecc2f_#fda729','#efdeb6_#d4c297','#35495d_#2e3e4f','#2b2b2b_#262626','#995cb3_#8c48ab','#3c6f80_#366271','#3b99d8_#2f80b6','#38ca73_#30ad64','#2abb9b_#239f86','#ecf0f1_#bec3c7','#95a5a5_#7e8c8d','#365e42_#2f5038','#7460c2_#5b4ba0','#5e4535_#513b2e','#5d345d_#4e2c4e','#ed737c_#d7555c','#a6c446_#90af30','#f27ec2_#d25e9d','#78302c_#652724','#a18672_#8d725e','#b8caef_#9aabd3','#5266a0_#394d7f');
					foreach ($color_arr as $key => $value) { ?>
					<div class="col-sm-2">
	                    <div>
		                    <input type="radio" value="<?php echo $value; ?>" <?php if($key==8){ ?> checked="checked" <?php } ?>  name="custom_color_picker" id="radio<?php echo $key; ?>">
		                    <label class="control-label col-sm-12" for="radio<?php echo $key; ?>" style="background-color: <?php $arr=explode("_",$value); echo $arr[0]; ?>"></label>
	                    </div>
	                </div>
	                <?php } ?>
				</div>
			</div>
            <h3>Edit Colors individually below</h3>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-12">Header Color : </label>
				<div class="col-sm-12">
					<input type="text" name="header_color" id="header_color" value="#3b99d8" class="color {hash:true} form-control name_group">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-12">Header Text Color : </label>
				<div class="col-sm-12">
					<input type="text" name="header_text_color" id="header_text_color" class="color {hash:true} form-control name_group">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-12">Left Hand Menu Color : </label>
				<div class="col-sm-12">
					<input type="text" name="left_hand_menu_color" value="#2f80b6" id="left_hand_menu_color" class="color {hash:true} form-control name_group">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-12">Left Hand Menu Text Color : </label>
				<div class="col-sm-12">
					<input type="text" name="left_hand_menu_text_color" id="left_hand_menu_text_color" class="color {hash:true} form-control name_group">
				</div>
			</div>
		</div>
		<div class="center_side_form_live_device_preview col-sm-6">
			<div class="iphone-screen2">
            	<div class="inner-wrap">
            	<div class="logo" id="logo_image_div">
                	<div class="placeholder-text" id="placeholder_text_div">Click here to add your Logo</div>
                   <img id="logo_preview_change" src="https://s-media-cache-ak0.pinimg.com/736x/9b/7f/5c/9b7f5cc7089f0125897bc7c74c2443e6.jpg" style="display: none;">
                </div>
                <div class="logo-tagline">
                   	<a href="javascript:void(0);" id="app_name_link_show">App Name</a>
                </div>
                <div class="menu">
                	<ul>
                    	<li style="color:#fff;background-color: #2f80b6;">Agenda</li>
						<li style="color:#fff;background-color: #2f80b6;">Presentation</li>
						<li style="color:#fff;background-color: #2f80b6;">Maps</li>
						<li style="color:#fff;background-color: #2f80b6;">Make Notes</li>
						<li style="color:#fff;background-color: #2f80b6;">Public Messaging</li>
						<li style="color:#fff;background-color: #2f80b6;">Sponsors</li>
						<li style="color:#fff;background-color: #2f80b6;">Feedback</li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="iphone-screen1">
            <div class="inner-wrap">
            	<div class="header" style="text-align: left;background:#3b99d8;">
            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
                	<!-- <i class="fa fa-th-large" style="color:#fff;"></i> -->
                    <span class="text" style="color:#fff;">Login</span>
                </div>
                <div class="banner" id="banner_images_div">
                	<div class="placeholder-text" id="header_placeholder_div">Click here to add your banner</div>
                    <img width="100%" id="header_div_images" src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQhYSpXbyD9kvuYc395DTR4ascx5pReN6MBh0cz_H6LHb_Zol2Mf56wrrrv"  style="display: none;">
                </div>
                <div class="home-screen-text" id="show_home_screen_text_div" data-toggle="modal" data-target="#edit_home_screen_text_popup">
                    <p id="show_home_screen_text_p">Click here to add your home screen text</p>
                </div>
                <div class="icon-list">
                	<ul id="iconset_one_icon">
                		<?php foreach ($iconset as $key => $value) { ?>
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>
                    		</li>
                    	<?php } ?>
                    </ul>
                    <ul id="iconset_two_icon" style="display: none;"> 
                    	<?php foreach ($iconset as $key => $value) { ?>		
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>	
                    		</li>
                    	<?php } ?>
                    </ul>
                    <ul id="iconset_thard_icon" style="display: none;"> 
                    	<?php foreach ($iconset as $key => $value) { ?>		
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>
                    		</li>
                    	<?php } ?>
                    </ul>
                </div>
            </div>
            </div>
		</div>
		<div class="right_side_form col-sm-3">
			<h3>Choose Icon Designs for your App</h3>
			<span style="display: inline-block;margin: 0 0 15px 15px;">You can upload your own icons when you Add Content.</span>
			<div class="form-group">
				<div class="col-sm-12">
					<div class="col-sm-4 col-xs-4" id="open_to_everyone_div" style="padding-left:0px;">
	                    <div>
		                    <input type="radio" value="1" checked="checked"  name="icon_set_type" id="radio01">
		                    <label for="radio01">
		                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-1.jpg'; ?>">
		                    </label>
	                    </div>
	                </div>
	                <div class="col-sm-4 col-xs-4" id="requires_login_div">
	                    <div>
		                    <input type="radio" value="2" name="icon_set_type" id="radio02">
		                    <label for="radio02">
		                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-2.jpg'; ?>">
		                    </label>
	                    </div>
	                </div>
	                <div class="col-sm-4 col-xs-4" id="private_div">
	                    <div>
		                    <input type="radio" value="3" name="icon_set_type" id="radio03">
		                    <label for="radio03">
		                    	<img src="<?php echo base_url().'assets/css/images/icons/radio-button-3.jpg'; ?>">
		                    </label>
	                    </div>
	                </div>
				</div>
			</div>
			<label for="form-field-1" class="control-label col-sm-12">Icon Background color : </label>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="text" name="icon_backgroung_color" value="#3b99d8" id="icon_backgroung_color" class="color {hash:true} form-control name_group">
				</div>
			</div>
			<h3>App Images and Text</h3>
			<div class="form-group">
				<div class="modal fade" id="logomodelscopetool" role="dialog">
				    <div class="modal-dialog modal-sm">
				    <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close close_logo_popup" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Event Logo</h4>
				        </div>
				        <div class="modal-body" id="show_logoupload_div_data">
				        	<div class="row"  id="cropping_logo_div">
				            	<div id="preview_logo_crop_tool">
				            		<img class="img-responsive" id="show_crop_img_model" src="" alt="Picture">
				            	</div>
				            	<div class="col-sm-3 crop-btn">
				            		<button class="btn btn-green btn-block" id="upload_result_btn_crop" data-dismiss="modal">Crop</button>   
				            	</div>
				            </div>
				            
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default close_logo_popup" data-dismiss="modal">Exit crop tool</button>
				        </div>
				    </div>
				    </div>
				</div>
				<div class="col-sm-12">
					<div data-provides="fileupload" class="fileupload fileupload-new">
						<span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Edit Logo Image</span><span class="fileupload-exists">Change</span>
							<input type="file" name="logo_image" id="logo_image">
						</span>
						<span class="fileupload-preview"></span>
						<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
							&times;
						</a>
					</div>
					<input type="hidden" id="logo_crope_images_text" name="logo_crope_images_text" value="">
				</div>
			</div>
			<div class="form-group">
				<div class="modal fade" id="headermodelscopetool" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Banner Image</h4>
				        </div>
				        <div class="modal-body">
			                <div class="row"  id="cropping_banner_div">
				            	<div id="preview_header_crop_tool">
				            		<img class="img-responsive" id="show_crop_banner_model" src="" alt="Picture">
				            	</div>
				            	<div class="col-sm-3 header_crop_btn">
				            		<button class="btn btn-green btn-block" id="upload_result_btn_header_crop" data-dismiss="modal">Crop</button>   
				            	</div>
				            </div>
				              
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default close_banner_popup" data-dismiss="modal">Exit crop tool</button>
				        </div>
				    </div>
				    </div>
				</div>
				<div class="col-sm-12">
					<div data-provides="fileupload" class="fileupload fileupload-new">
						<span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Edit Header Image</span><span class="fileupload-exists">Change</span>
							<input type="file" name="header_image" id="header_image">
						</span>
						<span class="fileupload-preview"></span>
						<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
							&times;
						</a>
					</div>
					<input type="hidden" id="header_crope_images_text" name="header_crope_images_text" value="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<button type="button" name="Edit_home_screen_text" id="Edit_home_screen_text" data-toggle="modal" data-target="#edit_home_screen_text_popup" class="btn btn-light-grey btn-block">Edit Home Screen Text</button>
				</div>
				<div id="edit_home_screen_text_popup" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Edit Home Screen Text</h4>
				      </div>
				      <div class="modal-body">
				      	<textarea name="edit_home_screen_content" id="edit_home_screen_content" class="summernote form-control"></textarea>
				      	<textarea name="edit_home_screen_html_text" id="edit_home_screen_html_text" style="display: none;" class="form-control"></textarea>
				      	<div class="row" style="margin-top: 15px;">
				      		<div class="col-sm-4">
							<button type="button" name="submit_home_screen_text" id="submit_home_screen_text" class="btn btn-green btn-block">Add To Your App</button>
							</div>
						</div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<!-- <h3>Like What You See?</h3> -->
			<span style="padding-left: 15px;display: inline-block;">If you are happy with how your App looks you can click Next to select Features. Don't worry! You can change the appearance of your App at anytime!</span>
			<div class="form-group">
				<div class="col-sm-12">
					<button type="button" name="next_step" id="fisrt_screen_next_btn" class="btn btn-green btn-block">Next: Select Features</button>
				</div>
			</div>
		</div>
	</section>
	<section class="col-sm-12" id="app_content_second_screen_section" style="display:none;">
		<div class="col-sm-7">
			<a href="javascript:void(0);" id="back_btn_second_screen"><img src="<?php echo base_url(); ?>assets/images/backbutton.png"> Go Back to Appearance</a>
			<h1>2. Select Features</h1>
			<p>Select the features you would like to include in your App. You can update these at any time. Content can be added in the Builder.</p>
			<div class="row">
				<div class="col-sm-12">
					<div class="tabbable tabs-left">
						<ul id="visitor_event_second_screen_tab" class="nav nav-tabs">
							<li class="active">
								<a href="#app_content_data_section" data-toggle="tab">
									App Content
								</a>
							</li>
							<li class="">
								<a href="#interactive_content_data_section" data-toggle="tab">
									Interactive Features
								</a>
							</li>
							<li class="">
								<a href="#event_content_data_section" data-toggle="tab">
									Event Features
								</a>
							</li>
							<li>
								<div class="add-btn">
									<button type="button" name="lets_active" id="lets_active" data-toggle="modal" data-target="#lets_active_popup_form" data-backdrop="static" class="btn btn-green btn-block">Register Your App</button>
								</div>	
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="app_content_data_section">
								<div class="col-sm-12">
									<div class="row">
										<div class="tabbing-content-row">

											<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor advertising icon.png'; ?>" /></div>
											<div class="content-info">
												<label class="control-label" id="label_5">Advertising</label>
												<p>Show advert banners within your App.</p>
											</div>
				                            <div class="onoffswitch">
				                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="advertising_active" value="5">
				                                <label class="onoffswitch-label" for="advertising_active">
				                                  <span class="onoffswitch-inner"></span>
				                                  <span class="onoffswitch-switch"></span>
				                                 </label>
				                            </div>
					                        
				                            <div class="edit-labeltxt">
				                            	<input type="hidden" value="Advertising" name="5" id="textbox_5">
				                            </div>
				                            
			                            </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor maps icon.png'; ?>" /></div>
                                        <div class="content-info">
												<label class="control-label" id="label_10">Maps</label>
												<p>Include Google Maps and Interactive Maps</p>
											</div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important" type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="maps_active" value="10">
			                                <label class="onoffswitch-label" for="maps_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
                                        
				                        <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('10');"><i class="fa fa-pencil"></i> Change Name</a>
			                            	<input type="hidden" value="Maps" name="10" id="textbox_10">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor social icon.png'; ?>" /></div>
                                          <div class="content-info">
												<label class="control-label" id="label_17">Social</label>
												<p>Include your social media links</p>
											</div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="social_active" value="17">
			                                <label class="onoffswitch-label" for="social_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
                                      
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('17');"><i class="fa fa-pencil"></i> Change Name</a>
			                            	<input type="hidden" value="Social" name="17" id="textbox_17">
			                            </div>
			                            
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor presentations icon.png'; ?>" /></div>
                                        <div class="content-info">
				                        <label class="control-label" id="label_9">Presentations</label>
                                        <p>Add Powerpoint presentations</p>
                                        </div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="presentations_active" value="9">
			                                <label class="onoffswitch-label" for="presentations_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
                                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('9');"><i class="fa fa-pencil"></i> Change Name</a>
			                            	<input type="hidden" value="Presentations" name="9" id="textbox_9">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor documents icon.png'; ?>" /></div>
                                        <div class="content-info">
				                        <label class="control-label" id="label_16">Documents</label>
                                        <p>Share multiple documents</p>
                                        </div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="documents_active" value="16">
			                                <label class="onoffswitch-label" for="documents_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
                                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('16');"><i class="fa fa-pencil"></i> Change Name</a>
			                            	<input type="hidden" value="Documents" name="16" id="textbox_16">
			                            </div>
                                        </div>
                					</div>
								</div>	
							</div>
							<div class="tab-pane fade in" id="interactive_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    	<div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor notes icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        <label class="control-label" id="label_6">Notes</label>
                                        <p>Allow Users to make notes.</p>
                                        </div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="notes_active" value="6">
			                                <label class="onoffswitch-label" for="notes_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('6');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Notes" name="6" id="textbox_6">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor private messaging icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        	<label class="control-label" id="label_12">Private Messaging</label>
                                        <p>Allow Users to message privately.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important" type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="private_messaging_active" value="12">
			                                <label class="onoffswitch-label" for="private_messaging_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('12');"><i class="fa fa-pencil"></i> Change Name</a>
			                            	<input type="hidden" value="Private Messaging" name="12" id="textbox_12">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor surveys icon.png'; ?>" /></div>
                                        <div class="content-info">
                                        	<label class="control-label" id="label_15">Surveys</label>
                                        <p>Include polls and surveys.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="surveys_active" value="15">
			                                <label class="onoffswitch-label" for="surveys_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('15');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Surveys" name="15" id="textbox_15">
			                            </div>
			                            </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor twitter feed icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_44">Twitter Feed</label><p>Include a Twitter Feed.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="twitter_feed_active" value="44">
			                                <label class="onoffswitch-label" for="twitter_feed_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('44');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Twitter Feed" name="44" id="textbox_44">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor instagram icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_46">Instagram Feed</label><p>Include an Instagram Feed.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="instagram_feed_active" value="46">
			                                <label class="onoffswitch-label" for="instagram_feed_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('46');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Instagram Feed" name="46" id="textbox_46">
			                            </div>
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor photos icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_11">Photos</label><p>Allow Users to share photos on a photo feed.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="photos_active" value="11">
			                                <label class="onoffswitch-label" for="photos_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('11');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Photos" name="11" id="textbox_11">
			                            </div>
                                        </div>
                                        </div>
                					
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor public messaging icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_13">Public Messaging</label><p>Allow Users share messages on a public message feed.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="public_messaging_active" value="13">
			                                <label class="onoffswitch-label" for="public_messaging_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('13');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Public Messaging" name="13" id="textbox_13">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor form builder icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_26">Form Builder</label><p>Include contact forms to generate leads.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="form_builder_active" value="26">
			                                <label class="onoffswitch-label" for="form_builder_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<input type="hidden" value="Form Builder" name="26" id="textbox_26">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor activity icon.png'; ?>" /></div>
                                         <div class="content-info">  <label class="control-label" id="label_45">Activity Feed</label><p>Show a full activity feed of all Users.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="activity_feed_active" value="45">
			                                <label class="onoffswitch-label" for="activity_feed_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                      
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('45');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Activity Feed" name="45" id="textbox_45">
			                            </div>
			                           
                                        </div>
                					</div>
                                    </div>
								</div>
						
							<div class="tab-pane fade in" id="event_content_data_section">
								<div class="col-sm-12">
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor agenda icon.png'; ?>" /></div>
			                            <div class="content-info"><label class="control-label" id="label_1">Agenda</label><p>Add multiple Agendas and sessions.</p></div>
                                        <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="agenda_active" value="1">
			                                <label class="onoffswitch-label" for="agenda_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('1');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Agenda" name="1" id="textbox_1">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor exhibitors icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_3">Exhibitors</label><p>Manage and include an Exhibitor Directory.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important" type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="exhibitors_active" value="3">
			                                <label class="onoffswitch-label" for="exhibitors_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('3');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Exhibitors" name="3" id="textbox_3">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    	<div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor sponsors icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_43">Sponsors</label><p>Manage and include a Sponsor Directory.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="sponsors_active" value="43">
			                                <label class="onoffswitch-label" for="sponsors_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('43');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Sponsors" name="43" id="textbox_43">
			                            </div>
			                            
                                        </div>
                					</div>
									<div class="row">
                                    <div class="tabbing-content-row">
										<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor attendees icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_2">Attendees</label><p>Manage and include an Attendee Directory.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="attendees_active" value="2">
			                                <label class="onoffswitch-label" for="attendees_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('2');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Attendees" name="2" id="textbox_2">
			                            </div>
			                            
                                        </div>
                					</div>
                					<div class="row">
                                    <div class="tabbing-content-row">
                						<div class="icon_img_modules"><img src="<?php echo base_url().'assets/css/images/icontwostep/visitor speakers icon.png'; ?>" /></div>
                                        <div class="content-info"><label class="control-label" id="label_7">Speakers</label><p>Manage and include a Speaker Directory.</p></div>
			                            <div class="onoffswitch">
			                                <input style="display:none !important"  type="checkbox" class="onoffswitch-checkbox" name="active_modules_arr[]" id="speakers_active" value="7">
			                                <label class="onoffswitch-label" for="speakers_active">
			                                  <span class="onoffswitch-inner"></span>
			                                  <span class="onoffswitch-switch"></span>
			                                 </label>
			                            </div>
				                        
			                            <div class="edit-labeltxt">
			                            	<a href="javascript:void(0);" data-toggle="modal" data-target="#edit_label_form_popup" onclick="show_label_values('7');"><i class="fa fa-pencil"></i>  Change Name</a>
			                            	<input type="hidden" value="Speakers" name="7" id="textbox_7">
			                            </div>
			                            </div>
                					</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="second_screen_text_div"><p>Register your app to save all changes, start adding your content and managing all of your features before you launch. You can edit anything instantly from the Content Management System.</p></div>
                    <div class="add-btn">
                    	<button type="button" name="lets_active" id="lets_active" data-toggle="modal" data-target="#lets_active_popup_form" data-backdrop="static" class="btn btn-green btn-block">Register Your App</button>
                    </div>
				</div>
			</div>	
		</div>
		<div class="col-sm-5 visitor-events-second-screen-second-sec">
			<div class="center_side_form_live_device_preview">
			<div class="iphone-screen2">
            	<div class="inner-wrap">
            	<div class="logo" id="logo_image_div_second">
                	<div class="placeholder-text" id="placeholder_text_div_second">Click here to add your Logo</div>
                        <img id="logo_preview_change_second" src="" style="display: none;">
                </div>
                <div class="logo-tagline">
                   	<a href="javascript:void(0);" id="app_name_link_show_second">App Name</a>
                </div>
                <div class="menu">
                	<ul id="left_hand_menu_ul">
                    	<li style="color:#fff;background-color: #2f80b6;">Agenda</li>
						<li style="color:#fff;background-color: #2f80b6;">Presentation</li>
						<li style="color:#fff;background-color: #2f80b6;">Maps</li>
						<li style="color:#fff;background-color: #2f80b6;">Make Notes</li>
						<li style="color:#fff;background-color: #2f80b6;">Public Messaging</li>
						<li style="color:#fff;background-color: #2f80b6;">Sponsors</li>
						<li style="color:#fff;background-color: #2f80b6;">Feedback</li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="iphone-screen1">
            <div class="inner-wrap">
            	<div class="header" style="text-align: left; background:#3b99d8;">
            		<img width="20" height="20" src="<?php echo base_url().'assets/images/menu-button.png'; ?>">
                	<!-- <i class="fa fa-th-large" style="color:#fff;"></i> -->
                    <span class="text" style="color:#fff;">Login</span>
                </div>
                <div class="banner" id="banner_images_div_second">
                	<div class="placeholder-text" id="header_placeholder_div_second">Click here to add your banner</div>
                    <img width="100%" id="header_div_images_second" src=""  style="display: none;">
                </div>
                <div class="home-screen-text" id="show_home_screen_text_div_second">
                    <p id="show_home_screen_text_p_second">Click here to add your home screen text</p>
                </div>
                <div class="icon-list">
                	<ul id="iconset_one_icon_second">
                		<?php  foreach ($iconset as $key => $value) { ?>
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon1/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>
                    		</li>
                    	<?php } ?>
                    </ul>
                    <ul id="iconset_two_icon_second" style="display: none;"> 
                    	<?php  foreach ($iconset as $key => $value) { ?>		
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon2/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>	
                    		</li>
                    	<?php } ?>
                    </ul>
                    <ul id="iconset_thard_icon_second" style="display: none;"> 
                    	<?php foreach ($iconset as $key => $value) { ?>		
                    		<li style="background:#3b99d8;">
                    			<div class="event-img">
                    				<img src="<?php echo base_url(); ?>assets/css/images/icons/icon3/<?php echo $value['image']; ?>" alt="">
                    			</div>	
                    			<div class="event-tile">
                    				<p class="icon_modules_label_<?php echo $key; ?>"><?php echo $value['name']; ?></p>
                    			</div>
                    		</li>
                    	<?php } ?>
                    </ul>
                </div>
            </div>
		</div>
		</div>
		
		<div id="lets_active_popup_form" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close close_resiter_close_popup" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Your App is ready to Publish!</h4>
			      </div>
				  <span class="first_span">It looks like you are ready to start adding your content! </span>
				  <span class="first_span">What happens now?</span>
				  
			      <div class="modal-body">
				  
				  
			      <div class="row">
					<div class="col-sm-12">
					
						<div class="col-sm-6" style="padding-left:0px;">
							<div class="form-group">
								<div class="col-sm-12">
								    <img style="width: 15%;" src="<?php echo base_url(); ?>assets/images/paint-tool.png">
									<span class="second_span">To keep your design and upload your content so your App is ready to publish you will need to save your App by filling in the form.</span>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-12">
								    <img style="width: 18%;" src="<?php echo base_url(); ?>assets/images/laptop-tool.png">
									<span class="second_span">You will be taken directly to the Content Management System where you can add your content. You can log back in and out with the credentials you set here at anytime and you won't lose anything.</span>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-12">
								    <img style="width: 35%;" src="<?php echo base_url(); ?>assets/images/btowser-third.jpg">
									<span class="second_span">When you are ready to Publish you can launch your App on iOS and Android. In the meantime we will provide you with a link for you to view your App on your phone.</span>
								</div>
							</div>
						</div>
					
						<div class="col-sm-6" style="padding-right:0px;">
							<h4 class="create_free">Register and Publish</h4>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="org_first_name" placeholder="First Name" id="org_first_name" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="org_last_name" placeholder="Last Name" id="org_last_name" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="org_acc_name" placeholder="Company/Account Name" id="org_acc_name" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="email" name="org_email" placeholder="Email Address" id="org_email" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="password" name="org_password" placeholder="Password" id="org_password" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="password" name="org_confirm_password" id="org_confirm_password" placeholder="Confirm Password" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="org_phone_no" placeholder="Phone Number" id="org_phone_no" class="form-control name_group required">
								</div>
							</div>
							<div class="form-group">
								<?php $countrylist = $this->profile_model->countrylist(); ?>
								<div class="col-sm-12">
									<select name="org_country" class="form-control" id="org_country">
										<option value="">Select Country</option>
										<?php foreach ($countrylist as $key => $value) { ?>
										<option value="<?php echo $value['id']; ?>" <?php if($value['id']==$user[0]->Country){ ?> selected="selected" <?php } ?>><?php echo $value['country_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
							  <div class="col-md-6">
								<span class="terms">By clicking Create you agree to the<a href="https://www.allintheloop.com/terms.html" target="_blank" style="text-decoration: underline !important;color: blue;">Terms</a></span>
							  </div>
							  <div class="col-md-6">
								<button class="btn btn-green btn-block create-btn" type="submit" id="active_your_app" disabled="disabled">Create</button>
							  </div>
							</div>
						</div>
					</div>
			      </div>	
				  
				  
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="modal fade" id="edit_label_form_popup" role="dialog">
		    <div class="modal-dialog modal-sm">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Edit Label</h4>
		        </div>
		        <div class="modal-body">
		        	<div class="row">
		        		<div class="form-group">
				      		<div class="col-sm-12">
								<input type="text" name="Edit_label_popup_textbox" id="Edit_label_popup_textbox" class="form-control name_group">
								<input type="hidden" name="Edit_label_popup_textbox_id" id="Edit_label_popup_textbox_id" value="">
								<span for="Edit_label_popup_textbox" class="help-block" id="error_Edit_label_span" style="display:none;">Please specify your Label Value.</span>
							</div>
			      		</div>
			      		<div class="form-group">
			              <div class="col-md-6">
			                <button class="btn btn-green btn-block" type="button" id="Save_label_values"style="margin-top:20px;">
			                  Save <i class="fa fa-arrow-circle-right"></i>
			                </button>
			              </div>
		            	</div>
		        	</div>
		        </div>
		      </div>
		    </div>
  		</div>
	</section>
	</form>
</div>
</body>
</html>
<div class="modal fade" id="leads_models" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Lead Information</h4>
			</div>
			<div class="modal-body">
				<div class="row" id="lead_form_div">
					<div class="col-sm-12">
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="lead_name" placeholder="Name" id="lead_name" class="form-control name_group required">
								<span for="lead_name" class="help-block" id="lead_name_error_span" style="display: none;"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="email" name="lead_email" placeholder="Email Address" id="lead_email" class="form-control name_group required">
								<span for="lead_email" class="help-block" id="lead_email_error_span" style="display: none;"></span>
							</div>
						</div>
						<div class="form-group">
			            	<div class="col-md-12">
			            		<button class="btn btn-green btn-block" type="button" id="save_lead_form_btn" style="margin-top:20px;" disabled="disabled">
			                		Save <i class="fa fa-arrow-circle-right"></i>
			                	</button>
			            	</div>
		            	</div>	
					</div>
				</div>
				<div class="row" id="submit_msg_show_div" style="display: none;">
					<h4 style="text-align: center;">Please continue on desktop to start building your app.</h4>
					<div class="form-group">
		            	<div class="col-md-12">
		            		<button class="btn btn-green btn-block" data-dismiss="modal" type="button" style="width: 120px;margin:0 auto;">
		                		Start 
		                	</button>
		            	</div>
	            	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	$('#leads_models').modal('show');
});
var BASE_URLS="<?php echo base_url(); ?>";
var libgcolor ='';
$(window).load(function(){
	//document.body.style.zoom="75%";
	$('#save_lead_form_btn').removeAttr('disabled');
	$('#active_your_app').removeAttr('disabled');
});
$('#save_lead_form_btn').click(function(){
	var vali=true;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if($('#lead_name').val()=="")
	{
		$('#lead_name_error_span').html('This field is required.');
		$('#lead_name_error_span').parent().parent().removeClass('has-success');
		$('#lead_name_error_span').parent().parent().addClass('has-error');
		$('#lead_name_error_span').show();
		vali=false;
	}
	else if($('#lead_name').val().length < 3)
	{
		$('#lead_name_error_span').html('Please enter at least 3 characters.');
		$('#lead_name_error_span').parent().parent().removeClass('has-success');
		$('#lead_name_error_span').parent().parent().addClass('has-error');
		$('#lead_name_error_span').show();
		vali=false;
	}
	else
	{
		$('#lead_name_error_span').parent().parent().removeClass('has-error');
		$('#lead_name_error_span').parent().parent().addClass('has-success');
		$('#lead_name_error_span').hide();
		vali=true;
	}

	if($('#lead_email').val()=="")
	{
		$('#lead_email_error_span').html('This field is required.');
		$('#lead_email_error_span').parent().parent().removeClass('has-success');
		$('#lead_email_error_span').parent().parent().addClass('has-error');
		$('#lead_email_error_span').show();
		vali=false;
	}
	else if(!re.test($('#lead_email').val()))
	{
		$('#lead_email_error_span').html('Please enter a valid email address.');
		$('#lead_email_error_span').parent().parent().removeClass('has-success');
		$('#lead_email_error_span').parent().parent().addClass('has-error');
		$('#lead_email_error_span').show();
		vali=false;
	}
	else
	{
		$('#lead_email_error_span').parent().parent().removeClass('has-error');
		$('#lead_email_error_span').parent().parent().addClass('has-success');
		$('#lead_email_error_span').hide();
		vali=true;
	}

	if(vali==true)
	{
		$.ajax({
			url:"<?php echo base_url().'Secondvisitorevent/save_lead_data'; ?>",
			type:'post',
			data:'user_name='+$('#lead_name').val()+'&user_email='+$('#lead_email').val(),
			success:function(data){
				$('#submit_msg_show_div').show();
				$('#lead_form_div').hide();
				//$('#leads_models').modal('hide');
			}
		});
	}
});
$("input[name='custom_color_picker']").click(function(){
	var color_arr=$(this).val().split('_');
	$('#header_color').val(color_arr[0]);
	$('#header_color').css('background-color',color_arr[0]);
	$('#left_hand_menu_color').val(color_arr[1]);
	$('#left_hand_menu_color').css('background-color',color_arr[1]);
	$('.header').css('background-color',color_arr[0]);
	$('.menu').find('li').css('background-color',color_arr[1]);
	$('#icon_backgroung_color').val(color_arr[0]);
	$('#icon_backgroung_color').css('background-color',color_arr[0]);
	$('.icon-list').find('li').css('background-color',color_arr[0]);
});
$('#header_color').change(function(){
	$('.header').css('background-color',$('#header_color').val());
});	
$('#header_text_color').change(function(){
	$('.header').find('span').css('color',$('#header_text_color').val());
	$('.header').find('i').css('color',$('#header_text_color').val());
});
$('#left_hand_menu_color').change(function(){
	$('.menu').find('li').css('background-color',$('#left_hand_menu_color').val());
});	
$('#left_hand_menu_text_color').change(function(){
	$('.menu').find('li').css('color',$('#left_hand_menu_text_color').val());
});	
$('#icon_backgroung_color').change(function(){
	$('.icon-list').find('li').css('background-color',$('#icon_backgroung_color').val());
});

$('#submit_home_screen_text').click(function(){
	var content=$('#edit_home_screen_content').code();
	$('#edit_home_screen_html_text').val(content);
	$('#show_home_screen_text_p').html(content);
	$('#show_home_screen_text_p_second').html(content);
	$('#edit_home_screen_text_popup').modal('hide');
});
$("input[name='icon_set_type']").click(function(){
	if($(this).val()==1)
	{
		$('#iconset_one_icon').show();
		$('#iconset_two_icon').hide();
		$('#iconset_thard_icon').hide();
		$('#iconset_one_icon_second').show();
		$('#iconset_two_icon_second').hide();
		$('#iconset_thard_icon_second').hide();
	}
	else if($(this).val()==2)
	{
		$('#iconset_one_icon').hide();
		$('#iconset_two_icon').show();
		$('#iconset_thard_icon').hide();
		$('#iconset_one_icon_second').hide();
		$('#iconset_two_icon_second').show();
		$('#iconset_thard_icon_second').hide();
	}
	else
	{
		$('#iconset_one_icon').hide();
		$('#iconset_two_icon').hide();
		$('#iconset_thard_icon').show();
		$('#iconset_one_icon_second').hide();
		$('#iconset_two_icon_second').hide();
		$('#iconset_thard_icon_second').show();
	}
});
$('#App_name').keydown(function(){
	if($.trim($('#App_name').val())!="")
	{
		$('#app_name_link_show').html($.trim($('#App_name').val()));
		$('#app_name_link_show_second').html($.trim($('#App_name').val()));
	}
	else
	{
		$('#app_name_link_show').html("App Name");
		$('#app_name_link_show_second').html("App Name");
	}
});
$('#App_name').blur(function(){
	if($.trim($('#App_name').val())!="")
	{
		$('#app_name_link_show').html($.trim($('#App_name').val()));
		$('#app_name_link_show_second').html($.trim($('#App_name').val()));
	}
	else
	{
		$('#app_name_link_show').html("App Name");
		$('#app_name_link_show_second').html("App Name");
	}
});
$('#banner_images_div').click(function(){
	$('#header_image').trigger('click');
});
$('#logo_image_div').click(function(){
	$('#logo_image').trigger('click');
});
$('#fisrt_screen_next_btn').click(function(){
        libgcolor=$('#left_hand_menu_ul').find('li').attr('style');
	if($.trim($('#App_name').val())!="")
	{
		$('#App_name_error_span').parent().parent().removeClass('has-error');
		$('#App_name_error_span').hide();
		$('#app_first_screen_section').hide();
		$('#app_content_second_screen_section').show();
		$('#frist_page_screen_link').attr('style','');
		$('#second_page_screen_link').attr('style','text-decoration: underline !important;');
		$('#step_one_li').show();
		$('#step_two_li').parent().parent().addClass('active');
	}
	else
	{
		$('#App_name_error_span').parent().parent().addClass('has-error');
		$('#App_name_error_span').show();
	}
});
function show_label_values(id)
{
	$('#Edit_label_popup_textbox_id').val(id);
	$('#Edit_label_popup_textbox').val($.trim($('#textbox_'+id).val()));
}
$('#Save_label_values').click(function(){
	var id=$('#Edit_label_popup_textbox_id').val();
	if($.trim($('#Edit_label_popup_textbox').val())!="")
	{
		$('#label_'+id).text($('#Edit_label_popup_textbox').val());
		$('.icon_modules_label_'+id).html($('#Edit_label_popup_textbox').val());
		$('#textbox_'+id).val($('#Edit_label_popup_textbox').val());
		var libgcolor=$('#left_hand_menu_ul').find('li').attr('style');
		var li='';
		if(id!="on" && id!=5 && id!=26){
			$('.icon-list > ul').find('li').hide();
		}
		$("input[type=checkbox]:checked").each(function(){
			if($(this).val()!="on" && $(this).val()!=5 && $(this).val()!=26)
			{
				li+="<li style='"+libgcolor+"'>";
				li+=$('#textbox_'+$(this).val()).val();
				li+="</li>";
				$('.icon_modules_label_'+$(this).val()).parent().parent().show();
			}
		});
		if(li!="")
		{
			$('#left_hand_menu_ul').html(li);
		}
		$('#edit_label_form_popup').modal('hide');
	}
	else
	{
		$('#error_Edit_label_span').parent().parent().addClass('has-error');
		$('#error_Edit_label_span').show();
	}
});
$('#Edit_label_popup_textbox').keydown(function(){
	if($.trim($('#Edit_label_popup_textbox').val())!="")
	{
		$('#error_Edit_label_span').parent().parent().removeClass('has-error');
		$('#error_Edit_label_span').hide();
	}
	else
	{
		$('#error_Edit_label_span').parent().parent().addClass('has-error');
		$('#error_Edit_label_span').show();
	}
});
$('#lets_active').click(function(){
	$('#second_page_screen_link').attr('style','');
	$('#three_page_screen_link').attr('style','text-decoration: underline !important;');
	$('#step_two_li').show();
});
$('#frist_page_screen_link').click(function(){
	$('#app_first_screen_section').show();
	$('#app_content_second_screen_section').hide();
	$('#frist_page_screen_link').attr('style','text-decoration: underline !important;');
	$('#second_page_screen_link').attr('style','');
	$('#step_one_li').hide();
	$('#step_two_li').parent().parent().removeClass('active');
	$('#step_two_li').hide();
});
$('#back_btn_second_screen').click(function(){
	$('#app_first_screen_section').show();
	$('#app_content_second_screen_section').hide();
	$('#frist_page_screen_link').attr('style','text-decoration: underline !important;');
	$('#second_page_screen_link').attr('style','');
	$('#step_one_li').hide();
	$('#step_two_li').parent().parent().removeClass('active');
	$('#step_two_li').hide();
});
$('#second_page_screen_link').click(function(){
    libgcolor=$('#left_hand_menu_ul').find('li').attr('style');
	if($.trim($('#App_name').val())!="")
	{	
		$('#App_name_error_span').parent().parent().removeClass('has-error');
		$('#App_name_error_span').hide();
		$('#app_first_screen_section').hide();
		$('#app_content_second_screen_section').show();
		$('#frist_page_screen_link').attr('style','');
		$('#second_page_screen_link').attr('style','text-decoration: underline !important;');
		$('#step_one_li').show();
		$('#step_two_li').parent().parent().addClass('active');
	}
	else
	{
		$('#App_name_error_span').parent().parent().addClass('has-error');
		$('#App_name_error_span').show();
	}

});
$('.close_resiter_close_popup').click(function(){
	$('#three_page_screen_link').attr('style','');
	$('#second_page_screen_link').attr('style','text-decoration: underline !important;');
	$('#step_two_li').hide();
});

$("input[type=checkbox]").click(function(){
	var li='';
	if($(this).val()!="on" && $(this).val()!=5 && $(this).val()!=26){
		$('.icon-list > ul').find('li').hide();
	}
	$("input[type=checkbox]:checked").each(function(){
		if($(this).val()!="on" && $(this).val()!=5 && $(this).val()!=26)
		{
			li+="<li style='"+libgcolor+"'>";
			li+=$('#textbox_'+$(this).val()).val();
			li+="</li>";
			$('.icon_modules_label_'+$(this).val()).parent().parent().show();
		}
	});
	$('#left_hand_menu_ul').html(li);
});
$('.close_logo_popup').click(function(){
	$('#logo_crope_images_text').val();
});
$('.close_banner_popup').click(function(){
    $('#header_crope_images_text').val();
});
</script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
//logo image code    
var $logoimage = $("#show_crop_img_model");
var $inputLogoImage = $('#logo_image');    
$inputLogoImage.change(function(){
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
      var files = this.files;
      var file;
      if (files && files.length) {
        file = files[0];
        if (/^image\/\w+$/.test(file.type)) {
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }
          uploadedImageURL = URL.createObjectURL(file);
          $logoimage.attr('src', uploadedImageURL);
          $('#logo_preview_change').attr('src',uploadedImageURL);
          $('#logo_preview_change_second').attr('src',uploadedImageURL);
          $('#logo_preview_change').show();
          $('#placeholder_text_div').hide();
          $('#logo_preview_change_second').show();
          $('#placeholder_text_div_second').hide();
          $('#logomodelscopetool').modal('toggle');  
          
        } else {
          window.alert('Please choose an image file.');
        }
      }
});
$('#logomodelscopetool').on('shown.bs.modal', function () {
    var croppable = false;
    var $button = $('#upload_result_btn_crop');
        $logoimage.cropper({
                built: function () {
                  croppable = true;
                }
        });
        $button.on('click', function () {
                var croppedCanvas;
                if (!croppable) {
                  return;
                }
                croppedCanvas = $logoimage.cropper('getCroppedCanvas');
                $('#logo_preview_change').attr('src',croppedCanvas.toDataURL());
                $('#logo_preview_change_second').attr('src', croppedCanvas.toDataURL());
                $('#logo_crope_images_text').val(croppedCanvas.toDataURL());
                $("#logo_image").val('');
        });
    }).on('hidden.bs.modal',function(){
         $logoimage.cropper('destroy');
});

//banner image code

var $bannerimage = $("#show_crop_banner_model");
var $inputBannerImage = $('#header_image');    
$inputBannerImage.change(function(){
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
   
    
      var files = this.files;
      var file;
      if (files && files.length) {
        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }
          
          uploadedImageURL = URL.createObjectURL(file);
          $bannerimage.attr('src', uploadedImageURL);
          $('#header_div_images').attr('src',uploadedImageURL);
          $('#header_div_images_second').attr('src',uploadedImageURL);
          $('#header_placeholder_div').hide();
          $('#header_placeholder_div_second').hide();
          $('#header_div_images_second').show();
          $('#header_div_images').show();
          $('#headermodelscopetool').modal('toggle');
        } else {
          window.alert('Please choose an image file.');
        }
      }
});
$('#headermodelscopetool').on('shown.bs.modal', function () {
    var croppable = false;
    var $button = $('#upload_result_btn_header_crop');
        $bannerimage.cropper({
                built: function () {
                  croppable = true;
                }
        });
        $button.on('click', function () {
                var croppedCanvas;
                if (!croppable) {
                  return;
                }
                croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
                $('#header_div_images').attr('src',croppedCanvas.toDataURL());
                $('#header_div_images_second').attr('src', croppedCanvas.toDataURL());
                $('#header_crope_images_text').val(croppedCanvas.toDataURL());
                $("#header_image").val('');
        });
    }).on('hidden.bs.modal',function(){
         $bannerimage.cropper('destroy');
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-validation.js?<?php echo time(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	Main.init();
	FormValidator.init();
});
</script>