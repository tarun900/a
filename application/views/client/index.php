<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Organizers <span class="text-bold">List</span></h4>
                    <?php if($user->Role_name=='Administrator'){ ?>
                    <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Client/add"><i class="fa fa-plus"></i> Add Organizer</a>
                    <?php } ?>
                    <div class="panel-tools"></div>
			</div>
			<div class="panel-body">
                <?php if($this->session->flashdata('client_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('client_data'); ?> Successfully.
                </div>
                <?php } ?>
				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
					<thead>
						<tr>
                            <th>#</th>
							<th>Name</th>
							<th class="hidden-xs">Email</th>
							<th class="hidden-xs">Address</th>
                            <th>Mobile</th>
                            <th>Status</th>
                            <th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
                        for($i=0;$i<count($Clients);$i++)
						{
						?>
						<tr>
                            <td><?php echo $i+1; ?></td>
							<td><?php echo $Clients[$i]['Firstname']; ?></td>
							<td class="hidden-xs"><?php echo $Clients[$i]['Email']; ?></td>
                            <td class="hidden-xs"><?php echo $Clients[$i]['Street'].", ".$clients[$i]['Suburb']; ?></td>
                            <td><?php echo $Clients[$i]['Mobile']; ?></td>
                            <td><span class="label label-sm 
                            <?php if($Clients[$i]['Active']=='1')
                                { 
                                ?> 
                                  label-success 
                                <?php
                                }
                                else
                                {
                                    ?> label-danger 
                                <?php
                                } ?>">
                                    <?php if($Clients[$i]['Active']=='1')
                                        { 
                                        echo "Active"; 
                                        
                                        }
                                        else 
                                        { 
                                            echo "Inactive";
                                        } 
                                        ?></span>
                            </td>
                            <td>
                                <a href="profile/update/<?php echo $Clients[$i]['uid']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                <a href="javascript:;" onclick="delete_client(<?php echo $Clients[$i]['uid']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                            </td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>

<script>
    function delete_client(id)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Client/delete/"+id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->