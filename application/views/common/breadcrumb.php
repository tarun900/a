<?php $url=current_url();
$arr_path=explode('/',$url); 
$arr=array();
foreach ($arr_path as $element) 
{
    if (is_numeric($element)) 
    {
     	$arr[]=$element;
    } 
}
$module=$this->router->fetch_class();


?>
<div class="row" style="height:0px;">
	<div class="col-md-9">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url(); ?>">
					Dashboard
				</a>
			</li>
			<li class="active">
			<?php if($module=="event" || $module=="event_template") { ?>
			<a href="<?php echo current_url();?>">
				<?php echo $breadcrumb; ?>
			</a>
                        <?php } else if($module=="setting") { ?>
			
			<a href="<?php echo base_url().ucfirst($module)."/email_templates/".$arr[0]; ?>">
				<?php echo $breadcrumb; ?>
			</a>
			<?php  } else { ?>
			
			<a href="<?php echo base_url().ucfirst($module)."/index/".$arr[0]; ?>">
				<?php echo $breadcrumb; ?>
			</a>
			<?php  } ?>
			</li>
		</ol>
	</div>
</div>
