<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name'); 
$params = $this->router->fetch_class();
$params = lcfirst($params);
?>
<div class="navbar-content" id="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
        <div class="user-profile border-top padding-horizontal-10 block" style="border:none;">
            <!-- <h2>ALL<span>INTHE</span>LOOP</h2> -->
            <img style="width: 100% !important;" src="<?php echo base_url(); ?>assets/images/adminpanellogo.png">
        </div>
        <div align="center" id="event_name_left_hand_div"><?php echo $event['Event_name']; ?></div>
        <div id="alldiv">
            <a class="rama-button" href="<?=base_url().'Dashboard'?>">Main Dashboard </a>
            <br/>
            <ul class="main-navigation-menu">
                <li class="active">
                    <a href="<?=base_url()?>">
                    <i class="fa fa-chart-pie"></i> Analytics
                    </a>
                </li>
                <li class="">
                    <a href="<?=base_url()?>">
                    <i class="fa fa-chart-pie"></i> Posts
                    </a>
                </li>
                <li class="">
                    <a href="<?=base_url()?>">
                    <i class="fa fa-chart-pie"></i> Users
                    </a>
                </li>
                <li class="">
                    <a href="<?=base_url()?>">
                    <i class="fa fa-chart-pie"></i> Push Notifications
                    </a>
                </li>
                <li class="">
                    <a href="<?=base_url()?>">
                    <i class="fa fa-chart-pie"></i> Survey
                    </a>
                </li>
            </ul>
            <div class="navigation-toggler hidden-sm hidden-xs">
                <a href="#main-navbar" id="hide" class="sb-toggle-left">Hide Menu
                </a>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
              $('.closedbar').click(function(){
                $('#hide').text('Hide Menu');
                  $('#hide').val('');
              });
              $('#hide').click(function(){
                if($(this).val()=='')
                {
                    $('#hide').text('Show Menu');
                    $('#hide').val('show');
                }
                else
                {
                  $('#hide').text('Hide Menu');
                  $('#hide').val('');
                }
              });  
            });
        </script> 
        <!-- end: MAIN NAVIGATION MENU -->
    </div>
    <!-- end: SIDEBAR -->
</div>

<div class="slide-tools">
    <div class="col-xs-6 text-left no-padding">
        <!-- <a class="btn btn-sm status" href="#">
            Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
            </a> -->
    </div>
    <div class="col-xs-6 text-right no-padding">
        <a class="btn btn-sm log-out text-right" href="<?php echo base_url(); ?>Login/logout">
        <i class="fa fa-power-off"></i> Log Out
        </a>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
    $('.sidebar_lunch_your_app_btn').fancybox({
      'type' : 'iframe',
      openEffect  : 'none',
      closeEffect : 'none',
      mouseWheel : false,
    });
        if($('#checkbox_values').length > 0){
          var checkval = $('#checkbox_values').val();
          var allVals = checkval.split(',');
        }
        else
        {
          var allVals = new Array();
        }
    
    
        function updatecheckbox()
        {
          var urlcheck="<?php echo $this->uri->segment(1).'/'.$this->uri->segment(2); ?>";
          var removeid=$(this).val();
          var active_home_screen_tab;
            if (jQuery.inArray($(this).val(), allVals) != "-1")
            {
                var removeItem = $(this).val();
                allVals = jQuery.grep(allVals, function(value) {
                    return value != removeItem;
                });
                active_home_screen_tab='0';
            }
            else
            {
                allVals.push($(this).val());
                active_home_screen_tab='1';
            }
            $('#checkbox_values').val(allVals);
            $.ajax({
                url: "<?php echo base_url(); ?>Event/edit_checkbox/<?php echo $event['Id']; ?>",
                            data: 'checkbox_values=' + $("#alldiv #checkbox_values").val()+'&active_home_screen_menu='+$(this).val()+'&active_home_screen_tab='+active_home_screen_tab,
                            type: "POST",
                            async: false,
                            success: function(result)
                            {
                                var values = result.split('###');
                                if (values[0] != "error")
                                {
                                    var dataitem = values[1].split(',');
                                    var dataitemlink = values[2].split(',');
    
                                    $('#alldiv input[type=checkbox]').each(function(i)
                                    {
                                        $(this).prop('checked', false);
                                        $(this).parent('div').next('a').prop('href', "javascript:void(0);");
                                        
                                        var check_id = $(this).attr('id');
                                        var check_ids = check_id.split('_');
                                        
                                        for (var j = 0; j < dataitemlink.length; j++)
                                        {
                                            var temp = dataitemlink[j].split('#');
                                            //alert(temp[0]+' '+temp[1]);
                                            if(check_ids[1] == temp[0])
                                            {
                                                $(this).prop('checked', true);
                                                $(this).parent('div').next('a').prop('href', temp[1]);
                                            }
                                        }
                                        
                                    });
                                    if($.trim(urlcheck)=="event/eventhomepage")
                                    {
                                      var url=$('#result_iframe_first').attr('src');
                                      $('#result_iframe_first').attr('src',url);
                                    }
                                }
    
                            }
                        });
                        return allVals;
                    }
                    $(function() {
                        $('#alldiv input').click(updatecheckbox);
                        updatecheckbox();
                    });
    
    function copyToClipboard(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        
        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        
        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    } 
    $('#show_my_web_app_link_btn_sliderbar').click(function(){
      $('#copy_my_web_link_btn').html('Copy My Web App Link');
    });         
    $('#copy_my_web_link_btn').click(function(){
      if(copyToClipboard(document.getElementById("show_app_link_p_tag")))
      {
        $('#copy_my_web_link_btn').html('Copied Your App Link');
      }
      else
      {
        $('#copy_my_web_link_btn').html('Copy My Web App Link');
      }
    });                
    
</script>
<style type="text/css">
    div#alldiv li{
    list-style: none;
    }
    ul.main-navigation-menu li a {
    border: 0 none;
    display: block;
    font-size: 13px;
    font-weight: 300;
    margin: 0;
    padding: 10px 15px;
    padding-left: 0px;
    position: relative;
    text-decoration: none !important;
    border-bottom: none !important;
    border-top: none !important;
    color: #000000 !important;
    }
    #alldiv input[type=checkbox]{
    float: left;
    margin-left: 0%;
    display: block;
    margin-top: 5%;
    }
    #alldiv ul.main-navigation-menu
    {
    float: left;
    padding-left: 1px;
    width: 100%;
    margin-top: 7%;
    }
</style>