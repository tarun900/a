<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name'); 
$params = $this->router->fetch_class();
$params = lcfirst($params);
?>
<?php if($this->uri->segment(3) == $news_event['Id'] && !empty($this->uri->segment(3))) : ?>
<div class="navbar-content" id="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
        <div class="user-profile border-top padding-horizontal-10 block" style="border:none;">
            <!-- <h2>ALL<span>INTHE</span>LOOP</h2> -->
            <img style="width: 100% !important;" src="<?php echo base_url(); ?>assets/images/adminpanellogo.png">
        </div>
        <div align="center" id="event_name_left_hand_div"><?php echo $event['Event_name']; ?></div>
        <div id="alldiv">
            <a class="rama-button" href="<?=base_url().'Dashboard'?>">Main Dashboard </a>
            <br/>
            <ul class="main-navigation-menu">

                <li <?= $this->uri->segment(1) == 'Leader_board' ? 'class="active"': ""?>>
                    <a href="<?=base_url().'Leader_board/index/'.$this->uri->segment(3)?>">
                    <i class="fa fa-chart-pie"></i> Analytics
                    </a>
                </li>
                <li <?= $this->uri->segment(1) == 'Attendee_admin' ? 'class="active"': ""?>>
                    <a href="<?=base_url().'Attendee_admin/index/'.$this->uri->segment(3)?>">
                    <i class="fa fa-chart-pie"></i> Users
                    </a>
                </li>
                <li <?= $this->uri->segment(1) == 'Notification' ? 'class="active"': ""?>>
                    <a href="<?=base_url().'Notification/index/'.$this->uri->segment(3)?>">
                    <i class="fa fa-chart-pie"></i> Push Notifications
                    </a>
                </li>
                <li <?= $this->uri->segment(1) == 'Survey' ? 'class="active"': ""?>>
                    <a href="<?=base_url().'Survey/index/'.$this->uri->segment(3)?>">
                    <i class="fa fa-chart-pie"></i> Survey
                    </a>
                </li>
                <li <?= $this->uri->segment(1) == 'Event' ? 'class="active"': ""?>>
                    <a href="<?=base_url().'Event/access_setting/'.$this->uri->segment(3)?>">
                    <i class="fa fa-chart-pie"></i> Access Settings
                    </a>
                </li>
            </ul>
            <div class="navigation-toggler hidden-sm hidden-xs">
                <a href="#main-navbar" id="hide" class="sb-toggle-left">Hide Menu
                </a>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
              $('.closedbar').click(function(){
                $('#hide').text('Hide Menu');
                  $('#hide').val('');
              });
              $('#hide').click(function(){
                if($(this).val()=='')
                {
                    $('#hide').text('Show Menu');
                    $('#hide').val('show');
                }
                else
                {
                  $('#hide').text('Hide Menu');
                  $('#hide').val('');
                }
              });  
            });
        </script> 
        <!-- end: MAIN NAVIGATION MENU -->
    </div>
    <!-- end: SIDEBAR -->
</div>

<div class="slide-tools">
    <div class="col-xs-6 text-left no-padding">
        <!-- <a class="btn btn-sm status" href="#">
            Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
            </a> -->
    </div>
    <div class="col-xs-6 text-right no-padding">
        <a class="btn btn-sm log-out text-right" href="<?php echo base_url(); ?>Login/logout">
        <i class="fa fa-power-off"></i> Log Out
        </a>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
    $('.sidebar_lunch_your_app_btn').fancybox({
      'type' : 'iframe',
      openEffect  : 'none',
      closeEffect : 'none',
      mouseWheel : false,
    });
        if($('#checkbox_values').length > 0){
          var checkval = $('#checkbox_values').val();
          var allVals = checkval.split(',');
        }
        else
        {
          var allVals = new Array();
        }
    
    
        function updatecheckbox()
        {
          var urlcheck="<?php echo $this->uri->segment(1).'/'.$this->uri->segment(2); ?>";
          var removeid=$(this).val();
          var active_home_screen_tab;
            if (jQuery.inArray($(this).val(), allVals) != "-1")
            {
                var removeItem = $(this).val();
                allVals = jQuery.grep(allVals, function(value) {
                    return value != removeItem;
                });
                active_home_screen_tab='0';
            }
            else
            {
                allVals.push($(this).val());
                active_home_screen_tab='1';
            }
            $('#checkbox_values').val(allVals);
            $.ajax({
                url: "<?php echo base_url(); ?>Event/edit_checkbox/<?php echo $event['Id']; ?>",
                            data: 'checkbox_values=' + $("#alldiv #checkbox_values").val()+'&active_home_screen_menu='+$(this).val()+'&active_home_screen_tab='+active_home_screen_tab,
                            type: "POST",
                            async: false,
                            success: function(result)
                            {
                                var values = result.split('###');
                                if (values[0] != "error")
                                {
                                    var dataitem = values[1].split(',');
                                    var dataitemlink = values[2].split(',');
    
                                    $('#alldiv input[type=checkbox]').each(function(i)
                                    {
                                        $(this).prop('checked', false);
                                        $(this).parent('div').next('a').prop('href', "javascript:void(0);");
                                        
                                        var check_id = $(this).attr('id');
                                        var check_ids = check_id.split('_');
                                        
                                        for (var j = 0; j < dataitemlink.length; j++)
                                        {
                                            var temp = dataitemlink[j].split('#');
                                            //alert(temp[0]+' '+temp[1]);
                                            if(check_ids[1] == temp[0])
                                            {
                                                $(this).prop('checked', true);
                                                $(this).parent('div').next('a').prop('href', temp[1]);
                                            }
                                        }
                                        
                                    });
                                    if($.trim(urlcheck)=="event/eventhomepage")
                                    {
                                      var url=$('#result_iframe_first').attr('src');
                                      $('#result_iframe_first').attr('src',url);
                                    }
                                }
    
                            }
                        });
                        return allVals;
                    }
                    $(function() {
                        $('#alldiv input').click(updatecheckbox);
                        updatecheckbox();
                    });
    
    function copyToClipboard(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        
        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        
        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    } 
    $('#show_my_web_app_link_btn_sliderbar').click(function(){
      $('#copy_my_web_link_btn').html('Copy My Web App Link');
    });         
    $('#copy_my_web_link_btn').click(function(){
      if(copyToClipboard(document.getElementById("show_app_link_p_tag")))
      {
        $('#copy_my_web_link_btn').html('Copied Your App Link');
      }
      else
      {
        $('#copy_my_web_link_btn').html('Copy My Web App Link');
      }
    });                
    
</script>
<style type="text/css">
    div#alldiv li{
    list-style: none;
    }
    ul.main-navigation-menu li a {
    border: 0 none;
    display: block;
    font-size: 13px;
    font-weight: 300;
    margin: 0;
    padding: 10px 15px;
    padding-left: 0px;
    position: relative;
    text-decoration: none !important;
    border-bottom: none !important;
    border-top: none !important;
    color: #000000 !important;
    }
    #alldiv input[type=checkbox]{
    float: left;
    margin-left: 0%;
    display: block;
    margin-top: 5%;
    }
    #alldiv ul.main-navigation-menu
    {
    float: left;
    padding-left: 1px;
    width: 100%;
    margin-top: 7%;
    }
</style>
<?php else:?>
<div class="navbar-content" id="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
        <div class="user-profile border-top padding-horizontal-10 block" style="border:none;">
            <!-- <h2>ALL<span>INTHE</span>LOOP</h2> -->
            <img style="width: 100% !important;" src="<?php echo base_url(); ?>assets/images/adminpanellogo.png">
        </div>
        <?php if(count($event) > 0){ ?>
          <div align="center" id="event_name_left_hand_div"><?php echo $event['Event_name']; ?></div>
            <?php if(($user[0]->Role_id !='7' && $user[0]->is_moderator !='1') && $user[0]->Role_id!='6') { ?>
              <div align="center" id="event_name_left_hand_div">
              <a class="btn btn-green" href="javascript:void(0);" data-toggle="modal" data-target="#show_adn_copy_app_link_model" id="show_my_web_app_link_btn_sliderbar" style="margin:10px 10px;width: 225px;color:white !important;">Show My Web App Link</a>
              <?php if(!empty($event['secure_key'])){ ?>
              <span>Your Secure Key = "<?php echo $event['secure_key']; ?>"</span>
              <?php } ?>
              </div>
            <?php } ?>

        <?php } ?>
        <div id="alldiv">
          <?php if($event['iseventfreetrial']=='1' && !empty($event['Id'])){ ?>
        <a class="rama-button" href="<?php echo base_url()."Event/eventfreetrial_previewapp/".$event['Id'];  ?>"> Preview Your App</a>
        <?php } ?>
           <?php $req_mod = $this->router->fetch_class();?>
           <?php if($user[0]->Role_id!='4' && $user[0]->Role_id!='7'){ ?>
           <a class="rama-button" href="<?php if($event_id_selected == ''){ echo base_url(); }else{ echo base_url()."Dashboard/index/".$event_id_selected; } ?>"> <?php if($req_mod == 'Dashboard') { ?> Dashboard <?php } else { ?> Main Dashboard <?php } ?> </a>
           <?php } ?>
           <br/>
           <?php if(strtolower($this->uri->segment(1)) == 'dashboard' && !empty($news_event)){?>
           <a class="btn btn-green" href="<?=base_url().'Leader_board/index/'.$news_event['Id']?>" style="margin:10px 10px;width: 225px;color:white !important;">Members Hub</a>
           <?php } ?>
           <?php if($Subdomain!=""){ ?>
           <?php if($user[0]->Role_name == 'Client') {  foreach ($event['menu_sidebar'] as $keyforfun => $valueforfun) {
            if($valueforfun->id==20){
           ?>
           <a class="rama-button" href="<?php echo base_url().'fundraising/'.$Subdomain.'/panel'; ?>" style="margin-top: 10px;"> E-Commerce Panel  </a>
          <?php } } }else { if(array_search('20', array_column_1($users_role, 'Menu_id')) != false) {?> <a class="rama-button" href="<?php echo base_url().'fundraising/'.$Subdomain.'/panel'; ?>" style="margin-top: 10px;"> E-Commerce Panel  </a> <?php } } ?>
          <?php } ?>
          <br/>
          <?php if($user[0]->Role_id=='3' && $hub_active=='1' && $this->uri->segment(1)=='dashboard'){  ?>
            <a class="rama-button" style="margin-top: 10px;" href="<?php echo base_url()."Dashboard/hubdesign/";  ?>"> Hub Design </a>
           <?php } ?>
           <ul class="main-navigation-menu"> 
            
          <?php if($event['Id']!=""){  
          if($user[0]->Role_id!='7' && $user[0]->Role_id!='6' && $user[0]->role_type!='1'){ ?> 
            <li <?php if($params=="event" && $this->uri->segment(2)=="eventhomepage"){ ?> class="active" <?php }else{ ?> <?php } ?>>
              <a href="<?php echo base_url(); ?>Event/eventhomepage/<?php echo $event['Id']; ?>">
                <i class="fa fa-home"></i> App Summary
              </a>
            </li>
             <?php if($user[0]->role_type != '1'){ ?>
          <?php  if($params=="event" && $this->uri->segment(2)=="edit"){ ?>
              <li class="active">
              <?php }else { echo "<li>";}?>
               <a href="<?php echo base_url(); ?>Event/edit/<?php echo $event['Id']; ?>"><i class="fa fa-pencil"></i><span style="padding-left: 5%;" class="title">App Design</span><i class="icon-arrow"></i></a>
              <ul class="sub-menu">
                <li> 
                  <a id="App_Name_link" onclick="hideshowdiv_in_app('app_name_section')">
                  Name
                  </a>
                </li>
                <li>
                  <a id="App_Images_link" onclick="hideshowdiv_in_app('app_images_section')" href="javascript:void(0);">
                    Images
                  </a>
                </li>
                <li>
                  <a id="App_color_link" onclick="hideshowdiv_in_app('app_colors_section')" href="javascript:void(0);">
                    Colors
                  </a>
                </li>
                <li>
                  <a id="App_icon_link" onclick="hideshowdiv_in_app('app_icons_section')" href="javascript:void(0);">
                    Icons
                  </a>
                </li>
                <li>
                  <a id="App_home_screen_link" onclick="hideshowdiv_in_app('app_home_screen_text_section')" href="javascript:void(0);">
                    Home Screen Text
                  </a>
                </li>
                <li>
                  <a id="app_advanced_design_link" onclick="hideshowdiv_in_app('app_advanced_design_section')" href="javascript:void(0);">
                    Advanced Design
                  </a>
                </li>
               </ul>
            </li> 
            <?php }} if($user[0]->Role_id=='7'){ ?>
            <li <?php if($params=="speaker_Messages"){ ?> class="active" <?php } ?>>
              <a href="<?php echo base_url(); ?>Speaker_Messages/index/<?php echo $event['Id']; ?>">
                <i class="fa fa-user"></i> Moderation Panel
              </a>
            </li>
          <?php } }
            $roleid=$user[0]->Role_id;
            $Role_name=$user[0]->Role_name;
            if($Role_name == 'Client')
            {
                $id = $event['Id'];
                $string = $event['checkbox_values'];
                $res = explode(',', $string);
                asort($res);
                $str = implode(',', $res);
                $event_id=0;

                if (isset($event['menu_sidebar']) && count($event['menu_sidebar'])>0) 
                {
                  $flag=0;$flag1=0;$flag2=0;$flag3=0;$flag4=0;$flag8=0;$acc_visi=0;
                  foreach ($event['menu_sidebar'] as $key => $value) 
                  {
                    if(!in_array($value->id, array(19,29,18)))
                    {
                        $event_id=$value->id;
                        if($acc_visi==0)
                        {$acc_visi=1;
                          ?>
                          <li <?php if($params=="event" && $this->uri->segment(2)=="access_setting"){ ?> class="active" <?php } ?>>
                            <a id="link_Access" href="<?php echo base_url().'Event/access_setting/'.$id; ?>"> 
                              <i class="fa fa-cog fa-fw"></i><span class="title">App Settings</span>
                            </a>
                          </li>
                          <?php 
                        }
                        if($value->type==1 && $flag==0) 
                        { 
                          $flag=1; 
                         if($params == 'advertising_admin' || $params == 'presentation_admin' || $params == 'map' || $params == 'document' || $params == 'social_admin'  || $params == 'cms_admin' || $params == 'document') { ?>
                          <li class="active"> 
                          <?php 
                        } 
                        else 
                        {?>
                          <li> 
                          <?php 
                        } ?>
                        <a href="javascript:void(0)"><i class="fa fa-edit"></i> <span class="title">App Content</span><i class="icon-arrow"></i></a>
                        <ul class="sub-menu">
                        <!--<li <?php //if ($params == 'cms_admin') 
                        { ?> class="active" <?php } ?>>
                        <a style="margin-left:-8%;" href="<?php //echo base_url(); ?>cms_admin/index/<?php //echo $id; ?>"><span style="padding-left: 7%;" class="title">Custom Screens</span></a>
                        </li>-->
                        <?php }
                        if ($value->type==2 && $flag1==0) 
                        { 
                          $flag1=1; if($flag==1){ ?>
                          </li>
                             </ul> 
                             <?php }   if($params == 'notes_admin' || $params == 'photos_admin' || $params == 'message' || $params == 'publicmessage' || $params == 'survey' || $params == 'formbuilder' || $params=='twitter_feed_admin' || $params=='activity_admin' || $params=='instagram_feed_admin' || $params=='facebook_feed_admin' || $params=='my_favorites_admin' || $params=='qa_admin') { ?>
                          <li class="active"> 
                          <?php 
                        } 
                        else 
                        { ?>
                          <li> 
                          <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-retweet"></i> <span class="title">  Interactive Features </span><i class="icon-arrow"></i></a>
                          <ul class="sub-menu">
                  <?php } ?>


                  <?php if ($value->type==3 && $flag2==0) 
                        {  
                          $flag2=1; if($flag1==1) { ?>
                          </li>
                          </ul>
                          <?php } if($params == 'agenda_admin' || $params == 'attendee_admin' || $params == 'exhibitor' || $params == 'speaker' || $params == 'leader_board' || $params == 'sponsors' || $params == 'virtual_supermarket_admin') { ?>
                          <li class="active"> 
                          <?php }  else { ?>
                          <li> 
                          <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-building"></i> <span class="title"> Event Features</span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                          <?php  } ?>
                        <?php if ($value->type==4 && $flag3==0) 
                        {  
                          $flag3=1; ?>
                          </li>
                          </ul>
                          <li style="display:none;">
                          <a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title"> Fundraising Features </span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                          <?php 
                        } ?> 
                        <?php if ($value->type==7 && $flag5==0) 
                        {  
                          $flag5=1; ?>
                          </li>
                          </ul>
                          <?php 
                          if($params == 'fundraising') 
                          { ?>
                            <li class="active" style="display:none;"> 
                            <?php 
                          } 
                          else 
                          { ?>
                            <li style="display:none;"> 
                    <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title">  Fundraising Features </span><i class="icon-arrow"></i></a>
                          <ul class="sub-menu">                             
                  <?php } ?>
                  <?php if ($value->type==6 && $flag4==0) 
                        {  
                          $flag4=1; ?>
                          </li>
                          </ul>
                         
                          <?php 
                          if($params == 'setting' || $params == 'notification' || $params == 'appnotitemplate') 
                          { ?>
                            <li class="active"> 
                              <?php 
                          } 
                          else 
                          { ?>
                            <li> 
                            <?php 
                          } ?>
                          <a href="javascript:void(0)"><i class="fa fa-bell"></i> <span class="title">Notifications  </span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                            <li>
                              <a href="<?php echo base_url().'Setting/fbloginsetting/'.$id; ?>">Alerts</a>
                            </li>

                          <?php 

                        } if($value->id!=14){?> 
                        <li <?php if ($pagetitle == $value->pagetitle) { ?> class="active" <?php } ?> >  

                        <!-- <input type="checkbox" <?php //echo in_array($value->id, $res) ? 'checked="checked"' : ''; ?> name="<?php //echo $value->menuname; ?>" id="checkbox_<?php //echo $value->id; ?>" value="<?php //echo $value->id; ?>"> -->
                       <?php if($value->id!=27 && $value->id!=28 && $value->id!=51){ ?>  
                         <div class="onoffswitch">
						<!--<input style="display:none !important" type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked> -->

						<input style="display:none !important" type="checkbox" class="onoffswitch-checkbox" <?php echo in_array($value->id, $res) ? 'checked="checked"' : ''; ?> name="<?php echo $value->menuname; ?>" id="checkbox_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>">
						<label class="onoffswitch-label" for="checkbox_<?php echo $value->id; ?>">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					   
					</div>
					<?php } ?>           
                          <?php if($value->id!=20):?>
                         <?php if($value->id!=27 && $value->id!=28 && $value->id!=51){ ?>
                          <a id="link_<?php echo $value->id; ?>" href="<?php echo in_array($value->id, $res) ? base_url() . ucfirst($value->menuurl) . $id : 'javascript: void(0)'; ?>"> 
                          <?php }else{?> 
                          <a id="link_<?php echo $value->id; ?>" href="<?php echo base_url() . ucfirst($value->menuurl) . $id; ?>"> 
                          <?php } ?>
                              <span class="title"> <?php echo $value->menuname; ?> </span>
                          </a>
                          <?php else:?>
                             <?php if($value->id!=27 && $value->id!=28){ ?>
                              <a id="link_<?php echo $value->id; ?>" href="<?php echo in_array($value->id, $res) ? base_url() .'fundraising/'.$Subdomain.'/panel'  : 'javascript: void(0)'; ?>"> 
                               <?php }else{?> 
                            <a id="link_<?php echo $value->id; ?>" href="<?php echo base_url() . ucfirst($value->menuurl) . $id; ?>"> 
                            <?php } ?>
                              <span class="title"> <?php echo $value->menuname; ?> </span>
                              </a>
                          <?php endif;?>
                        </li>
                        <?php }
                    }
                  }
                ?>
            </ul>
          </li>
          <?php  if($params == 'role_management') { ?>
          <li class="active"> 
          <?php } else { ?>
          <li> 
          <?php } ?>
          <?php $url=base_url().'Role_management/index/' ?> 
          <a href="<?php echo $url.$id; ?>"><i class="fa fa-group"></i> 
          <span class="title"> Administration </span>
          </a>
          </li>
          </li>
<?php } ?>
    <input type="hidden" name="checkbox_values" value="<?php echo $str; ?>" id="checkbox_values">
        <li style="display:none;" <?php if ($pagetitle == 'Setting'){ ?> class="active" <?php } ?>>
              <a href="<?php echo base_url(); ?>Setting/email_templates"><i class="fa fa-envelope"></i> 
                <span class="title"> Email Templates </span></a>
        </li>
<?php } else 
        {
           if($Role_name!='Exibitor') {
           $flag=0;$flag1=0;$flag2=0;$flag3=0;$flag4=0;$flag8=0;
           if ($users_role!='')  {
           foreach ($users_role as $key => $value) 
           { 
                        if($value['type']==1 && $flag==0) 
                        { 
                          $flag=1; 
                            if($params == 'advertising_admin' || $params == 'presentation_admin' || $params == 'map' || $params == 'document' || $params == 'social_admin'  || $params == 'cms_admin' || $params == 'document'){ ?>
                          <li class="active"> 
                          <?php 
                        } 
                        else 
                        {?>
                          <li> 
                          <?php 
                        } ?>
                        <a href="javascript:void(0)"><i class="fa fa-file-text"></i> <span class="title">App Content</span><i class="icon-arrow"></i></a>
                        <ul class="sub-menu">
                        <li <?php if ($pagetitle == 'Event') 
                        { ?> class="active" <?php } ?>>
                       <!--  <a style="margin-left:-8%;" href="<?php echo base_url(); ?>event/edit/<?php echo $value['event_id']; ?>"><i class="fa fa-pencil"></i><span style="padding-left: 7%;" class="title">Edit Your App</span></a>
          -->             <!--   <a style="margin-left:-8%;" href="<?php echo base_url(); ?>cms/index/<?php echo $id; ?>"><i class="fa fa-desktop"></i><span style="padding-left: 7%;" class="title">Create Custom Screen</span></a>
 -->                        </li>
                        <?php } 
                        if ($value['type']==2 && $flag1==0) 
                        {  
                          $flag1=1; if($flag==1){ ?>
                          </li>
                          </ul><?php } if($params == 'notes_admin' || $params == 'photos_admin' || $params == 'message' || $params == 'publicmessage' || $params == 'survey' || $params == 'formbuilder' || $params=='twitter_feed_admin' || $params=='activity_admin' || $params=='instagram_feed_admin' || $params=='facebook_feed_admin' || $params=='my_favorites_admin' || $params=='qa_admin') { ?>
                          <li class="active"> 
                          <?php 
                        } 
                        else 
                        { ?>
                          <li> 
                          <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-cog fa-fw"></i> <span class="title">  Interactive Features </span><i class="icon-arrow"></i></a>
                          <ul class="sub-menu">
                  <?php } ?>
                  <?php if ($value['type']==3 && $flag2==0) 
                        {  
                          $flag2=1;?>
                          </li>
                          </ul>
                          <?php   if($params == 'agenda_admin' || $params == 'attendee_admin' || $params == 'exhibitor' || $params == 'speaker' || $params == 'leader_board' || $params == 'sponsors' || $params == 'virtual_supermarket_admin') { ?>
                          <li class="active"> 
                          <?php 
                          } 
                          else 
                          { ?>
                          <li> 
                          <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-gavel"></i> <span class="title"> Event Features</span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                          <?php 
                        } ?>
                  <?php if ($value['type']==4 && $flag3==0) 
                        {  
                          $flag3=1;  ?>
                          </li>
                          </ul>
                          <li style="display:none;">
                          <a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title"> Fundraising Features </span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                          <?php 
                        } ?> 
                  <?php if ($value['type']==7 && $flag5==0) 
                        {  
                          $flag5=1; ?>
                          </li>
                          </ul>
                          <?php 
                          if($params == 'fundraising') 
                          { ?>
                            <li class="active" style="display:none;"> 
                            <?php 
                          } 
                          else 
                          { ?>
                            <li style="display:none;"> 
                    <?php } ?>
                          <a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title">  Fundraising Features </span><i class="icon-arrow"></i></a>
                          <ul class="sub-menu">                             
                  <?php } ?>
                  <?php if ($value['type']==6 && $flag4==0) 
                        {  
                          $flag4=1; ?>
                          </li>
                          </ul>
                          <?php 
                          if($params == 'setting' || $params == 'notification' || $params == 'appnotitemplate') 
                          { ?>
                            <li class="active"> 
                              <?php 
                          } 
                          else 
                          { ?>
                            <li> 
                            <?php 
                          } ?>
                          <a href="javascript:void(0)"><i class="fa fa-bell"></i> <span class="title">Notifications  </span><i class="icon-arrow"></i> </a>
                          <ul class="sub-menu">
                          <?php 
                        } ?>
                        <li <?php if ($pagetitle == $value['pagetitle']) { ?> class="active" <?php } ?> >  
                          
                          <?php if($value['id']!=20):?>
                            <?php if($value['id']!=14){ ?>
                          <a id="link_<?php echo $value['id']; ?>" href="<?php  echo base_url() . ucfirst($value['menuurl']) . $value['event_id'] ; ?>"> 
                              <span class="title"> <?php echo $value['menuname']; ?> </span>
                          </a>
                          <?php } ?>
                          <?php else:?>
                              <a id="link_<?php echo $value['id']; ?>" href="<?php echo  base_url() .'fundraising/'.$Subdomain.'/panel'; ?>"> 
                              <span class="title"> <?php echo $value['menuname']; ?> </span>
                              </a>
                          <?php endif;?>
                        </li>
           <?php  } ?>
           </ul>
          </li>
       <?php  } }
              else 
              {  
                $eid=$this->uri->segment(3);
                if($eid !='') { ?>
                <li class="<?php echo $params=='exhibitor_portal' ? 'active' : '' ; ?>">
                  <a href="<?php echo base_url().'Exhibitor_portal/index/'.$eid;?>"> <span class="title">My Exhibitor Profile</span></a>
                </li>
                <?php if(@$exibitor_premission[0]['add_surveys']=='1' && $event['allow_custom_survey_lead']=='1'){ ?>
                <li class="<?php echo $params=='exibitor_survey' ? 'active' : '' ; ?>">
                  <a href="<?php echo base_url().'Exibitor_survey/index/'.$eid;?>"><span class="title">Survey Questions</span></a>
                </li>
                <?php } ?>
                <li class="<?php echo $params=='exhibitor_leads' ? 'active' : '' ; ?>">
                  <a href="<?php echo base_url().'Exhibitor_leads/index/'.$eid;?>"> <span class="title">Leads</span></a>
                </li>
             <? } }
        }
        if($user[0]->Role_id=='92')
        {
          $eid=$this->uri->segment(3);
          if($eid !='') {
          ?>
          <li>
            <a href="<?php echo base_url().'Sponsors_user/index/'.$eid;?>"> <span class="title">sponsors Content</span></a>
          </li>
       <?php  } }
        ?>
      
  
  
   <?php if($this->uri->segment(2) == 'fbloginsetting') { ?>
    <li style="display:none;" class="active"> 
    <?php } else { ?>
    <li style="display:none;"> 
    <?php } ?>

      <?php if($event_id!='' && $Role_name=='Client') { ?>
      <a href="<?php echo base_url().'Setting/fbloginsetting/'.$event['Id']; ?>">
      <i class="fa fa-cog"></i><span>Settings </span>
      </a>
      <?php } ?>
    </li>
    <?php if($event_id!='' && $user[0]->Role_id!='6') { ?>
    <li>
    <a href="https://www.allintheloop.com/get-started/guide.php" target="_blank">
    <i class="fa fa-support"></i><span>Support</span>
    </a>
    </li>


     <!-- <li>
    <a href="<?php echo base_url().'Developmentusers/index/'; ?><?php echo $event['Id']; ?>" >
    <i class="fa fa-users"></i><span>Services Access Management</span>
    </a>
    </li> -->



   <?php } ?>
   <?php if($event_id!='' && $user[0]->Rid=='3' && count($iseventfreetrial) > 0){ ?>
    <li>
      <a class="btn btn-green btn-block sidebar_lunch_your_app_btn"  type="button" data-fancybox-type="iframe" href="<?php echo base_url().'Event/get_launch_your_app_function_content/'.$event['Id']; ?>" style="color:white !important;">Launch Your App</a>
    </li>
   <?php } ?>
    </ul>
	<div class="navigation-toggler hidden-sm hidden-xs">
            <a href="#main-navbar" id="hide" class="sb-toggle-left">Hide Menu
            </a>
    </div>
          </div> 
          <script type="text/javascript">
          $(document).ready(function(){
            $('.closedbar').click(function(){
              $('#hide').text('Hide Menu');
                $('#hide').val('');
            });
            $('#hide').click(function(){
              if($(this).val()=='')
              {
                  $('#hide').text('Show Menu');
                  $('#hide').val('show');
              }
              else
              {
                $('#hide').text('Hide Menu');
                $('#hide').val('');
              }
            });  
          });
          </script> 
        <!-- end: MAIN NAVIGATION MENU -->
    </div>
    <!-- end: SIDEBAR -->
</div>
<div class="modal fade" id="show_adn_copy_app_link_model" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">App Link</h4>
      </div>
      <div class="modal-body">
        <p id="show_app_link_p_tag" style="word-wrap: break-word;text-align: center;"><?php echo base_url().'App/'.$acc_name.'/'.$event['Subdomain'] ?></p>
        <a href="javascript:void(0);" id="copy_my_web_link_btn" class="btn btn-green btn-block">Copy My Web App Link</a>
      </div>
    </div>
  </div>
</div>
<div class="slide-tools">
    <div class="col-xs-6 text-left no-padding">
        <!-- <a class="btn btn-sm status" href="#">
                Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
        </a> -->
    </div>
    <div class="col-xs-6 text-right no-padding">
        <a class="btn btn-sm log-out text-right" href="<?php echo base_url(); ?>Login/logout">
            <i class="fa fa-power-off"></i> Log Out
        </a>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fancyapps-fancyBox'; ?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
$('.sidebar_lunch_your_app_btn').fancybox({
  'type' : 'iframe',
  openEffect  : 'none',
  closeEffect : 'none',
  mouseWheel : false,
});
    if($('#checkbox_values').length > 0){
      var checkval = $('#checkbox_values').val();
      var allVals = checkval.split(',');
    }
    else
    {
      var allVals = new Array();
    }


    function updatecheckbox()
    {
      var urlcheck="<?php echo $this->uri->segment(1).'/'.$this->uri->segment(2); ?>";
      var removeid=$(this).val();
      var active_home_screen_tab;
        if (jQuery.inArray($(this).val(), allVals) != "-1")
        {
            var removeItem = $(this).val();
            allVals = jQuery.grep(allVals, function(value) {
                return value != removeItem;
            });
            active_home_screen_tab='0';
        }
        else
        {
            allVals.push($(this).val());
            active_home_screen_tab='1';
        }
        $('#checkbox_values').val(allVals);
        $.ajax({
            url: "<?php echo base_url(); ?>Event/edit_checkbox/<?php echo $event['Id']; ?>",
                        data: 'checkbox_values=' + $("#alldiv #checkbox_values").val()+'&active_home_screen_menu='+$(this).val()+'&active_home_screen_tab='+active_home_screen_tab,
                        type: "POST",
                        async: false,
                        success: function(result)
                        {
                            var values = result.split('###');
                            if (values[0] != "error")
                            {
                                var dataitem = values[1].split(',');
                                var dataitemlink = values[2].split(',');

                                $('#alldiv input[type=checkbox]').each(function(i)
                                {
                                    $(this).prop('checked', false);
                                    $(this).parent('div').next('a').prop('href', "javascript:void(0);");
                                    
                                    var check_id = $(this).attr('id');
                                    var check_ids = check_id.split('_');
                                    
                                    for (var j = 0; j < dataitemlink.length; j++)
                                    {
                                        var temp = dataitemlink[j].split('#');
                                        //alert(temp[0]+' '+temp[1]);
                                        if(check_ids[1] == temp[0])
                                        {
                                            $(this).prop('checked', true);
                                            $(this).parent('div').next('a').prop('href', temp[1]);
                                        }
                                    }
                                    
                                });
                                if($.trim(urlcheck)=="event/eventhomepage")
                                {
                                  var url=$('#result_iframe_first').attr('src');
                                  $('#result_iframe_first').attr('src',url);
                                }
                            }

                        }
                    });
                    return allVals;
                }
                $(function() {
                    $('#alldiv input').click(updatecheckbox);
                    updatecheckbox();
                });

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
} 
$('#show_my_web_app_link_btn_sliderbar').click(function(){
  $('#copy_my_web_link_btn').html('Copy My Web App Link');
});         
$('#copy_my_web_link_btn').click(function(){
  if(copyToClipboard(document.getElementById("show_app_link_p_tag")))
  {
    $('#copy_my_web_link_btn').html('Copied Your App Link');
  }
  else
  {
    $('#copy_my_web_link_btn').html('Copy My Web App Link');
  }
});                

</script>
<style type="text/css">
div#alldiv li{
  list-style: none;
}
    ul.main-navigation-menu li a {
        border: 0 none;
        display: block;
        font-size: 13px;
        font-weight: 300;
        margin: 0;
        padding: 10px 15px;
        padding-left: 0px;
        position: relative;
        text-decoration: none !important;
        border-bottom: none !important;
        border-top: none !important;
        color: #000000 !important;
    }
    #alldiv input[type=checkbox]{
        float: left;
        margin-left: 0%;
        display: block;
        margin-top: 5%;
    }
    #alldiv ul.main-navigation-menu
    {
        float: left;
        padding-left: 1px;
        width: 100%;
        margin-top: 7%;
    }

</style>
<?php endif; ?>