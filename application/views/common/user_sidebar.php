<div class="navbar-content">
    <div class="main-navigation left-wrapper transition-left">
        <div class="navigation-toggler hidden-sm hidden-xs">
            <a href="#main-navbar" class="sb-toggle-left">
            </a>
        </div>
        <div class="user-profile border-top padding-horizontal-10 block">
            <div class="inline-block">
                <?php if ($user->Logo != '') { ?>
                    <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $user->Logo; ?>" class="img-circle" alt="" style="width:50px;">
                <?php } else { ?>
                    <img src="<?php echo base_url(); ?>assets/images/anonymous.jpg" style="width:50px;" class="img-circle" alt="">
                <?php } ?>
            </div>
            <div class="inline-block">
                <h5 class="no-margin"> Welcome </h5>
                <h4 class="no-margin"> 
                    <?php if ($user->Firstname != '') 
                        {
                            echo $user->Firstname;
                        } 
                        else 
                        {
                            echo $user->Company_name;
                        } 
                    ?> 
                </h4>           
            </div>
        </div>
        <ul class="main-navigation-menu">   
            <?php if ($user->Role_name == 'User' || $user->Role_name == 'Manager' || $user->Role_name == 'Social_media_manager' || $user->Role_name == 'Marketing') {  ?>

                
                <li <?php if ($pagetitle == 'Dashboard') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> <span class="title"> Dashboard </span></a>
                </li> 

                <?php if(in_array(1, $Menu_id)) { ?>

                <li <?php if ($pagetitle == 'Agenda') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Agenda">  <span class="title"> Agenda </span></a>
                </li>

                <?php } if(in_array(2, $Menu_id)) { ?>

                <li <?php if ($pagetitle == 'Attendee') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Attendee">  <span class="title"> Attendee </span></a>
                </li>

                <?php } if(in_array(3, $Menu_id)) { ?>

                <li <?php if ($pagetitle == 'Exibitors') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Exibitor">  <span class="title"> Exibitors </span></a>
                </li>

                <?php } if(in_array(4, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Sponsors') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Sponsor">  <span class="title"> Sponsors </span></a>
                </li>

                <?php } if(in_array(5, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Advertising') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Advertising">  <span class="title"> Advertising </span></a>
                </li>

                <?php } if(in_array(6, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Notes') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Notes">  <span class="title"> Notes </span></a>
                </li>

                <?php } if(in_array(7, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Speakers') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Speaker">  <span class="title"> Speakers </span></a>
                </li>

                <?php } if(in_array(8, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Leader Board') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Leader_board">  <span class="title"> Leader Board </span></a>
                </li>

                <?php } if(in_array(9, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Presentation') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Presentation">  <span class="title"> Presentation </span></a>
                </li>

                <?php } if(in_array(10, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Maps') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Map">  <span class="title"> Maps </span></a>
                </li>

                <?php } if(in_array(11, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Photos') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Photo">  <span class="title"> Photos </span></a>
                </li>

                <?php } if(in_array(12, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Messages') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Message">  <span class="title"> Messages </span></a>
                </li>

                <?php } if(in_array(13, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Surveys') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Survey">  <span class="title"> Surveys </span></a>
                </li>

                <?php } if(in_array(14, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Document') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Document">  <span class="title"> Document </span></a>
                </li>

                <?php } if(in_array(15, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Twitter') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Twitter">  <span class="title"> Twitter </span></a>
                </li>

                <?php } if(in_array(16, $permissions)) { ?>

                <li <?php if ($pagetitle == 'Facebook') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Facebook">  <span class="title"> Facebook </span></a>
                </li>

                <?php } if(in_array(17, $permissions)) { ?>

                <li <?php if ($pagetitle == 'QR Scanner') { ?> class="active" <?php } ?>>
                     <a href="<?php echo base_url(); ?>Qr_scanner">  <span class="title"> QR Scanner </span></a>
                </li>

            <?php } } ?>                                                     
        </ul>
    </div>
</div>

<div class="slide-tools">
    <div class="col-xs-6 text-right no-padding">
        <a class="btn btn-sm log-out text-right" href="<?php echo base_url(); ?>Login/logout">
            <i class="fa fa-power-off"></i> Log Out
        </a>
    </div>
</div>

<style type="text/css">
ul.main-navigation-menu li a {
border: 0 none;
display: block;
font-size: 13px;
font-weight: 300;
margin: 0;
padding: 10px 15px;
position: relative;
text-decoration: none !important;
border-bottom: none !important;
border-top: none !important;
color: #fff !important;
}
</style>