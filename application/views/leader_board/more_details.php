<!-- <link src="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
 -->
<!-- <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
 --><?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="Leader_board">
            <div class="panel-body" style="overflow-x:scroll;">
                <div class="table-responsive">
                    <?php if($is_register_user_list=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($register_user_list as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_modules_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Total Hit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($modules_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_user_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="Modules_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Modules Name</th>
                                <th>Total Hit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['menuname']); ?></td>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_os_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Device Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($os_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['device_name']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_question_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Answer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($question_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['ans']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_post_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Read Count</th>
                                <th>Avg. Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($adverts_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['view_count']; ?></td>
                                <td><?php echo $value['avg_time']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_advert_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Total Hit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($adverts_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_user_advert_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="Adverts_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Adverts Name</th>
                                <th>Total Hit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_adverts_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Advertisement_name']); ?></td>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_sponsor_visite_profile_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Total Visit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($sponsor_visite_profile_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_attendee_visit_sponsor_profile_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="Sponsors_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sponsors Name</th>
                                <th>Total Visit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($sponsors_list_visit_by_attendee as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Sponsors_name']); ?></td>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_all_user_messages_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="User_messages_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Messages</th>
                                <th>Reciver Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_messages_list as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo strip_tags($value['Message']); ?></td>
                                <td><?php echo ucfirst($value['user_name']); ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_exibitor_visite_profile_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Total Visit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($exibitor_visite_profile_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_attendee_visit_exhibitor_profile_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="Exibitor_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Exhibitor Name</th>
                                <th>Total Visit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($exhibitor_list_visit_by_attendee as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Heading']); ?></td>
                                <td><?php echo $value['total_hit']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_attendee_list_requested_meeting_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Total Meeting</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($attendee_list_requested_meeting as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php echo $value['total_meeting']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_exhibitor_list_requested_meeting_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="Exibitor_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Exhibitor Name</th>
                                <th>Total Meeting</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($exhibitor_list_requested_meeting as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Heading']); ?></td>
                                <td><?php echo $value['total_meeting']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } if($is_user_list_to_favorites_details=='1'){ ?>
                    <table class="table table-striped table-bordered table-hover table-full-width" name="user_list" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_list_to_exibitor as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo $value['Lastname']; ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo $value['Title']; ?></td>
                                <td><?php echo $value['Company_name']; ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
