<?php $acc_name=$this->session->userdata('acc_name');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newanalytics.css?'.time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css"/>
<style type="text/css">
.chart_wrap {
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.chart {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.barchart_wrap {
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.barchart {
	position: absolute;
	top: 0;
	left: 0;
	width: 90%;
	height: 100%;
}

#from_datetime,#to_datetime {
    width: 70%;
    display: inline-block;
}

.form-group span { 
    float: left;
    width: 30%;
    display: inline-block;
    text-align: right;
    padding-right: 10px;
    box-sizing: border-box;
    vertical-align: middle;
    padding-top: 5px;
 }
.panel .form-group { margin-bottom: 0px; }
.panel #refresh { color: #5381ce; }
.panel #refresh:hover, #refresh:focus { color: #5381ce; }
</style>
<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url().'Leader_board/index_new_time/'.$event_id; ?>">
    <div class="panel" style="background: #5381ce;font-size: 22px;color: #fff;padding: 15px;"> 

      <div class="col-md-3">
        <span style="float: left;">Select a Timeframe</span> 
      </div>

      <div class="col-md-9">
        <div class="analyicts_heading">
            <div class="form-group">
              <div class="col-sm-5">
                <span> From </span>
                <input type="text" name="from_datetime" id="from_datetime" contenteditable="false" class="form-control datetimepicker_coutome" value="<?php echo $from_datetime; ?>">
              </div>
              <div class="col-sm-5">
                <span> To </span>
                <input type="text" name="to_datetime" id="to_datetime" contenteditable="false" class="form-control datetimepicker_coutome" value="<?php echo $to_datetime; ?>">
              </div>
              <div class="col-sm-2">
                <input type="submit" class="btn btn-white" value="Refresh" id="refresh">
              </div>
            </div>
          </div>
      </div>

      <div style="clear:both"></div>
    </div>
    </form>
  </div>
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-body" style="padding: 0px;">
        <div class="tabbable">
          <ul id="myTab2" class="nav nav-tabs">
            <li class="active"> <a href="#app_activity" data-toggle="tab"> App Activity </a> </li>
            <li class=""> <a href="#agenda" data-toggle="tab"> Agenda </a> </li>
            <li class=""> <a href="#surveys" data-toggle="tab"> Surveys </a> </li>
            <li class=""> <a href="#contact_forms" data-toggle="tab"> Contact Forms </a> </li>
            <li class=""> <a href="#adverts" data-toggle="tab"> Adverts </a> </li>
            <li class=""> <a href="#sponsors" data-toggle="tab"> Sponsors </a> </li>
            <li class=""> <a href="#messaging" data-toggle="tab"> Messaging </a> </li>
            <li class=""> <a href="#cotact_swap" data-toggle="tab"> Contact Swap </a> </li>
            <li class=""> <a href="#exhibitors" data-toggle="tab"> Exhibitors </a> </li>
            <li class=""> <a href="#Favorites" data-toggle="tab"> Favorites </a> </li>
            <li class=""> <a href="#cms" data-toggle="tab"> CMS </a> </li>
          </ul>
        </div>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="app_activity">
            <div class="col-sm-12">
              <div class="col-sm-6 register-part">
                <h3 class="title_show1">Registrations</h3>
                <div class="row"> 
                  <span><?php echo $total_register_user[0]['total_user']; ?></span> 
                </div>
                <div class="col-sm-12"> 
                  <a href="<?php echo base_url().'Leader_board/get_all_register_user_by_event/'.$event_id; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                </div>
              </div>
              <div class="col-sm-6 are-part">
                <h3 class="title_show1">Most Popular Areas</h3>
                <?php foreach ($most_popular_area as $key => $value) { ?>
                <div class="col-sm-12" style="padding:0px;"> 
                  <span class="col-sm-4 col-xs-4"><?php echo $value['menuname']; ?></span> 
                  <span class="col-sm-4 col-xs-4 area-value">   <?php echo $value['total_menu_hit']; ?></span>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/get_most_popular_area_details/'.$event_id.'/'.$value['Id']; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="col-sm-6 user-part">
                <h3 class="title_show1">Most Active Users</h3>
                <?php foreach ($most_active_user as $key => $value) { ?>
                <div class="col-sm-12 panel-white shadow-panel"> 
                  <a href="<?php echo base_url().'Leader_board/get_most_active_user_details/'.$event_id.'/'.$value['Id']; ?>">
                  <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                    <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?>
                    <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
                    <?php }else{ ?>
                    <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                    <?php } ?>
                  </div>
                  <div class="col-sm-9 col-xs-9 user-name" style="padding:0px;">
                    <div class="col-sm-12"> <?php echo ucfirst($value['username']); ?> </div>
                    <?php if(!empty($value['Title']) && !empty($value['Company_name'])){ ?>
                    <div class="col-sm-12"> 
                      <?php echo ucfirst($value['Title']).' At '.ucfirst($value['Company_name']); ?> 
                    </div>
                    <?php } ?>
                  </div>
                  <div class="col-sm-1 col-xs-1" style="padding:0px;float:right">
                    <i class="fa fa-angle-right"></i>
                  </div>
                  </a> 
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-6 os-part">
                <h3 class="title_show1">Logins by Operating System</h3>
                <?php $devicenm=array('Web','Iphone','Android'); foreach ($most_login_os as $key => $value) { if(array_search($value['device_name'],$devicenm) !== FALSE){ unset($devicenm[array_search($value['device_name'],$devicenm)]); } ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 icn-img"> 
                    <img src="<?php echo base_url().'assets/images/'.$value['device_name'].'.png'; ?>" alt="<?php echo $value['device_name']; ?>"> 
                  </div>
                  <div class="col-sm-4 col-xs-4 user-number">
                    <?php echo $value['total_login_user']; ?>
                  </div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id.'/'.$value['device_name']; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                  </div>
                </div>
                <?php } $devicenm=array_values($devicenm); for($i=0;$i<count($devicenm);$i++) { ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 total-name" style="color:#5d93c5;"> 
                    <img src="<?php echo base_url().'assets/images/'.$devicenm[$i].'.png'; ?>" alt="<?php echo $devicenm[$i]; ?>"> 
                  </div>
                  <div class="col-sm-4 col-xs-4 total-number"><?php echo '0'; ?></div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id.'/'.$devicenm[$i]; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a>
                  </div>
                </div>
                <?php } ?>
                <div class="col-sm-12" style="padding:0px 10px;">
                  <div class="col-sm-4 col-xs-4 total-name" style="color:#5d93c5;"> Total </div>
                  <div class="col-sm-4 col-xs-4 total-number">
                    <?php echo array_sum(array_column_1($most_login_os,'total_login_user')); ?>
                  </div>
                  <div class="col-sm-4 col-xs-4"> 
                    <a href="<?php echo base_url().'Leader_board/login_os_details/'.$event_id; ?>" class="btn btn-default btn-block drill_down_button">Drill Down</a> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="agenda">
            <?php if(count($agenda) > 0) { ?>
            <div class="table-responsive">
              <table class="table data_table table-striped table-bordered table-hover table-full-width" id="sample_12">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Session Name</th>
                    <th>Date/Time</th>
                    <th>Agenda Rating</th>
                    <th>Number of Users Who Have Saved</th>
                    <th>Number of Users Who Have Checked In</th>
                    <th>Drill Down</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($agenda as $key => $value) { ?>
                  <tr>
                    <td><?php echo  $key+1 ?></td>
                    <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Heading']); ?></a></td>
                    <td><?php echo date("H:i - d/m/Y",strtotime($value['End_time'].' '.$value['End_date'])); ?></td>
                    <td><?php 
                                        $rating=$value['total_rating']/$value['no_user'];
                                        for($i=1;$i<=5;$i++){
                                            if($i<=$rating)
                                            {
                                                $img=base_url().'assets/images/star-on.png';
                                            }
                                            else
                                            {
                                                $img=base_url().'assets/images/star-off.png';
                                            }
                                            ?>
                      <img src="<?php echo $img; ?>" alert="start img"/>
                      <?php 
                                        }
                                         ?></td>
                    <td><?php /*if(!empty($value['total_save']) && !empty($value['Maximum_People'])){
                                            echo $value['total_save']."/".$value['Maximum_People']."=".number_format(($value['total_save']/$value['Maximum_People'])*100,2)."%";
                                        }*/
                                        echo !empty($value['total_save']) ? $value['total_save'] : '0';
                                         ?></td>
                    <td><?php if(!empty($value['total_check']) && !empty($value['total_save'])){ 
                                            echo $value['total_check']."/".$value['total_save']."=".number_format(($value['total_check']/$value['total_save'])*100,2)."%";
                                        }?></td>
                    <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>">More Details</a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <a class="pull-right" href="<?php echo base_url() .'Leader_board/view_more/'.$event_id.'/agenda'?>">View More agenda list</a>
            <?php }else{ ?>

            <div class="row"><h2 style="text-align: center;">No Agenda For This App.</h2></div>
            <?php } ?>
            <br>
            <div class="col-sm-12 second-row">
              <div class="col-sm-4 gray-bg">
                <h3 class="title_show1">Top Rated Sessions</h3>
                <?php if(count($top_rated_session) > 0){ ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-full-width">
                    <tbody>
                      <?php foreach ($top_rated_session as $key => $value) { ?>
                      <tr>
                        <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Heading']); ?></a></td>
                        <td><?php 
                                                $rating=$value['total_rating'];
                                                for($i=1;$i<=5;$i++){
                                                    if($i<=$rating)
                                                    {
                                                        $img=base_url().'assets/images/star-on.png';
                                                    }
                                                    else
                                                    {
                                                        $img=base_url().'assets/images/star-off.png';
                                                    }
                                                    ?>
                          <img src="<?php echo $img; ?>" alert="start img"/>
                          <?php 
                                                }
                                                ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <?php }else{ echo "<h4>No Top Rated Session For This App.</h4>"; } ?>
              </div>
              <div class="col-sm-4 gray-bg">
                <h3 class="title_show1">Most Popular Sessions</h3>
                <?php if(count($most_popular_session)>0){ ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-full-width">
                    <tbody>
                      <?php foreach ($most_popular_session as $key => $value) { ?>
                      <tr>
                        <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Heading']) ?></a></td>
                        <td><?php if(!empty($value['total_save']) && !empty($value['Maximum_People'])){
                                                    echo $value['total_save']."/".$value['Maximum_People']."=".number_format(($value['total_save']/$value['Maximum_People'])*100,2)."%";
                                                } ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <?php }else{ echo "<h4>No Most Popular Session For This App.</h4>"; } ?>
              </div>
              <div class="col-sm-4 gray-bg">
                <h3 class="title_show1">Most Checked In Sessions</h3>
                <?php if(count($most_checkin_session)>0){ ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-full-width">
                    <tbody>
                      <?php foreach ($most_checkin_session as $key => $value) { ?>
                      <tr>
                        <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Heading']); ?></a></td>
                        <td><?php if(!empty($value['total_checkin']) && !empty($value['total_save'])){
                                                    echo $value['total_checkin']."/".$value['total_save']."=".number_format(($value['total_checkin']/$value['total_save'])*100,2)."%";
                                                } ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <?php }else{ echo "<h4>No Most Check In Session For This App.</h4>"; } ?>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="surveys">
            <div class="col-sm-12">
              <?php if(count($survey_question_pie_chart)>0){ ?>
              <div class="col-sm-9" id="show_all_piechart">
                <?php foreach ($survey_question_pie_chart as $key => $value) { ?>
                <div class="col-sm-6">
                  <h3 style="text-align: center;" class="title_show"><?php echo $key; ?></h3>
                  <div class="chart_wrap" align="center">
                    <div id='piechart<?php echo $key; ?>' class="chart"></div>
                  </div>
                  <script type="text/javascript">
                  google.load("visualization", "1", {packages:["corechart"]});
                  google.setOnLoadCallback(drawChart);
                  function drawChart() {                                       
                  var data = google.visualization.arrayToDataTable([['Options', 'No. of users'],  <?php foreach ($value['chart'] as $a => $b) {?>
                  ['<?php echo $a; ?>', <?php echo $b; ?>],
                  <?php } ?>
                  ]);
                  var options = {
                  width:'100%',
                  height:'100%',
                  align: 'right', 
                  isStacked: true,   
                  pieSliceText: 'percentage',         
                  verticalAlign: 'middle',
                  pieSliceTextStyle: { color: 'black',},
                  fontSize: 20,
                  fontName:'Lato',
                  legend:{position:'right',textStyle: {color: 'blue', fontSize: 10,fontName:'Lato'}},
                  chartArea: {
                          left: "3%",
                          top: "5%",
                          height: "75%",
                          width: "94%"
                      }
                  };
                  var chart = new 
                  google.visualization.PieChart(document.getElementById('piechart<?php echo $key;?>'));
                  chart.draw(data, options);
                  }
                  </script>
                  <div class="col-sm-6 col-xs-6"> <a class="btn btn-default btn-block drill_down_button" href="<?php echo base_url().'Leader_board/get_question_details/'.$event_id.'/'.$value['Q_id']; ?>">Drill Down</a> </div>
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-9" id="show_all_barchart" style="display: none;">
                <?php foreach ($survey_question_pie_chart as $key => $value) { ?>
                <div class="col-sm-6">
                  <h3 style="text-align: center;" class="title_show"><?php echo $key; ?></h3>
                  <div class="barchart_wrap" align="center">
                    <div id='barchart<?php echo $key; ?>' class="barchart"></div>
                  </div>
                  <script type="text/javascript">
                  google.load("visualization", "1", {packages:["corechart"]});
                  google.setOnLoadCallback(drawChart);
                  function drawChart() {                                       
                  var data = google.visualization.arrayToDataTable([['Options','No. of users',{ role: 'style' }],  <?php foreach ($value['chart'] as $a => $b) {?>
                  ['<?php echo $a; ?>', <?php echo $b; ?>,'color:<?php echo sprintf("#%06x",rand(0,16777215)); ?>;'],
                  <?php } ?>
                  ]);
                  var options = {
                  width:'90%',
                  height:'100%',
                  align: 'center',           
                  verticalAlign: 'middle',
                  isStacked: true,
                  fontSize: 20,
                  fontName:'Lato',
                  legend: {position: 'none', maxLines: 3},
                  };
                  var chart = new 
                  google.visualization.BarChart(document.getElementById('barchart<?php echo $key;?>'));
                  var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                                 { calc: "stringify",
                                   sourceColumn: 1,
                                   type: "string",
                                   role: "annotation" },
                                 2]);
                  chart.draw(view, options);
                  }
                  </script>
                  <div class="col-sm-6 col-xs-6"> <a class="btn btn-default btn-block drill_down_button" href="<?php echo base_url().'Leader_board/get_question_details/'.$event_id.'/'.$value['Q_id']; ?>">Drill Down</a> </div>
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-3 chart-type">
                <h4>Change Chart Type</h4>
                <ul>
                  <li class="active"><a class="" onclick="show_chart('pie',this);" href="javascript:void(0);">Pie</a></li>
                  <li><a class="" onclick="show_chart('bar',this);" href="javascript:void(0);">Bar</a></li>
                </ul>
              </div>
              <?php }else{ ?>
              <div class="no-survey">
                <h4>No Surveys Question For This App.</h4>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="tab-pane fade" id="contact_forms">
            <div class="col-sm-12">
              <?php if(count($forms_data) > 0): foreach($forms_data as $intKey=>$strval): ?>
              <div class="col-sm-6">
                <div class="gray-bg">
                  <?php
                  $json_data = false; $formid="form"; $temp_form_id=rand(1,9999);
                  $json_data = isset($strval) ? $strval['json_data'] : FALSE;
                  $loader = new formLoader($json_data,base_url().'Leader_board/index_new/'.$event_id,$formid.$temp_form_id);
                  $loader->render_form();
                  unset($loader);
                  ?>
                </div>
                <div class="col-sm-12"> <a class="btn btn-green btn-block view-result" href="<?php echo base_url().'Leader_board/download_form_data_csv/'.$event_id.'/'.$strval['id']; ?>">View Result</a> </div>
              </div>
            
                <?php endforeach;  else: echo "<h4>No Form Builder For This App.</h4>"; endif;?>
           
            </div>
          </div>
    
        <div class="tab-pane fade" id="adverts">
          <?php if(count($advert_click_data) > 0){ ?>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Advert Name</th>
                  <th>Total Click</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($advert_click_data as $key => $value) { ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <td><a href="<?php echo base_url().'Leader_board/get_advert_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Advertisement_name']); ?></a></td>
                  <td><span><?php echo $value['total_hit']!=""? $value['total_hit'] : '0'; ?></span> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_advert_details/'.$event_id.'/'.$value['Id']; ?>">Drill Down</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php }else{ ?> <div class="row"><h2 style="text-align: center;">No Adverts For This App.</h2></div> <?php  } ?>
          <div class="col-sm-12 advert-part">
            <div class="col-sm-6 advert-left gray-bg">
              <h3 class="title_show1">Most Popular Adverts</h3>
              <?php if(count($most_popular_advert) > 0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($most_popular_advert as $key => $value) { ?>
                    <tr>
                      <td><a href="<?php echo base_url().'Leader_board/get_advert_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Advertisement_name']); ?></a></td>
                      <td><?php echo $value['total_hit']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No Most Popular Adverts For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 advert-right gray-bg">
              <h3 class="title_show1">People Who Clicked On Adverts</h3>
              <?php if(count($advert_click_user) > 0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($advert_click_user as $key => $value) { ?>
                    <tr>
                      <td><a href="<?php echo base_url().'Leader_board/get_user_advert_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['user_name']); ?></a></td>
                      <td><?php echo $value['total_hit']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No People Click On Adverts For This App.</h4>"; } ?>
            </div>
          </div>
        </div>

        <div class="tab-pane fade" id="cms">
          <?php if(count($cms_click_data) > 0){ ?>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
              <thead>
                <tr>
                  <th>#</th>
                  <th>CMS Name</th>
                  <th>Total Click</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($cms_click_data as $key => $value) { ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <td><a href="<?php echo base_url().'Leader_board/get_cms_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Menu_name']); ?></a></td>
                  <td><span><?php echo $value['total_hit']!=""? $value['total_hit'] : '0'; ?></span> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_cms_details/'.$event_id.'/'.$value['Id']; ?>">Drill Down</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php }else{ ?> <div class="row"><h2 style="text-align: center;">No CMS For This App.</h2></div> <?php  } ?>
          <div class="col-sm-12 advert-part">
            <div class="col-sm-6 advert-left gray-bg">
              <h3 class="title_show1">Most Popular CMS</h3>
              <?php if(count($most_popular_cms) > 0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($most_popular_cms as $key => $value) { ?>
                    <tr>
                      <td><a href="<?php echo base_url().'Leader_board/get_cms_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Menu_name']); ?></a></td>
                      <td><?php echo $value['total_hit']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No Most Popular CMS For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 advert-right gray-bg">
              <h3 class="title_show1">People Who Clicked On CMS</h3>
              <?php if(count($cms_click_user) > 0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($cms_click_user as $key => $value) { ?>
                    <tr>
                      <td><?php echo ucfirst($value['user_name']); ?></td>
                      <td><?php echo $value['total_hit']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No People Click On CMS For This App.</h4>"; } ?>
            </div>
          </div>
        </div>

        <div class="tab-pane fade" id="sponsors">
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Sponsor Profiles By Views</h3>
              <?php if(count($top_profileviews_sponsors)>0){ foreach ($top_profileviews_sponsors as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $clogo=json_decode($value['company_logo'],true); $Logo=$clogo[0]; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['Sponsors_name']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_attendee_visited_sponsord_profiles_details/'.$event_id.'/'.$value['Id']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Sponsors Profile Views For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Attendees who have Visited Sponsor Profiles</h3>
              <?php if (count($visit_soonsor_profiles_attendee)>0) { ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($visit_soonsor_profiles_attendee as $key => $value) { ?>
                    <tr>
                      <td><?php echo $value['user_name']; ?></td>
                      <td><?php echo $value['total_hit']; ?></td>
                      <td><a href="<?php echo base_url().'Leader_board/get_attendee_visited_sponsors_profiles_details/'.$event_id.'/'.$value['Id']; ?>">More details</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No Attendees Visited Sponsor Profiles.</h4>"; } ?>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="messaging">
          <div class="col-sm-12">
            <div class="col-sm-3 pull-right" style="margin-bottom: 15px;margin-right: 1%;">
              <a href="<?php echo base_url().'Leader_board/export_message_data/'.$event_id; ?>" class="btn btn-green btn-block">Export message data</a>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg msgbox1">
              <h3 class="title_show1">Users who sent the most Messages</h3>
              <?php if (count($most_user_messages)>0) { foreach ($most_user_messages as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel"> <a href="<?php echo base_url().'Leader_board/get_all_user_msg_by_event/'.$event_id.'/'.$value['Id']; ?>">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-2 col-xs-2 user-number"> <?php echo $value['total_private_msg'].' Private'; ?> </span> <span class="col-sm-2 col-xs-2 user-number"> <?php echo $value['total_public_msg'].' Public'; ?> </span> <span class="col-sm-2 col-xs-2 total-number"> <?php echo $value['total_msg']=='' ? 0 : $value['total_msg']; ?> </span> </a> </div>
              <?php } }else{ echo "<h4>No Messages For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Users who made the most Comments on Photos</h3>
              <?php if(count($most_user_photos_comment)>0){ foreach ($most_user_photos_comment as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-6 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-4 col-xs-4 user-number"> <?php echo $value['total_comment']; ?> </span> </div>
              <?php } }else{ echo "<h4>No Comments For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Users who made the most Comments on Messages</h3>
              <?php if(count($most_user_msg_comment)>0){ foreach ($most_user_msg_comment as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-6 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-4 col-xs-4 user-number"> <?php echo $value['total_comment']; ?> </span> </div>
              <?php } }else{ echo "<h4>No Comments For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Users who made the most Comments on Activity</h3>
              <?php if(count($most_user_activity_comment)>0){ foreach ($most_user_activity_comment as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-6 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-4 col-xs-4 user-number"> <?php echo $value['total_comment']; ?> </span> </div>
              <?php } }else{ echo "<h4>No Comments For This App.</h4>"; } ?>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Users who Shared the most Photos</h3>
              <?php if(count($most_user_shared_photos)){ foreach ($most_user_shared_photos as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-6 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-4 col-xs-4 user-number"> <?php echo $value['total_photos']; ?> </span> </div>
              <?php } }else{ echo "<h4>No Shared Photos For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Users who made the most Likes</h3>
              <?php if(count($most_user_likes)>0){ foreach ($most_user_likes as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2" style="padding-right:0px;">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-6 user-name">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-4 col-xs-4 user-number"> <?php echo $value['total_likes']; ?> </span> </div>
              <?php } }else{ echo "<h4>No  User Likes For This App.</h4>"; } ?>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="cotact_swap">
          <div class="col-sm-10 gray-bg">
            <h3 class="title_show1">Users Who Have Swapped Contact Details</h3>
            <?php if(count($swap_contact_user)>0){ foreach ($swap_contact_user as $key => $value) { ?>
            <div class="col-sm-12 panel-white shadow-panel">
              <div class="col-sm-4">
                <div class="col-sm-3 col-xs-3">
                  <?php if(!empty($value['from_logo']) && file_exists('./assets/user_files/'.$value['from_logo'])){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$value['from_logo']; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-9 col-xs-9 user-name" style="padding:0px;">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['from_name']); ?> </div>
                  <?php if(!empty($value['from_title']) && !empty($value['from_company_name'])){ ?>
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['from_title']).' At '.ucfirst($value['from_company_name']); ?> </div>
                  <?php } ?>
                </div>
              </div>
              <div class="col-sm-3">
                <h5 class="swap-txt">Swapped With</h5>
              </div>
              <div class="col-sm-5">
                <div class="col-sm-3 col-xs-3">
                  <?php if(!empty($value['to_logo']) && file_exists('./assets/user_files/'.$value['to_logo'])){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$value['to_logo']; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-9 col-xs-9 user-name" style="padding:0px;">
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['to_name']); ?> </div>
                  <?php if(!empty($value['to_title']) && !empty($value['to_company_name'])){ ?>
                  <div class="col-sm-12" style="padding:0px;"> <?php echo ucfirst($value['to_title']).' At '.ucfirst($value['to_company_name']); ?> </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <?php } }else{ echo "<h4>No Swapped Contact Details For This App.</h4>"; } ?>
          </div>
        </div>
        <div class="tab-pane fade" id="exhibitors">
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Exhibitor Profiles By Visit</h3>
              <?php if(count($top_profileviews_exibitor)>0){ foreach ($top_profileviews_exibitor as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $clogo=json_decode($value['company_logo'],true); $Logo=$clogo[0]; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['Heading']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_attendee_visited_exibitor_profiles_details/'.$event_id.'/'.$value['Id']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Exhibitor Profiles Visit.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Attendees Who Have Visited Exhibitor Profiles</h3>
              <?php if(count($visit_exhibitor_profiles_attendee)>0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($visit_exhibitor_profiles_attendee as $key => $value) { ?>
                    <tr>
                      <td><?php echo $value['user_name']; ?></td>
                      <td><?php echo $value['total_hit']; ?></td>
                      <td><a href="<?php echo base_url().'Leader_board/get_exhibitor_list_visited_attendee_details/'.$event_id.'/'.$value['Id']; ?>">More details</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <a href="<?php echo base_url() .'Leader_board/view_more/'.$event_id.'/exhibitors'?>">View More</a>
              <?php }else{ echo "<h4>No Attendees Visited Exhibitor Profiles.</h4>"; } ?>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Exhibitor Profiles By Meeting Requests</h3>
              <?php if(count($top_exibitor_have_request_meeting)){ foreach ($top_exibitor_have_request_meeting as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $clogo=json_decode($value['company_logo'],true); $Logo=$clogo[0]; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['Heading']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_meeting']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_attendee_request_meeting_exibitor/'.$event_id.'/'.$value['Id']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Exhibitor Requests Meeting For This App.</h4>"; } ?>
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Attendees Who Have Requested Meeting</h3>
              <?php if(count($top_requested_meeting_attendee)>0){ ?>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width">
                  <tbody>
                    <?php foreach ($top_requested_meeting_attendee as $key => $value) { ?>
                    <tr>
                      <td><?php echo $value['user_name']; ?></td>
                      <td><?php echo $value['total_meeting']; ?></td>
                      <td><a href="<?php echo base_url().'Leader_board/get_exhibitor_list_requested_meeting/'.$event_id.'/'.$value['Id']; ?>">More details</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php }else{ echo "<h4>No Attendees Requested Meeting.</h4>"; } ?>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="Favorites">
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Exhibitor By Favorited</h3>
              <?php if(count($top_exibitor_by_favorited)>0){ foreach ($top_exibitor_by_favorited as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $clogo=json_decode($value['company_logo'],true); $Logo=$clogo[0]; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['Heading']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_user_list_to_favorites/'.$event_id.'/'.$value['Id'].'/'.$value['module_type']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Exibitor Favorite For This App.</h4>"; } ?>  
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Sponsors By Favorited</h3>
              <?php if(count($top_sponsors_by_favorited)>0){ foreach ($top_sponsors_by_favorited as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $clogo=json_decode($value['company_logo'],true); $Logo=$clogo[0]; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['Sponsors_name']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_user_list_to_favorites/'.$event_id.'/'.$value['Id'].'/'.$value['module_type']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Sponsors Favorite For This App.</h4>"; } ?> 
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Speakers By Favorited</h3>
              <?php if(count($top_speakers_by_favorited)>0){ foreach ($top_speakers_by_favorited as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_user_list_to_favorites/'.$event_id.'/'.$value['Id'].'/'.$value['module_type']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Speakers Favorite For This App.</h4>"; } ?> 
            </div>
            <div class="col-sm-6 gray-bg">
              <h3 class="title_show1">Top Attendees By Favorited</h3>
              <?php if(count($top_attendee_by_favorited)>0){ foreach ($top_attendee_by_favorited as $key => $value) { ?>
              <div class="col-sm-12 panel-white shadow-panel">
                <div class="col-sm-2 col-xs-2">
                  <?php $Logo=$value['Logo']; if(!empty($Logo) && file_exists('./assets/user_files/'.$Logo)){ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/user_files/'.$Logo; ?>">
                  <?php }else{ ?>
                  <img width="50px" height="50px" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
                  <?php } ?>
                </div>
                <div class="col-sm-4 col-xs-4 user-name" style="padding:0px;">
                  <div class="col-sm-12"> <?php echo ucfirst($value['user_name']); ?> </div>
                </div>
                <span class="col-sm-3 col-xs-3 user-number"> <?php echo $value['total_hit']=='' ? 0 : $value['total_hit']; ?> </span>
                <div class="col-sm-3 col-xs-3 drillbtn" style="padding:0px;"> <a class="pull-right" href="<?php echo base_url().'Leader_board/get_user_list_to_favorites/'.$event_id.'/'.$value['Id'].'/'.$value['module_type']; ?>">Drill Down</a> </div>
              </div>
              <?php } }else{ echo "<h4>No Attendees Favorite For This App.</h4>"; } ?> 
            </div>
          </div>      
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>--> 
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script> 
<script>
    jQuery(document).ready(function() {
        //Charts.init();
        jQuery('.datetimepicker_coutome').datetimepicker({
          formatTime:'H:i',
          formatDate:'m/d/Y',
          step:5
        });
    });
    function  show_chart(type,e) 
    {
        if(type=="pie")
        {
            $('#show_all_piechart').show();
            $('#show_all_barchart').hide();
        }
        else if(type=="column")
        {
            $('#').show();
            $('#').hide();
        }
        else
        {
            $('#show_all_piechart').hide();
            $('#show_all_barchart').show();
        }
        $(e).parent().parent().find('li').removeClass('active');
        $(e).parent().addClass('active');
    }
</script>

