<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="Leader_board">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2 pull-right">
                        <a href="<?php echo base_url().'Leader_board/export_agenda_details/'.$this->uri->segment(3).'/'.$this->uri->segment(4); ?>" class="btn btn-green btn-block">Export</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Job Title</th>
                                <th>Company Name</th>
                                <th>Country</th>
                                <?php foreach ($keysdata as $key => $value) { ?>
                                <th><?php echo ucfirst($value['column_name']); ?></th>
                                <?php } ?>
                                <th>Session Rating</th>
                                <th>Save Agenda</th>
                                <th>Check In</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $ai=1; foreach ($agenda_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $ai;$ai++; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo ucfirst($value['Lastname']); ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php echo ucfirst($value['Title']); ?></td>
                                <td><?php echo ucfirst($value['Company_name']); ?></td>
                                <td><?php echo $value['Country']; ?></td>
                                <?php for($j=0;$j<count($keysdata);$j++){
                                $custom=json_decode($value['extra_column'],TRUE);
                                $keynm=$keysdata[$j]['column_name']; ?>
                                <td><?php echo $custom[$keynm]; ?></td>
                                <?php } ?>
                                <td><?php 
                                        for($i=1;$i<=5;$i++){
                                            if($i<=$value['rating'])
                                            {
                                                $img=base_url().'assets/images/star-on.png';
                                            }
                                            else
                                            {
                                                $img=base_url().'assets/images/star-off.png';
                                            }
                                            ?>
                                            <img src="<?php echo $img; ?>" alert="start img"/>
                                            <?php 
                                        }
                                         ?>
                                        </td>
                                <td><?php 
                                $arr=explode(",",$value['agenda_id']);
                                   if(in_array($agendaid,$arr)){
                                    echo "<h3><i class='fa fa-check'></i></h3>";
                                    }else{
                                        echo "<h3><i class='fa fa-times'></i></h3>";
                                    } ?></td>
                                <td><?php 
                                $arr=explode(",",$value['check_in_agenda_id']);
                                   if(in_array($agendaid,$arr)){
                                    echo "<h3><i class='fa fa-check'></i></h3>";
                                    }else{
                                        echo "<h3><i class='fa fa-times'></i></h3>";
                                    } ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
