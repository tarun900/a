<?php $acc_name=$this->session->userdata('acc_name');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/newanalytics.css?'.time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css"/>
<style type="text/css">
.chart_wrap {
  
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.chart {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.barchart_wrap {
	position: relative;
	padding-bottom: 90%;
	height: auto;
	overflow-x: visible;
}
.barchart {
	position: absolute;
	top: 0;
	left: 0;
	width: 90%;
	height: 100%;
}

#from_datetime,#to_datetime {
    width: 70%;
    display: inline-block;
}

.form-group span { 
    float: left;
    width: 30%;
    display: inline-block;
    text-align: right;
    padding-right: 10px;
    box-sizing: border-box;
    vertical-align: middle;
    padding-top: 5px;
 }
.panel .form-group { margin-bottom: 0px; }
.panel #refresh { color: #5381ce; }
.panel #refresh:hover, #refresh:focus { color: #5381ce; }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-body" style="padding: 0px;">
        <div class="tabbable">
          <ul id="myTab2" class="nav nav-tabs">
            <li class="active"> <a href="#app_activity" data-toggle="tab"> <?php echo ucfirst($type) ?> List</a> </li>
          </ul>
          <a href="<?php echo base_url() .'Leader_board/index/'.$event_id.''?>" class="btn btn-primary pull-right" style="    margin-top: -40px;
    margin-right: 10px;">Back</a>
        </div>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="app_activity">
            <?php
              switch ($type) {
                case 'agenda': ?>
                    <?php if(count($agenda) > 0) { ?>
                    <div class="table-responsive">
                      <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6 pull-right text-right">
                         
                            <form action="<?php echo base_url().'Leader_board/view_more/'.$event_id.'/agenda' ?>" method="post" accept-charset="utf-8">
                              <label>Search:</label> <input type="text" name="akeyword" value="<?php echo $akeyword; ?>">
                              <button type="submit" class="btn btn-primary" name="submit" value=""><i class="fa fa-search"></i></button>
                              </form>
                            
                        </div>
                      </div>
                      <table class="table table-striped table-bordered table-hover table-full-width" id="data_table1">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Session Name</th>
                            <th>Date/Time</th>
                            <th>Agenda Rating</th>
                            <th>Number of Users Who Have Saved</th>
                            <th>Number of Users Who Have Checked In</th>
                            <th>Drill Down</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($agenda as $key => $value) { ?>
                          <tr>
                            <td><?php echo ($this->uri->segment(5)!='')  ? ($this->uri->segment(5))+$key+1 :  $key+1 ?></td>
                            <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>"><?php echo ucfirst($value['Heading']); ?></a></td>
                            <td><?php echo date("H:i - d/m/Y",strtotime($value['End_time'].' '.$value['End_date'])); ?></td>
                            <td><?php 
                                                $rating=$value['total_rating']/$value['no_user'];
                                                for($i=1;$i<=5;$i++){
                                                    if($i<=$rating)
                                                    {
                                                        $img=base_url().'assets/images/star-on.png';
                                                    }
                                                    else
                                                    {
                                                        $img=base_url().'assets/images/star-off.png';
                                                    }
                                                    ?>
                              <img src="<?php echo $img; ?>" alert="start img"/>
                              <?php 
                                                }
                                                 ?></td>
                            <td><?php /*if(!empty($value['total_save']) && !empty($value['Maximum_People'])){
                                                    echo $value['total_save']."/".$value['Maximum_People']."=".number_format(($value['total_save']/$value['Maximum_People'])*100,2)."%";
                                                }*/
                                                echo !empty($value['total_save']) ? $value['total_save'] : '0';
                                                 ?></td>
                            <td><?php if(!empty($value['total_check']) && !empty($value['total_save'])){ 
                                                    echo $value['total_check']."/".$value['total_save']."=".number_format(($value['total_check']/$value['total_save'])*100,2)."%";
                                                }?></td>
                            <td><a href="<?php echo base_url().'Leader_board/agenda_details/'.$event_id.'/'.$value['Id']; ?>">More Details</a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      <div class="pull-right">
                                <?php if (isset($links)) { ?>
                                    <?php echo $links ?>
                                <?php } ?>
                            </div>
                    </div>
                    <?php }else{ ?>
                    <div class="row"><h2 style="text-align: center;">No Agenda For This App.</h2></div>
                    <?php } ?>
                <?php break;
                case 'exhibitors': ?>
                    <?php if(count($visit_exhibitor_profiles_attendee)>0){ ?>
                    <div class="table-responsive">
                      <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6 pull-right text-right">
                      <form action="<?php echo base_url().'Leader_board/view_more/'.$event_id.'/exhibitors' ?>" method="post" accept-charset="utf-8">
                              <label>Search:</label> <input type="text" name="ekeyword" value="<?php echo $ekeyword; ?>">
                              <button type="submit" class="btn btn-primary" name="submit" value=""><i class="fa fa-search"></i></button>
                              </form>
                            </div>
                            </div>
                      <table class="table table-striped table-bordered table-hover table-full-width" id="sample_12">
                        <thead>
                          <tr>
                            <th>Attendee Name</th>
                            <th>Total visit</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($visit_exhibitor_profiles_attendee as $key => $value) { ?>
                          <tr>
                            <td><?php echo $value['user_name']; ?></td>
                            <td><?php echo $value['total_hit']; ?></td>
                            <td><a href="<?php echo base_url().'Leader_board/get_exhibitor_list_visited_attendee_details/'.$event_id.'/'.$value['Id']; ?>">More details</a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      <div class="pull-right">
                                <?php if (isset($links)) { ?>
                                    <?php echo $links ?>
                                <?php } ?>
                            </div>
                    </div>
                    <?php }else{ echo "<h4>No Attendees Visited Exhibitor Profiles.</h4>"; } ?>

               <?php break; 
               default:
                  echo "<h1>No data found</h1>";
                  break;
              }
             ?>


          </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/charts.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script> 
<script>
    jQuery(document).ready(function() {
       /*$("#data_table").dataTable({
        "bPaginate": false,
        "bInfo": false,
       })*/
    });
</script>

