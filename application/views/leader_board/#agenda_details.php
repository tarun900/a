<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="Leader_board">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Job Title</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Session Rating</th>
                                <th>Save Agenda</th>
                                <th>Check In</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($agenda_details as $key => $value) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo ucfirst($value['Firstname']); ?></td>
                                <td><?php echo ucfirst($value['Lastname']); ?></td>
                                <td><?php echo ucfirst($value['Title']); ?></td>
                                <td><?php echo ucfirst($value['Company_name']); ?></td>
                                <td><?php echo $value['Email']; ?></td>
                                <td><?php 
                                        for($i=1;$i<=5;$i++){
                                            if($i<=$value['rating'])
                                            {
                                                $img=base_url().'assets/images/star-on.png';
                                            }
                                            else
                                            {
                                                $img=base_url().'assets/images/star-off.png';
                                            }
                                            ?>
                                            <img src="<?php echo $img; ?>" alert="start img"/>
                                            <?php 
                                        }
                                         ?>
                                        </td>
                                <td><?php 
                                $arr=explode(",",$value['agenda_id']);
                                   if(in_array($agendaid,$arr)){
                                    echo "<h3><i class='fa fa-check'></i></h3>";
                                    }else{
                                        echo "<h3><i class='fa fa-times'></i></h3>";
                                    } ?></td>
                                <td><?php 
                                $arr=explode(",",$value['check_in_agenda_id']);
                                   if(in_array($agendaid,$arr)){
                                    echo "<h3><i class='fa fa-check'></i></h3>";
                                    }else{
                                        echo "<h3><i class='fa fa-times'></i></h3>";
                                    } ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
