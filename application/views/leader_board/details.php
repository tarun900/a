<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="Leader_board">
            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                <div class="tab-pane fade active in" id="click_track">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Module Name</th>
                                        <th>Total Clicks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($module_click_data);$i++) { ?>
                                    <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><?php echo $module_click_data[$i]['menuname']." ".$module_click_data[$i]['Lastname']; ?></td>
                                        <td><?php echo $module_click_data[$i]['total_hit']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>
