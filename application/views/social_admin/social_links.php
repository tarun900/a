<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="exibitor">
            <div class="panel-heading">
                <h4 class="panel-title">Social <span class="text-bold">Links</span></h4>
                <?php if(empty($social)){ ?>
                    <a class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Social_links/add"><i class="fa fa-plus"></i> Add Social Links</a>
                <?php } else { } ?>
                <div class="panel-body">
                    <?php if($this->session->flashdata('social_data')){ ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Social Links <?php echo $this->session->flashdata('social_data'); ?> Successfully.
                    </div>
                    <?php } ?>
                    <?php
                    /*echo "<pre>"; 
                    print_r($social);
                    exit();*/
                    ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Heading</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i=0;$i<count($social);$i++) { /*echo '<pre>'; print_r($social); exit; */?>
                                <tr>
                                    <td><?php echo $i+1; ?></td>
                                    <td>Social Links</td>
                                   <td>
                                        <?php if($user->Role_name=='Attendee' || $user->Role_name=='Speaker'){ ?>
                                        <a href="<?php echo base_url() ?>Social_links/edit/<?php echo $social[$i]['Event_id'].'/'.$social[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" onclick="delete_social_links(<?php echo $social[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function delete_social_links(id,Event_id)
    {   
        <?php $test = $social[0]["Event_id"]; ?>
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Social_links/delete_social_links/"+id+"/"+<?php echo $test; ?>
        }
    }
</script>

<!-- end: PAGE CONTENT-->