<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Social Links</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Website Url 
                        </label>
                        <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Website Url" id="url" name="website_url" class="form-control name_group" value="<?php echo $social[0]['Website_url']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Facebook Url 
                        </label>
                        <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Facebook Url" id="url" name="facebook_url" value="<?php echo $social[0]['Facebook_url']; ?>" class="form-control name_group">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            Twitter Url 
                        </label>
                        <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Twitter Url" id="url" name="twitter_url" class="form-control name_group" value="<?php echo $social[0]['Twitter_url']; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                            LinkedIn Url 
                        </label>
                        <em style="padding-left:2%">URL(e.g: https://www.yoursite.com)</em>
                        <div class="col-sm-9">
                            <input type="text" placeholder="LinkedIn Url" id="url" name="linkedin_url" class="form-control name_group" value="<?php echo $social[0]['Linkedin_url']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit">
                                Update <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>