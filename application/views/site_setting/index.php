<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
			<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
				<li  class="active">
					<a data-toggle="tab" href="#panel_edit_account">
						Edit Account
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="panel_edit_account" class="tab-pane fade  in active">
                                    <form action="site_setting/update" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
						<div class="row">
                                                    <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
                                                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                                                        <i class="fa fa-remove-sign"></i> Updated Successfully.
                                                    </div>
                                                    <?php } ?>
							<div class="col-md-12">
								<h3>Site Setting</h3>
								<hr>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Site Title <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="Site Title" class="form-control" id="site_title" name="site_title" value="<?php echo $site_setting->Site_title ?>">
								</div>
								<div class="form-group">
									<label class="control-label">
										Reserved Right <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Reserved Right" class="form-control" id="reserved_right" name="reserved_right" value="<?php echo $site_setting->Reserved_right ?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>
										Site Logo
									</label>
                                                                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail"><img src="<?php echo base_url()."assets/user_files/",$site_setting->Site_logo ?>" alt="">
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail"></div>
										<div class="user-edit-image-buttons">
											<span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                                                            <input type="file" name="userfile">
											</span>
											<a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
												<i class="fa fa-times"></i> Remove
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-md-4 pull-right">
								<button class="btn btn-green btn-block" type="submit">
									Update <i class="fa fa-arrow-circle-right"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end: PAGE CONTENT-->