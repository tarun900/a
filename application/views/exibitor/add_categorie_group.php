<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />    
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css"> 
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Add <span class="text-bold">Categorie Group</span></h4>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;">
              Group Name <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <input type="text" placeholder="Group Name" class="form-control required" name="name">
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Exhibitor Categories <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <select style="height:auto;" multiple="multiple" class="select2-container select2-container-multi form-control search-select menu-section-select" name="category_ids[]">
                <?php foreach ($exibitor_category as $key => $value) { ?>
                <option value="<?=$value['id'];?>" <?php if(in_array($value['id'],$primary_country)){ ?> selected="selected" <?php } ?>><?=$value['category'];?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Submit <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>