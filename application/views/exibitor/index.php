<?php $acc_name=$this->session->userdata('acc_name');?>
<style type="text/css">
/*#exibitor_list_table_processing {
    background: rgba(0, 0, 0, 0.5) url("<?php echo base_url(); ?>assets/images/ajax-loader.gif") no-repeat scroll center center;
    height: 100%; left: 0; position: fixed; text-indent: -999999px; top: 0; width: 100%; z-index: 9999;
}*/
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <a style="top:7px;right: 5px;" class="btn btn-primary list_page_btn ex_btn" href="<?php echo base_url(); ?>Exhibitor/add/<?php echo $event['Id']; ?>"><i class="fa fa-plus"></i> Add Exhibitor</a>
                <a style="top:7px;margin-right: 11%;" class="btn btn-primary list_page_btn ex_btn" href="<?php echo base_url(); ?>Exhibitor/add_exhibitor_type/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add Exhibitor Type</a>
                   
                <?php if($this->session->flashdata('exibitor_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Exhibitor Content <?php echo $this->session->flashdata('exibitor_data'); ?> Successfully.
                </div>
                <?php } ?>

                 <?php if ($this->session->flashdata('ex_in_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Exhibitor <?php echo $this->session->flashdata('ex_in_data'); ?> Successfully.
                    </div>
                <?php } ?>

                 <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#Exhibitors_dropdown_link_div" data-close-others="true">
                               <span id="Exhibitors_textshow"> Exhibitors </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="attendee_dropdown_link_div" class="dropdown-menu dropdown-info"> 
                                <li class="active">
                                    <a href="#exibitor_list" data-toggle="tab">
                                        Exhibitors List
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#exibitor_invite" data-toggle="tab">
                                        Invite Individual Exhibitors
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#exibitor_multiple_invite" data-toggle="tab">
                                        Invite Mulitple Exhibitors
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#exibitor_invited" data-toggle="tab">
                                        Invited Exhibitors
                                    </a>
                                </li>
                                <?php if($event['Event_type']=='4'){ ?>
                                <li class="">
                                    <a href="#authorized_emails" data-toggle="tab">
                                        Authorized Emails
                                    </a>
                                </li>
                                <?php } ?>
                                <li class="">
                                    <a href="#front_end_settings" data-toggle="tab">
                                        Frontpage Settigns
                                    </a>
                                </li>
                                <?php if($user->Role_name=='Client'){ ?>
                                <li class="">
                                    <a href="#allow_messaging" data-toggle="tab">
                                        Interaction & Settings
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a href="#exhibitor_type" data-toggle="tab">
                                Exhibitor Types
                            </a>
                        </li>
                        <li>
                            <a href="#countries" data-toggle="tab">
                                Countries
                            </a>
                        </li>
                        <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#exhibitor_categories_dropdown" data-close-others="true">
                               <span id="Exhibitors_textshow"> Exhibitor Category </span> &nbsp; <i class="fa fa-caret-down width-auto"></i>
                            </a>
                            <ul id="exhibitor_categories_dropdown" class="dropdown-menu dropdown-info">
                                <li>
                                <a href="#exhibitor_categories" data-toggle="tab">
                                    Exhibitor Category
                                </a>
                                </li>
                                <li>
                                <a href="#exhibitor_categories_group" data-toggle="tab">
                                    Exhibitor Category Group
                                </a>
                                </li>
                            </ul>
                        </li>
                        <?php if($user->Role_name=='Client'){ ?>
                        <!-- <li class="">
                            <a href="#allow_messaging" data-toggle="tab">
                               Interaction
                            </a>
                        </li> -->
                        <li class="">
                            <a href="#menu_setting" data-toggle="tab">
                               Menu Name and Icon       
                            </a>
                        </li>
                        <?php } ?>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>  
                <div class="tab-content">
                        <div class="tab-pane fade active in" id="exibitor_list">
                            <div class="errorHandler alert alert-danger no-display" style="display: none;" id="premission_error_div">
                                
                            </div>
                            <div class="row" style="margin-bottom: 3%;">
                                <div class="col-sm-4">
                                    Ability to Add Survey Questions for Lead Retrieval
                                </div>
                                <div class="col-sm-3">
                                    <select name="add_survey_premission" id="add_survey_premission" class="form-control">  
                                        <option value="">Select Ability</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <a href="javascript:void(0);" id="add_survey_premission_btn" class="btn btn-green btn-block">
                                        Assign <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <a style="top:7px;" class="btn btn-primary list_page_btn ex_btn" href="<?php echo base_url(); ?>Exhibitor/mass_upload_page/<?php echo $event['Id']; ?>"> Mass Upload
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive custom_checkbox_table" style="overflow-x: scroll;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="exibitor_list_table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input name="selectall_checkbox" type="checkbox" class="flat-grey selectall checkbox_select_all_box">
                                                    </label>
                                                </div>
                                            </th>
                                            <th>Exhibitor</th>
											<th>Company name</th>
                                            <th class="hidden-xs">Email</th>
                                            <!-- <th>Ability to Add Survey Questions for Lead Retrieval</th> -->
                                            <!-- <th>Number of Sales Reps</th> -->
                                            <!-- <th>Maximum Number of Reps</th> -->
                                            <th class="no-sort">Edit or Delete</th>
                                            <th class="no-sort">Edit Profile</th>
                                         </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="exibitor_page">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>Company name</th>
                                        <th>Exibitor Email</th>
                                        <th>Edit or Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                    <?php for($i=0;$i<count($exibitor_page_list);$i++) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() ?>Exhibitor/page_edit/<?php echo $exibitor_page_list[$i]['Event_id'].'/'.$exibitor_page_list[$i]['Id']; ?>"><?php echo $exibitor_page_list[$i]['Heading']; ?></a></td>
                                        <td><a href="<?php echo base_url() ?>Exhibitor/page_edit/<?php echo $exibitor_page_list[$i]['Event_id'].'/'.$exibitor_page_list[$i]['Id']; ?>"><?php echo $exibitor_page_list[$i]['Email'] ?></a></td>
                                       
                                       <td>
                                            <a href="<?php echo base_url() ?>Exhibitor/page_edit/<?php echo $exibitor_page_list[$i]['Event_id'].'/'.$exibitor_page_list[$i]['Id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_exibitor(<?php echo $exibitor_page_list[$i]['Id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>

                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="exhibitor_type">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_31">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Exhibitor Type Name</th>
                                        <th>Position</th>
                                        <th>Hex Color</th>
                                        <th>Type Code</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody> 
                                    <?php $i=1; foreach ($exhibitor_type as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo ucfirst($value['type_name']); ?></td>
                                            <td><?php echo ucfirst($value['type_position']); ?></td>
                                            <td><?php echo ucfirst($value['type_color']); ?></td>
                                            <td><?php  echo $value['type_ucode']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>Exhibitor/exhibitor_type_edit/<?php echo $value['event_id'].'/'.$value['type_id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:;" onclick="delete_exhibitor_type(<?php echo $value['type_id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="countries">
                        <form method="post" action="<?php echo base_url().'Exhibitor/save_event_countries/'.$this->uri->segment(3); ?>" accept-charset="utf-8">
                            <div class="form-group">
                            <select multiple="multiple" style="margin-top:-8px; height: auto;" class="select2-container select2-container-multi form-control menu-section-select search-select required" name="countries[]" >
                                <?php foreach ($countries as $key => $value) { 
                                    ?>
                                <option <?php echo in_array($value['id'], $event_countries) ? 'selected="selected"' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['country_name']; ?></option>
                                <?php } ?>
                            </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" value="submit" name="submit">Save</button>
                            </div>
                        </form>
                         
                    </div>
                    
                    <div class="tab-pane" id="exhibitor_categories">
                        <div class="row">
                            <div class="col-sm-3 pull-right">
                                <a href="<?php echo base_url().'Exhibitor/add_parent_categorie/'.$event_id; ?>" class="btn btn-green btn-block">
                                    <i class="fa fa-plus"></i> Add Parent Categories 
                                </a>
                            </div>
                            <div class="col-sm-3 pull-right">
                                <a href="<?php echo base_url().'Exhibitor/add_exhibitor_categorie/'.$event_id; ?>" class="btn btn-green btn-block"><i class="fa fa-plus"></i> Add Exhibitor Categories</a>
                            </div>
                            <div class=" col-md-4 pull-right">
                            <form method="post" action="<?php echo base_url().'Exhibitor/save_slider_settings/'.$this->uri->segment(3); ?>" accept-charset="utf-8"> 

                                <div class="form-group">
                                    <h4><label for="show_slider">Show Category Slider <input type="checkbox" name="show_slider" <?php echo ($event['show_slider'] == '1') ? 'checked' : ''; ?> value="1"></label>
                                    
                                    <button class="btn btn-green" type="submit" value="submit" name="submit">Save</button>
                                    </h4>

                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_71">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Exhibitor Category</th>
                                        <th>Keywords</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($categorie_data as $key => $value){ ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($value['category']); ?></td>
                                        <td><?php echo $value['categorie_keywords']; ?></td>
                                        <td>
                                           <?php if($value['category_type']=='1'){ ?>
                                            <a href="<?php echo base_url() ?>Exhibitor/parent_categories_edit/<?php echo $value['event_id'].'/'.$value['id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <?php }else{ ?>
                                            <a href="<?php echo base_url() ?>Exhibitor/exhibitor_categories_edit/<?php echo $value['event_id'].'/'.$value['id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <?php } ?>
                                            <a href="javascript:;" onclick="delete_exhibitor_categorie(<?php echo $value['id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                    <div class="tab-pane" id="exhibitor_categories_group">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-3 pull-right">
                                <a href="<?php echo base_url().'Exhibitor/add_categorie_group/'.$event_id; ?>" class="btn btn-green btn-block">
                                    <i class="fa fa-plus"></i> Add Exhibitor Category Group 
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_81">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Exhibitor Category Group</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($categorie_group as $key => $value){ ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($value['name']); ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>Exhibitor/edit_categorie_group/<?php echo $value['event_id'].'/'.$value['id']; ?>" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_exhibitor_categorie_group(<?php echo $value['id']; ?>)" class="btn btn-red" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                    <div class="tab-pane fade" id="allow_messaging">
                        <div class="row padding-15">
                            <form class="" method="post" action="<?php echo base_url().'Exhibitor/saveallow_setting_for_Exhibitor/'.$this->uri->segment(3); ?>">
                                <h3>Interaction</h3>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="hide_request_meeting" <?php if($event['hide_request_meeting']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Allow Attendees to request meetings with Exhibitors.</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="allow_msg_user_to_exhibitor" <?php if($event['allow_msg_user_to_exhibitor']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Allow Exhibitors and Attendees to message each other.</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="allow_meeting_exibitor_to_attendee" <?php if($event['allow_meeting_exibitor_to_attendee']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Allow Exhibitors to request meetings with Attendees.</span>
                                    </label>
                                </div>
                                <h3>Settings</h3>
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="link_auto_attendee" <?php if($event['link_auto_attendee']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Link Attendees to Exhibitors Automatically.</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="front_end_settings">
                        <div class="row padding-15">
                            <form class="" method="post" action="<?php echo base_url().'Exhibitor/save_exhi_front_settings/'.$this->uri->segment(3); ?>">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" class="square-orange" name="show_topbar_exhi" <?php if($event['show_topbar_exhi']=='1') { ?> checked="checked" <?php } ?>>
                                        <span style="font-weight: bold;">Show Topbar and Sidebar in Frontside</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="exibitor_invite">
                       <form role="form" method="post" class="form-horizontal" id="form" action="<?php echo base_url().'Exhibitor/invite_individual_Exhibitor/'.$event_id; ?>" enctype="multipart/form-data">
                   
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Inviation Message
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea name="msgcontent" id="formContent" style="height: 200px;" class="ckeditor form-control" ></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Invite Exhibitors Individually <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                       <input type="text" placeholder="Exhibitor email address" class="form-control required" id="email_address" name="email_address" onchange=""><br/>
                                       <input type="text" placeholder="Exhibitor First name" class="form-control" id="first_name" name="first_name"><br/>
                                       <input type="text" placeholder="Exhibitor Last name" class="form-control" id="last_name" name="last_name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Stand Number
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Stand Number" class="form-control" id="invite_stand_number" name="invite_stand_number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Subject <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Subject" class="form-control required" id="subject1" name="subject1">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control required" id="sent_from_name" name="sent_from_name" value="<?php echo ucfirst($user->Firstname).' '.$user->Lastname; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                </label>
                                <div class="col-md-2">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#add-slides" class="btn btn-yellow btn-block" type="button" onclick="preview1('form');">
                                        Preview Email
                                    </button>
                                </div>
                                <div class="col-md-3">

                                    <button class="btn btn-yellow" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="exibitor_multiple_invite">

                        <form role="form" method="post" class="form-horizontal" id="form2" action="<?php echo base_url().'Exhibitor/invite_mulitple_Exhibitor/'.$event_id; ?>" enctype="multipart/form-data">
                   
                           <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Inviation Message
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <textarea name="msg" id="form2Content" style="height: 200px;" class="ckeditor form-control" ></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Short instructions
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-4" style="padding:0px;">
                                        <a style="position:relative;right: 0px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Exhibitor/download_csv/<?php echo $event_id; ?>">Download CSV</a>
                                        <br/><br/><small>Download Example CSV of multiple invite Exhibitors</small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Upload CSV <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-4" style="padding:0px;">
                                      <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                                          <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                  <input type="file" name="csv" id="csv">
                                          </span><br/><small>Browse CSV file only</small>

                                      </div>
                                    </div>
                                    
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Subject <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Subject" class="form-control required" id="subject" name="subject">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="text" placeholder="Sender Name" class="form-control required" id="sent_from_name" name="sent_from_name" value="<?php echo ucfirst($user->Firstname).' '.$user->Lastname; ?>">
                                    </div>
                                </div>
                            </div>

                           <!-- <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                    Sender Email <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="col-sm-12" style="padding:0px;">
                                        <input type="email" placeholder="Sender Email Address" class="form-control required" id="sent_from" name="sent_from" value="<?php echo $user->Email; ?>">
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">
                                </label>
                                <div class="col-md-2">
                                     <button class="btn btn-primary" data-toggle="modal" data-target="#add-slides" class="btn btn-yellow btn-block" type="button" onclick="preview1('form2');">
                                        Preview Email
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-yellow" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

               
						<div class="tab-pane fade" id="exibitor_invited">
                            <form role="form" method="post" class="form-horizontal" id="form5" action="" enctype="multipart/form-data">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_4">
                                        <thead>
                                            <tr>
                                                <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" class="flat-grey selectall">
                                                    </label>
                                                </div>
                                                </th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                <?php
                                for ($i = 0; $i < count($exibitor_invited_list); $i++) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if ($exibitor_invited_list[$i]['Status'] == "0") {
                                            ?>
                                            <div class="checkbox-table">
                                                <label>
                                                    <input type="checkbox" name="invites[]"  value="<?php echo $exibitor_invited_list[$i]['Emailid']; ?>" class="flat-grey foocheck">
                                                </label>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $exibitor_invited_list[$i]['Emailid']; ?></td>
                                        <td><?php echo $exibitor_invited_list[$i]['firstname']." ".$agenda_invited_list[$i]['lastname']; ?></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php
                                                if ($exibitor_invited_list[$i]['Status'] == '1') {
                                                    ?> 
                                                          label-success 
                                                          <?php
                                                      } else {
                                                          ?> label-danger 
                                                      <?php }
                                                      ?>">
                                                          <?php
                                                          if ($exibitor_invited_list[$i]['Status'] == '1') {
                                                              echo "Register";
                                                          } else {
                                                              echo "Not Register";
                                                          }
                                                ?>
                                                </span>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                    </table>
                                </div>

                                <div class="form-group" style="margin-top: 3%;">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Inviation Message</strong>
                                    </label>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <textarea name="msgcontent" id="form5Content" style="height: 200px;" class="ckeditor form-control" ></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Subject</strong>  <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <input type="text" placeholder="Subject" class="form-control required" id="subject" name="subject">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        Sender Name <span class="symbol required"></span>
                                    </label><br/>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <input type="text" placeholder="Sender Name" class="form-control required" id="sent_from_name" name="sent_from_name" value="<?php echo ucfirst($user->Firstname).' '.$user->Lastname; ?>">
                                        </div>
                                    </div>
                                </div>

                               <!-- <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">
                                        <strong>Sender Email</strong> <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12" style="padding:0px;">
                                            <input type="email" placeholder="Sender Email Address" class="form-control" id="" name="sent_from" value="<?php echo $user->Email; ?>">
                                        </div>
                                    </div>
                                </div> -->

                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal bs-example-modal-basic fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                    ×
                                                </button>
                                                <h4 style="text-align:center;" id="myModalLabel" class="modal-title">Preview Email</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4><?php echo ucfirst($user->Firstname).' '.$user->Lastname." : "; ?></h4>
                                                <h4><?php echo ucfirst($user->Firstname).' '.$user->Lastname." : "; ?>Hello - Set up your exhibitor profile</h4>
                                                <p>
                                                    Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                                                </p>
                                                <a href="<?php echo base_url(); ?>">Click here </a>to register.<br/><br/>
                                                <h4>Thanks</h4>
                                                <h4><?php echo ucfirst($user->Firstname).' '.$user->Lastname; ?></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#add-slides" class="btn btn-yellow btn-block" type="button" onclick="preview1('form5');">
                                            Preview Email
                                         </button>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-yellow" type="submit">
                                            Submit <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                <div class="tab-pane fade at_tabpan" id="authorized_emails">
                    <div class="row">
                        <div class="col-sm-3 pull-right">
                            <a href="<?php echo base_url().'Exhibitor/add_authorized_email/'.$event_id; ?>" class="btn btn-green btn-block">Add Authorized Emails</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_61">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($auth_user_list as $key => $value) { ?>
                                <tr>
                                    <td><?php $key++;echo $key; ?></td>
                                    <td><?php echo ucfirst($value['firstname']).' '.$value['lastname']; ?></td>
                                    <td><?php echo $value['Email']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url().'Exhibitor/edit_authorized_user/'.$event_id.'/'.$value['authorized_id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" onclick="delete_auth_user(<?php echo $value['authorized_id']; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>    
                </div>        

                <div class="tab-pane" id="menu_setting">
                        <div class="row padding-15">
                            <form class="" id="form1" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                <div class="col-md-12 alert alert-info">
                                    <h5 class="space15">Menu Title & Image</h5>
                                    <div class="form-group">
                                       <input type="text" name="title" id="title form-field-20" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                    </div>
                                    <div class="form-group">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div class="user-edit-image-buttons">
                                                 <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                      <input type="file" name="Images[]">
                                                 </span>
                                                 <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                      <i class="fa fa-times"></i> Remove
                                                 </a>
                                            </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                            Create Home Tab
                                        </label>
                                        <?php if($img !='') { ?>
                                        <label class="btn btn-red" style="float: right;margin: 10px;">
                                                <input type="checkbox" name="delete_image" id="delete_image" class="" style="opacity: 0;margin: -10px;" onClick="$('#form1').submit();">
                                                <i class="fa fa-times"></i> Remove
                                        </label>
                                        <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                        <?php } ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-1" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                        <div class="col-sm-5">
                                            <select style="margin-top:-8px;" id="img_view" class="form-control" name="img_view">
                                                    <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                    <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                        <?php if($this->uri->segment(4) =='1') { ?>
                        <div class="tab-pane fade active in" id="user_attendee">
                        <?php } else { ?>
                        <div class="tab-pane fade" id="user_attendee">
                        <?php } ?>
                            <form role="form" method="post" class="form-horizontal" id="form9" action="" enctype="multipart/form-data">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" class="flat-grey selectall">
                                                    </label>
                                                </div>
                                                </th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < count($attendee_user_list); $i++) {
                                                ?>
                                                <tr>
                                                    <td>
                                                       
                                                        <div class="checkbox-table">
                                                            <label>
                                                                <input type="checkbox" name="attendee[]"  value="<?php echo $attendee_user_list[$i]['User_id']; ?>" class="flat-grey foocheck">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $attendee_user_list[$i]['Email']; ?></td>
                                                    <td><?php echo $attendee_user_list[$i]['Firstname']." ".$attendee_user_list[$i]['Lastname']; ?></td>
                                                    
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btn-block" type="submit">
                                            Move to attendee <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="advertising">
                            <div class="row padding-15">
                                <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                    <div class="col-md-12 alert alert-info">
                                        <h5 class="space15">Menu Title & Image</h5>
                                        <div class="form-group">
                                            <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        </div>
                                        <div class="form-group">
                                             <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail"></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div class="user-edit-image-buttons">
                                                   <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                        <input type="file" name="Images[]">
                                                   </span>
                                                   <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                        <i class="fa fa-times"></i> Remove
                                                   </a>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                                Create Home Tab
                                            </label>
                                            <?php if($img !='') { ?>
                                            <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                            <?php } ?>
                                        </div>
                            
                                        <div class="form-group image_view:">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                            <div class="col-sm-5">
                                                <select id="img_view" class="form-control" name="img_view">
                                                        <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                        <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
						
						<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
							<div id="viewport" class="iphone">
									<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
							</div>
							<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
							<div id="viewport_images" class="iphone-l" style="display:none;">
								   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
							</div>
						</div>
						
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>
<div class='modal fade no-display' id='add-slides' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class="modal-header" style="border-bottom:1px solid #e5e5e5;">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="margin-top:-9px;">
                    ×
                </button>
            </div>
            <div class='test modal-body' style="padding:0px;" id="co">
            
            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<link rel="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script type="text/javascript">
  var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

$(document).ready(function() {
    $('#myTab2 > li > ul > li > a').click(function(){
        jQuery(this).parent().parent().parent().first('a').find('span').html($.trim(jQuery(this).text()));
    });

          TableData.init();
           $("#sample_61,#sample_31,#sample_71,#sample_81").DataTable({
             bStateSave: true,
            bDestroy: true,
            fnStateSave: function(settings,data) {
                  localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
                },
            fnStateLoad: function(settings) {
                return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
                }
        })
        $('#exibitor_list_table').dataTable( {
            "sDom": "<'row-fluid'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "bFilter": true,
             "bStateSave": true,
            "bDestroy": true,
            fnStateSave: function(settings,data) {
                  localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
                },
            fnStateLoad: function(settings) {
                return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
                },
            "bProcessing": true,
            "bServerSide": true,
             'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['no-sort']
            }],
            "sAjaxSource": "<?php echo base_url().'Exhibitor/ajax_manage/'.$this->uri->segment(3); ?>"
        });
     });
     
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
  
    function delete_user(id,eid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete/"+eid+ "/" + id;
        }
    }

    function delete_exibitor(id,Event_id)
    {   
        <?php $Event_id = $event['Id']; ?>
        alert("<?php echo base_url(); ?>Exhibitor/delete_exibitor/"+id+"/"+<?php echo $Event_id; ?>);
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete_exibitor/"+<?php echo $Event_id; ?>+"/"+id
        }
    }
    function delete_exhibitor_type(id)
    {
         if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete_exhibitor_type/"+<?php echo $event['Id']; ?>+"/"+id
        }
    }
    function delete_auth_user(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete_authorized_user/"+<?php echo $this->uri->segment(3); ?>+"/"+id
        }
    }
    function delete_exhibitor_categorie(cid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete_exhibitor_categorie/"+<?php echo $event['Id']; ?>+"/"+cid
        }
    }
    function delete_exhibitor_categorie_group(cid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Exhibitor/delete_exhibitor_categorie_group/"+<?php echo $event['Id']; ?>+"/"+cid
        }
    }
    function preview1(strid)
    {   
         var Content=CKEDITOR.instances[strid+'Content'].getData();
         if(strid=='form')
         {
            var fname=$("#first_name").val();
            var lname=$("#last_name").val();
            var Subject=$("#"+strid+" #subject1").val();   
         }
         else
         {
            var fname="{{fname}}";
            var lname="{{lname}}";
            var Subject=$("#"+strid+" #subject").val();   
         }
        
         var to="{{Senderemail}}";
       

         var Sendername=$("#"+strid+" #sent_from_name").val();
         var Senderemail=$("#"+strid+" #sent_from").val();
         var cont="<p><br>"+fname+" "+lname+""+"</p>";
         var cont2="<br/><a href='<?php echo base_url(); ?>'> Click here</a> to register<br/>";
         var cont3="<br/>Thank You"+"<br>"+Sendername+"<br/>";
         var data="<div style='padding:10px;'><strong>Subject:</strong>&nbsp;"+Subject+"<br>"+cont+Content+cont2+cont3+"</div>";
         $("#co").html(data);
        
     
    }
    $('#add_survey_premission_btn').click(function(){
    var table=$('#exibitor_list_table').DataTable();
    var chkbox_checked=table.$('input[type="checkbox"]:checked').length;
    if($.trim($('#add_survey_premission').val())!="" && chkbox_checked > 0)
    {
        $('#premission_error_div').hide();
        var data = table.$('input').serialize();
        data=data+"&premission="+$('#add_survey_premission').val();
        $.ajax({
            url:"<?php echo base_url().'Exhibitor/add_exibitor_premission/'.$event['Id'] ?>",
            type:'post',
            data:data,
            success:function(result){
                location.reload();
            }
        });
    }
    else
    {
        $('#premission_error_div').html('Please Select Ability Or Exibitor User');
        $('#premission_error_div').show();
    }
});

$(document).on('keydown','.maximum_Reps',function(e) {
    if (e.shiftKey || e.ctrlKey || e.altKey) 
    {
        e.preventDefault();
    } 
    else 
    {
        var key = e.keyCode;
        if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) 
        {
            e.preventDefault();
        }
    }
    var strrespid    = jQuery(this).prop("id");
    var arrmaximumresp   = jQuery(this).prop("id").split("__");
    delay(function(){
        if((key >= 35 && key <= 40))
        {
            
        }
        else
        {
            var user_id = arrmaximumresp[1];
            var numberreps     = jQuery('#'+strrespid).val();
            $.ajax({
            url: "<?php echo base_url().'Exhibitor/save_maximum_Reps/'.$event['Id'] ?>",
            type: 'POST',
            data: {maximum_Reps:numberreps,user_id:user_id},
            success: function(data) {
                var shortCutFunction ='success';
                var msg = 'Maximum Reps Save Successfully...';
                var title = 'Success';
                var $showDuration = 1000;
                var $hideDuration = 3000;
                var $timeOut = 10000;
                var $extendedTimeOut = 5000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass:'toast-bottom-right',
                    onclick: null
                };
                toastr.options.showDuration = $showDuration;
                toastr.options.hideDuration = $hideDuration;
                toastr.options.timeOut = $timeOut;                        
                toastr.options.extendedTimeOut = $extendedTimeOut;
                toastr.options.showEasing = $showEasing;
                toastr.options.hideEasing = $hideEasing;
                toastr.options.showMethod = $showMethod;
                toastr.options.hideMethod = $hideMethod;
                toastr[shortCutFunction](msg, title);
            }
        });
        }
    }, 1500 );
}); 
    </script>
<!-- end: PAGE CONTENT-->