<div class="panel panel-white">
	<div class="panel panel-body">
		<div class="row" align="center" style="margin: 10px auto;">
		<img src="<?php echo base_url().'assets/images/all-in-loop-logo.png'; ?>">
		</div>
		<h4 style="text-align: center;">This app was created on All In The Loop. Contact All In The Loop about having a mobile app at your next event.</h4>
		<?php if($this->session->flashdata('lead_success')){ ?>
		<div class="errorHandler alert alert-success no-display" style="display: block;">
            <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('lead_success'); ?>
        </div>
		<?php }if($this->session->flashdata('lead_error')){ ?>
		<div class="errorHandler alert alert-danger no-display" style="display: block;">
            <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('lead_error'); ?>
        </div>
		<?php } ?>
		<form action="<?php echo base_url().'App/'.$acc_name.'/'.$Subdomain.'/signature_form_data_save'; ?>" class="form-horizontal" method="post" id="signature_page_form" role="form">
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Firstname <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<input type="text" name="first_name_in_lead" id="first_name_in_lead" class="form-control name_group required">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Lastname <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<input type="text" name="last_name_in_lead" id="last_name_in_lead" class="form-control name_group required">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Phone Number <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<input type="text" name="phone_number_in_lead" id="phone_number_in_lead" class="form-control name_group required">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Email <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<input type="text" name="Email_in_lead" id="Email_in_lead" class="form-control name_group required">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Company Name <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<input type="text" name="company_in_lead" id="company_in_lead" class="form-control name_group required">
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2">Country <span class="symbol required"></span></label>
				<div class="col-sm-9">
					<select name="country_in_lead" id="country_in_lead" class="form-control name_group required">
						<option value="">Select Country...</option>
						<?php foreach ($countrylist as $key => $value) { ?>
						<option value="<?php echo $value['country_name']; ?>"><?php echo $value['country_name']; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class="control-label col-sm-2"></label>
				<div class="col-sm-3">
					<input type="submit" name="submit_form" id="submit_form_btn" value="Submit" class="btn btn-green btn-block">
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
function validemaild_email()
{
	alert('text');
	return true;
}
$("#signature_page_form").validate({	
errorElement: "label",
errorClass: 'help-block',  
rules: {
  first_name_in_lead:{
  	required:true,
    minlength: 3
  },
  last_name_in_lead:{
  	required:true,
  	minlength:3
  },
  Email_in_lead:{
  	required:true,
  	email:true
  },
  phone_number_in_lead:{
  	required:true,
    number:true,
    minlength: 10,
    maxlength:15
  }
},
highlight: function (element) 
{
  $(element).closest('.help-block').removeClass('valid');
  $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
},
unhighlight: function (element) {
  $(element).closest('.form-group').removeClass('has-error');
}
});	
</script>
