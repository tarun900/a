<!--<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/test/libs/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/dist/jquery.panzoom.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Plugin-For-Panning-Zooming-Any-Elements-panzoom/test/libs/jquery.mousewheel.js"></script>-->
 

<script src="<?php echo base_url(); ?>assets/plugins/Panzoom_jQuery_files/jquery_002.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/Panzoom_jQuery_files/jquery_003.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/Panzoom_jQuery_files/jquery.js"></script>

<style type="text/css">
.buttons.zoom-btn {
    left: 10px;
    margin-top: 0;
    position: absolute;
    text-align: center;
    top: 10px;
    width: 50px;
    z-index: 999;
}
.zoom-in
{
  background: #fff;
  border-radius: 50%;
  height: 50px;
  width: 50px;
  font-size: 40px;
  color: #2e96d7;
  font-weight: bold;
  border: 1px solid #ccc;
  text-overflow: ellipsis;
  cursor: pointer;
  outline: 0;
  background: #fff url('<?php echo base_url(); ?>assets/images/zoom-in-img.png') no-repeat center center;
  text-indent: -9999px;
  margin-bottom:10px;
}
.zoom-out
{
  background: #fff;
  border-radius: 50%;
  height: 50px;
  width: 50px;
  font-size: 40px;
  color: #2e96d7;
  font-weight: bold;
  border: 1px solid #ccc;
  text-overflow: ellipsis;
  cursor: pointer;
  background: #fff url('<?php echo base_url(); ?>assets/images/zoom-out-img.png') no-repeat center center;
  text-indent: -9999px;
  outline: 0;
}
#mocha-error
{
  display: none;
}
#map {
  height: 100%;
}
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input 
{
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

.modal-header 
{
    border-bottom:0;
    min-height: 0;
    padding: 0;
}
.modal-header .close 
{
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 999;
}
.btn-default 
{
    border: medium none;
    padding: 0;
    width: 100%;
}
.btn.btn-default.popovers > img 
 {
    height: auto;
    width: 100%;
}
.map1
{
  margin: 0 auto;
  /*width: 100% !important;
  height: auto !important;*/
}
#ref
{
  text-align: center;
}
.dwn{
     clear: both;
    display: block;
    float: right;
    margin-bottom: 20px;
    margin-right: 20px;
  margin-top:15px;
}
.zoom-range
{
  display: none !important;
}
section.map-img { text-align: center; margin:0;clear:both; position:relative;}
.panzoom-parent { border: 2px solid #333; }
.panzoom-parent .panzoom { border: 2px dashed #666; }
/*area { display:block; position:absolute !important; border: 3px solid red !important;}*/
.visuallyhidden { border: 0; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px; }
#panzoom { text-align: center; margin-left: -1px; margin-top: -1px; }
@media(max-width:1024px){
.panzoom .map1, .panzoom .map1 img{ width:100% !important; height:auto !important}
}

</style>



<?php
 $acc_name = $this->session->userdata('acc_name');
?>
<?php if($view_map[0]['Map_desc']!="" || $view_map[0]['area']!=""){ ?>
<div class="row row-box"> 
  <div class="user_msg_profile clearfix box-effect">
    <p style="padding:10px;"><?php echo html_entity_decode($view_map[0]['Map_desc']); ?></p>
    <h3 style="padding:10px;"><?php echo $view_map[0]['area']!="" ? "Area:  ".$view_map[0]['area'] : "";  ?></h3>
  </div>
</div><?php } ?>


<?php if($view_map[0]['include_map']=='1' && $view_map !='')  { ?>
<div class="map_view">
    <div class="agenda_content notes-content map_view">
        <div class="panel panel-white" style="height: 500px">
            <input id="pac-input" class="controls" type="text" placeholder="Search Box" style="display: none;">
            <div id="map"></div>
        </div>
    </div>
</div>
<?php } ?>
    <?php   
    $img = json_decode($view_map[0]['Images']); 
    if(!empty($img[0]))
    {

    ?>
    <a href="<?php echo base_url().'assets/user_files/'.$img[0]; ?>" class="dwn" target="_blank" id="" download >
    <input type="button" name="download" value="Download" id="btn" class="btn btn-primary">
    </a>
    <section class="map-img">
      
      <div id="ref" class="parent">
          <div class="panzoom">
          <img src="<?php echo base_url().'assets/user_files/'.$img[0]; ?>" id="#photo" style="position: relative;" usemap="#mape" class="map1">
          </div>
      </div>
          <map id="mape1" name="mape">
              <?php foreach($image_mapping as $key => $row):?> 

                      <area class="shwEx" title="<?php echo $row['Firstname']; ?>" data-link='<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$row['exid'];?>'  data-attr='ex_p<?php echo $row['id'];?>' alt="<?php echo $row['Firstname']; ?>"  id="ex_<?php echo $row['id'];?>" shape="rect"  href="javascript:void(0);" coords="<?php echo  $row['coords']; ?>">
                      
              <?php endforeach; ?>
          </map> 
          <div class="buttons zoom-btn">
            <button class="zoom-in">+</button>
            <button class="zoom-out">-</button>
            <input type="range" class="zoom-range">
            <!--<button class="reset">Reset</button> -->

              <script>
              (function() {
                  var $section = $('section').first();
                  $section.find('.panzoom').panzoom({
                    $zoomIn: $section.find(".zoom-in"),
                    $zoomOut: $section.find(".zoom-out"),
                    $zoomRange: $section.find(".zoom-range"),
                    $reset: $section.find(".reset")
                  });
                })();
            </script>            
          </div>
    </section>
    <?php 
    foreach($image_mapping as $key => $row):
        if($row['company_logo']!='' || $row['company_logo']!='null')
        {
            $arrImg = json_decode($row['company_logo']);
            if($arrImg[0]!='')
            {
                $strImg = base_url().'assets/user_files/'.$arrImg[0];
            }
            else
            {
                $strImg = base_url().'assets/images/event-3.jpg';
            }
            
        }
        else
        {
            $strImg = base_url().'assets/images/event-3.jpg';
        }
        ?> 
        <div id='ex_p<?php echo $row['id'];?>' class="modal fade bs-example-modal-sm maps_model" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
            <?php if(empty($row['session_id']) && !empty($row['user_id'])){ ?>
                <h4><strong><?php echo ucfirst($row['Heading']);?></strong></h4>
                <?php if($row['stand_number'] !='') { ?>
                <h5><strong>Stand Number: <?php echo $row['stand_number'];?></strong></h5>
                <?php } ?>
                <p><a class="btn btn-default popovers" href="<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$row['exid'];?>"><img src="<?php echo $strImg;?>" width="200" height="200" /></a></p>
                <?php }else{
                $ass_agenda=array();
                foreach ($row['session'] as $sskey => $ssvalue) { 
                  if(in_array($ssvalue['sid'],$user_agenda_ids)){
                    $ass_agenda[]=$ssvalue;
                  }
                }
                if(count($ass_agenda)>0){
                foreach ($ass_agenda as $skey => $svalue) {  ?>
                <a href="<?php echo base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$svalue['sid']; ?>"><h4><strong><?php echo "<span class='session_heading_span'>".ucfirst($svalue['session_heading'])."</span><span class='session_datetime_span'>".date("j F-h:i A",strtotime($svalue['Start_date']." ".$svalue['Start_time']))."</span>";?></strong></h4></a>
              <?php } }else{ 
                  echo "You have not been assigned to any sessions in this area."; }  }?>
            </div>
          </div>
    
        </div>
        
        </div>
    <?php  endforeach; ?>
    
    <?php } ?>
    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>

<div id="output"></div>

<?php

$lat_long=explode(',',$view_map[0]['lat_long']);
?>
    
<script>
$('#btn').click(function() {
 
  html2canvas($('#ref'), {
    onrendered: function(canvas) {
    /*  myImage = canvas.toDataURL("image/png");
      $('#output').append(canvas);
       alert(myImage);

        // save canvas image as data url (png format by default)
       
        // set canvasImg image src to dataURL
        // so it can be saved as an image
        document.getElementById('link').src = myImage;*/
         
      $('#link').attr('href', canvas.toDataURL());
    }
  });
});
 $(document).ready(function() {
	 
   $('.map1').mapster({
        selected: true,
        strokeColor: 'FF0000',
		strokeWidth: 2,
        fill: false,
        fillColor: 'FFFFFF',
        fillOpacity: 0.6,
        
    });
	
    $(".shwEx").click(function(){
        var strId = jQuery(this).attr("data-attr");
        $('#'+strId).modal('show');
    });
  });
  
<?php if($view_map!='' && $view_map[0]['include_map']=='1'): ?>
function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: <?php echo $lat_long[0]; ?>, lng: <?php echo $lat_long[1]; ?>},
    zoom: <?php echo $view_map[0]['zoom_level']; ?>,
    <?php if($view_map[0]['satellite_view']=='0'){ ?>
    mapTypeId: google.maps.MapTypeId.ROADMAP
    <?php }else{ ?>
    mapTypeId: google.maps.MapTypeId.HYBRID
    <?php } ?>
  });
  
  var icon = {
        url: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
  
  var marker = new google.maps.Marker({
    position: {lat: <?php echo $lat_long[0]; ?>, lng: <?php echo $lat_long[1]; ?>},
    map: map,
    icon: icon,
    title: '<?php echo $view_map[0]['place']; ?>'
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}
<?php endif;?>  

</script>
<?php if($view_map!='' && $view_map[0]['include_map']=='1'): ?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete" async defer></script>
<?php endif;?>