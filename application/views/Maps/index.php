<!-- start: PAGE CONTENT -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/event_template.css" />
<div class="row margin-left-10">
<?php
    $user = $this->session->userdata('current_user');
      $acc_name = $this->session->userdata('acc_name');
        if(!empty($form_data)):
        foreach($form_data as $intKey=>$strval):
            if($strval['frm_posi']==2)
            {
                continue;
            }
        ?>
      <div class="col-sm-6 col-sm-offset-3">
      <?php
        $json_data = false;
        $formid="form";
        if($intKey==0)
        {
          $formid="form";  
        }
        else
        {  
          $intKey+=1;
          $formid=$formid."".$intKey;
        }
        $json_data = isset($strval) ? $strval['json_data'] : FALSE;
      

        $loader = new formLoader($json_data, base_url().'Maps/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid);
        $loader->render_form();
        unset($loader);
        ?>
        </div>
      <?php endforeach;  
        endif;    
?>
</div>
<?php 

$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];

if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('10', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id']; ?>")'> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else {  if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>

<div class="row" style="margin-right: 0.5%;">
    <div class="col-md-12">
    <div class="agenda_content social-content map_content">
        <div class="panel panel-white">
            <h4 class="panel-title"><?php echo ucfirst($event_templates[$i]['Event_name']); ?></h4>
            <div class="panel-body">
                <ul class="activities columns activities-new">
                <?php
                    
                    foreach ($map as $key => $value) 
                    {
                    ?>
                     <li class="map_wrap">
                        <i class="fa fa-map-marker"></i>
                        <h4>
                            <a href="<?php echo base_url(); ?>Maps/<?php echo $acc_name."/".$Subdomain; ?>/View/<?php echo $value['Id']; ?>"><?php echo $value['Map_title']; ?>
                            <div class="time"><i class="fa fa-chevron-right"></i></div>
                            </a>
                        </h4>
                     </li>
                <?php } ?>
                </ul>
            </div>

        </div>
        
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function()
     {
       //SVExamples.init();
       //Main.init();
     });
</script>
<script src="<?php echo base_url(); ?>assets/formbuilder/formvalidation.js"></script>
<script src="<?php echo base_url(); ?>assets/wheelzoom.js"></script>
<script>
  $(document).ready(function() {
      
      SVExamples.init();
      // FormElements.init();
       FormValidator.init();

  });
  
    
  wheelzoom(document.querySelector('img.zoom'));
 
</script>