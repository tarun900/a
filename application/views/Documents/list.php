<div style="padding:10px;margin-top:10px;" class="panel panel-white box-effect">
	<div class="panel-group accordion" id="accordion">
		<?php foreach ($agenda_list as $key1 => $value1) { ?>
	    <div class="panel panel-white">
	        <div class="panel-heading">
	            <h5 class="panel-title">
	                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#accordion<?php echo $key1; ?>">
	                    <i class="icon-arrow"></i> 
	                    <?php 
	                        $adate = $key1;
	                        $a =  strtotime($adate);
	                        echo date("l, M jS, Y",$a);
	                    ?>
	                </a>
	            </h5>
	        </div>
	        <div id="accordion<?php echo $key1; ?>" class="collapse">
	           <div class="table-responsive">
	                <table class="table table-bordered table-hover" id="sample-table-1">
	                    <thead>
	                        <tr>
	                            <th>Agenda Name</th>
	                            <th>Agenda Time</th>
	                            <th>Agenda Place</th>
	                            <th>Action</th>
	                        </tr>
	                    </thead>
	                    <?php foreach ($value1 as $key => $value) { ?>
	                    <tbody>
	                        <tr>
	                        	<?php $domain = $this->uri->segment(2); ?>
	                            <td><a href="<?php echo base_url(); ?>Agenda/<?php echo $domain; ?>/View_agenda/<?php echo $value['Id']; ?>"><?php echo $value['Heading']; ?></a></td>
	                            <?php
	                                $asdate = $value['Start_time'];
	                                $aedate = $value['End_time'];
	                                $sdate = date('H:i A', strtotime($asdate));
	                                $edate = date('H:i A', strtotime($aedate));
	                            ?>
	                            <td><i class="fa fa-clock-o"></i> <?php echo $sdate.' '.'-'.' '.$edate?></td>
	                            <td><i class="fa fa-map-marker"></i> <a href="<?php echo base_url(); ?>Maps/<?php echo $domain; ?>/View/<?php echo $value['Address_map']; ?>"><?php echo $value['Map_title']; ?></a></td>
	                            <td>
	                                <a href="<?php echo base_url(); ?>Maps/<?php echo $domain; ?>/View/<?php echo $value['Address_map']; ?>">View on map</a>
	                            </td>
	                        </tr>
	                    </tbody>
	                    <?php } ?>
	                </table>
	            </div>
	        </div>
	    </div>
	    <?php } ?>
	</div>
</div>