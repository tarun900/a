<?php 

$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];

if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('14', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads alert alert-success alert-dismissable">
    <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery"> 

    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } ?>
 </div>
</div>
<?php } ?>


<div class="panel-body box-effect doc_list" <?php if(!empty($doc_categorylist[0]['coverimages'])) { ?> style="background-image:url('<?php echo base_url().'assets/user_files/'.$doc_categorylist[0]['coverimages']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;" <?php } ?>>
     <div class="controls">
<!--          <h4>Folders</h4>-->
          <div class="input-group ">
               <input type="text" class="form-control serach_widthset" id="search_content" placeholder="Search Folder or Files...">
               <div class="input-group-btn">
                    <button class="btn btn-green" type="button" id="search_btn">
                         <i class="fa fa-search"></i>
                    </button>
               </div>
          </div>
		  <hr/>
          <ul class="nav nav-pills">
               <li class="filter active" data-filter="all">
                    <a href="#" id="showall">
                         Show All
                    </a>
               </li>
               <?php
                    foreach ($doclist as $key=>$value)
                    {
               ?>
                         <li class="filter" data-filter=".category_<?php echo $value['id']; ?>">
                              <a href="#">
                                   <?php echo $value['title']; ?>
                              </a>
                         </li>
              <?php
                    }
              ?>
          </ul>
     </div>	
     <hr/>
     <!-- GRID -->
     <ul id="Grid" class="list-unstyled">
          <?php
                    foreach ($doclist as $key=>$value)
                    {
               ?>
          <li class="col-md-3 col-sm-6 col-xs-12 mix category_<?php echo $value['id']; ?> gallery-img" data-cat="1">
               <div class="portfolio-item">
                    <?php
                         $target="";
                         if($value['type']=='1')
                         {
                             $docshape=''; 
                             $cutlink=  explode('?v=',$value['link']); 
                             if(!empty($cutlink))
                             {
                                   $link=base_url()."assets/VideoGallery/video-req_1.php?video=".$cutlink[1];
                                   $display_fancy='';
                                   $colorbox='colorbox_'.$value['id'];
                                   
                                   //$docicon="https://i4.ytimg.com/vi/".$cutlink[1]."/0.jpg";
                                   if(!empty($value['docicon']))
                                   {
                                        $docicon=base_url()."assets/user_files/".$value['docicon'];
                                   }
                                   else
                                   {
                                        $docicon=base_url()."assets/images/dcoument_defult.png";
                                   }
                                   
                                   ?>
                                   <script>
                                   jQuery(document).ready(function() {
                                        jQuery(".colorbox_<?php echo $value['id']; ?>").colorbox({iframe:true, innerWidth:430, innerHeight:385});
                                   });
                                   </script>
                                        <?php
                             }
                             else
                             {
                                  if($value['doc_type']=='1')
                                  {    
                                        $link=base_url()."Documents/".$Subdomain."/displaydocs/".$value['id']; 
                                  }
                                  else
                                  {
                                       $link=base_url()."Documents/".$Subdomain."/displaydocs/".$value['id']; 
                                  }
                                   $display_fancy='';
                                   $docicon=base_url()."assets/images/dcoument_defult.png";
                             }
                             
                             
                         }
                         else
                         {
                             $docfiles=$this->document_model->get_document_file_list($value['id']);
                             
                             if($value['doc_type']=='1')
                             {    
                                  //echo 333;
                                  //exit;
                                   $link=base_url().'assets/user_documents/'.$docfiles[0]['document_file'];
                                   $docicon="";
                                   $fname = $docfiles[0]['document_file'];
                                   if(!empty($value['docicon']))
                                   {
                                        $docicon=base_url()."assets/user_files/".$value['docicon'];
                                   }
                                   else
                                   {
                                        $docicon=base_url()."assets/images/dcoument_defult.png";
                                   }

                                   if($value['image_view']=='1')
                                   {
                                        $docshape="style='border-radius:50%;'";
                                   }
                                   else
                                   {
                                        $docshape="";
                                   }
                                   
                                   $target='target="_open"';
                             } 
                             else
                             {
                                   $link=base_url()."Documents/".$Subdomain."/displaydocs/".$value['id']; 
                                  
                                   if(!empty($value['docicon']))
                                   {
                                        $docicon=base_url()."assets/user_files/".$value['docicon'];
                                   }
                                   else
                                   {
                                        $docicon=base_url()."assets/images/dcoument_defult.png";
                                   }
                             }
                             
                             $colorbox='';
                             $display_fancy='';
                             
                             
                         }
                    ?>
                    <a class="thumb-info <?php echo $colorbox; ?>" href="<?php echo $link; ?>"  <?php echo $display_fancy; ?> >
                         <img src="<?php echo $docicon; ?>" <?php echo $docshape; ?> class="img-responsive" alt="">
                    </a>
                    <div class="tools tools-bottom">
                         <?php
                         if($value['doc_type']=='1')
                         {
                              ?>
                         <a <?php echo $target; ?> href="<?php echo $link; ?>">
                         <?php
                         }
                         else
                         {
                             ?>
                              <a <?php echo $target; ?> href="<?php echo base_url(); ?>Documents/<?php echo $Subdomain; ?>/displaydocs/<?php echo $value['id']; ?>">
                             <?php
                         }
                         ?>
                              <i class="fa fa-link"></i>
                         </a>
                    </div>                        
               </div>
               <div class="title_doc"><?php echo $value['title']; ?></div>
          </li>
          <?php
                    }
          ?>
          <li class="gap"></li>
          <!-- "gap" elements fill in the gaps in justified grid -->
     </ul>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/VideoGallery/scripts/swfobject.js"></script>
     <script>
          jQuery(document).ready(function() {
               PagesGallery.init();
          });
          
          jQuery("#search_content").blur(function()
          {    
               jQuery(".nav-pills li").each(function(){
                    jQuery(this).removeClass('active');
               });
               jQuery(".nav-pills li:first").addClass('active');

               jQuery("#Grid li").each(function( index ) 
               {
                   var str=jQuery(this).find('.title_doc').html();
                   if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
                   {    
                        var str1=jQuery(this).find('.title_doc').html();
                        if(str1!=undefined)
                        {
                              var str1=jQuery(this).find('.title_doc').html().toLowerCase();
                              var content=jQuery("#search_content").val().toLowerCase();
                              if(content!=null)
                              {
                                   var n = str1.indexOf(content);
                                   //console.log(n);
                                   if(n!="-1")
                                   {
                                        $(this).css('display','inline-block');
                                   }
                                   else
                                   {
                                        $(this).css('display','none');
                                   }
                              }
                         }
                   }
               });
          });

          jQuery("#search_btn").click(function()
          {    
               jQuery(".nav-pills li").each(function(){
                    jQuery(this).removeClass('active');
               });
               jQuery(".nav-pills li:first").addClass('active');

               jQuery("#Grid li").each(function( index ) 
               {
                   var str=jQuery(this).find('.title_doc').html();
                   if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
                   {    
                        var str1=jQuery(this).find('.title_doc').html();
                        if(str1!=undefined)
                        {
                              var str1=jQuery(this).find('.title_doc').html().toLowerCase();
                              var content=jQuery("#search_content").val().toLowerCase();
                              if(content!=null)
                              {
                                   var n = str1.indexOf(content);
                                   //console.log(n);
                                   if(n!="-1")
                                   {
                                        $(this).css('display','inline-block');
                                   }
                                   else
                                   {
                                        $(this).css('display','none');
                                   }
                              }
                         }
                   }
               });
          });

          jQuery("#showall").click(function()
          {    
               jQuery(".nav-pills li").each(function(){
                    jQuery(this).removeClass('active');
               });
               jQuery(".nav-pills li:first").addClass('active');

               jQuery("#search_content").val('');
               jQuery("#Grid li").each(function( index ) 
               {
                   var str=jQuery(this).find('.title_doc').html();
                   if(jQuery.trim(str)!=undefined || jQuery.trim(str)!='')
                   {
                        var str1=jQuery(this).find('.title_doc').html();
                         if(str1!=undefined)
                         {
                              var n = str1.indexOf("");
                              if(n!="-1")
                              {
                                   $(this).css('display','inline-block');
                              }
                              else
                              {
                                   $(this).css('display','none');
                              }
                         }
                   }
               });
          });
     </script>
</div>