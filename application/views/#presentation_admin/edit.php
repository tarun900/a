<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="errorHandler alert alert-danger no-display" style="display: block;">
                <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php } ?>
            

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#presentatiom_list" data-toggle="tab">
                            Edit Presentation
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>
					
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="presentatiom_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                     
                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Start Date <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div class="input-group" style="padding-left: 1.7%;">
                                        <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                        <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                        <input type="text" name="Start_date" id="Start_date" contenteditable="false" value="<?php echo $presentation_list[0]['Start_date']; ?>" class="form-control">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Start Time <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker" contenteditable="false">
                                        <input type="text" id="Start_time" name="Start_time" class="form-control time-picker" value="<?php echo $presentation_list[0]['Start_time']; ?>">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    End Date <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div class="input-group" style="padding-left: 1.7%;">
                                        <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                        <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                        <input type="text" name="End_date" id="End_date" contenteditable="false" value="<?php echo $presentation_list[0]['End_date']; ?>" class="form-control">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    End Time <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-5">
                                    <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker" contenteditable="false">
                                        <input type="text" id="End_time" name="End_time" class="form-control time-picker" value="<?php echo $presentation_list[0]['End_time']; ?>">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 0.8%;" for="form-field-1">Status</label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" <?php if ($presentation_list[0]['Status'] == '1') echo ' checked="checked"'; ?> name="Status">
                                        Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" <?php if ($presentation_list[0]['Status'] == '0') echo ' checked="checked"'; ?> name="Status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Presentation Heading <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Presentation Heading" id="Heading" value="<?php echo $presentation_list[0]['Heading']; ?>" name="Heading" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Presentation Type <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Presentation Type" id="Types" value="<?php echo $presentation_list[0]['Types']; ?>" name="Types" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Thumbnail Status</label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" <?php if ($presentation_list[0]['Thumbnail_status'] == '1') echo ' checked="checked"'; ?> name="Thumbnail_status">
                                        Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" <?php if ($presentation_list[0]['Thumbnail_status'] == '0') echo ' checked="checked"'; ?> name="Thumbnail_status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" style="padding-top: 1.2%;" for="form-field-1">Auto Slide Status</label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" <?php if ($presentation_list[0]['Auto_slide_status'] == '1') echo ' checked="checked"'; ?> name="Auto_slide_status">
                                        Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" <?php if ($presentation_list[0]['Auto_slide_status'] == '0') echo ' checked="checked"'; ?> name="Auto_slide_status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-select-4">
                                    <!--Select Address -->Select Location
                                </label>
                                <div class="col-md-9">
                                    <select id="form-field-select-4" class="form-control" name="Address_map">
                                        <?php 
                                            echo"<option value=''>Select Address</option>";
                                            foreach ($map_list as $k => $v) { ?>
                                            <option value="<?php echo $v['Id']; ?>" <?php if($presentation_list[0]['Address_map'] == $v['Id']) { ?> selected <?php } ?>><?php echo $v["Map_title"]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Select Presentation Mode <span class="symbol required"></label></label>
                                <div class="col-sm-9">
                                    <select id="presentation_file_type" class="form-control" name="presentation_file_type">
                                        <option value="">Select Presentation Mode</option>
                                        <option value="0" <?php if($presentation_list[0]['presentation_file_type']=='0'){ ?> selected="selected" <?php } ?>>Images Presentation</option>
                                        <option value="1" <?php if($presentation_list[0]['presentation_file_type']=='1'){ ?> selected="selected" <?php } ?>>PPT Presentation</option>
                                    </select>       
                                </div>
                            </div>

                             <div class="form-group" <?php if($presentation_list[0]['presentation_file_type']=='1' || $presentation_list[0]['presentation_file_type']==''){ ?> style="display:none;" <?php } ?> id="presentation_images">
                                <label class="col-sm-2" for="form-field-1">Slides</label>
                                <em class="col-sm-9" style="color:#999;">Note:To add PPT slides go to Powerpoint and click File on the top menu bar. Under File select Export to convert your slides to JPG files and then you can upload them here. After you have uploaded your files you can change the order of the slides by dragging and dropping them into the desired order.</em>                        
                                <div class="col-sm-9" style="margin-left:17%;" id="sortable">
                                    
                                    <?php $images_array = json_decode($presentation_list[0]['Images']); ?>
                                     
                                    <?php for($i=0;$i<count($images_array);$i++) { ?>
                                       <div class="col-sm-3 fileupload-new thumbnail center ui-state-default" style="float:left;margin-right:10px;width: auto;height: auto;border: none;">
                                            <?php 
                                            $fname = $images_array[$i];
                                            $ext = pathinfo($fname, PATHINFO_EXTENSION);
                                            //echo $ext; exit; 
                                            if($ext == 'docx' || $ext == 'doc')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/docs.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'pdf')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/pdf.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'ppt')
                                            {
                                            
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/ppt.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'odg')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/ppt.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'txt')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/txt2.png"></a>
                                            <?php
                                            }
                                            elseif($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')
                                            {
                                            ?>
                                                <img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $fname; ?>">
                                            <?php
                                            }
                                            ?>
                                            <?php $test = json_decode($presentation_list[0]['Image_lock']); ?>
                                            <a class="btn btn-red remove_image" href="javascript:;" style="padding: 3px;margin-top: 0px;"><i class="fa fa-times fa fa-white"></i></a>
                                            <input type="checkbox" name="Image_lock[]" <?php if($test[$i] =='1') { ?> checked="checked" <?php } ?> value="<?php echo $images_array[$i]; ?>">
                                            <input type="radio" name="Image_current" <?php if($presentation_list[0]['Image_current']==$images_array[$i]) { ?> checked="checked" <?php } ?> value="<?php echo $images_array[$i]; ?>">
                                            <input type="hidden" name="old_image_lock[]" value="<?php echo $test[$i] ?>">
                                            <input type="hidden" name="old_images[]" value="<?php echo $images_array[$i]; ?>">
                                        </div>
                                    <?php } ?> 
                                    <div class="col-sm-3 fileupload-new thumbnail center" style="float:left;margin-right:10px;width: auto;height: auto;border: none;" id="filediv">
                                         <input name="Images[]" type="file" id="file1" onchange="preview(this);" multiple="true">
                                     </div> 
                                </div>
                            </div>

                            <div class="form-group" id="presentation_ppt" <?php if($presentation_list[0]['presentation_file_type']=='0' || $presentation_list[0]['presentation_file_type']==''){ ?> style="display:none;" <?php } ?>>
                                <label class="col-sm-2" for="form-field-1">PPT</label>
                                <div class="col-sm-9" style="margin-left:17%;"  id="sortable1">
                                    <?php $images_array = json_decode($presentation_list[0]['Images']); ?>
                                     
                                    <?php for($i=0;$i<count($images_array);$i++) { ?>
                                       <div class="col-sm-3 fileupload-new thumbnail center ui-state-default" style="float:left;margin-right:10px;width: auto;height: auto;border: none;">
                                            <?php 
                                            $fname = $images_array[$i];
                                            $ext = pathinfo($fname, PATHINFO_EXTENSION);
                                            //echo $ext; exit; 
                                            if($ext == 'ppt')
                                            {
                                            ?>
                                                <a style="display: block;" target="_blank" href="<?php echo base_url(); ?>assets/user_files/presentations/<?php echo $fname; ?>"><img style="width:50px !important;height:50px !important;" src="<?php echo base_url(); ?>assets/images/icon/ppt.png"></a>
                                            <?php
                                            }
                                            ?>
                                            <?php $test = json_decode($presentation_list[0]['Image_lock']); ?>
                                            <a class="btn btn-red remove_ppt" href="javascript:;" style="padding: 3px;margin-top: 0px;"><i class="fa fa-times fa fa-white"></i></a>
                                        </div>
                                    <?php } ?>
                                    <div  class="col-sm-3 fileupload-new thumbnail center" style="float:left;margin-right:10px;width: auto;height: auto;border: none;<?php if(!empty($images_array)){ ?>display: none; <?php } ?>" id="filediv1"> 
                                    <em class="col-sm-9" style="color:#999;">Note:Only .PPT Extensions will allowed.</em>
                                    <input name="PPT" type="file" id="file2">
                                    </div> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Update <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
					
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/edit_script.js"></script>

<script type="text/javascript">
$(document).ready(function(){
   //$( "#sortable" ).sortable();
     //  $( "#sortable" ).disableSelection();       
});  
$(document).on("click", (".remove_ppt"), function() 
{
     $.ajax({
        url: "<?php echo base_url().'presentation_admin/delete_pptfile/'.$event['Id'].'/'.$presentation_list[0]['Id']; ?>", 
        success: function(result){
            $('#filediv1').show();
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } );
        }
    });
    
});     
$(document).on("click", (".remove_image"), function() 
{
    $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
});
</script>

<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
            
       $( "#sortable" ).sortable();
       $( "#sortable" ).disableSelection();     
    });
</script>
<script language="javascript" type="text/javascript">
window.preview = function (input) {
    if (input.files && input.files[0]) {
        $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            $("#abcd1").hide();
            reader.onload = function (e) {
                $("#sortable").append("<img src='" + e.target.result + "' Width='100' height='100'>");
            }
        });
    }
}

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } ?>