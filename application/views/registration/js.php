<script>
    jQuery(document).ready(function() {
        FormElements.init();
        FormValidator.init();
    });
   function sendforgotmail()
    {           
        $.ajax({
        url : '<?php echo base_url(); ?>Login/forgot_pas',
        data :'email='+$("#forgotemail").val(),
        type: "POST",           
        success : function(data)
        {
            var values=data.split('###');
            if(values[0]=="error")
            {   
                $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
                $('#forgotemail').parent().find('.control-label span').removeClass('valid');
                $('#forgot_msg').html(values[1]);
                $("#forgot_msg").removeClass('no-display');
                $("#forgot_msg").removeClass('alert-success');
                $("#forgot_msg").fadeIn();
            }
            else
            {
                $('#forgot_msg').html(values[1]);
                $("#forgot_msg").removeClass('no-display').addClass('alert-success');
                $("#forgot_msg").removeClass('alert-danger');
                $("#forgot_msg").fadeOut(3000);
            }
            
        }
        });
    }
</script>
