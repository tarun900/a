<div class="row">
    <div class="col-sm-12">
    <div class="panel panel-white">
    <div class="panel-body">
    	<form class="" method="post" name="form" id="form" action="<?php echo base_url(); ?>Profile/set_session_time/<?php echo $this->uri->segment(3); ?>">
	    	<div class="form-group">
	            <label class="control-label">
	                    Session Time
	            </label> <span class="symbol required"></span>
	    		<input type="text" placeholder="Session Logout Time(1 second=10000)" id="session_time" name="session_time" class="form-control" value="<?php echo $session_time[0]['session_time']; ?>">
	    	</div>
	    	<div class="row">
	           <div class="col-md-4">
	               <button class="btn btn-green btn-block" type="submit" id="save"style="margin-top:20px;">
	                       Update <i class="fa fa-arrow-circle-right"></i>
	               </button>
	           </div>
	       </div>
	    </form>
	    </div>
	    </div>
    </div>
</div>