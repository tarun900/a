<?php $user = $this->session->userdata('current_user'); ?>
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />
<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/demo.css" />
<?php if($user_overview[0]->Role_name=="Speaker"){ ?>
    <style type="text/css">
        #panel_edit_account .fileupload .thumbnail > img {
            border-radius: 0px !important;
        }
        #panel_overview .fileupload .thumbnail > img {
            border-radius: 0px !important;
        }
    </style>
<?php } ?>
<?php //echo "<pre>" ; print_r($user_overview); echo $user_overview[0]->Role_name; die; ?>
<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
<?php if($this->session->flashdata('filemsg')!=NULL){?>
<div class="errorHandler alert alert-danger"> 
<i class="fa fa-remove-sign"></i>
<?php echo $this->session->flashdata('filemsg'); ?>
</div>
<?php } ?>
            <div style="top: 7px;right:21px;" class="back-button list_page_btn">
                <input type="button" class="btn btn-green btn-block" onclick="history.back(-1)" value="Back">
            </div>

            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#panel_overview">
                        Overview
                    </a>
                </li>                                                                            
                <li>
                    <a data-toggle="tab" href="#panel_edit_account">
                        Edit Profile
                    </a>
                </li>
                <?php if(!empty($arrSingupForm)) { $arr=json_decode($arrSingupForm);?>
                 <li class="">
                    <a data-toggle="tab" href="#extra_info">
                        Extra Info
                    </a>
                </li>   
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div id="panel_overview" class="tab-pane fade active in at_tabpan">
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <div class="user-left">
                                <div class="center">
                                    <h5>

                                        <?php
                                           
                                           if(!empty($user_overview[0]->Salutation) && !empty($user_overview[0]->Title) && !empty($user_overview[0]->Company_name))
                                            {

                                                 echo $user_overview[0]->Salutation.". ".$user_overview[0]->Firstname." ".$user_overview[0]->Title." At ".$user_overview[0]->Company_name; 
                                            }
                                            else
                                            {
                                                echo $user_overview[0]->Firstname;
                                            }
                                        ?>
                                    </h5>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="user-image">
                                            <div class="fileupload-new thumbnail" style="border:1px;">
                                                <?php if($user_overview[0]->Logo != '') {  ?>
                                                <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $user_overview[0]->Logo; ?>" alt="">
                                                <?php } else {?>
                                                <img src="<?php echo base_url(); ?>assets/images/anonymous.jpg" alt="">
                                                <?php } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                        </div>
                                    </div>                                    
                                </div> 
                                <?php  if($linkdin_login!='0' && $user_overview[0]->Id==$user[0]->Id){  ?>
                                   <div class="col-md-5" id="linkdin_import">
                                       <a href="<?php echo base_url().'Profile/getlinkdinprofile/'.$user_overview[0]->Id; ?>" class="btn btn-green btn-block" type="button" id="import_linkdin_profile"style="margin-top:20px;">
                                            Import linkedin Profile
                                       </a>
                                   </div>
                                <?php } ?>                                 
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Street</td>
                                            <td><?php echo $user_overview[0]->Street; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Suburb</td>
                                            <td><?php echo $user_overview[0]->Suburb; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <!-- <tr>
                                            <td>State</td>
                                            <td><?php echo $user_overview[0]->state_name; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr> -->
                                        <tr>
                                            <td>Postcode</td>
                                            <td><?php echo $user_overview[0]->Postcode; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>                                        
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Email:</td>
                                            <td>
                                                <a href="">
                                                    <?php echo $user_overview[0]->Email; ?>
                                                </a></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile:</td>
                                            <td><?php echo $user_overview[0]->Mobile; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Business Phone</td>
                                            <td><?php echo $user_overview[0]->Phone_business; ?></td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="extra_info" class="tab-pane fade at_tabpan">
                       <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <div class="user-left">
                                <div class="center">
                                                     
                                </div>                                
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Extra Users Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach($arr as $key => $value) { ?>
                                         
                                      
                                        <tr>
                                            <td><?php echo $key;?></td>
                                            <td><?php if(is_array($value)) { $str=implode(',',$value);echo $str;} else { echo $value; } ?></td>
                                            
                                        </tr>
                                        <?php }?>
                                </table>
                             
                            </div>
                        </div>
                    </div>  
                </div>
                <div id="panel_edit_account" class="tab-pane fade at_tabpan">
                    <form action="<?php echo base_url(); ?>Profile/updateclient/<?php echo $user_overview[0]->Id; ?>" method="post" enctype="multipart/form-data" role="form" id="form">
                        <div class="row">
                            <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
                            <div class="errorHandler alert alert-success no-display" style="display: block;">
                                <i class="fa fa-remove-sign"></i> Updated Successfully.
                            </div>
                            <?php } ?>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label">
                                        Salutation 
                                    </label>
                                    <input type="text" class="form-control" id="Salutation" name="Salutation" value="<?php echo $user_overview[0]->Salutation; ?>">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        First Name <span class="symbol required"></span>
                                    </label>
                                    <input type="text" placeholder="First Name" class="form-control" id="Firstname" name="Firstname" value="<?php echo $user_overview[0]->Firstname; ?>">
                                    <input type="hidden" placeholder="idval" class="form-control" id="idval" name="idval" value="<?php echo $user_overview[0]->Id;?>">
                                    <input type="hidden" placeholder="eventid" class="form-control" id="eventid" name="eventid" value="<?php echo $eventid;?>">
                                    <input type="hidden" placeholder="eventid" class="form-control" id="" name="url" value="<?php echo $url;?>">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        Last Name <span class="symbol required"></span>
                                    </label>
                                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname" value="<?php echo $user_overview[0]->Lastname; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Job Title  
                                    </label>
                                    <input type="text"  class="form-control" id="title1" name="Title1" value="<?php echo $user_overview[0]->Title; ?>">   
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        Company Name 
                                    </label>
                                    <input type="text" placeholder="Company Name" class="form-control" id="Company_name" name="Company_name" value="<?php echo $user_overview[0]->Company_name; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Email <span class="symbol required"></span>
                                    </label>
                                    <input type="text" placeholder="Email" class="form-control required" id="email" name="email" value="<?php echo $user_overview[0]->Email; ?>" onblur="checkemail();" <?php if($user[0]->Role_id!='3'){ ?> readonly="true" <?php } ?>>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">
                                                Password <span class="symbol required"></span>
                                        </label>
                                        <input type="password" class="form-control" id="update_password" name="update_password">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">
                                                Confirm Password <span class="symbol required"></span>
                                        </label>
                                        <input type="password" class="form-control" id="update_password_again" name="update_password_again">
                                    </div>
                                </div>
                                <?php if($user[0]->Role_name=='Client') {  ?>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                       <label for="form-field-select-1">
                                            Status <span class="symbol required"></span>
                                        </label>
                                        <select id="form-field-select-1" class="form-control required" name="Active">
                                            <option <?php if($user_overview[0]->Active=="1"){ ?> selected <?php } ?> value="1">Active</option>                                                                                
                                            <option <?php if($user_overview[0]->Active=="0"){ ?> selected <?php } ?> value="0">Inactive</option>
                                        </select>                                                                
                                    </div>
                                </div>
                                <?php
                                    $role_per=array(6,7);
                                   if($roles!='' && (!in_array($user_overview[0]->Rid,$role_per)))
                                   {
                                ?>
                                <?php if($user[0]->Rid != '3'): ?>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="form-field-select-1">
                                            Select Role <span class="symbol required"></span>
                                        </label>
                                        <select id="form-field-select-1 Role_id" class="form-control required" name="Role_id">
                                             <option value="">Select Role...</option>
                                             <?php 
                                             foreach ($roles as $key => $value) 
                                             {
                                                   $op_val=preg_replace('/\s+/', '',$value['Name']);
                                             ?>
                                                    <option value="<?php echo $value['Id'];?>" <?php if($user_overview[0]->Role_id==$value['Id']){ ?> selected <?php } ?>><?php echo $op_val;?></option>
                                             <?php
                                             }
                                             ?>
                                         </select>                                                      
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php  
                                   }
                                   }  ?>
                                 <div class="row">
                                    <div class="form-group col-md-12">
                                       <label for="form-field-select-1">
                                            Is profile <span class="symbol required"></span>
                                        </label>
                                        <select id="form-field-select-1" class="form-control" name="Isprofile">
                                            <option <?php if($user_overview[0]->Isprofile=="1"){ ?> selected <?php } ?> value="1">Public</option>                                                                                
                                            <option <?php if($user_overview[0]->Isprofile=="0"){ ?> selected <?php } ?> value="0">Private</option>
                                        </select>                                                                
                                    </div>
                                </div>
                                    <!--<div class="form-group">
                                        <em>Website URL(e.g: https://www.yoursite.com)</em><span class="symbol"></span>
                                        <div class="col-sm-12" style="padding-left: 0;margin-bottom: 10px;">
                                            <input type="text" placeholder="Website Url" id="website_url" name="website_url" class="form-control" value="<?php echo $social_links[0]['Website_url'] ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <em>Facebook URL(e.g: https://www.yoursite.com)</em><span class="symbol"></span>
                                        <div class="col-sm-12" style="padding-left: 0;margin-bottom: 10px;">
                                            <input type="text" placeholder="Facebook Url" id="facebook_url" name="facebook_url" class="form-control" value="<?php echo $social_links[0]['Facebook_url'] ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <em>Twitter URL(e.g: https://www.yoursite.com)</em><span class="symbol"></span>
                                        <div class="col-sm-12" style="padding-left: 0;margin-bottom: 10px;">
                                            <input type="text" placeholder="Twitter Url" id="twitter_url" name="twitter_url" class="form-control" value="<?php echo $social_links[0]['Twitter_url'] ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <em>LinkedIn URL(e.g: https://www.yoursite.com)</em><span class="symbol"></span>
                                        <div class="col-sm-12" style="padding-left: 0;margin-bottom: 10px;">
                                            <input type="text" placeholder="LinkedIn Url" id="linkedin_url" name="linkedin_url" class="form-control" value="<?php echo $social_links[0]['Linkedin_url'] ?>">
                                        </div>
                                    </div>-->
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Phone Business 
                                            </label>
                                            <input type="text" placeholder="Phone Business" class="form-control" id="Phone_business1" name="Phone_business" value="<?php echo $user_overview[0]->Phone_business; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                              Mobile No 
                                            </label>
                                            <input type="text" placeholder="Mobile No" class="form-control" id="Mobile" name="Mobile" value="<?php echo $user_overview[0]->Mobile; ?>">
                                        </div>
                                    </div>
                                </div>   
                                                         
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    Street 
                                            </label>
                                            <input type="text" placeholder="Street" class="form-control" id="Street" name="Street" value="<?php echo $user_overview[0]->Street; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    Suburb 
                                            </label>
                                            <input type="text" placeholder="Suburb" class="form-control" id="Suburb" name="Suburb" value="<?php echo $user_overview[0]->Suburb; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    Zip Code 
                                            </label>
                                            <input type="text" placeholder="Post Code" id="zipcode" name="zipcode" class="form-control" value="<?php echo $user_overview[0]->Postcode; ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    Country 
                                            </label>
                                            <select id="country" onchange="get_state();" class="form-control" name="Country">
                                                <option value="">Select...</option>
                                                <?php foreach($countrylist as $key=>$value) {  ?>
                                                        <option value="<?php echo $value['id']; ?>" <?php if($user_overview[0]->Country==$value['id']) { ?> selected <?php } ?>><?php echo $value['country_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    State 
                                            </label>
                                            <select id="state" class="form-control" name="state">
                                                <option value="">Select...</option>
                                                <?php foreach($Statelist as $key=>$value) {  ?>
                                                        <option value="<?php echo $value->id; ?>" <?php if($user_overview[0]->State==$value->id) { ?> selected <?php } ?>><?php echo $value->state_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                    Auto Fill 
                                            </label>
                                            <input type="checkbox" name="auto-fill" id="auto-fill" checked="true">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Biography</label>
                                            <?php if($user_overview[0]->Role_id == '4') { ?>
                                                <input type="text" id="Speaker_desc" name="Speaker_desc" value="<?php echo strip_tags($user_overview[0]->Speaker_desc); ?>" class="form-control">
                                            <?php } else { ?>
                                                <textarea id="Speaker_desc" name="Speaker_desc" class="summernote"><?php echo $user_overview[0]->Speaker_desc; ?></textarea>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                
                                    <label>
                                        Image Upload
                                    </label>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail">
                                        <?php if($user_overview[0]->Logo != '') {   ?>  
                                            <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $user_overview[0]->Logo; ?>" alt="">
                                            <?php  } else { ?>
                                            <img src="<?php echo base_url(); ?>assets/images/anonymous.jpg" alt="">
                                            <?php  }  ?>
                                    </div>

                                    <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                        <?php if($user_overview[0]->Logo != '') { ?>
                                            <div class="user-edit-image-buttons">
                                                <!--onclick="return confirm('Are you sure?')">-->
                                                <a href="#" class="btn btn-red fileupload-new" data-dismiss="fileupload" onclick="delete_image();">
                                                  <i class="fa fa-times"></i> Delete Image
                                                </a>
                                            </div>
                                        <?php }  ?>
                                        <div class="user-edit-image-buttons">
                                            <div class="actions">
                                            <a class="btn file-btn">
                                                <span>Upload Crop Image</span>
                                                <input type="file" name="userfile" id="upload" value="Choose a file" accept="image/*" />
                                            </a>
                                            <input type="hidden" name="img_val" id="img_val">
                                        </div>
                                            <!-- <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                               <input type="file" name="userfile">
                                            </span> -->
                                            <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-4">
                                       <button class="btn btn-green btn-block" type="submit" id="save"style="margin-top:20px;">
                                               Update <i class="fa fa-arrow-circle-right"></i>
                                       </button>
                                   </div>
                               </div>
                            </div>                          
                        </div>
                    </form>
                </div>   
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog" style="display:none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Profile Logo</h4>
        </div>
        <div class="modal-body">
          <section>
                <div class="demo-wrap" style="display:none;">
                    <div class="container">
                        <div class="grid">
                            <div class="col-1-2">
                                <div id="vanilla-demo"></div>
                            </div>
                            <div class="col-1-2">
                                <strong>Vanilla Example</strong>
                               
                                <div class="actions">
                                    <button class="vanilla-result">Result</button>
                                    <button class="vanilla-rotate" data-deg="-90">Rotate Left</button>
                                    <button class="vanilla-rotate" data-deg="90">Rotate Right</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="demo-wrap upload-demo">
                    <div class="container">
                    <div class="grid">
                        <div class="col-1-2">
                            <div class="upload-msg">
                                Upload a file to start cropping
                            </div>
                            <div id="upload-demo"></div>
                            <button class="upload-result" data-dismiss="modal">Crop</button>
                        </div>
                    </div>
                </div>
                </div>
                
        </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/prism.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image_crop/exif.js"></script>
<script>
    Demo.init();
</script>
<script type="text/javascript">  
    
      
        $("#country").change(function get_state(){
          
            $.ajax({
            url:"<?php echo base_url(); ?>Profile/getnewstate",    
            data: {id: $(this).val()},
            type: "POST",
            success: function(data)
            {
                
                $("#state").html(data);
            }
            
            });
       
        });
        $('#auto-fill').change(function () {
           if(!$( "#auto-fill" ).prop("checked"))
           {
                $.ajax({
                url:"<?php echo base_url(); ?>Profile/getnewstate",    
                data: {id: $(this).val(),flag:1},
                type: "POST",
                success: function(data)
                {
                    
                    $("#state").html(data);
                }
                
                });
           }
           else
           {
                $.ajax({
                url:"<?php echo base_url(); ?>Profile/getnewstate",    
                data: {id: $('#country').val()},
                type: "POST",
                success: function(data)
                {
                    
                    $("#state").html(data);
                }
                
                });
           }
        });
            
</script>




<script type="text/javascript">
    function delete_image()
    {
        if(confirm("Are you sure to delete this image?"))
        {
            window.location.href='<?php echo base_url(); ?>Profile/deleteprofile/<?php echo $user_overview[0]->Id;?>';
        }
    }
    
    function display_event_client(id)
    {
        <?php
        
            if($this->data['user']->Role_name=="Client")
            {
        ?>
                window.location.href = "<?php echo base_url(); ?>Order/view_staus/" + id;
        <?php
            }
            else
            {
        ?>
                window.location.href = "<?php echo base_url(); ?>Order/approveorders/" + id;
        <?php
            }
        ?>
    }
    
    function delete_event(id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Event/delete/" + id;
        }
    }
    
    function edit_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>Event/edit/" + id;
    }
    
    function display_event(id)
    {
         window.location.href = "<?php echo base_url(); ?>Event/detail/" + id;
    }

    jQuery("#myTab4 li a").click(function(e){
            e.stopPropagation();
            var strId = jQuery(this).attr("href").substring(1);
            jQuery("#myTab4 li").removeClass("active");
            jQuery(this).parent("li").addClass("active");  
            jQuery(".at_tabpan").removeClass("active");
            jQuery(".at_tabpan").removeClass("in");
            jQuery("#"+strId).addClass("active");
            jQuery("#"+strId).addClass("in");
    });
</script>
<script type="text/javascript">
    $('#update_password_again').blur(function(){
        if($(this).val()==$('#update_password').val())
        {
            $('#update_password_again').parent().removeClass('has-error').addClass('has-success');
            $('#update_password_again').parent().find('.control-label span').removeClass('required').addClass('ok');
            $('#update_password_again').parent().find('.help-block').addClass('valid').html(''); 
        }
    });
</script>
<style type="text/css">
a#import_linkdin_profile {
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-left: 40px;
    margin-top: 5px;
    padding: 3px 9px;
}
#linkdin_import #import_linkdin_profile
{
    display: inline-block;     
    margin: 0 auto;     
    text-align: center;     
    background: rgba(0,0,0,0) url("<?php echo base_url().'assets/images/linkedin-btn.png'; ?>") no-repeat;     
    font-size: 0;    
    width: 191px;     
    height: 38px;     
    background-size: 100% auto;    
    border: none;    
    padding: 0;
}
</style>