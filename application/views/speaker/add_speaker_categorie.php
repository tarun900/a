<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/image_crop/croppie.css" />    
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/cropper-master/dist/cropper.css"> 
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Add <span class="text-bold">Speaker Categorie</span></h4>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="categorie_name">
              Speaker Categorie Name <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <input type="text" placeholder="Speaker Categorie Name" class="form-control required" id="categorie_name" name="categorie_name">
            </div>
          </div>
          <div class="form-group" id="keyword">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Key Words <span class="symbol required"></span>
            </label>
            <div class="col-sm-9">
              <textarea id="Short_desc" class="form-control required tags" name="Short_desc"></textarea>     
            </div>
          </div>
          <!-- <div class="form-group">
            <label class="col-sm-2" for="form-field-1">
              Exhibitor Category Icon 
            </label>
            <div class="modal fade" id="categorieiconcropetool" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close close_banner_popup" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Exhibitor Category Icon</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row"  id="cropping_banner_div">
                      <div>
                        <img class="img-responsive" id="show_crop_icons_model" src="" alt="Picture">
                      </div>
                      <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-green btn-block" id="upload_result_btn_icon_crop" style="width: 120px;margin: 0 auto;" data-dismiss="modal">Crop</button>   
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-9">
              <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden" value="" name="">
                <div class="fileupload-preview fileupload-exists thumbnail" id="categories_icons_preview_images">
                </div>
                <span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                  <input type="file" id="categories_icons" name="categories_icons">
                </span>
                <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload" id="remove_categorie_icons_data"><i class="fa fa-times"></i> Remove</a>
              </div>
              <input type="hidden" name="categorie_icons_crop_data" id="categorie_icons_crop_data">
            </div>
          </div> -->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Submit <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/cropper-master/dist/cropper.js"></script>
<script type="text/javascript">
var $bannerimage = $("#show_crop_icons_model");
var $inputBannerImage = $('#categories_icons'); 
$(document).ready(function() {
  $('#Short_desc').tagsInput({
    maxTags: 6,
    allowSpaces: false,
    removeWithBackspace : true
  });
  $inputBannerImage.change(function(){
    var uploadedImageURL;
    var URL = window.URL || window.webkitURL;
    var files = this.files;
    var file;
    if (files && files.length) {
      file = files[0];
      if (/^image\/\w+$/.test(file.type)) {
        if (uploadedImageURL) {
          URL.revokeObjectURL(uploadedImageURL);
        }
        uploadedImageURL = URL.createObjectURL(file);
        $bannerimage.attr('src', uploadedImageURL);
        $('#categorie_icons_crop_data').val('');
        $('#categorieiconcropetool').modal('toggle');
      } else {
        window.alert('Please choose an image file.');
      }
    }
  });
  $(document).on('shown.bs.modal','#categorieiconcropetool' ,function () {
    $bannerimage.cropper('destroy');
    var croppable = false;
    var $button = $('#upload_result_btn_icon_crop');
    $bannerimage.cropper({ 
      aspectRatio: 1, 
      built: function () {
        croppable = true;
      }
    });
    $button.on('click', function () {
      var croppedCanvas;
      if (!croppable) {
        return;
      }
      croppedCanvas = $bannerimage.cropper('getCroppedCanvas');
      $('#categories_icons_preview_images').html("<img src='"+croppedCanvas.toDataURL()+"'>");
      $('#categorie_icons_crop_data').val(croppedCanvas.toDataURL());
      $("#categories_icons").val('');
    });
  }).on('hidden.bs.modal',function(){
    $bannerimage.cropper('destroy');
  }); 
});
</script>