<?php $acc_name=$this->session->userdata('acc_name'); ?>
<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-body" style="padding: 0px;">
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#speaker_list" data-toggle="tab">
                                Add Moderator
                            </a>
                        </li>
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="speaker_list">
                        <form role="form" method="post" class="smart-wizard form-horizontal" id="form" action="" enctype="multipart/form-data">
                            <div id="wizard" class="swMain">
                                <ul style="margin-top:15px;">
                                    <li>
                                        <a href="#step-1">
                                            <div class="stepNumber">
                                                1
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Moderator Name & Email</small></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <div class="stepNumber">
                                                2
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Invite Email</small></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <div class="stepNumber">
                                                3
                                            </div>
                                            <span class="stepDesc"> 
                                            <small>Finish</small></span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress progress-xs transparent-black no-radius active">
                                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                    </div>
                                </div>
                                <div id="step-1">
                                    <div class="col-sm-6">
                                        <h4>What is the name of the Moderator you would like to add?</h4>
                                        <div class="form-group">
                                            <label class="control-label">First Name <span class="symbol required"></span>
                                            </label>
                                            <input type="text" placeholder="First Name" class="form-control" id="Firstname" name="Firstname">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Last Name <span class="symbol required"></span>
                                            </label>
                                            <input type="text" placeholder="Last Name" class="form-control" id="Lastname" name="Lastname">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">
                                                Select Key People <!-- <span class="symbol required"></span> -->
                                            </label>
                                            <select id="keypeople" class="select2-container select2-container-multi form-control search-select menu-section-select" name="keypeople[]" multiple>
                                                <?php foreach($keypeople as $key=>$value) {  ?>
                                                    <option value="<?php echo $value['uid']; ?>"><?php echo ucfirst($value['Firstname']).' '.$value['Lastname']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Enter the person’s email and set a password</h4>
                                        <p>By entering the person’s email and setting a password they will be able to log in to the app, receive live questions and share their contact details upon request with other app users. If you do not want this person to have an account use another email address so you can moderate questions asked.</p>
                                        <div class="form-group">
                                            <label class="control-label">Email <span class="symbol required"></span>
                                            </label>
                                            <input type="text" placeholder="Email" class="form-control" id="email" name="email" onblur="checkemail()">
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Password <span class="symbol required"></span></label>
                                                <input type="password" class="form-control" id="password" name="password">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Confirm Password <span class="symbol required"></span></label>
                                                <input type="password" class="form-control" id="password_again" name="password_again">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button id="first_step_next_btn" class="btn btn-theme_green next-step btn-block">
                                                    Next Step >>>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2">
                                    <h4>Invite your Moderator to log in, view and edit their profile details</h4>
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <div class="form-group">
                                            <textarea id="Speaker_mail" name="Speaker_mail" class="summernote">
                                                <p>Dear {{sfnm}},</p>
                                                <p>Your Speaker Profile has been created on our app.</p>
                                                <p>To access the app please visit {{applink}} and use the following credentials to login:</p>
                                                <p>Username:{{semail}}</p>
                                                <p>Password:{{spassword}}</p>
                                                <p>After you have logged in click the button on the top right and select  ‘My Profile’. There you can view and edit your contact details, biography and profile image.</p>
                                                <p>Best regards,</p>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Subject</label>
                                            <input type="text" placeholder="Enter Email Subject" class="form-control" id="subject" name="subject">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Sender Email</label>
                                            <input type="text" placeholder="Enter Sender Email Address" class="form-control" id="sender_email" name="sender_email">
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input id="emailpreview" type="button" class="btn btn-yellow btn-block" data-toggle="modal" data-target="#myModal" value="Preview">
                                            </div>
                                        </div>
                                        <div class="form-group">    
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <button class="btn btn-theme_green next-step btn-block">
                                                    Send Mail
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <button class="btn btn-theme_green next-skip btn-block">
                                                    Skip
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3">
                                    <div class="col-sm-6">
                                        <h4>Your Moderator had been added to your app just click on finish button!</h4>
                                        <div id="result_div"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4" style="padding-left:0px;">
                                                <button type="submit" id="finishform" class="btn btn-theme_green btn-block" value="Finish">Finish</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="viewport_speaker" class="iphone">
                                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>

<style type="text/css">
.form-horizontal .form-group { margin-right: 0px !important; margin-left: 0px !important; }
#viewport_speaker.iphone {
    background: url('../../../assets/images/demo-iphone5s.png') no-repeat;
    width: 437px;
    height: 868px;
    margin: 0 auto;
    position: relative;
}
</style>
<!-- end: PAGE CONTENT-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">

        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript">  
$('#emailpreview').click(function(){
    var str='';
    if($.trim($('#subject').val())!="")
    {
        str="<p>Subject:"+$.trim($('#subject').val())+"</p>";
    }
    if($.trim($('#sender_email').val())!="")
    {
        str+="<p>Sender Email:"+$.trim($('#sender_email').val())+"</p>Contant:";
    }
    str+=$('#Speaker_mail').code();
    str=str.replace('{{sfnm}}',$('#Firstname').val());
    str=str.replace('{{applink}}',"<?php echo base_url().$acc_name.'/'.$event['Subdomain']; ?>");
    str=str.replace('{{semail}}',$('#email').val());
    str=str.replace('{{spassword}}',$('#password').val());
    $('.modal-body').html(str);
});   
</script>