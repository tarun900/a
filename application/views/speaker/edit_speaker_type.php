<?php 
function output ($tmp){
    foreach (range(1, 50) as $number) {
      $a = ($tmp == $number) ? "selected":"";
      echo '<option '.$a.' value='.$number.'>';
      echo ordinalize($number);
      echo '</option>';
      
    }
}

function ordinalize($num) {
    $suff = 'th';
    if ( ! in_array(($num % 100), array(11,12,13))){
        switch ($num % 10) {
            case 1:  $suff = 'st'; break;
            case 2:  $suff = 'nd'; break;
            case 3:  $suff = 'rd'; break;
        }
        return "{$num}{$suff}";
    }
    return "{$num}{$suff}";
}
?>
<script src="<?php  echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jscolor/jscolor.js"></script>

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Edit <span class="text-bold">Speaker Type</span></h4>
        <div class="panel-tools">
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Speaker Type Name <span class="symbol required"></span>
            </label>
            <div class="col-sm-6">
              <input type="text" placeholder="Speaker Type Name" class="form-control required" value="<?php echo $type_data[0]['type_name']; ?>" id="Firstname" name="speaker_type_name">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Position in Directory <span class="symbol required"></span>
            </label>
            <div class="col-sm-6">
              <select name="position_in_directory" class="form-control required" id="selRegs">
                <option value="">Select Position</option>
                <?=output($type_data[0]['type_position']);?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
              Color <span class="symbol required"></span>
            </label>
            <div class="col-sm-6 color-pick">
              <?php //$color_arr=array('#e64d43','#e47f31','#fecc2f','#efdeb6','#35495d','#2b2b2b','#995cb3','#3c6f80','#3b99d8','#38ca73','#2abb9b','#ecf0f1','#95a5a5','#365e42','#7460c2','#5e4535','#5d345d','#ed737c','#a6c446','#f27ec2','#78302c','#a18672','#b8caef','#5266a0');
              //foreach ($color_arr as $key => $value) { ?>
                  <input type="text" class="color {hash:true} form-control required" value="<?php echo $type_data[0]['type_color']; ?>" name="custom_color_picker">
              <?php //} ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-1"></label>
            <div class="col-md-4">
              <button class="btn btn-yellow btn-block" type="submit">
                Submit <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#selRegs').focus(function () {
        $('#selRegs').attr("size", "6");                  
    });
    $('#selRegs').change(function () {
        $('#selRegs').attr("size", "1");
        $("#selRegs").blur();
    });
});
</script>