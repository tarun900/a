<?php
$Sid = $this->uri->segment(4);
?>
<?php if(!empty($social_url)) { ?>
<div class="agenda_content panel-white">
    <div class="row" style="margin-bottom: 20px;">                        
        <div class="col-md-1 col-lg-1 col-sm-1">
            <?php if(!empty($social_url[0]['Logo'])) { ?>
            <img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $social_url[0]['Logo'];?>" alt="" />
            <?php } else { ?>
            <img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />
            <?php } ?>
        </div>
        <div class="col-md-11 col-lg-11 col-sm-11">
            <div class="desc">

                <h4><?php echo $social_url[0]["Firstname"].' '.$social_url[0]["Lastname"]; ?></h4><br>
            <?php echo $social_url[0]["Email"]; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <button class="btn btn-danger" type="button" id="user<?php echo $social_url[0]['Id']; ?>">Send them a Message</button>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="social-icons">
                <ul class="navbar-right">
                    <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="bottom">
                        <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                            Visit my Website
                        </a>
                    </li>
                    <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                        <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                            Follow me on Twitter
                        </a>
                    </li>
                    <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                        <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                            Follow me on Facebook
                        </a>
                    </li>
                    <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                        <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                            Follow me on LinkedIn
                        </a>
                    </li>   
                </ul>
            </div>
        </div>                                
    </div>
</div>
<?php } else { ?>
    
    <div class="agenda_content panel-white">
    <div class="row" style="margin-bottom: 20px;">                        
        <div class="col-md-1 col-lg-1 col-sm-1">
            <?php if(!empty($user_url[0]['Logo'])) { ?>
            <img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $user_url[0]['Logo'];?>" alt="" />
            <?php } else { ?>
            <img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />
            <?php } ?>
        </div>
        <div class="col-md-11 col-lg-11 col-sm-11">
            <div class="desc">

                <h4><?php echo $user_url[0]["Firstname"].' '.$user_url[0]["Lastname"]; ?></h4><br>
            <?php echo $user_url[0]["Email"]; ?>
            </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-6 col-lg-6 col-sm-6">
            
            
            <?php
$Sid = $this->uri->segment(4);
$user = $this->session->userdata('current_user');
$lid = $user[0]->Id;

echo '<div style="background: #fff;padding-top: 20px;">
      <div id="messages" style="overflow-y: scroll;height: 400px;width: 99%;padding: 10px;">';
        echo '<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with '.$view_chats1[0]['Recivername'].'<br/></span>';
        foreach ($view_chats1 as $key => $value) 
    	{           
            echo '<strong>From :</strong>' . $value['Sendername'];
            echo '<br/>';
            echo '<strong>To :</strong>' . $value['Recivername'];
            echo '<br/>';
            echo $value['Message'];
            echo '<br/>';
            echo timeAgo(strtotime($value['Time']));
            echo '<br/>';
            echo '<br/>';
    		
    	}
echo "</div></div>";
?>
            <form role="form" id="form" class="form-horizontal" novalidate="novalidate" action="" method="post">
                <?php

                    echo'<input type="hidden" name="Receiver_id" id="" value="'.$value['Id'].'">';

                ?>
                <input type="hidden" name="Sender_id" id="" value="">
                <input type="hidden" name="Event_id" id="event<?php echo $event_templates[0]['Id']; ?>" value="<?php echo $event_templates[0]['Id']; ?>">
                <select name="ispublic">
                    <option value="0">Is Private</option>
                    <option value="1">Is Public</option>
                </select>
                <textarea class="col-md-12 col-lg-12 col-sm-12" id="Message" name="Message" value=""></textarea><br/>
                <input class="btn btn-danger" type="button" name="submit" onclick="sendmessage();" value="Send Message">
            </form>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="social-icons">
                <ul class="navbar-right">
                    <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="bottom">
                        <a href="javascript:void(0)">
                            Visit my Website
                        </a>
                    </li>
                    <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                        <a href="javascript:void(0)">
                            Follow me on Twitter
                        </a>
                    </li>
                    <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                        <a href="#">
                            Follow me on Facebook
                        </a>
                    </li>
                    <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                        <a href="javascript:void(0)">
                            Follow me on LinkedIn
                        </a>
                    </li>   
                </ul>
            </div>
        </div>                                
    </div>
</div>
<?php } ?>
<script type="text/javascript">
    function sendmessage() 
    {
        var str = $("#form").serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>Messages/<?php echo $Subdomain; ?>/chatsviewdata/<?php echo $Sid; ?>",
                    data: str,
                    type: "POST",
                    async: true,
                    success: function(result)
                    {
                       $('#Message').val('');
                       $("#messages").html();
                       $("#messages").html(result);
                    }

            });
    }
</script>
<?php

function timeAgo($time_ago) {
    $cur_time = time();
    $time_elapsed = $cur_time - $time_ago;
    
    $seconds = $time_elapsed;
    $minutes = round($time_elapsed / 60);
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400);
    $weeks = round($time_elapsed / 604800);
    $months = round($time_elapsed / 2600640);
    $years = round($time_elapsed / 31207680);
    // Seconds
    if ($seconds <= 60) {
        echo "$seconds seconds ago";
    }
    //Minutes
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            echo "one minute ago";
        } else {
            echo "$minutes minutes ago";
        }
    }
    //Hours
    else if ($hours <= 24) {
        if ($hours == 1) {
            echo "an hour ago";
        } else {
            echo "$hours hours ago";
        }
    }
    //Days
    else if ($days <= 7) {
        if ($days == 1) {
            echo "yesterday";
        } else {
            echo "$days days ago";
        }
    }
    //Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            echo "a week ago";
        } else {
            echo "$weeks weeks ago";
        }
    }
    //Months
    else if ($months <= 12) {
        if ($months == 1) {
            echo "a month ago";
        } else {
            echo "$months months ago";
        }
    }
    //Years
    else {
        if ($years == 1) {
            echo "one year ago";
        } else {
            echo "$years years ago";
        }
    }
}
?>