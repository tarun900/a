<?php $user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
<style type="text/css">
#anonymous_user {
  margin-right: 10px;
  margin-top: 8px;
}
.facebok-container .submit-button-box #sendbtn.submit-button {
  position: relative;
  top: 8px;
}
.agenda_content.agenda-user-content .user_msg_profile.clearfix.box-effect h4 {
  margin-top: 2px;
}
.agenda_content.agenda-user-content .user_msg_profile.clearfix.box-effect {
  padding: 10px;
}
#imageform .icheckbox_minimal-grey {
  margin-right: 10px;
}
#imageform .checkbox-inline {
  color: #000;
  margin-bottom: 3px !important;
  padding-left: 2px;
}
</style>
<div class="agenda_content agenda-user-content">
    <div class="row row-box"> 
        <div class="user_msg_profile clearfix box-effect">
        	<div class="desc">
                <h4><?php echo ucfirst($qa_session[0]["Session_name"]); ?></h4>
                <?php echo $qa_session[0]["Session_description"]; ?>
            </div>
        </div>
    </div>
</div>
<div class="" id="section_msg">
	<form id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Speakers/<?php echo $acc_name."/".$Subdomain; ?>/upload_imag/<?php echo $Sid; ?>' style="clear:both">
		<div class="facebok-container">
			<div class="facebok-main">
				<div class="facebok-middle">
		        	<textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text" id="Message" placeholder="Type your Messages here..." name="Message"></textarea>
		      	</div>
				<div class="facebok-footer clearfix">
					<ul class="footer-left">
		                <li>
		                	<label class="checkbox-inline">
	                            <input type="checkbox" value="1" class="grey" name="anonymous_user" id="anonymous_user">
	                            Anonymous
	                        </label>
		                </li>
		           	</ul>
					<ul class="footer-right">
						<li class="submit-button-box">
							<button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
							  Send <i class="fa fa-arrow-circle-right"></i>
							</button>
		                </li>
					</ul>
				</div>
			</div>	
		</div>		
	</form>
	<div class="col-sm-12">
		<div class="errorHandler alert alert-success no-display" id="msg_div" style="display: none;">
	        <i class="fa fa-remove-sign"></i> Messages Send Successfully.
	    </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript">
function sendmessage()
{
	$("#sendbtn").html('Sending <i class="fa fa-refresh fa-spin"></i>');
	if($.trim($("#Message").val())=="")
	{
		alert("Please write Message...");
    	$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
	}
	else
	{
		var str = jQuery("#imageform").serialize();
		$.ajax({
			url: "<?php echo base_url().'QA/'.$acc_name.'/'.$Subdomain.'/send_message/'.$sid; ?>",
        	data: str,
        	type: "POST",
        	async: true,
        	success: function(result)
        	{
        		$("#sendbtn").html('Send <i class="fa fa-arrow-circle-right"></i>');
        		$('#Message').val('');
        		$('#anonymous_user').prop('checked',false);
        		$('.footer-left').find('div').removeClass('checked');
        		$('#msg_div').show();
        		setTimeout('$("#msg_div").hide()',2000);
        	}
		});
	}
}
</script>       