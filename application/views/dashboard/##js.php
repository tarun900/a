<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/lib/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/nv.d3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/src/models/historicalBar.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/src/models/historicalBarChart.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/src/models/stackedArea.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/nvd3/src/models/stackedAreaChart.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/index.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>

<script>
	jQuery(document).ready(function() {
		TableData.init();
		Main.init();
		Index.init();
	});

       
	var runChart1 = function() {
		function randValue() {
			return (Math.floor(Math.random() * (100 + 4000 - 2000))) + 2000;
		};
                
    function createSeries() {
            
			var y = date.getFullYear(), m = date.getMonth();
			var firstDay = new Date(y, m, 1);
			var fifthDay = new Date(y, m, 5);
			var tenthDay = new Date(y, m, 10);
			var fifteenthDay = new Date(y, m, 15); 
			var twentiethDay = new Date(y, m, 20);
			var twentyfifthDay = new Date(y, m, 25);
			var lastDay = new Date(y, m + 1, 0);
                        
                for(var d = new Date(new Date().setMonth(new Date().getMonth() )); d <= new Date(new Date().setMonth(new Date().getMonth() +    5)); d.setMonth(d.getMonth() + 1)) {
                        var qty=0;
                        <?php

                        if ($this->data['user']->Role_name == 'Client') {
                            for($i=0;$i<count($event_list);$i++)
                            {
                            ?>
	                           if(d3.time.format('%b')(new Date(d))=='<?php echo date('M',  strtotime($event_list[$i]['Month'])); ?>')
	                           {   
	                               qty=<?php echo $event_list[$i]['total_event']; ?>;
	                               maxvalue.push(qty);
	                           }
	                           <?php
                            }
                        }
                        else if ($this->data['user']->Role_name == 'Attendee') {
                            for($i=0;$i<count($event_list);$i++)
                            {
                            ?>
                               if(d3.time.format('%b')(new Date(d))=='<?php echo date('M',  strtotime($event_list[$i]['Month'])); ?>')
                               {   
                                   qty=<?php echo $event_list[$i]['total_event']; ?>;
                                   //console.log(qty);

                               }
                            <?php
                            }
                        }
                        ?>              
                        series1.push([new Date(d), qty]);
                }
		}

		if($("#chart1 > svg").length) {
			var date = new Date();
			var series1 = [];
			
            var maxvalue=[];
                        
			createSeries();
                        
             var maxValueInArray = Math.max.apply(Math, maxvalue);
                        
			var data = [{
				"key": "Events",
				"bar": true,
				"values": series1
			}];
			nv.addGraph(function() {
				var chart = nv.models.linePlusBarChart().margin({
					top: 15,
					right: 30,
					bottom: 15,
					left: 60
				})
				.x(function(d, i) {
					return i;
				}).y(function(d, i) {
					return d[1];
				}).color(['#DFDFDD', '#E66F6F']);

				chart.xAxis.tickFormat(function(d) {
					var dx = data[0].values[d] && data[0].values[d][0] || 0;
					return d3.time.format('%b')(new Date(dx));
				});

				chart.y1Axis.tickFormat(d3.format(',f'));

				chart.y2Axis.tickFormat(function(d) {
					return d3.format(',f')(d);
				});
                               
                    if(maxValueInArray=="-Infinity")
                    {
                        maxValueInArray=300;
                    }
                   
                    chart.bars.forceY([0, maxValueInArray]);
                    chart.lines.forceY([0, maxValueInArray]);

				d3.select('#chart1 svg').datum(data).transition().duration(0).call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	};
        
        
</script>