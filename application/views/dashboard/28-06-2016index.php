<?php  $acc_name = $this->session->userdata('acc_name');if($this->data['user']->Role_name == "Administrator1")  { ?>
<div class="col-md-6">
    <div class="row space20">
        <div class="col-md-7 col-lg-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <i class="clip-menu"></i>
                    <h4>Most Recent Organizer</h4>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body panel-scroll height-300">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  for ($i = 0; $i < count($recentclient); $i++) { ?>
                                    <tr>
                                        <td><?php echo $recentclient[$i]['Firstname']; ?></td>
                                        <td><?php echo $recentclient[$i]['Email']; ?></td>
                                        <td><span class="label label-sm 
                                            <?php if($recentclient[$i]['Active']=='1') {  ?> 
                                                  label-success 
                                                <?php } else {  ?> 
                                                  label-danger 

                                                <?php  } ?>">

                                                <?php if($recentclient[$i]['Active']=='1')
                                                { 
                                                echo "Active"; 

                                                }
                                                else 
                                                { 
                                                    echo "Inactive";
                                                } 
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php  if ($i == 4)  { break; }  } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="col-md-6">
    <div class="row space20">
        <div class="col-md-7 col-lg-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <i class="clip-menu"></i>
                    <h4>Most Recent Attendees</h4>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body panel-scroll height-300">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($recentattendee); $i++) { ?>
                                    <tr>
                                        <td><?php echo $recentattendee[$i]['Firstname']; ?></td>
                                        <td><?php echo $recentattendee[$i]['Email']; ?></td>
                                        <td>
                                            <span class="label label-sm 
                                                <?php if($recentattendee[$i]['Active']=='1') { ?> 
                                                      label-success 
                                                    <?php } else { ?> 
                                                       label-danger 

                                                    <?php  } ?>">
                                                    <?php if($recentattendee[$i]['Active']=='1')
                                                    { 
                                                    echo "Active"; 

                                                    }
                                                    else 
                                                    { 
                                                        echo "Inactive";
                                                    } 
                                                ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php if ($i == 4) { break;  }  } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="col-lg-12">
    <div class="panel panel-white">
        <div class="panel-heading border-light">
            <h4 class="panel-title">Event Allocation</h4>
        </div>
        <div class="panel-body no-padding">
            <div class="col-md-9 col-lg-10 no-padding partition-white">
                <div class="partition">
                    <div class="partition-body padding-15">
                        <div class="height-300">
                            <div id="chart1" class='with-3d-shadow with-transitions'>
                                <svg></svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="col-lg-12">
<div class="row space20">
<div class="col-md-12" style="padding-left:0px;padding-right:0px;">
<div class="panel-scroll height-320 panel-white ps-container" style="padding-right:0px !important;">
    <?php if($this->session->flashdata('speaker_data')){ ?>
    <div class="errorHandler alert alert-success no-display" style="display: block;">
        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('event_templates_data'); ?> Successfully.
    </div>
    <?php } ?>
    <?php if ($this->data['user']->Role_name == 'Client') { ?>
     <div class="col-md-2 pull-right">
        <a style="top: 7px;right:4%;" class="btn btn-green list_page_btn" href="<?php echo base_url(); ?>event/add"><i class="fa fa-plus"></i> Create New App</a>                
        <!--<a style="top: 7px;" class="btn btn-green list_page_btn" href="javascript: window.history.go(-2)">Back</a>-->
     </div>
     <?php } ?>
    <div class="tabbable">
    <ul id="myTab2" class="nav nav-tabs">
        <li class="">
            <a href="#archive_events" data-toggle="tab">
                Archive Apps<!--Archive Events-->
            </a>
        </li>
        <li class="active">
            <a href="#latest_events" data-toggle="tab">
                Active Apps<!--Active Events-->
            </a>
        </li>
        <li class="">
            <a href="#feature_events" data-toggle="tab">
                Future Apps<!--Future Events-->
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade" id="archive_events">
            <?php  if($Archive_event[0]['Id'] !='' && $this->data['user']->Role_name == 'Client') { ?>

            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
            <!--<span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>-->

            <?php  } ?>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                    <thead>
                        <tr>
                            <?php  if($this->data['user']->Role_name == "Client") {  ?>
                            <th></th> 
                            <?php } ?>
                            <th>#</th>
                            <th>Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;$i<count($Archive_event);$i++) { ?>
                        <tr>
                         <?php  if($this->data['user']->Role_name == "Client") {  ?>
                            <td>
                                <div class="checkbox-table">
                                    <label>
                                        <input type="checkbox" name="event_name[]" value="<?php echo $Archive_event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                    </label>
                                </div>
                            </td>
                            <?php } ?>
                            <td><?php echo $i+1; ?></td>
                            <td>
                                <?php 
                                if($this->data['user']->Role_id==7)
                                {
                                    $link=base_url().'speaker_Messages/index/'.$Archive_event[$i]['Id'];
                                }
                                else
                                {
                                    $link=base_url().'event/edit/'.$Archive_event[$i]['Id'];
                                }
                            ?>
                            <?php echo $Archive_event[$i]['Event_name']; ?> <span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo $link; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Archive_event[$i]['Subdomain']; ?>">View</a><button style="border-radius: 0px;margin-left: 3px;margin-top: -2px;padding: 4px 16px 4px 14px;" data-target=".bs-example-modal-sm<?php echo $Archive_event[$i]['Id']; ?>" data-toggle="modal" class="btn btn-green">Share</button></span></td>
                                
                            <div class="modal fade bs-example-modal-sm<?php echo $Archive_event[$i]['Id']; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 id="myLargeModalLabel" style="text-align:center;" class="modal-title">Social Sharing</h4>
                                        </div>
                                        <div class="modal-body" style="text-align:center;">
                                            <div id="share-buttons">
                                                

                                                <!-- Facebook -->
                                                <a class="fb-bg" href="https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=<?php echo base_url() ?>app/<?php echo $acc_name.'/'.$Archive_event[$i]['Subdomain']; ?>&display=popup&ref=plugin&src=share_button" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/facebook.png" alt="Facebook" />
                                                </a>


                                                <!-- Twitter -->
                                                <a href="https://twitter.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>&amp;hashtags=Support <?php echo ucfirst($acc_name); ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/twitter.png" alt="Twitter" />
                                                </a>


                                                <!-- Email -->
                                                <a href="mailto:?Subject=Allintheloop <?php echo ucfirst($acc_name); ?>&amp;Body=Allintheloop <?php echo ucfirst($acc_name); ?> <?php echo base_url(); ?>app/<?php echo $acc_name."/".$Archive_event[$i]['Subdomain']; ?>">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/email.png" alt="Email" />
                                                </a>
                                             
                                            
                                                <a href="https://plus.google.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Archive_event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/google.png" alt="Google" />
                                                </a> 
                                                
                                                <!-- LinkedIn -->
                                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Archive_event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/linkedin.png" alt="LinkedIn" />
                                                </a>
                                
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <td><?php echo $Archive_event[$i]['Start_date']; ?></td>
                            <td><?php echo $Archive_event[$i]['End_date']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
               </table>
            </div>
        </div>
        <div class="tab-pane fade active in" id="latest_events">
            <?php if($Event[0]['Id'] !='' && $this->data['user']->Role_name == 'Client') { ?>

            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
            <span class="label btn-blue pull-right margin-left-10 tooltips" onclick="check_edit_event()" data-placement="top" data-original-title="Edit Event"><i class="fa fa-edit"></i></span>
            <!--<span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>-->

            <?php  } ?>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                    <thead>
                        <tr>
                            <?php if($this->data['user']->Role_name == "Client") {  ?>
                            <th></th>
                            <?php } ?>
                            <th>#</th>
                            <th>Name</th>
                            <?php if(!in_array($this->data['user']->Role_id, array(4,6,7))) { ?>
                            <th>Fundraising</th>
                            <?php } ?>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;$i<count($Event);$i++) { ?>
                        <tr>
                            <?php if($this->data['user']->Role_name == "Client") {  ?>
                            <td>
                                <div class="checkbox-table">
                                    <label>
                                        <input type="checkbox" name="event_name[]" value="<?php echo $Event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                    </label>
                                </div>
                            </td>
                            <?php } ?>
                            <td><?php echo $i+1; ?></td>
                            <td>
                                <?php 
                                if($this->data['user']->Role_id==7)
                                {
                                    $link=base_url().'speaker_Messages/index/'.$Event[$i]['Id'];
                                }
                                else
                                {
                                    $link=base_url().'event/edit/'.$Event[$i]['Id'];
                                }
                            ?>
                            <a href="<?php echo base_url(); ?>event/edit/<?php echo $Event[$i]['Id']; ?>"><?php echo $Event[$i]['Event_name']; ?></a><span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo $link; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>">View</a> <button style="border-radius: 0px;margin-left: 3px;margin-top: -2px;padding: 4px 16px 4px 14px;" data-target=".bs-example-modal-sm<?php echo $Event[$i]['Id']; ?>" data-toggle="modal" class="btn btn-green">Share</button></span></td>
                            <div class="modal fade bs-example-modal-sm<?php echo $Event[$i]['Id']; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 id="myLargeModalLabel" style="text-align:center;" class="modal-title">Social Sharing</h4>
                                        </div>
                                        <div class="modal-body" style="text-align:center;">
                                            <div id="share-buttons">
                                                

                                                <!-- Facebook -->
                                                <a class="fb-bg" href="https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=<?php echo base_url() ?>../app/<?php echo $acc_name.'/'.$Event[$i]['Subdomain']; ?>&display=popup&ref=plugin&src=share_button" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/facebook.png" alt="Facebook" />
                                                </a>


                                                <!-- Twitter -->
                                                <a href="https://twitter.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>&amp;hashtags=Support <?php echo ucfirst($acc_name); ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/twitter.png" alt="Twitter" />
                                                </a>


                                                <!-- Email -->
                                                <a href="mailto:?Subject=Allintheloop <?php echo ucfirst($acc_name); ?>&amp;Body=Allintheloop <?php echo ucfirst($acc_name); ?> <?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/email.png" alt="Email" />
                                                </a>
                                             
                                            
                                                <a href="https://plus.google.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/google.png" alt="Google" />
                                                </a> 
                                                
                                                <!-- LinkedIn -->
                                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/linkedin.png" alt="LinkedIn" />
                                                </a>
                                
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if(!in_array($this->data['user']->Role_id, array(4,6,7))) { ?>
                            <td>
                                <?php 
                                $fundraising_val = explode(',', $Event[$i]['Checkbox_values']);
                                if(in_array(20, $fundraising_val))
                                {
                                    //echo '<a href="'.base_url().'fundraising/'.$Event[$i]['Subdomain'].'/panel/?k='.$output.'" target="_blank" >Fundraising Panel</a>';
                                    echo '<a href="'.base_url().'fundraising/'.$Event[$i]['Subdomain'].'/panel" target="_blank" >Fundraising Panel</a>';
                                }
                                ?>
                            </td>
                            <?php } ?>
                            <td><?php echo $Event[$i]['Start_date']; ?></td>
                            <td><?php echo $Event[$i]['End_date']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="feature_events">
            <?php  if($Feature_event[0]['Id'] !='' && $this->data['user']->Role_name == 'Client') { ?>

            <span id="delete_selection" name="delete_selection" class="label btn-red pull-right margin-left-10 tooltips" onClick="check_delete_events()" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></span>
            <span class="label btn-blue pull-right margin-left-10 tooltips" onclick="check_edit_event()" data-placement="top" data-original-title="Edit Event"><i class="fa fa-edit"></i></span>
            <!--<span class="label btn-green pull-right margin-left-10 tooltips" onclick="check_display_event()" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></span>-->

            <?php  } ?>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                    <thead>
                        <tr>
                          <?php if($this->data['user']->Role_name == "Client") {  ?>
                            <th></th>
                          <?php } ?>
                            <th>#</th>
                            <th>Name</th>
                            <?php if(!in_array($this->data['user']->Role_id, array(4,6,7))) { ?>
                            <th>Fundraising</th>
                            <?php } ?>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php for($i=0;$i<count($Feature_event);$i++) {  ?>
                        <tr>
                        <?php if($this->data['user']->Role_name == "Client") {  ?>
                            <td>
                                <div class="checkbox-table">
                                    <label>
                                        <input type="checkbox" name="event_name[]" value="<?php echo $Feature_event[$i]['Id']; ?>" id="toggleElement" class="flat-grey foocheck">
                                    </label>
                                </div>
                            </td>
                             <?php } ?>
                            <td><?php echo $i+1; ?></td>
                            <td>
                                <?php 
                                if($this->data['user']->Role_id==7)
                                {
                                    $link=base_url().'speaker_Messages/index/'.$Feature_event[$i]['Id'];
                                }
                                else
                                {
                                    $link=base_url().'event/edit/'.$Feature_event[$i]['Id'];
                                }
                            ?>
                            <a href="<?php echo base_url(); ?>event/edit/<?php echo $Feature_event[$i]['Id']; ?>"><?php echo $Feature_event[$i]['Event_name']; ?></a><span class="pull-right"> <a style="padding:8px 20px 8px 20px;" class="btn-yellow" href="<?php echo $link; ?>">Edit</a> <a style="padding:8px 20px 8px 20px;" class="btn-success" target="_blank" href="<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Feature_event[$i]['Subdomain']; ?>">View</a><button style="border-radius: 0px;margin-left: 3px;margin-top: -2px;padding: 4px 16px 4px 14px;" data-target=".bs-example-modal-sm<?php echo $Feature_event[$i]['Id']; ?>" data-toggle="modal" class="btn btn-green">Share</button></span></td>
                            
                            <div class="modal fade bs-example-modal-sm<?php echo $Feature_event[$i]['Id']; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                ×
                                            </button>
                                            <h4 id="myLargeModalLabel" style="text-align:center;" class="modal-title">Social Sharing</h4>
                                        </div>
                                        <div class="modal-body" style="text-align:center;">
                                            <div id="share-buttons">
                                                

                                                <!-- Facebook -->
                                                <a class="fb-bg" href="https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=<?php echo base_url() ?>../app/<?php echo $acc_name.'/'.$Feature_event[$i]['Subdomain']; ?>&display=popup&ref=plugin&src=share_button" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/facebook.png" alt="Facebook" />
                                                </a>


                                                <!-- Twitter -->
                                                <a href="https://twitter.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Feature_event[$i]['Subdomain']; ?>&amp;hashtags=Support <?php echo ucfirst($acc_name); ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/twitter.png" alt="Twitter" />
                                                </a>


                                                <!-- Email -->
                                                <a href="mailto:?Subject=Allintheloop <?php echo ucfirst($acc_name); ?>&amp;Body=Allintheloop <?php echo ucfirst($acc_name); ?> <?php echo base_url(); ?>app/<?php echo $acc_name."/".$Feature_event[$i]['Subdomain']; ?>">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/email.png" alt="Email" />
                                                </a>
                                             
                                            
                                                <a href="https://plus.google.com/share?url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/google.png" alt="Google" />
                                                </a> 
                                                
                                                <!-- LinkedIn -->
                                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(); ?>app/<?php echo $acc_name."/".$Feature_event[$i]['Subdomain']; ?>" target="_blank">
                                                    <img style="width: 17%;margin-right: 1%;" src="<?php echo base_url(); ?>assets/images/linkedin.png" alt="LinkedIn" />
                                                </a>
                                
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if(!in_array($this->data['user']->Role_id, array(4,6,7))) { ?>
                            <td>
                                <?php 
                                $fundraising_val = explode(',', $Feature_event[$i]['Checkbox_values']);
                                if(in_array(20, $fundraising_val))
                                {
                                    //echo '<a href="'.base_url().'fundraising/'.$Feature_event[$i]['Subdomain'].'/panel/?k='.$output.'" target="_blank" >Fundraising Panel</a>';
                                    echo '<a href="'.base_url().'fundraising/'.$Feature_event[$i]['Subdomain'].'/panel" target="_blank" >Fundraising Panel</a>';
                                }
                                ?>
                            </td>
                            <?php } ?>
                            <td><?php echo $Feature_event[$i]['Start_date']; ?></td>
                            <td><?php echo $Feature_event[$i]['End_date']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
</div>
</div>

<?php if ($this->data['user']->Role_name == 'Client') { ?>
<div class="col-lg-12">
    <div class="row space20">
        <div class="panel-scroll height-430 panel-white">
            <div class="panel panel-white">
                <div class="col-lg-10 panel-heading border-light">
                    <h4 class="panel-title">Event Allocation</h4>
                </div>
                <div class="col-lg-12">
                    <div class="col-md-8 col-lg-10 no-padding partition-white">
                        <div class="partition">
                            <div class="partition-body padding-15">
                                <div class="height-300">
                                    <div id="chart1" class='with-3d-shadow with-transitions'>
                                        <svg></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-2" style="background:#00b8c2;margin-top: -3%;">
                        <div class="partition-body padding-15">
                            <ul class="mini-stats">
                                <li class="col-md-12 col-sm-4 col-xs-4 no-padding">
                                    <div class="sparkline-bar">
                                        <span></span>
                                    </div>
                                    <div class="values">
                                        <strong><?php echo $num_total_event; ?></strong>
                                        Events
                                    </div>
                                </li>
                                <li class="col-md-12 col-sm-4 col-xs-4 no-padding">
                                    <div class="sparkline-bar">
                                        <span></span>
                                    </div>
                                    <div class="values">
                                        <strong><?php echo $num_total_public_event; ?></strong>
                                        Public Events
                                    </div>
                                </li>
                                <li class="col-md-12 col-sm-4 col-xs-4 no-padding">
                                    <div class="sparkline-bar">
                                        <span></span>
                                    </div>
                                    <div class="values">
                                        <strong><?php echo $num_total_private_event; ?></strong>
                                        Private Events
                                    </div>
                                </li>
                                <li class="col-md-12 col-sm-4 col-xs-4 no-padding">
                                    <div class="sparkline-bar">
                                        <span></span>
                                    </div>
                                    <div class="values">
                                        <strong><?php echo $num_total_active_event; ?></strong>
                                        Active Events
                                    </div>
                                </li>
                                <li class="col-md-12 col-sm-4 col-xs-4 no-padding">
                                    <div class="sparkline-bar">
                                        <span></span>
                                    </div>
                                    <div class="values">
                                        <strong><?php echo $num_total_inactive_event; ?></strong>
                                        Inactive Events
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="modal fade" id="myModal123" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
        <div class="form-group">
                   <label class="col-sm-12" style="padding-top: 1.2%;padding-left: 0px;" for="form-field-1"><h3 style="text-align: center;">Do You Have Stripe Account? </h3>
                   <p>
                   It looks like you are not setup to receive your funds</p><p>  That’s ok - We can take care of that for you no problem!  Just follow these steps:</p><p>  Firstly, do you already have a Stripe account you would like to connect to this app to receive your funds?</p>
                   </label>
                   <label onclick="show_btn('yes');" id="stripe_yes" class="radio-inline hover">
                        <input type="radio" class="purple" name="stripe_check" value="yes">
                        <span class="col-sm-3">Yes</span>
                   </label>
                   <label onclick="show_btn('no');" id="stripe_no" class="radio-inline hover">
                        <input type="radio" class="purple" name="stripe_check" value="yes">
                        <span class="col-sm-3">No</span>
                   </label>
                </div>
                <div id="result_msg1">
                    
                </div>
                <div class="form-group">
                    <?php
                     //$pageurl="https://".$_SERVER["SERVER_NAME"]."".$_SERVER["REQUEST_URI"];
                      //$this->session->set_userdata('pageurl',$pageurl);  
                     $authorize_request_body = array(
                            'response_type' => 'code',
                            'scope' => 'read_write',
                            'client_id' => $scret_key[0]['admin_client_id']
                    ); ?>
                    <a id="connect_stripe1" style="display:none;"  class="btn" href="https://connect.stripe.com/oauth/authorize?<?php echo http_build_query($authorize_request_body); ?>"><img src="<?php echo base_url().'fundraising/assets/images/strip_blue.png';?>"></a>
                    <button onclick="create_account();" style="width: 50%; display:none;"  class="btn btn-green btn-block" id="account_stripe1">Create Stripe Account</button>
                </div>
        </div>
        <div class="modal-footer">
         <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">Skip</button>
        </div>
      </div>
    </div>
  </div>
                 
<script src="https://code.jquery.com/jquery-1.8.1.min.js"></script>
<?php if($stripe_show[0]['stripe_show']=='1'){ if ($this->data['user']->Role_name == 'Client') { ?>
<?php if($scret_key[0]['secret_key']=="" && $scret_key[0]['public_key']=="" && $scret_key[0]['stripe_user_id']==""){ ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal123').attr('class','modal fade in');
        $('#myModal123').attr('style','display:block;');
    });
</script>
<?php } } } ?>

                 
<script src="https://code.jquery.com/jquery-1.8.1.min.js"></script>

<script type="text/javascript">
    $('#cancel').click(function(){
        $('#myModal123').attr('class','modal fade');
        $('#myModal123').attr('style','display:none;');
    });
    function show_btn(arge) {
        if(arge=="yes")
        {
            $('#account_stripe1').hide();
            $('#connect_stripe1').show();
        }
        else
        {
            $('#connect_stripe1').hide();
            $('#account_stripe1').show();
        }
    }
    function create_account()
    {
        $.ajax({
            url:"<?php echo base_url().'dashboard/create_account_in_stripe'; ?>",
            success:function(result) {
                var data=result.split("###");
                if(data[0]=="success")
                {
                    $('#result_msg1').addClass('alert alert-success');
                }
                else
                {
                    $('#result_msg1').addClass('alert alert-danger');
                }
                $('#result_msg1').html("<button class='close' data-dismiss='alert'>×</button>"+data[1]);
                $('#result_msg1').fadeOut(10000, "linear");
            }
        });
    }
    function edit_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>event/edit/" + id;
    }
    function display_event(id)
    {
        window.location.href = "<?php echo base_url(); ?>event/detail/" + id;
    }
    function check_edit_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            edit_event($('input[type=checkbox]:checked').val());
        }
    }
    function check_display_event()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else if($('input[type=checkbox]:checked').length > 1)
        { 
            alert('Please select only one checkbox');
        }
        else
        {
            display_event($('input[type=checkbox]:checked').val());
        }
    }
    
    function check_delete_events()
    {
        if($('input[type=checkbox]:checked').length == 0)
        {            
            alert('Please select atleast one checkbox');
        }
        else
        {
            if (confirm('Are you sure you want to delete this?')) 
            {
                event.preventDefault();

                $.each($('input[name="event_name[]"]:checked'), function() {  
                    $.ajax({
                      type: "POST",
                      url: '<?php echo base_url(); ?>event/delete',
                      data: 
                        { selected: $(this).val() },
                      success: function(data){              
                        setTimeout(function () {
                                        
                            window.location.href = window.location.href;
                         
                        }, 1000);
                        
                      },              
                    });           

                });
            }
        }
    }

</script>