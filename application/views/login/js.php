<script>
   function sendforgotmail()
        {
            if($("#forgotemail").val()!="")
            {           
                $.ajax({
                url : '<?php echo base_url(); ?>Login/forgot_pas',
                data :'email='+$("#forgotemail").val(),
                type: "POST",           
                success : function(data)
                {
                    var values=data.split('###');
                    if(values[0]=="error")
                    {   
                        $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
                        $('#forgotemail').parent().find('.control-label span').removeClass('valid');
                        $('#forgot_msg').html(values[1]);
                        $("#forgot_msg").removeClass('no-display');
                        $("#forgot_msg").removeClass('alert-success').addClass('alert-danger');
                        $("#forgot_msg").fadeIn();
                    }
                    else
                    {
                        $('#forgot_msg').html(values[1]);
                        $('#forgot_msg').fadeToggle();
                        $("#forgot_msg").removeClass('no-display').addClass('alert-success');
                        $("#forgot_msg").removeClass('alert-danger');
                        $("#forgot_msg").fadeOut(3000);
                    }
                    
                },
            });
        }
        else
        {
            $('#forgot_msg').show();
            $('#forforgotemail').show();
            $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
            $('#forgotemail').parent().parent().find('.control-label span').removeClass('ok').addClass('required');
            $('#forgotemail').parent().find('.help-block').removeClass('valid').html('this field is required');
        }
        }
</script>