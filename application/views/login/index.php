
<div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
    <div class="logo">
        <img src="<?php echo base_url(); ?>assets/images/logo.png">
    </div>
    <div class="box-login">
        <div class="box-login1">
            <div class="login-popup-header">
                <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                <p>
                    Enter your e-mail and password to login in your account.
                </p>
            </div>

            <form method="POST" class="form-login" action="<?php echo base_url();?>Login/check" autocomplete="off">
            <?php if($error != NULL){ ?>
            <div class="errorHandler alert alert-danger " style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $error; ?>
            </div>                  
            <?php } ?>
            <?php if($success != NULL){ ?>
            <div class="errorHandler alert alert-success" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $success; ?>
            </div>                  
            <?php } ?>
            <div class="errorHandler alert alert-danger no-display">
                <i class="fa fa-remove-sign"></i> There is an error in your submission.
            </div>
            <fieldset>
                <div class="form-group">
                    <span class="input-icon">
                        <input type="text" class="form-control" autocomplete="off" name="username" placeholder="Email" value="<?php echo $email;?>">
                        <i class="fa fa-user"></i> 
                    </span>
                </div>
                <div class="form-group form-actions">
                    <span class="input-icon">
                        <input type="password" class="form-control password" autocomplete="off" name="password" placeholder="Password">
                          <i class="fa fa-lock"></i>
                    </span>
                </div>
                <div class="form-actions">
                    <div class="forgetpassworddiv">
                        <div class="forgetpassworddiv1">
                            <a style="position: relative;" class="forgot" href="javascript:void(0);">
                                    I forgot my password
                                </a>
                            <a class="sitename">www.allintheloop.com</a>
                        </div>
                        <div class="forgetpassworddiv2">
                            <button type="submit" class="btn btn-green pull-right">
                                Login <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
            </form>

            <div class="copyright">
                <?php echo $reserved_right; ?>
            </div>      
        </div></div>
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <div class="box-forgot">
            <div class="login-popup-header">
                <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                <p>
                    Enter your e-mail address below to reset your password.
                </p>
            </div>

            <form method="POST" class="form-forgot" action="return false;">
                <div class="errorHandler alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> There is an error in your submission.
                </div>
                <div class="errorHandler alert alert-danger no-display" id="forgot_msg">
                        <i class="fa fa-remove-sign"></i> There is an error in your submission.
                </div>
                <fieldset>
                    <div class="form-group">
                        <span class="input-icon">
                            <input type="email" class="form-control required" name="email" placeholder="Email" id="forgotemail" autocomplete="off">
                            <span class="help-block" id="forforgotemail" style="display:none;" for="forgotemail">This field is required.</span>
                            <i class="fa fa-envelope"></i> </span>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-light-grey go-back">
                            <i class="fa fa-chevron-circle-left"></i> Log-In
                        </a>
                                                <button type="submit" class="btn btn-green pull-right" onclick="sendforgotmail(); return false;">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                </fieldset>
            </form>
            <div class="copyright">
                <?php echo $reserved_right; ?>
            </div>
        </div>
    </div>

<style>
body.login {
    background-image: url('<?php echo base_url(); ?>assets/images/bg_login.jpg');
    background-size: cover;
}
body.login .main-login {
    
    position: relative;
   
}
body.login .box-login {
    background: none;
    border-radius: 5px;
    box-shadow: none;
    overflow: hidden;
    padding: 15px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}
.box-login1 {
    background-color: #ffffff;
    padding: 15px;
    box-sizing: border-box;
    border-radius: 5px;
    float: left;
    width: 100%;
}

form.form-login {
    float: left;
    width: 100%;
}
.box-login1 h1 {
    width: 100%;
    float: left;
    text-align: center;
    margin: 23px 0 0 0;
    font-size: 43px;
    text-transform: uppercase;
    color: #1fb9c1;
    font-weight: lighter;
}
.box-login1 h1 span {
    color: #000000;
}
.box-login1 h3 {
    margin: 25px 0 25px 0;
    float: left;
    color: #909090;
}
.input-icon > input {
    padding-left: 25px;
    padding-right: 6px;
    height: 35px;
}
.input-icon > [class*="fa-"], .input-icon > [class*="clip-"] {
    top: 0;
}
body.login .form-actions {
    margin-top: 0;
    padding-top: 0;
    display: block;
    margin-bottom: 0;
}
input#password {
    margin-top: 2px;
}
.forgetpassworddiv {
    float: left;
    width: 100%;
    margin: 15px 0 0 0;
}

.forgetpassworddiv1 {
    float: left;
    width: 50%;
}
.forgetpassworddiv2 {
    float: right;
    width: 50%;
}
.forgetpassworddiv a {
    color: #909090;
    font-size: 19px;
    font-weight: normal;
    cursor: pointer;
}
a.sitename {
    margin: 11px 0 0 0;
    float: left;
}
.forgetpassworddiv2 button {
    background-color: #1fb9c1 !important;
    height: 50px;
    border: 0 !important;
    margin: 25px 0 0 0;
}
.box-login2 {
    background-color: #ccccce;
    float: left;
    width: 100%;
    margin: 37px 0 0 0;
    padding: 41px;
    box-sizing: border-box;
    border-radius: 5px;
    opacity: 0.8;
}
a.register {
    float: left;
    width: 46%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
a.visitguides {
    float: right;
    width: 47%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
body.login .main-login {
    margin-top: 30px;
    position: relative;
}

body.login .main-login {
    margin-top: 30px;
    position: relative;
    
}
@media only screen and (max-width:1024px){
body.login .main-login {
    
    width: 38% !important;
}
.box-login1 h1 {
    
    font-size: 26px;
   
}
.forgetpassworddiv1 {
    float: left;
    width: 55%;
}
.forgetpassworddiv2 {
    float: right;
    width: 45%;
}
a.register {
    
    font-size: 12px;
    
}
a.visitguides {
    
    font-size: 12px;
}
}
@media only screen and (max-width:980px){
body.login .main-login {
   
    width: 66.66666667% !important;
}
}

@media only screen and (max-width:767px){
body.login .main-login {
   
    width: 83.33333333% !important;
}
}
@media only screen and (max-width:640px){
.box-login1 h3 {
   
    font-size: 17px;
}
}
@media only screen and (max-width:480px){
.input-icon > input {
  
    width: 100%;
}
}
@media only screen and (max-width:360px){
.forgetpassworddiv1 {
    float: left;
    width: 100%;
}
.forgetpassworddiv2 {
    float: right;
    width: 100%;
}
.forgetpassworddiv2 button {
   
    width: 100%;
}
a.register {
    font-size: 15px;
    width: 100%;
}
a.visitguides {
    font-size: 15px;
    width: 100%;
    margin: 15px 0 0 0;
}
}
</style>