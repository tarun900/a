<div class="main-set-password col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
    <div class="logo">
        <img src="<?php echo base_url(); ?>assets/images/logo.png">
    </div>
    <div class="box-set-password">
        <div class="box-set-password1">
            <div class="login-popup-header">
                <img alt="" src="<?php echo base_url(); ?>assets/images/all-in-loop-logo.png">
                <p>
                    Exhibitor Portal
                </p>
            </div>
            <form id="set_passowrd_form" method="POST" class="form-set-password" action="<?php echo base_url();?>Login/set_exhibitor_password">
                <fieldset>
                    <input type="hidden" name="exhibitor_email" id="exhibitor_email" value="<?php echo $email; ?>">
                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                    <input type="hidden" name="stand_nubmer" id="stand_nubmer" value="<?php echo $stand_nubmer; ?>">
                    <div class="form-group form-actions">
                        <span class="input-icon">
                            <input type="password" class="form-control password required" name="new_password" id="new_password" placeholder="Password">
                              <i class="fa fa-lock"></i>
                        </span>
                    </div>
                    <div class="form-group form-actions">
                        <span class="input-icon">
                            <input type="password" class="form-control password required" name="new_confirm_password" placeholder="Confirm Password">
                              <i class="fa fa-lock"></i>
                        </span>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success btn-block">
                                Login <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div class="copyright">
                <?php echo $reserved_right; ?>
            </div>      
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$("#set_passowrd_form").validate({
    errorElement: "label",
    errorClass: 'help-block',  
    rules: {
      new_password:{
        minlength: 6,
      },
      new_confirm_password:{
        minlength: 6,
        equalTo: "#new_password"
      }
    },
    highlight: function (element) 
    {
      $(element).closest('.help-block').removeClass('valid');
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    }
});
</script>
<style>
body.login {
    background-image: url('<?php echo base_url(); ?>assets/images/bg_login.jpg');
    background-size: cover;
}
body.login .main-set-password {
    position: relative; 
}
body.login .box-set-password {
    background: none;
    border-radius: 5px;
    box-shadow: none;
    overflow: hidden;
    padding: 15px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}
.box-set-password1 {
    background-color: #ffffff;
    padding: 15px;
    box-sizing: border-box;
    border-radius: 5px;
    float: left;
    width: 100%;
}

form.form-set-password {
    float: left;
    width: 100%;
}
.box-set-password1 h1 {
    width: 100%;
    float: left;
    text-align: center;
    margin: 23px 0 0 0;
    font-size: 43px;
    text-transform: uppercase;
    color: #1fb9c1;
    font-weight: lighter;
}
.box-set-password1 h1 span {
    color: #000000;
}
.box-set-password1 h3 {
    margin: 25px 0 25px 0;
    float: left;
    color: #909090;
}
.input-icon > input {
    padding-left: 25px;
    padding-right: 6px;
    height: 35px;
}
.input-icon > [class*="fa-"], .input-icon > [class*="clip-"] {
    top: 0;
}
body.login .form-actions {
    margin-top: 0;
    padding-top: 0;
    display: block;
    margin-bottom: 0;
}
input#password {
    margin-top: 10px;
}

a.sitename {
    margin: 11px 0 0 0;
    float: left;
}
.forgetpassworddiv2 button {
    background-color: #1fb9c1 !important;
    height: 50px;
    border: 0 !important;
    margin: 25px 0 0 0;
}
.box-login2 {
    background-color: #ccccce;
    float: left;
    width: 100%;
    margin: 37px 0 0 0;
    padding: 41px;
    box-sizing: border-box;
    border-radius: 5px;
    opacity: 0.8;
}
a.register {
    float: left;
    width: 46%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
a.visitguides {
    float: right;
    width: 47%;
    background-color: #1fb9c1;
    padding: 10px;
    box-sizing: border-box;
    color: #ffffff;
    text-align: center;
    font-size: 16px;
    border-radius: 5px;
}
body.login .main-set-password {
    margin-top: 30px;
    position: relative;
}

body.login .main-set-password {
    margin-top: 30px;
    position: relative;
    
}
/*8-12-16*/
.login-popup-header p{margin:5px 0px 10px;}
.form-set-password .form-group.form-actions {
  margin-bottom: 10px;
}
button.btn.btn-success.btn-block{  display: block;
    margin: 10px auto 15px;
    text-align: center;
    width: 150px;
	background: #2db9c0 none repeat scroll 0 0;
	height:40px;border-radius:4px;border:1px solid #2db9c0;}
.form-actions input[type="text"]:focus, .form-actions input[type="password"]:focus {
  background-color: #fafec0 !important;
  border-color: #F7F7F7 !important;
  box-shadow: none !important;
}
.form-actions input[type="text"], .form-actions input[type="password"]{
  background-color: #fff;
  border: 1px solid #F7F7F7;
}
body.login .form-actions input.password {
  padding-right: 0;
}
.login .btn:focus, .login .btn:active:focus, .login .btn.active:focus {
  outline: medium none;
}

@media only screen and (max-width:1024px){
body.login .main-set-password {
    
    width: 38% !important;
}
.box-set-password1 h1 {
    
    font-size: 26px;
   
}

a.register {
    
    font-size: 12px;
    
}
a.visitguides {
    
    font-size: 12px;
}
}
@media only screen and (max-width:980px){
body.login .main-set-password {
   
    width: 66.66666667% !important;
}
body.login .form-control{
  width: 100%;
}
}

@media only screen and (max-width:767px){
body.login .main-set-password {
   
    width: 83.33333333% !important;
}
}
@media only screen and (max-width:640px){
.box-set-password1 h3 {
   
    font-size: 17px;
}
body.login .form-control{
  width: 100%;
}
}
@media only screen and (max-width:480px){
.input-icon > input {
  
    width: 100%;
}
}
@media only screen and (max-width:360px){

a.register {
    font-size: 15px;
    width: 100%;
}
a.visitguides {
    font-size: 15px;
    width: 100%;
    margin: 15px 0 0 0;
}
}
</style>