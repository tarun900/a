<div style="padding: 10px;" id="div<?php echo $value['Id']; ?>">
	<div class="row" style="margin-bottom: 20px;">                        
        <div class="col-md-1 col-lg-1 col-sm-1">
            <?php if(!empty($value['Logo'])) { ?>
    		<img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value['Logo'];?>" alt="" />
    		<?php } else { ?>
    		<img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />
    		<?php } ?>
        </div>
        <div class="col-md-11 col-lg-11 col-sm-11">
            <div class="desc">
            	<h4><?php echo $value["Firstname"]; ?></h4><br>
            <?php echo $value["Email"]; ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <button class="btn btn-danger" type="button" id="user<?php echo $value['Id']; ?>">Send them a Message</button>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="social-icons">
                <ul class="navbar-right">
                    <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="bottom">
                        <a target="_blank" href="#">
                            Visit my Website
                        </a>
                    </li>
                    <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                        <a target="_blank" href="https://www.twitter.com">
                            Follow me on Twitter
                        </a>
                    </li>
                    <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                        <a target="_blank" href="https://facebook.com">
                            Follow me on Facebook
                        </a>
                    </li>
                    <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                        <a target="_blank" href="https://linkedin.com">
                            Follow me on LinkedIn
                        </a>
                    </li>	
                </ul>
            </div>
        </div>                                
    </div>
    <div>
    	<?php echo html_entity_decode($value['Speaker_desc']); ?>
    </div>
</div>