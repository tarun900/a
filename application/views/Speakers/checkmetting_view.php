<?php $user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
$format="H:i";
if($time_format[0]['format_time']=='0')
{
 $format="h:i A";
}
else
{     
 $format="H:i";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/checkin_view.css?<?php echo time(); ?>">
<div class="col-sm-12 speakers-content">
    <div>
        <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
    </div>
	<div class="panel panel-white panel-white-new">
		<div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
			<div class="speakers-left-bot user_container_data">
				<?php if(!empty($attendees_metting)): ?>
                <div class="ps-container ps-container-new">
                	<ul class="activities columns columns-new">
                	<?php foreach ($attendees_metting as $key => $value) { ?>
                		<li>
                			<a class="activity" id="a<?php echo $value['metting_id']; ?>">
                                <div class="desc">
                                    <h4 class="user_container_name"><?php echo ucwords($value['name'])." Meeting With ".ucwords($value['recipient_name']);?></h4>
                                    <span><?php if($event_templates[0]['date_format']=='1')
                                                {
                                                    echo date($format." -  m/d/Y",strtotime($value['date'].' '.$value['time']));
                                                }
                                                else
                                                {
                                                    echo date($format." -  d/m/Y",strtotime($value['date'].' '.$value['time']));
                                                } ?></span>
                                    <p id="show_comments_tag" <?php if($value['status']=='0'){ ?> style="display: none;" <?php } ?>><?php echo $value['location']; ?></p>
                                </div>
                                <?php if($value['status']=='0'){ ?>
                                <div id="pending_status_btn_div_<?php echo $value['metting_id'] ?>" class="col-md-6 pull-right" style="margin-top: 15px;">
                                    <div class="col-md-3">
                                    	<button class="btn btn-success btn-block" onclick='changemettingstatus("<?php echo $value['metting_id']; ?>","1");'>Accept</button>
                                    </div>
                                    <div class="col-md-3">
                                    	<button class="btn btn-red btn-block" onclick='changemettingstatus("<?php echo $value['metting_id']; ?>","2");'>Reject</button>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-green btn-block" onclick='open_comment_popup("<?php echo $value['metting_id']; ?>");' data-toggle="modal" data-target="#save_comment_popup">Comments</button>
                                    </div>
                                   <!--  <div class="col-md-5" style="padding-left:0px !important;padding-right:5px !important;">
                                        <button class="btn btn-green btn-block" onclick='suggest_new_time("<?php echo $value['metting_id']; ?>");' data-toggle="modal" data-target="#request_meeting_popup">Suggest New Time</button>
                                    </div> -->
                                </div>
                                <?php } ?>
                                <div id="reject_meeting_status_<?php echo $value['metting_id'] ?>" class="col-sm-4 pull-right" <?php if($value['status']=='2'){ ?> style="margin-top:15px;" <?php }else{ ?> style="margin-top:15px;display: none;" <?php } ?>>
                                    <div class="col-sm-6">
                                        <button class="btn btn-red btn-block" disabled="disabled">Rejected</button>
                                    </div>
                                </div>
                                <div id="accept_meeting_status_<?php echo $value['metting_id'] ?>" class="col-sm-4 pull-right" <?php if($value['status']=='1'){ ?> style="margin-top:15px;" <?php }else{ ?> style="margin-top:15px;display: none;" <?php } ?>>
                                    <div class="col-sm-6">
                                        <button class="btn btn-success btn-block" disabled="disabled">Accepted</button>
                                    </div>
                                </div>
                            </a>
                		</li>
                	<?php } ?>	
                	</ul>
                </div>
				<?php  else: ?>
					<div class="tab-content">
                        <span>No Meetings have been booked.</span>
                    </div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div id="request_meeting_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span>Suggested Times</span>
            </div>
            <?php  
            function getDatesFromRange($start, $end, $format = 'Y-m-d') 
            {
                $array = array();
                $interval = new DateInterval('P1D');

                $realEnd = new DateTime($end);
                $realEnd->add($interval);
                
                $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
                foreach($period as $date) 
                { 
                    $array[] = $date->format($format); 
                }
                return $array;
            }
            if($event_templates[0]['Id']=='479')
            {
                $dates = getDatesFromRange(date('Y-m-d',strtotime('2017-11-07')),date('Y-m-d',strtotime('2017-11-08')));
            }
            else
            {
                $dates = getDatesFromRange($event_templates[0]['Start_date'],$event_templates[0]['End_date']); 
            }
            ?>
            <div class="modal-body">
                <form id="Suggested_user_validation_form" action="<?php echo base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/suggest_new_datetime'; ?>" method="post">
                    <div class="row" id="form_date_time_Suggested_form">
                        <div class="form-group"> 
                            <input type="hidden" value="" name="meeting_id_textbox" id="meeting_id_textbox">
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down1" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down1">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {   
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date($format,$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down2" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down2">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {   
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date($format,$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="control-label col-sm-2">Date:</label>
                            <div class="col-sm-10"> 
                                <select name="date[]" id="date_drop_down3" class="form-control">
                                    <?php foreach($dates as $key => $value) { ?>
                                       <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Time:</label>
                            <div class="col-sm-10">
                                <select name="time[]" class="form-control" id="time_drop_down3">
                                  <?php 
                                if($event_templates[0]['Id']=='479')
                                {
                                    $range=range(strtotime(date('10:40')),strtotime("19:10"),15*60);
                                }
                                else
                                {  
                                    $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                                }
                                    foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                                    <option value="<?php echo date("H:i",$time); ?>"><?php echo date($format,$time); ?></option>
                                  <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Locations:</label>
                            <div class="col-sm-10">
                                <select class="form-control required" id="location" name="location">
                                    <option value="">Select Location</option>
                                    <?php foreach ($meeting_locations as $key => $value) { ?>
                                    <option value="<?php echo $value['location']; ?>"><?php echo $value['location']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" id="send_new_suggested_times" class="btn btn-green btn-block">Send new Suggested Times</button>
                            </div>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="save_comment_popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span>Locations</span>
            </div>
            <div class="modal-body">
                <form id="comment_meeting_form" action="<?php echo base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/save_request_comments'; ?>" method="post">
                    <div class="row">
                        <input type="hidden" id="commment_meeting_id_textbox" name="meeting_id" value="">
                        <div class="form-group">
                            <label class="control-label col-sm-12">Location:</label>
                            <div class="col-sm-12">
                                <select class="form-control required" id="comment" name="comment">
                                    <option value="">Select Location</option>
                                    <?php foreach ($meeting_locations as $key => $value) { ?>
                                    <option value="<?php echo $value['location']; ?>"><?php echo $value['location']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" id="send_new_comment_btn" class="btn btn-green btn-block">Save</button>
                            </div>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery(".user_container_data li").each(function(index) 
    {
        $(this).css('display','inline-block');
        $(this).parent().css('display','block');
    });
});
jQuery("#search_user_content").keyup(function()
{    
    jQuery(".user_container_data li").each(function( index ) 
    {
        var str=jQuery(this).find('.user_container_name').html();
        if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
        {    
            var str1=jQuery(this).find('.user_container_name').html();
            if(str1!=undefined)
            {
                var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                var content=jQuery("#search_user_content").val().toLowerCase();
             	if(content!=null)
            	{          
                    jQuery(this).parent().parent().parent().find(".date_separator").each(function(index){
                        var content1=jQuery(this).text().toLowerCase();
                        var n1 = str1.indexOf(content1);
                    });          
                    var n = str1.indexOf(content);          
                    if(n!="-1")
                    {
                        jQuery(this).css('display','inline-block');
                        jQuery(this).parent().css('display','block');
                    }
                    else
                    {
                    	jQuery(this).css('display','none');
                    	jQuery(this).parent().css('display','none');
                    }
                }
            }
        }
    });  
});
function changemettingstatus(mid,status)
{
    $.ajax({
        url:"<?php echo base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/changemettingstatus/'; ?>"+mid,
        type:'post',
        data:'status='+status,
        success:function(data){
            var result=data.split('###');
            if($.trim(result[0])=="Success")
            {
                $('#show_comments_tag').show();
                if(status=='1')
                {
                    $('#pending_status_btn_div_'+mid).remove();
                    $('#accept_meeting_status_'+mid).show();
                }
                else
                {
                    $('#pending_status_btn_div_'+mid).remove();
                    $('#reject_meeting_status_'+mid).show();
                }
            }
        },
    });
}
function suggest_new_time(mid)
{
    $('#meeting_id_textbox').val(mid);
}
function open_comment_popup(mid)
{
    $('#commment_meeting_id_textbox').val(mid);   
}
</script>
