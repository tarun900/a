<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Speakers extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Speaker';
          $this->data['smalltitle'] = 'Speaker';
          $this->data['breadcrumb'] = 'Speaker';
          parent::__construct($this->data);
          $this->template->set_template('front_template');
          $this->load->model('event_template_model');
          $this->load->model('cms_model');
          $this->load->model('event_model');
          $this->load->model('setting_model');
          $this->load->model('speaker_model');
          $this->load->model('profile_model');
          $this->load->model('message_model');
     }

     public function index($Subdomain = NULL)
     {

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          $event_templates = $this->event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $notes_list = $this->event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $cmsmenu = $this->cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $speakers = $this->event_template_model->get_speaker_list($Subdomain);
          $this->data['speakers'] = $speakers;

          $advertisement_images = $this->event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;

          if ($this->input->post())
          {
               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $this->uri->segment(3),
                       'Time' => date("H:i:s"),
                       'ispublic' => $this->input->post('ispublic')
               );
               $this->message_model->send_speaker_message($data1);

               $Sid = $this->uri->segment(3);
               $view_chats1 = $this->message_model->view_hangouts($Subdomain, $Sid);

               $string = '';
               $string.='<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with ' . $view_chats1[0]['Recivername'] . '<br/></span>';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               foreach ($view_chats1 as $key => $value)
               {
                    if ($value['Sender_id'] == $lid)
                    {
                         echo '<strong>' . $value['Recivername'] . '</strong>';
                         echo '<br/>';
                         echo $value['Message'];
                         echo '<br/>';
                         echo $value['Time'];
                         echo '<br/>';
                         echo '<br/>';
                    }
                    else
                    {
                         echo '<strong>' . $value['Sendername'] . '</strong>';
                         echo '<br/>';
                         echo $value['Message'];
                         echo '<br/>';
                         echo $value['Time'];
                         echo '<br/>';
                         echo '<br/>';
                    }
               }

               echo $string;
               exit;
          }

          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $user = $this->session->userdata('current_user');
          if (empty($user))
          {
               $this->template->write_view('content', 'registration/index', $this->data, true);
          }
          else
          {
               $this->template->write_view('content', 'Speakers/index', $this->data, true);
          }

          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->write_view('footer', 'Speakers/footer', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          $this->template->render();
     }

     public function View($Subdomain = NULL, $id = null)
     {

          $event_templates = $this->event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          $this->data['subdomain'] = $Subdomain;
          $this->data['sp_id'] = $id;
          
          $user = $this->session->userdata('current_user');
          if (empty($user))
          {
               $this->template->write_view('content', 'registration/index', $this->data, true);
          }
          else
          {
               $view_chats1 = $this->message_model->view_hangouts($Subdomain, $id);
               $this->data['view_chats1'] = $view_chats1;

               $cmsmenu = $this->cms_model->get_cms_page($event_templates[0]['Id']);
               $this->data['cms_menu'] = $cmsmenu;

               $test = $this->event_template_model->get_speaker_social_url($Subdomain, $id);

               if (!empty($test))
               {
                    $social_url = $this->event_template_model->get_speaker_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
               }
               else
               {
                    $user_url = $this->event_template_model->get_speaker_user_url($Subdomain, $id);
                    $this->data['user_url'] = $user_url;
               }

               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);

          
               $this->template->write_view('content', 'Speakers/user', $this->data, true);
          }
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          $this->template->write_view('js', 'Speakers/js', $this->data, true);
          $this->template->render();
     }

     public function upload_imag($Subdomain = NULL, $id = null)
     {
          $event_templates = $this->event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          
//          echo "<pre>";
//          print_r($this->input->post());
          //exit;
          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {
               define ("MAX_SIZE","200000"); 
               $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
               //$datafiles=array();
               $uploaddir = "assets/user_files/"; //a directory inside
               foreach ($_FILES['photos']['name'] as $name => $value)
               {

                   $filename = stripslashes($_FILES['photos']['name'][$name]);
                   $size = filesize($_FILES['photos']['tmp_name'][$name]);
                   //get the extension of the file in a lower case format
                   $ext = $this->getExtension($filename);
                   $ext = strtolower($ext);
                   
                    if (in_array($ext, $valid_formats))
                    {
                         if ($size < (MAX_SIZE * 1024))
                         {
                              $image_name = time() . $filename;
                              echo "<img src='" .base_url().$uploaddir . $image_name . "' class='imgList'>";
                              $newname = $uploaddir . $image_name;

                              if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
                              {
                                   $time = time();
                                   $datafiles[]=$image_name;
                              }
                              else
                              {
                                   echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                              }
                         }
                         else
                         {
                              echo '<span class="imgList">You have exceeded the size limit!</span>';
                         }
                    }
                    else
                    {
                         echo '<span class="imgList">Unknown extension!</span>';
                    }
               }
               $string="";
               $string=json_encode($datafiles);
               echo '<input type="hidden" value="'.$string.'" name="imgvals" >';
          }
          //exit;
          
     }

     function getExtension($str)
     {
          $i = strrpos($str, ".");
          if (!$i)
          {
               return "";
          }
          $l = strlen($str) - $i;
          $ext = substr($str, $i + 1, $l);
          return $ext;
     }

}
