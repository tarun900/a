<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.wallform.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/table-data.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function() 
     {
           jQuery('#photoimg').die('click').live('change', function() 
           {
               jQuery("#imageform").ajaxForm({target: '#preview',
                    beforeSubmit: function() 
                    {
                         jQuery("#imageloadstatus").show();
                         jQuery("#imageloadbutton").hide();
                         jQuery(".addmore_photo").css("display","block");
                    },
                    success: function() 
                    {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();
                         jQuery(".addmore_photo").css("display","block");
                         jQuery(".photo_remove_btn").click(function()
                         {
                              jQuery(".addmore_photo").css("display","none");
                              jQuery(this).parent().parent().remove();
                              jQuery("#preview li").each(function(){
                                   jQuery(".addmore_photo").css("display","block");
                              });
                         });
                    },
                    error: function() 
                    {
                         jQuery("#imageloadstatus").hide();
                         jQuery("#imageloadbutton").show();
                         jQuery(".addmore_photo").css("display","none");
                    }}).submit();


          });
	});
</script>