<?php
$user = $this->session->userdata('current_user');
$acc_name=$this->session->userdata('acc_name');
?>

<div class="agenda_content agenda-user-content">
     <div class="row row-box"> 
          <div class="user_msg_profile clearfix box-effect">
               <?php
               $logo_images_array = json_decode($exibitors_data[0]['company_logo']); 
               if (!empty($logo_images_array))
               {
                    ?>
                    <img align="left" style="height:82px;width:82px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $logo_images_array[0]; ?>" alt="" />
                    <?php
               }
               else
               {
               ?>
			    <?php $color = sprintf("#%06x",rand(0,16777215)); ?>
				<span style="margin-top:5px;margin-bottom:5px;text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($exibitors_data[0]['Heading'], 0, 1)); ?></span>
               <!--<img align="left" src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
               <?php } ?>
               <div class="desc">
                    <h4><?php echo $user_url[0]["Firstname"] . ' ' . $user_url[0]["Lastname"]; ?></h4><br>
                         <?php echo $user_url[0]["Company_name"]; ?>
               </div>
               <div class="user_button">
                    <?php if($user[0]->Rid=='4' && $event_templates[0]['hide_request_meeting']=='1'){ ?>
                    <button id="more_info_user" class="btn btn-green user_btn" data-toggle="modal" data-target="#request_meeting_popup">Request Meeting</button>
                    <?php } if($event_templates[0]['allow_msg_user_to_exhibitor']=='1'){ ?>
                    <button class="btn btn-green user_btn" id="btn_askquestion" type="button">Send Message</button>
                    <?php } ?>
                    <!-- <button class="btn btn-green user_btn" id="btn_biography" type="button">View Biography</button> -->
               </div>     
          </div>

           <div id="request_meeting_popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <span>Request a meeting with <br/> <?php echo ucfirst($exibitors_data[0]['Heading']); ?></span>
          </div>
            <?php  
              function getDatesFromRange($start, $end, $format = 'Y-m-d') 
              {
                $array = array();
                $interval = new DateInterval('P1D');

                $realEnd = new DateTime($end);
                $realEnd->add($interval);
                
                $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
                foreach($period as $date) 
                { 
                  $array[] = $date->format($format); 
                }
                return $array;
              }
              $dates = getDatesFromRange($event_templates[0]['Start_date'],$event_templates[0]['End_date']); 
            ?>
          <div class="modal-body">
            <form id="metting_user_validation_form" action="" method="post">
            <div class="row" id="form_date_time_request_meeeting_form">
                <div class="form-group"> 
                  <label class="control-label col-sm-2">Date:</label>
                  <div class="col-sm-10"> 
                    <select name="date" id="date_drop_down" class="form-control">
                      <?php foreach($dates as $key => $value) { ?>
                         <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                      <?php } ?>
                    </select>
                   </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2">Time:</label>
                  <div class="col-sm-10">
                    <select name="time" class="form-control" id="time_drop_down">
                      <?php $range=range(strtotime(date('00:00')),strtotime("24:00"),15*60);
                        foreach($range as $time){ if(date("H:i",$time)!="00:00"){ ?>
                        <option value="<?php echo date("H:i",$time); ?>"><?php echo date("H:i",$time); ?></option>
                      <?php } } ?>
                    </select>
                   </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="button" id="request_meeting_btn_form_submitting" class="btn btn-green btn-block">Request Meeting</button>
                  </div>
                </div>    
            </div>
            <div class="row" id="form_date_time_request_meeeting_form_submittng_msg" style="display: none;">
                <div class="row">
                  <h5 id="msg_span_text_show" style="text-align: center;"></h5>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                      <button type="button" id="yes_meeting_btn_click" class="btn btn-green btn-block">Yes</button>
                    </div>
                    <div class="col-sm-6">
                      <button type="button" id="no_metting_btn_click" class="btn btn-green btn-block">No</button>
                    </div>
                </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>


          <div class="view_biography user_msg_profile clearfix box-effect" id="view_biography" style="display: none;">
               <h4>Biography</h4><br>
               <?php echo $user_url[0]["Speaker_desc"]; ?>
               <?php if (!empty($social_url)){ ?>
                    <div class="row">
                         <div class="col-md-6 col-lg-6 col-sm-6">
                              <div class="social-icons">
                                   <ul class="navbar-right">
                                        <?php if (!empty($social_url[0]["Website_url"])){ ?>
                                        <li class="social-dribbble tooltips" data-original-title="Visit my Website" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Website_url"]; ?>">
                                                  Visit my Website
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Twitter_url"])){ ?>
                                        <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Twitter_url"]; ?>">
                                                  Follow me on Twitter
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Facebook_url"])){ ?>
                                        <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Facebook_url"]; ?>">
                                                  Follow me on Facebook
                                             </a>
                                        </li>
                                        <?php } if (!empty($social_url[0]["Linkedin_url"])){ ?>
                                        <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="top">
                                             <a target="_blank" href="<?php echo $social_url[0]["Linkedin_url"]; ?>">
                                                  Follow me on LinkedIn
                                             </a>
                                        </li> 
                                        <?php } ?>  
                                   </ul>
                              </div>
                         </div>                                
                    </div> 
               <?php } ?>  
          </div>
            <div class="agenda_content">
    <div class="panel panel-white box-effect">
        <div class="demo1 col-md-12 col-lg-12 col-sm-12">
            <div class="tabbable-bot-inner">
                <?php foreach ($exibitors_data as $key => $value) { ?>
                  <?php } ?>
          <div class="row">
          <div class="col-md-12">
            <div class="page-title-heading">
              <h2><?php echo $exibitors_data[0]['Heading']; ?></h2>
            </div>
            <div class="tabbable-bot-inner-banner">
              <div id="slider-box" class="sponsor_slider carousel slide" data-ride="carousel">
                                <?php  $images_array = json_decode($value['Images']);  
                                        if(!empty($images_array)) { ?>
                <ol class="carousel-indicators">
                                    <li data-target="#slider-box" data-slide-to="0" class="active"></li>
                                    <?php for($j=1; $j<count($images_array); $j++) { ?>
                  <li data-target="#slider-box" data-slide-to="<?php echo $j; ?>"></li>
                                    <?php } ?>
                </ol>
                                <?php } ?>

                                    <?php  $images_array = json_decode($value['Images']);  
                                        if(!empty($images_array)) { ?>
                                       <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[0]; ?>">
                                            </div>
                                            <?php for($i=1; $i<count($images_array); $i++) { ?>
                                                <div class="item">
                                                    <img src="<?php echo base_url(); ?>assets/user_files/<?php echo $images_array[$i]; ?>">
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                </div>
            <div class="address-tabbable-bot">
              <p></p>
            </div>
            <?php if(!empty($exibitors_data)) { ?>
              <div class="social-net-blocks">
                <ul>
                  <?php if (!empty($exibitors_data[0]['facebook_url'])){ ?>
                  <li><a href="<?php echo $exibitors_data[0]['facebook_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/fb-social-icon.png" alt="facebook"></a></li>
                  <?php } if (!empty($exibitors_data[0]['twitter_url'])){ ?>
                  <li><a href="<?php echo $exibitors_data[0]['twitter_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/twtr-social-icon.png" alt="twitter"></a></li>
                  <?php } if (!empty($exibitors_data[0]['linkedin_url'])){ ?>
                  <li><a href="<?php echo $exibitors_data[0]['linkedin_url']; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/linkedin-social-icon.png" alt="linedin"></a></li>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>

            <div class="desc-blocks">
              <?php 
                  $user = $this->session->userdata('current_user');
                  if($user[0]->Role_id==4)
                  {
                    $extra=json_decode($custom[0]['extra_column'],true);
                    foreach ($extra as $key => $value) {
                      $keyword="{".str_replace(' ', '', $key)."}";
                      if(stripos($exibitors_data[0]['Description'],$keyword) !== false)
                      { 
                       $exibitors_data[0]['Description']=str_ireplace($keyword, $value,$exibitors_data[0]['Description']);
                      }
                    }
                  }
                ?>
              <p><?php echo $exibitors_data[0]['Description']; ?></p>
            </div>

            <div class="contact-detail">
              <?php if($exibitors_data[0]['stand_number'] !='') { ?>
                <p>Stand Number : <a href="tel:<?php echo $exibitors_data[0]['stand_number']; ?>"><?php echo $exibitors_data[0]['stand_number']; ?></a></p>
              <?php } ?>
              <?php if($exibitors_data[0]['website_url'] !='') { 
                if (!preg_match("~^(?:f|ht)tps?://~i", $exibitors_data[0]['website_url'])) {
                  $exibitors_data[0]['website_url'] = "https://" . $exibitors_data[0]['website_url'];
                }
                ?>
                <p>Website : <a target="_blank" href="<?php echo $exibitors_data[0]['website_url']; ?>"><?php echo $exibitors_data[0]['website_url']; ?></a></p>
              <?php } ?>
            </div>
            <?php if($exibitors_data[0]['Short_desc'] !='') { ?>
              <div class="short-desc-tabbable">
                <p>Keywords : <?php echo $exibitors_data[0]['Short_desc']; ?></p>
              </div>
            <?php } ?>
          </div>
          </div>
                </div>
            </div>
        </div>
    </div>
</div>
          <div class="" id="section_msg">     
              <?php if(!empty($user)): ?>
               <form id="imageform" style="display: none;" method="post" enctype="multipart/form-data" action='<?php echo base_url() ?>Speakers/<?php echo $acc_name."/".$Subdomain; ?>/upload_imag/<?php echo $Sid; ?>' style="clear:both">
                    <div class="facebok-container">
                         <div class="facebok-main">
                              <div class="facebok-head">
                                   <ul>
                                        <li class="facebok-head-status">
                                             <span>Status </span>
                                        </li>
                                        <li class="facebok-head-photo"> 
                                             <span> Add Photo </span>
                                             <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="Uploading...."/></div>
                                             <div id='imageloadbutton'>
                                                  <input type="file" name="photos[]" id="photoimg" capture accept="image/*" multiple="multiple" >
                                             </div>
                                        </li>
                                   </ul>
                              </div>
                              <div class="facebok-middle">
                                   <textarea class="col-md-12 col-lg-12 col-sm-12 facebok-textarea input-text" id="Message" placeholder="Type your question here..." name="Message"></textarea>
                              </div>
                              <div class="fb-upload-img-box">
                                   <ul id='preview'>
                                   </ul>
                                   <!--                                        <ul id="yourimage">
                                                                      </ul>-->
                                   <!--                                        <input type="file" capture="camera" accept="image/*" id="takePictureField">
                                   -->
                                   <div class="addmore_photo" style="display: none;" onclick="javascript: jQuery('#photoimg').click();">&nbsp;</div>
                              </div>
                              <div class="facebok-footer clearfix">
                                   <ul class="footer-left">
                                        <li class="camera-icon" id="camera_icon" onclick="javascript: jQuery('#photoimg').click();"></li>
                                   </ul>
                                   <ul class="footer-right">
                                     
                                   <input type="hidden" value="0" name="ispublic">
                                   	
                                   <li class="submit-button-box">
                                             <button class="submit-button" id="sendbtn" type="button"  onclick="sendmessage();">
                                                  Private message <i class="fa fa-arrow-circle-right"></i>
                                             </button>
                                    </li>
                                   </ul>
                              </div>
                         </div>    
                    </div>
               </form>     
               <?php
            
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               ?>
               <div class="col-md-12 col-lg-12 col-sm-12" id="preview_msg">

                    <?php
                    echo '<div>
             <div id="messages">';
                    //echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Question with ' . $user[0]->Firstname . '<br/></h3>';
                    foreach ($view_chats1 as $key => $value)
                    {
                         echo "<div class='message_container'>";

                         if ($value['Sender_id'] == $user[0]->Id)
                         {
                              echo "<div class='msg_edit-view-box'>";
                              echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                              echo "</div>";
                              echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                              echo "Delete";
                              echo "</div>";
                              echo "</div>";
                         }

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "")
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo $value['Sendername'];
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo $value['Recivername'];

                              echo '</a>';                                               
                              echo "</div>";
                         }
                         echo "</div>";

                         echo "<div class='msg_date'>";
                         echo timeAgo(strtotime($value['Time']));
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";


                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                              echo "</a>";
                              echo "</div>";
                         }

                         echo "<div class='toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                              <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$subdomain . "/upload_commentimag/" . $Sid . "'>
                                   <div class='comment_message_img'>";
                         if ($user[0]->Logo!="")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>
                         <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                   <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                   <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                   </ul>
                                   <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                   <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                                        
                              </form>
                              <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              $i = 0;
                              $flag = false;
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }

                                   echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                   if ($cval['Logo'] != "")
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                   }

                                   echo "</div>
                                   <div class='comment_wrapper'>        
                                   <div class='comment_username'>
                                        " . $cval['user_name'] . "
                                   </div>
                                   <div class='comment_text'>
                                        " . $cval['comment'] . "
                                   </div>
                                   <div class='comment_text' style='float:right;padding-left:13px;'>";
                                   ?>
                                   <?php timeAgo(strtotime($cval['Time'])); ?>
                                   <?php
                                   echo"</div>
                                   </div>";

                                   if ($cval['image'] != "")
                                   {
                                        $image_comment = json_decode($cval['image']);
                                        echo "<div class='msg_photo'>";
                                        echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                        echo "</a>";
                                        echo "</div>";
                                   }
                                   if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                   {
                                        echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                   }
                                   echo "</div>";


                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }

                                   $i++;
                              }
                         }
                         echo "</div>
                         </div>";
                         echo "</div>";
                    }
                    echo "</div></div>";
                    ?>                          
               </div>
               <?php
                    if (count($view_chats1) > 4)
                    {
                         ?>
                         <div id="loadmore" class="load_btn">
                              <a href="javascript: void(0)" onclick="loadmore(10,null);" class="loadmore panel-green"> 
                                   Load More
                              </a>
                         </div>
                         <?php
                    }
               ?>
              <?php else:?>
              <div class="errorHandler alert alert-info center" style="display: block;margin: 0 10%;">
                <i class="fa fa-remove-sign"></i> 
                 Login or Sign Up to proceed. To sign up or login tap the Sign Up button on the top right of the screen.
             </div>
              <?php endif;?>
          </div>
     </div>
</div>
<script type="text/javascript">
    $('#request_meeting_btn_form_submitting').click(function(){
      $.ajax({
        url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/request_meeting/'.$exibitors_data[0]['Id'] ?>",
        type:'post',
        data:"date="+$('#date_drop_down').val()+"&time="+$('#time_drop_down').val()+"&clash_check=1",
        success:function(data){
          var result=data.split('###');
          if($.trim(result[0])=="error")
          {
            $('#msg_span_text_show').html(result[1]);
            $('#form_date_time_request_meeeting_form').hide();
            $('#form_date_time_request_meeeting_form_submittng_msg').show();
          }
          else
          {
            $('#request_meeting_popup').modal('hide');
          }
        }
      });
  });
  $('#yes_meeting_btn_click').click(function(){
    var time;
    if($('#date_drop_down').val()=="<?php echo date('Y-m-d'); ?>")
    {
      time=$('#currnt_day_time').val();
    }
    else
    {
      time=$('#future_day_time').val()
    }
    $.ajax({
      url:"<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/request_meeting/'.$exibitors_data[0]['Id'] ?>",
      type:'post',
      data:"date="+$('#date_drop_down').val()+"&time="+time+"&clash_check=0",
      success:function(data){
        var result=data.split('###');
        if($.trim(result[0])=="error")
        {
          $('#msg_span_text_show').html(result[1]);
          $('#form_date_time_request_meeeting_form').hide();
          $('#form_date_time_request_meeeting_form_submittng_msg').show();
        }
        else
        {
          $('#request_meeting_popup').modal('hide');
        }
      }
    });
  });
  $('#no_metting_btn_click').click(function(){
    $('#msg_span_text_show').html('');
    $('#form_date_time_request_meeeting_form').show();
    $('#form_date_time_request_meeeting_form_submittng_msg').hide();
  });
     function sendmessage()
     {
          jQuery("#sendbtn").html('Submitting <i class="fa fa-refresh fa-spin"></i>');
          var str = jQuery("#imageform").serialize();
          var values = jQuery("input[name='unpublished_photo[]']")

          var flag=false;
          var flag1=false;
          
          if(jQuery("#Message").val() != "")
          {
               flag=true;
          }
          //alert(values.val());
          if(values.val() !=undefined)
          {
               console.log(values.val());
               flag1=true;
          }
          
          if ((flag==false && flag1==false))
          {
               alert("This status update appears to be blank. Please write something or attach a link or photo to update your status.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               jQuery("#sendbtn").removeAttr('disabled');
               return false;
          }

          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$subdomain; ?>/chatsexibitor/<?php echo $Sid; ?>",
               data: str,
               type: "POST",
               async: true,
               success: function(result)
               {
                    var loadfun = "loadmore(10,null);";
                    jQuery("#loadmore").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                    jQuery("#loadmore").css('display', 'block');
                    jQuery('#Message').val('');
                    jQuery("#preview_msg").html();
                    jQuery("#preview_msg").html(result);
                    jQuery("#preview").html('');
                    jQuery(".addmore_photo").css('display', 'none');
                    jQuery(".msg_edit-arrow").on('click', function() {
                         var id = jQuery(this).attr('data-id');
                         jQuery("#msg_edit-view" + id).slideToggle("slow");
                    });
                    jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               }

          });
     }
</script>
<?php

function timeAgo($time_ago)
{
     $cur_time = time();
     $time_elapsed = $cur_time - $time_ago;

     $seconds = $time_elapsed;
     $minutes = round($time_elapsed / 60);
     $hours = round($time_elapsed / 3600);
     $days = round($time_elapsed / 86400);
     $weeks = round($time_elapsed / 604800);
     $months = round($time_elapsed / 2600640);
     $years = round($time_elapsed / 31207680);
     // Seconds
     if ($seconds <= 60)
     {
          echo "$seconds seconds ago";
     }
     //Minutes
     else if ($minutes <= 60)
     {
          if ($minutes == 1)
          {
               echo "one minute ago";
          }
          else
          {
               echo "$minutes minutes ago";
          }
     }
     //Hours
     else if ($hours <= 24)
     {
          if ($hours == 1)
          {
               echo "an hour ago";
          }
          else
          {
               echo "$hours hours ago";
          }
     }
     //Days
     else if ($days <= 7)
     {
          if ($days == 1)
          {
               echo "yesterday";
          }
          else
          {
               echo "$days days ago";
          }
     }
     //Weeks
     else if ($weeks <= 4.3)
     {
          if ($weeks == 1)
          {
               echo "a week ago";
          }
          else
          {
               echo "$weeks weeks ago";
          }
     }
     //Months
     else if ($months <= 12)
     {
          if ($months == 1)
          {
               echo "a month ago";
          }
          else
          {
               echo "$months months ago";
          }
     }
     //Years
     else
     {
          if ($years == 1)
          {
               echo "one year ago";
          }
          else
          {
               echo "$years years ago";
          }
     }
}
?>
<script type="text/javascript">
     jQuery.noConflict();
     jQuery(document).ready(function() {
          jQuery(document).on('mouseover', '.msg_photo', function() {
               var my_class = $(this).find('a').attr('class');
               $("." + my_class).colorbox({rel: my_class, maxWidth: '95%', maxHeight: '95%'});
          });
          
          jQuery(document).on('mouseover', '.msg_fromname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'), maxHeight: '95%',width:"50%"});
          });
          
          jQuery(document).on('mouseover', '.msg_toname', function() {
               var my_class = $(this).find('button').attr('class');
               $("." + my_class).colorbox({rel: my_class,maxWidth: '95%',href:$("." + my_class).attr('data-href'),maxHeight: '95%',width:"50%"});
          });

          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
               jQuery("#photoimg").removeAttr('multiple');
          }
          else
          {
               jQuery("#photoimg").attr('multiple');
          }

          jQuery(".msg_edit-arrow").on('click', function() {
               var id = jQuery(this).attr('data-id');
               jQuery("#msg_edit-view" + id).slideToggle("slow");
          });

          //jQuery(".299").colorbox({rel:"299", transition:"none", width:"75%", height:"75%"});

     });

     jQuery("#btn_biography").click(function() {
          jQuery("#view_biography").toggle("slow");
     });

     jQuery("#btn_askquestion").click(function() {
          jQuery("#imageform").toggle("slow");
          jQuery("#Message").focus();
     });

     var desiredWidth;

     jQuery(document).ready(function() {
          console.log('onReady');
          jQuery("#takePictureField").on("change", gotPic);
          jQuery("#preview").load(getSwatches);
          desiredWidth = window.innerWidth;

          if (!("url" in window) && ("webkitURL" in window)) {
               window.URL = window.webkitURL;
          }

     });

     function getSwatches() {
          var colorArr = createPalette(jQuery("#preview"), 5);
          for (var i = 0; i < Math.min(5, colorArr.length); i++) {
               jQuery("#swatch" + i).css("background-color", "rgb(" + colorArr[i][0] + "," + colorArr[i][1] + "," + colorArr[i][2] + ")");
               console.log(jQuery("#swatch" + i).css("background-color"));
          }
     }

     //Credit: https://www.youtube.com/watch?v=EPYnGFEcis4&feature=youtube_gdata_player
     function gotPic(event) {
          if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {
               jQuery("#preview").append("<li><div><input type='hidden' name='unpublished_photo[]' value='" + event.target.files[0] + "'><img src='" + URL.createObjectURL(event.target.files[0]) + "' class='imgList'><button class='photo_remove_btn' type='button' title='' id='" + event.target.files[0].name + "'>&nbsp;</button></div></li>");
               jQuery(".photo_remove_btn").click(function()
               {
                    jQuery(".addmore_photo").css("display", "none");
                    jQuery(this).parent().parent().remove();
                    jQuery("#preview li").each(function() {
                         jQuery(".addmore_photo").css("display", "block");
                    });
               });
               //jQuery("#preview").attr("src",URL.createObjectURL(event.target.files[0]));
          }
     }

     function addcomment(id)
     {
          var str = jQuery("#commentform" + id).serialize();
          var values = jQuery("input[name='cmphto[]']");
          
          if (jQuery.trim(jQuery("#comment_text" + id).val()) != "" || values.val() != "")
          {
               jQuery.ajax({
                         url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$event_templates[0]['Subdomain']; ?>/commentadd/<?php echo $Sid; ?>",
                         data: str,
                         type: "POST",
                         async: true,
                         success: function(result)
                         {
                              jQuery("#comment_conten" + id).html(result);
                              jQuery("#comment_text" + id).val('');
                              jQuery("#cpreview" + id).html('');
                              jQuery('#camera_icon_comment').show();
                         }

                    });
          }
          else
          {
               alert("Please write something or photo to add comment.");
               jQuery("#sendbtn").html('Submit <i class="fa fa-arrow-circle-right"></i>');
               return false;
          }
               return false;
     }
     
     function loadmore(str, istype)
     {
          
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name.'/'.$subdomain; ?>/loadmore1/" + (str - 5) + "/" + str + "/" + istype + "/"+ <?php echo $Sid; ?>,
               type: "POST",
               async: true,
               success: function(result)
               {
                    if (result != "")
                    {
                         jQuery('#Message').val('');

                         jQuery("#preview_msg").append(result);
                        
                         var a = parseInt(str) + 5;
                         var loadfun = "loadmore(" + a + "," + istype + ");";
                         jQuery("#loadmore").html('<a href="javascript: void(0)" onclick="' + loadfun + '"  class="loadmore panel-green">Load More</a>');

                         jQuery("#preview").html('');
                         jQuery(".addmore_photo").css('display', 'none');

                         jQuery(".msg_edit-arrow").on('click', function()
                         {
                              var id = jQuery(this).attr('data-id');
                              jQuery("#msg_edit-view" + id).slideToggle("slow");
                         });
                    }
                    else
                    {
                         jQuery("#loadmore").css('display', 'none');
                    }
               }

          });
     }

     function viewmore_comment(id)
     {
          jQuery("#comment_conten" + id + " div").each(function() {
               jQuery(this).removeClass('comment_msg_hide');
          });
          jQuery("#comment_viewmore" + id).remove();
     }

     function removemsg(id, e)
     {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$subdomain; ?>/delete_message/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().parent().remove();
               }

          });
     }

     function removecomment(id, e)
     {
          jQuery.ajax({
               url: "<?php echo base_url(); ?>Messages/<?php echo $acc_name."/".$subdomain; ?>/delete_comment/" + id,
               type: "POST",
               async: true,
               success: function(result)
               {
                    jQuery(e).parent().remove();
               }

          });
     }

     function comment_photo(id)
     {

          //jQuery('#cmphto'+id).off('click').on('change', function() 
          //{
          jQuery("#commentform" + id).ajaxForm({target: '#cpreview' + id,
               beforeSubmit: function() {

                    //console.log('ttest');
                    jQuery('#camera_icon_comment').hide();
                    jQuery("#imageloadstatus").show();
                    jQuery("#imageloadbutton").hide();
               },
               success: function() {
                    //console.log('test');
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();
                    jQuery(".comment_section_btn").click(function()
                    {
                         jQuery(".addmore_photo").css("display", "none");
                         jQuery(this).parent().remove();
                         jQuery("#preview li").each(function() {
                              jQuery(".addmore_photo").css("display", "block");
                         });
                         jQuery('#camera_icon_comment').show();
                    });
               },
               error: function() {
                    //console.log('xtest');
                    jQuery("#imageloadstatus").hide();
                    jQuery("#imageloadbutton").show();

               }}).submit();
          //});

          return false;

     }

</script>
