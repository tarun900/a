<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
?>
<div class="row margin-left-10">
    <?php
           if(!empty($user)):  
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                  continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('3', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick="add_advertise_hit()"> 
       <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } }  ?>
 </div>
</div>
<?php } ?>
<?php if($user[0]->Rid=='6' && count($attendees_metting)>0){ ?>
<div class="col-sm-12"> <a id="check_metting_btn" href="<?php echo base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting'; ?>" class="btn btn-green user_btn">See My Meetings</a></div>
<?php } ?>
<div class="agenda_content">
     <div>
          <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
     </div>
  <div class="panel panel-white panel-white-new">
    <div class="tabbable panel-green" id="test">
        <div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left tabbable-bot">
          <div class="tabbable-bot-inner user_container_data">
            <?php if(!empty($exibitors_list)) { ?>
              <?php foreach ($exibitors_list as $key1 => $value1) { ?>
                <div class="date_separator panel-heading background-none" id="<?php echo ucfirst($key1); ?>" data-appear-top-offset="-400">
                    <i class="fa fa-user"></i><?php echo ' '.$key1; ?>
                </div>
                <div class="ps-container padding-right-0" id="<?php echo ucfirst($key1); ?>">
                    <ul class="activities columns activities-new">
                        <?php foreach ($value1 as $key => $value) {  ?>
                            <li>
                               
                                <a href="<?php echo base_url(); ?>Exhibitors/<?php echo $acc_name.'/'.$Subdomain;?>/View/<?php echo $value['Id']; ?>" class="activity" id="a<?php echo $value['Id']; ?>">
                                    <?php  $logo_images_array = json_decode($value['company_logo']); 

                                    if(!empty($logo_images_array)) { ?>
                                    <img style="width:72px;height:72px;border-radius: 0 !important;" src="<?php echo base_url(); ?>assets/user_files/<?php echo $logo_images_array[0]; ?>">
                                    <?php } else { ?>
									<?php $color = sprintf("#%06x",rand(0,16777215)); ?>
									<span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $color; ?>"><?php echo ucfirst(substr($value['Heading'], 0, 1)); ?></span>
                                    <!--<img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
                                    <?php } ?>
                                    <div class="desc descrip">
                                         <h4 class="user_container_name"><?php echo ucfirst($value['Heading']); ?></h4>
                                    </div>
                                    <div class="Exbitor_keyword" style="margin-left:8.4%;">
                                     <?php if($value['stand_number']!=""){
                                     echo "Stand Number:-".$value['stand_number']; 
                                    } ?>
                                    </div>
                                  <div class="time"><i class="fa fa-chevron-right"></i></div>
                                </a>
                           </li>
                       <?php } ?>
                    </ul>
                </div>
              <?php } ?>
              <?php } else{ ?>
              <div class="tab-content">
                  <span>No Exhibitors available for this App.</span>
              </div>
            <?php } ?>
            </div>
          </div>
          <div class="a-to-z-list pull-right">
            <ul class="timeline-scrubber inner-element">
              <?php
                $keys = array_keys($exibitors_list);
                $a=range("A","Z");
                foreach ($a as $char) 
                { 
                    if(!empty($exibitors_list))
                    {
                        echo '<li class="clearfix">';
                ?>
                        <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                    <?php   echo'</li>';
                    }
                }
              ?>
            </ul>
            
          </div>
          
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(function () {
       $("li.clearfix:first-child").addClass("selected");
       $(".clearfix").click(function(){
           $("li.clearfix.selected").removeClass("selected");
           $(this).addClass("selected");
        });
    });
</script>
<?php foreach ($attendees as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>

<script type="text/javascript">

    $( "#a<?php echo $value['Id']; ?>" ).click(function() {

        $('#div<?php echo $value['Id']; ?>').css('display','block');
        $('#div<?php echo $value['Id']; ?>').css('background','white');
        $('#test').css('display','none');
      
    });

</script>
<?php } } ?>
<script>
    jQuery(document).ready(function()
     {
       //Main.init();
       Timeline.init();
     });
     
     jQuery("#search_user_content").keyup(function()
     {    
          jQuery(".user_container_data li").each(function( index ) 
          {
              var str=jQuery(this).find('.user_container_name').html();
              var strkey=jQuery(this).find('.Exbitor_keyword').html();
              if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='' || jQuery.trim(strkey)!=undefined || jQuery.trim(strkey.toLowerCase())!='')
              {    
                   var str1=jQuery(this).find('.user_container_name').html();
                  var str1key=jQuery(this).find('.Exbitor_keyword').html();
                   if(str1!=undefined || str1key!=undefined)
                   {
                         var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
                         var str1key=jQuery(this).find('.Exbitor_keyword').html().toLowerCase();
                         var content=jQuery("#search_user_content").val().toLowerCase();
                         //content=content;
                         console.log(content);
                         if(content!=null)
                         {
                              
                              jQuery(this).parent().parent().parent().find(".date_separator").each(function(index) 
                              {
                                   var content1=jQuery(this).text().toLowerCase();
                                   //alert(content1);
                                   //alert(str1);
                                   var n1 = str1.indexOf(content1);
                                   //alert(n1);
//                                   if(n1!="-1")
//                                   {
//                                        jQuery(this).css('display','block');
//                                   }
//                                   else
//                                   {
//                                        jQuery(this).css('display','none');
//                                   }
                              });
                              
                              var n = str1.indexOf(content);
                              var nkey=str1key.indexOf(content);
                              if(n=="-1" && nkey=='-1')
                              {
                                  jQuery(this).css('display','none');
                                  //jQuery(this).parent().css('display','none'); 
                                  //$(this).parent().parent().parent().find(".date_separator").css('display','block');
                              }
                              else
                              {
                                jQuery(this).css('display','inline-block');
                                //jQuery(this).parent().css('display','block');
                                //$(this).parent().parent().parent().find(".date_separator").css('display','none');
                              }
                         }
                    }
              }
              
              //alert($(this).parents().find("li").css('display'))
          });
     });
          
</script>
<script type="text/javascript">
  function add_advertise_hit()
  {
     
      $.ajax({
                url : '<?php echo base_url().Agenda."/".$Subdomain ?>/add_advertise_hit',
                type: "POST",  
                async: false,
                success : function(data1)
                {

                }
             });
  }
</script>

