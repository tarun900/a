<?php 
    $arrMenu = array();
    $arrCms  = array();
    if(!empty($arrForm['m_id']))
    {
        $arrMenu = explode(',',$arrForm['m_id']);
    }
    if(!empty($arrForm['cms_id']))
    {
        $arrCms = explode(',',$arrForm['cms_id']);
    }
?>
<!-- start: PAGE CONTENT -->
<div class="row advertising">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Assign  <span class="text-bold">Modules</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="advertisement" style="background:#eee;padding:15px;">
                    <?php 
                    $Event_id = $event_templates[0]['Id'];
                    $arrRem = array(12,13,19,21,22,23,24,26,27,20,25,5,14,8,18,28);
                    ?>
                        <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Formbuilder/asssignmodule/<?php echo $arrForm['event_id'];?>/<?php echo $arrForm['id'];?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="form-field-1" style="padding-left:0px;padding-right:0px;" class="col-sm-2">Standard modules to show Form<span class="symbol required"></span></label>
                                <div class="col-sm-10">
                                    <select multiple="multiple" style="margin-top:-8px;" id="Menu_id" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id[]">
                                        <?php foreach ($menu_toal_data as $key => $value): 
                                            
                                            if(in_array($value['id'], $arrRem)):
                                            continue;
                                            endif;    
                                            ?>
                                        <option id="menu<?php echo $key; ?>" <?php if(in_array($value['id'], $arrMenu)): echo 'selected';endif; ?> value="<?php echo $value['id']; ?>"><?php echo $value['menuname']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                    <br/><br/>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Custom modules to show Form</label>
                                <div class="col-sm-10">
                                    <select multiple="multiple" style="margin-top:-8px;" id="Cms_id" class="select2-container select2-container-multi form-control search-select" name="Cms_id[]">
                                        <?php foreach ($cms_list as $key => $value) { ?>
                                        <option value="<?php echo $value['Id']; ?>" <?php if(in_array($value['Id'], $arrCms)): echo 'selected';endif; ?> ><?php echo $value['Menu_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                    <div style="clear:both;"></div>
                    <br/><br/>
                    <div class="form-group">
                                <label class="control-label col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Select Position of Form</label>
                                <div class="col-sm-10">
                                    <select style="margin-top:-8px;"  class="form-control required" name="frm_pos">
                                        <option value="">Select</option>
                                        <option value="1" <?php if($arrForm['frm_posi']=='1'): echo 'selected'; endif; ?>>Header</option>
                                        <option value="2" <?php if($arrForm['frm_posi']=='2'): echo 'selected'; endif; ?>>Footer</option>
                                        <option value="3" <?php if($arrForm['frm_posi']=='3'): echo 'selected'; endif; ?>>Both(Header & Footer)</option>
                                    </select>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                    <br/><br/>
                    <button type="submit" class="btn btn-primary">
                            Submit
                    </button>
                        </form>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->

<style type="text/css">
    #select2-result-label-7
    {
        display: none;
    }
</style>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#google').prop('checked',true);
        $('.green').show();
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="red"){
                $(".red").toggle();
                $(".green").hide();
            }
            if($(this).attr("value")=="green")
            {
                $(".green").toggle();
                $(".red").hide();
            }
        });
    });
</script>

<style type="text/css">
    .box{
        display: none;
    }
</style>