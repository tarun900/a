<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- start: DYNAMIC TABLE PANEL -->
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">

                <?php //if($user->Role_name=='Client'){ ?>
                <a style="top:7px;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>Formbuilder/add_form/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Create Form </a>
                <?php //} ?>

                <?php if($this->session->flashdata('formbuild_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> Form <?php echo $this->session->flashdata('formbuild_data'); ?> Successfully.
                </div>
                <?php } ?>
                
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#form_list" data-toggle="tab">
                                Form List
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
						
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="form_list">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th width="10%">Name</th>
                                        <th width="40%">Standard Modules to show the form</th>
                                        <th width="30%">Custom Modules to show the form</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($form_list);$i++) {
                                        $arrJson = json_decode($form_list[$i]['json_data']);
                                        ?>
                                    <tr>
                                        <td width="15%"><a href="<?php echo base_url() ?>Formbuilder/edit_form/<?php echo $form_list[$i]['event_id']; ?>/<?php echo $form_list[$i]['id']; ?>"><?php echo $arrJson->title; ?></a></td>
                                        <td width="40%"><?php echo $form_list[$i]['std_menu'];?></td>
                                        <td width="20%"><?php echo $form_list[$i]['cms_menu'];?></td>
                                        <td width="20%">
                                            <?php //if($user->Role_name=='Client'){ ?>
                                            <a href="<?php echo base_url() ?>Formbuilder/edit_form/<?php echo $form_list[$i]['event_id']; ?>/<?php echo $form_list[$i]['id']; ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" onclick="delete_form('<?php echo $form_list[$i]['id']; ?>','<?php echo $form_list[$i]['event_id']; ?>')" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                            <a  href="<?php echo base_url() ?>Formbuilder/formcsv/<?php echo $form_list[$i]['event_id'];?>/<?php echo $form_list[$i]['id']; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Csv">Csv</a>
                                            <br/><a href="<?php echo base_url() ?>Formbuilder/asssignmodule/<?php echo $form_list[$i]['event_id']; ?>/<?php echo $form_list[$i]['id']; ?>"  class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove">Assign Module</a>
                                            <?php //} ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>		
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/tableExport.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/jquery.base64.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/html2canvas.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/jquery.base64.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/jspdf/jspdf.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tableExport/jspdf/libs/base64.js"></script>
<script src="<?php echo base_url() ?>assets/js/table-export.js"></script>
<script type="text/javascript" language="javascript" class="init">
         $(document).ready(function() {
              Main.init();
              TableExport.init();
              //TableExport.init();
              // Data table
              $('#sample_2').dataTable({
                   "bPaginate": true,
                   "bLengthChange": true,
                   "bFilter": true,
                   "bSort": true,
                   "bInfo": true,
                   "bAutoWidth": false
              });
              </script>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script>
    function delete_form(id,Event_id)
    {
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Formbuilder/delete_form/" + Event_id + "/" + id;
        }
    }
</script>


