<!-- start: PAGE CONTENT -->

<div class="row advertising">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Edit <span class="text-bold">Form</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="advertisement" style="background:#eee;padding:15px;">
                        <div id="formBuilder"></div>
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->

<style type="text/css">
    #select2-result-label-7
    {
        display: none;
    }
</style>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#google').prop('checked',true);
        $('.green').show();
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="red"){
                $(".red").toggle();
                $(".green").hide();
            }
            if($(this).attr("value")=="green")
            {
                $(".green").toggle();
                $(".red").hide();
            }
        });
    });
</script>
<style type="text/css">
    .box{
        display: none;
    }
</style>