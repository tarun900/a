<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">User's Role <span class="text-bold">List</span></h4>
                <div class="panel-tools"></div>
			</div>
			<div class="panel-body">
                <?php if($this->session->flashdata('user_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('user_data'); ?> Successfully.
                </div>
                <?php } ?>
                <?php //print_r($user_overview); exit(); ?>
                <div class="table-responsive">
    				<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
    					<thead>
    						<tr>
    							<th>Sr. No</th>
    							<th>User Role</th>
                                <th>Action</th>
    						</tr>
    					</thead>
    					<tbody>
    						<tr>
    							<td>1</td>
    							<td>Manager</td>
                                <td>
                                    <?php if($user->Role_name=='Client'){ ?>
                                    <a href="user_permission/edit/29" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    <?php } ?>
                                    
                                </td>
    						</tr>
                            <tr>
                                <td>2</td>
                                <td>Social Media Manager</td>
                                <td>
                                    <?php if($user->Role_name=='Client'){ ?>
                                    <a href="user_permission/edit/30" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    <?php } ?>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Marketing</td>
                                <td>
                                    <?php if($user->Role_name=='Client'){ ?>
                                    <a href="user_permission/edit/31" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    <?php } ?>
                                    
                                </td>
                            </tr>
    					</tbody>
    				</table>
                </div>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->