<?php
    /*$survey_answer_chart = $this->event_template_model->get_survey_answer_chart();
    $this->data['survey_answer_chart'] = $survey_answer_chart;
*/
    $final_Array = array();

    foreach ($survey_answer_chart as $key => $value) 
    {
        $final_Array[$value['Question']][$value['panswer']][] = array(
                                                'id' => $value['puserid'],
                                                'Firstname' => $value['Firstname'],
                                                'Lastname' => $value['Lastname'],
                                                'Email' => $value['Email']
                                            );              

    }

    foreach($final_Array as $key => $value)
    {
        foreach ($value as $i => $j) 
        {
            $final_Array[$key][$i] = sizeof($j);
        }

    }
    $arryvalues=array();
    //$string="";
    foreach($final_Array as $key => $item)
    {
     ?>

    <?php if(!empty($item)) { ?>

    <div style="padding:10px;">

    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}"></script>
    <div id='piechart<?php echo $key; ?>' style='text-align:center;'></div>

            <script type="text/javascript">
                google.setOnLoadCallback(drawChart);
                  function drawChart() {                                       

                    var data = google.visualization.arrayToDataTable([
                      ['Options', 'No. of users'],
                      <?php foreach ($item as $a => $b) { ?>
            
                    ['<?php echo $a; ?>', <?php echo $b; ?>],
                    
            <?php
        }
        
        ?>
        ]);

                    var options = {
                      title: '<?php echo 'Question: '. $key; ?>','width':1100,'height':300
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo $key;?>'));

                    chart.draw(data, options);
                  }
            </script>
            <?php
            echo'</div>';

            //echo $string;
        } else { ?>

            <h3 style="text-align:center;">No survey record for this event.</h3>

      <?php  

     }
        
     }
     ?>