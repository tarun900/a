<?php $acc_name = $this->session->userdata('acc_name'); ?>

<div class="row">
    <div class="col-md-12">
	<div class="agenda_content notes-content box-effect thanks_page">
    <div class="panel panel-white">
    <!--<div class="panel-body"><a style="margin-right:1%;" class="btn btn-green pull-right view_survey" href="<?php echo base_url(); ?>Surveys/<?php echo $acc_name.'/'.$Subdomain; ?>/View">View survey</a></div>-->
 
    <?php if(!empty($survey_screens[0]['thanku_data'])) { ?>
         <div class="page-error animated shake">
            <div class="error-details col-sm-12 thanks_content1">
                <h3 class="propercontent"><?php echo $survey_screens[0]['thanku_data'];?></h3>
            </div>
        </div>
    <?php } else {  ?>
         <div class="page-error animated shake">
            <div class="error-number text-azure">
                Thank you
            </div>
            <div class="error-details col-sm-6 col-sm-offset-3 thanks_content">
                <h3 class="propercontent">for completing the Survey.</h3>
            </div>
        </div>
   <?php } ?>
   <div class="survey-finish">
      <input type="button" value="Finish" class="btn-green btn-block btn" onclick="finish1()" style="max-width:25%;"></input>
      </div>
    </div>
	</div>
	</div>
</div>
<style type="text/css">
    .row .panel .page-error .thanks_content
    {
        border: 0px !important;
        margin-left: 25% !important; 
    }
</style>
<script type="text/javascript">
    function finish1() 
    {
       location.href = "<?php echo base_url(); ?>app/<?php echo $acc_name.'/'.$Subdomain; ?>";
    }
</script>