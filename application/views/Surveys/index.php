<?php 
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
date_default_timezone_set("UTC");
$cdate = date('Y-m-d H:i:s');
if(!empty($event_templates[0]['Event_show_time_zone']))
{
  if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
  { 
    $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
    $intoffset=$arr[1]*3600;
    $intNew = abs($intoffset);
    $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
  }
  if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
  {
    $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
    $intoffset=$arr[1]*3600;
    $intNew = abs($intoffset);
    $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
  }
}
?>
<div class="surway-page">
  <div class="panel panel-white box-effect">
      <div class="panel-body">
          <?php if($survey_category[0]['relese_datetime']<=$cdate || empty($survey_category[0]['relese_datetime'])){ ?>
          <?php if(!empty($survey)) { ?>
          <form action="" role="form" class="smart-wizard form-horizontal" id="form" method="POST">
          <?php 
          if(!empty($user)) { ?>
              <input type="hidden" name="action" value="0" />
       <?php    } else {   ?>
            <input type="hidden" name="action" value="1" />
        <?php } ?>
              <?php if(!empty($survey_screens[0]['welcome_data'])) { ?>
               <div id="welcome_msg">
                    <?php echo $survey_screens[0]['welcome_data'];?>
                    <div class="start-survey">
                        <input type="button" class="btn btn-green btn-block" onclick="startSurvey()" value="Start" style="max-width: 25%;"></input>
                        
                    </div>
            
              </div>
              <?php } ?>
              <div id="wizard" class="swMain" style="<?php if(!empty($survey_screens[0]['welcome_data'])) { echo 'display:none'; } ?>">
                  <ul style="display:none;">
                      <?php 
                      foreach($survey as $key => $val){
                      ?>
                      <li> <a href="#step-<?php echo $key; ?>"></a> </li>
                      <?php } ?>
                  </ul>
                 
                  <div class="progress progress-striped transparent-black no-radius active">
                      <div aria-valuemax="100" aria-valuemin="10" role="progressbar" class="progress-bar partition-green step-bar"> <span class=""> 0% </span> </div>
                  </div>
                  <?php foreach($survey as $key => $val){ ?>
                  <div id="step-<?php echo $key; ?>">
                      <div class="list-group">
                          <div class="list-group-item active">
                              <h4 class="list-group-item-heading"><?php echo $val['Question']; ?></h4>
                              
                          </div>
                          <div class="list-group-item">
                              <div class="col-md-12 col-lg-6 col-sm-12">
                                  <div class="form-group"> <span class="hdr-span"> <?php if($val['Question_type']==1){ echo 'Please select one answer.';  }else if($val['Question_type']==2){ echo 'Please select mulitple answer.'; } ?></span>
                                      <div class="skins">
                                          <div class="skin skin-line">
                                              <dl class="clear">
                                                  <dd class="selected">
                                                      <div class="skin-section">
                                                          <ul class="list">
                                                              <?php 
                                                              $Option=  json_decode($val['Option']);
                                                              
                                                              foreach($Option as $keyo=>$valuo)
                                                              {
                                                                   ?>
                                                                   <li>
                                                                      <?php if($val['Question_type']==1){ ?>
                                                                      <input type="radio" name="question_option<?php echo $key; ?>" value="<?php echo $valuo; ?>" class="required radio-callback">
                                                                      <?php }else if($val['Question_type']==2){ ?>
                                                                      <input type="checkbox" name="question_option<?php echo $key; ?>[]" value="<?php echo $valuo; ?>" class="required">
                                                                      <?php }else if($val['Question_type']==3){ ?>
                                                                      <textarea name="question_option<?php echo $key; ?>[]" class="texta form-control required"><?php echo $valuo; ?></textarea>
                                                                      <?php } ?>
                                                                          <label for="line-radio-<?php echo $key; ?>"><?php echo $valuo; ?></label>
                                                                  </li>
                                                                   <?php
                                                              }

                                                               ?>
                                                               
                                                               
                                                          </ul>
                                                      </div>
                                                  </dd>
                                              </dl>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <?php if($key==0 && sizeof($survey) >1){ ?>
                                      <div class="row">
                                      <!-- <div class="col-sm-3" style="margin-right:50%;">
                                          <button class="btn btn-green Skip-step btn-block"> Skip <i class="fa fa-arrow-circle-right"></i> </button>
                                      </div> -->
                                      <div class="col-sm-3">
                                          <button class="btn btn-green next-step btn-block"> Next <i class="fa fa-arrow-circle-right"></i> </button>
                                      </div>
                                      </div>
                                      <?php } elseif($key==0 && sizeof($survey) ==1) { ?>

                                      <div class="col-sm-3 col-sm-offset-9">
                                          <button class="btn btn-primary finish-step btn-block"> Finish <i class="fa fa-arrow-circle-right"></i> </button>
                                      </div>


                                      <?php }  if($key>0){ ?>
                                      <div class="row">
                                      <div class="col-sm-3" style="margin-right:15%;">
                                          <button class="btn btn-green back-step btn-block"> <i class="fa fa-arrow-circle-left"></i> Back </button>
                                      </div>
                                      <div class="col-sm-3">
                                      <?php if($key!=count($survey)-1){ ?>
                                          <!--<button class="btn btn-green Skip-step btn-block"> Skip <i class="fa fa-arrow-circle-right"></i> </button>-->
                                       <?php } ?>
                                      </div>
                                      <div class="col-sm-3" style="margin-left:10%;">
                                          <?php if($key<count($survey)-1){ ?>
                                          <button class="btn btn-green next-step btn-block"> Next <i class="fa fa-arrow-circle-right"></i> </button>
                                          <?php }else if($key==count($survey)-1){ ?>
                                          <button class="btn btn-primary finish-step btn-block"> Finish <i class="fa fa-arrow-circle-right"></i> </button>
                                          <?php } ?>
                                      </div>
                                      </div>
                                      <?php } ?>
                                      
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <input type="hidden" name="question[]" value="<?php echo $val['Id']; ?>" />

                  <?php } ?>
              </div>
          </form>
      
          <?php } elseif(!empty($survey_value)) { ?>

              <h3 style="text-align:center;">There are no uncompleted surveys available, thank you for participating.</h3>

              <?php } else { ?> 

              <div class="tab-content">
               <h3 style="text-align:center;">No Question available for this Surveys.</h3>
              </div>

          <?php } }else{ ?>
          <div class="tab-content">
            <h3 style="text-align:center;">This Survey is not available yet. It will be available at <?php echo $time_format[0]['format_time']=='0' ? date('h:i A',strtotime($survey_category[0]['relese_datetime'])) : date('H:i',strtotime($survey_category[0]['relese_datetime']))  ; ?> – <?php echo $event_templates[0]['date_format']=='1' ? date("m/d/Y",strtotime($survey_category[0]['relese_datetime'])) : date("d/m/Y",strtotime($survey_category[0]['relese_datetime']));  ?></h3>
          </div>
          <?php } ?>
    </div>
     
  </div>
</div>
<style type="text/css">
.texta
{
  width: 100%;
  min-width: 100%;
  max-width: 100%;
  height: 200px;
  min-height: 200px;
  max-height: 200px;
}
.skins .checked 
{
  background-color: #333;
}
.skins .icheckbox_line-blue:hover 
{
  background-color: #333;
}
</style>
<script type="text/javascript">
function startSurvey()
{
  $("#welcome_msg").css("display", "none");
  $("#wizard").css("display", "block");
} 
</script>