<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Surveys</h4>
            </div>
            <div class="panel-body">
                <?php //echo'<pre>'; print_r($survey); exit();
                    for ($i = 0; $i < count($survey); $i++)
                    {
                        echo'<form role="form" class="form-horizontal" id="form'.$survey[$i]['Id'].'" action="" method="post" novalidate="novalidate">';
                        $test = count($survey);
                        $val = $i + 1;
                        $res = ($val / $test)*100;
                        $result = $res.'%';

                        echo'<div class="swMain">
                            <div style="height: 12px;margin-bottom: 0px;" class="progress progress-xs transparent-black no-radius active">
                                <div style="height: 42px;width:'.$result.';margin-top:-3%;" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                    <span style="clip: inherit;color: #000;width:auto;height: auto;" class="sr-only">Question '.$val.' of '.$test.'</span>
                                </div>
                            </div>
                        </div>';

                        echo '<span>'.$survey[$i]['Question'].'</span>'.'</br/>';
                        if($survey[$i]['Question_type'] =='1')
                        {
                            echo '<span>Please select one answer.</span>';
                        }
                        else
                        {
                            echo '<span>Please select mulitple answer.</span>';
                        }
                            echo '<br/>';

                        if($survey[$i]['Question_type'] =='1')
                        { 
                            echo '<span><input class="Answer" type="radio" name="Answer[]" value="'.$survey[$i]['Option1'].'">'.$survey[$i]['Option1'].'</span></br/>';
                            echo '<span><input class="Answer" type="radio" name="Answer[]" value="'.$survey[$i]['Option2'].'">'.$survey[$i]['Option2'].'</span></br/>';
                            echo '<span><input class="Answer" type="radio" name="Answer[]" value="'.$survey[$i]['Option3'].'">'.$survey[$i]['Option3'].'</span></br/>';
                            echo '<span><input class="Answer" type="radio" name="Answer[]" value="'.$survey[$i]['Option4'].'">'.$survey[$i]['Option4'].'</span></br/>';
                        }
                        else
                        {
                            echo '<span><input class="Answer" type="checkbox" name="Answer[]" value="'.$survey[$i]['Option1'].'">'.$survey[$i]['Option1'].'</span></br/>';
                            echo '<span><input class="Answer" type="checkbox" name="Answer[]" value="'.$survey[$i]['Option2'].'">'.$survey[$i]['Option2'].'</span></br/>';
                            echo '<span><input class="Answer" type="checkbox" name="Answer[]" value="'.$survey[$i]['Option3'].'">'.$survey[$i]['Option3'].'</span></br/>';
                            echo '<span><input class="Answer" type="checkbox" name="Answer[]" value="'.$survey[$i]['Option4'].'">'.$survey[$i]['Option4'].'</span></br/>';
                        }

                        echo '<input type="hidden" name="Question_id" value="'.$survey[$i]['Id'].'">
                              <input type="hidden" name="Event_id" value="'.$event_templates[0]['Id'].'">
                              <input type="hidden" name="Organisor_id" value="'.$event_templates[0]['Organisor_id'].'">
                              <input type="hidden" name="Value" value="true">';
                        echo '<input class="submit" type="button" value="Submit" onclick="sendans();">'.'<br/>';
                        echo'</form>';
                    }
                ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript">

    function sendans() 
    {
        var str = $("#form<?php echo $survey[0]['Id']; ?>").serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>Surveys/<?php echo $event_templates[0]['Subdomain']; ?>",
                        data: str,
                        type: "POST",
                        async: true,
                        success: function(result)
                        {
                            $("#form<?php echo $survey[0]['Id']; ?> .Answer").prop("disabled", true); 
                            $("#form<?php echo $survey[0]['Id']; ?> .submit").prop("disabled", true); 
                        }

                });
    }
</script>

<!-- end: PAGE CONTENT-->