<?php 
$k = array_rand($advertisement_images);
$advertisement_images = $advertisement_images[$k];
$user = $this->session->userdata('current_user');
$acc_name = $this->session->userdata('acc_name');
$favorites_user=array_filter(array_column_1($my_favorites,'module_id'));
$active_menu=array_filter(explode(",",$event_templates[0]['checkbox_values']));
?>
<div class="row margin-left-10">
    <?php
          if(!empty($form_data)):
          foreach($form_data as $intKey=>$strval):
              if($strval['frm_posi']==2)
              {
                continue;
              }
          ?>

        <div class="col-sm-6 col-sm-offset-3">

        <?php
          $json_data = false;
          $formid="form";
          
          $temp_form_id=rand(1,9999);
         
          $json_data = isset($strval) ? $strval['json_data'] : FALSE;
          $loader = new formLoader($json_data, base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/'.$strval['id'],$formid.$temp_form_id);
          $loader->render_form();
          unset($loader);
          ?>
          </div>
          <?php include 'validation.php'; ?>
            <script>
                jQuery(document).ready(function() {
                    FormValidator<?php echo $temp_form_id; ?>.init();
                });
            </script>
        <?php endforeach;  
        endif;
        ?>
</div>
<?php
if(!empty($advertisement_images)) { ?>
<div class="main-ads">
<?php 
    $string_cms_id = $advertisement_images->Cms_id;
    $Cms_id =  explode(',', $string_cms_id); 

    $string_menu_id = $advertisement_images['Menu_id'];
    $Menu_id =  explode(',', $string_menu_id); 

if(in_array('2', $Menu_id)) 
{

$image_array = json_decode($advertisement_images['H_images']);
$f_url = $advertisement_images['Footer_link'];

  if(!empty($image_array)) { ?>
  
  <div class="hdr-ads">
    
    <a class="thumb-info" target="_blank" href="<?php echo $advertisement_images['Header_link']; ?>" data-title="<?php echo ucfirst($event_templates[0]['Event_name'].' '); ?>Gallery" onclick='add_advertise_hit("<?php echo $advertisement_images['Id'] ?>")'> 
      <button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>
    <?php

      $img_url = base_url().'assets/user_files/'.$image_array[0];
      $size = getimagesize($img_url);

      $originalWidth = $size[0];
      $originalHeight = $size[1];

       if($originalHeight > '118')
       {
          $first = $originalWidth * 118;
          $width = $first / $originalHeight;

          echo'<img width="'.$width.'" height="118" alt="Logo" src="'.$img_url.'">';
       }
       elseif ($originalHeight < '118') 
       { 
          echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
       else
       {
           echo'<img width="'.$originalWidth.'" height="'.$originalHeight.'" alt="Advertisement" src="'.$img_url.'">';
       }
    ?>
    </a>
  <?php  } else { if(!empty($advertisement_images[0]['Google_header_adsense'])) { ?>

  <?php echo '<div style=""><button type="button" class="close-custom close" data-dismiss="alert" 
          aria-hidden="true"> &times; </button>'.$advertisement_images[0]['Google_header_adsense'].'</div>'; ?>
 <?php } } } ?>
 </div>
</div>
<?php } ?>
<div class="agenda_content attendee_content_div speakers-content">
  <div class="panel panel-white panel-white-new">
    <div class="tabbable panel-green">
      <div id="alldiv">
        <ul id="myTab6_for_attendee" class="nav nav-tabs">
          <li class="active"> 
            <a href="#full_directory_section" id="full_directory_tab" data-toggle="tab"> Full Directory </a> 
          </li>
          <li> 
            <a href="#my_contact_section" id="my_contact_tab" data-toggle="tab"> My Contacts </a> 
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade in active" id="full_directory_section">
            <div class="input-group search_box_user">
              <i class="fa fa-search search_user_icon"></i>    
              <input type="text" class="form-control" id="search_user_content_by_full_directory" placeholder="Search...">
            </div>
            <div class="panel panel-white panel-white-new">
              <div class="tabbable panel-green" id="test_full_directory_div">
                <div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
                  <div class="a-to-z-list pull-right">
                    <ul class="timeline-scrubber inner-element timeline-scrubber-right">
                      <?php $keys = array_keys($attendees);
                      $a=range("A","Z");
                      foreach ($a as $char) 
                      { 
                        if(!empty($attendees))
                        {
                          echo '<li class="clearfix">'; ?>
                          <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'" href="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                          <?php   echo'</li>';
                        }
                      } ?>
                    </ul>
                  </div>
				  
                  <div class="speakers-left-bot user_container_data_in_full_directory">
                    <?php if(!empty($attendees)) { 
                      foreach ($attendees as $key1 => $value1) {
                    ?> 
                      <div class="date_separator panel-heading date_separator-new" id="<?php echo ucfirst($key1); ?>" data-appear-top-offset="-400">
                            <i class="fa fa-user"></i> <?php echo ' '.$key1; ?>
                      </div>
                      <div class="ps-container ps-container-new" id="<?php echo ucfirst($key1); ?>">
                        <ul class="activities columns columns-new">
                          <?php foreach ($value1 as $key => $value) { ?>
                          <li>
                            <a href="<?php echo base_url(); ?>Attendee/<?php echo $acc_name."/".$Subdomain; ?>/View/<?php echo $value['Id']; ?>" class="activity" id="full_directory_a<?php echo $value['Id']; ?>">
                              <?php if(!empty($value['Logo']) && !empty(pathinfo($value['Logo'], PATHINFO_EXTENSION))) { ?>
                              <img width="72" height="72" src="<?php echo filter_var($value['Logo'], FILTER_VALIDATE_URL) ? $value['Logo'] : base_url().'assets/user_files/'.$value['Logo']; ?>" alt="" />
                              <?php } else { ?>
							  <span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $event_templates[0]['Top_background_color']; ?>"><?php echo ucfirst(substr($value['Firstname'], 0, 1)).''.ucfirst(substr($value['Lastname'], 0, 1)); ?></span>
                              <!--<img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
                              <?php } ?>
                              <div class="desc">
                                <h4 class="user_container_name"><?php echo ucwords($value['Firstname'].' '.$value['Lastname']);?></h4>
                                <span style="display:none;" class="user_container_title"><?php echo ucfirst($value['Title']); ?></span>
                                <span style="display:none;" class="user_container_comoanyname"><?php echo ucfirst($value['Company_name']); ?></span>
                                 <?php if($value['Title']!="" && $value['Company_name']!=""){ ?>
                                <small><?php echo ucfirst($value['Title']).' at '.ucfirst($value['Company_name']); ?></small>
                                <?php } ?>
                              </div>
                              <div class="time"><i class="fa fa-chevron-right"></i></div>
                            </a>
                            <?php if(!empty($user[0]->Id) && in_array('49',$active_menu) && $user[0]->Rid=='4' && $user[0]->Id!=$value['Id']){ ?> 
                            <div class="myfavorites_content_div">
                              <a onclick='my_favorites("<?php echo $value['Id']; ?>");'>
                                <img width="5%" src="<?php echo base_url().'assets/images/favorites_not_selected.png'; ?>" class="pull-right" id="favorites_not_selected_<?php echo $value['Id']; ?>" <?php if(in_array($value['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                                <img width="5%" src="<?php echo base_url().'assets/images/favorites_selected.png'; ?>" class="pull-right" id="favorites_selected_<?php echo $value['Id']; ?>" <?php if(!in_array($value['Id'],$favorites_user)){ ?> style="display:none;" <?php } ?>>
                              </a>
                              <a href="javascript:void(0);" style="display: none;">
                                <img width="5%" src="<?php echo base_url().'assets/images/ajax-loader.gif'; ?>" id="loder_icone_<?php echo $value['Id']; ?>">
                              </a>
                            </div>
                            <?php } ?>    
                          </li>
                          <?php } ?>
                        </ul>
                      </div>
                    <?php } }else{ ?>
                    <div class="tab-content">
                      <span>No Attendee available for this App.</span>
                    </div> 
                    <?php } ?>
                  </div>  
                </div>
                <?php foreach ($attendees as $key1 => $value1) { ?>
                <?php foreach ($value1 as $key => $value) { ?>
                    <div class="speakers-content-bot" id="full_directory_div<?php echo $value['Id']; ?>">
                    </div>
                <?php } } ?>
              </div>
            </div>  
          </div>
          <div class="tab-pane fade in" id="my_contact_section">
            <div class="input-group search_box_user">
              <i class="fa fa-search search_user_icon"></i>    
              <input type="text" class="form-control" id="search_user_content_by_my_contact" placeholder="Search...">
            </div>
            <div class="panel panel-white panel-white-new">
              <div class="tabbable panel-green" id="test_my_contact_div">
                <div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
                  <div class="a-to-z-list pull-right">
                    <ul class="timeline-scrubber inner-element timeline-scrubber-right">
                      <?php $keys = array_keys($attendees_mycontact);
                      $a=range("A","Z");
                      foreach ($a as $char) 
                      { 
                        if(!empty($attendees_mycontact))
                        {
                          echo '<li class="clearfix">'; ?>
                          <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'" href="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                          <?php   echo'</li>';
                        }
                      } ?>
                    </ul>
                  </div>
                  <div class="speakers-left-bot user_container_data_in_my_contact">
                    <?php if(!empty($attendees_mycontact)) {
                      foreach ($attendees_mycontact as $key2 => $value2) {
                    ?>
                      <div class="date_separator panel-heading date_separator-new" id="<?php echo ucfirst($key2); ?>" data-appear-top-offset="-400">
                            <i class="fa fa-user"></i><?php echo ' '.$key2; ?>
                      </div>
                      <div class="ps-container ps-container-new" id="<?php echo ucfirst($key2); ?>">
                        <ul class="activities columns columns-new">
                          <?php foreach ($value2 as $vkey => $vvalue) { ?>
                          <li>
                            <a href="<?php echo base_url(); ?>Attendee/<?php echo $acc_name."/".$Subdomain; ?>/View/<?php echo $vvalue['uid']; ?>" class="activity" id="my_contact_a<?php echo $vvalue['uid']; ?>">
                              <?php if(!empty($vvalue['Logo']) && !empty(pathinfo($vvalue['Logo'], PATHINFO_EXTENSION))) { ?>
                              <img width="72" height="72" src="<?php echo filter_var($vvalue['Logo'], FILTER_VALIDATE_URL) ? $vvalue['Logo'] : base_url().'assets/user_files/'.$vvalue['Logo']; ?>" alt="" />
                              <?php } else { ?>
							  <span style="text-align: center;width:72px;height:72px;border-radius:50%;padding-top: 18px;font-size: 26px;display: inline-block;color:#fff;background:<?php echo $event_templates[0]['Top_background_color']; ?>"><?php echo ucfirst(substr($vvalue['Firstname'], 0, 1)).''.ucfirst(substr($vvalue['Lastname'], 0, 1)); ?></span>
                              <!--<img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />-->
                              <?php } ?>
                              <div class="desc">
                                <h4 class="user_container_name"><?php echo ucwords($vvalue['Firstname'].' '.$vvalue['Lastname']);?></h4>
                                <span style="display:none;" class="user_container_title"><?php echo ucfirst($vvalue['Title']); ?></span>
                                <span style="display:none;" class="user_container_comoanyname"><?php echo ucfirst($vvalue['Company_name']); ?></span>
                                 <?php if($vvalue['Title']!="" && $vvalue['Company_name']!=""){ ?>
                                <small><?php echo ucfirst($vvalue['Title']).' at '.ucfirst($vvalue['Company_name']); ?></small>
                                <?php } ?>
                              </div>
                              <div class="time"><i class="fa fa-chevron-right"></i></div>
                            </a>    
                          </li>
                          <?php } ?>
                        </ul>
                      </div>
                    <?php } }else{ ?>
                    <div class="tab-content">
                      <span>No Contact available.</span>
                    </div> 
                    <?php } ?>
                  </div>  
                </div>
                <?php foreach ($attendees_mycontact as $key1 => $value1) { ?>
                <?php foreach ($value1 as $key => $value) { ?>
                    <div class="speakers-content-bot" id="my_contact_div<?php echo $value['uid']; ?>">
                    </div>
                <?php } } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>      
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(function () {
  $("li.clearfix:first-child").addClass("selected");
  $(".clearfix").click(function(){
    $("li.clearfix.selected").removeClass("selected");
    $(this).addClass("selected");
  });
});
</script>
<?php foreach ($attendees as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>
<script type="text/javascript">
$( "#full_directory_a<?php echo $value['Id']; ?>" ).click(function() {
  $('#full_directory_div<?php echo $value['Id']; ?>').css('display','block');
  $('#full_directory_div<?php echo $value['Id']; ?>').css('background','white');
  $('#test_full_directory_div').css('display','none');
});
</script>
<?php } } ?>
<?php foreach ($attendees_mycontact as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>
<script type="text/javascript">
$( "#my_contact_a<?php echo $value['uid']; ?>" ).click(function() {
  $('#my_contact_div<?php echo $value['uid']; ?>').css('display','block');
  $('#my_contact_div<?php echo $value['uid']; ?>').css('background','white');
  $('#test_my_contact_div').css('display','none');
});
</script>
<?php } } ?>
<script>
jQuery(document).ready(function(){
  if(location.hash){
    if($.trim(location.hash)=="#content"){
      $('#full_directory_tab').parent().removeClass('active');
      $('#my_contact_tab').parent().addClass('active');
      $('#full_directory_section').removeClass('active in');
      $('#my_contact_section').addClass('active in');
    }
  }
  jQuery(".user_container_data_in_full_directory li").each(function(index){
    $(this).css('display','inline-block');
    $(this).parent().css('display','block');
  });
  jQuery(".user_container_data_in_my_contact li").each(function(index){
    $(this).css('display','inline-block');
    $(this).parent().css('display','block');
  });
});
jQuery("#search_user_content_by_full_directory").keyup(function(){    
  jQuery(".user_container_data_in_full_directory li").each(function( index ){
    var strnm=jQuery(this).find('.user_container_name').html();
    var strtitle=jQuery(this).find('.user_container_title').html();
    var strcompany=jQuery(this).find('.user_container_comoanyname').html();
    if(jQuery.trim(strnm)!=undefined || jQuery.trim(strnm.toLowerCase())!='' || jQuery.trim(strtitle)!=undefined || jQuery.trim(strtitle.toLowerCase())!='' || jQuery.trim(strcompany)!=undefined || jQuery.trim(strcompany.toLowerCase())!='')
    {    
      var strnm1=jQuery(this).find('.user_container_name').html();
      var strtitle1=jQuery(this).find('.user_container_title').html();
      var strcompany1=jQuery(this).find('.user_container_comoanyname').html();
      if(strnm1!=undefined || strtitle1!=undefined || strcompany1!=undefined)
      {
        var strnm1=jQuery(this).find('.user_container_name').html().toLowerCase();
        var strtitle1=jQuery(this).find('.user_container_title').html().toLowerCase();
        var strcompany1=jQuery(this).find('.user_container_comoanyname').html().toLowerCase();
        var content=jQuery("#search_user_content_by_full_directory").val().toLowerCase();
        if(content!=null)
        {
          jQuery(this).parent().parent().parent().find(".date_separator").each(function(index){
            var content1=jQuery(this).text().toLowerCase();
            var n1 = strnm1.indexOf(content1);
          });                    
          var nm = strnm1.indexOf(content);
          var title = strtitle1.indexOf(content);
          var company = strcompany1.indexOf(content);                    
          if(nm!="-1" || title!="-1" || company!="-1")
          {
           jQuery(this).css('display','inline-block');
          }
          else
          {
            jQuery(this).css('display','none');
          }
        }
      }
    }
  });
});
jQuery("#search_user_content_by_my_contact").keyup(function(){    
  jQuery(".user_container_data_in_my_contact li").each(function( index ){
    var strnm=jQuery(this).find('.user_container_name').html();
    var strtitle=jQuery(this).find('.user_container_title').html();
    var strcompany=jQuery(this).find('.user_container_comoanyname').html();
    if(jQuery.trim(strnm)!=undefined || jQuery.trim(strnm.toLowerCase())!='' || jQuery.trim(strtitle)!=undefined || jQuery.trim(strtitle.toLowerCase())!='' || jQuery.trim(strcompany)!=undefined || jQuery.trim(strcompany.toLowerCase())!='')
    {    
      var strnm1=jQuery(this).find('.user_container_name').html();
      var strtitle1=jQuery(this).find('.user_container_title').html();
      var strcompany1=jQuery(this).find('.user_container_comoanyname').html();
      if(strnm1!=undefined || strtitle1!=undefined || strcompany1!=undefined)
      {
        var strnm1=jQuery(this).find('.user_container_name').html().toLowerCase();
        var strtitle1=jQuery(this).find('.user_container_title').html().toLowerCase();
        var strcompany1=jQuery(this).find('.user_container_comoanyname').html().toLowerCase();
        var content=jQuery("#search_user_content_by_my_contact").val().toLowerCase();
        if(content!=null)
        {
          jQuery(this).parent().parent().parent().find(".date_separator").each(function(index){
            var content1=jQuery(this).text().toLowerCase();
            var n1 = strnm1.indexOf(content1);
          });                    
          var nm = strnm1.indexOf(content);                    
          var title = strtitle1.indexOf(content);
          var company = strcompany1.indexOf(content);
          if(nm!="-1" || title!="-1" || company!="-1")
          {
           jQuery(this).css('display','inline-block');
          }
          else
          {
            jQuery(this).css('display','none');
          }
        }
      }
    }
  });
});
function my_favorites(uid)
{
  $('#loder_icone_'+uid).parent().show();
  $('#favorites_not_selected_'+uid).parent().hide();
  $.ajax({
    url:"<?php echo base_url().'My_Favorites/'.$acc_name.'/'.$Subdomain.'/save_myfavorites'; ?>",
    type:'post',
    data:'module_id='+uid+"&module_type=2",
    success:function(result){
      var data=result.split('###');
      if($.trim(data[0])=="success")
      {
        var name=$('#full_directory_a'+uid).find('.user_container_name').text();
        $('#loder_icone_'+uid).parent().hide();
        $('#favorites_not_selected_'+uid).parent().show();
        $('#favorites_not_selected_'+uid).parent().removeAttr('disabled');
        if(data[1]=='0')
        {
          $('#favorites_not_selected_'+uid).show();
          $('#favorites_selected_'+uid).hide();
          var msg = name+" Removed From My Favorites";
          var title = 'Removed From My Favorites';
        }
        else
        {
          $('#favorites_not_selected_'+uid).hide();
          $('#favorites_selected_'+uid).show();
          var msg = name+" Added Into Your My Favorites";
          var title = 'Added To My Favorites';
        }
      }
      else
      {
        $('#loder_icone_'+uid).parent().parent().remove();
        var msg = "Somthing Went To Wrong Please Reload the Page.";
        var title = 'Somthing Went To Wrong';
      }
      var shortCutFunction ='info';
      var $showDuration = 1000;
      var $hideDuration = 10000;
      var $timeOut = 10000;
      var $extendedTimeOut = 10000;
      var $showEasing = 'swing';
      var $hideEasing = 'linear';
      var $showMethod = 'fadeIn';
      var $hideMethod = 'fadeOut';
      toastr.options = {
        closeButton: true,
        debug: false,
        positionClass:'toast-bottom-right',
        onclick: null
      };
      toastr.options.showDuration = $showDuration;
      toastr.options.hideDuration = $hideDuration;
      toastr.options.timeOut = $timeOut;                        
      toastr.options.extendedTimeOut = $extendedTimeOut;
      toastr.options.showEasing = $showEasing;
      toastr.options.hideEasing = $hideEasing;
      toastr.options.showMethod = $showMethod;
      toastr.options.hideMethod = $hideMethod;
      toastr[shortCutFunction](msg, title);
    }
  });
}
</script>
<script type="text/javascript">
jQuery(document).ready(function(){   
  SVExamples.init();
});
</script>