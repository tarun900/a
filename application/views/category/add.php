<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Add <span class="text-bold">Category</span></h4>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal" id="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Category Name <span class="symbol required"></span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Category Name" id="Category_name" name="Category_name" class="form-control" onblur="checkcategory();">
                            <input type="hidden" placeholder="idval" class="form-control" id="idval" name="idval" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Description
                        </label>
                        <div class="col-sm-9">
                            <textarea placeholder="Description" id="Description" name="Description" class="form-control"></textarea>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            Event
                        </label>
                        <div class="col-sm-9">
                            <select multiple="multiple" id="form-field-select-4" class="form-control search-select" name="event[]">
                                <?php for ($i = 0; $i < count($Event); $i++) { ?>
                                    <option value="<?php echo $Event[$i]->Id; ?>"><?php if ($Event[$i]->Event_name != '') echo $Event[$i]->Event_name;
                                else echo $Event[$i]->Common_name; ?></option>
<?php } ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">

                        </label>
                        <div class="col-md-4">
                            <button class="btn btn-yellow btn-block" type="submit" onclick="checkcategory();">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->