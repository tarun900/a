<script type="text/javascript">
var FormValidator<?php echo $temp_form_id; ?> = function () {
    "use strict";
    var runValidator1 = function () {
        var form1 = $('#<?php echo $formid.$temp_form_id; ?>');

        var errorHandler1 = $('.errorHandler', form1);
        var successHandler1 = $('.successHandler', form1);
        $.validator.addMethod("FullDate", function () {
            if ($("#dd").val() != "" && $("#mm").val() != "" && $("#yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a day, month, and year');

        $.validator.addMethod("validateDate", function () {
            if ($("#Schedule").val() == "1")
            {
                if ($("#Schedual_date").val() != "") {
                    return true;
                } else {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }, 'Please select a date');
        $.validator.addMethod("csv", function () {
            if ($("#csv").val().toLowerCase().lastIndexOf(".csv") == -1)
            {
                return true;

            }
            else
            {
                return false;
            }
        }, 'Please select CSV file');

        $.validator.addMethod("validatefor", function () {
            if ($("#Schedule").val() == "1")
            {
                if ($("#Schedule_for").val() != "") {
                    return true;
                } else {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }, 'Please select Schedule For');

        $.validator.addMethod("lessThan", function (value, element, param) {
            var i = parseInt($.trim(value));
            var chi = parseInt($.trim($(param).html()));
            return (i <= chi) ? true : false;
        }, 'Select Qty less than available');


        $.validator.addMethod("checkvalidateemail", function () {
            var flag = checkemail();
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }, 'Email already exist. Please choose another email.');

        $.validator.addMethod("checkvalidatetitle", function () {
            var flag = checktitle();
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }, 'Title already exist. Please choose another Title.');
        $.validator.addMethod("checkvalidateroles", function () {
            var flag = checkrole();
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }, 'Role already exist. Please choose another Role.');

        $.validator.addMethod("checkvalidatesubdomain", function () {
            var flag = checksubdomain();
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }, 'Subdomain already exist. Please choose another Subdomain.');

        $('#<?php echo $formid.$temp_form_id; ?>').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy" || element.attr("name") == "Schedual_date" || element.attr("name") == "Start_date" || element.attr("name") == "End_date") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                }
            },
            ignore: "",
            rules: {},
            messages: {},
            groups: {
                DateofBirth: "dd mm yyyy",
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element)
            {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                successHandler1.show();
                errorHandler1.hide();
                $(".btn").each(function (index) {
                    console.log($(this).attr('type'));
                    if ($(this).attr('type') == "submit")
                    {
                        $(this).html('Submitting <i class="fa fa-refresh fa-spin"></i>');
                    }
                });
                form.submit();
            }
        });
    };
    return {
        init: function () {
            runValidator1();
        }
    };
}();

</script>