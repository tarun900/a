<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>
<script type="text/javascript">
     $(document).ready(function() 
     {
          $('.summernote').summernote({
               height: 300,
               minHeight: null,
               maxHeight: null,
          });
     });
</script>
<script type="text/javascript">
     jQuery(document).ready(function() 
     {
          jQuery('.image-editor').cropit({
               exportZoom: 1.25,
               imageBackground: true,
               imageBackgroundBorderWidth: 20,
               imageState: {
                    src: 'https://lorempixel.com/500/400/'
               }
          });
          jQuery('.export').click(function() {
               var imageData = $('.image-editor').cropit('export');
               window.open(imageData);
          });
     });
</script>
<div class="row" id="edit">
     <div class="col-sm-12">
          <div class="panel panel-white">
               <?php 
                    if ($this->session->flashdata('error'))
                    { 
               ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                         <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                    </div>
               <?php 
                    }
               ?>
               <div class="panel-body" style="padding: 0px;">
                    <div class="tabbable">
                         <ul id="myTab2" class="nav nav-tabs">
                              <li class="active">
                                   <a href="#edit_events" data-toggle="tab">
                                        Edit Roles
                                   </a>
                              </li>
							  
							    <li class="">
									<a id="view_events1" href="#view_events" data-toggle="tab">
										View App
									</a>
								</li>
					
                         </ul>
                         <div class="tab-content">
                              <div class="tab-pane fade active in" id="edit_events">
                                    <?php if ($this->session->flashdata('event_data')) { ?>
                                        <div class="errorHandler alert alert-success no-display" style="display: block;">
                                             <i class="fa fa-remove-sign"></i> Event <?php echo $this->session->flashdata('event_data'); ?> Successfully.
                                        </div>
									<?php } ?>
                                   <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                        <div class="form-group">
                                             <label class="col-sm-2" for="form-field-1">
                                                  Role <span class="symbol required"></span>
                                             </label>
                                             <div class="col-sm-4" style="padding-right:0px;">
                                                  <?php if($role['Id']!='95'){ ?>
                                                       <input  type="text"  value="<?php echo $role['Name']; ?>" placeholder="Role" id="role" name="role" class="form-control name_group required" onblur="checkrole();">
                                                  <?php }else{ echo $role['Name'];  } ?>
                                                  <input type="hidden" class="form-control" id="idval" name="idval" value="<?php echo $idval; ?>">
                                                  <input type="hidden" class="form-control" id="event_id" name="event_id" value="<?php echo $event_id; ?>">
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="col-sm-2" for="form-field-1">
                                                  Role Status <span class="symbol required"></span>
                                             </label>
                                             <div class="col-sm-4" style="padding-right:0px;">
                                                  <select id="form-field-select-1 Role_id" class="form-control required" name="Active">
                                                       <option value="1" <?php if ($role['Active'] == '1') { ?> selected <?php } ?>>Active</option>
                                                       <option value="0" <?php if ($role['Active'] == '0') { ?> selected <?php } ?>>Deactivate</option>
                                                  </select>           
                                             </div>                                                            
                                        </div>
                                      
                                        <div class="form-group">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Editable Modules</label>
                                            <div class="col-sm-10">
                                                <select multiple="multiple" style="margin-top:-8px;height:auto;"  class="select2-container select2-container-multi form-control menu-section-select search-select required" name="Menu_id[]">
                                                    <?php foreach ($role_menu as $key => $value) { ?>
                                                    <option <?php echo in_array($value->id, $array_permission) ? 'selected="selected"' : ''; ?> value="<?php echo $value->id; ?>"><?php echo $value->menuname; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row" style="clear:both;">
                                             <label class="col-sm-2 control-label" for="form-field-1"></label>
                                             <div class="col-md-4">
                                                  <button class="btn btn-yellow btn-block" type="submit">
                                                       Update <i class="fa fa-arrow-circle-right"></i>
                                                  </button>
                                             </div>
                                        </div>
                                   </form>
                              </div>
							  
							  <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
								<div id="viewport" class="iphone">
										<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
								</div>
								<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
								<div id="viewport_images" class="iphone-l" style="display:none;">
									   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
								</div>
							</div>
				
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div></div>


<style type="text/css">
     #map
     {
          margin-bottom: 20px;
          height: 250px;
          background-color: #fff !important;
     }
     .gm-style
     {
          left: 17.5% !important;
          width: 74% !important;
     }
     .fileupload-new.thumbnail
     {
          height:auto;
     }
</style>

<script type="text/javascript">

     function checkrole()
     {
          var sendflag = "";
          $.ajax({
               url: '<?php echo base_url(); ?>Client/checkrole',
               data: 'role=' + $("#role").val() + '&idval=' + $('#idval').val(),
               type: "POST",
               async: false,
               success: function(data)
               {
                    var values = data.split('###');

                    if (values[0] == "error")
                    {
                         $('#role').parent().removeClass('has-success').addClass('has-error');
                         $('#role').parent().find('.control-label span').removeClass('ok').addClass('required');
                         $('#role').parent().find('.help-block').removeClass('valid').html(values[1]);

                         sendflag = false;

                    }
                    else
                    {
                         $('#role').parent().removeClass('has-error').addClass('has-success');
                         $('#role').parent().find('.control-label span').removeClass('required').addClass('ok');
                         $('#role').parent().find('.help-block').addClass('valid').html('');
                         sendflag = true;
                    }
               }
          });
          return sendflag;
     }
</script>


<style>
     .cropit-image-preview {
          background-color: #f8f8f8;
          background-size: cover;
          border: 5px solid #ccc;
          border-radius: 3px;
          margin-top: 7px;
          width: 250px;
          height: 250px;
          cursor: move;
     }
     .cropit-image-background {
          opacity: .2;
          cursor: auto;
     }
     .image-size-label {
          margin-top: 10px;
     }
     input {
          /* Use relative position to prevent from being covered by image background */
          position: relative;
          /*z-index: 10;*/
          display: block;
     }
     .export {
          margin-top: 10px;
     }
</style>
<!-- end: PAGE CONTENT