<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->

<div class="row">
	<div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="panel panel-white">
		
			<div class="panel-body" style="padding: 0px;">
                <?php $url=base_url().'Role_management/add/'; ?>
                 <?php //if($user->Role_name=='Client'){
                     $url1=base_url().'User/add/';
                 ?>
                

                <a style="top: 7px;" class="btn btn-primary list_page_btn role_btn" href="<?php echo $url.$event_id11 ?>"><i class="fa fa-plus"></i> Add Role</a>
                <a style="top: 7px;display: none;" class="btn btn-primary list_page_btn user_btn"  href="<?php echo $url1.$event_id11 ?> "><i class="fa fa-plus"></i> Add User</a>

              
                <?php if($this->session->flashdata('user_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('user_data'); ?> Successfully.
                </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#role_management" data-toggle="tab">
                                Role management
                            </a>
                        </li>
                        <li class="">
                            <a href="#users_list" data-toggle="tab">
                                Users
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
					
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="role_management">
                      <div class="panel-body">

                            <div class="table-responsive">
    				            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
    					           <thead>
    						          <tr>
            							<th>#</th>
            							<th>Role Name</th>
                                        <th>Active</th>
                                        <th>Edit or Delete</th>
            						  </tr>
    					            </thead>
    					       <tbody>

                            <?php $srno=1;
                    
                            foreach ($role as $value) 
                            {
                                
                             /*if($value['Id']!=3 && $value['Id']!=4 && $value['Id']!=5 && $value['Id']!=6 && $value['Id']!=7) {*/
                                
                              if($value['status']==1 and $value['Name']=='Administrator')
                              {
                                echo "<tr><td>".$srno."</td>";
                                $srno++;
                                ?>
                                <td><?php echo $value['Name']; ?></td></td>
                                <?php if($value['Active']==1)
                                {
                                        echo "<td><span class='label label-sm label-success'>Active</td></span>";
                                }
                                else
                                {
                                  echo "<td><span class='label label-sm label-success'>InActive</td></span>";
                                } ?>
                                <td>
                                <?php if($value['status']==0) {  ?>
                                 <!--<a href="javascript:;" onclick="delete_role(<?php echo $value['Id']; ?>,<?php echo $event_id11; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>-->
                                <?php
                                echo "</tr>"; 
                                }
                              }
                              else
                              {
                                echo "<tr><td>".$srno."</td>";
                                $srno++; 
                                ?>
                                <td><a href="<?php echo base_url(); ?>Role_management/edit/<?php echo $event_id11.'/'.$value['Id'] ?>"><?php echo $value['Name']; ?></a></td>
                                <?php if($value['Active']==1)
                                {
                                   echo "<td><span class='label label-sm label-success'>Active</td></span>";
                                }
                                else
                                {
                                  echo "<td><span class='label label-sm label-success'>InActive</td></span>";
                                }?>
                                <td>
                                <a href="<?php echo base_url(); ?>Role_management/edit/<?php echo $event_id11.'/'.$value['Id'] ?>" class="label btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <?php
                                if($value['status']==0)
                                {
                                ?>
                                <!--<a href="javascript:;" onclick="delete_role(<?php echo $value['Id']; ?>,<?php echo $event_id11; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>-->
                                <?php
                                echo "</tr>"; 
                                }
                              }
                            /* }*/
                            }
                            ?>

    						                 
    						</tr> 
    					</tbody>
    				</table>
                </div>
            </div>

                </div>




             <div class="tab-pane fade" id="users_list">
                       
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th class="hidden-xs">Email</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                         </tr>
                                    </thead>
                      <tbody>
                            <?php for($i=0;$i<count($users);$i++) { ?>
                            <tr>
                                <td><?php echo $i+1; ?></td>
                                <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $users[$i]['Id']; ?>/<?php echo $event_id11; ?>"><?php echo $users[$i]['Firstname']; ?></a></td>
                                <td class="hidden-xs"><?php echo $users[$i]['Email']; ?></td>
                                <td>
                                    <span class="label label-sm 
                                        <?php if($users[$i]['Active']=='1')
                                            { 
                                            ?> 
                                              label-success 
                                            <?php
                                            }
                                            else
                                            {
                                                ?> label-danger 
                                            <?php
                                            } ?>">
                                            <?php if($users[$i]['Active']=='1')
                                                { 
                                                echo "Active"; 
                                                
                                                }
                                                else 
                                                { 
                                                    echo "Inactive";
                                                } 
                                        ?>
                                    </span>
                                </td>
                                <td>
                                    <?php //if($user->Role_name=='Client'){ ?>
                                    <a href="<?php echo base_url(); ?>Profile/update/<?php echo $users[$i]['Id']; ?>/<?php echo $event_id11; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                    
                                    <?php //} ?>
                                    <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                    <a href="javascript:;" onclick="delete_user(<?php echo $users[$i]['Id']; ?>,<?php echo $event_id11; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </div>


                <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                    <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                    </div>
                    <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                    <div id="viewport_images" class="iphone-l" style="display:none;">
                           <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
                    </div>
                </div>

                </div>
			</div>
		</div>
		<!-- end: DYNAMIC TABLE PANEL -->
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script>
    function delete_role(id,eid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>Role_management/delete/"+eid+ "/" + id;
        }
    }

    jQuery(function(){
        jQuery("#myTab2 li a").click(function(){
          var strTab = jQuery(this).prop("href");
          var arrTab = strTab.split("#");
       
          if(arrTab[1]=='users_list')
          {
              jQuery(".user_btn").show();
              jQuery(".role_btn").hide();
          }
          else if(arrTab[1]=='role_management')
          {
              jQuery(".role_btn").show();
              jQuery(".user_btn").hide();
          }
          else
          {
              jQuery(".role_btn").hide();
              jQuery(".user_btn").hide();
          }
        });   
    });
</script>
<script type="text/javascript">
    function delete_attendee(id,Event_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Attendee/delete/"+id+"/"+<?php echo $test; ?>
        }
    }
    function delete_user(id,eid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>User/delete/"+eid+ "/" + id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->