<!-- start: PAGE CONTENT -->

<div class="row">
     <div class="col-sm-12">
          <!-- start: TEXT FIELDS PANEL -->
          <div class="panel panel-white">
               <div class="panel-heading">
                    <h4 class="panel-title">Add <span class="text-bold">Role</span></h4>
                    <div class="panel-tools">
                         <a class="btn btn-xs btn-link panel-close" href="#">
                              <i class="fa fa-times"></i>
                         </a>
                    </div>
               </div>
               <div class="panel-body">
                    <form action="" method="POST"  role="form" id="form"  novalidate="novalidate">
                         <div class="row">
                              <?php 
                                   if ($this->session->flashdata('site_setting_data') == "Updated")
                                   { 
                              ?>
                                   <div class="errorHandler alert alert-success no-display" style="display: block;">
                                        <i class="fa fa-remove-sign"></i> Updated Successfully.
                                   </div>
                              <?php 
                                   } 
                              ?>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label class="control-label required">Name<span class="symbol required"></span> </span>
                                        </label>
                                        <input type="text" placeholder="Role" class="form-control required" id="role" name="role" onblur="checkrole();">
                                        <input type="hidden" class="form-control" id="idval" name="idval" value="">
                                        <input type="hidden" class="form-control" id="event_id" name="event_id" value="<?php echo $event_id; ?>">
                                   </div>                           

                                   <div class="form-group">
                                        <label for="form-field-select-1">Status <span class="symbol required"></span></label>
                                        <select id="form-field-select-1" class="form-control required" name="Active">
                                             <option value="1">Active</option>                                                                                
                                             <option value="0">Inactive</option>
                                        </select>                                                                
                                   </div>

                                            <div class="form-group">
                                      <label class="control-label" style="" for="form-field-1">Editable Modules<!--Standard modules--></label>
                              
                                    <select style="height:auto;" multiple="multiple" style="" id="Menu_id[]" class="select2-container select2-container-multi form-control search-select menu-section-select" name="Menu_id[]">
                                        <?php foreach ($role_menu as $key => $value) { ?>
                                        <option id="checkbox_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>"><?php echo $value->menuname; ?></option>
                                        <?php } ?>
                                    </select>
                                     
                                 </div>  

                                  <div class="row">
                                      <div class="col-md-4">
                                             <button class="btn btn-green btn-block" type="submit">
                                                  Add <i class="fa fa-arrow-circle-right"></i>
                                             </button>
                                        </div>
                                   </div>
                              </div>
                               </div>
                    </form>
               </div>
          </div>
          <!-- end: TEXT FIELDS PANEL -->
     </div>
</div>
<!-- end: PAGE CONTENT-->
