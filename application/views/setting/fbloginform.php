<?php $acc_name=$this->session->userdata('acc_name');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
                <?php if ($this->session->flashdata('agenda_data')) { ?>
                    <div class="errorHandler alert alert-success no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> Agenda <?php echo $this->session->flashdata('agenda_data'); ?> Successfully.
                    </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#emailpushnotidisplay">
                                Alerts 
                            </a>
                        </li>
						
						<li class="">
							<a id="view_events1" href="#view_events" data-toggle="tab">
								View App
							</a>
						</li>
                       
                    </ul>
                </div>

                <div class="tab-content">
                  
                    <div class="tab-pane fade active in" id="emailpushnotidisplay">
                            <form action="<?php echo base_url().'Setting/fbloginsetting/'.$this->uri->segment(3); ?>" method="post"> 
                                <script type="text/javascript">
                                $(document).ready(function(){
                                    $('input[type="checkbox"]').click(function(){
                                        if($('#emailsend').prop("checked") == true && $('#pushnoti').prop("checked") == true)
                                        {   
                                            $("#both").prop('checked', true); 
                                        }
                                        if($('#emailsend').prop("checked") == false || $('#pushnoti').prop("checked") == false)
                                        {
                                            $("#both").attr('checked', false);
                                        }
                                    }); 
                                });
                                function showcheck()
                                {
                                    var both=document.getElementById('both');
                                    var pushnoti=document.getElementById("pushnoti");
                                    var emailsend=document.getElementById("emailsend");
                                    if(both.checked==true)
                                    {
                                        pushnoti.checked = true;
                                        emailsend.checked = true;
                                    }
                                    else
                                    {
                                        pushnoti.checked = false;
                                        emailsend.checked = false;
                                    }
                                }
                                </script>
                                <div class="form-group">
                                    <label class="col-sm-2" for="form-field-1">Email Alerts<!--Email Send--></label>
                                    </div>
                                    <div class="col-sm-2" style="padding-right:0px;">
                                        <input type="checkbox" class="name_group" value="yes" <?php if($notificationsetting[0]['email_display']==1){ ?> checked="checked" <?php } ?> id="emailsend" name="emailsend">
                                    </div>
                                    <br><br>
                                     <div class="form-group">
                                    <label class="col-sm-2"  class="name_group"for="form-field-1">Push Notification</label>
                                    </div>
                                    <div class="col-sm-2" style="padding-right:0px;">
                                        <input type="checkbox" class="name_group"  value="yes"  <?php if($notificationsetting[0]['pushnoti_display']==1){ ?> checked="checked" <?php } ?> id="pushnoti" name="pushnoti">
                                    </div>
                                    <br><br>
                                    <div class="col-md-4">
                                        <input id="raisedbtn" name="displaybtn" value="Update"  class="btn btn-yellow btn-block" type="submit">  
                                    </div>
                                   <br><br>
                            </form>
                        </div> 
						
						<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
							<div id="viewport" class="iphone">
									<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
							</div>
							<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
							<div id="viewport_images" class="iphone-l" style="display:none;">
								   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
							</div>
						</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<!-- end: PAGE CONTENT-->