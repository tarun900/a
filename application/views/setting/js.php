<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/tableExport.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/jquery.base64.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/jquery.base64.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/jspdf/jspdf.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tableExport/jspdf/libs/base64.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-export.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.js"></script>
<script>
    jQuery(document).ready(function() {
        TableExport.init();
    });
    $('.view_display').click(function(){
       var id = $(this).attr('id');
       var idtoappend = "#"+id;
       $.ajax({
           url:"https://localhost/EventApp/event/detail_ajax",
           data:{id:id},
           type:"POST",
           dataType:"json",
           success: function(result){
                    //$("#eventTemplate").tmpl(result).insertAfter($(idtoappend).closest('tr'));                    
                    $( result.event ).each(function( index, value ) {
                    $(idtoappend).closest('tr').parent().find('.details').remove();
                    $(idtoappend).closest('tr').parent().find('.details_h').remove();
                    $("#eventTemplate").tmpl(value).fadeIn('6000').insertAfter($(idtoappend).closest('tr'));
                });
           }
       });
    });
</script>


<script id="eventTemplate" type="text/x-jQuery-tmpl">
<tr class="details">
<td colspan="4">
<table class="table table-bordered table-hover>
<thead>
<tr>
<th>#</th>
<th>Size</th>
<th>Quantity</th>
<th>Month</th>
</tr>
</thead>
<tbody>
{{if Attendee_New.length === 0 }}
<tr>
<td colspan="5">No records Found..</td>
</tr>
{{else}}
{{each Attendee_New}}
<tr>
<td>${$index + 1}</td>
<td>{{if $value.Size != ""}} ${$value.Size} {{else}}  0 {{/if}}</td>
<td>{{if $value.Qty != ""}} ${$value.Qty} {{else}}  0 {{/if}}</td>
<td>{{if $value.Month != ""}} ${$value.Month} {{else}}  0 {{/if}}</td>
</tr>
{{/each}}
{{/if}}
</tbody>
</table>
</td>
</tr>
</script>