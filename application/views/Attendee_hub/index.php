<?php $acc_name = $this->session->userdata('acc_name'); ?>
<div class="agenda_content speakers-content">
    <div class="row" style="text-align: center;font-size: 17px;">
    <p style="padding:0 10px;">Delegates will appear in the directory once they have logged into this app</p></div>
     <div>
          <div class="input-group search_box_user">
           <i class="fa fa-search search_user_icon"></i>    
           <input type="text" class="form-control" id="search_user_content" placeholder="Search...">
        </div>
     </div>
    <div class="panel panel-white panel-white-new">
        <div class="tabbable panel-green " id="test">
            <div class="demo1 col-md-12 col-lg-12 col-sm-12 pull-left speakers-left">
                 <div class="a-to-z-list pull-right">
                    <ul class="timeline-scrubber inner-element timeline-scrubber-right">
                        <?php
                            $keys = array_keys($attendees);
                            $a=range("A","Z");
                            foreach ($a as $char) 
                            { 
                                if(!empty($attendees))
                                {
                                    echo '<li class="clearfix">';
                            ?>
                                        <a <?php echo in_array($char, $keys) ? 'data-separator="#'.$char.'" href="#'.$char.'"'.'style="cursor:pointer;font-weight:bold;color:#707788;"' : 'style="color:#bbb;"'; ?>><?php echo $char; ?></a>
                                <?php   echo'</li>';
                                }
                            }
                        ?>
                    </ul>
                </div>
                <div class="speakers-left-bot user_container_data">
                    <?php if(!empty($attendees)) { ?>
                    <?php foreach ($attendees as $key1 => $value1) { ?>
                         <div class="date_separator panel-heading date_separator-new" id="<?php echo ucfirst($key1); ?>" data-appear-top-offset="-400">
                            <i class="fa fa-user"></i><?php echo ' '.$key1; ?>
                        </div> 
                        <div class="ps-container ps-container-new" id="<?php echo ucfirst($key1); ?>">
                            <ul class="activities columns columns-new"> 
                                <?php foreach ($value1 as $key => $value) {  ?>
                                    <li>
                                     
                                        <a href="<?php echo base_url(); ?>Attendee_hub/View/<?php echo $acc_name.'/'.$value['Id']; ?>" class="activity" id="a<?php echo $value['Id']; ?>">
                                            <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])) { ?>
                                            <img style="height:72px;width:72px;" src="<?php echo base_url() ?>assets/user_files/<?php echo $value['Logo'];?>" alt="" />
                                            <?php } else { ?>
                                            <img src="<?php echo base_url() ?>assets/images/user-2-icon.png" alt="" />
                                            <?php } ?>
                                            <div class="desc">
                                                <h4 class="user_container_name"><?php echo ucwords($value['Firstname'].' '.$value['Lastname']);?></h4>
                                                <?php if($value['Title']!="" && $value['Company_name']!=""){ ?>
                                                <small><?php echo ucfirst($value['Title']).' at '.ucfirst($value['Company_name']); ?></small>
                                                <?php } ?>
                                            </div>
                                          <div class="time"><i class="fa fa-chevron-right"></i></div>
                                        </a>
                                   </li>
                               <?php } ?>
                            </ul>
                        </div> 
                      <?php } } else{ ?>
                        <div class="tab-content">
                            <span>No Attendee available for this event.</span>
                        </div>
                    <?php } ?>
                    
                    </div>
                </div>
                
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(function () {
       $("li.clearfix:first-child").addClass("selected");
       $(".clearfix").click(function(){
           $("li.clearfix.selected").removeClass("selected");
           $(this).addClass("selected");
        });
    });
</script>
<?php foreach ($attendees as $key1 => $value1) { ?>
<?php foreach ($value1 as $key => $value) { ?>
<script type="text/javascript">
  $( "#a<?php echo $value['Id']; ?>" ).click(function() {

      $('#div<?php echo $value['Id']; ?>').css('display','block');
      $('#div<?php echo $value['Id']; ?>').css('background','white');
      $('#test').css('display','none');
    
  });
</script>
<?php } } ?>
<script>
jQuery(document).ready(function()
{
  jQuery(".user_container_data li").each(function(index) 
  {
    $(this).css('display','inline-block');
    $(this).parent().css('display','block');
  });
});
jQuery("#search_user_content").keyup(function()
{    
  jQuery(".user_container_data li").each(function( index ) 
  {
    var str=jQuery(this).find('.user_container_name').html();
    if(jQuery.trim(str)!=undefined || jQuery.trim(str.toLowerCase())!='')
    {    
      var str1=jQuery(this).find('.user_container_name').html();
      if(str1!=undefined)
      {
        var str1=jQuery(this).find('.user_container_name').html().toLowerCase();
        var content=jQuery("#search_user_content").val().toLowerCase();
        console.log(content);
        if(content!=null)
        {
          jQuery(this).parent().parent().parent().find(".date_separator").each(function(index) 
        {
        var content1=jQuery(this).text().toLowerCase();
        var n1 = str1.indexOf(content1);
        });

        var n = str1.indexOf(content);

        if(n!="-1")
        {
          jQuery(this).css('display','inline-block');
          jQuery(this).parent().css('display','block');
        }
        else
        {
          jQuery(this).css('display','none');
          jQuery(this).parent().css('display','none');
        }
        }
      }
    }
  });
});
</script>
<script>
jQuery(document).ready(function()
{   
  SVExamples.init();
}
</script>
