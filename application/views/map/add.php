<?php $acc_name=$this->session->userdata('acc_name');?>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#map_list" data-toggle="tab">
                            Add Map
                        </a>
                    </li>
					
					<li class="">
						<a id="view_events1" href="#view_events" data-toggle="tab">
							View App
						</a>
					</li>

                </ul>
            </div>


            <div class="panel-body" style="padding: 0px;">
                <?php if($this->session->flashdata('error')){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="map_list">
                      <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Title <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Map Title" class="form-control" id="Map_title" name="Map_title" onblur="checktitle();">
                                <input type="hidden" name="idval" id="idval" value="<?php echo $this->uri->segment(3); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Description 
                            </label>
                            <div class="col-sm-9">
                                <textarea maxlength="600" style="height: 135px;" id="Map_desc" class="form-control limited" name="Map_desc"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Include Map 
                            </label>

                             <div class="col-sm-1" style="padding-left: 0px;margin-left: -7px;">
                               <input type="checkbox" name="include-map" id="include-map" value="yes" class="form-control" checked="true"/>
                            </div> 
                        </div>

                        <!-- <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Satellite View 
                            </label>

                             <div class="col-sm-1" style="padding-left: 0px;margin-left: -7px;">
                               <input type="checkbox" name="satellite_view" id="satellite_view" value="yes" class="form-control"/>
                            </div> 
                        </div> -->
                        
                        <div class="form-group" id="maps">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Location
                            </label>
                            <div class="col-sm-9" style="height: 500px;">
                                <input type="hidden" name="zoom_level" id="zoom_level" value="" />
                                <input type="hidden" name="lat_long" id="lat_long" value="" />
                                <input type="hidden" name="place" id="place" value="" />
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                <div id="map"></div>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">
                                Area
                            </label>
                            <div class="col-sm-9">
                               <input id="area" name="area" value="" class="form-control" type="text" placeholder="Area"/>
                            </div>
                        </div> -->


                        <div class="form-group">
                            <label class="col-sm-2" for="form-field-1">Your Map Image</label>
                            <em style="margin-left:2%;color:#999;">Note: Image maximum size (Width:4000px & height:4000px)</em><br/>
                            <div class="col-sm-9">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail"></div>
                                    <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                        <input type="file" name="images">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                            </label>
                            <div class="col-md-4">
                                <button class="btn btn-yellow btn-block" type="submit">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
					
					<div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
						<div id="viewport" class="iphone">
								<iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
						</div>
						<img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
						<div id="viewport_images" class="iphone-l" style="display:none;">
							   <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"/></a>
						</div>
					</div>
						
                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>

 

<style>
      #map {
        height: 100%;
      }
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

    </style>

<script type="text/javascript">

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 51.5000, lng: 0.1167},
    zoom: 4,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
    $('#zoom_level').val(map.zoom);
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
      // Create a marker for each place.
      $('#lat_long').val(place.geometry.location);
      $('#place').val(place.name);
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}
$('#include-map').change(function () {
 if(!$( "#include-map" ).prop("checked"))
 {
   $('#maps').hide();
 }
 else
 {
    $('#maps').show();
 }
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxNvehdm6UpsFMELIePDjwgXcCxxfQ_Sg&libraries=places&callback=initAutocomplete" async defer></script>