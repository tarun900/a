<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Edit <span class="text-bold">Permision</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
			
			               <?php if(!empty($this->session->flashdata('error'))){ ?>
				                <div class="errorHandler alert alert-danger no-display" style="display: block;">
				                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
				                </div>
                           <?php } ?>

                            <form action="<?php echo base_url();?>Developmentusers/permisionedit/<?php echo $event_id;?>/<?php echo $permision_data[0]['id']; ?>" method="POST"  role="form" id="myform" enctype="multipart/form-data" novalidate="novalidate" >
						<div class="row">
                                
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Select Module <span class="symbol required"></span>
									</label>
                                                                    
                                    <select class="form-control required module_class" name="menu_id" event_id="<?php echo $event_id;?>">
                                        <?php foreach($menu as $key=>$value) { 

                                         if($value['id']==$permision_data[0]['menu_id']){
                                         	$selected="selected";
                                         }else{
                                         	$selected="";
                                         }

                                        ?>
                                    	<option value="<?php echo $value['id'];?>" <?php echo $selected;?>><?php echo $value['menuname'];?></option>
                                    	<?php } ?>
                                    </select>
								</div>

                                <div class="form-group">
									<label class="control-label">
										Select Functionality <span class="symbol required"></span>
									</label>
                                    <?php

                                     $selectedFunction=explode(",",$permision_data[0]['functionality_id']);
                                        // echo "<pre>"; print_r($selectedFunction); exit();
                                    ?>                                 
                                    <select class="form-control required functionality_class select2class" name="functionality_id[]" multiple="multiple">
                                        <?php 
                                         
                                        foreach($functionalitydata as $key2=>$value2) { 
                                        
                                         if(in_array($value2['id'] ,$selectedFunction)){
                                         	$selected2="selected";
                                         }else{
                                         	$selected2="";
                                         }

                                        ?>
                                    	<option value="<?php echo $value2['id'];?>" <?php echo $selected2;?>><?php echo $value2['functinality_name'];?></option>
                                    	<?php } ?>
                                    </select>
								</div>

                               <div class="form-group">
									<label class="control-label">
										Select Role <span class="symbol required"></span>
									</label>
                                                                    
                                    <select class="form-control required select2class" name="role_id" >
                                        <?php foreach($roledata as $key1=>$value1) { 
                                        
                                         if($value1['Id']==$permision_data[0]['role_id']){
                                         	$selected1="selected";
                                         }else{
                                         	$selected1="";
                                         }

                                        ?>
                                    	<option value="<?php echo $value1['Id'];?>" <?php echo $selected1;?>><?php echo $value1['Name'];?></option>
                                    	<?php } ?>
                                    </select>
								</div>
								                         
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-green  submit_button" type="submit">
                                                    Update	 <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                            <a href="<?php echo base_url();?>Developmentusers/permisionlist/<?php echo $event_id;?>" class="btn btn-green">
                                               Back <i class="fa fa-arrow-circle-left"></i>
                                            </a>
                                    </div>
                                </div>                            
							</div>
                                   						
						</div>
					</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->

<!--  SELECT2 Start -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">


     $('.select2class').select2();

     $('.select2-search input').remove();


</script>
<!--  SELECT2 End -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

 $(".submit_button").click(function(){
        $( "#myform" ).validate();
    });
});



$(document).on('change', '.module_class', function() {

  var menu_id=$(this).val();
  var event_id=$(this).attr('event_id');
  $.ajax({ 
      url: "<?php echo base_url();?>Developmentusers/getfunctionalityByAjax/"+event_id, 
      method: 'post', 
      data:{'menu_id':menu_id,'event_id':event_id},
      dataType: "JSON",
      success: function(result){ 
  	       if(result!='0'){
 
               var option='';

               $.each(result, function(i, item) {
			    option+="<option value='"+item.id+"'>"+item.functinality_name+"</option>";
			  });

             $(".functionality_class").html(option);

  	       }else{

  	       	 var option='<option value=""></option>';
  	       	  $(".functionality_class").html(option);
  	       }
  }}); 
});


</script>


