
<?php $acc_name=$this->session->userdata('acc_name');
$user = $this->session->userdata('current_user');?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white" id="survey">
            <div class="panel-body" style="padding: 0px;">
                
                 <?php if(!empty($this->session->flashdata('success'))){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <?php if(!empty($this->session->flashdata('error'))){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>


                <?php if($this->session->flashdata('survey_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('survey_data'); ?>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('survey_data_error')){ ?>
                <div class="errorHandler alert alert-danger no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('survey_data_error'); ?>
                </div>
                <?php } ?>
                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <li class="active" >
                            <a href="#survey_list" data-toggle="tab">
                                Users
                            </a>
                        </li>
                        <li class="" style="display: none;">
                            <a id="" href="#rep" data-toggle="tab">
                                Representatives
                            </a>
                        </li>
                        <li style="display: none;">
                        	<a id="" href="#my_leads" data-toggle="tab"> 
                        	My Leads
                        	</a>
                        </li>
                        <li class="" style="display: none;">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View Event
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    	<div class="row" style="margin-right: 15px;">
	                    	<a style="margin-top: 0.5%;margin-bottom:0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Developmentusers/add/<?php echo $event_id; ?>"><i class="fa fa-plus"></i> Add new User</a>

                            <a style="margin-top: 0.5%;margin-bottom:0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Developmentusers/permisionlist/<?php echo $event_id; ?>"><i class="fa fa-lock"></i> Module Permission</a>

                            <a style="margin-top: 0.5%;margin-bottom:0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>Developmentusers/functionalitylist/<?php echo $event_id; ?>"><i class="fa fa-bars"></i> Module functionality</a>

	                		<!-- <a style="margin-top: 0.5%;margin-right: 5px;margin-bottom:0.5%;" class="btn btn-primary pull-right" href="<?php echo base_url(); ?>exibitor_survey/reorder_question/<?php echo @$event_id; ?>"> Reorder Questions</a> -->
                		</div>
                    <div class="tab-pane fade active in" id="survey_list" >
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>last Name</th>
                                        <th>Email</th>
                                        <th>Assign Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($userdata as $key => $value) {

                                     $rolearray=array();
                                      if(isset($value['role_id']) && !empty($value['role_id'])){
                                           $newArry=explode(",",$value['role_id']);
                                           for ($i=0; $i <count($newArry) ; $i++) { 
                                               $newvar=$newArry[$i].'::'.$value['id'];
                                               array_push($rolearray,$newvar);
                                           }

                                      }


                                     ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td>
                                            <?php echo $value['fname']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['lname']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['email']; ?>
                                        </td>
                                        <td>
                                        <form method="POST" action="<?php echo base_url(); ?>Developmentusers/roleassignment">
                                        <input type="hidden" name="event_id" value="<?php echo $event_id;?>">
                                        <input type="hidden" name="id[]" value="<?php echo $value['id'];?>">
                                       
                                           
                                            <select class="assign_role js-example-basic-single" name="role[]" multiple="multiple" >
                                                <option value="">--select Role --</option>
                                                <?php foreach($roledata as $key1=>$value1){

                                                  if (in_array($value1['Id'].'::'.$value['id'], $rolearray))
                                                  {
                                                    $selected="selected";
                                                  }
                                                else
                                                  {
                                                    $selected="";
                                                    
                                                  }


                                              ?>
                                                  
                                                  <option value="<?php echo $value1['Id'].'::'.$value['id'];?>" <?php echo $selected;?>><?php echo $value1['Name'];?></option>
                                                 
                                                <?php }?>
                                            </select>
                                           
                                         <input type="submit"  name="submit_button" value="Assign" class="btn btn-xs btn-red tooltips">
                                           </form> 
                                           
                                        </td>
                                        <td>

                                            <a href="<?php echo base_url() ?>Developmentusers/edit/<?php echo $event_id.'/'.$value['id']; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" style="margin-top: 0;"  onclick="delete_user(<?php echo $value['id'].','.$event_id; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade  id="rep">
                    	<?php if($user[0]->no_of_reps > count($representatives)){?>
                      <a style="top: -57px;" class="btn btn-primary list_page_btn add_role"><i class="fa fa-plus"></i> Add New Rep</a>
                      <?php }?>
                       <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($representatives as $key => $value){ ?>
                                        <tr>
                                            <td><?=++$j;?></td>
                                            <td><?=$value['Firstname'].' '.$value['Lastname']?></td>
                                            <td><?=$value['Email']?></td>
                                            <td><?=$value['Title']?></td>
                                            <td><?=$value['Company_name']?></td>
                                            <td><?php if($value['status']=='1'){ ?>
                                                    <label class="label label-green">Active</label>
                                                <?php }else{ ?>
                                                    <label class="label label-danger">Disabled</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a title="Edit" class="btn btn-green edit_rep" data-id="<?php echo $value['Id'] ?>"  href="javascript:;"><i class="fa fa-edit"></i></a>
                                                <a title="Delete" style="margin-top: 0;" class="btn btn-red" href="<?php echo base_url().'Lead_representative_admin/delete_rep/'.$this->uri->segment(3).'/'.$value['Id'] ?>"><i class="fa fa-trash-o"></i></a>
                                                <?php if($value['status']=='1'){ ?>
                                                 <a title="Disable" style="margin-top: 0;" class="btn btn-red" href="<?php echo base_url().'Lead_representative_admin/change_rep_status/'.$this->uri->segment(3).'/'.$value['Id'].'/0' ?>"><i class="fa fa-times"></i></a> 
                                                 <?php }else{ ?>
                                                    <a title="Enable" style="margin-top: 0;" class="btn btn-success" href="<?php echo base_url().'Lead_representative_admin/change_rep_status/'.$this->uri->segment(3).'/'.$value['Id'].'/1' ?>"><i class="fa fa-check"></i></a> 
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="my_leads">
                    	<div class="row" style="margin: 5px">
                    		<div class="btn-group pull-right">
				              <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
				                  Export Lead Data <i class="fa fa-arrow-down"></i>
				              </button>
				              <ul class="dropdown-menu dropdown-light pull-right">
				                <li>
				                  <a href="<?php echo base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/export_my_lead'; ?>">
				                    Download Directly
				                  </a>
				                </li>
				                <li>
				                  <a href="<?php echo base_url().'Lead_retrieval/'.$acc_name.'/'.$Subdomain.'/email_my_lead'; ?>">
				                    Email My Leads
				                  </a>
				                </li>
				              </ul>
				            </div>
                    	</div>
                    	<div class="table-responsive"  style="overflow-x:scroll;">
                            <table class="table table-striped table-bordered table-hover table-full-width add-sesstion-box" id="sample_3">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        <th>Salutation</th>
                                        <th>Country</th>
                                        <th>Phone</th>
                                        <th>Badge Number</th>
                                        <th>Lead Rep</th>
                                        <th>Lead Scan Date</th>
                                        <?php
                                        $event_id = $this->uri->segment(3);
                                        if($event_id != '634' ) :
                                            if($event_id != '1012'):
                                         foreach ($custom_column as $key => $value) { ?>
                                        <th><?php echo ucfirst($value['column_name']); ?></th>
                                        <?php } endif;endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($my_lead as $key => $value){ ?>
                                        <tr>
                                            <td><?=++$k;?></td>
                                            <td><?=$value['Firstname'].' '.$value['Lastname']?></td>
                                            <td><?=$value['Email']?></td>
                                            <td><?=$value['Title']?></td>
                                            <td><?=$value['Company_name']?></td>
                                            <td><?=$value['salutation']?></td>
                                            <td><?=$value['country']?></td>
                                            <td><?=$value['mobile']?></td>
                                            <td><?=$value['badgeNumber']?></td>
                                            <td><?=$value['lead_rep']?></td>
                                            <td><?=$value['created_date']?></td>
                                            <?php 
                                            if($event_id != '634') :
                                                if($event_id != '1012'):
                                            $custom_data=json_decode($value['custom_column_data'],true); foreach ($custom_column as $key => $value) { ?>
                                            <td><?php echo $custom_data[$value['column_name']]; ?></td>
                                            <?php } endif;endif;?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                            <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                            <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo @$acc_name.'/'.@$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add-role" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Lead Rep</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_representative_admin/store/<?php echo $this->uri->segment(3); ?>">
            	<div class="row">
            		<div class="col-sm-3 form-group">
            			<label>
            				First name
            			</label>
            		</div>
            		<div class="col-sm-9 form-group">
            			<input type="text" name="Firstname"  class="form-control">
            		</div>
            		<div class="col-sm-3 form-group">
            			<label>
            				Last name
            			</label>
            		</div>
            		<div class="col-sm-9 form-group">
            			<input type="text" name="Lastname"  class="form-control">
            		</div>
            		<div class="col-sm-3 form-group">
            			<label>
            				Email Address
            			</label>
            		</div>
            		<div class="col-sm-9 form-group">
            			<input type="text" name="Email"  class="form-control">
            		</div>
            		<div class="col-sm-3 form-group">
            			<label>
            				Title
            			</label>
            		</div>
            		<div class="col-sm-9 form-group">
            			<input type="text" name="Title"  class="form-control">
            		</div>
            		<div class="col-sm-3 form-group">
            			<label>
            				Company
            			</label>
            		</div>
            		<div class="col-sm-9 form-group">
            			<input type="text" name="Company_name"  class="form-control">
            		</div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Password
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Password" class="form-control">
                    </div>
            	</div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-lead-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Lead Rep</h4>
          </div>

          <div class="modal-body">
            <form method="post" action="<?php echo base_url(); ?>Lead_representative_admin/edit_user/<?php echo $this->uri->segment(3); ?>">
                <div class="row">
                    <div class="col-sm-3 form-group">
                        <label>
                            First name
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Firstname" id="m_fname" class="form-control">
                        <input type="hidden" name="user_id" id="m_user_id" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Last name
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Lastname" id="m_lname" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Email Address
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Email" id="m_email" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Title
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Title" id="m_title" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Company
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Company_name" id="m_company" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>
                            Password
                        </label>
                    </div>
                    <div class="col-sm-9 form-group">
                        <input type="text" name="Password" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
            </div>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

<!--  SELECT2 Start -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">


     $('.js-example-basic-single').select2();

     $('.select2-search input').remove();

     $(".select2-container").css("width", "400px");
     


</script>
<!--  SELECT2 End -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){

$('#sample_1').DataTable();
    
 var dump_role_id=[];
 $(document).on('click', '.assign_role1', function() {
    var ids=$(this).val();
    if(ids==''){
        alert('select the role for assign.');
    }else{
       
       // var c=ids.split("::");
       dump_role_id.push(ids);
       
       alert(dump_role_id);
       if(dump_role_id==""){
        alert('empty');
       }else{

        alert('no empty');
       }
    // $.ajax({
    //         type: "POST",
    //         url: "<?php //echo base_url();?>/",
    //         data: {'user_id':user_id,'role_id':role_id},
    //         // dataType: "JSON",
    //         success: function(data) {
    //          $("#message").html(data);
    //         $("p").addClass("alert alert-success");
    //         },
    //         error: function(err) {
    //         alert(err);
    //         }
    //     });
      }
});
});
</script>


<script type="text/javascript">
function delete_user(id,$event_id)
{   
    if(confirm("Are you sure to delete this?"))
    {
       // $.ajax({
       //      type: "POST",
       //      url: "<?php echo base_url();?>Developmentusers/softdelete/1511",
       //      data: {'id':id},
       //      // dataType: "JSON",
       //      success: function(data) {
       //         // location.reload();
       //      }
       //  });

        // window.location.href="<?php base_url().'Developmentusers/softdelete/1511/'; ?>"+id;
        window.location.href="<?php echo base_url().'Developmentusers/softdelete/'.$event_id.'/'; ?>"+id;
    }
}
$(".add_role").click(function()
{
    $('#add-role').modal('show');
});

$(".edit_rep").click(function(){
    var id = $(this).data('id');
    $("#m_user_id").val(id);
     $.ajax({
          url : "<?php echo base_url().'Lead_representative_admin/get_user_data/'.$this->uri->segment(3).'/'; ?>"+id,
          type: "GET",
          success : function(result)
          {
            result = JSON.parse(result);
            $("#m_fname").val(result.Firstname);
            $("#m_lname").val(result.Lastname);
            $("#m_email").val(result.Email);
            $("#m_title").val(result.Title);
            $("#m_company").val(result.Company_name);
            $("#m_email").attr('disabled','disabled');
            $('#edit-lead-user').modal('show');
          }
        });  
    
})
</script>


