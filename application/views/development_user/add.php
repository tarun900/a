<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">user</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">


                        <div class="errorHandler alert alert-success no-display success_alert" style="display: none;">
		                   <span>Csv data uploaded Sucessfully.</span>
		                </div>
			            <div class="errorHandler alert alert-danger no-display error_alert" style="display:none;">
				             <span>!!Opps, Some Error Occure, try again..</span>
				        </div>

				        <div class="errorHandler alert alert-danger no-display invalid_alert" style="display:none;">
				             <span>Must be upload csv file.</span>
				        </div>


                           <input type="file" id="imgupload" style="display:none"/> 
                           
                            <button class="btn btn-green pull-right" id="OpenImgUpload" type="submit">
                             CSV Upload <i class="fa fa-arrow-circle-up"></i>
                            </button>

			               <?php if(!empty($this->session->flashdata('error'))){ ?>
				                <div class="errorHandler alert alert-danger no-display" style="display: block;">
				                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
				                </div>
                           <?php } ?>

                            <form action="<?php echo base_url();?>Developmentusers/add/<?php echo $event_id;?>" method="POST"  role="form" id="myform" enctype="multipart/form-data" novalidate="novalidate" >
						<div class="row">
                                
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										First Name <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="fname">
                                                                    <input type="hidden" placeholder="idval" class="form-control" id="idval" name="idval" value="">
								</div>
                               <div class="form-group">
									<label class="control-label">
										Last Name <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Last Name" class="form-control required" id="Company_name" name="lname" >
								</div>
								<div class="form-group">
									<label class="control-label">
										Email <span class="symbol required"></span>
									</label>
                                                                    <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
								</div>
                                                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="btn btn-green  submit_button" type="submit">
                                                        Add <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                             <a href="<?php echo base_url();?>Developmentusers/index/<?php echo $event_id;?>" class="btn btn-green">
                                               Back <i class="fa fa-arrow-circle-left"></i>
                                            </a>
                                        </div>
                                    </div>                            
							</div>
                                   						
						</div>
					</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

 $(".submit_button").click(function(){
        $( "#myform" ).validate();
    });
});

$('#OpenImgUpload').click(function(){ $('#imgupload').trigger('click'); });

$("#imgupload").on("change", function() {
    var file_data = $("#imgupload").prop("files")[0];   
    var form_data = new FormData();
    form_data.append("file", file_data);
    // alert(form_data);
    $.ajax({
        url: "<?php echo base_url();?>Developmentusers/add_multiple/<?php echo $event_id;?>",
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        dataType: "text",
        success: function(result){
           
            if(result=="success"){
                $(".success_alert").show().fadeOut(5000);
                 history.go(-1);
            }else if(result=="invalid"){
                 $(".invalid_alert").show().fadeOut(5000);   
            }else{
                $(".error_alert").show().fadeOut(5000); 
            }

        }
    });
});

</script>


