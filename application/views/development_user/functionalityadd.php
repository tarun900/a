<!-- start: PAGE CONTENT -->
<div class="row">
	<div class="col-sm-12">
		<!-- start: TEXT FIELDS PANEL -->
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Add <span class="text-bold">Functionality</span></h4>
				<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="panel-body">
			
			               <?php if(!empty($this->session->flashdata('error'))){ ?>
				                <div class="errorHandler alert alert-danger no-display" style="display: block;">
				                    <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('error'); ?>
				                </div>
                           <?php } ?>

                            <form action="<?php echo base_url();?>Developmentusers/functionalityadd/<?php echo $event_id;?>" method="POST"  role="form" id="myform" enctype="multipart/form-data" novalidate="novalidate" >
						<div class="row">
                                
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Select Module <span class="symbol required"></span>
									</label>
                                                                    
                                    <select class="form-control required" name="menu_id" >
                                        <?php foreach($menu as $key=>$value) { ?>
                                    	<option value="<?php echo $value['id'];?>"><?php echo $value['menuname'];?></option>
                                    	<?php } ?>
                                    </select>
								</div>
                               
								<div class="form-group">
									<label class="control-label">
										Functionality Name <span class="symbol required"></span>
									</label>
                                                                    <input type="text" placeholder="Enter name" class="form-control required" id="functinality_name" name="functionality_name">
                                                                 
								</div>
								                         
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-green submit_button" type="submit">
                                                    Add <i class="fa fa-arrow-circle-right"></i>
                                         </button>

                                          <a href="<?php echo base_url();?>Developmentusers/functionalitylist/<?php echo $event_id;?>" class="btn btn-green">
                                               Back <i class="fa fa-arrow-circle-left"></i>
                                         </a>

                                    </div>
                                </div>                            
							</div>
                                   						
						</div>
					</form>
			</div>
		</div>
		<!-- end: TEXT FIELDS PANEL -->
	</div>
</div>
<!-- end: PAGE CONTENT-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

 $(".submit_button").click(function(){
        $( "#myform" ).validate();
    });
});
</script>


