<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Add <span class="text-bold">User</span></h4>
        <div class="panel-tools">
          <a class="btn btn-xs btn-link panel-close" href="#">
          <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
          <form action="" method="POST"  role="form" id="form" enctype="multipart/form-data" novalidate="novalidate">
            <div class="row">
              <?php if($this->session->flashdata('site_setting_data') == "Updated"){ ?>
              <div class="errorHandler alert alert-success no-display" style="display: block;">
              <i class="fa fa-remove-sign"></i> Updated Successfully.
              </div>
              <?php } ?>
              <div class="col-md-6">

                <div class="form-group">
                   <label class="control-label">First Name <span class="symbol required"></span></span>
                   </label>
                    <input type="text" placeholder="First Name" class="form-control required" id="Firstname" name="Firstname">
                </div>

                <div class="form-group">
                   <label class="control-label">Last Name <span class="symbol required"></span> </span>
                   </label>
                    <input type="text" placeholder="Last Name" class="form-control required" id="Lastname" name="Lastname">
                </div>
                
                <div class="form-group">
                  <label class="control-label">Email <span class="symbol required"></span></label>
                  <input type="email" placeholder="Email" class="form-control required" id="email" name="email" onblur="checkemail();">
                  <input type="hidden" class="form-control" id="event_id" name="event_id" value="<?php echo $this->uri->segment(3); ?>">
                </div>

                <div class="row">
                   <div class="form-group col-md-6">
                        <label class="control-label">Password <span class="symbol required"></span></label>
                        <input type="password" class="form-control" id="password" name="password">
                   </div>
                   <div class="form-group col-md-6">
                         <label class="control-label">Confirm Password <span class="symbol required"></span></label>
                         <input type="password" class="form-control" id="password_again" name="password_again">
                   </div>
                </div>

                <div class="form-group">
                  <label for="form-field-select-1">Status <span class="symbol required"></span></label>
                  <select id="form-field-select-1" class="form-control" name="Active">
                      <option value="1">Active</option>                                                                                
                      <option value="0">Inactive</option>
                  </select>                                                                
                </div>

                <div class="form-group">
                    <label for="form-field-select-1">User Role <span class="symbol required"></span></label>
                     <select id="form-field-select-1 Role_id" class="form-control" name="Role_id">
                        <option value="">Select Role...</option>   

                      <?php foreach ($roles as $key => $value)  {
                            if($value['Id']!='3' && $value['Id']!='4' && $value['Id']!='5' && $value['Id']!='6' && $value['Id']!='7') {
                            $op_val=preg_replace('/\s+/', '',$value['Name']);
                            ?><option value="<?php echo $value['Id'];?>"><?php echo $op_val;?></option>
                     <?php  } } ?>   
                    </select>                                                                
                </div>

                <?php 
                  $user = $this->session->userdata('current_user');
                  $logged_in_user_id = $user[0]->Id; 
                ?>
                  <input type="hidden" id="Organisor_id" name="Organisor_id" value="<?php echo $logged_in_user_id; ?>" class="form-control">
                  <div class="row">
                      <div class="col-md-4">
                         <button class="btn btn-green btn-block" type="submit">Add <i class="fa fa-arrow-circle-right"></i></button>
                      </div>
                  </div>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>