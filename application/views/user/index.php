<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body" style="padding: 0px;">
               
               <?php if(!empty($user_list)) { ?>
                    <a style="top: 7px;right: 17%;" class="btn btn-primary list_page_btn" href="<?php echo base_url(); ?>User/exist/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-plus"></i> Add Existing User</a>
                <?php } ?>
                <?php //if($user->Role_name=='Client'){
                     $url=base_url().'User/add/';
                 ?>
                <a class="btn btn-primary list_page_btn" href="<?php echo $url.$event_id11 ?> "><i class="fa fa-plus"></i> Add User</a>
                <?php //} ?>
                   
                <?php if($this->session->flashdata('user_data')){ ?>
                <div class="errorHandler alert alert-success no-display" style="display: block;">
                    <i class="fa fa-remove-sign"></i> User <?php echo $this->session->flashdata('user_data'); ?> Successfully.
                </div>
                <?php } ?>

                <div class="tabbable">
                    <ul id="myTab2" class="nav nav-tabs">
                        <?php if($this->uri->segment(4) =='1') { ?>
                        <li class="">
                        <?php } else { ?>
                        <li class="active">
                        <?php } ?>
                            <a href="#users_list" data-toggle="tab">
                                Users List
                            </a>
                        </li>
                       
                        <li class="">
                            <a id="view_events1" href="#view_events" data-toggle="tab">
                                View App
                            </a>
                        </li>
                        
                       
                    </ul>
                    <div class="tab-content">
                        <?php if($this->uri->segment(4) !='1') { ?>
                        <div class="tab-pane fade active in" id="users_list">
                        <?php } else { ?>
                        <div class="tab-pane fade" id="users_list">
                        <?php } ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th class="hidden-xs">Email</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                         </tr>
                                    </thead>
                      <tbody>
                            <?php for($i=0;$i<count($users);$i++) { ?>
                            <tr>
                                <td><?php echo $i+1; ?></td>
                                <td><a href="<?php echo base_url(); ?>Profile/update/<?php echo $users[$i]['Id']; ?>/<?php echo $event_id; ?>"><?php echo $users[$i]['Firstname']; ?></a></td>
                                <td class="hidden-xs"><?php echo $users[$i]['Email']; ?></td>
                                <td>
                                    <span class="label label-sm 
                                        <?php if($users[$i]['Active']=='1')
                                            { 
                                            ?> 
                                              label-success 
                                            <?php
                                            }
                                            else
                                            {
                                                ?> label-danger 
                                            <?php
                                            } ?>">
                                            <?php if($users[$i]['Active']=='1')
                                                { 
                                                echo "Active"; 
                                                
                                                }
                                                else 
                                                { 
                                                    echo "Inactive";
                                                } 
                                        ?>
                                    </span>
                                </td>
                                <td>
                                    <?php //if($user->Role_name=='Client'){ ?>
                                    <a href="<?php echo base_url(); ?>Profile/update/<?php echo $users[$i]['Id']; ?>/<?php echo $event_id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                                    
                                    <?php //} ?>
                                    <!-- <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="View Event"><i class="fa fa-share"></i></a> -->
                                    <a href="javascript:;" onclick="delete_user(<?php echo $users[$i]['Id']; ?>,<?php echo $event_id11; ?>)" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
                        <?php if($this->uri->segment(4) =='1') { ?>
                        <div class="tab-pane fade active in" id="user_attendee">
                        <?php } else { ?>
                        <div class="tab-pane fade" id="user_attendee">
                        <?php } ?>
                            <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th>
                                                <div class="checkbox-table">
                                                    <label>
                                                        <input type="checkbox" class="flat-grey selectall">
                                                    </label>
                                                </div>
                                                </th>
                                                <th>Sr. No.</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < count($attendee_user_list); $i++) {
                                                ?>
                                                <tr>
                                                    <td>
                                                       
                                                        <div class="checkbox-table">
                                                            <label>
                                                                <input type="checkbox" name="attendee[]"  value="<?php echo $attendee_user_list[$i]['User_id']; ?>" class="flat-grey foocheck">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $i + 1; ?></td>
                                                    <td><?php echo $attendee_user_list[$i]['Email']; ?></td>
                                                    <td><?php echo $attendee_user_list[$i]['Firstname']." ".$attendee_user_list[$i]['Lastname']; ?></td>
                                                    
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btn-block" type="submit">
                                            Move to attendee <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="advertising">
                            <div class="row padding-15">
                                <form class="" id="form" name="form" method="POST" action="<?php echo base_url(); ?>Menu/index" enctype="multipart/form-data">
                                    <div class="col-md-12 alert alert-info">
                                        <h5 class="space15">Menu Title & Image</h5>
                                        <div class="form-group">
                                            <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?php echo $title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
                                        </div>
                                        <div class="form-group">
                                             <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail"></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div class="user-edit-image-buttons">
                                                   <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                                                        <input type="file" name="Images[]">
                                                   </span>
                                                   <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                                                        <i class="fa fa-times"></i> Remove
                                                   </a>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" class="square-orange" name="is_feture_product" id="is_feture_product" <?php if($is_feture_product=="1") { ?> checked="checked" <?php } ?>>
                                                Create Home Tab
                                            </label>
                                            <?php if($img !='') { ?>
                                            <img style="width:130px;" class="pull-right" src="<?php echo base_url(); ?>assets/user_files/<?php echo $img; ?>">
                                            <?php } ?>
                                        </div>
                            
                                        <div class="form-group image_view:">
                                            <label class="col-sm-2" style="padding-left:0px;padding-right:0px;" for="form-field-1">Image view:</label>
                                            <div class="col-sm-5">
                                                <select id="img_view" class="form-control" name="img_view">
                                                        <option value="0" <?php if($img_view =='0') { ?> selected <?php } ?>>Square</option>
                                                        <option value="1" <?php if($img_view =='1') { ?> selected <?php } ?>>Round</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe1" name="displayframe1" height="480" width="320" src="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    <!-- <div class="demo-controls" style="width:30%;">
                        <div class="device-list" id="size-select">
                            <a class="device-list phone current" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Phone']); return false;">Phone</a>
                            <a class="device-list tablet" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Tablet']); return false;">Tablet</a>
                            <a class="device-list laptop" onClick="_gaq.push(['_trackEvent', 'SampleApp', 'Selector', 'Laptop']); return false;">Laptop</a>
                        </div>
                    </div> -->
                    <!-- <div class="light intro">
                        <div class="row">
                            <section id="demo">
                                <figure id="devicePreview" class="phone">
                                    <iframe src="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>" scrolling="auto" frameborder="0"></iframe>
                                    <a class="laptopLink" target="_blank" href="<?php echo base_url(); ?>Events/<?php echo $event['Subdomain']; ?>">
                                    <img class="laptopImage hide" style="left: 145px;top: 46px;position: absolute;width: 67.3%;height: 383px;" src="<?php echo base_url(); ?>images/event_dummy.jpg">
                                    </a>
                                </figure>
                            </section>
                        </div>
                    </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end: DYNAMIC TABLE PANEL -->
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/table-data.js"></script>
<script>
     jQuery(document).ready(function() {
          TableData.init();
     });
</script>
<script type="text/javascript">
     
$(document).on("click", (".remove_image"), function() {
            $(this).closest('.fileupload-new').slideUp("normal", function() { $(this).remove(); } )
        });
</script>

<script type="text/javascript">
    function delete_attendee(id,Event_id)
    {
        <?php $test = $this->uri->segment(3); ?>
        if (confirm("Are you sure to delete this?"))
        {
            window.location.href = "<?php echo base_url(); ?>Attendee/delete/"+id+"/"+<?php echo $test; ?>
        }
    }
    function delete_user(id,eid)
    {
        if(confirm("Are you sure to delete this?"))
        {
            window.location.href ="<?php echo base_url(); ?>User/delete/"+eid+ "/" + id;
        }
    }
</script>
<!-- end: PAGE CONTENT-->