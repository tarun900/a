<div class="error-full-page" id="access-error">
	<div class="row">
		<!-- start: 404 -->
			<div class="col-sm-12 page-error animated shake">
			<img src="<?php echo base_url(); ?>assets/images/access-logo.png" alt="access-logo">
			<div class="error-number text-azure">
				Page Access !
			</div>
			<div class="error-details col-sm-6 col-sm-offset-3">
				<!-- <p>
				Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum 
				Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum 
				Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p> -->
				<div class="button-tag">
			        <a href="<?php echo base_url(); ?>app/<?php echo $Subdomain; ?>" class="btn btn-red">
						Return Home
					</a>
			    </div>
			</div>
		</div>
	</div>
</div>