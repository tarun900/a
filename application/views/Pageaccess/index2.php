<!-- start: PAGE CONTENT -->

	<div class="row">
		<div class="col-md-12">
			<div class="page-error animated shake">
				<div class="error-number text-azure">
					Page Access
				</div>
				<div class="error-details col-sm-6 col-sm-offset-3">
					<h3>Oops! Links is expaired on this App</h3>
					<p>
						This invite link has expired Please go to <a href="<?php echo base_url().'app/'.$acc_name.'/'.$Subdomain; ?>"><?php echo base_url().'app/'.$acc_name.'/'.$Subdomain; ?></a>  to access this app with the email you were invited with and the password you set
						<br>
						<a href="<?php echo base_url(); ?>" class="btn btn-red btn-return">
							Return home
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
<!-- end: PAGE CONTENT-->