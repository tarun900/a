<!-- start: PAGE CONTENT -->
	<div class="row">
		<div class="col-md-12">
			<div class="page-error animated shake">
				<div class="error-number text-azure">
					Thank you
				</div>
                 <br/>
				<div class="error-details col-sm-6 col-sm-offset-3"> <br/><br/> <br/><h3>Thank you for your business</h3>                                      
                 	<p> We got your event, we will revert you on this soon. </p>
				</div>
			</div>
		</div>
	</div>
<!-- end: PAGE CONTENT-->