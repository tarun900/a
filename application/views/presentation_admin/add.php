<?php $acc_name=$this->session->userdata('acc_name');?>
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: TEXT FIELDS PANEL -->
        <div class="panel panel-white">

            <div class="tabbable">
                <ul id="myTab2" class="nav nav-tabs">
                    <li class="active">
                        <a href="#presentation_list" data-toggle="tab">
                            Add Presentation
                        </a>
                    </li>
                    <li class="">
                        <a id="view_events1" href="#view_events" data-toggle="tab">
                            View App
                        </a>
                    </li>
                </ul>
            </div>

            <div class="panel-body" style="padding: 0px;">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="presentation_list">
                        <form role="form" method="post" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                    
                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Start Date <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-4">
                                    <div class="input-group" style="padding-left: 1.7%;">
                                        <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                        <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                        <input type="text" name="Start_date" id="Start_date" contenteditable="false" class="form-control">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Start Time <span class="symbol required" id="showdatetime_remark"></span>
                                </label>
                                <div class="col-sm-4">
                                    <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker">
                                        <input type="text" id="Start_time" name="Start_time" class="form-control time-picker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <!-- <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;;" for="form-field-1">
                                    End Date <span class="symbol required" id="showdate_remark"></span>
                                </label>
                                <div class="col-sm-4">
                                    <div class="input-group" style="padding-left: 1.7%;">
                                        <input type="hidden" value="<?php echo $event['Start_date']; ?>" name="event_start_date" id="event_start_date">
                                        <input type="hidden" value="<?php echo $event['End_date']; ?>" name="event_end_date" id="event_end_date">
                                        <input type="text" name="End_date" id="End_date" contenteditable="false" class="form-control">
                                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div>

                            <div class="form-group">
                                 <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    End Time <span class="symbol required" id="showtime_remark"></span>
                                </label>
                                <div class="col-sm-4">
                                    <div style="padding-left: 1.8%;" class="input-group input-append bootstrap-timepicker">
                                        <input type="text" id="End_time" name="End_time" class="form-control time-picker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <span for="Summary" class="help-block errorHandler hide" id="schedul_error">This field is required.</span>
                            </div> -->

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">Status <span class="symbol required"></label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" checked="checked" name="Status">
                                        Active
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" name="Status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Presentation Heading <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Presentation Heading" id="Heading" name="Heading" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 0.8%;" for="form-field-1">
                                    Presentation Type <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Presentation Type" id="Types" name="Types" class="form-control name_group">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Thumbnail Status</label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1" checked="checked" name="Thumbnail_status">
                                        Active
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="0" name="Thumbnail_status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Auto Slide Status</label>
                                <div class="col-sm-9">
                                    <label class="radio-inline">
                                        <input type="radio" class="purple" value="1"  name="Auto_slide_status">
                                        Active
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" class="purple" checked="checked" value="0" name="Auto_slide_status">
                                        Inactive
                                    </label>    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-select-4">
                                    <!--Select Address -->Select Location
                                </label>
                                <div class="col-md-9">
                                    <select id="form-field-select-4" class="form-control" name="Address_map">
                                        <?php 
                                            echo"<option value=''>Select Address</option>";
                                            foreach ($map_list as $key => $value) 
                                            {
                                                echo'<option value="'.$value['Id'].'">'.$value["Map_title"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Select Presentation Mode <span class="symbol required"></label></label>
                                <div class="col-sm-9">
                                    <select id="presentation_file_type" class="form-control" name="presentation_file_type">
                                        <option value="">Select Presentation Mode</option>
                                        <option value="0">Images Presentation</option>
                                        <option value="1">PPT Presentation</option>
                                    </select>       
                                </div>
                            </div>

                            <div id="presentation_images" style="display:none;">
                            <div class="form-group">
                                <label class="col-sm-2" for="form-field-1">Slides</label>
                                <em class="col-sm-9" style="color:#999;">Note:To add PPT slides go to Powerpoint and click File on the top menu bar. Under File select Export to convert your slides to JPG files and then you can upload them here. After you have uploaded your files you can change the order of the slides by dragging and dropping them into the desired order.</em><br/>
                                <div class="col-sm-9" style="margin-left:17%;"  id="sortable">
                                    <input name="Images[]" type="file" id="file1" onchange="preview(this);" multiple="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Presenter Permissions </label>
                                <div class="col-sm-9">
                                    <select id="user_permissions" class="select2-container select2-container-multi form-control search-select menu-section-select" name="user_permissions[]" multiple="multiple">
                                        <?php foreach($key_people as $key => $value){ ?>
                                            <option value="<?php echo $value['uid']; ?>"><?php echo $value['Firstname'].' '.$value['Lastname'];  ?></option>
                                        <?php } ?>
                                    </select>
                                </div>    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" style="text-align:left;padding-top: 1.2%;" for="form-field-1">Add a survey </label>
                                <div class="col-sm-9">
                                    <select id="add_survey" class="select2-container select2-container-multi form-control search-select menu-section-select" name="add_survey[]" multiple="multiple">
                                        <?php foreach($Surveys as $key => $value){ ?>
                                            <option value="<?php echo $value['Id']; ?>"><?php echo $value['Question'];  ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="row" style="margin-top:30px;">
                                        <em class="col-sm-12" style="color:#999;">Note:Choose a survey and click the Add button to add to your slides above. Drag and drop your survey to position it in the desired place in the presentation.</em>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group" id="presentation_ppt" style="display:none;">
                                <label class="col-sm-2" for="form-field-1">PPT</label>
                                <div class="col-sm-9" style="margin-left:17%;"  id="sortable1">
                                    <input name="sliedfile" type="file" id="file2">
                                </div>
                            </div>

                            <?php 
                            $Event_id = $this->uri->segment(3);
                            $user = $this->session->userdata('current_user');
                            $logged_in_user_id = $user[0]->Id;
                            ?>
                            <input type="hidden" id="Event_id" name="Event_id" value="<?php echo $Event_id; ?>">
                            <input type="hidden" id="Organisor_id" name="Organisor_id" value="<?php echo $logged_in_user_id; ?>">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-field-1">
                                </label>
                                <div class="col-md-4">
                                    <button class="btn btn-yellow btn-block" type="submit">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="view_events" style="min-height:500px;padding-left:1px !important;"> 
                        <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"></iframe>
                        </div>
                        <img style="position:absolute;top:20%;left:30%;" id="loading_image" src="<?php echo base_url(); ?>assets/images/loading.gif">
                        <div id="viewport_images" class="iphone-l" style="display:none;">
                               <a target="_blank" href="<?php echo base_url(); ?>App/<?php echo $acc_name.'/'.$event['Subdomain']; ?>"><img src="<?php echo base_url(); ?>images/event_dummy.jpg"></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end: TEXT FIELDS PANEL -->
    </div>
</div>
<!-- end: PAGE CONTENT-->
<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>

<script language="javascript" type="text/javascript">
window.preview = function (input) {
    if (input.files && input.files[0]) {
        $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            $("#abcd1").hide();
            reader.onload = function (e) {
                $("#sortable").append("<img src='" + e.target.result + "' Width='100' height='100'>");
            }
        });
    }
}
</script>


<?php if($event['Start_date'] <= date('Y-m-d')) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '-0d',
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<?php } else{ ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Start_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#End_date').datepicker({
            weekStart: 1,
            startDate: '<?php echo $event['Start_date']; ?>', 
            endDate: '<?php echo $event['End_date']; ?>', 
            format: 'yyyy-mm-dd',
            autoclose: true
            });
    });
</script>

<?php } ?>