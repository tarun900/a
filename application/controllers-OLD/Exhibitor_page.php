<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Exhibitor_page extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Your Exhibitors Pages';
          $this->data['smalltitle'] = 'Manage your pages.';
          $this->data['breadcrumb'] = 'Your Exhibitors Pages';
          //$this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Exibitor_page_model');
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          
          $user = $this->session->userdata('current_user');
          
          $event_templates = $this->Event_model->view_event_by_id($user[0]->Event_id);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $roledata = $this->Event_model->getUserRole($user[0]->Event_id);
          
          if(!empty($roledata))
          {    
               $roleid = $roledata[0]->Role_id;
               $eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = "Exhibitors";

               $cnt = $this->Agenda_model->check_auth($req_mod, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1)
          {    

               $this->load->model('Exibitor_page_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               redirect('Forbidden');
          }
     }

     public function index($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $exibitor_list = $this->Exibitor_page_model->get_exibitor_list($id);
          $this->data['exibitor_list'] = $exibitor_list;

          $menudata = $this->Event_model->geteventmenu($id, 3);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $this->template->write_view('css', 'exibitor_page/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'exibitor_page/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {

               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor_page/js', $this->data, true);
          $this->template->render();
     }

     public function add($id)
     {

          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Id;

          if ($this->input->post())
          {    

               if (!empty($_FILES['company_logo']['name'][0]))
               {

                    $tempname = explode('.', $_FILES['company_logo']['name']);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];

                    $this->upload->initialize(array(
                            "file_name" => $images_file,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => 'gif|jpg|png',
                            "max_size" => '100000',
                            "max_width" => '3000',
                            "max_height" => '3000'
                    ));

                    if (!$this->upload->do_multi_upload("company_logo"))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error', $error['error']);
                    }
               }

              if (!empty($_FILES['Images']['name'][0]))
               {
                    foreach ($_FILES['Images']['name'] as $k => $v)
                    {
                         $v = str_replace(' ', '', $v);
                         if (file_exists("./assets/user_files" . $v))
                              $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         else
                              $Images[] = $v;
                    }

                    $this->upload->initialize(array(
                            "file_name" => $Images,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '2000',
                            "max_height" => '1000'
                    ));

                    if (!$this->upload->do_multi_upload("Images"))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error', "" . $error['error']);
                    }
               }

               $data['exibitor_array']['Organisor_id'] = $logged_in_user_id;
               $data['exibitor_array']['Event_id'] = $Event_id;
               $data['exibitor_array']['Heading'] = $this->input->post('Heading');
               $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
               $data['exibitor_array']['Status'] = $this->input->post('Status');
               $data['exibitor_array']['Images'] = $Images;
               $data['exibitor_array']['website_url'] = $this->input->post('website_url');
               $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
               $data['exibitor_array']['phone_number2'] = $this->input->post('phone_number2');
               $data['exibitor_array']['email_address'] = $this->input->post('email_address');
               $data['exibitor_array']['company_logo'] = $images_file;

               $exibitor_id = $this->Exibitor_page_model->add_exibitor($data);
               $this->session->set_flashdata('exibitor_data', 'Added');
               redirect("Exhibitor_page/index/" . $id);
          }

          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $exibitor_list = $this->Exibitor_page_model->get_exibitor_list($id);
               $this->data['exibitor_list'] = $exibitor_list;
          }

          $this->template->write_view('css', 'exibitor_page/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor_page/add', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor_page/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit($id,$eid)
     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Id;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $exibitor_list = $this->Exibitor_page_model->get_exibitor_list($id);
          $this->data['exibitor_list'] = $exibitor_list;

          $exibitor_by_id = $this->Exibitor_page_model->get_exibitor_by_id($id,$eid);
          $this->data['exibitor_by_id'] = $exibitor_by_id;

          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Exibitor');
               }

               if ($this->input->post())
               {
                    if (!empty($_FILES['company_logo']['name'][0]))
                    {

                         $imgname = explode('.', $_FILES['company_logo']['name']);
                         $tempname = str_replace(" ", "_", $imgname);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file = $tempname_imagename . "." . $tempname[1];

                         $this->upload->initialize(array(
                                 "file_name" => $images_file,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => 'gif|jpg|png',
                                 "max_size" => '100000',
                                 "max_width" => '3000',
                                 "max_height" => '3000'
                         ));

                         if (!$this->upload->do_multi_upload("company_logo"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', $error['error']);
                         }
                    }

                    if (!empty($_FILES['Images']['name'][0]))
                    {
                         foreach ($_FILES['Images']['name'] as $k => $v)
                         {

                              $v = str_replace(' ', '', $v);

                              if (file_exists("./assets/user_files" . $v))
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                              else
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                         $this->upload->initialize(array(
                                 "file_name" => $Images,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => '*',
                                 "max_size" => '100000',
                                 "max_width" => '2000',
                                 "max_height" => '3000'
                         ));

                         if (!$this->upload->do_multi_upload("Images"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                    }

                    $data['exibitor_array']['Id'] = $this->uri->segment(4);
                    $data['exibitor_array']['Organisor_id'] = $logged_in_user_id;
                    $data['exibitor_array']['Event_id'] = $Event_id;
                    $data['exibitor_array']['Heading'] = $this->input->post('Heading');
                    $data['exibitor_array']['Status'] = $this->input->post('Status');
                    $data['exibitor_array']['website_url'] = $this->input->post('website_url');
                    $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
                    $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
                    $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
                    $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
                    $data['exibitor_array']['phone_number2'] = $this->input->post('phone_number2');
                    $data['exibitor_array']['email_address'] = $this->input->post('email_address');
                    $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
                    $data['exibitor_array']['Images'] = $Images;
                    $data['exibitor_array']['old_images'] = $this->input->post('old_images');
                    $data['exibitor_array']['company_logo'] = $images_file;
                    $data['exibitor_array']['old_company_logo'] = $this->input->post('old_company_logo');

                    $this->Exibitor_page_model->update_exibitor($data);
                    $this->session->set_flashdata('exibitor_data', 'Updated');
                    redirect("Exhibitor_page/index/" . $id);
               }

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'exibitor_page/add_css', $this->data, true);
               $this->template->write_view('content', 'exibitor_page/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Exibitor_page_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'exibitor_page/add_js', $this->data, true);
               $this->template->render();
          }
     }

     public function delete($id, $Event_id)
     {

          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $exibitor_list = $this->Exibitor_page_model->get_exibitor_list($id);
               $this->data['exibitor_list'] = $exibitor_list[0];
          }

          $exibitor = $this->Exibitor_page_model->delete_exibitor($id,$Event_id);
          $this->session->set_flashdata('exibitor_page_data', 'Deleted');
          redirect("Exhibitor_page/index/" . $Event_id);
     }
}
