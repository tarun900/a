<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'Category';
		$this->data['smalltitle'] = 'Event Category';
		$this->data['breadcrumb'] = 'Category';
		parent::__construct($this->data);
		
		$this->load->model('Category_model');
        $this->load->model('Event_model');
	}

	public function index()                 
	{                       		         		
		$category = $this->Category_model->get_category_list();
		$this->data['Category'] = $category;
			
		$this->template->write_view('css', 'admin/css', $this->data , true);
		$this->template->write_view('content', 'category/index', $this->data , true);
		$this->template->write_view('js', 'admin/js', $this->data , true);
		$this->template->render();
	}

	public function add()
	{
            if($this->input->post())
            {
                $category = $this->Category_model->add_category($this->input->post());
                $this->session->set_flashdata('category_data', 'Added');
                redirect('Category');
            }
            
            $event = $this->Event_model->get_event_list();
            $this->data['Event'] = $event;
	
            $this->template->write_view('css', 'admin/add_css', $this->data , true);
            $this->template->write_view('content', 'category/add', $this->data , true);
            $this->template->write_view('js', 'admin/add_js', $this->data , true);
            $this->template->render();
	}
        
        public function edit($id)
        {
            if($this->input->post())
            {
                $category = $this->Category_model->update_category($this->input->post(),$id);
                $this->session->set_flashdata('category_data', 'Updated');
                redirect('Category');
            }
            
            $category = $this->Category_model->get_category_list($id);
            $this->data['Category'] = $category;
            
            $event = $this->Event_model->get_event_list();
            $this->data['Event'] = $event;
	
            $this->template->write_view('css', 'admin/add_css', $this->data , true);
            $this->template->write_view('content', 'category/edit', $this->data , true);
            $this->template->write_view('js', 'admin/add_js', $this->data , true);
            $this->template->render();
        }
        
        public function delete($id)
        {
            
            $category = $this->Category_model->delete_category($id);
            $this->session->set_flashdata('category_data', 'Deleted');
            redirect('Category');
        }
        
        public function checkcategory()
        {            
            if($this->input->post())
            {
                $category = $this->Category_model->checkcategory($this->input->post('cname'),$this->input->post('idval'));
               
                if($category)
                {
                    echo "error###Category alerady exist. Please chose another category.";
                }
                else
                {
                    echo "success###";
                }
            }
            exit;
        }
}
