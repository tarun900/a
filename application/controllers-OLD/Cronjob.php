<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class cronjob extends CI_Controller
{
     function __construct()
     {
          parent::__construct($this->data);
          ini_set('auto_detect_line_endings', true);
          $this->load->model('Event_model');
          $this->load->model('Exibitor_model');
     }
     
     /* Import Attendees Using Cron Job*/
     public function tmpAttendeesImport()
     {
          /*$importData = $this->Event_model->getCSVEventImport('attendees');
          foreach ($importData as $key => $attendeeData)
          {
               $event_data = $this->Event_model->get_all_event_data_by_event_id($attendeeData['event_id']);
               if(!empty($event_data))
               {
                    $filepath = $attendeeData['csv_url'];
                    $contents = file_get_contents($attendeeData['csv_url']);
                    if (strlen($contents))
                    {
                         $handle = fopen($filepath, "r");
                         $datacsv = array();
                         while (($datacsv[] = fgetcsv($handle,1000,",")) !== FALSE)
                         {}
                         $csvKeys = array_shift($datacsv);
                         $csv = array();
                         foreach ($datacsv as $key => $value)
                         {
                              $csv[] = array_combine($csvKeys,$value);
                         }
                         $csvData = array_filter($csv);

                    }
                         $jdata = (array) json_decode($attendeeData['csv_mapping']); 

                         $array_keys = array_values($jdata);
                         $array_values = array_keys($jdata);
                         
                         foreach ($csvData as $key => $value)
                         {
                              ksort($value);
                              foreach ($value as $key1 => $value1)
                              {
                                   if(!in_array($key1, $array_keys))
                                   {
                                        unset($value[$key1]);
                                   }
                              }
                              $value = array_combine($array_values, array_values($value));

                              if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
                              {
                                   $Event_id = $event_data['Id'];
                                   $Organisor_id = $event_data['Organisor_id'];
                                   $Role_id = 4;

                                   $emailCheck = $this->Event_model->checkUserEmail($value['Email']);
                                   if(empty($emailCheck))
                                   {
                                        $add_user['Company_name'] = $value['Company_name'];
                                        $add_user['Firstname'] = $value['Firstname'];
                                        $add_user['Lastname'] = $value['Lastname'];
                                        $add_user['Email'] = $value['Email'];
                                        $add_user['Password'] = md5($value['Password']);
                                        $add_user['Salutation'] = $value['Salutation'];
                                        $add_user['Title'] = $value['Title'];
                                        $add_user['Organisor_id'] = $Organisor_id;
                                        $add_user['created_date'] = date('Y-m-d H:i:s');

                                        $User_id = $this->Event_model->addAttendeeUserOpenApi($add_user);

                                        $add_relation_event_user['Event_id'] = $Event_id;
                                        $add_relation_event_user['User_id'] = $User_id;
                                        $add_relation_event_user['Organisor_id'] = $Organisor_id;
                                        $add_relation_event_user['Role_id'] = $Role_id;

                                        $relationEventUser = $this->Event_model->addRelationEventUser($add_relation_event_user);
                                   }
                                   else
                                   {
                                        $update_user['Company_name'] = $value['Company_name'];
                                        $update_user['Firstname'] = $value['Firstname'];
                                        $update_user['Lastname'] = $value['Lastname'];
                                        $update_user['Email'] = $value['Email'];
                                        if(!empty($value['Password']))
                                        {
                                             $update_user['Password'] = md5($value['Password']);    
                                        }
                                        $update_user['Salutation'] = $value['Salutation'];
                                        $update_user['Title'] = $value['Title'];
                                        $update_user['Organisor_id'] = $Organisor_id;
                                        $update_user['updated_date'] = date('Y-m-d H:i:s');
                                        $updateUser = $this->Event_model->updateAttendeeUserOpenApi($emailCheck['Id'],$update_user);
                                   }
                              }
                         }
                    }
               }   
          }*/
          
          /*$AttendeeTMPtableData = $this->Event_model->getAttendeeTMPtable();
          $chunkData = array_chunk($AttendeeTMPtableData,100);
          foreach ($chunkData as $key => $data)
          {    
               $data_json = json_encode($data);
               $url = "https://www.allintheloop.net/aitldemo/cronjob/importAttendee";
               $ch = curl_init();
               curl_setopt($ch, CURLOPT_URL, $url);
               curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
               curl_setopt($ch, CURLOPT_POST, 1);
               curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               $response  = curl_exec($ch);
               curl_close($ch);               
          }*/

          /*$AttendeeTMPtableData = $this->Event_model->getAttendeeTMPtable();
          foreach ($AttendeeTMPtableData as $key => $value)
          {
               //ob_start();
               if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
               {
                    $Event_id = $value['event_id'];
                    $Organisor_id = $value['organisor_id'];
                    $Role_id = 4;

                    $emailCheck = $this->Event_model->checkUserEmail($value['Email']);
                    if(empty($emailCheck))
                    {
                         $add_user['Company_name'] = $value['Company_name'];
                         $add_user['Firstname'] = $value['Firstname'];
                         $add_user['Lastname'] = $value['Lastname'];
                         $add_user['Email'] = $value['Email'];
                         $add_user['Password'] = md5($value['Password']);
                         $add_user['Salutation'] = $value['Salutation'];
                         $add_user['Title'] = $value['Title'];
                         $add_user['Organisor_id'] = $Organisor_id;
                         $add_user['created_date'] = date('Y-m-d H:i:s');

                         $User_id = $this->Event_model->addAttendeeUserOpenApi($add_user);

                         $add_relation_event_user['Event_id'] = $Event_id;
                         $add_relation_event_user['User_id'] = $User_id;
                         $add_relation_event_user['Organisor_id'] = $Organisor_id;
                         $add_relation_event_user['Role_id'] = $Role_id;

                         $relationEventUser = $this->Event_model->addRelationEventUser($add_relation_event_user);
                    }
                    else
                    {
                         $update_user['Company_name'] = $value['Company_name'];
                         $update_user['Firstname'] = $value['Firstname'];
                         $update_user['Lastname'] = $value['Lastname'];
                         $update_user['Email'] = $value['Email'];
                         if(!empty($value['Password']))
                         {
                              $update_user['Password'] = md5($value['Password']);    
                         }
                         $update_user['Salutation'] = $value['Salutation'];
                         $update_user['Title'] = $value['Title'];
                         $update_user['Organisor_id'] = $Organisor_id;
                         $update_user['updated_date'] = date('Y-m-d H:i:s');
                         $updateUser = $this->Event_model->updateAttendeeUserOpenApi($emailCheck['Id'],$update_user);
                    }
               }
               $delete = $this->Event_model->deleteAttendeeTMPtable($value['id']);
               //ob_get_clean();
               //sleep(1);
          }
          echo "success";exit;*/
     }

     /*public function importAttendee()
     {
          $AttendeeTMPtableData = json_decode(file_get_contents('php://input'), true);
          foreach ($AttendeeTMPtableData as $key => $value)
          {
               if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
               {
                    $Event_id = $value['event_id'];
                    $Organisor_id = $value['organisor_id'];
                    $Role_id = 4;

                    $emailCheck = $this->Event_model->checkUserEmail($value['Email']);
                    if(empty($emailCheck))
                    {
                         $add_user['Company_name'] = $value['Company_name'];
                         $add_user['Firstname'] = $value['Firstname'];
                         $add_user['Lastname'] = $value['Lastname'];
                         $add_user['Email'] = $value['Email'];
                         $add_user['Password'] = md5($value['Password']);
                         $add_user['Salutation'] = $value['Salutation'];
                         $add_user['Title'] = $value['Title'];
                         $add_user['Organisor_id'] = $Organisor_id;
                         $add_user['created_date'] = date('Y-m-d H:i:s');

                         $User_id = $this->Event_model->addAttendeeUserOpenApi($add_user);

                         $add_relation_event_user['Event_id'] = $Event_id;
                         $add_relation_event_user['User_id'] = $User_id;
                         $add_relation_event_user['Organisor_id'] = $Organisor_id;
                         $add_relation_event_user['Role_id'] = $Role_id;

                         $relationEventUser = $this->Event_model->addRelationEventUser($add_relation_event_user);
                    }
                    else
                    {
                         $update_user['Company_name'] = $value['Company_name'];
                         $update_user['Firstname'] = $value['Firstname'];
                         $update_user['Lastname'] = $value['Lastname'];
                         $update_user['Email'] = $value['Email'];
                         if(!empty($value['Password']))
                         {
                              $update_user['Password'] = md5($value['Password']);    
                         }
                         $update_user['Salutation'] = $value['Salutation'];
                         $update_user['Title'] = $value['Title'];
                         $update_user['Organisor_id'] = $Organisor_id;
                         $update_user['updated_date'] = date('Y-m-d H:i:s');
                         $updateUser = $this->Event_model->updateAttendeeUserOpenApi($emailCheck['Id'],$update_user);
                    }
               }
          }
          return true;
     }*/
     
     /* Import Exhibitors Using Cron Job*/
     public function tmpExhibitorImport()
     {
          $importData = $this->Event_model->getCSVEventImport('exhibitors');
          foreach ($importData as $key => $exhibitorData)
          {
               $event_data = $this->Event_model->get_all_event_data_by_event_id($exhibitorData['event_id']);
               if(!empty($event_data))
               {
                    $filepath = $exhibitorData['csv_url'];
                    $contents = file_get_contents($exhibitorData['csv_url']);
                    if (strlen($contents))
                    {
                         $handle = fopen($filepath, "r");
                         $datacsv = array();
                         while (($datacsv[] = fgetcsv($handle,1000,",")) !== FALSE)
                         {}
                         $csvKeys = array_shift($datacsv);
                         $csv = array();
                         foreach ($datacsv as $key => $value)
                         {
                              $csv[] = array_combine($csvKeys,$value);
                         }
                         $csvData = array_filter($csv);
                         $jdata = (array) json_decode($exhibitorData['csv_mapping']); 

                         $array_keys = array_values($jdata);
                         $array_values = array_keys($jdata);
                         foreach ($csvData as $key => $value)
                         {
                              ksort($value);
                              foreach ($value as $key1 => $value1)
                              {
                                   if(!in_array($key1, $array_keys))
                                   {
                                        unset($value[$key1]);
                                   }
                              }
                              $value = array_combine($array_values, array_values($value));
                              
                              $flag = true;
                              if(!empty($value['Email']))
                              {
                                   if (!filter_var($value['Email'], FILTER_VALIDATE_EMAIL))
                                   {
                                        $flag = false;     
                                   }
                              }
                              if(!empty($value['Password']))
                              {
                                   if (strlen($value['Password']) < 6)
                                   {
                                        $flag = false;     
                                   }
                              }
                              if(empty($value['Firstname']) || empty($value['Lastname']) || empty($value['Heading']) || empty($value['Stand_Number']))
                              {
                                   $flag = false;   
                              }
                              if(empty($value['Email']))
                              {
                                   $domain = "@venturiapps.com";
                                   $value['Email'] = $value['Firstname'].''.$value['Lastname'].''.rand().''.$domain;
                              }
                              if($flag)
                              {
                                   $Event_id = $event_data['Id'];
                                   $Organisor_id = $event_data['Organisor_id'];
                                   
                                   $ex_user['Firstname'] = $value['Firstname'];
                                   $ex_user['Lastname'] = $value['Lastname'];
                                   $ex_user['Email'] = $value['Email'];
                                   $ex_user['Password'] = md5($value['Password']);
                                   $ex_user['Company_name'] = $value['Heading'];
                                   $ex_user['Organisor_id'] = $Organisor_id;
                                   $ex_user['Created_date'] = date('Y-m-d H:i:s');
                                   $ex_user['Active'] = '1';
                                   $ex_user['updated_date'] = date('Y-m-d H:i:s');
                                   
                                   $user_id = $this->Exibitor_model->addupdate_user_from_csv($ex_user,$Event_id);
                                   
                                   $ex_data['Organisor_id'] = $Organisor_id;
                                   $ex_data['Event_id'] = $Event_id;
                                   $ex_data['Heading'] = $value['Heading'];
                                   if(!empty($value['Exhibitor_Type']))
                                   {
                                        $et_id = $this->Exibitor_model->get_exibitor_type_id_by_type_code($value['Exhibitor_Type'],$Event_id);
                                        $ex_data['et_id'] = $et_id;
                                   }
                                   $ex_data['stand_number'] = $value['Stand_Number'];
                                   $ex_data['Short_desc'] = $value['Keywords'];
                                   $ex_data['Description'] = $value['Description'];
                                   $ex_data['website_url'] = $value['Website_Url'];
                                   $ex_data['facebook_url'] = $value['Facebook_Url'];
                                   $ex_data['twitter_url'] = $value['Twitter_Url'];
                                   $ex_data['linkedin_url'] = $value['Linkedin_Url'];
                                   $ex_data['instagram_url'] = $value['Instagram_Url'];
                                   $ex_data['youtube_url'] = $value['Youtube_Url'];
                                   $ex_data['country_id'] = $this->Exibitor_model->getCountryIdFromName($value['Country_name']);

                                   if(!empty($value['Company_Logo']))
                                   {
                                        $destination = "new_company_logo_".uniqid();
                                        $source = str_replace(' ','%20',$value['Company_Logo']);
                                        $info = getimagesize($source);
                                        $img_size = get_headers($source,'1');
                                        $img_size = $img_size['Content-Length']/1000;
                                        
                                        if ($info['mime'] == 'image/jpeg')
                                        { 
                                             $quality = ($img_size > '1000') ? '50' : '100';
                                             $logo = $destination.'.jpeg';
                                             $image = imagecreatefromjpeg($source);
                                             imagejpeg($image, "./assets/exhibitors/".$destination.'.jpeg', $quality);
                                        }
                                        elseif ($info['mime'] == 'image/gif')
                                        { 
                                             $logo = $destination.'.gif';
                                             $image = imagecreatefromgif($source);
                                             imagegif($image, "./assets/exhibitors/".$destination.'.gif');
                                        }
                                        elseif ($info['mime'] == 'image/png')
                                        {    
                                             $quality = ($img_size > '1000') ? '5' : '9';
                                             $logo = $destination.'.png';
                                             $image = imagecreatefrompng($source);
                                             $background = imagecolorallocatealpha($image,255,0,255,127);
                                             imagecolortransparent($image, $background);
                                             imagealphablending($image, false);
                                             imagesavealpha($image, true);
                                             imagepng($image, "./assets/exhibitors/".$destination.'.png',$quality);
                                        }
                                        $ex_data['company_logo'] = json_encode(array($logo));
                                   }
                                   if(!empty($value['Images']))
                                   {
                                        $img_arr = array_filter(explode(",",$value['Images']));
                                        $img_nm = array();
                                        foreach ($img_arr as $key => $value) {
                                             $images = "banner_".uniqid().'.png';
                                             $img_nm[$key] = $images;
                                             copy($value,"./assets/exhibitors/".$images);
                                        }
                                        $ex_data['Images'] = json_encode($img_nm);
                                   }
                                   $ex_data['user_id'] = $user_id;
                                   $ex_id = $this->Exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
                              }
                         }
                    }
               }
          }
          echo "success";exit;
     }

     public function saveAttendeeTMPtableData()
     {
          $importData = $this->Event_model->getCSVEventImport('attendees');
          foreach ($importData as $key => $attendeeData)
          {
               $event_data = $this->Event_model->get_all_event_data_by_event_id($attendeeData['event_id']);
               if(!empty($event_data))
               {
                    $filepath = $attendeeData['csv_url'];
                    $contents = file_get_contents($attendeeData['csv_url']);
                    if (strlen($contents))
                    {
                         $handle = fopen($filepath, "r");
                         $datacsv = array();
                         while (($datacsv[] = fgetcsv($handle,1000,",")) !== FALSE)
                         {}
                         $csvKeys = array_shift($datacsv);
                         $csv = array();
                         foreach ($datacsv as $key => $value)
                         {
                              $csv[] = array_combine($csvKeys,$value);
                         }
                         $csvData = array_filter($csv);

                         $jdata = (array) json_decode($attendeeData['csv_mapping']); 

                         $array_keys = array_values($jdata);
                         $array_values = array_keys($jdata);
                         
                         foreach ($csvData as $key => $value)
                         {
                              ksort($value);
                              foreach ($value as $key1 => $value1)
                              {
                                   if(!in_array($key1, $array_keys))
                                   {
                                        unset($value[$key1]);
                                   }
                              }
                              $value = array_combine($array_values, array_values($value));

                              if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
                              {
                                   $Event_id = $event_data['Id'];
                                   $Organisor_id = $event_data['Organisor_id'];
                                   $Role_id = 4;

                                   $emailCheck = $this->Event_model->checkUserEmail($value['Email']);
                                   if(empty($emailCheck))
                                   {
                                        $add_user['Company_name'] = $value['Company_name'];
                                        $add_user['Firstname'] = $value['Firstname'];
                                        $add_user['Lastname'] = $value['Lastname'];
                                        $add_user['Email'] = $value['Email'];
                                        $add_user['Password'] = md5($value['Password']);
                                        $add_user['Salutation'] = $value['Salutation'];
                                        $add_user['Title'] = $value['Title'];
                                        $add_user['Organisor_id'] = $Organisor_id;
                                        $add_user['created_date'] = date('Y-m-d H:i:s');

                                        $User_id = $this->Event_model->addAttendeeUserOpenApi($add_user);

                                        $add_relation_event_user['Event_id'] = $Event_id;
                                        $add_relation_event_user['User_id'] = $User_id;
                                        $add_relation_event_user['Organisor_id'] = $Organisor_id;
                                        $add_relation_event_user['Role_id'] = $Role_id;

                                        $relationEventUser = $this->Event_model->addRelationEventUser($add_relation_event_user);
                                   }
                                   else
                                   {
                                        $update_user['Company_name'] = $value['Company_name'];
                                        $update_user['Firstname'] = $value['Firstname'];
                                        $update_user['Lastname'] = $value['Lastname'];
                                        $update_user['Email'] = $value['Email'];
                                        if(!empty($value['Password']))
                                        {
                                             $update_user['Password'] = md5($value['Password']);    
                                        }
                                        $update_user['Salutation'] = $value['Salutation'];
                                        $update_user['Title'] = $value['Title'];
                                        $update_user['Organisor_id'] = $Organisor_id;
                                        $update_user['updated_date'] = date('Y-m-d H:i:s');
                                        $updateUser = $this->Event_model->updateAttendeeUserOpenApi($emailCheck['Id'],$update_user);
                                   }
                              }
                         }
                    }
               }   
          }
     }
    public function import_attendee()
    {   
        $this->load->model('Attendee_model');
        $this->load->model('Event_model');
        $tmp_attendee = $this->db->limit(500)->get('attendee_tmp')->result_array();
        $this->db->query('DELETE FROM attendee_tmp LIMIT 500');

        foreach ($tmp_attendee as $key1 => $value1)
        {   
            if(!empty($value1['Email']))
            {
               $data['firstname']=$value1['Firstname'];
               $data['lastname']=$value1['Lastname'];
               $data['Email']=$value1['Email'];
               $data['title']=$value1['Title'];
               $data['company_name']=$value1['Company_name'];
               $data['Salutation']=$value1['Salutation'];
               if(!empty($value1['Password']))
                    $data['Password']=md5(trim($value1['Password']));
               $this->Attendee_model->add_mass_attendee_without_invite_as_user($data,$value1['event_id'],$value1['organisor_id']);
               unset($data);
            } 
        }
        echo  'data added successfully<br>';
    }
    public function import_exhi()
    {     
          $this->load->model('Exibitor_model');
          $this->load->model('Event_model');
          $tmp_exhi = $this->db->limit(50)->get('exhi_tmp')->result_array();
          $this->db->query('DELETE FROM exhi_tmp LIMIT 50');

          foreach ($tmp_exhi as $key1=> $value1)
          {    
               $id = $value1['event_id'];
               updateModuleDate($eid,'speaker');
               $organisor_id = $value1['organisor_id'];
               updateModuleDate($id,'exhibitor');
               $datacsv = array_values($value1);
               if (!empty($datacsv[2]))
               {       
                    $ex_user['Firstname'] = $datacsv[0];
                    $ex_user['Lastname'] = $datacsv[1];
                    $ex_user['Email'] = $datacsv[2];
                    $ex_user['Password'] = md5($datacsv[3]);
                    $ex_user['Company_name'] = $datacsv[4];
                    $ex_user['Organisor_id'] = $organisor_id;
                    $ex_user['Created_date'] = date('Y-m-d H:i:s');
                    $ex_user['Active'] = '1';
                    $ex_user['updated_date'] = date('Y-m-d H:i:s');
                    $user_id = $this->Exibitor_model->add_user_from_csv($ex_user, $id);
                    $ex_data['Event_id'] = $id;
                    $ex_data['Heading'] = $datacsv[4];
                    $ex_data['Organisor_id'] = $organisor_id;

                    if (!empty($datacsv[5]))
                    {
                         $et_id = $this->Exibitor_model->get_exibitor_type_id_by_type_code($datacsv[5], $id);
                         $ex_data['et_id'] = $et_id;
                    }
                    $ex_data['stand_number'] = $datacsv[6];
                    $ex_data['Short_desc'] = $datacsv[7];
                    $ex_data['Description'] = htmlentities($datacsv[8]);
                    $ex_data['website_url'] = $datacsv[9];
                    $ex_data['facebook_url'] = $datacsv[10];
                    $ex_data['twitter_url'] = $datacsv[11];
                    $ex_data['linkedin_url'] = $datacsv[12];
                    $ex_data['instagram_url'] = $datacsv[13];
                    $ex_data['youtube_url'] = $datacsv[14];
                    $ex_data['country_id'] = $this->Exibitor_model->getCountryIdFromName($datacsv[17]);
                    if (!empty($datacsv[15]))
                    {
                         $destination = "new_company_logo_" . uniqid();
                         $source = str_replace(' ', '%20', $datacsv[15]);
                         $info = getimagesize($source);
                         $img_size = get_headers($source, '1');
                         $img_size = $img_size['Content-Length'] / 1000;
                         if ($info['mime'] == 'image/jpeg')
                         {
                              $quality = ($img_size > '1000') ? '50' : '100';
                              $logo = $destination . '.jpeg';
                              $image = imagecreatefromjpeg($source);
                              imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', $quality);
                         }
                         elseif ($info['mime'] == 'image/gif')
                         {
                              $logo = $destination . '.gif';
                              $image = imagecreatefromgif($source);
                              imagegif($image, "./assets/user_files/" . $destination . '.gif');
                         }
                         elseif ($info['mime'] == 'image/png')
                         {
                              $quality = ($img_size > '1000') ? '5' : '9';
                              $logo = $destination . '.png';
                              $image = imagecreatefrompng($source);
                              $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                              imagecolortransparent($image, $background);
                              imagealphablending($image, false);
                              imagesavealpha($image, true);
                              imagepng($image, "./assets/user_files/" . $destination . '.png', $quality);
                         }
                         $ex_data['company_logo'] = json_encode(array($logo));
                    }

                    if(!empty($datacsv[16]))
                    {
                         $img_arr = array_filter(explode(",", $datacsv[16]));
                         $img_nm = array();
                         foreach($img_arr as $key => $value)
                         {
                              $images = "banner_" . uniqid() . '.png';
                              $img_nm[$key] = $images;
                              copy($value, "./assets/user_files/" . $images);
                         }
                         $ex_data['Images'] = json_encode($img_nm);
                    }
                    $ex_data['user_id'] = $user_id;
                    $ex_id = $this->Exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
               }
               // $this->db->where($value1)->delete('exhi_tmp');
               unset($datacsv);
               unset($ex_data);
          }
    }
    public function test()//this function is set as crone in aother cron
    {   
        echo shell_exec('/usr/local/bin/php index.php cronjob import_attendee');
        // echo shell_exec('/usr/local/bin/php index.php cronjob import_exhi');
        // echo shell_exec('/usr/local/bin/php index.php cronjob import_agenda');
        // echo shell_exec('/usr/local/bin/php index.php cronjob import_speaker');
    }
    public function json_to_sql()
    {     
          //************Sample**********************//
          /*$attendee['Company_name'] = 'Company_name';
          $attendee['Firstname'] = 'Firstname';
          $attendee['Lastname'] = 'Lastname';
          $attendee['Email'] = 'Email';
          $attendee['Password'] = 'Password';
          $attendee['Salutation'] = 'Salutation';
          $attendee['Title'] = 'Title';*/
          
          $attendee = $this->db->select('Company_name,Firstname,Lastname,Email,Password,Salutation,Title')->get('attendee_tmp')->result_array();
          $data['result'] = 200;
          $data['type'] = 'attendee';
          $data['data'] = $attendee;
          echo json_encode($attendee);
    }
    public function json_to_sql_2()
    {     
          error_reporting(E_ALL);

          $jsonString = file_get_contents('https://www.allintheloop.net/assets/test/test_20000.json');

          $jsonDecoded = json_decode($jsonString, true);
          $csvFileName = $_SERVER['DOCUMENT_ROOT'].'/assets/test/test.csv';
           
          $fp = fopen($csvFileName, 'w');
          
          $attendee['Company_name'] = 'Company_name';
          $attendee['Firstname'] = 'Firstname';
          $attendee['Lastname'] = 'Lastname';
          $attendee['Email'] = 'Email';
          $attendee['Password'] = 'Password';
          $attendee['Salutation'] = 'Salutation';
          $attendee['Title'] = 'Title';
          fputcsv($fp, $attendee);
          foreach($jsonDecoded as $row)
          {
              fputcsv($fp, $row);
          }
            
          fclose($fp);

          $file_path =  'https://www.allintheloop.net/assets/test/test.csv';
          $this->db->query("LOAD DATA LOCAL INFILE '".$file_path."' 
                           INTO TABLE attendee_tmp 
                           FIELDS TERMINATED BY ','
                           OPTIONALLY ENCLOSED BY '\"'
                           IGNORE 1 LINES");
          lq();
    }
    public function import_data_to_temp() //Friday 04 May 2018 02:09:06 PM IST
    {
          $this->db->select('ec.*,e.Organisor_id');
          $this->db->join('event e', 'e.Id = ec.event_id');
          $this->db->where('e.End_date >=',date('Y-m-d'));
          $this->db->where('e.api_sync','1');
          $import_data = $this->db->get('event_csv_import ec')->result_array();
          foreach ($import_data as $key => $value)
          {    
               if($value['import_type'] == 'attendees')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONData($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveCSVData($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
          }
          echo "Data Imported successfully";
    }
    public function import_data_to_temp_exhi() //Wednesday 09 May 2018 11:48:31 AM IST
    {
          $this->db->select('ec.*,e.Organisor_id');
          $this->db->join('event e', 'e.Id = ec.event_id');
          $this->db->where('e.End_date >=',date('Y-m-d'));
          $this->db->where('e.api_sync','1');
          $import_data = $this->db->get('event_csv_import ec')->result_array();
          foreach ($import_data as $key => $value)
          {    
               if($value['import_type'] == 'exhibitors')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONDataExhi($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveJSONDataExhi($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
          }
          echo "Data Imported successfully";
    }

     public function csv_to_sql($eventid='259')
     {     
          $p = array(
              'Firstname' => array(
                  'field' => 'Firstname',
                  'null' => true,
              ) ,
              'Lastname' => array(
                  'field' => 'Lastname',
                  'null' => true,
              ) ,
              'Email' => array(
                  'field' => 'Email',
                  'null' => true,
              ) ,
              'Password' => array(
                  'field' => 'Password',
                  'null' => true,
              ) ,
              'ExhibitorName' => array(
                  'field' => 'ExhibitorName',
                  'null' => true,
              ) ,
              'ExhibitorType' => array(
                  'field' => 'ExhibitorType',
                  'null' => true,
              ) ,
              'StandNumber' => array(
                  'field' => 'StandNumber',
                  'null' => true,
              ) ,
              'Keywords' => array(
                  'field' => 'Keywords',
                  'null' => true,
              ) ,
              'Description' => array(
                  'field' => 'Description',
                  'null' => true,
              ) ,
              'WebsiteUrl' => array(
                  'field' => 'WebsiteUrl',
                  'null' => true,
              ) ,
              'FacebookUrl' => array(
                  'field' => 'FacebookUrl',
                  'null' => true,
              ) ,
              'TwitterUrl' => array(
                  'field' => 'TwitterUrl',
                  'null' => true,
              ) ,
              'LinkedinUrl' => array(
                  'field' => 'LinkedinUrl',
                  'null' => true,
              ) ,
              'InstagramUrl' => array(
                  'field' => 'InstagramUrl',
                  'null' => true,
              ) ,
              'YoutubeUrl' => array(
                  'field' => 'YoutubeUrl',
                  'null' => true,
              ) ,
              'CompanyLogo' => array(
                  'field' => 'CompanyLogo',
                  'null' => true,
              ) ,
              'Images' => array(
                  'field' => 'Images',
                  'null' => true,
              ) ,
              'Countryname' => array(
                  'field' => 'Countryname',
                  'null' => true,
              ) ,
          );   

          error_reporting(E_ALL);
          ini_set('display_errors','1');
          $file_path =  'http://allintheloop.net/assets/test/FHA_live.csv';
         /* $file = fopen($file_path, 'r');
          while (($line = fgetcsv($file)) !== FALSE) {
            //$line is an array of the csv elements
               // $tmp = array_combine($line, values)
            $data[] = $line;
          }
          j($data);
          fclose($file);*/
          $file_str = file_get_contents($file_path);
          preg_match_all('~(?>"[^"]*"|[^"\\r\\n]+)+~', $file_str, $xe);
          $rb = array_keys($p);
          $za = count($xe[0]);          
          $M  = ',';
          $K = array();
          foreach($xe[0] as $y => $X)
          {
               preg_match_all("~((?>\"[^\"]*\")+|[^$M]*)$M~", $X . $M, $ye);
               if (!$y && !array_diff($ye[1], $rb))
               {
                   $rb = $ye[1];
                   $za--;
               }
               else
               {    
                   $O = array();
                   foreach($ye[1] as $s => $mb) $O['"' . str_replace('"', '""', $rb[$s]) . '"'] = ($mb == "" && $p[$rb[$s]]["null"] ? "NULL" : $this->quote(str_replace('""', '"', preg_replace('~^"|"$~', '', $mb))));
                   $K[] = $O;
               }
          }
          $this->insertUpdate('fha_tmp_exhi',$K);
     }
     public function escape($a)
     {     
          $con=mysqli_connect($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);
          return mysqli_real_escape_string($con,$a);
     }
     public function quote($Q)
     {
          return ("'" .$this->escape($Q). "'");
          // return ($this->is_utf8($Q) ? "'" .$this->escape($Q). "'" : "x'" . reset(unpack('H*', $Q)) . "'");
     }

     public function is_utf8($X)
     {
         return (preg_match('~~u', $X) && !preg_match('~[\\0-\\x8\\xB\\xC\\xE-\\x1F]~', $X));
     }
     public function insertUpdate($R, $K)
     {    
          $e = array_keys(reset($K));
          $e = str_replace('"','`',implode(", ", $e));
          $Zf = "INSERT INTO `" .$R. "` (".$e.") VALUES\n";
          unset($K[0]);
          foreach($K as $O)
               $Ki[] = "(" . implode(", ", $O) . ")";

          $strq = $Zf . implode(",\n", $Ki);
          $this->db->query($strq);
          lq();
     }
     public function csv_to_sql_2($eventid='259')
     {     
          $p = array(
          'Firstname' => array(
             'field' => 'Firstname',
             'null' => true,
          ) ,
          'Lastname' => array(
             'field' => 'Lastname',
             'null' => true,
          ) ,
          'Email' => array(
             'field' => 'Email',
             'null' => true,
          ) ,
          'Password' => array(
             'field' => 'Password',
             'null' => true,
          ) ,
          'ExhibitorName' => array(
             'field' => 'ExhibitorName',
             'null' => true,
          ) ,
          'ExhibitorType' => array(
             'field' => 'ExhibitorType',
             'null' => true,
          ) ,
          'StandNumber' => array(
             'field' => 'StandNumber',
             'null' => true,
          ) ,
          'Keywords' => array(
             'field' => 'Keywords',
             'null' => true,
          ) ,
          'Description' => array(
             'field' => 'Description',
             'null' => true,
          ) ,
          'WebsiteUrl' => array(
             'field' => 'WebsiteUrl',
             'null' => true,
          ) ,
          'FacebookUrl' => array(
             'field' => 'FacebookUrl',
             'null' => true,
          ) ,
          'TwitterUrl' => array(
             'field' => 'TwitterUrl',
             'null' => true,
          ) ,
          'LinkedinUrl' => array(
             'field' => 'LinkedinUrl',
             'null' => true,
          ) ,
          'InstagramUrl' => array(
             'field' => 'InstagramUrl',
             'null' => true,
          ) ,
          'YoutubeUrl' => array(
             'field' => 'YoutubeUrl',
             'null' => true,
          ) ,
          'CompanyLogo' => array(
             'field' => 'CompanyLogo',
             'null' => true,
          ) ,
          'Images' => array(
             'field' => 'Images',
             'null' => true,
          ) ,
          'Countryname' => array(
             'field' => 'Countryname',
             'null' => true,
          ) ,
          );   

          error_reporting(E_ALL);
          ini_set('display_errors','1');
          $file_path =  'http://allintheloop.net/assets/test/FHA_live_1.csv';
          $file_str = file_get_contents($file_path);
          preg_match_all('~(?>"[^"]*"|[^"\\r\\n]+)+~', $file_str, $xe);
          $rb = array_keys($p);
          $za = count($xe[0]);          
          $M  = ',';
          $K = array();
          foreach($xe[0] as $y => $X)
          {
               preg_match_all("~((?>\"[^\"]*\")+|[^$M]*)$M~", $X . $M, $ye);
               if (!$y && !array_diff($ye[1], $rb))
               {
                   $rb = $ye[1];
                   $za--;
               }
               else
               {    
                   $O = array();
                   foreach($ye[1] as $s => $mb) $O['"' . str_replace('"', '""', $rb[$s]) . '"'] = ($mb == "" && $p[$rb[$s]]["null"] ? "NULL" : $this->quote(str_replace('""', '"', preg_replace('~^"|"$~', '', $mb))));
                   $K[] = $O;
               }
          }
          // $this->insertUpdate('fha_tmp_exhi',$K);

          // j($K);
          $Organisor_id = '1068';
          $csvFileName = $_SERVER['DOCUMENT_ROOT'] . '/assets/test/' . $eventid . '-attendees-' . $Organisor_id. '.csv';
          $fp = fopen($csvFileName, 'w');
          foreach ($K as $key => $value)
          {      
               foreach ($value as $key1 => $value1)
               {    
                    if($value1 == "''" OR $value1 == "' '" OR $value1 == "NULL")
                    {
                         $K[$key][$key1] = NULL;
                    }
                    else
                    {
                         $K[$key][$key1] = trim(str_replace(',','\,',$value1),"'");
                         /*if($key1 == '"Description"')
                         {
                              $K[$key][$key1] = htmlentities($K[$key][$key1]);
                         }*/
                    }
               }
          }
          foreach($K as $row)
          {   
            fputcsv($fp, $row);
          }
          fclose($fp);
          $this->db->query("LOAD DATA LOCAL INFILE '".$csvFileName."' 
                 INTO TABLE fha_tmp_exhi 
                 CHARACTER SET utf8
                 FIELDS TERMINATED BY ','
                 OPTIONALLY ENCLOSED BY '\"'
                 ESCAPED BY '\\\'
                 IGNORE 1 LINES
                 ");

          lq();
     }
     public function import_agenda()
     {    
          // exit('hi');
          $this->load->model('Agenda_model');
          $agenda_tmp = $this->db->limit(500)->get('agenda_tmp')->result_array();
          $this->db->query('DELETE FROM agenda_tmp LIMIT 500');

          $id = $agenda_tmp[0]['id'];
          $event = $this->Event_model->get_admin_event($id);

          foreach ($agenda_tmp as $key => $value)
          {    
               if($id != $value['event_id'])
               {    
                    $id =  $value['event_id'];
                    $event = $this->Event_model->get_admin_event($id);
               }
               $data['Organisor_id'] = $value['organisor_id'];
               $data['Event_id'] = $value['event_id'];
               $data['Start_date'] = date('Y-m-d', strtotime($value['Start_date']));
               $data['Start_time'] = date('H:i:s', strtotime($value['Start_time']));
               $data['End_date'] = date('Y-m-d', strtotime($value['End_date']));
               $data['End_time'] = date('H:i:s', strtotime($value['End_time']));
               $data['Heading'] = $value['Heading'];
               $check = $this->Agenda_model->check_exists_type_name($id, $value['Types']);
               if (count($check))
               {
                   $type_id = $check[0]['type_id'];
               }
               else
               {
                   $type_id = $this->Agenda_model->add_session_types($id, $value['Types']);
               }
               $data['Types'] = $type_id;
               $data['Agenda_status'] = '1';
               $data['description'] = $value['description'];
               $data['custom_speaker_name'] = $value['custom_speaker_name'];
               $data['custom_location'] = $value['custom_location'];
               $data['agenda_code'] = $value['session_code'];

               if(!empty($value['session_image']))
               {
                    $session_image="session_image".uniqid().'.png';
                    copy($value['Image'],"./assets/user_files/".$session_image);
                    $data['session_image']=$session_image;
               }

               $agenda = $this->db->where('agenda_code',$value['session_code'])
                                  ->where('Event_id',$value['event_id'])
                                  ->where('Organisor_id',$value['organisor_id'])
                                  ->get('agenda')->row_array();

               if($agenda)
               {    
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Id',$agenda['Id']);
                    $this->db->update('agenda',$data);
               }
               else
               {    
                    $data['created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('agenda',$data);
                    $agenda_id = $this->db->insert_id();

                    if(empty($agenda_category) || $agenda_category != $value['agenda_name'])
                    {
                         $agenda_category = $value['agenda_name'];
                         $cid =  $this->db->where('event_id',$value['event_id'])->where('category_name',$agenda_category)->get('agenda_categories')->row_array()['Id'];                         
                    }
                    $insert['agenda_id'] = $agenda_id;
                    $insert['category_id'] = $cid;
                    $this->db->insert('agenda_category_relation',$insert);
               }
          }
          j('agenda imported successfully');  
     }
     public function import_speaker()
     {
          $speaker_tmp = $this->db->limit(500)->get('speaker_tmp')->result_array();
          $this->db->query('DELETE FROM speaker_tmp LIMIT 500');
          foreach ($speaker_tmp as $key => $value) 
          {    
               $eid = $value['event_id'];
               updateModuleDate($eid,'speaker');
               $org_id = $value['organisor_id'];
               $userdata=$this->db->get_where('user',array('Email'=>trim($value['Email'])))->row_array();
               $user_data['Firstname']=$value['Firstname'];
               $user_data['Lastname']=$value['Lastname'];
               $user_data['Company_name']=$value['Company_name'];
               $user_data['Speaker_desc']=$value['Speaker_desc'];
               $user_data['Email']=$value['Email'];
               $user_data['Password']=md5(($value['Password'])?:'123456');
               $user_data['Title']=$value['Title'];
               $user_data['Salutation']=$value['Salutation'];
               if(!empty($value['Logo']))
               {   
                    $logoname="company_logo".uniqid().'.png';
                    copy($value['Logo'],"./assets/user_files/".$logoname);
                    $user_data['Logo']=$logoname; 
               }
               else
               {
                    $user_data['Logo']=NULL;
               }
               $user_data['Active']='1';
               $user_data['Organisor_id']=$org_id;
               
               if(!empty($userdata))
               {
                    $user_id=$userdata['Id'];
                    $user_data['updated_date']=date('Y-m-d H:i:s');
                    $this->db->where($userdata);
                    $this->db->update('user',$user_data);
               }
               else
               {   
                    $user_data['Created_date']=date('Y-m-d H:i:s');
                    $this->db->insert('user',$user_data);
                    $user_id=$this->db->insert_id();
               }
               unset($userdata);
               unset($user_data);

               $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
               if(empty($reldata))
               {
                    $rel_data['Event_id']=$eid;
                    $rel_data['User_id']=$user_id;
                    $rel_data['Organisor_id']=$org_id;
                    $rel_data['Role_id']=7;
                    $this->db->insert('relation_event_user',$rel_data);
               }

               unset($reldata);
               unset($rel_data);

               $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
               $social_link_data['Linkedin_url']=$value['Linkedin_url'];
               $social_link_data['Twitter_url']=$value['Twitter_url'];
               $social_link_data['Facebook_url']=$value['Facebook_url'];
               $social_link_data['Website_url']=$value['Website_url'];
               $social_link_data['Youtube_url']=$value['Youtube_url'];
               $social_link_data['Instagram_url']=$value['Instagram_url'];
               if(empty($sociallinkdata))
               {
                    $social_link_data['User_id']=$user_id;
                    $social_link_data['Event_id']=$eid;
                    $this->db->insert('user_social_links',$social_link_data);
               }
               else
               {
                    $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
               }
               unset($sociallinkdata);
               unset($social_link_data);
          }
          j('Data imported successfully');
     }
     public function import_data_temp() //Wednesday 30 May 2018 03:28:46 PM IST
     {
          $this->db->select('ec.*,e.Organisor_id');
          $this->db->join('event e', 'e.Id = ec.event_id');
          $this->db->where('e.End_date >=',date('Y-m-d'));
          $this->db->where('e.api_sync','1');
          $import_data = $this->db->get('event_csv_import ec')->result_array();
          foreach ($import_data as $key => $value)
          {    
               if($value['import_type'] == 'attendees')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONData($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveCSVData($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
               elseif($value['import_type'] == 'agenda')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONDataAgenda($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveCSVDataAgenda($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
               elseif($value['import_type'] == 'exhibitors')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONDataExhi($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveCSVDataExhi($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
               elseif($value['import_type'] == 'speaker')
               {
                    if($value['file_type'] == '1')
                    {
                         $this->Event_model->saveJSONDataSpeaker($value['json_url'],$value['event_id'],$value['Organisor_id']);
                    }
                    elseif ($value['file_type'] == '0')
                    {
                         $this->Event_model->saveCSVDataSpeaker($value['csv_url'],$value['event_id'],$value['Organisor_id']);
                    }
               }
          }
          echo "Data Imported successfully";
     }
}
