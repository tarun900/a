<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forbidden extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = '403';
		$this->data['smalltitle'] = 'Forbidden';
		$this->data['breadcrumb'] = 'Forbidden';
		parent::__construct($this->data);
	}

	public function index()                 
	{       
	        		         		
		$this->template->write_view('content', 'Forbidden/index', $this->data , true);
		$this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
		$this->template->render();
	}
}
