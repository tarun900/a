<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Speaker extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Key People';
        $this->data['smalltitle'] = 'Key People Directory';
        $this->data['page_edit_title'] = 'edit';
        $this->data['breadcrumb'] = 'Key People';
        parent::__construct($this->data);
        ini_set('auto_detect_line_endings', true);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $this->load->library('session');
        $this->load->library('upload');
        $user = $this->session->userdata('current_user');
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $event = $this->Event_model->get_module_event($eventid);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('7', $module) || !in_array('7', $menu_list))
        {
            redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Speaker")
            {
                $title = "Speakers";
            }
            $cnt = $this->Agenda_model->check_auth("Speakers", $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1)
        {
            $this->load->model('Speaker_model');
            $this->load->model('Setting_model');
            $this->load->model('Profile_model');
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('Forbidden');
        }
        $this->load->model('Sponsors_model');
    }
    
    public function index($id)
    { 
        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $speakers = $this->Speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event_id = $user[0]->event_id;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        $speaker_type = $this->Speaker_model->get_speaker_type_list($id);
        $this->data['speaker_type'] = $speaker_type;
        if ($event_id > 0)
        {
            $speakers = $this->Speaker_model->get_speaker_list($event_id);
            $this->data['speakers'] = $speakers;
            $speaker_list = $this->Speaker_model->get_all_speakers($event_id);
            $this->data['speaker_list'] = $speaker_list;
        }
        else
        {
            $speakers = $this->Speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
            $speaker_list = $this->Speaker_model->get_all_speakers($id);
            $this->data['speaker_list'] = $speaker_list;
        }
        $speakers_moderator = $this->Speaker_model->get_moderator_list($id);
        $this->data['speakers_moderator'] = $speakers_moderator;
        $menudata = $this->Event_model->geteventmenu($id, 7);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['categorie_data']=$this->Speaker_model->get_speaker_categories_by_event($id,NULL);
        $this->data['speaker_categories'] = $this->Speaker_model->get_speaker_categories_by_event($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'speaker/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/js', true);
        $this->template->render();
    }
    
    public function edit($id = '0')
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['speakers'] = $this->Speaker_model->get_speaker_list($id);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function edit_speaker($eventid, $id)
    {
        $speaker_detail = $this->Speaker_model->get_speaker_data($id, $eventid);
        $this->data['speaker_detail'] = $speaker_detail;
        $acc_name = $this->session->userdata('acc_name');
        $Event_id = $eventid;
        $event = $this->Event_model->get_admin_event($eventid);
        $doc_list = $this->Agenda_model->getAllDocumentonByEventId($eventid);
        $this->data['doc_list'] = $doc_list;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
        $this->data['users_role'] = $user_role;
        $speaker_type = $this->Speaker_model->get_speaker_type_list($eventid);
        $this->data['speaker_type'] = $speaker_type;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            if (empty($this->input->post('user_logo_crope_images_text')))
            {
                if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.', $_FILES['userfile']['name']);
                    $tempname[0] = str_replace(array(
                        ' ',
                        '%',
                        '+',
                        '&',
                        '-'
                    ) , array(
                        '_',
                        '_',
                        '_',
                        '_',
                        '_'
                    ) , $tempname[0]);
                    $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                    $_POST['Logo'] = $_FILES['userfile']['name'];
                    if ($this->data['user']->Role_name == 'Speaker')
                    {
                        $this->data['user']->Logo = $_FILES['userfile']['name'];
                    }
                    $this->upload->initialize(array(
                        "file_name" => $_FILES['userfile']['name'],
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png',
                        "max_size" => '2048',
                        "max_width" => '2048',
                        "max_height" => '2048'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                        echo "error###" . $this->upload->display_errors();
                        die;
                    }
                    else
                    {
                        $data = array(
                            'upload_data' => $this->upload->data()
                        );
                    }
                }
            }
            else
            {
                $img = $_POST['user_logo_crope_images_text'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_user_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $_POST['Logo'] = $images_file;
            }
            $documenu_ids = $this->input->post('document_id');
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval" && $k != "Speaker_mail" && $k != "document_id")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $array_add['document_id'] = implode(",", array_filter($documenu_ids));
            unset($array_add['Userfile']);
            unset($array_add['User_logo_crope_images_text']);
            $array_add['Organisor_id'] = $logged_in_user_id;
            unset($array_add['Address3']);
            unset($array_add['Files']);
            $this->Speaker_model->edit_speaker($array_add,$eventid);
            echo "success###Speaker Edit Successfully";
            die;
        }
        $statelist = $this->Profile_model->getstate($speaker_detail[0]['Country'], $flag);
        $this->data['Statelist'] = $statelist;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $event = $this->Event_model->get_admin_event($eventid);
        $this->data['event'] = $event[0];
        $agenda_list = $this->Agenda_model->get_agenda_list($eventid);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/edit', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function edit_moderator($eventid, $id)
    {
        $speaker_detail = $this->Speaker_model->get_moderator_data($id, $eventid);
        $this->data['speaker_detail'] = $speaker_detail;
        $this->data['moderator_user_data'] = $this->Speaker_model->get_speaker_list_by_moderator($id, $eventid);
        $acc_name = $this->session->userdata('acc_name');
        $Event_id = $eventid;
        $event = $this->Event_model->get_admin_event($eventid);
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
        $this->data['users_role'] = $user_role;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            if (empty($this->input->post('user_logo_crope_images_text')))
            {
                if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.', $_FILES['userfile']['name']);
                    $tempname[0] = str_replace(array(
                        ' ',
                        '%',
                        '+',
                        '&',
                        '-'
                    ) , array(
                        '_',
                        '_',
                        '_',
                        '_',
                        '_'
                    ) , $tempname[0]);
                    $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                    $_POST['Logo'] = $_FILES['userfile']['name'];
                    if ($this->data['user']->Role_name == 'Speaker')
                    {
                        $this->data['user']->Logo = $_FILES['userfile']['name'];
                    }
                    $this->upload->initialize(array(
                        "file_name" => $_FILES['userfile']['name'],
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '10000',
                        "max_width" => '2048',
                        "max_height" => '2048'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                        echo "error###" . $this->upload->display_errors();
                        die;
                    }
                    else
                    {
                        $data = array(
                            'upload_data' => $this->upload->data()
                        );
                    }
                }
            }
            else
            {
                $img = $_POST['user_logo_crope_images_text'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_user_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $_POST['Logo'] = $images_file;
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval" && $k != "Speaker_mail")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $keypeople = $array_add['Keypeople'];
            unset($array_add['Keypeople']);
            unset($array_add['Userfile']);
            unset($array_add['User_logo_crope_images_text']);
            $array_add['Organisor_id'] = $logged_in_user_id;
            unset($array_add['Address3']);
            unset($array_add['Files']);
            $this->Speaker_model->edit_speaker($array_add,$eventid);
            if (!empty($keypeople))
            {
                foreach($keypeople as $key => $value)
                {
                    $mod_rel['event_id'] = $eventid;
                    $mod_rel['moderator_id'] = $id;
                    $mod_rel['user_id'] = $value;
                    $this->Speaker_model->add_moderators_relation($mod_rel);
                }
            }
            $this->session->set_flashdata('moderator_data', 'Updated');
            redirect(base_url() . 'Speaker/index/' . $eventid);
        }
        $statelist = $this->Profile_model->getstate($speaker_detail[0]['Country'], $flag);
        $this->data['Statelist'] = $statelist;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $speakers = $this->Speaker_model->get_speaker_list($eventid);
        $this->data['keypeople'] = $speakers;
        $event = $this->Event_model->get_admin_event($eventid);
        $this->data['event'] = $event[0];
        $agenda_list = $this->Agenda_model->get_agenda_list($eventid);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/edit_moderator', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function add($id)
    {
        $acc_name = $this->session->userdata('acc_name');
        $this->load->library('email');
        /*$config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_port'] = 587;
        $config['smtp_user'] = "test@mailchimp.fmv.cc";
        $config['smtp_pass'] = "XvYIRK781AAaeruFbeSGnQ";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "text/html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);*/
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_port'] = 587;
        $config['smtp_user'] = "All In The Loop";
        $config['smtp_pass'] = "oD9B-JahFNNrJq2Sy9hkGg";
        $config['charset'] = "utf-8";
        $config['_encoding'] = 'base64';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        $logged_in_user_id = $user[0]->Id;
        $speaker_type = $this->Speaker_model->get_speaker_type_list($id);
        $this->data['speaker_type'] = $speaker_type;
        if ($this->input->post())
        {   
            if (empty($this->input->post('user_logo_crope_images_text')))
            {
                if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.', $_FILES['userfile']['name']);
                    $tempname[0] = str_replace(array(
                        ' ',
                        '%',
                        '+',
                        '&',
                        '-'
                    ) , array(
                        '_',
                        '_',
                        '_',
                        '_',
                        '_'
                    ) , $tempname[0]);
                    $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                    $_POST['Logo'] = $_FILES['userfile']['name'];
                    if ($this->data['user']->Role_name == 'Speaker')
                    {
                        $this->data['user']->Logo = $_FILES['userfile']['name'];
                    }
                    $this->upload->initialize(array(
                        "file_name" => $_FILES['userfile']['name'],
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png',
                        "max_size" => '2048',
                        "max_width" => '2048',
                        "max_height" => '2048'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                        echo "error###" . $this->upload->display_errors();
                        die;
                    }
                    else
                    {
                        $data = array(
                            'upload_data' => $this->upload->data()
                        );
                    }
                }
            }
            else
            {
                $img = $_POST['user_logo_crope_images_text'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_user_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $_POST['Logo'] = $images_file;
            }
            $documenu_ids = $this->input->post('document_id');
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval" && $k != "Speaker_mail" && $k != "document_id")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $array_add['document_id'] = implode(",", array_filter($documenu_ids));
            unset($array_add['Userfile']);
            unset($array_add['User_logo_crope_images_text']);
            $array_add['Organisor_id'] = $logged_in_user_id;
            $subject = $array_add['Subject'];
            unset($array_add['Subject']);
            $senderemail = $this->input->post('Sender_email');
            unset($array_add['Sender_email']);
            unset($array_add['Address3']);
            unset($array_add['Files']);
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $notification = $this->Setting_model->getnotificationsetting($Event_id);
            if ($notification[0]['email_display'] == 1)
            {
                $new_pass = $this->input->post('password');
                if ($subject != "")
                {
                    $subject = $em_template['Subject'];
                }
                if ($senderemail != "")
                {
                    $strFrom = $senderemail;
                }
                else
                {
                    $strFrom = EVENT_EMAIL;
                }
                if (strip_tags($this->input->post('Speaker_mail')) == "")
                {
                    $slug = "Add New speaker";
                    $em_template = $this->Setting_model->front_email_template($slug, $Event_id);
                    $em_template = $em_template[0];
                    $name = $this->input->post('Firstname') . ' ' . $this->input->post('Lastname');
                    $msg1 = $em_template['Content'];
                    $patterns = array();
                    $patterns[0] = '/{{FirstNameLastName}}/';
                    $patterns[1] = '/{{AppName}}/';
                    $patterns[2] = '/{{App URL}}/';
                    $patterns[3] = '/{{EmailAddress}}/';
                    $patterns[4] = '/{{Password}}/';
                    $patterns[5] = '/{{Administrator}}/';
                    $replacements = array();
                    $replacements[0] = $name;
                    $replacements[1] = $event[0]['Subdomain'];
                    $replacements[2] = "<a href='" . base_url() . "login/index/new' target='_blank'>" . base_url() . "</a>";
                    $replacements[3] = $this->input->post('email');
                    $replacements[4] = $new_pass;
                    $replacements[5] = 'Administrator';
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    // $msg .= "Email: " . $this->input->post('email') . "<br/>";
                    // $msg .= "Password: " . $new_pass."<br/>";
                    $msg.= $msg1;
                    if (strip_tags($em_template['Content']) == "")
                    {
                        $msg = "Dear " . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . ",<br/>";
                        $msg.= "A speaker profile has been created for you for " . $event[0]['Subdomain'] . "<br/>";
                        $msg.= "Please login via " . base_url() . " with the following credentials." . "<br/>";
                        $msg.= $this->input->post('email') . " " . $new_pass;
                        ".Once your login please click on your name on the top right of the screen and press My Profile to edit your profile<br/>";
                        $msg.= "many thanks <br/>Administrator";
                    }
                }
                else
                {
                    $msg = "<p>Dear " . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . ", </p>";
                    $msg.= "<p>A speaker profile has been created for you for " . $event[0]['Subdomain'] . ". <br/>";
                    $msg.= "Please login via <a href='" . base_url() . "login/index/new' target='_blank'>" . base_url() . "</a> with the following credentials. </p>";
                    $msg.= "Email:" . $this->input->post('email') . "<br/>";
                    $msg.= "Password:" . $new_pass . "<br/>";
                    $msg.= "<p>Once your login please click on your name on the top right of the screen and press My <br/>Profile to edit your profile.</p>";
                    $msg.= "<p>" . $_POST['Speaker_mail'] . "</p>";
                    $msg.= "Many thanks, <br/>";
                    $msg.= "Administrator";
                }
                if ($em_template['From'] != "")
                {
                    //  $strFrom = $em_template['From'];
                }
                /*$this->email->from('Registration@allintheloop.com', 'All In The Loop');
                $this->email->to($this->input->post('email'));
                $this->email->subject($subject); //'Speaker Account'
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                $this->email->send();*/
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            if(empty($array_add['Email']))
            {
                $array_add['Email'] = str_replace(' ','',$array_add['Firstname'].'-'.$array_add['Lastname'].'-'.uniqid().'@venturiapps.com');
            }
            $speaker_id = $this->Speaker_model->add_speaker($array_add);
            echo "success###Speaker Add Successfully";
            die;
        }
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $doc_list = $this->Agenda_model->getAllDocumentonByEventId($id);
        $this->data['doc_list'] = $doc_list;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function add_moderators($id)
    {
        $acc_name = $this->session->userdata('acc_name');
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_port'] = 587;
        $config['smtp_user'] = "All In The Loop";
        $config['smtp_pass'] = "oD9B-JahFNNrJq2Sy9hkGg";
        $config['charset'] = "utf-8";
        $config['_encoding'] = 'base64';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $user[0]->Organisor_id;
        }
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            if (empty($this->input->post('user_logo_crope_images_text')))
            {
                if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.', $_FILES['userfile']['name']);
                    $tempname[0] = str_replace(array(
                        ' ',
                        '%',
                        '+',
                        '&',
                        '-'
                    ) , array(
                        '_',
                        '_',
                        '_',
                        '_',
                        '_'
                    ) , $tempname[0]);
                    $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                    $_POST['Logo'] = $_FILES['userfile']['name'];
                    if ($this->data['user']->Role_name == 'Speaker')
                    {
                        $this->data['user']->Logo = $_FILES['userfile']['name'];
                    }
                    $this->upload->initialize(array(
                        "file_name" => $_FILES['userfile']['name'],
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '10000',
                        "max_width" => '2048',
                        "max_height" => '2048'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                        echo "error###" . $this->upload->display_errors();
                        die;
                    }
                    else
                    {
                        $data = array(
                            'upload_data' => $this->upload->data()
                        );
                    }
                }
            }
            else
            {
                $img = $_POST['user_logo_crope_images_text'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_user_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $_POST['Logo'] = $images_file;
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval" && $k != "Speaker_mail")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $keypeople_id = $array_add['Keypeople'];
            unset($array_add['Keypeople']);
            $array_add['is_moderator'] = '1';
            unset($array_add['Userfile']);
            unset($array_add['User_logo_crope_images_text']);
            $array_add['Organisor_id'] = $logged_in_user_id;
            $subject = $array_add['Subject'];
            unset($array_add['Subject']);
            $senderemail = $this->input->post('Sender_email');
            unset($array_add['Sender_email']);
            unset($array_add['Address3']);
            unset($array_add['Files']);
            unset($array_add['Keypeople']);
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $notification = $this->Setting_model->getnotificationsetting($Event_id);
            if ($notification[0]['email_display'] == 1)
            {
                $new_pass = $this->input->post('password');
                if ($subject != "")
                {
                    $subject = $em_template['Subject'];
                }
                if ($senderemail != "")
                {
                    $strFrom = $senderemail;
                }
                else
                {
                    $strFrom = EVENT_EMAIL;
                }
                if (strip_tags($this->input->post('Speaker_mail')) == "")
                {
                    $slug = "Add New speaker";
                    $em_template = $this->Setting_model->front_email_template($slug, $Event_id);
                    $em_template = $em_template[0];
                    $name = $this->input->post('Firstname') . ' ' . $this->input->post('Lastname');
                    $msg1 = $em_template['Content'];
                    $patterns = array();
                    $patterns[0] = '/{{FirstNameLastName}}/';
                    $patterns[1] = '/{{AppName}}/';
                    $patterns[2] = '/{{App URL}}/';
                    $patterns[3] = '/{{EmailAddress}}/';
                    $patterns[4] = '/{{Password}}/';
                    $patterns[5] = '/{{Administrator}}/';
                    $replacements = array();
                    $replacements[0] = $name;
                    $replacements[1] = $event[0]['Subdomain'];
                    $replacements[2] = "<a href='" . base_url() . "login/index/new' target='_blank'>" . base_url() . "</a>";
                    $replacements[3] = $this->input->post('email');
                    $replacements[4] = $new_pass;
                    $replacements[5] = 'Administrator';
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    // $msg .= "Email: " . $this->input->post('email') . "<br/>";
                    // $msg .= "Password: " . $new_pass."<br/>";
                    $msg.= $msg1;
                    if (strip_tags($em_template['Content']) == "")
                    {
                        $msg = "Dear " . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . ",<br/>";
                        $msg.= "A Moderator profile has been created for you for " . $event[0]['Subdomain'] . "<br/>";
                        $msg.= "Please login via " . base_url() . " with the following credentials." . "<br/>";
                        $msg.= $this->input->post('email') . " " . $new_pass;
                        ".Once your login please click on your name on the top right of the screen and press My Profile to edit your profile<br/>";
                        $msg.= "many thanks <br/>Administrator";
                    }
                }
                else
                {
                    $msg = "<p>Dear " . ucfirst($this->input->post('Firstname')) . " " . ucfirst($this->input->post('Lastname')) . ", </p>";
                    $msg.= "<p>A Moderator profile has been created for you for " . $event[0]['Subdomain'] . ". <br/>";
                    $msg.= "Please login via <a href='" . base_url() . "login/index/new' target='_blank'>" . base_url() . "</a> with the following credentials. </p>";
                    $msg.= "Email:" . $this->input->post('email') . "<br/>";
                    $msg.= "Password:" . $new_pass . "<br/>";
                    $msg.= "<p>Once your login please click on your name on the top right of the screen and press My <br/>Profile to edit your profile.</p>";
                    $msg.= "<p>" . $_POST['Speaker_mail'] . "</p>";
                    $msg.= "Many thanks, <br/>";
                    $msg.= "Administrator";
                }
                if ($em_template['From'] != "")
                {
                    //  $strFrom = $em_template['From'];
                }
                /*$this->email->from($strFrom, 'Event Management');
                $this->email->to($this->input->post('email'));
                $this->email->subject($subject); //'Speaker Account'
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                $this->email->send();*/
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            if(empty($array_add['Email']))
            {
                $array_add['Email'] = str_replace(' ','',$array_add['Firstname'].'-'.$array_add['Lastname'].'-'.uniqid().'@venturiapps.com');
            }
            $moderator_id = $this->Speaker_model->add_speaker($array_add);
            if (!empty($keypeople_id))
            {
                foreach($keypeople_id as $key => $value)
                {
                    $mod_rel['event_id'] = $id;
                    $mod_rel['moderator_id'] = $moderator_id;
                    $mod_rel['user_id'] = $value;
                    $this->Speaker_model->add_moderators_relation($mod_rel);
                }
            }
            $this->session->set_flashdata('moderator_data', 'Added');
            redirect(base_url() . 'Speaker/index/' . $id);
        }
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $speakers = $this->Speaker_model->get_speaker_list($id);
        $this->data['keypeople'] = $speakers;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/add_moderator', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function exist($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $speaker_list = $this->Speaker_model->get_all_speakers($id);
        $this->data['speaker_list'] = $speaker_list;
        $this->data['Event_id'] = $Event_id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['speaker_array']['Event_id'] = $this->input->post('Event_id');
            $data['speaker_array']['User_id'] = $this->input->post('User_id');
            $data['speaker_array']['Role_id'] = $this->input->post('rolename');
            $speaker_id = $this->Speaker_model->add_exist_speaker($data);
            $this->session->set_flashdata('speaker_data', 'Added');
            redirect(base_url() . 'Speaker/index/' . $Event_id);
            // echo '<script>window.location.href="'.base_url().'speaker/index/'.$Event_id.'"</script>';
        }
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/exist', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function add_speakers($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $dataval = "";
        if ($this->input->post())
        {
            if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
            {
                $tempname = explode('.', $_FILES['userfile']['name']);
                $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                $_POST['Logo'] = $_FILES['userfile']['name'];
                if ($this->data['user']->Role_name == 'Speaker')
                {
                    $this->data['user']->Logo = $_FILES['userfile']['name'];
                }
                $config['upload_path'] = './assets/user_files';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width'] = '2048';
                $config['max_height'] = '2048';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload())
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                else
                {
                    $data = array(
                        'upload_data' => $this->upload->data()
                    );
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }

            /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $notification = $this->Setting_model->getnotificationsetting($Event_id);
            if ($notification[0]['email_display'] == 1)
            {
                $slug = "WELCOME_MSG";
                $em_template = $this->Setting_model->front_email_template($slug, $Event_id);
                $em_template = $em_template[0];
                $new_pass = $this->input->post('password');
                $name = ($this->input->post('Firstname') == NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
                $msg = $em_template['Content'];
                $patterns = array();
                $patterns[0] = '/{{name}}/';
                $patterns[1] = '/{{password}}/';
                $patterns[2] = '/{{email}}/';
                $replacements = array();
                $replacements[0] = $name;
                $replacements[1] = $new_pass;
                $replacements[2] = $this->input->post('Email');
                $msg = preg_replace($patterns, $replacements, $msg);
                $msg.= "Email: " . $this->input->post('Email') . "<br/>";
                $msg.= "Password: " . $new_pass;
                if ($em_template['Subject'] == "")
                {
                    $subject = "WELCOME_MSG";
                }
                else
                {
                    $subject = $em_template['Subject'];
                }
                $strFrom = EVENT_EMAIL;
                if ($em_template['From'] != "")
                {
                    $strFrom = $em_template['From'];
                }
                /*$this->email->from($strFrom, 'Event Management');
                $this->email->to($this->input->post('email'));
                $this->email->subject($subject); //'Speaker Account'
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                $this->email->send();*/
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            if(empty($array_add['Email']))
            {
                $array_add['Email'] = str_replace(' ','',$array_add['Firstname'].'-'.$array_add['Lastname'].'-'.uniqid().'@venturiapps.com');
            }
            $speaker_id = $this->Speaker_model->add_speakers($array_add);
            $speaker_list = $this->Speaker_model->get_speaker_list($id);
            foreach($speaker_list as $keys => $values)
            {
                $dataval.= '<option value="' . $values['uid'] . '">' . $values["Firstname"] . ' ' . $values["Lastname"] . '</option>';
            }
        }
        echo $dataval;
        exit;
    }
    
    public function delete($Event_id, $id)
    {
        // $Event_id = $this->uri->segment(4);
        $speaker = $this->Speaker_model->delete_speaker($Event_id, $id);
        $this->session->set_flashdata('speaker_data', 'Deleted');
        redirect(base_url() . 'Speaker/index/' . $Event_id);
        // echo '<script>window.location.href="'.base_url().'speaker/index/'.$Event_id.'"</script>';
    }
    
    public function getnewstate()
    {
        $id_country = $this->input->post('id', TRUE);
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $statedata = $this->Profile_model->getstate($id_country);
        $output = null;
        foreach($statedata as $key => $value)
        {
            $output.= "<option value='" . $value['Id'] . "'>" . $value['state_name'] . "</option>";
        }
        echo $output;
    }
    
    public function checkemail($Event_id = NULL)
    {
        if ($this->input->post())
        {
            $client = $this->Speaker_model->checkemail($this->input->post('email') , $this->input->post('idval') , $this->input->post('event_id'));
            if ($client)
            {
                echo "error###Email already exist. Please choose another email.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }
    
    public function update_keypeople()
    {
        if ($this->input->post())
        {
            $this->Speaker_model->update_keypeople($this->input->post('id') , $this->input->post('data'));
        }
        exit();
    }
    
    public function saveorderby($id)
    {
        $ei['event_array']['Id'] = $id;
        $ei['event_array']['key_people_sort_by'] = $this->input->post('key_people_sort_by');
        $this->Event_model->update_admin_event($ei);
        redirect(base_url() . 'Speaker/index/' . $id);
    }
    
    public function saveallow_msg_keypeople_to_attendee($id)
    {
        $ei['event_array']['Id'] = $id;
        if ($this->input->post('allow_msg_keypeople_to_attendee') == '1')
        {
            $ei['event_array']['allow_msg_keypeople_to_attendee'] = $this->input->post('allow_msg_keypeople_to_attendee');
        }
        else
        {
            $ei['event_array']['allow_msg_keypeople_to_attendee'] = '0';
        }
        if ($this->input->post('allow_meeting_attendee_to_speaker') == '1')
        {
            $ei['event_array']['allow_meeting_attendee_to_speaker'] = $this->input->post('allow_meeting_attendee_to_speaker');
        }
        else
        {
            $ei['event_array']['allow_meeting_attendee_to_speaker'] = '0';
        }
        $this->Event_model->update_admin_event($ei);
        redirect(base_url() . 'Speaker/index/' . $id);
    }
    
    public function mass_upload_page($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $speakers = $this->Speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event_id = $user[0]->event_id;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'speaker/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/mass_add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/add_js', $this->data, true);
        $this->template->render();
    }
    
    public function download_template_csv($eid)
    {
        $this->load->helper('download');
        $filename = "speaker_demo.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Salutation";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Password";
        $header[] = "Title";
        $header[] = "Company Name";
        $header[] = "Mobile";
        $header[] = "Logo";
        $header[] = "Speaker Desc";
        $header[] = "Website Url";
        $header[] = "Facebook Url ";
        $header[] = "Twitter Url";
        $header[] = "Linkedin Url";
        $header[] = "Speaker Type";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $data['Salutation'] = "Mr.";
        $data['Firstname'] = "first";
        $data['Lastname'] = "last";
        $data['Email'] = "testing@gmail.com";
        $data['Password'] = "123456";
        $data['Title'] = "developer";
        $data['Company_name'] = "test pvt ltd";
        $data['Mobile'] = "9457526575";
        $data['Logo'] = "Logo Image Link";
        $data['Speaker_desc'] = "Speaker Description";
        $data['Website_url'] = "https://www.website.com";
        $data['Facebook_url'] = "https://www.facebook.com";
        $data['Twitter_url'] = "https://www.twitter.com";
        $data['Linkedin_url'] = "https://www.linkedin.com";
        $data['Speaker_Type'] = "Pro";
        fputcsv($fp, $data);
    }
    
    public function upload_csv_speaker($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        if (pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION) == "csv")
        {
            $file = $_FILES['csv']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            $email_exsis = 0;
            $email_not_exsis = 0;
            while (($datacsv = fgetcsv($handle, 0, ",")) !== FALSE)
            {
                if ($find_header != '0' && count($datacsv) > 1)
                {
                    $email_exsist = $this->Speaker_model->checkemail($datacsv[3], null, $id);
                    if ($email_exsist && !empty($datacsv[3]))
                    {
                        $email_exsis++;
                    }
                    else
                    {
                        $speaker_data['Organisor_id'] = $event[0]['Organisor_id'];
                        $speaker_data['Salutation'] = $datacsv[0];
                        $speaker_data['Firstname'] = $datacsv[1];
                        $speaker_data['Lastname'] = $datacsv[2];
                        $speaker_data['Email'] = $datacsv[3] ?: str_replace(' ','',$datacsv[1].'-'.$datacsv[2].'-'.uniqid().'@venturiapps.com');
                        if ($id == '447')
                        {
                            $this->load->library('RC4');
                            $rc4 = new RC4();
                            $speaker_data['Password'] = $rc4->encrypt($datacsv[4]);
                        }
                        else
                        {
                            $speaker_data['Password'] = md5($datacsv[4]);
                        }
                        $speaker_data['Title'] = $datacsv[5];
                        $speaker_data['Company_name'] = $datacsv[6];
                        $speaker_data['Mobile'] = $datacsv[7];
                        if (!empty($datacsv[8]))
                        {
                            $logo = "Profile_images_" . uniqid() . '.png';
                            copy($datacsv[8], './assets/user_files/' . $logo);
                            $speaker_data['Logo'] = $logo;
                        }
                        $speaker_data['Speaker_desc'] = $datacsv[9];
                        $speaker_data['Created_date'] = date('Y-m-d H:i:s');
                        $speaker_data['Active'] = '1';
                        $user_link['Website_url'] = $datacsv[10];
                        $user_link['Facebook_url'] = $datacsv[11];
                        $user_link['Twitter_url'] = $datacsv[12];
                        $user_link['Linkedin_url'] = $datacsv[13];
                        $speaker_id = $this->Speaker_model->add_csv_speaker($id, $speaker_data, $user_link);
                        if(!empty($datacsv[14]))
                        {
                            $this->Speaker_model->assign_spear_type($id,$datacsv[14],$speaker_id);
                        }
                        if (!empty($speaker_id))
                        {
                            $email_not_exsis++;
                            $notification = $this->Setting_model->getnotificationsetting($id);
                            if ($notification[0]['email_display'] == 1)
                            {
                                $slug = "WELCOME_MSG";
                                $em_template = $this->Setting_model->front_email_template($slug, $id);
                                $em_template = $em_template[0];
                                $new_pass = $datacsv[4];
                                $name = ($datacsv[1] == NULL) ? $datacsv[6] : $datacsv[1];
                                $msg = $em_template['Content'];
                                $patterns = array();
                                $patterns[0] = '/{{name}}/';
                                $patterns[1] = '/{{password}}/';
                                $patterns[2] = '/{{email}}/';
                                $replacements = array();
                                $replacements[0] = $name;
                                $replacements[1] = $new_pass;
                                $replacements[2] = $datacsv[3];
                                $msg = preg_replace($patterns, $replacements, $msg);
                                $msg.= "Email: " . $datacsv[3] . "<br/>";
                                $msg.= "Password: " . $new_pass;
                                if ($em_template['Subject'] == "")
                                {
                                    $subject = "Welcome Messagess";
                                }
                                else
                                {
                                    $subject = $em_template['Subject'];
                                }
                                $strFrom = EVENT_EMAIL;
                                if ($em_template['From'] != "")
                                {
                                    $strFrom = $em_template['From'];
                                }
                                /*$this->email->from($strFrom, 'All In The Loop');
                                $this->email->to($datacsv[3]);
                                $this->email->subject($subject);
                                $this->email->set_mailtype("html");
                                $this->email->message($msg);
                                $this->email->send();*/
                            }
                        }
                    }
                }
                $find_header++;
            }
            $this->session->set_flashdata('speaker_data', 'Added');
            redirect(base_url() . 'Speaker/index/' . $id);
        }
        else
        {
            $this->session->set_flashdata('speaker_error_data', ' please Upload csv Files.');
            redirect(base_url() . 'Speaker/mass_upload_page/' . $id);
        }
    }
    public function add_speaker_type($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {   
            $type_data['type_name'] = $this->input->post('speaker_type_name');
            $type_data['type_position'] = $this->input->post('position_in_directory');
            $type_data['type_color'] = $this->input->post('custom_color_picker');
            $type_data['event_id'] = $id;
            $type_data['created_date'] = date('Y-m-d H:i:s');
            $type_id = $this->Speaker_model->save_speaker_type($type_data);
            $this->session->set_flashdata('speaker_data', 'Type Added');
            redirect(base_url() . 'Speaker/index/' . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/add_speaker_type', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Sponsors_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function speaker_type_edit($id, $tid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            $type_data['type_name'] = $this->input->post('speaker_type_name');
            $type_data['type_position'] = $this->input->post('position_in_directory');
            $type_data['type_color'] = $this->input->post('custom_color_picker');
            $type_id = $this->Speaker_model->update_speaker_type($type_data, $id, $tid);
            $this->session->set_flashdata('speaker_data', 'Type Updated');
            redirect(base_url() . 'Speaker/index/' . $id);
        }
        $edit_type_data = $this->Speaker_model->get_edit_speaker_type($id, $tid);
        $this->data['type_data'] = $edit_type_data;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'speaker/edit_speaker_type', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Sponsors_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function delete_speaker_type($eid, $tid)
    {
        $this->Speaker_model->remove_speaker_type($eid, $tid);
        $this->session->set_flashdata('speaker_data', 'Type Deleted');
        redirect(base_url() . 'Speaker/index/' . $eid);
    }

    #7/9/2018 2:26:05 PM
    public function import_speaker($id)
    {   

        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role'] = $user_role;
        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;

        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/import_speaker', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function save_import_speaker($id)
    {         

        $event_data = $this->Event_model->get_event_accesskey_id($id,$this->input->post('access_key'));
        /*if(empty($event_data))
        {
            $this->session->set_flashdata('csv_flash_message', 'Access key is wrong.');
            redirect('speaker/import_speaker/'.$id);
        }*/

        if($this->input->post('file-type') == 'JSON')
        {
            $data['json_url'] = $this->input->post('json_url');
            $data['import_type'] =  'speaker';
            $data['event_id'] = $id;
            $data['file_type'] = '1';
            $this->Event_model->saveEventCSVImport($data);
            $event_data = $this->Event_model->get_event_accesskey_id($id,$this->input->post('access_key'));
            $saveCSVData = $this->Event_model->saveJSONDataSpeaker($data['json_url'],$event_data['Id'],$event_data['Organisor_id']);
            $this->session->set_flashdata('csv_flash_message', 'JSON Imported Successfully.');
            redirect(base_url().'Speaker/import_speaker/'.$id);
        }
        else
        {
             
            session_start();
            $filepath = $this->input->post('csv_url');
            $contents = file_get_contents($this->input->post('csv_url'));
            if (strlen($contents))
            {
                $_SESSION['access_key'] = $this->input->post('access_key');
                $_SESSION['csv_url'] = $this->input->post('csv_url');

                $handle = fopen($filepath, "r");
                $find_header = 0;
                while (($datacsv[] = fgetcsv($handle,1000,",")) !== FALSE)
                {}
                $keys = array_shift($datacsv);
                foreach ($datacsv as $key => $value){
                $csv[] = array_combine($keys,$value);
                }

                $nomatch = array();
                $csvtest = array_filter($csv);
                $this->data['import_csv_data'] = $csvtest;
                $_SESSION['save_import_attendees'] = $csvtest;
                $this->data['keysdata'] = $keys;
                foreach ($csv as $key => $value)
                {
                    $keysarr = array_keys($value);
                    $fixedc = array ('Salutation','Firstname','Lastname','Email','Password','Title','Company_name','Logo','Speaker_desc','Website_url', 'Facebook_url', 'Twitter_url', 'Linkedin_url');
                    for ($i=0;$i<count($keysarr);$i++) 
                    {
                        if(!in_array($keysarr[$i],$fixedc))
                        {
                           $kk = $keysarr[$i];
                           if(!in_array($kk,$nomatch))
                           {
                                $nomatch[]=$kk;
                           }
                        }
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('csv_flash_message', 'CSV file not found.');
                redirect('Speaker/import_speaker/'.$id);
            }
            $this->data['nomatch'] = count($nomatch);
            $this->data['Event_id'] = $id;
            $statelist = $this->Profile_model->statelist();
            $this->data['Statelist'] = $statelist;
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
            $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
            $this->data['category_list'] = $category_list;
            $agenda_list = $this->Agenda_model->get_agenda_list($id);
            $this->data['agenda_list'] = $agenda_list;
            $this->template->write_view('css', 'admin/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'speaker/maping_import_speaker', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
            $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
            $this->template->render();
        }
    }
    public function download_speaker_template_csv($eid)
    {
        $this->load->helper('download');
        $filename = "import_speaker.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Salutation";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Password";
        $header[] = "Title";
        $header[] = "Company_name";
        $header[] = "Logo";
        $header[] = "Speaker_desc";
        $header[] = "Website_url";
        $header[] = "Facebook_url";
        $header[] = "Twitter_url";
        $header[] = "Linkedin_url";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        for ($i=1; $i<=1 ; $i++)
        { 
            $data['Salutation'] = "Mr.";
            $data['Firstname'] = "first";
            $data['Lastname'] = "last";
            $data['Email'] = "speaker$i@venturiapps.com";
            $data['Password'] = "123456";
            $data['Title'] = "developer";
            $data['Company_name'] = "test pvt ltd";
            $data['Logo'] = "";
            $data['Speaker_desc'] = "Description";
            $data['Website_url'] = "https://www.website.com";
            $data['Facebook_url'] = "https://www.facebook.com";
            $data['Twitter_url'] = "https://www.twitter.com";
            $data['Linkedin_url'] = "https://www.linkedin.com";
            fputcsv($fp, $data);
        }
    }
    public function download_import_speaker_json()
    {    
        error_reporting(E_ALL);
        $this->load->helper('download');
        $filename = "import_speaker.json";
        $fp = fopen('php://output', 'w');

        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename='.$filename);

        $custom_speaker_name = array("Kay", "Joe","Susan", "Frank");
        for( $i = 1; $i<=1; $i++ )
        {
        $data['Salutation'] = "Mr.";
        $data['Firstname'] = "first";
        $data['Lastname'] = "last";
        $data['Email'] = "testing$i@gmail.com";
        $data['Password'] = "123456";
        $data['Title'] = "developer";
        $data['Company_name'] = "test pvt ltd";
        $data['Logo'] = "Logo Image Link";
        $data['Speaker_desc'] = "Description";
        $data['Website_url'] = "https://www.website.com";
        $data['Facebook_url'] = "https://www.facebook.com";
        $data['Twitter_url'] = "https://www.twitter.com";
        $data['Linkedin_url'] = "https://www.linkedin.com";
        $speaker[] = $data;
        }
        $data = json_encode($speaker);   
        fputs($fp, $data);
        exit;
    }
    public function show_import_speaker_json()
    {    
    echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
    {
        "Salutation": "<font color=green><b>(String)</b></font> Salutation",
        "Firstname": "<font color=green><b>(String)</b></font> Firstname <font color=red><b>**Required**</b></font>",
        "Lastname": "<font color=green><b>(String)</b></font> Lastname <font color=red><b>**Required**</b></font>",
        "Email": "<font color=green><b>(String)</b></font> Email <font color=red><b>**Required**</b></font>",
        "Password": "<font color=green><b>(String)</b></font> Password",
        "Title": "<font color=green><b>(String)</b></font> Title",
        "Company_name": "<font color=green><b>(String)</b></font> Company_name",
        "Logo": "<font color=green><b>(String)</b></font> Logo Image Link",
        "Speaker_desc": "<font color=green><b>(String)</b></font> Description",
        "Website_url": "<font color=green><b>(String)</b></font> https://www.website.com",
        "Facebook_url": "<font color=green><b>(String)</b></font> https://www.facebook.com",
        "Twitter_url": "<font color=green><b>(String)</b></font> https://www.twitter.com",
        "Linkedin_url": "<font color=green><b>(String)</b></font> https://www.linkedin.com"
    },
    {
        "Salutation": "Mr.",
        "Firstname": "first",
        "Lastname": "last",
        "Email": "testing1@gmail.com",
        "Password": "123456",
        "Title": "developer",
        "Company_name": "test pvt ltd",
        "Logo": "Logo Image Link",
        "Speaker_desc": "Description",
        "Website_url": "https://www.website.com",
        "Facebook_url": "https://www.facebook.com",
        "Twitter_url": "https://www.twitter.com",
        "Linkedin_url": "https://www.linkedin.com"
    }
]';
exit();
        error_reporting(E_ALL);
        header('Content-type: application/json');

        for($i=0;$i<2;$i++)
        {    
           if($i ==  0)
           {    
                $data['Salutation'] = "Salutation";
                $data['Firstname'] = "Firstname";
                $data['Lastname'] = "Lastname";
                $data['Email'] = "Email";
                $data['Password'] = "Password";
                $data['Title'] = "Title";
                $data['Company_name'] = "Company_name";
                $data['Logo'] = "Logo Image Link";
                $data['Speaker_desc'] = "Description";
                $data['Website_url'] = "https://www.website.com";
                $data['Facebook_url'] = "https://www.facebook.com";
                $data['Twitter_url'] = "https://www.twitter.com";
                $data['Linkedin_url'] = "https://www.linkedin.com";
           }
           else
           {
                $data['Salutation'] = "Mr.";
                $data['Firstname'] = "first";
                $data['Lastname'] = "last";
                $data['Email'] = "testing$i@gmail.com";
                $data['Password'] = "123456";
                $data['Title'] = "developer";
                $data['Company_name'] = "test pvt ltd";
                $data['Logo'] = "Logo Image Link";
                $data['Speaker_desc'] = "Description";
                $data['Website_url'] = "https://www.website.com";
                $data['Facebook_url'] = "https://www.facebook.com";
                $data['Twitter_url'] = "https://www.twitter.com";
                $data['Linkedin_url'] = "https://www.linkedin.com";
           }
           $fin[$i] = $data;
        }
        echo json_encode($fin, JSON_PRETTY_PRINT);
    }
    public function upload_import_speaker($eventid)
    {
        session_start();
        $key = array_keys($this->input->post());
        $postval = array_values($this->input->post());
        $postarr = array_combine($key,$postval);
        asort($postarr);

        $saveEventCSV['event_id'] = $eventid;
        $saveEventCSV['import_type'] = 'speaker';
        $saveEventCSV['csv_url'] = $_SESSION['csv_url'];
        $saveEventCSV['csv_mapping'] = json_encode($postarr);
        $csvSave = $this->Event_model->saveEventCSVImport($saveEventCSV);

        $access_key = $_SESSION['access_key'];
        $event_data = $this->Event_model->get_event_accesskey_id($eventid,$access_key);
        $saveCSVData = $this->Event_model->saveCSVDataSpeaker($saveEventCSV['csv_url'],$event_data['Id'],$event_data['Organisor_id']);

        unset($_SESSION['save_import_attendees']);
        unset($_SESSION['access_key']);
        unset($_SESSION['csv_url']);
        $this->session->set_flashdata('csv_flash_message', 'CSV Import Successfully.');
        redirect(base_url().'Speaker/import_speaker/'.$eventid);
    }
    public function add_speaker_categorie($id) //7/26/2018 6:33:15 PM
    {
         $user = $this->session->userdata('current_user');
         $roleid = $user[0]->Role_id;
         $event_id = $id;
         $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
         $this->data['users_role'] = $user_role;
         $logged_in_user_id = $user[0]->Id;
         if ($this->input->post())
         {
              $categorie_data['category'] = $this->input->post('categorie_name');
              $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
              $categorie_data['event_id'] = $id;
              $categorie_data['menu_id'] = '7';
              $categorie_data['updated_date'] = date('Y-m-d H:i:s');
              $categorie_data['created_date'] = date('Y-m-d H:i:s');
              $this->Speaker_model->add_speaker_categorie($categorie_data);
              $this->session->set_flashdata('speaker_data', 'Categorie Added');
              redirect(base_url() . 'Speaker/index/' . $id);
         }

         $event = $this->Event_model->get_admin_event($id);
         $this->data['event'] = $event[0];
         $this->session->set_userdata($data);
         $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
         $this->template->write_view('content', 'speaker/add_speaker_categorie', $this->data, true);
         if ($this->data['user']->Role_name == 'User')
         {
              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
         }
         else
         {
              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
         }

         $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
         $this->template->render();
    }

    public function speaker_categories_edit($id, $cid) //7/26/2018 6:33:15 PM
    {
         $user = $this->session->userdata('current_user');
         $roleid = $user[0]->Role_id;
         $event_id = $id;
         $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
         $this->data['users_role'] = $user_role;
         $logged_in_user_id = $user[0]->Id;
         if ($this->input->post())
         {
              $categorie_data['category'] = $this->input->post('categorie_name');
              $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
              $categorie_data['updated_date'] = date('Y-m-d H:i:s');
              $this->Speaker_model->update_speaker_categorie($categorie_data, $cid);
              $this->session->set_flashdata('speaker_data', ' Categorie Updated');
              redirect(base_url() . 'Speaker/index/' . $id);
         }

         $this->data['categorie_data'] = $this->Speaker_model->get_speaker_categories_by_event($id, $cid);
         $event = $this->Event_model->get_admin_event($id);
         $this->data['event'] = $event[0];
         $this->session->set_userdata($data);
         $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
         $this->template->write_view('content', 'speaker/edit_speaker_categorie', $this->data, true);
         if ($this->data['user']->Role_name == 'User')
         {
              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
         }
         else
         {
              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
         }

         $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
         $this->template->render();
    }

    public function delete_speaker_categorie($id, $cid) //7/26/2018 6:33:15 PM
    {
         $this->Speaker_model->delete_speaker_categorie($cid, $id);
         $this->session->set_flashdata('speaker_data', 'Categorie Deleted');
         // redirect(base_url().'attendee_admin/index/'.$id);
    }

    public function assign_speaker_category($eventid)
    {
        $speaker_id = $this->input->post('speaker_id');
        $speaker_categories = $this->input->post('speaker_categories');
        if (!empty($speaker_categories))
        {
            foreach($speaker_id as $key => $value)
            {
                $insert_data['user_id'] = $value;
                $insert_data['keyword'] = $speaker_categories;
                $this->Speaker_model->assign_speaker_categories($insert_data, $eventid);
            }
        }
        $this->session->set_flashdata('attendee_agenda_assign_data', 'Categorie Assign Successfully.');
        echo "success###" . base_url() . 'speaker/index/' . $eventid;
    }

    public function downloadDeepLinks($id)
    {
        $this->Speaker_model->downloadDeepLinks($id);
    } 
}