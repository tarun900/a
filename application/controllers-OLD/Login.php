﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller 
{
    function __construct() 
    {

        parent::__construct();
        $this->load->model('Setting_model');
        $this->load->model('Add_attendee_model');
        $this->load->model('Event_model');
        $this->load->model('API/Product_model');
        $this->load->model('API/Settings_model');
        /*include('application/libraries/nativeGcm.php');
        include_once(base_url().'assets/fonts/lato_fonts/lato_fonts_lib/updated_lib/new_checkip.php');*/

    }

    public function index() 
    {   
        if($this->uri->segment(3)=="new")
        {
            $this->session->sess_destroy();
            redirect('Login');
        }
        if(isset($_GET['email']))
        {
            $email=base64_decode($_GET['email']);
            $this->session->set_userdata('newexhibitor',$email);
            redirect('Login');
        }
        ///////////////Site Setting///////////////////////
        if(isset($_GET['link']))
        {
            $email=base64_decode($_GET['link']);
            $this->Login_model->update_active_status($email);
            $this->data['email']=$email;
            $strFrom="info@allintheloop.com";
            $this->email->from($strFrom, 'AllInTheLoop');
            $this->email->to($email);
            $this->email->subject('Welcome Email');
            $msg=file_get_contents('application/views/login/welcome-email.php');
            $this->email->message(html_entity_decode($msg));  
            $this->email->send();
            redirect('Login');
        }
        //$winner = $this->Event_model->setwinnerproduct();
        $site_setting = $this->Login_model->get_site_setting();
        $site_setting = $site_setting[0];
        $this->data['site_title'] = $site_setting['Site_title'];
        $this->data['site_logo'] = $site_setting['Site_logo'];
        $this->data['reserved_right'] = $site_setting['Reserved_right'];

        $user = $this->session->userdata('current_user');

        if ($user) 
        {
    		if (isset($_GET['code'])) 
    		{    
                $adminstripesetting=$this->Event_model->get_admin_stripe_setting();
    			define('CLIENT_ID', $adminstripesetting[0]['client_id']);
    			define('API_KEY', $adminstripesetting[0]['secret_key']);
    			define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');
    			define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');
    			$code = $_GET['code'];
    			$token_request_body = array(
    			    'client_secret' => $adminstripesetting[0]['secret_key'],
    			    'grant_type' => 'authorization_code',
    			    'client_id' => $adminstripesetting[0]['client_id'],
    			    'code' => $code,
    			);  
    			$req = curl_init(TOKEN_URI);
    			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    			curl_setopt($req, CURLOPT_POST, true );
    			curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

    			$respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
    			$resp = json_decode(curl_exec($req), true);
    			curl_close($req);
               
                $save['organisor_id']=$user[0]->Id;
                $save['stripe_user_id']=$resp['stripe_user_id'];
                $save['secret_key']=$resp['access_token'];
                $save['public_key']=$resp['stripe_publishable_key'];
                $this->Event_model->save_stripe_data($save);
                $strFrom="noreply@allintheloop.com";
                $this->email->from($strFrom, 'Allintheloop');
                $this->email->to($user[0]->Email);
                $this->email->subject('You  are now connected with Stripe');
                $msg="<p>Dear ".ucfirst($user[0]->Firstname).' '.ucfirst($user[0]->Lastname);
                $msg.="</p><p>Congratulations! You have connected your All In The Loop app with your Stripe account!</p>";
                $msg.="<p>Your funds will be deposited into your Stripe account automatically. Open your Stripe  account  to withdraw into your bank account.</p>";
                $msg.="<p>Warmest Regards,</p>";
                $msg.="<p>The  All In The Loop Team<br/> 
                       (Note: Please do not reply to this email because no one will     receive it     – to contact our team<br/>    
                            please email info@allintheloop.com)</p>";
                $msg.='<p><img height="55" width="317" src="https://www.allintheloop.net/assets/images/all-in-loop-logo.png"></p>';
                $msg.='<p><a rel="noreferrer" target="_blank" href="http://www.allintheloop.com/">www.allintheloop.com</a></p>';
                $msg.='<p style="color:gray;"><span>The
                information transmitted is intended only for the person or entity to
                which it is addressed and may contain confidential and/or privileged
                material. Any review, re-transmission, dissemination or other use of,
                or taking of any action in reliance upon, this information by persons
                or entities other than the intended recipient is prohibited. If you
                received this in error, please contact the sender and delete the
                material from any computer.</span></p>';
                $msg.='<p style="color:gray;"><span>Whilst
                all reasonable care has been taken to avoid the transmission of
                viruses, it is the responsibility of the recipient to ensure that the
                onward transmission, opening or use of this message and any
                attachments will not adversely affect its systems or data. No
                responsibility is accepted by All In The Loop in this regard and the
                recipient should carry out such virus and other checks as it
                considers appropriate.</span></p>';
                $this->email->message($msg);  
                $this->email->send();
    			//$data['access_token']=$resp['access_token'];
    			//$data['refresh_token']=$resp['refresh_token'];
    			//$data['stripe_publishable_key']=$resp['stripe_publishable_key'];
    			//$data['stripe_user_id']=$resp['stripe_user_id'];
    			//$data['user_id']=$user[0]->Organisor_id;	
    			//$this->Event_model->savestripe_data($data);
                if($this->session->userdata('pageurl')!="")
                {
                    $pageurl=$this->session->userdata('pageurl');
                    $this->session->unset_userdata('pageurl');
                    redirect($pageurl);        
                }
    		} 
            redirect('Dashboard');   
        }
        $check_error = $this->session->userdata('invalid_cred');
        if (!empty($check_error)) 
        {
            $this->data['error'] = "Invalid Credentials";
            $this->session->unset_userdata('invalid_cred');
        } 
        elseif($this->session->userdata('lock_acc')!="")
        {
            
            $this->data['error'] = "Your account is locked, please contact admin.";
            $this->session->unset_userdata('lock_acc');
        }
        elseif($this->session->userdata('success')!="")
        {
            
            $this->data['success'] = $this->session->userdata('success');
            $this->session->unset_userdata('success');
        }
        else 
        {
            if($this->session->userdata('inactive')!="")
            {
                $this->data['error'] = "Please contact sales to access your account";
                $this->session->unset_userdata('inactive');
            }
            else
            {
                $this->data['error'] = "";
            }
        }
        $this->template->write('pagetitle', 'Login');
        $this->template->write_view('content', 'login/index', $this->data, true);
        $this->template->write_view('js', 'login/js', $this->data, true);
        $this->template->render();
    }
    public function exibitorsetpassword($id,$code_email,$sno)
    {
        $eid=base64_decode(urldecode($id));
        $email=base64_decode(urldecode($code_email));
        $stand_nubmer=base64_decode(urldecode($sno));
        $exibitordata=$this->Setting_model->check_exsist_invited_exibitor($eid,$email);
        if(count($exibitordata) > 0)
        {
            $site_setting = $this->Login_model->get_site_setting();
            $site_setting = $site_setting[0];
            $this->data['site_title'] = $site_setting['Site_title'];
            $this->data['site_logo'] = $site_setting['Site_logo'];
            $this->data['reserved_right'] = $site_setting['Reserved_right'];
            $this->data['email']=$email;
            $this->data['event_id']=$eid;
            $this->data['stand_nubmer']=$stand_nubmer;
            $this->template->write('pagetitle', 'Set Password');
            $this->template->write_view('content', 'login/set_password_view', $this->data, true);
            $this->template->write_view('js', 'login/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            redirect(base_url().'Login');
        }
    }
    public function set_exhibitor_password()
    {
        $exibitordata=$this->Setting_model->check_exsist_invited_exibitor($this->input->post('event_id'),$this->input->post('exhibitor_email'));
        $ex_id=$this->Add_attendee_model->add_exhibitor_user($this->input->post());
        $login_data['username']=$this->input->post('exhibitor_email');
        $login_data['password']=$this->input->post('new_confirm_password');
        $logindetails['current_user'] = $this->Login_model->check_login($login_data);
        $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
        $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
        $logindetails['acc_name']=$acc_name;
        $this->session->set_userdata($logindetails);
        if(empty($ex_id))
        {
            redirect(base_url().'Exhibitor_portal/add/'.$this->input->post('event_id'));
        }
        else
        {
            redirect(base_url().'Exhibitor_portal/edit/'.$this->input->post('event_id').'/'.$ex_id);   
        }
    }

    /*public function cron_test_ssh()
    {
        $insert['email'] = 'Test Cron SSH'.date('Y-m-d H:i:s');
        $this->db->insert('tmp_user_session',$insert);
    }*/


    public function cropwinnerproduct()
    {   
        //$insert['email'] = 'Test Cron Cropwinnerproduct'.date('Y-m-d H:i:s') .$_SERVER['REMOTE_ADDR'];
        //$this->db->insert('tmp_user_session',$insert);
        
        $res=$this->Event_model->setwinnerproduct();
        if(!empty($res)){
            
            foreach ($res as $key => $value) 
            {
                $event_name = $this->Settings_model->event_name($value['event_id']);
                $obj = new Gcm($value['event_id']);
                $cart_data['product_id'] = $value['product_id'];
                $cart_data['user_id'] = $value['user_id'];
                $cart_data['quantity'] = 1;
                $cart_data['time'] = date('Y-m-d h:i:s');
                $cart_data['price'] = $value['price'];
                $cart_data['price_type'] = $value['auctionType'];
                $this->Product_model->insertCartDetails($cart_data);
                if($value['device'] == "Iphone")
                {
                    $msg =  "Congratulations! You won ".$value['name'];
                    $extra['message_id'] = $value['product_id'];
                    $extra['message_type'] = 'Won Auction';
                    $extra['event'] = $event_name;
                    $extra['title'] = "Won Auction";
                    $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                }
                else
                {
                    $msg['title'] = "Congratulations! You won ".$value['name'];
                    $msg['message'] = "Tap here to Checkout ";
       
                    $extra['message_id'] = $value['product_id'];
                    $extra['message_type'] = 'Won Auction';
                    $extra['event'] = $event_name;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result = $obj->send_notification($value['gcm_id'],$msg,$extra);
                }
            }
        }
       
    }

   /* public function check() 
    { 
        $logindetails['current_user'] = $this->Login_model->check_login($this->input->post());
        $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
        $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
        $logindetails['acc_name']=$acc_name;
        if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
        {
            $this->session->unset_userdata('current_user');
            if($logindetails['current_user']=="inactive")
                $error['inactive'] = array("error" => "login");
            else        
                $error['invalid_cred'] = array("error" => "login");

            $this->session->set_userdata($error);
            redirect('login');
        } 
        else 
        {
            if($logindetails['current_user'][0]->Rid=='4')
            {
                $error['invalid_cred'] = array("error" => "login");
                $this->session->set_userdata($error);
                redirect('login');
            }
            if($logindetails['current_user'][0]->Rid=='3')
            {   

                $superorgdata=$this->Event_model->check_demo_app_creted($logindetails['current_user'][0]->Email);
                if($superorgdata[0]['demo_app_created']=='0' && $superorgdata[0]['autodemoapp_status']=='1')
                {
                    $org_id=$logindetails['current_user'][0]->Id;
                    $copy_eid='426';
                    $eventdata['event_array']=$this->Event_model->get_copy_event_data_by_event_id($copy_eid);
                    unset($eventdata['event_array']['Id']);
                    $eventdata['event_array']['Organisor_id']=$org_id;
                    $eventdata['event_array']['Event_name']=$eventdata['event_array']['Event_name'].substr(uniqid(),0,4);
                    $eventdata['event_array']['Subdomain']=substr($logindetails['current_user'][0]->Firstname,0,3).substr($logindetails['current_user'][0]->Lastname,0,3).uniqid();
                    $eventdata['event_array']['secure_key']=substr($eventdata['event_array']['Subdomain'],0,3).substr(uniqid(),0,4);
                    $eventdata['event_array']['launch_your_app']='0';
                    $eventdata['event_array']['islaunchapp']='0';
                    $eventdata['event_array']['Images']=json_decode($eventdata['event_array']['Images']);
                    $event_id=$this->Event_model->add_admin_event($eventdata);
                    $role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
                    $this->load->model('Role_management_model');
                    foreach ($role_menu as $key=>$value)
                    {    
                        $role=$this->Role_management_model->insertdefultrole($event_id,3,$value->id);
                        $role=$this->Role_management_model->insertdefultrole($event_id,95,$value->id);
                        $role=$this->Role_management_model->insertdefultrole($event_id,5,$value->id);
                    }
                    $role=$this->Event_model->eventaddrelation($event_id,$org_id,$org_id,3);
                    $fun_data=$this->Event_model->get_all_fun_setting_data($copy_eid,$event_id);
                    $slideshow_data=$this->Event_model->get_all_fun_slideshow_data($copy_eid);
                    if(count($slideshow_data) > 0){
                        array_walk_recursive($slideshow_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('slideshow',$slideshow_data);
                    }
                    $currency_data=$this->Event_model->get_all_fun_currency_data($copy_eid,$event_id);
                    $menu_data=$this->Event_model->get_all_home_screen_tabs_data($copy_eid);
                    if(count($menu_data) > 0){
                        array_walk_recursive($menu_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('event_menu',$menu_data);
                    }
                    $adver_data=$this->Event_model->get_all_advertising_data($copy_eid);
                    if(count($adver_data) > 0){
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'created_date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('advertising',$adver_data);
                    }
                    $maps_data=$this->Event_model->get_all_map_data($copy_eid);
                    if(count($maps_data) > 0){
                        array_walk_recursive($maps_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($maps_data, array($this,'update_somthing'),array('matchkey'=>'Organizer_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('map',$maps_data);
                    }
                    $document_data=$this->Event_model->get_and_save_all_document_data($copy_eid,$event_id);
                    $social_data=$this->Event_model->get_all_social_data($copy_eid);
                    if(count($social_data) > 0){
                        array_walk_recursive($social_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($social_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('social',$social_data);
                    }
                    $cms_data=$this->Event_model->get_all_cms_data($copy_eid);
                    if(count($cms_data) > 0){
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Created_date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('cms',$cms_data);
                    }
                    $twitter_data=$this->Event_model->get_all_twitter_feed_data($copy_eid);
                    if(count($twitter_data) > 0){
                        array_walk_recursive($twitter_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        array_walk_recursive($twitter_data, array($this,'update_somthing'),array('matchkey'=>'date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('event_hashtags',$twitter_data);
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,4);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    }
                    $event_attendee_data=$this->Event_model->get_all_event_attendee_data($copy_eid);
                    $event_view=$this->Event_model->get_default_view_for_attendee($event_id);
                    if(count($event_attendee_data) > 0){
                        array_walk_recursive($event_attendee_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($event_attendee_data, array($this,'update_somthing'),array('matchkey'=>'views_id','newval'=>$event_view[0]['view_id']));
                        $this->Event_model->save_data_databasetables('event_attendee',$event_attendee_data);
                    }
                    $column_data=$this->Event_model->get_all_custom_column_data($copy_eid);
                    if(count($column_data) > 0){
                        array_walk_recursive($column_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('custom_column',$column_data);
                    }
                    $notes_data=$this->Event_model->get_all_attendee_notes_data($copy_eid);
                    if(count($notes_data) > 0){
                        array_walk_recursive($notes_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($notes_data, array($this,'update_somthing'),array('matchkey'=>'Created_at','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('attendee_notes',$notes_data);
                    }
                    $contact_data=$this->Event_model->get_all_attendee_share_contact($copy_eid);
                    if(count($contact_data) > 0){
                        array_walk_recursive($contact_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        array_walk_recursive($contact_data, array($this,'update_somthing'),array('matchkey'=>'share_contact_datetime','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('attendee_share_contact',$contact_data);
                    }
                    $invitation_data=$this->Event_model->get_all_attendee_invitation_data($copy_eid);
                    if(count($invitation_data) > 0){
                        array_walk_recursive($invitation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('attendee_invitation',$invitation_data);
                    }
                    $temp_invitation_data=$this->Event_model->get_all_attendee_temp_invitation_data($copy_eid);
                    if(count($temp_invitation_data) > 0){
                        array_walk_recursive($temp_invitation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('temp_attendee_invitation',$temp_invitation_data);
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,6);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    }
                    $exibitor_data=$this->Event_model->get_all_exibitor_data($copy_eid,NULL);
                    if(count($exibitor_data) > 0){
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'et_id','newval'=>NULL));
                        $this->Event_model->save_data_databasetables('exibitor',$exibitor_data);
                    }
                    $exibitor_type=$this->Event_model->get_all_exibitor_type_data_by_event($copy_eid);
                    foreach ($exibitor_type as $key => $value) {
                        $exibitor_data=$this->Event_model->get_all_exibitor_data($copy_eid,$value['type_id']);
                        $et_id=$this->Event_model->add_exibitor_type_data($value,$event_id);
                        if(count($exibitor_data) > 0){
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'et_id','newval'=>$et_id));
                          $this->Event_model->save_data_databasetables('exibitor',$exibitor_data);
                        }
                    }
                    $sponsors_data=$this->Event_model->get_all_sponsors_data($copy_eid,NULL);
                    if(count($sponsors_data) > 0){
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'st_id','newval'=>NULL));
                        $this->Event_model->save_data_databasetables('sponsors',$sponsors_data);
                    }
                    $sponsors_type=$this->Event_model->get_all_sponsors_type_data($copy_eid);
                    foreach ($sponsors_type as $key => $value) {
                        $sponsors_data=$this->Event_model->get_all_sponsors_data($copy_eid,$value['type_id']);
                        $st_id=$this->Event_model->add_sponsors_type_data($value,$event_id);
                        if(count($sponsors_data) > 0){
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'st_id','newval'=>$st_id));
                          $this->Event_model->save_data_databasetables('sponsors',$sponsors_data);
                        }
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,7);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    } 
                    $survey_category=$this->Event_model->get_survey_category_data($copy_eid);
                    foreach ($survey_category as $key => $value) 
                    {
                        $this->Event_model->duplicate_all_suvery_category_data($value,$copy_eid,$event_id,$org_id);
                    }
                    // $survey_data=$this->Event_model->get_all_survey_data($copy_eid);
                    // if(count($survey_data) > 0){
                    //     array_walk_recursive($survey_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                    //     $this->Event_model->save_data_databasetables('survey_data',$survey_data);
                    // }
                    // $Question_data=$this->Event_model->get_all_survey_Question_data($copy_eid);
                    // if(count($Question_data) > 0){
                    //     array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                    //     array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                    //     array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'created_date','newval'=>date('Y-m-d H:i:s')));
                    //     $this->Event_model->save_data_databasetables('survey',$Question_data);
                    // }
                    $presentation_data=$this->Event_model->get_all_Presentation_data($copy_eid);
                    $newpresenatation_data=$presentation_data;
                    if(count($newpresenatation_data) > 0){
                        array_walk_recursive($newpresenatation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($newpresenatation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                    }
                    foreach ($presentation_data as $key => $value) {
                        $slide=json_decode($value['Images']);
                        foreach ($slide as $key1 => $value1) {
                          $ext = pathinfo($value1, PATHINFO_EXTENSION);
                          if(empty($ext))
                          {
                            $q_id=$this->Event_model->get_survey_question_id($value1,$event_id);
                            if(!empty($q_id[0]['Id']))
                              $slide[$key1]=$q_id[0]['Id'];
                          }
                        }
                        $newpresenatation_data[$key]['Images']=json_encode($slide);
                    }
                    if(count($newpresenatation_data) > 0){
                        $this->Event_model->save_data_databasetables('presentation',$newpresenatation_data);
                    }
                    $this->Event_model->save_all_agenda($copy_eid,$event_id,'0',array('2'),$org_id);

                    $qa_session_data=$this->Event_model->get_qa_session_data($copy_eid);
                    if(count($qa_session_data) > 0){
                        array_walk_recursive($qa_session_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('qa_session',$qa_session_data);
                    }
                    $gamification_header_data=$this->Event_model->get_all_gamification_header_data($copy_eid);
                    if(count($gamification_header_data) > 0){
                        array_walk_recursive($gamification_header_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_header',$gamification_header_data);
                    }

                    $gamification_rank_color_data=$this->Event_model->get_all_gamification_rank_color_data($copy_eid);
                    if(count($gamification_rank_color_data) > 0){
                        array_walk_recursive($gamification_rank_color_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_rank_color',$gamification_rank_color_data);
                    }

                    $gamification_rank_point_data=$this->Event_model->get_all_gamification_rank_point_data($copy_eid);
                    if(count($gamification_rank_point_data) > 0){
                        array_walk_recursive($gamification_rank_point_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_rank_point',$gamification_rank_point_data);
                    }

                    $this->Event_model->update_auto_creted_event_status($superorgdata[0]['Id']);
                }
            }
                
            $this->session->set_userdata($logindetails);
            if( $this->session->userdata('newexhibitor')==$logindetails['current_user'][0]->Email)
            {
                redirect('profile/update/'.$logindetails['current_user'][0] ->Id);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }*/
     public function check() 
    {
      
        $logindetails['current_user'] = $this->Login_model->check_login($this->input->post());
        $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
        $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
        $logindetails['acc_name']=$acc_name;
        $attempts = $this->Login_model->getUserAttempts($logindetails['current_user'][0]->Id,'login_attempts');
        $current_date = date('Y-m-d');

        if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
        {   
            
            if($logindetails['current_user']=="inactive")
                $error['inactive'] = array("error" => "login");
            else   
            {
                $user = $this->Login_model->getUser($this->input->post('username'));
                $attempts = $this->Login_model->getUserAttempts($user['Id'],'login_attempts');

                if(!empty($attempts))
                {
                    $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                    $current_date = date('Y-m-d');
                    if( $compare_date == $current_date && $attempts['value'] > 2)
                    {
                       $error['lock_acc'] = array("error" => "login");
                    }
                    else
                    {
                        $error['invalid_cred'] = array("error" => "login");
                    } 
                    $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
                }
                elseif(!empty($user))
                {
                    $this->Login_model->insertUserAttempts($user['Id'],'login_attempts');
                    $error['invalid_cred'] = array("error" => "login");
                }
                else
                {
                    $error['invalid_cred'] = array("error" => "login");
                }     
                
                $this->session->unset_userdata('current_user');
                
            }
            $this->session->set_userdata($error);
            redirect('Login');
        } 
        elseif(!empty($attempts) && ((date('Y-m-d',strtotime($attempts['created_date']))) == $current_date) && $attempts['value'] > 2)
        {
            $error['lock_acc'] = array("error" => "login");
            $this->session->set_userdata($error);
            redirect('Login');
        }
        else{


            /*$data = file_get_contents('http://www.geoplugin.net/xml.gp?ip='.$this->input->ip_address());
            $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
            $location_details = json_encode($xml);*/
            $array_log['user_id'] = $logindetails['current_user'][0]->Id;
            $array_log['ip_address'] = $this->input->ip_address();
            $array_log['location'] = "";
            $login_log = $this->Login_model->add_login_log($array_log);

            $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],0);
            if($logindetails['current_user'][0]->Rid=='3')
            {
                $superorgdata=$this->Event_model->check_demo_app_creted($logindetails['current_user'][0]->Email);
                //$this->session->set_userdata($logindetails);

                if($superorgdata[0]['demo_app_created']=='0' && $superorgdata[0]['autodemoapp_status']=='1')
                {
                    $org_id=$logindetails['current_user'][0]->Id;
                    $copy_eid='426';
                    $eventdata['event_array']=$this->Event_model->get_copy_event_data_by_event_id($copy_eid);
                    unset($eventdata['event_array']['Id']);
                    $eventdata['event_array']['Organisor_id']=$org_id;
                    $eventdata['event_array']['Start_date']=date("Y-m-d");
                    $eventdata['event_array']['End_date'] = date("Y-m-d", strtotime(date("Y-m-d")."+1 years"));
                    $eventdata['event_array']['Event_name']=$eventdata['event_array']['Event_name'].substr(uniqid(),0,4);
                    $eventdata['event_array']['Subdomain']=substr($logindetails['current_user'][0]->Firstname,0,3).substr($logindetails['current_user'][0]->Lastname,0,3).uniqid();
                    $eventdata['event_array']['secure_key']=substr($eventdata['event_array']['Subdomain'],0,3).substr(uniqid(),0,4);
                    $eventdata['event_array']['launch_your_app']='0';
                    $eventdata['event_array']['islaunchapp']='0';
                    $eventdata['event_array']['Images']=json_decode($eventdata['event_array']['Images']);
                    $event_id=$this->Event_model->add_admin_event($eventdata);
                    $logindetails['current_user'][0]->Event_id = $event_id;
                    $this->session->set_userdata($logindetails);
                    $role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
                    $this->load->model('Role_management_model');
                    foreach ($role_menu as $key=>$value)
                    {    
                        $role=$this->Role_management_model->insertdefultrole($event_id,3,$value->id);
                        $role=$this->Role_management_model->insertdefultrole($event_id,95,$value->id);
                        $role=$this->Role_management_model->insertdefultrole($event_id,5,$value->id);
                    }
                    $role=$this->Event_model->eventaddrelation($event_id,$org_id,$org_id,3);
                    $fun_data=$this->Event_model->get_all_fun_setting_data($copy_eid,$event_id);
                    $slideshow_data=$this->Event_model->get_all_fun_slideshow_data($copy_eid);
                    if(count($slideshow_data) > 0){
                        array_walk_recursive($slideshow_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('slideshow',$slideshow_data);
                    }
                    $currency_data=$this->Event_model->get_all_fun_currency_data($copy_eid,$event_id);
                    $menu_data=$this->Event_model->get_all_home_screen_tabs_data($copy_eid);
                    if(count($menu_data) > 0){
                        array_walk_recursive($menu_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('event_menu',$menu_data);
                    }
                    $adver_data=$this->Event_model->get_all_advertising_data($copy_eid);
                    if(count($adver_data) > 0){
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($adver_data, array($this,'update_somthing'),array('matchkey'=>'created_date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('advertising',$adver_data);
                    }
                    $maps_data=$this->Event_model->get_all_map_data($copy_eid);
                    if(count($maps_data) > 0){
                        array_walk_recursive($maps_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($maps_data, array($this,'update_somthing'),array('matchkey'=>'Organizer_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('map',$maps_data);
                    }
                    $document_data=$this->Event_model->get_and_save_all_document_data($copy_eid,$event_id);
                    $social_data=$this->Event_model->get_all_social_data($copy_eid);
                    if(count($social_data) > 0){
                        array_walk_recursive($social_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($social_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('social',$social_data);
                    }
                    $cms_data=$this->Event_model->get_all_cms_data($copy_eid);
                    if(count($cms_data) > 0){
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($cms_data, array($this,'update_somthing'),array('matchkey'=>'Created_date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('cms',$cms_data);
                    }
                    $twitter_data=$this->Event_model->get_all_twitter_feed_data($copy_eid);
                    if(count($twitter_data) > 0){
                        array_walk_recursive($twitter_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        array_walk_recursive($twitter_data, array($this,'update_somthing'),array('matchkey'=>'date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('event_hashtags',$twitter_data);
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,4);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    }
                    $event_attendee_data=$this->Event_model->get_all_event_attendee_data($copy_eid);
                    $event_view=$this->Event_model->get_default_view_for_attendee($event_id);
                    if(count($event_attendee_data) > 0){
                        array_walk_recursive($event_attendee_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($event_attendee_data, array($this,'update_somthing'),array('matchkey'=>'views_id','newval'=>$event_view[0]['view_id']));
                        $this->Event_model->save_data_databasetables('event_attendee',$event_attendee_data);
                    }
                    $column_data=$this->Event_model->get_all_custom_column_data($copy_eid);
                    if(count($column_data) > 0){
                        array_walk_recursive($column_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('custom_column',$column_data);
                    }
                    $notes_data=$this->Event_model->get_all_attendee_notes_data($copy_eid);
                    if(count($notes_data) > 0){
                        array_walk_recursive($notes_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($notes_data, array($this,'update_somthing'),array('matchkey'=>'Created_at','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('attendee_notes',$notes_data);
                    }
                    $contact_data=$this->Event_model->get_all_attendee_share_contact($copy_eid);
                    if(count($contact_data) > 0){
                        array_walk_recursive($contact_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        array_walk_recursive($contact_data, array($this,'update_somthing'),array('matchkey'=>'share_contact_datetime','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('attendee_share_contact',$contact_data);
                    }
                    $invitation_data=$this->Event_model->get_all_attendee_invitation_data($copy_eid);
                    if(count($invitation_data) > 0){
                        array_walk_recursive($invitation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('attendee_invitation',$invitation_data);
                    }
                    $temp_invitation_data=$this->Event_model->get_all_attendee_temp_invitation_data($copy_eid);
                    if(count($temp_invitation_data) > 0){
                        array_walk_recursive($temp_invitation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('temp_attendee_invitation',$temp_invitation_data);
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,6);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    }
                    $exibitor_data=$this->Event_model->get_all_exibitor_data($copy_eid,NULL);
                    if(count($exibitor_data) > 0){
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'et_id','newval'=>NULL));
                        $this->Event_model->save_data_databasetables('exibitor',$exibitor_data);
                    }
                    $exibitor_type=$this->Event_model->get_all_exibitor_type_data_by_event($copy_eid);
                    foreach ($exibitor_type as $key => $value) {
                        $exibitor_data=$this->Event_model->get_all_exibitor_data($copy_eid,$value['type_id']);
                        $et_id=$this->Event_model->add_exibitor_type_data($value,$event_id);
                        if(count($exibitor_data) > 0){
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                          array_walk_recursive($exibitor_data, array($this,'update_somthing'),array('matchkey'=>'et_id','newval'=>$et_id));
                          $this->Event_model->save_data_databasetables('exibitor',$exibitor_data);
                        }
                    }
                    $sponsors_data=$this->Event_model->get_all_sponsors_data($copy_eid,NULL);
                    if(count($sponsors_data) > 0){
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'st_id','newval'=>NULL));
                        $this->Event_model->save_data_databasetables('sponsors',$sponsors_data);
                    } 
                    $sponsors_type=$this->Event_model->get_all_sponsors_type_data($copy_eid);
                    foreach ($sponsors_type as $key => $value) {
                        $sponsors_data=$this->Event_model->get_all_sponsors_data($copy_eid,$value['type_id']);
                        $st_id=$this->Event_model->add_sponsors_type_data($value,$event_id);
                        if(count($sponsors_data) > 0){
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                          array_walk_recursive($sponsors_data, array($this,'update_somthing'),array('matchkey'=>'st_id','newval'=>$st_id));
                          $this->Event_model->save_data_databasetables('sponsors',$sponsors_data);
                        }
                    }
                    $relation_data=$this->Event_model->get_all_relation_data($copy_eid,7);
                    if(count($relation_data) > 0){
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($relation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        $this->Event_model->save_data_databasetables('relation_event_user',$relation_data);
                    }
                    $survey_category=$this->Event_model->get_survey_category_data($copy_eid);
                    foreach ($survey_category as $key => $value) 
                    {
                        $this->Event_model->duplicate_all_suvery_category_data($value,$copy_eid,$event_id,$org_id);
                    }
                    /*$survey_data=$this->Event_model->get_all_survey_data($copy_eid);
                    if(count($survey_data) > 0){
                        array_walk_recursive($survey_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('survey_data',$survey_data);
                    }
                    $Question_data=$this->Event_model->get_all_survey_Question_data($copy_eid);
                    if(count($Question_data) > 0){
                        array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                        array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'created_date','newval'=>date('Y-m-d H:i:s')));
                        $this->Event_model->save_data_databasetables('survey',$Question_data);
                    }*/
                    $presentation_data=$this->Event_model->get_all_Presentation_data($copy_eid);
                    $newpresenatation_data=$presentation_data;
                    if(count($newpresenatation_data) > 0){
                        array_walk_recursive($newpresenatation_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        array_walk_recursive($newpresenatation_data, array($this,'update_somthing'),array('matchkey'=>'Organisor_id','newval'=>$org_id));
                    }
                    foreach ($presentation_data as $key => $value) {
                        $slide=json_decode($value['Images']);
                        foreach ($slide as $key1 => $value1) {
                          $ext = pathinfo($value1, PATHINFO_EXTENSION);
                          if(empty($ext))
                          {
                            $q_id=$this->Event_model->get_survey_question_id($value1,$event_id);
                            if(!empty($q_id[0]['Id']))
                              $slide[$key1]=$q_id[0]['Id'];
                          }
                        }
                        $newpresenatation_data[$key]['Images']=json_encode($slide);
                    }
                    if(count($newpresenatation_data) > 0){
                        $this->Event_model->save_data_databasetables('presentation',$newpresenatation_data);
                    }
                    $this->Event_model->save_all_agenda($copy_eid,$event_id,'0',array('2'),$org_id);

                    $qa_session_data=$this->Event_model->get_qa_session_data($copy_eid);
                    if(count($qa_session_data) > 0){
                        array_walk_recursive($qa_session_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('qa_session',$qa_session_data);
                    }

                    $gamification_header_data=$this->Event_model->get_all_gamification_header_data($copy_eid);
                    if(count($gamification_header_data) > 0){
                        array_walk_recursive($gamification_header_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_header',$gamification_header_data);
                    }

                    $gamification_rank_color_data=$this->Event_model->get_all_gamification_rank_color_data($copy_eid);
                    if(count($gamification_rank_color_data) > 0){
                        array_walk_recursive($gamification_rank_color_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_rank_color',$gamification_rank_color_data);
                    }

                    $gamification_rank_point_data=$this->Event_model->get_all_gamification_rank_point_data($copy_eid);
                    if(count($gamification_rank_point_data) > 0){
                        array_walk_recursive($gamification_rank_point_data, array($this,'update_somthing'),array('matchkey'=>'event_id','newval'=>$event_id));
                        $this->Event_model->save_data_databasetables('game_rank_point',$gamification_rank_point_data);
                    }

                   $this->Event_model->update_auto_creted_event_status($superorgdata[0]['Id']);
                }
            }
            $this->session->set_userdata($logindetails);

            $this->session->set_userdata('loggedin',uniqid());
            // echo "<pre>B"; print_r($logindetails['current_user'][0]->Email); exit();
            if($this->session->userdata('newexhibitor')==$logindetails['current_user'][0]->Email)
            {
                redirect('Profile/update/'.$logindetails['current_user'][0] ->Id);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    public function update_somthing(&$item, $key,$arr) {    
      if($key==$arr['matchkey'])         
        $item = $arr['newval']; 
    }
    public function send_queue_mail()
    {   
        //$insert['email'] = 'Send queue mail '.date('Y-m-d H:i:s');
        //$this->db->insert('tmp_user_session',$insert);
        
        //$eventids=$this->Event_model->get_queue_mail_event_id();
        //foreach ($eventids as $key => $value1) {
            //$queue_mail=$this->Event_model->get_queue_mail_list_by_event($value1['event_id']);
            //foreach ($queue_mail as $key => $value) {
                $this->load->library('email');
                $config['protocol']   = 'smtp';
                $config['smtp_host']  = 'smtp.mandrillapp.com';
                $config['smtp_port']  = '587';
                $config['smtp_user']  = 'All In The Loop';
                $config['smtp_pass']  = 'XlRm9AnFNLCaSd9ICOgUQw';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com','AITL');
                $this->email->to('tarun@elsner.com');
                $this->email->subject('SMTP Testing');
                $this->email->message("Testing"); 


                if($this->email->send()) 
                {
                    echo 'sent!';
                }
                else 
                {
                    echo $this->email->print_debugger();
                }

                //if($this->email->send()){
                    //$this->Event_model->delete_in_queue_mail_list($value['queue_id']);
                //} 
            //}
        //}
    }
   /* public function forgot_pas() 
    {
        $is_valid_email = $this->Login_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            echo "error###Invalid Email";die;
        } 
        else 
        {
            $new_pass = $this->generate_password();

            $slug = "FORGOT_PASSWORD";
            $em_template = $this->Setting_model->back_email_template($slug);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = $em_template['Content'];

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $new_pass;
            $msg = preg_replace($patterns, $replacements, $msg);

            $new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $this->Login_model->change_pas($new_pass_array);
            if($em_template['Subject']=="")
            {
                $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $this->email->from('Registration@allintheloop.com', 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject);//'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();

            echo "success###Please check your email for your temporary password.";die;
        }
    }*/
     public function forgot_pas() 
    {
        $is_valid_email = $this->Login_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            echo "error###Invalid Email";die;
        } 
        else 
        {
            $attempts = $this->Login_model->getUserAttempts($is_valid_email[0]->Id,'forgot_password');
            if(!empty($attempts))
            {
                $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                $current_date = date('Y-m-d');
                if( $compare_date == $current_date && $attempts['value'] > 2)
                {
                   echo "error###You have tried 3 times forgot password. Now you can make forgot password request by tommorow.";die; 
                }
            }
            else
            {
                $this->Login_model->insertUserAttempts($is_valid_email[0]->Id,'forgot_password');
            }
            $new_pass = $this->generate_password();

            $slug = "FORGOT_PASSWORD";
            $em_template = $this->Setting_model->back_email_template($slug);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = $em_template['Content'];

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = "<a href='".base_url()."Login/reset_password/".$new_pass."'>Click here to change your password.</a> ";
            $msg = preg_replace($patterns, $replacements, $msg);
            $this->Login_model->insertUserAttempts($is_valid_email[0]->Id,'reset_password',$new_pass);
            /*$new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $this->Login_model->change_pas($new_pass_array);*/
            if($em_template['Subject']=="")
            {
                $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $this->email->from($strFrom, 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject);//'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message(html_entity_decode($msg));
            $this->email->send();
            if(!empty($attempts))
                $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
            echo "success###We have sent a mail to your email address.";die;
        }
    }
    public function reset_password($token="",$role="")
    {
        $error = NULL;
        $check = $this->Login_model->checkToken($token);
        if($role == 'lead')
        {
            $user = $this->Login_model->getUserFromToken($token);
            $this->data['email'] = $user['Email'];
            if(empty($user))
                redirect(base_url().'Login');
        }
        if($this->input->post())
        {   
            date_default_timezone_set('UTC');
            $check = $this->Login_model->checkToken($token);
            $data['email'] = $this->input->post('email');
            $user = $this->Login_model->check_user_email($data);
            $attempts = $this->Login_model->getUserAttempts($user[0]->Id,'reset_password');
            $date = (int) strtotime(date('Y-m-d H:i:s'));

            $compare_date = (int) strtotime($attempts['created_date']);
            $diff = round(($date - $compare_date)/60,2);
            if($check == 0 && $role!="lead")
            {
                $error = 'Your token is not valid.';
            }
            else if($diff > 30 && $role!="lead")
            {
                $error = 'Your input is invalid or your time to reset password is finished.';
            }
            else
            {
                $new_pass_array['email'] = $this->input->post('email');
                $new_pass_array['new_pass'] = $this->input->post('password');
                $this->Login_model->change_pas($new_pass_array);
                $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],"");
                $this->session->set_userdata('success','Your password changed successfully. Now you can do login');
                redirect(base_url().'Login');
            }
        }
        if($error!=NULL)
        {
            $this->data['error'] = $error;
        }
        $this->data['token'] = $token;
        $this->template->write('pagetitle', 'Reset Password');
        $this->template->write_view('content', 'login/reset_password', $this->data, true);
        $this->template->write_view('js', 'login/js', $this->data, true);
        $this->template->render();
        
    }
    public function superadminlogin($oid)
    {
        $userdetails=$this->Login_model->get_user_details_by_user_id($oid);
        $logindetails['current_user'] = $this->Login_model->super_admin_user_account_login($userdetails[0]['Email']);
        $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
        $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
        $logindetails['acc_name']=$acc_name;
        if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
        {
            $this->session->unset_userdata('current_user');
            if($logindetails['current_user']=="inactive")
                $error['inactive'] = array("error" => "login");
            else        
                $error['invalid_cred'] = array("error" => "login");
            $this->session->set_userdata($error);
            redirect('Login');
        } 
        else 
        {
            $this->session->set_userdata($logindetails);
            redirect(base_url().'Dashboard');
        }
    }
    public function generate_password() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 9; $i++) 
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public function logout() 
    {   
        unset($_SERVER['HTTP_COOKIE']);

        $this->session->unset_userdata('current_user');
        $this->session->unset_userdata('invalid_cred');
        $this->session->sess_destroy();
        $this->security->xss_clean();
        $this->load->helper('cookie');
        delete_cookie("ci_session");
        unset($_SESSION['csrf_token']);
        redirect('Login');
    }
     public function lead_reset_pass($email) 
    {
        $is_valid_email = $this->Login_model->check_user_email($email);
        if (empty($is_valid_email)) 
        {
            echo "error###Invalid Email";die;
        } 
        else 
        {
            $attempts = $this->Login_model->getUserAttempts($is_valid_email[0]->Id,'forgot_password');
            if(!empty($attempts))
            {
                $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                $current_date = date('Y-m-d');
                if( $compare_date == $current_date && $attempts['value'] > 2)
                {
                   echo "error###You have tried 3 times forgot password. Now you can make forgot password request by tommorow.";die; 
                }
            }
            else
            {
                $this->Login_model->insertUserAttempts($is_valid_email[0]->Id,'forgot_password');
            }
            $new_pass = $this->generate_password();

            $slug = "FORGOT_PASSWORD";
            $em_template = $this->Setting_model->back_email_template($slug);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = $em_template['Content'];

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = "<a href='".base_url()."Login/reset_password/".$new_pass."'>Click here to change your password.</a> ";
            $msg = preg_replace($patterns, $replacements, $msg);
            $this->Login_model->insertUserAttempts($is_valid_email[0]->Id,'reset_password',$new_pass);
            /*$new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $this->Login_model->change_pas($new_pass_array);*/
            if($em_template['Subject']=="")
            {
                $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $this->email->from($strFrom, 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject);//'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message(html_entity_decode($msg));
            $this->email->send();
            if(!empty($attempts))
                $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
            echo "success###We have sent a mail to your email address.";die;
        }
    }
}
