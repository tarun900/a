<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lead_retrieval_admin extends FrontendController
{
	function __construct()
	{
		$this->data['pagetitle'] = 'Your Lead';
		$this->data['smalltitle'] = 'Upload or individually add your Lead list on this page.';
		$this->data['page_edit_title'] = 'edit';
		$this->data['breadcrumb'] = 'Lead Retrival';
		parent::__construct($this->data);
		$this->load->model('Formbuilder_model');
		$this->load->model('Agenda_model');
		$this->load->model('Event_model');
		$this->load->model('Setting_model');
		$this->load->model('Event_template_model');
		$this->load->model('Lead_retrieval_model');
		$this->load->model('Exibitor_survey_model');
		$this->load->library('upload');
		$this->load->library('email');
		$user = $this->session->userdata('current_user');
		$eventid=$this->uri->segment(3);

         $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

		$eventmodule=$this->Event_model->geteventmodulues($eventid);
		$module=json_decode($eventmodule[0]['module_list']);
		if(!in_array('53',$module))
		{
			echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
		}
		$event_templates = $this->Event_model->view_event_by_id($eventid);
		$this->data['Subdomain'] = $event_templates[0]['Subdomain'];

		$event = $this->Event_model->get_module_event($eventid);
		$menu_list = explode(',', $event[0]['checkbox_values']);

		$roledata = $this->Event_model->getUserRole($eventid);
		if(!empty($roledata))
		{
		   $roleid = $roledata[0]->Role_id;
		   $rolename = $roledata[0]->Name;
		   $cnt = 0;
		   $req_mod = ucfirst($this->router->fetch_class());
		   if ($this->data['pagetitle'] == "Your Lead")
		   {
		        $title = "lead_retrieval";
		   }
		   $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
		}
		else
		{
		   $cnt=0;
		}
		if (!empty($user[1]['event_id_selected']))
		{
		   $this->data['event_id_selected'] = $user[1]['event_id_selected'];
		}
		if ($cnt == 1)
		{
		   $this->load->model('Attendee_model');
		   $this->load->model('Profile_model');
		   $this->load->library('session');
		   $roles = $this->Event_model->get_menu_list($roleid,$eventid);
		   $this->data['roles'] = $roles;
		}
		else
		{
		   echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
		}
	}
	public function index($id)
	{
		$event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role; 
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 53);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->data['lead_role'] =  $this->Lead_retrieval_model->get_lead_role($id);		
        $this->data['lead_users'] =  $this->Lead_retrieval_model->getLeadUsers($id);		
        $this->data['my_lead']=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($id,$user[0]->Id);		
        $questions=$this->Exibitor_survey_model->get_all_exibitor_user_questions($id,$user[0]->Id);		
        $this->data['question_list']=$questions;    		
        $this->data['representatives']=$this->Event_model->get_all_representatives($id,$user[0]->Id);		
        $this->data['my_lead']=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($id,$user[0]->Id);		
        $this->data['custom_column']=$this->Lead_retrieval_model->get_all_custom_column_data($event_templates[0]['Id']);

		$this->template->write_view('css', 'lead_retrieval_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'lead_retrieval_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'lead_retrieval_admin/js', true);
        $this->template->render();
	}
	public function save_lead_setting($id)
	{
		$data['event_array']['Id'] = $id;
		if($this->input->post('allow_export_lead_data')=='1')
		{
			$data['event_array']['allow_export_lead_data']='1';
		}
		else
		{
			$data['event_array']['allow_export_lead_data']='0';	
		}
		if($this->input->post('allow_custom_survey_lead')=='1')
		{
			$data['event_array']['allow_custom_survey_lead']='1';
		}
		else
		{
			$data['event_array']['allow_custom_survey_lead']='0';	
		}
        $this->Event_model->update_admin_event($data);
        $this->session->set_flashdata('setting_data', 'Lead Retrieval Setting Saved Successfully.');
        redirect('Lead_retrieval_admin/index/'.$id);
	}
	public function store($id)
	{	
		$create_data['event_id'] = $id;
		$create_data['ask_survey'] = ($this->input->post('ask_survey') == "on") ? '1' : '0';
		$create_data['add_survey'] = ($this->input->post('add_survey') == "on") ? '1' : '0' ;
		$create_data['status'] = '1';
		$create_data['Name'] = $this->input->post('role');
		$create_data['role_type'] = '1';
		$create_data['Active'] = '1';

		$where['event_id'] = $id;
		$where['Name'] = $this->input->post('role');

        $result = $this->Lead_retrieval_model->addRole($create_data,$where);

        if($result['success'])
        {
        	$this->session->set_flashdata('setting_data', 'Role created Successfully.');
        	redirect('Lead_retrieval_admin/index/'.$id);
        }
    	else
    	{
			$this->session->set_flashdata('upload_error_data', "Error - ".$result['msg']);
			redirect('Lead_retrieval_admin/index/'.$id);
    	}
        
	}

	public function update($event_id)
	{
		$update_data['ask_survey'] = ($this->input->post('ask_survey') == "on") ? '1' : '0';
		$update_data['add_survey'] = ($this->input->post('add_survey') == "on") ? '1' : '0' ;
		$update_data['Name'] = $this->input->post('role');

		$where['Id'] = $this->input->post('id');

        $this->Lead_retrieval_model->updateRole($update_data,$where);
        $this->session->set_flashdata('setting_data', 'Role updated Successfully.');
        redirect('Lead_retrieval_admin/index/'.$event_id);
	}

	public function delete($event_id,$item_id)
	{
		$where['Id'] = $item_id;

		$this->Lead_retrieval_model->deleteRole($where);
        $this->session->set_flashdata('setting_data', 'Role deleted Successfully.');
        redirect('Lead_retrieval_admin/index/'.$event_id);

	}

	public function uploadUsers($event_id)
	{
		$event = $this->Event_model->get_admin_event($event_id);
		if (pathinfo($_FILES['csv_files']['name'],PATHINFO_EXTENSION)=="csv")
	      {
	           $file = $_FILES['csv_files']['tmp_name'];
	           $handle = fopen($file, "r");
	           $find_header = 0;
	           while (($datacsv = fgetcsv($handle,0,",")) !== FALSE)
	           {
	                if($find_header!='0' && !empty($datacsv[2]))
	                {
	                     $data['Firstname']=$datacsv[0];
	                     $data['Lastname']=$datacsv[1];
	                     $data['Email']=$datacsv[2];
	                     $data['Title']=$datacsv[3];
	                     $data['Company_name']=$datacsv[4];
	                     $data['no_of_reps']=$datacsv[6];
	                     $data['Password']=md5($datacsv[7]);
	                    
	                     $result = $this->Lead_retrieval_model->add($data,$event_id,$event[0]['Organisor_id'],$datacsv[5]);
	                     if(!$result['result'])
	                     {
	                     	$this->session->set_flashdata('upload_error_data', $result['msg']);
		        			redirect('Lead_retrieval_admin/index/'.$event_id);
		        			break;exit;
	                     }
	                } 
	                $find_header++;     
	           }
		        $this->session->set_flashdata('setting_data', 'Users uploaded successfully.');
		        redirect('Lead_retrieval_admin/index/'.$event_id);
	      }
	      else
	      {
		        $this->session->set_flashdata('upload_error_data', 'Please upload csv file.');
		        redirect('Lead_retrieval_admin/index/'.$event_id);
	      }
	}


	public function download_template_csv($eid)
    {
          $this->load->helper('download');
          $filename = "lead_user.csv";
          $fp = fopen('php://output', 'w');
          $header[] = "First Name";
          $header[] = "Last Name";
          $header[] = "Email Address";
          $header[] = "Title";
          $header[] = "Company";
          $header[] = "Lead Retrieval Role";
          $header[] = "Number of Reps";
          $header[] = "Password";
          
          header('Content-type: application/csv');
          header('Content-Disposition: attachment; filename='.$filename);
          fputcsv($fp, $header);
          $data['First Name']="John";
          $data['Last Name']="Doe";
          $data['Email Address']="john@domain.com";
          $data['Title']="Manager";
          $data['Company']="All In the Loop";
          $data['Lead Retrieval Role']="ROLE 1";
          $data['Number of Reps']="1";
          $data['Password']="123456";
          fputcsv($fp, $data);
    }
	public function delete_lead_user($event_id,$item_id)
	{
		$where['User_id'] = $item_id;
		$where['Event_id'] = $event_id;
		$this->Lead_retrieval_model->deleteLeadUser($where);
        $this->session->set_flashdata('setting_data', 'Lead user deleted Successfully.');
        redirect('Lead_retrieval_admin/index/'.$event_id);
	}
	public function update_lead_user($event_id)
	{	
		$user_update['Firstname'] = $this->input->post('first-name');
		$user_update['Lastname'] = $this->input->post('last-name');
		$user_update['Firstname'] = $this->input->post('title');
		$user_update['Title'] = $this->input->post('title');
		$user_update['Company_name'] = $this->input->post('company-name');
		$user_update['no_of_reps'] = $this->input->post('max_rep');
		if(!empty($this->input->post('new_password')))
		{
			$user_update['Password'] = md5($this->input->post('new_password'));
		}
		$where['Id'] = $this->input->post('uid');
		$this->Lead_retrieval_model->updateLeadUser($where,$user_update);
		$role_update['Role_id'] = $this->input->post('new-role-id');
		unset($where);
		$where['Event_id'] = $event_id;
		$where['User_id']  = $this->input->post('uid');
		$where['Role_id']  = $this->input->post('rid');
		$this->Lead_retrieval_model->updateLeadUserRole($where,$role_update);
		$this->session->set_flashdata('setting_data', 'Lead user updated Successfully.');
        redirect('Lead_retrieval_admin/index/'.$event_id);
	}
	public function add_lead_user($event_id)
	{	
		extract($this->input->post(),EXTR_SKIP);
		if(!empty($email))
		{	
			$event = $this->Event_model->get_admin_event($event_id);
			$data['Firstname']=$firstname;
			$data['Lastname']=$lastname;
			$data['Email']=$email;
			$data['Title']=$title;
			$data['Company_name']=$company_name;
			$data['no_of_reps']=$no_of_reps;
			$data['Password']=($update_password)? md5($update_password) : md5('123456');
			$result = $this->Lead_retrieval_model->add($data,$event_id,$event[0]['Organisor_id'],$role_name);
			if(!$result['result'])
			{
				$this->session->set_flashdata('upload_error_data', $result['msg']);
				redirect('Lead_retrieval_admin/index/'.$event_id);
				exit;
			}
			$this->session->set_flashdata('setting_data', 'Lead User Added successfully.');
	        redirect('Lead_retrieval_admin/index/'.$event_id);
		}
	}
}