<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Client extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'Organizer';
		$this->data['smalltitle'] = 'Organizer Details';
		$this->data['breadcrumb'] = 'Organizer';
		parent::__construct($this->data);
		$this->load->model('Client_model');
		$this->load->model('Profile_model');
        $this->load->model('Setting_model');
	}

	public function index()                 
	{                       
        		         		
		$clients = $this->Client_model->get_client_list();
        $this->data['Clients'] = $clients;
		$this->template->write_view('css', 'client/css', $this->data , true);
		$this->template->write_view('content', 'client/index', $this->data , true);
		$this->template->write_view('js', 'client/js', $this->data , true);
		$this->template->render();
	}
        
    public function edit($id = '0')                 
	{   
            $this->data['clients'] = $this->Client_model->get_client_list($id);
            $this->template->write_view('css', 'client/add_css', $this->data , true);
            $this->template->write_view('content', 'client/add', $this->data , true);
            $this->template->write_view('js', 'client/add_js', $this->data , true);
            $this->template->render();    
	}

	public function add()
	{
            if($this->input->post())
            {
                if($_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.',$_FILES['userfile']['name']);
                    $tempname[0] = $tempname[0].strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0].".".$tempname[1];                
                    $_POST['Logo'] = $_FILES['userfile']['name'];
                    if($this->data['user']->Role_name == 'client')
                    {
                        $this->data['user']->Logo=$_FILES['userfile']['name'];
                    }
                    $config['upload_path'] = './assets/user_files';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '2048';
                    $config['max_height']  = '2048';
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload())
                    {
                            $error = array('error' => $this->upload->display_errors());
                    }
                    else
                    {
                            $data1 = array('upload_data' => $this->upload->data());                        
                    }
                }

                foreach($this->input->post() as $k=>$v)
                {
                    if($k != "password_again" && $k != "idval")
                    {
                        if($k == "zipcode"){ $k = "Postcode";}
                        //if($k == "image_name"){ $k = "Logo";}
                        $k = ucfirst(strtolower($k));
                        $array_add[$k] = $v;
                    }
                }
                
                /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
                $slug = "WELCOME_MSG";                
                $em_template = $this->Setting_model->email_template($slug);
                $em_template = $this->Setting_model->email_template($slug);
                $em_template = $em_template[0];
                $new_pass = $this->input->post('password');
                $name = ($this->input->post('Firstname')==NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
                $msg = html_entity_decode($em_template['Content']);
                $patterns = array();
                $patterns[0] = '/{{name}}/';
                $patterns[1] = '/{{password}}/';
                $patterns[2] = '/{{email}}/';
                $replacements = array();
                $replacements[0] = $name;
                $replacements[1] = $new_pass;
                $replacements[2] = $this->input->post('email');
                $msg = preg_replace($patterns, $replacements, $msg);
                
                $this->email->from('your@example.com', 'Your Name');
                $this->email->to($this->input->post('email')); 
                $this->email->subject('Client Account');
                $this->email->set_mailtype("html");
                $this->email->message($msg);	
                $this->email->send();
                /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////

                
                $category = $this->Client_model->add_client($array_add);
                $this->session->set_flashdata('client_data', 'Added');
                redirect('Client');
                
                
            }
            
            $statelist=$this->Profile_model->statelist();
            $this->data['Statelist'] = $statelist;
            
            $this->template->write_view('css', 'client/add_css', $this->data , true);
            $this->template->write_view('content', 'client/add', $this->data , true);
            $this->template->write_view('js', 'client/add_js', $this->data , true);
            $this->template->render();
	}
        
        public function delete($id)
        {
            $client = $this->Client_model->delete_client($id);
            $this->session->set_flashdata('category_data', 'Deleted');
            redirect('Client');
        }
        
        public function checkemail()
        {            
        if($this->input->post())
        {
            $client = $this->Client_model->checkemail($this->input->post('email'),$this->input->post('idval'),$this->input->post('event_id'));
            if($client)
            {
                echo "error###Email already exist. Please choose another email.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
        }
}
