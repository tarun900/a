<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 
class Appnotitemplate extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Standard Notifications';
          $this->data['smalltitle'] = 'Standard Notifications Template';
          $this->data['breadcrumb'] = 'Standard Notifications Template';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);

          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $eventid=$this->uri->segment(3);
          $user = $this->session->userdata('current_user');
          
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               //$eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());

               $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          //echo $cnt;
          if ($cnt == 1)
          {
               $this->load->model('Appnotitemplate_model');
               $this->load->model('Event_template_model');
               $this->load->model('Setting_model');
               $this->load->model('Speaker_model');
               $this->load->model('Profile_model');
               $this->load->model('Agenda_model');
               $this->load->model('Cms_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Appnotitemplate_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $arrTemplate = $this->Appnotitemplate_model->getAppTemplate($id);
          $this->data['arrTemplate'] = $arrTemplate;
          
          $arrPushTemplate = $this->Appnotitemplate_model->getAppPushTemplate($id);
          $this->data['arrPushTemplate'] = $arrPushTemplate;

          $menudata = $this->Event_model->geteventmenu($id, 26);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;
          
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;


          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $this->template->write_view('css', 'appnotitemplate/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'appnotitemplate/index', $this->data, flase);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('js', 'appnotitemplate/js', true);
          $this->template->render();
     }
     
     public function edit($intEventId = null, $intTempId = null)
     {
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
          $this->data['users_role']=$user_role;

          $logged_in_user_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
          $this->data['cms_list'] = $cms_list;
          

          $event = $this->Event_model->get_admin_event($intEventId);
          $this->data['event'] = $event[0];
          


          $arrForm = $this->Appnotitemplate_model->getAppTemplate($intEventId,$intTempId);
          $this->data['arrTemplate'] = $arrForm;
          

          if ($this->data['page_edit_title'] == 'edit')
          {
               if ($intEventId == NULL || $intEventId == '')
               {
                    redirect('Event');
               }
               $temppost = $this->input->post();
                if(!empty($temppost))
                {
                    $arrUpdate = array();
                    $arrUpdate['event_id'] = $intEventId;
                    $arrUpdate['id']       = $intTempId;
                    $arrUpdate['Subject']  = $this->input->post('Subject');    
                    $arrUpdate['From']  = $this->input->post('From');    
                    $arrUpdate['Content']  = $this->input->post('Content');    
                    $advertising_id = $this->Appnotitemplate_model->update_template($arrUpdate);
                    $this->session->set_flashdata('appnoti_data','Updated');
                    redirect("Appnotitemplate/index/" . $intEventId);
                }
               

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'appnotitemplate/add_css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'appnotitemplate/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->advertising_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'appnotitemplate/edit_js', $this->data, true);
               $this->template->render();
          }
          //}
     }
     public function pushedit($intEventId = null, $intTempId = null)
     {
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
          $this->data['users_role']=$user_role;

          $logged_in_user_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
          $this->data['cms_list'] = $cms_list;
          

          $event = $this->Event_model->get_admin_event($intEventId);
          $this->data['event'] = $event[0];
          
          

          $arrForm = $this->Appnotitemplate_model->getAppPushTemplate($intEventId,$intTempId);
          $this->data['arrTemplate'] = $arrForm;
          

          if ($this->data['page_edit_title'] == 'edit')
          {
               if ($intEventId == NULL || $intEventId == '')
               {
                    redirect('Event');
               }
               $temppost = $this->input->post();
                if(!empty($temppost))
                {
                    $arrUpdate = array();
                    $arrUpdate['event_id'] = $intEventId;
                    $arrUpdate['id']       = $intTempId;
                    $arrUpdate['Content']  = $this->input->post('Content');    
                    $advertising_id = $this->Appnotitemplate_model->update_pushtemplate($arrUpdate);
                    $this->session->set_flashdata('appnoti_data','Updated');
                    redirect("Appnotitemplate/index/" . $intEventId);
                }
               

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'appnotitemplate/add_css', $this->data, true);
               $this->template->write_view('content', 'appnotitemplate/pushedit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->advertising_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'appnotitemplate/edit_js', $this->data, true);
               $this->template->render();
          }
          //}
     }
     public function checkmenusection()
     {
          if ($this->input->post())
          {
               $menu = $this->advertising_model->checkmenusection($this->input->post('Menu_id'), $this->input->post('idval'));

               if (empty($menu))
               {
                    echo "error###Menu already exist. Please choose another menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }

     public function checkcmssection()
     {
          if ($this->input->post())
          {
               $cms = $this->advertising_model->checkcmssection($this->input->post('Cms_id'), $this->input->post('idval'));
               if (empty($cms))
               {
                    echo "error###Cms already exist. Please choose another cms menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }
}
