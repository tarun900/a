<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Custom Module';
          $this->data['smalltitle'] = 'Custom Module';
          $this->data['breadcrumb'] = 'Custom Module';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Message_model');
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Profile_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Agenda_model');
          $this->load->library('formloader');

          $user = $this->session->userdata('current_user');
          
          $cnt = 0;
          $eventname=$this->Event_model->get_all_event_name();
          
          if(in_array($this->uri->segment(3),$eventname))
          {

            if ($this->data['pagetitle'] == 'Custom Module')
            {
                 $title = "Cms";
            }
            
            if ($user != '')
            {
                 $req_mod = $this->router->fetch_class();
                 if($req_mod=="Cms")
                 {
                      $req_mod="Custom modules";
                 }
                 $menu_id=$this->Event_model->get_menu_id($req_mod);
                 $current_date=date('Y/m/d');
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $logged_in_user_id=$user[0]->Id;
                 $this->Event_model->add_view_hit($logged_in_user_id,$current_date,$menu_id,$event_templates[0]['Id']); 
              
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                /* if(!empty($roledata))
                 {
                      $roleid = $roledata[0]->Role_id;
                      $eventid = $event_templates[0]['Id'];
                      $rolename = $roledata[0]->Name;
                      $cnt = 0;
                      $req_mod = ucfirst($this->router->fetch_class());

                      //$cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
                      $cnt = 1;
                 }
                */
                $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                if ($cnt == 1)
                {
                         $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                         $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type=$event_templates[0]['Event_type'];
                    if($event_type==3)
                    {
                          $this->session->unset_userdata('current_user');
                          $this->session->unset_userdata('invalid_cred');
                          $this->session->sess_destroy();
                    }
                    else
                    {
                          $parameters = $this->uri->uri_to_assoc(1);
                          $Subdomain=$this->uri->segment(3);
                          $acc_name=$this->uri->segment(2);
                          echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
                    }
                           //redirect('Unauthenticate/' . $parameters[$this->data['pagetitle']]."/".$Subdomain);
                }
            }
          }
          else
          {
               $Subdomain=$this->uri->segment(3);
               $flag=1;
               echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
          }
     }

     public function View($acc_name=NULL,$Subdomain,$menuid)
     {
         
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          $this->data['categories'] = $this->Event_model->category($event_templates[0]['Id']);
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
               $this->data['fb_login_data'] = $fb_login_data;
               $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

           $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
               $this->data['menu_list'] = $menu_list;
          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
          $this->data['cms_feture_menu'] = $cms_feture_menu;

          $cms_menu_details = $this->Cms_model->get_cms_page($event_templates[0]['Id'], $menuid);
          $this->data['cms_menu_details'] = $cms_menu_details;
         
          
          $eid=$event_templates[0]['Id'];
          $res=$this->Agenda_model->getCmsforms($eid,$cms_menu_details[0]['Id']);
          $this->data['form_data']=$res;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $this->data['Subdomain'] = $Subdomain;
          

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          if($user[0]->Role_id==4)
          {
            $custom=$this->Event_model->get_user_custom_clounm($user[0]->Id,$event_templates[0]['Id']);
            $this->data['custom']=$custom;
            
          }
            $this->data['custom_column'] = $this->db->where('event_id',$event_templates[0]['Id'])->get('custom_column')->result_array();
            $array_temp_past = $this->input->post();
            if(!empty($array_temp_past))
            {
              
             $fid=$this->uri->segment(6);
            
              $aj=json_encode($this->input->post());
              $formdata=array('f_id' =>$fid,
                  'cms_id'=>$cms_menu_details[0]['Id'],
                  'event_id'=>$eid,
                  'json_submit_data'=>$aj
               );

             $this->Agenda_model->formsinsert($formdata);
             echo '<script>window.location.href="'.base_url().'Cms/'.$acc_name.'/'.$Subdomain.'/View/'.$menuid.'"</script>';
             //echo '<script>window.location.href="'.base_url().'Cms/'.$acc_name."/ ".$Subdomain.'/View/'.$cms_menu_details[0]['Id'].'"</script>';
             exit;
             //redirect('Cms/'.$Subdomain.'/View/'.$cms_menu_details[0]['Id'],'refresh');
            }
                
               $lid = $user[0]->Id;

               if (!empty($cms_menu_details[0]['fecture_module']))
               {
                    $datavalue = explode(',', $cms_menu_details[0]['fecture_module']);
               }
               else
               {
                    $datavalue = array();
               }

               if (!empty($cms_menu_details[0]['cms_fecture_module']))
               {
                    $cmsdatavalue = explode(',', $cms_menu_details[0]['cms_fecture_module']);
               }
               else
               {
                    $cmsdatavalue = array();
               }

               $this->data['cms_display_menu'] = $cmsdatavalue;

               $this->data['display_menu'] = $datavalue;

               $advertisement_images = $this->Event_template_model->get_advertising_images_cms($Subdomain);
               $this->data['advertisement_images'] = $advertisement_images;


               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $orid = $this->data['user']->Id;
             

               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);

               if ($event_templates[0]['Event_type'] == '1')
               {
                   if (empty($user))
                   {
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                   }
                   else
                   {
                        $this->template->write_view('content', 'Cms/index', $this->data, true);
                       $this->template->write_view('footer', 'Cms/footer', $this->data, true);
                   }
               }
               elseif ($event_templates[0]['Event_type'] == '2')
               {
                   if (empty($user))
                   {
                       $this->template->write_view('content', 'registration/index', $this->data, true);
                   }
                   else
                   {
                       $this->template->write_view('content', 'Cms/index', $this->data, true);
                       $this->template->write_view('footer', 'Cms/footer', $this->data, true);
                   }
               }
               elseif ($event_templates[0]['Event_type'] == '3') 
               {
                   $this->session->unset_userdata('acc_name');
                   $acc['acc_name'] =  $acc_name;
                   $this->session->set_userdata($acc);
                   $this->template->write_view('content', 'Cms/index', $this->data, true);
                   $this->template->write_view('footer', 'Cms/footer', $this->data, true);
               }
               else
               {
                  if (empty($user))
                  {
                    $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                  }
                  else
                  {
                    $this->template->write_view('content', 'Cms/index', $this->data, true);
                    $this->template->write_view('footer', 'Cms/footer', $this->data, true);
                  }
               }
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->render();

        
     }

}
