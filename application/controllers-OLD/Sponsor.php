<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sponsor extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'Sponsors';
		$this->data['smalltitle'] = 'Sponsors Details';
		$this->data['breadcrumb'] = 'Sponsors';
        $this->data['page_edit_title'] = 'edit';
		parent::__construct($this->data);
		$this->load->model('Sponsor_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
	}

	public function index($id)                 
	{        
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];	

        $orid = $this->data['user']->Id;
        if($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission; 
        }	   
        if($this->data['user']->Role_name == 'Client')
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
        }      		
        
		$sponsers = $this->Sponsor_model->get_sponsor_list();
        $this->data['Sponsers'] = $sponsers;
		$this->template->write_view('css', 'sponsor/css', $this->data , true);
		$this->template->write_view('content', 'sponsor/index', $this->data , true);
        
        if($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        }

		$this->template->write_view('js', 'sponsor/js', $this->data , true);
		$this->template->render();
	}
}
