<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Presentation extends CI_Controller
{

     function __construct()
     {
          error_reporting(0);
          ini_set('error_reporting', off);
          ini_set('display_errors', off);
          ini_set('log_errors', off);

          $this->data['pagetitle'] = 'Presentation';
          $this->data['smalltitle'] = 'Presentation';
          $this->data['breadcrumb'] = 'Presentation';
          parent::__construct($this->data);
          //$this->startSocketServer();
          $this->load->library('formloader');
          $this->load->library('formloader1');
          $this->load->model('Agenda_model');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $this->load->model('Presentation_model');
          $this->load->library('Gcm');
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('9',$menu_list))
          {
                echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          $eventname=$this->Event_model->get_all_event_name();
          if(in_array($this->uri->segment(3),$eventname))
          {
            if ($user != '')
            {
                 $logged_in_user_id=$user[0]->User_id;
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                 
                 $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                 if ($cnt == 1)
                 {
                      $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                      $this->data['notes_list'] = $notes_list;
                 }
                 else
                 {
                      $event_type=$event_templates[0]['Event_type'];
                      if($event_type==3)
                      {
                          $this->session->unset_userdata('current_user');
                          $this->session->unset_userdata('invalid_cred');
                          $this->session->sess_destroy();
                      }
                      else
                      {
                          $parameters = $this->uri->uri_to_assoc(1);
                          $Subdomain=$this->uri->segment(3);
                          $acc_name=$this->uri->segment(2);
                          echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
                      }
                 }
            }
          }
          else
          {
              $Subdomain=$this->uri->segment(3);
              $flag=1;
              echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
          }
     }

     public function index($acc_name=NULL,$Subdomain = NULL,$intFormId=NULL)
     {
          $presentations = $this->Event_template_model->get_presentation($Subdomain);
          $this->data['presentations'] = $presentations;

          $presentation_types = $this->Event_template_model->get_presentation_with_type($Subdomain);
          $this->data['presentation_types'] = $presentation_types;

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;
          $user = $this->session->userdata('current_user');
          if(!empty($user[0]->Id))
          {
            $req_mod = $this->router->fetch_class();
            if($req_mod=="Presentation")
            {
              $req_mod="Presentations";
            }
            $menu_id=$this->Event_model->get_menu_id($req_mod);
            $current_date=date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id,$current_date,$menu_id,$event_templates[0]['Id']); 
          }
          $this->data['timezonetype']=$this->Event_model->get_timezone_desc_and_type($event_templates[0]['Event_time_zone']);

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
          $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];

          $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
          $this->data['time_format'] = $time_format;
            
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          for($i=0;$i<count($menu_list);$i++)
          {
            if('Presentation'==$menu_list[$i]['pagetitle'])
            {
               $mid=$menu_list[$i]['id'];
            }
          }
          $this->data['menu_id']=$mid;
          $this->data['menu_list'] = $menu_list;
          

          $eid=$event_templates[0]['Id'];
          $res=$this->Agenda_model->getforms($eid,$mid);
          $this->data['form_data']=$res;

          $array_temp_past = $this->input->post();
          if(!empty($array_temp_past))
            {
               
              $aj=json_encode($this->input->post());
              $formdata=array('f_id' =>$intFormId,
                  'm_id'=>$mid,
                  'user_id'=>$user[0]->Id,
                  'event_id'=>$eid,
                  'json_submit_data'=>$aj
               );
             $this->Agenda_model->formsinsert($formdata);
             echo '<script>window.location.href="'.base_url().'Presentation/'.$acc_name.'/'.$Subdomain.'"</script>';
             exit;
            }

          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $this->data['Subdomain'] = $Subdomain;

            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $user = $this->session->userdata('current_user');
           if ($event_templates[0]['Event_type'] == '1')
           {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'Presentation/index', $this->data, true);
                  $this->template->write_view('footer', 'Presentation/footer', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '2')
           {

              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'Presentation/index', $this->data, true);
                  $this->template->write_view('footer', 'Presentation/footer', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '3') 
           {
              $this->session->unset_userdata('acc_name');
              $acc['acc_name'] =  $acc_name;
              $this->session->set_userdata($acc);
              $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('9',$event_templates[0]['Id']);
              if($isforcelogin['is_force_login']=='1' && empty($user))
              {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
              }
              else
              {
                $this->template->write_view('content', 'Presentation/index', $this->data, true);
                $this->template->write_view('footer', 'Presentation/footer', $this->data, true);  
              }
           }
           else
           {
              if (empty($user))
              {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
              }
              else
              {
                $this->template->write_view('content', 'Presentation/index', $this->data, true);
                $this->template->write_view('footer', 'Presentation/footer', $this->data, true);
              }
           }
           $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
           $this->template->write_view('js', 'frontend_files/js', $this->data, true);
           $this->template->render();
     }

     public function View_presentation($acc_name,$Subdomain = NULL, $id = NULL)
     {
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;
       
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
          $this->data['time_format'] = $time_format;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
          $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;


          $presentations = $this->Event_template_model->get_presentation_by_id($Subdomain, $id);
          $img=json_decode($presentations[0]['Images'],TRUE);
          $user = $this->session->userdata('current_user');

         /* if(!empty($presetation_tool[0]))
          {
            if($presetation_tool[0]['push_images']!='')
              $data['lock_image']  = $presetation_tool[0]['push_images'];
            if($presetation_tool[0]['push_result']!='')
              $data['lock_image']  = $presetation_tool[0]['push_result'];
            $this->Event_template_model->save_presentation_lock_images($id,$data);
          }
          else
          {
             $data['lock_image']  = $img[0];
             $this->Event_template_model->save_presentation_lock_images($id,$data);
          }*/
          $presetation_tool=$this->Event_template_model->get_presentation_tool_data($id);


          $this->data['presetation_tool']=$presetation_tool;

          $user_permissions=explode(",",$presentations[0]['user_permissions']);
          
        
           $view_result_btn="0";
          foreach ($img as $key => $value) {
            $ext = pathinfo($value, PATHINFO_EXTENSION);
            if(empty($ext))
            {
              if(!empty($user[0]->Id))
              {
                $ans=$this->Presentation_model->get_survey_and_by_user_id($value,$user[0]->Id);
              }
              /*if(empty($ans[0]['Answer']))
              {*/
                $img[$key]=$this->Presentation_model->get_edit_survey_in_presentation($event_templates[0]['Id'],$value);
                $img[$key][0]['Answer']=$ans[0]['Answer'];
              /*}
              else
              {
                $removekey[]=$key;
              }*/
              $ans1=$this->Presentation_model->get_survey_and_by_user_id($value);
              if(!empty($ans1[0]['Answer']))
              {
                $view_result_btn="1";
              }
            }
          }
          /*if(!empty($removekey)){
            $img=array_values(array_diff_key($img, array_flip($removekey)));
          }*/
          $presentations[0]['Imagesno']=$presentations[0]['Images'];
          $presentations[0]['Images']=json_encode($img);
          $this->data['presentations'] = $presentations;

         

          $this->data['view_result_btn']=$view_result_btn;
          $this->data['Subdomain'] = $Subdomain;
          $survey_answer_chart=$this->Presentation_model->get_survey_answer_chart_by_survry_ids($presetation_tool[0]['push_result']);
            $final_Array_chart = array();
            foreach ($survey_answer_chart as $key => $value) 
            {
              $final_Array_chart[$value['Question']][$value['panswer']][] = array(
                'id' => $value['puserid'],
                'Firstname' => $value['Firstname'],
                'Lastname' => $value['Lastname'],
                'Email' => $value['Email']
              );
            }
            foreach($final_Array_chart as $key => $value)
            {
              foreach ($value as $i => $j) 
              {
                $final_Array_chart[$key][$i] = sizeof($j);
              }
            }
            $this->data['final_Array_chart']=$final_Array_chart;
          $presentation_types = $this->Event_template_model->get_presentation_with_type($Subdomain);
          $this->data['presentation_types'] = $presentation_types;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;
          $this->data['event_id']=$event_templates[0]['Id'];
          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $user = $this->session->userdata('current_user');
          $user_permissions=explode(",",$presentations[0]['user_permissions']);
          //if(in_array($user[0]->Id,$user_permissions)){
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          //} 
          
           if ($event_templates[0]['Event_type'] == '1')
           {
              if (empty($user))
              { 
                  $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {   
                  $this->template->write_view('js', 'Presentation/js1', $this->data, true);
                  if($presentations[0]['presentation_file_type']==0){
                    $this->template->write_view('content', 'Presentation/view_slider', $this->data, true);
                  }
                  else
                  {
                   $this->template->write_view('content', 'Presentation/view_ppt', $this->data, true); 
                  }
              }
           }
           elseif ($event_templates[0]['Event_type'] == '2')
           {

              if (empty($user))
              {   
                  $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                $this->template->write_view('js', 'Presentation/js1', $this->data, true);
                if($presentations[0]['presentation_file_type']==0){
                  $this->template->write_view('content', 'Presentation/view_slider', $this->data, true);
                }
                else
                {
                 $this->template->write_view('content', 'Presentation/view_ppt', $this->data, true); 
                }
              }
           }
           elseif ($event_templates[0]['Event_type'] == '3') 
           {
              $this->session->unset_userdata('acc_name');
              $acc['acc_name'] =  $acc_name;
              $this->session->set_userdata($acc);
              $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('9',$event_templates[0]['Id']);
              if($isforcelogin['is_force_login']=='1' && empty($user))
              {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
              }
              else
              {
                $this->template->write_view('js', 'Presentation/js1', $this->data, true);
                if($presentations[0]['presentation_file_type']==0){
                  $this->template->write_view('content', 'Presentation/view_slider', $this->data, true);
                }
                else
                {
                 $this->template->write_view('content', 'Presentation/view_ppt', $this->data, true); 
                }
              }  
           }
           else
           {
              if (empty($user))
              { 
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
              }
              else
              {   
                $this->template->write_view('js', 'Presentation/js1', $this->data, true);
                if($presentations[0]['presentation_file_type']==0){
                    $this->template->write_view('content', 'Presentation/view_slider', $this->data, true);
                }
                else
                {
                   $this->template->write_view('content', 'Presentation/view_ppt', $this->data, true); 
                }
              }
           }
           $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
           $this->template->render();
     }

     public function Get_slider_images($acc_name,$Subdomain = NULL, $id = NULL)
     {
          $Subdomain = $this->uri->segment(3);
          $id = $this->uri->segment(5);
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;
          $presentations = $this->Event_template_model->get_presentation_by_id($Subdomain, $id);
          $img=json_decode($presentations[0]['Images'],TRUE);
          $user = $this->session->userdata('current_user');
          foreach ($img as $key => $value) {
            $ext = pathinfo($value, PATHINFO_EXTENSION);
            if(empty($ext))
            {
              if(!empty($user[0]->Id))
              {
                $ans=$this->Presentation_model->get_survey_and_by_user_id($value,$user[0]->Id);
              }
              /*if(empty($ans[0]['Answer']))
              {*/
                $img[$key]=$this->Presentation_model->get_edit_survey_in_presentation($event_templates[0]['Id'],$value);
                $img[$key][0]['Answer']=$ans[0]['Answer'];
              /*}
              else
              {
                $removekey[]=$key;
              }*/
            }
          }
          /*if(!empty($removekey)){
            $img=array_values(array_diff_key($img, array_flip($removekey)));
          }*/
          $presentations[0]['Imagesno']=$presentations[0]['Images'];
          $presentations[0]['Images']=json_encode($img);
          $this->data['presentations'] = $presentations;
          $array1 = json_decode($presentations[0]['Images']);
          $array2 = json_decode($presentations[0]['Image_lock']);
          $final_array = array();
          $presetation_tool=$this->Event_template_model->get_presentation_tool_data($id);
          $survey_answer_chart=$this->Presentation_model->get_survey_answer_chart_by_survry_ids($presetation_tool[0]['push_result']);
            $final_Array_chart = array();
            foreach ($survey_answer_chart as $key => $value) 
            {
              $final_Array_chart[$value['Question']][$value['panswer']][] = array(
                'id' => $value['puserid'],
                'Firstname' => $value['Firstname'],
                'Lastname' => $value['Lastname'],
                'Email' => $value['Email']
              );
            }
            foreach($final_Array_chart as $key => $value)
            {
              foreach ($value as $i => $j) 
              {
                $final_Array_chart[$key][$i] = sizeof($j);
              }
            }
            $this->data['final_Array']=$final_Array_chart;
          $user_permissions=explode(",",$presentations[0]['user_permissions']);
          if(!in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){
            if(!empty($presetation_tool[0]['push_result'])){
              $color_arr=array('#109618','#ff9900','#3366cc','#dc3912','#800000','#FF0000','#C14C33','#593027','#96944C','#156A15','#3B2129','#54073E','#5711A7','#093447','#0C3F33');
            if($presetation_tool[0]['chart_type']=='0'){  
              foreach($final_Array_chart as $key => $item) 
              {
                $colorindex=0; foreach ($item as $key1 => $value1) { if($colorindex==count($item)-1){$colorta.="'".$color_arr[$colorindex]."'";}else{ $colorta.="'".$color_arr[$colorindex]."',"; } $colorindex++;  } 
                $chart.="<div class='sp_chart_as_slider_div'><h3 style='text-align: center;'>".$key."</h3>";
                $chart.="<div id='piechart".$key."' class='chart'></div></div>";
                $chart.="<ul style='list-style: none;' class='option-list'>";
                $dint=0; foreach ($item as $key2 => $value2) {
                  $chart.="<li><label style='border-radius: 100%;background-color:".$color_arr[$dint]."'>&nbsp;&nbsp;&nbsp;&nbsp;</label>".$key2."</li>";
                  $dint++;
                }
                $chart.="</ul>";
                $chart.='<script type="text/javascript">';
                /*$chart.='google.load("visualization", "1", {packages:["corechart"]});';
                $chart.='google.setOnLoadCallback(drawChart);';
                $chart.='function drawChart() { ';
                $chart.="var data = google.visualization.arrayToDataTable([['Options', 'No. of users'], ";
                foreach ($item as $a => $b) {
                  $chart.="['".$a."',".$b."],";
                }
                $chart.="]);";
                $chart.="var options = {";
                $chart.="colors:[".$colorta."],width:'100%',height:'100%',align: 'center',pieSliceText: 'percentage',verticalAlign: 'middle',pieSliceTextStyle: { color: 'black',},fontSize: 20,legend:{position:'none',textStyle: {color: 'blue', fontSize: 20}},";
                $chart.='chartArea: {left: "3%",top: "1%",height: "70%",width: "94%"}};';
                $chart.="var chart = new 
              google.visualization.PieChart(document.getElementById('piechart".$key."'));
              chart.draw(data, options);";
                $chart.="}";*/
                $chart.="function drawChart() { Highcharts.chart('piechart".$key."', {chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false,type: 'pie'},";
                  $chart.="colors:".json_encode($color_arr).",title: {text: 'Question:".$key."',style: {display: 'none'}},";
                  $chart.="tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},plotOptions: {pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: {enabled: true,
                     format: '<br>{point.percentage:.1f} %',style:{fontSize: '25px',fontWeight:'bold',color:'contrast',textOutline:'1px contrast'}},showInLegend: false},series: {animation: false}},series: [{name: 'Answer',colorByPoint: true,data:";
                  $arr=array(); 
                  foreach ($item as $a => $b) { 
                    $arr[]=array('name'=>$a,'y'=>$b);
                  }
                  $chart.=json_encode($arr)."}] }); }";
                $chart.="</script>";
              }
            }
            else
            {
              foreach($final_Array_chart as $key => $item) 
              {
                $colorindex=0; foreach ($item as $key1 => $value1) { if($colorindex==count($item)-1){$colorta.="'".$color_arr[$colorindex]."'";}else{ $colorta.="'".$color_arr[$colorindex]."',"; } $colorindex++;  } 
                $chart.="<div class='sp_chart_as_slider_div'><h3 style='text-align: center;'>".$key."</h3>";
                $chart.="<div id='barchart".$key."' class='chart' style='margin:0 auto;' text-align='center'></div></div>";
                $chart.="<ul style='list-style: none;' class='option-list'>";
                $dint=0; foreach ($item as $key2 => $value2) {
                  $chart.="<li><label style='border-radius: 100%;background-color:".$color_arr[$dint]."'>&nbsp;&nbsp;&nbsp;&nbsp;</label>".$key2."</li>";
                  $dint++;
                }
                $chart.="</ul>";
                $chart.='<script type="text/javascript">';
                /*$chart.='google.load("visualization", "1", {packages:["corechart"]});';
                $chart.='google.setOnLoadCallback(drawChart);';
                $chart.='function drawChart() { ';
                $chart.="var data = google.visualization.arrayToDataTable([['Options', 'No. of users',{ role: 'style' }], ";
                $i=0;
                foreach ($item as $a => $b) {
                  $chart.="['".$a."',".$b.",'".$color_arr[$i]."'],";
                  $i++;
                }
                $chart.="]);";
                $chart.="var options = {";
                $chart.="width:'70%',height:'90%',align: 'center',pieSliceText: 'percentage',verticalAlign: 'middle',pieSliceTextStyle: { color: 'black',},fontSize: 20,legend:{position:'none',textStyle: {color: 'blue', fontSize: 20}},";
                $chart.='chartArea: {left: "20%",top: "1%",height: "70%",width: "70%"}};';
                $chart.="var chart = new 
                google.visualization.BarChart(document.getElementById('barchart".$key."'));
                chart.draw(data, options);";
                $chart.="}";*/
                $chart.="var data = ".json_encode(array_values($item)).";";
                $chart.="var dataSum = 0;for (var i=0;i < data.length;i++){dataSum += data[i]}"; 
                $chart.="function drawChart() {Highcharts.chart('barchart".$key."', {";
                $chart.="chart: {type: 'column'},title: {text: 'Question: ".$key."',style: {display: 'none'}},";
                $chart.="tooltip: {valueSuffix: ' Users'},xAxis: {type: 'category'},yAxis: {min: 0,title: {text: 'Total User Answers'}},plotOptions: {column: {dataLabels: {enabled: true,
                     formatter:function(){var pcnt = (this.y / dataSum) * 100;return Highcharts.numberFormat(pcnt) + '%';},style: {fontSize: '25px',fontWeight: 'bold',color: 'contrast',textOutline: '1px contrast'},}},series: {animation: false}},";
                $chart.="colors:".json_encode($color_arr).",legend: {enabled:false,},";
                $chart.="series: [{name: 'Answers',colorByPoint: true,data:";
                $arr=array(); foreach ($item as $a => $b) { 
                  $arr[]=array('name'=>$a,'y'=>$b);
                }
                $chart.=json_encode($arr)."}]});}";
                $chart.="</script>";
              }
            }
              $final_array[]['div']=$chart;
            }
          }
          foreach ($array2 as $key => $value)
          {
               if ($value)
               {
                  if(!empty($array1[$key])){
                    if(!is_array($array1[$key])){
                      $final_array[]['img'] = $array1[$key];
                    }
                    else
                    {
                      $div="<div class='sp_survey_as_slider_div'><h4 class='list-group-item-heading'>".$array1[$key][0]->Question."</h4>";
                      if(!empty($array1[$key][0]->Answer)){
                        $div.="<span class='hdr-span' id='msg_question_".$array1[$key][0]->Id."'>Thank you for your answer.</span>";
                      }
                      else
                      {
                        $div.="<span class='hdr-span' id='msg_question_".$array1[$key][0]->Id."'>Please select one answer.</span>";
                      }
                      $div.="<ul class='list'>";
                      $ans=json_decode($array1[$key][0]->Option,TRUE);
                      foreach ($ans as $key1 => $value1) {
                        $div.="<li>";
                        $name="question_option".$array1[$key][0]->Id;
                        if($value1==$array1[$key][0]->Answer)
                        {
                          $che="checked='checked'";
                        }
                        else
                        {
                          $che="";
                        }
                        $div.="<input type='radio' name='".$name."' value='".$value1."' class='required radio-callback' ".$che.">";
                        $for="line-radio-".$array1[$key][0]->Id;
                          $div.="<label for='".$for."'>".$value1."</label>";
                        $div.="</li>";
                      }
                      $div.="</ul>";
                      if(empty($array1[$key][0]->Answer))
                      {
                        $div.="<div class='col-sm-12'><button name='next_slied' id='submit_ans_btn_".$array1[$key][0]->Id."' class='btn btn-green btn-block' onclick='saveansbyquestion(".$array1[$key][0]->Id.");' value='Next'>Submit</button></div>";
                      }
                      $div.="</div>";
                      $final_array[]['div']=$div; 
                    }
                  }
               }
          }
          $data['images'] = $final_array;
          $user_permissions=explode(",",$presentations[0]['user_permissions']);
          if(!in_array($user[0]->Id,$user_permissions) && !empty($user[0]->Id)){
            if(!empty($presetation_tool[0]['push_result'])){
              //$chrthum="<h4>Chart Div</h4>";
              $final_array1[]['div']="poll_default_img.png";
            }
          }
          foreach ($array2 as $key => $value)
          {
               if ($value)
               {
                  if(!empty($array1[$key])){
                    if(!is_array($array1[$key])){
                      $final_array1[]['img'] = $array1[$key];
                    }
                    else
                    {
                      $final_array1[]['div']= "poll_default_img.png";
                    }
                  }
               }
          }
          
          $data['thum'] = $final_array1;
          $data['current_img'] = array_search($presentations[0]['Image_current'],$array1);
          $data['current_image'] = $presentations[0]['Image_current'];
          $presetation_tool=$this->Event_template_model->get_presentation_tool_data($id);
          $user_permissions=explode(",",$presentations[0]['user_permissions']);
          if(!in_array($user[0]->Id,$user_permissions)){
           //$data['c_loke_images'] = '';
           if(!empty($presetation_tool[0]['lock_image'])){
              $data['c_loke_images']="index###".array_search($presetation_tool[0]['lock_image'],json_decode($presentations[0]['Imagesno']));
            }
            if(!empty($presetation_tool[0]['push_images'])){
              $data['c_push_images']="index###".array_search($presetation_tool[0]['push_images'],json_decode($presentations[0]['Imagesno']));
             // $data['c_loke_images']=(!empty($presetation_tool[0]['lock_image'])) ? '' :"index###".array_search($presetation_tool[0]['push_images'],json_decode($presentations[0]['Imagesno']));
              
            }
            if(!empty($presetation_tool[0]['push_result'])){
              $data['push_result']=$presetation_tool[0]['push_result'];
              //$data['c_loke_images']=(!empty($presetation_tool[0]['lock_image'])) ? '' : "index###".array_search($presetation_tool[0]['push_result'],json_decode($presentations[0]['Imagesno']));
            }
            else
            {
              $data['push_result']='NO';    
            }
             /*if(empty($presetation_tool[0]['lock_image'])){
              $data['c_loke_images']="index###".array_search($presetation_tool[0]['lock_image'],json_decode($presentations[0]['Imagesno']));
            }*/
          }
          else
          {
            $data['push_result']='NO'; 
          }
          if(!empty($presetation_tool[0]['push_images']) || !empty($presetation_tool[0]['lock_image']))
          {
            if(!empty($presetation_tool[0]['push_images'])){
              $ext1 = pathinfo($presetation_tool[0]['push_images'], PATHINFO_EXTENSION);
              $push_slider=$presetation_tool[0]['push_images'];
            }
            else
            {
              $ext1 = pathinfo($presetation_tool[0]['lock_image'], PATHINFO_EXTENSION); 
              $push_slider=$presetation_tool[0]['lock_image'];
            }
            if(!empty($ext1)){
              $img_user=base_url()."assets/user_files/".$push_slider;
              $data['push_full_image']="<img width='100%' height='100%' class='sp-image' src='".$img_user."'/>";
            }
            else
            {
              $ans=$this->Presentation_model->get_survey_and_by_user_id($push_slider,$user[0]->Id);
              $full_data=$this->Presentation_model->get_edit_survey_in_presentation($event_templates[0]['Id'],$push_slider);
              $full_data[0]['Answer']=$ans[0]['Answer'];
              $slide_data="<div class='my_slider_sliderPro' id='my_slider_silderpro_div'><div id='example3' class='slider-pro'><div class='sp-slides'><div class='sp-slide'><div class='sp-image'><div class='sp_survey_as_slider_div'>";
              $slide_data.="<h4 class='list-group-item-heading'>".$full_data[0]['Question']."</h4>";
              if(!empty($array1[$key][0]->Answer)){
                $slide_data.="<span class='hdr-span' id='msg_question'>Thank you for your answer.</span>";
              }
              else
              {
                $slide_data.="<span class='hdr-span' id='msg_question'>Please select one answer.</span>";
              }
              $slide_data.="<ul class='list'>";
              $ans=json_decode($full_data[0]['Option'],TREU);
              foreach ($ans as $key1 => $value1) {
                $slide_data.="<li>";
                $name="question_option";
                if($value1==$full_data[0]['Answer'])
                {
                  $che="checked='checked'";
                }
                else
                {
                  $che="";
                }
                $slide_data.="<input type='radio' name='".$name."' value='".$value1."' class='required radio-callback' ".$che.">";
                $for="line-radio-";
                $slide_data.="<label for='".$for."'>".$value1."</label>";
                  $slide_data.="</li>";
                }
                $slide_data.="</ul>";
                if(empty($full_data[0]['Answer']))
                {
                  $slide_data.="<div class='col-sm-12'><button name='next_slied' id='submit_ans_btn1' class='btn btn-green btn-block' value='Next'>Submit</button></div>";
                }
              $slide_data.="</div></div></div></div></div></div>";
              $data['push_full_image']=$slide_data;
            }
          }
          else
          {
            $data['push_full_image']="<h3>No Slide Pushed Or Locked</h3>";
          }
          echo json_encode($data);
          exit;
     }
     public function save_lock_images_presentation($acc_name,$Subdomain,$pid)
     {
       $slid_no=$this->input->get_post('slid_no');
       $presentations = $this->Event_template_model->get_presentation_by_id($Subdomain, $pid);
       
        
        if($this->input->get_post('slid_no')!=""){
          $img=json_decode($presentations[0]['Images']);
          $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
          if($presetation_tool[0]['lock_image']!='')
            $lock_data['lock_image']=$presetation_tool[0]['lock_image']; 
          if($presetation_tool[0]['push_result']!='')
            $lock_data['lock_image']=$presetation_tool[0]['push_result']; 
          if($presetation_tool[0]['push_images']!='')
            $lock_data['lock_image']=$presetation_tool[0]['push_images']; 
        }
        else
        {
          $lock_data['lock_image']='';
        }
       $this->Event_template_model->save_presentation_lock_images($pid,$lock_data);
       $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
       $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        //$this->sendNotification($event_templates[0]['Id']);
        $this->sendSocketMessage($pid);
     }
     public function save_puch_images_presentation($acc_name,$Subdomain,$pid)
     {
       $slid_no=$this->input->get_post('slid_no');
       $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
       $presentations = $this->Event_template_model->get_presentation_by_id($Subdomain, $pid);
        if($this->input->get_post('slid_no')!="")
        {
          $img=json_decode($presentations[0]['Images']);
          $lock_data['push_images']=$img[$slid_no];
          $lock_data['lock_image']=($presetation_tool[0]['lock_image']!='') ? $img[$slid_no] : '';
          $lock_data['push_result']='';
        }
        else
        {
          $lock_data['push_images']='';
        }
       $this->Event_template_model->save_presentation_lock_images($pid,$lock_data);
       $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
       $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        //$this->sendNotification($event_templates[0]['Id']);

        $this->sendSocketMessage($pid);
     }
     public function save_survey_ans_presentation($acc_name,$Subdomain)
     {
        $user = $this->session->userdata('current_user');
        if(!empty($user[0]->Id))
        {
          $save_ans['Question_id']=$this->input->post('sid');
          $save_ans['User_id']=$user[0]->Id;
          $save_ans['Answer']=$this->input->post('ans');
          $this->Presentation_model->save_ans_in_presentation($save_ans);
          echo "success###";die;
        }
        else
        {
          echo "error###User Not Login";die;
        }
     }
     public function showchartdata($acc_name,$Subdomain,$id)
     {
        $survey_answer_chart=$this->Presentation_model->get_survey_answer_chart_by_survry_ids($id);
        $final_Array = array();
        foreach ($survey_answer_chart as $key => $value) 
        {
          $final_Array[$value['Question']][$value['panswer']][] = array(
            'id' => $value['puserid'],
            'Firstname' => $value['Firstname'],
            'Lastname' => $value['Lastname'],
            'Email' => $value['Email']
          );
        }
        foreach($final_Array as $key => $value)
        {
          foreach ($value as $i => $j) 
          {
            $final_Array[$key][$i] = sizeof($j);
          }
        }
        $ext1 = pathinfo($id, PATHINFO_EXTENSION); 
        if(empty($ext1)){
          $this->data['final_Array']=$final_Array;
        }
        if(count($final_Array)<=0 && empty($ext1))
        {
          $this->data['vews_error_msg']="No results have been collected for this poll yet.";
        }
        else
        {
          $this->data['vews_error_msg']="Plase Select Poll Slide.";
        }
        $this->load->view('Presentation/view_result',$this->data);
     }
     public function showbarchartdata($acc_name,$Subdomain,$id)
     {
        $survey_answer_chart=$this->Presentation_model->get_survey_answer_chart_by_survry_ids($id);
        $final_Array = array();
        foreach ($survey_answer_chart as $key => $value) 
        {
          $final_Array[$value['Question']][$value['panswer']][] = array(
            'id' => $value['puserid'],
            'Firstname' => $value['Firstname'],
            'Lastname' => $value['Lastname'],
            'Email' => $value['Email']
          );
        }
        foreach($final_Array as $key => $value)
        {
          foreach ($value as $i => $j) 
          {
            $final_Array[$key][$i] = sizeof($j);
          }
        }
        $ext1 = pathinfo($id, PATHINFO_EXTENSION); 
        if(empty($ext1)){
          $this->data['final_Array']=$final_Array;
        }
        if(count($final_Array)<=0 && empty($ext1))
        {
          $this->data['vews_error_msg']="No results have been collected for this poll yet.";
        }
        else
        {
          $this->data['vews_error_msg']="Plase Select Poll Slide.";
        }
        $this->load->view('Presentation/view_barchartresult',$this->data);
     }
     public function save_puch_result_in_presentation($acc_name,$Subdomain,$pid)
     {
      $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
      $lock_data['chart_type']=$this->input->post('chart_type');
      $lock_data['push_result']=$this->input->get_post('poll_id');
      //$lock_data['push_images']='';
      $lock_data['lock_image']=($presetation_tool[0]['lock_image']!='') ?$this->input->get_post('poll_id') : '';
      if($this->input->get_post('poll_id')=='')
      {
        if($presetation_tool[0]['lock_image']!='')
          $lock_data['lock_image']=$presetation_tool[0]['lock_image'];
        if($presetation_tool[0]['push_result']!='')
          $lock_data['lock_image']=$presetation_tool[0]['push_result'];
        if($presetation_tool[0]['push_images']!='')
          $lock_data['lock_image']=$presetation_tool[0]['push_images'];
        
      }
      $this->Event_template_model->save_presentation_lock_images($pid,$lock_data);
      $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
      //$this->sendNotification($event_templates[0]['Id']);
      $this->sendSocketMessage($pid);
      echo "Success###";
     }
     public function View_fullscreen_presentation($acc_name,$Subdomain,$pid)
     {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting']=$notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $presetation_tool=$this->Event_template_model->get_presentation_tool_data($pid);
        if(!empty($presetation_tool[0]['push_images']))
        {
          $push_slider=$presetation_tool[0]['push_images'];
        }
        else
        {
          $push_slider=$presetation_tool[0]['lock_image'];
        }
        if(!empty($push_slider)){
          $ext = pathinfo($push_slider, PATHINFO_EXTENSION);
          if(empty($ext))
          {
            $ans=$this->Presentation_model->get_survey_and_by_user_id($push_slider,$user[0]->Id);
            $img=$this->Presentation_model->get_edit_survey_in_presentation($event_templates[0]['Id'],$push_slider);
            $img[0]['Answer']=$ans[0]['Answer'];
          }
          else
          {
            $img=$push_slider;
          } 
          $this->data['img_data']=$img;
        } 
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        //$this->template->write_view('header', 'frontend_files/header', $this->data, true);
        //$this->template->write_view('js', 'Presentation/js', $this->data, true);
        $this->template->write_view('content','Presentation/view_fullscreen', $this->data,true);
        //$this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
     }
    public function startSocketServer()
    {
        $address=$this->config->item('ip');$port=$this->config->item('port');
        
        $this->sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );

        socket_set_option($this->sock, SOL_SOCKET, SO_REUSEADDR, 1) ;
        
        if(!socket_bind( $this->sock, 0, $port ))
        {
            socket_connect($this->sock, $address, $port) ;
        }
        
        //socket_close($this->sock);
    }
    public function sendNotification($event_id)
    {
      $this->startSocketServer();

        $msg="200";
        if($this->sock)
        {
            socket_listen( $this->sock );
            $addresses = [];
            $repeated = [];
            $rand = rand();
            $i = 0;
            while($rand = socket_accept( $this->sock ) )
            {
                $gcm_id = socket_read($rand,1024);
                
                            
                socket_getpeername ( $rand , $address ,$port );
                
                if(!in_array($gcm_id, $addresses)  )
                {
                    $sent = socket_write($rand,$msg,strlen($msg));
                    $addresses[] = $gcm_id;
                }
                else
                {
                    $repeated[] = $gcm_id;
                    if(count($addresses) == count($repeated))
                    {
                        socket_close($rand);
                        break;
                    }
                }
                $i++;
            }
        }
        unset($addresses);
        
        /*$event = $this->Event_model->get_admin_event($event_id);
        $obj = new Gcm();
        $users = $this->Event_model->getAllUsersGCM_id_by_event_id($event_id);
        $count = count($users);
        if($count > 100)
        {
           $limit = 100;
            for ($i=0;$i<$count;$i++) 
            {
                $page_no        = $i;
               
                $start          = ($page_no)*$limit;
                $users1         = array_slice($users,$start,$limit);
                foreach ($users1 as $key => $value) 
                {
                    if($value['gcm_id']!='')
                    {
                      $msg =  '';
                      $extra['message_type'] = 'presentation';
                      $extra['message_id'] = '';
                      $extra['event'] = $event[0]['Event_name'];
                      if($value['device'] == "Iphone")
                      {
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      }
                      else
                      {
                          $msg['title'] = '';
                          $msg['message'] = '';
                          $msg['vibrate'] = 1;
                          $msg['sound'] = 1;
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      } 
                    }
                }
            }
        }
        else
        {
            foreach ($users as $key => $value) {
                if($value['gcm_id']!='')
                {
                  $msg =  '';
                  $extra['message_type'] = 'presentation';
                  $extra['message_id'] = '';
                  $extra['event'] = $event[0]['Event_name'];
                  if($value['device'] == "Iphone")
                  {
                      $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                  }
                  else
                  {
                      $msg['title'] = '';
                      $msg['message'] = '';
                      $msg['vibrate'] = 1;
                      $msg['sound'] = 1;
                      $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                  } 
                }
            }
        }*/
    }

    public function sendSocketMessage($presentation_id)
    {
      $host=$_SERVER['SERVER_ADDR'];
      $port=$this->config->item('port');
         
      $fp = fsockopen($host, $port, $errno, $errstr, 30);
         
      if($fp)
      {
          $write['operation'] = '1';
          $write['user_type'] = "P";
          $write['p_id'] = $presentation_id;
          $write['gcm_id'] = "";
          fwrite($fp, json_encode($write));
         
          fclose($fp);

      }
    }
}
