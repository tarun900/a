<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Unauthenticate extends CI_Controller {
	function __construct() {

      		$this->data['pagetitle'] = 'Access Denied';
      		$this->data['smalltitle'] = 'Access Denied';
      		$this->data['breadcrumb'] = 'Access Denied';
      		parent::__construct($this->data);
          $this->session->unset_userdata('current_user');
          $this->session->sess_destroy();
      		$this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->library('formloader1');
          $this->load->model('Event_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Profile_model');
          $this->load->model('Doccategory_model');
          $this->load->model('Document_model');
          $this->load->model('Message_model');
	}

	public function index($acc_name,$Subdomain)                 
	{ 	
          //echo $acc_name;die;
		      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          
          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;
          
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $notifiy_msg= $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;
          
          $this->data['Subdomain'] = $Subdomain;
	   
	        $this->data['acc_name'] = $acc_name;
          $this->session->unset_userdata('current_user');
          $this->session->sess_destroy();
  	      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('content', 'Pageaccess/access', $this->data , true);
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                          
  		    $this->template->render();
	}
}
