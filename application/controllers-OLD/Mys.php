<?php /*if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mys extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Event_model');
        $this->load->model('Attendee_model');
        $this->load->model('Exibitor_model');
    }
    public function index()
    {
    	
    }
    public function generate_token()
    {
        $data=array();
        $username=$this->input->get_post('username');
        $password=$this->input->get_post('password');
        if(!empty($username) && !empty($password))
        {
            if($username == "gulFood" && $password == "gulFooddubai@!2017@@")
            {
                $tokenarr['token']=uniqid().substr(sha1($username.$password),0,10).uniqid();
                $tokenarr['exiprydate']=date('Y-m-d H:i:s',strtotime('+1 hour'));
                $this->Attendee_model->add_token_in_db($tokenarr);
                $token=$this->Attendee_model->get_token();
                $data=array('token'=>$token[0]['token']);
            }
            else
            {
                $data=array('error'=>"Invalid username or password");       
            }
        }
        else
        {
            $data=array('error'=>"username and password must be required.");
        }
        echo json_encode($data);die;
    }
    public function get_all_exibitor_id()
    {
        $data=array();
        $header=getallheaders();
        if(!empty($header['Authorization']))
        {
            $token=$this->Attendee_model->get_token();
            if($header['Authorization']==$token[0]['token'] && strtotime($token[0]['exiprydate']) > strtotime(date('Y-m-d H:i:s')))
            {
                if(!empty($this->input->get_post('fromDate')) && !empty($this->input->get_post('toDate')))
                {
                    if(strtotime($this->input->get_post('toDate')) > strtotime($this->input->get_post('fromDate')))
                    {
                        $fdate=explode("-",$this->input->get_post('fromDate'));
                        $tdate=explode("-",$this->input->get_post('toDate'));
                        if(checkdate($fdate[1],$fdate[2],$fdate[0]) && checkdate($tdate[1],$tdate[2],$tdate[0]))
                        {
                            $fromdate=date("Y-m-d H:i",strtotime($this->input->get_post('fromDate')));
                            $todate=date("Y-m-d H:i",strtotime($this->input->get_post('toDate')));
                            $eid='447';
                            $org_id='4659';
                            $ids=$this->Exibitor_model->get_all_exibitor_user_id_by_date($fromdate,$todate,$eid,'6');
                            if(count($ids) > 0)
                            {
                                $data[0]['exhibitors']=$ids;
                            }
                            else
                            {
                                $data=array('msg'=>"No data Found Between two date time.");
                            }
                        }
                        else
                        {
                            $data=array('error'=>"Either fromDate or toDate is not a valid date value");
                        }
                    }
                    else
                    {
                        $data=array('error'=>"todate must be bigger.");       
                    }
                }
                else
                {
                    $data=array('error'=>"fromDate and toDate must be required.");
                }
            }
            else
            {
                $data=array('error'=>"Authorization Token is Exipry");
            }
        }
        else
        {
            $data=array('error'=>"Authorization Token must be required.");
        }
        echo json_encode($data);die;
    }
    public function get_exibitor_details_by_ex_id()
    {
        $data=array();
        $header=getallheaders();
        if(!empty($header['Authorization']))
        {
            $token=$this->Attendee_model->get_token();
            if($header['Authorization']==$token[0]['token'] && strtotime($token[0]['exiprydate']) > strtotime(date('Y-m-d H:i:s')))
            {
                if(!empty($this->input->get_post('exhID')))
                {
                    if(is_numeric($this->input->get_post('exhID')))
                    {
                        $eid='447';
                        $org_id='4659';
                        $exhID=$this->input->get_post('exhID');
                        $exdata=$this->Exibitor_model->get_exibitor_user_details_by_exibitor_id($exhID,$eid);
                        if(count($exdata) > 0)
                        {
                            $data['exhibitor_details']=$exdata;
                        }
                        else
                        {
                            $data=array('msg'=>"Exhibitor data Not Found.");  
                        }
                    }
                    else
                    {
                        $data=array('error'=>"Exhibitor Id Must Be Numeric.");       
                    }
                }
                else
                {
                    $data=array('error'=>"Exhibitor Id Must Be required.");
                }
            }
            else
            {
                $data=array('error'=>"Authorization Token is Exipry");
            }
        }
        else
        {
            $data=array('error'=>"Authorization Token must be required.");
        }    
        echo json_encode($data);die;
    }
    public function get_all_attendee_id()
    {
        $data=array();
        $header=getallheaders();
        if(!empty($header['Authorization']))
        {
            $token=$this->Attendee_model->get_token();
            if($header['Authorization']==$token[0]['token'] && strtotime($token[0]['exiprydate']) > strtotime(date('Y-m-d H:i:s')))
            {
                if(!empty($this->input->get_post('fromDate')) && !empty($this->input->get_post('toDate')))
                {
                    if(strtotime($this->input->get_post('toDate')) > strtotime($this->input->get_post('fromDate')))
                    {
                        $fdate=explode("-",$this->input->get_post('fromDate'));
                        $tdate=explode("-",$this->input->get_post('toDate'));
                        if(checkdate($fdate[1],$fdate[2],$fdate[0]) && checkdate($tdate[1],$tdate[2],$tdate[0]))
                        {
                            $fromDate=date("Y-m-d H:i",strtotime($this->input->get_post('fromDate')));
                            $toDate=date("Y-m-d H:i",strtotime($this->input->get_post('toDate')));
                            $eid='447';
                            $org_id='4659';
                            $ids=$this->Attendee_model->get_all_user_email_by_date($fromDate,$toDate,$eid,'4');
                            if(count($ids) > 0)
                            {
                                $data[0]['attendees']=$ids;
                            }
                            else
                            {
                                $data=array('msg'=>"No data Found Between two date time.");
                            }
                        }
                        else
                        {
                            $data=array('error'=>"Either fromDate or toDate is not a valid date value");
                        }
                    }
                    else
                    {
                        $data=array('error'=>"toDate must be bigger.");       
                    }
                }
                else
                {
                    $data=array('error'=>"fromDate and toDate must be required.");
                }
            }
            else
            {
                $data=array('error'=>"Authorization Token is Exipry");
            }
        }
        else
        {
            $data=array('error'=>"Authorization Token must be required.");
        }    
        echo json_encode($data);die;
    }
    public function get_attendee_details_by_id()
    {
        $data=array();
        $header=getallheaders();
        if(!empty($header['Authorization']))
        {
            $token=$this->Attendee_model->get_token();
            if($header['Authorization']==$token[0]['token'] && strtotime($token[0]['exiprydate']) > strtotime(date('Y-m-d H:i:s')))
            {
                if(!empty($this->input->get_post('emailAddress')))
                {
                    if(filter_var($this->input->get_post('emailAddress'),FILTER_VALIDATE_EMAIL))
                    {
                        $eid='447';
                        $org_id='4659';
                        $email=$this->input->get_post('emailAddress');
                        $attendee_data=$this->Attendee_model->get_attendee_user_details_by_user_email($email,$eid);
                        if(count($attendee_data) > 0)
                        {
                            $data['attendee_details']=$attendee_data;
                        }
                        else
                        {
                            $data=array('msg'=>"Attendee data Not Found.");  
                        }
                    }
                    else
                    {
                        $data=array('error'=>"Invalid Visitor Email Address.");       
                    }
                }
                else
                {
                    $data=array('error'=>"Visitor Email Address Must Be required.");
                }
            }
            else
            {
                $data=array('error'=>"Authorization Token is Exipry");
            }
        }
        else
        {
            $data=array('error'=>"Authorization Token must be required.");
        }       
        echo json_encode($data);die;
    }
    public function get_attendee_mettings_by_id()
    {
        $data=array();
        $header=getallheaders();
        if(!empty($header['Authorization']))
        {
            $token=$this->Attendee_model->get_token();
            if($header['Authorization']==$token[0]['token'] && strtotime($token[0]['exiprydate']) > strtotime(date('Y-m-d H:i:s')))
            {
                if(!empty($this->input->get_post('emailAddress')))
                {
                    if(filter_var($this->input->get_post('emailAddress'),FILTER_VALIDATE_EMAIL))
                    {
                        $eid='447';
                        $org_id='4659';
                        $email=$this->input->get_post('emailAddress');
                        $Meetings=$this->Attendee_model->get_all_metting_by_user_email($email,$eid);
                        if(count($Meetings) > 0)
                        {
                            $data['Meetings']=$Meetings;
                        }
                        else
                        {
                            $data=array('msg'=>"Attendee Metting data Not Found.");  
                        }
                    }
                    else
                    {
                        $data=array('error'=>"Invalid Visitor Email Address.");       
                    }
                }
                else
                {
                    $data=array('error'=>"Visitor Email Address Must Be required.");
                }
            }
            else
            {
                $data=array('error'=>"Authorization Token is Exipry");
            }
        }
        else
        {
            $data=array('error'=>"Authorization Token must be required.");
        }       
        echo json_encode($data);die;
    }
}*/
?>