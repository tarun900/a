<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('../application/libraries/Rssparser.php');

class Apiv3 extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_model');
        $this->load->model('Add_attendee_model');
        $this->load->model('Event_model');
        $this->load->model('Allapi_model');
    }
    public function index( $offset = 0 )
    {
         die("Direct Access Denied..");
    }

    public function add_temp_SugarEthanolBrazil()
    {
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKB2277/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }

    public function add_SugarEthanolBrazil_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1346);
        //$this->Allapi_model->add_KNECT365_speakers(1346);
        $this->Allapi_model->add_KNECT365_session(1346, 1257);
        echo "SuccessFully.....";
        die;

    }

    public function add_temp_GlobalPortMarineOperations()
    {
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3462/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }

    public function add_GlobalPortMarineOperations_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1536);
        //$this->Allapi_model->add_KNECT365_speakers(1536);
        $this->Allapi_model->add_KNECT365_session(1536, 1507);
        echo "SuccessFully.....";
        die;

    }

    public function TestSql($again=false,$tramscation_count)
    {   
        error_reporting(E_ALL);
        //insert data
        try
        {   

            if($again)
            {
                switch ($tramscation_count) 
                {   
                    case '1':
                            for ($i=1; $i <= 100; $i++)
                            {   
                                $insert[$i]['start_time'] = microtime();
                                $insert[$i]['`interval`'] = "$i";
                            }
                            $this->db->insert_batch('records_load_100',$insert);
                            $data[] = $this->db->affected_rows();
                            $tramscation_count++;
                        break;

                    case '2':
                            $res = $this->db->where('`interval`','2')->get('records_load_100')->result_array();
                            $data[] = $this->db->affected_rows();
                            $tramscation_count++;
                        break;

                    case '3':
                            $update['`interval`'] = '2000';
                            $where['`interval`'] = '2';
                            $this->db->where($where);
                            $this->db->update('records_load_100',$update);
                            $data[] = $this->db->affected_rows();
                            $tramscation_count++;
                        break;

                    case '4':
                            $this->db->where('`interval`','2000');        
                            $this->db->delete('records_load_100');
                            $data[] = $this->db->affected_rows();
                            $tramscation_count++;
                        break;
                }
                if($tramscation_count <= 4)
                    $this->TestSql(true,$tramscation_count);
            }
            else
            {
                $tramscation_count = 0;
                for ($i=1; $i <= 100; $i++)
                {   
                    $insert[$i]['start_time'] = microtime();
                    $insert[$i]['`interval`'] = "$i";
                }
                $this->db->insert_batch('records_load_100',$insert);
                $data[] = $this->db->last_query();
                $tramscation_count++;

                //select data
                $res = $this->db->where('`interval`','2')->get('records_load_100')->result_array();
                $data[] = $this->db->last_query();
                $tramscation_count++;
                

                //update data
                $update['`interval`'] = '2000';
                $where['`interval`'] = '2';
                $this->db->where($where);
                $this->db->update('records_load_100',$update);
                $data[] = $this->db->last_query();
                $tramscation_count++;


                //delete data
                $this->db->where('`interval`','2000');        
                $this->db->delete('records_load_100');
                $data[] = $this->db->last_query();
                $tramscation_count++;
                $this->db->close();
/*
                $data = json_encode($data);
                $this->db->insert('Test_sql',$data)*/;

            }

        }
        catch( Exception $e ) {
          throw $e;
            $this->TestSql(true,$tramscation_count);
            echo "<br>transcation count".$tramscation_count;

        }
        
    }
    public function db_connection_test($value='')
    {   
        //$this->load->database();
        $this->load->dbutil();
        $this->db->close();
        // check connection details
        if( !$this->dbutil->database_exists('amber_allnet'))
        {
            echo 'Not connected to a database, or database not exists';
        }
        else
        {
            echo "connected";
        }
        j($this->db);
    }

    public function add_temp_Knect_GlobalLiner()
    {
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3423/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }

    public function add_Knect_GlobalLiner_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1366);
        //$this->Allapi_model->add_KNECT365_speakers(1366);
        $this->Allapi_model->add_KNECT365_session(1366, 1329);
        echo "SuccessFully.....";
        die;

    }
    public function EPSON_attendee($value='')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.eventsforce.net/appointmentgroup/api/v2/events/206/attendees.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization: Basic QVBQT0lOVE1FTlRHUk9VUDpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ==",
            "cache-control: no-cache",
            "postman-token: ab6ef232-963a-6467-5c9c-b9f8a430a4a7"
          ),
        ));

        $response = curl_exec($curl);

        //echo "<pre>";
        

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        }
        else
        {   
            $response = json_decode($response,true)['data'];
            foreach ($response as $key => $value)
            {   
                $insert['Firstname'] = $value['firstName'];
                $insert['Lastname'] = $value['lastName'];
                $insert['Email'] = $value['email'];
                $insert['Title'] = $value['jobTitle'];
                $insert['Salutation'] = $value['title'];
                $insert['Company_name'] = $value['company'];
                $insert['Mobile'] = $value['phoneNumber'];
                $insert['Phone_business'] = $value['workPhoneNumber'];

                $opts = array(
                          'http'=>array(
                            'method'=>"GET",
                            'header'=>"Authorization: Basic QVBQT0lOVE1FTlRHUk9VUDpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ==",
                                "Cache-Control: no-cache",
                                "Postman-Token: ab6ef232-963a-6467-5c9c-b9f8a430a4a7"
                            )
                        );
                $context = stream_context_create($opts);
                $extra_column = json_decode(file_get_contents($value['detailsURL'],false, $context),true)['data'];
                $insert['Country'] = $extra_column['country'];
                $insert['Postcode'] = $extra_column['postCode'];

                $insert_extra['Work Phone Number'] = $value['workPhoneNumber'];
                $insert_extra['Address Line 1'] = $extra_column['addressLine1'];
                $insert_extra['Address Line 2'] = $extra_column['addressLine2'];
                $insert_extra['Post Code'] = $extra_column['postCode'];
                $insert_extra['Town'] = $extra_column['town'];
                $insert_extra['County'] = $extra_column['county'];
                $insert_extra['Country'] = $extra_column['country'];

                $allKeys = array_column($extra_column['customRegistrationData'], 'key');
                $allValues = array_column($extra_column['customRegistrationData'], 'value');
                $customData = array_combine($allKeys,$allValues);
                $finalMerge = array_merge($insert_extra,$customData);
                $insert['extra_column'] = json_encode($finalMerge);

                $check_tmp_user = $this->db->where('Email',$value['email'])->get('temp_epson_users')->row_array();
                if(empty($check_tmp_user))
                {
                    $this->db->insert('temp_epson_users',$insert);
                }
                else
                {   
                    $this->db->where($check_tmp_user);
                    $this->db->update('temp_epson_users',$insert);  
                }
            }
            j('Temp Data Addedd Successfully');
        }
    }

    public function add_attendees_epson()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $temp_epson_users = $this->db->limit('50')->get('temp_epson_users')->result_array();
        
        foreach ($temp_epson_users as $key => $value)
        {  
            $event_id = 876;
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
            $Email = $value['Email'];
            $data = $value;
            unset($data['id']);
            unset($data['extra_column']);
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $user_id = $user['Id'];
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
            }
            $event_attendee = $this->db->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get('event_attendee')->row_array();
            if(!empty($event_attendee))
            {
                $update['extra_column'] = $value['extra_column'];
                $this->db->where($event_attendee);
                $this->db->update('event_attendee',$update);
            }
            $this->db->where($value);
            $this->db->delete('temp_epson_users');
        }
        $temp_epson_users = $this->db->get('temp_epson_users')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $temp_epson_users . " Refresh Page -> " . round($temp_epson_users / 50 ) . " Time";
        j($msg);
    }

    public function csv_to_sql()
    {   
        error_reporting(E_ALL);


        $file_path =  'http://allintheloop.net/assets/test/FHA.csv';
        $this->db->query("LOAD DATA LOCAL INFILE '".$file_path."' 
                           INTO TABLE user_tmp 
                           FIELDS TERMINATED BY ','
                           IGNORE 1 LINES
                           (@var1,@var2)
                           SET Unique_No = @var1,Firstname = @var2
                           ");
        
        lq();
    }
    public function upload_mass_attendee_source_media()
    {   
        $this->load->model('Attendee_model');
        $this->load->model('Event_model');
        $eid = 1378;
        $custom=$this->Attendee_model->get_all_custom_modules($eid);
        $event = $this->Event_model->get_admin_event($eid);

        $tmp_attendee = $this->db->limit(100)->get('user_tmp')->result_array();

        foreach ($tmp_attendee as $key1 => $value1)
        {   
            $datacsv = array_values($value1);
            if(!empty($datacsv[2]))
            {
                $data['firstname']=$datacsv[0];
                $data['lastname']=$datacsv[1];
                $data['Email']=$datacsv[2];
                $data['title']=$datacsv[3];
                $data['company_name']=$datacsv[4];
                $data['keywords']=$datacsv[6];
                $data['Unique_no']=$datacsv[7];
                if(!empty($datacsv[5]))
                {
                    $images="profilelogo".uniqid().".png";
                    copy($datacsv[5],"./assets/user_files/".$images);
                    $data['Logo']=$images;
                }
                $extra_arr=array();
                $i=8;
                foreach ($custom as $key => $value)
                {
                    if(!empty($datacsv[$i]))
                    {
                        $extra_arr[$value['column_name']]=$datacsv[$i];
                    }
                    $i++;
                }
                if(count($extra_arr) > 0)
                {
                    $data['extra_column']=json_encode($extra_arr);
                }
                $this->Attendee_model->add_mass_attendee_without_invite_as_user($data,$eid,$event[0]['Organisor_id']);
                $tmp_data[] = $data;
                $this->db->where('Email',$value1['Email']);
                $this->db->delete('user_tmp');
                unset($data);
            } 
        }
        j($tmp_data);
    }
    public function assign_epson_agenda()
    {   
        $this->db->where('tus.agenda_category_id IS NOT NULL');
        $this->db->where('tus.user_id IS NOT NULL');
        $this->db->group_by('tus.id');
        $res = $this->db->get('tmp_user_session tus')->result_array();
        foreach ($res as $key => $value)
        {
            $insert['attendee_id'] = $value['user_id'];
            $insert['agenda_category_id'] = $value['agenda_category_id'];
            $insert['event_id'] = '876';
            $this->db->where($insert);
            $data = $this->db->get('attendee_agenda_relation')->row_array();
            if(empty($data))
            {
                $this->db->insert('attendee_agenda_relation',$insert);
            }
        }
        $ids = array_column($res,'id');
        $this->db->where_in('id',$ids);
        $this->db->delete('tmp_user_session');
        /*foreach ($res as $key => $value)
        {
            $this->db->where('user_id',$value['user_id']);
            $users_agenda = $this->db->get('users_agenda')->row_array();
            if(!empty($users_agenda))
            {
                $old_agenda_ids = explode(',',$users_agenda['agenda_id']);
                if(!empty($old_agenda_ids))
                {
                    $new_agenda_id = explode(',',$value['agenda_id']);
                    $new_agenda_id = array_unique(array_filter(array_merge($old_agenda_ids,$new_agenda_id)));
                    $update['agenda_id'] = implode(',',$new_agenda_id);
                }
                else
                {
                    $update['agenda_id'] = $value['agenda_id'];
                }
                $this->db->where($users_agenda);
                $this->db->update('users_agenda',$update);
                }
            else
            {
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['user_id'] = $value['user_id'];
                $this->db->insert('users_agenda',$insert);
                unset($insert);
            }
            $this->db->where('id',$value['id']);
            $this->db->delete('tmp_user_session');
        }*/
        j('data imported successfully');
    }
    public function scrape_iot_speakers()
    {   
        // show_errors();
        error_reporting(0);
        include($_SERVER['DOCUMENT_ROOT'].'/simple_html_dom.php');
        $html = file_get_html("https://www.iottechexpo.com/europe/speakers/");
        // $displaybody = $html->find('div[class=speaker_list]', 0)->plaintext;
        $i = 0;
        foreach ($html->find('div[class=speaker-top-expand]') as $value)
        {   
            $i++;
            $data[$i]['logo'] = $value->find('img')[0]->attr['src'];
            $data[$i]['company_logo'] = $value->find('img')[1]->attr['src'];
            $data[$i]['name'] = $value->getElementByTagName('h3')->plaintext;
            $tmp = $value->getElementsByTagName('h4');
            foreach ($tmp as $val)
            {
                $t_c[] = $val->plaintext;            
            }
            $tmp = $value->getElementsByTagName('h4');
            $data[$i]['title'] = $t_c[0];
            $data[$i]['company'] = $t_c[1];
            $html2 = file_get_html($value->find('a')[0]->attr['href']);
            foreach ($html2->find('div[class=sbio-text]') as  $val1)
            {   
                foreach($val1->getElementsByTagName('p') as $val2)
                {   
                        $data[$i]['desc'] .= htmlentities($val2);
                }
            }
            foreach($html2->find('div[class=sd_agenda-day]') as $v1)
            {   
                $data[$i]['agenda_day'][] = $v1->plaintext;
            }
            foreach($html2->find('div[class=sd_agenda-title]') as $v1)
            {
                $data[$i]['agenda'][] = $v1->getElementByTagName('h5')->plaintext;
            }
            // j($data);
            unset($t_c);
        }
        j($data);
        echo $displaybody ;exit;
    }
    public function backupDB()
    {   date_default_timezone_set('UTC -4');
        show_errors();
        // j($_SERVER);

        $folder_name = '/home/allintheloop/public_html/db/'.date('d-m-Y-H:i:s');
        if (!file_exists($folder_name)) {
            mkdir($folder_name, 0777, true);
        }
        $tables = array_column($this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='amber_allnet'")->result_array(),'TABLE_NAME');
        sort($tables);
        foreach ($tables as $value)
        {
            $filename=$value.'-'.date('d-m-Y-H-i-s').'.sql';
            $result=exec('mysqldump  --password=WFe2GR76 --user=amber_allnet --single-transaction amber_allnet '.$value.' >'.$folder_name.'/'.$filename,$output);
        }
        $filename='FULL_DB-'.date('d-m-Y-H-i-s').'.sql';
        $result=exec('mysqldump  --password=WFe2GR76 --user=amber_allnet --single-transaction amber_allnet >'.$folder_name.'/'.$filename,$output);
        j('done');
    }
    public function add_attendees_dig_banking()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $dig_tmp_attendee = $this->db->limit('100')->get('dig_tmp_attendee')->result_array();
        $event_id = 1512;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($dig_tmp_attendee as $key => $value)
        {  
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                unset($data['a1']);
                unset($data['a2']);
                $data['Street'] = $value['a1'].','.$value['a2'];
                $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                $data['Country'] = ($country_id['id'])?:$value['Country'];
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where($value);
                $this->db->delete('dig_tmp_attendee');
            }
        }
        $dig_tmp_attendee = $this->db->get('dig_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $dig_tmp_attendee . " Refresh Page -> " . round($dig_tmp_attendee / 100 ) . " Time";
        j($msg);
    }
    public function testWebhook()
    {    
        $entityBody = file_get_contents('php://input');
        $insert['text'] = $entityBody;
        $this->db->insert('webhook',$insert);
        $event_id = 1511;
        $this->load->model('Native_single_fcm/App_login_model');
        $org_id = $this->App_login_model->getOrganizerByEvent($event_id);

        $data = json_decode($entityBody,true);


        if($data['config']['action'] == 'attendee.updated')
        {   
            $getAttendee = file_get_contents($data['api_url'].'?token=YXVSLZPEJFGM7MQ67M55');
            $getAttendee = json_decode($getAttendee,true);
            $value['Firstname'] = $getAttendee['profile']['first_name'];
            $value['Lastname'] = $getAttendee['profile']['last_name'];
            $value['Email'] = $getAttendee['profile']['email'];
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {   
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
            }
        }
        header('Content-Type: application/json');
        $return['success'] = 200;
        $return['message'] = 'Attendee Added Successfully';
        $return['data'] = $value;
        echo json_encode($return,JSON_PRETTY_PRINT);exit();
    }
    public function podcast_tmp_attendee()
    {
        $tmp_data = file_get_contents('https://www.eventbriteapi.com/v3/events/37233649849/attendees/?token=SIHG3UU3LSYDBDZZWVHF');
        $tmp_data = json_decode($tmp_data,true);
        $total_page = $tmp_data['pagination']['page_count'];
        for ($i=21; $i <= $total_page; $i++)
        { 
            $tmp_data = file_get_contents('https://www.eventbriteapi.com/v3/events/37233649849/attendees/?token=SIHG3UU3LSYDBDZZWVHF&page='.$i);
            $tmp_data = json_decode($tmp_data,true);
            $data['page'.$i] = $tmp_data['attendees'];
        }
        $attendees = array();
        foreach ($data as $key => $value)
        {
            foreach ($value as $k1 => $v1)
            {   
                $k2 = count($attendees);
                $k2++;
                $attendees[$k2]['Firstname'] = $v1['answers'][array_search(16240571, array_column($v1['answers'], 'question_id'))]['answer']?:$v1['profile']['first_name'];

                $attendees[$k2]['Lastname'] = $v1['answers'][array_search(16240572, array_column($v1['answers'], 'question_id'))]['answer']?:$v1['profile']['last_name'];

                $attendees[$k2]['Title'] = $v1['profile']['job_title']?:'';
                $attendees[$k2]['Email'] = $v1['profile']['email'];
                $attendees[$k2]['Company_name'] = $v1['answers'][array_search(16240573, array_column($v1['answers'], 'question_id'))]['answer'];
                $attendees[$k2]['ticket_type'] = $v1['ticket_class_name'];
            }
        }
        // j($attendees);
        $this->db->insert_batch('podcast_tmp_attendee',$attendees);
        $query = 'update podcast_tmp_attendee pu
                  join podcast_tmp_user_type ug on ug.api_ticket= pu.ticket_type
                  set pu.ticket_type = ug.group_id';
        $this->db->query($query);
        j($attendees);
    }
    public function podcast_attendee_add($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $podcast_tmp_attendee = $this->db->limit('100')->get('podcast_tmp_attendee')->result_array();
                break;            
            case 2:
                $podcast_tmp_attendee = $this->db->limit('100','100')->get('podcast_tmp_attendee')->result_array();
                break;
            case 3:
                $podcast_tmp_attendee = $this->db->limit('100','200')->get('podcast_tmp_attendee')->result_array();
                break;
            case 4:
                $podcast_tmp_attendee = $this->db->limit('100','300')->get('podcast_tmp_attendee')->result_array();
                break;
        }
        // j($podcast_tmp_attendee);
        // $podcast_tmp_attendee = $this->db->limit('100')->get('podcast_tmp_attendee')->result_array();
        $event_id = 1381;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($podcast_tmp_attendee as $key => $value)
        {  
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                $group_id = $value['ticket_type'];
                unset($data['id']);
                unset($data['ticket_type']);
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {   
                    if(empty($data['Title']))
                        unset($data['Title']);

                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                if(is_numeric($group_id))
                {
                    $this->db->where('Event_id',$event_id);
                    $this->db->where('Attendee_id',$user_id);
                    $res = $this->db->get('event_attendee')->row_array();
                    if(!empty($res))
                    {   
                        $update['group_id'] = $group_id;
                        $this->db->where($res)->update('event_attendee',$update);
                    }
                    else
                    {
                        $insert['Event_id'] = $event_id;
                        $insert['Attendee_id'] = $user_id;
                        $insert['group_id'] = $group_id;
                        $this->db->insert('event_attendee',$insert);
                    }
                }
                $this->db->where('Email',$Email);
                $this->db->delete('podcast_tmp_attendee');
            }
        }
        $podcast_tmp_attendee = $this->db->get('podcast_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $podcast_tmp_attendee . " Refresh Page -> " . round($podcast_tmp_attendee / 100 ) . " Time";
        j($msg);
    }
    public function getEventUpdateLog()
    {
        $event_ids = array_column($this->db->select('Id')->where('Id >','1086')->get('event')->result_array(),'Id');
        foreach ($event_ids as $key => $value)
        {
            $updated = json_decode(file_get_contents('https://www.allintheloop.net/native_single_fcm/app/getModulesUpdatedLogs?event_id='.$value),true);
            unset($updated['success']);
            unset($updated['updated_date']);
            unset($updated['super_group']);
            unset($updated['success']);
            foreach ($updated as $k1 => $v1)
            {
                $data['event_id'] = $value;
                $data['module_name'] = $k1;
                $data['updated_date'] = ($v1)?:NULL;
                $this->db->insert('modules_update_log',$data);               
            }
        }
        j('Data Updated');
    }
    public function poadcastWebhook()
    {    
        $entityBody = file_get_contents('php://input');
        $insert['text'] = $entityBody;
        $this->db->insert('webhook',$insert);
        $event_id = 1381;
        $this->load->model('Native_single_fcm/App_login_model');
        $org_id = $this->App_login_model->getOrganizerByEvent($event_id);

        $data = json_decode($entityBody,true);
        if($data['config']['action'] == 'attendee.updated')
        {   
            $getAttendee = file_get_contents($data['api_url'].'?token=SIHG3UU3LSYDBDZZWVHF');
            $getAttendee = json_decode($getAttendee,true);
            $value['Firstname'] = $getAttendee['answers'][array_search(16240571, array_column($getAttendee['answers'], 'question_id'))]['answer']?:$getAttendee['profile']['first_name'];

            $value['Lastname'] = $getAttendee['answers'][array_search(16240572, array_column($getAttendee['answers'], 'question_id'))]['answer']?:$getAttendee['profile']['last_name'];

            $value['Title'] = $getAttendee['profile']['job_title']?:'';
            $value['Email'] = $getAttendee['profile']['email'];
            $value['Company_name'] = $getAttendee['answers'][array_search(16240573, array_column($getAttendee['answers'], 'question_id'))]['answer'];
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {   
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
            }
            header('Content-Type: application/json');
            $return['success'] = 200;
            $return['message'] = 'Attendee Added Successfully';
            $return['data'] = $value;
            echo json_encode($return,JSON_PRETTY_PRINT);exit();
        }
        echo "Bad Request";exit;
    }
    public function testUpdateDate()
    {   
        $event_id = $this->input->post('event_id');
        if($event_id)
        {
            $this->db->where('event_id',$event_id);
            $res = $this->db->get('modules_update_log')->result_array();
            foreach ($res as $key => $value)
            {
                $data[$value['module_name']] = ($value['updated_date'])?:'';
            }
            $data['success'] = true;
            echo json_encode($data);exit;
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
            echo json_encode($data);exit;
        }
    }
    public function updateModuleDate($event_id,$module)
    {   
        $where['event_id'] = $event_id;
        $where['module_name'] = $module;
        if($this->db->where($where)->get('modules_update_log')->row())
        {   
            $data['updated_date'] = date('Y-m-d H:i:s');
            $this->db->where($where)->update('modules_update_log',$data);
        }
        else
        {   
            $data = $where;
            $data['updated_date'] = date('Y-m-d H:i:s');
            $this->db->insert('modules_update_log',$data);
        }
    }
    public function add_attendees_fime()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $fime_tmp_attendee = $this->db->limit('500')->get('fime_tmp_attendee')->result_array();
        $event_id = 1580;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($fime_tmp_attendee as $key => $value)
        {  
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {   
                $this->db->join('relation_event_user reu','reu.User_id = u.Id');
                $this->db->where('u.Email',$value['Email']);
                $this->db->where('reu.Role_id','3');
                $is_org = $this->db->get('user u')->row_array();
                if(empty($is_org))
                {
                    $this->db->select('u.*');
                    $this->db->join('relation_event_user reu','reu.User_id = u.Id');
                    $this->db->where('u.Unique_no',$value['Unique_no']);
                    $this->db->where('reu.Event_id',$event_id);
                    $user = $this->db->get('user u')->row_array();
                    $Email = $value['Email'];
                    $data = $value;
                    unset($data['a1']);
                    unset($data['a2']);
                    $data['Street'] = $value['a1'].','.$value['a2'];
                    $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                    $data['Country'] = ($country_id['id'])?:$value['Country'];
                    if($user)
                    {   
                        $data['updated_date'] = date('Y-m-d H:i:s');
                        $this->db->where('Id',$user['Id']);
                        $this->db->update('user',$data);
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                        $user_id = $user['Id'];
                    }
                    else
                    {
                        $data['Organisor_id'] = $org_id;
                        $data['Created_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('user',$data);
                        $user_id=$this->db->insert_id();
                        $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                    }
                    
                    $this->db->where($value);
                    $this->db->delete('fime_tmp_attendee');
                }
            }
        }
        $fime_tmp_attendee = $this->db->get('fime_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $fime_tmp_attendee . " Refresh Page -> " . round($fime_tmp_attendee / 500 ) . " Time";
        j($msg);
    }
    public function add_attendees_invest()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $invest_tmp_attendee = $this->db->limit('100')->get('invest_tmp_attendee')->result_array();
        $event_id = 1609;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($invest_tmp_attendee as $key => $value)
        {  
            if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {   
                $this->db->select('u.*');
                $this->db->join('relation_event_user reu','reu.User_id = u.Id');
                $this->db->where('u.Unique_no',$value['Unique_no']);
                $this->db->where('reu.Event_id',$event_id);
                $user = $this->db->get('user u')->row_array();
                $Email = $value['Email'];
                $data = $value;
                unset($data['a1']);
                unset($data['a2']);
                $data['Street'] = $value['a1'].','.$value['a2'];
                $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                $data['Country'] = ($country_id['id'])?:$value['Country'];
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Id',$user['Id']);
                    $this->db->update('user',$data);
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where($value);
                $this->db->delete('invest_tmp_attendee');
            }
        }
        $invest_tmp_attendee = $this->db->get('invest_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $invest_tmp_attendee . " Refresh Page -> " . round($invest_tmp_attendee / 100 ) . " Time";
        j($msg);
    }
    public function invest_fill_lead_firstname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1609');
        $this->db->where('(el.firstname = "" || el.firstname IS NULL || el.firstname = "(null)")');
        $this->db->where('(u.Firstname != "" && u.Firstname IS NOT NULL && u.Firstname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }
    public function invest_fill_lead_email()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1609');
        $this->db->where('(el.email = "" || el.email IS NULL || el.email = "(null)")');
        $this->db->where('(u.Email != "" && u.Email IS NOT NULL && u.Email != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->get('exibitor_lead el')->result_array();
        // j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
            unset($update);
        }
        j($data);
    }
    public function invest_fill_lead_lastname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1609');
        $this->db->where('(el.lastname = "" || el.lastname IS NULL || el.lastname = "(null)")');
        $this->db->where('(u.Lastname != "" && u.Lastname IS NOT NULL && u.Lastname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }
    public function pullWPData()
    {   
        require 'HttpRequest.php';
        function getData($i)
        {   
            $r = new HttpRequest("get", "https://www.concordia.net/wp-json/wp/v2/posts?per_page=100&page=".$i);
            if ($r->getError()) {
                return 0;
            } else {
                $data = json_decode($r->getResponse());
                return $data;
            }
        }
        $j = 0;
        for ($i=1; $i < 50; $i++)
        { 
            $tmpdata = getData($i);
            if($tmpdata == 0)
            {   
                foreach ($data as $key => $value)
                {   
                    foreach ($value as $key1 => $value1)
                    {       
                        $last_date = $this->db->where('organizer_id','15663')->order_by('date','desc')->get('rss_feed')->row();
                        if(strtotime($value1->date) > strtotime($last_date->date))
                        {
                            $insert[$j]['title'] = $value1->title->rendered;
                            $insert[$j]['content'] = $value1->content->rendered;
                            $insert[$j]['link'] = $value1->link;
                            $insert[$j]['date'] = date('Y-m-d H:i:s',strtotime($value1->date));
                            $insert[$j]['organizer_id'] = '15663';
                            $featuredmedia = json_decode(json_encode($value1->_links), true);
                            $r = new HttpRequest("get", $featuredmedia['wp:featuredmedia'][0]['href']);
                            if ($r->getError()) {
                                $insert[$j]['image'] = '';
                            } else {
                                $featuredmedia = json_decode($r->getResponse());
                                $insert[$j]['image'] = $featuredmedia->source_url;
                            }
                            $j++;
                        }
                    }
                    if(!empty($insert))
                    $this->db->insert_batch('rss_feed',$insert);
                    j($insert);
                }
            }
            else
            {
                $data[$i] = $tmpdata;
            }
        }
    }

    public function assignHowardAgenda()
    {   
        //error_reporting(E_ALL);
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('howard_user_session_temp_j uct','uct.user_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->group_by('u.Id');
        $this->db->where('uct.user_id != ""');
        $this->db->limit(100);
        $res = $this->db->get()->result_array();
        foreach ($res as $key => $value)
        {
            $tmp_user_agenda = $this->db->where('user_id',$value['Id'])->get('users_agenda')->row_array();
            if($tmp_user_agenda)
            {
                $tmp_agenda = explode(',',$tmp_user_agenda['agenda_id']);
                $new_agenda  = explode(',',$value['agenda_id']);
                $fin_agenda = array_merge($tmp_agenda,$new_agenda);
                $this->db->where($tmp_user_agenda);
                $update['agenda_id'] = implode(',',array_unique($fin_agenda));
                $this->db->update('users_agenda',$update);
            }   
            else
            {
                $insert['user_id'] = $value['Id'];
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['updated_date'] = date('Y-m-d H:i:s');
                $this->db->insert('users_agenda',$insert);
            }
            $this->db->where('email',$value['email']);
            $this->db->delete('howard_user_session_temp_j');
        }
        j('data added successfully');
    }

    public function vitafood_Exhi()
    {    
        //error_reporting(E_ALL);
        //$curl = curl_init();

        $curl = curl_init();
          curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.showoff.asp.com/public/exhibitor?limit=500",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic N0Q0QzREMENERDcwQkREMDMwQTU3RERGNUEwN0M0NDYxRTE0NzcyMDp3LmRzXi5WNlchfm1IRSgu",
            "Cache-Control: no-cache",
            "Postman-Token: d298ea74-f872-4903-8aef-09d9d6824be4"
          ),
        ));
        $response = curl_exec($curl);

        $data = json_decode($response);

        if($_GET['test'])
        {
            j($data[0]->exhibitor);
        }
        $this->load->model('exibitor_model');

        $i = 0;
        foreach ($data[0]->exhibitor  as $key => $value)
        {   
            $Exhibitor[$i]['twitter_url'] = $value->social_twitter;
            $Exhibitor[$i]['FirstName'] = $value->contact[0]->FirstName;
            $Exhibitor[$i]['LastName'] = $value->contact[0]->LastName;
            $Exhibitor[$i]['Email'] = $value->contact[0]->emailAddress;
            $Exhibitor[$i]['company_logo'] = $value->logo;
            $Exhibitor[$i]['facebook_url'] = $value->social_facebook;
            $Exhibitor[$i]['stand_number'] = $value->stand;
            $Exhibitor[$i]['Description'] = $value->description;
            $Exhibitor[$i]['linkedin_url'] = $value->social_linkedin;
            $Exhibitor[$i]['Heading'] = $value->Company;
            $Exhibitor[$i]['youtube_url'] = $value->social_youtube;
            $Exhibitor[$i]['instagram_url'] = $value->social_instagram;
            $Exhibitor[$i]['website_url'] = $value->websites;
            
            $country[] = $value->Country;
            $tags = explode(',',$value->tags);
            $category = array_column(json_decode(json_encode($value->category), true),'Label');
            $short_desc = array_filter(array_merge($country,$tags,$category));
            $Exhibitor[$i]['Short_desc'] = implode(',',$short_desc);
            $Exhibitor[$i]['country_id'] = $this->exibitor_model->getCountryIdFromName($value->Country);
            $i++;
        }
        $this->db->insert_batch('vitafood_exibitors',$Exhibitor);
        j($Exhibitor);
        j('Data inserted Successfully');
    }
    public function vitafood_Exhi_add()
    {   
        //error_reporting(E_ALL);
        // die("Direct Access Denied..");
        $oid = '322861';
        $eid = '1397';
        $temp_exibitor = $this->db->select('*')->from('vitafood_exibitors')->limit('50')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['LastName'];
                $user_data['Company_name'] = $value['Heading'];
                $user_data['Country'] = $value['country_id'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {   
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                
                $ebi_data['twitter_url'] = $value['twitter_url'];
                $ebi_data['main_email_address'] = $value['Email'];
                $ebi_data['facebook_url'] = $value['facebook_url'];
                $ebi_data['Short_desc'] = $value['Short_desc'];
                $ebi_data['stand_number'] = $value['stand_number'];
                $ebi_data['Description'] = $value['Description'];
                $ebi_data['linkedin_url'] = $value['linkedin_url'];
                $ebi_data['Heading'] = $value['Heading'];
                $ebi_data['youtube_url'] = $value['youtube_url'];
                $ebi_data['linkedin_url'] = $value['linkedin_url'];
                $ebi_data['instagram_url'] = $value['instagram_url'];
                $ebi_data['website_url'] = $value['website_url'];
                $ebi_data['country_id'] = $value['country_id'];

                if (!empty($value['company_logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['company_logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                
                $ebi_data['Images'] = NULL;
                
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Heading']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('vitafood_exibitors');
        }
        echo "Success";
        updateModuleDate($eid,'exhibitor');
        die;
    }

    public function add_vitafood_attendee()
    {   
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $Password = $this->input->post('Password');
        $custom_column = json_decode($this->input->post('custom_column'),true);

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            foreach ($custom_column as $key => $value)
            {   
                $tmp_custom_column[strip_tags($value['key'])] = $value['value'];
                if(strip_tags($value['key']) == "Do you want to be part of our pre-event 'Distributor Connect' networking group? ")
                {
                    if($value['value'] == 'Yes')
                    {
                        $event_attendee['group_id'] = '148';
                    }
                    elseif($value['value'] == 'No')
                    {
                        $event_attendee['group_id'] = '197';
                    }
                }
            }
            $event_attendee['extra_column'] = json_encode($tmp_custom_column);
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            /*if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1397;//1397
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            if(!empty($Password))
                $data['Password'] = md5($Password);
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
            //save extra column
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id',$event_id);
            $this->db->where('Attendee_id',$user_id);
            $qu=$this->db->get();
            $extra_info=$qu->row_array();
            if(!empty($extra_info))
            {
                $this->db->where('Event_id',$event_id);
                $this->db->where('Attendee_id',$user_id);
                $this->db->update('event_attendee',$event_attendee);
            }
            else
            {
                $event_attendee['Event_id']=$event_id;
                $event_attendee['Attendee_id']=$user_id;
                $this->db->insert('event_attendee',$event_attendee);
            }
            if($event_attendee['group_id'] != '197')
            {
                $this->vitafoodSaveExtraColumn($event_id);
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function fime_fill_lead_firstname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1580');
        $this->db->where('(el.firstname = "" || el.firstname IS NULL || el.firstname = "(null)")');
        $this->db->where('(u.Firstname != "" && u.Firstname IS NOT NULL && u.Firstname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }
    public function fime_fill_lead_email()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1580');
        $this->db->where('(el.email = "" || el.email IS NULL || el.email = "(null)")');
        $this->db->where('(u.Email != "" && u.Email IS NOT NULL && u.Email != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->get('exibitor_lead el')->result_array();
        // j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
            unset($update);
        }
        j($data);
    }
    public function fime_fill_lead_lastname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','1580');
        $this->db->where('(el.lastname = "" || el.lastname IS NULL || el.lastname = "(null)")');
        $this->db->where('(u.Lastname != "" && u.Lastname IS NOT NULL && u.Lastname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }
    public function test()
    {
        $custom_column = array_column($this->db->where('event_id',1397)->get('custom_column')->result_array(),'column_name');
        foreach ($custom_column as $key => $value)
        {
            $tmp_custom_column[$key]['key'] = $value;
            $tmp_custom_column[$key]['value'] = 'Your Ans.';
        }
        $tmp_custom_column = json_encode($tmp_custom_column);
        echo $tmp_custom_column;exit();
    }
    public function add_mema_attendee()
    {   
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $remove = $this->input->post('remove');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            

            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }

            
            /*$check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1761;
            if($remove == '1')
            {   
                $this->removeAttendee($event_id,$Email);
                $data = array(
                    'success' => false,
                    'message' => 'Attendee Removed'
                );
                echo json_encode($data);exit;
            }
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();


            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function add_ppma_attendee()
    {   
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            /*$check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1394;
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function add_HITEC_HOUSTON_attendee()
    {   
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            /*$check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1404;
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function add_Annual_Convention_attendee()
    {   
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            /*$check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1413;
            $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['Active'] = '1';
            $data['is_from_api'] = '1';
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function removeAttendee($event_id,$email)
    {   
        $this->db->select('reu.*');
        $this->db->join('relation_event_user reu','u.Id = reu.User_id');
        $this->db->where('u.Email',$email);
        $this->db->where('reu.Event_id',$event_id);
        $this->db->where('reu.Role_id','4');
        $res = $this->db->get('user u')->row_array();
        if(!empty($res))
        {
            $this->db->where($res);
            $this->db->delete('relation_event_user');
        }
    }
    public function vitafoodSaveExtraColumn($event_id=1511)
    {   
        $this->db->where('event_id',$event_id);
        $this->db->where('(extra_column IS NOT NULL AND extra_column != "null")');
        $this->db->where('Attendee_id not in (select distinct user_id from attendee_extra_column where event_id='.$event_id.')');
        $res = $this->db->get('event_attendee')->result_array();
        $j = 0;
        foreach ($res as $key => $value)
        {   
            $json = json_decode($value['extra_column'],true);
            foreach ($json as $key1 => $value1)
            {   
                if(!empty($value1) && $value1 != 'null' && $value1 != NULL)
                {
                    $insert[$j]['event_id'] = $value['Event_id'];
                    $insert[$j]['user_id'] = $value['Attendee_id'];
                    $insert[$j]['key1'] = "$key1";
                    $insert[$j]['value'] = $value1;
                    $j++;
                }
            }
        }
        if(!empty($insert))
            $this->db->insert_batch('attendee_extra_column',$insert);
    }
    public function vitafoodProducts()
    {   
        //Omega-3 Product Showcases  : 1531
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.showoff.asp.com/public/libraries/029A1253-1825-4127-B1A-929844C8C271/entries?l=100",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic N0Q0QzREMENERDcwQkREMDMwQTU3RERGNUEwN0M0NDYxRTE0NzcyMDp3LmRzXi5WNlchfm1IRSgu",
            "Cache-Control: no-cache",
            "Postman-Token: 5f6164fc-fe5f-40ec-a993-fda3929744ac"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
            
            foreach ($response as $key => $value) {
                $tmpdata[$key]['Name'] = htmlentities('<b>'.$value['Name'].'</b><br>');
                $tmpdata[$key]['Owner'] = htmlentities('<b>'.$value['Owner']['Name'].'</b><br>');
                if(!empty($value['FieldData']['Supporting-Image-1']))
                $tmpdata[$key]['s1'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-1'].'">');

                if(!empty($value['FieldData']['Supporting-Image-2']))
                $tmpdata[$key]['s2'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-2'].'"><br>');
                $tmpdata[$key]['abstract'] = htmlentities($value['FieldData']['abstract'].'<br><br>');
            }
            $tmp = "";
            foreach ($tmpdata as $key => $value)
            {
                $tmp .= implode('',$value);
            }
            $update['Description'] = $tmp;
            $this->db->where('Id','1531');
            $this->db->update('cms',$update);
            echo "<br> Omega-3 Product Showcases  : 1531 ".$tmp."<br>";
        }

        //Tasting Centre Awards 1572
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.showoff.asp.com/public/libraries/E58A2CAF-6BC5-4473-BD4-C8873A1A18F1/entries?l=100",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic N0Q0QzREMENERDcwQkREMDMwQTU3RERGNUEwN0M0NDYxRTE0NzcyMDp3LmRzXi5WNlchfm1IRSgu",
            "Cache-Control: no-cache",
            "Postman-Token: 5f6164fc-fe5f-40ec-a993-fda3929744ac"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
            
            foreach ($response as $key => $value) {
                $tmpdata[$key]['Name'] = htmlentities('<b>'.$value['Name'].'</b><br>');
                $tmpdata[$key]['Owner'] = htmlentities('<b>'.$value['Owner']['Name'].'</b><br>');
                if(!empty($value['FieldData']['Supporting-Image-1']))
                $tmpdata[$key]['s1'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-1'].'">');

                if(!empty($value['FieldData']['Supporting-Image-2']))
                $tmpdata[$key]['s2'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-2'].'"><br>');
                $tmpdata[$key]['abstract'] = htmlentities($value['FieldData']['abstract'].'<br><br>');
            }
            $tmp = "";
            foreach ($tmpdata as $key => $value)
            {
                $tmp .= implode('',$value);
            }
            $update['Description'] = $tmp;
            $this->db->where('Id','1572');
            $this->db->update('cms',$update);
            echo "<br> Tasting Centre Awards 1572 ".$tmp."<br>";
        }

        //Finished Products 1518
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.showoff.asp.com/public/libraries/2580B4C6-E6E2-406E-BF1-96461D3FBDD6/entries?l=100",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic N0Q0QzREMENERDcwQkREMDMwQTU3RERGNUEwN0M0NDYxRTE0NzcyMDp3LmRzXi5WNlchfm1IRSgu",
            "Cache-Control: no-cache",
            "Postman-Token: 5f6164fc-fe5f-40ec-a993-fda3929744ac"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
            
            foreach ($response as $key => $value) {
                $tmpdata[$key]['Name'] = htmlentities('<b>'.$value['Name'].'</b><br>');
                $tmpdata[$key]['Owner'] = htmlentities('<b>'.$value['Owner']['Name'].'</b><br>');
                if(!empty($value['FieldData']['Supporting-Image-1']))
                $tmpdata[$key]['s1'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-1'].'">');

                if(!empty($value['FieldData']['Supporting-Image-2']))
                $tmpdata[$key]['s2'] = htmlentities('<img width="100%" src="https://ezone.vitafoodsasia.com/'.$value['FieldData']['Supporting-Image-2'].'"><br>');
                $tmpdata[$key]['abstract'] = htmlentities($value['FieldData']['abstract'].'<br><br>');
            }
            $tmp = "";
            foreach ($tmpdata as $key => $value)
            {
                $tmp .= implode('',$value);
            }
            $update['Description'] = $tmp;
            $this->db->where('Id','1518');
            $this->db->update('cms',$update);
            echo "<br> Finished Products 1518 ".$tmp."<br>";
        }
    }
    public function vitafoodAssignGroup()
    {   
        $where = "`event_id` = '1397' AND `key1` = 'Do you want to be part of our pre-event \'Distributor Connect\' networking group?'";
        $this->db->where($where);
        $this->db->where('group_id IS NULL');
        $res = $this->db->get('attendee_extra_column')->result_array();
        foreach ($res as $key => $value)
        {   
            $this->db->where('Attendee_id',$value['user_id']);
            $this->db->where('Event_id',$value['event_id']);
            if($value['value'] == 'Yes')
            {
                $update['group_id'] = '148';
            }
            elseif($value['value'] == 'No')
            {
                $update['group_id'] = '197';
            }
            $this->db->update('event_attendee',$update);
        }
        j('group updated successfully');
    }
    public function vitafoodAssignKeywords()
    {   
        $where = "`event_id` = '1397' AND `key1` = 'Which of these best describes your company\'s primary business activity? <single>'";
        $this->db->where($where);
        $this->db->where('group_id != 197');
        $res = $this->db->get('attendee_extra_column')->result_array();
        
        foreach ($res as $key => $value)
        {
            $where1['user_id'] = $value['user_id'];
            $where1['event_id'] = $value['event_id'];
            $where1['keyword'] = $value['value'];
            if(!$this->db->where($where1)->get('attendee_keywords')->row())
            {
                $this->db->insert('attendee_keywords',$where1);
            }
        }
        j('data added successfully');
    }

    public function Vitafood_Agenda()
    {    
        //error_reporting(E_ALL);

        /***
        Conference 
            https://api.showoff.asp.com/public/seminars/C25674C1-B773-49D6-904-407314EBC488/sessions?l=500
            1823
        
        Global Health Theatre
            https://api.showoff.asp.com/public/seminars/BE807C35-368A-4841-9EA-F300AB37DA11/sessions?l=500
            1824

        Life Stages Theatre
            https://api.showoff.asp.com/public/seminars/0BF34514-5223-4EEB-9FA-4712FE14F388/sessions?l=500
            1827
        ***/

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.showoff.asp.com/public/seminars/0BF34514-5223-4EEB-9FA-4712FE14F388/sessions?l=500",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic N0Q0QzREMENERDcwQkREMDMwQTU3RERGNUEwN0M0NDYxRTE0NzcyMDp3LmRzXi5WNlchfm1IRSgu",
            "Cache-Control: no-cache",
            "Postman-Token: 5f6164fc-fe5f-40ec-a993-fda3929744ac"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            $response = json_decode($response,true);
            foreach ($response  as $key => $value)
            {   
                $agenda['tmp_id'] = 'vitafood-'.$value['Uuid'];
                $agenda['StartDate'] = empty($value['StartDate'])? '' : date('Y-m-d',strtotime($value['StartDate']));
                $agenda['EndDate'] = empty($value['EndDate'])? '' :date('Y-m-d',strtotime($value['EndDate']));
                $agenda['StartTime'] = empty($value['LocalStartTime'])? '' :$value['LocalStartTime'];
                $agenda['EndTime'] = empty($value['LocalEndTime'])? '' :$value['LocalEndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Streams'][0]['Name'])? 'Sessions' :$value['Streams'][0]['Name'];
                $agenda['Description'] = empty($value['Abstract'])? '' :$value['Abstract'];
                $agenda['Location'] = empty($value['Location']['Name'])? '' :$value['Location']['Name'];
                
                if(!empty($value['Speakers']))
                {
                    $temp_speaker = array_filter(array_column($value['Speakers'],'Name'));
                    $agenda['Speakers'] = empty($value['Speakers'])? '' :implode(',',$temp_speaker);
                }
                else
                {
                    $agenda['Speakers'] = '';
                }
                
                $tmp_agenda = $this->db->where('tmp_id','vitafood-'.$value['Uuid'])->get('Vitafood_Agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('Vitafood_Agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('Vitafood_Agenda',$agenda);
                }
            }
            j('data Insert Successfully');
        }
    }

    public function Vitafood_Agenda_add()
    {   

        /***
        Conference 
            https://api.showoff.asp.com/public/seminars/C25674C1-B773-49D6-904-407314EBC488/sessions?l=500
            1823
        
        Global Health Theatre
            https://api.showoff.asp.com/public/seminars/BE807C35-368A-4841-9EA-F300AB37DA11/sessions?l=500
            1824

        Life Stages Theatre
            https://api.showoff.asp.com/public/seminars/0BF34514-5223-4EEB-9FA-4712FE14F388/sessions?l=500
            1827
        ***/

        $org_id=322861;
        $Agenda_id=1827;
        $eid=1397;
        $temp_session=$this->db->select('*')->from('Vitafood_Agenda')->limit(50)->get()->result_array();

        foreach ($temp_session as $key => $value) 
        {   
            $session_data['Start_date']=$value['StartDate'];
            $session_data['Start_time']=$value['StartTime'];
            $session_data['End_date']=$value['EndDate'];
            $session_data['End_time']=$value['EndTime'];
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['custom_location']=$value['Location'];
            $session_data['custom_speaker_name']=$value['Speakers'];
            $session_data['show_rating'] = '1';
            $session_data['allow_clashing'] = '1';
            $session_data['allow_comments'] = '1';
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Vitafood_Agenda');
        }
        $total_exhi = $this->db->select()->from('Vitafood_Agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }

    public function updateExtraColumn()
    {
        $this->db->where('Event_id','1397');
        $this->db->where('(extra_column IS NOT NULL AND extra_column != "null")');
        $res = $this->db->get('event_attendee')->result_array();
        foreach ($res as $key => $value)
        {
            $extra_column = json_decode($value['extra_column'],true);
            foreach ($extra_column as $key1 => $value1)
            {
                $key2 = strip_tags($key1);
                unset($extra_column[$key1]);
                $extra_column[$key2] = $value1;
            }
            $res[$key]['extra_column'] = json_encode($extra_column);
            unset($extra_column);
            $this->db->where('Id',$value['Id']);
            $this->db->update('event_attendee',$res[$key]);
        }
        j('data updated successfully');
    }
    public function updateCustomColumn()
    {
        $this->db->where('event_id','1397');
        $res = $this->db->get('custom_column')->result_array();
        foreach ($res as $key => $value)
        {   
            foreach ($value as $key1 => $value1)
            {    
                if($key1 == 'column_name')
                {   
                    $res[$key][$key1] = strip_tags($value1);
                }
            }
            $this->db->where('column_id',$value['column_id']);
            $this->db->update('custom_column',$res[$key]);
        }
        j('column updated successfully');
    }

    public function ipexpo_europe_speaker()
    {
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ipexpoeurope.com/feeds/MobileSpeakers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            

            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            //j($xmlArr);
            foreach ($xmlArr['Speaker']  as $key => $value)
            {   
                //j($value);
                $speaker['tmp_id'] = $value['SpeakerID'].'-ipexpo2018';
                $speaker['Email'] =  str_replace(" ","",$value['FirstName'].'-'.$value['LastName'].'-'.$value['SpeakerID'].'@venturiapps.com');
                $speaker['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $speaker['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $speaker['JobTitle'] = empty($value['JobTitle'])? '' :$value['JobTitle'];
                $speaker['Company'] = empty($value['Company'])? '' :$value['Company'];
                $speaker['Biography'] = (is_array($value['Description']))? '' :$value['Description'];
                $speaker['Website'] = empty($value['Website'])? '' :$value['Website'];
                $speaker['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $speaker['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $speaker['Image'] = empty($value['Photo'])? '' :$value['Photo'];

                $temp_speaker = $this->db->where('tmp_id',$value['SpeakerID'].'-ipexpo2018')->get('temp_ipexpo_Speakers')->row_array();
                if(empty($temp_speaker))
                {
                    $this->db->insert('temp_ipexpo_Speakers',$speaker);
                }
                else
                {
                    $this->db->where($temp_speaker);
                    $this->db->update('temp_ipexpo_Speakers',$speaker);
                }
            }
            j('data inserted successfully');
        }
    }
    public function ipexpo_europe_speaker_add()
    {
        $org_id = 48217;
        $eid = 1682;
        $temp_speakers=$this->db->select('*')->from('temp_ipexpo_Speakers')->limit(50)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id'])))->row_array();
            
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Email'] =  $value['Email'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="company_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            $user_data['is_from_api'] = '1';
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);


            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }

            unset($reldata);
            unset($rel_data);

            $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            $social_link_data['Linkedin_url']=$value['Linkedin'];
            $social_link_data['Twitter_url']=$value['Twitter'];
            $social_link_data['Website_url']=$value['Website'];
            if(empty($sociallinkdata))
            {
                $social_link_data['User_id']=$user_id;
                $social_link_data['Event_id']=$eid;
                $this->db->insert('user_social_links',$social_link_data);
            }
            else
            {
                $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
            }
            unset($sociallinkdata);
            unset($social_link_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ipexpo_Speakers');
            //lq();
        }
        $total_exhi = $this->db->select()->from('temp_ipexpo_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        updateModuleDate($eid,'speaker');
        die;
    }

    public function ipexpo_agenda()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ipexpoeurope.com/feeds/MobileSeminars",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            // j($xmlArr);

            foreach ($xmlArr['Seminar'] as $key => $value)
            {   
                // j($value);
                $agenda['tmp_id'] = $value['SeminarID'].'-ipexpo2018';
                $agenda['StartDate'] = empty($value['StartDate'])? '' :$value['StartDate'];
                $agenda['EndDate'] = empty($value['EndDate'])? '' :$value['EndDate'];
                $agenda['StartTime'] = empty($value['StartTime'])? '' :$value['StartTime'];
                $agenda['EndTime'] = empty($value['EndTime'])? '' :$value['EndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Theatre'])? '' :$value['Theatre'];
                if(!is_array($value['Description']))
                $agenda['Description'] = empty($value['Description']) ? '' :$value['Description'];
                
                if(array_key_exists('SpeakerID',$value['Speakers']))
                {   
                    if(is_array($value['Speakers']['SpeakerID']))
                    {
                        $agenda['Speakers'] = empty($value['Speakers']['SpeakerID'])? '' :implode(',',array_filter($value['Speakers']['SpeakerID']));
                    }
                    else
                    {   
                        
                            $agenda['Speakers'] = $value['Speakers']['SpeakerID'];
                    }
                }
                else
                {
                    $agenda['Speakers'] = NULL;
                }

                $tmp_agenda = $this->db->where('tmp_id',$value['SeminarID'].'-ipexpo2018')->get('temp_ipexpo_agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('temp_ipexpo_agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('temp_ipexpo_agenda',$agenda);
                }
                $data[] = $agenda;
                unset($agenda);
            }
            j($data);
            j('data Insert Successfully');
        }
    }
    public function ipexpo_agenda_add()
    {
        $org_id=48217;
        $Agenda_id=1838;
        $eid=1682;
        $temp_session=$this->db->select('*')->from('temp_ipexpo_agenda')->limit(50)->get()->result_array();
        // j($temp_session);
        foreach ($temp_session as $key => $value) 
        {
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->get_where('user',array('knect_api_user_id'=>$svalue.'-ipexpo2018'))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            
            $session_data['Start_date']=DateTime::createFromFormat('d/m/y',$value['StartDate'])->format('Y-m-d');
            $session_data['Start_time']=date('H:i:s',strtotime($value['StartTime']));
            $session_data['End_date']=DateTime::createFromFormat('d/m/y',$value['EndDate'])->format('Y-m-d');
            $session_data['End_time']=date('H:i:s',strtotime($value['EndTime']));
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['allow_clashing'] = '1';
            $session_data['show_checking_in'] = '0';
            $session_data['show_rating'] = '1';
            $session_data['allow_comments'] = '1';

            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            elseif(trim($value['Type']) == '')
            {
                $type_id = ($eid == 936) ? 1780 : 1779;
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            $data[] = $session_data;
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ipexpo_agenda');
        }
        updateModuleDate($eid,'agenda');
        j($data);
        $total_exhi = $this->db->select()->from('temp_ipexpo_agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    
    public function PPMAaddExhi()
    {   
        // header('HTTP/1.1 403', true, 403);exit;
        extract($this->input->post());
        $event_id = '1394';
        $org_id   = '286590';
        
        if($username != 'ppma' || $password != '^D6X/8wNn-`d:?g.' || empty($username)  || empty($password))
        {
            $data = array(
                    'success' => false,
                    'message' => 'API access is not allowed.'
                );
            echo json_encode($data);exit; 
        }


        if(!empty($Email) && !empty($Company_name) && !empty($Firstname) && !empty($Lastname))
        {
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $user['Firstname'] = $Firstname;
            $user['Lastname'] = $Lastname;
            $user['Company_name'] = $Company_name;
            $user['Email'] = $Email;
            $user['Password'] = $Password ? md5($Password) : md5('123456');
            $user['Organisor_id'] = $org_id;
            $user['Created_date'] = date('Y-m-d H:i:s');
            $user['Active'] = '1';
            $user['updated_date'] = date('Y-m-d H:i:s');
            $user['is_from_api'] = '1';
            $user['Country'] = $this->getCountryIdFromName($Country);
            $user_id = $this->addExhiUser($user,$event_id);

            $ex_data['Event_id'] = $event_id;
            $ex_data['Heading'] = $Company_name;
            $ex_data['Organisor_id'] = $org_id;
            if(!empty($Type))
            {
                $ex_data['et_id'] = $this->getExhiTypeId($event_id,$Type);
            }
            $ex_data['stand_number'] = $stand_number;
            $ex_data['Short_desc'] = $Short_desc;
            $ex_data['Description'] = htmlentities($Description);
            $ex_data['website_url'] = $website_url;
            $ex_data['facebook_url'] = $facebook_url;
            $ex_data['twitter_url'] = $twitter_url;
            $ex_data['linkedin_url'] = $linkedin_url;
            $ex_data['instagram_url'] = $instagram_url;
            $ex_data['youtube_url'] = $youtube_url;
            $ex_data['country_id'] = $this->getCountryIdFromName($Country);
            $this->load->library('upload');
            if (!empty($_FILES['company_logo']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['company_logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("company_logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['company_logo'] = json_encode(array($images_file));
            }

            if (!empty($_FILES['banner_image']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['banner_image']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("banner_image"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['Images'] = json_encode(array($images_file));
            }
            
            $ex_data['user_id'] = $user_id;
            $this->add_exhibitor($ex_data);
            updateModuleDate($event_id,'exhibitor');
            $data = array(
                    'success' => true,
                    'message' => 'Exhibitor Updated Successfully'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                    );
        }
        echo json_encode($data);exit;
    }
    public function addExhiUser($user_data, $eid)
    {
        $this->db->select('*')->from('user');
        $this->db->where('Email', $user_data['Email']);
        $res = $this->db->get()->result_array();
        if (count($res) > 0)
        {
            $this->db->where('Id',$res[0]['Id']);
            $this->db->update('user',$user_data);
            $this->db->select('*')->from('relation_event_user');
            $this->db->where('Event_id', $eid);
            $this->db->where('User_id', $res[0]['Id']);
            $reldata = $this->db->get()->result_array();
            if (count($reldata) > 0)
            {
                $this->db->where('Event_id', $eid);
                $this->db->where('User_id', $res[0]['Id']);
                $this->db->update('relation_event_user', array(
                    'Role_id' => '6'
                ));
            }
            else
            {
                $reldata['User_id'] = $res[0]['Id'];
                $reldata['Event_id'] = $eid;
                $reldata['Organisor_id'] = $user_data['Organisor_id'];
                $reldata['Role_id'] = '6';
                $this->db->insert('relation_event_user', $reldata);
            }
            return $res[0]['Id'];
        }
        else
        {
            $this->db->insert('user', $user_data);
            $user_id = $this->db->insert_id();
            $reldata['User_id'] = $user_id;
            $reldata['Event_id'] = $eid;
            $reldata['Organisor_id'] = $user_data['Organisor_id'];
            $reldata['Role_id'] = '6';
            $this->db->insert('relation_event_user', $reldata);
            return $user_id;
        }
    }

    public function getCountryIdFromName($name)
    {
        $country = $this->db->select('id')->from('country')->where('country_name', $name)->get()->row_array();
        if (empty($country) && !empty($name))
        {
            $insert['country_name'] = $name;
            $this->db->insert('country', $insert);
            return $this->db->insert_id();
        }
        else
        {
            return $country['id'];
        }
    }

    public function getExhiTypeId($event_id,$type)
    {
        $where['event_id'] = $event_id;
        $where['type_name'] = $type;
        $res = $this->db->where($where)->get('exhibitor_type')->row_array();
        if($res)
        {
            return $res['type_id'];
        }
        else
        {   
            $where['type_ucode'] = substr(sha1(uniqid()) , 0, 5);
            $this->db->insert('exhibitor_type',$where);
            return $this->db->insert_id();
        }
    }

    public function add_exhibitor($exdata)
    {
        $this->db->select('*')->from('exibitor');
        $this->db->where('Event_id', $exdata['Event_id']);
        $this->db->where('user_id', $exdata['user_id']);
        $ex_res = $this->db->get()->result_array();
        if (count($ex_res) > 0)
        {
            $user_id = $exdata['user_id'];
            $eid = $exdata['Event_id'];
            unset($exdata['Event_id']);
            unset($exdata['user_id']);
            $this->db->where('Event_id', $eid);
            $this->db->where('user_id', $user_id);
            $this->db->update('exibitor', $exdata);
            return $ex_res[0]['Id'];
        }
        else
        {
            $this->db->insert('exibitor', $exdata);
            return $this->db->insert_id();
        }
    }


    public function Euromedicom_Exhi_MCA()
    {    
        error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/exhibitors.php?id=260",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            $response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);
            $response = str_replace('&bull;','<![CDATA[&acute;]]>',$response);
            $response = str_replace('&mdash;','<![CDATA[&acute;]]>',$response);
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            
            //j($xmlArr);
            foreach ($xmlArr['Informa']['MCA_2018']['Exhibitors']['Exhibitor']  as $key => $value)
            {   
                $Exhibitor['tmp_id'] = 'euromedicom-mca-'.$value['ID'];
                $Exhibitor['Email'] = empty($value['Email'])? 'euromedicom-mca-'.$value['ID'].'@venturiapps.com' :$value['Email'];
                $Exhibitor['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $Exhibitor['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $Exhibitor['StandNumber'] = empty($value['StandNumber'])? '' :$value['StandNumber'];
                $Exhibitor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $Exhibitor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $Exhibitor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $Exhibitor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $Exhibitor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $Exhibitor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $Exhibitor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $Exhibitor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $Tmp_Exhibitor = $this->db->where('tmp_id','euromedicom-mca-'.$value['ID'])->get('Euromedicom_Exhibitors')->row_array();
                $Exhibitor['CompanyDescription'] =$Exhibitor['CompanyDescription'];
                $data[] = $Exhibitor;
                if(empty($Tmp_Exhibitor))
                {
                    $this->db->insert('Euromedicom_Exhibitors',$Exhibitor);
                }
                else
                {   
                    $this->db->where($Tmp_Exhibitor);
                    $this->db->update('Euromedicom_Exhibitors',$Exhibitor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Exhi_add_MCA($value='')
    {   
        //error_reporting(E_ALL);
        // die("Direct Access Denied..");
        $oid = '32052';
        $eid = '1045';
        $temp_exibitor = $this->db->select('*')->from('Euromedicom_Exhibitors')->limit('50')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['LastName'];
                $user_data['Company_name'] = $value['Company'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Company']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Company'];
                $ebi_data['Description'] = $value['CompanyDescription'];
                $ebi_data['stand_number'] = $value['StandNumber'];

                if (!empty($value['Image']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['Image'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['Website'];
                $ebi_data['facebook_url'] = $value['Facebook'];
                $ebi_data['linkedin_url'] = $value['Linkedin'];
                $ebi_data['instagram_url'] = $value['Instagram'];
                $ebi_data['twitter_url'] = $value['Twitter'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Company']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('Euromedicom_Exhibitors');
        }
        echo "Success";
        die;
    }

    public function Euromedicom_Sponsors_MCA()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/sponsors.php?id=260",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $response = html_entity_decode($response);
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            // j($xml);
            foreach ($xml['Informa']['MCA_2018']['Sponsors']['Sponsor']  as $key => $value)
            {   
                //j($value);
                $sponsor['tmp_id'] = 'euromedicom-mca-'.$value['ID'];
                $sponsor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $sponsor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $sponsor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $sponsor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $sponsor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $sponsor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $sponsor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $sponsor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $tmp_sponsor = $this->db->where('tmp_id','euromedicom-mca-'.$value['ID'])->get('Euromedicom_Sponsors')->row_array();
                if(empty($tmp_sponsor))
                {
                    $this->db->insert('Euromedicom_Sponsors',$sponsor);
                }
                else
                {   
                    $this->db->where($tmp_sponsor);
                    $this->db->update('Euromedicom_Sponsors',$sponsor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Sponsors_add_MCA()
    {
        $event_id = 1045;
        $org_id   = 32052;
        $tmp_sponsor=$this->db->select('*')->from('Euromedicom_Sponsors')->limit(50)->get()->result_array();
        foreach ($tmp_sponsor as $key => $value)
        {
            $check_sponsor = $this->db->where('api_id',$value['tmp_id'])->where('Event_id',$event_id)->get('sponsors')->row_array();
            $data['api_id'] = $value['tmp_id'];
            $data['Company_name'] = $value['Company'];
            $data['Description']  = $value['CompanyDescription'];
            $data['website_url']  = $value['Website'];
            $data['facebook_url'] = $value['Facebook'];
            $data['twitter_url  '] = $value['Twitter'];
            $data['linkedin_url'] = $value['Linkedin'];
            $data['instagram_url'] = $value['Instagram'];
            $data['Event_id'] = $event_id;
            $data['Organisor_id'] = $org_id;

            if (!empty($value['Image']))
            {
                $destination = "company_logo_" . uniqid();
                $source = $value['Image'];
                $info = getimagesize($source);
                if ($info['mime'] == 'image/jpeg')
                {
                    $logo = $destination . '.jpeg';
                    $image = imagecreatefromjpeg($source);
                    imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                }
                elseif ($info['mime'] == 'image/gif')
                {
                    $logo = $destination . '.gif';
                    $image = imagecreatefromgif($source);
                    imagegif($image, "./assets/user_files/" . $destination . '.gif');
                }
                elseif ($info['mime'] == 'image/png')
                {
                    $logo = $destination . '.png';
                    $image = imagecreatefrompng($source);
                    $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                    imagecolortransparent($image, $background);
                    imagealphablending($image, false);
                    imagesavealpha($image, true);
                    imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                }
                $data['company_logo'] = json_encode(array(
                    $logo
                ));
            }

            if(empty($check_sponsor))
            {
                $this->db->insert('sponsors',$data);
            }
            else
            {
                $this->db->where($check_sponsor);
                $this->db->update('sponsors',$data);
            }
            unset($data);
             $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Sponsors');
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Sponsors')->get()->num_rows();
        echo "Success -> Remaning Sponsor. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('Sponsor added  successfully');
    }
    public function add_attendees_digmort()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $dig_tmp_attendee = $this->db->limit('100')->get('digmort_tmp_attendee')->result_array();
        $event_id = 1804;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($dig_tmp_attendee as $key => $value)
        {   
            if(empty($value['Email']))
            {   
                $value['Email'] = $value['Unique_no'].'-digmort@venturiapps.com';
            }

            if(!empty($value['Email']))
            {
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                unset($data['a1']);
                unset($data['a2']);
                $data['Street'] = $value['a1'].','.$value['a2'];
                $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                $data['Country'] = ($country_id['id'])?:$value['Country'];
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where('Unique_no',$value['Unique_no']);
                $this->db->delete('digmort_tmp_attendee');
            }
        }
        $dig_tmp_attendee = $this->db->get('digmort_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $dig_tmp_attendee . " Refresh Page -> " . round($dig_tmp_attendee / 100 ) . " Time";
        j($msg);
    }

     public function euromedicom_agenda_update()
    {   
        $this->db->where('Event_id','1045');
        $res = $this->db->get('agenda')->result_array();
        if($_GET['t'] == '1')
        j($res);

        foreach ($res as $key => $value)
        {   
            $heading = html_entity_decode($value['Heading'],ENT_QUOTES, "UTF-8");
            $str = "UPDATE agenda SET Heading = ".$this->db->escape($heading)." WHERE Id = '".$value['Id']."'";
            $this->db->query($str);
            echo $this->db->last_query().";";
        }
        updateModuleDate('1045','agenda');
        j('done');
    }
    public function NCrealotrsAttendee()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.ramcoams.com/api/v2/",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\nAllInTheLoop-NorthCarolina-Prod-8ccd0c21271dfc38d42abaa9e3081fa4110e9324\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"Operation\"\r\n\r\nGetEntities\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"Entity\"\r\n\r\ncontact\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"Attributes\"\r\n\r\nContactId,FirstName,LastName,emailaddress1,cobalt_password\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"filter\"\r\n\r\nramco_primaryassociationid<eq>#1e1b1809-ec23-4e8f-b439-07c5ac397444#\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Postman-Token: 4968bdf6-399a-4457-ada6-53abfcfb7cb8",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {
            $response = json_decode($response,true)['Data'];
            foreach ($response as $key => $value)
            {
                $insert[$key]['Firstname'] = $value['FirstName'];
                $insert[$key]['LastName'] = $value['LastName'];
                $insert[$key]['Email'] = $value['EMailAddress1'] ?: $value['ContactId'].'@venturiapps.com';
                $insert[$key]['Password'] = ($value['cobalt_Password'] == 'password' || empty($value['cobalt_Password'])) ? '123456' : $value['cobalt_Password'];
            }
            $this->db->insert_batch('NCRealtors_attendee', $insert); 
            j($insert);
        }
    }
    public function NCrealotrsAttendee_add()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $NCRealtors_attendee = $this->db->limit('100')->get('NCRealtors_attendee')->result_array();
        $event_id = 1362;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($NCRealtors_attendee as $key => $value)
        {   
            if(!empty($value['Email']))
            {
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                $data['Password'] = md5($value['Password']);
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where('Email',$value['Email']);
                $this->db->delete('NCRealtors_attendee');
            }
        }
        $NCRealtors_attendee = $this->db->get('NCRealtors_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $NCRealtors_attendee . " Refresh Page -> " . round($NCRealtors_attendee / 100 ) . " Time";
        j($msg);
    }
    public function add_attendees_BenefitsForum()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $dig_tmp_attendee = $this->db->limit('100')->get('BenefitsForum_tmp_attendee')->result_array();
        $event_id = 1850;
        ##->where('Unique_no','33785979')
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($dig_tmp_attendee as $key => $value)
        {   
            if(empty($value['Email']))
            {   
                $value['Email'] = $value['Unique_no'].'-BenefitsForum@venturiapps.com';
            }

            if(!empty($value['Email']))
            {   
                $user = $this->db->where('Organisor_id',$org_id)->where('Unique_no',$value['Unique_no'])->get('user')->row_array();
                if($user)
                {   
                    $role = array('4','6','7','3');
                    $this->db->where('Event_id','1850');
                    $this->db->where('User_id',$user['Id']);
                    $this->db->where_not_in('Role_id',$role);
                    $rel = $this->db->get('relation_event_user')->row_array();
                    if(!empty($rel))
                    {
                        $value['Email'] = $value['Email'].'#'.$value['Unique_no'];
                        unset($user);
                    }
                }
                $Email = $value['Email'];
                $data = $value;
                unset($data['a1']);
                unset($data['a2']);
                
                $tmp_ad[] = $value['a1'];
                $tmp_ad[] = $value['a2'];
                $data['Street'] = implode(',',array_filter($tmp_ad));
                unset($tmp_ad);

                $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                $data['Country'] = ($country_id['id'])?:$value['Country'];
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Id',$user['Id']);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkUniqueNoAlreadyByEvent($value['Unique_no'],$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {   
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where('Unique_no',$value['Unique_no']);
                $this->db->delete('BenefitsForum_tmp_attendee');
            }
        }
        $dig_tmp_attendee = $this->db->get('BenefitsForum_tmp_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $dig_tmp_attendee . " Refresh Page -> " . round($dig_tmp_attendee / 100 ) . " Time";
        j($msg);
    }
    public function LT2018_add($id=1)
    {   
        $event_id = 1756;
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($id) {
            case '1':
                $LT2018 = $this->db->limit('100')->get('LT2018',100)->result_array();
                break;

            case '2':
                $LT2018 = $this->db->limit('100')->get('LT2018',100,150)->result_array();
                break;

            case '3':
                $LT2018 = $this->db->limit('100')->get('LT2018',100,250)->result_array();
                break;

            case '4':
                $LT2018 = $this->db->limit('100')->get('LT2018',100,350)->result_array();
                break;

            case '5':
                $LT2018 = $this->db->limit('100')->get('LT2018',100,450)->result_array();
                break;
        }
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);

        foreach ($LT2018 as $key => $value)
        {   
            if(!empty($value['Email']))
            {
                $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                /*if(!empty($value['Country']))
                {
                    $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
                    if(empty($country_id))
                    {   
                        $insert['country_name'] = $value['Country'];
                        $this->db->insert('country',$insert);
                        $country_id['id'] = $this->db->insert_id();
                    }
                    $data['Country'] = ($country_id['id'])?:$value['Country'];
                }*/
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                $data['Password'] = md5($value['Password']);
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                
                $this->db->where('Email',$value['Email']);
                $this->db->delete('LT2018');
            }
        }
        $LT2018 = $this->db->get('LT2018')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $LT2018 . " Refresh Page -> " . round($LT2018 / 100 ) . " Time";
        j($msg);
    }

    public function attendee()
    {    
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
        {
            "Company_name": "<font color=green><b>(String)</b></font> Company Name",
            "Firstname": "<font color=green><b>(String)</b></font> Firstname of Attendee",
            "Lastname": "<font color=green><b>(String)</b></font> Lastname of Attendee",
            "Email": "<font color=green><b>(String)</b></font> Email of Attendee <font color=red><b>**Required**</b></font>",
            "Password": "<font color=green><b>(String)</b></font> Password",
            "Salutation": "<font color=green><b>(String)</b></font> Salutation",
            "Title": "<font color=green><b>(String)</b></font> Title"
        },
        {
            "Company_name": "ABC Pvt. Ltd.",
            "Firstname": "John",
            "Lastname": "Doe",
            "Email": "john@doe.com",
            "Password": "123456",
            "Salutation": "Mr.",
            "Title": "CEO"
        }
        ]';exit();
    }


    public function exhibitor()
    {    
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo 
        '[{
            "Firstname": "<font color=green><b>(String)</b></font> Firstname of Exhibitor",
            "Lastname": "<font color=green><b>(String)</b></font> Lastname of Exhibitor",
            "Email": "<font color=green><b>(String)</b></font> Email of Exhibitor <font color=red><b>**Required**</b></font>",
            "Password": "<font color=green><b>(String)</b></font> Password",
            "Exhibitor_Name": "<font color=green><b>(String)</b></font> Company Name",
            "Exhibitor_Type": "<font color=green><b>(String)</b></font> Type Code",
            "Stand_Number": "<font color=green><b>(String)</b></font> Stand Number",
            "Keywords": "<font color=green><b>(String)</b></font> Exhibitor Short Description <b>(coma separated)</b>",
            "Description": "<font color=green><b>(String)</b></font> Company Description of Exhibitors",
            "Website_Url": "<font color=green><b>(String)</b></font> Company Website URL",
            "Facebook_Url": "<font color=green><b>(String)</b></font> Company Facebook URL",
            "Twitter_Url": "<font color=green><b>(String)</b></font> Company Twitter URL",
            "Linkedin_Url": "<font color=green><b>(String)</b></font> Company Linkedin URL",
            "Instagram_Url": "<font color=green><b>(String)</b></font> Company Instagram URL",
            "Youtube_Url": "<font color=green><b>(String)</b></font> Company Youtube URL",
            "Company_Logo": "<font color=green><b>(String)</b></font> URL of Company Logo",
            "Images": "<font color=green><b>(Json String Array)</b></font> URL of Company Images",
            "Country_name": "<font color=green><b>(String)</b></font> Country Name",
            "Heading": "<font color=green><b>(String)</b></font> Heading"
        },
        {
            "Firstname": "John",
            "Lastname": "doe",
            "Email": "Jhon@doe.com",
            "Password": "123456",
            "Exhibitor_Name": "ABC PVT. LTD.",
            "Exhibitor_Type": "GOLD",
            "Stand_Number": "A-34",
            "Keywords": "food,meet,veg",
            "Description": "ABC PVT. LTD. was establised in 1990. it have more then 1000 clients all over world",
            "Website_Url": "https:\/\/www.abc.com",
            "Facebook_Url": "https:\/\/www.facebook.com\/abc",
            "Twitter_Url": "https:\/\/www.twitter.com\/abc",
            "Linkedin_Url": "https:\/\/www.linkedin.com\/abc",
            "Instagram_Url": "https:\/\/www.instagram.com\/abc",
            "Youtube_Url": "https:\/\/www.youtube.com\/abc",
            "Company_Logo": "https:\/\/www.abc.com\/logo.png",
            "Images": "",
            "Country_name": "Argentina",
            "Heading": "ABC PVT. LTD Argentina"
        }]';exit;
    }


    public function agenda()
    {    
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
        {
            "Start_date": "<font color=green><b>(String)</b></font> Start Date if Agenda <font color=red><b>**Required**</b></font>",
            "Start_time": "<font color=green><b>(String)</b></font> Start time of Agenda <font color=red><b>**Required**</b></font>",
            "End_date": "<font color=green><b>(String)</b></font> End date of Agenda <font color=red><b>**Required**</b></font>",
            "End_time": "<font color=green><b>(String)</b></font> End time of Agenda <font color=red><b>**Required**</b></font>",
            "Heading": "<font color=green><b>(String)</b></font> Heading <font color=red><b>**Required**</b></font>", 
            "Types": "<font color=green><b>(String)</b></font> Types <font color=red><b>**Required**</b></font>",
            "description": "<font color=green><b>(String)</b></font> description",
            "session_image": "<font color=green><b>(String)</b></font> session_image"
            "custom_location": "<font color=green><b>(String)</b></font> Custom Location",
            "custom_speaker_name": "<font color=green><b>(String)</b></font> Custom Speaker Name",
            "session_code": "<font color=green><b>(String)</b></font> Session Code <font color=red><b>**Required**</b></font>",
            "agenda_name": "<font color=green><b>(String)</b></font> Name of Agenda Category <font color=red><b>**Required**</b></font>"
        },
        {
            "Start_date": "2018-05-28",
            "Start_time": "12:10:00",
            "End_date": "2018-05-28",
            "End_time": "12:30:00",
            "Heading": "Welcome session",
            "Types": "Type",
            "description": "Description",
            "session_image": "Image URL",
            "custom_location": "Hall 1",
            "custom_speaker_name": "John Doe",
            "session_code": "sccdr1",
            "agenda_name": "Agenda 1"
        }
        ]';
        exit();
    }


    public function speaker()
    {    
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
        {
            "Salutation": "<font color=green><b>(String)</b></font> Salutation",
            "Firstname": "<font color=green><b>(String)</b></font> Firstname <font color=red><b>**Required**</b></font>",
            "Lastname": "<font color=green><b>(String)</b></font> Lastname <font color=red><b>**Required**</b></font>",
            "Email": "<font color=green><b>(String)</b></font> Email <font color=red><b>**Required**</b></font>",
            "Password": "<font color=green><b>(String)</b></font> Password",
            "Title": "<font color=green><b>(String)</b></font> Title",
            "Company_name": "<font color=green><b>(String)</b></font> Company_name",
            "Logo": "<font color=green><b>(String)</b></font> Logo Image Link",
            "Speaker_desc": "<font color=green><b>(String)</b></font> Description",
            "Website_url": "<font color=green><b>(String)</b></font> https://www.website.com",
            "Facebook_url": "<font color=green><b>(String)</b></font> https://www.facebook.com",
            "Twitter_url": "<font color=green><b>(String)</b></font> https://www.twitter.com",
            "Linkedin_url": "<font color=green><b>(String)</b></font> https://www.linkedin.com"
        },
        {
            "Salutation": "Mr.",
            "Firstname": "John",
            "Lastname": "Doe",
            "Email": "john@doe.com",
            "Password": "123456",
            "Title": "Developer",
            "Company_name": "test pvt ltd",
            "Logo": "Logo Image Link",
            "Speaker_desc": "Description",
            "Website_url": "https://www.website.com",
            "Facebook_url": "https://www.facebook.com",
            "Twitter_url": "https://www.twitter.com",
            "Linkedin_url": "https://www.linkedin.com"
        }
        ]';
        exit();
    }

}
