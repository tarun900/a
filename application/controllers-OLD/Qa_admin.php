<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Qa_admin extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Q&A';
        $this->data['smalltitle'] = 'Create Q&A Sessions';
        $this->data['breadcrumb'] = 'Q&A';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);

         $eventid=$this->uri->segment(3);

         $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }
        
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('50', $module))
        {
            redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Q&A")
            {
                $title = "Q&A";
            }
            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('50', $menu_list))
        {
            $this->load->model('Qa_model');
            $this->load->model('Agenda_model');
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('Forbidden');
        }
    }
    public function index($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $session = $this->Qa_model->get_qa_session_list_by_event($id, null);
        $this->data['session'] = $session;
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 50);
        $this->data['module_group_list'] = $module_group_list;
        $menudata = $this->Event_model->geteventmenu($id, 50);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['show_on_screen'] = $this->Qa_model->getQaSettingShowOnScreen($id);

        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'qa_admin/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'qa_admin/js', $this->data, true);
        $this->template->render();
    }
    public function add($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $speaker_moderator = $this->Qa_model->get_all_speaker_or_moderator_by_event_id($id);
        $this->data['speaker_moderator'] = $speaker_moderator;
        if ($this->input->post())
        {
            $data['session_array']['Event_id'] = $Event_id;
            $data['session_array']['Session_name'] = $this->input->post('Session_name');
            $data['session_array']['Session_description'] = $this->input->post('Session_description');
            $data['session_array']['Moderator_speaker_id'] = $this->input->post('Moderator_speaker_id');
            $data['session_array']['start_time'] = date('H:i:s', strtotime($this->input->post('start_time')));
            $data['session_array']['end_time'] = date('H:i:s', strtotime($this->input->post('end_time')));
            $data['session_array']['session_date'] = date('Y-m-d', strtotime($this->input->post('session_date')));
			$data['session_array']['is_closed_qa'] = ($this->input->post('is_closed_qa'))?'1':'0';
            $social_id = $this->Qa_model->add_session($data);
            
            $this->session->set_flashdata('session_data', 'Added');
            redirect("Qa_admin/index/" . $id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'qa_admin/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'qa_admin/js', $this->data, true);
        $this->template->render();
    }
    public function edit($id, $sid)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $speaker_moderator = $this->Qa_model->get_all_speaker_or_moderator_by_event_id($id);
        $this->data['speaker_moderator'] = $speaker_moderator;
        $session = $this->Qa_model->get_qa_session_list_by_event($id, $sid);
        $this->data['session'] = $session;
        if ($id == NULL || $id == '')
        {
            redirect('Qa_admin');
        }
        if ($this->input->post())
        {	
            $data['session_array']['Id'] = $sid;
            $data['session_array']['Event_id'] = $id;
            $data['session_array']['Session_name'] = $this->input->post('Session_name');
            $data['session_array']['Session_description'] = $this->input->post('Session_description');
            $data['session_array']['Moderator_speaker_id'] = $this->input->post('Moderator_speaker_id');
            $data['session_array']['start_time'] = date('H:i:s', strtotime($this->input->post('start_time')));
            $data['session_array']['end_time'] = date('H:i:s', strtotime($this->input->post('end_time')));
            $data['session_array']['session_date'] = date('Y-m-d', strtotime($this->input->post('session_date')));
			$data['session_array']['is_closed_qa'] = ($this->input->post('is_closed_qa'))?'1':'0';
            $this->Qa_model->update_session($data);
            $this->session->set_flashdata('session_data', 'Updated');
            redirect("Qa_admin/index/" . $id);
        }
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'qa_admin/edit', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'qa_admin/js', $this->data, true);
        $this->template->render();
    }
    public function delete($Event_id, $sid)
    {
        $session = $this->Qa_model->delete_session($sid,$Event_id);
        $this->session->set_flashdata('session_data', 'Deleted');
        redirect("Qa_admin/index/" . $Event_id);
    }
    public function add_group($id)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $session = $this->Qa_model->get_qa_session_list_by_event($id, null);
        $this->data['session'] = $session;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 50;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, NULL);
            redirect(base_url() . 'Qa_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'qa_admin/add_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_group($id, $mgid)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_group_data($mgid,$id,50);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $session = $this->Qa_model->get_qa_session_list_by_event($id, null);
        $this->data['session'] = $session;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 50;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, $mgid);
            redirect(base_url() . 'Qa_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'qa_admin/edit_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_group($event_id, $group_id)
    {
        $this->Event_template_model->delete_group($group_id,$event_id);
        $this->session->set_flashdata('session_data', 'Group Deleted');
        redirect(base_url() . 'Qa_admin/index/' . $event_id);
    }
    public function save_setting($event_id)
    {   
        $data['show_on_screen_qa'] = $this->input->post('show_on_screen_qa')?'1':'0';
        $this->Qa_model->save_setting($data,$event_id);
        $this->session->set_flashdata('session_data', 'Setting Saved');
        redirect(base_url() . 'Qa_admin/index/' . $event_id.'#session_list');
    }
    public function exportQA($event_id)
    {
        $session = $this->Qa_model->get_qa_session_list_by_event($event_id, null);
        $filename = "QA_Questions.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Q&A Session Name";
        $header[] = "Sender First Name";
        $header[] = "Sender Last Name";
        $header[] = "Sender Title";
        $header[] = "Sender Company";
        $header[] = "Sender Email Address";
        $header[] = "Question Asked";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        foreach ($session as $key => $value) {
            $question = $this->Qa_model->get_all_qa_session_question_by_qa_session_id($event_id,$value['Id']);
            foreach ($question as $key1 => $value1)
            {   
                $sdata['Q&A Session Name'] = $value['Session_name'];
                $sdata['Sender First Name'] = !$value1['Sender_id'] ? 'Anonymous' : ucfirst($value1['Firstname']);
                $sdata['Sender Last Name'] = !$value1['Sender_id'] ? 'Anonymous' :  ucfirst($value1['Lastname']);
                $sdata['Sender Title'] = $value1['Title'];
                $sdata['Sender Company'] = $value1['Company_name'];
                $sdata['Sender Email Address'] = $value1['Email'];
                $sdata['Question Asked'] = $value1['Message'];
                fputcsv($fp, $sdata);
            }
        }
    }
}
