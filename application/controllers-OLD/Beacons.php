<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Beacons extends FrontendController
{

     function __construct()
     {    

          $this->data['pagetitle'] = 'Beacons';
          $this->data['smalltitle'] = 'Set up and manage your Beacons';
          $this->data['breadcrumb'] = 'Beacons';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);

          ini_set('auto_detect_line_endings', true);
          $this->load->model('Event_model');
          $this->load->model('Beacons_model');
          $this->load->model('Agenda_model');
          $this->load->model('Notification_model');
          $this->load->model('Cms_model');
          $eventid=$this->uri->segment(3);
          $user = $this->session->userdata('current_user');
          if($user[0]->Role_id == '4')
               redirect('Forbidden');
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $eventmodule=$this->Event_model->geteventmodulues($eventid);

          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($id);
          $this->data['cms_list'] = $cms_list;

          $module=json_decode($eventmodule[0]['module_list']);

          if(!in_array('51',$module))
          {
               redirect('Forbidden');
            //echo '<script>window.location.href="'.base_url().'forbidden/'.'"</script>'; 
          }
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          

          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               $cnt = $this->Agenda_model->check_auth($req_mod, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          /*if ($cnt == 1 && in_array('51',$menu_list))
          {*/
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Map_model');
               $this->load->model('Speaker_model');
               $this->load->model('Event_template_model');
               $this->load->model('Profile_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          /*}
          else
          {
                    redirect('forbidden');
               //echo '<script>window.location.href="'.base_url().'forbidden/'.'"</script>'; 
          }*/
     }

     public function index($id)
     {
     	    $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $time_format = $this->Event_model->getTimeFormat($id);
          $this->data['time_format'] = $time_format;

          $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
          $this->data['category_list'] = $category_list;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $rolename = $user[0]->Role_name;

          $menudata = $this->Event_model->geteventmenu($id, 1);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $rolename = $user[0]->Role_name;

          //$menudata = $this->Event_model->geteventmenu($id, 1);
          //$menu_toal_data = $this->Event_model->get_total_menu($id);

          $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
          $this->data['beacon_data']=$beacon_data;

          $get_triggers = $this->Beacons_model->get_triggers($id);
          $this->data['get_triggers'] = $get_triggers;

          $heatmap = $this->Beacons_model->get_heatmaps($id);
          $this->data['heatmap'] = $heatmap;



          $number_of_hits = $this->Beacons_model->show_heat_movements($id);
          $this->data['number_of_hits'] = $number_of_hits;


     	    $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/js', true);
          $this->template->render();
     }

     public function add_trigger($id)
     {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
        $this->data['beacon_data']=$beacon_data;

        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;

        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;

        if(!empty($user))
        {
	          if($this->input->post())
	          {
		            $sdom=$event[0]['Subdomain'];
		            $data['modules']=$this->input->post('modules');
		            $data['trigger_name']=$this->input->post('trigger_name');

		            $data['beacon_id']=implode(",",array_filter($this->input->post('beacon_id')));

		            $data['message']=$this->input->post('message');
		            $data['event_id'] = $id;

		            $add_id = $this->Beacons_model->add_trigger($data);

		            $this->session->set_flashdata('trigger_data', 'Added');
		            redirect("Beacons/index/".$id);
	          }

     	    $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/add_trigger', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/js', true);
          $this->template->render();
        }
     }
     public function edit_trigger($id,$tid)
     {

        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
        $this->data['beacon_data']=$beacon_data;

        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;

        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;

        $trigger_data = $this->Beacons_model->get_trigger($tid,$id);
        $this->data['trigger_data'] = $trigger_data;

        if(!empty($user))
        {
	          if($this->input->post())
	          {
		            $sdom=$event[0]['Subdomain'];
		            $data['modules']=$this->input->post('modules');
		            $data['trigger_name']=$this->input->post('trigger_name');

		            $data['beacon_id']=implode(",",array_filter($this->input->post('beacon_id')));

		            $data['message']=$this->input->post('message');
		            $data['event_id'] = $id;

		            $this->Beacons_model->update_trigger($id,$tid,$data);
		            $this->session->set_flashdata('Trigger', 'Updated');
		            redirect("Beacons/index/".$id);
          	}

     	    $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/edit_trigger', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/js', true);
          $this->template->render();
        }
     }

     public function edit_beacon_name($id)
     {  
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;

        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;

        $beacon_id = $this->input->post('id');
        $data['beacon_name'] = $this->input->post('beacon_name');
        $heatmap = $this->Beacons_model->update_beacon($beacon_id,$data);
        $this->session->set_flashdata('beacons_data', 'Updated');

        redirect("Beacons/index/".$id);

     	  $this->template->write_view('css', 'beacons/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'beacons/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'beacons/js', true);
        $this->template->render();
     }

     public function add_heat_map($id)
     {  
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
          $this->data['beacon_data']=$beacon_data;

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
     	
     	    $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/add_heat_map', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/js', true);
          $this->template->render();
     }
     public function edit_heat_map($id,$mid)
     {  
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $heatmap_data = $this->Beacons_model->get_heatmap($id,$mid);
          $this->data['heatmap_data'] = $heatmap_data;

          $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
          $this->data['beacon_data']=$beacon_data;

          $heatmap_mapping_data=$this->Beacons_model->get_heatmap_mapping_data($id,$mid);
          $this->data['heatmap_mapping_data']=$heatmap_mapping_data;
            
          $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/edit_heat_map', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/add_js', $this->data, true); 
          //$this->template->write_view('js', 'beacons/js', true);
          $this->template->render();
     }

     public function delete_heatmap($Event_id, $id)
     {    
          $heatmap_data = $this->Beacons_model->get_heatmap($id);
          $path = "./assets/user_files/beacons/".$heatmap_data[0]['image'];
          unlink($path);  
          $heatmap = $this->Beacons_model->delete_heatmap($id);
          $this->session->set_flashdata('heatmap', 'Deleted');
          redirect("Beacons/index/".$Event_id.'#heat_maps');
     }

     public function delete_trigger($Event_id, $id)
     {
     	 $heatmap = $this->Beacons_model->delete_trigger($id,$cid);
          $this->session->set_flashdata('beacons_data', 'Deleted');
          redirect("Beacons/index/".$Event_id.'/'.$cid);
     }

     public function delete_beacon($Event_id, $id)
     {
     	    $heatmap = $this->Beacons_model->delete_beacon($id,$cid);
          $this->session->set_flashdata('beacons_data', 'Deleted');
          redirect("Beacons/index/".$Event_id.'/'.$cid);
     }

     public function upload_heatmap($id)
     {  

        $imgname = explode('.', $_FILES['heatmap']['name']);
        $tempname = str_replace(" ","_",$imgname);
        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
        $images_file = $tempname_imagename . "." . $tempname[1];
        $this->upload->initialize(array(
                "file_name" => $images_file,
                "upload_path" => "./assets/user_files/beacons",
                "allowed_types" => 'gif|jpg|png|jpeg',
                "max_size" => '100000',
                "max_width" => '3000',
                "max_height" => '3000'
        ));

//                 $config['upload_path']          = './assets/user_files/beacons';
//                 $config['allowed_types']        = 'gif|jpg|png|jpeg';
//                 $config['max_size']             = 100000;
//                 $config['max_width']            = 3000;
//                 $config['max_height']           = 3000;

//           $this->load->library('upload', $config);

//             if ( ! $this->upload->do_upload('heatmap'))
//                 {
//                         $error = array('error' => $this->upload->display_errors());

//                         echo "<pre>cv"; print_r($error);
//                 }
//                 else
//                 {
//                         echo "succes"; 
//                 }

// exit();






     //echo "<pre>"; print_r($this->upload->do_upload('heatmap')); exit();
   
        if (!$this->upload->do_upload('heatmap')) 
        {
          $error = array('error' => $this->upload->display_errors()); 
 
          redirect("Beacons/add_heat_map/".$id);
        }

        else
        { 
          $data['image'] = $images_file;
          $data['event_id'] = $id;
          $h_id = $this->Beacons_model->add_heat_map($data);
          redirect("Beacons/edit_heat_map/".$id."/".$h_id);
        }

     }
     public function save_heatmap_coords($event_id,$id)
     {  
        $data['heatmap_id']=$this->uri->segment(4);
        $data['coords']=$this->input->post('coor');
        $data['beacons_id']=$this->input->post('name');
        if ($this->input->post())
        {
            $id=$this->Beacons_model->save_heatmap_coords($data);
            echo '<area title="'.$value['beacon_name'].'" alt="Mapping"  id="are" shape="rect"  onclick="delete1('.$id.')"  href="javascript:void(0);"                          
            coords="'.$data['coords'].'">';
        }
     }

     public function delete_heatmap_coords($event_id,$h_id,$id)
     {
          $map = $this->Beacons_model->delete_heatmap_coords($id);
          $this->session->set_flashdata('map_data', 'Deleted');
          redirect("Beacons/edit_heat_map/".$event_id."/".$h_id);
     }
     public function save_uuid($event_id)
     {
        $data['event_array']['Id'] = $event_id;
        $data['event_array']['UUID'] = $this->input->post('UUID');
        $this->Event_model->update_admin_event($data);
        $this->session->set_flashdata('uuid_data', 'UUID Save Successfully.');
        redirect(base_url()."Beacons/index/".$event_id);
     }
     public function show_heat_movements($id,$mid)
     {
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $heatmap_data = $this->Beacons_model->get_heatmap($id,$mid);
          $this->data['heatmap_data'] = $heatmap_data;

          $beacon_data=$this->Beacons_model->get_all_beacons_by_id($id);
          $this->data['beacon_data']=$beacon_data;

          $heatmap_mapping_data=$this->Beacons_model->get_heatmap_mapping_data($id,$mid);
          $this->data['heatmap_mapping_data']=$heatmap_mapping_data;
          
          $heat_movements=$this->Beacons_model->get_heat_movements($mid);
          $this->data['heat_movements'] = $heat_movements;
            
          $this->template->write_view('css', 'beacons/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'beacons/show_heat_movement', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'beacons/js', $this->data, true);
          $this->template->render();
    
     }

}

