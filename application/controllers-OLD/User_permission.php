<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_permission extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'User Permission';
		$this->data['smalltitle'] = 'User permission Details';
        $this->data['page_edit_title'] = 'User_permission';
		$this->data['breadcrumb'] = 'User permission';
		parent::__construct($this->data);
        $this->load->model('User_model');
		$this->load->model('User_permission_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
	}

	public function index()                 
	{        	
		$users = $this->User_model->get_user_list();
        $this->data['users'] = $users;

		$this->template->write_view('css', 'user_permission/css', $this->data , true);
		$this->template->write_view('content', 'user_permission/index', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
		$this->template->write_view('js', 'user_permission/js', $this->data , true);
		$this->template->render();
	}

    public function edit($roleid)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $permissions = $this->User_permission_model->get_role_menu($roleid);

        //echo'<pre>'; print_r($permissions); exit;
        //$this->data['permissions'] = $permissions;
        $array_permission=array();
        foreach ($permissions as $key=>$value)
        {
            $array_permission[]=$value['Menu_id'];
        }
        $this->data['array_permission']=$array_permission;
        if($this->input->post())
        {
            $diffdata=array_diff($array_permission,$this->input->post('Menu_id'));
            
            foreach ($diffdata as $key=>$val)
            {
                $this->User_permission_model->delete_permssion($roleid,$val);
            }
            
            
            $adddata=array_diff($this->input->post('Menu_id'),$array_permission);
            foreach ($adddata as $key=>$val)
            {
                $data=array();
                $data['Menu_id'] = $val;
                $data['Role_id']= $roleid;
                $this->User_permission_model->insert_permssion($data);
            }
            $this->session->set_flashdata('permission_data', 'Updated');
            redirect('/User_permission/edit/'.$roleid);
        }
        
        $this->template->write_view('css', 'user_permission/css', $this->data , true);
        $this->template->write_view('content', 'user_permission/edit', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        $this->template->write_view('js', 'user_permission/js', $this->data , true);
        $this->template->render();
    }

    
}
