<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Message';
          $this->data['smalltitle'] = 'Message Details';
          $this->data['breadcrumb'] = 'Message';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          ini_set('memory_limit', '1024M');
          $this->load->library('formloader1');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Message_model');
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Profile_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('notifications_model');
          $user = $this->session->userdata('current_user');
          $eventname=$this->Event_model->get_all_event_name();
          if(!in_array($this->uri->segment(3),$eventname))
          {
               $Subdomain=$this->uri->segment(3);
               $flag=1;
               redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);
          }
     }

     public function index($Subdomain)
     {  
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('12',$menu_list) && !in_array('13',$menu_list))
          {
               redirect(base_url().'Forbidden/');
          }
          if ($this->data['pagetitle'] == "Message")
          {
               $title = "Messages";
          }
          if ($user != '')
          {
               $req_mod = $this->router->fetch_class();
               $menu_id=$this->Event_model->get_menu_id($req_mod);
               $current_date=date('Y/m/d');
               $logged_in_user_id=$user[0]->User_id;
               $this->Event_model->add_view_hit($logged_in_user_id,$current_date,$menu_id); 
            
               $parameters = $this->uri->uri_to_assoc(1);
               $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));

               $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
               $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);

               if ($cnt == 1)
               {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                    $this->data['notes_list'] = $notes_list;
               }
               else
               {
                    $event_type=$event_templates[0]['Event_type'];
                    if($event_type==3)
                    {
                         $this->session->unset_userdata('current_user');
                         $this->session->unset_userdata('invalid_cred');
                         $this->session->sess_destroy();
                    }
                    else
                    {
                         $parameters = $this->uri->uri_to_assoc(1);
                         $Subdomain=$this->uri->segment(3);
                         $acc_name=$this->uri->segment(2);
                         redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                    }
               }
          }
          $user = $this->session->userdata('current_user');
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
          $this->data['sign_form_data']=$res1;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $this->data['Subdomain'] = $Subdomain;

          /* $cms_menu_details = $this->Cms_model->get_cms_page($event_templates[0]['Id'],$menuid);
            $this->data['cms_menu_details'] = $cms_menu_details; */

          if (empty($user))
          {
               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
               $this->template->write_view('content', 'registration/index', $this->data, true);
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);
               $this->template->render();
          }
          else
          {

               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $this->data['event_templates'] = $event_templates;

               $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
               $this->data['menu'] = $menu;

               $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
               $this->data['menu_list'] = $menu_list;

               $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
               $this->data['cms_menu'] = $cmsmenu;

               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
               $orid = $this->data['user']->Id;

               $message = $this->Message_model->get_speaker_value($Subdomain);
               $this->data['message'] = $message;

               $chat_users = $this->Message_model->get_chat_users($Subdomain);
               $this->data['chat_users'] = $chat_users;

               $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
               $this->data['speakers'] = $speakers;

               $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
               $this->data['attendees'] = $attendees;

               $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
               $this->data['advertisement_images'] = $advertisement_images;

               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
               $this->template->write_view('content', 'Messages/index', $this->data, true);
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->write_view('footer', 'Messages/footer', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);
               $this->template->render();
          }
     }

     public function chatsmsgprivate($acc_name,$Subdomain)
     {
          if (!$this->input->post())
          {
               $user = $this->session->userdata('current_user');
               $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
               $eventid = $event_val[0]['Id'];
               $event = $this->Event_model->get_module_event($eventid);
               $menu_list = explode(',', $event[0]['checkbox_values']);
               if(!in_array('12',$menu_list) && !in_array('13',$menu_list))
               {
                    redirect(base_url().'Forbidden/');
               }
               if ($this->data['pagetitle'] == "Message")
               {
                    $title = "Messages";
               }
               if ($user != '')
               {
                    //$req_mod = $this->router->fetch_class();
                    $req_mod ="Private Message";
                    $menu_id=$this->Event_model->get_menu_id($req_mod);
                    $current_date=date('Y/m/d');
                    $logged_in_user_id=$user[0]->User_id;
                    $this->Event_model->add_view_hit($logged_in_user_id,$current_date,'12',$event_val[0]['Id']); 
                 
                    $parameters = $this->uri->uri_to_assoc(1);
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                    if ($cnt == 1)
                    {
                         $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                         $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                         $event_type=$event_templates[0]['Event_type'];
                         if($event_type==3)
                         {
                              $this->session->unset_userdata('current_user');
                              $this->session->unset_userdata('invalid_cred');
                              $this->session->sess_destroy();
                         }
                         else
                         {
                              $parameters = $this->uri->uri_to_assoc(1);
                              $Subdomain=$this->uri->segment(3);
                              $acc_name=$this->uri->segment(2);
                              redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                         }
                    }
               }
          }

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
               $this->data['fb_login_data'] = $fb_login_data;
               $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          for($i=0;$i<count($menu_list);$i++)
          {
                if('Photos'==$menu_list[$i]['pagetitle'])
                {
                     $mid=$menu_list[$i]['id'];
                }
          }
          $this->data['menu_id']=$mid;
          $this->data['menu_list'] = $menu_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          /* $cms_menu_details = $this->Cms_model->get_cms_page($event_templates[0]['Id'],$menuid);
            $this->data['cms_menu_details'] = $cms_menu_details; */

          $this->data['subdomain'] = $Subdomain;
          $this->data['Subdomain'] = $Subdomain;

          $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
          $this->data['speakers'] = $speakers;

          $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
          $this->data['attendees'] = $attendees;
          if(!empty($user))
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               if($user[0]->is_moderator!='1'){
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id']);
                    $this->data['view_chats1'] = $view_chats1;
               }
               
               $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
               $this->data['notify_msg'] = $notifiy_msg;

               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $orid = $this->data['user']->Id;
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               if($user[0]->Rid == '4')
               {
                    $tmp = $this->data['attendees'];
                    foreach ($tmp as $key => $value)
                    {
                         foreach($value as $k1 => $v1)
                         {    
                              $status = $this->Event_template_model->check_message_permisson($v1['Id'],$user[0]->Id,$event_templates[0]['Id']);
                              if($status == '0')
                              {
                                   unset($tmp[$key][$k1]);
                              }
                         }
                    }
                    $this->data['attendees'] = $tmp;
               }

               if ($this->input->post())
               {
                    $imgdataarray = $this->input->post('unpublished_photo');
                    if (!empty($imgdataarray))
                    {
                         $imag_photos = json_encode($imgdataarray);
                    }
                    else
                    {
                         $imag_photos = NULL;
                    }

                    if ($this->uri->segment(3) != "")
                    {
                         $resiver = $this->uri->segment(3);
                    }
                    else
                    {
                         $resiver = NULL;
                    }

                    $data1 = array(
                            'Message' => $this->input->post('Message'),
                            'Sender_id' => $lid,
                            'Receiver_id' => $resiver,
                            'Parent' => '0',
                            'Time' => date("Y-m-d H:i:s"),
                            'image' => $imag_photos,
                            'ispublic' => $this->input->post('ispublic'),
                            'qasession_id'=>NULL
                    );
                    $this->Message_model->send_speaker_message($data1);
                    $Sid = $this->uri->segment(3);
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
                    $string = '';
                    $user = $this->session->userdata('current_user');
                    $lid = $user[0]->Id;
                    echo '<div style="padding-top: 20px;">
                    <div id="messages">';
                    foreach ($view_chats1 as $key => $value)
                    {
                         echo "<div class='message_container'>";
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "'>";
                         echo "test";
                         echo "</div>";
                         echo "</div>";
                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Senderlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                              }
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/avatar-1-small1416918047.jpg" >';
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Sendername']);
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo ucfirst($value['Recivername']);
                              echo '</a>';                                               
                              echo "</div>";
                         }
                         echo "</div>";
                         echo "<div class='msg_date'>";
                         echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";
                         if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                              echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                         }
                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '" >';
                              echo "</div>";
                         }
                         echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         echo "<div class='comment_panel' id='slidepanel" . $value['Id'] . "'>
                                   <form action='' method='post' name='form" . $value['Id'] . "' id='form" . $value['Id'] . "'>
                                        <div class='comment_message_img'>";
                         if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                         {
                              if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $user[0]->Logo . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div><textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea><br />
                         <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                         <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                         </form>
                         <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $i = 0;
                              $flag = false;
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }


                                   echo "<div class='comment_container " . $classadded . "'>  
                                           <div class='comment_message_img'>";
                                   if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                                   {
                                        if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                        {
                                             echo "<img src='" . $cval['Logo'] . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                        }
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/avatar-1-small1416918047.jpg'>";
                                   }

                                   echo "</div>
                                   <div class='comment_text'>" . $cval['comment'] . "</div>";
                                   ?>
                                   <?php echo $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']); ?>
                                   <?php echo "</div>";
                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }
                                   $i++;
                              }
                         }
                         echo "</div>";
                         echo "</div>";
                         echo "</div>";
                    }
                    echo "</div></div>";
                    exit;
               }
          }
          
           $this->template->write_view('css', 'frontend_files/css', $this->data, true);
           $this->template->write_view('header', 'frontend_files/header', $this->data, true);
           $this->template->write_view('js', 'frontend_files/js', $this->data, true);
           if ($event_templates[0]['Event_type'] == '1')
           {
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                      $this->template->write_view('content', 'Messages/chatsprivate', $this->data, true);
                  } 
           }
          elseif ($event_templates[0]['Event_type'] == '2')
          {

             if (empty($user))
             {
                 $this->template->write_view('content', 'registration/index', $this->data, true);
             }
             else
             {
                $this->template->write_view('content', 'Messages/chatsprivate', $this->data, true);
             }
          }
          elseif ($event_templates[0]['Event_type'] == '3') 
          {
               $this->session->unset_userdata('acc_name');
               $acc['acc_name'] =  $acc_name;
               $this->session->set_userdata($acc);
               $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('12',$event_templates[0]['Id']);
               if($isforcelogin['is_force_login']=='1' && empty($user))
               {
                   $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
               }
               else
               {
                    $this->template->write_view('content', 'Messages/chatsprivate', $this->data, true);
               }
          }
          else
          {
              if (empty($user))
             {
                 $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
             }
             else
             {
                 $this->template->write_view('content', 'Messages/chatsprivate', $this->data, true);
             } 
          }
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->render();  
     }
     
     
     public function chatsmsgpublic($acc_name,$Subdomain)
     {
        if($_GET['action']==1)
        {
		  $singupclickmsg['singupclickmsg']='1';
          $this->session->set_userdata($singupclickmsg);
          redirect(base_url().'Messages/'.$acc_name.'/'.$Subdomain.'/publicmsg');
        }
          if (!$this->input->post())
          {
               $user = $this->session->userdata('current_user');
               $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
               $eventid = $event_val[0]['Id'];
               $event = $this->Event_model->get_module_event($eventid);
               $menu_list = explode(',', $event[0]['checkbox_values']);
               if(!in_array('12',$menu_list) && !in_array('13',$menu_list))
               {
                    redirect(base_url().'Forbidden/');
               }
               if ($this->data['pagetitle'] == "Message")
               {
                    $title = "Messages";
               }
               if ($user != '')
               {
                    //$req_mod = $this->router->fetch_class();
                     $req_mod = "Public Messages";
                    $menu_id=$this->Event_model->get_menu_id($req_mod);

                    $current_date=date('Y/m/d');
                    $logged_in_user_id=$user[0]->User_id;

                    $this->Event_model->add_view_hit($logged_in_user_id,$current_date,'13',$eventid); 
                    $parameters = $this->uri->uri_to_assoc(1);

                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                    if ($cnt == 1)
                    {
                         $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                         $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                         $event_type=$event_templates[0]['Event_type'];
                         if($event_type==3)
                         {
                              $this->session->unset_userdata('current_user');
                              $this->session->unset_userdata('invalid_cred');
                              $this->session->sess_destroy();
                         }
                         else
                         {
                              $parameters = $this->uri->uri_to_assoc(1);
                              $Subdomain=$this->uri->segment(3);
                              $acc_name=$this->uri->segment(2);
                              redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                         }
                    }
               }
          }

          $user = $this->session->userdata('current_user');
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
               $this->data['fb_login_data'] = $fb_login_data;
               $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          for($i=0;$i<count($menu_list);$i++)
          {
                if('Photos'==$menu_list[$i]['pagetitle'])
                {
                     $mid=$menu_list[$i]['id'];
                }
          }
          $this->data['menu_id']=$mid;
          $this->data['menu_list'] = $menu_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          /* $cms_menu_details = $this->Cms_model->get_cms_page($event_templates[0]['Id'],$menuid);
            $this->data['cms_menu_details'] = $cms_menu_details; */

          $this->data['subdomain'] = $Subdomain;
          $this->data['Subdomain'] = $Subdomain;

          $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
          $this->data['speakers'] = $speakers;

          $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
          $this->data['attendees'] = $attendees;

          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          
          $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;
          if(!empty($user))
          {
               $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
               $this->data['notify_msg'] = $notifiy_msg;

               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $orid = $this->data['user']->Id;
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               if ($this->input->post())
               {
                    $imgdataarray = $this->input->post('unpublished_photo');
                    if (!empty($imgdataarray))
                    {
                         $imag_photos = json_encode($imgdataarray);
                    }
                    else
                    {
                         $imag_photos = NULL;
                    }

                    if ($this->uri->segment(3) != "")
                    {
                         $resiver = $this->uri->segment(3);
                    }
                    else
                    {
                         $resiver = NULL;
                    }

                    $data1 = array(
                            'Message' => $this->input->post('Message'),
                            'Sender_id' => $lid,
                            'Receiver_id' => $resiver,
                            'Parent' => '0',
                            'Time' => date("Y-m-d H:i:s"),
                            'image' => $imag_photos,
                            'ispublic' => $this->input->post('ispublic'),
                            'qasession_id'=>NULL
                    );
                    $this->Message_model->send_speaker_message($data1);
                    $Sid = $this->uri->segment(3);
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
                    $string = '';
                    $user = $this->session->userdata('current_user');
                    $lid = $user[0]->Id;
                    echo '<div style="padding-top: 20px;">
                    <div id="messages">';
                    foreach ($view_chats1 as $key => $value)
                    {
                         echo "<div class='message_container'>";
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "'>";
                         echo "test";
                         echo "</div>";
                         echo "</div>";

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Senderlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                              }
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/avatar-1-small1416918047.jpg" >';
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Sendername']);
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo ucfirst($value['Recivername']);
                              echo '</a>';                                               
                              echo "</div>";
                         }
                         echo "</div>";
                         echo "<div class='msg_date'>";
                         echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";
                         if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                              echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                         }
                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '" >';
                              echo "</div>";
                         }
                         echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         echo "<div class='comment_panel' id='slidepanel" . $value['Id'] . "'>
                                   <form action='' method='post' name='form" . $value['Id'] . "' id='form" . $value['Id'] . "'>
                                        <div class='comment_message_img'>";
                         if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                         {
                              if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $user[0]->Logo . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div><textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea><br />
                         <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                         <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                         </form>
                         <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";
                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $i = 0;
                              $flag = false;
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {

                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }


                                   echo "<div class='comment_container " . $classadded . "'>  
                                           <div class='comment_message_img'>";
                                   if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                                   {
                                        if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                        {
                                             echo "<img src='" . $cval['Logo'] . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                        }
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/avatar-1-small1416918047.jpg'>";
                                   }

                                   echo "</div>
                                   <div class='comment_text'>" . $cval['comment'] . "</div>";
                              ?>
                                   <?php echo $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']); ?>
                                   <?php echo "</div>";
                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }
                                   $i++;
                              }
                         }
                         echo "</div>";
                         echo "</div>";
                         echo "</div>";
                    }
                    echo "</div></div>";
                    exit;
               }
            }
            
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          if ($event_templates[0]['Event_type'] == '1')
          {
             if (empty($user))
             {
                 $this->template->write_view('content', 'registration/index', $this->data, true);
             }
             else
             {
                 $this->template->write_view('content', 'Messages/chatspublic', $this->data, true);
             }
          }
          elseif ($event_templates[0]['Event_type'] == '2')
          {

             if (empty($user))
             {
                 $this->template->write_view('content', 'registration/index', $this->data, true);
             }
             else
             {
                $this->template->write_view('content', 'Messages/chatspublic', $this->data, true);
             }
          }
          elseif ($event_templates[0]['Event_type'] == '3') 
          {
               $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('13',$event_templates[0]['Id']);
               if($isforcelogin['is_force_login']=='1' && empty($user))
               {
                   $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
               }
               else
               {
                    if($this->session->userdata('singupclickmsg')=='1')
                    {
                         $this->session->unset_userdata('singupclickmsg');
                         $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                         $this->session->unset_userdata('acc_name');
                         $acc['acc_name'] =  $acc_name;
                         $this->session->set_userdata($acc);
                         $this->template->write_view('content', 'Messages/chatspublic', $this->data, true);
                    }
               }
          }
          else
          {
               if (empty($user))
             {
               $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
             }
             else
             {
                 $this->template->write_view('content', 'Messages/chatspublic', $this->data, true);
             }
          }
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->render();  
          
     }

     function deletemssage($id)
     {
          $chat_users = $this->Message_model->deletemsg($id);
          $this->data['chat_users'] = $chat_users;
          exit;
     }
     
     public function chatsviewdata($id, $acc_name,$Subdomain = null)
     { 
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $Sid = $id;
          $this->data['Sid'] = $Sid;
          $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }


               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $Sid,
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic'),
                       'qasession_id'=>NULL
               );
               $current_date=date('Y/m/d');
               $this->Message_model->add_msg_hit($lid,$current_date,$Sid,$dataevents[0]['Id']); 
            
               $this->Message_model->send_speaker_message($data1);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               if (!empty($view_chats1[0]['Recivername']))
               {
                    $username = $view_chats1[0]['Recivername'];
               }
               else
               {
                    $username = "User";
               }

               echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Messages to ' . $username . '<br/></h3>';
               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";
                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                    {
                         if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                         {
                              echo '<img src="' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                         }
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";
                    if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                         echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                    }
                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id']. "'>
                                        <div class='comment_message_img'>";
                    if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                    {
                         if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                         {
                              echo "<img src='" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                    <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
                    <div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                    <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />                         
                    </form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                              {
                                   if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                   {
                                        echo "<img src='" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                   }
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                    </div>";
                    echo "</div>";
               }
               exit;
          }
          exit;
     }
     public function chatsexibitor($uid, $acc_name,$Subdomain = null)
     {

          $this->load->model('User_model');
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$uid);

          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
               }
               else
               {
                    $resiver = NULL;
               }

               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $uid,
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic'),
                       'qasession_id'=>NULL
               );

               $current_date=date('Y/m/d');
               $this->Message_model->add_msg_hit($lid,$current_date,$uid,$dataevents[0]['Id']); 
               $this->Message_model->send_speaker_message($data1);

               if ($this->input->post('ispublic') == '1')
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
               }
               else
               {
                   /*if(!empty($resiver))
                   {
                       $strRec = @implode(',',$resiver);
                       if(!empty($strRec))
                       {
                           $arrUser = $this->User_model->getUserFromIds($strRec);
                           if(!empty($arrUser))
                           {
                               foreach($arrUser as $intKey=>$strValue)
                               {
                                   $strMsg = $this->input->post('Message');
                                   $strMsg .= "<br/>";
                                   $strMsg .= "Event Link : ".  base_url()."Events/".$Subdomain;
                                   $this->email->from($user[0]->Email, $user[0]->Firstname);
                                   $this->email->to($strValue['Email']);
                                   $this->email->subject("Private Message");
                                   $this->email->set_mailtype("html");
                                   $this->email->message($strMsg);    
                                   $this->email->send();
                               }
                           }
                       }
                   }*/
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$uid);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
               <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";
                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                    {
                         if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                         {
                              echo '<img src="' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                         }
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";
                    echo "<div class='msg_date'>";
                    echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";
                    if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                         echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                    }
                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }
                    
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    }
                    else
                    {
                          echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                    }
                    
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                                        <div class='comment_message_img'>";
                    if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                    {
                         if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                         {
                              echo "<img src='" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>";
 
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                    }
                    else
                    {
                         echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                    }
                    
                    echo "<div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                    }
                    else
                    {
                        echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",0);' />";
                    }
                                             
                    echo "</form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                              {
                                   if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                   {
                                        echo "<img src='" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                   }
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

                              echo "</div>
                              <div class='comment_wrapper'>        
                              <div class='comment_username'>
                                   " . ucfirst($cval['user_name']) . "
                              </div>
                              <div class='comment_text'>
                                   " . $cval['comment'] . "
                              </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";

                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                         </div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }
     public function chatsspeaker($id, $acc_name,$Subdomain = null)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $Sid = $id;
          $this->data['Sid'] = $Sid;
          $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $mid=array();
               $modertor_id=$this->Event_model->get_user_all_moderator($dataevents[0]['Id'],$Sid);
               if(count($modertor_id) > 0)
               {
                    $mids=array_column($modertor_id,'moderator_id');
               }
               else
               {
                    $mids=array($Sid);
               }
               $resiver=array_unique($mids);
               foreach ($resiver as $key => $value) 
               {
                    $data1 = array(
                            'Message' => $this->input->post('Message'),
                            'Sender_id' => $lid,
                            'Receiver_id' => $value,
                            'Parent' => '0',
                            'Event_id' => $dataevents[0]['Id'],
                            'Time' => date("Y-m-d H:i:s"),
                            'image' => $imag_photos,
                            'org_msg_receiver_id'=>$Sid,
                            'ispublic' => $this->input->post('ispublic'),
                            'qasession_id'=>NULL
                    );
                    $this->Message_model->send_speaker_message($data1);
                    $this->Message_model->add_msg_hit($lid,date("Y-m-d"),$value,$dataevents[0]['Id']); 
               }
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Question with ' . $user[0]->Firstname . '<br/></h3>';
               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";
                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }
                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                    {
                         if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                         {
                              echo '<img src="' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                         }
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";
                    if(!empty($value['org_Firstname'].$value['org_Lastname']))
                    {
                         echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                    }
                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                                        <div class='comment_message_img'>";
                    if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                    {
                         if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                         {
                              echo "<img src='" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                    <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
                    <div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                    <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />                         
                    </form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                              {
                                   if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                   {
                                        echo "<img src='" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                   }
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";


                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               exit;
          }
          exit;
     }

     public function chatspublic($acc_name,$Subdomain = null)
     {
        
          $this->load->model('User_model');
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
                    $mid=array();
                    $org_msg_receiver_id=array();
                    foreach ($resiver as $key => $value) 
                    {
                         $modertor_id=$this->Event_model->get_user_all_moderator($dataevents[0]['Id'],$value);
                         if(count($modertor_id) > 0)
                         {
                              $mids=array_column($modertor_id,'moderator_id');
                              foreach($mids as $key1 => $value1) 
                              {
                                   $org_msg_receiver_id[]=$value;
                              }
                         }
                         else
                         {
                              $mids=array($value);
                         }
                         $mid=array_merge($mid,$mids);
                    }
                    $resiver=$mid;
               }
               else
               {
                    $resiver = NULL;
               }

               $data1 = array(
                  'Message' => $this->input->post('Message'),
                  'Sender_id' => $lid,
                  'Receiver_id' => $resiver,
                  'Parent' => '0',
                  'Event_id' => $dataevents[0]['Id'],
                  'Time' => date("Y-m-d H:i:s"),
                  'image' => $imag_photos,
                  'org_msg_receiver_id'=>$org_msg_receiver_id,
                  'ispublic' => $this->input->post('ispublic')
               );

               $current_date=date('Y/m/d');
               foreach ($resiver as $key => $value) {
                    $this->Message_model->add_msg_hit($lid,$current_date,$value,$dataevents[0]['Id']); 
               }
               
               $this->Message_model->send_grp_speaker_message($data1);
               if ($this->input->post('ispublic') == '1')
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
               }
               else
               {
                   /*if(!empty($resiver))
                   {
                       $strRec = @implode(',',$resiver);
                       if(!empty($strRec))
                       {
                           $arrUser = $this->User_model->getUserFromIds($strRec);
                           if(!empty($arrUser))
                           {
                               foreach($arrUser as $intKey=>$strValue)
                               {
                                   $strMsg = $this->input->post('Message');
                                   $strMsg .= "<br/>";
                                   $strMsg .= "Event Link : ".  base_url()."Events/".$Subdomain;
                                   $this->email->from($user[0]->Email, $user[0]->Firstname);
                                   $this->email->to($strValue['Email']);
                                   $this->email->subject("Private Message");
                                   $this->email->set_mailtype("html");
                                   $this->email->message($strMsg);    
                                   $this->email->send();
                               }
                           }
                       }
                   }*/
                   
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id']);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
               <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                    {
                         if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                         {
                              echo '<img src="' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                         }
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";
                    echo "<div class='msg_date'>";
                    echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";
                    if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                         echo "<span style='text-align:right;color:black;'>Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname']."</span>";
                    }
                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }
                    
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    }
                    else
                    {
                          echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                    }
                    
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                    <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                    <div class='comment_message_img'>";
                    if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                    {
                         if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                         {
                              echo "<img src='" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>";
 
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                    }
                    else
                    {
                         echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                    }
                    
                    echo "<div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                       if($this->input->post('ispublic') == '1')
                       {
                              echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                       }
                       else
                       {
                             echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",0);' />";
                       }
                                             
                    echo "</form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                              {
                                   if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                   {
                                        echo "<img src='" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                   }
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

                              echo "</div>
                              <div class='comment_wrapper'>        
                              <div class='comment_username'>
                                   " . ucfirst($cval['user_name']) . "
                              </div>
                              <div class='comment_text'>
                                   " . $cval['comment'] . "
                              </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";

                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div></div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }
     
     public function chatsdata($Subdomain = null)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
               }
               else
               {
                    $resiver = NULL;
               }
               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $resiver,
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic'),
                       'qasession_id'=>NULL
               );

               $this->Message_model->send_speaker_message($data1);
               if ($this->input->post('ispublic') == '1')
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
               }
               else
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id']);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
               <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";
                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                    {
                         if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                         {
                              echo '<img src="' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                         }
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";

                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                    <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                    <div class='comment_message_img'>";
                    if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                    {
                         if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                         {
                              echo "<img src='" . $user[0]->Logo . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                         }
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                    <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
                    <div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                    <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />                  
                    </form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                              {
                                   if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                   {
                                        echo "<img src='" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                   }
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }
                              echo "</div>
                              <div class='comment_wrapper'>        
                              <div class='comment_username'>
                                   " . ucfirst($cval['user_name']) . "
                              </div>
                              <div class='comment_text'>
                                   " . $cval['comment'] . "
                              </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                         </div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }

     public function loadmore($acc_name,$Subdomain, $start, $end, $type, $Sid = null)
     {   
          if ($type == '1')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id'], $start, $end);
          }
          else if ($type == '0')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               if($this->input->post('ismsgsrceen')=='1'){
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id'], $start, $end);
               }
               else
               {
                    $view_chats1=$this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],$start,$end,null,$Sid);
               }
          }
          else
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid, $msgid = null, $start, $end);
          }

          if (!empty($view_chats1))
          {
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               foreach ($view_chats1 as $key => $value)
               {
                    if (!empty($value['Id']))
                    {
                         echo "<div class='message_container'>";
                         if ($value['Sender_id'] == $user[0]->Id || ($type=='1' && $user[0]->Role_id=='3'))
                         {
                              echo "<div class='msg_edit-view-box'>";
                              echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                              echo "</div>";
                              echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                              echo "Delete";
                              echo "</div>";
                              echo "</div>";
                         }

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Sender_id'] == $user[0]->Id && $value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Senderlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                              }
                         }
                         else if ($value['Receiver_id'] == $user[0]->Id && $value['Receiverlogo']!="" && !empty(pathinfo($value['Receiverlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Receiverlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Receiverlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Receiverlogo'] . '" >';    
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Sendername']);
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo ucfirst($value['Recivername']);

                              echo '</a>';                                               
                              echo "</div>";
                         }
                         echo "</div>";
                         echo "<div class='msg_date'>";
                         echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";
                         if(!empty($value['org_Firstname'].$value['org_Lastname'])){
                              echo "Desired Receiver:  ".ucfirst($value['org_Firstname']).' '.$value['org_Lastname'];
                         }
                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                              echo "</a>";
                              echo "</div>";
                         }
                         
                         if ($type == '0')
                         {
                              echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                         }
                         else
                         {
                              echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         }
                         
                         echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                         <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                         <div class='comment_message_img'>";
                         if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                         {
                              if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $user[0]->Logo . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>";
                         if ($type == '0')
                         {
                              echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                         }
                         else
                         {
                              echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                         }
                               
                         echo "<div class='photo_view_icon'>     
                         <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                         <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                         <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                         <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                         </ul>
                         <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                         if ($type == '0')
                         {
                              echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
                         }
                         else
                         {
                              echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
                         }
                         
                         echo "</form>
                         <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              $i = 0;
                              $flag = false;
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }

                                   echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                   if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                                   {
                                        if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                        {
                                             echo "<img src='" . $cval['Logo'] . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                        }
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                   }

                                   echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                                   if ($cval['image'] != "")
                                   {
                                        $image_comment = json_decode($cval['image']);
                                        echo "<div class='msg_photo'>";
                                        echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                        echo "</a>";
                                        echo "</div>";
                                   }
                                   if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                   {
                                        echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                   }
                                   echo "</div>";


                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }

                                   $i++;
                              }
                         }
                         echo "</div>
                              </div>";
                         echo "</div>";
                    }
               }
               exit;
          }
          else
          {
               echo "";
               exit;
          }
     }
     public function loadmore1($acc_name,$Subdomain, $start, $end, $type, $Sid = null)
     {     
          if ($type == '1')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id'], $start, $end);
          }
          else if ($type == '0')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id'], $start, $end);
          }
          else
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], $start, $end, $msgid = null,$Sid);
          }

          if (!empty($view_chats1))
          {
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               foreach ($view_chats1 as $key => $value)
               {
                    if (!empty($value['Id']))
                    {
                         echo "<div class='message_container'>";

                         if ($value['Sender_id'] == $user[0]->Id)
                         {
                              echo "<div class='msg_edit-view-box'>";
                              echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                              echo "</div>";
                              echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                              echo "Delete";
                              echo "</div>";
                              echo "</div>";
                         }

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Sender_id'] == $user[0]->Id && $value['Senderlogo'] != "" && !empty(pathinfo($value['Senderlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Senderlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Senderlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';    
                              }
                         }
                         else if ($value['Receiver_id'] == $user[0]->Id && $value['Receiverlogo'] != "" && !empty(pathinfo($value['Receiverlogo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($value['Receiverlogo'], FILTER_VALIDATE_URL))
                              {
                                   echo '<img src="' . $value['Receiverlogo'] . '" >';
                              }
                              else
                              {
                                   echo '<img src="' . base_url() . '/assets/user_files/' . $value['Receiverlogo'] . '" >';    
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Sendername']);
                         echo '</a>';
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo ucfirst($value['Recivername']);
                              echo '</a>';                                               
                              echo "</div>";
                         }
                         echo "</div>";
                         echo "<div class='msg_date'>";
                         echo $this->get_timeago($value['Time'],$dataevents[0]['Event_show_time_zone']);
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";

                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                              echo "</a>";
                              echo "</div>";
                         }
                         
                         if ($type == '0')
                         {
                              echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                         }
                         else
                         {
                              echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         }
                         
                         echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                         <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $value['Id'] . "'>
                         <div class='comment_message_img'>";
                         if ($user[0]->Logo!="" && !empty(pathinfo($user[0]->Logo, PATHINFO_EXTENSION)))
                         {
                              if(filter_var($user[0]->Logo, FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $user[0]->Logo . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>";
                         if ($type == '0')
                         {
                              echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                         }
                         else
                         {
                              echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                         }
                         
                         echo "<div class='photo_view_icon'>     
                         <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                         <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                         <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                         <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                         </ul>
                         <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                         if ($type == '0')
                         {
                              echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
                         }
                         else
                         {
                              echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
                         }
                              
                         echo "</form>
                         <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              $i = 0;
                              $flag = false;
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }

                                   echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                   if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                                   {
                                        if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                                        {
                                             echo "<img src='" . $cval['Logo'] . "'>";
                                        }
                                        else
                                        {
                                             echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                                        }
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                   }

                                   echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";

                                   if ($cval['image'] != "")
                                   {
                                        $image_comment = json_decode($cval['image']);
                                        echo "<div class='msg_photo'>";
                                        echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                        echo "</a>";
                                        echo "</div>";
                                   }
                                   if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                   {
                                        echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                   }
                                   echo "</div>";
                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }

                                   $i++;
                              }
                         }
                         echo "</div>
                              </div>";
                         echo "</div>";
                    }
               }
               exit;
          }
          else
          {
               echo "";
               exit;
          }
     }
     public function commentadd($id, $acc_name,$Subdomain = NULL) 
     {
         
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");
               $msg_id=$this->input->post('msg_id');
               $sender_id=$this->Message_model->get_senderid($msg_id);
               // j($Subdomain);
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               // lq();
               // j($dataevents);
               $comment_id = $this->Message_model->add_comment($dataevents[0]['Id'],$array_add);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id, $this->input->post('msg_id'));
               $current_date=date('Y/m/d');
               $this->Message_model->add_comment_hit($lid,$current_date,$sender_id,$dataevents[0]['Id']); 
               if (!empty($view_chats1[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_chats1[0]['comment'] = array_reverse($view_chats1[0]['comment']);
                    foreach ($view_chats1[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'>  
                         <div class='comment_message_img'>";
                         if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>
                         <div class='comment_wrapper'>        
                         <div class='comment_username'>
                              " . ucfirst($cval['user_name']) . "
                         </div>
                         <div class='comment_text'>
                              " . $cval['comment'] . "
                         </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";


                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;
     }

     public function commentaddpublic($acc_name,$Subdomain = NULL, $flag)
     {
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");
               $msg_id=$this->input->post('msg_id');
               $sender_id=$this->Message_model->get_senderid($msg_id);
               $current_date=date('Y/m/d');
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $this->Message_model->add_comment_hit($lid,$current_date,$sender_id,$dataevents[0]['Id']); 
               $comment_id = $this->Message_model->add_comment($dataevents[0]['Id'],$array_add);
               
               if ($flag == 1)
               {    
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }
               else
               {
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }
               
               if (!empty($view_chats1[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_chats1[0]['comment'] = array_reverse($view_chats1[0]['comment']);
                    foreach ($view_chats1[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'>  
                         <div class='comment_message_img'>";
                         if ($cval['Logo'] != "" && !empty(pathinfo($cval['Logo'], PATHINFO_EXTENSION)))
                         {
                              if(filter_var($cval['Logo'], FILTER_VALIDATE_URL))
                              {
                                   echo "<img src='" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";    
                              }
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>
                         <div class='comment_wrapper'>        
                         <div class='comment_username'>
                              " . ucfirst($cval['user_name']) . "
                         </div>
                         <div class='comment_text'>
                              " . $cval['comment'] . "
                         </div>" . $this->get_timeago($cval['Time'],$dataevents[0]['Event_show_time_zone']) . "</div>";


                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;
     }

     public function readbyuser($acc_name,$Subdomain)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $data = $this->Message_model->msg_notify_update($dataevents[0]['Id']);
          echo 333;
          exit;
     }

     public function delete_message($acc_name,$Subdomain,$id)
     {
          echo "333";
          $chats = $this->Message_model->delete_message($id);
          exit;
     }

     public function delete_comment($acc_name,$Subdomain,$id)
     {
          echo "333";
          $chats = $this->Message_model->delete_comment($id);
          exit;
     }

     function get_timeago($ptime,$timezone)
     {
          date_default_timezone_set("UTC");
          if(!empty($timezone))
          {
             if(strpos($timezone,"-")==true)
             { 
               $arr=explode("-",$timezone);
               $intoffset=$arr[1]*3600;
               $intNew = abs($intoffset);
               $ptime = strtotime(date('Y-m-d H:i:s',strtotime($ptime)-$intNew));
             }
             if(strpos($timezone,"+")==true)
             {
               $arr=explode("+",$timezone);
               $intoffset=$arr[1]*3600;
               $intNew = abs($intoffset);
               $ptime = strtotime(date('Y-m-d H:i:s',strtotime($ptime)+$intNew));
             }
          }

          $estimate_time = time() - $ptime;

          if ($estimate_time < 1)
          {
               return '1 second ago';
          }

          $condition = array(
                  12 * 30 * 24 * 60 * 60 => 'year',
                  30 * 24 * 60 * 60 => 'month',
                  24 * 60 * 60 => 'day',
                  60 * 60 => 'hour',
                  60 => 'minute',
                  1 => 'second'
          );

          foreach ($condition as $secs => $str)
          {
               $d = $estimate_time / $secs;

               if ($d >= 1)
               {
                    $r = round($d);
                    return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
               }
          }
     }
     
     
     public function autonotificationload($acc_name,$Subdomain)
     {
        
          $menu_id1='';
          if($this->input->post('menu_id'))
          {
               $menu_id1=$this->input->post('menu_id');
          }
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $data = $this->Message_model->msg_notify_autoload($dataevents[0]['Id']);
          $req_mod = $this->router->fetch_class();
               
          $menu_id=$this->Event_model->get_menu_id($req_mod);
          $current_date=date('Y/m/d');
          $user = $this->session->userdata('current_user');
          $logged_in_user_id=$user[0]->User_id;
          $this->Event_model->delete_view_hit($logged_in_user_id,$current_date,$menu_id); 
          $notify_setting=$this->Event_model->getnotificationsetting($dataevents[0]['Id']);   
          $notify_res=array();
          if(empty($data))
          {
               $notify_res['notification_res_cnt'] = 0;
               $notify_res['success'] = "0";    
          }
          else
          {
               $notify_res=array();
               $notify_res['success'] = "1";
               $notify_res['notification_res_cnt'] = count($data);
               $notify_res['notification_res'] = $data;
          }
          if(!empty($user[0]->Id))
          {
            $notify_message = $this->notifications_model->get_push_notification($dataevents[0]['Id'],$user[0]->Id,$acc_name,$Subdomain,$dataevents[0]['Event_show_time_zone'],$user[0]->Email,$notify_setting[0]['email_display']);

            if(empty($notify_message))
            {
                $notify_res['push_success'] = "0";
            }
            else
            {
                $notify_res['push_success'] = "1";
                $notify_res['push_notification'] = $notify_message;
            }
          }
          if(!empty($user[0]->Id))
          {
               $session_notify=$this->notifications_model->get_session_notification($dataevents[0]['Id'],$user[0]->Id,$acc_name,$Subdomain,$dataevents[0]['Event_show_time_zone']);
               if(empty($session_notify))
               {
                    $notify_res['session_success'] = "0";
               }
               else
               {
                    $notify_res['session_success'] = "1";
                    $notify_res['session_notify'] = $session_notify;
               }
          }
          $private_message_popup = $this->notifications_model->get_private_msg_notification($user[0]->Id,$dataevents[0]['Id'],$acc_name,$Subdomain,$user[0]->Email,$notify_setting[0]['email_display']);
          if(empty($private_message_popup))
          {
               $notify_res['push_private_success'] = "0";
          }
          else
          {
               $notify_res['push_private_success'] = "1";
               $notify_res['push_private_notification'] = $private_message_popup;
          }
          $request_metting_popup=$this->notifications_model->get_request_metting_by_event_user($user[0]->Id,$user[0]->Role_id,$dataevents[0]['Id'],$acc_name,$Subdomain,$user[0]->Email,$notify_setting[0]['email_display']);
          if(count($request_metting_popup) > 0)
          {
               $notify_res['request_metting_success'] = "1";
               $notify_res['request_metting_notification'] = $request_metting_popup; 
          }
          else
          {
               $notify_res['request_metting_success'] = "0";
          }
          $outbit_popup=$this->notifications_model->get_auction_outbid_notify($dataevents[0]['Id'],$user[0]->Id,$acc_name,$Subdomain);
          if(count($outbit_popup) > 0)
          {
               $notify_res['outbit_popup_success'] = "1";
               $notify_res['outbit_popup_notification'] = $outbit_popup; 
          }
          else
          {
               $notify_res['outbit_popup_success'] = "0";
          }
          $data1=json_encode($notify_res);
          echo $data1;
          exit;
     }
     public function auctionoutbidnotify($Subdomain)
     {

          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $req_mod = $this->router->fetch_class();
               
          $menu_id=$this->Event_model->get_menu_id($req_mod);
          $current_date=date('Y/m/d');
          $user = $this->session->userdata('current_user');
          $logged_in_user_id=$user[0]->User_id;
          $data = $this->Message_model->bid_msg_notify_autoload($dataevents[0]['Id'],$logged_in_user_id);
          $this->Event_model->delete_view_hit($logged_in_user_id,$current_date,$menu_id); 
           
          $notify_res=array();
          if(empty($data))
          {
               $notify_res['notification_res_cnt'] = 0;
               $notify_res['success'] = "0";
          }
          else
          {
               $notify_res['success'] = "1";
               $notify_res['product_name']=$data[0]['name'];
               $notify_res['notification_res_cnt'] = count($data);
               $notify_res['notification_res'] = $data;
          }
                    
          $data1=json_encode($notify_res);
          echo $data1;
          exit;
     }
}
