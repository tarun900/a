<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Secondvisitorevent extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Visitor_Event_model');
		$this->load->model('Login_model');
		$this->load->model('Event_model');
		$this->load->model('Role_management_model');
		$this->load->model('Add_Attendee_model');
		$this->load->library('upload');
		$this->load->library('email');
	}
	public function index()
	{
	    $this->load->view('visitorevent/secondvisitor',$this->data);
	}
	public function check_accname_valid()
	{
		$acc_name=str_replace(" ","_",$this->input->post('org_acc_name'));
		$valid=$this->Visitor_Event_model->check_acc_name_valid($acc_name);
		if(count($valid) > 0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
	}
	public function check_email_valid()
	{
		$org_email=$this->input->post('org_email');
		$superadminemail=$this->Visitor_Event_model->check_org_email_valid($org_email);
		$useremail=$this->Visitor_Event_model->check_user_email_valid($org_email);
		if(count($superadminemail) > 0 || count($useremail) > 0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
	}
	public function visitor_event_create_account()
	{

		$org_user['acc_name']=preg_replace('/[^A-Za-z0-9\-]/', '',$this->input->post('org_acc_name'));
		$org_user['Company_name']=$this->input->post('org_acc_name');
		$org_user['Firstname']=$this->input->post('org_first_name');
		$org_user['Lastname']=$this->input->post('org_last_name');
		$org_user['Email']=$this->input->post('org_email');
		$org_user['Password']=md5($this->input->post('org_password'));
		$org_user['Country']=$this->input->post('org_country');
		$org_user['Mobile']=$this->input->post('org_phone_no');
		$org_user['subscriptiontype']=23;
		$org_user['Created_date']=date("Y-m-d H:i:s");
		$super_org_id=$this->Visitor_Event_model->create_super_admin_org($org_user);
		$user['acc_name']=preg_replace('/[^A-Za-z0-9\-]/', '',$this->input->post('org_acc_name'));
		$user['Company_name']=$this->input->post('org_acc_name');
		$user['Firstname']=$this->input->post('org_first_name');
		$user['Lastname']=$this->input->post('org_last_name');
		$user['Email']=$this->input->post('org_email');
		$user['Password']=md5($this->input->post('org_password'));
		$user['Country']=$this->input->post('org_country');
		$user['Mobile']=$this->input->post('org_phone_no');
		$user['Created_date']=date("Y-m-d H:i:s");
		$org_id=$this->Visitor_Event_model->create_user_org($user);
		$this->Login_model->update_active_status($this->input->post('org_email'));
		$event_data['event_array']['Organisor_id']=$org_id;
		$event_data['event_array']['Subdomain']=$user['acc_name'].preg_replace('/[^A-Za-z0-9\-]/','',$this->input->post('App_name'));
		$event_data['event_array']['Event_name']=$this->input->post('App_name');
		$event_data['event_array']['Description']=$this->input->post('edit_home_screen_html_text');
		$event_data['event_array']['Top_background_color']=$this->input->post('header_color');
		$event_data['event_array']['fun_top_background_color']=$this->input->post('header_color');
		$event_data['event_array']['Top_text_color']=$this->input->post('header_text_color');
		$event_data['event_array']['fun_top_text_color']=$this->input->post('header_text_color');
		$event_data['event_array']['Icon_text_color']=$this->input->post('icon_backgroung_color');
		$event_data['event_array']['Background_color']=$this->input->post('icon_backgroung_color');
		$event_data['event_array']['menu_background_color']=$this->input->post('left_hand_menu_color');
		$event_data['event_array']['menu_hover_background_color']=$this->input->post('left_hand_menu_color');
		$event_data['event_array']['menu_text_color']=$this->input->post('left_hand_menu_text_color');
		$event_data['event_array']['Start_date'] = date("Y-m-d");
        $event_data['event_array']['End_date'] = date("Y-m-d", strtotime(date("Y-m-d")."+1 years"));
        $event_data['event_array']['Status'] = '1';
        $event_data['event_array']['Event_type'] = '3';
        $event_data['event_array']['icon_set_type']=$this->input->post('icon_set_type');
        $event_data['event_array']['Created_date']=date("Y-m-d");
        if(empty($this->input->post('header_crope_images_text')))
        {
	        if (!empty($_FILES['header_image']['name']))
	        {
	            $tempname = explode('.', $_FILES['header_image']['name']);
	            $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
	            $images_file = $tempname_imagename . "." . $tempname[1];
		        $this->upload->initialize(array(
		            "file_name" => $images_file,
		            "upload_path" => "./assets/user_files",
		            "allowed_types" => 'gif|jpg|png|jpeg',
		            "max_size" => '10000',
		            "max_width" => '2000',
		            "max_height" => '2000'
		        ));
	            if ($this->upload->do_multi_upload("header_image"))
	            {
		            copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
		            $event_data['event_array']['fun_image']="assets/images/slideshow/".$images_file;
	                $images=array($images_file);
	                $event_data['event_array']['Images'] =$images;
	            }
	        }
	    }
	    else
    	{
    		$img=$_POST['header_crope_images_text'];
            $filteredData=substr($img, strpos($img, ",")+1); 
            $unencodedData=base64_decode($filteredData);
            $images_file = strtotime(date("Y-m-d H:i:s"))."_event_crop_banner.png"; 
            $filepath = "./assets/user_files/".$images_file; 
            file_put_contents($filepath, $unencodedData);
            copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
	        $event_data['event_array']['fun_image']="assets/images/slideshow/".$images_file;
            $images=array($images_file);
            $event_data['event_array']['Images'] =$images;
    	}
        if(empty($this->input->post('logo_crope_images_text')))
        {
	        if (!empty($_FILES['logo_image']['name']))
	        {
	            $tempname1 = explode('.', $_FILES['logo_image']['name']);
	            $tempname_imagename1 = $tempname1[0] . strtotime(date("Y-m-d H:i:s"));
	            $logo_images = $tempname_imagename1 . "." . $tempname1[1];
	            $this->upload->initialize(array(
	                "file_name" => $logo_images,
	                "upload_path" => "./assets/user_files",
	                "allowed_types" => 'gif|jpg|png|jpeg',
	                "max_size" => '10000',
	                "max_width" => '2000',
	                "max_height" => '2000'
	            ));
	            if ($this->upload->do_multi_upload("logo_image"))
	            {
	            	copy(base_url()."assets/user_files/".$logo_images,"./fundraising/assets/user_files/".$logo_images);
	            	$event_data['event_array']['fun_logo_images']=$logo_images;
	                $event_data['event_array']['Logo_images'] =$logo_images;
	            }         
	        }
    	}
    	else
    	{
    		$img=$_POST['logo_crope_images_text'];
            $filteredData=substr($img, strpos($img, ",")+1); 
            $unencodedData=base64_decode($filteredData);
            $logo_images = strtotime(date("Y-m-d H:i:s"))."_event_logo_crop.png"; 
            $filepath = "./assets/user_files/".$logo_images; 
            file_put_contents($filepath, $unencodedData);
            copy(base_url()."assets/user_files/".$logo_images,"./fundraising/assets/user_files/".$logo_images);
	        $event_data['event_array']['fun_logo_images']=$logo_images;
	        $event_data['event_array']['Logo_images'] =$logo_images;
    	}
		$event_id=$this->Event_model->add_admin_event($event_data);
		$role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
        foreach ($role_menu as $key=>$value)
        {    
            $role=$this->Role_management_model->insertdefultrole($event_id,3,$value->id);
            $role=$this->Role_management_model->insertdefultrole($event_id,95,$value->id);
            $role=$this->Role_management_model->insertdefultrole($event_id,5,$value->id);
        }
        $role=$this->Event_model->eventaddrelation($event_id,$org_id,$org_id,3);
        $checkbox_arr=$this->input->post('active_modules_arr');
        $imcheckbox=implode(",",$checkbox_arr).',';
        $event_data_update['checkbox_values']=$imcheckbox;
        $this->Event_model->update_admin_event_checkbox_values($event_id,$event_data_update);
        $icon_set=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
        foreach ($this->input->post('active_modules_arr') as $key => $value) 
        {
			$event_menu['menu_id']=$value;	
			$event_menu['event_id']=$event_id;
			$event_menu['title']=$this->input->post($value);
			$event_menu['img']="";
			if(in_array($value, $icon_set))
			{
				$event_menu['is_feture_product']='1';
			}
			else
			{
				$event_menu['is_feture_product']='0';	
			}
			$event_menu['img_view']='0';
			$event_menu['Background_color']=$this->input->post('icon_backgroung_color');
			$this->Event_model->add_left_hand_menu_title($event_menu);
		}
		$name=ucfirst($this->input->post('org_first_name')).' '.$this->input->post('org_last_name');
		$msg="<p>Congratulations on starting your App ".$name.",</p>";
		$msg.="<p>I wanted to let you know that myself and the team are here to answer any questions and give you guidance throughout your build. Please feel free to get in touch with me if you need a helping hand or need anything answered.</p>";
		$msg.="<p>When you are ready to launch your App click the “Launch My App” on the bottom of the left hand menu in the Content Management System.</p>";
		$msg.="<p>For your records, your username is ".$this->input->post('org_email').".</p>";
		$msg.="You can access the Content Management System by going to <a href='".base_url()."' target='_blank'>".base_url()."</a> and logging with your credentials.";
		$msg.="<p>All the best,</p>";
		$msg.="<br/><p>Lawrence Gill<br/>Head of Operations";
		$msg.="<br/><a href='http://www.allintheloop.com'>www.allintheloop.com</a></p>";
		$msg.="<p><span>The information transmitted is intended only for the person or entity to which it is addressed and may contain confidential and/or privileged material. Any review, re-transmission, dissemination or other use of, or taking of any action in reliance upon, this information by persons or entities other than the intended recipient is prohibited. If you received this in error, please contact the sender and delete the material from any computer.</span></p>";
		$msg.="<p><span>Whilst all reasonable care has been taken to avoid the transmission of viruses, it is the responsibility of the recipient to ensure that the onward transmission, opening or use of this message and any attachments will not adversely affect its systems or data. No responsibility is accepted by All In The Loop in this regard and the recipient should carry out such virus and other checks as it considers appropriate.</span></p>";
        $config['protocol']   = 'smtp';
        $config['smtp_host']  = 'localhost';
        $config['smtp_port']  = '25';
        $config['smtp_user']  = 'invite@allintheloop.com';
        $config['smtp_pass']  = 'xHi$&h9M)x9m';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from('lawrence@allintheloop.com','Lawrence Gill');
        $this->email->to($this->input->post('org_email'));
        $this->email->subject('Welcome to All In The Loop');
        $this->email->message(html_entity_decode($msg));
        $this->email->send();
        $msg1="Hello Admin,";
        $msg1.="<br/>User Name : ".$name;
        $msg1.="<br/>Email : ".$this->input->post('org_email');
        $msg1.="<br/>Password : ".$this->input->post('org_password');
        $msg1.="<br/>Phone : ".$this->input->post('org_phone_no');
        $msg1.="<br/>Company : ".$this->input->post('org_acc_name');
        $config['protocol']   = 'smtp';
        $config['smtp_host']  = 'localhost';
        $config['smtp_port']  = '25';
        $config['smtp_user']  = 'invite@allintheloop.com';
        $config['smtp_pass']  = 'xHi$&h9M)x9m';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from('invite@allintheloop.com','All In The Loop');
        $this->email->to('lawrence@allintheloop.com');
        $this->email->subject('New Visitor Sign Up');
        $this->email->message(html_entity_decode($msg1));
        $this->email->send();
        $login_data['username']=$this->input->post('org_email');
        $login_data['password']=$this->input->post('org_password');
        $logindetails['current_user'] = $this->Login_model->check_login($login_data);
        $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
        $acc_name=$this->Add_Attendee_model->get_acc_name($Organisor_id);
        $logindetails['acc_name']=$acc_name;
        $this->session->set_userdata($logindetails);
        redirect(base_url().'Event/eventhomepage/'.$event_id);
	}
	public function save_lead_data()
	{
		$uldata['user_name']=$this->input->post('user_name');
		$uldata['user_email']=$this->input->post('user_email');
		$uldata['created_date']=date('Y-m-d H:i:s');
		//$this->Event_model->save_lead_user_data($uldata);
		echo "Success###";die;
	}
}
?>