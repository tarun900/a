<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Virtual_supermarket_admin extends FrontendController {

    function __construct() {
        $this->data['pagetitle'] = 'Virtual Supermarket';
        $this->data['smalltitle'] = 'Organise Your Exhibitor Group list';
        $this->data['breadcrumb'] = 'Virtual Supermarket';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Event_model');
        $this->load->model('User_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
        $this->load->model('Speaker_model');
        $this->load->model('Exibitor_model');
        $this->load->model('Agenda_model');
        $this->load->model('Map_model');
        $this->load->model('Event_template_model');
        $this->load->library('email'); 
        $this->load->library('upload');
        $this->load->library('session');
    }
    public function index($id) 
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $group_data=$this->Exibitor_model->get_exhibitor_group_list($id);
        $this->data['group_data']=$group_data;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
       
        if ($this->data['user']->Role_name == 'User') 
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        if ($this->data['user']->Role_name == 'Client')
        {
           $event = $this->Event_model->get_admin_event($id);
           $this->data['event'] = $event[0];
        }
        $menudata=$this->Event_model->geteventmenu($id,48);
        $menu_toal_data=$this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'virtual_supermarket_admin/css', $this->data, true);
        $this->template->write_view('content', 'virtual_supermarket_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User') 
        {

            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } 
        else 
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'virtual_supermarket_admin/js', $this->data, true);
        $this->template->render();
    }
    public function add_exhibitor_group($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        if($this->input->post())
        {
            $type_data['group_name']=$this->input->post('exhibitor_group_name');
            $type_data['group_position']=$this->input->post('position_in_directory');
            $type_data['group_color']=$this->input->post('custom_color_picker');
            $type_data['event_id']=$id;
            $type_data['created_date']=date('Y-m-d H:i:s');
            $type_id=$this->Exibitor_model->save_exhibitor_group($type_data);
            $this->session->set_flashdata('ex_in_data', 'Group Added');
            redirect(base_url().'Virtual_supermarket_admin/index/'.$id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'virtual_supermarket_admin/css', $this->data, true);
        $this->template->write_view('content', 'virtual_supermarket_admin/add_exhibitor_group', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Exibitor_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'virtual_supermarket_admin/js', $this->data, true);
        $this->template->render();
    }
    public function exhibitor_group_edit($id,$gid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        if($this->input->post())
        {
            $type_data['group_name']=$this->input->post('exhibitor_group_name');
            $type_data['group_position']=$this->input->post('position_in_directory');
            $type_data['group_color']=$this->input->post('custom_color_picker');
            $type_id=$this->Exibitor_model->update_exhibitor_group($type_data,$id,$gid);
            $this->session->set_flashdata('ex_in_data', 'Group Updated');
            redirect(base_url().'Virtual_supermarket_admin/index/'.$id);
        }
        $edit_group_data=$this->Exibitor_model->get_edit_exhibitors_group($id,$gid);
        $this->data['group_data']=$edit_group_data;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'virtual_supermarket_admin/css', $this->data, true);
        $this->template->write_view('content', 'virtual_supermarket_admin/edit_exhibitor_group', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Exibitor_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'virtual_supermarket_admin/js', $this->data, true);
        $this->template->render();
    }
    public function delete_exhibitor_group($eid,$gid)
    {
        $this->Exibitor_model->remove_exhibitor_group($eid,$gid);
        $this->session->set_flashdata('ex_in_data', 'Group Deleted');
        redirect(base_url().'Virtual_supermarket_admin/index/'.$eid);
    }
}