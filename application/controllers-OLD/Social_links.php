<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Social_links extends FrontendController {

    function __construct() {
        $this->data['pagetitle'] = 'Social links';
        $this->data['smalltitle'] = 'Social links Details';
        $this->data['breadcrumb'] = 'Social links';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Social_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
    }

    public function index($id) 
    {

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;

        if ($this->data['user']->Role_name == 'Attendee' || $this->data['user']->Role_name == 'Speaker') 
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $social = $this->Social_model->get_social_links_list();
            $this->data['social'] = $social;
        }
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('content', 'social/social_links', $this->data, true);
        if ($this->data['user']->Role_name == 'User') 
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } 
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'social/js', $this->data, true);
        $this->template->render();
    }

    public function add($id)
    {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $event_id = $this->Social_model->get_event_id($id);
        $this->data['event_id'] = $event_id;

        if($this->data['page_edit_title'] = 'add')
        {
            if($this->input->post())
            {
                $data['social_array']['User_id'] = $logged_in_user_id;
                $data['social_array']['Event_id'] = $event_id[0]['Event_id'];
                $data['social_array']['website_url'] = $this->input->post('website_url');
                $data['social_array']['facebook_url'] = $this->input->post('facebook_url');
                $data['social_array']['twitter_url'] = $this->input->post('twitter_url');
                $data['social_array']['linkedin_url'] = $this->input->post('linkedin_url');

                $social_id = $this->Social_model->add_social_links($data);
                $this->session->set_flashdata('social_data', 'Added');
                redirect("Social_links/index/".$id);
            }
        }
        $this->template->write_view('css', 'admin/add_css', $this->data , true);
        $this->template->write_view('content', 'social/add_social_links', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        $this->template->write_view('js', 'social/js', $this->data , true);
        $this->template->render();
    }

    public function edit($id)
    {

        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        if ($this->data['user']->Role_name == 'Attendee' || $this->data['user']->Role_name == 'Speaker') 
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $social = $this->Social_model->get_social_links_list($id);
            $this->data['social'] = $social;
        }

        if($this->data['page_edit_title'] = 'edit')
        {

            if($id == NULL || $id == '')
            {
                redirect('Social_links');
            }
            if($this->input->post())
            {  

                $data['social_array']['Organisor_id'] = $logged_in_user_id;
                $data['social_array']['Event_id'] = $Event_id;
                $data['social_array']['website_url'] = $this->input->post('website_url');
                $data['social_array']['facebook_url'] = $this->input->post('facebook_url');
                $data['social_array']['twitter_url'] = $this->input->post('twitter_url');
                $data['social_array']['linkedin_url'] = $this->input->post('linkedin_url');
                $this->Social_model->update_social_links($data);
                $this->session->set_flashdata('social_data', 'Updated');
                redirect("Social_links/index/".$id);
            }
        }

        $this->session->set_userdata($data);
        $this->template->write_view('css', 'admin/add_css', $this->data , true);
        $this->template->write_view('content', 'social/edit_social_links', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        $this->template->write_view('js', 'social/js', $this->data , true);
        $this->template->render();
    }

    public function delete_social_links($id,$Event_id)
    {
        if($this->data['user']->Role_name == 'Attendee')
        {
            $social_list = $this->Social_model->get_social_links_list($id);
            $this->data['social_list'] = $social_list;
        }
        
        $social = $this->Social_model->delete_social_links($id);
        $this->session->set_flashdata('social_data', 'Deleted');
        redirect("Social_links/index/".$Event_id);
    }
}
