<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Advertising_admin extends FrontendController
{

     function __construct()
     {

          $this->data['pagetitle'] = 'Sponsors & Adverts';
          $this->data['smalltitle'] = 'Include sponsors’ messages, banners & adverts in your app or use a Google Adsense code.';
          $this->data['breadcrumb'] = 'Sponsors & Adverts';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);

          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Event_template_model');

          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
              
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('5',$module))
          {
               
               redirect('Forbidden');
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 1;
               $req_mod = ucfirst($this->router->fetch_class());
               $cnt = $this->Agenda_model->check_auth("Advertising", $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
         
          
          if ($cnt == 1 && in_array('5',$menu_list))
          {
               $this->load->model('Advertising_model');
               $this->load->model('Setting_model');
               $this->load->model('Speaker_model');
               $this->load->model('Profile_model');
               $this->load->model('Agenda_model');
               $this->load->model('Cms_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               
               redirect('Forbidden'); 
          }
     }

     public function index($id)
     {

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Advertising_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
      
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $advertising_list = $this->Advertising_model->get_advertising_list($id);
          $this->data['advertising_list'] = $advertising_list;

          $menudata = $this->Event_model->geteventmenu($id, 5);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $this->template->write_view('css', 'advertising_admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'advertising_admin/index', $this->data, flase);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('js', 'advertising_admin/js', $this->data, true);
          $this->template->render();
     }

     public function add($id)
     {

          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
         
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $Organisor_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($id);
          $this->data['cms_list'] = $cms_list;

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $advertising_list = $this->Advertising_model->get_advertising_list($id);
          $this->data['advertising_list'] = $advertising_list;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];      

          if ($this->input->post())
          {
               if($this->input->post('colorCheckbox')=="green")
               {
                    if(empty($this->input->post('h_image_crop_data_textbox')))
                    {     
                         if (!empty($_FILES['H_images']['name']))
                         {
                              $tempname = explode('.', $_FILES['H_images']['name']);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $images_file = $tempname_imagename . "." . $tempname[1];
                              $this->upload->initialize(array(
                                      "file_name" => $images_file,
                                      "upload_path" => "assets/user_files",
                                      "allowed_types" => 'gif|jpg|png|jpeg',
                                      "max_size" => '100000',
                                      "max_width" => '3000',
                                      "max_height" => '3000'
                              ));
                              if (!$this->upload->do_multi_upload("H_images"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', $error['error']);
                              }
                         }
                    }
                    else
                    {
                         $img=$_POST['h_image_crop_data_textbox'];
                         $filteredData=substr($img, strpos($img, ",")+1); 
                         $unencodedData=base64_decode($filteredData);
                         $images_file = strtotime(date("Y-m-d H:i:s"))."header_image_crop.png"; 
                         $filepath = "./assets/user_files/".$images_file; 
                         file_put_contents($filepath, $unencodedData); 
                    }

                    if(empty($this->input->post('f_image_crop_data_textbox')))
                    {
                         if (!empty($_FILES['F_images']['name']))
                         {
                              $tempname = explode('.', $_FILES['F_images']['name']);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $filenames = $tempname_imagename . "." . $tempname[1];

                              $this->upload->initialize(array(
                                      "file_name" => $filenames,
                                      "upload_path" => "assets/user_files",
                                      "allowed_types" => 'gif|jpg|png|jpeg',
                                      "max_size" => '100000',
                                      "max_width" => '3000',
                                      "max_height" => '3000'
                              ));
                              if (!$this->upload->do_multi_upload("F_images"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', $error['error']);
                              }
                         }
                    }
                    else
                    {
                         $img=$_POST['f_image_crop_data_textbox'];
                         $filteredData=substr($img, strpos($img, ",")+1); 
                         $unencodedData=base64_decode($filteredData);
                         $filenames = strtotime(date("Y-m-d H:i:s"))."footer_image_crop.png"; 
                         $filepath = "./assets/user_files/".$filenames; 
                         file_put_contents($filepath, $unencodedData); 
                    }
               }     

               $Menu_id = $this->input->post('Menu_id');
               $data['advertising_array']['Menu_id'] = implode(',', $Menu_id);
               $Cms_id = $this->input->post('Cms_id');
               $data['advertising_array']['Cms_id'] = implode(',', $Cms_id);
               if($this->input->post('colorCheckbox')=="red")
               {
                    $data['advertising_array']['Google_header_adsense'] = $this->input->post('Google_header_adsense');
                    $data['advertising_array']['Google_footer_adsense'] = $this->input->post('Google_footer_adsense');
               }
               if($this->input->post('colorCheckbox')=="green")
               {
                    if($this->input->post('Header_link') || $this->input->post('Footer_link'))
                    {
                         $data['advertising_array']['Header_link'] = $this->input->post('Header_link');
                         $data['advertising_array']['Footer_link'] = $this->input->post('Footer_link');
                    }
                    else
                    {
                         $data['advertising_array']['Header_link'] = $this->input->post('Header');
                         $data['advertising_array']['Footer_link'] = $this->input->post('Footer');
                    }
               }
               $data['advertising_array']['Advertisement_name'] = $this->input->post('Advertisement_name');
               $data['advertising_array']['Organisor_id'] = $Organisor_id;
               $data['advertising_array']['Event_id'] = $Event_id;

               if ($images_file != "")
               {
                    $data['advertising_array']['H_images'] = $images_file;
               }

               if ($filenames != "")
               {
                    $data['advertising_array']['F_images'] = $filenames;
               }
               $data['advertising_array']['show_all'] = $this->input->post('show_all');
               $data['advertising_array']['show_sticky'] = $this->input->post('show_sticky');

               $advertising_id = $this->Advertising_model->add_advertising($data);
               $this->session->set_flashdata('advertising_data', 'Added');
               redirect("Advertising_admin/index/" . $id);
          }

          $this->template->write_view('css', 'advertising_admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'advertising_admin/add', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'advertising_admin/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = null, $aid = null)
     {
          $Event_id = $id;
          $data['Id'] = $aid;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($id);
          $this->data['cms_list'] = $cms_list;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $advertising_by_id = $this->Advertising_model->get_advertising_list_by_id($id, $aid);
          $this->data['advertising_by_id'] = $advertising_by_id;

          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Event');
               }
               if ($this->input->post())
               {	
                    if($this->input->post('colorCheckbox')=="green")
                    {	
                         if(empty($this->input->post('h_image_crop_data_textbox')))
                         {
                              if (!empty($_FILES['H_images']['name']))
                              {

                                   $imgname = explode('.', $_FILES['H_images']['name']);
                                   $tempname = str_replace(" ", "_", $imgname);
                                   $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                                   $images_file = $tempname_imagename . "." . $tempname[1];

                                   $this->upload->initialize(array(
                                           "file_name" => $images_file,
                                           "upload_path" => "./assets/user_files",
                                           "allowed_types" => 'gif|jpg|png|jpeg',
                                           "max_size" => '100000',
                                           "max_width" => '3000',
                                           "max_height" => '3000'
                                   ));

                                   if (!$this->upload->do_multi_upload("H_images"))
                                   {
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->session->set_flashdata('error', $error['error']);
                                   }
                              }
                              else
                              {
                                   $iff=json_decode($advertising_by_id[0]['H_images']);
                                   $images_file=$iff[0];
                              }
                         }
                         else
                         {
                              $img=$_POST['h_image_crop_data_textbox'];
                              $filteredData=substr($img, strpos($img, ",")+1); 
                              $unencodedData=base64_decode($filteredData);
                              $images_file = strtotime(date("Y-m-d H:i:s"))."header_image_crop.png"; 
                              $filepath = "./assets/user_files/".$images_file; 
                              file_put_contents($filepath, $unencodedData); 
                         }
                         if(empty($this->input->post('f_image_crop_data_textbox')))
                         {
                              if (!empty($_FILES['F_images']['name']))
                              {

                                   $imgname = explode('.', $_FILES['F_images']['name']);
                                   $tempname = str_replace(" ", "_", $imgname);
                                   $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                                   $filenames = $tempname_imagename . "." . $tempname[1];
                                   $this->upload->initialize(array(
                                      "file_name" => $filenames,
                                      "upload_path" => "./assets/user_files",
                                      "allowed_types" => 'gif|jpg|png|jpeg',
                                      "max_size" => '100000',
                                      "max_width" => '3000',
                                      "max_height" => '3000'
                                   ));
                                   if (!$this->upload->do_multi_upload("F_images"))
                                   {
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->session->set_flashdata('error', $error['error']);
                                   }
                              }
                              else
                              {
                                   $iff=json_decode($advertising_by_id[0]['F_images']);
                                   $filenames=$iff[0];
                              }
                         }
                         else
                         {
                              $img=$_POST['f_image_crop_data_textbox'];
                              $filteredData=substr($img, strpos($img, ",")+1); 
                              $unencodedData=base64_decode($filteredData);
                              $filenames = strtotime(date("Y-m-d H:i:s"))."footer_image_crop.png"; 
                              $filepath = "./assets/user_files/".$filenames; 
                              file_put_contents($filepath, $unencodedData); 
                         }
                    } 

                    $data['advertising_array']['Organisor_id'] = $logged_in_user_id;
                    $data['advertising_array']['Event_id'] = $Event_id;
                    if ($images_file != "")
                    {
                         $H_images[] = $images_file;
                         $data['advertising_array']['H_images'] = json_encode($H_images);
                    }

                    if ($filenames != "")
                    {
                         $F_images[] = $filenames;
                         $data['advertising_array']['F_images'] = json_encode($F_images);
                    }

                    $Menu_id = $this->input->post('Menu_id');
                    $data['advertising_array']['Menu_id'] = implode(',', $Menu_id);
                    $Cms_id = $this->input->post('Cms_id');
                    $data['advertising_array']['Cms_id'] = implode(',', $Cms_id);
                    if($this->input->post('colorCheckbox')=="green")
                    {
                         if($this->input->post('Header_link') || $this->input->post('Footer_link'))
                         {
                              $data['advertising_array']['Header_link'] = $this->input->post('Header_link');
                              $data['advertising_array']['Footer_link'] = $this->input->post('Footer_link');
                         }
                         else
                         {
						/*$data['advertising_array']['Header_link'] = $this->input->post('Header');
                              $data['advertising_array']['Footer_link'] = $this->input->post('Footer');*/                
                              $data['advertising_array']['Header_link'] = NULL;
                              $data['advertising_array']['Footer_link'] = NULL;

                         }
                    }
                    
                    $data['advertising_array']['Advertisement_name'] = $this->input->post('Advertisement_name');
                    if($this->input->post('colorCheckbox')=="red")
                    {
                         $data['advertising_array']['Google_header_adsense'] = $this->input->post('Google_header_adsense');
                         $data['advertising_array']['Google_footer_adsense'] = $this->input->post('Google_footer_adsense');
                         $data['advertising_array']['H_images']=NULL;
                         $data['advertising_array']['F_images']=NULL;
                         $data['advertising_array']['Header_link']=NULL;
                         $data['advertising_array']['Footer_link']=NULL;
                    }
                    else
                    {
                         $data['advertising_array']['Google_header_adsense']=NULL;
                         $data['advertising_array']['Google_footer_adsense']=NULL;
                    }
                    if($this->input->post('delete_h_image'))
                    {
                         $data['advertising_array']['H_images']=NULL;
                         $data['advertising_array']['Header_link']=NULL;
                    }
                    if($this->input->post('delete_f_image'))
                    {
                         $data['advertising_array']['F_images']=NULL;
                         $data['advertising_array']['Footer_link']=NULL;
                    }
                    $data['advertising_array']['show_all'] = $this->input->post('show_all');
                    $data['advertising_array']['show_sticky'] = $this->input->post('show_sticky');
                    $this->Advertising_model->update_advertising($data);
                    $this->session->set_flashdata('advertising_data', 'Updated');
                    redirect("Advertising_admin/index/" . $id);
               }

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'advertising_admin/add_css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'advertising_admin/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Advertising_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'advertising_admin/add_js', $this->data, true);
               $this->template->render();
          }
     }

     public function delete_advertising($Event_id, $id)
     {

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $advertising_list = $this->Advertising_model->get_advertising_list($id);
          $this->data['advertising_list'] = $advertising_list[0];

          $advertising = $this->Advertising_model->delete_advertising($id);
          $this->session->set_flashdata('advertising_data', 'Deleted');
          redirect("Advertising_admin/index/" . $Event_id);
     }

     public function checkmenusection()
     {
          if ($this->input->post())
          {
               $menu = $this->Advertising_model->checkmenusection($this->input->post('Menu_id'), $this->input->post('idval'));

               if (empty($menu))
               {
                    echo "error###Menu already exist. Please choose another menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }

     public function checkcmssection()
     {
          if ($this->input->post())
          {
               $cms = $this->Advertising_model->checkcmssection($this->input->post('Cms_id'), $this->input->post('idval'));
               if (empty($cms))
               {
                    echo "error###Cms already exist. Please choose another cms menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }

     public function settings($event_id)
     { 
          
          /*$update['show_all'] = $this->input->post('show_all');
          $update['show_sticky'] = $this->input->post('show_sticky');*/
          $update['adv_show_sticky'] = ($this->input->post('show_sticky'))?:'0';

          $where['Id'] = $event_id;
        
          $advertising = $this->Advertising_model->saveSettings($update,$where);
          $this->session->set_flashdata('advertising_data', 'settings updated.');
          redirect("Advertising_admin/index/" . $event_id);

     }

}