<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once('Twitter_api/twitter_api.php');
class Activity extends CI_Controller
{
  function __construct()
  {
    $this->data['pagetitle'] = 'Activity';
    $this->data['smalltitle'] = 'Activity Details';
    $this->data['breadcrumb'] = 'Activity';
    $this->data['page_edit_title'] = 'edit';
    parent::__construct($this->data);
    $this->load->library('formloader1');
    $this->template->set_template('front_template');
    $this->load->model('Event_template_model');
    $this->load->model('Cms_model');
    $this->load->model('Message_model');
    $this->load->model('Agenda_model');
    $this->load->model('Event_model');
    $this->load->model('Setting_model');
    $this->load->model('Profile_model');
    $this->load->model('Activity_model');
    $this->load->model('Notes_admin_model');
    $this->load->model('Notifications_model');
    $user = $this->session->userdata('current_user');
    $logged_in_user_id=$user[0]->Id;
    $eventname=$this->Event_model->get_all_event_name();
    $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
    $eventid = $event_val[0]['Id'];
    $eventmodule=$this->Event_model->geteventmodulues($eventid);
    $module=json_decode($eventmodule[0]['module_list']);
    /*if(!in_array('45',$module))
    {
      redirect(base_url().'Forbidden/');
    }*/
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);

    if(in_array('45',$menu_list))
    {
      if(in_array($this->uri->segment(3),$eventname))
      {
        if ($user != '')
        {
          $parameters = $this->uri->uri_to_assoc(1);
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
          $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
          if ($cnt == 1)
          {
            $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
            $this->data['notes_list'] = $notes_list;
          }
          else
          {
            $event_type=$event_templates[0]['Event_type'];
            if($event_type==3)
            {
              $this->session->unset_userdata('current_user');
              $this->session->unset_userdata('invalid_cred');
              $this->session->sess_destroy();
            }
            else
            {
              $parameters = $this->uri->uri_to_assoc(1);
              $Subdomain=$this->uri->segment(3);
              $acc_name=$this->uri->segment(2);
              redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
            }
          }
        }
        else
        {   
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $Organisor_id=$event_templates[0]['Organisor_id'];
          $acc_name=$this->Event_template_model->get_acc_name($Organisor_id);
        }
      }
      else
      {
        $parameters = $this->uri->uri_to_assoc(1);
        $flag=1;
        redirect(base_url().'Pageaccess/'.$parameters[$this->data['pagetitle']].'/'.$flag);
      }
    }
    else
    {
      redirect(base_url().'Forbidden/');
    }
  }
  public function index($acc_name,$Subdomain)
  {
       
    if($_GET['action']==1)
    {
      $singupclickmsg['singupclickmsg']='1';
      $this->session->set_userdata($singupclickmsg);
      redirect(base_url().'Activity/'.$acc_name.'/'.$Subdomain);
    }
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $user = $this->session->userdata('current_user');
    if(!empty($user[0]->Id))
    {
      $current_date=date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id,$current_date,'45',$event_templates[0]['Id']); 
    }
    $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting']=$notificationsetting;
    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $user = $this->session->userdata('current_user');
    $arrPermision = $this->Activity_model->getPermission($event_templates[0]['Id']);
    $this->data['arrPermission'] = $arrPermision[0];
    $public_message_feeds = array();
    $photo_feed = array();
    $check_in_feed = array();
    $rating_feed = array();
    $user_feed = array();
    if($arrPermision[0]['public']==1)
    {
      $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgpublic_message_feed=$this->Activity_model->getPublicMessageFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['photo']==1)
    {
      $photo_feed = $this->Activity_model->getPhotoFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgphoto_feed = $this->Activity_model->getPhotoFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['check_in']==1)
    {
      $check_in_feed = $this->Activity_model->getCheckInFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgcheck_in_feed = $this->Activity_model->getCheckInFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['rating']==1)
    {
      $rating_feed = $this->Activity_model->getRatingFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgrating_feed = $this->Activity_model->getRatingFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['update_profile']==1)
    {
      $user_feed = $this->Activity_model->getUserFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orguser_feed = $this->Activity_model->getUserFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['show_session']==1)
    {
      $arrSaveSession = $this->Activity_model->getSaveSessionFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrSaveSession = $this->Activity_model->getSaveSessionFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['auction_bids']==1)
    {
      $arrauctionbid = $this->Activity_model->getauctionbidsFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrauctionbid = $this->Activity_model->getauctionbidsFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['comments']==1)
    {
      $arrcomments = $this->Activity_model->getcommentfeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrcomments = $this->Activity_model->getcommentfeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    $activity_feeds = $this->Activity_model->getActivityFeeds($event_templates[0]['Id'],$user[0]->Id);
    $orgactivity_feeds = $this->Activity_model->getActivityFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    $notify=$this->Activity_model->get_all_notification_by_event($event_templates[0]['Id'],$user[0]->Id);
    if(!empty($arrPermision[0]['hashtag']))
    {
      $settings = array(
      'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
      'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
      'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
      'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
      );
      $url = 'https://api.twitter.com/1.1/search/tweets.json';
      $getfield = '?q=#'.$arrPermision[0]['hashtag'].'&count=10';
      $requestMethod = 'GET';
      $twitter = new TwitterAPIExchange($settings);
      $tweest= $twitter->setGetfield($getfield)
                  ->buildOauth($url, $requestMethod)
                  ->performRequest();
      $tweet=json_decode($tweest,true);
      $this->data['tweets']=$tweet;
    }
    if(!empty($arrPermision[0]['fbpage_name']))
    {
      $profile_id=$arrPermision[0]['fbpage_name'];
      $app_id = "206032402939985";
      $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
      $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
      $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=10");
      $facebook_feeds=json_decode($json_object,true);
      $this->data['facebook_feeds'] = $facebook_feeds;
    }
    $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
    $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);
    function sortByTime($a, $b)
    {
      $a = $a['Time'];
      $b = $b['Time'];
      if ($a == $b)
      {
        return 0;
      }
      return ($a > $b) ? -1 : 1;
    } 
    if(!empty($activity_data))
    {
      usort($activity_data,'sortByTime');
    }
    if(!empty($orgactivity_data))
    {
      usort($orgactivity_data,'sortByTime');
    }
    $this->data['internal_loadmore_show']=count($activity_data) > 10 ? '1' : '0';
    $limit= 10;
    $activity_data= array_slice($activity_data,$start,$limit);
    foreach ($activity_data as $key => $value) {
      $activity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
    }
    $this->data['activity_data']=$activity_data;
    $this->data['alerts_loadmore_show']=count($orgactivity_data) > 10 ? '1' : '0';
    $limit= 10;
    $orgactivity_data= array_slice($orgactivity_data,$start,$limit);
    foreach ($orgactivity_data as $key => $value) 
    {
      $orgactivity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
    }
    $this->data['orgactivity_data']=$orgactivity_data;
    $this->data['fb_login_data'] = $fb_login_data;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $user = $this->session->userdata('current_user');
    $user_id=$user[0]->Id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    for($i=0;$i<count($menu_list);$i++)
    {
      if('activity'==$menu_list[$i]['pagetitle'])
      {
        $mid=$menu_list[$i]['id'];
      }
    }
    $this->data['menu_id']=$mid;
    $this->data['menu_list'] = $menu_list;
    $eid=$event_templates[0]['Id'];
    $res=$this->Agenda_model->getforms($eid,$mid);
    $this->data['form_data']=$res;
    $res1=$this->Event_template_model->get_singup_forms($eid);
    $this->data['sign_form_data']=$res1;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;
    $this->data['Subdomain'] = $Subdomain;
    $array_temp_past = $this->input->post();
    if(!empty($array_temp_past))
    {
      $aj=json_encode($this->input->post());
      $formdata=array('f_id' =>$intFormId,
        'm_id'=>$mid,
        'user_id'=>$user[0]->Id,
        'event_id'=>$eid,
        'json_submit_data'=>$aj
      );
      $this->Agenda_model->formsinsert($formdata);
      redirect(base_url().'Agenda/'.$acc_name.'/'.$Subdomain);
      exit;
    }
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new', $this->data, true);
        $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new', $this->data, true);
        $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('45',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        if($this->session->userdata('singupclickmsg')=='1')
        {
          $this->session->unset_userdata('singupclickmsg');
          $this->template->write_view('content', 'registration/index', $this->data, true);
        }
        else
        {
          $this->session->unset_userdata('acc_name');
          $acc['acc_name'] =  $acc_name;
          $this->session->set_userdata($acc);
          $this->template->write_view('content', 'activity/index_new', $this->data, true);
          $this->template->write_view('footer', 'activity/footer', $this->data, true);
        }
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new', $this->data, true);
        $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }

  public function indextest($acc_name,$Subdomain)
  {
     
    if($_GET['action']==1)
    {
      $singupclickmsg['singupclickmsg']='1';
      $this->session->set_userdata($singupclickmsg);
      redirect(base_url().'activity/'.$acc_name.'/'.$Subdomain);
    }
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;

    $user = $this->session->userdata('current_user');
    if(!empty($user[0]->Id))
    {
      $current_date=date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id,$current_date,'45',$event_templates[0]['Id']); 
    }

   /* social wall heading */
   $socialwall_heading = $this->Activity_model->get_socialwallData($event_templates[0]['Id']);
   $this->data['socialwall_heading'] = $socialwall_heading;
    /*                   */

    $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting']=$notificationsetting;
    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $user = $this->session->userdata('current_user');
    $arrPermision = $this->Activity_model->getPermission($event_templates[0]['Id']);
    $this->data['arrPermission'] = $arrPermision[0];
    $public_message_feeds = array();
    $photo_feed = array();
    $check_in_feed = array();
    $rating_feed = array();
    $user_feed = array();
    if($arrPermision[0]['public']==1)
    {
      $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgpublic_message_feed=$this->Activity_model->getPublicMessageFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['photo']==1)
    {
      $photo_feed = $this->Activity_model->getPhotoFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgphoto_feed = $this->Activity_model->getPhotoFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['check_in']==1)
    {
      $check_in_feed = $this->Activity_model->getCheckInFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgcheck_in_feed = $this->Activity_model->getCheckInFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['rating']==1)
    {
      $rating_feed = $this->Activity_model->getRatingFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgrating_feed = $this->Activity_model->getRatingFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['update_profile']==1)
    {
      $user_feed = $this->Activity_model->getUserFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orguser_feed = $this->Activity_model->getUserFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['show_session']==1)
    {
      $arrSaveSession = $this->Activity_model->getSaveSessionFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrSaveSession = $this->Activity_model->getSaveSessionFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['auction_bids']==1)
    {
      $arrauctionbid = $this->Activity_model->getauctionbidsFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrauctionbid = $this->Activity_model->getauctionbidsFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['comments']==1)
    {
      $arrcomments = $this->Activity_model->getcommentfeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrcomments = $this->Activity_model->getcommentfeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    $activity_feeds = $this->Activity_model->getActivityFeeds($event_templates[0]['Id'],$user[0]->Id);
    $orgactivity_feeds = $this->Activity_model->getActivityFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    $notify=$this->Activity_model->get_all_notification_by_event($event_templates[0]['Id'],$user[0]->Id);
    if(!empty($arrPermision[0]['hashtag']))
    {
      $settings = array(
      'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
      'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
      'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
      'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
      );
      $url = 'https://api.twitter.com/1.1/search/tweets.json';
      $getfield = '?q=#'.$arrPermision[0]['hashtag'].'&count=10';
      $requestMethod = 'GET';
      $twitter = new TwitterAPIExchange($settings);
      $tweest= $twitter->setGetfield($getfield)
                  ->buildOauth($url, $requestMethod)
                  ->performRequest();
      $tweet=json_decode($tweest,true);
      $this->data['tweets']=$tweet;
      $ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
      $insert['data'] = $ip.'-main-'.$_SERVER['HTTP_REFERER'];
      $this->db->insert('tmp',$insert);
    }
    if(!empty($arrPermision[0]['fbpage_name']))
    {

      $profile_id=$arrPermision[0]['fbpage_name'];
      $app_id = "206032402939985";
      $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
      $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
      $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=10");
      $facebook_feeds=json_decode($json_object,true);
      $this->data['facebook_feeds'] = $facebook_feeds;
      // echo "<pre>"; print_r($this->data); exit();
    }
    $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
    $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);
    function sortByTime($a, $b)
    {
      $a = $a['Time'];
      $b = $b['Time'];
      if ($a == $b)
      {
        return 0;
      }
      return ($a > $b) ? -1 : 1;
    } 
    if(!empty($activity_data))
    {
      usort($activity_data,'sortByTime');
    }
    if(!empty($orgactivity_data))
    {
      usort($orgactivity_data,'sortByTime');
    }
    $this->data['internal_loadmore_show']=count($activity_data) > 10 ? '1' : '0';
    $limit= 10;
    $activity_data= array_slice($activity_data,$start,$limit);
    foreach ($activity_data as $key => $value) {
      $activity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
    }
    $this->data['activity_data']=$activity_data;
    $this->data['alerts_loadmore_show']=count($orgactivity_data) > 10 ? '1' : '0';
    $limit= 10;
    $orgactivity_data= array_slice($orgactivity_data,$start,$limit);
    foreach ($orgactivity_data as $key => $value) 
    {
      $orgactivity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
    }
    $this->data['orgactivity_data']=$orgactivity_data;
    $this->data['fb_login_data'] = $fb_login_data;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $user = $this->session->userdata('current_user');
    $user_id=$user[0]->Id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    for($i=0;$i<count($menu_list);$i++)
    {
      if('activity'==$menu_list[$i]['pagetitle'])
      {
        $mid=$menu_list[$i]['id'];
      }
    }
    $this->data['menu_id']=$mid;
    $this->data['menu_list'] = $menu_list;
    $eid=$event_templates[0]['Id'];
    $res=$this->Agenda_model->getforms($eid,$mid);
    $this->data['form_data']=$res;
    $res1=$this->Event_template_model->get_singup_forms($eid);
    $this->data['sign_form_data']=$res1;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;
    $this->data['Subdomain'] = $Subdomain;
    $array_temp_past = $this->input->post();
    if(!empty($array_temp_past))
    {
      $aj=json_encode($this->input->post());
      $formdata=array('f_id' =>$intFormId,
        'm_id'=>$mid,
        'user_id'=>$user[0]->Id,
        'event_id'=>$eid,
        'json_submit_data'=>$aj
      );
      $this->Agenda_model->formsinsert($formdata);
      redirect(base_url().'Agenda/'.$acc_name.'/'.$Subdomain);
      exit;
    }
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new3', $this->data, true);
        // $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new3', $this->data, true);
        // $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('45',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        if($this->session->userdata('singupclickmsg')=='1')
        {
          $this->session->unset_userdata('singupclickmsg');
          $this->template->write_view('content', 'registration/index', $this->data, true);
        }
        else
        {
          $this->session->unset_userdata('acc_name');
          $acc['acc_name'] =  $acc_name;
          $this->session->set_userdata($acc);
          $this->template->write_view('content', 'activity/index_new3', $this->data, true);
          // $this->template->write_view('footer', 'activity/footer', $this->data, true);
        }
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'activity/index_new3', $this->data, true);
        // $this->template->write_view('footer', 'activity/footer', $this->data, true);
      }
    }
  //  $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }



  public function addupdate_feed($acc_name,$Subdomain)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $savedata['message']=$this->input->post('Message');
    if(!empty($this->input->post('unpublished_photo')))
    {
      $savedata['image']=json_encode($this->input->post('unpublished_photo'));
    }
    $savedata['sender_id']=$user[0]->Id;
    $savedata['event_id']=$event_templates[0]['Id'];
    $savedata['time']=date('Y-m-d H:i:s');
    $this->Activity_model->save_update_feed($savedata);
    echo "Success";die;
  }
  public function upload_imag($acc_name,$Subdomain)
  {
    if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
    {
      define("MAX_SIZE", "200000");
      $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
      $uploaddir = "assets/user_files/";
      $thumbuploaddir = "assets/user_files/thumbnail/";
      foreach ($_FILES['photos']['name'] as $name => $value)
      {
        $filename = stripslashes($_FILES['photos']['name'][$name]);
        $size = filesize($_FILES['photos']['tmp_name'][$name]);
        $ext = $this->getExtension($filename);
        $ext = strtolower($ext);
        if (in_array($ext, $valid_formats))
        {
          if ($size < (MAX_SIZE * 1024))
          {
            $fnm=explode(".",$filename);
            $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
            $filename=$filename.".".$fnm[1];
            $image_name = time() . $filename;
            echo "<li>
                    <div class='msg_photo'>
                         <input type='hidden' name='unpublished_photo[]' value='" . $image_name . "'>
                         <img src='" . base_url() . $thumbuploaddir . $image_name . "' class='imgList'>
                         <button class='photo_remove_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button>
                    </div>
               </li>";
            $fnm=explode(".",$image_name);
            $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
            $image_name=$image_name.".".$fnm[1];
            $newname = $uploaddir . $image_name;
            $thumbnewname = $thumbuploaddir . $image_name;
            if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
            {
              $time = time();
              $datafiles[] = $image_name;
              $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
            }
            else
            {
              echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
            }
          }
          else
          {
          echo '<span class="imgList">You have exceeded the size limit!</span>';
          }
        }
        else
        {
          echo '<span class="imgList">Unknown extension!</span>';
        }
      }
      $string = "";
      $string = json_encode($datafiles);
    }
    exit;
  }
  public function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth, $quality)
  { 
    $details = getimagesize("$imageDirectory/$imageName") or die('Please only upload images.'); 
    $type = preg_replace('@^.+(?<=/)(.+)$@', '$1', $details['mime']); 
    eval('$srcImg = imagecreatefrom'.$type.'("$imageDirectory/$imageName");'); 
    $thumbHeight = $details[1] * ($thumbWidth / $details[0]); 
    $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight); 
    imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight,  
    $details[0], $details[1]); 
    eval('image'.$type.'($thumbImg, "$thumbDirectory/$imageName"'. 
    (($type=='jpeg')?', $quality':'').');'); 
    imagedestroy($srcImg); 
    imagedestroy($thumbImg); 
  } 
  public function getExtension($str)
  {
    $i = strrpos($str, ".");
    if (!$i)
    {
      return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
  }
  public function all_loadmore_feed($acc_name,$Subdomain,$start)
  {
    $htmlarr=json_decode($this->get_all_activity_html($acc_name,$Subdomain,$start,$this->input->post('facebook_next_url'),$this->input->post('twitter_next_result')),true);
    $res_arr['all_html']=$htmlarr['internal_html'].$htmlarr['facebook_html'].$htmlarr['twitter_html'];
    $res_arr['all_facebook_next_url']=urlencode($htmlarr['facebook_next_url']);
    $res_arr['all_twitter_next_result']=urlencode($htmlarr['twitter_next_results']);
    echo json_encode($res_arr);
  }

      public function all_loadmore_feed_socialwall($acc_name,$Subdomain,$start)
  {

    $htmlarr=json_decode($this->get_all_activity_html_socialwall($acc_name,$Subdomain,$start,$this->input->post('facebook_next_url'),$this->input->post('twitter_next_result')),true);
    $res_arr['all_html']=$htmlarr['internal_html'].$htmlarr['facebook_html'].$htmlarr['twitter_html'];
    $res_arr['all_facebook_next_url']=urlencode($htmlarr['facebook_next_url']);
    $res_arr['all_twitter_next_result']=urlencode($htmlarr['twitter_next_results']);
    echo json_encode($res_arr);
  }



  public function internal_loadmore_feed($acc_name,$Subdomain,$start)
  {
    $htmlarr=json_decode($this->get_all_activity_html($acc_name,$Subdomain,$start,NULL,NULL),true);
    if(!empty($htmlarr['internal_html']))
    {
      echo $htmlarr['internal_html'];
    }
    else
    {
      echo "No Feed";
    }die;
  }
  public function social_loadmore_feed($acc_name,$Subdomain,$start)
  {
    $htmlarr=json_decode($this->get_all_activity_html($acc_name,$Subdomain,'0',$this->input->post('facebook_next_url'),$this->input->post('twitter_next_result')),true);
    $res_arr['social_html']=$htmlarr['facebook_html'].$htmlarr['twitter_html'];
    $res_arr['social_facebook_next_url']=urlencode($htmlarr['facebook_next_url']);
    $res_arr['social_twitter_next_result']=urlencode($htmlarr['twitter_next_results']);
    echo json_encode($res_arr);
  }
  public function alerts_loadmore_feed($acc_name,$Subdomain,$start)
  {
    $htmlarr=json_decode($this->get_all_activity_html($acc_name,$Subdomain,$start,NULL,NULL),true);
    if(!empty($htmlarr['alerts_html']))
    {
      echo $htmlarr['alerts_html'];
    }
    else
    {
      echo "No Feed";
    }die;
  }
  public function refresh_all_feed($acc_name,$Subdomain)
  {
    $firstcall=json_decode($this->get_all_activity_html($acc_name,$Subdomain,'0','refresh','refresh'),true);
    $secondcall=json_decode($this->get_all_activity_html($acc_name,$Subdomain,'0','refresh','refresh'),true);
    $res_arr['all_html']=$firstcall['internal_html'].$firstcall['facebook_html'].$firstcall['twitter_html'];
    $res_arr['all_facebook_next_url']=urlencode($firstcall['facebook_next_url']);
    $res_arr['all_twitter_next_result']=urlencode($firstcall['twitter_next_results']);
    if($firstcall['internal_loadmore_show']=='1' || !empty($firstcall['facebook_next_url']) || !empty($firstcall['twitter_next_results']))
    {
      $res_arr['all_loadmore_show']='1';
    }
    else
    {
      $res_arr['all_loadmore_show']='0';
    }
    $res_arr['internal_html']=$secondcall['internal_html'];
    $res_arr['internal_loadmore_show']=$secondcall['internal_loadmore_show'];
    $res_arr['social_html']=$secondcall['facebook_html'].$secondcall['twitter_html'];
    $res_arr['social_facebook_next_url']=urlencode($secondcall['facebook_next_url']);
    $res_arr['social_twitter_next_result']=urlencode($secondcall['twitter_next_results']);
    if(!empty($secondcall['facebook_next_url']) || !empty($secondcall['twitter_next_results']))
    {
      $res_arr['social_loadmore_show']='1';
    }
    else
    {
      $res_arr['social_loadmore_show']='0';
    }
    $res_arr['alerts_html']=$secondcall['alerts_html'];
    $res_arr['alerts_loadmore_show']=$secondcall['alerts_loadmore_show'];
    echo json_encode($res_arr);
  } 
  public function Save_user_like_data($acc_name,$Subdomain)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $like_data=$this->input->post();
    $like_data['event_id']=$event_templates[0]['Id'];
    $like=$this->Activity_model->getFeedLike($like_data);
    if($like!='')
    {
      $update_data = array(
        'like' => ($like == '1') ? '0'  :  '1',
      );
      $this->Activity_model->updateFeedLike($update_data,$like_data);
    }
    else
    {
      $like_data['like']='1';
      $like_data['datetime']=date("Y-m-d H:i:s");
      $this->Activity_model->makeFeedLike($like_data);
    }
    unset($like_data['user_id']);
    $like_data['like']='1';
    echo $this->Activity_model->getFeed_total_Like($like_data);
  }
  public function upload_commentimag($acc_name,$Subdomain)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
    {
      define("MAX_SIZE", "20000000");
      $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
      $uploaddir = "assets/user_files/"; //a directory inside
      foreach ($_FILES['cmphto']['name'] as $name => $value)
      {
        $filename = stripslashes($_FILES['cmphto']['name'][$name]);
        $size = filesize($_FILES['cmphto']['tmp_name'][$name]);
        $ext = $this->getExtension($filename);
        $ext = strtolower($ext);
        if (in_array($ext, $valid_formats))
        {
          if ($size < (MAX_SIZE * 1024))
          {
            $fnm=explode(".",$filename);
            $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
            $filename=$filename.".".$fnm[1];
            $image_name = time() . $filename;
            echo "<div class='comment_container_afteruploadimag'><li><div class='msg_photo'>
            <input type='hidden' name='unpublished_commentphoto[]' value='" . $image_name . "'>
            <img src='" . base_url() . $uploaddir . $image_name . "' class='imgList'>
            </div></li><button class='comment_section_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button></div>";
            $fnm=explode(".",$image_name);
            $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
            $image_name=$image_name.".".$fnm[1];
            $newname = $uploaddir . $image_name;
            if (move_uploaded_file($_FILES['cmphto']['tmp_name'][$name], $newname))
            {
              $time = time();
              $datafiles[] = $image_name;
              $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
            }
            else
            {
              echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
            }
          }
          else
          {
            echo '<span class="imgList">You have exceeded the size limit!</span>';
          }
        }
        else
        {
        echo '<span class="imgList">Unknown extension!</span>';
        }
      }
      $string = "";
      $string = json_encode($datafiles);
    }
    exit;
  }
  public function commentaddpublic($acc_name,$Subdomain)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $comment_data['module_type']=$this->input->post('msg_type');
    $comment_data['module_primary_id']=$this->input->post('msg_id');
    $comment_data['user_id']=$user[0]->Id;
    if(!empty($this->input->post('unpublished_commentphoto')))
    {
      $comment_data['comment_image']=json_encode($this->input->post('unpublished_commentphoto'));
    }
    $comment_data['event_id']=$event_templates[0]['Id'];
    $comment_data['comment']=$this->input->post('comment');
    $comment_data['datetime']=date('Y-m-d H:i:s');
    $this->Activity_model->makeFeedComment($comment_data);
    $key=$this->input->post('key_post');
    $comments=$this->Activity_model->getComments($this->input->post('msg_id'),$this->input->post('msg_type'),$event_templates[0]['Id']);
    echo "<div class='comment_data' id='comment_conten".$key."'>";
    if (!empty($comments))
    {
      $i = 0;
      $flag = false;
      foreach ($comments as $ckey => $cval) 
      {  
        echo "<div class='comment_container " . $classadded . " comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
        if ($cval['Logo'] != "")
        {
          echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
        }
        else
        {
          echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
        }
        echo "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['name']) . "</div>
          <div class='comment_text'>" . $cval['comment'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
        echo $cval['datetime'];
        echo"</div></div>";
        if ($cval['comment_image'] != "")
        {
          $image_comment = json_decode($cval['comment_image']);
          echo "<div class='msg_photo photos_feed'>";
          foreach ($image_comment as $ikey => $ivalue) {
          echo '<a class="colorbox_C_' . $key.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
          if($ikey=='0'){                                                            
            echo "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
          }
          echo "</a>";
          }
        echo "</div>";
        }
        if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
        {
          $removecimmentfun="removecomment('".$cval['comment_id']."','".$key."',this)";
          echo '<button class="comment_section_btn" type="button" title="" onclick="'.$removecimmentfun.'">&nbsp;</button>';
        }
        $i++;
        echo "</div>";
      } 
    }
    echo "</div>";die;
  }
  public function delete_comment($acc_name,$Subdomain,$cid)
  {
    $this->Activity_model->delete_comment_form_post($cid);
    echo "success###";
  }
  public function get_all_activity_html($acc_name,$Subdomain,$start,$fburl=NULL,$twitternext=NULL)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $arrPermision = $this->Activity_model->getPermission($event_templates[0]['Id']);
    $public_message_feeds = $photo_feed = $check_in_feed = $rating_feed = $user_feed = $arrSaveSession = $arrauctionbid = $arrcomments =array();
    $orgpublic_message_feed = $orgphoto_feed = $orgcheck_in_feed = $orgrating_feed = $orguser_feed = $orgarrSaveSession = $orgarrauctionbid = $orgarrcomments =array();
    $internal_html = $facebook_html = $twitter_html = $alerts_html = "";
    if($arrPermision[0]['public']==1)
    {
      $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgpublic_message_feed=$this->Activity_model->getPublicMessageFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['photo']==1)
    {
      $photo_feed = $this->Activity_model->getPhotoFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgphoto_feed = $this->Activity_model->getPhotoFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['check_in']==1)
    {
      $check_in_feed = $this->Activity_model->getCheckInFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgcheck_in_feed = $this->Activity_model->getCheckInFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['rating']==1)
    {
      $rating_feed = $this->Activity_model->getRatingFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgrating_feed = $this->Activity_model->getRatingFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['update_profile']==1)
    {
      $user_feed = $this->Activity_model->getUserFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orguser_feed = $this->Activity_model->getUserFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['show_session']==1)
    {
      $arrSaveSession = $this->Activity_model->getSaveSessionFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrSaveSession = $this->Activity_model->getSaveSessionFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['auction_bids']==1)
    {
      $arrauctionbid = $this->Activity_model->getauctionbidsFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrauctionbid = $this->Activity_model->getauctionbidsFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['comments']==1)
    {
      $arrcomments = $this->Activity_model->getcommentfeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrcomments = $this->Activity_model->getcommentfeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    $activity_feeds = $this->Activity_model->getActivityFeeds($event_templates[0]['Id'],$user[0]->Id);
    $orgactivity_feeds = $this->Activity_model->getActivityFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    $notify=$this->Activity_model->get_all_notification_by_event($event_templates[0]['Id'],$user[0]->Id);
    
    $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
    $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);
    $res_arr['internal_loadmore_show']=count($activity_data) > 10 ? '1' : '0';
    if(!function_exists('sortByTime'))
    {
      function sortByTime($a, $b)
      {
        $a = $a['Time'];
        $b = $b['Time'];
        if ($a == $b)
        {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      }
    } 
    if(!empty($activity_data))
    {
      usort($activity_data,'sortByTime');
      $limit= 10;
      $activity_data=array_slice($activity_data,$start,$limit);
      foreach ($activity_data as $key => $value) 
      {
        $internaldivid=uniqid().md5(rand());
        $comments=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
        $internal_html.= "<div class='message_container internal_message' data-sort='".strtotime($value['Time'])."'>";
        $internal_html.= "<div class='msg_main_body'>";
        $internal_html.= "<div class='message_img'>";
        if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){
          $logo=base_url().'assets/user_files/'.$value['Logo'];
        }
        else
        {
          $logo=base_url().'/assets/images/anonymous.jpg';
        }
        $internal_html.= "<img src='".$logo."'></div>";
        $internal_html.= "<div class='msg_type_images pull-right'>";
        $internal_html.= "<img width='40' height='40' src='".base_url()."assets/images/activity_internal_img.png'></div>";
        $internal_html.= "<div class='msg_fromname'>";
        if($value['Role_id'] == '4')
        {
            $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif($value['Role_id'] == '6')
        {
            $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif ($value['Role_id'] == '7')
        {
            $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        else
        {
            $pro_url = '#';
        }
        $nm=ucfirst($value['Firstname']).' '.$value['Lastname'];
        $internal_html.= '<a href="'.$pro_url.'" class="tooltip_data">'.$nm.'</a>';  
        $internal_html.= '</div>';
        $internal_html.= '<div class="msg_toname activity_msg">';
        $internal_html.= ucfirst($value['activity']).'</div>';
        $internal_html.= "<div class='msg_user_companyname'>";
        if(!empty($value['Title']) && !empty($value['Company_name']))
        {
          $internal_html.= $value['Title'].' at '.$value['Company_name'];
        }
        else
        {
          $internal_html.= "";
        }
        $internal_html.= "</div>";
        $internal_html.= "<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['Time']))."</div>";
        if($value['activity_no']=='4')
        {
          $internal_html.= '<div class="activity_session_rating">';
          for ($i=1;$i<=5;$i++) 
          { 
            if($i<=$value['rating'])
            {
                 $img="star-on.png";
            }
            else
            {
                 $img="star-off.png";
            }
            $im_ms=base_url().'assets/images/'.$img;
            $internal_html.= '<img src="'.$im_ms.'">';
          }
          $internal_html.= "</div>";  
        }
        $internal_html.= "</div>";
        $internal_html.= "<div class='msg_message'>";
        if(!empty($value['agenda_id']))
        { 
          $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
          $internal_html .= "<h5><a href=".$agenda_url." class=msg_toname activity_msg>".$value['Message']."</a></h5>";
          $internal_html .="<div class=msg_date>".$value['agenda_time']."</div>";
        }
        else
        {
          if(!empty($value['Message']))
          {
            $internal_html.='<p class="short_msg_show">'.substr($value['Message'],0,300).'</p>';
            $internal_html.='<p class="fully_msg_show" style="display:none;">'.$value['Message'].'</p>';
            if(strlen($value['Message'])>300)
            {
              $internal_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
            }
          }
        }
        $internal_html.= "</div>";
        if(count(json_decode($value['image'],TRUE)) > 0)
        {
          $img=json_decode($value['image'],TRUE); 
          $img1=1; 
          foreach ($img as $ikey => $ivalue) 
          {
            $internal_html.= '<div class="msg_photo">';
            $ms_in=base_url().'assets/user_files/'.$ivalue;
            $vid="colorbox_".$internaldivid;
            $internal_html.= '<a class="'.$vid.'" href="'.$ms_in.'">';
            if($img1=='1')
            {
              $internal_html.= "<img src='".$ms_in."'>";
              $img1++; 
            }
            $internal_html.= "</span></a></div>";
          } 
        }
        $internal_html.= "<div class='like_user_totale'>";
        $internal_html.= "<span style='text-align:left;float: left;'><p class='count_like count_like_".$value['id']."' style='float:left;'>";
        $internal_html.= $value['like_count']=='' ? 0 : $value['like_count'];
        $liketext=$value['like_count'] > 1 ? 'Likes' : 'Like';
        $internal_html.= "</p><p style='float:left;' class='like_text_".$value['id']."'>".$liketext."</p></span>";
        $internal_html.= "<span style='text-align:left;'><p class='comment_count comment_count_".$value['id']."' style='float:left;'>";
        $internal_html.= count($comments);
        $commenttext=count($comments) > 1 ? 'Comments' : 'Comment';
        $internal_html.= "</p><p style='float:left;' class='comment_text_".$value['id']."'>".$commenttext."</p></span></div>";
        $internal_html.= "<div class='col-md-12 col-sm-12 col-xs-12 activity_feed_btn'>";
        $internal_html.= "<div class='like-dislike-btn col-md-3 col-sm-3 col-xs-3'>";
        $unlikeinstyle=$value['is_like']!='1' ? 'display:none;' : '';
        $internal_html.= "<div class='like blue_like dislike_".$value['id']."' style='".$unlikeinstyle."'>";
        $unfunarg="unlike('".$value['id']."','".$value['type']."')";
        //$internal_html.= '<a href="#" class="like_btn blue_btn" onclick="'.$unfunarg.'"><i style="font-size:40px;" class="fa fa-thumbs-o-up"></i></a></div>';
        $internal_html.= '<a href="#" class="like_btn blue_btn" onclick="'.$unfunarg.'"><img width="40" height="40" src="'.base_url().'assets/images/like new.png"></a></div>';
        $likeinstyle=$value['is_like']=='1' ? 'display:none;' : '';
        $internal_html.= "<div class='like like_".$value['id']."' style='".$likeinstyle."'>";
        $likefunarg="like('".$value['id']."','".$value['type']."')";
        //$internal_html.= '<a href="#" class="like_btn" onclick="'.$likefunarg.'"><i style="font-size:40px;" class="fa fa-thumbs-o-up"></i></a></div>';
        $internal_html.= '<a href="#" class="like_btn" onclick="'.$likefunarg.'"><img width="40" height="40" src="'.base_url().'assets/images/not liked.png"></a></div>';
        $internal_html.= "</div><div class='show_add_comment_popup_btn col-md-3 col-sm-3 col-xs-3'>";
        $commfun="showcommentbox('".$internaldivid."');";
        $internal_html.= '<a href="javascript: void(0);" onclick="'.$commfun.'" class="comment_btn">';
        //$internal_html.= "<i class='flaticon-commenting' aria-hidden='true'></i></a></div>";
        $internal_html.= "<img width='40' height='40' src='".base_url()."assets/images/comment new.png'></a></div>";
        $internal_html.= "<div class='show_all_comment_popup_btn col-md-3 col-sm-3 col-xs-3'>";
        $getcommfun="getcomment_popup(".$value['id'].");";
        $internal_html.= '<a href="javascript:void(0);" onclick="'.$getcommfun.'">';
        //$internal_html.= "<i class='fa fa-eye' aria-hidden='true'></i></a></div>";
        $internal_html.= "<img width='40' height='40' src='".base_url()."assets/images/view new.png'></a></div>";
        $internal_html.= "<div class='share_post_btn col-md-3 col-sm-3 col-xs-3'>";
        $internal_html.='<a href="javascript:void(0);" onclick="get_share_post_popup(this);">';
        //$internal_html.= "<i class='fa fa-share-square-o' aria-hidden='true'></i></a>";
        $internal_html.= "<img width='40' height='40' src='".base_url()."assets/images/share new.png'></a>";
        $internal_html.= "<div class='twitter_share_href_data_div' style='display: none;'>";
        $internal_html.= "http://twitter.com/home?status=“".$value['Message']."”".base_url()."activity/".$acc_name."/".$Subdomain."</div>";
        $internal_html.= "<div class='linkdin_share_href_data_div' style='display: none;'>";
        $linkedin="https://www.linkedin.com/shareArticle?mini=true&url=".base_url()."activity/".$acc_name."/".$Subdomain."&title=".ucfirst($value['activity'])."&summary=".$value['Message']; 
        if(!empty($img[0])){
          $linkedin.="&source=".base_url()."assets/user_files/".$img[0];
        }else{
          $linkedin.="&source=LinkedIn;";
        }
        $internal_html.= $linkedin."</div></div>";
        $internal_html.= "</div>";
        $internal_html.= "<div class='comment_panel clearfix' id='slidepanel".$internaldivid."' style='display:none;'>";
        $formurl=base_url().'activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag';
        $internal_html.= "<form method='post' name='commentform_".$internaldivid."' id='commentform_".$internaldivid."' enctype='multipart/form-data' action='".$formurl."'><div class='comment_message_img'>";
        if($user[0]->Logo!="")
        {
          $internal_html.= "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
        }
        else
        {
          $internal_html.= "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
        }
        $internal_html.= "</div><textarea class='user_comment_input' id='comment_text_".$internaldivid."' placeholder='Comment' name='comment'></textarea><div class='photo_view_icon'>";
        $internal_html.= "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $internaldivid . "\").click();' >&nbsp;</div>";
        $commentphotofun="comment_photo('".$internaldivid."');";
        $internal_html.= '<input type="file" capture accept="image/*" class="comment_photoviewer" style="display: none;" name="cmphto[]" id="cmphto'.$internaldivid.'" onchange="'.$commentphotofun.'" multiple></div>';
        $internal_html.= "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
        $internal_html.= "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
        $internal_html.= "<ul id='cpreview".$internaldivid."' class='cpreview clearfix'></ul>";
        $internal_html.= "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
        $addcommentfun="addcomment('".$value['id']."','".$internaldivid."');";
        $internal_html.= '<input type="button" value="Comment"  class="comment_submit" onclick="'.$addcommentfun.'" />';
        $internal_html.= "</form><div class='comment_data comment_content_".$value['id']."' style='display: none;'>";
        if (!empty($comments))
        {
          $i = 0;
          $flag = false;
          foreach ($comments as $ckey => $cval) 
          {  
            $internal_html.= "<div class='comment_container comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
            if ($cval['Logo'] != "")
            {
              $internal_html.= "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
            }
            else
            {
              $internal_html.= "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
            }
            $internal_html.= "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['name']) . "</div>
              <div class='comment_text'>" . $cval['comment'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
            $internal_html.= $cval['datetime'];
            $internal_html.="</div></div>";
            if ($cval['comment_image'] != "")
            {
              $image_comment = json_decode($cval['comment_image']);
              $internal_html.= "<div class='msg_photo photos_feed'>";
              foreach ($image_comment as $ikey => $ivalue) {
                $internal_html.= '<a class="colorbox_C_' . $start.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
              if($ikey=='0'){                                                            
                $internal_html.= "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
              }
              $internal_html.= "</a>";
              }
              $internal_html.= "</div>";
            }
            if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
            {
              $removecommentfun="removecomment('".$cval['comment_id']."','".$value['id']."',this)";
              $internal_html.= '<button class="comment_section_btn" type="button" title="" onclick="'.$removecommentfun.'">&nbsp;</button>';
            }
            $internal_html.= "</div>";
          } 
        }
        $internal_html.= "</div>";
        $internal_html.= "</div>";
        $internal_html.= "</div>";
      }
    }
    $res_arr['internal_html']=$internal_html;
    if(!empty(trim($fburl)))
    {
      if(trim($fburl)=="refresh")
      {
        $profile_id=$arrPermision[0]['fbpage_name'];
        $app_id = "206032402939985";
        $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
        $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
        $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=10");
      }
      else
      {
        $json_object = file_get_contents(trim(urldecode($fburl)));
      }
      $facebook_feeds=json_decode($json_object,true);
      foreach ($facebook_feeds['data'] as $key => $value) 
      {
        $facebookalldivid=uniqid().md5(rand());
        $facebook_html.="<div class='message_container facebook_message' data-sort='".strtotime($value['created_time'])."'>";
        $facebook_html.="<div class='msg_main_body'>";
        $facebook_html.="<div class='message_img'>";
        $logo="https://graph.facebook.com/".$arrPermision[0]['fbpage_name']."/picture?type=large";
        $facebook_html.="<img src='".$logo."'></div>";
        $facebook_html.="<div class='msg_type_images pull-right'>";
        $facebook_html.="<img width='40' height='40' src='".base_url()."assets/images/facebook-logo.png'></div>";
        $facebook_html.="<div class='msg_fromname'>";
        $facebook_html.='<a href="#" class="tooltip_data">'.ucfirst($value['from']['name']).'</a>';  
        $facebook_html.='</div>';
        $facebook_html.="<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['created_time']))."</div>";
        $facebook_html.="</div>";
        $facebook_html.="<div class='msg_message'>";
        if(!empty($value['message']))
        {
          $facebook_html.='<p class="short_msg_show">'.substr($value['message'],0,300).'</p>';
          $facebook_html.='<p class="fully_msg_show" style="display:none;">'.$value['message'].'</p>';
          if(strlen($value['message'])>300)
          {
            $facebook_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
          }
        }
        $facebook_html.="</div>";
        if(!empty($value['full_picture']))
        {
          $facebook_html.='<div class="msg_photo">';
          $facebook_html.='<a class="colorbox_'.$facebookalldivid.'" href="'.$value['full_picture'].'">';
          $facebook_html.="<img src='".$value['full_picture']."'></a>";
          if(array_key_exists('subattachments',$value['attachments']['data'][0]))
          {
            $medialoop=$value['attachments']['data'][0]['subattachments']['data'];
          }
          else
          {
            $medialoop=$value['attachments']['data'];
          }
          foreach ($medialoop as $ikey => $ivalue) 
          {
            if($ivalue['media']['image']['src']!=$value['full_picture'])
            {
              $vid="colorbox_".$facebookalldivid;
              $facebook_html.='<a class="'.$vid.'" href="'.$ivalue['media']['image']['src'].'">';
              $facebook_html.="</a>";
            }
          } 
          $facebook_html.="</div>";
        }
        $facebook_html.="<div class='like_user_totale'>";
        $facebook_html.="<span style='text-align:left;float: left;'><p class='count_like count_like_".$value['id']."' style='float:left;'>";
        $facebook_html.=count($value['likes']['data']);
        $liketext=count($value['likes']['data']) > 1 ? 'Likes' : 'Like';
        $facebook_html.="</p><p style='float:left;' class='like_text_".$value['id']."'>".$liketext."</p></span>";
        $facebook_html.="<span style='text-align:left;'><p class='comment_count comment_count_".$value['id']."' style='float:left;'>";
        $facebook_html.=count($value['comments']['data']);
        $commenttext=count($value['comments']['data']) > 1 ? 'Comments' : 'Comment';
        $facebook_html.="</p><p style='float:left;' class='comment_text_".$value['id']."'>".$commenttext."</p></span></div>";
        $facebook_html.="<div class='col-md-12 col-sm-12 col-xs-12 activity_feed_btn'>";
        $facebook_html.="<div class='share_post_btn col-md-3 col-sm-3 col-xs-3 pull-right'>";
        $facebook_html.='<a href="javascript:void(0);" onclick="get_share_post_popup(this);">';
        //$facebook_html.="<i class='fa fa-share-square-o' aria-hidden='true'></i></a>";
        $facebook_html.="<img width='40' height='40' src='".base_url()."assets/images/share new.png'></a>";
        $f_post_id = explode('_',$value['id']);
        $facebook_html.="<div class='facebook_share_href_data_div' style='display: none;'>";
        $facebook_html.="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/".$arrPermission['fbpage_name']."/posts/".$f_post_id['1']."</div>";
        $facebook_html.="<div class='twitter_share_href_data_div' style='display: none;'>";
        $facebook_html.="http://twitter.com/home?status=https://www.facebook.com/".$arrPermission['fbpage_name']."/posts/".$f_post_id['1']."</div>";
        $facebook_html.="<div class='linkdin_share_href_data_div' style='display: none;'>";
        $facebook_html.="https://www.linkedin.com/shareArticle?mini=true&url=https://www.facebook.com/".$arrPermission['fbpage_name']."/posts/".$f_post_id['1']."</div></div>";
        $facebook_html.="<div class='show_all_comment_popup_btn col-md-3 col-sm-3 col-xs-3 pull-right'>";
        $getcommfun="getcomment_popup(".$value['id'].");";
        $facebook_html.='<a href="javascript:void(0);" onclick="'.$getcommfun.'">';
        //$facebook_html.="<i class='fa fa-eye' aria-hidden='true'></i></a></div>";
        $facebook_html.="<img width='40' height='40' src='".base_url()."assets/images/view new.png'></a></div>";
        $facebook_html.="</div>";
        $facebook_html.="<div class='comment_panel clearfix' id='slidepanel".$facebookalldivid."' style='display:none;'>";
        $facebook_html.="<div class='comment_data comment_content_".$value['id']."' style='display: none;'>";
        if (!empty($value['comments']['data']))
        {
          $i = 0;
          $flag = false;
          foreach ($value['comments']['data'] as $ckey => $cval) 
          {  
            $facebook_html.="<div class='comment_container comment_container_".$cval['id']."'><div class='comment_message_img'>";
            $facebook_html.="<img src='http://graph.facebook.com/".$cval['from']['id']."/picture'>";
            $facebook_html.="</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['from']['name']) . "</div>
              <div class='comment_text'>" . $cval['message'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
            $facebook_html.=$this->Activity_model->get_timeago(strtotime($cval['created_time']));
            $facebook_html.="</div></div>";
            $facebook_html.="</div>";
          } 
        }
        $facebook_html.="</div>";
        $facebook_html.="</div>";
        $facebook_html.="</div>";  
      }
    }
    $res_arr['facebook_html']=$facebook_html;
    $res_arr['facebook_next_url']=!empty($facebook_feeds['paging']['next']) ? urlencode($facebook_feeds['paging']['next']) : '';
    if(!empty(trim($twitternext)))
    {
      $settings = array(
        'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
        'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
        'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
        'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
      );
      $url = 'https://api.twitter.com/1.1/search/tweets.json';
      if(trim($twitternext)=="refresh")
      {
        $getfield = '?q=#'.$arrPermision[0]['hashtag'].'&count=10';
      }
      else
      {
        $getfield = trim(urldecode($twitternext));
      }
      $requestMethod = 'GET';
      $twitter = new TwitterAPIExchange($settings);
      $tweest= $twitter->setGetfield($getfield)
                      ->buildOauth($url, $requestMethod)
                      ->performRequest();
      $tweet=json_decode($tweest,true);
      foreach ($tweet['statuses'] as $key => $value) 
      {
        $twitteralldivid=uniqid().md5(rand());
        $twitter_html.="<div class='message_container twitter_message' data-sort='".strtotime($value['created_at'])."'>";
        $twitter_html.="<div class='msg_main_body'>";
        $twitter_html.="<div class='message_img'>";
        $twitter_html.="<img src='".$value['user']['profile_image_url']."'></div>";
        $twitter_html.="<div class='msg_type_images pull-right'>";
        $twitter_html.="<img width='40' height='40' src='".base_url()."assets/images/activity_twitter.png'></div>";
        $twitter_html.="<div class='msg_fromname'>";
        $twitter_html.='<a href="#" class="tooltip_data">'.ucfirst($value['user']['screen_name']).'</a>';  
        $twitter_html.='</div>';
        $twitter_html.="<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['created_at']))."</div>";
        $twitter_html.="</div>";
        $twitter_html.="<div class='msg_message'>";
        if(!empty($value['text']))
        {
          $twitter_html.='<p class="short_msg_show">'.substr($value['text'],0,300).'</p>';
          $twitter_html.='<p class="fully_msg_show" style="display:none;">'.$value['text'].'</p>';
          if(strlen($value['text'])>300)
          {
            $twitter_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
          }
        }
        $twitter_html.="</div>";
        if(!empty($value['entities']['media'][0]['media_url']))
        {
          $twitter_html.='<div class="msg_photo">';
          $twitter_html.='<a class="colorbox_'.$twitteralldivid.'" href="'.$value['entities']['media'][0]['media_url'].'">';
          $twitter_html.="<img src='".$value['entities']['media'][0]['media_url']."'></a>";
          $twitter_html.="</div>";
        }
        $twitter_html.="<div class='like_user_totale'>";
        $twitter_html.="<span style='text-align:left;float: left;'><p class='count_like count_like_".$value['id']."' style='float:left;'>";
        $twitter_html.=$value['favorite_count'];
        $liketext=$value['favorite_count'] > 1 ? 'Likes' : 'Like';
        $twitter_html.="</p><p style='float:left;' class='like_text_".$value['id']."'>".$liketext."</p></span></div>";
        $twitter_html.="<div class='col-md-12 col-sm-12 col-xs-12 activity_feed_btn'>";
        $twitter_html.="<div class='share_post_btn col-md-3 col-sm-3 col-xs-3 pull-right'>";
        $twitter_html.='<a href="javascript:void(0);" onclick="get_share_post_popup(this);">';
        //$twitter_html.="<i class='fa fa-share-square-o' aria-hidden='true'></i></a>";
        $twitter_html.="<img width='40' height='40' src='".base_url()."assets/images/share new.png'></a>";
        $f_post_id = explode('_',$value['id']);
        $twitter_html.="<div class='facebook_share_href_data_div' style='display: none;'>";
        $twitter_html.="https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str']."</div>";
        $twitter_html.="<div class='twitter_share_href_data_div' style='display: none;'>";
        $twitter_html.="http://twitter.com/home?status=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str']."</div>";
        $twitter_html.="<div class='linkdin_share_href_data_div' style='display: none;'>";
        $twitter_html.="https://www.linkedin.com/shareArticle?mini=true&url=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str']."</div></div>";
        $twitter_html.="</div>";
        $twitter_html.="</div>";  
      }
    }
    $res_arr['twitter_html']=$twitter_html;
    $res_arr['twitter_next_results']=!empty($tweet['search_metadata']['next_results']) ? urlencode($tweet['search_metadata']['next_results']) : ''; 
    $res_arr['alerts_loadmore_show']=count($orgactivity_data) > 10 ? '1' : '0';
    if(!empty($orgactivity_data))
    {
      usort($orgactivity_data,'sortByTime');
      $limit= 10;
      $orgactivity_data=array_slice($orgactivity_data,$start,$limit);
      foreach ($orgactivity_data as $key => $value) 
      {
        $alertsdivid=uniqid().md5(rand());
        $comments=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
        $alerts_html.= "<div class='message_container alerts_message' data-sort='".strtotime($value['Time'])."'>";
        $alerts_html.= "<div class='msg_main_body'>";
        $alerts_html.= "<div class='message_img'>";
        if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo']))
        {
          $logo=base_url().'assets/user_files/'.$value['Logo'];
        }
        else
        {
          $logo=base_url().'/assets/images/anonymous.jpg';
        }
        $alerts_html.= "<img src='".$logo."'></div>";
        $alerts_html.= "<div class='msg_type_images pull-right'>";
        $alerts_html.= "<img width='40' height='40' src='".base_url()."assets/images/activity_alert.png'></div>";
        $alerts_html.= "<div class='msg_fromname'>";
        $nm=ucfirst($value['Firstname']).' '.$value['Lastname'];
        if($value['Role_id'] == '4')
        {
            $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif($value['Role_id'] == '6')
        {
            $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif ($value['Role_id'] == '7')
        {
            $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        else
        {
            $pro_url = '#';
        }
        $alerts_html.= '<a href="'.$pro_url.'" class="tooltip_data">'.$nm.'</a>';  
        $alerts_html.= '</div>';
        $alerts_html.= '<div class="msg_toname activity_msg">';
        $alerts_html.= ucfirst($value['activity']).'</div>';
        $alerts_html.= "<div class='msg_user_companyname'>";
        if(!empty($value['Title']) && !empty($value['Company_name']))
        {
          $alerts_html.= $value['Title'].' at '.$value['Company_name'];
        }
        else
        {
          $alerts_html.= "";
        }
        $alerts_html.= "</div>";
        $alerts_html.= "<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['Time']))."</div>";
        if($value['activity_no']=='4')
        {
          $alerts_html.= '<div class="activity_session_rating">';
          for ($i=1;$i<=5;$i++) 
          { 
            if($i<=$value['rating'])
            {
                 $img="star-on.png";
            }
            else
            {
                 $img="star-off.png";
            }
            $im_ms=base_url().'assets/images/'.$img;
            $alerts_html.= '<img src="'.$im_ms.'">';
          }
          $alerts_html.= "</div>";  
        }
        $alerts_html.= "</div>";
        $alerts_html.= "<div class='msg_message'>";
        if(!empty($value['agenda_id']))
        { 
          $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
          $alerts_html .= "<h5><a href=".$agenda_url." class=msg_toname activity_msg>".$value['Message']."</a></h5>";
          $alerts_html .="<div class=msg_date>".$value['agenda_time']."</div>";
        }
        else
        {
          if(!empty($value['Message']))
          {
            $alerts_html.='<p class="short_msg_show">'.substr($value['Message'],0,300).'</p>';
            $alerts_html.='<p class="fully_msg_show" style="display:none;">'.$value['Message'].'</p>';
            if(strlen($value['Message'])>300)
            {
              $alerts_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
            }
          }
        }
        $alerts_html.= "</div>";
        if(count(json_decode($value['image'],TRUE)) > 0)
        {
          $img=json_decode($value['image'],TRUE); 
          $img1=1; 
          foreach ($img as $ikey => $ivalue) 
          {
            $alerts_html.= '<div class="msg_photo">';
            $ms_in=base_url().'assets/user_files/'.$ivalue;
            $vid="colorbox_".$alertsdivid;
            $alerts_html.= '<a class="'.$vid.'" href="'.$ms_in.'">';
            if($img1=='1')
            {
              $alerts_html.= "<img src='".$ms_in."'>";
              $img1++; 
            }
            $alerts_html.= "</span></a></div>";
          } 
        }
        $alerts_html.= "<div class='like_user_totale'>";
        $alerts_html.= "<span style='text-align:left;float: left;'><p class='count_like count_like_".$value['id']."' style='float:left;'>";
        $alerts_html.= $value['like_count']=='' ? 0 : $value['like_count'];
        $liketext=$value['like_count'] > 1 ? 'Likes' : 'Like';
        $alerts_html.= "</p><p style='float:left;' class='like_text_".$value['id']."'>".$liketext."</p></span>";
        $alerts_html.= "<span style='text-align:left;'><p class='comment_count comment_count_".$value['id']."' style='float:left;'>";
        $alerts_html.= count($comments);
        $commenttext=count($comments) > 1 ? 'Comments' : 'Comment';
        $alerts_html.= "</p><p style='float:left;' class='comment_text_".$value['id']."'>".$commenttext."</p></span></div>";
        $alerts_html.= "<div class='col-md-12 col-sm-12 col-xs-12 activity_feed_btn'>";
        $alerts_html.= "<div class='like-dislike-btn col-md-3 col-sm-3 col-xs-3'>";
        $unlikeinstyle=$value['is_like']!='1' ? 'display:none;' : '';
        $alerts_html.= "<div class='like blue_like dislike_".$value['id']."' style='".$unlikeinstyle."'>";
        $unfunarg="unlike('".$value['id']."','".$value['type']."')";
        //$alerts_html.= '<a href="#" class="like_btn blue_btn" onclick="'.$unfunarg.'"><i style="font-size:40px;" class="fa fa-thumbs-o-up"></i></a></div>';
        $alerts_html.='<a href="#" class="like_btn blue_btn" onclick="'.$unfunarg.'"><img width="40" height="40" src="'.base_url().'assets/images/like new.png"></a></div>';
        $likeinstyle=$value['is_like']=='1' ? 'display:none;' : '';
        $alerts_html.= "<div class='like like_".$value['id']."' style='".$likeinstyle."'>";
        $likefunarg="like('".$value['id']."','".$value['type']."')";
        //$alerts_html.= '<a href="#" class="like_btn" onclick="'.$likefunarg.'"><i style="font-size:40px;" class="fa fa-thumbs-o-up"></i></a></div>';
        $alerts_html.= '<a href="#" class="like_btn" onclick="'.$likefunarg.'"><img width="40" height="40" src="'.base_url().'assets/images/not liked.png"></a></div>';
        $alerts_html.= "</div><div class='show_add_comment_popup_btn col-md-3 col-sm-3 col-xs-3'>";
        $commfun="showcommentbox('".$alertsdivid."');";
        $alerts_html.= '<a href="javascript: void(0);" onclick="'.$commfun.'" class="comment_btn">';
        //$alerts_html.= "<i class='flaticon-commenting' aria-hidden='true'></i></a></div>";
        $alerts_html.= "<img width='40' height='40' src='".base_url()."assets/images/comment new.png'></a></div>";
        $alerts_html.= "<div class='show_all_comment_popup_btn col-md-3 col-sm-3 col-xs-3'>";
        $getcommfun="getcomment_popup(".$value['id'].");";
        $alerts_html.= '<a href="javascript:void(0);" onclick="'.$getcommfun.'">';
        //$alerts_html.= "<i class='fa fa-eye' aria-hidden='true'></i></a></div>";
        $alerts_html.= "<img width='40' height='40' src='".base_url()."assets/images/view new.png'></a></div>";
        $alerts_html.= "<div class='share_post_btn col-md-3 col-sm-3 col-xs-3'>";
        $alerts_html.='<a href="javascript:void(0);" onclick="get_share_post_popup(this);">';
        //$alerts_html.= "<i class='fa fa-share-square-o' aria-hidden='true'></i></a>";
        $alerts_html.= "<img width='40' height='40' src='".base_url()."assets/images/share new.png'></a>";
        $alerts_html.= "<div class='twitter_share_href_data_div' style='display: none;'>";
        $alerts_html.= "http://twitter.com/home?status=“".$value['Message']."”".base_url()."activity/".$acc_name."/".$Subdomain."</div>";
        $alerts_html.= "<div class='linkdin_share_href_data_div' style='display: none;'>";
        $linkedin="https://www.linkedin.com/shareArticle?mini=true&url=".base_url()."activity/".$acc_name."/".$Subdomain."&title=".ucfirst($value['activity'])."&summary=".$value['Message']; 
        if(!empty($img[0])){
          $linkedin.="&source=".base_url()."assets/user_files/".$img[0];
        }else{
          $linkedin.="&source=LinkedIn;";
        }
        $alerts_html.= $linkedin."</div></div>";
        $alerts_html.= "</div>";
        $alerts_html.= "<div class='comment_panel clearfix' id='slidepanel".$alertsdivid."' style='display:none;'>";
        $formurl=base_url().'activity/'.$acc_name.'/'.$Subdomain.'/upload_commentimag';
        $alerts_html.= "<form method='post' name='commentform_".$alertsdivid."' id='commentform_".$alertsdivid."' enctype='multipart/form-data' action='".$formurl."'><div class='comment_message_img'>";
        if($user[0]->Logo!="")
        {
          $alerts_html.= "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
        }
        else
        {
          $alerts_html.= "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
        }
        $alerts_html.= "</div><textarea class='user_comment_input' id='comment_text_".$alertsdivid."' placeholder='Comment' name='comment'></textarea><div class='photo_view_icon'>";
        $alerts_html.= "<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $alertsdivid . "\").click();' >&nbsp;</div>";
        $commentphotofun="comment_photo('".$alertsdivid."');";
        $alerts_html.= '<input type="file" capture accept="image/*" class="comment_photoviewer" style="display: none;" name="cmphto[]" id="cmphto'.$alertsdivid.'" onchange="'.$commentphotofun.'" multiple></div>';
        $alerts_html.= "<input type='hidden' name='msg_id' value='" . $value['id'] . "'>";
        $alerts_html.= "<input type='hidden' name='msg_type' value='" . $value['type'] . "'>";
        $alerts_html.= "<ul id='cpreview".$alertsdivid."' class='cpreview clearfix'></ul>";
        $alerts_html.= "<div id='imageloadstatus' style='display:none'><img src='".base_url()."assets/images/loading.gif' alt='Uploading....'/></div>";
        $addcommentfun="addcomment('".$value['id']."','".$alertsdivid."');";
        $alerts_html.= '<input type="button" value="Comment"  class="comment_submit" onclick="'.$addcommentfun.'" />';
        $alerts_html.= "</form><div class='comment_data comment_content_".$value['id']."' style='display: none;'>";
        if (!empty($comments))
        {
          $i = 0;
          $flag = false;
          foreach ($comments as $ckey => $cval) 
          {  
            $alerts_html.= "<div class='comment_container comment_container_".$cval['comment_id']."'><div class='comment_message_img'>";
            if ($cval['Logo'] != "")
            {
              $alerts_html.= "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
            }
            else
            {
              $alerts_html.= "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
            }
            $alerts_html.= "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['name']) . "</div>
              <div class='comment_text'>" . $cval['comment'] . "</div><div class='comment_text' style='float:right;padding-left:13px;'>";
            $alerts_html.= $cval['datetime'];
            $alerts_html.="</div></div>";
            if ($cval['comment_image'] != "")
            {
              $image_comment = json_decode($cval['comment_image']);
              $alerts_html.= "<div class='msg_photo photos_feed'>";
              foreach ($image_comment as $ikey => $ivalue) {
                $alerts_html.= '<a class="colorbox_C_' . $start.$cval['comment_id'] . '" href="' . base_url() . 'assets/user_files/' . $ivalue . '">';
              if($ikey=='0'){                                                            
                $alerts_html.= "<img src='" . base_url() . "/assets/user_files/" . $ivalue . "'>";
              }
              $alerts_html.= "</a>";
              }
              $alerts_html.= "</div>";
            }
            if ($value['user_id'] == $user[0]->Id || $cval['comment_user_id'] == $user[0]->Id)
            {
              $removecommentfun="removecomment('".$cval['comment_id']."','".$value['id']."',this)";
              $alerts_html.= '<button class="comment_section_btn" type="button" title="" onclick="'.$removecommentfun.'">&nbsp;</button>';
            }
            $alerts_html.= "</div>";
          } 
        }
        $alerts_html.= "</div>";
        $alerts_html.= "</div>";
        $alerts_html.= "</div>";
      }
    }
    $res_arr['alerts_html']=$alerts_html;
    return json_encode(array_map('utf8_encode',$res_arr));
  } 

   public function get_all_activity_html_socialwall($acc_name,$Subdomain,$start,$fburl=NULL,$twitternext=NULL)
  {
     
    
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $arrPermision = $this->Activity_model->getPermission($event_templates[0]['Id']);
    $public_message_feeds = $photo_feed = $check_in_feed = $rating_feed = $user_feed = $arrSaveSession = $arrauctionbid = $arrcomments =array();
    $orgpublic_message_feed = $orgphoto_feed = $orgcheck_in_feed = $orgrating_feed = $orguser_feed = $orgarrSaveSession = $orgarrauctionbid = $orgarrcomments =array();
    $internal_html = $facebook_html = $twitter_html = $alerts_html = "";
    if($arrPermision[0]['public']==1)
    {
      $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgpublic_message_feed=$this->Activity_model->getPublicMessageFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['photo']==1)
    {
      $photo_feed = $this->Activity_model->getPhotoFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgphoto_feed = $this->Activity_model->getPhotoFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['check_in']==1)
    {
      $check_in_feed = $this->Activity_model->getCheckInFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgcheck_in_feed = $this->Activity_model->getCheckInFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['rating']==1)
    {
      $rating_feed = $this->Activity_model->getRatingFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orgrating_feed = $this->Activity_model->getRatingFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['update_profile']==1)
    {
      $user_feed = $this->Activity_model->getUserFeeds($event_templates[0]['Id'],$user[0]->Id);
      $orguser_feed = $this->Activity_model->getUserFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['show_session']==1)
    {
      $arrSaveSession = $this->Activity_model->getSaveSessionFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrSaveSession = $this->Activity_model->getSaveSessionFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['auction_bids']==1)
    {
      $arrauctionbid = $this->Activity_model->getauctionbidsFeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrauctionbid = $this->Activity_model->getauctionbidsFeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    if($arrPermision[0]['comments']==1)
    {
      $arrcomments = $this->Activity_model->getcommentfeed($event_templates[0]['Id'],$user[0]->Id);
      $orgarrcomments = $this->Activity_model->getcommentfeed_by_org($event_templates[0]['Id'],$user[0]->Id);
    }
    $activity_feeds = $this->Activity_model->getActivityFeeds($event_templates[0]['Id'],$user[0]->Id);
    $orgactivity_feeds = $this->Activity_model->getActivityFeeds_by_org($event_templates[0]['Id'],$user[0]->Id);
    $notify=$this->Activity_model->get_all_notification_by_event($event_templates[0]['Id'],$user[0]->Id);
    
    $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
    $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);
    $res_arr['internal_loadmore_show']=count($activity_data) > 10 ? '1' : '0';
    if(!function_exists('sortByTime'))
    {
      function sortByTime($a, $b)
      {
        $a = $a['Time'];
        $b = $b['Time'];
        if ($a == $b)
        {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      }
    } 

    
    if(!empty($activity_data))
    {
      usort($activity_data,'sortByTime');
      $limit= 10;
      $activity_data=array_slice($activity_data,$start,$limit);


      foreach ($activity_data as $key => $value) 
      {
        $internaldivid=uniqid().md5(rand());
        $comments=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
        $internal_html.= "<div class='card' data-sort='".strtotime($value['Time'])."'><div class='message_container internal_message' data-sort='".strtotime($value['Time'])."'>";

        if(count(json_decode($value['image'],TRUE)) > 0)
        {
          $img=json_decode($value['image'],TRUE); 
          $img1=1; 
          foreach ($img as $ikey => $ivalue) 
          {
            $internal_html.= '<div class="msg_photo">';
            $ms_in=base_url().'assets/user_files/'.$ivalue;
            $vid="colorbox_".$internaldivid;
            $internal_html.= '<a class="'.$vid.'" href="'.$ms_in.'">';
            if($img1=='1')
            {
              $internal_html.= "<img src='".$ms_in."'>";
              $img1++; 
            }
            $internal_html.= "</span></a></div>";
          } 
        }
 
        $internal_html.= "<div class='msg_message'>";
        if(!empty($value['agenda_id']))
        { 
          $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
          $internal_html .= "<h5><a href=".$agenda_url." class=msg_toname activity_msg>".$value['Message']."</a></h5>";
          $internal_html .="<div class=msg_date>".$value['agenda_time']."</div>";
        }
        else
        {
          if(!empty($value['Message']))
          {
            $internal_html.='<p class="short_msg_show">'.substr($value['Message'],0,300).'</p>';
            $internal_html.='<p class="fully_msg_show" style="display:none;">'.$value['Message'].'</p>';
            if(strlen($value['Message'])>300)
            {
              $internal_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
            }
          }
        }
        $internal_html.= "</div>";


        $internal_html.= "<div class='msg_main_body'>";
        $internal_html.= "<div class='message_img'>";
        if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){
          $logo=base_url().'assets/user_files/'.$value['Logo'];
        }
        else
        {
          $logo=base_url().'/assets/images/anonymous.jpg';
        }
        $internal_html.= "<img src='".$logo."'></div>";
        $internal_html.= "<div class='msg_type_images pull-right'>";
        $internal_html.= "<img width='40' height='40' src='".base_url()."assets/images/activity_internal_img.png'></div>";
        $internal_html.= "<div class='msg_fromname'>";
        if($value['Role_id'] == '4')
        {
            $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif($value['Role_id'] == '6')
        {
            $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        elseif ($value['Role_id'] == '7')
        {
            $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
        }
        else
        {
            $pro_url = '#';
        }
        $nm=ucfirst($value['Firstname']).' '.$value['Lastname'];
        $internal_html.= '<a href="'.$pro_url.'" class="tooltip_data">'.$nm.'</a>';  
        $internal_html.= '</div>';
        
        $internal_html.= "<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['Time']))."</div>";
        if($value['activity_no']=='4')
        {
          $internal_html.= '<div class="activity_session_rating">';
          for ($i=1;$i<=5;$i++) 
          { 
            if($i<=$value['rating'])
            {
                 $img="star-on.png";
            }
            else
            {
                 $img="star-off.png";
            }
            $im_ms=base_url().'assets/images/'.$img;
            $internal_html.= '<img src="'.$im_ms.'">';
          }
          $internal_html.= "</div>";  
        }
        $internal_html.= "</div>";
        
        
        $internal_html.= "</div>";
        $internal_html.= "</div>";

        $internal_html.= "</div>"; /* card new*/
      }
    }
    $res_arr['internal_html']=$internal_html;
 


    // if(!empty(trim($fburl)))
    // {

      // if(trim($fburl)=="refresh")
      // {
        $profile_id=$arrPermision[0]['fbpage_name'];

        $app_id = "206032402939985";
        $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
        $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");

           /* code by Jagdish */

        // $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=10");

        /* code by RR */ 

         $fb_access_token=$arrPermision[0]['fb_access_token'];
        // $profile_id="1861248627478052";

         $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?access_token=".$fb_access_token."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=10");

        

      // }

        
      // else
      // {
      //   $json_object = file_get_contents(trim(urldecode($fburl)));
      // }
      $facebook_feeds=json_decode($json_object,true);

     // echo "<pre>dd"; print_r($facebook_feeds);
     // echo "<pre>"; print_r($facebook_feeds['paging']['next']); exit();
      foreach ($facebook_feeds['data'] as $key => $value) 
      {
        $facebookalldivid=uniqid().md5(rand());
        $facebook_html.="<div class='card' mycard_id_2='".$value['id']."' data-sort='".strtotime($value['created_time'])."'><div class='message_container facebook_message' data-sort='".strtotime($value['created_time'])."'>";

        if(!empty($value['full_picture']))
        {
          $facebook_html.='<div class="msg_photo">';
          $facebook_html.='<a class="colorbox_'.$facebookalldivid.'" href="'.$value['full_picture'].'">';
          $facebook_html.="<img src='".$value['full_picture']."'></a>";
          if(array_key_exists('subattachments',$value['attachments']['data'][0]))
          {
            $medialoop=$value['attachments']['data'][0]['subattachments']['data'];
          }
          else
          {
            $medialoop=$value['attachments']['data'];
          }
          foreach ($medialoop as $ikey => $ivalue) 
          {
            if($ivalue['media']['image']['src']!=$value['full_picture'])
            {
              $vid="colorbox_".$facebookalldivid;
              $facebook_html.='<a class="'.$vid.'" href="'.$ivalue['media']['image']['src'].'">';
              $facebook_html.="</a>";
            }
          } 
          $facebook_html.="</div>";
        }

       $facebook_html.="<div class='msg_message'>";
        if(!empty($value['message']))
        {
          $facebook_html.='<p class="short_msg_show">'.substr($value['message'],0,300).'</p>';
          $facebook_html.='<p class="fully_msg_show" style="display:none;">'.$value['message'].'</p>';
          if(strlen($value['message'])>300)
          {
            // $facebook_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
          }
        }
        $facebook_html.="</div>";

        $facebook_html.="<div class='msg_main_body'>";
        $facebook_html.="<div class='message_img'>";
        $logo="https://graph.facebook.com/".$arrPermision[0]['fbpage_name']."/picture?type=large";
        $facebook_html.="<img src='".$logo."'></div>";
        $facebook_html.="<div class='msg_type_images pull-right'>";
        $facebook_html.="<img width='40' height='40' src='".base_url()."assets/images/facebook-logo.png'></div>";
        $facebook_html.="<div class='msg_fromname'>";
        $facebook_html.='<a href="#" class="tooltip_data">'.ucfirst($value['from']['name']).'</a>';  
        $facebook_html.='</div>';
        $facebook_html.="<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['created_time']))."</div>";
        $facebook_html.="</div>";
      

        $facebook_html.="</div>";  
        $facebook_html.="</div>"; /* card new */
      }
    // }
    $res_arr['facebook_html']=$facebook_html;
    $res_arr['facebook_next_url']=!empty($facebook_feeds['paging']['next']) ? urlencode($facebook_feeds['paging']['next']) : '';
    // if(!empty(trim($twitternext)))
    // {
      $settings = array(
        'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
        'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
        'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
        'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"

      );
      $url = 'https://api.twitter.com/1.1/search/tweets.json';
      // if(trim($twitternext)=="refresh")
      // {
        $getfield = '?q=#'.$arrPermision[0]['hashtag'].'&count=10';
      // }
      // else
      // {
      //   $getfield = trim(urldecode($twitternext));
      // }
      $requestMethod = 'GET';
      $twitter = new TwitterAPIExchange($settings);
      $tweest= $twitter->setGetfield($getfield)
                      ->buildOauth($url, $requestMethod)
                      ->performRequest();
      $tweet=json_decode($tweest,true);
      #123
      $ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
      $insert['data'] = $ip.'-ajax-'.$_SERVER['HTTP_REFERER'];
      $this->db->insert('tmp',$insert);
      // echo "<pre>jk"; print_r($tweet); exit();
      foreach ($tweet['statuses'] as $key => $value) 
      {
        $twitteralldivid=uniqid().md5(rand());
        $twitter_html.="<div class='card' mycard_id='".$value['id']."' data-sort='".strtotime($value['created_at'])."'><div class='message_container twitter_message' data-sort='".strtotime($value['created_at'])."'>";
 
        if(!empty($value['entities']['media'][0]['media_url']))
        {
          $twitter_html.='<div class="msg_photo">';
          $twitter_html.='<a class="colorbox_'.$twitteralldivid.'" href="'.$value['entities']['media'][0]['media_url'].'">';
          $twitter_html.="<img src='".$value['entities']['media'][0]['media_url']."'></a>";
          $twitter_html.="</div>";
        }

        $twitter_html.="<div class='msg_message'>";
        if(!empty($value['text']))
        {
          $twitter_html.='<p class="short_msg_show">'.substr($value['text'],0,300).'</p>';
          $twitter_html.='<p class="fully_msg_show" style="display:none;">'.$value['text'].'</p>';
          if(strlen($value['text'])>300)
          {
            $twitter_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
          }
        }
        $twitter_html.="</div>";

        $twitter_html.="<div class='msg_main_body'>";
        $twitter_html.="<div class='message_img'>";
        $twitter_html.="<img src='".$value['user']['profile_image_url']."'></div>";
        $twitter_html.="<div class='msg_type_images pull-right'>";
        $twitter_html.="<img width='40' height='40' src='".base_url()."assets/images/activity_twitter.png'></div>";
        $twitter_html.="<div class='msg_fromname'>";
        $twitter_html.='<a href="#" class="tooltip_data">'.ucfirst($value['user']['screen_name']).'</a>';  
        $twitter_html.='</div>';
        $twitter_html.="<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['created_at']))."</div>";
        $twitter_html.="</div>";
        
         
        $twitter_html.="</div>";
        $twitter_html.="</div>";  
        $twitter_html.="</div>";   /* card new */
      }
    // }
    $res_arr['twitter_html']=$twitter_html;
    $res_arr['twitter_next_results']=!empty($tweet['search_metadata']['next_results']) ? urlencode($tweet['search_metadata']['next_results']) : ''; 
    $res_arr['alerts_loadmore_show']=count($orgactivity_data) > 10 ? '1' : '0';
    if(!empty($orgactivity_data))
    {
      // usort($orgactivity_data,'sortByTime');
      // $limit= 10;
      // $orgactivity_data=array_slice($orgactivity_data,$start,$limit);
      // foreach ($orgactivity_data as $key => $value) 
      // {
      //   $alertsdivid=uniqid().md5(rand());
      //   $comments=$this->Activity_model->getComments($value['id'],$value['type'],$event_templates[0]['Id']);
      //   $alerts_html.= "<div class='card'><div class='message_container alerts_message' data-sort='".strtotime($value['Time'])."'>";

      //   if(count(json_decode($value['image'],TRUE)) > 0)
      //   {
      //     $img=json_decode($value['image'],TRUE); 
      //     $img1=1; 
      //     foreach ($img as $ikey => $ivalue) 
      //     {
      //       $alerts_html.= '<div class="msg_photo">';
      //       $ms_in=base_url().'assets/user_files/'.$ivalue;
      //       $vid="colorbox_".$alertsdivid;
      //       $alerts_html.= '<a class="'.$vid.'" href="'.$ms_in.'">';
      //       if($img1=='1')
      //       {
      //         $alerts_html.= "<img src='".$ms_in."'>";
      //         $img1++; 
      //       }
      //       $alerts_html.= "</span></a></div>";
      //     } 
      //   }
 

      //   $alerts_html.= "<div class='msg_message'>";
      //   if(!empty($value['agenda_id']))
      //   { 
      //     $agenda_url = base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/View_agenda/'.$value['agenda_id'];  
      //     $alerts_html .= "<h5><a href=".$agenda_url." class=msg_toname activity_msg>".$value['Message']."</a></h5>";
      //     $alerts_html .="<div class=msg_date>".$value['agenda_time']."</div>";
      //   }
      //   else
      //   {
      //     if(!empty($value['Message']))
      //     {
      //       $alerts_html.='<p class="short_msg_show">'.substr($value['Message'],0,300).'</p>';
      //       $alerts_html.='<p class="fully_msg_show" style="display:none;">'.$value['Message'].'</p>';
      //       if(strlen($value['Message'])>300)
      //       {
      //         $alerts_html.='<a href="javascript:void(0);" onclick="readmore(this);">Read More</a>';
      //       }
      //     }
      //   }
      //   $alerts_html.= "</div>";


      //   $alerts_html.= "<div class='msg_main_body'>";
      //   $alerts_html.= "<div class='message_img'>";
      //   if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo']))
      //   {
      //     $logo=base_url().'assets/user_files/'.$value['Logo'];
      //   }
      //   else
      //   {
      //     $logo=base_url().'/assets/images/anonymous.jpg';
      //   }
      //   $alerts_html.= "<img src='".$logo."'></div>";
      //   $alerts_html.= "<div class='msg_type_images pull-right'>";
      //   $alerts_html.= "<img width='40' height='40' src='".base_url()."assets/images/activity_alert.png'></div>";
      //   $alerts_html.= "<div class='msg_fromname'>";
      //   $nm=ucfirst($value['Firstname']).' '.$value['Lastname'];
      //   if($value['Role_id'] == '4')
      //   {
      //       $pro_url = base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
      //   }
      //   elseif($value['Role_id'] == '6')
      //   {
      //       $pro_url = base_url().'Exhibitors/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
      //   }
      //   elseif ($value['Role_id'] == '7')
      //   {
      //       $pro_url = base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/View/'.$value['user_id'];
      //   }
      //   else
      //   {
      //       $pro_url = '#';
      //   }
      //   $alerts_html.= '<a href="'.$pro_url.'" class="tooltip_data">'.$nm.'</a>';  
      //   $alerts_html.= '</div>';
      
    
      //   $alerts_html.= "<div class='msg_date'>".$this->Activity_model->get_timeago(strtotime($value['Time']))."</div>";
      //   if($value['activity_no']=='4')
      //   {
      //     $alerts_html.= '<div class="activity_session_rating">';
      //     for ($i=1;$i<=5;$i++) 
      //     { 
      //       if($i<=$value['rating'])
      //       {
      //            $img="star-on.png";
      //       }
      //       else
      //       {
      //            $img="star-off.png";
      //       }
      //       $im_ms=base_url().'assets/images/'.$img;
      //       $alerts_html.= '<img src="'.$im_ms.'">';
      //     }
      //     $alerts_html.= "</div>";  
      //   }
      //   $alerts_html.= "</div>";
        
        
      //   $alerts_html.= "</div>";
       
      //   $alerts_html.= "</div>";

      //   $alerts_html.= "</div>"; /* card new */
      // }
    }
    //$res_arr['alerts_html']=$alerts_html;
    return json_encode(array_map('utf8_encode',$res_arr));
  }      
}