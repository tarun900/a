<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Notes_admin extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Notes';
          $this->data['smalltitle'] = 'Switch on Notes to allow your Users to make Notes in the App.';
          $this->data['breadcrumb'] = 'Notes';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('6',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('6',$menu_list))
          {
               $this->load->model('Notes_admin_model');
               $this->load->model('Event_template_model');
               $this->load->model('Setting_model');
               $this->load->model('Agenda_model');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;


          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $orid = $this->data['user']->Id;

          $total_permission = $this->Agenda_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;

          $notes = $this->Notes_admin_model->get_notes_list($id);
          $this->data['notes'] = $notes;

          $menudata = $this->Event_model->geteventmenu($id, 6);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;


          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;


          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'notes_admin/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'notes_admin/js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = '0')
     {

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
  
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $notes = $this->Notes_admin_model->get_notes($id);
          $this->data['notes'] = $notes;

          if ($id == NULL || $id == '')
          {
               redirect('Notes_admin');
          }
          if ($this->input->post())
          {

               $data['notes_array']['User_id'] = $logged_in_user_id;
               $data['notes_array']['Organisor_id'] = $logged_in_user_id;
               $data['notes_array']['Event_id'] = $Event_id;
               $data['notes_array']['Heading'] = $this->input->post('Heading');
               $data['notes_array']['Description'] = $this->input->post('Description');
               $this->Notes_admin_model->update_notes($data);
               $this->session->set_flashdata('notes_data', 'Updated');
               redirect("Notes_admin/index/" . $id);
          }

          $this->session->set_userdata($data);
          $this->template->write_view('css', 'admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'notes_admin/edit', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'admin/add_js', $this->data, true);
          $this->template->render();
     }

     public function add($id)
     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Id;


         
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          $orid = $this->data['user']->Id;

          $total_permission = $this->Agenda_model->get_permission_list();
          $this->data['total_permission'] = $total_permission;
   
          if ($this->input->post())
          {
               $data['notes_array']['User_id'] = $logged_in_user_id;
               $data['notes_array']['Organisor_id'] = $logged_in_user_id;
               $data['notes_array']['Event_id'] = $Event_id;
               $data['notes_array']['Heading'] = $this->input->post('Heading');
               $data['notes_array']['Description'] = $this->input->post('Description');
               $data['notes_array']['Created_at'] = $this->input->post('Created_at');
               $notes_id = $this->Notes_admin_model->add_notes($data);
               $this->session->set_flashdata('notes_data', 'Added');
               redirect("Notes_admin/index/" . $id);
          }

          $this->template->write_view('css', 'admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'notes_admin/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'admin/add_js', $this->data, true);
          $this->template->render();
     }

     public function delete($Event_id, $id)
     {

          if ($this->data['user']->Role_name == 'Client')
          {

               $notes_list = $this->Notes_admin_model->get_notes_list($id);
               $this->data['notes_list'] = $notes_list[0];
          }

          $notes = $this->Notes_admin_model->delete_notes($id);
          $this->session->set_flashdata('notes_data', 'Deleted');
          redirect("Notes_admin/index/" . $Event_id);
     }

}
