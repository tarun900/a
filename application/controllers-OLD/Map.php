<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Map extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Maps & Floorplans';
        $this->data['smalltitle'] = 'Include Google Map links of your locations and floor plans.'; //'Include Google Map links of your locations.';
        $this->data['breadcrumb'] = 'Maps & Floorplans';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('User_model');
        $this->load->model('Event_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);

         $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('10', $module))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Maps & Floorplans")
            {
                $title = "Maps";
            }
            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('10', $menu_list))
        {
            $this->load->model('Map_model');
            $this->load->model('Agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Profile_model');
            $this->load->model('Event_template_model');
            $this->load->library('upload');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
    }
    public function delete_coords($eid, $mid, $reg_id)
    {
        $this->Map_model->delete_region($reg_id);
        $this->session->set_flashdata('map_region_data', ' Deleted');
        redirect(base_url() . 'Map/set_mapping/' . $eid . '/' . $mid);
    }
    public function index($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $menudata = $this->Event_model->geteventmenu($id, 10);
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 10);
        $this->data['module_group_list'] = $module_group_list;
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $orid = $this->data['user']->Id;
        $user = $this->session->userdata('current_user');
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $map_list = $this->Map_model->get_maps($id);
        $this->data['map_list'] = $map_list;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'map/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('css', 'admin/css', true);
            // $this->template->write_view('header', 'common/header', true);
            $this->template->write_view('content', 'map/index', true);
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            $this->template->write_view('js', 'admin/js', true);
            $this->template->render();
        }
    }
    public function add($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($id == NULL || $id == '')
        {
            redirect("Map/index/" . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            if (!empty($_FILES['images']['name']))
            {
                $imgname = explode('.', $_FILES['images']['name']);
                // $tempname = preg_replace('/\s+/', '_', $imgname);
                $tempname = str_replace(array(
                    ' ',
                    '%',
                    '+',
                    '&',
                    '-'
                ) , array(
                    '_',
                    '_',
                    '_',
                    '_',
                    '_'
                ) , $imgname);
                // $tempname = str_replace(" ", "_", $imgname);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $images_file_final = $images_file;
                $this->upload->initialize(array(
                    "file_name" => $images_file_final,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '0',
                    "max_width" => '0',
                    "max_height" => '0'
                ));
                if (!$this->upload->do_upload("images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', $error['error']);
                    redirect("Map/add/" . $Event_id);
                }
            }
            if ($this->input->post('satellite_view') == 'yes')
            {
                $data['map_array']['satellite_view'] = '1';
            }
            else
            {
                $data['map_array']['satellite_view'] = '0';
            }
            if ($this->input->post('include-map') == 'yes')
            {
                $lat_long = str_replace("(", "", $this->input->post('lat_long'));
                $lat_long = str_replace(")", "", $lat_long);
                $lat_long = str_replace(" ", "", $lat_long);
                if ($this->input->post('lat_long') == '')
                {
                    $data['map_array']['lat_long'] = '51.5000,0.1167';
                }
                else
                {
                    $data['map_array']['lat_long'] = $lat_long;
                }
                $data['map_array']['place'] = $this->input->post('place');
                $data['map_array']['zoom_level'] = $this->input->post('zoom_level');
                $data['map_array']['include_map'] = '1';
            }
            else
            {
                $data['map_array']['include_map'] = '0';
            }
            $data['map_array']['Map_title'] = $this->input->post('Map_title');
            $data['map_array']['Map_desc'] = $this->input->post('Map_desc');
            $data['map_array']['Images'] = $images_file;
            $data['map_array']['Organizer_id'] = $logged_in_user_id;
            $data['map_array']['Event_id'] = $Event_id;
            $data['map_array']['area'] = $this->input->post('area');
            $map_id = $this->Map_model->add_map($data);
            $this->session->set_flashdata('map_data', 'Added');
            redirect("Map/index/" . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'map/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function set_mapping($id, $map_id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($map_id == NULL || $map_id == '')
        {
            redirect("Map/index/" . $id);
        }
        $this->data['event_id'] = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $agenda_list = $this->Agenda_model->get_all_agenda_in_map($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->data['location_list'] = $this->Map_model->get_all_meeting_location_list($id);
        $map_data = $this->Map_model->get_map_list($id, $map_id);
        $exhibitor = $this->User_model->get_exhib_list_byevent($id);
        $this->data['exhibitor'] = $exhibitor;
        $this->data['map_id'] = $map_id;
        $this->data['map_data'] = $map_data;
        $this->data['image_mapping'] = $this->Map_model->get_map_image_data_list($map_id);
        if ($this->input->post())
        {
            $event_id = $this->Map_model->update_map($data, $map_id);
            $this->session->set_flashdata('map_data', 'Added');
            redirect("Map/index/" . $id);
        }
        $this->template->write_view('css', 'map/css', $this->data, true);
        $this->template->write_view('content', 'map/image_mapping', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->render();
    }
    public function save_coords($eventid, $map_id)
    {
        $user_id = $this->input->post('name');
        $user_details = $this->User_model->get_user_details($user_id);
        $fname = $user_details[0]['Firstname'];
        $data['map_id'] = $map_id;
        $data['coords'] = $this->input->post('coor');
        $data['user_id'] = $user_id;
        if ($this->input->post())
        {
            $id = $this->Map_model->saveCoords($data);
            echo '<area title="' . $fname . '" alt="Mapping"  id="are" shape="rect"  onclick="delete1(' . $id . ')"  href="javascript:void(0);"                          
            coords="' . $data['coords'] . '">';
        }
    }
    public function save_coords_with_session($event_id, $map_id)
    {
        $data['map_id'] = $map_id;
        $data['coords'] = $this->input->post('coor');
        $data['session_id'] = $this->input->post('session_id');
        if ($this->input->post())
        {
            $id = $this->Map_model->saveCoords($data);
            echo '<area title="' . $fname . '" alt="Mapping"  id="are" shape="rect"  onclick="delete1(' . $id . ')"  href="javascript:void(0);"                          
          coords="' . $data['coords'] . '">';
        }
    }
    public function save_coords_with_locations($event_id, $map_id)
    {
        $data['map_id'] = $map_id;
        $data['coords'] = $this->input->post('coor');
        $data['location_id'] = $this->input->post('location_id');
        if ($this->input->post())
        {
            $id = $this->Map_model->saveCoords($data);
            echo '<area title="' . $fname . '" alt="Mapping"  id="are" shape="rect"  onclick="delete1(' . $id . ')"  href="javascript:void(0);"                          
          coords="' . $data['coords'] . '">';
        }
    }
    public function edit($id, $map_id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($map_id == NULL || $map_id == '')
        {
            redirect("Map/index/" . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $map_data = $this->Map_model->get_map_list($id, $map_id);
        $this->data['map_data'] = $map_data;
        if ($this->input->post())
        {
            if (!empty($_FILES['images']['name']))
            {
                $imgname = explode('.', $_FILES['images']['name']);
                // $tempname = str_replace(" ", "_", $imgname);
                $tempname = str_replace(array(
                    ' ',
                    '%',
                    '+',
                    '&',
                    '-'
                ) , array(
                    '_',
                    '_',
                    '_',
                    '_',
                    '_'
                ) , $imgname);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '0',
                    "max_width" => '0',
                    "max_height" => '0'
                ));
                if (!$this->upload->do_upload("images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', $error['error']);
                    redirect("Map/edit/" . $id . "/" . $map_id);
                }
            }
            if ($this->input->post('satellite_view') == 'yes')
            {
                $data['map_array']['satellite_view'] = '1';
            }
            else
            {
                $data['map_array']['satellite_view'] = '0';
            }
            if ($this->input->post('include-map') == 'yes')
            {
                $lat_long = str_replace("(", "", $this->input->post('lat_long'));
                $lat_long = str_replace(")", "", $lat_long);
                $lat_long = str_replace(" ", "", $lat_long);
                if ($this->input->post('lat_long') == '')
                {
                    $data['map_array']['lat_long'] = '51.5000,0.1167';
                }
                else
                {
                    $data['map_array']['lat_long'] = $lat_long;
                }
                $data['map_array']['place'] = $this->input->post('place');
                $data['map_array']['zoom_level'] = $this->input->post('zoom_level');
                $data['map_array']['include_map'] = '1';
            }
            else
            {
                $data['map_array']['include_map'] = '0';
            }
            $data['map_array']['Organisor_id'] = $logged_in_user_id;
            $data['map_array']['Event_id'] = $id;
            $data['map_array']['Map_title'] = $this->input->post('Map_title');
            $data['map_array']['Map_desc'] = $this->input->post('Map_desc');
            $data['map_array']['area'] = $this->input->post('area');
            if ($images_file != "")
            {
                $data['map_array']['Images'] = $images_file;
            }
            $event_id = $this->Map_model->update_map($data, $map_id);
            $this->session->set_flashdata('map_data', 'Added');
            redirect("Map/index/" . $id);
        }
        $this->template->write_view('css', 'map/css', $this->data, true);
        $this->template->write_view('content', 'map/edit', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete($Event_id, $id)
    {
        $map = $this->Map_model->delete_map($id,$Event_id);
        $this->session->set_flashdata('map_data', 'Deleted');
        redirect("Map/index/" . $Event_id);
    }
    public function checktitle($eid, $id = null)
    {
        if ($this->input->post())
        {
            $map_title = $this->Map_model->checktitle($this->input->post('Map_title') , $this->input->post('idval') , $eid, $id);
            if ($map_title)
            {
                echo "error###Title already exist. Please choose another title.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }
    public function change_dwg_files_status($eid, $mid)
    {
        echo $this->Map_model->change_dwg_files_status_by_map($mid);
        die;
    }
    public function add_group($id)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $map_list = $this->Map_model->get_maps($id);
        $this->data['map_list'] = $map_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 10;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, NULL);
            redirect(base_url() . 'Map/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'map/add_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_group($id, $mgid)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_group_data($mgid,$id,10);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $map_list = $this->Map_model->get_maps($id);
        $this->data['map_list'] = $map_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 10;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, $mgid);
            redirect(base_url() . 'Map/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'map/edit_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_group($event_id, $group_id)
    {
        $this->Event_template_model->delete_group($group_id,$event_id);
        $this->session->set_flashdata('map_data', 'Group Deleted');
        redirect(base_url() . 'Map/index/' . $event_id);
    }
}
