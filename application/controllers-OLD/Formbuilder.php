<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class FormBuilder extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Form Builder';
          $this->data['smalltitle'] = 'Include feedback forms anywhere in the app.';//'Include Forms';
          $this->data['breadcrumb'] = 'Form Builder';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);

          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Event_template_model');
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }
          
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('26',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
              
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());

               $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          //echo $cnt;
          if ($cnt == 1 && in_array('26',$menu_list))
          {
               $this->load->model('Formbuilder_model');
               $this->load->model('Setting_model');
               $this->load->model('Speaker_model');
               $this->load->model('Profile_model');
               $this->load->model('Agenda_model');
               $this->load->model('Cms_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
              echo '<script>window.location.href="'.base_url().'forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {
        if ($this->data['user']->Role_name == 'User')
        {
           $total_permission = $this->Formbuilder_model->get_permission_list();
           $this->data['total_permission'] = $total_permission;
        }
        
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];

        $form_list = $this->Formbuilder_model->get_evnt_form_list($id);
        $this->data['form_list'] = $form_list;

        $form_datalist=$this->Formbuilder_model->get_event_from_data($id);
        $this->data['formdata']=$form_datalist;

        $menudata = $this->Event_model->geteventmenu($id, 26);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

        $this->template->write_view('css', 'formbuilder/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'formbuilder/index', $this->data, flase);

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;

        if ($this->data['user']->Role_name == 'User')
        {
             $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
             $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }

        $this->template->write_view('js', 'formbuilder/js', true);
        $this->template->render();
     }
     
     public function formcsv($intEventId,$intFormId)
     {
         $arrData = $this->Formbuilder_model->getFormData($intEventId,$intFormId);
         if(!empty($arrData))
         {
             $arrTemp       =   json_decode($arrData[0]['json_submit_data'],TRUE);
             $intColCnt     =   count($arrTemp);
             $output = '"User",';
             foreach($arrTemp as $intKey=>$strValue)
             {
                if($intKey==1)
                {
                    $output .= '"User",';   
                }
                $output .= '"'.$intKey.'",';
             }
             
             $output .="\n";
             

             foreach($arrData as $intKey=>$strValue)
             {
                 $arrTempJson   =  json_decode($strValue['json_submit_data'],TRUE);
                 $output .='"'.$strValue['tuname'].'",';
                 foreach($arrTempJson as $intK=>$strV)
                 {
                    if(is_array($strV))
                    {
                        $output .='"'.@implode(',',$strV).'",';
                    }
                    else
                    {
                        $output .='"'.$strV.'",';
                    }
                    
                 }
                 $output .="\n";
             }
             $filename = "Forms.csv";
            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename='.$filename);
            echo $output;
            exit;
         }
         else
         {
             $output ="";
             $output .= '"No Data Available for this form",';
             $output .="\n";
             $filename = "Forms.csv";
            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename='.$filename);
            echo $output;
            exit;
         }
         
     }

     public function add_form($intEventId)
     {

          $user = $this->session->userdata('current_user');
          $Organisor_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
          $this->data['cms_list'] = $cms_list;

          $event_templates = $this->Event_model->get_event_template_by_id_list($intEventId);
          $this->data['event_templates'] = $event_templates;
        
          
          $event = $this->Event_model->get_admin_event($intEventId);
          $this->data['event'] = $event[0];


          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
          $this->data['users_role']=$user_role;


          if ($this->input->post())
          {
               $arrInsert = array();
               $arrInsert['event_id'] = $intEventId;
               $arrInsert['json_data'] = $_POST['formData'];
               $advertising_id = $this->Formbuilder_model->add_form($arrInsert);
               $this->session->set_flashdata('formbuild_data', 'Added');
               redirect("Formbuilder/index/" . $intEventId);
          }

          $this->template->write_view('css', 'formbuilder/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'formbuilder/add', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'formbuilder/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit_form($intEventId = null, $intFormId = null)
     {
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
          $this->data['cms_list'] = $cms_list;
          

          $event = $this->Event_model->get_admin_event($intEventId);
          $this->data['event'] = $event[0];

          $arrForm = $this->Formbuilder_model->get_form_list($intEventId,$intFormId);
          $this->data['arrForm'] = $arrForm;

          if ($this->data['page_edit_title'] == 'edit')
          {
               if ($intEventId == NULL || $intEventId == '')
               {
                    redirect('Event');
               }
               if ($this->input->post())
               {
                    $arrUpdate = array();
                    $arrUpdate['event_id'] = $intEventId;
                    $arrUpdate['id']       = $intFormId;
                    $arrUpdate['json_data'] = $_POST['formData'];
                    $advertising_id = $this->Formbuilder_model->update_form($arrUpdate);
                    $this->session->set_flashdata('formbuild_data','Updated');
                    redirect("Formbuilder/index/" . $intEventId);
               }

               //$this->session->set_userdata($data);
               $this->template->write_view('css', 'formbuilder/add_css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'formbuilder/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->advertising_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'formbuilder/edit_js', $this->data, true);
               $this->template->render();
          }
     }
     
     public function getJsonData($intEventId = null, $intFormId = null)
     {
         $arrForm = $this->Formbuilder_model->get_form_list($intEventId,$intFormId);
         echo $arrForm[0]['json_data'];
     }

     public function delete_form($intEventId,$intFormId)
     {

          $advertising = $this->Formbuilder_model->delete_form($intFormId,$intEventId);
          $this->session->set_flashdata('formbuilder_data', 'Deleted');
          redirect("Formbuilder/index/".$intEventId);
     }

     public function checkmenusection()
     {
          if ($this->input->post())
          {
               $menu = $this->advertising_model->checkmenusection($this->input->post('Menu_id'), $this->input->post('idval'));

               if (empty($menu))
               {
                    echo "error###Menu already exist. Please choose another menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }

     public function checkcmssection()
     {
          if ($this->input->post())
          {
               $cms = $this->advertising_model->checkcmssection($this->input->post('Cms_id'), $this->input->post('idval'));
               if (empty($cms))
               {
                    echo "error###Cms already exist. Please choose another cms menu.";
               }
               else
               {
                    echo "success###";
               }
          }
          exit;
     }
     public function asssignmodule($intEventId,$intFormId)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
          $this->data['users_role']=$user_role;
          $Organisor_id = $user[0]->Id;

          $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
          $this->data['cms_list'] = $cms_list;

          $event_templates = $this->Event_model->get_event_template_by_id_list($intEventId);
          $this->data['event_templates'] = $event_templates;
          
          $arrForm = $this->Formbuilder_model->get_form_list($intEventId,$intFormId);
          $this->data['arrForm'] = $arrForm[0];
          
          $event = $this->Event_model->get_admin_event($intEventId);
          $this->data['event'] = $event[0];

          if ($this->input->post())
          {
               $strCmsId = NULL;
               $strMenuId = NULL;
               $intPosition = '1';
               $menuid = $this->input->post('Menu_id');
               $cmsid =  $this->input->post('Cms_id');
               if(!empty($menuid))
               {
                   $strMenuId = @implode(',',$this->input->post('Menu_id'));
               }
               if(!empty($cmsid))
               {
                   $strCmsId = @implode(',',$this->input->post('Cms_id'));
               }
               if($this->input->post('frm_pos')!='')
               {
                   $intPosition = $this->input->post('frm_pos');
               }
               
               $arrUpdate = array();
               $arrUpdate['event_id'] = $intEventId;
               $arrUpdate['id']       = $intFormId;
               $arrUpdate['m_id']       = $strMenuId;
               $arrUpdate['cms_id']       = $strCmsId;
               $arrUpdate['frm_posi']   = $intPosition;
               $intUpdate = $this->Formbuilder_model->update_form_module($arrUpdate);
               $this->session->set_flashdata('formbuild_data', 'Updated');
               redirect("Formbuilder/index/" . $intEventId);
          }


          $this->template->write_view('css', 'formbuilder/add_css', $this->data, true);
          $this->template->write_view('content', 'formbuilder/assignmodule', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'formbuilder/add_js', $this->data, true);
          $this->template->render();
     }

}
