<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Profile';
          $this->data['smalltitle'] = 'Manage Profile';
          $this->data['breadcrumb'] = 'Profile';
          //$this->data['page_edit_title'] = 'edit';pco
          parent::__construct($this->data);
          $this->load->model('Profile_model');
          $this->load->library('Gcm'); 
          $this->load->model('Formbuilder_model');
          $this->load->model('Agenda_model');
          $this->load->model('User_model');
          $user = $this->session->userdata('current_user');
          if($user[0]->Role_id == '4' && $this->uri->segment(3)!=$user[0]->Id && empty($this->input->post()))
               redirect('Forbidden');
     }

     public function index()
     {

          //$user = $this->session->userdata('current_user');
          redirect($_SERVER['HTTP_REFERER']);
     }
     public function edit()
     {    
          $this->data['url']=$_SERVER['HTTP_REFERER'];
          $orid = $this->data['user']->Id;
          $user = $this->session->userdata('current_user');
          
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($user[0]->Event_id);
          $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $this->data['eventid'] = $eventid;
          $arrSingupForm = $this->Formbuilder_model->get_singup_form_data_userwise($id);
          $this->data['arrSingupForm'] = $arrSingupForm[0]['json_data'];
         
          if (!empty($eventid))
          {
               $user = $this->session->userdata('current_user');
               $rolename = $user[0]->Role_name;
               $this->data['roles'] = $this->User_model->get_role_list_version2($eventid);

          }
          else
          {
               $this->data['roles'] = array();
          }

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          $id = $user[0]->Id;
          $getid = $this->data['user']->Id;
          $this->data['user_overview'] = $this->Profile_model->get_profile_list($id,$eventid);
          $this->data['social_links'] = $this->Profile_model->get_social_links($id);
          
          if (empty($this->data['user_overview']))
          {
               redirect('Forbidden');
          }

          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;

          $countrylist = $this->Profile_model->countrylist();
          $this->data['countrylist'] = $countrylist;


          $this->template->write_view('css', 'profile/css', $this->data, true);
          $this->template->write_view('content', 'profile/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('js', 'profile/js', $this->data, true);
          $this->template->render();
     }
     public function update($id , $eventid )
     {    
          $this->data['url']=$_SERVER['HTTP_REFERER'];
          $orid = $this->data['user']->Id;
          $user = $this->session->userdata('current_user');
          if($user[0]->Id != $id && empty($eventid))
          {    
               $this->load->database();
               $event_attendees = $this->db->where('Organisor_id',$user[0]->Organisor_id)->get('relation_event_user')->result_array();
               $event_attendees = array_column($event_attendees,'User_id');
               if(!in_array($id,$event_attendees))
                    redirect('Forbidden');
	          
               //redirect('forbidden');
          }
          elseif(!empty($eventid))
          {
               $this->load->database();
               $event_attendees = $this->db->where('Organisor_id',$user[0]->Organisor_id)->get('relation_event_user')->result_array();
               $event_attendees = array_column($event_attendees,'User_id');
               if(!in_array($id,$event_attendees))
                    redirect('Forbidden');
          }
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($user[0]->Event_id);
          $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $this->data['eventid'] = $eventid;
          $arrSingupForm = $this->Formbuilder_model->get_singup_form_data_userwise($id);
          $this->data['arrSingupForm'] = $arrSingupForm[0]['json_data'];
         
          if (!empty($eventid))
          {
               $user = $this->session->userdata('current_user');
               $rolename = $user[0]->Role_name;
               $this->data['roles'] = $this->User_model->get_role_list_version2($eventid);

          }
          else
          {
               $this->data['roles'] = array();
          }

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          
          if ($id != "")
          {
               $getid = $id;
          }
          else
          {
               $getid = $this->data['user']->Id;
          }
          $getid = $this->data['user']->Id;
          $this->data['user_overview'] = $this->Profile_model->get_profile_list($id,$eventid);
          $this->data['social_links'] = $this->Profile_model->get_social_links($id);
          
          if (empty($this->data['user_overview']))
          {
               redirect('Forbidden');
          }

          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;

          $countrylist = $this->Profile_model->countrylist();
          $this->data['countrylist'] = $countrylist;


          $this->template->write_view('css', 'profile/css', $this->data, true);
          $this->template->write_view('content', 'profile/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('js', 'profile/js', $this->data, true);
          $this->template->render();
     }

     public function updateclient($id)
     {
          if (isset($_POST))
          {
               $eventid=$_POST['eventid'];
               $url=$_POST['url'];
               $user = $this->session->userdata('current_user');
               $eventid=$user[0]->Event_id;
               $res=$this->Profile_model->getAllGcmID();
               $role_details=$this->Profile_model->getUserRole($id,$eventid);
               $role=$role_details[0]['Name'];
               if(empty($this->input->post('img_val')))
               {
                    if ($_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                    {
                         $imgname = explode('.', $_FILES['userfile']['name']);
                         $tempname = str_replace(
                                                 array(' ','%','+','&','-'),
                                                 array('_','_','_','_','_'),
                                                 $imgname[0]);
                         $tempname = $tempname.'_'. strtotime(date("Y-m-d H:i:s"));
                         $_FILES['userfile']['name'] = $tempname . "." . $imgname[1];
                         //$tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         ///$_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                         $_POST['Logo'] = $_FILES['userfile']['name'];
                         $this->data['user']->Logo = $_FILES['userfile']['name'];
                         $config['upload_path'] = './assets/user_files';
                         $config['allowed_types'] = 'gif|jpg|jpeg|png';
                         $config['max_size'] = '100000';
                         $config['max_width'] = '2000';
                         $config['max_height'] = '2000';
                         $this->load->library('upload', $config);
                         
                         if (!$this->upload->do_upload())
                         {
                               $error = array('error' => $this->upload->display_errors());
                               $this->session->set_flashdata('filemsg',$error['error']);
                               redirect(base_url().'Profile/update/'.$id);
                              //echo "<pre>"; print_r($error);
                              //die;
                         }
                         else
                         {
                              $data['updated_date']=date('Y-m-d H:i:s');
                              /*foreach ($res as $key => $value) 
                              {
                                   $gcmRegId = $value['gcm_id'];
                                   if($gcmRegId!='')
                                   {
                                        $message="Profile Image Updated";
                                        $extra_param = array(
                                             'title' => 'Profile Updated',  
                                             'role'=>$role,
                                             'user_id'=>$id,
                                             'image'=> $_FILES['userfile']['name']         
                                             ); 
                                        $result = $this->gcm->send_notification($gcmRegId, $message,$extra_param,$value['device']);

                                   }
                              }*/
                              $data1 = array('upload_data' => $this->upload->data());
                         }
                         
                    }
               }
               else if(!empty($this->input->post('img_val')))
               {
                    $img=$_POST['img_val'];
                   $filteredData=substr($img, strpos($img, ",")+1); 
                   $unencodedData=base64_decode($filteredData);
                   $file = strtotime(date("Y-m-d H:i:s"))."_imge_crop.png"; 
                   $filepath = "./assets/user_files/".$file; 
                   file_put_contents($filepath, $unencodedData);
                   /*foreach ($res as $key => $value) 
                    {
                              $gcmRegId = $value['gcm_id'];
                              if($gcmRegId!='')
                              {
                                   $message="Profile Image Updated";
                                   $extra_param = array(
                                        'title' => 'Profile Updated',  
                                        'role'=>$role,
                                        'user_id'=>$id,
                                        'image'=> $file        
                                        ); 
                                 
                                   $result = $this->gcm->send_notification($gcmRegId, $message,$extra_param);
                              }
                    }*/
                    $data['updated_date']=date('Y-m-d H:i:s');
               }
               $data['Email'] = $this->input->post('email');
               $data['Company_name'] = $this->input->post('Company_name');
               $data['Salutation'] = $this->input->post('Salutation');
               $data['Title'] = $this->input->post('Title1');
               $data['Firstname'] = $this->input->post('Firstname');
               $data['Lastname'] = $this->input->post('Lastname');
               //$data['Email'] = $this->input->post('email');
               $data['Mobile'] = $this->input->post('Mobile');
               $data['Speaker_desc'] = $this->input->post('Speaker_desc');
               $data['Phone_business'] = $this->input->post('Phone_business');
               $data['Isprofile'] = $this->input->post('Isprofile');
               
               if ($this->input->post('Active') != "")
               {
                    $data['Active'] = $this->input->post('Active');
               }

               if ($this->input->post('update_password') != "")
               {
                    $data['Password'] = md5($this->input->post('update_password'));
               }

               $data['Street'] = $this->input->post('Street');
               $data['Suburb'] = $this->input->post('Suburb');
               $data['Postcode'] = $this->input->post('zipcode');
               $data['State'] = $this->input->post('state');
               $data['Country'] = $this->input->post('Country');
               if ($this->input->post('Logo') != "")
               {
                    if ($this->data['user']->Id == $id)
                    {
                         $getid = '';
                         $user = $this->session->userdata('current_user');
                         $usernew['current_user'] = $user;
                         $usernew['current_user'][0]->Logo = $data1['upload_data']['file_name'];
                         $usernew['current_user'][0]->Company_name = $this->input->post('Company_name');
                         $usernew['current_user'][0]->Firstname = $this->input->post('Firstname');
                         $usernew['current_user'][0]->Email = $this->input->post('email');
                         $usernew['current_user'][0]->Lastname = $this->input->post('Lastname');
                         $this->session->set_userdata($usernew);
                    }
                    else
                    {
                         $getid = $id;
                    }

                    $data['Logo'] = $data1['upload_data']['file_name'];
               }
               else
               {
                    if ($this->data['user']->Id == $id)
                    {
                         $getid = '';
                         $user = $this->session->userdata('current_user');
                         $usernew['current_user'] = $user;
                         if(!empty($file)){
                         $usernew['current_user'][0]->Logo = $file;}
                         $usernew['current_user'][0]->Company_name = $this->input->post('Company_name');
                         $usernew['current_user'][0]->Firstname = $this->input->post('Firstname');
                         $usernew['current_user'][0]->Email = $this->input->post('email');
                         $usernew['current_user'][0]->Lastname = $this->input->post('Lastname');
                         $this->session->set_userdata($usernew);
                    }
                    else
                    {
                         $getid = $id;
                    }
                    if(!empty($file)){
                    $data['Logo']=$file;}
               }
                $user = $this->session->userdata('current_user');

                $logged_in_user_id = $user[0]->Id;
          
                $user_soc['User_id'] = $logged_in_user_id;
                $user_soc['Website_url'] = $this->input->post('website_url');
                $user_soc['Facebook_url'] = $this->input->post('facebook_url');
                $user_soc['Linkedin_url'] = $this->input->post('linkedin_url');
                $user_soc['Twitter_url'] = $this->input->post('twitter_url');
                $data['Id'] = $id;
                $this->Profile_model->updateprofile($data,$user_soc);
                
                if($this->input->post('Role_id')!='')
                {
                    $role_data['Role_id']=$this->input->post('Role_id');
                    $this->Profile_model->updateroledata($role_data,$id,$_POST['eventid']);
                }
                
               
               //redirect('profile/update/' . $id);
               if ($this->data['user']->Role_id == '4')
               {
                    redirect('Profile/update/' . $id);
               }
               else
               {
                    redirect($url);
               }
                
          }
     }

     public function deleteprofile($id)
     {
          $data['Logo'] = NULL;
          $data['Id'] = $id;
          if ($this->data['user']->Id == $id)
          {
               $user = $this->session->userdata('current_user');
               $usernew['current_user'] = $user;
               $usernew['current_user'][0]->Logo = $this->input->post('Logo');
               $this->session->set_userdata($usernew);
          }
          $this->Profile_model->updateprofile($data);
          redirect('Profile/update/'.$id);
     }
     public function get_user_detail()
     {
          $id_user = $this->input->post('id', TRUE);
          $eventid = $this->input->post('eventid', TRUE);
          $flag = $this->input->post('flag', TRUE);
          $userdata = $this->Profile_model->get_user_detail($id_user);
          $output = null;
          $j=0;
          $roleid=array();
          foreach ($userdata as $key => $value)
          {
               if($j==0)
               {
                    $output .= "<div class='info_wrap box-effect'>";
                    if (!empty($value['Logo']))
                    {
                         $output .= "<div class='img'><img style='width:100px;height:100px;' src='" . base_url() . "assets/user_files/" . $value['Logo'] . "'></div>";
                    }
                    else
                    {
                         $output .= "<div class='img'><img style='width:100px;height:100px;' src='" . base_url() . "assets/images/anonymous.jpg'></div>";
                    }
                    $output .= "<div class='info_content'>";

                    if(!empty($value['Firstname']) || !empty($value['Lastname']))
                    {
                         $output .= "<p> <span> Name : </span> " . $value['Firstname'] . ' ' . $value['Lastname'] . " </p>";
                    }

                    if(!empty($value['Email']))
                    {
                         $output .= "<p> <span> Email : </span> " . $value['Email'] . " </p>";
                    }

                    if(!empty($value['Speaker_desc']))
                    {
                         $output .= "<p> " . $value['Speaker_desc'] . "</p>";
                    }

                    if(!empty($value['state_name']))
                    {
                         $output .= "<p> <span> State : </span> " . $value['state_name'] . " </p>";
                    }

                    if(!empty($value['country_name']))
                    {    
                         $output .= "<p> <span> Country : </span> " . $value['country_name'] . " </p>";
                    }
                    
                    if(!empty($value['Name']))
                    {
                         $output .= "<p> <span> Role Name : </span>";
                    }
                    
               }
               
               $roleid[]=$value['Role_id'];
               $rolenm.=$value['Name'].','; 
               
               if($j==count($userdata)-1)
               {    
                    $rolenm=  substr($rolenm,0,  strlen($rolenm)-1);
                    $output .=$rolenm." </p>";
                    if($flag==1)
                    {
                         
                        

                         $roles=$this->User_model->get_role_list_notcurrent($eventid);
                         $i=0;
                         foreach ($roles as $keyr=>$valuer)
                         {

                              if($i==0)
                              {
                                   $output .="<p> <span> Select Role: </span>  <select name='rolename' id='rolename' class='required' > ";
                              }

                              $output .="<option value='".$valuer['Id']."'>".$valuer['Name']."</option>";

                              if($i==count($roles)-1)
                              {
                                   $output .="</select>";
                              }

                              $i++;
                         }
                    }
                    else
                    {
                         $output.="<input type='hidden' name='rolename' value='".$value['Role_id']."' >";
                    }

                    $output .="</div>";

                    $output .= "</div>";
               }
               
               $j++;
          }
          echo $output;
     }

     public function getnewstate()
     {
          $id_country = $this->input->post('id', TRUE);
          $flag=$this->input->post('flag');

          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;

          $statedata = $this->Profile_model->getstate($id_country,$flag);

          $output = null;
          $output ="<option value=''>select......</option>";
          foreach ($statedata as $key => $value)
          {
               $output .= "<option value='" . $value['Id'] . "'>" . $value['state_name'] . "</option>";
          }

          echo $output;
     }

     public function set_session_time($id = null)
     {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Organisor_id;

        $session_time = $this->Profile_model->get_session_time($id);
        $this->data['session_time'] = $session_time;

          if($this->data['user']->Role_name == 'Client')
          {                  
               if($this->input->post())
               {
                    $data['session_array']['organizer_id'] = $logged_in_user_id;
                    $data['session_array']['session_time'] = $this->input->post('session_time');
                    $this->Profile_model->add_session_time($data);
                    redirect(base_url().'Profile/set_session_time/'.$id,'refresh');
               }

               $this->session->set_userdata($data);

               $this->template->write_view('css', 'profile/css', $this->data, true);
               $this->template->write_view('content', 'profile/session', $this->data, true);
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('js', 'profile/js', $this->data, true);
               $this->template->render();
          }



     }
     public function getlinkdinprofile($userid)
     {
         $linkedin_config = array(
               'appKey' => '75vd9jtqtfpr2c',
              'appSecret' => '9IocoHLNVgIOgsog',
             'callbackUrl' => base_url().'profile/data/'.$userid
         );
         $this->load->library('linkedin', $linkedin_config);
         $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
         $token = $this->linkedin->retrieveTokenRequest();
         $this->session->set_userdata('redirect_link',$_SERVER['HTTP_REFERER']);
         $this->session->set_userdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
         $this->session->set_userdata('oauth_request_token', $token['linkedin']['oauth_token']);
         $link = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
         redirect($link);
     }
    public function data($userid)
    {
          $user = $this->session->userdata('current_user');
          $eventid=$user[0]->Event_id;
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $acc=$this->Event_model->getAccname_event_id($eventid);
          $acc_name=$acc[0]['acc_name'];
          $subdonam=$event_templates[0]['Subdomain'];
       $linkedin_config = array(
         'appKey' => '75vd9jtqtfpr2c',
         'appSecret' => '9IocoHLNVgIOgsog',
         'callbackUrl' => base_url().'profile/data/'.$userid
       );
       $this->load->library('linkedin', $linkedin_config);
       $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
       $oauth_token = $this->session->userdata('oauth_request_token');
       $oauth_token_secret = $this->session->userdata('oauth_request_token_secret');
       $oauth_verifier = $this->input->get('oauth_verifier');
       $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
       if ($response['success'] === TRUE) 
       {
         $oauth_expires_in = $response['linkedin']['oauth_expires_in'];
         $oauth_authorization_expires_in = $response['linkedin']['oauth_authorization_expires_in'];
         $response = $this->linkedin->setTokenAccess($response['linkedin']);
         $profile = $this->linkedin->profile('~:(id,first-name,last-name,picture-url,email-address,summary,headline)');
         $profile_connections = $this->linkedin->profile('~/connections:(id,firstName,lastName,headline,email-address,summary,educations,last-modified-timestamp,interests,skills,date-of-birth,positions,picture-url,languages,projects)');
         $user = json_decode($profile['linkedin']);
         $user_array = array('linkedin_id' => $user->id, 'second_name' => $user->lastName, 'profile_picture' => $user->pictureUrl, 'first_name' => $user->firstName, 'email-address' => $user->emailAddress,'summary' => $user->summary,'headline'=>$user->headline);
         
         $userdata['Firstname']=$user_array['first_name'];
         $userdata['Lastname']=$user_array['second_name'];
         $logo=$user_array['first_name']."_linkdin_".strtotime(date("Y-m-d H:i:s")).".jpeg";
         copy($user_array['profile_picture'],"./assets/user_files/".$logo);
         $userdata['Logo']=$logo;
         $arr=explode(" at ",$user_array['headline']);
         $userdata['Title']=$arr[0];
         $userdata['Company_name']=$arr[1];
         if($user_array['summary']!="")
         {
            $userdata['Speaker_desc']=$user_array['summary'];
         }
         /*if($user_array['email-address']!="")
         {
            $userdata['Email']=$user_array['email-address'];
         }*/
         $this->Profile_model->updateprofile_linkdin($userdata,$userid);
          $user = $this->session->userdata('current_user');
          $usernew['current_user'] = $user;
          $usernew['current_user'][0]->Logo = $userdata['Logo'];
          $usernew['current_user'][0]->Company_name = $userdata['Company_name'];
          $usernew['current_user'][0]->Firstname = $userdata['Firstname'];
          $usernew['current_user'][0]->Lastname = $userdata['Lastname'];
          $usernew['current_user'][0]->Title = $userdata['Title'];
          $this->session->set_userdata($usernew);
          $company = $this->linkedin->company('1337:(id,name,ticker,description,logo-url,locations:(address,is-headquarters))');
       } 
       else
       {
            $this->session->set_flashdata('error','not get data');
       }
       if(!empty($this->session->userdata('redirect_link')))
       {
          $url=$this->session->userdata('redirect_link');
          $this->session->unset_userdata('redirect_link');
          redirect($url);
       }
       else
       {
         if($acc_name!="" && $subdonam!="")
          {
               redirect(base_url().'App/'.$acc_name.'/'.$subdonam);
          }
          else
          {
            redirect('Profile/update/' .$userid);   
          }
        }
    }
    public function front_update_profile($user_id)
    {
          $user = $this->session->userdata('current_user');
          if($this->input->post('delete_profiles_images_textbox')=='1')
          {
               $user = $this->session->userdata('current_user');
               unlink('./assets/user_files/'.$user[0]->Logo);
               $usernew['current_user'] = $user;
               $usernew['current_user'][0]->Logo = "";
               $this->session->set_userdata($usernew);
               $data['Logo'] = NULL;
               $data['Id'] = $user_id;
               $this->Profile_model->updateprofile($data);
          }
          if ($_FILES['user_profile_image_in_models']['name'] != NULL && $_FILES['user_profile_image_in_models']['name'] != '')
          {
               $imgname = explode('.', $_FILES['user_profile_image_in_models']['name']);
               $tempname = str_replace(array(' ','%','+','&','-'),array('_','_','_','_','_'),$imgname[0]);
               $tempname = $tempname.'_'. strtotime(date("Y-m-d H:i:s"));
               $_FILES['user_profile_image_in_models']['name'] = $tempname . "." . $imgname[1];
               $_POST['Logo'] = $_FILES['user_profile_image_in_models']['name'];
               $config['file_name']=$_FILES['user_profile_image_in_models']['name'];
               $config['upload_path'] = './assets/user_files';
               $config['allowed_types'] = 'gif|jpg|jpeg|png';
               $config['max_size'] = '100000';
               $config['max_width'] = '2000';
               $config['max_height'] = '2000';
               $this->load->library('upload', $config);
               if (!$this->upload->do_upload('user_profile_image_in_models')){
               }
               else
               {
                   /* $obj = new Gcm();
                    $data['updated_date']=date('Y-m-d H:i:s');
                    $gcmRegId = $user[0]->gcm_id;
                    $device = $user[0]->device;
                    if($gcmRegId!='')
                    {
                         $profile['image'] = $_FILES['user_profile_image_in_models']['name'];
                         $profile['role']    = $user[0]->Role_name; 
                         $profile['user_id']  = $user[0]->Id; 

                         if($device == "Iphone")
                         {
                              $msg =  "Profile Image Updated";
                                
                              $result = $obj->send_notification($gcmRegId,$msg,'',$device,$profile);
                              //echo $result;exit;

                         }
                         else
                         {
                         $msg['title'] = "Profile Updated";
                         $msg['message'] ="Profile Image Updated";
                         $msg['vibrate'] = 1;
                         $msg['sound'] = 1;

                         $result = $obj->send_notification($gcmRegId,$msg,'','',$profile);
                         }

                         
                    }*/
                   
                    $data1 = array('upload_data' => $this->upload->data());
               }
               
          }
          $data['Company_name'] = $this->input->post('Company_name');
          $data['Salutation'] = $this->input->post('Salutation');
          $data['Title'] = $this->input->post('Title1');
          $data['Firstname'] = $this->input->post('Firstname');
          $data['Lastname'] = $this->input->post('Lastname');
          if(!empty($data1['upload_data']['file_name']))
          {
               $data['Logo']=$data1['upload_data']['file_name'];
          }
          $this->Profile_model->update_front_user_by_id($data,$user_id);
          $user = $this->session->userdata('current_user');
          $usernew['current_user'] = $user;
          if(!empty($data1['upload_data']['file_name']))
          {
               $usernew['current_user'][0]->Logo = $data1['upload_data']['file_name'];
          }
          $usernew['current_user'][0]->Salutation=$this->input->post('Salutation');
          $usernew['current_user'][0]->Company_name = $this->input->post('Company_name');
          $usernew['current_user'][0]->Firstname = $this->input->post('Firstname');
          $usernew['current_user'][0]->Lastname = $this->input->post('Lastname');
          $usernew['current_user'][0]->Title = $this->input->post('Title1');
          $this->session->set_userdata($usernew);
          redirect($this->input->post('return_back_url'));
    }

}
