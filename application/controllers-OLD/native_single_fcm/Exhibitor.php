<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exhibitor extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->model('Native_single_fcm/Exhibitor_model');
        $this->load->model('Native_single_fcm/Message_model');
        $this->load->model('Native_single_fcm/Attendee_model');

        $this->load->library('memcached_library');

        include('application/libraries/nativeGcm.php');
        include('application/libraries/Fcm.php');
        $this->load->model('Native_single_fcm/Gamification_model');

        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    /***Old Apps Exihibitor list **/

    public function exhibitor_list()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventId($event_id,$page_no,$user_id);
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);
                if($event_id == '447')
                {
                    $data = array(
                        'exhibitor_list' => $exhibitor_list['exhibitors'],
                        'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                        'total_pages'=> $exhibitor_list['total'],
                    );
                }
                else
                {
                     $data = array(
                        'exhibitor_list' => $exhibitor_list,
                        'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    );
                }
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Exihibitor View **/

    public function exhibitor_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $exhibitor_page_id=$this->input->post('exhibitor_page_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $exhibitor_id!=''  && $exhibitor_page_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $exhibitor = $this->Exhibitor_model->getExhibitorDetails($exhibitor_page_id,$user_id,$event_id);
                $event = $this->Event_model->getEventData($event_id);
                if(!empty($exhibitor[0]['company_logo']))
                {
                    $company_logo_decode=json_decode($exhibitor[0]['company_logo']);

                    $exhibitor[0]['company_logo']=($company_logo_decode) ? $company_logo_decode[0] : "" ;
                }

                if(!empty($exhibitor[0]['Images']))
                {
                    if($exhibitor[0]['Images']=="[]")
                    {

                        $exhibitor[0]['Images']=[];
                    }
                    else
                    {
                        $image_decode=json_decode($exhibitor[0]['Images']);
                        
                        $exhibitor[0]['Images']=($image_decode) ? $image_decode : [];
                    }
                    
                }
                else
                {
                    $exhibitor[0]['Images'][]= 'http://www.allintheloop.net/assets/images/Exhibitor-Profile-Image.jpg';
                }
                
                $limit=10;
                $message = $this->Message_model->view_private_msg_coversation($user_id,$exhibitor_id,$event_id,$page_no,$limit);
                $total_pages=$this->Message_model->total_pages($user_id,$exhibitor_id,$event_id,$limit);
                $where['event_id'] = $event_id;
                $where['from_id'] = $user_id;
                $where['to_id'] = $exhibitor_id;
                $exhibitor[0]['approval_status'] = ($exhibitor_id != $user_id) ?  $this->Attendee_model->getApprovalStatus($where,$event_id) : '';
                $where1['to_id'] = $user_id;
                $exhibitor[0]['share_details'] = ($user_id == $exhibitor_id) ? $this->Attendee_model->getShareDetails($where1,$event_id) : [] ;
                $exhibitor[0]['contact_details'] = ($exhibitor[0]['approval_status'] == '1') ? $this->Attendee_model->getAttendeeConatctDetails($exhibitor_id) : [] ;
                
                $data = array(
                    'exhibitor_details' => $exhibitor[0] ? $exhibitor[0] : new stdClass,
                    'message' => $message,
                    'hide_request_meeting' => $event['hide_request_meeting'],
                    'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                    'total_pages' => $total_pages,
                );
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Message Feature**/

    public function sendMessage()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $message=$this->input->post('message');
        if($event_id!='' && $token!='' && $exhibitor_id!='' && $user_id!='' && $message!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data['Message']=$message;
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$exhibitor_id;
                $message_data['Parent']=0;
                $message_data['image']="";
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']='0';
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->Message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    $this->Message_model->add_msg_hit($user_id,$current_date,$exhibitor_id,$event_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Successfully Saved",
                        'message_id' => $message_id
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Send Message Feature**/

    public function msg_images_request()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $exhibitor_id!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data1=$this->Message_model->getMessageDetails($message_id);

                $newImageName = round(microtime(true) * 1000).".jpeg";
                $target_path= "././assets/user_files/".$newImageName; 
                $target_path1= "././assets/user_files/thumbnail/";                 
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $update_data['image'] =$newImageName;
                if($message_data1[0]['image']!='')
                {
                    $arr=json_decode($message_data1[0]['image']);
                    $arr[]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id,$update_arr);
                }
                else
                {

                    $arr[0]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id,$update_arr);
                }
                $a[0]=$newImageName;
                $message_data['Message']='';
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$exhibitor_id;
                $message_data['Parent']=$message_id;
                $message_data['image']=json_encode($a);
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']='1';
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->Message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Send Message Feature**/

    public function delete_message()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->Message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Send Message Feature**/

    public function make_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($_FILES['image']['name']!='')
                {
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    if($newImageName!='')
                    {
                        $arr[0]=$newImageName;
                        $comment_arr['image']=json_encode($arr);
                    }
                }
                $comment_arr['comment']=$comment;
                $comment_arr['user_id']=$user_id;
                $comment_arr['msg_id']=$message_id;
                $comment_arr['Time']=date("Y-m-d H:i:s");;
                $this->Message_model->make_comment($comment_arr,$event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Send Message Feature**/

    public function delete_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $comment_id=$this->input->post('comment_id');
        if($event_id!='' && $token!='' && $comment_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->Message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Send Message Feature**/

    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $exhibitor_id=$this->input->post('exhibitor_id');
        if($event_id!='' && $exhibitor_id!='')
        {
            
                $images = $this->Exhibitor_model->getImages($event_id,$exhibitor_id);
                if($images != ''){
                     $data = array(
                    'images' => json_decode($images),
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data
                    );
                }else{
                     $data = array(
                          'success' => false,
                          'message' => "No images found"
                        );
                }
               
         }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Old Apps Exhibitor searching**/

    public function exhibitorSearch()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $keywords=$this->input->post('keywords');
        if($event_id!=''  && $keywords!='')
        {
            $user_id = $this->Exhibitor_model->getUserIdByToken($token);
            $data = $this->Exhibitor_model->getSerachableRecords($event_id,$keywords,$user_id);
            $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);
            $data = array(
                'exhibitor_list' => $data,
                'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor RequestMeetingDateTime**/

    public function getRequestMeetingDateTime()
    {
        $event_id = $this->input->post('event_id');
        $this->load->model('Native_single_fcm/Attendee_model');
        if($event_id!='')
        {   
            if($event_id == '479')
            {
                $data = $this->Exhibitor_model->getDateTimeArray_for_Solar($event_id);
            }
            else
            {
                $data = $this->Attendee_model->getDateTimeArray($event_id);
            }
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor RequestMeeting**/

    public function requestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $exhibitor_id   = $this->input->post('exhibitor_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = date('H:i:s',strtotime($this->input->post('time')));

        if($event_id!='' && $exhibitor_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $data['event_id'] = $event_id;
            $data['exhibiotor_id'] = $exhibitor_id;
            $data['attendee_id'] = $attendee_id;
            
            $event_date_format = $this->db->select('date_format')->where('Id',$event_id)->get('event')->row_array();

            if($event_date_format['date_format'] == 0)
                $date = DateTime::createFromFormat('d-m-Y',$date);
            else
                $date = DateTime::createFromFormat('m-d-Y',$date);
            
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;

            $check_session = $this->Exhibitor_model->checkSessionClash($attendee_id,$date,$time);
            if($check_session['result'])
            {
                $result = $this->Exhibitor_model->saveRequest($data);
                if($result)
                {   
                    $this->add_user_game_point($attendee_id,$event_id,2);
                    $url=base_url().$this->Exhibitor_model->getUrlData($event_id);
                    $user = $this->Exhibitor_model->getUsersData($attendee_id);
                    $ex = $this->Exhibitor_model->getExUser($exhibitor_id,$event_id);
                    
                    $Message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".$user['Lastname']." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                    $message1 = ucfirst($user['Firstname'])." ".$user['Lastname']." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $ex['Id'],
                        'Event_id'=>$event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s"),
                        'ispublic' => '0',
                        'msg_creator_id'=>$attendee_id,
                        'msg_type'  => '2',
                      );
                    $this->Exhibitor_model->saveSpeakerMessage($data1);
                    $template = $this->Message_model->getNotificationTemplate($event_id,'Meeting');
                    if($ex['gcm_id']!='')
                    {
                        $extra['message_type'] = 'RequestMeeting';
                        $extra['message_id'] = $ex['Id'];
                        $extra['event'] = $this->Exhibitor_model->getEventName($event_id);
                        if($ex['device'] == 'Iphone')
                        {
                            $obj = new Fcm();
                            $extra['title'] = $template['Slug'];
                            $msg = $template['Content'];
                            $obj->send(1,$ex['gcm_id'],$msg,$extra,$ex['device']);
                        }
                        else
                        {
                            $obj = new Gcm($event_id);
                            $msg['title'] = $template['Slug'];
                            $msg['message'] = $template['Content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $obj->send_notification($ex['gcm_id'],$msg,$extra,$ex['device']);
                        }

                    }
                    if($template['send_email'] == '1')
                    {
                    $this->Event_model->sendEmailToAttendees($event_id,$template['email_content'],$user['Email'],$template['email_subject']);
                    }
                    $data = array(
                      'success' => true,
                      'message' => "Your request sent successfully",
                      'flag'    => '1',
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
            }
            else
            {
                $session = $check_session['agenda_name'];
                $data = array(
                  'success' => false,
                  'message' => "This will clash with $session - would you like to proceed?",
                  'flag'    => '0',
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }

    /***Exhibitor saveRequestMeeting**/

      public function saveRequestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $exhibitor_id   = $this->input->post('exhibitor_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = $this->input->post('time');

        if($event_id!='' && $exhibitor_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $user = $this->Exhibitor_model->getUsersData($attendee_id);
            $exibitor = $this->Exhibitor_model->getExUser($exhibitor_id,$event_id);
            $data['event_id'] = $event_id;
            $data['exhibiotor_id'] = $exhibitor_id;
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;

            $result = $this->Exhibitor_model->saveRequest($data);
            if($result)
            {

            $url=base_url().$this->Exhibitor_model->getUrlData($event_id);
                
                $Message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".$user['Lastname']." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                $message1 = ucfirst($user['Firstname'])." ".$user['Lastname']." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                $data1 = array(
                    'Message' => $Message,
                    'Sender_id' => $attendee_id,
                    'Receiver_id' => $exibitor['user_id'],
                    'Event_id'=>$event_id,
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s"),
                    'ispublic' => '0',
                    'msg_creator_id'=>$attendee_id,
                    'msg_type'  => '2',
                  );
                    $this->Exhibitor_model->saveSpeakerMessage($data1);
                    $template = $this->Message_model->getNotificationTemplate($event_id,'Meeting');
                    if($exibitor['gcm_id']!='')
                    {
                        $extra['message_type'] = 'RequestMeeting';
                        $extra['message_id'] = $exibitor['Id'];
                        $extra['event'] = $this->Exhibitor_model->getEventName($event_id);   
                        if($exibitor['device'] == 'Iphone')
                        {
                            $obj = new Fcm();
                            $extra['title'] = $template['Slug'];
                            $msg = $template['Content'];
                            $req = $obj->send(1,$exibitor['gcm_id'],$msg,$extra,$exibitor['device']);
                        }
                        else
                        {
                            $obj = new Gcm($event_id);
                            $msg['title'] = $template['Slug'];
                            $msg['message'] = $template['Content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $req = $obj->send_notification($exibitor['gcm_id'],$msg,$extra,$exibitor['device']);
                        }
                    }
                    $this->Event_model->sendEmailToAttendees($event_id,$template['Content'],$user['Email'],"Notification");
                    $data = array(
                      'success' => true,
                      'message' => "Your request sent successfully",
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }


    /***Exhibitor getAllMeetingRequest**/

    public function getAllMeetingRequest()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->Exhibitor_model->getAllMeetingRequest($event_id,$user_id);
            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor RequestMeetingResponse**/

    public function respondRequest()
    {
        $id = $this->input->post('request_id');
        $exhibitor_id = $this->input->post('exhibitor_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id'); 
        $rejection_msg = $this->input->post('rejection_msg'); 

        if($status!='' && $id!='' && $exhibitor_id!='' && $user_id!='')
        {
            $update_data['status'] = $status;
            $where['Id'] = $id;
            $result_data = $this->Exhibitor_model->updateRequest($update_data,$where);
            $exhibitor = $this->Exhibitor_model->getExhibitor($exhibitor_id);
            $request_data = $this->Exhibitor_model->getRequestData($id);
            if($status == '2')
            {
                $message=ucfirst($exhibitor['Heading'])." has denied your meeting request due to a clash. Please try another time.";
                $message .= !empty($rejection_msg) ? ' - '.$rejection_msg : '';
            }
            else
            {   
                $this->add_user_game_point($exhibitor['user_id'],$event_id,3);
                $date_f = date("d-m-Y",strtotime($request_data['date']));
                $date_f = str_replace('-', '/', $date_f);
                if($request_data['location'] !='')
                    $message=ucfirst($exhibitor['Heading'])." has accepted your meeting request at ".date("H:i",strtotime($request_data['time']))." ". $date_f." at ".$request_data['location'] ;
                else
                    $message=ucfirst($exhibitor['Heading'])." has accepted your meeting request at ".date("H:i",strtotime($request_data['time']))." ". $date_f ;
            }
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $result_data['attendee_id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id
            );
            $this->Exhibitor_model->saveSpeakerMessage($data1);
            $user = $this->Exhibitor_model->getUsersData($result_data['attendee_id']);
            if($user['gcm_id']!='')
            {
                $extra['message_type'] = 'RespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Exhibitor_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1,$user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }

            }
            $this->Event_model->sendEmailToAttendees($event_id,$message,$user['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully'  ,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor SuggestMeetingTime**/

    public function suggestMeetingTime()
    {
        $date = json_decode($this->input->post('date'),true);
        $time = json_decode($this->input->post('time'),true);
        $id = $this->input->post('request_id');
        $exhibitor_id = $this->input->post('exhibitor_id');
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 

        if($date!=''&& $time!='' && $id!='' && $exhibitor_id!='' && $user_id!='' && $event_id!='')
        {
            
            $exhibitor = $this->Exhibitor_model->getExhibitor($exhibitor_id);
            $user = $this->Exhibitor_model->getAttenee($id);
            $message=$exhibitor['Heading']." is unable to meet with you at the time you specified. ".$exhibitor['Heading']." has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";

            foreach ($date as $key => $value) {
              $temp = (strpos($time[$key], "AM")) ? str_replace(' AM', "", $time[$key]) : date("G:i", strtotime($time[$key]));
              $date=date('Y-m-d H:i',strtotime($value.' '.$temp));
              $link=base_url().$this->Exhibitor_model->getSuggestUrlData($event_id).'changemeetingdate/'.strtotime($date).'/'.$id;
              $message.="<a href='".$link."'>".$date."</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['exhibitor_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->Exhibitor_model->saveSuggestedDate($suggest);
            }
            $message1 = $exhibitor['Heading']." is unable to meet with you at this time.Please click here.";
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $user['Id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
              'msg_type'  => '4',
            );
            $this->Exhibitor_model->saveSpeakerMessage($data1);

            
            if($user['gcm_id']!='')
            {
                $extra['message_type'] = 'SuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->Exhibitor_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Suggest Request Time";
                    $msg = $message1;
                    $obj->send(1,$user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }

            }
            $tempmsg = $exhibitor['Heading']." is unable to meet with you at this time.";
            $this->Event_model->sendEmailToAttendees($event_id,$tempmsg,$user['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor getSuggestedMeetingTimings**/

    public function getSuggestedTimings()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $meeting_id = $this->input->post('meeting_id');

        if($user_id!='' && $event_id!='')
        {
            $where['attendee_id'] = $user_id;
            $where['sm.event_id'] = $event_id;
            $where['e.event_id'] = $event_id;
            if($meeting_id!='')
            $where['meeting_id'] = $meeting_id;

            $available_times = $this->Exhibitor_model->getAvailableTimes($where);
            foreach ($available_times as $key => $value) {
                $available_times[$key]['date_time'] = date("H:i l, m/d/Y",strtotime($value['date_time']));
                $available_times[$key]['api_date_time'] = date("H:i m/d/Y",strtotime($value['date_time']));
            }
            $data = array(
              'success' => true,
              'data' => $available_times,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor BookSuggestedMeetingTimings**/

    public function bookSuggestedTime()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $suggested_id = $this->input->post('suggested_id');
        $date_time = $this->input->post('date_time');

        if($user_id!='' && $event_id!='' && $suggested_id!='' && $date_time!='')
        {
            $where['attendee_id'] = $user_id;
            $where['event_id'] = $event_id;
            $where['Id'] = $suggested_id;

            $user = $this->Exhibitor_model->bookSuggestedTime($where,$date_time);
            if(!empty($user))
            {
                $attendee = $this->Exhibitor_model->getUsersData($user_id);
                if($attendee['title']!='')
                    $message1 = $attendee['Firstname'] ." ".$attendee['Lastname']." at ".$attendee['title'] ." has accepted your new suggested time of $date_time. This meeting has now been booked.";
                else
                    $message1 = $attendee['Firstname'] ." ".$attendee['Lastname']." has accepted your new suggested time of $date_time. This meeting has now been booked.";

                $data1 = array(
                  'Message' => $message1,
                  'Sender_id' => $user_id,
                  'Receiver_id' => $user['Id'],
                  'Event_id'=>$event_id,
                  'Parent' => '0',
                  'Time' => date("Y-m-d H:i:s"),
                  'ispublic' => '0',
                  'msg_creator_id'=>$user_id
                );
                $this->Exhibitor_model->saveSpeakerMessage($data1);

                if($user['gcm_id']!='')
                {
                    $extra['message_type'] = 'BookedMeeting';
                    $extra['message_id'] = $id;
                    $extra['event'] = $this->Exhibitor_model->getEventName($event_id);
                    if($user['device'] == 'Iphone')
                    {
                        $obj = new Fcm();
                        $extra['title'] = "Booked Meeting";
                        $msg = $message1;
                        $obj->send(1,$user['gcm_id'],$msg,$extra,$user['device']);
                    }
                    else
                    {
                        $obj = new Gcm($event_id);
                        $msg['title'] = "Booked Meeting";
                        $msg['message'] = $message1;
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                    }
                }
                $this->Event_model->sendEmailToAttendees($event_id,$message1,$user['Email'],"Notification");
            }
            $data = array(
              'success' => true,
              'message' => 'Your meeting booked successfully.',
            );

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor shareContactInformation**/

    public function shareContactInformation()
    {

        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $to_user_id=$this->input->post('to_user_id');
        if($event_id!='' && $user_id!='' && $to_user_id!='')
        {
            $where['event_id']         = $event_id;
            $where['from_id']          = $user_id;
            $where['to_id']            = $to_user_id;

            $share_data['event_id']         = $event_id;
            $share_data['from_id']          = $user_id;
            $share_data['to_id']            = $to_user_id;
            $share_data['approval_status']  ='0';
            $share_data['contact_type']  ='1';
            $share_data['share_contact_datetime']  =date('Y-m-d H:i:s');
            $id = $this->Attendee_model->saveShareContactInformation($where,$share_data);
            
            $this->add_user_game_point($user_id,$event_id,4);
            // dd($id);

            $url_data = $this->Attendee_model->getUrlData($event_id);
            $url=base_url().$url_data.$id;

            $user = $this->Attendee_model->getUser($user_id);

            $message="<a href='".$url."'>".ucfirst($user['Firstname'])." has shared contact details with you.</a>";
           
            $message_data['Message']=$message;
            $message_data['Sender_id']=$user_id;
            $message_data['Receiver_id']=$to_user_id;
            $message_data['Event_id']=$event_id;
            $message_data['Parent']='0';
            $message_data['Time']=date("Y-m-d H:i:s");
            $message_data['ispublic']='0';
            $message_data['msg_creator_id']=$user_id;
            $message_data['msg_type']='1';
            $this->Message_model->savePublicMessage($message_data);
            $attendee = $this->Attendee_model->getUser($to_user_id);
            $message1=ucfirst($user['Firstname'])." ".$user['Lastname']." has shared their contact details with you.";
            if($attendee['gcm_id']!='')
            {
                $message1=ucfirst($user['Firstname'])." ".$user['Lastname']." has shared their contact details with you.";
                $extra['message_type'] = 'Private';
                $extra['message_id'] = $user_id;
                $this->load->model('Native_single_fcm/Settings_model');
                $extra['event'] = $this->Settings_model->event_name($event_id);
                if($attendee['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Contact Share";
                    $msg =  $message1;
                    $result = $obj->send(1,$attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }   
                else
                {
                    $obj = new Gcm();
                    $msg['title'] = "Contact Share";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result = $obj->send_notification($attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id,$message1,$attendee['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => 'You have successfully shared your information.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Save Favorite**/

    public function saveToFavorites()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $exhibitor_page_id=$this->input->post('exhibitor_page_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $exhibitor_id!=''  && $exhibitor_page_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $favorites_data['event_id']     = $event_id;
                $favorites_data['user_id']      = $user_id;
                $favorites_data['module_id']    = $exhibitor_page_id;
                $favorites_data['module_type']  = "3";
                $this->load->model('Native_single_fcm/Favorites_model');
                $result = $this->Favorites_model->addOrRemoveFavorites($favorites_data);

                $exhibitor = $this->Exhibitor_model->getExhibitorDetails($exhibitor_page_id,$user_id,$event_id);
                $event = $this->Event_model->getEventData($event_id);
                if(!empty($exhibitor[0]['company_logo']))
                {
                    $company_logo_decode=json_decode($exhibitor[0]['company_logo']);

                    $exhibitor[0]['company_logo']=($company_logo_decode) ? $company_logo_decode[0] : "" ;
                }

                if(!empty($exhibitor[0]['Images']))
                {
                    if($exhibitor[0]['Images']=="[]")
                    {

                        $exhibitor[0]['Images']=[];
                    }
                    else
                    {
                        $image_decode=json_decode($exhibitor[0]['Images']);
                        
                        $exhibitor[0]['Images']=($image_decode) ? $image_decode : [];
                    }
                    
                }
                else
                {
                    $exhibitor[0]['Images']=[];
                }
                
                $limit=10;
                $message = $this->Message_model->view_private_msg_coversation($user_id,$exhibitor_id,$event_id,$page_no,$limit);
                $total_pages=$this->Message_model->total_pages($user_id,$exhibitor_id,$event_id,$limit);
                // #share_contact
                $where['event_id'] = $event_id;
                $where['from_id'] = $user_id;
                $where['to_id'] = $exhibitor_id;
                $exhibitor[0]['approval_status'] = ($exhibitor_id != $user_id) ?  $this->Attendee_model->getApprovalStatus($where,$event_id) : '';
                $where1['to_id'] = $user_id;
                $exhibitor[0]['share_details'] = ($user_id == $exhibitor_id) ? $this->Attendee_model->getShareDetails($where1,$event_id) : [] ;
                $exhibitor[0]['contact_details'] = ($exhibitor[0]['approval_status'] == '1') ? $this->Attendee_model->getAttendeeConatctDetails($exhibitor_id) : [] ;

                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                $exhibitor[0]['show_visited_button'] = $org_id == '311376' ? '0':'1';
                $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$exhibitor_id);
                $data = array(
                    'exhibitor_details' => $exhibitor[0] ? $exhibitor[0] : new stdClass,
                    'message' => $message,
                    'hide_request_meeting' => $event['hide_request_meeting'],
                    'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                    'total_pages' => $total_pages,
                    'unread_count' =>$unread_count[0]['unread_count'],
                    'show_visited_button' => $org_id == '311376' ? '0':'1',
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Detail**/

     public function exhibitor_view_unread_count()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $lang_id=$this->input->post('lang_id');
        $exhibitor_page_id=$this->input->post('exhibitor_page_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $exhibitor_id!=''  && $exhibitor_page_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $exhibitor = $this->Exhibitor_model->getExhibitorDetails($exhibitor_page_id,$user_id,$event_id,$lang_id);
                $event = $this->Event_model->getEventData($event_id);
                if(!empty($exhibitor[0]['company_logo']))
                {
                    $company_logo_decode=json_decode($exhibitor[0]['company_logo']);

                    $exhibitor[0]['company_logo']=($company_logo_decode) ? $company_logo_decode[0] : "" ;
                }
                $exhibitor[0]['Description'] = html_entity_decode($exhibitor[0]['Description']);
                if(!empty($exhibitor[0]['Images']))
                {   
                    if($exhibitor[0]['Images']=="[]")
                    {   
                        //if($event_id == '634' || $event_id == '1012')
                            $exhibitor[0]['Images']=['white.jpg'];
                        //else
                            //$exhibitor[0]['Images']=['exhi_def_img.jpg'];
                    }
                    else
                    {
                        $image_decode=json_decode($exhibitor[0]['Images']);
                        $image_decode = $this->compress_image($image_decode);
                        $exhibitor[0]['Images']=($image_decode) ? $image_decode : [];
                    }
                }
                else
                {
                    //if($event_id == '634' || $event_id == '1012')
                        $exhibitor[0]['Images']=['white.jpg'];
                    //else
                        //$exhibitor[0]['Images']=['exhi_def_img.jpg'];
                }
                
                $limit=10;
                $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$exhibitor_id);
               
                $where['event_id'] = $event_id;
                $where['from_id'] = $user_id;
                $where['to_id'] = $exhibitor_id;
                $exhibitor[0]['approval_status'] = ($exhibitor_id != $user_id) ?  $this->Attendee_model->getApprovalStatus($where,$event_id) : '';

                if($event_id == '1761')
                {
                    $exhibitor[0]['stand_number'] = "";
                }

                if($event_id == '1397' || $event_id == '1394')
                {
                    $exhibitor[0]['Short_desc'] = "";
                }


                if($event_id == '585' || $event_id =='978' || $event_id =='870' || $event_id =='1383' || $event_id == '1543' || $event_id == '1394' || $event_id == '1761' || $event_id == '1825' || $event_id == '1856')
                {
                    $exhibitor[0]['approval_status'] = "";
                }
                
                $this->load->model('Native_single_fcm/App_login_model');
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);

                if($org_id == '20152' || $org_id =='39756' || $org_id == '311376')
                {
                    $exhibitor[0]['approval_status'] = "";
                }


                $where1['to_id'] = $user_id;
                $exhibitor[0]['share_details'] = ($user_id == $exhibitor_id) ? $this->Attendee_model->getShareDetails($where1,$event_id) : [] ;
                $exhibitor[0]['contact_details'] = ($exhibitor[0]['approval_status'] == '1') ? $this->Attendee_model->getAttendeeConatctDetails($exhibitor_id) : [] ;
                $exhibitor[0]['linked_attendees'] = $this->Exhibitor_model->get_linked_attendee($exhibitor_page_id);
                $exhibitor[0]['Short_desc'] = str_replace(',',', ',$exhibitor[0]['Short_desc']);
                $exhibitor[0]['Short_desc'] = html_entity_decode($exhibitor[0]['Short_desc']);
                $exhibitor[0]['Description'] = html_entity_decode($exhibitor[0]['Description']);
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                $data = array(
                    'exhibitor_details' => $exhibitor[0] ? $exhibitor[0] : new stdClass,
                    'hide_request_meeting' => $event['hide_request_meeting'],
                    'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                    'unread_count' =>$unread_count[0]['unread_count'],
                    'show_visited_button' => $org_id == '311376' ? '0':'1',
                );

                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /***Exhibitor List Old apps (When Online Load) **/

    public function exhibitor_list_native()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $keyword = $this->input->post('keyword');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        $c_id=$this->input->post('category_id');
        $parent_c_id = $this->input->post('parent_c_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative($event_id,$page_no,$user_id,($where) ? $where : '');
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative($event_id,$page_no,$user_id,($where) ? $where : '');
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);
            
            foreach ($exhibitor_list['exhibitors'][0]['data'] as $key => $value) 
            {   

                if(!empty($value['company_logo']))
                {
                    $source_url = base_url()."assets/user_files/".$value['company_logo'];
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value['company_logo'];
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                    if ($info['mime'] == 'image/jpeg')
                    {   
                        $quality = 40;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {   
                        $quality = 4;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);

                    }
                    elseif ($info['mime'] == 'image/png')
                    {   
                        $quality = 4;
                        $image = imagecreatefrompng($source_url);

                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $exhibitor_list['exhibitors'][0]['data'][$key]['company_logo'] = $new_name;
                }
            }

                $data = array(
                    'exhibitor_list' => $exhibitor_list['exhibitors'],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Add Gamification Point. **/

    public function add_user_game_point($user_id,$event_id,$rank_id)
    {   
        
        $this->Gamification_model->add_user_point($user_id,$event_id,$rank_id);
        return true;
    }

    /***Exhibitor Category. **/

    public function exhibitor_category()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $parent_c_id = $this->input->post('parent_c_id');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $categories = $this->Exhibitor_model->get_ex_category($event_id,$parent_c_id);
                foreach($categories as $key => $value)
                {
                    $categories[$key]['categorie_icon'] = base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'];
                }
                $data = array(
                    'categories' => $categories,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Parent Category. **/

    public function exhibitor_parent_category()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $categories = $this->Exhibitor_model->get_ex_parent_category($event_id);
                
                foreach($categories as $key => $value)
                {
                    $categories[$key]['categorie_icon'] = base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'];
                }

                if($event_id == '539')
                {


                    function customShift($array, $id){
                        foreach($array as $key => $val)
                        {   
                            if($val['c_id'] == $id)
                            {
                                unset($array[$key]);
                                array_unshift($array, $val);
                                return $array;             
                            }
                        }
                    }

                    $categories = customShift($categories,805);
                }

                $data = array(
                    'categories' => $categories,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Compress image for Listing . **/

    public function compress_image($data)
    {   
        foreach ($data as $key => $value) 
        {   
            if(!empty($value))
            {
                $source_url = "assets/user_files/".$value;
                $info = getimagesize($source_url);
                $new_name = "new_".$value;
                $destination_url ="assets/user_files/".$new_name;

                if ($info['mime'] == 'image/jpeg')
                {   
                    $quality = 50;
                    $image = imagecreatefromjpeg($source_url);
                    imagejpeg($image, $destination_url, $quality);
                }
                elseif ($info['mime'] == 'image/gif')
                {   
                    $quality = 5;
                    $image = imagecreatefromgif($source_url);
                    imagegif($image, $destination_url, $quality);

                }
                elseif ($info['mime'] == 'image/png')
                {   
                    $quality = 5;
                    $image = imagecreatefrompng($source_url);

                    $background = imagecolorallocatealpha($image,255,0,255,127);
                    imagecolortransparent($image, $background);
                    imagealphablending($image, false);
                    imagesavealpha($image, true);
                    imagepng($image, $destination_url, $quality);
                }
                $new_data[] = $new_name;
            }
        }
        return $new_data;
    }

    /***Exhibitor Listing for Online apps . **/

    public function exhibitor_list_native_new()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $keyword = $this->input->post('keyword');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        $c_id=$this->input->post('category_id');
        $parent_c_id = $this->input->post('parent_c_id');
        $last_type = $this->input->post('last_type');
        
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative_new($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative_new($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $data = array(
                    'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor getAllMeetingRequest Latest. **/

    public function getAllMeetingRequestNew()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->Exhibitor_model->getAllMeetingRequestNew($event_id,$user_id);
            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor getAllExhibitorsList Latest. **/

    public function getAllExhibitorsList()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $keyword = $this->input->post('keyword');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        $c_id=$this->input->post('category_id');
        $parent_c_id = $this->input->post('parent_c_id');
        $last_type = $this->input->post('last_type');
        
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsList($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsList($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $data = array(
                    'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor getAllExhibitorsList with Parent Category Latest Apps. **/

    public function  getExhibitorListCategoryPcategories()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $keyword = $this->input->post('keyword');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        $c_id=$this->input->post('category_id');
        $parent_c_id = $this->input->post('parent_c_id');
        $last_type = $this->input->post('last_type');
        
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsList($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsList($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $categories = $this->Exhibitor_model->get_ex_category_with_parent($event_id,$parent_c_id);
                foreach($categories as $key => $value)
                {   
                    
                    if($event_id == '634')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/arab_health_exhibitor_category_icon.jpg';
                    }
                    else if($event_id == '1012')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/medlab_cat_icon.jpg';   
                    }
                    else
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:'';
                    }
                }

                $pcategories = $this->Exhibitor_model->get_ex_parent_category($event_id);

                foreach($pcategories as $key => $value)
                {
                    $pcategories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'] : '';
                }

                $data = array(
                    'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                    'categories' => $categories,
                    'pcategories' => $pcategories,
                    'countries' => $this->Exhibitor_model->getEventCountries($event_id),
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Not useful. **/

    public function exhibitor_list_native_load()
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        //$this->output->enable_profiler(TRUE);
        $starttime = $this->App_login_model->get_endtime_of_last_rec_50($this->input->post('event_id'));
        $diff = microtime(true) - $starttime;

        $sec = intval($diff);
        $micro = $diff - $sec;

        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));

        $mem = $this->getSystemMemInfo();

        
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $keyword = $this->input->post('keyword');
        $token=$this->input->post('_token');
        $page_no=$this->input->post('page_no');
        $c_id=$this->input->post('category_id');
        $parent_c_id = $this->input->post('parent_c_id');
        $last_type = $this->input->post('last_type');
        
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative_new($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative_new($event_id,$page_no,$user_id,($where) ? $where : '',$last_type);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $data = array(
                    'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;


        $total_time = round(($finish - $start), 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['event_id'] = $event_id;
        $insert['type'] = 'Exhi.';
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->App_login_model->add_server_data_50($insert);
    }

    /***Not useful just  for Load Testing to get info. **/

    /*public function getSystemMemInfo() 
    {       
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach ($data as $line)
        {   
            list($key, $val) = array_pad(explode(":", $line, 2), 2, null);
            $meminfo[$key] = trim($val);
        }
        return $meminfo;
    }*/

    
    /***Exhibitor Listing for offline feature apps . **/

    public function  getExhibitorListCategoryPcategoriesoffline()
    {
        //error_reporting(E_ALL);
        $event_id       = $this->input->post('event_id');
        $event_type     = $this->input->post('event_type');
        $keyword        = $this->input->post('keyword');
        $token          = $this->input->post('_token');
        $page_no        = $this->input->post('page_no');
        $c_id           = $this->input->post('category_id');
        $parent_c_id    = $this->input->post('parent_c_id');
        $last_type      = $this->input->post('last_type');
        $lang_id        = $this->input->post('lang_id');
        $user_id        = $this->input->post('user_id');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $categories = $this->Exhibitor_model->get_ex_category_with_parent($event_id,$parent_c_id);
                foreach($categories as $key => $value)
                {   
                    if($event_id == '634')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/arab_health_exhibitor_category_icon.jpg';
                    }
                    else if($event_id == '1012')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/medlab_cat_icon.jpg';   
                    }
                    else
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:'';
                    }
                }

                $pcategories = $this->Exhibitor_model->get_ex_parent_category($event_id);

                foreach($pcategories as $key => $value)
                {
                    $pcategories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'] : '';
                }
                $event = $this->Event_model->getEventData($event_id);
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                foreach ($exhibitor_list['exhibitors'] as $key => $value)
                {   
                    foreach ($value['data'] as $key1 => $value1)
                    {   
                        $value1['Short_desc'] = str_replace(',',', ',$value1['Short_desc']);
                        $value1['Description'] = html_entity_decode($value1['Description']);
                        if($event_id == '1761')
                        {
                            $exhibitor_list['exhibitors'][$key]['stand_number'] = "";
                        }
                        if($event_id == '1397' || $event_id == '1394')
                        {
                            $value1['Short_desc'] = "";
                        }
                        if(!empty($value1['Images']))
                        {   
                            if($value1['Images']=="[]")
                            {   
                                //if($event_id == '634' || $event_id == '1012')
                                    $value1['Images']=['white.jpg'];
                                //else
                                    //$value1['Images']=['exhi_def_img.jpg'];
                            }
                            else
                            {
                                $image_decode=json_decode($value1['Images']);
                                $image_decode = $this->compress_image($image_decode);
                                $value1['Images']=($image_decode) ? $image_decode : [];
                            }
                        }
                        else
                        {
                            //if($event_id == '634' || $event_id == '1012')
                                $value1['Images']=['white.jpg'];
                            //else
                                //$value1['Images']=['exhi_def_img.jpg'];
                        }

                        $ex_details_data[] = array(
                            'exhibitor_details' => $value1 ? $value1 : new stdClass,
                            'hide_request_meeting' => $event['hide_request_meeting'],
                            'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                            'unread_count' =>"0",
                            'show_visited_button' => $org_id == '311376' ? '0':'1',
                        );
                    }
                }


                //exhi category group Monday 30 April 2018 11:26:43 AM IST
                $exhi_cat_group = $this->Exhibitor_model->GetExhiCategoryGroup($event_id);

                $data = array(
                    'exhibitor_list' => (array_values($exhibitor_list['exhibitors']))?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                    'categories' => $categories,
                    'pcategories' => $pcategories,
                    'countries' => $this->Exhibitor_model->getEventCountries($event_id),
                    'exhi_cat_group' => $exhi_cat_group,
                    'exhibitor_details_data' => ($ex_details_data)?:[],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Listing for offline feature apps . (We create it for our testing) **/


    public function  getExhibitorListCategoryPcategoriesofflineTest()
    {
        //error_reporting(E_ALL);
        $event_id       = $this->input->post('event_id');
        $event_type     = $this->input->post('event_type');
        $keyword        = $this->input->post('keyword');
        $token          = $this->input->post('_token');
        $page_no        = $this->input->post('page_no');
        $c_id           = $this->input->post('category_id');
        $parent_c_id    = $this->input->post('parent_c_id');
        $last_type      = $this->input->post('last_type');
        $lang_id        = $this->input->post('lang_id');
        $user_id        = $this->input->post('user_id');

        if($event_id!='' && $event_type!='')
        {
            if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
            {
                $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                foreach($c_keyword as $key => $value)
                {
                    if($key == 0)
                    {
                        $where = "(FIND_IN_SET('".$value."',Short_desc))";
                    }
                    else
                    {
                        $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                    }
                }
                if(count($c_keyword) == '1')
                {   
                    $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                }
                else
                {   
                    if(!empty($where))
                    {   
                        $where = '('.$where.')';
                        if($keyword!='')
                        $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    }
                } 
                $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
            }
            else
            {
                if($keyword!='')
                $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
            }
            $categories = $this->Exhibitor_model->get_ex_category_with_parent($event_id,$parent_c_id);
            foreach($categories as $key => $value)
            {   
                if($event_id == '634')
                {
                    $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/arab_health_exhibitor_category_icon.jpg';
                }
                else if($event_id == '1012')
                {
                    $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/medlab_cat_icon.jpg';   
                }
                else
                {
                    $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:'';
                }
            }
            
            $pcategories = $this->Exhibitor_model->get_ex_parent_category($event_id);

            foreach($pcategories as $key => $value)
            {
                $pcategories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'] : '';
            }
            $event = $this->Event_model->getEventData($event_id);
            
            foreach ($exhibitor_list['exhibitors'] as $key => $value)
            {   
                foreach ($value['data'] as $key1 => $value1)
                {   
                    $value1['Short_desc'] = str_replace(',',', ',$value1['Short_desc']);
                    $value1['Description'] = html_entity_decode($value1['Description']);
                    if(!empty($value1['Images']))
                    {   
                        if($value1['Images']=="[]")
                        {   
                            //if($event_id == '634' || $event_id == '1012')
                                $value1['Images']=['white.jpg'];
                            //else
                                //$value1['Images']=['exhi_def_img.jpg'];
                        }
                        else
                        {
                            $image_decode=json_decode($value1['Images']);
                            $image_decode = $this->compress_image($image_decode);
                            $value1['Images']=($image_decode) ? $image_decode : [];
                        }
                    }
                    else
                    {
                        //if($event_id == '634' || $event_id == '1012')
                            $value1['Images']=['white.jpg'];
                        //else
                            //$value1['Images']=['exhi_def_img.jpg'];
                    }

                    $ex_details_data[] = array(
                        'exhibitor_details' => $value1 ? $value1 : new stdClass,
                        'hide_request_meeting' => $event['hide_request_meeting'],
                        'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                        'unread_count' =>"0",
                    );
                }
            }
            
            $data = array(
                'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                'total_pages'=> $exhibitor_list['total'],
                'categories' => $categories,
                'pcategories' => $pcategories,
                'countries' => $this->Exhibitor_model->getEventCountries($event_id),
                'exhibitor_details_data' => ($ex_details_data)?:[],
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Exhibitor Meeting Request with available dates **/
 public function getAllMeetingRequestWithDate() //Thursday 15 March 2018 11:26:41 AM IST
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->Exhibitor_model->getAllMeetingRequestWithDate($event_id,$user_id);
            $event = $this->Exhibitor_model->checkEventDateFormat($event_id);
            $map_meeting = $this->Exhibitor_model->getMapMeetingLocation($event_id);
            
            $start = date('Y-m-d',strtotime($event[0]['Start_date']));
            while($start <= $event[0]['End_date'])
            {   
                $data[]['date'] = date('Y-m-d',strtotime($start));
                $start = date('Y-m-d',strtotime($start.'+1 day'));
            }
            foreach ($data as $key => $value)
            {   
                if(empty($value['request_id']))
                {
                    unset($data[$key]);
                }
                else
                {
                    if($event[0]['date_format'] == 0)
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("d/m/Y",$a);
                    }
                    else
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("m/d/Y",$a);
                    }
                    if(!empty($value['request_id']))
                    {
                        $data[$key]['date'] = date("Y-m-d",strtotime(str_replace('/','-',$key)));
                        $value['map_location'] = $map_meeting[$value['location']] ?: new stdClass;
                        $value['sender_id'] = '';
                        $value['receiver_id'] = '';
                    }
                    $meeting[$value['date']][] = $value;
                }
            }
            foreach($meeting as $key => $value)
            {   
                $data_new[$j]['date'] = date("Y-m-d",strtotime(str_replace('/','-',$key)));
                // $data_new[$j]['date'] = $key;
                usort($value, 'sortByTime');
                if(!empty($value[0]['request_id']))
                $data_new[$j]['data'] = $value;
                else
                $data_new[$j]['data'] = [];
                if(count($data_new[$j]['data']) != '1')
                unset ($data_new[$j]['data'][count($data_new[$j]['data'])-1]);
                $j++;
            }
            function sortByTime($a, $b)
            {
                $a =  strtotime($a['time']);
                $b =  strtotime($b['time']);
                if ($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            }
            function sortByDate($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];
                if($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            } 
            usort($data_new, 'sortByDate');

            $data = array(
              'success' => true,
              'data' => $data_new?:[]
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /****************************************************/

    /***We create this for our Testing for caching feature (Not Useful) **/

    public function  getExhibitorListCategoryPcategoriesofflineTest06042018()
    {
        // error_reporting(E_ALL);
        //$this->memcached_library->delete('abc');
        $result = $this->memcached_library->get("abc");
        if($result)
        {
            echo 'if<pre>';
            print_r($result);
            echo '<pre>';
            exit;    
        }
        else
        {
            $data = array("1","2","3");
            $test = $this->memcached_library->add("abc",$data);    
            $result = $this->memcached_library->get("abc");
            echo 'else<pre>';
            print_r($result);
            echo '<pre>';
            exit;    
        }
    }

    /***We create this for our Testing for reducing time from response of data (Not Useful) **/

    public function  getExhibitorListCategoryPcategoriesoffline_V2()
    {
        //error_reporting(E_ALL);
        $event_id       = $this->input->post('event_id');
        $event_type     = $this->input->post('event_type');
        $keyword        = $this->input->post('keyword');
        $token          = $this->input->post('_token');
        $page_no        = $this->input->post('page_no');
        $c_id           = $this->input->post('category_id');
        $parent_c_id    = $this->input->post('parent_c_id');
        $last_type      = $this->input->post('last_type');
        $lang_id        = $this->input->post('lang_id');
        $user_id        = $this->input->post('user_id');

        if($event_id!='' && $event_type!='')
        {   
            /*$file = './assets/temp_files/fhaexhi.json';
            if(file_exists($file))
            {
                $data = file_get_contents($file);
                echo $data;exit();
            }*/
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->Exhibitor_model->getUserIdByToken($token);
                if(!empty($c_id) || (empty($c_id) && !empty($parent_c_id)))
                {
                    $c_keyword = $this->Exhibitor_model->get_keyword_by_category($c_id,$parent_c_id);
                    foreach($c_keyword as $key => $value)
                    {
                        if($key == 0)
                        {
                            $where = "(FIND_IN_SET('".$value."',Short_desc))";
                        }
                        else
                        {
                            $where .= " or (FIND_IN_SET('".$value."',Short_desc))"; 
                        }
                    }
                    if(count($c_keyword) == '1')
                    {   
                        $where = "(FIND_IN_SET('".$c_keyword[0]."',Short_desc)!=0)";
                    }
                    else
                    {   
                        if(!empty($where))
                        {   
                            $where = '('.$where.')';
                            if($keyword!='')
                            $where .= " and (Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                        }
                    } 
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline_V2($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
                }
                else
                {
                    if($keyword!='')
                    $where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";
                    $exhibitor_list = $this->Exhibitor_model->getAllExhibitorsListOffline_V2($event_id,$page_no,$user_id,($where) ? $where : '',$last_type,$lang_id);
                }
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);

                $categories = $this->Exhibitor_model->get_ex_category_with_parent($event_id,$parent_c_id);
                foreach($categories as $key => $value)
                {   
                    if($event_id == '634')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/arab_health_exhibitor_category_icon.jpg';
                    }
                    else if($event_id == '1012')
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:base_url().'assets/exhibtior_category_icon/medlab_cat_icon.jpg';   
                    }
                    else
                    {
                        $categories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon']:'';
                    }
                }

                $pcategories = $this->Exhibitor_model->get_ex_parent_category($event_id);

                foreach($pcategories as $key => $value)
                {
                    $pcategories[$key]['categorie_icon'] = ($value['categorie_icon']!='') ? base_url().'assets/exhibtior_category_icon/'.$value['categorie_icon'] : '';
                }
                $event = $this->Event_model->getEventData($event_id);
                
                $data = array(
                    'exhibitor_list' => ($exhibitor_list['exhibitors'])?: [],
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'total_pages'=> $exhibitor_list['total'],
                    'categories' => $categories,
                    'pcategories' => $pcategories,
                    'countries' => $this->Exhibitor_model->getEventCountries($event_id),
                    'hide_request_meeting' => $event['hide_request_meeting'],
                    'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                    'unread_count' =>"0",
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
                /*$file = './assets/temp_files/fhaexhi.json';
                if(file_exists($file))
                {
                    $data = file_get_contents($file);
                    echo $data;exit();
                }
                else
                {   
                    $myfile = fopen($file, "w");
                    fwrite($myfile, json_encode($data));
                    fclose($myfile);
                }*/
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***We create this for our Testing for reducing time from response of data (Not Useful) **/
    
    public function exhibitor_view_unread_count_V2()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $lang_id=$this->input->post('lang_id');
        $exhibitor_page_id=$this->input->post('exhibitor_page_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $exhibitor_id!=''  && $exhibitor_page_id!='')
        {
            $exhibitor = $this->Exhibitor_model->getExhibitorDetails_V2($exhibitor_page_id,$user_id,$event_id,$lang_id);
            $event = $this->Event_model->getEventData($event_id);
            
            if(!empty($exhibitor[0]['company_logo']))
            {
                $company_logo_decode=json_decode($exhibitor[0]['company_logo']);

                $exhibitor[0]['company_logo']=($company_logo_decode) ? $company_logo_decode[0] : "" ;
            }

            if(!empty($exhibitor[0]['Images']) && $exhibitor[0]['Images'] != "[]")
            {   
                $image_decode=json_decode($exhibitor[0]['Images']);
                $image_decode = $this->compress_image($image_decode);
                $exhibitor[0]['Images']=($image_decode) ? $image_decode : [];
            }
            else
            {
                $exhibitor[0]['Images']=['white.jpg'];
            }
            
            $limit=10;
            $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$exhibitor_id);
           
            $where['event_id'] = $event_id;
            $where['from_id'] = $user_id;
            $where['to_id'] = $exhibitor_id;
            $exhibitor[0]['approval_status'] = ($exhibitor_id != $user_id) ?  $this->Attendee_model->getApprovalStatus($where,$event_id) : '';

            if($event_id == '585' || $event_id =='978' || $event['Organisor_id'] == '20152')
            {
                $exhibitor[0]['approval_status'] = "";
            }

            $where1['to_id'] = $user_id;
            $exhibitor[0]['share_details'] = ($user_id == $exhibitor_id) ? $this->Attendee_model->getShareDetails($where1,$event_id) : [] ;
            $exhibitor[0]['contact_details'] = ($exhibitor[0]['approval_status'] == '1') ? $this->Attendee_model->getAttendeeConatctDetails($exhibitor_id) : [] ;
            $exhibitor[0]['linked_attendees'] = $this->Exhibitor_model->get_linked_attendee($exhibitor_page_id);
            $exhibitor[0]['Short_desc'] = str_replace(',',', ',$exhibitor[0]['Short_desc']);
            $exhibitor[0]['Short_desc'] = html_entity_decode($exhibitor[0]['Short_desc']);
            $exhibitor[0]['Description'] = html_entity_decode($exhibitor[0]['Description']);


            $exhibitor = array_slice($exhibitor,0,1);
            $data = array(
                'exhibitor_details' => $exhibitor ? $exhibitor : new stdClass,
                'hide_request_meeting' => $event['hide_request_meeting'],
                'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                'unread_count' =>$unread_count[0]['unread_count'],
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}