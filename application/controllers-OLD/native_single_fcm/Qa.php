<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Qa extends CI_Controller     
{
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('Native_single_fcm/Qa_model');
        include('application/libraries/NativeGcm.php');
        include('application/libraries/Fcm.php');

    }

    /***Get All Sessions Lists **/

    public function getAllSession()
    {
        $event_id = $this->input->post('event_id');
        $lang_id  = $this->input->post('lang_id');
        $group_id = $this->input->post('group_id');
        $user_id   = $this->input->post('user_id');

        if($event_id!='')
        {
            $sessions = $this->Qa_model->getAllSesssions($event_id,$lang_id,$group_id,$user_id);
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
                );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Get Session Detail **/
    public function getSessionDetail()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $user_id = $this->input->post('user_id');
        $device_id = $this->input->post('device_id');
        $lang_id  = $this->input->post('lang_id');

        if($event_id!='' && $session_id!='')
        {
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id,$lang_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            if($user_id != 0)
            {
                $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator);
            }
            else
            {
                $messages = $this->Qa_model->getMessages($session_id,$device_id,$is_moderator);
            }
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Send Message **/

    public function sendMessage()
    {
        $event_id               = $this->input->post('event_id');
        $session_id             = $this->input->post('session_id');
        $user_id                = $this->input->post('user_id');
        $Moderator_speaker_id   = $this->input->post('Moderator_speaker_id');
        $message                = $this->input->post('message');
        $is_closed_qa           = $this->input->post('is_closed_qa');

        if($event_id!='' && $session_id!='' && $message!='' && $Moderator_speaker_id!='')
        {
            $insert_data['event_id']        = $event_id;
            $insert_data['Sender_id']       = ($user_id) ? $user_id : NULL;
            $insert_data['message']         = $message;
            $insert_data['qasession_id']    = $session_id;
            $insert_data['qa_approved'] = ($is_closed_qa) ? '0': '1';

            $zone = $this->Qa_model->getEventTimeZone($event_id);

            date_default_timezone_set("UTC");
            $cdate=date('Y-m-d H:i:s');
            if(!empty($zone))
            {
                if(strpos($zone,"-")==true)
                { 
                    $arr=explode("-",$zone);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                }
                if(strpos($zone,"+")==true)
                {
                    $arr=explode("+",$zone);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                }
            }

            $insert_data['send_datetime']   = $cdate;
            $this->Qa_model->saveMessage($insert_data,$Moderator_speaker_id);

            $data = array(
                  'success'     => true,
                  'message'     => ($is_closed_qa) ? "Thank you for your question. It has been sent to the moderator for review":"Your message was sent successfully",
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Vote Message **/

    public function VoteMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');
        $device_id = $this->input->post('device_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            if($user_id != 0)
            {
                $this->Qa_model->vote($message_id,$user_id);
                $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator);
            }
            else
            {
                $this->Qa_model->voteAnonymous($message_id,$device_id);
                $messages = $this->Qa_model->getMessages($session_id,$device_id,$is_moderator);   
            }
            
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Delete Message **/

    public function DeleteMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            
            $this->Qa_model->deleteMessage($message_id);
            $obj = new Gcm();
            $obj1 = new Fcm();
			$users = $this->Qa_model->getAllUsersGCM_id_by_event_id($event_id);
			$count = count($users);
			
			if($count > 100)
			{
			    $limit = 100;
			    for ($i=0;$i<$count;$i++) 
			    {
			        $page_no        = $i;

			        $start          = ($page_no)*$limit;
			        $users1         = array_slice($users,$start,$limit);
			        foreach ($users1 as $key => $value) 
			        {
			            if($value['gcm_id']!='')
			            {
			                $msg =  '';
			                $extra['message_type'] = 'QAQusetion';
			                $extra['message_id'] = '';
			                $extra['event'] = $event[0]['Event_name'];
                            $extra['title'] = '';
			                if($value['device'] == "Iphone")
			                {
			                    $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
			                }
			                else
			                {
			                    $msg['title'] = '';
			                    $msg['message'] = '';
			                    $msg['vibrate'] = 1;
			                    $msg['sound'] = 1;
			                    $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			                } 
			            }
			        }
			    }
			}
			else
			{
			    foreach ($users as $key => $value)
			    {
			        if($value['gcm_id']!='')
			        {
			            $msg = '';
			            $extra['message_type'] = 'QAQusetion';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        $extra['title'] = '';
			            if($value['device'] == "Iphone")
			            {
			                $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
			            }
			            else
			            {
			                $msg['title'] = '';
			                $msg['message'] = '';
			                $msg['vibrate'] = 1;
			                $msg['sound'] = 1;
			                $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			            } 
			        }
			    }
			}
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator);
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Approve QA **/

    public function Approve_qa()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            
            $this->Qa_model->approve_msg($message_id,$session_id,$event_id);
            
            $obj = new Gcm();
            $obj1 = new Fcm();
            $users = $this->Qa_model->getAllUsersGCM_id_by_event_id($event_id);
            $count = count($users);
            
            if($count > 100)
            {
                $limit = 100;
                for ($i=0;$i<$count;$i++) 
                {
                    $page_no        = $i;

                    $start          = ($page_no)*$limit;
                    $users1         = array_slice($users,$start,$limit);
                    foreach ($users1 as $key => $value) 
                    {
                        if($value['gcm_id']!='')
                        {
                            $msg =  '';
                            $extra['message_type'] = 'QAQusetion';
                            $extra['message_id'] = '';
                            $extra['event'] = $event[0]['Event_name'];
                            $extra['title'] = '';
                            if($value['device'] == "Iphone")
                            {
                                $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
                            }
                            else
                            {
                                $msg['title'] = '';
                                $msg['message'] = '';
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                        }
                    }
                }
            }
            else
            {
                foreach ($users as $key => $value)
                {
                    if($value['gcm_id']!='')
                    {
                        $msg = '';
                        $extra['message_type'] = 'QAQusetion';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        $extra['title'] = '';
                        if($value['device'] == "Iphone")
                        {
                            $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                    }
                }
            }
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator);
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    //**Show on Web**//
    public function show_on_web()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            
            $this->Qa_model->show_on_web($message_id,$session_id,$event_id);
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator);
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    public function hideMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            $this->Qa_model->hideMessage($message_id,$status);
            $obj = new Gcm();
            $obj1 = new Fcm();
            $users = $this->Qa_model->getAllUsersGCM_id_by_event_id($event_id);
            $count = count($users);
            if($count > 100)
            {
                $limit = 100;
                for ($i=0;$i<$count;$i++) 
                {
                    $page_no        = $i;

                    $start          = ($page_no)*$limit;
                    $users1         = array_slice($users,$start,$limit);
                    foreach ($users1 as $key => $value) 
                    {
                        if($value['gcm_id']!='')
                        {
                            $msg =  '';
                            $extra['message_type'] = 'QAQusetion';
                            $extra['message_id'] = '';
                            $extra['event'] = $event[0]['Event_name'];
                            $extra['title'] = '';
                            if($value['device'] == "Iphone")
                            {
                                $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
                            }
                            else
                            {
                                $msg['title'] = '';
                                $msg['message'] = '';
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                        }
                    }
                }
            }
            else
            {   
                foreach ($users as $key => $value)
                {
                    if($value['gcm_id']!='')
                    {
                        $msg = '';
                        $extra['message_type'] = 'QAQusetion';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        $extra['title'] = '';
                        if($value['device'] == "Iphone")
                        {
                            $result[] = $obj1->send_silent(1,$value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                    }
                }
            }
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            $tmp_status = $status == '1' ? '0' : '1';
            $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator,$tmp_status);
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    public function getHiddenQuestion()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $user_id = $this->input->post('user_id');
        $device_id = $this->input->post('device_id');
        $lang_id  = $this->input->post('lang_id');

        if($event_id!='' && $session_id!='')
        {
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id,$lang_id);
            $is_moderator = ($sessions[0]['Moderator_speaker_id'] == $user_id)?'1':'0';
            if($user_id != 0)
            {
                $messages = $this->Qa_model->getMessages($session_id,$user_id,$is_moderator,'1');
            }
            else
            {
                $messages = $this->Qa_model->getMessages($session_id,$device_id,$is_moderator,'1');
            }
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    public function ModeratorVoteMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');
        $vote = $this->input->post('vote');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='' && is_numeric($vote))
        {   
            $sessions = $this->Qa_model->getSessiondetail($event_id,$session_id);
            $this->Qa_model->vote($message_id,$user_id,$vote);
            $messages = $this->Qa_model->getMessages($session_id,$user_id,'1');
            
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }
}
?>   