<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/Event_template_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->model('Native_single_fcm/Settings_model');
        include('application/libraries/NativeGcm.php');
        include('application/libraries/Fcm.php');
    }

    public function headerSettings()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('_token');
        $user = $this->App_login_model->check_token_with_event($token,$event_id);
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $menu = $this->Settings_model->getNotesHeader($event_id);
            $menu_list = explode(',', $menu->checkbox_values);
            $menu_settings = (in_array(6, $menu_list)) ? 1 : 0;
            

            $data = array(
                'event' => $user[0],
                'event_feture_product' => $fetureproduct,
                'note_enable' => $menu_settings,
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function notification()
    {   
        $users = $this->Settings_model->getAllUsers();
        foreach ($users as $key => $value) 
        {
            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $notification = $this->Settings_model->getSceduledNotification($value['Id'],$value['gcm_id'],$event_id);
                if(!empty($notification))
                {
                    foreach ($notification as $key => $noti) 
                    {
                        if(!empty($noti['moduleslink']))
                        {
                            $extra['message_type'] = 'cms';
                            $extra['message_id'] = $noti['moduleslink'];
                        }
                        elseif(!empty($noti['custommoduleslink']))
                        {
                            $extra['message_type'] = 'custom_page';
                            $extra['message_id'] = $noti['custommoduleslink'];
                        }
                        $extra['event'] = $this->Settings_model->event_name($event_id);
                        if($value['device'] == "Iphone")
                        {
                            $obj = new Fcm();
                            $extra['title'] = $noti['title'];
                            $msg =  $noti['content'];
                            $result[] = $obj->send(1,$value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $obj = new Gcm($event_id);
                            $msg['title'] =   $noti['title'];
                            $msg['message'] = $noti['content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                        $this->Event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                    }
                }
            }
        }
        print_r($result);  
    }

    public function session_notification()
    {
        $users = $this->Settings_model->get_session_save_Users();
        $result = [];
        foreach ($users as $key => $value) 
        {
            $date = $this->Settings_model->getSessionScehduleTime($value['event_id']);
            if($date < $value['notify_datetime'])
                continue;

            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $this->Settings_model->markAsSendSessionNotification($value['user_id'],$value['Id']);
                $extra['message_type'] = 'cms';
                $extra['message_id'] = '1';
                $extra['event'] = $this->Settings_model->event_name($event_id);
                if($value['device'] == "Iphone")
                {
                    $obj = new Fcm();
                    $extra['title'] = $value['title'];
                    $msg =  $value['messages'];
                    $result[] = $obj->send(1,$value['gcm_id'],$msg,$extra,$value['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] =   $value['title'];
                    $msg['message'] = $value['messages'];
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                } 
                //$this->Event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
            }
        }
        print_r($result);  
    }

    public function getAdvertising()
    {
        $event_id   = $this->input->post('event_id');
        $menu_id    = $this->input->post('menu_id');

        if($event_id!='' && $menu_id!='')
        {
            $advertise_data           = $this->Settings_model->getAdvertisingData($event_id,$menu_id);
            $advertise_data->H_images = (json_decode($advertise_data->H_images)) ?json_decode($advertise_data->H_images) :[] ;
            $advertise_data->F_images = (json_decode($advertise_data->F_images)) ? json_decode($advertise_data->F_images) : [];
            if($advertise_data)
            {
            $data = array(
                    'success'   => true,
                    'data'      => $advertise_data,
                );
            }
            else
            {
                $data = array(
                    'success'   => true,
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => "Invalid Parameters",
                );
        }
        echo json_encode($data);
    }

    public function getAdvertising_new()
    {
        $event_id   = $this->input->post('event_id');
        $menu_id    = $this->input->post('menu_id');
        $is_cms    = $this->input->post('is_cms');

        if($event_id!='' && $menu_id!='')
        {
            $advertise_data           = $this->Settings_model->getAdvertisingData_new($event_id,$menu_id,$is_cms);
            $advertise_data->H_images = (json_decode($advertise_data->H_images)) ?json_decode($advertise_data->H_images) :[] ;
            $advertise_data->F_images = (json_decode($advertise_data->F_images)) ? json_decode($advertise_data->F_images) : [];
            if($advertise_data)
            {
            $data = array(
                    'success'   => true,
                    'data'      => $advertise_data,
                );
            }
            else
            {
                $data = array(
                    'success'   => true,
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => "Invalid Parameters",
                );
        }
        echo json_encode($data);
    }

    public function fundraisingDonationPayment()
    {
        $total              = $this->input->post('total');
        $stripeToken        = $this->input->post('stripeToken');
        $currency           = $this->input->post('currency');
        $admin_secret_key   = $this->input->post('admin_secret_key'); // sk_test_MQdSDqW91LmspHHYRzZ5FNyk

        if( $total==''  || $stripeToken=='' || $currency=='' || $admin_secret_key=='')
        {
             $data = array(
              'success' => false,
              'message' => "Invalid parameters"
             );
             echo json_encode($data);
             exit;
        }
        else
        {
            require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
            try
            {
                \Stripe\Stripe::setApiKey($admin_secret_key); 
                
                $payment =  \Stripe\Charge::create(
                      array(
                      "amount"      => $total * 100,
                      "currency"    => strtoupper($currency),
                      "source"      => $stripeToken,
                      "description" => "All In The Loop"
                    )
                );
                
                $data1['transaction_id']=$stripeToken;
                if($payment['status']=="succeeded")
                {
                    $data1['order_status']="completed";
                     $data = array(
                      'success'         => true,
                      'message'         => "completed",
                      'transaction_id'  => $data1['transaction_id'],
                    );
                }
                else
                {
                    $data1['order_status']=$payment['status'];  
                    $data = array(
                      'success'         => false,
                      'message'         => "failed",
                      'transaction_id'  => $data1['order_status'],
                    );
                }
                echo json_encode($data);
            }
            catch(Exception $e)
            {
               $data = array(
                      'success' => false,
                      'message' => "Something went wrong.Please try again.",
                      'error'   => $e,
                    );

                echo json_encode($data);
            }
        }
    }
    public function checkVersionCode()
    {



       // echo  "<pre>"; print_r($_POST);
       // var_dump($this->input->post()); exit();
        $device = $this->input->post('device');
        
        if($device!='')
        {
            $where['app'] = 'ALlInTheLoop';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function checkVersionCode_new()
    {
        error_reporting(E_ALL);
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ALlInTheLoop';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode_new($where,$device);
            $URL = $this->Settings_model->getVersionCode_new_url($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
              'URL'  => ($URL) ? $URL : ''
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkWine4TradeVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'WIne4Trade';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkSolarplazaVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Solarplaza';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkApexVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Apex';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkGlidewellVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Glidewell';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }  
    public function userClickBoard()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $exhibitor_page_id = $this->input->post('exhibitor_page_id');
        $sponser_id = $this->input->post('sponser_id');
        $advertise_id = $this->input->post('advertise_id');
        $banner_id = $this->input->post('banner_id');
        $cms_id = $this->input->post('cms_id');
        $exhi_cat_id = $this->input->post('exhi_cat_id');
        $menu_id = $this->input->post('menu_id');
        $click_type = $this->input->post('click_type');

        if($user_id!='' && $event_id!='' && $click_type!='' && $user_id !='0')
        {
            switch ($click_type) {
                case 'AD':
                    $where['advert_id'] = $advertise_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserClickBoard($where);

                    $adwhere['event_id'] = $event_id;
                    $adwhere['user_id'] = $user_id;
                    $adwhere['menu_id'] = '5';
                    $adwhere['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                break;
                
                case 'SP':
                    $where['sponsor_id'] = $sponser_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserClickBoard($where);
                break;
            
                case 'EX':
                    $where['exhibitor_id'] = $exhibitor_page_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserClickBoard($where);
                break;

                case 'OT':
                    $adwhere['event_id'] = $event_id;
                    $adwhere['user_id'] = $user_id;
                    $adwhere['menu_id'] = $menu_id;
                    $adwhere['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                break;

                case 'BN': //banner CLICK
                    $where['banner_id'] = substr($banner_id,4);
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserClickBoard($where);
                break;

                case 'CMS': //CMS CLICK
                    $where['cms_id'] = $cms_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->Settings_model->hitUserClickBoard($where);
                break;

                case 'EXHI_CAT': //EXHI CATEGORY CLICK
                    $exhi_cat_id = explode(',',$exhi_cat_id);
                    foreach ($exhi_cat_id as $key => $value)
                    {
                        $where['exhi_cat_id'] = $value;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->Settings_model->hitUserClickBoard($where);
                    }
                break;
            }
        }
    }

    public function getAllApps()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $apps = $this->Settings_model->getAllApps($device);
            $data = array(
              'success'  => true,
              'data'  => ($apps) ? $apps : [],
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function updateVersionCode()
    {
        $device = $this->input->post('device');
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        //$URL = $this->input->post('URL');
        if($device!='' && $id!='')
        {
            $device = strtolower($device);
            $where['Id'] = $id;
            $update_data[$device."_code"] =  $code;
            //$URL = $URL;
            //$apps = $this->Settings_model->updateVersionCode($update_data,$where,$URL);
            $apps = $this->Settings_model->updateVersionCode($update_data,$where);
            $data = array(
              'success'  => true,
              'message'  => "Version updated successfully.",
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function updateVersionCode_new()
    {
        $device = $this->input->post('device');
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $URL = $this->input->post('URL');
        if($device!='' && $id!='')
        {
            $device = strtolower($device);
            $where['Id'] = $id;
            $update_data[$device."_code"] =  $code;
            $update_data['URL'] =  $URL;
            $apps = $this->Settings_model->updateVersionCode($update_data,$where);
            $data = array(
              'success'  => true,
              'message'  => "Version updated successfully.",
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function get_o_screen()
    {
        $event_id = $this->input->get_post('event_id');
        if($event_id!='')
        {
            
            $data = $this->Settings_model->get_onboarding_settings($event_id);
            $data = array(
              'success'  => true,
              'code'  => ($data) ? $data : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkKnectVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Knect';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function checkGamesforumVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Gamesforum';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function send_Notification_FHA_update()
    {
        $this->db->from('user u');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('reu.Event_id','585');
        $this->db->where('device','Android');
        // $this->db->where('Email','iosfha@venturiapps.com');
        $this->db->where('(gcm_id IS NOT NULL AND gcm_id != "")');
        $users = $this->db->get()->result_array();
        // j($users);
        $users = array_chunk($users,300);
        foreach ($users[1] as $key => $value)
        {   
            $extra['message_type'] = 'custom_page';
            $extra['message_id'] = '1368';
            $obj = new Gcm('585');
            $msg['title'] =   'Update Available';
            $msg['message'] = 'Please download the latest version of the FHA&PWA App';
            $msg['vibrate'] = 1;
            $msg['sound'] = 1;
            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
        }
        j($result);
    }
}