<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->model('Native_single_fcm/Attendee_model');

        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'),$this->input->post('lang_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    /***Get CMS page list direcly**/

    public function get_cms_page()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $cms_id=$this->input->post('cms_id');
        $token=$this->input->post('token');
        $lang_id = $this->input->post('lang_id');

        if($event_id!='' && $event_type!='' && $cms_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $cms_page=$this->Cms_model->get_cms_page($event_id,$cms_id,$token,$lang_id);
               
                if(!empty($cms_page[0]['Images']))
                {
                    $cmsbannerimage_decode = json_decode($cms_page[0]['Images']);
                    $cms_page[0]['Images'] = $cmsbannerimage_decode[0];
                }
                if(!empty($cms_page[0]['Logo_images']))
                {
                    $cmslogoimage_decode = json_decode($cms_page[0]['Logo_images']);
                    $cms_page[0]['Logo_images'] = $cmslogoimage_decode[0];
                }
                if(!empty($cms_page[0]['Description']))
                {
                    $cms_page[0]['Description'] = html_entity_decode($cms_page[0]['Description']);
                }

                if($cms_id=='1235' && $event_id == '1307' && !empty($cms_page[0]['url']))
                    $cms_page[0]['url'] = "";

                if($cms_id=='843' && $event_id == '1107' && !empty($cms_page[0]['url']))
                    $cms_page[0]['url'] = "";
                
                if($this->Cms_model->getOrganizer($event_id) == '20152' && !empty($cms_page[0]['url']) )
                    $cms_page[0]['url'] = "";

                if($cms_id == '1432')
                    $cms_page[0]['url'] = "";
                
                if(strpos($cms_page[0]['url'],'.js') !== false)
                {   
                    $cms_page[0]['url'] = "";
                }
                $data = array(
                    'cms_details' => $cms_page
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get CMS Super Groups**/

    public function get_cms_super_group()
    {
        $event_id = $this->input->post('event_id');
        if(!empty($event_id))
        {
            $data['super_group'] = $this->Cms_model->get_super_group($event_id);
            $data = array(
                'success' => true,
                'data' => $data
            ); 
        }
        else
        {
            $data = array(
                'success' => flase,
                'message' => 'Invalid parameters'
            );
        }
        echo json_encode($data);
    }

    /***Get CMS Child CMS Groups**/

    public function get_chid_cms_group()
    {   
        $event_id = $this->input->post('event_id');
        $super_group_id = $this->input->post('super_group_id');
        if(!empty($event_id))
        {
            $data['child_group'] = $this->Cms_model->get_child_group($event_id,$super_group_id);
            $data = array(
                'success' => true,
                'data' => $data
            );            
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters'
            );
        }
        echo json_encode($data);
    }

    /***Get CMS Child CMS List**/

    public function get_child_cms_list()
    {
        $event_id = $this->input->post('event_id');
        $child_id = $this->input->post('child_id');
        if(!empty($event_id))
        {
            $data['cms_list'] = $this->Cms_model->get_child_cms_list($event_id,$child_id);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }

    /***Get CMS Details**/

    public function get_cms_list_details()
    {
        $event_id = $this->input->post('event_id');
        $child_id = $this->input->post('child_id');
        if(!empty($event_id))
        {
            $data = $this->Cms_model->get_cms_list_details($event_id,$child_id);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
}
