<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Surveys extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Surveys';
          $this->data['smalltitle'] = 'Surveys';
          $this->data['breadcrumb'] = 'Surveys';
          parent::__construct($this->data);
          $this->load->library('formloader');
          $this->load->library('formloader1');
          $this->load->model('Agenda_model');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Survey_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Profile_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Message_model');
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('15',$menu_list))
          {
                echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          $eventname=$this->Event_model->get_all_event_name();
          if(in_array($this->uri->segment(3),$eventname))
          {
            if ($user != '')
            {
                 $logged_in_user_id=$user[0]->User_id;
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
               
                  $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                  if ($cnt == 1)
                  {
                       $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                       $this->data['notes_list'] = $notes_list;
                  }
                  else
                  {
                    $event_type=$event_templates[0]['Event_type'];
                    if($event_type==3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain=$this->uri->segment(3);
                        $acc_name=$this->uri->segment(2);
                        echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
                    }
                  }
            }
          }
          else
          {
               $Subdomain=$this->uri->segment(3);
               $flag=1;
               echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
          }
     }

     public function index($acc_name,$Subdomain,$intFormId=NULL)
     {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $survey_category=$this->Event_template_model->get_all_surveys_category_by_event($event_templates[0]['Id']);
        if(count($survey_category)==1)
        {
          redirect(base_url().'Surveys/'.$acc_name.'/'.$Subdomain.'/question_index/'.$survey_category[0]['survey_id']);
        }
        $this->data['survey_category']=$survey_category;
        $user = $this->session->userdata('current_user');
        if(!empty($user[0]->Id))
        {
          $req_mod = $this->router->fetch_class();
          $menu_id=$this->Event_model->get_menu_id($req_mod);
          $current_date=date('Y/m/d');
          $this->Event_model->add_view_hit($user[0]->Id,$current_date,$menu_id,$event_templates[0]['Id']);   
        }
        $intUId = $user[0]->Id;
        $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting']=$notificationsetting;

        $oid=$event_templates[0]['Organisor_id'];
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;

        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
    
        $this->data['sign_form_data']=$res1;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $uid=$user[0]->Id;

        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;

        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;

        $this->data['Subdomain'] = $Subdomain;

        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $survey_screens = $this->Survey_model->get_survey_screens($event_templates[0]['Id']);
        $this->data['survey_screens'] = $survey_screens;
        for($i=0;$i<count($menu_list);$i++)
        {
          if('Surveys'==$menu_list[$i]['pagetitle'])
          {
            $mid=$menu_list[$i]['id'];
          }
        }
        $this->data['menu_list'] = $menu_list;
        $this->data['menu_id']=$mid;
        $eid=$event_templates[0]['Id'];
        $res=$this->Agenda_model->getforms($eid,$mid);
        $this->data['form_data']=$res;

        $array_temp_past = $this->input->post();
        if(!empty($array_temp_past))
        {
            unset($_POST['hdnFormBuild']);
            $aj=json_encode($this->input->post());
            $formdata=array('f_id' =>$intFormId,
                'm_id'=>$mid,
                'user_id'=>$intUId,  
                'event_id'=>$eid,
                'json_submit_data'=>$aj
             );
           $this->Agenda_model->formsinsert($formdata);
           redirect(base_url().'Surveys/'.$acc_name.'/'.$Subdomain);
           //echo '<script>window.location.href="'.base_url().'Surveys/'.$acc_name.'/'.$Subdomain.'"</script>';
           exit;
        }
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        if ($event_templates[0]['Event_type'] == '1')
        {
          if (empty($user))
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
          }
          else
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'Surveys/main_index', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
          }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
          if (empty($user))
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
          }
          else
          { 
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'Surveys/main_index', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
          }
        } 
        elseif ($event_templates[0]['Event_type'] == '3') 
        {
          $this->session->unset_userdata('acc_name');
          $acc['acc_name'] =  $acc_name;
          $this->session->set_userdata($acc);
          $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('15',$event_templates[0]['Id']);
          if($isforcelogin['is_force_login']=='1' && empty($user))
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
          }
          else
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'Surveys/main_index', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
          }
        }
        else
        {
          if (empty($user))
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
          }
          else
          {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'Surveys/main_index', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
          }
        }  
        $this->template->render();  
     }
     public function question_index($acc_name,$Subdomain,$sid)
     {
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $user = $this->session->userdata('current_user');
          $intUId = $user[0]->Id;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $oid=$event_templates[0]['Organisor_id'];
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $uid=$user[0]->Id;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $this->data['Subdomain'] = $Subdomain;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $survey_category=$this->Survey_model->get_edit_survey_category_by_id($event_templates[0]['Id'],$sid);
          $this->data['survey_category']=$survey_category;
          $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
          $this->data['time_format'] = $time_format;
          if(!empty($user[0]->Id)){
            $survey_screens = $this->Survey_model->get_survey_screens($event_templates[0]['Id'],$sid);  
            $this->data['survey_screens'] = $survey_screens;
            $survey = $this->Event_template_model->get_survey($Subdomain,$sid);
            $this->data['survey'] = $survey;
            $survey_value = $this->Event_template_model->get_survey_value($Subdomain,$sid);
            $this->data['survey_value'] = $survey_value;
            $survey_answer_chart = $this->Event_template_model->get_survey_answer_chart();
            $this->data['survey_answer_chart'] = $survey_answer_chart;
          }
          for($i=0;$i<count($menu_list);$i++)
          {
            if('Surveys'==$menu_list[$i]['pagetitle'])
            {
              $mid=$menu_list[$i]['id'];
            }
          }
          $this->data['menu_list'] = $menu_list;
          $this->data['menu_id']=$mid;
          $eid=$event_templates[0]['Id'];
          if ($event_templates[0]['Event_type'] == '1')
               {
                  if (empty($user))
                  {
                      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                      $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                      $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                      $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                        if ($this->input->post())
                        {

                             $question = $this->input->post('question');
                             $user = $this->session->userdata('current_user');
                             $userid = $user[0]->Id;

                             foreach ($question as $key => $val)
                             {
                                  $data = array();
                                  $data['survey_array']['User_id'] = $userid;
                                  $data['survey_array']['Question_id'] = $val;
                                  $data['survey_array']['Answer'] = is_array($this->input->post('question_option' . $key)) ? implode(',', $this->input->post('question_option' . $key)) : $this->input->post('question_option' . $key);

                                  $this->Event_template_model->add_survey($data);
                             }

                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/thankyou', $this->data, true);
                        }
                        else
                        {    
                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/index', $this->data, true);
                        }
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '2')
               {
  
                  if (empty($user))
                  {
                      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                      $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                      $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                      $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                        if ($this->input->post())
                        {

                             $question = $this->input->post('question');
                             $user = $this->session->userdata('current_user');
                             $userid = $user[0]->Id;

                             foreach ($question as $key => $val)
                             {
                                  $data = array();
                                  $data['survey_array']['User_id'] = $userid;
                                  $data['survey_array']['Question_id'] = $val;
                                  $data['survey_array']['Answer'] = is_array($this->input->post('question_option' . $key)) ? implode(',', $this->input->post('question_option' . $key)) : $this->input->post('question_option' . $key);

                                  $this->Event_template_model->add_survey($data);
                             }
                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/thankyou', $this->data, true);
                        }
                        else
                        {    
                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/index', $this->data, true);
                        }
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '3') 
               {

                  $this->session->unset_userdata('acc_name');
                  $acc['acc_name'] =  $acc_name;
                  $this->session->set_userdata($acc);
                  $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('15',$event_templates[0]['Id']);
                  if($isforcelogin['is_force_login']=='1' && empty($user))
                  {
                    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                    $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
                  }
                  else
                  {
                        if(!empty($user))
                        {
                          if ($this->input->post())
                          {
                               $question = $this->input->post('question');
                               $user = $this->session->userdata('current_user');
                               $userid = $user[0]->Id;

                               foreach ($question as $key => $val)
                               {
                                    $data = array();
                                    $data['survey_array']['User_id'] = $userid;
                                    $data['survey_array']['Question_id'] = $val;
                                    $data['survey_array']['Answer'] = is_array($this->input->post('question_option' . $key)) ? implode(',', $this->input->post('question_option' . $key)) : $this->input->post('question_option' . $key);

                                    $this->Event_template_model->add_survey($data);
                               }
                               $this->template->write_view('css', 'Surveys/css', $this->data, true);
                               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                               $this->template->write_view('js', 'Surveys/js', $this->data, true);
                               $this->template->write_view('content', 'Surveys/thankyou', $this->data, true);
                          }
                          else
                          {    
                               $this->template->write_view('css', 'Surveys/css', $this->data, true);
                               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                               $this->template->write_view('js', 'Surveys/js', $this->data, true);
                              $this->template->write_view('content', 'Surveys/index', $this->data, true);
                          }
                        }
                        else
                        {
                               $this->template->write_view('css', 'Surveys/css', $this->data, true);
                               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                               $this->template->write_view('js', 'Surveys/js', $this->data, true);
                               if($this->session->userdata('surveys')==1)
                               {
                                 $this->session->unset_userdata('surveys');
                                 $this->template->write_view('content', 'registration/index', $this->data, true);
                               }
                               else
                               {
                                  $this->template->write_view('content', 'Surveys/index', $this->data, true);
                               }
                        }
                  }            
               }
               else
               {
                  if (empty($user))
                  {
                      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                      $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                      $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                      $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                      $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                  }
                  else
                  {
                        if ($this->input->post())
                        {

                             $question = $this->input->post('question');
                             $user = $this->session->userdata('current_user');
                             $userid = $user[0]->Id;

                             foreach ($question as $key => $val)
                             {
                                  $data = array();
                                  $data['survey_array']['User_id'] = $userid;
                                  $data['survey_array']['Question_id'] = $val;
                                  $data['survey_array']['Answer'] = is_array($this->input->post('question_option' . $key)) ? implode(',', $this->input->post('question_option' . $key)) : $this->input->post('question_option' . $key);

                                  $this->Event_template_model->add_survey($data);
                             }

                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/thankyou', $this->data, true);
                        }
                        else
                        {    
                             $this->template->write_view('css', 'Surveys/css', $this->data, true);
                             $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                             $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                             $this->template->write_view('js', 'Surveys/js', $this->data, true);
                             $this->template->write_view('content', 'Surveys/index', $this->data, true);
                        }
                  }
               }
               $this->template->render();
          
     }

     public function View($acc_name,$Subdomain = NULL)
     {
          
          $user = $this->session->userdata('current_user');

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $this->data['Subdomain'] = $Subdomain;

          $survey_answer_chart = $this->Event_template_model->get_survey_answer_chart($Subdomain);
          $this->data['survey_answer_chart'] = $survey_answer_chart;
         
          $user = $this->session->userdata('current_user');
          if (empty($user))
          {
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('content', 'registration/index', $this->data, true);
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
          }
          else
          {

            $this->template->write_view('css', 'Surveys/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('content', 'Surveys/view', $this->data, true);
            $this->template->write_view('footer', 'Surveys/footer', $this->data, true);
            $this->template->write_view('js', 'Surveys/js', $this->data, true);
            $this->template->render();
          }
     }

}
