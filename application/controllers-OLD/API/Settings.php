<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/event_template_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/settings_model');
        $this->load->library('Gcm');
       
        require('socket/server.php');
        
        

        $this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }

    public function headerSettings()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('_token');
        $user = $this->app_login_model->check_token_with_event($token,$event_id);
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $menu = $this->settings_model->getNotesHeader($event_id);
            $menu_list = explode(',', $menu->checkbox_values);
            $menu_settings = (in_array(6, $menu_list)) ? 1 : 0;
            

            $data = array(
                'event' => $user[0],
                'event_feture_product' => $fetureproduct,
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu,
                'note_enable' => $menu_settings,
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }
    /*public function notification()
    {
        $event_id=$this->input->post('event_id');
        $menu_id=$this->input->post('menu_id');
        $user_id=$this->input->post('user_id');

        $result = array();    
            if($event_id=='' || $menu_id=='' || $user_id=='')
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Invalid parameter.'
                    )
                ); 
            }
            else
            {
                $gcm_id = $this->settings_model->getGcmId($user_id);
                $device = $this->settings_model->getDevice($user_id);
                if($gcm_id!='')
                {
                    $data = $this->settings_model->is_notification_read($event_id,$menu_id,$user_id);
                    $extra['message_type'] = '';
                    $extra['message_id'] = '';
                    $extra['event'] = $this->settings_model->event_name($event_id);
                    if(!empty($data))
                    {
                        $obj = new Gcm();
                        foreach ($data as $key => $value) 
                        {
                            if($device == "Iphone")
                            {
                                $msg =  $value['content'];
                                
                                $result = $obj->send_notification($gcm_id,$msg,$extra,$device);
                            }
                            else
                            {
                                $msg['title'] = "New Notification";
                                $msg['message'] = $value['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($gcm_id,$msg,$extra);
                            } 
                        }
                    }
                }   
                $data = array(
                    'success' => true,
                    'data' => $result,
                );
        }
        echo json_encode($data);    
    }*/
    public function notification()
    {
        $users = $this->settings_model->getAllUsers();
        
        foreach ($users as $key => $value) {
        if($value['gcm_id']!='')
          {
            $notification = $this->settings_model->getSceduledNotification($value['Id'],$value['gcm_id'],$value['event_id']);
            
            if(!empty($notification))
            {
                foreach ($notification as $key => $noti) {
                   
                      $obj = new Gcm();
                      $extra['message_type'] = '';
                      $extra['message_id'] = '';
                      $extra['event'] = $this->settings_model->event_name($noti['event_id']);
                      if($value['device'] == "Iphone")
                      {
                          $msg =  $noti['content'];
                          
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      }
                      else
                      {
                          $msg['title'] =   $noti['title'];
                          $msg['message'] = $noti['content'];
                          $msg['vibrate'] = 1;
                          $msg['sound'] = 1;
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      } 
            }
        }
        }
        }
       // print_r($result);
    }
    public function testNotification()
    {

        error_reporting(1);
        $event_id = $this->input->post('event_id');
        $gcm_id = $this->settings_model->getGcmId($event_id);
        $gcm_ids = array_values(array_filter($gcm_id));
        $obj = new Gcm();
          
        $msg['title'] = 'test';
        $msg['message'] = 'test';
        $msg['vibrate'] = 1;
        $msg['sound'] = 1;
        $extra['message_type'] = 'cms';
        $extra['message_id'] = '1';
        $extra['event'] = $this->settings_model->event_name($event_id);
        define('API_ACCESS_KEY','AIzaSyAI_aRYdKUw3Gc1LjbLNwwxLLN9A6KvcSg');

        $registrationIds = $gcm_ids;
            
        $fields = array
        (
            'priority' => 'high',
            'registration_ids'  => $registrationIds,
            'data'              => array("message"=>$message,"extra"=>$extra,"profile"=>$profile),
        );
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json',
        );
      
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,'https://android.googleapis.com/gcm/send');
        curl_setopt($ch,CURLOPT_POST, true );
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        $result = json_decode($result);
           
        
        echo json_encode($result);
    }

    public function getAdvertising()
    {
        $event_id   = $this->input->post('event_id');
        $menu_id    = $this->input->post('menu_id');

        if($event_id!='' && $menu_id!='')
        {
            $advertise_data           = $this->settings_model->getAdvertisingData($event_id,$menu_id);
            $advertise_data->H_images = (json_decode($advertise_data->H_images)) ?json_decode($advertise_data->H_images) :[] ;
            $advertise_data->F_images = (json_decode($advertise_data->F_images)) ? json_decode($advertise_data->F_images) : [];
            if($advertise_data)
            {
            $data = array(
                    'success'   => true,
                    'data'      => $advertise_data,
                );
            }
            else
            {
                $data = array(
                    'success'   => true,
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => "Invalid Parameters",
                );
        }
        echo json_encode($data);
    }
     public function fundraisingDonationPayment()
    {
        //error_reporting(1);
        $total              = $this->input->post('total');
        $stripeToken        = $this->input->post('stripeToken');
        $currency           = $this->input->post('currency');
        $admin_secret_key   = $this->input->post('admin_secret_key'); // sk_test_MQdSDqW91LmspHHYRzZ5FNyk

        if( $total==''  || $stripeToken=='' || $currency=='' || $admin_secret_key=='')
        {
             $data = array(
              'success' => false,
              'message' => "Invalid parameters"
             );
             echo json_encode($data);
             exit;
        }
        else
        {
            require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
            try
            {
                \Stripe\Stripe::setApiKey($admin_secret_key); 
                //\Stripe\Stripe::$apiBase = "https://api-tls12.stripe.com";

                
                $payment =  \Stripe\Charge::create(
                      array(
                      "amount"      => $total * 100,
                      "currency"    => strtoupper($currency),
                      "source"      => $stripeToken,
                      "description" => "All In The Loop"
                    )
                );
                
                $data1['transaction_id']=$stripeToken;
                if($payment['status']=="succeeded")
                {
                    $data1['order_status']="completed";
                     $data = array(
                      'success'         => true,
                      'message'         => "completed",
                      'transaction_id'  => $data1['transaction_id'],
                    );
                }
                else
                {
                    $data1['order_status']=$payment['status'];  
                    $data = array(
                      'success'         => false,
                      'message'         => "failed",
                      'transaction_id'  => $data1['order_status'],
                    );
                }
                echo json_encode($data);
            }
            catch(Exception $e)
            {
               $data = array(
                      'success' => false,
                      'message' => "Something went wrong.Please try again.",
                      'error'   => $e,
                    );

                echo json_encode($data);
            }
        }
    }
    public function checkVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ALlInTheLoop';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function userClickBoard()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $exhibitor_page_id = $this->input->post('exhibitor_page_id');
        $sponser_id = $this->input->post('sponser_id');
        $advertise_id = $this->input->post('advertise_id');
        $menu_id = $this->input->post('menu_id');
        $click_type = $this->input->post('click_type');

        if($user_id!='' && $event_id!='' && $click_type!='')
        {
            switch ($click_type) {
                case 'AD':
                        $where['advert_id'] = $advertise_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);

                        $adwhere['event_id'] = $event_id;
                        $adwhere['user_id'] = $user_id;
                        $adwhere['menu_id'] = '5';
                        $adwhere['date'] = date('Y-m-d');
                        $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');

                        break;
                
                case 'SP':
                        $where['sponsor_id'] = $sponser_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);
                        break;
                
                case 'EX':
                        $where['exhibitor_id'] = $exhibitor_page_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);
                        break;

                case 'OT':
                        $adwhere['event_id'] = $event_id;
                        $adwhere['user_id'] = $user_id;
                        $adwhere['menu_id'] = $menu_id;
                        $adwhere['date'] = date('Y-m-d');
                        $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                        break;
              
            }
        }

    }
    public function countarray()
    {
        $str = "10650,10829,10968,11006,11114,11115,11116,11117,11118,11119,11122,11123,11124,11127,11128,11129,11130,11132,11133,11134,11135,11137,11138,11139,11140,11141,11143,11145,11146,11148,11152,11154,11160,11163,11164,11167,11168,11652,11765,12538,12690,12755,13015,13017,13020,13021,13022,13029,13030,13031,13034,13035,13037,13039,13040,13066,13085,13087,13089,13092,13093,13101,13102,13104,13109,13115,13326,13330,13336,13338,13339,13340,13341,13343,13347,13348,13349,13351,13353,13355,13359,13360,13361,13367,13370,13374,13412,13436,13452,13467,13484,13491,13513,13527,13528,13530,13535,13538,13539,13541,13554,13624,13637,13638,13645,13655,13656,13658,13659,13661,13669,13675,13676,13677,13678,13679,13696,13697,13698,13708,13718,14102,14103,14104,14107,14108,14111,14112,14113,14118,14139,14175,14179,14180,14196,14199,14203,14227,14257,14258,14259,14260,14261,14262,14263,14264,14265,14266,14267,14291,14292,14293,14296,14298,14299,14304,14306,14307,14308,14309,14310,14311,14313,14315,14316,14344,14346,14351,14352,14353,14354,14355,14357,14358,14359,14360,14361,14362,14363,14364,14365,14366,14367,14368,14369,14370,14372,14373,14374,14376,14378,14380,14383,14384,14385,14386,14390,14402,14404,14405,14406,14407,14408,14412,14413,14415,14416,14426,14428,14429,14430,14431,14433,14434,14437,14438,14439,14440,14441,14442,14443,14444,14445,14447,14448,14449,14452,14478,14479,14480,14482,14483,14484,14485,14486,14487,14489,14490,14491,14492,14493,14494,14495,14496,14497,14499,14500,14501,14502,14503,14506,14508,14509,14510,14516,14517,14518,14519,14520,14521,14522,14523,14524,14525,14526,14527,14528,14530,14531,14532,14534,14535,14536,14537,14538,14539,14540,14542,14543,14544,14545,14546,14547,14548,14549,14556,14559,14560,14561,14562,14563,14573,14576,14577,14578,14579,14580,14581,14583,14584,14585,14586,14587,14588,14589,14590,14591,14593,14595,14596,14597,14598,14599,14601,14602,14603,14605,14606,14607,14608,14609,14610,14611,14612,14613,14614,14615,14616,14617,14619,14620,14622,14623,14624,14625,14626,14632,14633,14634,14638,14640,14642,14643,14644,14701,14702,14704,14707,14708,14709,14711,14712,14713,14714,14715,14716,14717,14718,14719,14721,14722,14724,14725,14726,14727,14728,14730,14731,14732,14733,14734,14735,14736,14738,14739,14740,14742,14743,14744,14745,14746,14747,14748,14749,14750,14751,14753,14754,14756,14757,14758,14759,14760,14761,14762,14763,14764,14765,14766,14767,14768,14769,14770,14771,14772,14773,14774,14775,14777,14778,14779,14780,14781,14782,14783,14784,14785,14786,14787,14788,14789,14790,14791,14792,14793,14794,14795,14796,14797,14798,14799,14800,14801,14802,14804,14805,14806,14807,14808,14809,14811,14812,14813,14815,14816,14817,14818,14819,14820,14821,14822,14823,14824,14825,14826,14828,14829,14830,14831,14832,14833,14835,14356,14729,14776,14574,14435,14446";
        $array = explode(',', $str);
        echo count($array);
    }
    public function socketServer()
    {
        $file = FCPATH."socket/server.php";
		$server = new SocketServer("104.28.25.209",2085); 
    	$server->send("Server : Hello client");  
        echo "done";
    	
    }
    public function client()
    {
        error_reporting(E_ALL);
        $host='199.83.214.85';
        $port = 6789;
         
        $timeout=30;
        $sk=fsockopen($host,$port,$errnum,$errstr,$timeout) ;

        if (!is_resource($sk)) {
            exit("connection fail: ".$errnum." ".$errstr) ;
        } else {
            
            while (!feof($sk)) {
                echo fgets($sk, 1024);
            }
            fclose($sk);
        }
    }
    /*public function client()
    {
        $host = "199.83.214.85";
        $port = 6789;
        // No Timeout 
        set_time_limit(0);
    
    $socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");

    $result = socket_connect($socket, $host, $port) or die("Could not connect toserver\n");

    socket_write($socket, "Hello", strlen("Hello")) or die("Could not send data to server\n");

    $result = socket_read ($socket, 1024) or die("Could not read server response\n");
echo "Reply From Server  :".$result;


socket_close($socket);


    }*/
}