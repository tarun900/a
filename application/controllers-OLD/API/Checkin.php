<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Checkin extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/checkin_model');
        $this->load->model('API/app_profile_model');
        //date_default_timezone_set("UTC");
    }
    public function attendeeList()
    {
        $event_id   = $this->input->post('event_id');
        if($event_id!='')
        {
            $attendee = $this->checkin_model->getAttendeeListByEventId($event_id);
            $data = array(
                'attendee_list' => $attendee,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendeeCheckIn()
    {
        $event_id    = $this->input->post('event_id');
        $attendee_id = $this->input->post('attendee_id');

        if($event_id!='' && $attendee_id!='')
        {
            $data['Event_id']       = $event_id;
            $data['Attendee_id']    = $attendee_id;

            $result = $this->checkin_model->saveCheckIn($data);
            $data   = array(
                'check_in_btn_color' => ($result) ? "#449d44" : "#007CBA" ,
                'is_checked_in'      => ($result) ? "1" : "0" ,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function viewAttendeeInfo()
    {
        $event_id       = $this->input->post('event_id');
        $attendee_id    = $this->input->post('attendee_id');

        if($event_id!='' && $attendee_id!='')
        {
        
            $where['Event_id']      = $event_id;
            $where['Attendee_id']   = $attendee_id;
            $personal_info          = $this->checkin_model->getAttendeeDetails($attendee_id);
            $custom_info            = $this->checkin_model->getCustomInfo($where);
            $data = array(
                'personal_info' => $personal_info,
                'custom_info'   => $custom_info,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function saveAttendeeInfo()
    {
        $event_id       = $this->input->post('event_id');
        $attendee_id    = $this->input->post('attendee_id');
        $personal_info  = json_decode($this->input->post('personal_info'),true); // json
        $custom_info['extra_column']    = ($this->input->post('custom_info')); // json

        if($event_id!='' && $attendee_id!='' && $personal_info!=NULL && $custom_info['extra_column']!=NULL)
        {
            if(!empty(($_FILES['logo']['name'])))
            {
                $newImageName = $attendee_id . "_" . round(microtime(true) * 1000).".jpeg";
                $target_path = "././assets/user_files/".$newImageName;          
                move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $this->app_profile_model->make_thumbnail($attendee_id,$newImageName);
                $personal_info['Logo'] =$newImageName;
            }
            $personal_info['updated_date'] = date('Y-m-d H:i:s');

            $where['Event_id']      = $event_id;
            $where['Attendee_id']   = $attendee_id;
            $this->checkin_model->saveAttendeeDetails($personal_info,$attendee_id);
            $this->checkin_model->saveCustomInfo($custom_info,$where);
           
            $data = array(
              'success' => true,
              'data'    => 'Details saved successfully'
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
?>   