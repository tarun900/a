public class Client extends AsyncTask 
{    
	String dstAddress;    
	int dstPort;    
	String response = "";    
	TextView textResponse;    
	Client(String addr, int port, TextView textResponse) 
	{        
		dstAddress = addr;        
		dstPort = port;        
		this.textResponse = textResponse;    
	}    
	@Override    
	protected String doInBackground(Object... arg0) 
	{        
		Socket socket = null;        
		try {            
			socket = new Socket(dstAddress, dstPort);            
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);            
			byte[] buffer = new byte[1024];            
			int bytesRead;            
			InputStream inputStream = socket.getInputStream();			
			while ((bytesRead = inputStream.read(buffer)) != -1) 
			{                
				byteArrayOutputStream.write(buffer, 0, bytesRead);                
				response += byteArrayOutputStream.toString("UTF-8");            
			}        
		} catch (UnknownHostException e) 
		{                   
		} catch (IOException e) 
		{                
		}
	}
}