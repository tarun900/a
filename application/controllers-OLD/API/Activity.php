<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/activity_model');
        //date_default_timezone_set("UTC");
    }
    public function getFeeds()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id'); // optional
        $page_no =$this->input->post('page_no');
        
        if($event_id!= '' && $page_no!= '')
        {
            $public_message_feeds = $this->activity_model->getPublicMessageFeeds($event_id,$user_id);
            $photo_feed     = $this->activity_model->getPhotoFeeds($event_id,$user_id);
            $check_in_feed  = $this->activity_model->getCheckInFeeds($event_id,$user_id);
            $rating_feed    = $this->activity_model->getRatingFeeds($event_id,$user_id);
            $user_feed      = $this->activity_model->getUserFeeds($event_id,$user_id);
            $activity_feeds = $this->activity_model->getActivityFeeds($event_id,$user_id);
            
            $data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds);

            function sortByTime($a, $b)
            {
                $a = $a['Time'];
                $b = $b['Time'];

                if ($a == $b)
                {
                    return 0;
                }

                return ($a > $b) ? -1 : 1;
            } 
            if(!empty($data))
            {
                usort($data, 'sortByTime');
                foreach ($data as $key => $value) {
                    $data[$key]['Time'] = $this->activity_model->get_timeago(strtotime($value['Time']));
                }
            }  
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);
            foreach ($data as $key => $value) {
                $comments = $this->activity_model->getComments($value['id'],$value['type'],$user_id); 
                $data[$key]['total_comments'] = count($comments);
                $data[$key]['comments'] = $comments;
            }
            $data = array(
                  'success'     => true,
                  'data'        => $data,
                  'total_page'  => $total_page,
                  'total'       =>$total,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   
    public function getFeedDetails()
    {
        $type   = $this->input->post('type');
        $id     = $this->input->post('id');
        
        if($id!= '' && $type!= '')
        {
            if($type == "activity")
            {
                $feed_details           = $this->activity_model->getFeedDetails($id);
                $image_array            = $feed_details->image;
                $feed_details->image    = json_decode($image_array);
            }

            $data = array(
                  'success' => true,
                  'data'    => ($feed_details) ? $feed_details : "",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   
    public function postUpdate()
    {
        $event_id   = $this->input->post('event_id');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $message    = $this->input->post('message');

        if($event_id!='' && $message!='' && $token!='' && $user_id!='')
        {
            $this->load->model('API/app_login_model');
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                        )
                    );   
            } 
            else 
            {
                $message_data['message']    = $message;
                $message_data['sender_id']  = $user_id;
                $message_data['event_id']   = $event_id;
                $message_data['image']      = "";
                $message_data['time']       = date("Y-m-d H:i:s");

                $message_id = $this->activity_model->savePublicPost($message_data);

                if($message_id!=0)
                {
                    $data = array(
                        'success'       => true,
                        'message'       => "Successfully Saved",
                        'message_id'    => $message_id,
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something went wrong. Please try again.",
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveActivityImages()
    {
        $activity_id    = $this->input->post('activity_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            //copy("./assets/user_files/".$new_image_name,"./assets/user_files/thumbnail/".$new_image_name);
            $images         = $new_image_name;
            $message_data   = $this->activity_model->getMessageDetails($activity_id);
            
            $update_data['image'] = $new_image_name;
            if($message_data->image!='')
            {
                $arr                    = json_decode($message_data->image);
                $arr[]                  = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->activity_model->updateMessageImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->activity_model->updateMessageImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $activity_id=$this->input->post('activity_id');
        if($event_id!='' && $activity_id!='')
        {
            $images = $this->activity_model->getImages($event_id,$activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function feedLike()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $where = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                );
            $like = $this->activity_model->getFeedLike($where);
            if($like!='')
            {
                $update_data = array(
                    'like' => ($like == '1') ? '0'  :  '1',
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->updateFeedLike($update_data,$where);
            }
            else{
                $data = array(
                    'module_type' => $module_type,
                    'module_primary_id' => $module_primary_id,
                    'user_id' => $user_id,
                    'like' => ($like) ? 0  :  1,
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->makeFeedLike($data);
            }    
            $data = array(
                'success' => true,
                'message' => 'Successfully done.',
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function feedComment()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='' && $comment!='')
        {
            $data = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                );
            $id = $this->activity_model->makeFeedComment($data);
              
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => $id,
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveCommentImages()
    {
        $activity_id    = $this->input->post('comment_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
           
            $images         = $target_path;
            $message_data   = $this->activity_model->getCommentDetails($activity_id);
            
            if($message_data->comment_image!='')
            {
                $arr                    = json_decode($message_data->comment_image);
                $arr[]                  = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->activity_model->updateCommentImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->activity_model->updateCommentImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getCommentImageList()
    {
        $activity_id=$this->input->post('comment_id');
        if($activity_id!='')
        {
            $images = $this->activity_model->getCommentImages($activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function deleteFeedComment()
    {
        $comment_id = $this->input->post('comment_id');

        if($comment_id!='')
        {
            $this->activity_model->deleteComment($comment_id);
            $data = array(
                'success' => true,
                'message' => 'Comment deleted successfully',
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
                );
        }
        echo json_encode($data);
    }
}
?>   