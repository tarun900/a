<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller 
{
	public $menu;
	public $cmsmenu;
	
	function __construct() 
	{
		//error_reporting(1);
		parent::__construct();
		$this->load->model('API/app_login_model');
		$this->load->model('API/cms_model');
		$this->load->model('API/event_model');
		$this->load->model('API/message_model');
		$this->load->model('API/attendee_model');
		$this->load->model('API/speaker_model');
		$this->load->model('API/settings_model');
		$this->load->library('Gcm');
		
		$this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
		$this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

		foreach ($this->cmsmenu as $key => $values) 
		{
			$cmsbannerimage_decode = json_decode($values['Images']);
			$cmslogoimage_decode = json_decode($values['Logo_images']);
			$this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
			$this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
		}
	}
	public function getPublicMessages()
	{
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $event_type!='' && $page_no!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_public_msg_coversation($event_id,$page_no,$limit);
				$total_pages=$this->message_model->public_msg_total_pages($event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);

	}
	public function publicMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		if($event_id!='' && $message!='' && $token!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data['Message']=$message;
				$message_data['Sender_id']=$user_id;
				$message_data['Event_id']=$event_id;
				$message_data['Parent']=0;
				$message_data['image']="";
				$message_data['Time']=date("Y-m-d H:i:s");
				$message_data['ispublic']='1';
				$message_data['msg_creator_id']=$user_id;
				$message_id = $this->message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Successfully Saved",
						'message_id' => $message_id
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function public_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data1 	= $this->message_model->getMessageDetails($message_id);
				$newImageName 	= round(microtime(true) * 1000).".jpeg";
				$target_path 	= "././assets/user_files/".$newImageName; 
				$target_path1 	= "././assets/user_files/thumbnail/";         

				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				
				$update_data['image'] = $newImageName;
				if($message_data1[0]['image']!='')
				{
					$arr=json_decode($message_data1[0]['image']);
					$arr[]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->message_model->updateMessageImage($message_id,$update_arr);
				}
				else
				{

					$arr[0]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->message_model->updateMessageImage($message_id,$update_arr);
				}
				$a[0]                           = $newImageName;
				
				$message_data['Message']        = '';
				$message_data['Sender_id']      = $user_id;
				$message_data['Event_id']       = $event_id;
				$message_data['Parent']         = $message_id;
				$message_data['image']          = json_encode($a);
				$message_data['Time']           = date("Y-m-d H:i:s");
				$message_data['ispublic']       = '1';
				$message_data['msg_creator_id'] = $user_id;

				$message_id = $this->message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_message()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!=''  && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->message_model->delete_message($message_id);
				$data = array(
					'success' => true,
					'message' => "Successfully deleted"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function make_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$comment=$this->input->post('comment');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				if($_FILES['image']['name']!='')
				{
					$newImageName = round(microtime(true) * 1000).".jpeg";
					$target_path= "././assets/user_files/".$newImageName; 
					$target_path1= "././assets/user_files/thumbnail/";                 
					move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
					copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
					if($newImageName!='')
					{
						$arr[0]=$newImageName;
						$comment_arr['image']=json_encode($arr);
					}
				}
				$comment_arr['comment']=$comment;
				$comment_arr['user_id']=$user_id;
				$comment_arr['msg_id']=$message_id;
				$comment_arr['Time']=date("Y-m-d H:i:s");;
				$this->message_model->make_comment($comment_arr,$event_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$comment_id=$this->input->post('comment_id');
		if($event_id!='' && $token!='' && $comment_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->message_model->delete_comment($comment_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getAllSpeakersAttendee()
	{
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		if($event_id!='' && $event_type!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$speakers = $this->speaker_model->getSpeakersListByEventId($event_id);
				$attendee = $this->attendee_model->getAttendeeListByEventId($event_id);
				$data = array(
					'success' => true,
					'speakers' => $speakers,
					'attendees' => $attendee
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getPrivateMessages()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_private_section_msg_coversation($event_id,$user_id,$page_no,$limit);
				foreach ($message as $key => $value) {
					$string = $value['message'];
					if($string != strip_tags($string)) 
					{
						$message[$key]['message'] = strip_tags($value['message']);
					}
				}
				$total_pages=$this->message_model->private_msg_total_pages($user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function privateMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		$receiver_id=json_decode($this->input->post('receiver_id'));
		if($event_id!='' && $message!=''  && $user_id!='' && !empty($receiver_id))
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_id = $this->message_model->savePrivateMessage($this->input->post());

				if(!empty($message_id))
				{
					$moderator = $this->message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->message_model->getGcmIds($receiver_id);
						$device = $this->message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->message_model->getSender($user_id);
					
						
					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm();
						
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $message;
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

                          
							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
							
                        }
                        else
                        {
							$msg['title'] = "New Message Received";
							$msg['message'] = $message;
							$msg['tickerText'] = "New Message Received";
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
						}
					}
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function private_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=json_decode($this->input->post('message_id'));
		$receiver_id=json_decode($this->input->post('receiver_id'));

		if($event_id!='' && $token!='' && $user_id!='' && !empty($receiver_id) && !empty($message_id) && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
				foreach ($receiver_id as $key => $value) 
				{
					$message_data1=$this->message_model->getMessageDetails($message_id[$key]);
					if($key==0)
					{
						$newImageName = round(microtime(true) * 1000).".jpeg";
						$target_path= "././assets/user_files/".$newImageName; 
						$target_path1= "././assets/user_files/thumbnail/";                 
						move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
						copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
						$update_data['image'] =$newImageName;
						$old_image=$newImageName;
					}
					else
					{
						$update_data['image'] = $old_image;
					}
					if($message_data1[0]['image']!='')
					{
						$arr=json_decode($message_data1[0]['image']);
						$arr[]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					else
					{

						$arr[0]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					$a[0]=$newImageName;
					$message_data['Message']='';
					$message_data['Sender_id']=$user_id;
					$message_data['Receiver_id']=$value;
					$message_data['Event_id']=$event_id;
					$message_data['Parent']=$message_id[$key];
					$message_data['image']=json_encode($a);
					$message_data['Time']=date("Y-m-d H:i:s");
					$message_data['ispublic']='0';
					$message_data['msg_creator_id']=$user_id;
					$this->message_model->savePrivateImageImage($message_data);


				}
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getImageList()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $message_id!='')
		{
			
				$images = $this->message_model->getImages($event_id,$message_id);

				$data = array(
					'images' => json_decode($images),
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	// 16 Aug 2016
	public function notificationCounter()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$token=$this->input->post('token');
		if($event_id!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$notify_data = $this->message_model->getNotificationCounter($event_id,$user_id);
				$data = array(
					'count' => json_decode(count($notify_data)),
					'notify_data'  => $notify_data,
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function messageRead()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$message_type=$this->input->post('message_type'); // public=1,private=0

		if($event_id!='' && $user_id!=''){
			$result = $this->message_model->updateMessageReadStatus($event_id,$user_id,$message_type);

			if($result){
				$data = array(
					'success' => true,
					);
			}else{
				$data = array(
					'success' => false,
					'message' => "Something went wrong. Please try again.",
					);
			}
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getNotificationListing()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');

		if($event_id!='' && $user_id!=''){
			$result = $this->message_model->getNotificationListing($event_id,$user_id);

			
		$data = array(
			'success' => true,
			'data'	=> $result,
		);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function deleteNotification()
	{
		$notification_id 	= $this->input->post('notification_id');
		$user_id 			= $this->input->post('user_id');

		if($notification_id!='' && $user_id!='')
		{
			$data1['umn_id'] 	= $notification_id;
			$data1['user_id'] 			= $user_id;
			$result = $this->message_model->deleteNotification($data1);

			$data = array(
				'success' 	=> true,
				'message'	=> 'Notification deleted successfully.',
			);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
			);
		}
		echo json_encode($data);
	}
}