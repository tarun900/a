<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_setting extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'Site Setting';
		$this->data['smalltitle'] = 'Mange Site Settings';
		$this->data['breadcrumb'] = 'Site Setting';
		parent::__construct($this->data);
		$this->load->model('Site_Setting_model');
        redirect('Forbidden');
        
	}
	public function index()
	{                                
		$this->data['site_setting'] = $this->Site_Setting_model->get_site_setting();
		$this->template->write_view('content', 'site_setting/index', $this->data , true);
		$this->template->write_view('js', 'site_setting/js', $this->data , true);
		$this->template->render();
	}
        public function update()
        {
                if($_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
                {
                    $tempname = explode('.',$_FILES['userfile']['name']);
                    $tempname[0] = $tempname[0].strtotime(date("Y-m-d H:i:s"));
                    $_FILES['userfile']['name'] = $tempname[0].".".$tempname[1];                
                    $_POST['image_name'] = $_FILES['userfile']['name'];
                    $config['upload_path'] = './assets/user_files';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '100';
                    $config['max_width']  = '1024';
                    $config['max_height']  = '768';
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload())
                    {
                            $error = array('error' => $this->upload->display_errors());
                    }
                    else
                    {
                            $data = array('upload_data' => $this->upload->data());                        
                    }
                }
                $updated = $this->Site_Setting_model->update($_POST);
                $this->session->set_flashdata('site_setting_data', 'Updated');
                redirect('Site_setting');
        }
}