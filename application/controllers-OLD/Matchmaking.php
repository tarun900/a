<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Matchmaking extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Matchmaking';
        $this->data['smalltitle'] = 'Manage Matchmaking';
        $this->data['breadcrumb'] = 'Matchmaking';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $eventid = $this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('forbidden');
        }

        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('59', $module))
        {
            redirect('forbidden');
        }

        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Match making")
            {
                $title = "Match Making";
            }

            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }

        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }

        if ($cnt == 1 && in_array('59', $menu_list))
        {
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('forbidden');
        }
        $this->load->model('Matchmaking_model');
    }

    public function index($id)
    {   
        // show_errors();
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }   


        $this->data['module'] = $this->Matchmaking_model->getModuleName($id);
        $this->data['module_setting'] = $this->Matchmaking_model->getModuleSetting($id);
        $this->data['rules'] = $this->Matchmaking_model->getRule($id);

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $menudata = $this->Event_model->geteventmenu($id, 59);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'matchmaking/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'matchmaking/js', $this->data, true);
        $this->template->render();
    }
    public function saveModuleSetting($event_id)
    {
        $data['attendee'] = $this->input->post('menuid_2') ?: '0';
        $data['exhibitor'] = $this->input->post('menuid_3') ?: '0';
        $data['speaker'] = $this->input->post('menuid_7') ?: '0';
        $data['sponsor'] = $this->input->post('menuid_43') ?: '0';
        $data['event_id'] = $event_id;
        $this->Matchmaking_model->saveModuleSetting($data,$event_id);
        $this->session->set_flashdata('session_data', 'Modules Updated.');
        redirect("Matchmaking/index/" . $event_id);
    }
    public function addRule($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['module'] = $this->Matchmaking_model->getModuleName($id);
        $this->data['custom_column'] = $this->Matchmaking_model->getCustomColumn($id);
        if($this->input->post())
        {   
            $data = $this->input->post();
            $data['event_id'] = $id;
            $this->Matchmaking_model->addRule($data);
            $this->session->set_flashdata('session_data', 'Rule Added');
            redirect("Matchmaking/index/" . $id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'matchmaking/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'matchmaking/js', $this->data, true);
        $this->template->render();
    }
    public function deleteRule($id,$rule_id)
    {   
        $this->Matchmaking_model->deleteRule($rule_id);
        $this->session->set_flashdata('session_data', 'Rule Deleted');
        redirect("Matchmaking/index/" . $id);
    }
    public function editRule($id,$rule_id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['module'] = $this->Matchmaking_model->getModuleName($id);
        $this->data['rule'] = $this->Matchmaking_model->editRule($rule_id);
        $this->data['custom_column'] = $this->Matchmaking_model->getCustomColumn($id);
        if($this->input->post())
        {   
            $data = $this->input->post();
            $this->Matchmaking_model->updateRule($rule_id,$data);
            $this->session->set_flashdata('session_data', 'Rule Updated');
            redirect("Matchmaking/index/" . $id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'matchmaking/edit', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'matchmaking/js', $this->data, true);
        $this->template->render();
    }
}