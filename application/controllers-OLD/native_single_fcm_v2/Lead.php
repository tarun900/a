<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Lead extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/Lead_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
    }

    /*** For Scan Lead  ***/

    public function scan_lead()
    {   
        //error_reporting(1);
        $user_id  =  $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $scan_id = $this->input->post('scan_id');
        $scan_data = $this->input->post('scan_data');
        $flag = 0;
        if(!empty($user_id) && !empty($event_id))
        {   
            
            if($event_id == '634' ||  $event_id == '1012' ||  $event_id == '1378' ||  $event_id == '1512' ||  $event_id == '1609' ||  $event_id == '1580')
            {   

                if(!empty($scan_id))
                {   
                    if (strpos($scan_id, '@') !== false) {
                        $flag = 1;
                    }
                    else
                    {   
                        if($event_id == '1580')
                        {
                            $scan_id = explode('|',$scan_id)[1];
                        }
                        $scan_id = $scan_id;
                    }
                }
                else
                {   
                    if($event_id == '1580')
                    {
                        $scan_data = explode('|',$scan_data)[1];
                    }
                    $scan_id = $scan_data;
                }
            }
            else
            {
                if(!empty($scan_id))
                {   
                    //enter manually
                    if (strpos($scan_id, '@') !== false) {
                        $flag = 1;
                    }
                    else
                    {
                        $tmp = explode('-',$scan_id);
                        $scan_id = $tmp[1];
                    }
                }
                else
                {   
                    if (strpos($scan_data, '@') !== false) {
                        $flag = 1;
                        $scan_id = $scan_data;
                    }
                    else
                    {
                        $tmp = explode('-',$scan_data);
                        $scan_id = $tmp[1];
                    }
                    /*$tmp = explode('-',$scan_data);//scan badge
                    $scan_id = $tmp[1];*/
                }
            }
            $user_info = $this->Lead_model->get_scan_info($event_id,$scan_id,$user_id,$flag);

            if(!empty($user_info) && empty($user_info['message']))
            {
                $custom_column = $this->Lead_model->get_custom_column($event_id);
                //save lead here 6/20/2018 11:11:45 AM
                $login_user_data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                $exhibitor_user_id=$user_id;
                if($login_user_data['Rid']=='4')
                {
                    if($exhibitor_user_id !='')
                    {
                        $exhibitor_user_id=$this->Lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
                    }
                    elseif($exhibitor_user_id =='')
                    {
                        $exhibitor_user_id=$user_id;
                    }
                }
                $lead_user_id = $user_info['Id'];
                $lead_data = array();
                $this->Lead_model->save_scan_lead($lead_data,$event_id,$exhibitor_user_id,$lead_user_id);
                
                $data = array(
                    'user_info' => $user_info,
                    'custom_column' => $custom_column
                ); 
            }
            elseif (!empty($user_info['message']))
            {
                $data = array(
                    'user_info' => new stdClass(),
                    'message' => $user_info['message']
                    );
            }
            else
            {
                $data = array(
                    'user_info' => new stdClass(),
                    'message' => 'Full data for leads that register onsite during the show will be available in the web portal shortly',
                    );
            }

            $data = array(
                'success' =>true,
                'data' => $data
            );

        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Save Scan Lead  ***/

    public function save_scan_lead()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $lead_user_id = $this->input->post('lead_user_id');
        $lead_data['firstname'] = $this->input->post('firstname');
        $lead_data['lastname'] = $this->input->post('lastname');
        $lead_data['email'] = $this->input->post('email');
        $lead_data['company_name'] = $this->input->post('company_name');
        $lead_data['title'] = $this->input->post('title');
        $lead_data['custom_column_data'] = $this->input->post('custom_column_data');
        $lead_data['event_id'] = $this->input->post('event_id');
        $lead_data['salutation'] = $this->input->post('salutation');
        $lead_data['country'] = $this->input->post('country');
        $lead_data['mobile'] = $this->input->post('mobile');
        $lead_data['badgeNumber'] = $this->input->post('badgeNumber');
        $isFirstTimeScan = $this->input->post('isFirstTimeScan');

        if(!empty($user_id) && !empty($event_id))
        {   
            $login_user_data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            if($login_user_data['Rid']=='4')
            {
                if($exhibitor_user_id !='')
                {
                    $exhibitor_user_id=$this->Lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
                }
                elseif($exhibitor_user_id =='')
                {
                    $exhibitor_user_id=$user_id;
                }
                //echo $exhibitor_user_id; exit();
            }
            if($isFirstTimeScan == '1')
            {
                $update['Firstname'] = $lead_data['firstname'];
                $update['Lastname'] = $lead_data['lastname'];
                $update['Email'] = $lead_data['email'];
                $update['Company_name'] = $lead_data['company_name'];
                $update['Title'] = $lead_data['title'];
                $update['Salutation'] = $lead_data['salutation'];
                $country_id = $this->db->where('country_name',$lead_data['country'])->get('country')->row_array()['id'];
                $update['Country'] = ($country_id)?:$lead_data['country'];
                $update['Mobile'] = $lead_data['mobile'];
                $this->db->where('Id',$lead_user_id)->update('user',$update);
            }
            
            $this->Lead_model->save_scan_lead($lead_data,$event_id,$exhibitor_user_id,$lead_user_id);
            $survey = $this->Lead_model->get_all_exibitor_user_questions($event_id,$exhibitor_user_id,$lead_user_id);
            $data = array(
                'success' =>true,
                'survey' => $survey,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Save Questions  ***/

    public function save_questions()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');    
        $lead_user_id = $this->input->post('lead_user_id');
        $survey_data = json_decode($this->input->post('survey_data'),true);

        if(!empty($user_id) && !empty($event_id) && !empty($lead_user_id))
        {	
            $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);
        	
            foreach ($survey_data as $key => $value)
        	{
        		$survey_data[$key]['event_id'] = $event_id;
        		$survey_data[$key]['user_id'] = $lead_user_id;
                $survey_data[$key]['exhi_id'] = $rep['exibitor_user_id'];
                if(!empty($rep['rep_id']))
                {
                    $survey_data[$key]['rep_id'] = $rep['rep_id'];
                }
        		$survey_data[$key]['answer_date'] = date('Y-m-d H:i:s');
        	}
        	$this->Lead_model->add_question_answer($survey_data);

        	$data = array(
        		'success' => true,
        		'message' => "Lead added Successfully"
        		);
        }   
        else
        {
            $data = array(
                'success' => false ,
                'message' =>'Invalid parameters'
                );
        }
        echo json_encode($data);
    }

    /*** For Get Exhibior Leads ***/

    public function get_exhi_leads()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($user_id) && !empty($event_id))
        {   
            $login_user_data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);

            if(!empty($rep['rep_id']))
            {
                $exhibitor_user_id=$rep['rep_id'];
                $flag = 1;
            }

            
            if($keyword!='')
            $where = "(el.lastname like '%".$keyword."%' OR el.firstname like '%".$keyword."%' OR el.company_name like '%".$keyword."%')";

            $leads = $this->Lead_model->get_all_my_lead_by_exibitor_user($event_id,$exhibitor_user_id,$where,$flag);
           
            
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($leads);
            $total_page     = ceil($total/$limit);
            $leads          = array_slice($leads,$start,$limit);

            $data = array(
                'leads' => $leads,
                'total_page' => $total_page,
            );
            $data = array(
                'success' =>true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Get Representatives Lists ***/

    public function rep_list()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($event_id) && !empty($user_id))
        {
            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%')";

            $total_rep=$this->Lead_model->get_all_my_representative_by_exibitor_user($event_id,$user_id,$where);
            

            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($total_rep);
            $total_page     = ceil($total/$limit);
            $total_rep      = array_slice($total_rep,$start,$limit);

            $data = array(
                'rep_list' => $total_rep,
                'total_page' => $total_page,
                
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);       
    }

    /*** For Add New Representative ***/

    public function add_new_representative()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $rep_id = json_decode($this->input->post('rep_id'),true);

        if(!empty($user_id) && !empty($event_id) && !empty($rep_id))
        {
            if(!empty($rep_id))
            {
                foreach ($rep_id as $value) 
                {   
                    $this->Lead_model->add_new_representative($event_id,$user_id,$value); 
                }
            }
            $data = array(
                'success' => true,
                'message' => 'Representative Added Successfully'
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /*** For Get Attebdee List of Representative ***/

    public function get_attendee_list_for_rep()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($user_id) && !empty($event_id))
        {   
            $max=$this->Lead_model->get_exibitor_user_premission($event_id,$user_id);

            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%')";

            $attendee_list = $this->Lead_model->get_event_all_attendee_from_assing_resp($event_id,$where);
            
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($attendee_list);
            $total_page     = ceil($total/$limit);
            $attendee_list  = array_slice($attendee_list,$start,$limit);

            $data = array(
                'list' => $attendee_list,
                'total_page' => $total_page,
                'limit' =>$max
                );

            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Remove Representative ***/

    public function remove_rep()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $rep_id = $this->input->post('rep_id');
        if(!empty($event_id) && !empty($rep_id))
        {   
            $this->Lead_model->remove_my_representative($event_id,$rep_id);
            $data = array(
                'message' => 'Representative Removed'
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        } 
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Export Leads ***/

    public function export_lead()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $send_email = $this->input->post('send_email');

        if(!empty($event_id) && !empty($user_id))
        {
            $login_user_data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            if($login_user_data['Rid']=='4')
            {
                $exhibitor_user_id=$this->Lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
            }

            $custom_column = $this->Lead_model->get_custom_column($event_id);
            $questions=$this->Lead_model->get_all_exibitor_user_questions_for_export($event_id,$exhibitor_user_id);
            $filename = "./assets/lead_csv/lead_user_detail.csv";
            $fp = fopen($filename, 'w');
            $header[] = "FirstName";
            $header[] = "LastName";
            $header[] = "Email";
            $header[] = "Title";
            $header[] = "Company Name";
            foreach ($custom_column as $key => $value)
            {
                $header[]=ucfirst($value['column_name']);
            }
            foreach ($questions as $key => $value)
            {
                $header[]=$value['Question'];
            }
            
            fputcsv($fp, $header);
            $my_lead = $this->Lead_model->get_all_my_lead_by_exibitor_user($event_id,$exhibitor_user_id);
            foreach ($my_lead as $key => $value) 
            {
                $data['FirstName']=$value['Firstname'];
                $data['LastName']=$value['Lastname'];
                $data['Email']=$value['Email'];
                $data['Title']=$value['Title'];
                $data['Company Name']=$value['Company_name'];
                $custom_column_data=json_decode($value['custom_column_data'],true);
                foreach ($custom_column as $ckey => $cvalue) 
                {
                    $data[ucfirst($cvalue['column_name'])]=$custom_column_data[$cvalue['column_name']];
                }
                foreach ($questions as $qkey => $qvalue) 
                {
                    $ans=$this->Lead_model->get_user_question_answer($event_id,$value['Id'],$qvalue['q_id'],$user_id);
                    $data[]= !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment']; 
                }
                fputcsv($fp, $data);
                unset($data);
            }
            fclose($fp);
            file_put_contents($filename, $fp);
            if($send_email)
            {   
                $event_name = $this->Lead_model->get_event_name($event_id);
                $this->load->library('email');
                $config['protocol']   = 'smtp';
                $config['smtp_host']  = 'localhost';
                $config['smtp_port']  = '25';
                $config['smtp_user']  = 'invite@allintheloop.com';
                $config['smtp_pass']  = 'xHi$&h9M)x9m';
                $config['_encoding']  = 'base64';
                $config['mailtype']   = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com',$event_name);
                $this->email->to($login_user_data['Email']);
                //$this->email->to('jagdish@elsner.com.au');
                $this->email->subject("My Lead Csv File");
                $this->email->message("Please find below Attachment");
                $this->email->attach($filename);
                $this->email->send();
                $this->email->clear(TRUE);
                $data['message'] = "CSV email successfully";
            }
            else
            {
            $data = array(
                'url' => base_url().'assets/lead_csv/lead_user_detail.csv',
                'message' => ""
                );    
            }
            

            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /*** For Get Offline Data ***/

    public function get_offline_data()
    {   
        $event_id = $this->input->post('event_id');
        $exhi_id = $this->input->post('exhi_id');
        
        if(!empty($event_id) && !empty($exhi_id))
        {
            $custom_column = $this->Lead_model->get_custom_column($event_id);
            $survey = $this->Lead_model->get_all_exibitor_user_questions($event_id,$exhi_id,'1');
            $data = array(
                'success' => true,
                'survey' => $survey,
                'custom_column' => $custom_column,
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /*** For Save Scan Data ***/

    public function save_scan_upload()
    {   
            
        $event_id = $this->input->post('event_id');
        $user_id  = $this->input->post('user_id');
        $scan_id = $this->input->post('scan_id');
        $scan_data = $this->input->post('scan_data');
        $created_date = $this->input->post('created_date'); 
        $upload_lead = json_decode($this->input->post('upload_lead'),true);
        $survey_data = json_decode($this->input->post('survey_data'),true);
        $test_lead   = $this->input->post('test_lead');
        
        if(!empty($event_id) && !empty($user_id))
        {    

            if($event_id == '1580')
            {
                $scan_data = explode('|',$scan_data)[1];
            }
            $scan_id = $this->Lead_model->get_user_by_id($event_id,$scan_data,$upload_lead);
            
            if(!empty($scan_id))
            {   

                $exhibitor_user_id=$user_id;
                $upload_lead['custom_column_data'] = !empty($upload_lead['custom_column_data']) ? json_encode($upload_lead['custom_column_data']) : NULL;
                if($event_id == '634' ||  $event_id == '1012' || $event_id == '1378' ||  $event_id == '1512' ||  $event_id == '1609' ||  $event_id == '1580')
                {
                    $upload_lead['badgeNumber'] = $scan_data;
                }

                $message = "";

                $lead_msg = $this->Lead_model->save_scan_lead($upload_lead,$event_id,$exhibitor_user_id,$scan_id,$created_date,$test_lead);
                
                if($lead_msg)
                {       
                    if(!empty($survey_data))
                    {   
                        $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);
                        

                        // $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);
            
                        foreach ($survey_data as $key => $value)
                        {
                            $vals = $value;
                            $vals['event_id'] = $event_id;
                            $vals['user_id'] = $scan_id;
                            $vals['answer_date'] = !empty($created_date) ? $created_date :date('Y-m-d H:i:s');
                            $vals['exhi_id'] = $rep['exibitor_user_id'];
                            if(!empty($rep['rep_id']))
                            {
                                $vals['rep_id'] = $rep['rep_id'];
                            }

                            $where['event_id'] = $event_id;
                            $where['user_id'] = $scan_id;
                            $where['exhi_id'] = $rep['exibitor_user_id'];
                            if(!empty($rep['rep_id']))
                            {
                                $where['rep_id'] = $rep['rep_id'];
                            }
                            $where['exibitor_questionid'] = $value['exibitor_questionid'];
                            $this->Lead_model->add_update_question_answer($vals,$where);
                        }
                        
                        /*foreach ($survey_data as $key => $value)
                        {
                            $survey_data[$key]['event_id'] = $event_id;
                            $survey_data[$key]['user_id'] = $scan_id;
                            $survey_data[$key]['exhi_id'] = $rep['exibitor_user_id'];
                            if(!empty($rep['rep_id']))
                            {
                                $survey_data[$key]['rep_id'] = $rep['rep_id'];
                            }
                            $survey_data[$key]['answer_date'] = !empty($created_date) ? $created_date :date('Y-m-d H:i:s');
                        }
                        $this->Lead_model->add_question_answer($survey_data);*/
                    }
                    $message = "Lead scanned successfully.";
                }
                else
                {
                    $message = "Lead is already scanned.";
                }

            }
            $data = array(
                'success' => true,
                'message'    => $message
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /*** For Get Lead Details ***/

    public function get_lead_details()
    {
        $lead_id = $this->input->post('lead_id');
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        
        if(!empty($user_id) && !empty($event_id) && !empty($lead_id))
        {   
            $leads = $this->Lead_model->getLeadDetails($event_id,$lead_id,$user_id);
            $survey = $this->Lead_model->get_all_exibitor_user_questions($event_id,$user_id,$lead_id);
            foreach ($survey as $key => $value)
            {
                $ans = $this->Lead_model->get_user_question_answer($event_id,$lead_id,$value['q_id'],$user_id);
                if($value['Question_type'] == '2')
                    $survey[$key]['survey_que_ans'] = ($ans[0]['Answer']) ? explode(',', $ans[0]['Answer']) : [];
                elseif(($ans[0]['Answer'])!='')
                    $survey[$key]['survey_que_ans'][] =  $ans[0]['Answer'] ;
                else
                    $survey[$key]['survey_que_ans'] = [];
            }
            if(count($leads))
            {
                $custom_column_data=json_decode($leads['custom_column_data'],true);
               
                $extra = [];
                foreach ($custom_column_data as $ckey => $cvalue) 
                {
                    $cdata['Que']=$ckey;
                    $cdata['Ans']=$cvalue;
                    $extra[] = $cdata;
                }
                $leads['custom_column_data'] = $extra;
                $leads['survey'] = $survey;
            }
          
            $data = array(
                'leads' => $leads,
                'custom_column' => $this->Lead_model->get_custom_column($event_id)

            );
            $data = array(
                'success' =>true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Update Leads ***/

    public function updateLead()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $lead_user_id = $this->input->post('lead_user_id');
        $lead_data['firstname'] = $this->input->post('firstname');
        $lead_data['lastname'] = $this->input->post('lastname');
        $lead_data['email'] = $this->input->post('email');
        $lead_data['company_name'] = $this->input->post('company_name');
        $lead_data['title'] = $this->input->post('title');
        $lead_data['salutation'] = $this->input->post('salutation');
        $lead_data['country'] = $this->input->post('country');
        $lead_data['mobile'] = $this->input->post('mobile');
        $lead_data['badgeNumber'] = $this->input->post('badgeNumber');
        $lead_data['custom_column_data'] = $this->input->post('custom_column_data');
        $lead_data['event_id'] = $this->input->post('event_id');
        $isFirstTimeScan = $this->input->post('isFirstTimeScan');

        if(!empty($user_id) && !empty($event_id))
        {   
            if(empty($lead_data['badgeNumber']))
                unset($lead_data['badgeNumber']);

            if($isFirstTimeScan == '1')
            {
                $update['Firstname'] = $lead_data['firstname'];
                $update['Lastname'] = $lead_data['lastname'];
                $update['Email'] = $lead_data['email'];
                $update['Company_name'] = $lead_data['company_name'];
                $update['Title'] = $lead_data['title'];
                $update['Salutation'] = $lead_data['salutation'];
                $country_id = $this->db->where('country_name',$lead_data['country'])->get('country')->row_array()['id'];
                $update['Country'] = ($country_id)?:$lead_data['country'];
                $update['Mobile'] = $lead_data['mobile'];
                $this->db->where('Id',$lead_user_id)->update('user',$update);
            }
            $this->Lead_model->update_scan_lead($lead_data,$event_id,$user_id,$lead_user_id);
            $survey = $this->Lead_model->get_all_exibitor_user_questions($event_id,$user_id,$lead_user_id);
            $data = array(
                'success' =>true,
                'survey' => $survey,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** For Update Question Answers ***/

    public function update_questions_answer()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');    
        $lead_user_id = $this->input->post('lead_user_id');
        $update_data = json_decode($this->input->post('survey_data'),true);
        if(!empty($user_id) && !empty($event_id) && !empty($lead_user_id))
        {   

            $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);
            
            foreach ($update_data as $key => $value)
            {
                $vals = $value;
                $vals['event_id'] = $event_id;
                $vals['user_id'] = $lead_user_id;
                $vals['answer_date'] = date('Y-m-d H:i:s');
                $vals['exhi_id'] = $rep['exibitor_user_id'];
                if(!empty($rep['rep_id']))
                {
                    $vals['rep_id'] = $rep['rep_id'];
                }

                $where['event_id'] = $event_id;
                $where['user_id'] = $lead_user_id;
                $where['exhi_id'] = $rep['exibitor_user_id'];
                if(!empty($rep['rep_id']))
                {
                    $where['rep_id'] = $rep['rep_id'];
                }
                $where['exibitor_questionid'] = $value['exibitor_questionid'];
                $this->Lead_model->add_update_question_answer($vals,$where);
            }

            $data = array(
                'success' => true,
                'message' => "Lead Updated Successfully"
                );
        }   
        else
        {
            $data = array(
                'success' => false ,
                'message' =>'Invalid parameters'
                );
        }
        echo json_encode($data);
    }

    /*** For Reset Question Answers ***/

    public function reset_questions_answer()
    {
        $event_id = $this->input->post('event_id');    
        $lead_user_id = $this->input->post('lead_user_id');
        $user_id = $this->input->post('user_id');

        if(!empty($lead_user_id) && !empty($event_id))
        {   
           
            $where['event_id'] = $event_id;
            $where['user_id'] = $lead_user_id;
            $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);

            if(!empty($rep['rep_id']))
            {
                $where['rep_id'] = $rep['rep_id'];
            }
            else
            {
                $where['exhi_id'] = $user_id;
            }

            $this->Lead_model->reset_questions_answer($where);
           
            $data = array(
                'success' => true,
                'message' => "Reset Successfully"
                );
        }   
        else
        {
            $data = array(
                'success' => false ,
                'message' =>'Invalid parameters'
                );
        }
        echo json_encode($data);
    }

    /*** For Exhi Leads Offline ***/

    public function get_exhi_leads_offline()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');

        if(!empty($user_id) && !empty($event_id))
        {   
            $login_user_data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            $rep =$this->Lead_model->get_lead_rep($event_id,$user_id);

            if(!empty($rep['rep_id']))
            {
                $exhibitor_user_id=$rep['rep_id'];
                $flag = 1;
            }


            $leads = $this->Lead_model->get_all_my_lead_by_exibitor_user($event_id,$exhibitor_user_id,$where,$flag);
            foreach ($leads as $key => $value)
            {   
                $lead_id = $value['Id'];
                $leads_tmp = $this->Lead_model->getLeadDetails($event_id,$lead_id,$user_id);
                $survey = $this->Lead_model->get_all_exibitor_user_questions($event_id,$user_id,$lead_id);
                foreach ($survey as $key1 => $value1)
                {
                    $ans = $this->Lead_model->get_user_question_answer($event_id,$lead_id,$value1['q_id'],$user_id);
                    if($value1['Question_type'] == '2')
                        $survey[$key1]['survey_que_ans'] = ($ans[0]['Answer']) ? explode(',', $ans[0]['Answer']) : [];
                    elseif(($ans[0]['Answer'])!='')
                        $survey[$key1]['survey_que_ans'][] =  $ans[0]['Answer'] ;
                    else
                        $survey[$key1]['survey_que_ans'] = [];
                }
                if(count($leads_tmp))
                {
                    $custom_column_data=json_decode($leads_tmp['custom_column_data'],true);
                   
                    $extra = [];
                    foreach ($custom_column_data as $ckey => $cvalue) 
                    {
                        $cdata['Que']=$ckey;
                        $cdata['Ans']=$cvalue;
                        $extra[] = $cdata;
                    }
                    $leads_tmp['custom_column_data'] = $extra;
                    $leads_tmp['survey'] = $survey;
                }
              
                $data = array(
                    'leads' => $leads_tmp,
                    'custom_column' => $this->Lead_model->get_custom_column($event_id)

                );
                $leads[$key]['data'] = $data;
            }
            $data = array(
                'leads' => $leads,
            );
            $data = array(
                'success' =>true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}