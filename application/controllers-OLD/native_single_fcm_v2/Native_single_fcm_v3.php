<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Native_single_fcm_v3 extends CI_Controller
{
	function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/native_single_fcm_v3_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Settings_model');
    }

    public function getL3techDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getL3techDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkL3techVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'L3tech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getLearn2019DefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getLearn2019DefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
               $event[$key]['default_lang']['3__stand'] = '';
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function Learn2019VersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Learn2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getParaxelEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1779');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function getUKCongressDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1740');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
               $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Stream';
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function UKCongressVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'UKCongress';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getAEI2019DefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1830');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function AEI2019VersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'AEI2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function checkVenturefestVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Venturefest';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function VenturefestSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v3_model->VenturefestSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }


     public function getVenturefestAllPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getVenturefestAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);

               if($event[$key]['default_lang']['lang_id'] == '978')
               {
                 $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Stream';
               }
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }


     public function checkRiskUsaVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'RiskEvents';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function RiskUsaSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v3_model->RiskUsaSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }


    public function getRiskUsaAllPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getRiskUsaAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function GlobalWealthTechDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1995');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function GlobalWealthTechVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'GlobalWealthTech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function OlukaiVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Olukai';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function OlukaiDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1809');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function OilCouncilVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'OilCouncil';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function OilCouncilDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1635');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
              
            }
            $data  = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkWatersUsaVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'WatersUSA';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function WatersUsaSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v3_model->WatersUsaSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }


    public function getWatersUsaAllPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getWaterskUsaAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function getInnovarioDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1353');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function InnovarioVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Innovario';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function CrugatherSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v3_model->CrugatherSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }


    public function getCrugatherAllPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getCrugatherAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkCrugatherVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Crugather';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getOffshoreDecommissioningConference2018DefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1667');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function OffshoreDecommissioningConference2018VersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'OffshoreDecommissioningConference2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getTheManufacturingExpoDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1864');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function TheManufacturingExpoVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'TheManufacturingExpo';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getOEBBerlinDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1757');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function OEBBerlinVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'OEBBerlin';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

     public function getSolarPowerEuropeDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1808');
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function SolarPowerEuropeVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'SolarPowerEurope';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

}