<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matchmaking extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('native_single_fcm_v2/Matchmaking_model');
	}
	public function getModules()
	{
		extract($this->input->post());
		if(!empty($event_id))
		{
			$modules = $this->Matchmaking_model->getModules($event_id);
			if($modules['attendee'] == '1')
				$menu_id[] = '2';
			if($modules['exhibitor'] == '1')
				$menu_id[] = '3';
			if($modules['speaker'] == '1')
				$menu_id[] = '7';
			if($modules['sponsor'] == '1')
				$menu_id[] = '43';
			$modules_name = $this->Matchmaking_model->getModuleName($event_id,$menu_id);
			$data = array(
				'success' => true,
				'modules_name' => $modules_name
				);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function getAttendees()
	{
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id) && !empty($role_id))
		{
			$modules = $this->Matchmaking_model->getModules($event_id);

			if($keyword!='')
            $where = "((u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%' OR u.Title like '%".$keyword."%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";

			if($modules['attendee'] == '1')
				$data = $this->Matchmaking_model->getAttendees($event_id,$user_id,$role_id,$where);
			else
				$data = [];

			$limit          = 20;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);

			$data = array(
				'success' => true,
				'data' => $data,
				'total_page'  => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function getExhibitor()
	{
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id) && !empty($role_id))
		{
			$modules = $this->Matchmaking_model->getModules($event_id);
			if($keyword != '')
			$where = "(Short_desc like '%".$keyword."%' or Heading like '%".$keyword."%' or u.Firstname like '%".$keyword."%' or u.Lastname like '%".$keyword."%')";

			if($modules['exhibitor'] == '1')
				$data = $this->Matchmaking_model->getExhibitor($event_id,$user_id,$role_id,$where);
			else
				$data = [];

			$limit          = 20;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);

			$data = array(
				'success' => true,
				'data' => $data,
				'total_page'  => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function getSpeaker()
	{
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id) && !empty($role_id))
		{
			$modules = $this->Matchmaking_model->getModules($event_id);

			if($keyword!='')
            $where = "((u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%' OR u.Title like '%".$keyword."%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('".$keyword."', ' ', '%'), '%'))";

			if($modules['speaker'] == '1')
				$data = $this->Matchmaking_model->getSpeaker($event_id,$user_id,$role_id,$where);
			else
				$data = [];

			$limit          = 20;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);
            
			$data = array(
				'success' => true,
				'data' => $data,
				'total_page'  => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function getSponsor()
	{
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id) && !empty($role_id))
		{
			$modules = $this->Matchmaking_model->getModules($event_id);
			if($keyword!='')
            $where = "(s.Sponsors_name like '%".$keyword."%' OR s.Company_name like '%".$keyword."%')";

			if($modules['sponsor'] == '1')
				$data = $this->Matchmaking_model->getSponsor($event_id,$user_id,$role_id,$where);
			else
				$data = [];

			$limit          = 20;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);

			$data = array(
				'success' => true,
				'data' => $data,
				'total_page'  => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function markAsVisited()
	{
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id) && !empty($visited_id) && !empty($menu_id))
		{
			$this->Matchmaking_model->markAsVisited($this->input->post());
			$data = array(
				'success' => true,
				'message' => 'Marked as Visited'
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
}

/* End of file matchmaking.php */
/* Location: ./application/controllers/matchmaking.php */