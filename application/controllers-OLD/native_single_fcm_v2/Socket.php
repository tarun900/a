<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Socket extends CI_Controller 
{
	public $addresses;
	public $presentation;
	public $to_id;
	public $connected;

	function __construct() 
	{
		error_reporting(0);
        ini_set('error_reporting', off);
        ini_set('display_errors', off);
        ini_set('log_errors', off);
		parent::__construct();
		$this->load->model('native_single_fcm_v2/App_login_model');
		$this->load->model('native_single_fcm_v2/Cms_model');
		$this->load->model('native_single_fcm_v2/Event_model');
		$this->load->model('native_single_fcm_v2/Message_model');
		$this->load->model('native_single_fcm_v2/Attendee_model');
		$this->load->model('native_single_fcm_v2/Speaker_model');
		$this->load->model('native_single_fcm_v2/Settings_model');
		$this->load->model('native_single_fcm_v2/Gamification_model');
        $this->load->model('native_single_fcm_v2/Presentation_model');
		include('application/libraries/nativeGcm.php');
        include('application/libraries/FcmV2.php');
	}
	public function index()
    {   
        $this->startSocketServer();
        if($this->sock)
        {
            socket_listen( $this->sock );
            while($client = socket_accept( $this->sock ) )
            {
                $data = '';
                while($i = socket_read($client,10000000,PHP_BINARY_READ))
                {
                    $data .= $i;
                    if($this->isJson($data))
                        break;
                }
                $temp = json_decode($data,true);

                switch ($temp['operation']) {
                    case '1': // 1 presentation
                        $this->doPresentation($data,$client);
                        break;
                    case '2': // 2 = message
                        $this->doMessaging($data,$client);
                        break;
                    case '3': // 3 = survey 
                        $this->doSurvey($data,$client);
                        break;
                    default:
                        continue;
                }
            }
        }
    }
    public function doPresentation($data,$client)
    {
        $data = json_decode($data,true);
        //print_r($data);exit;
        $type = $data['user_type'];
        $p_id = $data['p_id'];
    	$gcm_id = $data['gcm_id'];
        socket_getpeername ( $client , $address ,$port );
        if($type == "P")
        {
            //$msg['change'] = $data['change'];
            //print_r($this->presentation);exit;
            $msg['p_id'] = $p_id;
            $write = "200";
        	foreach ($this->presentation[$p_id] as $key => $value) {
        		$sent = socket_write($value,$write,strlen($write));
                /*if($this->addresses[$p_id][$key]['isandroid'] == '1')
        		  socket_close($value);*/
               // echo $sent." p_id = ".$p_id;
        	}
            socket_close($client);
            $this->addresses[$p_id] = null;
            $this->presentation[$p_id] = null;
        }
        else
        {
            $this->addresses[$p_id][] = $data;
            $this->presentation[$p_id][] = $client;
        }
       
    }
    public function doSurvey($data,$client)
    {
        $write = 'connected native_single_fcm 3';
        $sent = socket_write($client,$write,strlen($write));
        $data = json_decode($data,true);
        $user_id = $data['user_id'];
        $survay_id = $data['survay_id'];
        $ans = $data['ans'];
        if($user_id!='' && $survay_id!='' && $ans!='')
        {
            $save_ans['Question_id']=$survay_id;
            $save_ans['User_id']=$user_id;
            $save_ans['Answer']=$ans;
            $save_ans['answer_date']=date('Y-m-d H:i:s');
            $this->Presentation_model->save_ans_in_presentation($save_ans);
            $data1 = array(
                'success' => true,
                'message' => 'Successfully saved',
            );

        }
        else
        {
            $data1 = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        /*$write = json_encode($data1);
        $sent = socket_write($client,$write,strlen($write));*/
        $write = '200';
        $sent = socket_write($client,$write,strlen($write));
        //socket_close($client);
       
    }
    public function doMessaging($data,$client)
    {
        $data = json_decode($data,true);
        if(!array_key_exists('message', $data) && !array_key_exists('image', $data) )
        {
            if(!in_array($data['user_id'], $this->to_id))
            {
                $this->to_id[]        = $data['user_id'];
                $this->connected[]    = $client;
            }
            continue;
        }

        $event_id       = $data['event_id'];
        $token          = $data['token'];
        $user_id        = $data['user_id'];
        $message        = $data['message'];
        $receiver_id    = $data['receiver_id'];     
        $limit          = 10;     
        $page_no        = 1;
        socket_getpeername ( $client , $address ,$port );
        $flag = 0 ;
      

        if(array_key_exists('image', $data))
        {
            $newImageName = time().date('Ymd').".jpeg";
            $ifp = fopen( "././assets/user_files/$newImageName", 'wb' ); 
            fwrite( $ifp, base64_decode( $data['image'] ) );
            fclose( $ifp ); 
            copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
            $message_data['Message']='';
            $message_data['Sender_id']=$user_id;
            $message_data['Receiver_id']=$receiver_id;
            $message_data['Event_id']=$event_id;
            $arr[0] = $newImageName;
            $message_data['image']=json_encode($arr);
            $message_data['Time']=date("Y-m-d H:i:s");
            $message_data['ispublic']='0';
            $message_data['msg_creator_id']=$user_id;
            $message_data['is_read'] = (in_array($data['receiver_id'], $this->to_id)  ) ? '1' : '0';
            $message_id = $this->Message_model->savePrivateImageImageOnly($message_data);
        }
        else
        {
            $post_data['event_id']      = $data['event_id'];
            $post_data['token']         = $data['token'];
            $post_data['user_id']       = $data['user_id'];
            $post_data['message']       = $data['message'];
            $post_data['receiver_id']   = $data['receiver_id'];
            $post_data['receiver_id']   = $this->Message_model->getUserIdByEx($receiver_id,$event_id);
            $post_data['is_read'] = (in_array($data['receiver_id'], $this->to_id)  ) ? '1' : '0';
            $message_id     = $this->Message_model->savePrivateMessageText($post_data);

        }
        $m_data         = $this->Message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
        $m_data[0]['image'] = $data['image'];
        if(in_array($data['receiver_id'], $this->to_id)  )
        {
            $key        = array_search($data['receiver_id'], $this->to_id);
            
            $sent   = socket_write($this->connected[$key],json_encode($m_data[0]),strlen(json_encode($m_data[0])));
            $flag   = 1;
            socket_close($this->connected[$key]);
            unset($this->to_id[$key]);
            unset($this->connected[$key]);
            
        }
        
        $sent   = socket_write($client,json_encode($m_data[0]),strlen(json_encode($m_data[0])));
        $key    = array_search($data['user_id'], $this->to_id);
        socket_close($client);
       
        unset($this->to_id[$key]);
        unset($this->connected[$key]);
        if(!empty($message_id))
        {
            $moderator = $this->Message_model->getModerators($speaker_id,$event_id);
            if(empty($moderator))
            {
                $gcm_ids    = $this->Message_model->getGcmIds($receiver_id);
                $device     = $this->Message_model->getDevice($receiver_id);
            }
            else
            {
                $gcm_ids    = $this->Message_model->getModeratorsGcmIds($receiver_id,$event_id);
                $device     = $this->Message_model->getModeratorDevice($receiver_id,$event_id);
            }
            $sender         = $this->Message_model->getSender($user_id);
            
            $message        = $this->Message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
            $sender_detail  = $this->Message_model->getSenderDetails($receiver_id,$event_id);
            foreach ($message as $key => $value) {
                $string         = $value['message'];
                $message[$key]['message'] = strip_tags($value['message']);
            }
            $total_pages    = $this->Message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);
            $obj        = new Gcm($event_id);
            $obj1        = new Fcm();
            foreach ($gcm_ids as $key => $value) {
                $template   = $this->Message_model->getNotificationTemplate($event_id,'Message');
                if($flag == 0)
                {
                    if($device[$key]['device'] == "Iphone")
                    {
                        
                        $msg                    =  $template['Content'];
                        $extra['sender_name']   = $sender;
                        $extra['message_type']  = 'Private';
                        $extra['message_id']    = $user_id;
                        $extra['event']         = $this->Settings_model->event_name($event_id);
                        $extra['title']         = $template['Slug'];
                        $result = $obj1->send(1,$value['gcm_id'],$msg,$extra,$device[$key]['device'],$event_id);
                    }
                    else
                    {
                        $msg['title']           = $template['Slug'];
                        $msg['message']         = $template['Content'];
                        $extra['sender_name']   = $sender;
                        $extra['message_type']  = 'Private';
                        $extra['message_id']    = $user_id;
                        $extra['event']         = $this->Settings_model->event_name($event_id);
                        $msg['vibrate']         = 1;
                        $msg['sound']           = 1;
                        $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
                    }
                }
                $this->add_user_game_point($user_id,$event_id,1);
                if($template['send_email'] == '1' && !$flag)
                {
                    //$this->Event_model->sendEmailToAttendees($event_id,$template['email_content'],$value['Email'],$template['email_subject']);
                }
            }
        }
        
    }
    public function startSocketServer()
    {
        $address=$this->config->item('ip');
        $port=$this->config->item('port');
        $this->sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
        socket_set_option($this->sock, SOL_SOCKET, SO_REUSEADDR, 1) ;
        if(!socket_bind( $this->sock, 0, $port ))
        {
            socket_connect($this->sock, $address, $port) ;
        }
    }
    
    public function isJson($string) {
		 json_decode($string);
		 return (json_last_error() == JSON_ERROR_NONE);
	}
}
