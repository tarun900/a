<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Favorites extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/favorites_model');
        $this->load->model('native_single_fcm_v2/Attendee_model');
        $this->load->model('native_single_fcm_v2/Speaker_model');
        $this->load->model('native_single_fcm_v2/Sponsors_model');
        $this->load->model('native_single_fcm_v2/Exhibitor_model');
        $this->load->model('native_single_fcm_v2/Message_model');
    }
    
    /*** Add or Remove from Favorites ***/

    public function addOrRemove()
    {
        $event_id       = $this->input->post('event_id');
        $user_id        = $this->input->post('user_id');
        $module_id      = $this->input->post('module_id');
        $module_type    = $this->input->post('module_type'); 
        $token          = $this->input->post('_token'); 
        $page_no        = $this->input->post('page_no'); 

        if($event_id!='' && $user_id!='' && $module_id!='' && $module_type!='')
        {
            $favorites_data['event_id']     = $event_id;
            $favorites_data['user_id']      = $user_id;
            $favorites_data['module_id']    = $module_id;
            $favorites_data['module_type']  = $module_type;

            $result = $this->favorites_model->addOrRemoveFavorites($favorites_data);
            switch ($module_type) {
                case '2':// attendee
                        $attendee = $this->Attendee_model->getAttendeeListByEventId_new($event_id,"",'',$user_id);
                        $contact_attendee = $this->Attendee_model->getContactAttendeeListByEventId_new($event_id,$user_id);
                        $data = array(
                            'attendee_list' => $attendee['attendees'],
                            'contact_attendee' => $contact_attendee['attendees'],
                           
                        );
                    break;
                case '7': // speaker
                        $speakers = $this->Speaker_model->getSpeakersListByEvent($event_id,$user_id);
                        $data = array(
                            'speaker_list' => $speakers,
                            
                        );
                    break;
                case '3': // exhibitor
                        $exhibitor_list = $this->Exhibitor_model->getExhibitorListByEventIdNative($event_id,$page_no,$user_id);
                        $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$token);
                        $data = array(
                            'exhibitor_list' => $exhibitor_list['exhibitors'],
                            'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                           
                        );
                    break;
                case '43': // sponsor
                        $sponsors_list = $this->Sponsors_model->getSponsorsListByEventId($event_id,$user_id);
                        $data = array(
                            'sponsors_list' => $sponsors_list,
                           
                        );
                    break;
            }
            $data = array(
              'success'     => true,
              'message'     => ($result) ? 'Added successfully' : 'Removed successfully',
              'result'      => $result,
              'data'        => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add or Remove from Attendees  ***/

    public function addOrRemoveAttendee()
    {
        $event_id       = $this->input->post('event_id');
        $user_id        = $this->input->post('user_id');
        $module_id      = $this->input->post('attendee_id');
        $module_type    = '2'; 
        $token          = $this->input->post('_token'); 
        $page_no        = $this->input->post('page_no'); 
        if($event_id!='' && $user_id!='' && $module_id!='' && $module_type!='')
        {
            $favorites_data['event_id']     = $event_id;
            $favorites_data['user_id']      = $user_id;
            $favorites_data['module_id']    = $module_id;
            $favorites_data['module_type']  = $module_type;

            $result = $this->favorites_model->addOrRemoveFavorites($favorites_data);
            switch ($module_type) {
                case '2':// attendee
                        $attendee = $this->Attendee_model->getAttendeeDetails($module_id,$user_id);
                        if($attendee)
                        {
                            $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$module_id);
                           
                            $social_links = $this->Attendee_model->getSocialLinks($module_id);
                            $attendee[0]->Facebook_url = ($social_links[0]->Facebook_url) ? $social_links[0]->Facebook_url : '';
                            $attendee[0]->Twitter_url =  ($social_links[0]->Twitter_url) ? $social_links[0]->Twitter_url : '';
                            $attendee[0]->Linkedin_url = ($social_links[0]->Linkedin_url) ? $social_links[0]->Linkedin_url : '';
                            $where['event_id'] = $event_id;
                            $where['from_id'] = $user_id;
                            $where['to_id'] = $module_id;
                            $attendee[0]->approval_status = ($user_id != $module_id) ?  $this->Attendee_model->getApprovalStatus($where,$event_id) : '';
                            $where1['to_id'] = $user_id;
                            $attendee[0]->share_details = ($user_id == $module_id) ? $this->Attendee_model->getShareDetails($where1,$event_id) : [] ;
                            $attendee[0]->contact_details = ($attendee[0]->approval_status == '1') ? $this->Attendee_model->getAttendeeConatctDetails($module_id) : [] ;
                        }
                        $allow_ex_contact = $this->Attendee_model->getAllowAttendee($module_id,$event_id);
                        $event_data = $this->Attendee_model->getEvenetData($event_id);
                        $myfav = $this->Attendee_model->getAttendeemyfav($module_id,$event_id,$user_id);

                        $myfav = (!empty($myfav)) ?  '1' : '0';
                        
                        $data['user_id_from'] = $user_id;
                        $data['user_id_to'] = $module_id;
                        $data['event_id'] = $event_id;
                        $is_blocked = $this->Attendee_model->checkUserBlocked($data);
                        $blocked_by_me = $this->Attendee_model->checkBlockedByMe($data);

                        $data = array(
                            'attendee_details' => ($attendee) ? $attendee : [],
                            'allowexhcontact' => $allow_ex_contact,
                            'my_faviorite' => $myfav,
                            'unread_count' =>($unread_count[0]['unread_count']) ? $unread_count[0]['unread_count'] : '0',
                            'attendee_hide_request_meeting' => $event_data['attendee_hide_request_meeting'],
                            'allow_meeting_exibitor_to_attendee' => $event_data['allow_meeting_exibitor_to_attendee'],
                            'is_blocked' => $is_blocked,
                            'blocked_by_me' => $blocked_by_me
                        );
                    break;
                
            }
            $data = array(
              'success'     => true,
              'message'     => ($result) ? 'Added successfully' : 'Removed successfully',
              'result'      => $result,
              'data'        => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Get Favorites Lists***/

    public function getFavoritesList()
    {
    	$event_id 		= $this->input->get_post('event_id');
    	$user_id 		= $this->input->get_post('user_id');
    	
    	if($event_id!='' && $user_id!='')
    	{
    		$where['mf.event_id'] 	= $event_id;
    		$where['mf.user_id'] 	= $user_id;

    		$result = $this->favorites_model->getAllFavorites($where);

    		$data = array(
              'success' 	=> true,
              'data'		=> ($result) ? $result : [],
            );
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
    	}
    	$data = json_encode($data);
        $data = str_replace(null, "", $data);
        echo $data;
    }
}
?>   