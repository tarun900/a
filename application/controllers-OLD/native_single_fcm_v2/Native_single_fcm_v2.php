<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Native_single_fcm_v2 extends CI_Controller
{
	function __construct() 
    {
        //error_reporting(E_ALL);

        parent::__construct();
        $this->load->model('native_single_fcm_v2/native_single_fcm_v2_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Settings_model');
    }

    public function getPensionBridgeAnnualDefaultEvent() //Tuesday 21 March 2018
    {

        $event = $this->native_single_fcm_v2_model->getPensionBridgeAnnualDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function getPrivateEquityExclusiveDefaultEvent() //Tuesday 21 March 2018
    {
        $event = $this->native_single_fcm_v2_model->getPrivateEquityExclusiveDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkPensionBridgeVersionCode() //Tuesday 21 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'PensionBridge';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getNapecDefaultEvent() //Tuesday 23 March 2018
    {
        $event = $this->native_single_fcm_v2_model->getNapecDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    
    public function checkNapecVersionCode() //Tuesday 23 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'NAPEC';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }


    public function getHFTPAllPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getHFTPAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkHFTPVersionCode() //Tuesday 22 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'HFTP';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function HFTPSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->HFTPSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function getBathandWestShowDefaultEvent() //Tuesday 26 March 2018
    {

        $event = $this->native_single_fcm_v2_model->getBathandWestShowDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);


               if($event[$key]['default_lang']['lang_id'] == '978')
               {
                
                 $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Location';
               }

            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkBathandWestShowVersionCode() //Tuesday 26 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'BathAndWest';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getIOTAllPublicEvents() //Tuesday 27 March 2018
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getIOTAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkIOTVersionCode() //Tuesday 27 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'IOT';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function IOTsearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->IOTSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events,
                  'URL' => 'https://allintheloop.com/privacy-policy.html'
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function getNACFBDefaultEvent() //Tuesday 29 March 2018
    {

        $event = $this->native_single_fcm_v2_model->getNACFBDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);


               if($event[$key]['default_lang']['lang_id'] == '978')
               {
                
                 $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Location';
               }

            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkNACFBVersionCode() //Tuesday 29 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'NACFB';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    
   
    public function getUHGAllPublicEvents() //Friday 30 March 2018
    {
        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getUHGAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
               $publicevents[$key]['default_lang']['1__save_session'] = 'Reserve my Seat';
               $publicevents[$key]['default_lang']['3__stand'] = 'Location';
               $publicevents[$key]['default_lang']['1__saved'] = 'Reserved';

            }
            $array_sort = [1169,1330,1331,1319,1332,1333,1318,1334,1335,1336,1317,1337,1320,1338,1339,1340,1307];
            foreach ($publicevents as $key => $value)
		    {
			    $kkey=array_search($value['event_id'], $array_sort);

			    if($kkey!='')
			    $array[$kkey] = $value;
			    else
			    $array[$key] = $value;
		    }

		   for ($i = 0 ;$i<(count($array)); $i++) 
		   {
		    $array1[] = $array[$i];
		   }
		  
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $array1
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }   

    public function checkUHGVersionCode() //Tuesday 27 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'UHG';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function UHGsearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->UHGSearchEvent($event_name);
            if(!empty($events))
            {   
                foreach ($events as $key => $value) 
                {
                   
                   $events[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
                   $events[$key]['Event_name'] =ucfirst($value['Event_name']);
                   $events[$key]['default_lang']['1__save_session'] = 'Reserve my Seat';
                   $events[$key]['default_lang']['3__stand'] = 'Location';
                   $events[$key]['default_lang']['1__saved'] = 'Reserved';
                }
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getIdahoRealtorsDefaultEvent() //Tuesday 3 April 2018
    {

        $event = $this->native_single_fcm_v2_model->getIdahoRealtorsDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkIdahoRealtorsVersionCode() //Tuesday 3 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'IdahoRealtors';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getMuveoAllPublicEvents() //Friday 30 March 2018
    {
        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getMuveoAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
            }
            
          
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }   

    public function checkMuveoVersionCode() //Tuesday 27 March 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Muveo';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function MuveosearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->MuveoSearchEvent($event_name);
            if(!empty($events))
            {   
                foreach ($events as $key => $value) 
                {
                   
                   $events[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
                   $events[$key]['Event_name'] =ucfirst($value['Event_name']);
                }
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    } 

    public function getCityWeekDefaultEvent() //Tuesday 10 April 2018
    {
        $event = $this->native_single_fcm_v2_model->getCityWeekDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
               $event[$key]['default_lang']['1__view_my_agenda'] = 'My Agenda';
               $event[$key]['default_lang']['1__sort_by_time'] = 'Conference Agenda';
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkCityWeekVersionCode() //Tuesday 10 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Cityweek';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getAICEDefaultEvent() //Tuesday 18 April 2018
    {
        $event = $this->native_single_fcm_v2_model->getAICEDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkAICEVersionCode() //Tuesday 18 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'AICE';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getVenusDefaultEvent() //Tuesday 20 April 2018
    {
        $event = $this->native_single_fcm_v2_model->getVenusDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkVenusVersionCode() //Tuesday 20 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Venus';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getEuroviaDefaultEvent() //Tuesday 20 April 2018
    {
        $event = $this->native_single_fcm_v2_model->getEuroviaDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);

               $event[$key]['default_lang']['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';
        
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkEuroviaVersionCode() //Tuesday 20 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Eurovia';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getSourceMediaAllPublicEvents() //Wednesday 25 April 2018
    {
        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getSourceMediaAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
            }
            
          
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }   

    public function checkSourceMediaVersionCode() //Wednesday 25 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'SourceMedia';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function SourceMediasearchEvent() //Wednesday 25 April 2018
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->SourceMediasearchEvent($event_name);
            if(!empty($events))
            {   
                foreach ($events as $key => $value) 
                {
                   
                   $events[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
                   $events[$key]['Event_name'] =ucfirst($value['Event_name']);
                }
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    } 

    public function Certificatevalid() //Wednesday 25 April 2018
    {

        $data = array(
          'success'  => true,
          'status'  => '1',
        );
        echo json_encode($data);
    }

    public function getConnectechDefaultEvent() //Thursday 26 April 2018
    {
        $event = $this->native_single_fcm_v2_model->getConnectechDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);

               if($event[$key]['default_lang']['lang_id'] == '526')
               {
                 $event[$key]['default_lang']['my_profile__login'] = 'Login/Sign Up';
               }
               $event[$key]['default_lang']['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';
        
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkConnectechVersionCode() //Thursday 26 April 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Connectech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getEpsonDefaultEvent() //Thursday 03 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getEpsonDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);

               $event[$key]['default_lang']['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';

            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function searchEpsonEventBySecurekey()
    {
      $key=$this->input->post('secure_key');
      if($key!='')
      {
        $events = $this->Event_model->searchEpsonEventBySecurekey($key);
        if(!empty($events))
        {
          
          $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $events,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
          $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
      }
      else
      {
        $data = array(
              'success' => false,
              'message' => "Invalid parameters"
          );
      }
        echo json_encode($data);
    }

    public function checkEpsonVersionCode() //Thursday 03 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Epson';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getiAmericas2018DefaultEvent() //May 04 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getiAmericas2018DefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkiAmericas2018VersionCode() //May 04 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'iAmericas2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getMIT2018DefaultEvent() //May 04 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getMIT2018DefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkMIT2018VersionCode() //May 04 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'MIT2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getGlobalRegTechDefaultEvent() //May 07 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getGlobalRegTechDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkGlobalRegTechVersionCode() //May 07 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'GlobalRegTech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getBigDataTorontoDefaultEvent() //May 07 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getBigDataTorontoDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkBigDataTorontoVersionCode() //May 07 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'BigDataToronto';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /** Get SIAM Public Events **/

    public function getSIAMAllPublicEvents() //Wednesday 16 May 2018
    {
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getSIAMAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
            }
            
          
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }   

    /** Check version of SIAM **/

    public function checkSIAMVersionCode() //Wednesday 16 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'SIAM';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /** Search Events for only SIAM Account **/

    public function SIAMsearchEvent() //Wednesday 16 May 2018
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->SIAMsearchEvent($event_name);
            if(!empty($events))
            {   
                foreach ($events as $key => $value) 
                {
                   
                   $events[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
                   $events[$key]['Event_name'] =ucfirst($value['Event_name']);
                }
                $data = array(
                  'success' => true,
                  'data' => $events,
                  'URL' => 'https://allintheloop.com/privacy-policy.html'

                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    } 

    /*** Get Retail Bulletin Master App**/

    public function getRetailBulletinMasterEvent() //17-05-2018
    { 
        $event = $this->native_single_fcm_v2_model->getRetailBulletinMasterEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Retail Bulletin Version Code**/

    public function checkRetailBulletinVersionCode() //17-05-2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'RetailBulletin';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /*** Get Retail Bulletin Public Apps**/

    public function getRetailBulletinPublicEvents() //17-05-2018
    {
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getRetailBulletinPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Closing the Gap Default Event**/

    public function getClosingTheGapDefaultEvent() //May 22 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getClosingTheGapDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Closing the Gap  Version Code**/

    public function checkClosingTheGapVersionCode() //May 22 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ClosingTheGap';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

     /*** Get Podcast Default Event**/

    public function getPodcastMovementDefaultEvent() //May 24 May 2018
    {
        $event = $this->native_single_fcm_v2_model->getPodcastMovementDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Podcast  Version Code**/

    public function checkPodcastMovementVersionCode() //May 24 May 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'PodcastMovement';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /*** Get UHG Single APP Default Event Data**/

    public function getUHGLatestDefaultEvent() //May 01 June 2018
    {
        $event = $this->native_single_fcm_v2_model->getUHGLatestDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);     

               if($event[$key]['default_lang']['lang_id'] == '835')
               {
                 $event[$key]['default_lang']['3__stand'] = 'Location';
               } 
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get FIME Single APP Default Event Data**/

    public function getFIMEDefaultEvent() //June 11 2018
    {
        $event = $this->native_single_fcm_v2_model->getFIMEDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get FIME  Version Code**/

    public function checkFIMEVersionCode() //June 11 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'FIME';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /*** Get Smart Airports Single APP Default Event Data**/

    public function getSmartAirportsDefaultEvent() //June 11 2018
    {
        $event = $this->native_single_fcm_v2_model->getSmartAirportsDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Smart Airports  Version Code**/

    public function checkSmartAirportsVersionCode() //June 11 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'SmartAirports';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /*** Get Howard hughes Single APP Default Event Data**/

    public function getHowardhughesDefaultEvent() //June 12 2018
    {
        $event = $this->native_single_fcm_v2_model->getHowardhughesDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Howard hughes  Version Code**/

    public function checkHowardhughesVersionCode() //June 12 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Howardhughes';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getDynamiteAllPublicEvents()  //Friday 15 June 2018
    {   

        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getDynamiteAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkDynamiteVersionCode() //Friday 15 June 2018
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Dynamite';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function DynamiteSearchEvent() //Friday 15 June 2018
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->DynamiteSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    /*** Get Food Matters Single APP Default Event Data**/

    public function getFoodMattersDefaultEvent() //June 18 2018
    {
        $event = $this->native_single_fcm_v2_model->getFoodMattersDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Food Matters  Version Code**/

    public function checkFoodMattersVersionCode() //June 18 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'FoodMattersLive';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getConcordiaAllPublicEvents()  //Monday 25 June 2018
    {   

        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getConcordiaAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkConcordiaVersionCode() //Monday 25 June 2018
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Concordia2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function ConcordiaSearchEvent() //Monday 25 June 2018
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->ConcordiaSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function searchSmartAirportEventBySecurekey()
    {
      $key=$this->input->post('secure_key');
      if($key!='')
      { 
        $events = $this->Event_model->searchSmartAirportEventBySecurekey($key);
        if(!empty($events))
        {
          
          $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $events,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
          $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
      }
      else
      {
        $data = array(
              'success' => false,
              'message' => "Invalid parameters"
          );
      }
        echo json_encode($data);
    }
    public function getConcordiaMasterEvent()
    {
        $event = $this->native_single_fcm_v2_model->getConcordiaMasterEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Compassionate Friends Single APP Default Event Data**/

    public function getCompassionateFriendsDefaultEvent() //July 10 2018
    {
        $event = $this->native_single_fcm_v2_model->getCompassionateFriendsDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Food Matters  Version Code**/

    public function checkCompassionateFriendsVersionCode() //June 18 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'CompassionateFriends';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getNCRealtorAllPublicEvents()  //11 July 2018
    {   

        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getNCRealtorAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkNCRealtorVersionCode() //11 July 2018
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'NCRealtor';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function NCRealtorDefaultEvent() //7/11/2018 12:46:59 PM
    {
        $event = $this->native_single_fcm_v2_model->NCRealtorDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function NCRealtorSearchEvent() //7/11/2018 12:18:33 PM
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->NCRealtorSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function getPurposeBuiltCommunitiesDefaultEvent() //July 12 2018
    {
        $event = $this->native_single_fcm_v2_model->getPurposeBuiltCommunitiesDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkPurposeBuiltCommunitiesVersionCode() //July 12 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'PurposeBuiltCommunities';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getPPMADefaultEvent() //July 18 2018
    {
        $event = $this->native_single_fcm_v2_model->getPPMADefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkPPMAVersionCode() //July 18 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'PPMA';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getTricordAllPublicEvents()  //7/20/2018 4:02:44 PM
    {   

        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getTricordAllPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL'  => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkTricordVersionCode() //7/20/2018 4:02:52 PM
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Tricord';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function TricordSearchEvent() //7/20/2018 4:02:57 PM
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->TricordSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function searchTricordEventBySecurekey()
    {
      $key=$this->input->post('secure_key');
      if($key!='')
      {
        $events = $this->Event_model->searchTricordEventBySecurekey($key);
        if(!empty($events))
        {
          
          $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $events,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
          $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
      }
      else
      {
        $data = array(
              'success' => false,
              'message' => "Invalid parameters"
          );
      }
        echo json_encode($data);
    }
    public function searchCoburnsEventBySecurekey()
    {
      $key=$this->input->post('secure_key');
      if($key!='')
      {
        $events = $this->Event_model->searchCoburnsEventBySecurekey($key);
        if(!empty($events))
        {
          
          $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $events,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
          $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
      }
      else
      {
        $data = array(
              'success' => false,
              'message' => "Invalid parameters"
          );
      }
        echo json_encode($data);
    }


    /*** Get Warrior 2018 Single APP Default Event Data**/

    public function getWarriorDefaultEvent() //July 25 2018
    {
        $event = $this->native_single_fcm_v2_model->getWarriorDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);      
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    /*** Get Warrior 2018  Version Code**/

    public function checkWarriorVersionCode() //July 25 2018
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Warrior2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getAASADefaultEvent() //7/27/2018 11:14:04 AM
    {
        $event = $this->native_single_fcm_v2_model->getAASADefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);

               $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Type';
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    
    public function checkAASAVersionCode() //7/27/2018 11:14:04 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'AASA';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getDCACDefaultEvent() //8/8/2018 11:38:30 AM
    {
        $event = $this->native_single_fcm_v2_model->getDCACDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    
    public function checkDCACersionCode() //8/8/2018 11:38:23 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'DCAC';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getVitafoodsDefaultEvent() //8/23/2018 10:51:48 AM
    {
        $event = $this->native_single_fcm_v2_model->getVitafoodsDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
               $event[$key]['default_lang']['1__sort_by_track'] = 'Sort By Stream';
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkVitafoodsersionCode() //8/8/2018 11:38:23 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Vitafoods';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getFinancialServicesDefaultEvent() //8/31/2018 10:30:26 AM
    {
        $event = $this->native_single_fcm_v2_model->getFinancialServicesDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkFinancialServicesVersionCode() //8/31/2018 10:31:02 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'FinancialServices';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getAlignHRDefaultEvent() //9/04/2018 10:30:26 AM
    {
        $event = $this->native_single_fcm_v2_model->getAlignHRDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkAlignHRVersionCode() //9/04/2018 10:31:02 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'AlignHR2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getFranklinCoveyDefaultEvent() //9/04/2018 10:30:26 AM
    {
        $event = $this->native_single_fcm_v2_model->getFranklinCoveyDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkFranklinCoveyVersionCode() //9/04/2018 10:31:02 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'FranklinCovey';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getPCAWaterproofingDefaultEvent() //9/5/2018 10:06:51 AM
    {
        $event = $this->native_single_fcm_v2_model->getPCAWaterproofingDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkPCAWaterproofingVersionCode() //9/5/2018 10:06:56 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'PCAWaterproofing';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getCRForumLisbonDefaultEvent() //9/5/2018 10:06:51 AM
    {
        $event = $this->native_single_fcm_v2_model->getCRForumLisbonDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkCRForumLisbonVersionCode() //9/5/2018 10:06:56 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'CRForum2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getIPExpoEuropeDefaultEvent() //9/6/2018
    {
        $event = $this->native_single_fcm_v2_model->getIPExpoEuropeDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
               $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Theatre';
               $event[$key]['default_lang']['1__check_in'] = 'Request Slides';

            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkIPExpoEuropeVersionCode() //9/6/2018 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'IPExpoEurope';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getBTSConferenceDefaultEvent() //9/10/2018 10:02:46 AM
    {
        $event = $this->native_single_fcm_v2_model->getBTSConferenceDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkBTSConferenceVersionCode() //9/10/2018 10:02:50 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'BTSConference';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getOTRFilmFestDefaultEvent() //9/10/2018 10:02:46 AM
    {
        $event = $this->native_single_fcm_v2_model->getOTRFilmFestDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkOTRFilmFestVersionCode() //9/10/2018 10:02:50 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'OTRFilmFest';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getGemConferenceDefaultEvent() //9/11/2018 10:00:15 AM
    {
        $event = $this->native_single_fcm_v2_model->getGemConferenceDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkGemConferenceVersionCode() //9/11/2018 10:00:22 AM
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'GemConference';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getLogisticsDefaultEvent() //9/11/2018 10:00:15 AM
    {
        $event = $this->native_single_fcm_v2_model->getLogisticsDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkLogisticsVersionCode() //9/11/2018 10:00:22 AM
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Logistics';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getClarionDefaultEvent() //9/11/2018 10:00:15 AM
    {
        $event = $this->native_single_fcm_v2_model->getClarionDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkClarionVersionCode() //9/11/2018 10:00:22 AM
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Clarion';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getShareFair2018DefaultEvent() //9/21/2018 12:28:49 PM
    {
        $event = $this->native_single_fcm_v2_model->getShareFair2018DefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkShareFair2018VersionCode() //9/21/2018 12:28:54 PM
    {   
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ShareFair2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    /** Get Paraxel  Public Events **/


    public function searchParaxelEventBySecurekey()
    {
      $key=$this->input->post('secure_key');
      if($key!='')
      {
        $events = $this->Event_model->searchParaxelEventBySecurekey($key);
        if(!empty($events))
        {
          
          $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $events,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
          $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
      }
      else
      {
        $data = array(
              'success' => false,
              'message' => "Invalid parameters"
          );
      }
        echo json_encode($data);
    }

    /** Check version of Paraxel  **/

    public function checkParaxelVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Paraxel';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getParaxelPublicEvents() 
    {

        //error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v2_model->getParaxelPublicEvents($gcm_id);
        if(!empty($publicevents))
        {
            foreach ($publicevents as $key => $value) 
            {
               
               $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }

    public function ParaxelSearchEvent()
    {
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->native_single_fcm_v2_model->ParaxelSearchEvent($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function getIAAArkansasDefaultEvent()
    {
        $event = $this->native_single_fcm_v2_model->getIAAArkansasDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event,
              'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkIAAArkansasVersionCode() 
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'IAAArkansas';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where,$device);
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
}