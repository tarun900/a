<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Checkin extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/Checkin_model');
        $this->load->model('native_single_fcm_v2/App_profile_model');
        //date_default_timezone_set("UTC");
    }

    /*** Give Attendee List ***/

    public function attendeeList()
    {
        $event_id   = $this->input->post('event_id');
        if($event_id!='')
        {
            $attendee = $this->Checkin_model->getAttendeeListByEventId($event_id);
            $badge_logo = $this->Checkin_model->getBadgeLogo($event_id);
            
            $checkedin_count = $this->Checkin_model->get_checkedin_attendee_count($event_id);
            $data = array(
                'attendee_list' => $attendee,
                'badge_logo' => $badge_logo,
                'total_registerd' => count($attendee),
                'total_checkedin' => $checkedin_count
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Attendee Check In ***/

    public function attendeeCheckIn()
    {
        $event_id    = $this->input->post('event_id');
        $attendee_id = $this->input->post('attendee_id');

        if($event_id!='' && $attendee_id!='')
        {
            $data['Event_id']       = $event_id;
            $data['Attendee_id']    = $attendee_id;

            $result = $this->Checkin_model->saveCheckIn($data);
            $data   = array(
                'check_in_btn_color' => ($result) ? "#449d44" : "#007CBA" ,
                'is_checked_in'      => ($result) ? "1" : "0" ,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** To View Attendee info ***/

    public function viewAttendeeInfo()
    {
        $event_id       = $this->input->post('event_id');
        $attendee_id    = $this->input->post('attendee_id');

        if($event_id!='' && $attendee_id!='')
        {
        
            $where['Event_id']      = $event_id;
            $where['Attendee_id']   = $attendee_id;
            $personal_info          = $this->Checkin_model->getAttendeeDetails($attendee_id);
            $custom_info            = $this->Checkin_model->getCustomInfo($where);
            $custom_info = ($custom_info) ? $custom_info : [];
            $data = array(
                'personal_info' => $personal_info,
                'custom_info'   => $custom_info,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** To Save Attendee info ***/

    public function saveAttendeeInfo()
    {
        $event_id       = $this->input->post('event_id');
        $attendee_id    = $this->input->post('attendee_id');
        $personal_info  = json_decode($this->input->post('personal_info'),true); // json
        $custom_info['extra_column']    = ($this->input->post('custom_info')); // json

        if($event_id!='' && $attendee_id!='' && $personal_info!=NULL && $custom_info['extra_column']!=NULL)
        {
            if(!empty(($_FILES['logo']['name'])))
            {
                $newImageName = $attendee_id . "_" . round(microtime(true) * 1000).".jpeg";
                $target_path = "././assets/user_files/".$newImageName;          
                move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $this->App_profile_model->make_thumbnail($attendee_id,$newImageName);
                $personal_info['Logo'] =$newImageName;
            }
            $personal_info['updated_date'] = date('Y-m-d H:i:s');

            $where['Event_id']      = $event_id;
            $where['Attendee_id']   = $attendee_id;
            $this->Checkin_model->saveAttendeeDetails($personal_info,$attendee_id);
            $this->Checkin_model->saveCustomInfo($custom_info,$where);
           
            $data = array(
              'success' => true,
              'data'    => 'Details saved successfully'
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add Check In info ***/

    public function addCheckin()
    {
        $event_id   = $this->input->post('event_id');
        $email   = $this->input->post('email');
        if($event_id!='' && $email!='')
        {   
            $data['event_id'] = $event_id;
            $data['email'] = $email;

            $attendee = $this->Checkin_model->getAttendeeDetailsCheckin($email,$event_id);
            $attendee1 = $this->Checkin_model->getAttendeeListByEventId($event_id);
            $badge_logo = $this->Checkin_model->getBadgeLogo($event_id);
            
            $checkedin_count = $this->Checkin_model->get_checkedin_attendee_count($event_id);


            $data1 = array(
                'attendee_list' => $attendee,
                'badge_logo' => $badge_logo,
                'total_registerd' => count($attendee1),
                'total_checkedin' => $checkedin_count
            );
            $data = array(
              'success' => true,
              'message'    => "User Checkin/CheckOut",
              'unique_no' => $email."##".rand('111111','999999'),
              'data' => $data1
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Get List of Check In Attendees ***/

    public function get_checkedin_attendee($event_id)
    {   
        $event_id   = $this->input->post('event_id');
        if($event_id!='')
        {   
            $attendee = $this->Checkin_model->getAttendeeListByEventId($event_id);
            $checkedin_list = $this->Checkin_model->get_checkedin_attendee_list($event_id);
            $badge_logo = $this->Checkin_model->getBadgeLogo($event_id);
            $checkedin_count = $this->Checkin_model->get_checkedin_attendee_count($event_id);
            
            $data = array(
                'attendee_list' => $checkedin_list,
                'badge_logo' => $badge_logo,
                'total_registerd' => count($attendee),
                'total_checkedin' => $checkedin_count

            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Get PDF for Attendees for Checkin***/

    public function get_pdf()
    {   
        $event_id   = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if($event_id!='' && $user_id!='')
        {   
            $user = $this->Checkin_model->get_user_info($user_id,$event_id);
            $badgesdata = $this->Checkin_model->get_badges($event_id);
            $attendee_type = "Visitor";
            $color = "#000000";
            if($badgesdata[0]['code_type']!='1')
            {
                $this->load->library('Ciqrcode');
                $params['data'] = $event_id.'-'.$user_id;
                $params['level'] = 'M';
                $params['size'] = 2;
                $params['savename'] = 'tes.png';
                $this->ciqrcode->generate($params);
            }
            else
            {
                $this->load->library('zend');
                $this->zend->load('Zend/Barcode');
                $imageResource=Zend_Barcode::factory('code128', 'image', array('text'=>$event_id.'-'.$user['Id'],'drawText' => false,'barHeight'=> 50,'factor'=>3), array('barHeight'=> 50,'factor'=>3))->draw();
                imagepng($imageResource, './assets/user_files/Barcode.png');
            }

            
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');

            if($event_id =='1367')
            {
                $pdf = new TCPDF('l', PDF_UNIT, 'A5', true, 'UTF-8', false);
                $pdf->SetLeftMargin(2);
                $pdf->SetRightMargin(5);
                $pdf->SetTopMargin(5);
                $pdf->SetFooterMargin(5);
                $pdf->SetAutoPageBreak(false, 0);
                $pdf->setPrintFooter(false);
            }
            else
            {
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
            }
            
            //$pdf->SetFont('lato', '', 10);
            $pdf->SetTitle('Badge Design Final - Visitor');
            $pdf->AddPage();
            if($event_id !='1367')
            {
                $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th colspan="4" height="10"></th>
                            </tr>
                            <tr>
                                <th width="2%"></th>
                                <th width="58%" align="left" style="vertical-align:top;">';
                                if(!empty($badgesdata[0]['badges_logo_images'])){
                                $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                }
                                $htm.='</th>
                                <th width="38%" align="right" style="margin-top:5%;">';
                                    if($badgesdata[0]['code_type']!='1')
                                    {
                                        $htm.='<img src="tes.png"/>';
                                    }
                                    else
                                    {
                                        $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                    }
                                    
                                $htm .= '</th>
                                <th width="2%" align="right"></th>
                            </tr>
                            <tr style="padding-bottom:15px;">
                                <th width="3%"></th>
                                <th width="97%" colspan="3">
                                    <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                    <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                    <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                    <font size="10">'.ucfirst($user['Title']).'</font><br>
                                    <font size="10">Badge Number : '.$event_id.'-'.$user['Id'].'</font>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4" height="20"></th>
                            </tr>
                            <tr style="">
                                <th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th>
                            </tr>
                        </table>
                    </td>
                    <td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th colspan="4" height="10"></th>
                            </tr>
                            <tr>
                                <th width="2%"></th>
                                <th width="58%" align="left" style="vertical-align:top;">';
                                if(!empty($badgesdata[0]['badges_logo_images'])){
                                $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                }
                                $htm.='</th>
                                <th width="38%" align="right" style="margin-top:5%;">';
                                    if($badgesdata[0]['code_type']!='1')
                                    {
                                        $htm.='<img src="tes.png"/>';
                                    }
                                    else
                                    {
                                        $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                    }
                                $htm .= '</th>
                                <th width="2%" align="right"></th>
                            </tr>
                            <tr style="padding-bottom:15px;">
                                <th width="3%"></th>
                                <th width="97%" colspan="3">
                                    <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                    <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                    <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                    <font size="10">'.ucfirst($user['Title']).'</font><br>
                                    <font size="10">Badge Number : '.$event_id.'-'.$user['Id'].'</font>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4" height="20"></th>
                            </tr>
                            <tr style="">
                                <th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>';
            }
            else
            {
                    $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid white;margin-top:0px;">
                        <tr>
                            <td width="49%" align="left">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th width="100%" align="left" style="vertical-align:top;">';
                                    if(!empty($badgesdata[0]['badges_logo_images'])){
                                    $htm.='<img width="524" height="220" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                    }
                                    $htm.='</th>
                                </tr>
                                <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>                         
                            <tr>
                                <td colspan="2" align="center">
                                    <font size="22"><span style="text-align:center;"><b>'.ucfirst($user['Firstname']).'</b></span></font>
                                    <font size="22"><span style="text-align:center;"><b>'.ucfirst($user['Lastname']).'</b></span></font>
                                </td>
                            </tr>
                                <tr>
                                    <td style="height:10" height="10">&nbsp;</td>
                                </tr>                             
                            <tr>    
                                <td colspan="2" align="center">
                                    <span style="text-align:center; padding-top:20px; font-size:20px;">'.ucfirst($user['Company_name']).'</span>
                                </td>                            
                            </tr>
                                <tr>
                                    <td style="height:20" height="20">&nbsp;</td>
                                </tr>                         

                            <tr>
                                <th width="100%" align="center" style="margin-top:2%;float:right;">';
                                    if($badgesdata[0]['code_type']!='1')
                                    {
                                        $htm.='<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                                    }
                                    else
                                    {
                                        $htm.='<img style="width:200px; height:50px; float:right;" src="'.base_url().'assets/user_files/Barcode.png"/>';
                                    }
                                    
                                $htm .= '</th>
                            </tr>
                            <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>                            
                            <tr>
                                <td width="96%">
                                    <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png"> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="2%"></td>
                    <td width="49%" align="right">
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th width="100%" align="left" style="vertical-align:top;">';
                                if(!empty($badgesdata[0]['badges_logo_images'])){
                                $htm.='<img width="524" height="220" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                }
                                $htm.='</th>
                             </tr>
                            <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>                         
                            <tr>
                                <td colspan="2" align="center">
                                    <font size="22"><span style="text-align:center;"><b>'.ucfirst($user['Firstname']).'</b></span></font>
                                    <font size="22"><span style="text-align:center;"><b>'.ucfirst($user['Lastname']).'</b></span></font>
                                </td>
                            </tr>
                                <tr>
                                    <td style="height:10" height="10">&nbsp;</td>
                                </tr>                             
                            <tr>    
                                <td colspan="2" align="center">
                                    <span style="text-align:center; padding-top:20px; font-size:20px;">'.ucfirst($user['Company_name']).'</span>
                                </td>                            
                            </tr>
                                <tr>
                                    <td style="height:20" height="20">&nbsp;</td>
                                </tr>                         

                            <tr>
                                <th width="100%" align="center" style="margin-top:2%;float:right;">';
                                    if($badgesdata[0]['code_type']!='1')
                                    {
                                        $htm.='<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                                    }
                                    else
                                    {
                                        $htm.='<img style="width:200px; height:50px; float:right;" src="'.base_url().'assets/user_files/Barcode.png"/>';
                                    }
                                    
                                $htm .= '</th>
                            </tr>
                            <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>                          

                            <tr>
                                <td width="96%">
                                    <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png"> 
                                </td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
                </table>';
            }
            $pdf->writeHTML($htm);
            if($event_id == '1802')
            {
                 $pdf = new TCPDF(PDF_PAGE_ORIENTATION, "pt", 'A6', true, 'UTF-8', false);
                 $pdf->SetTitle('Badge Design');
                 $pdf->AddPage();
                 $pdf->SetFontSize(22);
                 $htm = '<table style="text-align:center;" align="center">
                                 <tbody>
                                        <tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr>
                                      <tr>
                                           <td ><b>'.ucfirst($user['Firstname']).'</b></td>
                                      </tr>

                                      <tr >
                                           <td ><p><b>'.ucfirst($user['Lastname']).'</b></p></td>
                                      </tr>
                                    
                                      <tr >
                                           <td ><span></span><small class="cmpnyname">'.ucfirst($user['Company_name']).'</small></td>
                                      </tr>
                                      <tr>
                                           <td>';
                                               if($badgesdata[0]['code_type']!='1')
                                               {
                                                   $htm.='<img style="width:75px; height:75px; margin-top:30px;" src="tes.png"/>';
                                               }
                                               else
                                               {
                                                   $htm.='<img style="width:200px; height:50px; " src="'.base_url().'assets/user_files/Barcode.png"/>';
                                               }
                                           $htm .= '</td>
                                      </tr>
                                 </tbody>
                            </table>';
                      
                    $pdf->writeHTML($htm, true, false, false, false, 'C');
            }
            ob_end_clean();
            //$pdf->Output('badges_files_mobile.pdf', 'I');die;
            unlink('./assets/badges_files/badges_files'.$user_id.'.pdf');
            file_put_contents('./assets/badges_files/badges_files'.$user_id.'.pdf',$pdf->Output('badges_files_mobile.pdf', 'S'));
            
            $name = base_url()."assets/badges_files/badges_files".$user_id.".pdf";
            if($badgesdata[0]['code_type']=='1')
            {
                unlink('./assets/user_files/Barcode.png');
            }
            $data = array(
                'pdf_name' => $name,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function get_pdf_new()
    {   
        $event_id   = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_id = '1367';
        $user_id = '314036';
        if($event_id!='' && $user_id!='')
        {   
            $user = $this->Checkin_model->get_user_info($user_id,$event_id);
            $badgesdata = $this->Checkin_model->get_badges($event_id);
            $attendee_type = "Visitor";
            $color = "#000000";
            if($badgesdata[0]['code_type']!='1')
            {
                $this->load->library('Ciqrcode');
                $params['data'] = $event_id.'-'.$user_id;
                $params['level'] = 'M';
                $params['size'] = 2;
                $params['savename'] = 'tes.png';
                $this->ciqrcode->generate($params);
            }
            else
            {
                $this->load->library('zend');
                $this->zend->load('Zend/Barcode');
                $imageResource=Zend_Barcode::factory('code128', 'image', array('text'=>$event_id.'-'.$user['Id'],'drawText' => false,'barHeight'=> 50,'factor'=>3), array('barHeight'=> 50,'factor'=>3))->draw();
                imagepng($imageResource, './assets/user_files/Barcode.png');
            }

            show_errors();
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');
            $pdf = new TCPDF('l', PDF_UNIT, 'A5', true, 'UTF-8', false);
            //case 'A5' : {$pf = array(  419.528,  595.276); break;}
            $pdf->SetTitle('Badge Design Final - Visitor');
            $pdf->SetLeftMargin(2);
            $pdf->SetRightMargin(5);
            $pdf->SetTopMargin(5);
            $pdf->SetFooterMargin(5);
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(false, 0);
            $pdf->setPrintFooter(false);

            $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid white;margin-top:0px;">
                    <tr>
                        <td width="49%" align="left">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th width="100%" align="left" style="vertical-align:top;">';
                                if(!empty($badgesdata[0]['badges_logo_images'])){
                                $htm.='<img width="524" height="230" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                }
                                $htm.='</th>
                            </tr>
                            <tr>
                            <td style="height:30" height="30">&nbsp;</td>
                        </tr>                         
                        <tr>
                            <td colspan="2" align="center">
                                <font size="24"><span style="text-align:center;"><b>'.ucfirst($user['Firstname']).'</b></span></font>
                                <font size="24"><span style="text-align:center;"><b>'.ucfirst($user['Lastname']).'</b></span></font>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:10" height="10">&nbsp;</td>
                            </tr>                             
                        <tr>    
                            <td colspan="2" align="center">
                                <span style="text-align:center; padding-top:20px; font-size:20px;">'.ucfirst($user['Company_name']).'</span>
                            </td>                            
                        </tr>
                            <tr>
                                <td style="height:30" height="30">&nbsp;</td>
                            </tr>                         

                        <tr>
                            <th width="100%" align="center" style="margin-top:2%;float:right;">';
                                if($badgesdata[0]['code_type']!='1')
                                {
                                    $htm.='<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                                }
                                else
                                {
                                    $htm.='<img style="width:200px; height:50px; float:right;" src="'.base_url().'assets/user_files/Barcode.png"/>';
                                }
                                
                            $htm .= '</th>
                        </tr>
                        <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>                            
                        <tr>
                            <td width="96%">
                                <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png"> 
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="2%"></td>
                <td width="49%" align="right">
                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th width="100%" align="left" style="vertical-align:top;">';
                            if(!empty($badgesdata[0]['badges_logo_images'])){
                            $htm.='<img width="524" height="230" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                            }
                            $htm.='</th>
                         </tr>
                        <tr>
                            <td style="height:30" height="30">&nbsp;</td>
                        </tr>                         
                        <tr>
                            <td colspan="2" align="center">
                                <font size="24"><span style="text-align:center;"><b>'.ucfirst($user['Firstname']).'</b></span></font>
                                <font size="24"><span style="text-align:center;"><b>'.ucfirst($user['Lastname']).'</b></span></font>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:10" height="10">&nbsp;</td>
                            </tr>                             
                        <tr>    
                            <td colspan="2" align="center">
                                <span style="text-align:center; padding-top:20px; font-size:20px;">'.ucfirst($user['Company_name']).'</span>
                            </td>                            
                        </tr>
                            <tr>
                                <td style="height:30" height="30">&nbsp;</td>
                            </tr>                         

                        <tr>
                            <th width="100%" align="center" style="margin-top:2%;float:right;">';
                                if($badgesdata[0]['code_type']!='1')
                                {
                                    $htm.='<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                                }
                                else
                                {
                                    $htm.='<img style="width:200px; height:50px; float:right;" src="'.base_url().'assets/user_files/Barcode.png"/>';
                                }
                                
                            $htm .= '</th>
                        </tr>
                        <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>                          

                        <tr>
                            <td width="96%">
                                <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png"> 
                            </td>
                        </tr>
                       
                    </table>
                </td>
            </tr>
            </table>';
            $pdf->writeHTML($htm);
            ob_end_clean();
            $pdf->Output('badges_files_mobile.pdf', 'I');die;
            unlink('./assets/badges_files/badges_files'.$user_id.'.pdf');
            file_put_contents('./assets/badges_files/badges_files'.$user_id.'.pdf',$pdf->Output('badges_files_mobile.pdf', 'S'));
            
            $name = base_url()."assets/badges_files/badges_files".$user_id.".pdf";
            if($badgesdata[0]['code_type']=='1')
            {
                unlink('./assets/user_files/Barcode.png');
            }
            $data = array(
                'pdf_name' => $name,
            );
            
            $data = array(
              'success' => true,
              'data'    => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add Checkin Latest***/

    public function addCheckin_new()
    {
        $event_id   = $this->input->post('event_id');
        $email   = $this->input->post('email');//data from code
        if($event_id!='' && $email!='')
        {   
            $data['event_id'] = $event_id;
            $data['email'] = $email;

            $tmp = explode('-',$email);
            // start- for email QR codes - rashmi
            if(isset($tmp[1]))
                $user_id = $tmp[1];
            else
                $user_id = $email;
            // end - for email QR codes - rashmi

            $attendee = $this->Checkin_model->getAttendeeDetailsCheckin_new($user_id,$event_id);
            $attendee1 = $this->Checkin_model->getAttendeeListByEventId($event_id);
            $badge_logo = $this->Checkin_model->getBadgeLogo($event_id);
            
            $checkedin_count = $this->Checkin_model->get_checkedin_attendee_count($event_id);


            $data1 = array(
                'attendee_list' => $attendee,
                'badge_logo' => $badge_logo,
                'total_registerd' => count($attendee1),
                'total_checkedin' => $checkedin_count
            );
            $data = array(
              'success' => true,
              'message'    => "User Checkin/CheckOut",
              'unique_no' => $email,
              'data' => $data1
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
?>   