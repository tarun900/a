<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Note extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Note_model');
        
        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    /*** Get Notes List ***/
    public function note_list()
    {

        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!=''  && $token!='' && $user_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $notes = $this->Note_model->getNotesListByEventId($event_id,$user_id);
                $date_format = $this->Note_model->checkEventDateFormat($event_id);

                if(!empty($notes))
                {
                    foreach ($notes as $key => $value) 
                    {
                        if($date_format[0]['date_format'] == 0)
                        {   
                            $a =  strtotime($value['Created_at']);
                            $Created_at=date("d/m/Y H:i:s",$a);


                        }
                        else
                        {   
                            $a =  strtotime($value['Created_at']);
                            $Created_at=date("m/d/Y H:i:s",$a);

                        }
                        $notes[$key]['Created_at'] = $Created_at;
                    }
                }



                $data = array(
                    'note_list' => $notes,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add Notes ***/

    public function add_note()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $heading=$this->input->post('heading');
        $description=$this->input->post('description');
        if($event_id!='' && $token!='' && $user_id!='' && $heading!='' && $description!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {
                $add_arr['Heading']=$heading;
                $add_arr['Description']=$description;
                $add_arr['Created_at']=date('Y-m-d H:i:s');
                $add_arr['Event_id']=$event_id;
                $add_arr['User_id']=$user_id;
                $add_arr['Organisor_id']=$this->Note_model->getOrganisorIdByUserId($user_id,$event_id);
                $notes = $this->Note_model->add_note($add_arr);
                $data = array(
                  'success' => true,
                  'message' => "Successfully added"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Edit  Notes ***/
    public function edit_note()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $note_id=$this->input->post('note_id');
        if($event_id!='' && $token!='' && $user_id!='' && $note_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {

                if($this->input->post('heading'))
                {
                    $update_arr['Heading']=$this->input->post('heading');
                }
                if($this->input->post('description'))
                {
                    $update_arr['Description']=$this->input->post('description');
                }
                if($this->input->post('Menu_id'))
                {
                    $update_arr['Menu_id']=$this->input->post('Menu_id');
                }
                if($this->input->post('Module_id'))
                {
                    $update_arr['Module_id']=$this->input->post('Module_id');
                }
                
                $timezone = $this->Note_model->get_event_timezone($event_id);
                date_default_timezone_set("UTC");
                $cdate=date('Y-m-d H:i:s');
                if(!empty($timezone['Event_show_time_zone']))
                {
                    if(strpos($timezone['Event_show_time_zone'],"-")==true)
                    { 
                      $arr=explode("-",$timezone['Event_show_time_zone']);
                      $intoffset=$arr[1]*3600;
                      $intNew = abs($intoffset);
                      $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                    }
                    if(strpos($timezone['Event_show_time_zone'],"+")==true)
                    {
                      $arr=explode("+",$timezone['Event_show_time_zone']);
                      $intoffset=$arr[1]*3600;
                      $intNew = abs($intoffset);
                      $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                    }
                }

                $update_arr['Updated_at']=$cdate;
                if(!empty($update_arr))
                {
                    $notes = $this->Note_model->update_note($note_id,$update_arr);
                }
                
                $data = array(
                  'success' => true,
                  'message' => "Successfully updated"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Get Notes List Latest ***/

    public function note_list_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!=''  && $token!='' && $user_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $notes = $this->Note_model->getNotesListByEventIdNew($event_id,$user_id);
                $date_format = $this->Note_model->checkEventDateFormat($event_id);

                $data = array(
                    'note_list' => $notes,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add Notes List Latest ***/

    public function add_note_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $heading=$this->input->post('heading');
        $description=$this->input->post('description');
        $Menu_id=$this->input->post('Menu_id');
        $Module_id=$this->input->post('Module_id');
        $is_cms=$this->input->post('is_cms');

        if($event_id!='' && $token!='' && $user_id!='' && $heading!='' && $description!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {   
                $timezone = $this->Note_model->get_event_timezone($event_id);
                date_default_timezone_set("UTC");
                $cdate=date('Y-m-d H:i:s');
                if(!empty($timezone['Event_show_time_zone']))
                {
                    if(strpos($timezone['Event_show_time_zone'],"-")==true)
                    { 
                      $arr=explode("-",$timezone['Event_show_time_zone']);
                      $intoffset=$arr[1]*3600;
                      $intNew = abs($intoffset);
                      $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                    }
                    if(strpos($timezone['Event_show_time_zone'],"+")==true)
                    {
                      $arr=explode("+",$timezone['Event_show_time_zone']);
                      $intoffset=$arr[1]*3600;
                      $intNew = abs($intoffset);
                      $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                    }
                }

                $add_arr['Heading']=$heading;
                $add_arr['Description']=$description;
                $add_arr['Created_at']=$cdate;
                $add_arr['Event_id']=$event_id;
                $add_arr['User_id']=$user_id;
                $add_arr['Organisor_id']=$this->Note_model->getOrganisorIdByUserId($user_id,$event_id);
                
                if(!empty($Menu_id) && is_numeric($Module_id))
                {
                    $add_arr['Menu_id']=$Menu_id;
                }
                
                $add_arr['is_cms'] = $is_cms;
                $add_arr['Module_id']=$Module_id;


                $notes = $this->Note_model->add_note($add_arr);
                $data = array(
                  'success' => true,
                  'message' => "Successfully added",
                  'insert_id' => $notes
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Add Notes List Latest ***/

    public function delete_note()
    {   
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $note_id=$this->input->post('note_id');
        if($event_id!='' && $token!='' && $user_id!='' && $note_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {
                $notes = $this->Note_model->delete_note($note_id,$update_arr);
                $notes = $this->Note_model->getNotesListByEventIdNew($event_id,$user_id);
                $date_format = $this->Note_model->checkEventDateFormat($event_id);

                $data = array(
                    'note_list' => $notes,
                );
                $data = array(
                  'success' => true,
                  'data' => $data,
                  'message' => "Successfully Deleted"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /*** Download PDF ***/

    public function download_pdf()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $user_id!='')
        {
            $notes = $this->Note_model->getNotesListByEventIdNew($event_id,$user_id);
            $date_format = $this->Note_model->checkEventDateFormat($event_id);
            $user =  $this->Note_model->get_user_info($user_id);
            $event = $this->Note_model->get_event($event_id);
            $htm = "<br><h1>".$event['Event_name']."</h1><h2>".$user['Firstname']." ".$user['Lastname']."</h2><br><br>";
            foreach ($notes as $key => $value)
            {   
                $htm .="<h3>".$value['Heading']."</h3>";
                $htm .="<h5>".$value['title']."</h5>";
                $htm .="<p>".$value['Description']."</p>";
                $htm .="<small>".$value['time']."</small>";
                $htm .="<br><br>";
            }

            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
            $pdf->SetTopMargin(10);
            $pdf->SetTitle($event['Event_name'].' ('.$user['Firstname'].' '.$user['Lastname'].')');
            $pdf->AddPage();
            $pdf->writeHTML($htm);
            // $pdf->Output('notes.pdf', 'I');die;
            if (!file_exists('./assets/notes/'.$event_id)) {
                mkdir('./assets/notes/'.$event_id, 0777, true);
            }
            $filename = $user_id.'-'.time();
            file_put_contents('./assets/notes/'.$event_id.'/'.$filename.'.pdf',$pdf->Output('notes.pdf', 'S'));
            $name = base_url()."assets/notes/".$event_id."/".$filename.".pdf";
            $data['pdf_name']=$name;
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
