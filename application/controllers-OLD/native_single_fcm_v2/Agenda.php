<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Agenda extends CI_Controller

{
    public $menu;

    public $cmsmenu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_template_model');
        $this->load->model('native_single_fcm_v2/Agenda_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Attendee_model');
        date_default_timezone_set("UTC");
        $this->menu = $this->Event_model->geteventmenu_list($this->input->post('event_id') , null, 1, $this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
    }

    public function getAgendaByTime()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('_token');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda = $this->Agenda_model->getAllAgendaByTimeEvent($event_id, null, $user_id, $lang_id, $category_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                if (empty($agenda))
                {
                    $agenda = array();
                }
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getAgendaByType()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('_token');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda = $this->Agenda_model->getAllAgendaByTypeEvent($event_id, null, null, $user_id, $lang_id, $category_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array();
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getUserAgendaByType()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                $meeting = $this->Agenda_model->getAllMeetingsByType($event_id, $user_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $str = $agenda_data[0]['agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $agenda = $this->Agenda_model->getAllAgendaByType($event_id, 0, $agenda_id_arr, NULL, $lang_id);
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (!empty($meeting))
                {
                    foreach($meeting as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("m/d/Y", $a);
                            }
                            $meeting[$key1]['data'][$key]['date'] = $start_date;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->Agenda_model->getShowSuggestionButton($user_id, $event_id) ,
                    'time_format' => $time_format[0]['format_time'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getUserAgendaByTime()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if ($user_id == '0')
            {
                $data = array(
                    'event' => $user[0],
                    'agenda' => [],
                    'meeting' => [],
                    'show_suggest_button' => '',
                    'time_format' => ''
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
                echo json_encode($data);
                exit;
            }
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $str = $agenda_data[0]['agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $agenda = $this->Agenda_model->getAllAgendaByTime($event_id, $agenda_id_arr, $user_id, $lang_id, $category_id);
                $meeting = $this->Agenda_model->getAllMeetings($event_id, $user_id);
                if (!empty($meeting))
                {
                    foreach($meeting as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['time']);
                            if ($time_format[0]['time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $a = strtotime($value['date']);
                            $start_date = date("l, M jS, Y", $a);
                            $meeting[$key1]['date'] = $start_date;
                            $meeting[$key1]['date_time'] = $start_date;
                            $meeting[$key1]['data'][$key]['time'] = $start_time;
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("m/d/Y", $a);
                            }
                            $meeting[$key1]['data'][$key]['date'] = $start_date;
                        }
                    }
                }
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->Agenda_model->getShowSuggestionButton($user_id, $event_id) ,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    // 123

    public function getUserAgendaByTimeWithMeeting()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if ($user_id == '0')
            {
                $data = array(
                    'event' => $user[0],
                    'agenda' => [],
                    'meeting' => [],
                    'show_suggest_button' => '',
                    'time_format' => ''
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
                echo json_encode($data);
                exit;
            }
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $str = $agenda_data[0]['agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $agenda = $this->Agenda_model->getAllAgendaByTime($event_id, $agenda_id_arr, $user_id, $lang_id, $category_id);
                if($event_id != '1573')
                {
                    $meeting = $this->Agenda_model->getAllMeetingRequestWithDate($event_id, $user_id);
                }
                $date_format = $this->Attendee_model->checkEventDateFormat($event_id);

                function sortByTime2($a, $b)
                {
                    $a = $a['time'];
                    $b = $b['time'];
                    if ($a == $b)
                    {
                        return 0;
                    }
                    return ($a < $b) ? -1 : 1;
                }
                usort($meeting, 'sortByTime2');

                for ($i = 0; $i < count($meeting); $i++)
                {
                    $timedate = $meeting[$i]['date'];
                    $prev = "";
                    if ($prev == "" || $timedate != $prev)
                    {
                        $prev = $timedate;
                        if ($date_format[0]['date_format'] == 0)
                        {
                            $a = strtotime($meeting[$i]['date']);
                            $date = date("d/m/Y", $a);
                        }
                        else
                        {
                            $a = strtotime($meeting[$i]['date']);
                            $date = date("m/d/Y", $a);
                        }
                        $meeting[$i]['date'] = $date;
                        $meeting[$i]['is_exhi'] = $meeting[$i]['exhi_user_id'] ? '1' : '0';
                        $meeting[$i]['stand_number'] = $meeting[$i]['stand_number'] ?:'';
                        $meeting[$i]['is_meeting'] = '1';
                        $meeting[$i]['metting_id'] = $meeting[$i]['request_id'];
                        unset($meeting[$i]['Firstname']);
                        unset($meeting[$i]['request_id']);
                        if(!empty($meeting[$i]['metting_id']))
                        $meeting_tmp[$timedate][] = $meeting[$i];
                    }
                }
                

                $j = 0;
                foreach($meeting_tmp as $key => $value)
                {
                    $data[$j]['date'] = $key;
                    $data[$j]['date_time'] = $key;
                    $data[$j]['data'] = $value;
                    $j++;
                }
                function sortByTime1($a, $b)
                {
                    $a = $a['date'];
                    $b = $b['date'];
                    if ($a == $b)
                    {
                        return 0;
                    }
                    return ($a < $b) ? -1 : 1;
                }
                usort($data, 'sortByTime1');
                $meeting = $data;
                if (!empty($meeting))
                {
                    foreach($meeting as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['time']);
                            if ($time_format[0]['time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $a = strtotime($value['date']);
                            $start_date = date("l, M jS, Y", $a);
                            $meeting[$key1]['date'] = $start_date;
                            $meeting[$key1]['date_time'] = $start_date;
                            $meeting[$key1]['data'][$key]['time'] = $start_time;
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("m/d/Y", $a);
                            }
                            $meeting[$key1]['data'][$key]['date'] = $start_date;
                        }
                    }
                }
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting?:[],
                    'show_suggest_button' => $this->Agenda_model->getShowSuggestionButton($user_id, $event_id) ,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getAgendaById()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $event_type != '' && $agenda_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_value = $this->Agenda_model->getAgendaById($event_id, $agenda_id, $user_id, $lang_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                if (!empty($agenda_value))
                {
                    if ($event_id == '992' || $event_id == '1107')
                    {
                        // $agenda_value[0]['show_checking_in'] = '0';
                        $agenda_value[0]['show_rating'] = '0';
                    }
                    $Start_time = strtotime($agenda_value[0]['Start_time']);
                    $start_time = date("H:i", $Start_time);
                    $bh_stime = date("H:i:s A", $Start_time);
                    $End_time = strtotime($agenda_value[0]['End_time']);
                    $end_time = date("H:i", $End_time);
                    $a = strtotime($agenda_value[0]['Start_date']);
                    $start_date_api = str_replace('-', '/', date("d-m-Y", $a));
                    $bh_sdate = date("m-d-Y", $a);
                    $a = strtotime($agenda_value[0]['End_date']);
                    $end_date_api = str_replace('-', '/', date("d-m-Y", $a));
                    if ($date_format[0]['date_format'] == 0)
                    {
                        $a = strtotime($agenda_value[0]['Start_date']);
                        $start_date = date("d/m/Y", $a);
                        $a = strtotime($agenda_value[0]['End_date']);
                        $end_date = date("d/m/Y", $a);
                    }
                    else
                    {
                        $a = strtotime($agenda_value[0]['Start_date']);
                        $start_date = date("m/d/Y", $a);
                        $a = strtotime($agenda_value[0]['End_date']);
                        $end_date = date("m/d/Y", $a);
                    }
                    $agenda_value[0]['Start_date'] = $start_date;
                    $agenda_value[0]['Start_date_cal'] = $start_date_api;
                    $agenda_value[0]['start_date_time'] = $bh_sdate . " " . $bh_stime;
                    $agenda_value[0]['End_date_cal'] = $end_date_api;
                    $agenda_value[0]['End_date'] = $end_date;
                    $agenda_value[0]['Start_time'] = $start_time;
                    $agenda_value[0]['End_time'] = $end_time;
                    $curr_date = date("Y-m-d");
                    $start1_date = date("Y-m-d", $a);
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['agenda_id'] = $agenda_id;
                    $agenda_value[0]['show_comment_box'] = $this->Agenda_model->checkShowCommentBox($where);
                    $agenda_value[0]['show_reminder_button'] = $date_format[0]['show_reminder_button'];
                    if ($agenda_value[0]['show_comment_box'] == '0' && $agenda_value[0]['show_feedback'] == '1')
                    {
                        $agenda_value[0]['show_feedback'] = '0';
                    }
                    $agenda_value[0]['description'] = html_entity_decode($agenda_value[0]['description']);
                }
                if ($user_id != '')
                {
                    $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                }
                else
                {
                    $flag = "login_0";
                }
                $uhg_event_id = array(
                    1307,
                    1316,
                    1317,
                    1318,
                    1319,
                    1320,
                    1330,
                    1331,
                    1332,
                    1333,
                    1334,
                    1335,
                    1336,
                    1337,
                    1338,
                    1339,
                    1340
                );
                if (in_array($event_id, $uhg_event_id))
                {
                    // $show_session_button = ($agenda_value[0]['Types'] == 'Personal Stories') ? '1' : '0';
                    $show_session_button = '0';
                }
                elseif ($event_id == '1169')
                {
                    // $show_session_button = ($agenda_value[0]['Types'] == 'R&D Breakout Sessions') ? '0' : '1';
                    $show_session_button = '0';
                }
                else
                {
                    $show_session_button = '1';
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda_value,
                    'reminder' => [],
                    'time_format' => $time_format[0]['format_time'],
                    'user_flag' => $flag,
                    'show_session_button' => $show_session_button,
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function saveUserAgenda()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $confirm_clash = ($this->input->post('confirm_clash')) ? : '0';
        if ($event_id != '' && $agenda_id != '' && $user_id)
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $is_pending_agenda = $this->Agenda_model->getIsPendingAgenda($event_id);
                $allow_clashing = $this->Agenda_model->getAgendaData($agenda_id);
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                if (!empty($agenda_data))
                {
                    $str = $agenda_data[0][($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'];
                    $agenda_id_arr = explode(',', $str);
                    if (in_array($agenda_id, $agenda_id_arr))
                    {
                        $data = array(
                            'success' => false,
                            'msg' => "Agenda already saved",
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        if ($allow_clashing[0]['allow_clashing'] == 0 && $this->Agenda_model->get_overlapping_agenda($allow_clashing, $user_id, $is_pending_agenda) == true)
                        {
                            if ($user_id != '')
                            {
                                $max_people = $this->Agenda_model->getMaxPeople($agenda_id, $user_id);
                            }
                            $agenda_id_arr[] = $agenda_id;
                            $agenda_id_str = (count($agenda_id_arr) == 1) ? $agenda_id : implode(',', $agenda_id_arr);
                            $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'] = $agenda_id_str;
                            $this->Agenda_model->updateUserAgenda($user_id, $data);
                            $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                            $data = array(
                                'success' => true,
                                'msg' => "Successfully updated",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,
                            );
                        }
                        elseif ($allow_clashing[0]['allow_clashing'] == 1 || $confirm_clash == 1)
                        {
                            if ($user_id != '')
                            {
                                $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                                $max_people = $this->Agenda_model->getMaxPeople($agenda_id, $user_id);
                            }
                            $data['user_id'] = $user_id;
                            array_push($agenda_id_arr, $agenda_id);
                            $agenda_id_str = (count($agenda_id_arr) == 1) ? $agenda_id : implode(',', $agenda_id_arr);
                            $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'] = $agenda_id_str;
                            $this->Agenda_model->addUserAgenda($data);
                            $data = array(
                                'success' => true,
                                'msg' => "Successfully Added.",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,
                            );
                        }
                        else
                        {
                            $data = array(
                                'success' => false,
                                'msg' => "You can not save this agenda. It is overlapping with another",
                                'agenda_id' => $agenda_id,
                            );
                            if ($event_id == '992' || $event_id == '1107')
                            {
                                $data['msg'] = 'This session clashes with another session you have saved â€“ Are you sure you want to save this?';
                            }
                        }
                    }
                }
                else
                {
                    if ($allow_clashing[0]['allow_clashing'] == 0 && $this->Agenda_model->get_overlapping_agenda($allow_clashing, $user_id, $is_pending_agenda) == true)
                    {
                        if ($user_id != '')
                        {
                            $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                            $max_people = $this->Agenda_model->getMaxPeople($agenda_id, $user_id);
                        }
                        $data['user_id'] = $user_id;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'] = $agenda_id;
                        $this->Agenda_model->addUserAgenda($data);
                        $data = array(
                            'success' => true,
                            'msg' => "Successfully Added.",
                            'user_flag' => $flag,
                            'Maximum_people' => ($max_people) ? $max_people : 0,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    elseif ($allow_clashing[0]['allow_clashing'] == 1 || $confirm_clash == 1)
                    {
                        if ($user_id != '')
                        {
                            $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                            $max_people = $this->Agenda_model->getMaxPeople($agenda_id, $user_id);
                        }
                        $data['user_id'] = $user_id;
                        // $data['agenda_id']=$agenda_id;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'] = $agenda_id;
                        $this->Agenda_model->addUserAgenda($data);
                        $data = array(
                            'success' => true,
                            'msg' => "Successfully Added.",
                            'user_flag' => $flag,
                            'Maximum_people' => ($max_people) ? $max_people : 0,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg' => "You can not save this agenda. It is overlapping with another.",
                            'agenda_id' => $agenda_id,
                        );
                        if ($event_id == '992' || $event_id == '1107')
                        {
                            $data['msg'] = 'This session clashes with another session you have saved â€“ Are you sure you want to save this?';
                        }
                    }
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function deleteUserAgenda()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $agenda_id != '' && $user_id)
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                if (!empty($agenda_data))
                {
                    $str = $agenda_data[0]['agenda_id'];
                    $agenda_id_arr = explode(',', $str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false)
                    {
                        unset($agenda_id_arr[$key]);
                        if (count($agenda_id_arr) == 0)
                        {
                            $this->Agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['agenda_id'] = $agenda_id_str;
                            $this->Agenda_model->updateUserAgenda($user_id, $data);
                        }
                        if ($user_id != '')
                        {
                            $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg' => "Successfully deleted",
                            'user_flag' => $flag,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg' => "No Agenda found.",
                            'agenda_id' => $agenda_id,
                        );
                    }
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'msg' => "No Agenda found",
                        'agenda_id' => $agenda_id,
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function checkInAgenda()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $agenda_id != '' && $user_id)
        {
            $event_data = $this->Event_model->getEvent($event_id);
            $agenda_data = $this->Agenda_model->getCheckInAgendaByUserId($user_id);
            $agenda_value = $this->Agenda_model->getAgendaById($event_id, $agenda_id, $user_id);
            $cdate = date('Y-m-d H:i:s');
            if (!empty($event_data[0]['checking_datetime']))
            {
                $cdate = $this->getTimeZoneDate($event_data);
            }
            else
            {
                $event_data[0]['checking_datetime'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - 3600);
            }
            $str = $agenda_data[0]['check_in_agenda_id'];
            $agenda_id_arr = explode(',', $str);
            if (in_array($agenda_id, $agenda_id_arr))
            {
                $key = array_search($agenda_id, $agenda_id_arr);
                unset($agenda_id_arr[$key]);
                $agenda_id_str = implode(',', $agenda_id_arr);
                $data['check_in_agenda_id'] = $agenda_id_str;
                $this->Agenda_model->updateUserAgenda($user_id, $data);
                $this->Agenda_model->updateCheckIn($user_id, $agenda_id);
                $data = array(
                    'success' => true,
                    'msg' => "Check In removed",
                    'btn_color' => '#428bca',
                );
            }
            else
            {
                if ($event_data[0]['checking_datetime'] <= $cdate)
                {
                    $str = $agenda_data[0]['agenda_id'];
                    $agenda_id_arr = explode(',', $str);
                    $str = $agenda_data[0]['check_in_agenda_id'];
                    $check_in_agenda_id_arr = explode(',', $str);
                    $key = array_search($agenda_id, $agenda_id_arr);
                    $agenda_ids = array();
                    for ($i = 0; $i < $key; $i++)
                    {
                        $agenda_ids[] = $agenda_id_arr[$i];
                    }
                    $rating_data = $this->Agenda_model->getRatingDataNew($user_id, ($key == 0) ? $agenda_id_arr[$key] : $agenda_ids, $event_id);
                    $force_checkin_events = array(
                        '1107',
                        '992'
                    );
                    if (empty($rating_data) || in_array($event_id, $force_checkin_events))
                    {
                        $data['user_id'] = $user_id;
                        $data['check_in_agenda_id'] = ($key) ? $agenda_data[0]['check_in_agenda_id'] . ',' . $agenda_id : $agenda_id;
                        $this->Agenda_model->addUserAgenda($data);
                        $this->Agenda_model->saveCheckIn($agenda_id, $user_id);
                        $data = array(
                            'success' => true,
                            'msg' => "Successfully Checked In",
                            'btn_color' => '#5cb85c',
                        );
                    }
                    else
                    {
                        $rating_data = $this->Agenda_model->getRatingDataNew($user_id, ($key == 0) ? $agenda_id_arr[$key] : $agenda_ids, $event_id);
                        $agenda_data1 = $this->Agenda_model->getAgendaNameTimeById($rating_data['agenda_id']);
                        $is_checkin = (in_array($rating_data['agenda_id'], $check_in_agenda_id_arr)) ? 0 : 1;
                        $data = array(
                            'success' => false,
                            'message1' => "You must rate: ",
                            'message2' => ($agenda_data1) ? $agenda_data1 : new stdClass,
                            'message3' => "before you can Check In to this session.",
                            'show_check_in' => $is_checkin,
                            'flag' => 1,
                        );
                    }
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'msg' => "You cannot check in for this session. It is not started.",
                        'flag' => 0,
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getTimeZoneDate($event_templates)
    {
        if (!empty($event_templates[0]['Event_show_time_zone']))
        {
            if (strpos($event_templates[0]['Event_show_time_zone'], "-") == true)
            {
                $arr = explode("-", $event_templates[0]['Event_show_time_zone']);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) - $intNew);
            }
            if (strpos($event_templates[0]['Event_show_time_zone'], "+") == true)
            {
                $arr = explode("+", $event_templates[0]['Event_show_time_zone']);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) + $intNew);
            }
        }
        return $cdate;
    }

    public function saveRating()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $rating = $this->input->post('rating');
        if ($event_id != '' && $agenda_id != '' && $user_id != '' && $rating != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $ratings = $this->Agenda_model->getRatingData($user_id, $agenda_id);
                $rating_data['user_id'] = $user_id;
                $rating_data['session_id'] = $agenda_id;
                $rating_data['rating'] = $rating;
                $rating_data['date'] = date('Y-m-d H:i:s');
                if (count($ratings))
                {
                    $this->Agenda_model->updateRating($rating_data, $ratings->Id);
                    $data = array(
                        'success' => true,
                        'msg' => "Rating updated successfully",
                    );
                }
                else
                {
                    $this->Agenda_model->insertRating($rating_data);
                    $data = array(
                        'success' => true,
                        'msg' => "Rating saved successfully",
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getPendingUserAgendaByTime()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getPendingAgendaByUserId($user_id);
                $str = $agenda_data[0]['pending_agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $agenda = $this->Agenda_model->getAllAgendaByTime($event_id, $agenda_id_arr, NULL, $lang_id);
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getPendingUserAgendaByType()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getPendingAgendaByUserId($user_id);
                $str = $agenda_data[0]['pending_agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $agenda = $this->Agenda_model->getAllAgendaByType($event_id, 0, $agenda_id_arr, NULL, $lang_id);
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function savePendingUserAgenda()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id)
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $agenda_data = $this->Agenda_model->getPendingAgendaByUserId($user_id);
                if (!empty($agenda_data))
                {
                    $str = $agenda_data[0]['pending_agenda_id'];
                    $agenda_id_arr = explode(',', $str);
                    $result = $this->Agenda_model->savePendingAgenda($user_id);
                    $data = array(
                        'success' => true,
                        'data' => ($result != null) ? $result : [],
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function deleteUserPendingAgenda()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $agenda_id != '' && $user_id)
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $agenda_data = $this->Agenda_model->getPendingAgendaByUserId($user_id);
                if (!empty($agenda_data))
                {
                    $str = $agenda_data[0]['pending_agenda_id'];
                    $agenda_id_arr = explode(',', $str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false)
                    {
                        unset($agenda_id_arr[$key]);
                        if (count($agenda_id_arr) == 0)
                        {
                            $this->Agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['pending_agenda_id'] = $agenda_id_str;
                            $this->Agenda_model->updateUserAgenda($user_id, $data);
                        }
                        if ($user_id != '')
                        {
                            $flag = $this->Agenda_model->checkAgendaByUserId($agenda_id, $user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg' => "Successfully deleted",
                            'user_flag' => $flag,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg' => "No Agenda found.",
                            'agenda_id' => $agenda_id,
                        );
                    }
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'msg' => "No Agenda found",
                        'agenda_id' => $agenda_id,
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function saveAgendaComments()
    {
        $event_id = $this->input->post('event_id');
        $agenda_id = $this->input->post('agenda_id');
        $comments = $this->input->post('comments');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $agenda_id != '' && $comments != '' && $user_id != '')
        {
            $insert_data['event_id'] = $event_id;
            $insert_data['agenda_id'] = $agenda_id;
            $insert_data['comments'] = $comments;
            $insert_data['user_id'] = $user_id;
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $this->Agenda_model->saveAgendaComments($insert_data);
            $data = array(
                'success' => true,
                'message' => "Thank you for your feedback",
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getUserAgendaByTimeNew()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getAgendaByUserId($user_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $str = $agenda_data[0]['agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $agenda = $this->Agenda_model->getAllAgendaByTimeNew($event_id, $agenda_id_arr);
                $meeting = $this->Agenda_model->getAllMeetings($event_id, $user_id);
                if (!empty($meeting))
                {
                    foreach($meeting as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['time']);
                            if ($time_format[0]['time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $a = strtotime($value['date']);
                            $start_date = date("l, M jS, Y", $a);
                            $meeting[$key1]['date'] = $start_date;
                            $meeting[$key1]['date_time'] = $start_date;
                            $meeting[$key1]['data'][$key]['time'] = $start_time;
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['date']);
                                $start_date = date("m/d/Y", $a);
                            }
                            $meeting[$key1]['data'][$key]['date'] = $start_date;
                        }
                    }
                }
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->Agenda_model->getShowSuggestionButton($user_id, $event_id) ,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getPendingUserAgendaByTimeNew()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda_data = $this->Agenda_model->getPendingAgendaByUserId($user_id);
                $str = $agenda_data[0]['pending_agenda_id'];
                if ($str != '')
                {
                    $agenda_id_arr = explode(',', $str);
                }
                else
                {
                    $agenda_id_arr[0] = NULL;
                }
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                $agenda = $this->Agenda_model->getAllAgendaByTimeNew($event_id, $agenda_id_arr);
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                if (empty($agenda))
                {
                    $agenda = array(
                        
                    );
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getSystemMemInfo()
    {
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach($data as $line)
        {
            list($key, $val) = array_pad(explode(":", $line, 2) , 2, null);
            $meminfo[$key] = trim($val);
        }
        return $meminfo;
    }

    public function getAgendaByTime_load()
    {
        $this->load->model('native_single_fcm_v2/App_login_model');
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        // $this->output->enable_profiler(TRUE);
        $starttime = $this->App_login_model->get_endtime_of_last_rec_50($this->input->post('event_id'));
        $diff = microtime(true) - $starttime;
        $sec = intval($diff);
        $micro = $diff - $sec;
        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));
        $mem = $this->getSystemMemInfo();
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('_token');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $time_format = $this->Agenda_model->getTimeFormat($event_id);
                $agenda = $this->Agenda_model->getAllAgendaByTimeEvent($event_id, null, $user_id, $lang_id, $category_id);
                $date_format = $this->Agenda_model->checkEventDateFormat($event_id);
                if (empty($agenda))
                {
                    $agenda = array();
                }
                if (!empty($agenda))
                {
                    foreach($agenda as $key1 => $value)
                    {
                        foreach($value['data'] as $key => $value)
                        {
                            $Start_time = strtotime($value['Start_time']);
                            if ($time_format[0]['format_time'] == '0') $start_time = date("h:i a", $Start_time);
                            else $start_time = date("H:i", $Start_time);
                            $End_time = strtotime($value['End_time']);
                            if ($time_format[0]['format_time'] == '0') $end_time = date("h:i a", $End_time);
                            else $end_time = date("H:i", $End_time);
                            if ($date_format[0]['date_format'] == 0)
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("d/m/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("d/m/Y", $a);
                            }
                            else
                            {
                                $a = strtotime($value['Start_date']);
                                $start_date = date("m/d/Y", $a);
                                $a = strtotime($value['End_date']);
                                $end_date = date("m/d/Y", $a);
                            }
                            $agenda[$key1]['data'][$key]['Start_date'] = $start_date;
                            $agenda[$key1]['data'][$key]['End_date'] = $end_date;
                            $agenda[$key1]['data'][$key]['Start_time'] = $start_time;
                            $agenda[$key1]['data'][$key]['End_time'] = $end_time;
                        }
                    }
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start) , 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['event_id'] = $event_id;
        $insert['type'] = 'Agenda';
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->App_login_model->add_server_data_50($insert);
    }

    public function get_agenda_offline()
    {
        $event_id = $this->input->post('event_id');
        $lang_id = $this->input->post('lang_id');
        if (!empty($event_id))
        {
            $data['agenda_list'] = $this->Agenda_model->get_agenda_offline($event_id, $lang_id);
            $data['category'] = $this->Agenda_model->get_agenda_category_offline($event_id);
            $data['category_relation'] = $this->Agenda_model->get_agenda_category_relation_offline($event_id);
            $data['types'] = $this->Agenda_model->get_agenda_types_offline($event_id);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }

    public function get_agenda_offline_test()
    {
        $event_id = $this->input->get_post('event_id');
        $lang_id = $this->input->post('lang_id');
        if (!empty($event_id))
        {
            $data['agenda_list'] = $this->Agenda_model->get_agenda_offline($event_id, $lang_id);
            $data['category'] = $this->Agenda_model->get_agenda_category_offline($event_id);
            $data['category_relation'] = $this->Agenda_model->get_agenda_category_relation_offline($event_id);
            $data['types'] = $this->Agenda_model->get_agenda_types_offline($event_id);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
}
