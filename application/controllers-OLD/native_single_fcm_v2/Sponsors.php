<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sponsors extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Sponsors_model');
        
        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    /** Get sponsors List **/

    public function sponsors_list()
    {
        //$start_fun_time = microtime(true);
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                //$user_id = $this->Sponsors_model->getUserId($token);
                $user_id = '';
                $sponsors_list = $this->Sponsors_model->getSponsorsListByEventId($event_id,$user_id);
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                if($org_id != '311376' && $org_id != '359852')
                {
                    foreach ($sponsors_list as $key1 => $value1)
                    {   
                       
                        foreach ($value1['data'] as $key => $value) 
                            {    
                                
                                if(!empty($value['company_logo']))
                                {
                                    $source_url = base_url()."assets/user_files/".$value['company_logo'];
                                    $info = getimagesize($source_url);
                                    //$new_name = "new_".$value['company_logo'];
                                    $new_name = $value['company_logo'];
                                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                                    if ($info['mime'] == 'image/jpeg')
                                    {   
                                        $quality = 50;
                                        $image = imagecreatefromjpeg($source_url);
                                        imagejpeg($image, $destination_url, $quality);
                                    }
                                    elseif ($info['mime'] == 'image/gif')
                                    {   
                                        $quality = 5;
                                        $image = imagecreatefromgif($source_url);
                                        imagegif($image, $destination_url, $quality);

                                    }
                                    elseif ($info['mime'] == 'image/png')
                                    {   
                                        $quality = 5;
                                        $image = imagecreatefrompng($source_url);
                                        $background = imagecolorallocatealpha($image,255,0,255,127);
                                        imagecolortransparent($image, $background);
                                        imagealphablending($image, false);
                                        imagesavealpha($image, true);
                                        imagepng($image, $destination_url, $quality);
                                    }
                                    $sponsors_list[$key1]['data'][$key]['company_logo'] = $new_name;
                                }
                            }
                    }
                }
                $data = array(
                    'sponsors_list' => $sponsors_list,
                   /* 'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /** Get Sponsors Details **/

    public function sponsors_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $sponsor_id=$this->input->post('sponsor_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!='' && $event_type!='' && $sponsor_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {   
                $sponsors = $this->Sponsors_model->getSponsorsDetails($sponsor_id,$event_id,$token,$lang_id);
                if(!empty($sponsors[0]))
                {   
                    $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                    if($org_id == '311376' || $org_id == '359852')
                    {
                        $sponsors[0]['company_logo'] = json_decode($sponsors[0]['company_logo'])[0];
                    }
                    else if(!empty($sponsors[0]['company_logo']))
                    {   
                        $company_logo_decode=json_decode($sponsors[0]['company_logo']);
                        $value['company_logo'] = $company_logo_decode[0];
                        $this->load->library('image_lib');
                        $source_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$value['company_logo'];
                        $info = getimagesize($source_url);
                        $new_name = "new_".$value['company_logo'];
                        $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/thumb/".$new_name;

                        if(!file_exists($destination_url))
                        {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = $source_url;
                            $config['new_image'] = $destination_url;
                            $config['create_thumb'] = TRUE;
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = '250';
                            $config['height'] = '250';
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $extension_pos = strrpos($new_name, '.');
                            $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                            $sponsors[0]['company_logo'] = 'thumb/'.$new_name;
                        }
                        elseif(file_exists($destination_url))
                        {
                            $sponsors[0]['company_logo'] = 'thumb/'.$new_name;    
                        }
                        else
                        {
                            $sponsors[0]['company_logo']=($company_logo_decode[0]) ? $company_logo_decode[0] : '';
                        }
                    }
                    if(!empty($sponsors[0]['Images']))
                    {
                        if($sponsors[0]['Images']=="[]")
                        {

                            $sponsors[0]['Images']=[];
                        }
                        else
                        {
                            $image_decode=json_decode($sponsors[0]['Images']);
                            $sponsors[0]['Images']=($image_decode) ? : [] ;
                        }
                        
                    }
                    else
                    {
                        $sponsors[0]['Images']=[];
                    }
                    $sponsors[0]['Description'] = html_entity_decode($sponsors[0]['Description']);
                }
                $data = array(
                    'sponsors_details' => ($sponsors[0]) ? $sponsors[0] : new stdClass,
                   /* 'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /** Get Sponsors List For Offline **/

    public function sponsors_list_offline()
    {
        //$start_fun_time = microtime(true);
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        if($event_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                //$user_id = $this->Sponsors_model->getUserId($token);
                $user_id = '';
                $sponsors_list = $this->Sponsors_model->getSponsorsListByEventId_offline($event_id,$user_id);
                $this->load->library('image_lib');
                $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
                if($org_id != '311376' && $org_id != '359852')
                {

                    foreach ($sponsors_list as $key1 => $value1)
                    {   

                        foreach ($value1['data'] as $key => $value) 
                            {    
                                $new_name = "new_".$value['company_logo'];
                                $extension_pos = strrpos($new_name, '.');
                                $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                                $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/thumb/".$new_name;
                                    
                                if(!empty($value['company_logo']) && !file_exists($destination_url))
                                {   
                                    $source_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$value['company_logo'];
                                    $info = getimagesize($source_url);
                                    $new_name = "new_".$value['company_logo'];
                                    // $new_name = $value['company_logo'];
                                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/thumb/".$new_name;

                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $source_url;
                                    $config['new_image'] = $destination_url;
                                    $config['create_thumb'] = TRUE;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width'] = '250';
                                    $config['height'] = '250';
                                    $this->image_lib->initialize($config);
                                    $this->image_lib->resize();
                                    $extension_pos = strrpos($new_name, '.');
                                    $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                                    $sponsors_list[$key1]['data'][$key]['company_logo'] = 'thumb/'.$new_name;
                                }
                                elseif(file_exists($destination_url))
                                {
                                    $sponsors_list[$key1]['data'][$key]['company_logo'] = 'thumb/'.$new_name;
                                }

                            }
                    }
                }
                $data = array(
                    'sponsors_list' => $sponsors_list,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /** Get Favorite Sponsors **/

    public function get_favorited_sponser()
    {
        $event_id = $this->input->post('event_id');
        $user_id  = $this->input->post('user_id');
        if(!empty($event_id) && !empty($user_id))
        {   

            $data = $this->Sponsors_model->get_favorited_sponser($event_id,$user_id);
            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
}
