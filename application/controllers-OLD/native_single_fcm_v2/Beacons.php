<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Beacons extends CI_Controller 
{	
	function __construct() 
	{
		parent::__construct();
        $this->load->model('native_single_fcm_v2/Beacon_model');
        include('application/libraries/nativeGcm.php');
        include('application/libraries/FcmV2.php');
        $this->load->model('native_single_fcm_v2/Exhibitor_model');
        $this->load->model('native_single_fcm_v2/Gamification_model');
	}

	/*** Save Beacon from Native ***/
	public function save_beacon()
	{
		$event_id=$this->input->post('event_id');
		$UDID=$this->input->post('UDID');
		$organizer_id=$this->input->post('organizer_id');
		$major=$this->input->post('major');
		$minor=$this->input->post('minor');
		$nameSpaceId=$this->input->post('nameSpaceId');
		$InstanceId=$this->input->post('InstanceId');

		if($event_id!='' && ($UDID!='' || $nameSpaceId!='' && $InstanceId!='') && $organizer_id!='')
		{
			
			$insert['event_id'] = $event_id;
			if($UDID != '')
			$insert['UDID'] = $UDID;
			$insert['organizer_id'] = $organizer_id;
			$insert['beacon_name'] = "Beacon";
			if($major != '')
			$insert['major'] = $major;
			if($minor != '')
			$insert['minor'] = $minor;
			if($nameSpaceId!='')
			$insert['nameSpaceId'] = $nameSpaceId;
			if($InstanceId!='')
			$insert['InstanceId'] = $InstanceId;

			if($UDID != '')
			$where['UDID'] = $UDID;
			$where['organizer_id'] = $organizer_id;
			$where['event_id'] = $event_id;
			if($major != '')
			$where['major'] = $major;
			if($minor != '')
			$where['minor'] = $minor;
			if($nameSpaceId!='')
			$where['nameSpaceId'] = $nameSpaceId;
			if($InstanceId!='')
			$where['InstanceId'] = $InstanceId;

			$msg = $this->Beacon_model->insert_beacon($insert,$where);
			$data = array(
				'success' => true,
				'message' => $msg
				);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}

	/*** Rename Beacon from Native ***/

	public function rename_beacon()
	{
		$beacon_name = $this->input->post('beacon_name');
		$id = $this->input->post('id');
		$event_id=$this->input->post('event_id');
		$organizer_id=$this->input->post('organizer_id');

		if($beacon_name!='' && $id!='' && $event_id!='' && $organizer_id!='')
		{
			$data['beacon_name'] = $beacon_name;
			$update = $this->Beacon_model->edit_beacon($id,$data);
			$updated_data = $this->Beacon_model->get_all_beacon($event_id,$organizer_id);
			$msg = array(
				'success' => true,
				'message' => $update,
				'data' => $updated_data
				);
			echo json_encode($msg);exit();
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
			echo json_encode($data);
		}

	}

	/*** Delete Beacon from Native ***/

    public function delete_beacon()
	{
		$id = $this->input->post('id');
		$event_id=$this->input->post('event_id');
		if($id!='')
		{
			$data['beacon_name'] = $beacon_name;
			$update = $this->Beacon_model->delete_beacon($id);
			$msg = array(
				'success' => true,
				'message' => $update
				);
			echo json_encode($msg);exit();
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
			echo json_encode($data);
		}

	}

	/*** Get all Beacons in Native ***/

	public function get_all_beacon()
	{
		$event_id=$this->input->post('event_id');
		$organizer_id=$this->input->post('organizer_id');

		if($event_id!='' && $organizer_id!='')
		{
						
			$data = $this->Beacon_model->get_all_beacon($event_id,$organizer_id);
			
			$msg = array(
				'success' => true,
				'data' => $data
				);
			echo json_encode($msg);exit();
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
			echo json_encode($data);
		}
		
	}

	/*** Notification in Beacons ***/

	public function get_notification()
	{
		$event_id=$this->input->post('event_id');
		$UDID=$this->input->post('UDID');
		$major=$this->input->post('major');
		$minor=$this->input->post('minor');
		$user_id=$this->input->post('user_id');
		$nameSpaceId=$this->input->post('nameSpaceId');
		$InstanceId=$this->input->post('InstanceId');
		
		if($event_id!='' && $user_id!='')
		{
			$this->add_user_game_point($user_id,$event_id,5);
			$user = $this->Beacon_model->get_users($event_id,$user_id);

			$beacon['UDID'] = $UDID;
			$beacon['major'] = $major;
			$beacon['minor'] = $minor;
			if(!empty($nameSpaceId))
			{
				$beacon['nameSpaceId'] = $nameSpaceId;
				$beacon['InstanceId'] = $InstanceId;
			}
			$beacon_id = $this->Beacon_model->get_beacon_id($beacon);
			$check_beacon_movement = $this->Beacon_model->check_beacon_movement($user_id,$beacon_id);

		if(!$check_beacon_movement)
		{
			if($UDID!='')
			$where['b.UDID'] = $UDID;

			if($major!='')
			$where['b.major'] = $major;

			if($minor!='')
			$where['b.minor'] = $minor;

			if($nameSpaceId!='')
			$where['b.nameSpaceId'] = $nameSpaceId;

			if($InstanceId!='')
			$where['b.InstanceId'] = $InstanceId;

			$where['b.event_id'] = $event_id;
			$where['b.organizer_id'] = $user['Organisor_id'];
			$data = $this->Beacon_model->get_notification($where);
		}

			
			if(!empty($data))
			{
				
				if($user['gcm_id']!='')
	        	{
		            $extra['message_type'] = 'cms';
		            $extra['message_id'] = $data[0]['modules'];
		            $extra['event'] = $this->Exhibitor_model->getEventName($event_id);
	            
			        if($user['device'] == 'Iphone')
			        {
			        	$obj = new Fcm();
			            $extra['title'] = $data[0]['trigger_name'];
			            $msg = $data[0]['message'];
			            $obj->send(1,$user['gcm_id'],$msg,$extra,$user['device'],$event_id);
			        }
			        else
			        {
			        	$obj = new Gcm($event_id);
			            $msg['title'] = $data[0]['trigger_name'];
			            $msg['message'] = $data[0]['message'];
			            $msg['vibrate'] = 1;
			            $msg['sound'] = 1;
			            $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
			        }	

			        $insert['beacon_id'] = $data[0]['Id'];
			        $insert['user_id'] = $user_id;

			        $add_beacon_movement = $this->Beacon_model->add_beacon_movement($insert);
			    	$msg = array(
						'success' => true,
						'data' => $data
						);
			       	echo json_encode($msg);
			    }
				
            }
            else
            {
            	$data = array(
				'success' => true,
				'message' => "No notification found"
				);
			   echo json_encode($data);
            }
		}		
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
			echo json_encode($data);
		}	
		
	}

	/*** Add points for Users ***/

    public function add_user_game_point($user_id,$event_id,$rank_id)
    {   
        $this->Gamification_model->add_user_point($user_id,$event_id,$rank_id);

        /*$obj = new Gcm();
        $users = $this->Gamification_model->getAllUsersGCM_id_by_event_id($event_id);
        $count = count($users);
        
        if($count > 100)
        {
            $limit = 100;
            for ($i=0;$i<$count;$i++) 
            {
                $page_no        = $i;

                $start          = ($page_no)*$limit;
                $users1         = array_slice($users,$start,$limit);
                foreach ($users1 as $key => $value) 
                {
                    if($value['gcm_id']!='')
                    {
                        $msg =  '';
                        $extra['message_type'] = 'gamification';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        if($value['device'] == "Iphone")
                        {
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                    }
                }
            }
        }
        else
        {
            foreach ($users as $key => $value)
            {
                if($value['gcm_id']!='')
                {
                    $msg = '';
                    $extra['message_type'] = 'gamification';
                    if($value['device'] == "Iphone")
                    {
                        $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                    }
                    else
                    {
                        $msg['title'] = '';
                        $msg['message'] = '';
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                    } 
                }
            }
        }*/
        return true;
    }

}