<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/libraries/Rssparser.php');

class Rss extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('native_single_fcm_v2/rss_model');
	}
	public function update_rss()
	{
		//$insert['email'] = 'Update RSS '.date('Y-m-d H:i:s');
        //$this->db->insert('tmp_user_session',$insert);
		$this->rss_model->update_rss();
	}
	public function get_top_news()
	{	
		$event_id = $this->input->post('event_id');

		if(!empty($event_id))
		{
			$data = $this->rss_model->get_top_news($event_id);
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function get_my_news()
	{
		$event_id = $this->input->post('event_id');
		$keyword  = $this->input->post('keyword');
		$page_no  = $this->input->post('page_no');

		if(!empty($event_id))
		{	
			
			if(!empty($keyword))
				$keyword = explode(',',$keyword);
				$keyword = array_map('trim',$keyword);
			$data = $this->rss_model->get_my_news($page_no,$keyword);
			$limit          = 15;
        	$page_no        = (!empty($page_no))?$page_no:1;
        	$start          = ($page_no-1)*$limit;
        	$total          = count($data);
        	$total_page     = ceil($total/$limit);
        	$data           = array_slice($data,$start,$limit);

			$data = array(
				'success' => true,
				'data'    => $data,
				'total_page' => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);	
	}
	public function get_tags()
	{
		$event_id = $this->input->post('event_id');

		if(!empty($event_id))
		{
			$data = $this->rss_model->get_rss_tags();
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function get_news_details()
	{
		$event_id = $this->input->post('event_id');
		$news_id  = $this->input->post('news_id');
		$user_id  = $this->input->post('user_id');

		if(!empty($event_id) && !empty($news_id))
		{
			$data = $this->rss_model->get_news_details($news_id,$event_id,$user_id);
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function like_news()
	{
		$event_id = $this->input->post('event_id');
		$user_id  = $this->input->post('user_id');
		$news_id  = $this->input->post('news_id');
		$like  = $this->input->post('like');

		if(!empty($event_id) && !empty($user_id) && !empty($news_id))
		{
			$this->rss_model->like_news($news_id,$event_id,$user_id,$like);
			$data = array(
				'success' => true,
			    'message' => ($like) ? 'Liked Successfully' : 'Disliked Successfully' 
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function make_comment()
    {
        $news_id=$this->input->post('news_id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $event_id = $this->input->post('event_id');

        if($event_id!='' && $news_id!='' && $user_id!='' && $comment!='')
        {
            $data = array(
                'module_type' => 'rss',
                'module_primary_id' => $news_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                'event_id' => $event_id,
                );

            $id = $this->rss_model->make_comment($data);  
            
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => (string)$id
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveCommentImages()
    {
        $comment_id    = $this->input->post('comment_id');

        if($comment_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
           
            $images         = $target_path;
            $message_data   = $this->rss_model->getCommentDetails($comment_id);
            
            if($message_data->comment_image!='')
            {
                $arr                           = json_decode($message_data->comment_image);
                $arr[]                         = $new_image_name;
                $update_arr['comment_image']   = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->rss_model->updateCommentImage($comment_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->rss_model->updateCommentImage($comment_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }


    public function update_rss_global()
	{
		//$insert['email'] = 'RSS Global'.date('Y-m-d H:i:s');
        //$this->db->insert('tmp_user_session',$insert);
		$this->rss_model->update_rss_global();
	}
	public function get_top_news_global()
	{	
		$event_id = $this->input->post('event_id');
		$organizer_id = $this->rss_model->getorganizer_id($event_id);
		$orid = $organizer_id[0]->Organisor_id;

		if(!empty($event_id))
		{
			$data = $this->rss_model->get_top_news_global($orid);
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function get_top_news_page()
	{	
		$event_id = $this->input->post('event_id');
		$page_no = $this->input->post('page_no');
		$organizer_id = $this->rss_model->getorganizer_id($event_id);
		$orid = $organizer_id[0]->Organisor_id;

		if(!empty($event_id))
		{
			$data = $this->rss_model->get_top_news_page($orid);
			$limit          = 15;
        	$page_no        = (!empty($page_no))?$page_no:1;
        	$start          = ($page_no-1)*$limit;
        	$total          = count($data);
        	$total_page     = ceil($total/$limit);
        	$data           = array_slice($data,$start,$limit);

        	foreach ($data as $key => $value)
			{
				$data[$key]['title'] = html_entity_decode($value['title']);
			}
			$data = array(
				'success' => true,
				'data'    => $data,
				'total_page' => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function get_my_news_global()
	{
		$event_id = $this->input->post('event_id');
		$keyword  = $this->input->post('keyword');
		$page_no  = $this->input->post('page_no');

		$organizer_id = $this->rss_model->getorganizer_id($event_id);
		$orid = $organizer_id[0]->Organisor_id;

		if(!empty($event_id))
		{	
			
			if(!empty($keyword))
				$keyword = explode(',',$keyword);
				$keyword = array_map('trim',$keyword);
			$data = $this->rss_model->get_my_news_global($page_no,$keyword,$orid);
			$limit          = 15;
        	$page_no        = (!empty($page_no))?$page_no:1;
        	$start          = ($page_no-1)*$limit;
        	$total          = count($data);
        	$total_page     = ceil($total/$limit);
        	$data           = array_slice($data,$start,$limit);

			$data = array(
				'success' => true,
				'data'    => $data,
				'total_page' => $total_page,
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);	
	}
	public function get_tags_global()
	{
		$event_id = $this->input->post('event_id');
		$organizer_id = $this->rss_model->getorganizer_id($event_id);
		$orid = $organizer_id[0]->Organisor_id;

		if(!empty($event_id))
		{
			$data = $this->rss_model->get_rss_tags_global($orid);
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function get_news_details_global()
	{
		$event_id = $this->input->post('event_id');
		$news_id  = $this->input->post('news_id');
		$user_id  = $this->input->post('user_id');

		$organizer_id = $this->rss_model->getorganizer_id($event_id);
		$orid = $organizer_id[0]->Organisor_id;

		if(!empty($event_id) && !empty($news_id))
		{
			$data = $this->rss_model->get_news_details_global($news_id,$event_id,$user_id);
			$data = array(
				'success' => true,
				'data'    => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message'    => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function like_news_global()
	{
		$event_id = $this->input->post('event_id');
		$user_id  = $this->input->post('user_id');
		$news_id  = $this->input->post('news_id');
		$like  = $this->input->post('like');

		if(!empty($event_id) && !empty($user_id) && !empty($news_id))
		{
			$this->rss_model->like_news_global($news_id,$event_id,$user_id,$like);
			$data = array(
				'success' => true,
			    'message' => ($like) ? 'Liked Successfully' : 'Disliked Successfully' 
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
	}
	public function make_comment_global()
    {
        $news_id=$this->input->post('news_id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $event_id = $this->input->post('event_id');

        if($event_id!='' && $news_id!='' && $user_id!='' && $comment!='')
        {
            $data = array(
                'module_type' => 'rss',
                'module_primary_id' => $news_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                'event_id' => $event_id,
                );

            $id = $this->rss_model->make_comment_global($data);  
            
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => (string)$id
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveCommentImages_global()
    {
        $comment_id    = $this->input->post('comment_id');
        if($comment_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
           
            $images         = $target_path;
            $message_data   = $this->rss_model->getCommentDetails_global($comment_id);
            
            if($message_data->comment_image!='')
            {
                $arr                           = json_decode($message_data->comment_image);
                $arr[]                         = $new_image_name;
                $update_arr['comment_image']   = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->rss_model->updateCommentImage_global($comment_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->rss_model->updateCommentImage_global($comment_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}