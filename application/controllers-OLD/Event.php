<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event extends FrontendController {
  
    function __construct() {
         
        if(!in_array("access_setting",explode("/",$_SERVER['REQUEST_URI'])))
        {
          $this->data['pagetitle'] = 'Edit Your App';
          $this->data['smalltitle'] = 'Edit Your App.';
        }
        else
        {
          $this->data['pagetitle'] = 'App Settings';
          $this->data['smalltitle'] = 'Set the privacy of your app.';
        }
        $this->data['breadcrumb'] = 'Create & Edit Your Apps';
        parent::__construct($this->data);
       
          //$this->load->model('category_model');
          $this->load->model('Event_model');
          $this->load->model('Role_management_model');
          $this->load->model('Event_template_model');
          $this->load->model('Event_model');
          $this->load->model('Cms_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Profile_model');
          $this->load->model('User_model');
          $this->load->model('Login_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Message_model');
          $this->load->model('Agenda_model');
          $this->load->model('activity_model');
          $this->load->model('Lead_retrieval_model');
          $this->load->model('Exibitor_survey_model');
          $this->load->library('upload');
          $this->load->library('image_lib');
          $this->load->library('pagination');
          $this->load->library('Session');

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          if($roleid == '4')
            redirect('Forbidden');

          $eventid=$this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->select('Event_id')->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          foreach ($user_events as $key => $value)
          {
            if($value['Event_id'])
            $tmp[] = $value['Event_id'];
          }
          $user_events = $tmp;

          if($this->uri->uri_string != 'Event/add' && $this->uri->uri_string != 'Event/add_event' && $this->uri->uri_string != 'Event/eventhomepage')
          {
      			if(!in_array($eventid,$user_events))
      			{
      				redirect('Forbidden');
      			}
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
    }

    public function index($id)                 
    {       
   
      
        $user = $this->session->userdata('current_user');
        $menudata=$this->Event_model->geteventmenu($id,19);
        
        $menu_toal_data=$this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->data['login_user'] = $user;

        if($this->data['user']->Role_name == 'Client' || $this->data['user']->Role_name == 'Administrator')
        {  
            $event_name = $this->Event_model->get_admin_event($id);
            $this->data['event_name'] = $event_name;

            $event = $this->Event_model->get_event_list();
            $this->data['Event'] = $event;

            $archive_event = $this->Event_model->get_archive_event_list();
            $this->data['Archive_event'] = $archive_event;

            $feature_event = $this->Event_model->get_feature_event_list();
            $this->data['Feature_event'] = $feature_event;

            $this->template->write_view('css', 'event/client/css', $this->data , true);
            $this->template->write_view('header', 'common/header', $this->data , true);
            $this->template->write_view('content', 'event/client/index', $this->data , true);
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
            $this->template->write_view('js', 'event_template/js', $this->data , true);
            $this->template->render();
        }

        if($this->data['user']->Role_name == 'Attendee')
        {  
            $event = $this->Event_model->get_event_list();
            $this->data['Event'] = $event;
            $this->template->write_view('css', 'event/attendee/css', $this->data , true);
            $this->template->write_view('content', 'event/attendee/index', $this->data , true);
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
            $this->template->write_view('js', 'event/attendee/js', $this->data , true);
            $this->template->render();
        }
    }

    public function add()                 
    {          

        $user = $this->session->userdata('current_user');
        $admin_scret_key = $this->Event_model->get_admin_stripe_setting();
        $this->data['admin_scret_key']=$admin_scret_key;
        $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
        $ismulitieventaccount=$this->Event_model->check_muliti_event_account_by_org_email($user[0]->Email);
        $multievent=$this->Event_model->get_multievent_by_org_id($user[0]->Id);
        $this->data['multievent']=$multievent;
        $this->data['ismulitieventaccount']=$ismulitieventaccount;
        $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
        $logged_in_user_id = $user[0]->Id;
        if($this->data['user']->Role_name == 'Client')
        {                    
            if($this->input->post())
            { 
              if($this->input->post('banner_image_tetbox')=="") 
                {
              if (!empty($_FILES['images']['name']))
                  {
                      $tempname = explode('.', $_FILES['images']['name']);
                      $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                      $images_file = $tempname_imagename . "." . $tempname[1];

                      $this->upload->initialize(array(
                              "file_name" => $images_file,
                              "upload_path" => "./assets/user_files",
                              "allowed_types" => 'gif|jpg|png|jpeg',
                              "max_size" => '10000',
                              "max_width" => '2000',
                              "max_height" => '2000'
                      ));

                      if (!$this->upload->do_multi_upload("images"))
                      {
                          echo "error###".$this->upload->display_errors();die;
                      }
                      if($usersub[0]['stripe_show']=='0'){
                      copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
                      }
                 }
               }
               else
               {
                $images_file=$this->input->post('banner_image_tetbox');
                  if($usersub[0]['stripe_show']=='0'){
                      copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
                    }
               }
                 if($usersub[0]['stripe_show']=='1'){
                  $data['event_array']['stripe_show']='1';
                   if($this->input->post('image_url')!="")
                   {
                      $imgurl=$this->input->post('image_url');
                      $imgname=strtotime(date("Y-m-d H:i:s")).".jpg";
                      copy($imgurl,"./assets/user_files/".$imgname);
                      if($images_file=="")
                      {
                        $images_file=$imgname;
                      }
                      else
                      {
                        $images=array($images_file,$imgname);
                        $data['event_array']['Images'] =$images;
                        $images_file="";
                      }
                   }
                   if($this->input->post('video_url')!="")
                   {
                      $data['event_array']['event_video_link']=$this->input->post('video_url');
                   }
                   if($this->input->post('event_currncy')!="")
                   {
                      $data['event_array']['currency']=$this->input->post('event_currncy');
                   }
                   if($this->input->post('fun_target')!="")
                   {
                      $data['event_array']['Fundraising_target']=$this->input->post('fun_target');
                   }
                   if($this->input->post('fun_target_show')!="")
                   {
                      $data['event_array']['target_raisedsofar_display']=$this->input->post('fun_target_show');
                   }
                   if($this->input->post('bids_donations_show')!="")
                   {
                      $data['event_array']['bids_donations_display']=$this->input->post('bids_donations_show');
                   }
                   if($this->input->post('select_slide_image')!="")
                   {
                      $data['event_array']['slide_image']=$this->input->post('select_slide_image');
                   }
                 }
                  if($this->input->post('logo_image_tetbox')=="") 
                {
                 if (!empty($_FILES['logo_images']['name']))
                 {
                      $tempname1 = explode('.', $_FILES['logo_images']['name']);
                      $tempname_imagename1 = $tempname1[0] . strtotime(date("Y-m-d H:i:s"));
                      $logo_images = $tempname_imagename1 . "." . $tempname1[1];

                      $this->upload->initialize(array(
                              "file_name" => $logo_images,
                              "upload_path" => "./assets/user_files",
                              "allowed_types" => 'gif|jpg|png|jpeg',
                              "max_size" => '10000',
                              "max_width" => '2000',
                              "max_height" => '2000'
                      ));

                      if (!$this->upload->do_multi_upload("logo_images"))
                      {
                          echo "error###".$this->upload->display_errors();die;
                      }
                      copy(base_url()."assets/user_files/".$logo_images,"./fundraising/assets/user_files/".$logo_images);
                 }
               }
               else
                {
                  $logo_images=$this->input->post('logo_image_tetbox');
                   copy(base_url()."assets/user_files/".$logo_images,"./fundraising/assets/user_files/".$logo_images);
                }
                 if (!empty($_FILES['background_img']['name']))
                  {
                      $background_imgname = explode('.', $_FILES['background_img']['name']);
                      $background_imgtempname = str_replace(" ","_",$background_imgname);
                      $tempname_background_imgname = $background_imgtempname[0] . strtotime(date("Y-m-d H:i:s"));
                      $background_imgname_file = $tempname_background_imgname . "." . $background_imgtempname[1];

                      $this->upload->initialize(array(
                              "file_name" => $background_imgname_file,
                              "upload_path" => "./assets/user_files",
                              "allowed_types" => 'gif|jpg|png|jpeg',
                              "max_size" => '100000',
                              "max_width" => '3000',
                              "max_height" => '3000'
                      ));

                      if (!$this->upload->do_multi_upload("background_img"))
                      {
                          echo "error###".$this->upload->display_errors();die;
                      }
                   }

                $data['event_array']['menu_background_color']=$this->input->post('menu_background_color');
                $data['event_array']['menu_text_color']=$this->input->post('menu_text_color');
                $data['event_array']['menu_hover_background_color']=$this->input->post('menu_hover_background_color');
                $data['event_array']['Event_name'] = $this->input->post('Event_name');
                $data['event_array']['Subdomain'] = str_replace(" ",'_',$this->input->post('Subdomain'));
                $data['event_array']['Description'] = $this->input->post('Description');
                $data['event_array']['Organisor_id'] = $logged_in_user_id;
                $data['event_array']['Background_color'] = $this->input->post('Background_color');
                $data['event_array']['Top_background_color'] = $this->input->post('Top_background_color');
                $data['event_array']['Top_text_color'] = $this->input->post('Top_text_color');
                $data['event_array']['Footer_background_color'] = $this->input->post('Footer_background_color');
                $data['event_array']['Start_date'] = date("Y-m-d");
                $data['event_array']['End_date'] = date("Y-m-d", strtotime($sdate."+1 years"));
                $data['event_array']['Status'] = '1';
                if(count($iseventfreetrial) > 0)
                {
                  $data['event_array']['Event_type'] = '1';
                  $data['event_array']['secure_key'] = substr($this->input->post('Event_name'),0,3).substr(uniqid(),0,3);
                }
                else
                {
                  $data['event_array']['Event_type'] = '3';
                }
                if($images_file!=null)
                {
                  if($usersub[0]['stripe_show']=='0'){
                    $data['event_array']['fun_image']="assets/images/slideshow/".$images_file;
                  }
                  $images=array($images_file);
                  $data['event_array']['Images'] =$images;
                }
                if($logo_images!=null)
                {
                  $data['event_array']['fun_logo_images']=$logo_images;
                  $data['event_array']['Logo_images'] =$logo_images;
                }
                if($background_imgname_file!=null)
                {
                  $back=array($background_imgname_file);
                  $data['event_array']['Background_img'] =json_encode($back);
                }
                $data['event_array']['Event_time'] = $this->input->post('Event_time');
                $data['event_array']['Event_time_option'] = $this->input->post('Event_time_option');
                $data['event_array']['Contact_us'] = $this->input->post('Contact_us');
                $event_id=$this->Event_model->add_admin_event($data);
                $role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
             
                foreach ($role_menu as $key=>$value)
                {    
                     $role=$this->Role_management_model->insertdefultrole($event_id,3,$value->id);
                     $role=$this->Role_management_model->insertdefultrole($event_id,95,$value->id);
                     $role=$this->Role_management_model->insertdefultrole($event_id,5,$value->id);
                     //$role=$this->Role_management_model->insertdefultrole($event_id,4,$value->id);
                    // $role=$this->Role_management_model->insertdefultrole($event_id,7,$value->id);
                }
                $user = $this->session->userdata('current_user');

                $role=$this->Event_model->eventaddrelation($event_id,$user[0]->Id,$user[0]->Organisor_id,$user[0]->Role_id);
              $eventmodule=$this->Event_model->geteventmodulues($event_id);
              $module=json_decode($eventmodule[0]['module_list']);
              if(in_array('20',$module))
              {
                echo "success###".base_url().'fundraising/'.$this->input->post('Subdomain').'/panel';die;
              }
              else
              {
                echo "success###".base_url().'event/edit/'.$event_id;die; 
              }
            } 
            $this->data['organisor_events']=$this->Event_model->check_first_event_by_organisor($user[0]->Id);
            
            $this->template->write_view('css', 'event/client/add_css', $this->data , true);
            $this->template->write_view('content', 'event/client/addeventform', $this->data , true);
            /*if($usersub[0]['stripe_show']=='0'){
              $this->template->write_view('content', 'event/client/addform', $this->data , true);
            }
            else
            {
              $this->template->write_view('content', 'event/client/addform_sub', $this->data , true);
            }*/
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
            $this->template->write_view('js', 'event/client/add_js', $this->data , true);
            $this->template->render();
        }
    }
    public function add_event()
    {
      $ip = $_SERVER['REMOTE_ADDR'];
      $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
      if($query && $query['status'] == 'success') 
      {
        if($query['countryCode'] == 'UK')
        {
          $currency="GBP";
        }
        else
        {
          $currency="USD";
        }
      }
      else
      {
        $currency="USD";
      }
      $user = $this->session->userdata('current_user');
      $admin_scret_key = $this->Event_model->get_admin_stripe_setting();
      $org_scret_key=$this->Event_model->get_organisor_scret_key_by_organisor_id($user[0]->Id);
      $user_key=$org_scret_key[0]['stripe_user_id'];
      $payment;
      if(!empty($this->input->post('Event_name'))){
        if(!empty($this->input->post('stripeToken'))){
            if(!class_exists('\Stripe\Stripe')){
              require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
            }
            \Stripe\Stripe::setApiKey($admin_scret_key[0]['secret_key']);
            //\Stripe\Stripe::setApiKey('sk_test_B9lPcKzb5oIMZ3SiDOlDEu8i');
            $payment =  \Stripe\Charge::create(
              array(
              "amount" => 499 * 100,
              "currency" => $currency,
              "source" => $this->input->post('stripeToken'),
              "description" => "All In The Loop"
              )
            );
          }
        $data['event_array']['launch_your_app']=$this->input->post('launch_your_app_type');
        if($this->input->post('launch_your_app_type')!='0')
        {
          $data['event_array']['islaunchapp']='1';
        }
        $acc_name=$this->session->userdata('acc_name');
        $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);  
        $data['event_array']['Event_name'] = $this->input->post('Event_name');
        $data['event_array']['Subdomain'] = str_replace(" ",'_', $acc_name).preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(" ",'_',$this->input->post('Event_name')));
        $data['event_array']['Organisor_id'] = $user[0]->Id;
        $data['event_array']['Start_date'] = date("Y-m-d");
        $data['event_array']['End_date'] = date("Y-m-d", strtotime($sdate."+1 years"));
        $data['event_array']['Status'] = '1';
        if(count($iseventfreetrial) > 0)
        {
          $data['event_array']['Event_type'] = '1';
        }
        else
        {
          $data['event_array']['Event_type'] = '3';
        }
        $data['event_array']['secure_key'] = substr(preg_replace('/[^A-Za-z0-9\-]/', '',$this->input->post('Event_name')),0,3).substr(uniqid(),0,3);
        $data['event_array']['Background_color'] = "#3B99D8";
        $data['event_array']['Top_background_color'] ='#3B99D8';
        $data['event_array']['Top_text_color'] ='#FFFFFF';
        $data['event_array']['menu_background_color']='#2F80B6';
        $data['event_array']['menu_text_color']='#FFFFFF';
        $event_id=$this->Event_model->add_admin_event($data);
        $icon_set=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
        foreach ($icon_set as $key => $value) 
        {
          $menudata=$this->Event_model->get_menu_data_by_menu_id($value);
          $event_menu['menu_id']=$value;  
          $event_menu['event_id']=$event_id;
          $event_menu['title']=$menudata[0]['menuname'];
          $event_menu['img']="";
          $event_menu['is_feture_product']='1';
          $event_menu['img_view']='0';
          $event_menu['Background_color']="#3B99D8";
          $this->Event_model->add_left_hand_menu_title($event_menu);
        }
        $role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
        foreach ($role_menu as $key=>$value)
        {    
          $role=$this->Role_management_model->insertdefultrole($event_id,3,$value->id);
          $role=$this->Role_management_model->insertdefultrole($event_id,95,$value->id);
          $role=$this->Role_management_model->insertdefultrole($event_id,5,$value->id);
        }
        $user = $this->session->userdata('current_user');
        $role=$this->Event_model->eventaddrelation($event_id,$user[0]->Id,$user[0]->Organisor_id,$user[0]->Role_id);
        redirect(base_url().'Event/eventhomepage/'.$event_id);
      }
      else
      {
        redirect(base_url().'Event/add');
      }
    }
    public function delete_banner($id)
    {

        $this->Event_model->delete_banner($id);
        redirect("Event/edit/".$id);
    }
    public function delete_backgroundimg($eid)
    {
      $this->Event_model->delete_backgroundimg($eid);
      redirect("Event/edit/".$eid);
    }
    public function delete_logo($id)
    {
        
        $this->Event_model->delete_logo($id);
        redirect("Event/edit/".$id);
    }
    public function edit_checkbox($id)
    {
        $login_user = $this->session->userdata('current_user');
        
        if(empty($id))
        {
            echo "error###Something went wrong";
        }
        else
        {
            $data['event_array']['Id'] = $id;
            
            $checkedval=  explode(',',$this->input->post('checkbox_values'));
            sort($checkedval);
            $data['event_array']['checkbox_values'] = implode(',', $checkedval);
            $this->Event_model->update_admin_event($data);
            
            $final_str='';            
            $menu_list = $this->Event_model->geteventmenu($id);       
            $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
            
            $event = $this->Event_model->get_admin_event($id);
            if(in_array($this->input->post('active_home_screen_menu'), $iconset))
            {
              $menudata=$this->Event_model->get_menu_data_by_menu_id($this->input->post('active_home_screen_menu'));
              $event_menu['menu_id']=$this->input->post('active_home_screen_menu');  
              $event_menu['event_id']=$id;
              $event_menu['title']=$menudata[0]['menuname'];
              $event_menu['img']="";
              $event_menu['is_feture_product']=$this->input->post('active_home_screen_tab'); 
              $event_menu['img_view']='0';
              $event_menu['Background_color']=$event[0]['Background_color'];
              $this->Event_model->update_home_screen_tab_modules($event_menu);
            }
            $str = $event[0]['checkbox_values'];
            $str_array = explode(',', $str);
            $data = $str;
            
            foreach ($menu_list as $key => $val)
            {                
                if(in_array($val->id, $str_array))
                {
                    if($val->id == 20)
                    {
                        $final_str .= empty($final_str)?'':',';
                        $final_str .= $val->id.'#'.base_url().'fundraising/'.$event[0]['Subdomain'].'/panel';
                    }
                    else
                    {
                        $final_str .= empty($final_str)?'':',';
                        $final_str .= $val->id.'#'.base_url().$val->menuurl.$id;
                    }
                }
            }
           
            echo "success###".$data."###".$final_str;
            die;
            echo "success###".$data."###".$final_str;
        }
        exit;
    }
    

     public function access_setting($id)
    {
      $user = $this->session->userdata('current_user');
      if($user[0]->Role_id != '3')
      {
        redirect('forbidden');
      }
      if($this->input->post()){
          $data['event_array']['Id'] = $id;
          $data['event_array']['Event_type'] = $this->input->post('Event_type');
          $data['event_array']['Start_date'] = $this->input->post('Start_date');
          $data['event_array']['End_date'] = $this->input->post('End_date');
          $data['event_array']['Status'] = $this->input->post('Status');
          $data['event_array']['Beacon_feature'] = $this->input->post('Beacon_feature');
          $data['event_array']['Event_time_zone'] = $this->input->post('Event_time_zone');
          $data['event_array']['authorized_email'] = $this->input->post('authorized_email');
          $data['event_array']['date_format'] = $this->input->post('date_format');
          $data['event_array']['Event_show_time_zone']=$this->input->post('Event_show_time_zone');
          $data['event_array']['no_of_login_attempts'] = $this->input->post('no_of_login_attempts');
          $data['event_array']['hide_user_identity'] = $this->input->post('hide_user_identity') ? '1':'0';
          $data['event_array']['show_reminder_button'] = $this->input->post('show_reminder_button') ? '1':'0';
          $data['event_array']['enable_block_button'] = $this->input->post('enable_block_button') ? '1':'0';
          $data['event_array']['enable_hide_my_identity'] = $this->input->post('enable_hide_my_identity') ? '1':'0';
          
          if($this->input->post('hub_btn')=='1')
            $data['event_array']['hub_menu_show'] = '1';
          else
            $data['event_array']['hub_menu_show'] = '0';

          if($this->input->post('social_media')=='1')
            $data['event_array']['social_media']='1';
          else
            $data['event_array']['social_media']='0'; 
          
          $data['event_array']['hub_menu_title'] = $this->input->post('hub_btn_title');
          $format_time=$this->input->post('format_time');
          if(!empty($this->input->post('secure_key')))
          {
            $key = $this->Event_model->checkkey($this->input->post('secure_key'),$id);
            if(count($key) <= 0)
            {
              $data['event_array']['secure_key'] = $this->input->post('secure_key');
            }
          }
          else
          {
            $data['event_array']['secure_key']="";
          }

          if($this->input->post('show_login_screen'))
            $data['event_array']['show_login_screen'] = '1';
          else
            $data['event_array']['show_login_screen'] = '0';

          $this->Event_model->update_admin_event($data);
          
          if($_POST['facebook_login']==1)
          {
            $facebook_login="1";
          }
          else
          {
            $facebook_login="0";
          }
          if($_POST['linkedin_login_enabled']==1)
          {
            $linkedin_login_enabled="1";
          }
          else
          {
            $linkedin_login_enabled="0";
          }
          $this->Event_model->add_facebook_login($format_time,$facebook_login,$linkedin_login_enabled,$id);
          $this->session->set_flashdata('success', 'Access Settings Saved Successfully.');
          redirect("Event/access_setting/$id");
      }
     $arrAcc = $this->Event_template_model->getAccname($id); 
     $Subdomain=$this->Event_template_model->get_subdomain($id);
     $this->data['timezone']=$this->Event_model->get_all_time_zone();
      $this->data['Subdomain'] = $Subdomain;
     $this->data['notificationsetting']=$this->Event_model->getnotificationsetting($id); 
     $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
     $this->data['event_templates'] = $event_templates;
     $this->data['arrAct'] = $arrAcc;
     $default_label=$this->Event_template_model->get_all_default_label();
     $this->data['default_label']=$default_label;
     $countrylist = $this->Profile_model->countrylist();
     $this->data['countrylist'] = $countrylist;
     $lang_menu_list = $this->Event_template_model->geteventmenu_list_for_language($event_templates[0]['Id']);
      $this->data['lang_menu_list'] = $lang_menu_list;
     $session_category=$this->Event_template_model->get_all_session_category_for_multilanguage($event_templates[0]['Id']);
     $this->data['session_category']=$session_category;
     $agenda_list=$this->Event_template_model->get_all_session_in_event_for_multilanguage($event_templates[0]['Id'],$session_category[0]['Id']);
     $this->data['agenda_list']=$agenda_list;
     $language_list=$this->Event_template_model->get_language_list_by_event($event_templates[0]['Id']);
     $this->data['language_list']=$language_list;
     $language_all_list=$this->Event_template_model->get_language_all_list_by_event($event_templates[0]['Id']);
     $this->data['language_all_list']=$language_all_list;
     $language_label=$this->Event_template_model->get_language_label_values_by_event($event_templates[0]['Id']);
     $this->data['language_label']=$language_label;
     $language_menu=$this->Event_template_model->get_language_menu_values_by_event($event_templates[0]['Id']);
     $this->data['language_menu']=$language_menu;

     $language_content=$this->Event_template_model->get_language_content_by_event($event_templates[0]['Id']);
     $this->data['language_content']=$language_content;
     $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
     $this->data['menu'] = $menu;
     $this->data['forcelogin_menu'] = $this->Event_model->geteventmenu_rolemanagement($id);
     $this->data['forceloginmenu_ids']=$this->Event_model->get_all_menu_id_by_froce_login($id);
     $user = $this->session->userdata('current_user');
     $this->data['iseventfreetrial']=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
     $roleid = $user[0]->Role_id;
     $orid = $user[0]->Id;
     $hubdesing=$this->Event_model->get_hub_event($orid);
     if(count($hubdesing)>0)
     {
        $this->data['is_hub_created']=1;
     }
     else
     {  
        $this->data['is_hub_created']=0;
     }
     $this->data['hub_active']=$this->Event_model->get_hub_active_by_organizer($orid);
     $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
     $this->data['menu_list'] = $menu_list;
     $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
     $this->data['cms_feture_menu'] = $cms_feture_menu;
     $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
     $this->data['cms_menu'] = $cmsmenu;
     $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
     $this->data['users_role']=$user_role;
     
     $o_settings = $this->Event_model->get_onboarding_settings($id);
     $this->data['o_settings'] = $o_settings;

     $this->data['login_screen'] =  $this->Event_model->get_event_settings($id);
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $this->data['flag'] = 1;
      $this->session->set_userdata($data);
      $this->data['smalltitle'] = 'Access Settings';
      $this->data['breadcrumb'] = 'Set the privacy of your app';
      $this->template->write_view('css', 'event/client/add_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data , true);
      $this->template->write_view('content','event/client/access_setting',$this->data,true);
      $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
      $this->template->write_view('js', 'event/client/add_js', $this->data , true);
      $this->template->render();
    }




     public function save_login_screen($id,$delete_image=false)
    {   
        if (!file_exists('./assets/login_screen/'.$id)) {
            mkdir('./assets/login_screen/'.$id, 0777, true);
        }
        if(!empty($_FILES['banner_Images']['name']))
        {
            $imgname = explode('.', $_FILES['banner_Images']['name']);
            $tempname = str_replace(" ","_",$imgname);
            $tempname_imagename = $id.'-login_screen-'.date('Y-m-d-H:i:s');
            $images_file = $tempname_imagename . "." . $tempname[1];

            $this->upload->initialize(array(
                  "file_name" => $images_file,
                  "upload_path" => "./assets/login_screen/".$id,
                  "allowed_types" => 'gif|jpg|png|jpeg',
                  "max_size" => '100000',
                  "max_width" => '3000',
                  "max_height" => '3000'
            ));
            $this->upload->do_upload("banner_Images");
            $data['login_screen_image'] = $images_file;
        }
        if($delete_image)
        {
            $data['login_screen_image'] = NULL;
            $login_screen_image = htmlentities($this->Event_model->get_event_settings($id)['login_screen_image']);
            unlink("./assets/login_screen/".$id."/".$login_screen_image);
        }
        if(!empty($this->input->post('screen_content')))
            $data['login_screen_text'] = $this->input->post('screen_content');
        $this->Event_model->save_event_settings($id,$data);
        $this->session->set_flashdata('success', 'Login Screen Settings Saved Successfully.');
        redirect("Event/access_setting/".$id);
    }
    public function forceloginsave($id)
    {
        $force_login_menu_ids=$this->input->post('Menu_id');
        $this->Event_model->edit_force_login_status($force_login_menu_ids,$id);
        $this->session->set_flashdata('success', 'Force Login Modules Saved Successfully.');
        redirect("Event/access_setting/".$id);
    }
    public function editsubmit($id)
    {
      $user = $this->session->userdata('current_user');
      $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
      if($this->input->post())
      {
         if($this->input->post('banner_image_tetbox')=="") 
        {
          if (!empty($_FILES['images']['name']))
          {

              $imgname = explode('.', $_FILES['images']['name']);
              $tempname = str_replace(" ","_",$imgname);
              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
              $images_file = $tempname_imagename . "." . $tempname[1];

              $this->upload->initialize(array(
                      "file_name" => $images_file,
                      "upload_path" => "./assets/user_files",
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000',
                      "max_width" => '3000',
                      "max_height" => '3000'
              ));

              if (!$this->upload->do_multi_upload("images"))
              {
                  $response['msg']="error###".$this->upload->display_errors();
                  echo json_encode($response);die;
              }
              $response['image']=base_url()."assets/user_files/".$images_file;
              if($usersub[0]['stripe_show']=='0'){
              copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
              }
          }
          }
          else
          {
            $images_file=$this->input->post('banner_image_tetbox');
            if($usersub[0]['stripe_show']=='0'){
              copy(base_url()."assets/user_files/".$images_file,"./fundraising/assets/images/slideshow/".$images_file);
            }
          }
          if($usersub[0]['stripe_show']=='1'){
            $data['event_array']['stripe_show']='1';
             if($this->input->post('image_url')!="")
             {
                $imgurl=$this->input->post('image_url');
                $imgname=strtotime(date("Y-m-d H:i:s")).".jpg";
                copy($imgurl,"./assets/user_files/".$imgname);
                if($images_file=="")
                {
                  $images_file=$imgname;
                }
                else
                {
                  $images=array($images_file,$imgname);
                  $data['event_array']['Images'] =$images;
                  $images_file="";
                }
             }
             if($this->input->post('video_url')!="")
             {
                $data['event_array']['event_video_link']=$this->input->post('video_url');
             }
             if($this->input->post('event_currncy')!="")
             {
                $data['event_array']['currency']=$this->input->post('event_currncy');
             }
             if($this->input->post('fun_target')!="")
             {
                $data['event_array']['Fundraising_target']=$this->input->post('fun_target');
             }
             if($this->input->post('fun_target_show')!="")
             {
                $data['event_array']['target_raisedsofar_display']=$this->input->post('fun_target_show');
             }
             if($this->input->post('bids_donations_show')!="")
             {
                $data['event_array']['bids_donations_display']=$this->input->post('bids_donations_show');
             }
             if($this->input->post('select_slide_image')!="")
             {
                $data['event_array']['slide_image']=$this->input->post('select_slide_image');
             }
           }
           if($this->input->post('logo_image_tetbox')=="") 
         {
         if (!empty($_FILES['logo_images']['name']))
         {
          
              $imgname = explode('.', $_FILES['logo_images']['name']);
              $tempname = str_replace(" ","_",$imgname);
              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
              $filenames = $tempname_imagename . "." . $tempname[1];

              $this->upload->initialize(array(
                      "file_name" => $filenames,
                      "upload_path" => "./assets/user_files",
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000',
                      "max_width" => '3000',
                      "max_height" => '3000'
              ));

              if (!$this->upload->do_multi_upload("logo_images"))
              {
                $response['msg']="error###".$this->upload->display_errors();
                echo json_encode($response);die;
              }
              $response['logo_image']=base_url()."assets/user_files/".$filenames;
              copy(base_url()."assets/user_files/".$filenames,"./fundraising/assets/user_files/".$filenames);
          }
          }
          else
          {
            $filenames=$this->input->post('logo_image_tetbox');
            copy(base_url()."assets/user_files/".$filenames,"./fundraising/assets/user_files/".$filenames);
          }

          if (!empty($_FILES['background_img']['name']))
          {

              $background_imgname = explode('.', $_FILES['background_img']['name']);
              $background_imgtempname = str_replace(" ","_",$background_imgname);
              $tempname_background_imgname = $background_imgtempname[0] . strtotime(date("Y-m-d H:i:s"));
              $background_imgname_file = $tempname_background_imgname . "." . $background_imgtempname[1];

              $this->upload->initialize(array(
                      "file_name" => $background_imgname_file,
                      "upload_path" => "./assets/user_files",
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000',
                      "max_width" => '3000',
                      "max_height" => '3000'
              ));

              if (!$this->upload->do_multi_upload("background_img"))
              {
                $response['msg']="error###".$this->upload->display_errors();
                echo json_encode($response);die;
              }
              $response['back_image']=base_url()."assets/user_files/".$background_imgname_file;
          }
          $data['event_array']['menu_background_color']=$this->input->post('menu_background_color');
          $data['event_array']['menu_text_color']=$this->input->post('menu_text_color');
          $data['event_array']['menu_hover_background_color']=$this->input->post('menu_hover_background_color');
          $data['event_array']['Id'] = $id;                    
          $data['event_array']['Event_name'] = $this->input->post('Event_name');
          $data['event_array']['Description'] = $this->input->post('Description');
          $data['event_array']['Top_background_color'] = $this->input->post('Top_background_color');
          $data['event_array']['Top_text_color'] = $this->input->post('Top_text_color');
          $data['event_array']['Footer_background_color'] = $this->input->post('Footer_background_color');
          

          if($images_file != "")
          {
            if($usersub[0]['stripe_show']=='0'){
              $data['event_array']['fun_image']="assets/images/slideshow/".$images_file;
            }
            $images=array($images_file);
            $data['event_array']['Images'] =$images;
          }
             
          if($filenames != "")
          {
            $data['event_array']['fun_logo_images']=$filenames;
            $data['event_array']['Logo_images'] = $filenames;
          }

          if($background_imgname_file != "")
          {
              $data['event_array']['Background_img'] = $background_imgname_file;
          }
          $this->Event_model->update_admin_event($data);
          $response['msg']="success###App edit successfully";
          echo json_encode($response);die;
      }
    }
    public function checkkey()
    {            
        if($this->input->post())
        {
            $key = $this->Event_model->checkkey($this->input->post('key'));
            if(count($key)>0)
            {
                echo "error###Secure Key already exist. Please choose another key.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }
    public function edit_backup($id=null)
    { 
         $arrAcc = $this->Event_template_model->getAccname($id); 
         $Subdomain=$this->Event_template_model->get_subdomain($id);
          
         $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
         $this->data['event_templates'] = $event_templates;
         $this->data['arrAct'] = $arrAcc;

         $this->data['fun_setting']=$this->Event_model->get_all_fun_setting($event_templates[0]['Id']);
         $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
         $this->data['menu'] = $menu;
         $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
         $this->data['notify_msg'] = $notifiy_msg;

         $user = $this->session->userdata('current_user');
         $roleid = $user[0]->Role_id;
        
         $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
         $this->data['menu_list'] = $menu_list;
         
         $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
         $this->data['cms_feture_menu'] = $cms_feture_menu;

         $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
         $this->data['cms_menu'] = $cmsmenu;

         $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
         $this->data['speakers'] = $speakers;

         $notes_list = $this->Event_template_model->get_notes($Subdomain);
         $this->data['notes_list'] = $notes_list;

         $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
         $this->data['agendas'] = $agendas;

         $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
         $this->data['attendees'] = $attendees;

         $map = $this->Event_template_model->get_map($Subdomain);
         $this->data['map'] = $map;

         $this->data['Subdomain'] = $Subdomain;
         $this->data['Email'] = $Email;
         $this->data['speaker_flag']=$flag;

         $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
         $this->data['users_role']=$user_role;
 
            if($this->data['page_edit_title'] = 'edit')
            {
                if($id == NULL || $id == '')
                {
                    redirect('Event');
                }
                
                $user = $this->session->userdata('current_user');
                $user[0]->Event_id=$id;

                if($this->input->post())
                {
                    if (!empty($_FILES['images']['name']))
                    {

                        $imgname = explode('.', $_FILES['images']['name']);
                        $tempname = str_replace(" ","_",$imgname);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $images_file = $tempname_imagename . "." . $tempname[1];

                        $this->upload->initialize(array(
                                "file_name" => $images_file,
                                "upload_path" => "./assets/user_files",
                                "allowed_types" => 'gif|jpg|png|jpeg',
                                "max_size" => '100000',
                                "max_width" => '3000',
                                "max_height" => '3000'
                        ));

                        if (!$this->upload->do_multi_upload("images"))
                        {
                             $error = array('error' => $this->upload->display_errors());
                             $this->session->set_flashdata('error',$error['error']);
                        }
                    }

                   if (!empty($_FILES['logo_images']['name'][0]))
                   {
                    
                        $imgname = explode('.', $_FILES['logo_images']['name']);
                        $tempname = str_replace(" ","_",$imgname);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $filenames = $tempname_imagename . "." . $tempname[1];

                        $this->upload->initialize(array(
                                "file_name" => $filenames,
                                "upload_path" => "./assets/user_files",
                                "allowed_types" => 'gif|jpg|png|jpeg',
                                "max_size" => '100000',
                                "max_width" => '3000',
                                "max_height" => '3000'
                        ));

                        if (!$this->upload->do_multi_upload("logo_images"))
                        {
                             $error = array('error' => $this->upload->display_errors());
                             $this->session->set_flashdata('error', $error['error']);
                             redirect("Event/edit/".$id);
                        }
                    }

                    if (!empty($_FILES['background_img']['name']))
                    {

                        $background_imgname = explode('.', $_FILES['background_img']['name']);
                        $background_imgtempname = str_replace(" ","_",$background_imgname);
                        $tempname_background_imgname = $background_imgtempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $background_imgname_file = $tempname_background_imgname . "." . $background_imgtempname[1];

                        $this->upload->initialize(array(
                                "file_name" => $background_imgname_file,
                                "upload_path" => "./assets/user_files",
                                "allowed_types" => 'gif|jpg|png|jpeg',
                                "max_size" => '100000',
                                "max_width" => '3000',
                                "max_height" => '3000'
                        ));

                        if (!$this->upload->do_multi_upload("background_img"))
                        {
                             $error = array('error' => $this->upload->display_errors());
                             $this->session->set_flashdata('error',$error['error']);
                        }
                    }

                    $data['event_array']['Id'] = $id;                    
                    $data['event_array']['Event_name'] = $this->input->post('Event_name');
                    $data['event_array']['Description'] = $this->input->post('Description');
                    $data['event_array']['Background_color'] = $this->input->post('Background_color');
                    $data['event_array']['Top_background_color'] = $this->input->post('Top_background_color');
                    $data['event_array']['Top_text_color'] = $this->input->post('Top_text_color');
                    $data['event_array']['Icon_text_color'] = $this->input->post('Icon_text_color');
                    $data['event_array']['Footer_background_color'] = $this->input->post('Footer_background_color');
                    $data['event_array']['Start_date'] = $this->input->post('Start_date');
                    //$data['event_array']['Start_time'] = $this->input->post('Start_time');
                    $data['event_array']['End_date'] = $this->input->post('End_date');
                    $data['event_array']['Contact_us'] = $this->input->post('Contact_us');
                    $data['event_array']['Status'] = $this->input->post('Status');
                    $data['event_array']['Event_type'] = $this->input->post('Event_type');
                    $data['event_array']['Event_time'] = $this->input->post('Event_time');
                    $data['event_array']['Event_time_option'] = $this->input->post('Event_time_option');
                    $data['event_array']['checkbox_values'] = $this->input->post('checkbox_values');
                    $data['event_array']['img_view'] = $this->input->post('img_view');
                    

                    if($images_file != "")
                    {
                        $data['event_array']['Images'] = $images_file;
                    }
                       
                    if($filenames != "")
                    {
                        $data['event_array']['Logo_images'] = $filenames;
                    }

                    if($background_imgname_file != "")
                    {
                        $data['event_array']['Background_img'] = $background_imgname_file;
                    }

                    $this->Event_model->update_admin_event($data);
                    $this->session->set_flashdata('event_data', 'Updated');
                    redirect("Event/edit/$id");
                }
                $user = $this->session->userdata('current_user');
                $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
                $event = $this->Event_model->get_admin_event($id);
                $this->data['event'] = $event[0];
                $this->data['flag'] = 1;

                $this->session->set_userdata($data);
  
                $this->template->write_view('css', 'event/client/add_css', $this->data , true);
                $this->template->write_view('header', 'common/header', $this->data , true);
                if($usersub[0]['stripe_show']=='0'){
                $this->template->write_view('content', 'event/client/editform', $this->data , true);
                }else{
                  $this->template->write_view('content', 'event/client/editform_sub', $this->data , true);
                }
                $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
                $this->template->write_view('js', 'event/client/add_js', $this->data , true);
                $this->template->render();
            }        
       // }
    }
  
    public function delete()
    {
        $id = $this->input->post('selected'); 
        $client = $this->Event_model->delete_event($id);
        redirect('Event');
    }
    public function slideshowupload()
    {
      for ($i=0; $i <count($_FILES['txtfiles']['name']); $i++) 
      {
        $_FILES['txtfiles']['name'][$i]=preg_replace("/[^a-zA-Z0-9.]/","", $_FILES['txtfiles']['name'][$i]);
      }
      $config["upload_path"]    = "./fundraising/assets/images/slideshow/";
      $config["overwrite"]    = TRUE;
      $config["encrypt_name"]   = TRUE;
      $config["remove_spaces"]  = TRUE;
      $config["allowed_types"]  = "gif|jpg|png|jpeg";
      $config["max_size"]     = 20000;
      $config["xss_clean"]    = FALSE;
      $this->load->library('upload');
      $this->upload->initialize($config);
      $upload_path = "./fundraising/assets/images/slideshow/";
      if ( ! file_exists($upload_path)) {
        if ( ! mkdir($upload_path, 0777, true)) {
          echo ("ERROR: failed to create directory <b>assets/images/slideshow/</b>");
          echo (", Make sure you have read/write permission");
        }
      }
      if ($this->upload->do_multi_upload("txtfiles")) {
        $images = $this->upload->get_multi_upload_data();

        for ($i=0; $i < count($images); $i++) {
          $image_url  = $upload_path.$images[$i]['file_name'];
          $path     = str_replace("./", "", $image_url);
          $config['image_library']  = 'gd2';
          $config['source_image']   = $image_url;
          $config['file_name']    = $image_url;
          $config['create_thumb']   = FALSE;
          $config['maintain_ratio']   = FALSE;
          $config['width']      = 1510;
          $config['height']     = 480;
          $this->load->library('image_lib');
          $this->image_lib->initialize($config);
          $this->image_lib->resize();  
        }
        $a="true";
      } else {  
        $a="false";
      }
      if($a=="true")
      {
        foreach ($images as $key => $slide)
        {
          $sinm[]=$slide['file_name'];
          $uid="slide_image_".$key;
          echo "<div class='col-sm-6 col-md-6 col-lg-4 banner-wrap' id='".$uid."'>";
          echo "<div class='overview widget' style='margin-top:0;height:auto;border-bottom-color:#888'>";
          $fnm='delete_slide_image('.$uid.',"'.$slide['file_name'].'");';
          echo "<div onclick='$fnm' class='titles'>";
          echo "<i class='fa fa-times delete-slide' slide-id='".$key."' style='color:#d43f3a'></i>&nbsp</div>";
          $imgurl=base_url().'fundraising/assets/images/slideshow/'.$slide['file_name'];
          echo "<img src='".$imgurl."' style='width:100%;height:100px;'></div></div>";
        }
        echo "<input type='hidden' id='select_slide_image' name='select_slide_image' value='".implode(",",$sinm)."' />";
        die;
      }
      else
      {
        echo "image upload problem";die;
      }
    }
    public function delete_slide($id)
    {
      $this->Event_model->delete_slide($id);
    }
    public function checksubdomain()
    {            
        if($this->input->post())
        {
            if (!preg_match("/^[a-zA-Z0-9_]*$/", $this->input->post('Subdomain'))) 
            {
              echo "error###Subdomain is not proper, please do not use special characters.";   
              exit;         
            }

            $subdomain = $this->Event_model->checksubdomain($this->input->post('Subdomain'),$this->input->post('idval'));
            if($subdomain)
            {
                echo "error###Subdomain already exist. Please choose another subdomain.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }

        ////////////////////search//////////////////
    public function search()
    {
        $search_event = $this->Event_model->search_event($this->input->post());            
        $my_array = array();
        for($i=0;$i<count($search_event);$i++)
        {
            $my_array[$i]['Event_id'] = $search_event[$i]['Id'];
            
            if(!empty($search_event[$i]['Event_name']))              
                $my_array[$i]['Name'] = $search_event[$i]['Event_name'];
            else if(!empty($search_event[$i]['Common_name']))              
                $my_array[$i]['Name'] = $search_event[$i]['Common_name'];
            
            $img= json_decode($search_event[$i]['Images']);
            $my_array[$i]['Image'] = $img[0];
            
            $cat = '';
            
            for($j=0;$j<count($search_event[$i]['Category']);$j++)
            {
                $cat .= ' category_'.$search_event[$i]['Category'][$j]['Id'];
            }
            $my_array[$i]['Category']= $cat;
        }           
        echo json_encode($my_array);
    }
        
    public function detail($id)                 
    {   
        if(empty($id))
            redirect ('Forbidden');
        
        if($this->data['user']->Role_name != 'Client')
        {

            $this->data['Event']= $this->Event_model->view_event_by_id($id);
        }
        else
        {
            $this->data['Event']= $this->Event_model->view_event_by_id($id);

        }     
        
        if(empty($this->data['Event']))
            redirect ('Forbidden');
        
        $this->template->write_view('css', 'event/css', $this->data , true);
        $this->template->write_view('content', 'event/detail', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        $this->template->write_view('js', 'event/js', $this->data , true);
        $this->template->render();
    }
        
    public function clearsearch()
    {
        $this->session->unset_userdata('botanicalname');
        $this->session->unset_userdata('monthname');
        redirect('Event');
    }

    public function get_event($id)                 
    {       
        $eventdata = $this->Event_model->get_event_list();
        echo json_encode($eventdata);
    }
    public function bannerupload()
    {
      $imgname = explode('.', $_FILES['images']['name']);
      $tempname = str_replace(" ","_",$imgname);
      $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
      $images_file = $tempname_imagename . "." . $tempname[1];
      $this->upload->initialize(array(
              "file_name" => $images_file,
              "upload_path" => "./assets/temp_file",
              "allowed_types" => 'gif|jpg|png|jpeg',
              "max_size" => '100000',
              "max_width" => '3000',
              "max_height" => '3000'
      ));
      if (!$this->upload->do_multi_upload("images"))
      {
          echo $this->upload->display_errors();
      }
      else
      {
        $data['imgname']=$images_file;
        $data['link']=base_url().'assets/temp_file/'.$images_file;
        $data['logo_or_banner']='1';
        $this->load->view('event/client/imagecrop',$data);
      }
    }
    public function imageupload()
    {
      $imgname = explode('.', $_FILES['logo_images']['name']);
      $tempname = str_replace(" ","_",$imgname);
      $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
      $filenames = $tempname_imagename . "." . $tempname[1];

      $this->upload->initialize(array(
              "file_name" => $filenames,
              "upload_path" => "./assets/temp_file",
              "allowed_types" => 'gif|jpg|png|jpeg',
              "max_size" => '100000',
              "max_width" => '3000',
              "max_height" => '3000'
      ));

      if (!$this->upload->do_multi_upload("logo_images"))
      {
        echo $this->upload->display_errors();
      }
      else
      {
        $data['imgname']=$filenames;
        $data['link']=base_url().'assets/temp_file/'.$filenames;
        $data['logo_or_banner']='0';
        $this->load->view('event/client/imagecrop',$data);
      }
    }
    public function savecropimage($eid)
    {
      $arr=explode(".",$_POST['organalimage']);
      $original_image = 'assets/temp_file/'.$_POST['organalimage'];
      $new_image = 'assets/user_files/'.$_POST['organalimage'];
      if($arr[1]=="png")
      {
        $image_quality = '0';  
      }
      else
      {
        $image_quality = '95';
      }
      list( $current_width, $current_height ) = getimagesize( $original_image );
      $x1 = $_POST['x1'];
      $y1 = $_POST['y1'];
      $x2 = $_POST['x2'];
      $y2 = $_POST['y2'];
      $width = $_POST['w'];
      $height = $_POST['h'];
      $crop_width = $width;
      $crop_height = $height; 
      $new = imagecreatetruecolor( $crop_width, $crop_height );
      if($arr[1]=="jpeg" || $arr[1]=="jpg")
      {
        $current_image = imagecreatefromjpeg( $original_image );
      }
      elseif($arr[1]=="png")
      {
       $current_image = imagecreatefrompng( $original_image ); 
      }
      else
      {
        $current_image = imagecreatefromgif( $original_image );
      }
      imagecopyresampled( $new, $current_image, 0, 0, $x1, $y1, $crop_width, $crop_height, $width, $height );
      if($arr[1]=="jpeg" || $arr[1]=="jpg")
      {
        imagejpeg( $new, $new_image, $image_quality );
      }
      elseif($arr[1]=="png")
      { 
        imagepng( $new, $new_image, $image_quality );
      }
      else
      {
        imagegif( $new, $new_image, $image_quality );
      }
      unlink($original_image);
      echo base_url().$new_image;die;
    }
    public function showhubmenusetting($eid)
    {
      $user = $this->session->userdata('current_user');
      $hub_active=$this->Event_model->get_hub_active_by_organizer($user[0]->Id);
      if($hub_active!='1')
      {
        redirect('Forbidden');
      }
      else
      {
        $hubevent=$this->Event_model->get_hub_event_menu_setting($eid);
        $event = $this->Event_model->get_admin_event($eid);
        $this->data['event'] = $event[0];
        if($this->input->post())
        {
          if(!empty($this->input->post('title')))
          {
            $data['title']=$this->input->post('title');
          }
          else
          {
            $data['title']=$event[0]['Event_name'];
          }
          if (!empty($_FILES['Images']['name'])) 
          {   
            $_FILES['Images']['name'] = str_replace(' ', '',$_FILES['Images']['name']);
            if (file_exists("./assets/user_files/" . $_FILES['Images']['name']))
            {
              $Images = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['Images']['name'];
            }
            else
            {
              $Images = $_FILES['Images']['name'];
            }
            $this->upload->initialize(array(
                "file_name" => $Images,
                "upload_path" => "./assets/user_files",
                "allowed_types" => '*',
                "max_size" => '10000',
                "max_width" => '3000',
                "max_height" => '3000'
            ));
            if (!$this->upload->do_multi_upload("Images")) 
            {
              $error = array('error' => $this->upload->display_errors());
              $this->session->set_flashdata('error', "For Menu Images, " . $error['error']);
              redirect(base_url().'Event/showhubmenusetting/'.$eid);
            }
            else
            {
              $data['hub_home_tab_image']=$Images;
            }
          }
          if($this->input->post('is_feture_product')=='1')
          {
            $data['is_feture_product']=$this->input->post('is_feture_product');
          }
          else
          {
            $data['is_feture_product']='0';
          }
          if($this->input->post('show_photos')=='1')
          {
            $data['show_photos']=$this->input->post('show_photos');
          }
          else
          {
            $data['show_photos']='0';
          }
          if($this->input->post('show_publicmsg')=='1')
          {
            $data['show_publicmsg']=$this->input->post('show_publicmsg');
          }
          else
          {
            $data['show_publicmsg']='0';
          }
          if($this->input->post('show_attendee')=='1')
          {
            $data['show_attendee']=$this->input->post('show_attendee');
          }
          else
          {
            $data['show_attendee']='0';
          }
          $data['img_view']=$this->input->post('img_view');
          $data['event_id']=$this->input->post('event_id');
          $this->Event_model->addhubevent($data);
          $this->session->set_flashdata('msg','Data Save successfully');
          redirect(base_url().'Event/showhubmenusetting/'.$eid);
        }
        $this->data['event_id']=$eid;
        $this->data['menusettingid']=$hubevent[0]['menusettingid'];
        if(!empty($hubevent[0]['title'])){
          $this->data['title']=$hubevent[0]['title'];
        }
        else
        {
          $this->data['title']=$event[0]['Event_name']; 
        }
        $this->data['is_feture_product']=$hubevent[0]['is_feture_products'];
        $this->data['show_photos'] = $hubevent[0]['show_photos'];
        $this->data['show_publicmsg'] = $hubevent[0]['show_publicmsg'];
        $this->data['show_attendee'] = $hubevent[0]['show_attendee'];
        $this->data['img_view']=$hubevent[0]['img_view'];
        $this->data['img']=$hubevent[0]['hub_home_tab_image'];
        $this->template->write_view('css', 'event/css', $this->data , true);
        $this->template->write_view('header', 'common/header', $this->data , true);
        $this->template->write_view('content', 'event/client/hub_view', $this->data , true);
        $this->template->write_view('js', 'event/client/js', $this->data , true);
        $this->template->render();
      }
    }
    public function detelefromhub($eid,$heid)
    {
      $this->Event_model->deletefromhub($heid); 
      $this->session->set_flashdata('msg','Data Delete successfully');
      redirect(base_url().'Event/showhubmenusetting/'.$eid);
    }
    public function show_your_app_time($eid)
    {
      date_default_timezone_set("UTC");
      $StartDateTime='';
      if(strpos($this->input->post('strtime'),"-")==true)
      { 
          $arr=explode("-",$this->input->post('strtime'));
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $StartDateTime = date('Y-m-d H:i',strtotime(date('Y-m-d H:i'))-$intNew);
      }
      if(strpos($this->input->post('strtime'),"+")==true)
      {
          $arr=explode("+",$this->input->post('strtime'));
          $intoffset=$arr[1]*3600;
          $intNew = abs($intoffset);
          $StartDateTime = date('Y-m-d H:i',strtotime(date('Y-m-d H:i'))+$intNew);
      }
      if(!empty($StartDateTime))
      {
        echo $StartDateTime;die;
      }
      else
      {
        echo date("Y-m-d H:i",strtotime(date('Y-m-d H:i')));die;
      }
    }
    public function eventhomepage($id)
    { 
      $arrAcc = $this->Event_template_model->getAccname($id); 
      $Subdomain=$this->Event_template_model->get_subdomain($id);    
      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
      $this->data['event_templates'] = $event_templates;
      $this->data['arrAct'] = $arrAcc;
      $this->data['fun_setting']=$this->Event_model->get_all_fun_setting($event_templates[0]['Id']);
      $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
      $this->data['menu'] = $menu;
      $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
      $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
      $this->data['menu_list'] = $menu_list;
      $flage_data=$this->Event_model->get_all_flage_data_by_event($event_templates[0]['Id']);   
      $this->data['flage_data']=$flage_data;
      $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
      $this->data['cms_feture_menu'] = $cms_feture_menu;
      $user = $this->session->userdata('current_user');

      if($user[0]->role_type == '1')
      {
        $questions=$this->Exibitor_survey_model->get_all_exibitor_user_questions($id,$user[0]->Id);
        $this->data['question_list']=$questions;    
        $this->data['representatives']=$this->Event_model->get_all_representatives($id,$user[0]->Id);
        $this->data['my_lead']=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($id,$user[0]->Id);
        $this->data['custom_column']=$this->Lead_retrieval_model->get_all_custom_column_data($event_templates[0]['Id']);

      }
      $roleid = $user[0]->Role_id;
      $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
      $this->data['users_role']=$user_role;
      $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
      $this->data['cms_menu'] = $cmsmenu;
      $this->data['notify_msg'] = $notifiy_msg;
      $this->data['Subdomain'] = $Subdomain;
      $this->data['Email'] = $Email;
      $this->data['speaker_flag']=$flag;
      $user = $this->session->userdata('current_user');
      $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $this->data['event_id']=$id;
      $this->data['flag'] = 1;

      $user = $this->session->userdata('current_user');
      $public_message_feeds = $this->activity_model->getPublicMessageFeeds($event_templates[0]['Id'],$user[0]->Id);
      $photo_feed     = $this->activity_model->getPhotoFeeds($event_templates[0]['Id'],$user[0]->Id);
      //j($user);
      /*$check_in_feed  = $this->activity_model->getCheckInFeeds($event_templates[0]['Id'],$user[0]->Id);
      $rating_feed    = $this->activity_model->getRatingFeeds($event_templates[0]['Id'],$user[0]->Id);
      $user_feed      = $this->activity_model->getUserFeeds($event_templates[0]['Id'],$user[0]->Id);
      $activity_feeds = $this->activity_model->getActivityFeeds($event_templates[0]['Id'],$user[0]->Id);
      $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds);*/
      $this->data['activity_data']=count($activity_data);
      $this->data['advertising_count']=$this->Event_model->get_all_advertising_count($id);
      $this->data['maps_count']=$this->Event_model->get_all_maps_count($id);
      $this->data['social_count']=$this->Event_model->get_all_social_link_count($id);
      $this->data['presentation_count']=$this->Event_model->get_all_presentation_count($id);
      $this->data['document_count']=$this->Event_model->get_all_documents_count($id);
      $this->data['notes_count']=$this->Event_model->get_all_notes_count($id);
      $this->data['private_messages_count']=$this->Event_model->get_all_private_messages_count($id);
      $this->data['public_messages_count']=$this->Event_model->get_all_public_messages_count($id);
      $this->data['photos_count']=$this->Event_model->get_all_photos_count($id);
      $this->data['surveys_count']=$this->Event_model->get_all_surveys_count($id);
      $this->data['hashtags_count']=$this->Event_model->get_all_twitter_hashtags_count($id);
      $this->data['formbuilder_count']=$this->Event_model->get_all_form_builder_count($id);
      $this->data['session_count']=$this->Event_model->get_all_agenda_category_count($id);
      $this->data['exhibitors_count']=$this->Event_model->get_all_exhibitors_count($id);
      $this->data['sponsors_count']=$this->Event_model->get_all_sponsors_count($id);
      $this->data['attendee_count']=$this->Event_model->get_all_attendee_count($id);
      $this->data['speakers_count']=$this->Event_model->get_all_speakers_count($id);
      $this->session->set_userdata($data);
      $this->template->write_view('css', 'event/client/add_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data , true);
      
      if($user[0]->role_type != '1')
      { 
        $this->template->write_view('content', 'event/client/eventhomepage_view', $this->data , true);
      }
      else
      { 


        $this->data['pagetitle'] = 'Leads';
        $this->data['smalltitle'] = 'Manage Your leads and representatives.';
        $this->template->write_view('content', 'event/lead_user/index', $this->data , true);
        $this->template->write_view('js', 'event/lead_user/js', $this->data , true);

      }
      $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
      //$this->template->write_view('js', 'event/client/add_js', $this->data , true);
      $this->template->render();
    }
    public function edit($id)
    {
      $arrAcc = $this->Event_template_model->getAccname($id); 
      $Subdomain=$this->Event_template_model->get_subdomain($id);    
      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
      $this->data['event_templates'] = $event_templates;
      $this->data['arrAct'] = $arrAcc;
      $this->data['fun_setting']=$this->Event_model->get_all_fun_setting($event_templates[0]['Id']);
      $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
      $this->data['menu'] = $menu;
      $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);

      $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
      $this->data['category_list']=$category_list;

      $modules_group = $this->Event_template_model->get_all_modules_group($event_templates[0]['Id'],'1');
      $this->data['modules_group']=$modules_group;

      $exhibitor_category = $this->Event_template_model->get_exibitor_parent_categories($event_templates[0]['Id']);
      $this->data['exhibitor_category']=$exhibitor_category;

      $exhibitor_child_category = $this->Event_template_model->get_exibitor_child_categories($event_templates[0]['Id']);
      $this->data['exhibitor_child_category']=$exhibitor_child_category;

      $cms_super_group = $this->Event_template_model->get_all_super_group($event_templates[0]['Id'],'21');
      $this->data['cms_super_group'] = $cms_super_group;
      $cam_sub_group = $this->Event_template_model->get_all_modules_group($event_templates[0]['Id'],'21');
      $this->data['cam_sub_group']=$cam_sub_group;


      $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
      $this->data['menu_list'] = $menu_list;
      $this->load->model('Cms_model');
      $frontcmsmenu = $this->Cms_model->get_cms_page($id);
      $this->data['frontcmsmenu'] = $frontcmsmenu;
      
      $active_icon=$this->Event_model->get_all_active_icon_list_by_event_id($event_templates[0]['Id']);
      $this->data['active_icon']=$active_icon;
      $inactive_icon=$this->Event_model->get_all_in_active_icon_list_by_event_id($event_templates[0]['Id']);
      $this->data['inactive_icon']=$inactive_icon;
      $all_homeicone=$this->Event_model->get_all_homescreenicon_by_event_id($event_templates[0]['Id']);
      $this->data['all_homeicone']=$all_homeicone;
      $active_iconmenu_ids=array_values(array_filter(array_column($active_icon,'menu_id')));
      $emenu=array_filter(explode(",",$event[0]['checkbox_values']));
      $activemenuid=array_diff($emenu,$active_iconmenu_ids);
      $active_menu=$this->Event_model->get_active_menu_list_by_event($event_templates[0]['Id'],$activemenuid);
      $this->data['active_menu']=$active_menu;
      $active_iconcmsmenu_ids=array_values(array_filter(array_column($active_icon,'cms_id')));
      $active_cms_menu=$this->Event_model->get_all_active_cms_menu_by_event($event_templates[0]['Id'],$active_iconcmsmenu_ids);
      $this->data['active_cms_menu']=$active_cms_menu;
      $active_cms_menu1=$this->Event_model->get_all_active_cms_menu_by_event($event_templates[0]['Id'],NULL);
      $this->data['active_cms_menu1']=$active_cms_menu1;
      $banner_list=$this->Event_model->get_all_banner_list_by_event($event_templates[0]['Id']);
      $this->data['banner_list']=$banner_list;

      $lefthandmenu_image_list=$this->Event_model->get_all_left_hand_menu_image_list_by_event($event_templates[0]['Id']);
      $this->data['lefthandmenu_image_list']=$lefthandmenu_image_list;

      $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
      $this->data['cms_feture_menu'] = $cms_feture_menu;
      $user = $this->session->userdata('current_user');
      $roleid = $user[0]->Role_id;
      $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
      $this->data['users_role']=$user_role;
      $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
      $this->data['cms_menu'] = $cmsmenu;
      $this->data['notify_msg'] = $notifiy_msg;
      $this->data['Subdomain'] = $Subdomain;
      $this->data['Email'] = $Email;
      $this->data['speaker_flag']=$flag;
      $user = $this->session->userdata('current_user');
      $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $this->data['flag'] = 1;
      $this->data['home_screen_tab']=$this->Event_model->get_home_screen_tabs($id);
      $this->session->set_userdata($data);
      $this->template->write_view('css', 'event/client/add_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data , true);
      $this->template->write_view('content', 'event/client/event_name_page', $this->data , true);
      $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
      $this->template->write_view('js', 'event/client/add_js', $this->data , true);
      $this->template->render();
    }
    public function save_app_name($id)
    {
      $this->Event_model->SaveAppdata($this->input->post(),$id);
      echo "success";die;
    }
    public function save_app_image_upload($id)
    {
      $headerimage=$this->input->post('header_images_crope_text');
      if(is_array($this->input->post('old_baaner_image_name')))
      {
        $oldheaderimage=$this->input->post('old_baaner_image_name');
      }
      else
      {
        $oldheaderimage=array(); 
      }
      $event = $this->Event_model->get_admin_event($id);
      $eventimgarr=json_decode($event[0]['Images'],true);
      foreach ($headerimage as $key => $value) {
        if(!empty($value)){
          $img=$value;
          $filteredData=substr($img, strpos($img, ",")+1); 
          $unencodedData=base64_decode($filteredData);
          $images_file = uniqid()."_event_crop_banner.png"; 
          $filepath = "./assets/user_files/".$images_file; 
          file_put_contents($filepath, $unencodedData);
          if(empty($oldheaderimage[$key]))
          {
            array_push($oldheaderimage,$images_file);
          }
          else
          {
            unlink("./assets/user_files/".$oldheaderimage[$key]);
            $oldheaderimage[$key]=$images_file; 
          }
        }
      }
      $event_image['Images']=json_encode(array_values($oldheaderimage));
      if(empty($this->input->post('logo_crope_images_text')))
      {
        if (!empty($_FILES['logo_image']['name'][0]))
        {
          $imgname = explode('.', $_FILES['logo_image']['name']);
          $tempname = str_replace(" ","_",$imgname);
          $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
          $filenames = $tempname_imagename . "." . $tempname[1];
          $this->upload->initialize(array(
            "file_name" => $filenames,
            "upload_path" => "./assets/user_files",
            "allowed_types" => 'gif|jpg|png|jpeg',
            "max_size" => '0',
            "max_width" => '0',
            "max_height" => '0'
          ));
          if (!$this->upload->do_multi_upload("logo_image"))
          {
            echo "error###".$this->upload->display_errors();die;
          }
          else
          {
            $event_image['Logo_images']=$filenames;          
          }
        }
      }
      else
      {
        $img=$_POST['logo_crope_images_text'];
        $filteredData=substr($img, strpos($img, ",")+1); 
        $unencodedData=base64_decode($filteredData);
        $logo_images = strtotime(date("Y-m-d H:i:s"))."_event_logo_crop.png"; 
        $filepath = "./assets/user_files/".$logo_images; 
        file_put_contents($filepath, $unencodedData);
        $event_image['Logo_images']=$logo_images; 
      }
      if(count($event_image) > 0)
      {   
        $this->Event_model->SaveAppdata($event_image,$id);
      }
      redirect(base_url().'Event/edit'.$id.'#app_images_section');
      //echo "success###";die;
    }
    public function save_app_color($id)
    {
      $this->Event_model->SaveAppdata($this->input->post(),$id);
      echo "success";die;
    }
    public function save_app_homeScreen_content($id)
    {
      $this->Event_model->SaveAppdata($this->input->post(),$id);
      echo "success";die;
    }
    public function save_app_icon_and_color($id)
    {
      $event = $this->Event_model->get_admin_event($id);
      $home_screen_tab=$this->Event_model->get_home_screen_tabs($id);
      $eventdata['Background_color']=$this->input->post('icon_backgroung_color');
      $eventdata['icon_set_type']=$this->input->post('icon_set_type');
      $this->Event_model->SaveAppdata($eventdata,$id);
      $iconset=array('1','2','3','6','7','9','10','11','12','13','15','16','17','43','44','45','46');
      $activemenu=explode(",",$event[0]['checkbox_values']);
      $iconset1=array_intersect($iconset,$activemenu); 
      foreach ($iconset1 as $key => $value) 
      {
        if(array_key_exists('modules_name_'.$value,$this->input->post()) || array_key_exists('modules_crop_image_'.$value,$this->input->post()) || array_key_exists('create_home_screen_tab_'.$value,$this->input->post()) || array_key_exists('home_screen_tab_back_color_'.$value,$this->input->post()))
        {
          $event_menu['menu_id']=$value;  
          $event_menu['event_id']=$id;
          if(!empty($this->input->post('modules_name_'.$value)))
          {
            $event_menu['title']=$this->input->post('modules_name_'.$value);
          }
          if(!empty($this->input->post('modules_crop_image_'.$value)))
          {
            $img=$_POST['modules_crop_image_'.$value];
            $filteredData=substr($img, strpos($img, ",")+1); 
            $unencodedData=base64_decode($filteredData);
            $imagename = uniqid()."icon_crop_logo.png"; 
            $filepath = "./assets/user_files/".$imagename; 
            file_put_contents($filepath, $unencodedData);
            $event_menu['img']=$imagename;
          }
          else
          {
            if(!array_key_exists($value,$home_screen_tab))
            {
              $event_menu['img']="";
            }
            else
            {
              $event_menu['img']=$home_screen_tab[$value]['img'];
            }
          }
          $event_menu['is_feture_product']=$this->input->post('create_home_screen_tab_'.$value); 
          $event_menu['img_view']='0';
          $event_menu['Background_color']=$this->input->post('home_screen_tab_back_color_'.$value);
          $this->Event_model->add_left_hand_menu_title($event_menu);
        }
      }
      echo "success";die;
    }
    public function get_formdata($eid)
    {
      $headerimage=$this->input->post('header_images_crope_text');
      if(is_array($this->input->post('old_baaner_image_name')))
      {
        $oldheaderimage=$this->input->post('old_baaner_image_name');
      }
      else
      {
        $oldheaderimage=array(); 
      }
      $event = $this->Event_model->get_admin_event($id);
      $eventimgarr=json_decode($event[0]['Images'],true);
      foreach ($headerimage as $key => $value) {
        if(!empty($value)){
          $img=$value;
          $filteredData=substr($img, strpos($img, ",")+1); 
          $unencodedData=base64_decode($filteredData);
          $images_file = uniqid()."_event_crop_banner.png"; 
          $filepath = "./assets/user_files/".$images_file; 
          file_put_contents($filepath, $unencodedData);
          if(empty($oldheaderimage[$key]))
          {
            array_push($oldheaderimage,$images_file);
          }
          else
          {
            unlink("./assets/user_files/".$oldheaderimage[$key]);
            $oldheaderimage[$key]=$images_file; 
          }
        }
      }
      $event_image['Images']=json_encode(array_values($oldheaderimage));
      if(empty($this->input->post('logo_crope_images_text')))
      {
        if (!empty($_FILES['logo_image']['name'][0]))
        {
          $imgname = explode('.', $_FILES['logo_image']['name']);
          $tempname = str_replace(" ","_",$imgname);
          $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
          $filenames = $tempname_imagename . "." . $tempname[1];
          $this->upload->initialize(array(
            "file_name" => $filenames,
            "upload_path" => "./assets/user_files",
            "allowed_types" => 'gif|jpg|png|jpeg',
            "max_size" => '0',
            "max_width" => '0',
            "max_height" => '0'
          ));
          if (!$this->upload->do_multi_upload("logo_image"))
          {
            echo "error###".$this->upload->display_errors();die;
          }
          else
          {
            $event_image['Logo_images']=$filenames;          
          }
        }
      }
      else
      {
        $img=$_POST['logo_crope_images_text'];
        $filteredData=substr($img, strpos($img, ",")+1); 
        $unencodedData=base64_decode($filteredData);
        $logo_images = strtotime(date("Y-m-d H:i:s"))."_event_logo_crop.png"; 
        $filepath = "./assets/user_files/".$logo_images; 
        file_put_contents($filepath, $unencodedData);
        $event_image['Logo_images']=$logo_images; 
      }
      if(count($event_image) > 0)
      {   
        $this->Event_model->SaveAppdata($event_image,$eid);
      }
      if(array_key_exists('edit_home_screen_content',$this->input->post()))
      {
        $data['Description']=$this->input->post('edit_home_screen_content');
        $this->Event_model->SaveAppdata($data,$eid);
      }
      redirect(base_url().'Event/edit/'.$eid);
    }
    public function eventfreetrial_previewapp($id)
    {
      $arrAcc = $this->Event_template_model->getAccname($id); 
      $Subdomain=$this->Event_template_model->get_subdomain($id);    
      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
      $this->data['event_templates'] = $event_templates;
      $this->data['arrAct'] = $arrAcc;
      $this->data['fun_setting']=$this->Event_model->get_all_fun_setting($event_templates[0]['Id']);
      $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
      $this->data['menu'] = $menu;
      $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
      $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
      $this->data['menu_list'] = $menu_list;
      $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
      $this->data['cms_feture_menu'] = $cms_feture_menu;
      $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
      $this->data['cms_menu'] = $cmsmenu;
      $this->data['notify_msg'] = $notifiy_msg;
      $this->data['Subdomain'] = $Subdomain;
      $this->data['Email'] = $Email;
      $this->data['speaker_flag']=$flag;
      $user = $this->session->userdata('current_user');
      $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $this->data['flag'] = 1;
      $this->data['home_screen_tab']=$this->Event_model->get_home_screen_tabs($id);
      $this->session->set_userdata($data);
      $this->template->write_view('css', 'event/client/add_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data , true);
      $this->template->write_view('content', 'event/client/preview_app_view', $this->data , true);
      $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
      $this->template->write_view('js', 'event/client/add_js', $this->data , true);
      $this->template->render();
    }
    public function on_modules_in_home($eid)
    {
      $menu_id=$this->input->post('mid');
      $event = $this->Event_model->get_admin_event($eid);
      $active_menu=array_filter(explode(",",$event[0]['checkbox_values']));
      if(!in_array($menu_id,$active_menu))
      {
        array_push($active_menu,$menu_id);
        sort(array_filter($active_menu));
        $data['event_array']['Id'] = $eid;
        $data['event_array']['checkbox_values'] = implode(",",$active_menu);
        $this->Event_model->update_admin_event($data);
      }
      echo "success###";die;
    }
    public function save_as_complate($eid)
    {
      $flage_data=$this->input->post();
      $this->Event_model->save_complate_flage_data($flage_data,$eid);
    }
    public function get_launch_your_app_function_content($eid)
    {
      $this->data['event_id']=$eid;
      $event = $this->Event_model->get_admin_event($eid);
      $this->data['event']=$event[0];
      $countrylist = $this->Profile_model->countrylist();
      $this->data['countrylist'] = $countrylist;
      $admin_scret_key = $this->Event_model->get_admin_stripe_setting();
      $this->data['admin_scret_key']=$admin_scret_key;
      $splash_screen=$this->Event_model->get_splash_screen_data($event[0]['Organisor_id']);
      $this->data['splash_screen']=$splash_screen;
      $user = $this->session->userdata('current_user');
      $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
      $this->data['iseventfreetrial']=$iseventfreetrial;
      if(count($iseventfreetrial) > 0 && $iseventfreetrial[0]['launch_stripe_payment']=='1')
      {
        $this->data['which_launch_your_app_btn_click']=$iseventfreetrial[0]['launch_multi_app'];
        $this->load->view('event/client/splace_screen_upload',$this->data);
      }
      else
      {
        $this->load->view('event/client/launch_your_app_views',$this->data);
      }
    }
    public function make_launch_stripe_payment($eid)
    {
      $this->data['event_id']=$eid;
      $ip = $_SERVER['REMOTE_ADDR'];
      $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
      if($query && $query['status'] == 'success') 
      {
        if($query['countryCode'] == 'UK')
        {
          $currency="GBP";
        }
        else
        {
          $currency="USD";
        }
      }
      else
      {
        $currency="USD";
      }
      $event = $this->Event_model->get_admin_event($eid);
      $admin_scret_key = $this->Event_model->get_admin_stripe_setting();
      $org_scret_key=$this->Event_model->get_organisor_scret_key_by_organisor_id($event[0]['Organisor_id']);
      $user_key=$org_scret_key[0]['stripe_user_id'];
      $payment;
      if(!empty($this->input->post('stripeToken')) && !empty($this->input->post('payment_price'))){
        if(!class_exists('\Stripe\Stripe')){
          require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
        }
        //\Stripe\Stripe::setApiKey('sk_test_B9lPcKzb5oIMZ3SiDOlDEu8i');
        \Stripe\Stripe::setApiKey($admin_scret_key[0]['secret_key']);
        $payment =  \Stripe\Charge::create(
          array(
          "amount" => $this->input->post('payment_price') * 100,
          "currency" => $currency,
          "source" => $this->input->post('stripeToken'),
          "description" => "All In The Loop"
          )
        );
      }
      if($payment['status']=="succeeded")
      {
        $user = $this->session->userdata('current_user');
        if($this->input->post('payment_price')=='1999')
        {
          $st['launch_multi_app']='2';
        }
        else
        {
          $st['launch_multi_app']='1';
        }
        $st['launch_stripe_payment']='1';
        $this->Event_model->change_done_stripe_payment_status($user[0]->Email,$st);
      }
      if($payment['status']=="succeeded" && $this->input->post('payment_price')!="999")
      {
        $this->data['which_launch_your_app_btn_click']=$this->input->post('which_launch_your_app_btn_click');
        $this->load->view('event/client/splace_screen_upload',$this->data);
      }
      else
      {
        if($this->input->post('payment_price')=='999' && $payment['status']=="succeeded")
        {
          $launchevent['event_array']['Id']=$eid;
          $launchevent['event_array']['launch_your_app']='1';
          $launchevent['event_array']['islaunchapp']='1';
          $this->Event_model->update_admin_event($launchevent);
          $message="Hi admin,";
          $user = $this->session->userdata('current_user');
          $message.="<br/> User Name=".ucfirst($user[0]->Firstname)." ".$user[0]->Lastname;
          $config['protocol']   = 'smtp';
          $config['smtp_host']  = 'localhost';
          $config['smtp_port']  = '25';
          $config['smtp_user']  = 'invite@allintheloop.com';
          $config['smtp_pass']  = 'IKIG{ADoF]*P';
          $config['mailtype'] = 'html';
          $this->email->initialize($config);
          $this->email->from('invite@allintheloop.com','All In The Loop');
          //$this->email->to('gaurang@xhtmljunkies.com');
          $this->email->to('lawrence@allintheloop.com');
          $this->email->subject('Launch Your App');
          $this->email->message($message);
          $this->email->send();
          $org_email=$this->Event_model->getOrgEmail($event[0]['Organisor_id']);
          $subscription_type=21;
          $this->Event_model->change_subscription_type_by_org_email($org_email,$subscription_type);
          $splash_screen=$this->Event_model->get_splash_screen_data($event[0]['Organisor_id']);
          $this->data['splash_screen']=$splash_screen;
        }
        $this->data['status']=$payment['status'];
        $this->load->view('event/client/launch_your_app_submit_msg',$this->data);
      }
    }
    public function save_launch_your_app_data($eid)
    {
      $user = $this->session->userdata('current_user');
      $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
      $event = $this->Event_model->get_admin_event($eid);
      if(!empty($this->input->post('splash_screen_images_data_textbox')))
      {
        $img=$_POST['splash_screen_images_data_textbox'];
        $filteredData=substr($img, strpos($img, ",")+1); 
        $unencodedData=base64_decode($filteredData);
        $imagename = uniqid()."splash_screen_images.png"; 
        $filepath = "./assets/user_files/".$imagename; 
        file_put_contents($filepath, $unencodedData);
        $splash_screen['splash_screen_images']=$imagename;
      }
      if(!empty($this->input->post('app_icon_crope_images_text')))
      {
        $img=$_POST['app_icon_crope_images_text'];
        $filteredData=substr($img, strpos($img, ",")+1); 
        $unencodedData=base64_decode($filteredData);
        $imagename1 = uniqid()."app_icon_images.png"; 
        $filepath = "./assets/user_files/".$imagename1; 
        file_put_contents($filepath, $unencodedData);
        $splash_screen['app_icon_images']=$imagename1;
      }
      $splash_screen['app_name']=$this->input->post('app_name_textbox');
      $splash_screen['company_name']=$this->input->post('company_name_textbox');
      $splash_screen['privacy_policy_url']=$this->input->post('privacy_policy_url');
      $splash_screen['primary_language']=$this->input->post('primary_language');
      $splash_screen['category_primary']=$this->input->post('category_primary');
      $splash_screen['category_secondary']=$this->input->post('category_secondary');
      $splash_screen['support_url']=$this->input->post('support_url');
      $splash_screen['marketing_url']=$this->input->post('marketing_url');
      $splash_screen['app_description']=$this->input->post('app_description');
      $splash_screen['app_keyword']=$this->input->post('app_keyword');
      $splash_screen['copyright']=$this->input->post('copyright');
      $splash_screen['app_launch_country']=$this->input->post('country');
      $splash_screen['app_user_first_name']=$this->input->post('app_user_first_name');
      $splash_screen['app_user_last_name']=$this->input->post('app_user_last_name');
      $splash_screen['app_user_address']=$this->input->post('app_user_address');
      $splash_screen['app_user_city']=$this->input->post('app_user_city');
      $splash_screen['app_user_state']=$this->input->post('app_user_state');
      $splash_screen['app_user_zipcode']=$this->input->post('app_user_zipcode');
      $splash_screen['app_user_country']=$this->input->post('app_user_country');
      $splash_screen['app_user_phone_number']=$this->input->post('app_user_phone_number');
      $splash_screen['app_user_email']=$this->input->post('app_user_email');
      $splash_screen['multi_event_name']=$this->input->post('multi_event_name_textbox');
      $launchevent['event_array']['Id']=$eid;
      if(!empty($this->input->post('multi_event_name_textbox')))
      {
        $launchevent['event_array']['launch_your_app']='2';
      }
      else
      {
        $launchevent['event_array']['launch_your_app']='1'; 
      }
      $launchevent['event_array']['islaunchapp']='1';
      $this->Event_model->update_admin_event($launchevent);
      $message="Hi admin,";
      $user = $this->session->userdata('current_user');
      $message.="<br/> User Name=".ucfirst($user[0]->Firstname)." ".$user[0]->Lastname;
      if(count($splash_screen)>0)
      {
        $splash_screen['organisor_id']=$event[0]['Organisor_id'];
        $this->Event_model->save_splash_screen_data($splash_screen);
        $message.="<br/> App Name=".$splash_screen['app_name'];
        $message.="<br/> Multi Event Name=".$splash_screen['multi_event_name'];
        $message.="<br/> Splash Screen=".base_url()."assets/user_files/".$splash_screen['splash_screen_images'];
        $message.="<br/> App Icon=".base_url()."assets/user_files/".$splash_screen['app_icon_images'];
        $message.="<br/> Primary_Language=".$splash_screen['primary_language'];
        $message.="<br/> App Description=".$splash_screen['app_description'];
        $message.="<br/> App Keyword=".$splash_screen['app_keyword'];
      }
      $config['protocol']   = 'smtp';
      $config['smtp_host']  = 'localhost';
      $config['smtp_port']  = '25';
      $config['smtp_user']  = 'invite@allintheloop.com';
      $config['smtp_pass']  = 'IKIG{ADoF]*P';
      $config['mailtype'] = 'html';
      $this->email->initialize($config);
      $this->email->from('invite@allintheloop.com','All In The Loop');
      //$this->email->to('gaurang@xhtmljunkies.com');
      $this->email->to('lawrence@allintheloop.com');
      $this->email->subject('Launch Your App');
      $this->email->message($message);
      $this->email->send();
      $org_email=$this->Event_model->getOrgEmail($event[0]['Organisor_id']);
      if($iseventfreetrial[0]['launch_multi_app']=='1')
      {
        $subscription_type=20;
      }
      else
      {
        $subscription_type=19;
      }
      $this->Event_model->change_subscription_type_by_org_email($org_email,$subscription_type);
      $splash_screen=$this->Event_model->get_splash_screen_data($event[0]['Organisor_id']);
      $this->data['splash_screen']=$splash_screen;
      $this->data['status']="succeeded";
      $this->load->view('event/client/launch_your_app_submit_msg',$this->data);
    }
    public function delete_banner_image($eid)
    {
      $event = $this->Event_model->get_admin_event($eid);
      $imagearr=json_decode($event[0]['Images']);
      $old_banner_url = json_decode($event[0]['banner_url'],true);
      
      unset($old_banner_url[$this->input->post('imagename')]);

      if(in_array($this->input->post('imagename'),$imagearr))
      {
        $key=array_search($this->input->post('imagename'),$imagearr);
        unset($imagearr[$key]);
        unlink('./assets/user_files/'.$this->input->post('imagename'));
      }
      $data['banner_url'] = json_encode($old_banner_url);
      $data['Images'] = json_encode(array_values($imagearr));
      $this->Event_model->SaveAppdata($data,$eid);
      echo "success";die;
    }
    public function saveeditorfiles($eid)
    {
      $filenames = uniqid().'_'.$_FILES['file']['name'];
      $this->upload->initialize(array(
        "file_name" => $filenames,
        "upload_path" => "./assets/user_files",
        "allowed_types" => 'gif|jpg|png|jpeg',
        "max_size" => '0',
        "max_width" => '0',
        "max_height" => '0'
      ));
      if ($this->upload->do_multi_upload("file"))
      {
        echo base_url().'assets/user_files/'.$filenames;
      }
      else
      {
        echo "";
      }die;
    }
    public function save_banner_gif_image($id)
    {
      $filenames = uniqid().'_'.$_FILES['gifimage']['name'];
      $this->upload->initialize(array(
        "file_name" => $filenames,
        "upload_path" => "./assets/user_files",
        "allowed_types" => 'gif|jpg|png|jpeg',
        "max_size" => '0',
        "max_width" => '0',
        "max_height" => '0'
      ));
      if ($this->upload->do_multi_upload("gifimage"))
      {
        $bannerid=$this->input->post('bannerid');
        $banner_details=$this->Event_model->get_banner_details_by_banner_id($bannerid);
        $advance_data['Image']=$filenames;
        $advance_data['event_id']=$id;
        if(count($banner_details) > 0)
        {
          unlink("./assets/user_files/".$banner_details[0]['Image']);
          $data['banner_id']=$bannerid;
          $this->Event_model->update_banner_image_in_event($advance_data,$bannerid);
        }
        else
        {
          $advance_data['image_type']='0';
          $advance_data['datetime']=date('Y-m-d H:i:s');
          $data['banner_id']=$this->Event_model->save_banner_image_in_event($advance_data);
        }
        $data['status']="Success";
        $data['img_url']=base_url()."assets/user_files/".$filenames;
        $data['img_name']=$filenames;
        echo json_encode($data);
      }
      else
      {
        echo json_encode(array('status'=>"error",'error'=>$this->upload->display_errors()));
      }
      die;
    }
    public function save_advance_design_image_content($eid)
    {
      $order_icon=$this->input->post('active_order_icon');
      $order_icon1=$this->input->post('order_menu_id');
      $order=count($order_icon);
      foreach ($order_icon1 as $key => $value) {
        $this->Event_model->update_active_icon_order($value,$order+1,'0');
        $order++;
      }
      foreach($order_icon as $key => $value) 
      {
        $this->Event_model->update_active_icon_order($value,$key+1,'1');
        if(!empty($this->input->post('convert_'.$value)))
        {
          if(filter_var($this->input->post('convert_'.$value), FILTER_VALIDATE_URL))
          {
            $this->Event_model->add_custom_modules_link($eid,$value,$this->input->post('convert_'.$value));
          }
          else
          {
            $emtid=explode("_",$this->input->post('convert_'.$value));
            if($emtid[0]=="menuid")
            {
              $menu_id=$emtid[1];
            }
            else if($emtid[0]=="cmsid")
            {
              $cms_id=$emtid[1];
            }
            else{}
            $this->Event_model->delete_event_menu_exists_by_menu_or_cms_id($eid,$menu_id,$cms_id);
            $this->Event_model->update_event_menu_modules($eid,$value,$menu_id,$cms_id);
          }
        }
      }
      #cache
      /*$this->load->library('memcached_library');
      $this->memcached_library->delete('banner_images_coords$event_id');*/
      #cache

      redirect(base_url().'Event/edit/'.$eid.'#app_advanced_design_section');
    }
  public function compress($source, $destination, $quality) { 

    $info = getimagesize($source); 

    if ($info['mime'] == 'image/jpeg') 
      $image = imagecreatefromjpeg($source); 
    elseif ($info['mime'] == 'image/gif') 
      $image = imagecreatefromgif($source); 
    elseif ($info['mime'] == 'image/png') 
      $image = imagecreatefrompng($source); 

    imagejpeg($image, $destination, $quality); 

    return $destination; 
  }   
  public function save_banner_image($id)
  {
    $bannerid=$this->input->post('bannerid');
    $banner_details=$this->Event_model->get_banner_details_by_banner_id($bannerid);
    $img=utf8_decode($this->input->post('image'));
    $filteredData=substr($img, strpos($img, ",")+1); 
    
    $filteredData=str_replace('removed]','', $filteredData);
   
    $unencodedData=base64_decode($filteredData);
    $imagename1 = uniqid()."app_advance_images.png";  
    $filepath = "./assets/user_files/".$imagename1; 
    file_put_contents($filepath, $unencodedData);
    $source_image="./assets/user_files/".$imagename1;
    $imagename12=uniqid().$imagename1;
    $destination_img="./assets/user_files/".$imagename12;
    $d = $this->compress($source_image, $destination_img, 50);
    $advance_data['Image']=$imagename12;
    $advance_data['event_id']=$id;
    if(count($banner_details) > 0)
    {
      unlink("./assets/user_files/".$banner_details[0]['Image']);
      $data['banner_id']=$bannerid;
      $this->Event_model->update_banner_image_in_event($advance_data,$bannerid);
    }
    else
    {
      $advance_data['image_type']='0';
      $advance_data['datetime']=date('Y-m-d H:i:s');
      $data['banner_id']=$this->Event_model->save_banner_image_in_event($advance_data);
    }
    $data['img_url']=base_url()."assets/user_files/".$imagename12;
    $data['img_name']=$imagename12;
    echo json_encode($data);die;
  }
  public function save_advance_content($eid)
  {
    $advance_data['Content']=$this->input->post('content');
    $advance_data['event_id']=$eid;
    $this->Event_model->update_banner_image_in_event($advance_data,$this->input->post('banner_id'));
    echo json_encode($advance_data);die; 
  }
  // public function save_banner_area_coords($eid)
  // {
  //   $area_data['banner_id']=$this->input->post('banner_id');
  //   $area_data['coords']=$this->input->post('coords');
  //   if(!empty($this->input->post('area_link')))
  //   {
  //     $area_data['redirect_url']=$this->input->post('area_link');
  //     $area_data['cmsid']=NULL;
  //     $area_data['menuid']=NULL;
  //     $area_data['agenda_id']=NULL;
  //   }
  //   else
  //   {
  //     $area_data['redirect_url']=NULL;
  //     $arr=explode("_",$this->input->post('menu_id'));
  //     if($arr[0]=="menuid")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=$arr[1];
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;
        
  //     }
  //     else if($arr[0]=="cmsid")
  //     {
  //       $area_data['cmsid']=$arr[1];
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     } 
  //     else if($arr[0]=="agendaid")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=$arr[1];
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     }
  //     else if($arr[0]=="exhiid")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['exhi_id']=$arr[1];
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     }
  //     else if($arr[0]=="cmssupergroup")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['super_group_id']=$arr[1];
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     }
  //     else if($arr[0]=="exhisubid")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['exhi_sub_cat_id']=$arr[1];
  //       $area_data['super_group_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     }
  //     else if($arr[0]=="allexhi")
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['group_id']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['all_exhi_sub_cat']='1';
  //     }
  //     else
  //     {
  //       $area_data['cmsid']=NULL;
  //       $area_data['agenda_id']=NULL;
  //       $area_data['menuid']=NULL;
  //       $area_data['exhi_id']=NULL;
  //       $area_data['super_group_id']=NULL;
  //       $area_data['group_id']=$arr[1];
  //       $area_data['exhi_sub_cat_id']=NULL;
  //       $area_data['all_exhi_sub_cat']=NULL;

  //     }
  //   }
  //   $area_id=$this->Event_model->save_banner_image_area_coords($area_data);
  //   echo '<area alt="Mapping" shape="rect" onclick="delete_area(this,'.$area_id.')" href="javascript:void(0);" coords="'.$area_data['coords'].'">';die;
  // }

  public function save_banner_area_coords($eid)
  {
    $area_data['banner_id']=$this->input->post('banner_id');
    $area_data['coords']=$this->input->post('coords');
    if(!empty($this->input->post('area_link')))
    {
      $area_data['redirect_url']=$this->input->post('area_link');
      $area_data['cmsid']=NULL;
      $area_data['menuid']=NULL;
      $area_data['agenda_id']=NULL;
    }
    else
    {
      $area_data['redirect_url']=NULL;
      $arr=explode("_",$this->input->post('menu_id'));
      if($arr[0]=="menuid")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=$arr[1];
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;
        
      }
      else if($arr[0]=="cmsid")
      {
        $area_data['cmsid']=$arr[1];
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      } 
      else if($arr[0]=="agendaid")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=$arr[1];
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      }
      else if($arr[0]=="exhiid")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['exhi_id']=$arr[1];
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      }
      else if($arr[0]=="cmssupergroup")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['super_group_id']=$arr[1];
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      }
      else if($arr[0]=="exhisubid")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['exhi_sub_cat_id']=$arr[1];
        $area_data['super_group_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      }
      else if($arr[0]=="allexhi")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['all_exhi_sub_cat']='1';
      }
      else if($arr[0]=="myagenda")
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']='1';
        $area_data['group_id']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;
        $area_data['myagenda'] = '1';
      }
      else
      {
        $area_data['cmsid']=NULL;
        $area_data['agenda_id']=NULL;
        $area_data['menuid']=NULL;
        $area_data['exhi_id']=NULL;
        $area_data['super_group_id']=NULL;
        $area_data['group_id']=$arr[1];
        $area_data['exhi_sub_cat_id']=NULL;
        $area_data['all_exhi_sub_cat']=NULL;

      }
    }
    $area_id=$this->Event_model->save_banner_image_area_coords($area_data);
    echo '<area alt="Mapping" shape="rect" onclick="delete_area(this,'.$area_id.')" href="javascript:void(0);" coords="'.$area_data['coords'].'">';die;
  }
  public function delete_banner_area($eid,$area_id)
  {
    $this->Event_model->delete_banner_image_area($area_id);
    echo "success";die;
  }
  public function delete_advance_banner_image($eid,$bid)
  {
    $banner_details=$this->Event_model->get_banner_details_by_banner_id($bid);
    if(count($banner_details) > 0)
    {
      unlink("./assets/user_files/".$banner_details[0]['Image']);
      $this->Event_model->delete_Advance_banner($bid);
      echo "success";die;
    }
    else
    {
      echo "error";die;
    }
  }
  public function save_o_screen_upload($id)
  {
    $headerimage=$this->input->post('header_images_crope_text');
    if(is_array($this->input->post('old_baaner_image_name')))
    {
      $oldheaderimage=$this->input->post('old_baaner_image_name');
    }
    else
    {
      $oldheaderimage=array(); 
    }
    $event = $this->Event_model->get_admin_event($id);
    foreach ($headerimage as $key => $value) {
      if(!empty($value)){
        $img=$value;
        $filteredData=substr($img, strpos($img, ",")+1); 
        $unencodedData=base64_decode($filteredData);
        $images_file = $id."_".time()."_o_screen.png"; 
        $filepath = "./assets/onboarding_screen/".$images_file; 
        file_put_contents($filepath, $unencodedData);
        if(empty($oldheaderimage[$key]))
        {
          array_push($oldheaderimage,$images_file);
        }
        else
        {
          unlink("./assets/onboarding_screen/".$oldheaderimage[$key]);
          $oldheaderimage[$key]=$images_file; 
        }
      }
    }
    if(!empty($_FILES['gifimage']['name']))
    {
      $filenames = uniqid().'_'.$_FILES['gifimage']['name'];
      $this->upload->initialize(array(
        "file_name" => $filenames,
        "upload_path" => "./assets/onboarding_screen",
        "allowed_types" => 'gif|jpg|png|jpeg',
        "max_size" => '0',
        "max_width" => '0',
        "max_height" => '0'
      ));
      if ($this->upload->do_multi_upload("gifimage"))
      {
        array_push($oldheaderimage,$filenames);
      }
    }
    $event_image['o_screen']=json_encode(array_values($oldheaderimage));
    if(count($event_image) > 0)
    {   
      $this->Event_model->SaveAppdata($event_image,$id);
    }
    $event = $this->Event_model->get_admin_event($id);
    $data = json_decode($event[0]['o_screen']);
    $key = count($data)-1;
    $value = end($data);?>
    <li class="item-1" name="<?= $value?>" id="<?=$key+1?>">
      <div data-provides="fileupload" class="fileupload fileupload-exists">
          <input type="hidden" value="<?php echo $value; ?>" name="old_baaner_image_name[<?php echo $key+1 ?>]">
          <div class="file_upload_div">
              <img src="<?= base_url()?>assets/onboarding_screen/<?=$value?>" >
              <a data-dismiss="fileupload" class="close-image fileupload-exists float-none" href="#" onclick="delete_banner_image(this,'<?=$value?>','<?=$key+1?>');">
                  &times;
              </a>
          </div>
      </div>
    </li>
    <?php
  }
  public function delete_o_screen($eid)
  {
    $event = $this->Event_model->get_admin_event($eid);
    $imagearr=json_decode($event[0]['o_screen']);
    
    if(in_array($this->input->post('imagename'),$imagearr))
    {
      $key=array_search($this->input->post('imagename'),$imagearr);
      unset($imagearr[$key]);
      unlink('./assets/onboarding_screen/'.$this->input->post('imagename'));
    }
    $data['o_screen'] = json_encode(array_values($imagearr));
    $this->Event_model->SaveAppdata($data,$eid);
    echo "success";die;
  }
  public function save_sort_order($eid)
  { 
    $event_image['o_screen']=json_encode(array_values($this->input->post('data')));
    $this->Event_model->SaveAppdata($event_image,$eid);    
    exit;
  }
  public function save_onboarding_settings($eid)
  {
    $data = $_POST;
    $this->Event_model->save_onboarding_settings($data,$eid);
    $this->session->set_flashdata('success', 'Onboarding Screens Settings Saved Successfully.');
    redirect(base_url().'Event/access_setting/'.$eid.'#o_screen');
  }
  public function add_new_language($eid)
  {
    $lang_arr['lang_name']=trim($this->input->post('lang_name'));
    $lang_arr['lang_icon']=trim($this->input->post('language_icon'));
    $lang_arr['event_id']=$eid;
    $lang_arr['lang_default']='0';
    $this->Event_template_model->save_user_custom_language($lang_arr,$lang_id);
    $this->session->set_flashdata('success', 'Language Save Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid);
  }
  public function check_language_name($eid)
  {
    $lang_name=trim($this->input->post('lang_name'));
    $lang_id=$this->input->post('lang_id');
    $checkvalid=$this->Event_template_model->check_valid_language_name($eid,$lang_name,$lang_id);
    if($checkvalid)
    {
      echo "Success###";
    }
    else
    {
      echo "Error###Language Name Already Exists Please Enter Another Name...";
    }
  }
  public function make_primary_language($eid)
  {
    $lang_id=$this->input->post('lang_id');
    $this->Event_template_model->make_event_primary_language($eid,$lang_id);
    echo "Success###";
  }
  public function save_language_labeldata($eid)
  {
    $postarr=array_filter($this->input->post());
    foreach ($postarr as $key => $value) 
    {
      if(strpos($key, '__') !== false)
      {
        $keyarr=explode("__",$key);
        $kv_data['event_id']=$eid;
        $kv_data['lang_id']=$keyarr[1];
        $kv_data['menu_id']= !empty($this->input->post('menu_id')) ? $this->input->post('menu_id') : NULL;
        $kv_data['custom_modules_name']=!empty($this->input->post('custom_modules_name')) ? $this->input->post('custom_modules_name') : NULL;
        $kv_data['label_key']=$keyarr[0];
        $kv_data['label_values']=trim($value);
        $this->Event_template_model->save_language_label_data($kv_data);
        unset($kv_data);
      }
    }
    $this->session->set_flashdata('success', 'Language Label Data Save Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid);
  }
  public function save_language_menudata($eid)
  {
    $postarr=array_filter($this->input->post());
    $custom_modules_name=$postarr['custom_modules_name'];
    unset($postarr['custom_modules_name']);
    foreach ($postarr as $key => $value) {
      if(strpos($key, '___') !== false)
      {
        $customdata=explode("___",$key);
        $keyarr=explode("__",$customdata[1]);
        $kv_data['event_id']=$eid;
        $kv_data['lang_id']=$keyarr[1];
        $kv_data['menu_id']= !empty($this->input->post('menu_id')) ? $this->input->post('menu_id') : NULL;
        $kv_data['custom_modules_name']=!empty($custom_modules_name) ? $custom_modules_name : NULL;
        $kv_data['label_key']=$keyarr[0];
        $kv_data['label_values']=trim($value);
        $this->Event_template_model->save_language_label_data($kv_data);
        unset($kv_data);
      }
      else
      {
        $menukeyarr=explode("__",$key);
        $menu_data['event_id']=$eid;
        $menu_data['menu_id']= $menukeyarr[0];
        $menu_data['lang_id']=$menukeyarr[1];
        $menu_data['lang_menu_name']=trim($value);
        $this->Event_template_model->save_language_menu_data($menu_data);
        unset($menu_data);
      }
    }
    $this->session->set_flashdata('success', 'Language Label Data Save Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid);
  }
  public function save_language_contentdata($eid)
  {
    $add_session_array=array();
    $postarr=array_filter($this->input->post());
    foreach($postarr as $key => $value) 
    {
      if(strpos($key, '__') !== false)
      {
        $keyarr=explode("__",$key);
        $checkuliqkey=$keyarr[0].$keyarr[2];
        if(!in_array($checkuliqkey,$add_session_array))
        {
          array_push($add_session_array, $checkuliqkey);
          $content_data['title']= !empty($this->input->post($keyarr[0].'__title__'.$keyarr[2])) ? trim($this->input->post($keyarr[0].'__title__'.$keyarr[2])) : NULL;
          $content_data['type']= !empty($this->input->post($keyarr[0].'__type__'.$keyarr[2])) ? trim($this->input->post($keyarr[0].'__type__'.$keyarr[2])) : NULL;
          $content_data['content']= !empty($this->input->post($keyarr[0].'__content__'.$keyarr[2])) ? trim($this->input->post($keyarr[0].'__content__'.$keyarr[2])) : NULL;
          $option=array();
          foreach (array_filter($this->input->post($keyarr[0].'__option__'.$keyarr[2])) as $key => $value) 
          {
            $option[$value]=$this->input->post($keyarr[0].'__'.$value.'__'.$keyarr[2]);
          }
          $content_data['option'] = !empty($option) ? json_encode($option) : NULL;
          if(!empty(array_filter($content_data)))
          {
            $content_data['event_id']=$eid;
            $content_data['lang_id']=$keyarr[2];
            $content_data['menu_id']= !empty($this->input->post('menu_id')) ? $this->input->post('menu_id') : NULL;
            $content_data['modules_id']=$keyarr[0];
            $this->Event_template_model->save_language_content_data($content_data);
            unset($content_data);
          }
        }
      }
    }
    unset($add_session_array);
    $this->session->set_flashdata('success', 'Language Content Data Save Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid);      
  }
  public function get_modules_category($eid)
  {
    $htm="";
    if($this->input->post('menu_id')=='1')
    {
      $session_category=$this->Event_template_model->get_all_session_category_for_multilanguage($eid);
      foreach ($session_category as $key => $value) {
        $htm.="<option value='".$value['Id']."'>".$value['category_name']."</option>";
      }
      echo $htm;die;
    }
    else if($this->input->post('menu_id')=='15')
    {
      $survey_category=$this->Event_template_model->get_all_survey_category_for_multilanguage($eid);
      foreach ($survey_category as $key => $value) {
        $htm.="<option value='".$value['survey_id']."'>".$value['survey_name']."</option>";
      }
      echo $htm;die;
    }
    else
    {}
  }
  public function get_modiles_content_data($eid)
  {
    $menu_id=$this->input->post('menu_id');
    $category_id=$this->input->post('module_category');
    $htm="";
    $language_list=$this->Event_template_model->get_language_list_by_event($eid);
    $language_label=$this->Event_template_model->get_language_label_values_by_event($eid);
    $language_content=$this->Event_template_model->get_language_content_by_event($eid);
    $primarr=array_column($language_list,'lang_default');
    if($menu_id=='1')
    {
      $agenda_list=$this->Event_template_model->get_all_session_in_event_for_multilanguage($eid,$category_id);
      foreach ($agenda_list as $skey => $session) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Session Name</th><th>Session Type</th><th>Session Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($session['Heading'])."</td><td>".ucfirst($session['type_name'])."</td><td>".strip_tags($session['description'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$session['Id']."__title__".$value['lang_id']."' value='".$language_content[$session["Id"]."__1__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><input type='text' class='form-control' name='".$session['Id']."__type__".$value['lang_id']."' value='".$language_content[$session["Id"]."__1__".$value["lang_id"]]["type"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$session['Id']."__content__".$value['lang_id']."'>".$language_content[$session["Id"]."__1__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='3')
    {
      $exibitor_list=$this->Event_template_model->get_all_exibitor_in_event_for_multilanguage($eid);
      foreach ($exibitor_list as $skey => $exibitor) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Exhibitor Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".strip_tags($exibitor['Description'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><textarea type='text' class='form-control' name='".$exibitor['Id']."__content__".$value['lang_id']."'>".$language_content[$exibitor["Id"]."__3__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='7')
    {
      $speaker_list=$this->Event_template_model->get_all_speaker_in_event_for_multilanguage($eid);
      foreach ($speaker_list as $skey => $speaker) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Speaker Title</th><th>Speaker Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($speaker['Title'])."</td><td>".strip_tags($speaker['Speaker_desc'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$speaker['Id']."__title__".$value['lang_id']."' value='".$language_content[$speaker["Id"]."__7__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$speaker['Id']."__content__".$value['lang_id']."'>".$language_content[$speaker["Id"]."__7__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='9')
    {
      $presentation_list=$this->Event_template_model->get_all_presentation_in_event_for_multilanguage($eid);
      foreach ($presentation_list as $skey => $presentation) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Presentation Name</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($presentation['Heading'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$presentation['Id']."__title__".$value['lang_id']."' value='".$language_content[$presentation["Id"]."__9__".$value["lang_id"]]["title"]."'></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='10')
    {
      $map_list=$this->Event_template_model->get_all_map_in_event_for_multilanguage($eid);
      foreach ($map_list as $skey => $map) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Map Name</th><th>Map Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($map['Map_title'])."</td><td>".strip_tags($map['Map_desc'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$map['Id']."__title__".$value['lang_id']."' value='".$language_content[$map["Id"]."__10__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$map['Id']."__content__".$value['lang_id']."'>".$language_content[$map["Id"]."__10__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='15')
    {
      $question_list=$this->Event_template_model->get_all_question_in_event_for_multilanguage($eid,$category_id);
      foreach ($question_list as $skey => $question) 
      {
        $option_arr=array_filter(json_decode($question['Option'],true));
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Question</th>";
        foreach ($option_arr as $okey => $option) {
          $htm.="<th>Option</th>"; 
        }
        $htm.="</tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".$question['Question']."</td>";
        foreach ($option_arr as $key => $option) {
          $htm.="<td>".$option."</td>";
        }
        $htm.="</tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$question['Id']."__title__".$value['lang_id']."' value='".$language_content[$question["Id"]."__15__".$value["lang_id"]]["title"]."'></td>";
          $ans_option_arr=json_decode($language_content[$question["Id"]."__15__".$value["lang_id"]]["option"],true);
          foreach ($option_arr as $okey => $option) {
            $htm.="<td><input type='hidden' name='".$question['Id']."__option__".$value['lang_id']."[]' value='".strtolower(str_ireplace(" ","_",$option))."'><input type='text' class='form-control' name='".$question['Id']."__".strtolower(str_ireplace(" ","_",$option))."__".$value['lang_id']."' value='".$ans_option_arr[strtolower(str_ireplace(" ","_",$option))]."'></td>";
          }
          $htm.="</tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='16')
    {
      $document_list=$this->Event_template_model->get_all_document_in_event_for_multilanguage($eid);
      foreach ($document_list as $skey => $document) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Document Title</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($document['title'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$document['id']."__title__".$value['lang_id']."' value='".$language_content[$document["id"]."__16__".$value["lang_id"]]["title"]."'></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='21')
    {
      $cms_list=$this->Event_template_model->get_all_cms_in_event_for_multilanguage($eid);
      foreach ($cms_list as $skey => $cms) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Menu Name</th><th>Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($cms['Menu_name'])."</td><td>".strip_tags($cms['Description'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$cms['Id']."__title__".$value['lang_id']."' value='".$language_content[$cms["Id"]."__21__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$cms['Id']."__content__".$value['lang_id']."'>".$language_content[$cms["Id"]."__21__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='27')
    {
      $notification_list=$this->Event_template_model->get_all_notification_in_event_for_multilanguage($eid);
      foreach ($notification_list as $nkey => $notification) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Notification Title</th><th>Notification Content</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($notification['title'])."</td><td>".strip_tags($notification['content'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$notification['Id']."__title__".$value['lang_id']."' value='".$language_content[$notification["Id"]."__27__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$notification['Id']."__content__".$value['lang_id']."'>".$language_content[$notification["Id"]."__27__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='43')
    {
      $sponsors_list=$this->Event_template_model->get_all_sponsors_in_event_for_multilanguage($eid);
      foreach ($sponsors_list as $skey => $sponsor) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Sponsors Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".strip_tags($sponsor['Description'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><textarea type='text' class='form-control' name='".$sponsor['Id']."__content__".$value['lang_id']."'>".$language_content[$sponsor["Id"]."__43__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
    else if($menu_id=='50')
    {
      $qa_list=$this->Event_template_model->get_all_qa_session_in_event_for_multilanguage($eid);
      foreach ($qa_list as $skey => $qa) {
        $htm.="<table class='table table-striped table-bordered table-hover table-full-width dataTable'><thead><tr><th>Language</th><th>Q&A Session Name</th><th>Q&A Session Description</th></tr></thead>";
        $engprimary= !in_array('1',$primarr) ? '(Primary)' : '';
        $htm.="<tbody><tr><td>English ".$engprimary."</td><td>".ucfirst($qa['Session_name'])."</td><td>".strip_tags($qa['Session_description'])."</td></tr>";
        foreach ($language_list as $key => $value) 
        {
          $htm.="<tr><td>".ucfirst($value['lang_name']);
          if($value['lang_default']=='1'){ 
            $htm.=" (Primary)"; 
          } 
          $htm.="</td><td><input type='text' class='form-control' name='".$qa['Id']."__title__".$value['lang_id']."' value='".$language_content[$qa["Id"]."__50__".$value["lang_id"]]["title"]."'></td>";
          $htm.="<td><textarea type='text' class='form-control' name='".$qa['Id']."__content__".$value['lang_id']."'>".$language_content[$qa["Id"]."__50__".$value["lang_id"]]["content"]."</textarea></td></tr>";
        }
        $htm.="</tbody></table>";
      }
      echo $htm;die;
    }
  }
  public function save_lefthandmenu_image($eid)
  {
    $bannerid=$this->input->post('bannerid');
    $filesupload_number=$this->input->post('filesupload_number');
    $banner_details=$this->Event_model->get_banner_details_by_banner_id($bannerid);

    $img=utf8_decode($this->input->post('image'));
    $filteredData=substr($img, strpos($img, ",")+1); 
    $unencodedData=base64_decode($filteredData);
    $imagename1 = uniqid().'+'.time()."+lefthandmenu_advance_images.png";  
    $filepath = "./assets/user_files/".$imagename1; 
    file_put_contents($filepath, $unencodedData);

    $lefthandmenuadvance_data['Image']=$imagename1;
    $lefthandmenuadvance_data['event_id']=$eid;
    $data['html']='';

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
    {      $ip=$_SERVER['HTTP_CLIENT_IP'];    }    
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
    {      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];    }
    else    
    {      $ip=$_SERVER['REMOTE_ADDR'];    }
    if(count($banner_details) > 0)
    {
      unlink("./assets/user_files/".$banner_details[0]['Image']);
       $lefthandmenuadvance_data['datetime']=date('Y-m-d H:i:s');
       $lefthandmenuadvance_data['log'] = $ip;
      $this->Event_model->update_banner_image_in_event($lefthandmenuadvance_data,$bannerid);
      $data['banner_id']=$bannerid;
    }
    else
    {
      $lefthandmenuadvance_data['image_type']='1';
      $lefthandmenuadvance_data['datetime']=date('Y-m-d H:i:s');
      $lefthandmenuadvance_data['log'] =$ip;
      $banner_id=$this->Event_model->save_banner_image_in_event($lefthandmenuadvance_data);
      $image_url=base_url()."assets/user_files/".$imagename1;
      $htm="<div class='row' id='lefthandmenu_image_contener_div_".$filesupload_number."'><img width='283' style='height: auto;' data-bannerid='".$banner_id."' class='lefthandmenu_map' usemap='#lefthandmenu_mape_".$banner_id."' src='".$image_url."'><map id='lefthandmenu_mape_".$banner_id."' name='lefthandmenu_mape_".$banner_id."'></map></div>";
      $data['banner_id']=$banner_id;
      $data['html']=$htm;
    }
    $data['image_url']=base_url()."assets/user_files/".$imagename1;
    $data['img_name']=$imagename1;
    echo json_encode($data);die;
  }
  public function delete_language($eid,$lang_id)
  {
    $this->Event_template_model->delete_language_from_event($lang_id);
    $this->session->set_flashdata('success', 'Language Delete Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid); 
  }
  public function edit_language($eid)
  {
    $lang_data['lang_name']=$this->input->post('lang_name');
    $lang_data['lang_icon']=$this->input->post('lang_icon');
    $this->Event_template_model->update_language_data($lang_data,$this->input->post('lang_id'));
    $this->session->set_flashdata('success', 'Language Updated Successfully...');
    redirect(base_url().'Event/access_setting/'.$eid); 
  }
  public function add_url_to_banner($id)
  {
      $arrAcc = $this->Event_template_model->getAccname($id); 
      $Subdomain=$this->Event_template_model->get_subdomain($id);    
      $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
      $this->data['event_templates'] = $event_templates;
      $this->data['arrAct'] = $arrAcc;
      $this->data['fun_setting']=$this->Event_model->get_all_fun_setting($event_templates[0]['Id']);
      $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
      $this->data['menu'] = $menu;
      $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);

      $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
      $this->data['category_list']=$category_list;

      $modules_group = $this->Event_template_model->get_all_modules_group($event_templates[0]['Id'],'1');
      $this->data['modules_group']=$modules_group;

      $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
      $this->data['menu_list'] = $menu_list;

      $this->load->model('Cms_model');
      $frontcmsmenu = $this->Cms_model->get_cms_page($id);
      $this->data['frontcmsmenu'] = $frontcmsmenu;
      
      $active_icon=$this->Event_model->get_all_active_icon_list_by_event_id($event_templates[0]['Id']);
      $this->data['active_icon']=$active_icon;
      $inactive_icon=$this->Event_model->get_all_in_active_icon_list_by_event_id($event_templates[0]['Id']);
      $this->data['inactive_icon']=$inactive_icon;
      $all_homeicone=$this->Event_model->get_all_homescreenicon_by_event_id($event_templates[0]['Id']);
      $this->data['all_homeicone']=$all_homeicone;
      $active_iconmenu_ids=array_values(array_filter(array_column($active_icon,'menu_id')));
      $emenu=array_filter(explode(",",$event[0]['checkbox_values']));
      $activemenuid=array_diff($emenu,$active_iconmenu_ids);
      $active_menu=$this->Event_model->get_active_menu_list_by_event($event_templates[0]['Id'],$activemenuid);
      $this->data['active_menu']=$active_menu;
      $active_iconcmsmenu_ids=array_values(array_filter(array_column($active_icon,'cms_id')));
      $active_cms_menu=$this->Event_model->get_all_active_cms_menu_by_event($event_templates[0]['Id'],$active_iconcmsmenu_ids);
      $this->data['active_cms_menu']=$active_cms_menu;
      $active_cms_menu1=$this->Event_model->get_all_active_cms_menu_by_event($event_templates[0]['Id'],NULL);
      $this->data['active_cms_menu1']=$active_cms_menu1;
      $banner_list=$this->Event_model->get_all_banner_list_by_event($event_templates[0]['Id']);
      $this->data['banner_list']=$banner_list;

      $old_banner_url = $this->Event_template_model->get_old_banner_url($event_templates[0]['Id']);
      $this->data['old_banner_url'] = json_decode($old_banner_url,true);

      $lefthandmenu_image_list=$this->Event_model->get_all_left_hand_menu_image_list_by_event($event_templates[0]['Id']);
      $this->data['lefthandmenu_image_list']=$lefthandmenu_image_list;

      $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
      $this->data['cms_feture_menu'] = $cms_feture_menu;
      $user = $this->session->userdata('current_user');
      $roleid = $user[0]->Role_id;
      $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
      $this->data['users_role']=$user_role;
      $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
      $this->data['cms_menu'] = $cmsmenu;
      $this->data['notify_msg'] = $notifiy_msg;
      $this->data['Subdomain'] = $Subdomain;
      $this->data['Email'] = $Email;
      $this->data['speaker_flag']=$flag;
      $user = $this->session->userdata('current_user');
      $usersub=$this->Event_model->get_subscription_type($user[0]->Email);
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $this->data['flag'] = 1;
      $this->data['home_screen_tab']=$this->Event_model->get_home_screen_tabs($id);
      $this->session->set_userdata($data);
      $this->template->write_view('css', 'event/client/add_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data , true);
      $this->template->write_view('content', 'event/client/url_banner', $this->data , true);
      $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
      $this->template->write_view('js', 'event/client/add_js', $this->data , true);
      $this->template->render();
  }
  public function save_banner_url($event_id)
  { 
    $old_banner_url = $this->Event_template_model->get_old_banner_url($event_id);
    if(!empty($old_banner_url))
    {
        $old_banner_url = json_decode($old_banner_url,true);
    }
        $old_banner_url[$this->input->post('image')] = $this->input->post('url');
        $update['banner_url '] = json_encode($old_banner_url);
        $this->Event_template_model->update_banner_url($event_id,$update);
        redirect(base_url().'Event/add_url_to_banner/'.$event_id);
  }
  public function save_access_key($eid)
    {    
        $access_key = $this->input->post('access_key');
        $access_key_check = $this->Event_model->check_access_key_event($eid,$access_key);
        if(count($access_key_check) > 0)
        {
            echo "Error###";
        }
        else
        {   
            $data['access_key'] = $access_key;
            $data['api_sync'] = ($this->input->post('api_sync') == 'true') ? '1':'0';
            $this->Event_model->update_access_key_event($eid,$data);
            echo "Success###";
        }
    }
}       
