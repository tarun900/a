<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Speech extends FrontendController {
    function __construct() {
        $this->data['pagetitle'] = 'Speech';
        $this->data['smalltitle'] = 'Speech Details';
        $this->data['page_edit_title'] = 'edit';
        $this->data['breadcrumb'] = 'Speech';
        parent::__construct($this->data);
        $this->load->model('Speech_model');
        $this->load->model('Speaker_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
        $this->load->library('session');
        $this->load->library('upload');
    }

    public function index()                 
    {           
        $speaker_events = $this->Speaker_model->get_speaker_event();
        $this->data['speaker_events'] = $speaker_events;

        $speech_events = $this->Speech_model->get_speaker_speech($id);
        $this->data['speech_events'] = $speech_events;

        //echo'<pre>'; print_r($speech_events); exit;

        $orid = $this->data['user']->Id;
        if($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission; 
           
        }
        if($this->data['user']->Role_name == 'Client')
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];

            $speeches = $this->Speech_model->get_speech_list($id);
            $this->data['speeches'] = $speeches;
            //echo '<pre>'; print_r($speakers); exit();
        }
        
        $this->template->write_view('css', 'speech/css', $this->data , true);
        $this->template->write_view('content', 'speech/index', $this->data , true);
        if($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        }
        $this->template->write_view('js', 'speech/js', $this->data , true);
        $this->template->render();
    }
        
   public function edit($id = '0')                 
   {   
         $orid = $this->data['user']->Id;

       if($this->data['user']->Role_name == 'Client' || $this->data['user']->Role_name == 'Speaker')
        { 
            $speaker_events = $this->Speech_model->get_speaker_event();
            $this->data['speaker_events'] = $speaker_events;

            $speaker_agenda = $this->Speech_model->get_speaker_agenda();
            $this->data['speaker_agenda'] = $speaker_agenda;

            $speech_events = $this->Speech_model->get_speaker_speech_by_id($id);
            $this->data['speech_events'] = $speech_events;

            //echo '<pre>'; print_r($speech_events); exit();


            $this->session->set_userdata($data);

            if($id == NULL || $id == '')
            {
                redirect('Speech');
            }
            
            if($this->input->post())
            {
                if(!empty($_FILES['Documents']['name'][0]))
                {
                    foreach ($_FILES['Documents']['name'] as $x=>$y)
                    {
                        $y= str_replace(' ', '', $y);
                        
                        if(file_exists("./assets/speech_documents/".$y))
                            $Documents[] = strtotime(date("Y-m-d H:i:s")).'_'.$y;
                        else
                            $Documents[] = $y;
                    }
                    $this->upload->initialize(array(
                        "file_name"     => $Documents,
                        "upload_path"   => "./assets/speech_documents",
                        "allowed_types" => '*'
                    ));
                    
                    if (!$this->upload->do_multi_upload("Documents")) {    
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error',"For Speech Documents, ".$error['error']);
                        //return false;
                    }
                }

                $data['speech_array']['Id'] = $id; 
                $data['speech_array']['Speaker_id'] = $orid;
                $data['speech_array']['Speech_text'] = $this->input->post('Speech_text');
                $data['speech_array']['Event_id'] = $this->input->post('Event_id');
                $data['speech_array']['Agenda_id'] = $this->input->post('Agenda_id');
                $data['speech_array']['Documents'] = $Documents;
                $data['speech_array']['old_documents'] = $this->input->post('old_documents');

                //print_r($data); exit;

                $this->Speech_model->update_speech_event($data);
                $this->session->set_flashdata('speech_data', 'Speech Succesfully', 'Updated');
                redirect("Speech");
            }

            $this->template->write_view('css', 'speech/add_css', $this->data , true);
            $this->template->write_view('content', 'speech/edit', $this->data , true);
            if($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
            }
            $this->template->write_view('js', 'speech/add_js', $this->data , true);
            $this->template->render();       
        }  
    }

    public function add()
    {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $speaker_events = $this->Speech_model->get_speaker_event();
        $this->data['speaker_events'] = $speaker_events;

        $speaker_agenda = $this->Speech_model->get_speaker_agenda();
        $this->data['speaker_agenda'] = $speaker_agenda;

        $speech = $this->Speech_model->get_speech_event($id);
        $this->data['speech'] = $speech;

        if($this->input->post())
        {       

           if(!empty($_FILES['Documents']['name'][0]))
            {
                foreach ($_FILES['Documents']['name'] as $k=>$v)
                {

                    $v= str_replace(' ', '', $v);
                    
                    if(file_exists("./assets/speech_documents/".$v))
                    {
                        $Documents[] = strtotime(date("Y-m-d H:i:s")).'_'.$v;
                    }
                    else
                    {
                        $Documents[] = $v;
                    }
               }
                $this->upload->initialize(array(
                    "file_name"     => $Documents,
                    "upload_path"   => "./assets/speech_documents",
                    "allowed_types" => '*',
                    "max_size" => '2048',
                    "max_width"  => '2048',
                    "max_height"  => '2048'
                ));
                
                if (!$this->upload->do_multi_upload("Documents")) {    
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error',"For Speech Documents, ".$error['error']);
                    //return false;
                }
                
            }

            $data['speech_array']['Speech_text'] = $this->input->post('Speech_text');
            $data['speech_array']['Speaker_id'] =  $this->input->post('Speaker_id');
            $data['speech_array']['Documents'] = $Documents;
            $data['speech_array']['Event_id'] = $this->input->post('Event_id');
            $data['speech_array']['Agenda_id'] = $this->input->post('Agenda_id');


            $speech_id = $this->Speech_model->add_speech($data);
            $this->session->set_flashdata('speech_data', 'Speech Succesfully', 'Added');
            redirect("Speech");
        }

        $this->template->write_view('css', 'speech/add_css', $this->data , true);
        $this->template->write_view('content', 'speech/add', $this->data , true);

        if($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data , true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        }
        $this->template->write_view('js', 'speech/add_js', $this->data , true);
        $this->template->render();
    }
        
    public function delete($id)
    {
        $user = $this->Speech_model->delete_speech($id);
        $this->session->set_flashdata('speech_data', 'Speech Succesfully', 'Deleted');
        redirect("Speech");
    }
}
