<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Publicmessage extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Activate Public Messaging';
          $this->data['smalltitle'] = 'Allow guests to send public messages to all users';
          $this->data['breadcrumb'] = 'Activate Public Messaging';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('13',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               //$eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Activate Public Messaging")
               {
                    $title = "Public Messages";
               }
               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('13',$menu_list))
          {
               $this->load->model('Message_model');
               $this->load->model('Agenda_model');
               $this->load->model('Event_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->model('Event_template_model');
         
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          $user = $this->session->userdata('current_user');

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
          }

          $message = $this->Message_model->get_speaker_value($Subdomain);
          $this->data['message'] = $message;

          $chat_users = $this->Message_model->get_chat_users($Subdomain);
          $this->data['chat_users'] = $chat_users;

//        echo'<pre>'; print_r($chat_users); exit;
          //echo'<pre>'; print_r($message); exit();

          $menudata = $this->Event_model->geteventmenu($id, 13);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;


          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;


          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'message/index', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('js', 'message/js', $this->data, true);
          $this->template->render();
     }

     public function chats($id, $Subdomain = null)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $Sid = $id;
          $this->data['Sid'] = $Sid;
          $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }


               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $this->uri->segment(3),
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic')
               );
               $this->Message_model->send_speaker_message($data1);
               //$Sid = $this->uri->segment(3);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               if (!empty($view_chats1[0]['Recivername']))
               {
                    $username = $view_chats1[0]['Recivername'];
               }
               else
               {
                    $username = "User";
               }

               echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Messages to ' . $username . '<br/></h3>';
               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    echo $value['Sendername'];
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         echo $value['Recivername'];
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago(strtotime($value['Time']));
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";


                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $Subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $value['Senderlogo'] . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                              <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                                             
                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "")
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

//                                        echo "</div>
//                                        <div class='comment_text'>
//                                             " . $cval['comment'] . "
//                                        </div></div>";

                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . $cval['user_name'] . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               exit;
          }
          exit;
     }

     public function chatsspeaker($id, $Subdomain = null)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $Sid = $id;
          $this->data['Sid'] = $Sid;
          $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }


               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $this->uri->segment(3),
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic')
               );
               $this->Message_model->send_speaker_message($data1);
               //$Sid = $this->uri->segment(3);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Question with ' . $user[0]->Firstname . '<br/></h3>';
               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    echo $value['Sendername'];
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         echo $value['Recivername'];
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago(strtotime($value['Time']));
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";


                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $Subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $value['Senderlogo'] . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                              <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                                             
                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "")
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

//                                        echo "</div>
//                                        <div class='comment_text'>
//                                             " . $cval['comment'] . "
//                                        </div></div>";

                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . $cval['user_name'] . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";


                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               exit;
          }
          exit;
     }

     public function chatspublic($Subdomain = null)
     {
          //$Sid = $id;
          //$this->data['Sid'] = $Sid;
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
               }
               else
               {
                    $resiver = NULL;
               }

               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $resiver,
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic')
               );

//               echo "<pre>";
//               print_r($data1);
//               exit;
               $this->Message_model->send_speaker_message($data1);
               //$Sid = $this->uri->segment(3);

               if ($this->input->post('ispublic') == '1')
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id']);
               }
               else
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id']);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
                  <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    echo $value['Sendername'];
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         echo $value['Recivername'];
                         echo "</div>";
                    }
                    echo "</div>";

                    echo "<div class='msg_date'>";
                    echo $this->get_timeago(strtotime($value['Time']));
                    echo "</div>";
                    echo "<div class='msg_message'>";
                    echo $value['Message'];
                    echo "</div>";


                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }

                    echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $Subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $value['Senderlogo'] . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>
                              <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' name='comment'></textarea>
<div class='photo_view_icon'>     
<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />
                                             
                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "")
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }

//                                        echo "</div>
//                                        <div class='comment_text'>
//                                             " . $cval['comment'] . "
//                                        </div></div>";

                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . $cval['user_name'] . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";


                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }

     public function loadmore($Subdomain, $start, $end, $type, $Sid = null)
     {
          if ($type == '1')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id'], $start, $end);
          }
          else if ($type == '0')
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id'], $start, $end);
          }
          else
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid, $msgid = null, $start, $end);
          }

          if (!empty($view_chats1))
          {
               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               foreach ($view_chats1 as $key => $value)
               {
                    if (!empty($value['Id']))
                    {
                         echo "<div class='message_container'>";

                         if ($value['Sender_id'] == $user[0]->Id)
                         {
                              echo "<div class='msg_edit-view-box'>";
                              echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                              echo "</div>";
                              echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                              echo "Delete";
                              echo "</div>";
                              echo "</div>";
                         }

                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "")
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                         }

                         echo "</div>";
                         echo "<div class='msg_fromname'>";
                         echo $value['Sendername'];
                         echo "</div>";
                         if (!empty($value['Receiver_id']))
                         {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              echo $value['Recivername'];
                              echo "</div>";
                         }
                         echo "</div>";

                         echo "<div class='msg_date'>";
                         echo $this->get_timeago(strtotime($value['Time']));
                         echo "</div>";
                         echo "<div class='msg_message'>";
                         echo $value['Message'];
                         echo "</div>";


                         $img_data = json_decode($value['image']);
                         foreach ($img_data as $kimg => $valimg)
                         {
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                              echo "</a>";
                              echo "</div>";
                         }

                         echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                         echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $Subdomain . "/upload_commentimag/" . $Sid . "'>
                                        <div class='comment_message_img'>";
                         if ($value['Senderlogo'] != "")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $value['Senderlogo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>
                              <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' name='comment'></textarea>
     <div class='photo_view_icon'>     
     <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
     <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                                        </ul>
                                        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />

                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                         if (!empty($view_chats1[$key]['comment']))
                         {
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              $i = 0;
                              $flag = false;
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {
                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }

                                   echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                                   if ($cval['Logo'] != "")
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                                   }

                                   //                                        echo "</div>
                                   //                                        <div class='comment_text'>
                                   //                                             " . $cval['comment'] . "
                                   //                                        </div></div>";

                                   echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . $cval['user_name'] . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

                                   if ($cval['image'] != "")
                                   {
                                        $image_comment = json_decode($cval['image']);
                                        echo "<div class='msg_photo'>";
                                        echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                                        echo "</a>";
                                        echo "</div>";
                                   }
                                   if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                                   {
                                        echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                                   }
                                   echo "</div>";


                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }

                                   $i++;
                              }
                         }
                         echo "</div>
                              </div>";
                         echo "</div>";
                    }
               }
               exit;
          }
          else
          {
               echo "";
               exit;
          }
     }

     public function commentadd($id, $Subdomain = NULL)
     {

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          if ($this->input->post())
          {

               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");

               $comment_id = $this->Message_model->add_comment($array_add);
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id, $this->input->post('msg_id'));

               if (!empty($view_chats1[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_chats1[0]['comment'] = array_reverse($view_chats1[0]['comment']);
                    foreach ($view_chats1[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'>  
                  <div class='comment_message_img'>";
                         if ($cval['Logo'] != "")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>
                         <div class='comment_wrapper'>        
                         <div class='comment_username'>
                              " . $cval['user_name'] . "
                         </div>
                         <div class='comment_text'>
                              " . $cval['comment'] . "
                         </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";


                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;
     }

     public function commentaddpublic($Subdomain = NULL, $flag)
     {

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");

               $comment_id = $this->Message_model->add_comment($array_add);

               if ($flag == 1)
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_public_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }
               else
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }

               if (!empty($view_chats1[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_chats1[0]['comment'] = array_reverse($view_chats1[0]['comment']);
                    foreach ($view_chats1[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'>  
                  <div class='comment_message_img'>";
                         if ($cval['Logo'] != "")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>
                         <div class='comment_wrapper'>        
                         <div class='comment_username'>
                              " . $cval['user_name'] . "
                         </div>
                         <div class='comment_text'>
                              " . $cval['comment'] . "
                         </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";


                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;
     }

     public function chats1($id)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $Sid = $id;
          $this->data['Sid'] = $Sid;
          $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $Sid = $id;
               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $this->uri->segment(3),
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'ispublic' => $this->input->post('ispublic')
               );
               $this->Message_model->send_speaker_message($data1);

               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
               $string = '';
               echo '<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with ' . $view_chats1[0]['Recivername'] . '<br/></span>';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               foreach ($view_chats1 as $key => $value)
               {
                    echo '<strong>From :</strong>' . $value['Sendername'];
                    echo '<br/>';
                    echo '<strong>To :</strong>' . $value['Recivername'];
                    echo '<br/>';
                    echo $value['Message'];
                    echo '<br/>';
                    echo $this->get_timeago(strtotime($value['Time']));
                    echo '<br/>';
                    echo '<br/>';
               }

               echo $string;
               exit;
          }

          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('content', 'message/chats', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'message/js', $this->data, true);
          $this->template->render();
     }

     public function readbyuser($Subdomain)
     {
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $data = $this->Message_model->msg_notify_update($dataevents[0]['Id']);
          echo 333;
          exit;
     }

     public function delete($id)
     {
          $chats = $this->Message_model->delete_message($id);
          $this->session->set_flashdata('hangout_data', 'Hangout Succesfully', 'Deleted');
          redirect("Message");
     }

     public function delete_message($id)
     {
          echo "333";
          $chats = $this->Message_model->delete_message($id);
          exit;
     }

     public function delete_comment($id)
     {
          echo "333";
          $chats = $this->Message_model->delete_comment($id);
          exit;
     }

     function get_timeago($ptime)
     {
          $estimate_time = time() - $ptime;

          if ($estimate_time < 1)
          {
               return '1 second ago';
          }

          $condition = array(
                  12 * 30 * 24 * 60 * 60 => 'year',
                  30 * 24 * 60 * 60 => 'month',
                  24 * 60 * 60 => 'day',
                  60 * 60 => 'hour',
                  60 => 'minute',
                  1 => 'second'
          );

          foreach ($condition as $secs => $str)
          {
               $d = $estimate_time / $secs;

               if ($d >= 1)
               {
                    $r = round($d);
                    return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
               }
          }
     }

}
