<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/event_template_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/settings_model');
        include('application/libraries/nativeGcm.php');
    }

    public function headerSettings()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('_token');
        $user = $this->app_login_model->check_token_with_event($token,$event_id);
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $menu = $this->settings_model->getNotesHeader($event_id);
            $menu_list = explode(',', $menu->checkbox_values);
            $menu_settings = (in_array(6, $menu_list)) ? 1 : 0;
            

            $data = array(
                'event' => $user[0],
                'event_feture_product' => $fetureproduct,
                'note_enable' => $menu_settings,
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function notification()
    {   
        $users = $this->settings_model->getAllUsers();
        foreach ($users as $key => $value) 
        {
            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $notification = $this->settings_model->getSceduledNotification($value['Id'],$value['gcm_id'],$event_id);
                if(!empty($notification))
                {
                    foreach ($notification as $key => $noti) 
                    {
                        $obj = new Gcm($event_id);
                        if(!empty($noti['moduleslink']))
                        {
                            $extra['message_type'] = 'cms';
                            $extra['message_id'] = $noti['moduleslink'];
                        }
                        elseif(!empty($noti['custommoduleslink']))
                        {
                            $extra['message_type'] = 'custom_page';
                            $extra['message_id'] = $noti['custommoduleslink'];
                        }
                        $extra['event'] = $this->settings_model->event_name($event_id);
                        if($value['device'] == "Iphone")
                        {
                            $extra['title'] = $noti['title'];
                            $msg =  $noti['content'];
                              
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $msg['title'] =   $noti['title'];
                            $msg['message'] = $noti['content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                        $this->event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                    }
                }
            }
        }
        print_r($result);  
    }

    public function session_notification()
    {
        $users = $this->settings_model->get_session_save_Users();
        $result = [];
        foreach ($users as $key => $value) 
        {
            $date = $this->settings_model->getSessionScehduleTime($value['event_id']);
            if($date < $value['notify_datetime'])
                continue;

            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $this->settings_model->markAsSendSessionNotification($value['user_id'],$value['Id']);

                $obj = new Gcm($event_id);
                $extra['message_type'] = 'cms';
                $extra['message_id'] = '1';
                $extra['event'] = $this->settings_model->event_name($event_id);
                if($value['device'] == "Iphone")
                {
                    $extra['title'] = $value['title'];
                    $msg =  $value['messages'];
                      
                    $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                }
                else
                {
                    $msg['title'] =   $value['title'];
                    $msg['message'] = $value['messages'];
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                } 
                        //$this->event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                }
        }
        print_r($result);  
    }

    public function getAdvertising()
    {
        $event_id   = $this->input->post('event_id');
        $menu_id    = $this->input->post('menu_id');

        if($event_id!='' && $menu_id!='')
        {
            $advertise_data           = $this->settings_model->getAdvertisingData($event_id,$menu_id);
            $advertise_data->H_images = (json_decode($advertise_data->H_images)) ?json_decode($advertise_data->H_images) :[] ;
            $advertise_data->F_images = (json_decode($advertise_data->F_images)) ? json_decode($advertise_data->F_images) : [];
            if($advertise_data)
            {
            $data = array(
                    'success'   => true,
                    'data'      => $advertise_data,
                );
            }
            else
            {
                $data = array(
                    'success'   => true,
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => "Invalid Parameters",
                );
        }
        echo json_encode($data);
    }
    public function fundraisingDonationPayment()
    {
        $total              = $this->input->post('total');
        $stripeToken        = $this->input->post('stripeToken');
        $currency           = $this->input->post('currency');
        $admin_secret_key   = $this->input->post('admin_secret_key'); // sk_test_MQdSDqW91LmspHHYRzZ5FNyk

        if( $total==''  || $stripeToken=='' || $currency=='' || $admin_secret_key=='')
        {
             $data = array(
              'success' => false,
              'message' => "Invalid parameters"
             );
             echo json_encode($data);
             exit;
        }
        else
        {
            require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
            try
            {
                \Stripe\Stripe::setApiKey($admin_secret_key); 
                
                $payment =  \Stripe\Charge::create(
                      array(
                      "amount"      => $total * 100,
                      "currency"    => strtoupper($currency),
                      "source"      => $stripeToken,
                      "description" => "All In The Loop"
                    )
                );
                
                $data1['transaction_id']=$stripeToken;
                if($payment['status']=="succeeded")
                {
                    $data1['order_status']="completed";
                     $data = array(
                      'success'         => true,
                      'message'         => "completed",
                      'transaction_id'  => $data1['transaction_id'],
                    );
                }
                else
                {
                    $data1['order_status']=$payment['status'];  
                    $data = array(
                      'success'         => false,
                      'message'         => "failed",
                      'transaction_id'  => $data1['order_status'],
                    );
                }
                echo json_encode($data);
            }
            catch(Exception $e)
            {
               $data = array(
                      'success' => false,
                      'message' => "Something went wrong.Please try again.",
                      'error'   => $e,
                    );

                echo json_encode($data);
            }
        }
    }
    public function checkVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'DIMS';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkEDSCVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'EDSC';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkNAPECVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'NAPEC';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkJoicoVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Joico';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkLearnVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Learn';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkDufourVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Dufour';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkChnVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'chn';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkGamesforumVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Gamesforum';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkKnectVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Knect';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => $code ?: '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }   
    public function userClickBoard()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $exhibitor_page_id = $this->input->post('exhibitor_page_id');
        $sponser_id = $this->input->post('sponser_id');
        $advertise_id = $this->input->post('advertise_id');
        $menu_id = $this->input->post('menu_id');
        $click_type = $this->input->post('click_type');

        if($user_id!='' && $event_id!='' && $click_type!='')
        {
            switch ($click_type) {
                case 'AD':
                    $where['advert_id'] = $advertise_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->settings_model->hitUserClickBoard($where);

                    $adwhere['event_id'] = $event_id;
                    $adwhere['user_id'] = $user_id;
                    $adwhere['menu_id'] = '5';
                    $adwhere['date'] = date('Y-m-d');
                    $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                break;
                
                case 'SP':
                    $where['sponsor_id'] = $sponser_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->settings_model->hitUserClickBoard($where);
                break;
            
                case 'EX':
                    $where['exhibitor_id'] = $exhibitor_page_id;
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['date'] = date('Y-m-d');
                    $this->settings_model->hitUserClickBoard($where);
                break;

                case 'OT':
                    $adwhere['event_id'] = $event_id;
                    $adwhere['user_id'] = $user_id;
                    $adwhere['menu_id'] = $menu_id;
                    $adwhere['date'] = date('Y-m-d');
                    $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                break;
            }
        }
    }

    public function getAllApps()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $apps = $this->settings_model->getAllApps($device);
            $data = array(
              'success'  => true,
              'data'  => ($apps) ? $apps : [],
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function updateVersionCode()
    {
        $device = $this->input->post('device');
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        if($device!='' && $id!='')
        {
            $device = strtolower($device);
            $where['Id'] = $id;
            $update_data[$device."_code"] =  $code;
            $apps = $this->settings_model->updateVersionCode($update_data,$where);
            $data = array(
              'success'  => true,
              'message'  => "Version updated successfully.",
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function get_o_screen()
    {
        $event_id = $this->input->post('event_id');
        if($event_id!='')
        {
            
            $data = $this->settings_model->get_onboarding_settings($event_id);
            $data = array(
              'success'  => true,
              'code'  => ($data) ? $data : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
}