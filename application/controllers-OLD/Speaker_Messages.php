<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Speaker_Messages extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Moderation Panel';
        // $this->data['smalltitle'] = 'Key People Messages';
        $this->data['page_edit_title'] = 'Messages';
        $this->data['breadcrumb'] = 'Moderation Panel';
        parent::__construct($this->data);
        $this->load->model('Profile_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Event_template_model');
        $this->load->model('Message_model');
        $this->load->model('Qa_model');
        $eventid = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $this->load->library('session');
        $this->load->library('upload');
        include ('application/libraries/nativeGcm.php');

        $user = $this->session->userdata('current_user');
    }
    public function index($id, $qaid = NULL)

    {
        $user = $this->session->userdata('current_user');
        $arrAcc = $this->Event_template_model->getAccname($id);
        $Subdomain = $this->Event_template_model->get_subdomain($id);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $this->data['arrAct'] = $arrAcc;
        $qa_session = $this->Qa_model->get_qa_session_list_by_event($id, null);
        $this->data['qa_session'] = $qa_session;
        $this->data['speaker_data'] = $this->Event_model->get_speaker_id_by_moderator_id($event_templates[0]['Id'], $user[0]->Id);
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
        $this->data['speakers'] = $speakers;
        $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
        $this->data['attendees'] = $attendees;
        $view_chats1 = $this->Message_model->view_hangouts_private_msg_moderator_panel($id, NULL, $qaid);
        $this->data['view_chats1'] = $view_chats1;
        if($user[0]->is_moderator && $event_templates[0]['Id'] == '1511')
        {   
            $this->data['meeting'] = $this->Message_model->getAllMeetingRequestModerator($event_templates[0]['Id'],$user[0]->Id);
        }

        $this->template->write_view('css', 'speaker/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'speaker/Speaker_Messages_view', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'speaker/js', true);
        $this->template->render();
    }
    public function addcomment($eid)

    {
        if (!empty($_FILES['comment_images']['name']))
        {
            $tempname = explode('.', $_FILES['comment_images']['name']);
            $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
            $imag_photos = $tempname_imagename . "." . $tempname[1];
            $this->upload->initialize(array(
                "file_name" => $imag_photos,
                "upload_path" => "./assets/user_files",
                "allowed_types" => '*'
            ));
            if (!$this->upload->do_multi_upload("comment_images"))
            {
                echo "error###" . $this->upload->display_errors();
                die;
            }
        }
        $user = $this->session->userdata('current_user');
        $lid = $user[0]->Id;
        $array_add['comment'] = trim($this->input->post('comment'));
        $array_add['msg_id'] = $this->input->post('msg_id');
        $imagearr = json_encode(array(
            $imag_photos
        ));
        $array_add['image'] = $imagearr;
        $array_add['user_id'] = $lid;
        $array_add['Time'] = date("Y-m-d H:i:s");
        $msg_id = $this->input->post('msg_id');
        $sender_id = $this->Message_model->get_senderid($msg_id);
        $current_date = date('Y/m/d');
        $this->Message_model->add_comment_hit($lid, $current_date, $sender_id);
        $comment_id = $this->Message_model->add_comment($eid, $array_add);
        redirect(base_url() . 'Speaker_Messages/index/' . $eid);
    }
    public function showfullmsg($eid, $mid, $type)

    {
        $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($eid, $mid);
        $imgarr = json_decode($view_chats1[0]['image']);
        $viewmsg = "";
        if ($type == '0')
        {
            if ($view_chats1[0]['Message'] != "");
            {
                $viewmsg = "<div class='col-sm-12'>";
                $viewmsg.= $view_chats1[0]['Message'];
                $viewmsg.= "</div>";
            }
            if (count($imgarr) > 0)
            {
                $viewmsg.= "<div class='col-sm-12'>";
                for ($i = 0; $i < count($imgarr); $i++)
                {
                    $imgpath = base_url() . 'assets/user_files/' . $imgarr[$i];
                    $viewmsg.= "<div style='width:500px; clear: both;display: block;height: auto;padding: 10px 4px;position: relative;'>";
                    $viewmsg.= '<img src="' . $imgpath . '" style="width:100%;height:Auto;" alt="Messages Image"/></div>';
                }
                $viewmsg.= "</div>";
            }
        }
        else
        {
            $viewmsg = $view_chats1[0]['Message'];
        }
        echo $viewmsg;
        die;
    }
    public function get_duplicate_elements($array)

    {
        $res = array();
        $counts = array_count_values($array);
        foreach($counts as $id => $count)
        {
            if ($count > 0)
            {
                $r = array();
                $keys = array_keys($array, $id);
                foreach($keys as $k) $r[$k] = $id;
                $res[] = $r;
            }
        }
        return sizeof($res) > 0 ? $res : false;
    }
    public function forwardmsg($eventid)

    {
        $mid = array_filter($_POST['order_to_forward']);
        $mid = array_intersect_key($mid, array_flip($this->input->post('msg_forward')));
        asort($mid);
        $fin_arr = $this->get_duplicate_elements($mid);
        $rid = explode(",", $_POST['Receiver_id']);
        $user = $this->session->userdata('current_user');
        $lid = $user[0]->Id;
        $data1 = array();
        $pri = 1;
        for ($i = 0; $i < count($fin_arr); $i++)
        {
            $msgforward = array_keys($fin_arr[$i]);
            foreach($msgforward as $key => $value1)
            {
                $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($eventid, $value1);
                if (!empty($view_chats1[0]['msg_creator_id']) && $view_chats1[0]['msg_creator_id'] != '0')
                {
                    $msgc = $view_chats1[0]['msg_creator_id'];
                }
                else
                {
                    $msgc = $lid;
                }
                $data1 = array(
                    'Message' => $view_chats1[0]['Message'],
                    'Sender_id' => $lid,
                    'Receiver_id' => $rid,
                    'Parent' => '0',
                    'Event_id' => $eventid,
                    'Time' => date("Y-m-d H:i:s") ,
                    'image' => $view_chats1[0]['image'],
                    'ispublic' => '0',
                    'priority_no' => $fin_arr[$i][$value1],
                    'msg_creator_id' => $msgc
                );
                $current_date = date('Y/m/d');
                foreach($rid as $key => $value)
                {
                    $for['msg_id'] = $view_chats1[0]['Id'];
                    $for['resiver_id'] = $value;
                    $for['sender_id'] = $lid;
                    $for['event_id'] = $eventid;
                    $for['type'] = '0';
                    $for['isread'] = '0';
                    $for['issent'] = '1';
                    $for['Created_date'] = date("Y-m-d H:i:s");
                    $this->Message_model->forwardmsg($for);
                    $this->Message_model->add_msg_hit($lid, $current_date, $value);
                }
                $this->Message_model->send_grp_speaker_message($data1);
            }
            $pri = $pri + 1;
        }
        echo base_url() . 'speaker_Messages/index/' . $eventid;
        die;
    }
    public function userreplay($eid, $ispublic)

    {
        $user = $this->session->userdata('current_user');
        $lid = $user[0]->Id;
        if (!empty($_FILES['replay_images']['name']))
        {
            $tempname = explode('.', $_FILES['replay_images']['name']);
            $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
            $imag_photos = $tempname_imagename . "." . $tempname[1];
            $this->upload->initialize(array(
                "file_name" => $imag_photos,
                "upload_path" => "./assets/user_files",
                "allowed_types" => '*'
            ));
            if (!$this->upload->do_multi_upload("replay_images"))
            {
                echo "error###" . $this->upload->display_errors();
                die;
            }
        }
        if ($ispublic == '0')
        {
            $array_add['comment'] = trim($this->input->post('reply_content'));
            $array_add['msg_id'] = $this->input->post('msg_id_open_div');
            if (!empty($imag_photos))
            {
                $imagearr = json_encode(array(
                    $imag_photos
                ));
                $array_add['image'] = $imagearr;
            }
            $array_add['user_id'] = $lid;
            $array_add['Time'] = date("Y-m-d H:i:s");
            $msg_id = $this->input->post('msg_id_open_div');
            $sender_id = $this->Message_model->get_senderid($msg_id);
            $current_date = date('Y/m/d');
            $this->Message_model->add_comment_hit($lid, $current_date, $sender_id);
            $comment_id = $this->Message_model->add_comment($eid, $array_add);
        }
        else
        {
            $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($eid, $this->input->post('msg_id_open_div'));
            if (!empty($view_chats1[0]['msg_creator_id']) && $view_chats1[0]['msg_creator_id'] != '0')
            {
                $msgc = $view_chats1[0]['msg_creator_id'];
            }
            else
            {
                $msgc = $lid;
            }
            $data1 = array(
                'Message' => $view_chats1[0]['Message'],
                'Sender_id' => $view_chats1[0]['Sender_id'],
                'Receiver_id' => $rid,
                'Parent' => '0',
                'Event_id' => $eid,
                'Time' => date("Y-m-d H:i:s") ,
                'image' => $view_chats1[0]['image'],
                'ispublic' => '1',
                'msg_creator_id' => $msgc
            );
            $current_date = date('Y/m/d');
            foreach($resiver as $key => $value)
            {
                $this->Message_model->add_msg_hit($lid, $current_date, $value);
            }
            $msg_id = $this->Message_model->send_grp_speaker_message_public_reply($data1);
            $array_add['comment'] = trim($this->input->post('reply_content'));
            $array_add['msg_id'] = $msg_id;
            if (!empty($imag_photos))
            {
                $imagearr = json_encode(array(
                    $imag_photos
                ));
                $array_add['image'] = $imagearr;
            }
            $array_add['user_id'] = $lid;
            $array_add['Time'] = date("Y-m-d H:i:s");
            $sender_id = $this->Message_model->get_senderid($msg_id);
            $current_date = date('Y/m/d');
            $this->Message_model->add_comment_hit($lid, $current_date, $sender_id);
            $comment_id = $this->Message_model->add_comment($eid, $array_add);
        }
        echo "success###" . base_url() . 'speaker_Messages/index/' . $eid;
        die;
    }
    public function savenote($eid)

    {
        $this->Message_model->update_modrate_msg($this->input->post('msg_id') , $this->input->post('mgs_content'));
        $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($eid, $this->input->post('msg_id'));
        $Heading = "Notes By " . $view_chats1[0]['Recivername'];
        $org_id = $this->Event_model->get_org_id_by_event($eid);
        $data = array(
            'User_id' => $view_chats1[0]['Sender_id'],
            'Organisor_id' => $org_id,
            'Event_id' => $eid,
            'Heading' => $Heading,
            'Description' => $this->input->post('notes_content') ,
            'Created_at' => date('Y-m-d H:i:s')
        );
        $this->Event_template_model->add_notes($data);
        echo "success###" . base_url() . 'speaker_Messages/index/' . $eid;
        die;
    }
    public function exportcsv($event_id)

    {
        $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($event_id);
        $filename = "Key_People_Message.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "         ";
        $header[] = "Sender Name";
        $header[] = "Forwarded Name";
        $header[] = "Messages";
        $header[] = "Read";
        $header[] = "Received At";
        $header[] = "Priority Number";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($view_chats1 as $key => $value)
        {
            $data['empty'] = "   ";
            $data['Sendername'] = ucfirst($value['Sendername']);
            $data['forward_name'] = ucfirst($value['forward_name']);
            if (!empty($value['Message']))
            {
                $data['Message'] = $value['Message'];
            }
            else
            {
                $imgnm = json_decode($value['image']);
                $data['Message'] = $imgnm[0];
            }
            if ($value['isread'] == '0')
            {
                $data['isread'] = "Yes";
            }
            else
            {
                $data['isread'] = "No";
            }
            $data['Time'] = $value['Time'];
            $data['priority_no'] = $value['priority_no'];
            fputcsv($fp, $data);
        }
        die;
    }
    public function readmessages($eventid)

    {
        $aff = $this->Message_model->msg_read_update($eventid, $this->input->post('mid'));
        if ($aff > 0)
        {
            echo "success";
            die;
        }
    }
    public function forwardordersave($eventid)

    {
        $mid = array_filter($_POST['order_to_forward']);
        $user = $this->session->userdata('current_user');
        $lid = $user[0]->Id;
        foreach($mid as $key => $value)
        {
            $data['event_id'] = $eventid;
            $data['user_id'] = $lid;
            $data['msg_id'] = $key;
            $data['Priority_no'] = $value;
            $this->Message_model->savepriorityno($data);
        }
        echo base_url() . 'speaker_Messages/index/' . $eventid;
        die;
    }
    public function getuserdata($eventid, $userid)

    {
        $userdata = $this->Profile_model->get_user_detail($userid);
        echo "<div><span>" . $userdata[0]['Firstname'] . '  ' . $userdata[0]['Lastname'] . '</span></div>';
        echo "<p>" . $userdata[0]['Title'] . ' At ' . $userdata[0]['Company_name'] . '</p>';
        die;
    }
    public function fornt_index($id)

    {
        $user = $this->session->userdata('current_user');
        $arrAcc = $this->Event_template_model->getAccname($id);
        $Subdomain = $this->Event_template_model->get_subdomain($id);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $this->data['arrAct'] = $arrAcc;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $this->data['speaker_data'] = $this->Event_model->get_speaker_id_by_moderator_id($event_templates[0]['Id'], $user[0]->Id);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
        $this->data['speakers'] = $speakers;
        $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
        $this->data['attendees'] = $attendees;
        $view_chats1 = $this->Message_model->view_hangouts_private_msg_speaker($id);
        $this->data['view_chats1'] = $view_chats1;
        $this->load->view('speaker/Speaker_Messages_view_fornt', $this->data);
    }
    public function delete_qa_question($eid, $mid)

    {
        $this->Message_model->delete_qa_question_by_messages_id($mid);
        $event = $this->Event_model->get_admin_event($eid);
        $obj = new Gcm();
        $users = $this->Event_model->getAllUsersGCM_id_by_event_id($eid);
        $count = count($users);
        if ($count > 100)
        {
            $limit = 100;
            for ($i = 0; $i < $count; $i++)
            {
                $page_no = $i;
                $start = ($page_no) * $limit;
                $users1 = array_slice($users, $start, $limit);
                foreach($users1 as $key => $value)
                {
                    if ($value['gcm_id'] != '')
                    {
                        $msg = '';
                        $extra['message_type'] = 'QAQusetion';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        if ($value['device'] == "Iphone")
                        {
                            $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                        }
                    }
                }
            }
        }
        else
        {
            // echo "<pre>";print_r($users);die;
            foreach($users as $key => $value)
            {
                if ($value['gcm_id'] != '')
                {
                    $msg = '';
                    $extra['message_type'] = 'QAQusetion';
                    if ($value['device'] == "Iphone")
                    {
                        $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                    }
                    else
                    {
                        $msg['title'] = '';
                        $msg['message'] = '';
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                    }
                }
            }
        }
        redirect(base_url() . 'Speaker_Messages/index/' . $eid);
    }
    public function add_moderator_voites($eid)

    {
        $this->Qa_model->add_qa_voites_by_modetor($this->input->post('voites') , $this->input->post('question_id'));
        echo "Success###";
        die;
    }
    public function ajax_manager($eid)

    {
        $user = $this->session->userdata('current_user');
        $aColumns = array(
            'u.Firstname',
            'u.Lastname',
            'u.Title',
            'u.Company_name',
            'sm.Message',
            'sm.Time',
            'sm.priority_no'
        );
        $sIndexColumn = "sm.id";
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
        {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
        }
        if (isset($_GET['iSortCol_0']))
        {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++)
            {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i]) ] == "true")
                {
                    $sOrder.= $aColumns[intval($_GET['iSortCol_' . $i]) ] . "
                  " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY")
            {
                $sOrder = "";
            }
        }
        if ($_GET['sSearch'] != "")
        {
            $sWhere = " And (";
            for ($i = 0; $i < count($aColumns); $i++)
            {
                $sWhere.= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere.= ")";
        }
        for ($i = 0; $i < count($aColumns); $i++)
        {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
            {
                if ($sWhere == "")
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere.= " AND ";
                }
                $sWhere.= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
            }
        }
        // $sQuery = "SELECT *,concat('selfgenerated','_',self_id) as cus_id,concat(Firstname,' ',Lastname) as client_name from selfgenerated s LeFT join client_details cd ON s.Email=cd.client_email ".$sWhere." GROUP BY s.Email ".$sOrder." ".$sLimit;
        // $sQuery = "SELECT mn.isread, (case when concat(u.Firstname,' ', u.Lastname) Is NULL then 'Anonymous' else concat(u.Firstname, ' ', u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*, u.Logo as Senderlogo, u1.Logo as Reciverlogo, r.Name as SenderRole, r1.Name as ReciverRole, u.Company_name as SenderCompnayname, u.Title as SenderTitle, u.Street as SenderStreet, u.Suburb as SenderSuburb, u.State as SenderState, u.Country as SenderCountry, u.Postcode as SenderPostcode, u.Mobile as SenderMobile, u.Phone_business as SenderPhone_business, u.Isprofile as SenderIsprofile, u1.Company_name as ReciverCompnayname, u1.Street as ReciverStreet, u1.Suburb as ReciverSuburb, u1.State as ReciverState, u1.Country as ReciverCountry, u1.Postcode as ReciverPostcode, u1.Mobile as ReciverMobile, u1.Phone_business as ReciverPhone_business, u1.Isprofile as ReciverIsprofile, sm.priority_no, spn.Priority_no as p_no, concat(oru.Firstname,' ', oru.Lastname) as msg_creator_name, u3.Firstname as org_Firstname, u3.Lastname as org_Lastname, u3.Id as org_receiver_id FROM (speaker_msg sm) LEFT JOIN user u ON u.Id=sm.Sender_id LEFT JOIN relation_event_user ru ON ru.User_id =sm.Sender_id and ru.Event_id='".$eid."' LEFT JOIN role r ON r.Id =ru.Role_id LEFT JOIN user u1 ON u1.Id=sm.Receiver_id LEFT JOIN user u3 ON u3.Id=sm.org_msg_receiver_id LEFT JOIN relation_event_user ru1 ON ru1.User_id =sm.Receiver_id and ru1.Event_id='".$eid."' LEFT JOIN role r1 ON r1.Id =ru1.Role_id LEFT JOIN msg_notifiy mn ON mn.msg_id =sm.Id LEFT JOIN speaker_priority_no spn ON spn.msg_id=sm.Id LEFT JOIN user oru ON oru.Id=sm.msg_creator_id WHERE `mn`.`type` = '0' AND `sm`.`Event_id` = '".$eid."' AND `sm`.`Receiver_id` = '".$user[0]->Id."' AND `sm`.`ispublic` = '0' AND `sm`.`Parent` = '0' ".$sWhere." GROUP BY sm.id  ORDER BY sm.id ".$sLimit;
        $sQuery = "SELECT mn.isread, (case when concat(u.Firstname,' ', u.Lastname) Is NULL then 'Anonymous' else concat(u.Firstname, ' ', u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*,u.Company_name as SenderCompnayname, u1.Company_name as ReciverCompnayname,spn.Priority_no as p_no, concat(oru.Firstname,' ', oru.Lastname) as msg_creator_name, u3.Firstname as org_Firstname, u3.Lastname as org_Lastname, u3.Id as org_receiver_id FROM (speaker_msg sm) LEFT JOIN user u ON u.Id=sm.Sender_id LEFT JOIN relation_event_user ru ON ru.User_id =sm.Sender_id and ru.Event_id='" . $eid . "' LEFT JOIN user u1 ON u1.Id=sm.Receiver_id LEFT JOIN user u3 ON u3.Id=sm.org_msg_receiver_id LEFT JOIN relation_event_user ru1 ON ru1.User_id =sm.Receiver_id and ru1.Event_id='" . $eid . "' LEFT JOIN msg_notifiy mn ON mn.msg_id =sm.Id LEFT JOIN speaker_priority_no spn ON spn.msg_id=sm.Id LEFT JOIN user oru ON oru.Id=sm.msg_creator_id WHERE `mn`.`type` = '0' AND `sm`.`Event_id` = '" . $eid . "' and `mn`.`event_id` = '" . $eid . "' AND `sm`.`Receiver_id` = '" . $user[0]->Id . "' AND `sm`.`ispublic` = '0' AND `sm`.`Parent` = '0'  " . $sWhere . "  GROUP BY sm.id  ORDER BY sm.id " . $sLimit;
        $rResult = $this->db->query($sQuery)->result_array();
        $sQuery1 = "SELECT mn.isread, (case when concat(u.Firstname,' ', u.Lastname) Is NULL then 'Anonymous' else concat(u.Firstname, ' ', u.Lastname) end) as Sendername, u1.Firstname as Recivername, sm.*,u.Company_name as SenderCompnayname, u1.Company_name as ReciverCompnayname,spn.Priority_no as p_no, concat(oru.Firstname,' ', oru.Lastname) as msg_creator_name, u3.Firstname as org_Firstname, u3.Lastname as org_Lastname, u3.Id as org_receiver_id FROM (speaker_msg sm) LEFT JOIN user u ON u.Id=sm.Sender_id LEFT JOIN relation_event_user ru ON ru.User_id =sm.Sender_id and ru.Event_id='" . $eid . "' LEFT JOIN user u1 ON u1.Id=sm.Receiver_id LEFT JOIN user u3 ON u3.Id=sm.org_msg_receiver_id LEFT JOIN relation_event_user ru1 ON ru1.User_id =sm.Receiver_id and ru1.Event_id='" . $eid . "' LEFT JOIN msg_notifiy mn ON mn.msg_id =sm.Id LEFT JOIN speaker_priority_no spn ON spn.msg_id=sm.Id LEFT JOIN user oru ON oru.Id=sm.msg_creator_id WHERE `mn`.`type` = '0' AND `sm`.`Event_id` = '" . $eid . "' AND `mn`.`event_id` = '" . $eid . "' AND `sm`.`Receiver_id` = '" . $user[0]->Id . "' AND `sm`.`ispublic` = '0' AND `sm`.`Parent` = '0'  " . $sWhere . "  GROUP BY sm.id  ORDER BY sm.id";
        $rResultTotal = $this->db->query($sQuery1)->result_array();
        $iTotal = count($rResultTotal);
        $output = array(
            "sEcho" => intval($_GET['sEcho']) ,
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array()
        );
        if (empty($_GET['iDisplayStart']))
        {
            $intCnt = 1;
        }
        else
        {
            $intCnt = $_GET['iDisplayStart'] + 1;
        }
        if (!empty($rResult))
        {
            foreach($rResult as $intKey => $value)
            {
                $this->db->select('mn.resiver_id as forward_id,u.Firstname as forward_name');
                $this->db->from('msg_notifiy mn');
                $this->db->join('user u', 'u.Id=mn.resiver_id', 'left');
                $this->db->where('mn.sender_id', $user[0]->Id);
                $this->db->where('mn.event_id', $eid);
                $this->db->where('mn.msg_id', $value['Id']);
                $query_for = $this->db->get();
                $for = $query_for->result_array();
                $row = array();
                $row[0] = $intCnt;
                $msgforwardhtml = '<input type="checkbox" name="msg_forward[]" value="' . $value['Id'] . '" id="forward_key_people_' . $value['Id'] . '" class=""><label for="forward_key_people_' . $value['Id'] . '"></label>';
                $row[1] = $msgforwardhtml;
                $inputhtml = '<input type="text" style="width:30px; " value="' . $value['p_no'] . '" name="order_to_forward[' . $value['Id'] . ']" id="order_to_forward_' . $value['Id'] . '" />';
                $row[2] = $inputhtml;
                $row[3] = $user[0]->is_moderator == '1' ? ucfirst($value['org_Firstname']) . ' ' . $value['org_Lastname'] : $value['p_no'];
                $row[4] = ucfirst($value['Sendername']);
                $row[5] = ucfirst($value['SenderTitle']);
                $row[6] = ucfirst($value['SenderCompnayname']);
                $row[7] = ucfirst($for[0]['forward_name']);
                if ($value['Message'] != "")
                {
                    $functionname = "showfillmag('" . $value['Id'] . "','0')";
                    $msghtml = '<a href="javascript:void(0);" onclick="' . $functionname . '" data-toggle="modal" data-target="#view_full_messages">' . substr(strip_tags($value['Message']) , 0, 200) . '</a>';
                }
                else
                {
                    $imgnm = json_decode($value['image']);
                    $functionname = 'showfillmag("' . $value['Id'] . '")';
                    $msghtml = '<a href="javascript:void(0);" onclick="' . $functionname . '" data-toggle="modal" data-target="#view_full_messages"><img src="' . base_url() . "assets/user_files/" . $imgnm[0] . '" alt="Messages Images" style=" border-radius: 100%; height: 40px;width: 40px;"></a>';
                }
                $row[8] = $msghtml;
                $funname = 'readmsg("' . $value["Id"] . '")';
                if ($value['isread'] == '0')
                {
                    $strcheck = "checked='checked'";
                }
                else
                {
                    $strcheck = "";
                }
                $readhtml = '<input type="checkbox" ' . $strcheck . ' name="msg_read" onchange="' . $funname . '" class="" id="key_people_' . $value['Id'] . '"><label for="key_people_' . $value['Id'] . '"></label>';
                $row[9] = $readhtml;
                if (!empty($for[0]['forward_id']))
                {
                    $foreardstr = "checked='checked'";
                }
                else
                {
                    $foreardstr = "";
                }
                $forwardhtml = '<input type="checkbox" disabled="disabled" name="msg_forward" class="" id="forward_people_' . $for[0]['forward_id'] . '" ' . $foreardstr . '><label for="forward_people_' . $for[0]['forward_id'] . '"></label>';
                $row[10] = $forwardhtml;
                $row[11] = $value['Time'];
                $row[12] = $value['priority_no'];
                $fnm = "showfillmag('" . $value['Id'] . "','1')";
                $actionhtml = '<button class="btn btn-success" type="button" data-toggle="modal" data-target="#open_model_div" onclick="' . $fnm . '">Open</button>';
                $row[13] = $actionhtml;
                $output['aaData'][] = $row;
                $intCnt++;
            }
        }
        echo json_encode($output);
    }
}
?>