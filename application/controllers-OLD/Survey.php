<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Survey extends FrontendController
{
    function __construct()
    {
        $this->data['pagetitle'] = 'Polls & Feedback';
        $this->data['smalltitle'] = 'Design multiple, flexible surveys for attendees to complete';
        $this->data['breadcrumb'] = 'Polls & Feedback';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('15', $module))
        {
            redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            // $eventid = $user[0]->Event_id;
            $rolename = $roledata[0]->Name;
            $cnt = 1;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Polls & Feedback")
            {
                $title = "Surveys";
            }
            $cnt = $this->Agenda_model->check_auth("Surveys", $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('15', $menu_list))
        {
            $this->load->model('Survey_model');
            $this->load->model('Agenda_model');
            $this->load->model('Event_model');
            $this->load->model('Setting_model');
            $this->load->model('Profile_model');
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('Forbidden');
        }
    }

    public function index($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event_id = $user[0]->event_id;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey_category = $this->Survey_model->get_all_surveys_category_list_by_event($id);
        $this->data['survey_category'] = $survey_category;
        $menudata = $this->Event_model->geteventmenu($id, 15);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/surveylist', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function check_survey_cateory($eventid)
    {
        $survey_name = $this->input->post('survey_name');
        $sid = $this->input->post('sid');
        $getcate = $this->Survey_model->get_surveys_category_by_name($survey_name, $eventid, $sid);
        if (count($getcate) > 0)
        {
            echo "error###Surveys Name already exist. Please choose another Surveys Name.";
            die;
        }
        else
        {
            echo "success###";
            die;
        }
    }

    public function add_survey($eid)
    {
        $Event_id = $eid;
        if ($this->input->post())
        {
            $sid = $this->input->post('survey_id');
            $data['survey_name'] = $this->input->post('survey_name');
            $getcate = $this->Survey_model->get_surveys_category_by_name($data['survey_name'], $Event_id, $sid);
            if (count($getcate) > 0)
            {
                $this->session->set_flashdata('survey_error', 'Surveys Name already exist. Please choose another Surveys Name.');
                redirect("Survey/question_index/" . $Event_id);
            }
            else
            {
                if (empty($sid))
                {
                    $data['created_date'] = date("Y-m-d H:i:s");
                    $category_id = $this->Survey_model->add_survey_category($data, $Event_id);
                }
                else
                {
                    $category_id = $this->Survey_model->update_survey_category($data, $sid);
                }
            }
            $this->session->set_flashdata('survey_success', 'Surveys Name Save Successfully');
            redirect("Survey/question_index/" . $Event_id . '/' . $category_id);
        }
    }

    public function delete_survey($eid, $sid)
    {
        $this->Survey_model->delete_survey_category_by_id($sid);
        $this->session->set_flashdata('survey_success', 'Surveys Deleted Successfully...');
        redirect("Survey/index/" . $eid);
    }

    public function change_hide_surveys_status($eid, $sid)
    {
        $effctrow = $this->Survey_model->change_surveys_hide_status_by_surveys_id($eid, $sid);
        echo $effctrow;
        die;
    }

    public function question_index($id, $sid = NULL)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event_id = $user[0]->event_id;
        $org = $user[0]->Organisor_id;
        $this->data['org'] = $org;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey_category = $this->Survey_model->get_edit_survey_category_by_id($id, $sid);
        $this->data['survey_category'] = $survey_category;
        $survey_user = $this->Event_model->get_survey_users($id);
        $this->data['survey_user'] = $survey_user;
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $Surveys = $this->Survey_model->get_survey_list($id, $sid);
        $this->data['Surveys'] = $Surveys;
        $survey_screens = $this->Survey_model->get_survey_screens($id, $sid);
        $this->data['survey_screens'] = $survey_screens;
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        $survey_answer_chart = $this->Event_model->get_survey_answer_chart($Event_id, NULL, $sid);
        $this->data['survey_answer_chart'] = $survey_answer_chart;
        $menudata = $this->Event_model->geteventmenu($id, 15);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->data['sid'] = $sid;
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function add($id, $sid)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        if ($this->input->post())
        {
            $data['survey_array']['Organisor_id'] = $logged_in_user_id;
            $data['survey_array']['Event_id'] = $Event_id;
            $data['survey_array']['Question'] = $this->input->post('Question');
            $data['survey_array']['Question_type'] = $this->input->post('Question_type');
            $data['survey_array']['Option'] = json_encode(array_values($this->input->post('Option')));
            $data['survey_array']['Answer'] = $this->input->post('Answer');
            $survey_id = $this->Survey_model->add_survey($data, $sid);
            $this->session->set_flashdata('survey_data', 'Added');
            redirect("Survey/question_index/" . $id . "/" . $sid);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function add_welcome_screen($id, $sid)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['Event_id'] = $id;
            $data['surveys_id'] = $sid;
            $data['welcome_data'] = $this->input->post('welcome_data');
            $survey_id = $this->Survey_model->add_welcome_screen($data);
            $this->session->set_flashdata('survey_wl_screen_data', 'Added');
            redirect("Survey/question_index/" . $id . "/" . $sid);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/add_welcome_screen', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function add_thanks_screen($id, $sid)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['Event_id'] = $id;
            $data['surveys_id'] = $sid;
            $data['thanku_data'] = $this->input->post('thanku_data');
            $survey_id = $this->Survey_model->add_welcome_screen($data);
            $this->session->set_flashdata('survey_thk_screen_data', 'Added');
            redirect("Survey/question_index/" . $id . "/" . $sid);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/add_thankyou_screen', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function thanku_edit_screen($event_id, $id, $sid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $survey_screens = $this->Survey_model->get_survey_screens($event_id, $sid);
        $this->data['survey_screens'] = $survey_screens;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($event_id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['thanku_data'] = $this->input->post('thanku_data');
            $this->Survey_model->update_screen($id, $sid, $data);
            $this->session->set_flashdata('survey_thk_screen_data', 'Updated');
            redirect("Survey/question_index/" . $event_id . "/" . $sid);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/edit_thanku_screen', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function welcome_edit_screen($event_id, $id, $sid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $survey_screens = $this->Survey_model->get_survey_screens($event_id, $sid);
        $this->data['survey_screens'] = $survey_screens;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($event_id);
        $this->data['event'] = $event[0];
        if ($this->input->post())
        {
            $data['welcome_data'] = $this->input->post('welcome_data');
            $this->Survey_model->update_screen($id, $sid, $data);
            $this->session->set_flashdata('survey_wl_screen_data', 'Updated');
            redirect("Survey/question_index/" . $event_id . "/" . $sid);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/edit_welcome_screen', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function delete_welcome($id, $qid, $sid)
    {
        $survey = $this->Survey_model->delete_welcome_screen($qid, $sid);
        $this->session->set_flashdata('survey_wl_screen_data', 'Deleted');
        redirect("Survey/question_index/" . $id . "/" . $sid);
    }

    public function delete_thankyou($id, $qid, $sid)
    {
        $survey = $this->Survey_model->delete_thanks_screen($qid, $sid);
        $this->session->set_flashdata('survey_thk_screen_data', 'Deleted');
        redirect("Survey/question_index/" . $id . "/" . $sid);
    }

    public function welcome_content($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        if ($this->input->post())
        {
            $survey_id = $this->Survey_model->add_survey($data);
            $this->session->set_flashdata('survey_data', 'Added');
            redirect("Survey/index/" . $id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/welcome_page', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }

    public function thanku_content($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        if ($this->input->post())
        {
            $survey_id = $this->Survey_model->add_survey($data);
            $this->session->set_flashdata('survey_data', 'Added');
            redirect("Survey/index/" . $id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/thanks_page', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    
    public function edit($id, $qid, $sid)
    {
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        if ($this->data['page_edit_title'] = 'edit')
        {
            if ($id == NULL || $id == '')
            {
                redirect('Survey');
            }
            if ($this->input->post())
            {
                $data['survey_array']['Organisor_id'] = $logged_in_user_id;
                $data['survey_array']['Event_id'] = $Event_id;
                $data['survey_array']['Question'] = $this->input->post('Question');
                $data['survey_array']['Question_type'] = $this->input->post('Question_type');
                $data['survey_array']['Option'] = json_encode(array_values($this->input->post('Option')));
                $data['survey_array']['Answer'] = $this->input->post('Answer');
                $this->Survey_model->update_survey($data);
                $this->session->set_flashdata('survey_data', 'Updated');
                redirect("Survey/question_index/" . $Event_id . "/" . $sid);
            }
        }
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/edit', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    public function view($id, $Question_id)

    {
        
        $Event_id = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $survey_answer = $this->Event_model->get_survey_answer($id);
        $this->data['survey_answer'] = $survey_answer;
        $survey_answer_chart = $this->Event_model->get_survey_answer_chart();
        $this->data['survey_answer_chart'] = $survey_answer_chart;
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('content', 'survey/view', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    public function user_export($eventid, $user_id)

    {
        $survey_answer = $this->Event_model->get_userwise_survey_answer_export($user_id);
        $filename = "user_survery_details.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Question";
        $header[] = "Answer";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array(
            "",
            "",
            "Users Wise Surveys",
            "",
            ""
        ));
        fputcsv($fp, array());
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($survey_answer as $key => $value)
        {
            fputcsv($fp, $value);
        }
        exit();
    }
    public function export($event_id, $Question_id)

    {
        $survey_answer = $this->Event_model->get_survey_answer_export($event_id, $Question_id);
        $filename = "user_click_details.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Firstname";
        $header[] = "Secondname";
        $header[] = "Email";
        $header[] = "Company_name";
        $header[] = "Question";
        $header[] = "Answer";
        $header[] = "ExtratInfo";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array(
            "",
            "",
            "",
            "",
            "",
            "Question Wise Surveys",
            "",
            "",
            "",
            ""
        ));
        fputcsv($fp, array());
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($survey_answer as $key => $value)
        {
            fputcsv($fp, $value);
        }
        exit();
    }
    public function delete($Event_id, $id, $sid)

    {
        $survey_list = $this->Survey_model->get_survey_list($id);
        $this->data['survey_list'] = $survey_list[0];
        $survey = $this->Survey_model->delete_survey($id);
        $this->session->set_flashdata('survey_data', 'Deleted');
        redirect("Survey/question_index/" . $Event_id . "/" . $sid);
    }
    public function hide_survey($id)

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $survey = $this->Survey_model->get_edit_survey_list($id);
        $this->data['survey'] = $survey;
        if ($this->input->post())
        {
            if ($this->input->post('hide_survey') == '1')
            {
                $hide_survey = '1';
            }
            else
            {
                $hide_survey = '0';
            }
            $survey_id = $this->Event_model->update_survey($id, $hide_survey);
            $this->session->set_flashdata('survey_data', 'Updated');
            redirect("Survey/index/" . $id);
        }
        /* $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'survey/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();*/
    }
    public function change_hide_question_status($eid, $qid)

    {
        $effctrow = $this->Survey_model->change_question_hide_status_by_question_id($eid, $qid);
        echo $effctrow;
        die;
    }
    public function exprot_surveys($eid, $sid = NULL)

    {
        $this->load->model('Attendee_model');
        $survey = $this->Survey_model->get_exporting_surveys_by_event($eid, $sid);
        $customkey = $this->Attendee_model->get_all_custom_modules($eid);
        $filename = "surveys_answer_user.csv";
        $fp = fopen('php://output', 'w');
        if (empty($sid))
        {
            $header[] = 'Survey name';
        }
        $header[] = 'Question';
        $header[] = 'Answer';
        $header[] = 'First Name';
        $header[] = 'Last Name';
        $header[] = 'Title';
        $header[] = 'Company';
        $header[] = 'Email';
        foreach($customkey as $key => $value)
        {
            $header[] = ucfirst($value['column_name']);
        }
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($survey as $key => $value)
        {
            if (empty($sid))
            {
                $data['Survey name'] = ucfirst($value['survey_name']);
            }
            $data['Question'] = $value['Question'];
            $data['Answer'] = $value['Answer'];
            $data['Firstname'] = ucfirst($value['Firstname']);
            $data['Lastname'] = ucfirst($value['Lastname']);
            $data['Title'] = $value['Title'];
            $data['Company_name'] = $value['Company_name'];
            $data['Email'] = $value['Email'];
            $custom = json_decode($value['extra_column'], TRUE);
            foreach($customkey as $key => $value)
            {
                $keynm = $value['column_name'];
                if (!empty($custom[$keynm]))
                {
                    $data[$key] = $custom[$keynm];
                }
                else
                {
                    $data[$key] = '';
                }
            }
            fputcsv($fp, $data);
            unset($data);
        }
    }
    public function add_relese_date($eid, $sid)

    {
        if ($this->input->post())
        {
            $sid = $this->input->post('survey_id');
            $data['relese_datetime'] = $this->input->post('relese_datetime');
            $category_id = $this->Survey_model->update_survey_category($data, $sid);
            $this->session->set_flashdata('survey_success', 'Surveys Release Time Save Successfully');
            redirect("Survey/question_index/" . $eid . '/' . $sid);
        }
    }
    public function add_clone_survey($eid)
    {    
        extract($this->input->post()); 
        if(is_numeric($count) && !empty($survey_id))
        {
           $this->Survey_model->add_clone_entry($eid,$survey_id,$count);     
        }
        redirect("Survey/index/".$eid);
    }
    public function downloadQR($event_id,$survey_id,$name)
    {      
        $this->load->helper('download');
        $this->load->library('Ciqrcode');
        $params['data'] = $event_id.'-'.$survey_id;
        $params['level'] = 'M';
        $params['size'] = 20;
        $params['savename'] = './assets/'.$name.'.png';
        $this->ciqrcode->generate($params);
        $file = './assets/'.$name.'.png';
        $info = new SplFileInfo($file);
        $contentDisposition = 'attachment';
        
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: image/jpeg');
            // change inline to attachment if you want to download it instead
            header('Content-Disposition: '.$contentDisposition.'; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
        else echo "Not a file";
    }
}