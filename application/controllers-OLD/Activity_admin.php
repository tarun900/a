<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_admin extends FrontendController {

    function __construct() {
        $this->data['pagetitle'] = 'Activities';
        $this->data['smalltitle'] = 'Activity Details';
        $this->data['breadcrumb'] = 'Activities';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Activity_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        
        $user = $this->session->userdata('current_user');
        $eventid=$this->uri->segment(3);

         $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $eventmodule=$this->Event_model->geteventmodulues($eventid);
        $module=json_decode($eventmodule[0]['module_list']);

        if(!in_array('45',$module))
        {
          echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
        }
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);

        if(!empty($roledata))
        {
          $roleid = $roledata[0]->Role_id;
          $rolename = $roledata[0]->Name;
          $cnt = 0;
          $req_mod = ucfirst('activity');
          $cnt = $this->Agenda_model->check_auth($req_mod, $roleid, $rolename,$eventid);

        }
        else
        {
          $cnt=0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
          $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('45',$menu_list))
        {
          $this->load->model('Setting_model');
          $this->load->model('Map_model');
          $this->load->model('Speaker_model');
          $this->load->model('Event_template_model');
          $this->load->model('Profile_model');
          $this->load->library('upload');
          $roles = $this->Event_model->get_menu_list($roleid,$eventid);
          $this->data['roles'] = $roles;
        }
        else
        {
          echo '<script>window.location.href="'.base_url().'forbidden/'.'"</script>'; 
        }
    }
    public function index($id) 
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
		$user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
		$user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
       
        if ($this->data['user']->Role_name == 'User') 
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
		if ($this->data['user']->Role_name == 'Client')
		{
		   $event = $this->Event_model->get_admin_event($id);
		   $this->data['event'] = $event[0];
		}
        $menudata=$this->Event_model->geteventmenu($id,45);
        $menu_toal_data=$this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

        $arrPermision = $this->Activity_model->getPermission($id);
        $this->data['arrPermission'] = $arrPermision[0];

        $show_on_homescreen = $this->Activity_model->getshow_on_homescreen($id);
        $this->data['show_on_homescreen'] = $show_on_homescreen;

        $activity_share_icon_setting = $this->Activity_model->get_activity_share_icon_setting($id);
        $this->data['activity_share_icon_setting'] = $activity_share_icon_setting;

        $socialwall_heading = $this->Activity_model->get_socialwallData($id);
        $this->data['socialwall_heading'] = $socialwall_heading;

        $public_message_feeds = array();
        $photo_feed = array();
        $check_in_feed = array();
        $rating_feed = array();
        $user_feed = array();
        $arrauctionbid = $arrSaveSession = $arrcomments = array();
        if($arrPermision[0]['public']==1)
        {
          $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($id,$user[0]->Id);

        }
        if($arrPermision[0]['photo']==1)
        {
          $photo_feed = $this->Activity_model->getPhotoFeeds($id,$user[0]->Id);
        }
        if($arrPermision[0]['check_in']==1)
        {
          $check_in_feed = $this->Activity_model->getCheckInFeeds($id,$user[0]->Id);
        }
        if($arrPermision[0]['rating']==1)
        {
          $rating_feed = $this->Activity_model->getRatingFeeds($id,$user[0]->Id);
        }
        if($arrPermision[0]['update_profile']==1)
        {
          $user_feed = $this->Activity_model->getUserFeeds($id,$user[0]->Id);
        }
        if($arrPermision[0]['show_session']==1)
        {
          $arrSaveSession = $this->Activity_model->getSaveSessionFeed($id,$user[0]->Id);
        }
        if($arrPermision[0]['auction_bids']==1)
        {
          $arrauctionbid = $this->Activity_model->getauctionbidsFeed($id,$user[0]->Id);

        }
        if($arrPermision[0]['comments']==1)
        {
          $arrcomments = $this->Activity_model->getcommentfeed($id,$user[0]->Id);

        }
        $activity_feeds = $this->Activity_model->getActivityFeeds($id,$user[0]->Id);
        $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
        $this->data['activity_data']=$activity_data;

        $this->data['Surveys'] = $this->Activity_model->getSurvey($id);
        $this->data['advert'] = $this->Activity_model->getAdvert($id);

        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'activity_admin/css', $this->data, true);
        $this->template->write_view('content', 'activity_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User') 
        {

            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } 
        else 
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'activity_admin/js', $this->data, true);
        $this->template->render();
    }
    public function save_permission($eid)
    {
        if($this->input->post())
        {
            $arrData = $this->input->post();
            $intEvenId = $this->input->post('event_id');
            unset($arrData['event_id']);
            if(empty($arrData['photo'])) {$arrData['photo']='0';}
            if(empty($arrData['public'])) {$arrData['public']='0';}
            if(empty($arrData['check_in'])) {$arrData['check_in']='0';}
            if(empty($arrData['rating'])) {$arrData['rating']='0';}
            if(empty($arrData['comments'])) {$arrData['comments']='0';}
            if(empty($arrData['update_profile'])) {$arrData['update_profile']='0';}
            if(empty($arrData['show_session'])) {$arrData['show_session']='0';}
            if(empty($arrData['auction_bids'])) {$arrData['auction_bids']='0';}
            if($intEvenId!='' && !empty($arrData))
            {
                $this->Activity_model->update_acvity_permission($intEvenId,$arrData);
                redirect(base_url().'Activity_admin/index/'.$eid);
            }
        }
    }

    public function show_on_homescreen($eid)
    {
        if($this->input->post())
        {
            $arrData = $this->input->post();
            $intEvenId = $this->input->post('event_id');
            unset($arrData['event_id']);
            if(empty($arrData['activity_feed_on_homescreen'])) {$arrData['activity_feed_on_homescreen']='0';}
            if($intEvenId!='' && !empty($arrData))
            {
                $this->Activity_model->update_show_on_homescreen($intEvenId,$arrData);
                redirect(base_url().'Activity_admin/index/'.$eid);
            }
        }
    }

    public function activity_share_icon_setting($eid)
    {
        if($this->input->post())
        {
            $arrData = $this->input->post();
            $intEvenId = $this->input->post('event_id');
            unset($arrData['event_id']);
            if(empty($arrData['activity_share_icon_setting'])) {$arrData['activity_share_icon_setting']='0';}
            if($intEvenId!='' && !empty($arrData))
            {
                $this->Activity_model->update_activity_share_icon_setting($intEvenId,$arrData);
                redirect(base_url().'Activity_admin/index/'.$eid);
            }
        }
    }

    public function save_social_feed($eid)
    {
        $arrData['hashtag']=$this->input->post('hashtags');
        $arrData['fbpage_name']=$this->input->post('fbpage_name');
        $arrData['fb_access_token']=$this->input->post('fb_access_token');
        $this->Activity_model->update_acvity_permission($eid,$arrData);
        redirect(base_url().'Activity_admin/index/'.$eid); 
    }

    
    public function save_social_wall($eid)
    {
        $arrData['socialwall_heading']=$this->input->post('socialwall_heading');
    
        $this->Activity_model->update_socialwall($eid,$arrData);
        redirect(base_url().'Activity_admin/index/'.$eid); 
    }

    public function delete_activityfeed($eid,$id,$activity_no)
    {
        $this->Activity_model->remove_activityfeed($eid,$id,$activity_no);
        redirect(base_url().'Activity_admin/index/'.$eid);
    }

    public function add($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        
        if ($this->input->post())
        {   
            $insert['Event_id'] = $id;
            $insert['Question'] = $this->input->post('Question');
            $insert['Question_type'] = '1';
            $insert['Option'] = json_encode($this->input->post('Option'));
            $insert['show_question'] = $this->input->post('show_question') ? '0' : '1';
            if(!$this->input->post('publish_result'))
            {
                $insert['publish_date'] = date('Y-m-d H:i:s',strtotime($this->input->post('publish_date')));
                $insert['publish_result'] = '0';
            }
            else
            {
                $insert['publish_result'] = '1';
            }
            $this->Activity_model->addSurveyQuestion($insert);
            redirect(base_url().'Activity_admin/index/'.$id.'#survey');
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'activity_admin/add_survey', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    public function chageQuestionStatus($event_id,$id)
    {   
        $status = $this->Activity_model->changeQuestionStatus($id);
        echo '1';
    }
    public function edit($event_id,$id)
    {
        $Event_id = $event_id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $event_id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($event_id);
        $this->data['event'] = $event[0];
        $this->data['survey'] = $this->Activity_model->getSurvey($event_id,$id);


        if ($this->input->post())
        {   
            $where['Event_id'] = $event_id;
            $where['Id'] = $id;
            $update['Question'] = $this->input->post('Question');
            $update['Option'] = json_encode($this->input->post('Option'));
            $update['show_question'] = $this->input->post('show_question') ? '0' : '1';
            if($this->input->post('publish_result'))
            {
                $update['publish_result'] = '1';
            }
            else
            {
                $update['publish_date'] = date('Y-m-d H:i:s',strtotime($this->input->post('publish_date')));
                $update['publish_result'] = '0';
            }
            $this->Activity_model->updateSurveyQuestion($where,$update);
            redirect(base_url().'Activity_admin/index/'.$event_id.'#survey');
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'activity_admin/edit_survey', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    public function delete($event_id,$id)
    {
        $this->Activity_model->deleteSurvey($id);
        redirect(base_url().'Activity_admin/index/'.$event_id.'#survey');
    }
    public function export($event_id)
    {
        $export = $this->Activity_model->export($event_id);
    }
    public function addSponsoredPost($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        
        if ($this->input->post())
        {   
            if (!file_exists('./assets/activity_advert/'.$id)) {
            mkdir('./assets/activity_advert/'.$id, 0777, true);
            }
            if(!empty($_FILES['image']['name']))
            {
                $imgname = explode('.', $_FILES['image']['name']);
                $tempname = str_replace(" ","_",$imgname);
                $tempname_imagename = $id.'-activity_advert-'.date('Y-m-d-H:i:s');
                $images_file = $tempname_imagename . "." . $tempname[1];

                $this->upload->initialize(array(
                      "file_name" => $images_file,
                      "upload_path" => "./assets/activity_advert/".$id,
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000',
                      "max_width" => '700',
                      "max_height" => '100'
                ));
                $this->upload->do_upload("image");
                $insert['image'] = $images_file;
            }
            $insert['event_id'] = $id;
            $insert['title'] = $this->input->post('title');
            $insert['url'] = $this->input->post('url');
            $this->Activity_model->addSponsoredPost($insert);
            redirect(base_url().'Activity_admin/index/'.$id.'#sponsored_post');
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'activity_admin/add_advert', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    
    public function editSponsoredPost($id,$survey_id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['survey'] = $this->Activity_model->getAdvert($id,$survey_id);

        if ($this->input->post())
        {   
            if (!file_exists('./assets/activity_advert/'.$id)) {
            mkdir('./assets/activity_advert/'.$id, 0777, true);
            }
            if(!empty($_FILES['image']['name']))
            {
                $imgname = explode('.', $_FILES['image']['name']);
                $tempname = str_replace(" ","_",$imgname);
                $tempname_imagename = $id.'-activity_advert-'.date('Y-m-d-H:i:s');
                $images_file = $tempname_imagename . "." . $tempname[1];

                $this->upload->initialize(array(
                      "file_name" => $images_file,
                      "upload_path" => "./assets/activity_advert/".$id,
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000',
                      "max_width" => '700',
                      "max_height" => '100'
                ));
                $this->upload->do_upload("image");
                $update['image'] = $images_file;
                $this->Activity_model->deleteAdvertImage($id,$survey_id);
            }
            $update['title'] = $this->input->post('title');
            $update['url'] = $this->input->post('url');
            $where['id'] = $survey_id;
            $where['event_id'] = $id;
            $this->Activity_model->editSponsoredPost($where,$update);
            redirect(base_url().'Activity_admin/index/'.$id.'#sponsored_post');
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'activity_admin/edit_advert', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
    }
    public function deleteAdvert($event_id,$survey_id)
    {
        $this->Activity_model->deleteAdvert($survey_id,$event_id);
        redirect(base_url().'Activity_admin/index/'.$event_id.'#sponsored_post');
    }
}
