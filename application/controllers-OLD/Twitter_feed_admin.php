<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Twitter_feed_admin extends FrontendController
{
    function __construct()
    {
     	$this->data['pagetitle'] = 'Add Hashtags';
	    $this->data['smalltitle'] = 'Add a Twitter Hashtag or @ to your App to show related posts on Twitter.';
	    $this->data['breadcrumb'] = 'Hashtags';
	    $this->data['page_edit_title'] = 'edit Hashtags';
	    parent::__construct($this->data);
	    $this->load->model('Event_model');
	    $this->load->model('Agenda_model');
	    $this->load->model('Twitter_feed_model');
        $user = $this->session->userdata('current_user');
	    $eventid=$this->uri->segment(3);

        $this->load->database();
      $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
      $user_events =  array_filter(array_column($user_events,'Event_id'));
      if(!in_array($eventid,$user_events))
      {
         redirect('Forbidden');
      }

	
	    $event_templates = $this->Event_model->view_event_by_id($eventid);
	    $eventmodule=$this->Event_model->geteventmodulues($eventid);
	    $module=json_decode($eventmodule[0]['module_list']);
	    if(!in_array('44',$module))
	    {
	        echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
	    }  
	    $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if(!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $cnt = $this->Agenda_model->check_auth("twitter_feed", $roleid, $rolename,$eventid);
        }
        else
        {
            $cnt=0;
        }  
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('44',$menu_list))
        {
            $this->load->model('Agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Map_model');
            $this->load->model('Speaker_model');
            $this->load->model('Event_template_model');
            $this->load->model('Profile_model');
            $this->load->library('upload');
            $roles = $this->Event_model->get_menu_list($roleid,$eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
        }
    }
    public function index($id)
    {
     	$event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $hashtags_list=$this->Twitter_feed_model->get_hashtags_list_by_event_id($id);
        $this->data['hashtags_list']=$hashtags_list;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role; 
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 44);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'twitter_feed_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'twitter_feed_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'twitter_feed_admin/js', true);
        $this->template->render();
    }
    public function add($id)
    {
    	if($this->input->post())
    	{
    		$hash['event_id']=$id;
    		$hash['hashtags']=$this->input->post('hashtags');
    		$hash['status']=$this->input->post('hashtags_status');
    		$hash['date']=date("Y-m-d h:i:s");
    		$this->Twitter_feed_model->add_hashtags($hash);
    		$this->session->set_flashdata('feed_data','Hashtags Add');
    		redirect(base_url().'Twitter_feed_admin/index/'.$id);
    	}
   		$event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0]; 	
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $this->template->write_view('css', 'twitter_feed_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'twitter_feed_admin/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'twitter_feed_admin/js', true);
        $this->template->render();
    }
    public function checkvalidhashtags($id)
    {
    	$check=$this->Twitter_feed_model->checkhashtagsvalid($id,$this->input->post('hashtags'),$this->input->post('hashtags_id'));
    	if($check=="TRUE")
    	{
    		echo "success###";
    	}
    	else
    	{
    		echo "error###This Hashtags Already Exists Plase try Another Hashtags.";
    	}
    	die;
    }
    public function edit($id,$hid)
    {
    	if($this->input->post())
    	{
    		$hash['hashtags']=$this->input->post('hashtags');
    		$hash['status']=$this->input->post('hashtags_status');
    		$hash['event_id']=$id;
    		$hash['date']=date("Y-m-d h:i:s");
    		$this->Twitter_feed_model->update_hashtags($hid,$hash);
    		$this->session->set_flashdata('feed_data','Hashtags Update');
    		redirect(base_url().'Twitter_feed_admin/index/'.$id);
    	}
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0]; 
   		$hashtags = $this->Twitter_feed_model->getHashtagsById($hid,$id);      
   		$this->data['hashtags'] = $hashtags[0]; 	
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id; 
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);

        $this->data['users_role']=$user_role;
        $this->template->write_view('css', 'twitter_feed_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'twitter_feed_admin/edit', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'twitter_feed_admin/js', true);
        $this->template->render();
    }
    public function delete($eid,$hid){

   	    $this->Twitter_feed_model->deleteHashtags($hid); 	
   	    $this->session->set_flashdata('feed_data','Hashtags Delete');
    	redirect(base_url().'Twitter_feed_admin/index/'.$eid);
    }

 }
 ?>