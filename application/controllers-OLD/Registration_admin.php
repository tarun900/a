<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Registration_admin extends FrontendController
{
    function __construct()
    {
        $this->data['pagetitle'] = 'Event Registration';
        $this->data['smalltitle'] = 'Collect registration before and during your event.';
        $this->data['breadcrumb'] = 'Registration';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Registration_admin_model');
        $this->load->library('upload');
        $this->load->library('email');
        $user = $this->session->userdata('current_user');
        $eventid=$this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
        $user_events =  array_filter(array_column($user_events,'Event_id'));
        if(!in_array($eventid,$user_events))
        {
           redirect('Forbidden');
        }
        $eventmodule=$this->Event_model->geteventmodulues($eventid);
        $module=json_decode($eventmodule[0]['module_list']);
        if(!in_array('55',$module))
        {
            redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if(!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Registration")
            {
               $title = "Registration";
            }
            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
        }
        else
        {
            $cnt=0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('55',$menu_list))
        {
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid,$eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            redirect('Forbidden');
        }
    }
    public function index($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }

        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $menudata = $this->Event_model->geteventmenu($id, 55);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

        $this->data['screen_data']=$this->Registration_admin_model->get_registration_screen_data($id);
        $this->data['screen_data']['show_in_front'] = $menudata[0]->show_in_front;
        $this->data['stages']=$this->Registration_admin_model->get_stages($id);
        $this->data['questions'] = $this->Registration_admin_model->get_questions($id);
        $this->data['all_disc'] = $this->Registration_admin_model->get_all_disc($id);
        $this->data['badges_data'] = $this->Registration_admin_model->get_badge_data($id);
        $this->data['tickets'] = $this->Registration_admin_model->get_tickets($id);

        if(!empty($this->input->post()))
        {   
            if(empty($this->input->post('company_banner_crop_data_textbox')))
            {
                if (!empty($_FILES['banner_Images']['name']))
                {
                     $tempname = explode('.', $_FILES['banner_Images']['name']);
                     $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                     $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                     $images_file1 = $tempname_imagename . "." . $tempname[1];
                     $this->upload->initialize(array(
                          "file_name" => $images_file1,
                          "upload_path" => "./assets/user_files",
                          "allowed_types" => '*',
                          "max_size" => '0',
                          "max_width" => '0',
                          "max_height" => '0'
                     ));
                     if (!$this->upload->do_multi_upload("banner_Images"))
                     {
                          $this->session->set_flashdata('attendee_registration_error_data', $this->upload->display_errors());
                     }
                     $registration_data['banner_image']=$images_file1;
                }
            }
            else
            {
                $img=$_POST['company_banner_crop_data_textbox'];
                $filteredData=substr($img, strpos($img, ",")+1); 
                $unencodedData=base64_decode($filteredData);
                $images_file1 = strtotime(date("Y-m-d H:i:s"))."_crop_banner_image.png"; 
                $filepath = "./assets/user_files/".$images_file1; 
                file_put_contents($filepath, $unencodedData); 
                $registration_data['banner_image']=$images_file1;
            }

            if (!empty($_FILES['background_image']['name']))
            {
                $tempname = explode('.', $_FILES['background_image']['name']);
                $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $background_image = $tempname_imagename . "." . $tempname[1];
                $this->upload->initialize(array(
                     "file_name" => $background_image,
                     "upload_path" => "./assets/user_files",
                     "allowed_types" => '*',
                     "max_size" => '0',
                     "max_width" => '0',
                     "max_height" => '0'
                ));
                if (!$this->upload->do_multi_upload("background_image"))
                {
                     $this->session->set_flashdata('attendee_registration_error_data', $this->upload->display_errors());
                }
                $registration_data['background_image']=$background_image;
            }

            $registration_data['event_id']=$id;
            $registration_data['screen_content']=$this->input->post('screen_content');
            $registration_data['theme_color']=$this->input->post('theme_color');
            if($this->input->post('send_qr_code')=='1')
            {
                $registration_data['send_qr_code']='1';
            }
            else
            {
                $registration_data['send_qr_code']='0';
            }
            if($this->input->post('send_pdf')=='1')
            {
                $registration_data['send_pdf']='1';
            }
            else
            {
                $registration_data['send_pdf']='0';
            }
            if($this->input->post('email_notify_admin')=='1')
            {
                $registration_data['email_notify_admin']='1';
            }
            else
            {
                $registration_data['email_notify_admin']='0';
            }
            if($this->input->post('one_agenda_each_date')=='1')
            {
                $registration_data['one_agenda_each_date']='1';
            }
            else
            {
                $registration_data['one_agenda_each_date']='0';
            }
            if($this->input->post('show_in_front') == '1')
            {
                $this->Registration_admin_model->reg_show_in_front($id,'1');
            }
            else
            {
                $this->Registration_admin_model->reg_show_in_front($id,'0');
            }
            $this->Registration_admin_model->save_registration_screen_data($registration_data);
            $this->session->set_flashdata('attendee_view_data','Attendee Registration Screen Updated Successfully.');
            redirect(base_url().'Registration_admin/index/'.$id);
        }

        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'registration_admin/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'registration_admin/js', $this->data, true);
        $this->template->render();
    }
    public function add_question($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['stages'] = $this->Registration_admin_model->get_stages($id);
        $this->data['tickets'] = $this->Registration_admin_model->get_tickets($id);

        if ($this->input->post())
        {   
            $data['type'] = $this->input->post('question_type');
            $data['question'] = trim($this->input->post('Question'));
            $data['stage_id'] = !empty($this->input->post('stage_id')) ? $this->input->post('stage_id') : NULL;
            $data['isrequired'] = !empty($this->input->post('isrequired')) ? '1' : '0';
            $data['update_alert'] = !empty($this->input->post('update_alert')) ? '1' : '0';
            $data['event_id'] = $id;
            $data['created_at'] = date('Y-m-d H:i:s');
            if($this->input->post('question_type') == '4')
            {   
                $data['product_name'] = $this->input->post('product_name');
                $data['product_price'] = $this->input->post('product_price');
            }
            elseif($this->input->post('question_type') == '5')
            {
                $data['ticket_id'] = $this->input->post('t-id');
            }
            elseif($this->input->post('question_type') == '6')
            {
                $data['option'] = json_encode($this->input->post('date'));
            }
            else
            {
                $data['option'] = json_encode($this->input->post('Option'));
            }
            $this->Registration_admin_model->add_question($data);
            $this->session->set_flashdata('attendee_view_data','Question Added Successfully');
            redirect(base_url().'Registration_admin/index/'.$id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'registration_admin/add_question', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'registration_admin/js', $this->data, true);
        $this->template->render();
    }
    public function edit_question($id,$q_id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['stages'] = $this->Registration_admin_model->get_stages($id);
        $question=$this->Registration_admin_model->get_question($q_id,$id);
        $this->data['question'] = $question;
        if(!empty($this->data['question']['option']))
            $this->data['question']['option'] = json_decode($this->data['question']['option'],true);
        $this->data['tickets'] = $this->Registration_admin_model->get_tickets($id);
        if ($this->input->post())
        {   
            $data['type'] = $this->input->post('question_type');
            $data['question'] = $this->input->post('Question');
            $data['stage_id'] = !empty($this->input->post('stage_id')) ? $this->input->post('stage_id') : NULL;
            $data['isrequired'] = !empty($this->input->post('isrequired')) ? '1' : '0';
            $data['update_alert'] = !empty($this->input->post('update_alert')) ? '1' : '0';
            
            if($this->input->post('question_type') == '4')
            {   
                $data['product_name'] = $this->input->post('product_name');
                $data['product_price'] = $this->input->post('product_price');
                $data['option'] = NULL;
            }
            else
            {   
                if($this->input->post('question_type') == '5')
                {
                    $data['ticket_id'] = $this->input->post('t-id');
                    $data['option'] = NULL;
                }
                elseif($this->input->post('question_type') == '6')
                {
                    $data['option'] = json_encode($this->input->post('date'));
                }
                elseif($this->input->post('question_type') != '3')
                {
                    $data['option'] =  json_encode($this->input->post('Option'));
                }
                $data['product_name'] = NULL;
                $data['product_name'] = NULL;
            }

            $this->Registration_admin_model->update_custom_column($id,$question['question'],$data['question']);

            $this->Registration_admin_model->update_question($data,$q_id);

            $arroption=!empty($this->input->post('Option')) ? $this->input->post('Option') : array();
            $older_option=!empty($this->input->post('older_option')) ? $this->input->post('older_option') : array();

            foreach ($older_option as $key => $value) 
            {
                if($value != $arroption[$key])
                {
                    $this->Registration_admin_model->update_option_in_skiplogic($q_id,$value,$arroption[$key]);
                }
            }

            $delete_option=array_diff(array_filter(json_decode($question['option'],true)),$older_option);
            foreach ($delete_option as $key => $value) 
            {
                $this->Registration_admin_model->delete_skip_login_option($q_id,$value);
            }

            $this->session->set_flashdata('attendee_view_data','Question Updated Successfully');
            redirect(base_url().'Registration_admin/index/'.$id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'registration_admin/edit_question', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'registration_admin/js', $this->data, true);
        $this->template->render();
    }
    public function delete_question($event_id,$id)
    {   
        $where['event_id'] = $event_id;
        $where['id'] = $id;
        $this->Registration_admin_model->delete_question($where);
        $this->session->set_flashdata('attendee_view_data','Question Removed Successfully');    
        redirect(base_url().'Registration_admin/index/'.$event_id);
    }
    public function edit_stage($id,$stage_id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['stage'] = $this->Registration_admin_model->get_stage($stage_id,$id);
        $this->data['questions'] = $this->Registration_admin_model->get_questions_for_add($id);

        if ($this->input->post())
        {   
            $where['event_id'] = $id;
            $where['id'] = $stage_id;
            $update['stage_name'] = $this->input->post('stage_name');
            $this->Registration_admin_model->update_stage($where,$update);
            $this->session->set_flashdata('attendee_view_data','Stage Updated Successfully');    
            redirect(base_url().'Registration_admin/index/'.$id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'registration_admin/add_stage', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'registration_admin/js', $this->data, true);
        $this->template->render();
    }
    public function add_html_to_stage($id,$stage_id)
    {
        $where['event_id'] = $id;
        $where['id'] = $stage_id;
        $update['stage_html'] = $this->input->post('stage_html');
        $this->Registration_admin_model->update_stage($where,$update);
        $this->session->set_flashdata('attendee_view_data','Stage Updated Successfully');    
        redirect(base_url().'Registration_admin/edit_stage/'.$id.'/'.$stage_id);
    }
    public function add_question_to_stage($id,$stage_id)
    {
        if(!empty($this->input->post()))
        {   
            $q_id  = $this->input->post('q_id');
            foreach ($q_id as $key => $value)
            {
                $where['id'] = $value;
                $update['stage_id'] = $stage_id;
                $this->Registration_admin_model->add_question_to_stage($where,$update);
            }
        }
        else
        {
           $where['stage_id'] = $stage_id;
           $update['stage_id'] = NULL;
           $this->Registration_admin_model->add_question_to_stage($where,$update);
        }
        $this->session->set_flashdata('attendee_view_data','Question Added To Stage Successfully');    
        redirect(base_url().'Registration_admin/edit_stage/'.$id.'/'.$stage_id);
    }
    public function delete_register_banner_image($id,$rsid)
    {
        $rsdata['event_id']=$id;
        $rsdata['banner_image']=NULL;
        $this->Registration_admin_model->save_registration_screen_data($rsdata);
        redirect(base_url().'Registration_admin/index/'.$id.'/'.$rsid);
    }
    public function delete_register_back_image($id,$rsid)
    {
        $rsdata['event_id']=$id;
        $rsdata['background_image']=NULL;
        $this->Registration_admin_model->save_registration_screen_data($rsdata);
        redirect(base_url().'Registration_admin/index/'.$id.'/'.$rsid);
    }
    public function add_stage($id)
    {
        $insert['event_id'] = $id;
        $insert['stage_name'] = $this->input->post('stage_name');
        $this->Registration_admin_model->save_stage($insert);
        $this->session->set_flashdata('attendee_view_data','Stage Added Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);
    }
    public function delete_stage($event_id,$id)
    {
        $where['event_id'] = $event_id;
        $where['id'] = $id;
        $this->Registration_admin_model->delete_stage($where);
        $this->session->set_flashdata('attendee_view_data','Stage Removed Successfully');    
        redirect(base_url().'Registration_admin/index/'.$event_id);
    }
    public function get_logic_html($event_id,$stage_id)
    {   
        $stages=$this->Registration_admin_model->get_stage_for_redirect_list($event_id,$stage_id);
        $question = $this->Registration_admin_model->get_question($this->input->post('id'));
        if(!empty($question['option']))
            $question['option'] = json_decode($question['option'],true);

        $skip_logic = $this->Registration_admin_model->get_skip_logic($question['id']);
        $skiplogic_option=array_column($skip_logic,'option');
        $skiplogic_rqid=array_column($skip_logic,'redriect_stage_id');
        $skiplogic_data=array_combine($skiplogic_option, $skiplogic_rqid);
        
        foreach ($question['option'] as $key => $value)
        {   
            $html .= "<input type=hidden name=q_id id=q_id value=".$question['id'].">";
            $html .= "<div class='row stage_opt'>";
            $html .= "<div class='col-sm-4'>".$value."</div>";
            $html .= "<div class='col-sm-4'> Redirect to </div>";
            $html .= "<div class='col-sm-4'><select required name=".$question['id']."__".str_replace(' ','_',$value)." class='form-control' style='margin-bottom: 10px;'>";
            $html .= "<option value=''>Select Stage</option>";
            foreach ($stages as $k => $v)
            {   
                $selected = ($v['id'] == $skiplogic_data[$value]) ? 'selected' : '';
                $html .= "<option ".$selected." value=".$v['id'].">".$v['stage_name']."</option>";
            }
            $html .= "</select></div>";
            $html .= "</div>";
        }
        echo $html;exit();
    }
    public function add_skip_logic($event_id)
    {   
        if(!empty($this->input->post()))
        {   
            $data = $this->input->post();
            $id = $this->input->post('q_id');
            $question = $this->Registration_admin_model->get_question($id);
            if(!empty($question['option']))
                $question['option'] = json_decode($question['option'],true);
            foreach ($question['option'] as $key => $value)
            {   
                $insert['que_id'] = $id;
                $insert['option'] = $value;
                $insert['redriect_stage_id'] = $data[$id.'__'.str_replace(' ','_',$value)];
                $insert['event_id'] = $event_id;
                $this->Registration_admin_model->add_skip_logic($insert);
            }
        }
        $this->session->set_flashdata('attendee_view_data','Logic Added Successfully');    
        redirect(base_url().'registration_admin/index/'.$event_id);
    }
    public function reorder_stage($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['stages'] = $this->Registration_admin_model->get_stages($id);
        
        if($this->input->post())
        {   
            $data = $this->input->post('stage_id');
            foreach ($data as $key => $value)
            {
                $update['sort_order'] = $key;
                $where['id'] = $value;
                $this->Registration_admin_model->update_stage($where,$update);
            }
            $this->session->set_flashdata('attendee_view_data','Stage Ordering Successfully');    
            redirect(base_url().'Registration_admin/index/'.$id);
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'registration_admin/order_question_views', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'registration_admin/js', $this->data, true);
        $this->template->render();
    }
    public function save_payment_processor($id)
    {
        $update['show_payment_screen'] = $this->input->post('show_payment_screen')=='1' ? '1' : '0';
        $update['payment_type'] = $this->input->post('payment_type');
        $update['stripe_secret_key'] = $this->input->post('stripe_secret_key');
        $update['stripe_secret_key'] = $this->input->post('stripe_secret_key');
        $update['stripe_public_key'] = $this->input->post('stripe_public_key');
        $update['authorise_transaction_key'] = $this->input->post('authorise_transaction_key');
        $update['authorise_api_name'] = $this->input->post('authorise_api_name');
        $update['paypal_api_username'] = $this->input->post('paypal_api_username');
        $update['paypal_api_password'] = $this->input->post('paypal_api_password');
        $update['paypal_api_signature'] = $this->input->post('paypal_api_signature');
        $update['stripe_payment_currency'] = $this->input->post('stripe_payment_currency');
        $update['show_pay_by_invoice'] = !empty($this->input->post('show_pay_by_invoice')) ? '1' : '0';
        $where['event_id'] = $id;
        $this->Registration_admin_model->update_screen_data($where,$update);
        $this->session->set_flashdata('attendee_view_data','Payment Deatils Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);
    }
    public function add_discount($id)
    {
        $data = $this->input->post();
        $data = array_filter($data);
        foreach ($data['product_ids'] as $key => $value)
        {   
            $where['que_id'] = $value;
            $update['code']   = $data[$value.'__code'];
            $update['price']  = $data[$value.'__price'];
            $update['code_type'] = '0';
            $update['status'] = array_key_exists($value.'__status',$data) ? '1' : '0';
            $update['event_id'] = $id;
            if(!empty($update['code']))
            $this->Registration_admin_model->add_discount($where,$update);
        }
        unset($where);
        unset($update);        
        $update['code'] = $data['all__code'];
        $update['percentage'] = $data['all__percentage'];
        $update['status'] = array_key_exists('all__status',$data) ? '1' : '0';
        $update['code_type'] = '1';
        $update['event_id'] = $id;
        if(!empty($update['code']))
        $this->Registration_admin_model->add_discount(NULL,$update);
        $this->session->set_flashdata('attendee_view_data','Discount Deatils Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);
    }
    public function save_badges_vales($eid)
    {      
          $badgesdata=$this->Registration_admin_model->get_badge_data($eid);
          if (!empty($_FILES['badges_logo_images']['name']))
          {
               $tempname = explode('.', $_FILES['badges_logo_images']['name']);
               $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
               $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
               $filesname = $tempname_imagename . "." . $tempname[1];
               $this->upload->initialize(array(
                    "file_name" => $filesname,
                    "upload_path" => "./assets/badges_files",
                    "allowed_types" => '*',
                    "max_size" => '0',
                    "max_width" => '0',
                    "max_height" => '0'
               ));
               if (!$this->upload->do_multi_upload("badges_logo_images"))
               {
                    $this->session->set_flashdata('attendee_registration_error_data', $this->upload->display_errors());
               }
               $badges_data['badges_logo_images']=$filesname;
               if(!empty($badgesdata['badges_logo_images']))
               {
                    unlink('./assets/badges_files/'.$badgesdata['badges_logo_images']);
               }
          }
          $badges_data['send_email'] = !empty($this->input->post('send_email')) ? '1' : '0';
          $badges_data['sender_name']=$this->input->post('sender_name');
          $badges_data['subject']=$this->input->post('subject');
          $badges_data['email_body']=$this->input->post('email_body');
          $badges_data['code_type']=$this->input->post('code_type');
          $where['event_id'] = $eid;
          $this->Registration_admin_model->save_badge_data($where,$badges_data);
          redirect("Registration_admin/index/".$eid);
    }
    public function add_ticket($id)
    {
        $insert['event_id'] = $id;
        $insert['ticket_name'] = $this->input->post('ticket_name');
        $insert['created_at']  = date('Y-m-d H:i:s');
        $this->Registration_admin_model->save_ticket($insert);
        $this->session->set_flashdata('attendee_view_data','Ticket Added Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);
    }
    public function delete_ticket($id,$ticket_id)
    {   
        $where['id'] = $ticket_id;
        $this->Registration_admin_model->delete_ticket($where);
        $this->session->set_flashdata('attendee_view_data','Ticket Removed Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);
    }
    public function update_ticket($id)
    {
        $where['id'] = $this->input->post('t-id');
        $update['ticket_name'] = $this->input->post('ticket_name');
        $this->Registration_admin_model->update_ticket($where,$update);
        $this->session->set_flashdata('attendee_view_data','Ticket Updated Successfully');    
        redirect(base_url().'Registration_admin/index/'.$id);   
    }
    public function save_thank_you_screen($id)
    {
        $registration_data['event_id']=$id;
        $registration_data['thank_you_content']=$this->input->post('thank_you_content');    
        $this->Registration_admin_model->save_registration_screen_data($registration_data);
        $this->session->set_flashdata('attendee_view_data','Thank You Screen Save Successfully.');
        redirect(base_url().'Registration_admin/index/'.$id);
    }

}