<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller
{
    function __construct()
    {
        $this->data['pagetitle'] = 'Events';
        $this->data['smalltitle'] = 'Events Details';
        $this->data['breadcrumb'] = 'Events Template';
        parent::__construct($this->data);
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Event_model');
        $this->load->model('Cms_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Profile_model');
        $this->load->model('User_model');
        $this->load->model('Login_model');
        $this->load->model('Attendee_model');
        $this->load->model('Notes_admin_model');
        $this->load->model('Add_attendee_model');
        $this->load->model('Message_model');
        $this->load->library('upload');
        $this->load->library('formloader');
        $this->load->library('formloader1');
        $this->load->library('custom_formloader');
        $this->load->model('Agenda_model');
        $this->load->library('email');
        $eventname = $this->Event_model->get_all_event_name();
        /*echo strtolower($this->uri->segment(3));
        echo "<pre>";
        print_r($eventname);die;*/
        if(($this->uri->segment(2) != "reset_password"))
        {
            if (in_array($this->uri->segment(3) , $eventname))
            {
                $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                $this->data['notes_list'] = $notes_list;
                $user = $this->session->userdata('current_user');
                $logged_in_user_id = $user[0]->Id;
                if ($user != '')
                {
                    $parameters = $this->uri->uri_to_assoc(1);
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                    // echo $cnt;die;
                    if ($cnt == 1)
                    {
                        $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                        $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                        $event_type = $event_templates[0]['Event_type'];
                        if ($event_type == 3)
                        {
                            $this->session->unset_userdata('current_user');
                            $this->session->unset_userdata('invalid_cred');
                            $this->session->sess_destroy();
                        }
                        else
                        {
                            $parameters = $this->uri->uri_to_assoc(1);
                            $Subdomain = $this->uri->segment(3);
                            $acc_name = $this->uri->segment(2);
                            redirect(base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain);
                            // echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
                        }
                    }
                }
            }
            else
            {
                $parameters = $this->uri->uri_to_assoc(1);
                $Subdomain = $this->uri->segment(3);
                $flag = 1;
                redirect(base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag);
                // echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
            }
        }
              
    }
    public function add_advertise_hit($acc_name, $Subdomain, $aid)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $logged_in_user_id = $user[0]->Id;
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($logged_in_user_id, $current_date, 5, $event_templates[0]['Id']);
            $this->Event_model->add_advert_hit($event_templates[0]['Id'], $aid, $logged_in_user_id, $current_date);
        }
    }
    public function passwordset($acc_name, $Subdomain = NULL, $Email = NULL, $code = NULL)
    {
        $email = base64_decode(urldecode($Email));
        $code = base64_decode(urldecode($code));
        // echo $code; exit;
        $cnt = $this->Setting_model->check_authe_url($email, $code);
        // echo  $cnt; exit();
        if ($cnt >= 1)
        {
            if (!$Subdomain)
            {
                redirect('Login');
            }
            else
            {
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $this->data['event_templates'] = $event_templates;
                $event_id = $event_templates[0]['Id'];
                $fb_login_data = $this->Event_model->getraisedsetting($event_id);
                $this->data['fb_login_data'] = $fb_login_data;
                $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_id);
                $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
                $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
                $this->data['menu'] = $menu;
                $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
                $this->data['notify_msg'] = $notifiy_msg;
                $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
                if ($this->session->userdata('checklogin') != '1' && $this->session->flashdata('invlaidlogin_data') == "")
                {
                    if ($fundraisingenbled[0]['fundraising_enbled'] == 1)
                    {
                        if ($event_templates[0]['Event_type'] != 3 && $this->session->userdata('current_user') == "")
                        {
                        }
                        else
                        {
                            redirect(base_url() . 'fundraising/' . $acc_name . '/' . $Subdomain);
                        }
                    }
                }
                $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
                $this->data['notisetting'] = $notificationsetting;
                $user = $this->session->userdata('current_user');
                $roleid = $user[0]->Role_id;
                $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
                $this->data['menu_list'] = $menu_list;
                $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1, $acc_name);
                $this->data['cms_feture_menu'] = $cms_feture_menu;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;
                $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
                $this->data['speakers'] = $speakers;
                $notes_list = $this->Event_template_model->get_notes($Subdomain);
                $this->data['notes_list'] = $notes_list;
                $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
                $this->data['agendas'] = $agendas;
                $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
                $this->data['attendees'] = $attendees;
                $map = $this->Event_template_model->get_map($Subdomain);
                $this->data['map'] = $map;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['Email'] = $Email;
                $this->data['speaker_flag'] = $flag;
                $res = $this->Event_template_model->get_singup_forms($event_id);
                $this->data['form_data'] = $res;
                $countrylist = $this->Profile_model->countrylist();
                $this->data['countrylist'] = $countrylist;
                $arrCustomCols = $this->Event_template_model->getCustomColumns($event_id);
                $this->data['arrCustomCols'] = $arrCustomCols;
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $user = $this->session->userdata('current_user');
                if ($event_templates[0]['Event_type'] == '1')
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/passwordset', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '2')
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '3')
                {
                    if ($this->session->userdata('checklogin') == '1' || $this->session->flashdata('invlaidlogin_data') != "")
                    {
                        $this->session->unset_userdata('checklogin');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->session->unset_userdata('acc_name');
                        $acc['acc_name'] = $acc_name;
                        $this->session->set_userdata($acc);
                        $this->template->write_view('content', 'events/index', $this->data, true);
                    }
                }
                else
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index', $this->data, true);
                    }
                }
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
            }
        }
        else
        {
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            if ($event_templates[0]['Event_type'] == '3')
            {
                $this->session->set_userdata('checklogin', '1');
            }
            redirect('App/' . $acc_name . '/' . $Subdomain);
            /*$flag=2;
            redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);*/
            // echo "link is expaired";
        }
    }
    /*public function index($acc_name, $Subdomain = NULL, $Email = NULL, $code = NULL)
    {   
        // if($this->uri->segment('3') == 'ArabHealthArabHealth')
        // {
        //     error_reporting(E_ALL);
        // }
         if($this->session->userdata('checkreset') == '1')
        {
            $this->data['is_reset'] = true;
            $this->data['toekn'] = $_GET['token'];

        }
        $email = base64_decode(urldecode($Email));
        $code = base64_decode(urldecode($code));
        if ($email != '' && $code != '')
        {
            $cnt = $this->Setting_model->check_authe_url($email, $code);
        }
        else
        {
            $cnt = 2;
        }
        if ($cnt >= 1)
        {
            if (!$Subdomain)
            {
                redirect('login');
            }
            else
            {
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $this->data['event_templates'] = $event_templates;
                $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
                $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
                $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
                if (count($category_list) == 1)
                {
                    $cid = array(
                        $category_list[0]['cid']
                    );
                }
                else
                {
                    if (!empty($agenda_category[0]['agenda_category_id']))
                    {
                        $cid = array_column($agenda_category, 'agenda_category_id');
                    }
                    else
                    {
                        $cid = array(
                            $primary_agenda[0]['Id']
                        );
                    }
                }
                $event_id = $event_templates[0]['Id'];
                $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
                $this->data['menu_list'] = $menu_list;
                $banner_list = $this->Event_model->get_all_banner_list_by_event($event_id, $cid, array_column($menu_list, 'id') , $event_templates[0]['allow_show_all_agenda']);
                $this->data['banner_list'] = $banner_list;
                $active_icon = $this->Event_model->get_all_active_icon_list_by_event_id($event_id, $cid);
                $this->data['active_icon'] = $active_icon;
                $data = $this->Setting_model->getDataFromIvAttendeeByEmailCode($email, $code);
                $this->data['firstname'] = $data[0]['firstname'];
                $this->data['lastname'] = $data[0]['lastname'];
                $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
                $this->data['fb_login_data'] = $fb_login_data;
                $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
                $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
                $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
                $this->data['menu'] = $menu;
                $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
                $this->data['notify_msg'] = $notifiy_msg;
                if ($this->session->userdata('checklogin') != '1' && $this->session->flashdata('invlaidlogin_data') == "")
                {
                    if ($fundraisingenbled[0]['fundraising_enbled'] == 1)
                    {
                        if ($event_templates[0]['Event_type'] != 3 && $this->session->userdata('current_user') == "")
                        {
                        }
                        else
                        {
                            redirect(base_url() . 'fundraising/' . $acc_name . '/' . $Subdomain);
                        }
                    }
                }
                $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
                $this->data['notisetting'] = $notificationsetting;
                $user = $this->session->userdata('current_user');
                $roleid = $user[0]->Role_id;
                if ($user[0]->Role_id == 4)
                {
                    $custom = $this->Event_model->get_user_custom_clounm($user[0]->Id, $event_templates[0]['Id']);
                    $this->data['custom'] = $custom;
                }
                $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1, $acc_name);
                $this->data['cms_feture_menu'] = $cms_feture_menu;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;

                $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
                $this->data['speakers'] = $speakers;

                $notes_list = $this->Event_template_model->get_notes($Subdomain);
                $this->data['notes_list'] = $notes_list;
                $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
                $this->data['agendas'] = $agendas;

                if($event_id !='634')
                {
                    $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
                    $this->data['attendees'] = $attendees;
                }
                

                $map = $this->Event_template_model->get_map($Subdomain);
                $this->data['map'] = $map;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['Email'] = $Email;
                $this->data['speaker_flag'] = $flag;
                $res = $this->Event_template_model->get_singup_forms($event_id);
                $this->data['form_data'] = $res;
                $countrylist = $this->Profile_model->countrylist();
                $this->data['countrylist'] = $countrylist;
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $user = $this->session->userdata('current_user');
                if ($event_templates[0]['Event_type'] == '1')
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '2')
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '3')
                {
                    if ($this->session->userdata('checklogin') == '1' || $this->session->flashdata('invlaidlogin_data') != "")
                    {
                        $this->session->unset_userdata('checklogin');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->session->unset_userdata('acc_name');
                        $acc['acc_name'] = $acc_name;
                        $this->session->set_userdata($acc);
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                else
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->render();
            }
        }
        else
        {
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            if ($event_templates[0]['Event_type'] == '3')
            {
                $this->session->set_userdata('checklogin', '1');
            }
            redirect('app/' . $acc_name . '/' . $Subdomain);
            // $flag=2;
            // redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);
        }
    }*/
    /*public function index($acc_name, $Subdomain = NULL, $Email = NULL, $code = NULL)
    {   
        if($this->session->userdata('checkreset') == '1')
        {
            $this->data['is_reset'] = true;
            $this->data['toekn'] = $_GET['token'];

        }
        $email = base64_decode(urldecode($Email));
        $code = base64_decode(urldecode($code));
        if ($email != '' && $code != '')
        {
            $cnt = $this->Setting_model->check_authe_url($email, $code);
        }
        else
        {
            $cnt = 2;
        }
        if ($cnt >= 1)
        {
            if (!$Subdomain)
            {
                redirect('Login');
            }
            else
            {
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $this->data['event_templates'] = $event_templates;
                $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
                $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
                $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
                if (count($category_list) == 1)
                {
                    $cid = array(
                        $category_list[0]['cid']
                    );
                }
                else
                {
                    if (!empty($agenda_category[0]['agenda_category_id']))
                    {
                        $cid = array_column($agenda_category, 'agenda_category_id');
                    }
                    else
                    {
                        $cid = array(
                            $primary_agenda[0]['Id']
                        );
                    }
                }
                $event_id = $event_templates[0]['Id'];
                $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
                $this->data['menu_list'] = $menu_list;
                $banner_list = $this->Event_model->get_all_banner_list_by_event($event_id, $cid, array_column($menu_list, 'id') , $event_templates[0]['allow_show_all_agenda']);
                $this->data['banner_list'] = $banner_list;
                $active_icon = $this->Event_model->get_all_active_icon_list_by_event_id($event_id, $cid);
                $this->data['active_icon'] = $active_icon;
                $data = $this->Setting_model->getDataFromIvAttendeeByEmailCode($email, $code);
                $this->data['firstname'] = $data[0]['firstname'];
                $this->data['lastname'] = $data[0]['lastname'];
                $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
                $this->data['fb_login_data'] = $fb_login_data;
                $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
                $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
                $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
                $this->data['menu'] = $menu;
                $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
                $this->data['notify_msg'] = $notifiy_msg;
                if ($this->session->userdata('checklogin') != '1' && $this->session->flashdata('invlaidlogin_data') == "")
                {
                    if ($fundraisingenbled[0]['fundraising_enbled'] == 1)
                    {
                        if ($event_templates[0]['Event_type'] != 3 && $this->session->userdata('current_user') == "")
                        {
                        }
                        else
                        {
                            redirect(base_url() . 'fundraising/' . $acc_name . '/' . $Subdomain);
                        }
                    }
                }
                $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
                $this->data['notisetting'] = $notificationsetting;
                $user = $this->session->userdata('current_user');
                $roleid = $user[0]->Role_id;
                if ($user[0]->Role_id == 4)
                {
                    $custom = $this->Event_model->get_user_custom_clounm($user[0]->Id, $event_templates[0]['Id']);
                    $this->data['custom'] = $custom;
                }
                $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1, $acc_name);
                $this->data['cms_feture_menu'] = $cms_feture_menu;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;

                $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
                $this->data['speakers'] = $speakers;

                $notes_list = $this->Event_template_model->get_notes($Subdomain);
                $this->data['notes_list'] = $notes_list;
                $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
                $this->data['agendas'] = $agendas;

                if($event_id !='634')
                {
                    $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
                    $this->data['attendees'] = $attendees;
                }
                

                $map = $this->Event_template_model->get_map($Subdomain);
                $this->data['map'] = $map;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['Email'] = $Email;
                $this->data['speaker_flag'] = $flag;
                $res = $this->Event_template_model->get_singup_forms($event_id);
                $this->data['form_data'] = $res;
                $countrylist = $this->Profile_model->countrylist();
                $this->data['countrylist'] = $countrylist;
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $user = $this->session->userdata('current_user');
                $this->data['acc_name'] = $acc_name;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['token'] = $_GET['token'];

                if ($event_templates[0]['Event_type'] == '1')
                {
                    if (empty($user))
                    {

                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '2')
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '3')
                {

                    if ($this->session->userdata('checklogin') == '1' || $this->session->flashdata('invlaidlogin_data') != "")
                    {

                        $this->session->unset_userdata('checklogin');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    elseif($this->session->userdata('checkreset') == '1')
                    {

                        $this->data['acc_name'] = $acc_name;
                        $this->data['Subdomain'] = $Subdomain;
                        $this->data['token'] = $_GET['token'];
                        $this->session->unset_userdata('checkreset');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }

                    else
                    {
                        $this->session->unset_userdata('acc_name');
                        $acc['acc_name'] = $acc_name;
                        $this->session->set_userdata($acc);
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                else
                {
                    if (empty($user))
                    {
                        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->render();
            }
        }
        else
        {
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            if ($event_templates[0]['Event_type'] == '3')
            {
                $this->session->set_userdata('checklogin', '1');
            }
            redirect('App/' . $acc_name . '/' . $Subdomain);
        }
    }*/
    public function index($acc_name, $Subdomain = NULL, $Email = NULL, $code = NULL)
    {   
               
       
        /*if($this->uri->segment('3') == 'ArabHealthArabHealth')
        {
            error_reporting(E_ALL);
        }*/
        if($this->session->userdata('checkreset') == '1')
        {
            $this->data['is_reset'] = true;
            $this->data['toekn'] = $_GET['token'];

        }
        $email = base64_decode(urldecode($Email));
        $code = base64_decode(urldecode($code));
        if ($email != '' && $code != '')
        {
            $cnt = $this->Setting_model->check_authe_url($email, $code);
        }
        else
        {
            $cnt = 2;
        }

        if ($cnt >= 1)
        {
            if (!$Subdomain)
            {
                redirect('login');
            }
            else
            {
                   


                $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $this->data['event_templates'] = $event_templates;
                $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
                $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
                $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
                if (count($category_list) == 1)
                {
                    $cid = array(
                        $category_list[0]['cid']
                    );
                }
                else
                {
                    if (!empty($agenda_category[0]['agenda_category_id']))
                    {
                        $cid = array_column($agenda_category, 'agenda_category_id');
                    }
                    else
                    {
                        $cid = array(
                            $primary_agenda[0]['Id']
                        );
                    }
                }
                $event_id = $event_templates[0]['Id'];
                $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
                $this->data['menu_list'] = $menu_list;
                $banner_list = $this->Event_model->get_all_banner_list_by_event($event_id, $cid, array_column($menu_list, 'id') , $event_templates[0]['allow_show_all_agenda']);
                $this->data['banner_list'] = $banner_list;
                $active_icon = $this->Event_model->get_all_active_icon_list_by_event_id($event_id, $cid);
                $this->data['active_icon'] = $active_icon;
                $data = $this->Setting_model->getDataFromIvAttendeeByEmailCode($email, $code);
                $this->data['firstname'] = $data[0]['firstname'];
                $this->data['lastname'] = $data[0]['lastname'];
                $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
                $this->data['fb_login_data'] = $fb_login_data;
                $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
                $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
                $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
                $this->data['menu'] = $menu;
                $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
                $this->data['notify_msg'] = $notifiy_msg;

                if ($this->session->userdata('checklogin') != '1' && $this->session->flashdata('invlaidlogin_data') == "")
                {
                      
                    if ($fundraisingenbled[0]['fundraising_enbled'] == 1)
                    {
                        if ($event_templates[0]['Event_type'] != 3 && $this->session->userdata('current_user') == "")
                        {
                            
                        }
                        else
                        {
                               
                            redirect(base_url() . 'fundraising/' . $acc_name . '/' . $Subdomain);
                        }
                    }
                }
                $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
                $this->data['notisetting'] = $notificationsetting;
                $user = $this->session->userdata('current_user');
                $roleid = $user[0]->Role_id;
                if ($user[0]->Role_id == 4)
                {
                                   $custom = $this->Event_model->get_user_custom_clounm($user[0]->Id, $event_templates[0]['Id']);
                    $this->data['custom'] = $custom;
                }

                $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1, $acc_name);
                $this->data['cms_feture_menu'] = $cms_feture_menu;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;

                $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
                $this->data['speakers'] = $speakers;

                $notes_list = $this->Event_template_model->get_notes($Subdomain);
                $this->data['notes_list'] = $notes_list;
                $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
                $this->data['agendas'] = $agendas;

                if($event_id !='634')
                {
                    $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
                    $this->data['attendees'] = $attendees;
                }

                $map = $this->Event_template_model->get_map($Subdomain);
                $this->data['map'] = $map;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['Email'] = $Email;
                $this->data['speaker_flag'] = $flag;
                $res = $this->Event_template_model->get_singup_forms($event_id);
                $this->data['form_data'] = $res;
                $countrylist = $this->Profile_model->countrylist();
                $this->data['countrylist'] = $countrylist;
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $user = $this->session->userdata('current_user');
                $this->data['acc_name'] = $acc_name;
                $this->data['Subdomain'] = $Subdomain;
                $this->data['token'] = $_GET['token'];
                
                if ($event_templates[0]['Event_type'] == '1')
                {
                                    
                                       if (empty($user))
                    {

                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '2')
                {
                                    

                    if (empty($user))
                    {

                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    else
                    {

                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                elseif ($event_templates[0]['Event_type'] == '3')
                {

                                
                    if ($this->session->userdata('checklogin') == '1' || $this->session->flashdata('invlaidlogin_data') != "")
                    {

                        $this->session->unset_userdata('checklogin');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }
                    elseif($this->session->userdata('checkreset') == '1')
                    {

                                        
    
                        $this->data['acc_name'] = $acc_name;
                        $this->data['Subdomain'] = $Subdomain;
                        $this->data['token'] = $_GET['token'];
                        $this->session->unset_userdata('checkreset');
                        $this->template->write_view('content', 'registration/index', $this->data, true);
                    }

                    else
                    {

                        $this->session->unset_userdata('acc_name');
                        $acc['acc_name'] = $acc_name;
                        $this->session->set_userdata($acc);
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                else
                {
                    if (empty($user))
                    {
                       
                        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                    }
                    else
                    {
                       
                        $this->template->write_view('content', 'events/index_new', $this->data, true);
                    }
                }
                //  // //
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->render();
            }
        }
        else
        {
        
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    
            if ($event_templates[0]['Event_type'] == '3')
            {
                $this->session->set_userdata('checklogin', '1');
            }

            redirect('app/' . $acc_name . '/' . $Subdomain);
            /*$flag=2;
            redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);*/
        }
    }
    function logindemo($acc_name, $Subdomain)
    {
        $this->session->set_userdata('checklogin', '1');
         $this->session->unset_userdata('checkreset');
        // echo '<script>window.location.href="'.base_url().'app/'.$acc_name.'/'.$Subdomain.'"</script>';
        redirect('App/' . $acc_name . '/' . $Subdomain);
    }
    function reset_password($acc_name, $Subdomain,$token)
    {

        $error = NULL;
        if($this->input->post())
        {
            date_default_timezone_set('UTC');
            $check = $this->Login_model->checkToken($token);
            $data['email'] = $this->input->post('email');
            $user = $this->Login_model->check_user_email($data);
            $attempts = $this->Login_model->getUserAttempts($user[0]->Id,'reset_password');
            $date = (int) strtotime(date('Y-m-d H:i:s'));
            
            $compare_date = (int) strtotime($attempts['created_date']);
            $diff = round(($date - $compare_date)/60,2);
            if($check == 0)
            {
                $error = 'Your token is not valid.';
            }
            else if($diff > 30)
            {
                $error = 'Your input is invalid or your time to reset password is finished.';
            }
            else
            {
                $new_pass_array['email'] = $this->input->post('email');
                $new_pass_array['new_pass'] = $this->input->post('password');
                $this->Login_model->change_pas($new_pass_array);
                $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],"");
                $this->session->set_flashdata('success','Your password changed successfully. Now you can do login');
                $this->session->set_userdata('checklogin', '1');
                $this->session->unset_userdata('checkreset');
                redirect('App/' . $acc_name . '/' . $Subdomain);
            }

        }
        if($error!=NULL)
        {
            $this->session->set_flashdata('invlaidlogin_data', $error);
        }
        $this->session->set_userdata('checkreset', '1');
        redirect('App/' . $acc_name . '/' . $Subdomain.'?token='.$token);
    }
    function preview($acc_name, $Subdomain = NULL)
    {
        if (empty($Subdomain))
        {
            redirect('Login');
        }
        else
        {
            $images_file = "";
            if (!empty($_FILES['images']['name']))
            {
                $imgname = explode('.', $_FILES['images']['name']);
                $tempname = str_replace(" ", "_", $imgname);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '100000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                $images[] = $images_file;
                $data['Images'] = json_encode($images);
            }
            else
            {
                $data['Images'] = json_encode($this->input->post('old_images'));
            }
            if (!empty($_FILES['logo_images']['name']))
            {
                $imgname = explode('.', $_FILES['logo_images']['name']);
                $tempname = str_replace(" ", "_", $imgname);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '100000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("logo_images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                $logoimages[] = $images_file;
                $data['Logo_images'] = json_encode($logoimages);
            }
            else
            {
                $data['Logo_images'] = json_encode($this->input->post('old_logo_images'));
            }
            if (!empty($_FILES['background_img']['name']))
            {
                $background_imgname = explode('.', $_FILES['background_img']['name']);
                $background_imgtempname = str_replace(" ", "_", $background_imgname);
                $tempname_background_imgname = $background_imgtempname[0] . strtotime(date("Y-m-d H:i:s"));
                $background_imgname_file = $tempname_background_imgname . "." . $background_imgtempname[1];
                $this->upload->initialize(array(
                    "file_name" => $background_imgname_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '100000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("background_img"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
            }
            if ($background_imgname_file != "")
            {
                $data['Background_img'] = '["' . $background_imgname_file . '"]';
            }
            else
            {
                $data['Background_img'] = json_encode($this->input->post('old_background_img'));
            }
            $data['Background_color'] = $this->input->post('Background_color');
            $data['Top_background_color'] = $this->input->post('Top_background_color');
            $data['Top_text_color'] = $this->input->post('Top_text_color');
            $data['Footer_background_color'] = $this->input->post('Footer_background_color');
            $data['Description'] = $this->input->post('Description');
            $data['Subdomain'] = $Subdomain;
            $data['Event_name'] = $this->input->post('Event_name');
            $data['Id'] = $this->input->post('id');
            if ($this->input->post('Event_name') != "")
            {
                $this->Event_template_model->update_temp_events($Subdomain, $data);
            }
            $event_templates = $this->Event_template_model->get_event_template_by_id_list_preview($Subdomain, $data);
            if (empty($event_templates))
            {
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            }
            $this->data['event_templates'] = $event_templates;
            $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
            $this->data['menu'] = $menu;
            $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
            $this->data['notify_msg'] = $notifiy_msg;
            $user = $this->session->userdata('current_user');
            $roleid = $user[0]->Role_id;
            $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
            $this->data['menu_list'] = $menu_list;
            $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1, $acc_name);
            $this->data['cms_feture_menu'] = $cms_feture_menu;
            $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
            $this->data['cms_menu'] = $cmsmenu;
            $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
            $this->data['speakers'] = $speakers;
            $notes_list = $this->Event_template_model->get_notes($Subdomain);
            $this->data['notes_list'] = $notes_list;
            $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
            $this->data['agendas'] = $agendas;
            $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
            $this->data['attendees'] = $attendees;
            $map = $this->Event_template_model->get_map($Subdomain);
            $this->data['map'] = $map;
            $this->data['Subdomain'] = $Subdomain;
            $this->data['Email'] = $Email;
            $this->data['speaker_flag'] = $flag;
            $user = $this->session->userdata('current_user');
            if (empty($user))
            {
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('content', 'registration/index', $this->data, true);
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
            }
            else
            {
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('content', 'events/index', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
            }
        }
    }
    public function signature_form($acc_name, $Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
        $this->data['cms_feture_menu'] = $cms_feture_menu;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $countrylist = $this->Profile_model->countrylist();
        $this->data['countrylist'] = $countrylist;
        $this->data['acc_name'] = $acc_name;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('content', 'signature_form/index', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->render();
    }
    public function signature_form_data_save($acc_name, $Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $lead_data['firstname'] = $this->input->post('first_name_in_lead');
        $lead_data['lastname'] = $this->input->post('last_name_in_lead');
        $lead_data['phone_number'] = $this->input->post('phone_number_in_lead');
        $lead_data['Email'] = $this->input->post('Email_in_lead');
        $lead_data['Company'] = $this->input->post('company_in_lead');
        $lead_data['Country'] = $this->input->post('country_in_lead');
        $lead_data['source'] = "allintheloop.net";
        $lead_data['created_at'] = date('Y-m-d H:i:s');
        $leadid = $this->Event_model->save_lead_data($lead_data);
        if (!empty($leadid))
        {
            $msg = "hiii,";
            $msg.= "<br/>Name :" . ucfirst($this->input->post('first_name_in_lead')) . ' ' . $this->input->post('last_name_in_lead');
            $msg.= "<br/>Phone Number: " . $this->input->post('phone_number_in_lead');
            $msg.= "<br/>Email :" . $this->input->post('Email_in_lead');
            $msg.= "<br/> App Name : " . $event_templates[0]['Event_name'];
            $this->load->library('email');
            $sent_from = 'invite@allintheloop.com';
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'localhost';
            $config['smtp_port'] = '25';
            $config['smtp_user'] = 'invite@allintheloop.com';
            $config['smtp_pass'] = 'IKIG{ADoF]*P';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from($sent_from, 'All In The Loop');
            $this->email->to('info@allintheloop.com');
            $this->email->subject('New Lead');
            $this->email->message($msg);
            $this->email->send();
            $this->session->set_flashdata('lead_success', 'Contact Information Save Successfully.');
        }
        else
        {
            $this->session->set_flashdata('lead_error', 'Something Went Worng');
        }
        redirect(base_url() . 'App/' . $acc_name . '/' . $Subdomain . '/signature_form');
    }
    public function attendee_registration_screen($acc_name, $Subdomain)
    {   
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

        $this->data['event_templates'] = $event_templates;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);

        $this->data['category_list'] = $category_list;
        $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
        $this->data['cms_feture_menu'] = $cms_feture_menu;
        /*$this->data['attendee_types']=$this->Attendee_model->get_all_attendee_types_by_event($event_templates[0]['Id']);*/
        $this->data['registration_screen'] = $this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'], null);


        if ($this->data['registration_screen'][0]['payment_type'] == '0' && $this->data['registration_screen'][0]['stripe_payment_currency'] == 'AED' && $this->data['registration_screen'][0]['stripe_public_key'] != '')
        {
            $this->data['registration_screen'][0]['payment_type'] = '2';
        }
        $this->data['stages'] = $this->Event_template_model->get_all_stage_data_by_event($event_templates[0]['Id']);
        $this->data['discount_code'] = $this->Event_template_model->get_discount_code_for_all($event_templates[0]['Id']);

        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['acc_name'] = $acc_name;

        $this->load->library('user_agent');
        if ($this->input->is_ajax_request())
        {   
            if($_GET['user_id'])
            {
                $id = $_GET['user_id'];
                $arrpost = $this->input->post();

                if (!empty($arrpost['First_Name']) && !empty($arrpost['Email']))
                {
                    $update['Firstname'] = $arrpost['First_Name'];
                    $update['Lastname'] = $arrpost['Last_Name'];
                    $update['Company_name'] = $arrpost['Company'];
                    $update['Title'] = $arrpost['Title'];
                }
                else
                {
                    $update = 0;
                }
                $savesession = array();
                foreach($arrpost['question_ids'] as $key => $value)
                {
                    $question = $this->Add_attendee_model->get_reg_question($value);
                    $extra_column[$question['question']] = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                    $question_info[$key]['question_id'] = $value;
                    $question_info[$key]['answer'] = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                    if (!empty($this->input->post('savesession' . $value)))
                    {
                        $savesession = array_merge($savesession, $this->input->post('savesession' . $value));
                    }
                    if($question['update_alert'])
                    {
                        $update_alert[$key]['question'] = $question;
                        $update_alert[$key]['answer']   = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                    }
                }
                /*if($event_templates[0]['Id'] == '259')
                {   
                    if(!empty($update_alert))
                    {   
                        $org_email=$this->Event_template_model->get_organizer_email_by_id($event_templates[0]['Organisor_id']);      
                        $tmp_user_question1 = $this->Add_attendee_model->get_registered_user_answer($event_templates[0]['Id'],$id);
                        $user_details = $this->Add_attendee_model->get_user($id);

                        foreach ($update_alert as $key => $value)
                        {

                            $old_ans = array_keys(array_combine(array_keys($tmp_user_question1), array_column($tmp_user_question1, 'que_id')),$value['question']['id']);
                            $old_ans = is_array($tmp_user_question1[$old_ans[0]]['ans']) ? implode(',',$tmp_user_question1[$old_ans[0]]['ans']) : $tmp_user_question1[$old_ans[0]]['ans'];
                            $new_ans = is_array($value['answer']) ? implode(',',$value['answer']) : $value['answer'];
                            //$adminmsg .= '<br><b>Question: </b>'.$value['question']['question'] .'<b> Ans : </b> '.$new_ans.' <b> Old Answer :</b>'.$old_ans;
                            if(!empty($old_ans))
                            {
                                $adminmsg .= '<br><b>Question: </b>'.$value['question']['question'] .'<b> Ans : </b> '.$new_ans;
                            }
                            unset($old_ans);
                            unset($new_ans);
                        }

                        if(!empty($adminmsg))
                        {       
                            $adminmsg1=ucfirst($user_details['Firstname']).' '.$user_details['Lastname']." has updated follwing infomation.<br>";
                            $adminmsg = $adminmsg1.$adminmsg;
                            $config['protocol']   = 'smtp';
                            $config['smtp_host']  = 'localhost';
                            $config['smtp_port']  = '25';
                            $config['smtp_user']  = 'invite@allintheloop.com';
                            $config['smtp_pass']  = 'xHi$&h9M)x9m';
                            $config['_encoding']  = 'base64';
                            $config['mailtype']   = 'html';
                            $this->email->initialize($config);
                            $this->email->from('invite@allintheloop.com','All In The Loop');
                            //$this->email->to($org_email['Email']);
                            $this->email->to('jagdish@xhtmljunkies.com');
                            $this->email->subject('Registration Update - '.$user_details['Firstname'].' '.$user_details['Lastname']);
                            $this->email->message($adminmsg);
                            $this->email->send();
                            $this->email->clear(TRUE);
                        }
                    }
                }*/

                $this->Add_attendee_model->update_user_data($id, $update, $question_info, $event_templates[0]['Id'], $savesession);
                echo "success###sed";exit();
            }
            else
            {
                $arrpost = $this->input->post();
                $arrpost['Email'] = trim($arrpost['Email']);
                $is_org = $this->Add_attendee_model->check_org_email($arrpost['Email']);
                if($is_org)
                {
                    echo "error###Admin can not register as attendee, please use another Email Address.";
                    exit;
                }
                $client = $this->Add_attendee_model->checkemail($arrpost['Email'], NULL, $event_templates[0]['Id']);
                if (!$client)
                {
                    if (!empty($arrpost['First_Name']) && !empty($arrpost['Email']))
                    {
                        $tempuserdata['first_name'] = $arrpost['First_Name'];
                        $tempuserdata['last_name'] = $arrpost['Last_Name'];
                        $tempuserdata['email'] = trim($arrpost['Email']);
                        $tempuserdata['company_name'] = $arrpost['Company'];
                        $tempuserdata['title'] = $arrpost['Title'];
                        $tempuserdata['event_id'] = $event_templates[0]['Id'];
                        $this->Add_attendee_model->save_registration_temp_user($tempuserdata);
                    }
                    else
                    {
                        $tempuserdata = $this->Add_attendee_model->get_temp_reg_user($event_templates[0]['Id'], $_GET['email']);
                        $question_info = array();
                        $extra_column = array();
                        $savesession = array();
                        $step_session = array();
                        $tmp_user_question = json_decode($tempuserdata['question_info'], true);
                        $tmp_user_question1 = json_decode($tempuserdata['question_info'], true);
                        $tmp_step_agenda = json_decode($tempuserdata['step_agenda'],true);
                        foreach($arrpost['question_ids'] as $key => $value)
                        {
                            $question = $this->Add_attendee_model->get_reg_question($value);
                            $extra_column[$question['question']] = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                            $question_info[$key]['question_id'] = $value;
                            $question_info[$key]['answer'] = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                            if($question['update_alert'])
                            {
                                $update_alert[$key]['question'] = $question;
                                $update_alert[$key]['answer']   = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                            }
                            foreach($tmp_user_question as $k => $v)
                            {
                                if ($v['question_id'] == $value)
                                {
                                    $tmp_user_question[$k]['answer'] = !empty($this->input->post('question_option' . $value)) ? $this->input->post('question_option' . $value) : NULL;
                                    // print_r($this->input->post('question_option' . $value));
                                    unset($question_info[$key]);
                                }
                            }
                            if (!empty($this->input->post('savesession' . $value)))
                            {
                                $savesession = array_merge($savesession, $this->input->post('savesession' . $value));
                                $step_session['savesession'.$value] = $this->input->post('savesession' . $value);
                                if(array_key_exists('savesession'.$value,$tmp_step_agenda))
                                {
                                    unset($tmp_step_agenda['savesession'.$value]);
                                    $tmp_step_agenda['savesession'.$value] = $this->input->post('savesession' . $value);
                                }
                                else
                                {
                                    $tmp_step_agenda['savesession'.$value] = $this->input->post('savesession' . $value);   
                                }
                            }
                        }
                        if($event_templates[0]['Id'] == '259')
                        {   
                            if(!empty($update_alert))
                            {   
                                $org_email=$this->Event_template_model->get_organizer_email_by_id($event_templates[0]['Organisor_id']);

                                
                                
                                foreach ($update_alert as $key => $value)
                                {   
                                    $old_ans = array_keys(array_combine(array_keys($tmp_user_question1), array_column($tmp_user_question1, 'question_id')),$value['question']['id']);
                                    $old_ans = is_array($tmp_user_question1[$old_ans[0]]['answer']) ? implode(',',$tmp_user_question1[$old_ans[0]]['answer']) : $tmp_user_question1[$old_ans[0]]['answer'];
                                    $new_ans = is_array($value['answer']) ? implode(',',$value['answer']) : $value['answer'];
                                    //$adminmsg .= '<br><b>Question: </b>'.$value['question']['question'] .'<b> Ans : </b> '.$new_ans.' <b> Old Answer :</b>'.$old_ans;
                                    if(!empty($old_ans))
                                    {
                                    	$adminmsg .= '<br><b>Question: </b>'.$value['question']['question'] .'<b> Ans : </b> '.$new_ans;
                                    }
                                    unset($old_ans);
                                    unset($new_ans);
                                }

                                if(!empty($adminmsg))
                                {		
                                	$adminmsg1=ucfirst($tempuserdata['first_name']).' '.$tempuserdata['last_name']." has updated follwing infomation.<br>";
                                	$adminmsg = $adminmsg1.$adminmsg;
	                                $config['protocol']   = 'smtp';
	                                $config['smtp_host']  = 'localhost';
	                                $config['smtp_port']  = '25';
	                                $config['smtp_user']  = 'invite@allintheloop.com';
	                                $config['smtp_pass']  = 'xHi$&h9M)x9m';
	                                $config['_encoding']  = 'base64';
	                                $config['mailtype']   = 'html';
	                                $this->email->initialize($config);
	                                $this->email->from('invite@allintheloop.com','All In The Loop');
	                                //$this->email->to($org_email['Email']);
	                                // $this->email->to('jagdish@xhtmljunkies.com');
	                                $this->email->subject('Registration Update - '.$tempuserdata['first_name'].' '.$tempuserdata['last_name']);
	                                $this->email->message($adminmsg);
	                                $this->email->send();
	                                $this->email->clear(TRUE);
                                }
                            }
                        }
                        $tempuserdata['save_agenda_ids'].= ',' . implode(',', $savesession);
                        $tempuserdata['save_agenda_ids'] = implode(',', array_unique(array_filter(explode(',', $tempuserdata['save_agenda_ids']))));
                        if (!empty($tmp_user_question)) $question_info = array_merge($question_info, $tmp_user_question);
                        if (!empty($tempuserdata['extra_column'])) 
                        {
                            $tempuserdata['extra_column'] = json_encode(array_merge($extra_column, json_decode($tempuserdata['extra_column'], true)));
                        }
                        else
                        {
                            $tempuserdata['extra_column'] = json_encode($extra_column);
                        }
                        $tempuserdata['question_info'] = json_encode($question_info);
                        $tempuserdata['step_agenda'] = json_encode($tmp_step_agenda);
                        $tempuserdata['save_agenda_ids'] = implode(',', call_user_func_array('array_merge', $tmp_step_agenda));
                        $this->Add_attendee_model->save_registration_temp_user($tempuserdata);
                        echo "<pre>".$this->db->last_query();
                        exit;
                    }
                    echo "<pre>".$this->db->last_query();
                    exit;
                }
                else
                {
                    echo "error###Email already exist. Please choose another email.";
                    exit;
                }
            }
        }
        if ($this->input->post() && !$this->input->is_ajax_request())
        {   
            $is_org = $this->Add_attendee_model->check_org_email($this->input->post('user-email'));
            if($is_org)
            {
                $this->session->set_flashdata('reg_user_update_success', 'Admin can not register as attendee, please use another Email Address.');
                redirect('App/' . $acc_name . '/' . $Subdomain.'/attendee_registration_screen');
            }
            else
            {
                if ($this->input->post('user-email'))
                {
                    $user = $this->Add_attendee_model->get_temp_reg_user($event_templates[0]['Id'], $this->input->post('user-email'));
                    if (empty($user))
                    {
                        $tmp_user = $this->User_model->get_user_by_email_event($this->input->post('user-email') , $event_templates[0]['Id']) [0];
                        if (!empty($tmp_user))
                        {
                            $user_ans = $this->Add_attendee_model->get_registered_user_answer($event_templates[0]['Id'], $tmp_user['Id']);
                            $user['save_agenda_ids'] = $this->Add_attendee_model->get_users_agenda_list($event_templates[0]['Id'], $tmp_user['Id']) ['agenda_id'];
                            foreach($user_ans as $key => $value)
                            {
                                if ($value['type'] == '2')
                                {
                                    $user_ans[$key]['ans'] = explode(',', $value['ans']);
                                }
                            }
                            $que_id = array_column($user_ans, 'que_id');
                            $ans = array_column($user_ans, 'ans');
                            $user['Id'] = $tmp_user['Id'];
                            $user['first_name'] = $tmp_user['Firstname'];
                            $user['last_name'] = $tmp_user['Lastname'];
                            $user['email'] = $tmp_user['Email'];
                            $user['company_name'] = $tmp_user['Company_name'];
                            $user['title'] = $tmp_user['Title'];
                            $user_ans = array_combine($que_id, $ans);
                        }
                    }
                    else
                    {
                        $user_ans = json_decode($user['question_info'], true);
                        $que_id = array_column($user_ans, 'question_id');
                        $ans = array_column($user_ans, 'answer');
                        $user_ans = array_combine($que_id, $ans);
                    }
                    if (!empty($user))
                    {
                        $this->data['hide_reg_modal'] = "1";
                        $user['user_ans'] = $user_ans;
                        $this->data['user_info'] = $user;
                    }
                    else
                    {
                        if($acc_name == 'CandyBerman' && $Subdomain == 'CanBer5a2a5ca878165'):
                        $this->session->set_flashdata('reg_user_update_success', 'Thanks so much for your interest in our 2018 Purpose Built Communities Conference. If you are having trouble registering, please contact Linda Roberts at lroberts@purposebuiltcommunities.org.');
                        else:
                        $this->session->set_flashdata('reg_user_update_success', 'No User Found');
                        endif;
                        redirect('app/' . $acc_name . '/' . $Subdomain.'/attendee_registration_screen');
                    }
                }
            }
        }
        $this->load->view('events/new_attendee_registrationscreen', $this->data);
    }
    /*public function show_multi_registration($acc_name,$Subdomain)
    {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $this->data['event_id']=$event_templates[0]['Id'];
    $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
    $this->data['registration_screen']=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);
    $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
    $this->data['category_list'] = $category_list;
    $this->data['Subdomain'] = $Subdomain;
    $this->data['acc_name'] = $acc_name;
    $this->load->view('events/multi_forms_views', $this->data);
    }
    public function get_session_list($acc_name,$Subdomain)
    {
    $ticket_type=$this->input->post('ticket_type');
    $numnerforms=$this->input->post('numnerforms');
    $useremail=$this->input->post('useremail');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $event_id=$event_templates[0]['Id'];
    $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
    $agenda = $this->Event_template_model->get_agenda_list_by_ticket_type($Subdomain,$ticket_type);
    $htm="";
    $format="H:i";
    if($time_format[0]['format_time']=='0')
    {
    $format="h:i A";
    }
    else
    {
    $format="H:i";
    }
    if(count($agenda) > 0){
    foreach ($agenda as $vkey => $vvalue) {
    $htm.='<tr>';
    $htm.='<td>'.ucfirst($vvalue['Heading']).'</td>';
    $htm.='<td>'.date($format,strtotime($vvalue['Start_time'])).' - '.date($format,strtotime($vvalue['End_time'])).'</td>';
    $htm.='<td>'.$vvalue['custom_speaker_name'].'</td>';
    $place=$vvalue['Place_left']<='0' ? 'NO Place Left' : $vvalue['Place_left'];
    $htm.='<td>'.$place.'</td>';
    $htm.='<td><a class="btn btn-green btn-block" onclick="savesession(this,'.$vvalue['Id'].','.$vvalue['Place_left'].');">Save</a></td>';
    $htm.='</tr>';
    }
    $htm='<div class="panel-group accordion" id="accordion'.$numnerforms.'">';
    $couterkey=0;
    foreach ($agenda as $key => $value) {
    static $f1=0;if($f1==0) { $classname='';$f1++; } else { $classname='collapsed';$f1++;}
    static $f=0;if($f==0) { $classname1='in';$f++; } else { $classname1='';$f++; }
    $htm.='<div class="panel panel-white">';
    $htm.='<div class="panel-heading">';
    $htm.='<h5 class="panel-title agenda_title">';
    $htm.='<a class="accordion-toggle '.$classname.'" data-toggle="collapse" data-parent="#accordion'.$numnerforms.'" href="#accordion'.$couterkey.$numnerforms.'"><i class="icon-arrow"></i>';
    $htm.=date("m/d/Y",strtotime($key));
    $htm.='</a></h5>';
    $htm.='</div>';
    $htm.='<div id="accordion'.$couterkey.$numnerforms.'" class="collapse '.$classname1.'">';
    $couterkey++;
    $htm.='<table class="table table-striped table-bordered table-hover table-full-width" id="sample_'.$key1.$numnerforms.'">';
    $htm.='<thead><tr><th>Session Name</th><th>Time</th><th>Speaker</th><th>Places left</th><th>Save Session</th></tr></thead><tbody>';
    foreach ($value as $key1 => $vvalue)
    {
    $htm.='<tr>';
    $htm.='<td>'.ucfirst($vvalue['Heading']).'</td>';
    $htm.='<td>'.date($format,strtotime($vvalue['Start_time'])).' - '.date($format,strtotime($vvalue['End_time'])).'</td>';
    $htm.='<td>'.$vvalue['custom_speaker_name'].'</td>';
    $place=$vvalue['Place_left']<='0' ? 'NO Place Left' : $vvalue['Place_left'];
    $htm.='<td>'.$place.'</td>';
    $htm.='<td><a class="btn btn-green btn-block" onclick="savesession(this,'.$vvalue['Id'].','.$vvalue['Place_left'].','.$numnerforms.');">Save</a></td>';
    $htm.='</tr>';
    }
    $htm.='</tbody></table>';
    $htm.='</div></div>';
    }
    $htm.='</div>';
    }
    else
    {
    $htm.='<h4 style="text-align:center">No Session Found...</h4>';
    }
    $tempuser_agenda['save_agenda_ids']=NULL;
    if(!empty($useremail))
    {
    $this->Event_template_model->save_temp_user_session($tempuser_agenda,$useremail,$event_id);
    }
    echo $htm;die;
    }*/
     public function save_tempusersession($acc_name, $Subdomain, $aid)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $event_id = $event_templates[0]['Id'];
        $savesession = array_filter(explode(",", $this->input->post('savesession')));
        $email = $this->input->post('email');
        $client = $this->Add_attendee_model->checkemail($email, NULL, $event_id);
        if($event_id == '1114')
        {
            $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $aid);
            // j($agenda_value);
            if ($agenda_value[0]['allow_clashing'] == '0')
            {
                $overlapping = $this->Event_template_model->get_overlapping_agenda_by_temp_registre_user($agenda_value, $savesession, $event_id);
                if ($overlapping == false)
                {
                    die('error###This session clashes with another session you have saved â€“ Unfortunately you have not been booked onto this session');
                }
                else
                {   
                    $user = $this->Event_template_model->getUserByEmail($this->input->post('email'),$event_id);
                    if(!empty($user))
                    {
                        $data['agenda_id'] = $this->input->post('savesession');
                        $data['user_id'] = $user['Id'];
                        $this->Event_template_model->saveUserSession($data);
                    }
                }
            }
            else
            {
                $user = $this->Event_template_model->getUserByEmail($this->input->post('email'),$event_id);
                if(!empty($user))
                {
                    $data['agenda_id'] = $this->input->post('savesession');
                    $data['user_id'] = $user['Id'];
                    $this->Event_template_model->saveUserSession($data);
                }
            }
            
            // $this->Event_template_model->save_temp_user_session($aid,$email,$event_id);
            die('Success###');
        }
        else
        {
            if ($client)
            {
                die("error###Email already exist. Please choose another email.");
            }
            else
            {
                $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $aid);
                if (!in_array($aid, $savesession))
                {
                    if ($agenda_value[0]['allow_clashing'] == '0')
                    {
                        $overlapping = $this->Event_template_model->get_overlapping_agenda_by_temp_registre_user($agenda_value, $savesession, $event_id);
                        if ($overlapping == false)
                        {
                            die('error###This session clashes with another session you have saved â€“ Unfortunately you have not been booked onto this session');
                        }
                    }
                }
                // $this->Event_template_model->save_temp_user_session($aid,$email,$event_id);
                die('Success###');
            }
        }
    }

}