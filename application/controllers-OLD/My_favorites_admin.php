<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class My_favorites_admin extends FrontendController {
	function __construct() 
	{
        $this->data['pagetitle'] = 'My Favorites';
        $this->data['smalltitle'] = 'My Favorites People';
        $this->data['breadcrumb'] = 'My Favorites';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $eventid=$this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        $eventid=$this->uri->segment(3);

        $this->load->database();
        $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
        $user_events =  array_filter(array_column($user_events,'Event_id'));
        if(!in_array($eventid,$user_events))
        {
             redirect('Forbidden');
        }

        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $eventmodule=$this->Event_model->geteventmodulues($eventid);
        $module=json_decode($eventmodule[0]['module_list']);
	    if(!in_array('49',$module))
	    {
	        echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
	    }
	    $event = $this->Event_model->get_module_event($eventid);
	    $menu_list = explode(',', $event[0]['checkbox_values']);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if(!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst('My_Favorites');
            $cnt = $this->Agenda_model->check_auth($req_mod, $roleid, $rolename,$eventid);
        }
        else
        {
            $cnt=0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('49',$menu_list))
        {
			$this->load->model('Setting_model');
			$this->load->model('Map_model');
			$this->load->model('Speaker_model');
			$this->load->model('Event_template_model');
			$this->load->model('Profile_model');
			$this->load->library('upload');
			$roles = $this->Event_model->get_menu_list($roleid,$eventid);
			$this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
        }
    }
    public function index($id) 
    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
		$user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
		$user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
        if ($this->data['user']->Role_name == 'User') 
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
        }
		if ($this->data['user']->Role_name == 'Client')
		{
		   $event = $this->Event_model->get_admin_event($id);
		   $this->data['event'] = $event[0];
		}
        $modules_arr=$this->Event_template_model->get_all_modules_order_by_event_id($id);
        $this->data['modules_arr']=$modules_arr;
        $menudata=$this->Event_model->geteventmenu($id,49);
        $menu_toal_data=$this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'My_favorites_admin/css', $this->data, true);
        $this->template->write_view('content', 'My_favorites_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User') 
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } 
        else 
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'My_favorites_admin/js', $this->data, true);
        $this->template->render();
    }
    public function save_modules_order($id)
    {
        $modules_arr[0]['event_id']=$id;
        $modules_arr[0]['modules_id']=2;
        $modules_arr[0]['position']=$this->input->post('attendee_position');
        $modules_arr[0]['color']=$this->input->post('attendees_color');

        $modules_arr[1]['event_id']=$id;
        $modules_arr[1]['modules_id']=3;
        $modules_arr[1]['position']=$this->input->post('exhibitors_position');
        $modules_arr[1]['color']=$this->input->post('exhibitors_color');

        $modules_arr[2]['event_id']=$id;
        $modules_arr[2]['modules_id']=7;
        $modules_arr[2]['position']=$this->input->post('speakers_position');
        $modules_arr[2]['color']=$this->input->post('speakers_color');

        $modules_arr[3]['event_id']=$id;
        $modules_arr[3]['modules_id']=43;
        $modules_arr[3]['position']=$this->input->post('sponsors_position');
        $modules_arr[3]['color']=$this->input->post('sponsors_color');
        $this->Event_template_model->save_my_favorites_modules_order($modules_arr);
        redirect(base_url().'My_favorites_admin/index/'.$id);
    }
    public function Export_favorites($eid)
    {
        $res=$this->Event_template_model->get_all_favorites_data_Export($eid);
        $filename = "favorites_user.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Profile type";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Title";
        $header[] = "Company Name";
        $header[] = "Email";
        $header[] = "Favorited by First Name";
        $header[] = "Favorited by Last Name";
        $header[] = "Favorited by Title";
        $header[] = "Favorited by Company";
        $header[] = "Favorited by Email Address";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp,array());
        fputcsv($fp, $header);
        foreach ($res as $key => $value) {
            if($value['module_type']=='7')
            {
                $sdata['Profile_type']="Speaker";
            }
            else if($value['module_type']=='43')
            {
                $sdata['Profile_type']="Sponsors";   
            }
            else if($value['module_type']=='3')
            {
                $sdata['Profile_type']="Exhibitors";   
            }
            else
            {
                $sdata['Profile_type']="Attendee";   
            }
            $sdata['Firstname']=ucfirst($value['Firstname']);
            $sdata['Lastname']=ucfirst($value['Lastname']);
            $sdata['Title']=$value['Title'];
            if($value['module_type']=='43')
            {
                $sdata['Company Name']=$value['Sponsors_name'];
            }
            else
            {
                $sdata['Company Name']=$value['Company_name'];
            }
            $sdata['Email']=$value['Email'];
            $sdata['Favorited by First Name']=ucfirst($value['fbyfnm']);
            $sdata['Favorited by Last Name']=ucfirst($value['fbylnm']);
            $sdata['Favorited by Title']=$value['fbytitle'];
            $sdata['Favorited by Company']=$value['fbycompany'];
            $sdata['Favorited by Email Address']=$value['fbyemail'];
            fputcsv($fp, $sdata);
        }
    }
}
?>