<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require $_SERVER['DOCUMENT_ROOT'].'/sdk-php-master/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class Attendee_login extends CI_Controller {
    function __construct() 
    {
        $this->data['pagetitle'] = 'Events';
        $this->data['smalltitle'] = 'Events Details';
        $this->data['breadcrumb'] = 'Events Template';
        parent::__construct($this->data);
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Attendee_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Profile_model');
        $this->load->model('Login_model');
        $this->load->model('Add_attendee_model');
        $this->load->model('Event_template_model');
        $this->load->library('session');
        $this->load->library('email'); 
        
    }
    public function setpassword($Subdomain=NULL)
    {
        $event_id=$this->input->post('Event_id');
        $this->data['Subdomain'] = $Subdomain;
        if($this->input->post())
        {   
            $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

            $this->data['event_templates'] = $event_templates;
            $json_arr=array();
            
            if(count($this->input->post('arrCustom')) > 0)
            {
                $arrCustomCol  = $this->input->post('arrCustom');
                $strCustomJson = json_encode($arrCustomCol);
                unset($_POST['arrCustom']);
            }
            
            $arrayName = array('firstname','lastname','password','password_again','Company_name','Title','Country','Event_id','Active','attendee_flag','Organisor_id','email');   
            foreach($this->input->post() as $k=>$v)
            {
                if($k != "password_again" && $k != "idval")
                {
                    if($k == "zipcode")
                    { 
                        $k = "Postcode";
                    }
                    if($k == "Permissions")
                    {
                       $v = implode(',', $v);
                    }
                    if($k == "Company_name1")
                    {
                       $k = "Company_name";
                    }
                    if($k == "Title1")
                    {
                       $k = "Title";
                    }
                    if(!in_array($k,$arrayName))
                    {
                        $json_arr[$k]=$this->input->post($k);
                    }
                    else
                    {
                          $k = ucfirst(strtolower($k));
                         $array_add[$k] = $v;
                    }
                  
                }
                
            }   
            $json_data=json_encode($json_arr);
            $datavalues=$this->Attendee_model->get_invited_attendee_bymail($this->input->post('email'),$event_id);
            
            if(!empty($datavalues))
            {
                $count_attendee=$this->Event_model->get_all_attendee_count($event_id);
                $org_id=$this->Event_model->get_org_id_by_event($event_id);
                $org_email=$this->Event_model->getOrgEmail($org_id);
                $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($org_email);
                if(count($iseventfreetrial) <= 0 || $count_attendee < 3)
                {
                   $array_add['Firstname'] = $datavalues[0]['firstname'];
                   $array_add['Lastname'] = $datavalues[0]['lastname'];
                   $array_add['agenda_id']=$datavalues[0]['agenda_id'];
                   $array_add['Company_name']=$datavalues[0]['Company_name'];
                   $array_add['Title']=$datavalues[0]['Title'];
                   $array_add['extra_column']=$datavalues[0]['extra_column'];
                }
                else
                {
                    $this->session->set_flashdata('invlaidlogin_data', 'You can only add up to 3 Attendee Profiles until you Launch Your App.');
                    redirect("App/".$acc_name."/".$Subdomain."");
                }   
            }
            
            /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            $eid=$event_templates[0]['Id'];
            $acc=$this->Event_template_model->getAccname($eid);
            $acc_name=$acc[0]['acc_name'];
            $notisetting=$this->Setting_model->getnotificationsetting($eid);
            $role_flag=$this->Attendee_model->check_invited_attendee($event_id,$this->input->post('email'));
            
            $this->Attendee_model->update_register_user($event_id,$this->input->post('email'));
            if($notisetting[0]['email_display']=='1')
            {   
                if($role_flag=='1')
                {
                    $slug = "WELCOME_MSG";
                    $em_template = $this->Setting_model->front_email_template($slug,$eid);
                   
                    $strFrom = $em_template[0]['From'];
                   
                    /*$this->load->library('email');  
                    $config['protocol'] = "smtp";
                    $config['smtp_host'] = "ssl://smtp.gmail.com";
                    $config['smtp_port'] = "465";
                    $config['smtp_user'] = "nakranichetan32@gmail.com"; 
                    $config['smtp_pass'] = "chetan_9099118056";
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "text/html";
                    $config['newline'] = "\r\n";

                    $this->email->initialize($config);*/
                   
                    $subject="WELCOME_MSG";
                   
                    $msg="Hello,".ucfirst($this->input->post('firstname'))."<br>";
                   
                    $msg.="Your username is  : ".$this->input->post('email')."<br/>";
                    $msg.="Your password is  : ".$this->input->post('password')."<br/>";
                    $msg.= "<a href=".base_url().">Click here</a> to login and add your exhibitor details"."<br/>";
                    
                    $this->email->from($strFrom, 'AllInTheLoop');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject($subject);//'User Account'
                    $this->email->set_mailtype("html");
                    $this->email->message($msg);    
                    $this->email->send();
                }
                else
                {
                    $slug = "WELCOME_MSG";                
                    $em_template = $this->Setting_model->front_email_template($slug,$eid);
                    $em_template = $em_template[0];
                    $new_pass = $this->input->post('password');
                    $name = ($this->input->post('Firstname')==NULL) ? $datavalues[0]['firstname'] : $this->input->post('Firstname');
                    $msg = html_entity_decode($em_template['Content']);
                    $patterns = array();
                    $patterns[0] = '/{{name}}/';
                    $patterns[1] = '/{{password}}/';
                    $patterns[2] = '/{{email}}/';
                    //$patterns[3] = '/{{link}}/';
                    $replacements = array();
                    $replacements[0] = $name;
                    $replacements[1] = $new_pass;
                    $replacements[2] = $this->input->post('email');
                    $msg = preg_replace($patterns, $replacements, $msg);
                    $msg .= "Email: ".$this->input->post('email')."<br/>";
                    //$msg .= "Login Link: ".base_url()."<br/>";
                    //$msg .= "Password: ".$new_pass;
                   if($em_template['Subject']=="")
                   {
                        $subject="WELCOME_MSG";
                   }
                   else
                   {
                        $subject=$em_template['Subject'];
                   }
                   $strFrom= EVENT_EMAIL;
                    if($em_template['From']!="")
                    {
                        $strFrom = $em_template['From'];
                    }
                    $this->email->from($strFrom, 'AllInTheLoop');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject($subject);//'User Account'
                    $this->email->set_mailtype("html");
                    $this->email->message($msg);    
                    $this->email->send();
                    }
                
            }
            
            
            /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            if(!empty($arrCustomCol)){
                $array_add['extra_column'] = $strCustomJson;
            }
            $user_id = $this->Add_attendee_model->add_user($array_add,$role_flag);
            
           
            
            
            
            $logindetails['current_user'] = $this->Add_attendee_model->check_sign_login($this->input->post());
            $user1 = $this->session->userdata('current_user');

            if (empty($logindetails['current_user'])) 
            {
                $this->session->unset_userdata('current_user');
                $error['invalid_cred'] = array("error" => "login");
                $this->session->set_userdata($error);
                redirect("App/".$acc_name."/".$Subdomain."");
            } 
            else 
            {
                $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
                $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
                $logindetails['acc_name']=$acc_name;
                if($this->input->post())
                {
                    $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
                    $subdomain = $sub[0]['Subdomain'];
                    $redirect = base_url().''.'App/'.$acc_name."/".$subdomain.'';
                    $this->session->set_userdata($logindetails);
                    if($role_flag=='1')
                    {
                        redirect('Profile/update/'.$user_id);
                    }
                    else
                    {
                        if($this->input->post('reurl')=="")
                        {   
                            $registration_screen=$this->Attendee_model->get_registration_screen_data($this->input->post('Event_id'),null);
                            if(count($registration_screen) > 0)
                            {
                                redirect(base_url().'App/'.$acc_name."/".$Subdomain.'/attendee_registration_screen');
                            }
                            else
                            {
                                redirect(base_url().'App/'.$acc_name."/".$Subdomain);   
                            }
                        }
                        else
                        {
                            $reurl=$this->input->post('reurl');
                            $this->session->unset_userdata('reurl');
                            redirect($reurl);
                            //echo "<script>window.top.location.href='".$reurl."'</script>";
                        }
  
                    }
                }
            } 
        }
    }
    public function add($Subdomain=NULL)
    {

        //echo "<pre>";print_r($_POST);die;
        $this->data['Subdomain'] = $Subdomain;
        if($this->input->post())
        {   
            $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

            $this->data['event_templates'] = $event_templates;

            if(isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
            {
                $tempname = explode('.',$_FILES['userfile']['name']);
                $tempname[0] = $tempname[0].strtotime(date("Y-m-d H:i:s"));
                $_FILES['userfile']['name'] = $tempname[0].".".$tempname[1];                
                $_POST['Logo'] = $_FILES['userfile']['name'];
                if($this->data['user']->Role_name == 'User')
                {
                    $this->data['user']->Logo=$_FILES['userfile']['name'];
                }
                $config['upload_path'] = './assets/user_files';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width']  = '2048';
                $config['max_height']  = '2048';
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data1 = array('upload_data' => $this->upload->data());                        
                }
            }   
            $json_arr=array();
            $arrayName = array('firstname','lastname','password','password_again','Company_name','Title','Country','Event_id','Active','attendee_flag','Organisor_id','email');   
            foreach($this->input->post() as $k=>$v)
            {
                if($k != "password_again" && $k != "idval")
                {
                    if($k == "zipcode")
                    { 
                        $k = "Postcode";
                    }
                    if($k == "Permissions")
                    {
                       $v = implode(',', $v);
                    }
                    if($k == "Company_name1")
                    {
                       $k = "Company_name";
                    }
                    if($k == "Title1")
                    {
                       $k = "Title";
                    }
                    if(!in_array($k,$arrayName))
                    {
                        $json_arr[$k]=$this->input->post($k);
                    }
                    else
                    {
                          $k = ucfirst(strtolower($k));
                         $array_add[$k] = $v;
                    }
                  
                }
                
            }   
            $json_data=json_encode($json_arr);
            $datavalues=$this->Attendee_model->get_invited_attendee_bymail($this->input->post('email'));
            
            if(!empty($datavalues))
            {
                $count_attendee=$this->Event_model->get_all_attendee_count($eid);
                $org_id=$this->Event_model->get_org_id_by_event($eid);
                $org_email=$this->Event_model->getOrgEmail($org_id);
                $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($org_email);
                if(count($iseventfreetrial) <= 0 || $count_attendee < 3)
                {
                    $array_add['Firstname'] = $datavalues[0]['firstname'];
                    $array_add['Lastname'] = $datavalues[0]['lastname'];
                    $array_add['agenda_id']=$datavalues[0]['agenda_id'];
                    $array_add['extra_column']=$datavalues[0]['extra_column'];
                }
                else
                {
                    $this->session->set_flashdata('invlaidlogin_data', 'You can only add up to 3 Attendee Profiles until you Launch Your App.');
                    redirect("App/".$acc_name."/".$Subdomain."");
                }
            }
        
            /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            $eid=$event_templates[0]['Id'];
            $acc=$this->Event_template_model->getAccname($eid);
            $acc_name=$acc[0]['acc_name'];
            $notisetting=$this->Setting_model->getnotificationsetting($eid);
            $role_flag=$this->Attendee_model->check_invited_attendee($this->input->post('Event_id'),$this->input->post('email'));
            $aff_rows_byLink=0;
            $aff_rows_byLink=$this->Attendee_model->update_register_user($eid,$this->input->post('email'));
            if($notisetting[0]['email_display']=='1')
            {   
                if($role_flag=='1')
                {
                    $slug = "WELCOME_MSG";
                    $em_template = $this->Setting_model->front_email_template($slug,$eid);
                   
                    $strFrom = $em_template[0]['From'];
                   
                    /*$this->load->library('email');  
                    $config['protocol'] = "smtp";
                    $config['smtp_host'] = "ssl://smtp.gmail.com";
                    $config['smtp_port'] = "465";
                    $config['smtp_user'] = "nakranichetan32@gmail.com"; 
                    $config['smtp_pass'] = "chetan_9099118056";
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "text/html";
                    $config['newline'] = "\r\n";

                    $this->email->initialize($config);*/
                   
                    $subject="WELCOME_MSG";
                   
                    $msg="Hello,".ucfirst($this->input->post('firstname'))."<br>";
                   
                    $msg.="Your username is  : ".$this->input->post('email')."<br/>";
                    $msg.="Your password is  : ".$this->input->post('password')."<br/>";
                    $msg.= "<a href=".base_url().">Click here</a> to login and add your exhibitor details"."<br/>";
                    
                    $this->email->from($strFrom, 'AllInTheLoop');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject($subject);//'User Account'
                    $this->email->set_mailtype("html");
                    $this->email->message($msg);    
                    $this->email->send();
                }
                else
                {
                    $count_attendee=$this->Event_model->get_all_attendee_count($eid);
                    $org_id=$this->Event_model->get_org_id_by_event($eid);
                    $org_email=$this->Event_model->getOrgEmail($org_id);
                    $iseventfreetrial=$this->Event_model->check_event_free_trial_account_by_org_email($org_email);
                    if(count($iseventfreetrial) <= 0 || $count_attendee < 3){}else{
                        $this->session->set_flashdata('invlaidlogin_data', 'You can only add up to 3 Attendee Profiles until you Launch Your App.');
                        redirect("App/".$acc_name."/".$Subdomain."");
                    }
                    $slug = "WELCOME_MSG";                
                    $em_template = $this->Setting_model->front_email_template($slug,$eid);
                    $em_template = $em_template[0];
                    $new_pass = $this->input->post('password');
                    $name = ($this->input->post('Firstname')==NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
                    $msg = html_entity_decode($em_template['Content']);
                    $patterns = array();
                    $patterns[0] = '/{{name}}/';
                    $patterns[1] = '/{{password}}/';
                    $patterns[2] = '/{{email}}/';
                    //$patterns[3] = '/{{link}}/';
                    $replacements = array();
                    $replacements[0] = $name;
                    $replacements[1] = $new_pass;
                    $replacements[2] = $this->input->post('email');
                    $msg = preg_replace($patterns, $replacements, $msg);
                    $msg .= "Email: ".$this->input->post('email')."<br/>";
                    //$msg .= "Login Link: ".base_url()."<br/>";
                    //$msg .= "Password: ".$new_pass;
                   if($em_template['Subject']=="")
                   {
                        $subject="WELCOME_MSG";
                   }
                   else
                   {
                        $subject=$em_template['Subject'];
                   }
                   $strFrom= EVENT_EMAIL;
                    if($em_template['From']!="")
                    {
                        $strFrom = $em_template['From'];
                    }
                    $this->email->from($strFrom, 'AllInTheLoop');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject($subject);//'User Account'
                    $this->email->set_mailtype("html");
                    $this->email->message($msg);    
                    $this->email->send();
                    }
                
            }
            
            
            /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////

            $user_id = $this->Add_attendee_model->add_user($array_add,$role_flag);

            $form_data=array('user_id'=>$user_id,'json_data'=>$json_data);
            $this->Add_attendee_model->add_user_form_data($form_data);
          
            $logindetails['current_user'] = $this->Add_attendee_model->check_sign_login($this->input->post());
            $user1 = $this->session->userdata('current_user');
            if (empty($logindetails['current_user'])) 
            {
                $this->session->unset_userdata('current_user');
                $error['invalid_cred'] = array("error" => "login");
                $this->session->set_userdata($error);
                redirect("App/".$acc_name."/".$Subdomain."");
            } 
            else 
            {
                $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
                $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
                $logindetails['acc_name']=$acc_name;
                if($this->input->post())
                {
                    $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
                    $subdomain = $sub[0]['Subdomain'];
                    $redirect = base_url().''.'App/'.$acc_name."/".$subdomain.'';
                    $this->session->set_userdata($logindetails);
                    if($role_flag=='1')
                    {
                        redirect('Profile/update/'.$user_id);
                    }
                    else
                    {
                        echo $this->input->post('reurl');
                        if($this->input->post('reurl')=="")
                        {

                            if($aff_rows_byLink>0)
                            {
                                
                                redirect(base_url().'App/'.$acc_name."/".$Subdomain);
                            }
                            else
                            {
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                        else
                        {
                            $reurl=$this->input->post('reurl');
                            $this->session->unset_userdata('reurl');
                            redirect($reurl);
                            //echo "<script>window.top.location.href='".$reurl."'</script>";
                        }
  
                    }
                }
            } 
        }
    }
    public function check_auth_user_login($Subdomain)
    {

        if(!empty($this->input->post('username')))
        {
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            if(!empty($this->input->post('Event_id'))){
                $event_id=$this->input->post('Event_id');
            }
            else
            {
                $event_id=$event_templates[0]['Id'];
            }
            $email=$this->input->post('username');
            $user_data=$this->Add_attendee_model->check_user_already_exists($email,$event_id);

            if(count($user_data) <= 0)
            {
                $adduserdata=array();
                if($event_templates[0]['authorized_email']=='1')
                {
                    $check_auth_email=$this->Add_attendee_model->check_authorized_email_by_email($email,$event_id);
                    if(count($check_auth_email) > 0)
                    {
                        $adduserdata=$check_auth_email[0];
                        $adduserdata['Event_id']=$event_id;
                        $adduserdata['Organisor_id']=$event_templates[0]['Organisor_id'];
                        $this->Add_attendee_model->remove_authorized_user($check_auth_email[0]['authorized_id']);
                    }
                }
                else
                {
                    $check_auth_email=$this->Add_attendee_model->check_authorized_email_by_email($email,$event_id);
                    if(count($check_auth_email) > 0)
                    {
                        $filledprofiles=0;
                        $adduserdata=$check_auth_email[0];
                        $adduserdata['Event_id']=$event_id;
                        $adduserdata['Organisor_id']=$event_templates[0]['Organisor_id'];
                        $this->Add_attendee_model->remove_authorized_user($check_auth_email[0]['authorized_id']);
                    }
                    else
                    {
                        $filledprofiles=1;
                        $adduserdata['Email']=$email;
                        $adduserdata['Event_id']=$event_id;
                        $adduserdata['Organisor_id']=$event_templates[0]['Organisor_id'];
                    }
                }
                if(count($adduserdata) > 0)
                {
                    $this->Add_attendee_model->add_authoried_Emails_user($adduserdata);
                }
                $profileshow=1;
            }
            else
            {
                $profileshow=0;
            }

            $logindetails['current_user'] = $this->Add_attendee_model->check_auth_user_login_only_email($email,$event_id);
            $acc=$this->Event_template_model->getAccname($event_id);   
            $acc_name=$acc[0]['acc_name'];

            if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
            {
                
                $this->session->unset_userdata('current_user');
                if($logindetails['current_user']=="inactive")
                {
                   
                    $this->session->set_flashdata('invlaidlogin_data', 'Please contact sales to access your account');
                }
                else
                {
                    
                    $this->session->set_flashdata('invlaidlogin_data', 'Unauthorized Email Address.');
                }

               
                redirect("App/".$acc_name."/".$Subdomain);
            }
            else
            {
                if($logindetails['current_user'][0]->Rid=='3')
                {
                    $this->session->set_flashdata('invlaidlogin_data', 'Unauthorized Email Address.');
                    redirect("App/".$acc_name."/".$Subdomain);
                }
                $logindetails['acc_name']=$acc_name;
                $this->session->set_userdata($logindetails);

                if($profileshow=='1' && $logindetails['current_user'][0]->Rid=='4')
                {   
                    if($filledprofiles=='1')
                    {
                        redirect("App/".$acc_name."/".$Subdomain.'#userfillProfiles');
                    }
                    else
                    {
                        redirect("App/".$acc_name."/".$Subdomain.'#userProfiles');
                    }
                }
                else
                { 
                     // echo "ggg"; exit();
                    // $this->session->sess_destroy();
                    redirect("App/".$acc_name."/".$Subdomain."");   
                }
            } 
        }
    }
    /*public function check($Subdomain=NULL) 
    {
        
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $logindetails['current_user'] = $this->Add_attendee_model->check_login($this->input->post(),$event_templates[0]['Id']);
        $logindetails['isLoggedIn']=true;
        $eid=$event_templates[0]['Id'];
        $acc=$this->Event_template_model->getAccname($eid);   
        $acc_name=$acc[0]['acc_name'];
        if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
        {
            $this->session->unset_userdata('current_user');
            if($logindetails['current_user']=="inactive")
            {
                $this->session->set_flashdata('invlaidlogin_data', 'Please contact sales to access your account');
            }
            else
            {
                $this->session->set_flashdata('invlaidlogin_data', 'Invalid username or password');
            }
            if($this->input->post('reurl')=="")
            {
                redirect("App/".$acc_name."/".$Subdomain."");
            }
            else
            {
                $this->session->set_userdata('reurl',$this->input->post('reurl'));
                redirect("App/".$acc_name."/".$Subdomain."");
            }
        } 
        else 
        {

            if($this->input->post())
            {
                $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
                $subdomain = $sub[0]['Subdomain'];
                $redirect = base_url().''.'App/'.$acc_name."/".$subdomain.'';
                $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
               
                $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
                $logindetails['acc_name']=$acc_name;
                $this->session->set_userdata($logindetails);
                $arr=explode("/",$_SERVER["HTTP_REFERER"]);
                if($this->input->post('reurl')=="")
                {
                    if($arr[4]=='App')
                    {
                        $url=$_SERVER["HTTP_REFERER"];
                    }
                    else
                    {
                        $new=array_slice($arr, -3);
                        $url=base_url().$new[0].'/'.$acc_name."/".$new[2];
                    }
                    $url=$_SERVER["HTTP_REFERER"];
                    redirect($url);
                    //echo "<script>window.top.location.href='".$url."'</script>";
                }
                else 
                {
                    $reurl=$this->input->post('reurl');
                    $this->session->unset_userdata('reurl');
                    redirect($reurl);
                    //echo "<script>window.top.location.href='".$reurl."'</script>";  
                }    
                
            }
        }
    }*/
    public function check($Subdomain=NULL) 
    {
        
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $logindetails['current_user'] = $this->Add_attendee_model->check_login($this->input->post(),$event_templates[0]['Id']);
        $max_attempts = (int) ($event_templates[0]['no_of_login_attempts']-1);

        $logindetails['isLoggedIn']=true;
        $eid=$event_templates[0]['Id'];
        $acc=$this->Event_template_model->getAccname($eid);   
        $acc_name=$acc[0]['acc_name'];
        $attempts = $this->Add_attendee_model->getUserAttempts($logindetails['current_user'][0]->Id,'login_attempts');
        $current_date = date('Y-m-d');
        if (empty($logindetails['current_user']) || $logindetails['current_user']=="inactive") 
        {
            $this->session->unset_userdata('current_user');
            if($logindetails['current_user']=="inactive")
            {
                $this->session->set_flashdata('invlaidlogin_data', 'Your Account has Inactive');
            }
            else
            {
                $user = $this->Add_attendee_model->getUser($this->input->post('username'));
                $attempts = $this->Add_attendee_model->getUserAttempts($user['Id'],'login_attempts');
               
                if(!empty($attempts))
                {
                    $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                    $current_date = date('Y-m-d');
                    if( $compare_date == $current_date && $attempts['value'] > $max_attempts)
                    {
                       $this->session->set_flashdata('invlaidlogin_data', 'Your Account has been locked for today. Please try to login by tommorow.');
                    }
                    else
                    {
                         $this->session->set_flashdata('invlaidlogin_data', ($attempts['value'] == $max_attempts) ? 'This is your last attempt if it is not correct then this time your account will be locked out for 24 hours.': 'Invalid Username or password');
                    }
                    $this->Add_attendee_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
                }
                elseif(!empty($user))
                {
                    $this->Add_attendee_model->insertUserAttempts($user['Id'],'login_attempts');
                    $error['invalid_cred'] = array("error" => "login");
                    $this->session->set_flashdata('invlaidlogin_data', 'Invalid username or password');
                }     
                
            }
            if($this->input->post('reurl')=="")
            {
                redirect("App/".$acc_name."/".$Subdomain."");
            }
            else
            {
                $this->session->set_userdata('reurl',$this->input->post('reurl'));
                redirect("App/".$acc_name."/".$Subdomain."");
            }
        } 
        elseif(!empty($attempts) && ((date('Y-m-d',strtotime($attempts['created_date']))) == $current_date) && $attempts['value'] > $max_attempts)
        {
            $this->session->set_flashdata('invlaidlogin_data', 'Your Account has been locked for today. Please try to login by tommorow.');
            redirect("App/".$acc_name."/".$Subdomain."");
        }
        else 
        {

            if($this->input->post())
            {
                $sub = $this->Event_template_model->get_sub($this->input->post('Event_id'));
                $subdomain = $sub[0]['Subdomain'];
                $redirect = base_url().''.'App/'.$acc_name."/".$subdomain.'';
                $Organisor_id=$logindetails['current_user'][0]->Organisor_id;
               
                $acc_name=$this->Add_attendee_model->get_acc_name($Organisor_id);
                $logindetails['acc_name']=$acc_name;

                $this->session->set_userdata($logindetails);
                $user = $this->session->userdata('current_user');

                $arr=explode("/",$_SERVER["HTTP_REFERER"]);
                if($this->input->post('reurl')=="")
                {
                    if($arr[4]=='App')
                    {
                        $url=$_SERVER["HTTP_REFERER"];
                    }
                    else
                    {
                        $new=array_slice($arr, -3);
                        $url=base_url().$new[0].'/'.$acc_name."/".$new[2];
                    }
                    $url=$_SERVER["HTTP_REFERER"];
                    redirect($url);
                    //echo "<script>window.top.location.href='".$url."'</script>";
                }
                else 
                {
                    
                    $reurl=$this->input->post('reurl');
                    $this->session->unset_userdata('reurl');
                    redirect($reurl);
                    //echo "<script>window.top.location.href='".$reurl."'</script>";  
                }    
                
            }
        }
    }
   public function fblogin($event_name)
    {
        $img=$this->input->post('img');
        $gda="&__gda__=".$this->input->post('__gda__');
        $oa="&oe=".$this->input->post('oe');
        $name=$this->input->post('name');
        $email=$this->input->post('email');
        $img.=$oa;
        $img.=$gda;
        $logo="facebook_logo".strtotime(date("Y-m-d H:i:s"));
        copy($img,"./assets/user_files/".$logo);
        $logindetails['current_user']=$this->Add_attendee_model->check_exists_users($email,$name,$logo,$event_name);

        $event_templates = $this->Event_template_model->get_event_template_by_id_list($event_name);
        $eid=$event_templates[0]['Id'];

        $acc=$this->Event_template_model->getAccname($eid);
        
        $acc_name=$acc[0]['acc_name'];

        $logindetails['isLoggedIn']=true;
        $logindetails['acc_name']=$acc_name;
        $this->session->set_userdata($logindetails);
        //$redirect = base_url().''.'Events/'.$event_name.'';
        $user = $this->session->userdata('current_user');
        if(!empty($user))
        {
             echo "success#".$acc_name;
        }
       
       
    }
    public function checkemail($Subdomain=NULL)
    {         
        if($this->input->post())
        {
            $client = $this->Add_attendee_model->checkemail($this->input->post('email'),$this->input->post('idval'),$this->input->post('event_id'));
        

            if($client)
            {
                echo "error###Email already exist. Please choose another email.";
            }
            else
            {
                echo "success###";
            }
        }
        exit;
    }

    /*public function forgot_pas() 
    {
        $is_valid_email = $this->Add_attendee_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            echo "error###Invalid Email";die;
        } 
        else 
        {
            $new_pass = $this->generate_password();
            $slug = "FORGOT_PASSWORD";
            $intEventId = $this->input->post('intEvent');
            $em_template = $this->Setting_model->front_email_template($slug,$intEventId);
            $em_template = $em_template[0];
            $name = ucfirst(($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname);

            $msg = $em_template['Content'];
            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $new_pass;
            $msg = preg_replace($patterns, $replacements, $msg);

            $new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $new_pass_array['event_id']=$intEventId;
            $this->Login_model->change_pas($new_pass_array);
            if($em_template['Subject']=="")
            {
               $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'Registration@allintheloop.com';
            $config['smtp_pass']  = '*94-?CPKrwTM';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from("Registration@allintheloop.com", 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject); //'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            if(!$this->email->send())
            {
                echo "error###".$this->email->print_debugger();die;
            }

            echo "success###Please check your email for your temporary password.";die;
        }
    }*/
    public function forgot_pas($Subdomain=NULL) 
    {
        $Subdomain = $this->uri->segment(3);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $eid=$event_templates[0]['Id'];
        $acc=$this->Event_template_model->getAccname($eid);   
        $acc_name=$acc[0]['acc_name'];
        $is_valid_email = $this->Add_attendee_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            echo "error###Invalid Email";
        } 
        else 
        {
            $attempts = $this->Add_attendee_model->getUserAttempts($is_valid_email[0]->Id,'forgot_password');
            if(!empty($attempts))
            {
                $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                $current_date = date('Y-m-d');
                if( $compare_date == $current_date && $attempts['value'] > 2)
                {
                   echo "error###You have tried 3 times forgot password. Now you can make forgot password request by tommorow.";die; 
                }
            }
            else
            {
                $this->Add_attendee_model->insertUserAttempts($is_valid_email[0]->Id,'forgot_password');
            }
            $new_pass = $this->generate_password();

            $slug = "FORGOT_PASSWORD";
            $intEventId = $this->input->post('intEvent');
            $em_template = $this->Setting_model->front_email_template($slug,$intEventId);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = html_entity_decode($em_template['Content']);

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = "<a href='".base_url()."App/reset_password/".$acc_name."/".$Subdomain."/".$new_pass. "'>Click here to change your password.</a> ";
            $msg = preg_replace($patterns, $replacements, $msg);
            $this->Login_model->insertUserAttempts($is_valid_email[0]->Id,'reset_password',$new_pass);

            /*$new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $this->Login_model->change_pas($new_pass_array);*/
            if($em_template['Subject']=="")
            {
               $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
/*            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = 'IKIG{ADoF]*P';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
*/          
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }  
            $this->email->from($strFrom, 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject); //'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            if(!empty($attempts))
                $this->Add_attendee_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
            echo "success###We have sent a mail to your email address.";
        }
    }
    public function reset_password($token="")
    {
        $error =  $success = NULL;
        if($this->input->post())
        {
            date_default_timezone_set('UTC');
            $data['email'] = $this->input->post('username');
            $user = $this->Login_model->check_user_email($data);
            $attempts = $this->Login_model->getUserAttempts($user[0]->Id,'reset_password');

            $date = (int) strtotime(date('Y-m-d H:i:s'));
            $compare_date = (int) strtotime($attempts['created_date']);
            $diff = round(($date - $compare_date)/60,2);
            if($diff > 30)
            {
                $error = 'Your token is expired.';
            }
            else
            {
                $new_pass_array['email'] = $this->input->post('username');
                $new_pass_array['new_pass'] = $this->input->post('password');
                $this->Login_model->change_pas($new_pass_array);
                $this->Login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],"");
                $success = 'Your password changed successfully.';
            }
        }
        if($error!=NULL)
        {
            $this->data['error'] = $error;
        }
        if($success!=NULL)
        {
            $this->data['success'] = $success;
        }
        $this->data['token'] = $token;
        //$this->template->set_template('front_template');
        $this->template->write('pagetitle', 'Reset Password');
        $this->template->write_view('content', 'login/attendee_reset_pass', $this->data, true);
        $this->template->write_view('js', 'login/js', $this->data, true);
        $this->template->render();
        
    }
    public function generate_password() 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 9; $i++) 
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
    
    public function logout($Subdomain) 
    {
        $this->session->unset_userdata('current_user');
        $this->session->unset_userdata('invalid_cred');
        $acc_name=$this->session->userdata('acc_name');
        //$this->session->sess_destroy();
        
        /*$sess_array = $this->session->all_userdata();

        foreach($sess_array as $key =>$val){
            if($key!='session_id'||$key!='last_activity'||$key!='admin_id'){
               $this->session->unset_userdata($key);
            }
        }*/
        
      
        redirect("App/".$acc_name."/".$Subdomain);
    }
    /*public function add_registration_screen_user($acc_name,$Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) 
        {
            $this->session->set_flashdata('registration_screen_error',"Please Enter Proper Email Address.");
            redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
        }
        else
        {

            $client = $this->Add_attendee_model->checkemail($this->input->post('email'),$this->input->post('idval'),$event_templates[0]['Id']);
            if(!$client)
            {
                $insertrecordstatus='1';
                $badgesdata=$this->Attendee_model->get_badges_values_by_event($event_templates[0]['Id']);
                $type_color=json_decode($badgesdata[0]['attendee_type_color'],true);
                $color= !empty($type_color[$this->input->post('attendee_type')]) ? $type_color[$this->input->post('attendee_type')] : '#000000';
                $registration_screen=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);

                if($registration_screen[0]['payment_type']=='2')
                {
                    if(!empty($this->input->post('stripeToken')))
                    {
                        if(!class_exists('\Stripe\Stripe'))
                        {
                          require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
                        }
                        \Stripe\Stripe::setApiKey($registration_screen[0]['stripe_secret_key']);
                        $payment =  \Stripe\Charge::create(
                          array(
                          "amount" => $registration_screen[0]['stripe_payment_price'] * 100,
                          "currency" => $registration_screen[0]['stripe_payment_currency'],
                          "source" => $this->input->post('stripeToken'),
                          "description" => "All In The Loop"
                          )
                        );
                        if($payment['status']=="succeeded")
                        {
                            $insertrecordstatus='1';
                        }
                        else
                        {
                            $this->session->set_flashdata('registration_screen_error',"Something Went Worng.");
                            redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('registration_screen_error',"Missing Stripe Token..");
                        redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                    }
                }
                else
                {
                    $insertrecordstatus='1';
                }
                if($insertrecordstatus=='1')
                {
                    $user=$this->Add_attendee_model->save_registration_screen_user($this->input->post(),$event_templates[0]['Id'],$event_templates[0]['Organisor_id']);
                    if($registration_screen[0]['send_pdf']=='1')
                    {
                        if($badgesdata[0]['code_type']!='1')
                        {
                            $this->load->library('Ciqrcode');
                            //$params['data'] = $user['Email'].'##'.rand('111111','999999');
                            $params['data'] = $event_templates[0]['Id'].'-'.$user['Id'];
                            $params['level'] = 'M';
                            $params['size'] = 2;
                            $params['savename'] = 'tes.png';
                            $this->ciqrcode->generate($params);
                        }
                        else
                        {
                            $this->load->library('zend');
                            $this->zend->load('Zend/Barcode');
                            $imageResource=Zend_Barcode::factory('code128', 'image', array('text'=>$event_templates[0]['Id'].'-'.$user['Id'],'drawText' => false,'barHeight'=> 50,'factor'=>3), array('barHeight'=> 50,'factor'=>3))->draw();
                            imagepng($imageResource, './assets/user_files/Barcode.png');
                        }
                        require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
                        require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');
                        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
                        //$pdf->SetFont('lato', '', 10);
                        $pdf->SetTitle('Badge Design Final - Visitor');
                        $pdf->AddPage();
                        $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th colspan="4" height="10"></th>
                                        </tr>
                                        <tr>
                                            <th width="2%"></th>
                                            <th width="58%" align="left" style="vertical-align:top;">';
                                            if(!empty($badgesdata[0]['badges_logo_images'])){
                                            $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                            }
                                            $htm.='</th>
                                            <th width="38%" align="right" style="margin-top:5%;">';
                                            if($registration_screen[0]['send_qr_code']=='1')
                                            {
                                                if($badgesdata[0]['code_type']!='1')
                                                {
                                                    $htm.='<img src="tes.png"/>';
                                                }
                                                else
                                                {
                                                    $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                                }
                                            }
                                            $htm.='</th>
                                            <th width="2%" align="right"></th>
                                        </tr>
                                        <tr style="padding-bottom:15px;">
                                            <th width="3%"></th>
                                            <th width="97%" colspan="3">
                                                <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                                <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                                <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                                <font size="10">'.ucfirst($user['Title']).'</font><br>
                                                <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" height="20"></th>
                                        </tr>';
                                        if($registration_screen[0]['show_attendee_type']=='1'){
                                            $htm.='<tr style=""><th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($this->input->post('attendee_type')).'</th></tr>';
                                        }
                                    $htm.='</table>
                                </td>
                                <td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th colspan="4" height="10"></th>
                                        </tr>
                                        <tr>
                                            <th width="2%"></th>
                                            <th width="58%" align="left" style="vertical-align:top;">';
                                            if(!empty($badgesdata[0]['badges_logo_images'])){
                                            $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                            }
                                            $htm.='</th>
                                            <th width="38%" align="right" style="margin-top:5%;">';
                                            if($registration_screen[0]['send_qr_code']=='1')
                                            {
                                                if($badgesdata[0]['code_type']!='1')
                                                {
                                                    $htm.='<img src="tes.png"/>';
                                                }
                                                else
                                                {
                                                    $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                                }
                                            }
                                            $htm.='</th>
                                            <th width="2%" align="right"></th>
                                        </tr>
                                        <tr style="padding-bottom:15px;">
                                            <th width="3%"></th>
                                            <th width="97%" colspan="3">
                                                <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                                <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                                <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                                <font size="10">'.ucfirst($user['Title']).'</font><br>
                                                <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" height="20"></th>
                                        </tr>';
                                        if($registration_screen[0]['show_attendee_type']=='1')
                                        {
                                            $htm.='<tr style=""><th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($this->input->post('attendee_type')).'</th></tr>';
                                        }
                                    $htm.='</table>
                                </td>
                            </tr>
                            </table>';
                        $pdf->writeHTML($htm);
                        file_put_contents('./assets/user_files/badges_files.pdf',$pdf->Output('badges_files.pdf', 'S'));
                    }    
                    $Subject=!empty($badgesdata[0]['subject']) ? $badgesdata[0]['subject'] : "Welcome Message";
                    $sender_name=!empty($badgesdata[0]['sender_name']) ? $badgesdata[0]['sender_name'] : 'All In The Loop';
                    $msg=$badgesdata[0]['email_body'];
                    $patterns = array();
                    $patterns[0] = '/{{name}}/';
                    $replacements = array();
                    $replacements[0] = ucfirst($user['Firstname']).' '.$user['Lastname'];
                    $msg = preg_replace($patterns, $replacements, $msg);
                    $config['protocol']   = 'smtp';
                    $config['smtp_host']  = 'localhost';
                    $config['smtp_port']  = '25';
                    $config['smtp_user']  = 'invite@allintheloop.com';
                    $config['smtp_pass']  = 'xHi$&h9M)x9m';
                    $config['_encoding'] = 'base64';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from('invite@allintheloop.com',$sender_name);
                    $this->email->to($user['Email']);
                    //$this->email->to('gaurang@xhtmljunkies.com');
                    $this->email->subject($Subject);
                    $this->email->message($msg);
                    if($registration_screen[0]['send_pdf']=='1')
                    {
                        $this->email->attach('./assets/user_files/badges_files.pdf');
                    }  
                    $this->email->send();
                    $this->email->clear(TRUE);
                    if($registration_screen[0]['send_pdf']=='1')
                    {
                        unlink('./assets/user_files/badges_files.pdf');
                    }
                    if($badgesdata[0]['code_type']=='1' && $registration_screen[0]['send_qr_code']=='1')
                    {
                        unlink('./assets/user_files/Barcode.png');
                    }
                    if($registration_screen[0]['email_notify_admin']=='1')
                    {
                        $org_email=$this->Event_template_model->get_organizer_email_by_id($event_templates[0]['Organisor_id']);
                        $adminmsg=ucfirst($user['Firstname']).' '.$user['Lastname']." has registered for ".ucfirst($event_templates[0]['Event_name']);
                        $config['protocol']   = 'smtp';
                        $config['smtp_host']  = 'localhost';
                        $config['smtp_port']  = '25';
                        $config['smtp_user']  = 'invite@allintheloop.com';
                        $config['smtp_pass']  = 'xHi$&h9M)x9m';
                        $config['_encoding'] = 'base64';
                        $config['mailtype'] = 'html';
                        $this->email->initialize($config);
                        $this->email->from('invite@allintheloop.com','All In The Loop');
                        $this->email->to($org_email['Email']);
                        $this->email->subject('New Registration');
                        $this->email->message($adminmsg);
                        $this->email->send();
                        $this->email->clear(TRUE);
                    }
                    $this->session->set_flashdata('registration_screen_success',"Thank you for registering");
                    redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                }
            }
            else
            {
                $this->session->set_flashdata('registration_screen_error',"Email already exist. Please choose another email.");
                redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
            }
        }
    }
    public function multi_user_save_in_temp($acc_name,$Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $registration_screen=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);
        $coupon_code=$registration_screen[0]['coupon_code'];
        $arrpost=$this->input->post(); 
        $stripe_price=$registration_screen[0]['stripe_payment_price'] * $this->input->post('numberuser');
        $stripeToken=$this->input->post('stripeToken');
        unset($arrpost['stripeToken']);
        $pay_by_invoice=$this->input->post('pay_by_invoice');
        unset($arrpost['pay_by_invoice']);
        $choose_agenda=array();
        $arrayprice=array();
        $tempemails=array();
        for ($i=1;$i<=$this->input->post('numberuser');$i++) 
        { 
            array_push($tempemails,$this->input->post('Email__'.$i));
        }
        $this->Add_attendee_model->delete_another_user_data($event_templates[0]['Id'],$tempemails);
        for($i=1; $i <=$this->input->post('numberuser'); $i++) {
            $temp_uid=$this->input->post('temp_user_id_'.$i);
            unset($arrpost['temp_user_id_'.$i]);
            $userdata['first_name']=$this->input->post('FirstName__'.$i); 
            unset($arrpost['FirstName__'.$i]);
            $userdata['last_name']=$this->input->post('LastName__'.$i); 
            unset($arrpost['LastName__'.$i]);
            $userdata['email']=$this->input->post('Email__'.$i); 
            unset($arrpost['Email__'.$i]);
            $userdata['company_name']=$this->input->post('Company_Name__'.$i); 
            unset($arrpost['Company_Name__'.$i]);
            $userdata['title']=$this->input->post('Title__'.$i); 
            unset($arrpost['Title__'.$i]);
            $userdata['attendee_type']=$this->input->post('attendee_type__'.$i); 
            unset($arrpost['attendee_type__'.$i]);

            if(in_array($this->input->post('choose_agenda__'.$i),$choose_agenda))
            {
                $numberofuser[$this->input->post('choose_agenda__'.$i)]=$numberofuser[$this->input->post('choose_agenda__'.$i)]+1;
            }
            else
            {
                array_push($choose_agenda,$this->input->post('choose_agenda__'.$i));
                $numberofuser[$this->input->post('choose_agenda__'.$i)]=1;  
            }
            if(!empty($this->input->post('choose_agenda__'.$i))){
                if(in_array('June 28 Networking Reception 5:00pm - 7:00pm ($50)',$this->input->post('choose_agenda__'.$i)) && in_array('June 29 Speed Networking Breakfast 8:00am - 9:30am ($99)',$this->input->post('choose_agenda__'.$i)))
                {
                    array_push($arrayprice,249);
                }
                else
                {
                    if(in_array('June 28 Networking Reception 5:00pm - 7:00pm ($50)',$this->input->post('choose_agenda__'.$i)))
                    {
                        array_push($arrayprice,150);
                    }
                    if(in_array('June 29 Speed Networking Breakfast 8:00am - 9:30am ($99)',$this->input->post('choose_agenda__'.$i)))
                    {
                        array_push($arrayprice,199);
                    }
                }
            }
            else
            {
                array_push($arrayprice,100);
            }
            if($this->input->post('numberuser') <= 2)
            {
                if(trim($this->input->post('choose_agenda__'.$i))=="Foresight Pass (Includes Tech on Tap Party Pass) - $249")
                {
                    if($this->input->post('coupon_code__'.$i)==$coupon_code && date('Y-m-d') <= "2017-06-30")
                    {
                        $price[$i]=225;
                    }
                    else
                    {
                        $price[$i]=249;   
                    }
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Innovation Pass - $149")
                {
                    $price[$i]=149;
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Innovation Pass + Tech on Tap Party Pass - $198")
                {
                    $price[$i]=198;   
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Technology Pass - $99")
                {
                    $price[$i]=99;   
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Technology Pass + Tech on Tap Party Pass - $148")
                {
                    $price[$i]=148;   
                }
                else
                {
                    $price[$i]=49;      
                }
            }
            else
            {
                if(trim($this->input->post('choose_agenda__'.$i))=="Foresight Pass (Includes Tech on Tap Party Pass) - $249")
                {
                    $price[$i]=249;   
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Innovation Pass - $149")
                {
                    $price[$i]=149;
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Innovation Pass + Tech on Tap Party Pass - $198")
                {
                    $price[$i]=198;   
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Technology Pass - $99")
                {
                    $price[$i]=99;   
                }
                else if(trim($this->input->post('choose_agenda__'.$i))=="Technology Pass + Tech on Tap Party Pass - $148")
                {
                    $price[$i]=148;   
                }
                else
                {
                    $price[$i]=49;      
                }
            }
            unset($arrpost['saveagenda__'.$i]);
            $extra_info=array();
            foreach ($arrpost as $key => $value) {
                if(strpos($key,'__'.$i) !== false)
                {
                    if(is_array($value))
                    {
                        $extra_info[str_ireplace(array('__'.$i,'_'),array('',' '),$key)]=implode(",",$value);
                    }
                    else
                    {
                        $extra_info[str_ireplace(array('__'.$i,'_'),array('',' '),$key)]=$value;
                    }
                }
            }
            if(!array_key_exists('Pay By Invoice', $extra_info))
            {
                if($pay_by_invoice=='1')
                {
                    $extra_info['Pay By Invoice']='TRUE';
                }
                else
                {
                    $extra_info['Pay By Invoice']='FALSE';   
                }
            }
            $userdata['extra_info']=json_encode($extra_info);
            $userdata['event_id']=$event_templates[0]['Id'];
            $this->Add_attendee_model->save_multiple_user_registration($userdata,$temp_uid);
        }
        if($pay_by_invoice=='1')
        {
            header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain.'?UMstatus=Approved');
        }
        else
        {   
            if($registration_screen[0]['payment_type']=='1')
            {
                $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
                //$merchantAuthentication->setName("2pD7Hkr9ZghH");
                //$merchantAuthentication->setTransactionKey("3tV346Nn3xCg36QH");

                $merchantAuthentication->setName("4w2pJE9X");
                $merchantAuthentication->setTransactionKey("9Nba26m628grNjSq");

                $refId = 'ref' . time();

                $transactionRequestType = new AnetAPI\TransactionRequestType();
                $transactionRequestType->setTransactionType("authCaptureTransaction"); 
                $transactionRequestType->setAmount(array_sum($arrayprice));

                $setting1 = new AnetAPI\SettingType();
                $setting1->setSettingName("hostedPaymentButtonOptions");
                $setting1->setSettingValue(json_encode(array('text'=>'Pay')));

                $setting2 = new AnetAPI\SettingType();
                $setting2->setSettingName("hostedPaymentOrderOptions");
                $setting2->setSettingValue(json_encode(array('show'=>true)));

                $setting3 = new AnetAPI\SettingType();
                $setting3->setSettingName("hostedPaymentReturnOptions");
                $setting3->setSettingValue(json_encode(array('url'=>base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain.'?UMstatus=Approved','cancelUrl'=>base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain,'showReceipt'=>true)));  

                $request = new AnetAPI\GetHostedPaymentPageRequest();
                $request->setMerchantAuthentication($merchantAuthentication);
                $request->setTransactionRequest($transactionRequestType);

                $request->addToHostedPaymentSettings($setting1);
                $request->addToHostedPaymentSettings($setting2);
                $request->addToHostedPaymentSettings($setting3);
                
                //execute request
                $controller = new AnetController\GetHostedPaymentPageController($request);
                $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
                if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
                {
                    $this->data['Paymenttoken']=$response->getToken();  
                    $this->data['Price']=array_sum($arrayprice);
                    $this->data['event_id']=$event_templates[0]['Id'];
                    $this->data['registration_screen']=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);
                    $this->data['Subdomain'] = $Subdomain;
                    $this->data['acc_name'] = $acc_name;
                    $this->load->view('events/multi_forms_views', $this->data);
                }
                else
                {
                    $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
                    $errorMessages = $response->getMessages()->getMessage();
                    $this->session->set_flashdata('Token_Error',$errorMessages[0]->getText());
                    redirect(base_url()."App/".$acc_name."/".$Subdomain."/show_multi_registration");
                }        
            }
            else if($registration_screen[0]['payment_type']=='2')
            {
                if(!empty($stripeToken))
                {
                    if(!class_exists('\Stripe\Stripe'))
                    {
                      require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
                    }
                    \Stripe\Stripe::setApiKey($registration_screen[0]['stripe_secret_key']);
                    $payment =  \Stripe\Charge::create(
                      array(
                      "amount" => $stripe_price * 100,
                      "currency" => $registration_screen[0]['stripe_payment_currency'],
                      "source" => $stripeToken,
                      "description" => "All In The Loop"
                      )
                    );
                    if($payment['status']=="succeeded")
                    {
                        header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain.'?UMstatus=Approved');
                    }
                    else
                    {
                        header('Location:'.base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain);
                    }
                }
                else
                {
                    header('Location:'.base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain);
                }
            }
            else
            {
                $htmcomment='';
                foreach($choose_agenda as $key => $value) {
                    if($key+1==count($choose_agenda))
                    {
                        $htmcomment.=$value." * ".$numberofuser[$value];
                    }
                    else
                    {
                        $htmcomment.=$value." * ".$numberofuser[$value]." , ";
                    }
                } 
                if($this->input->post('numberuser') >=3 && $this->input->post('numberuser')<=4)
                {
                    $discont=(array_sum($price)*10)/100;
                    $amount=array_sum($price)-$discont;
                }
                else if($this->input->post('numberuser') >=5)
                {
                    $discont=(array_sum($price)*15)/100;
                    $amount=array_sum($price)-$discont;   
                }
                else
                {
                    $amount=array_sum($price);
                }
                $query_data=array(
                    'UMkey'=>"5g11meSh51Y4l5xr2XBbVJ8cXoBP9Bii",
                    'UMcommand'=>"sale",
                    'UMamount'=>$amount,
                    'UMinvoice'=>'1',
                    'UMredirApproved'=>base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain,
                    'UMredirDeclined'=>base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain,
                    'UMtestmode'=>true,
                    'UMdescription'=>$htmcomment,
                    'UMcomments'=>$htmcomment
                    );
                header('Location:https://secure.ebizcharge.com/interface/epayform/?'.http_build_query($query_data));
            }    
        } 
    }
    public function payment_success_save_user($acc_name,$Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        if($_GET['UMstatus']=="Approved")
        {
            $userdata=$this->Add_attendee_model->get_all_temp_multiuser_by_event($event_templates[0]['Id']);
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'Registration@allintheloop.com';
            $config['smtp_pass']  = '*94-?CPKrwTM';
            $config['_encoding'] = 'base64';
            $config['mailtype'] = 'html';
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "smtp.mandrillApp.com";
            $config['smtp_port'] = 587;
            $config['smtp_user'] = "All In The Loop"; 
            $config['smtp_pass'] = "oD9B-JahFNNrJq2Sy9hkGg";
            $config['charset'] = "utf-8";
            $config['_encoding'] = 'base64';
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);
            if($badgesdata[0]['code_type']!='1')
            {
                $this->load->library('Ciqrcode');
            }
            else
            {
                $this->load->library('zend');
                //load in folder Zend
                $this->zend->load('Zend/Barcode');
            }
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
            require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');
            $badgesdata=$this->Attendee_model->get_badges_values_by_event($event_templates[0]['Id']);
            $registration_screen=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);
            $org_email=$this->Event_template_model->get_organizer_email_by_id($event_templates[0]['Organisor_id']);
            foreach ($userdata as $key => $value) {
                $saveuserdata['first_name']=$value['first_name']; 
                $saveuserdata['last_name']=$value['last_name']; 
                $saveuserdata['email']=$value['email']; 
                $saveuserdata['company_name']=$value['company_name']; 
                $saveuserdata['title']=$value['title'];  
                $user=$this->Add_attendee_model->save_registration_screen_user($saveuserdata,$event_templates[0]['Id'],$event_templates[0]['Organisor_id']);
                $attendee_type=!empty($value['attendee_type']) ? $value['attendee_type'] : 'Visitor';
                $type_color=json_decode($badgesdata[0]['attendee_type_color'],true);
                $color= !empty($type_color[$attendee_type]) ? $type_color[$attendee_type] : '#000000';
                if($registration_screen[0]['send_pdf']=='1')
                {
                    if($badgesdata[0]['code_type']!='1')
                    {
                        //$params['data'] = $user['Email'].'##'.rand('111111','999999');
                        $params['data'] = $event_templates[0]['Id'].'-'.$user['Id'];
                        $params['level'] = 'M';
                        $params['size'] = 2;
                        $params['savename'] = 'tes.png';
                        $this->ciqrcode->generate($params);
                    }
                    else
                    {
                        $imageResource=Zend_Barcode::factory('code128', 'image', array('text'=>$event_templates[0]['Id'].'-'.$user['Id'],'drawText' => false,'barHeight'=> 50,'factor'=>3), array('barHeight'=> 50,'factor'=>3))->draw();
                        imagepng($imageResource, './assets/user_files/Barcode.png');
                    }
                    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
                    $pdf->SetTitle('Badge Design Final - Visitor');
                    $pdf->AddPage();
                    $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th colspan="4" height="10"></th>
                                    </tr>
                                    <tr>
                                        <th width="2%"></th>
                                        <th width="58%" align="left" style="vertical-align:top;">';
                                        if(!empty($badgesdata[0]['badges_logo_images'])){
                                        $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                        }
                                        $htm.='</th>
                                        <th width="38%" align="right" style="margin-top:5%;">';
                                        if($registration_screen[0]['send_qr_code']=='1')
                                        {
                                            if($badgesdata[0]['code_type']!='1')
                                            {
                                                $htm.='<img src="tes.png"/>';
                                            }
                                            else
                                            {
                                                $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                            }
                                        }
                                        $htm.='</th>
                                        <th width="2%" align="right"></th>
                                    </tr>
                                    <tr style="padding-bottom:15px;">
                                        <th width="3%"></th>
                                        <th width="97%" colspan="3">
                                            <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                            <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                            <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                            <font size="10">'.ucfirst($user['Title']).'</font><br>
                                            <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="4" height="20"></th>
                                    </tr>';
                                    if($registration_screen[0]['show_attendee_type']=='1')
                                    {
                                        $htm.='<tr style=""><th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th></tr>';
                                    }
                                $htm.='</table>
                            </td>
                            <td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th colspan="4" height="10"></th>
                                    </tr>
                                    <tr>
                                        <th width="2%"></th>
                                        <th width="58%" align="left" style="vertical-align:top;">';
                                        if(!empty($badgesdata[0]['badges_logo_images'])){
                                        $htm.='<img width="504" height="158" src="'.base_url().'assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                        }
                                        $htm.='</th>
                                        <th width="38%" align="right" style="margin-top:5%;">';
                                        if($registration_screen[0]['send_qr_code']=='1')
                                        {
                                            if($badgesdata[0]['code_type']!='1')
                                            {
                                                $htm.='<img src="tes.png"/>';
                                            }
                                            else
                                            {
                                                $htm.='<img src="'.base_url().'assets/user_files/Barcode.png"/>';
                                            }
                                        }
                                        $htm.='</th>
                                        <th width="2%" align="right"></th>
                                    </tr>
                                    <tr style="padding-bottom:15px;">
                                        <th width="3%"></th>
                                        <th width="97%" colspan="3">
                                            <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                            <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                            <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                            <font size="10">'.ucfirst($user['Title']).'</font><br>
                                            <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="4" height="20"></th>
                                    </tr>';
                                    if($registration_screen[0]['show_attendee_type']=='1')
                                    {
                                        $htm.='<tr style=""><th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th></tr>';
                                    }
                                $htm.='</table>
                            </td>
                        </tr>
                        </table>';
                    $pdf->writeHTML($htm);
                    file_put_contents('./assets/user_files/badges_files.pdf',$pdf->Output('badges_files.pdf', 'S'));
                    unset($htm);
                }    
                $Subject=!empty($badgesdata[0]['subject']) ? $badgesdata[0]['subject'] : "Welcome Message";
                $sender_name=!empty($badgesdata[0]['sender_name']) ? $badgesdata[0]['sender_name'] : 'All In The Loop';
                $msg=$badgesdata[0]['email_body'];
                $patterns = array();
                $patterns[0] = '/{{name}}/';
                $replacements = array();
                $replacements[0] = ucfirst($user['Firstname']).' '.$user['Lastname'];
                $msg = preg_replace($patterns, $replacements, $msg);
               
                $this->email->from('Registration@allintheloop.com',$sender_name);
                $this->email->to($user['Email']);
                //$this->email->to('gaurang@xhtmljunkies.com');
                $this->email->subject($Subject);
                $this->email->message($msg);
                if($registration_screen[0]['send_pdf']=='1')
                {
                    $this->email->attach('./assets/user_files/badges_files.pdf');
                }  
                $this->email->send();
                $this->email->clear(TRUE);
                if($registration_screen[0]['send_pdf']=='1')
                {
                    unlink('./assets/user_files/badges_files.pdf');
                }
                if($badgesdata[0]['code_type']=='1' && $registration_screen[0]['send_qr_code']=='1')
                {
                    unlink('./assets/user_files/Barcode.png');
                }
                if($registration_screen[0]['email_notify_admin']=='1')
                {
                    $adminmsg=ucfirst($user['Firstname']).' '.$user['Lastname']." has registered for ".ucfirst($event_templates[0]['Event_name']);
                    $config['protocol']   = 'smtp';
                    $config['smtp_host']  = 'localhost';
                    $config['smtp_port']  = '25';
                    $config['smtp_user']  = 'invite@allintheloop.com';
                    $config['smtp_pass']  = 'xHi$&h9M)x9m';
                    $config['_encoding'] = 'base64';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from('invite@allintheloop.com','All In The Loop');
                    $this->email->to($org_email['Email']);
                    $this->email->subject('New Registration');
                    $this->email->message($adminmsg);
                    $this->email->send();
                    $this->email->clear(TRUE);
                }
                unset($pdf);
            }
            $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
            $this->session->set_flashdata('registration_screen_success',"Thank you for registering");
            if($event_templates[0]['Id']=='662')
            {
                header('location: http://www.allintheloop.com/sightline-confirm.html');
            }
            else
            {
                header('location: http://allintheloop.com/registration-confirm.html');
            }
        }
        else
        {
            $this->session->set_flashdata('registration_screen_error',"Something Went Worng...<br/>".$_GET['UMerror']);
            redirect(base_url()."App/".$acc_name."/".$Subdomain."/show_multi_registration");
        }
    }
    public function payment_failed_show_error_msg($acc_name,$Subdomain)
    {   
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
        $this->session->set_flashdata('registration_screen_error',"Something Went Worng...<br/>".$_GET['UMerror']);
        redirect(base_url()."App/".$acc_name."/".$Subdomain."/show_multi_registration");
    }
    public function checkmultiuseremail($Subdomain=NULL)
    {         
        if($this->input->post())
        {
            $event_id=$this->input->post('event_id');
            $emails=array_values($this->input->post());
            $client = $this->Add_attendee_model->checkemail($emails[0],NULL,$event_id);
            if($client)
            {
                echo "false";
            }
            else
            {
                echo "true";
            }
        }
        exit;
    }*/
    public function registraion_user_save_in_temp($acc_name,$Subdomain)
    {   

        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $event_id=$event_templates[0]['Id'];
        $registration_screen=$this->Attendee_model->get_registration_screen_data($event_id,null);
        $arrpost=$this->input->post();
        $client = $this->Add_attendee_model->checkemail($arrpost['Email'],NULL,$event_id);
        if($this->input->post('update_user'))
        {   
            $id = $this->input->post('update_user');
            $update['Firstname'] = $arrpost['First_Name'];
            $update['Lastname'] = $arrpost['Last_Name'];
            $update['Company_name'] = $arrpost['Company'];
            $update['Title'] = $arrpost['Title'];
            $savesession = array();
            foreach ($arrpost['question_ids'] as $key => $value)
            {
                $question=$this->Add_attendee_model->get_reg_question($value);
                $extra_column[$question['question']]=!empty($this->input->post('question_option'.$value)) ? $this->input->post('question_option'.$value) : NULL;

                $question_info[$key]['question_id']=$value;
                $question_info[$key]['answer']= !empty($this->input->post('question_option'.$value)) ? $this->input->post('question_option'.$value) : NULL;
                //$savesession.=implode(",",$this->input->post('savesession'.$value));
                if(!empty($this->input->post('savesession'.$value)))
                {
                    $savesession = array_merge($savesession,$this->input->post('savesession'.$value));
                }
            }
            if($event_id == '1114')
            {
                $this->Add_attendee_model->update_user_data($id,$update,$question_info,$event_id,$extra_column);
                header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain.'/'.$id);
            }
            else
            {
                $this->Add_attendee_model->update_user_data($id,$update,$question_info,$event_id,$extra_column);
                $this->session->set_flashdata('reg_user_update_success','Registration User info updated');
                redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
            }
        }
        else
        {

            if(!$client)
            {   
                $tempuserdata['first_name']=$arrpost['First_Name'];
                $tempuserdata['last_name']=$arrpost['Last_Name'];
                $tempuserdata['email']=trim($arrpost['Email']);
                $tempuserdata['company_name']=$arrpost['Company'];
                $tempuserdata['title']=$arrpost['Title'];
                $tempuserdata['event_id']=$event_id;
                $question_info=array();
                $extra_column=array();
                $savesession=array();
                foreach ($arrpost['question_ids'] as $key => $value) {
                    $question=$this->Add_attendee_model->get_reg_question($value);
                    $extra_column[$question['question']]=!empty($this->input->post('question_option'.$value)) ? $this->input->post('question_option'.$value) : NULL;

                    $question_info[$key]['question_id']=$value;
                    $question_info[$key]['answer']= !empty($this->input->post('question_option'.$value)) ? $this->input->post('question_option'.$value) : NULL;
                    //$savesession .=implode(",",$this->input->post('savesession'.$value));
                    if(!empty($this->input->post('savesession'.$value)))
                    {
                        $savesession = array_merge($savesession,$this->input->post('savesession'.$value));
                    }
                }
                $savesession = implode(",",$savesession);
                $tempuserdata['save_agenda_ids']=$savesession;   
                $tempuserdata['extra_column']=json_encode($extra_column);
                $tempuserdata['question_info']=json_encode($question_info);
                
                $this->Add_attendee_model->save_registration_temp_user($tempuserdata);
                if($arrpost['discount-per'] == '100')
                {
                    header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain);
                }
                else
                {
                    if(!empty($arrpost['price']) && $registration_screen[0]['show_payment_screen']=='1')
                    {   
                        if($arrpost['pay_by_invoice']=='1')
                        {   
                            header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain);
                        }
                        else
                        {
                            if($registration_screen[0]['payment_type'] == '0' && $registration_screen[0]['stripe_payment_currency'] == 'AED' && $registration_screen[0]['stripe_public_key']!='')
                            {
                                $registration_screen[0]['payment_type'] = '2';
                            }
                            elseif($registration_screen[0]['stripe_payment_currency'] == 'AED' && $registration_screen[0]['stripe_public_key']!='')
                            {

                                $this->session->set_flashdata('registration_screen_error',"Currency AED is not supported to paypal.");
                                redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                            }

                            if($registration_screen[0]['payment_type']=='0')
                            {
                                $paypal_details = array(
                                    'API_username' => $registration_screen[0]['paypal_api_username'], 
                                    'API_signature' => $registration_screen[0]['paypal_api_signature'], 
                                    'API_password' => $registration_screen[0]['paypal_api_password'],
                                    'sandbox_status' => false
                                );
                                $this->load->library('paypal_ec', $paypal_details);
                                $to_buy = array(
                                    'desc' => 'Attendee Registration', 
                                    'currency' => strtoupper($registration_screen[0]['stripe_payment_currency']), 
                                    'type' => 'Sale', 
                                    'return_URL' => base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain, 
                                    'cancel_URL' => base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain,
                                    'shipping_amount' =>$arrpost['price'], 
                                    'get_shipping' => true
                                );
                                $set_ec_return = $this->paypal_ec->set_ec($to_buy);
                                if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) 
                                {
                                    $this->paypal_ec->redirect_to_paypal($set_ec_return['TOKEN']);
                                } 
                                else 
                                {
                                    header('Location:'.base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain);
                                    //$this->_error($set_ec_return);
                                }
                                /*$query_data=array(
                                    'business'=>$registration_screen[0]['paypal_email'],
                                    'cmd'=>'_xclick',
                                    'item_name' => 'Attendee Registration',
                                    'amount'=>$arrpost['price'],
                                    'currency_code'=>strtoupper($registration_screen[0]['stripe_payment_currency']),
                                    'return'=>base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain,
                                    'notify_url'=>base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain,
                                    'cancel_url'=>base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain
                                );
                                //header('Location:https://www.paypal.com/cgi-bin/webscr?'.http_build_query($query_data));
                                header('Location:https://www.sandbox.paypal.com/cgi-bin/webscr?'.http_build_query($query_data));*/
                            }
                            else if($registration_screen[0]['payment_type']=='1')
                            {

                                $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
                                $merchantAuthentication->setName($registration_screen[0]['authorise_api_name']);
                                $merchantAuthentication->setTransactionKey($registration_screen[0]['authorise_transaction_key']);

                                $refId = 'ref' . time();

                                $transactionRequestType = new AnetAPI\TransactionRequestType();
                                $transactionRequestType->setTransactionType("authCaptureTransaction"); 
                                $transactionRequestType->setAmount($arrpost['price']);
                                $transactionRequestType->setCurrencyCode($registration_screen[0]['stripe_payment_currency']);

                                $setting1 = new AnetAPI\SettingType();
                                $setting1->setSettingName("hostedPaymentButtonOptions");
                                $setting1->setSettingValue(json_encode(array('text'=>'Pay')));

                                $setting2 = new AnetAPI\SettingType();
                                $setting2->setSettingName("hostedPaymentOrderOptions");
                                $setting2->setSettingValue(json_encode(array('show'=>true)));

                                $setting3 = new AnetAPI\SettingType();
                                $setting3->setSettingName("hostedPaymentReturnOptions");
                                $setting3->setSettingValue(json_encode(array('url'=>base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain,'cancelUrl'=>base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain,'showReceipt'=>true)));  

                                $request = new AnetAPI\GetHostedPaymentPageRequest();
                                $request->setMerchantAuthentication($merchantAuthentication);
                                $request->setTransactionRequest($transactionRequestType);

                                $request->addToHostedPaymentSettings($setting1);
                                $request->addToHostedPaymentSettings($setting2);
                                $request->addToHostedPaymentSettings($setting3);
                                
                                //execute request
                                $controller = new AnetController\GetHostedPaymentPageController($request);
                                $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
                                if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
                                {
                                    $this->data['Paymenttoken']=$response->getToken();  
                                    $this->data['Price']=$arrpost['price'];      
                                    $this->data['event_id']=$event_templates[0]['Id'];
                                    $this->data['registration_screen']=$this->Attendee_model->get_registration_screen_data($event_templates[0]['Id'],null);

                                    $countrylist = $this->Profile_model->countrylist();
                                    $this->data['countrylist'] = $countrylist;
                                    $this->data['stages']=$this->Event_template_model->get_all_stage_data_by_event($event_templates[0]['Id']);
                                    $this->data['discount_code']=$this->Event_template_model->get_discount_code_for_all($event_templates[0]['Id']);
                                    $this->data['Subdomain'] = $Subdomain;
                                    $this->data['acc_name'] = $acc_name;
                                    $this->load->view('events/new_attendee_registrationscreen', $this->data);    
                                }
                                else
                                {
                                    $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
                                    $errorMessages = $response->getMessages()->getMessage();
                                    $this->session->set_flashdata('Token_Error',$errorMessages[0]->getText());
                                    redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                                } 
                            }
                            else if($registration_screen[0]['payment_type']=='2')
                            {
                                if(!empty($arrpost['stripeToken']))
                                {
                                    if(!class_exists('\Stripe\Stripe'))
                                    {
                                        require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
                                    }
                                    \Stripe\Stripe::setApiKey($registration_screen[0]['stripe_secret_key']);
                                    $payment =  \Stripe\Charge::create(
                                        array(
                                            "amount" => $arrpost['price'] * 100,
                                            "currency" => $registration_screen[0]['stripe_payment_currency'],
                                            "source" => $arrpost['stripeToken'],
                                            "description" => "All In The Loop"
                                        )
                                    );
                                    if($payment['status']=="succeeded")
                                    {
                                        header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain);
                                    }
                                    else
                                    {
                                        header('Location:'.base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain);
                                    }
                                }
                                else
                                {
                                    $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
                                    $this->session->set_flashdata('registration_screen_error',"Missing Stripe Token...");
                                    redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                                }
                            }
                            else
                            {
                                header('Location:'.base_url().'Attendee_login/payment_failed_show_error_msg/'.$acc_name.'/'.$Subdomain);
                            }
                        }
                    }
                    else
                    {
                        if($registration_screen[0]['show_payment_screen']!='1')
                        {   
                            $this->payment_success_save_user($acc_name,$Subdomain,$tempuserdata['email']);
                            /*header('Location:'.base_url().'Attendee_login/payment_success_save_user/'.$acc_name.'/'.$Subdomain);*/   
                        }
                        else
                        {
                            $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
                            $this->session->set_flashdata('registration_screen_error',"Please Select at Least One Product...");
                            
                            redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
                        }
                    }
                }
            }    
            else
            {
                $this->session->set_flashdata('registration_screen_error',"Email Address Already Exists Please Try Another Email Address...");
                redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
            }
        }
    }
    public function paypalpayment_success_save_user($acc_name,$Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates']=$event_templates[0];
        $registration_screen=$this->Attendee_model->get_registration_screen_data($event_id,null);
        $this->data['registration_screen']=$registration_screen;
        $this->load->view('events/thankyou_screen',$this->data);
    }
    public function payment_success_save_user($acc_name,$Subdomain,$email)
    {   
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates']=$event_templates[0];
        $event_id=$event_templates[0]['Id'];
        $registration_screen=$this->Attendee_model->get_registration_screen_data($event_id,null);
        $this->data['registration_screen']=$registration_screen;
        $badgesdata=$this->Attendee_model->get_badges_values_by_event($event_id);
        if($acc_name == 'CandyBerman' && $Subdomain == 'CanBer5a2a5ca878165'):
            $user = $this->Attendee_model->get_user_info($email);
        else:
        $user=$this->Add_attendee_model->save_registration_screen_user($event_id,$event_templates[0]['Organisor_id'],$email);
        endif;

        if($badgesdata[0]['send_email']=='1')
        {
            if($registration_screen[0]['send_pdf']=='1')
            {
                if($badgesdata[0]['code_type']!='1')
                {
                    $this->load->library('Ciqrcode');
                    $params['data'] = $event_templates[0]['Id'].'-'.$user['Id'];
                     $params['level'] = 'M';
                    $params['size'] = 2;
                    $params['savename'] = 'tes.png';
                    $this->ciqrcode->generate($params);
                }
                else
                {
                    $this->load->library('zend');
                    $this->zend->load('Zend/Barcode');
                    $imageResource=Zend_Barcode::factory('code128', 'image', array('text'=>$event_templates[0]['Id'].'-'.$user['Id'],'drawText' => false,'barHeight'=> 50,'factor'=>3), array('barHeight'=> 50,'factor'=>3))->draw();
                    imagepng($imageResource, './assets/user_files/Barcode.png');
                }
                require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/config/tcpdf_config.php');
                require_once($_SERVER['DOCUMENT_ROOT']. '/TCPDF-master/tcpdf.php');
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
                $pdf->SetTitle('Badge Design Final - Visitor');
                $pdf->AddPage();
                $htm='<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="4" height="10"></th>
                                </tr>
                                <tr>
                                    <th width="2%"></th>
                                    <th width="58%" align="left" style="vertical-align:top;">';
                                    if(!empty($badgesdata[0]['badges_logo_images'])){
                                    $htm.='<img width="504" height="158" src="./assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                    }
                                    $htm.='</th>
                                    <th width="38%" align="right" style="margin-top:5%;">';
                                    if($registration_screen[0]['send_qr_code']=='1')
                                    {
                                        if($badgesdata[0]['code_type']!='1')
                                        {
                                            $htm.='<img src="tes.png"/>';
                                        }
                                        else
                                        {
                                            $htm.='<img src="./assets/user_files/Barcode.png"/>';
                                        }
                                    }
                                    $htm.='</th>
                                    <th width="2%" align="right"></th>
                                </tr>
                                <tr style="padding-bottom:15px;">
                                    <th width="3%"></th>
                                    <th width="97%" colspan="3">
                                        <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                        <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                        <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                        <font size="10">'.ucfirst($user['Title']).'</font><br>
                                        <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="4" height="20"></th>
                                </tr>
                                <tr style="">
                                    <th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th>
                                </tr>';
                            $htm.='</table>
                        </td>
                        <td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="4" height="10"></th>
                                </tr>
                                <tr>
                                    <th width="2%"></th>
                                    <th width="58%" align="left" style="vertical-align:top;">';
                                    if(!empty($badgesdata[0]['badges_logo_images'])){
                                    $htm.='<img width="504" height="158" src="./assets/badges_files/'.$badgesdata[0]['badges_logo_images'].'"/>';
                                    }
                                    $htm.='</th>
                                    <th width="38%" align="right" style="margin-top:5%;">';
                                    if($registration_screen[0]['send_qr_code']=='1')
                                    {
                                        if($badgesdata[0]['code_type']!='1')
                                        {
                                            $htm.='<img src="tes.png"/>';
                                        }
                                        else
                                        {
                                            $htm.='<img src="./assets/user_files/Barcode.png"/>';
                                        }
                                    }
                                    $htm.='</th>
                                    <th width="2%" align="right"></th>
                                </tr>
                                <tr style="padding-bottom:15px;">
                                    <th width="3%"></th>
                                    <th width="97%" colspan="3">
                                        <font size="18"><b>'.ucfirst($user['Firstname']).'</b></font><br>
                                        <font size="14"><b>'.ucfirst($user['Lastname']).'</b></font><br><br>
                                        <font size="10"><i>'.ucfirst($user['Company_name']).'</i></font><br>
                                        <font size="10">'.ucfirst($user['Title']).'</font><br>
                                        <font size="10"> Badge Number : '.$event_templates[0]['Id'].'-'.$user['Id'].'</font>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="4" height="20"></th>
                                </tr>
                                <tr style="">
                                    <th colspan="4" width="100%" style="background-color:'.$color.';color:#fff; text-align:center; vertical-align:center;">'.strtoupper($attendee_type).'</th>
                                </tr>';
                            $htm.='</table>
                        </td>
                    </tr>
                    </table>';
                $pdf->writeHTML($htm);
                // $pdf->Output('badges_files.pdf', 'I');exit;
                file_put_contents('./assets/user_files/badges_files.pdf',$pdf->Output('badges_files.pdf', 'S'));
            } 
            $Subject=!empty($badgesdata[0]['subject']) ? $badgesdata[0]['subject'] : "Welcome Message";
            $sender_name=!empty($badgesdata[0]['sender_name']) ? $badgesdata[0]['sender_name'] : 'All In The Loop';
            $msg=html_entity_decode($badgesdata[0]['email_body']);
            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $replacements = array();
            $replacements[0] = ucfirst($user['Firstname']).' '.$user['Lastname'];
            $msg = preg_replace($patterns, $replacements, $msg);
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = 'xHi$&h9M)x9m';
            $config['_encoding'] = 'base64';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('invite@allintheloop.com',$sender_name);
            $this->email->to($user['Email']);
            //$this->email->to('tarun@elsner.com');
            $this->email->subject($Subject);
            $this->email->message($msg);
            if($registration_screen[0]['send_pdf']=='1')
            {
                $this->email->attach('./assets/user_files/badges_files.pdf');
            }  
            $this->email->send();
            $this->email->clear(TRUE);
            if($registration_screen[0]['send_pdf']=='1')
            {
                unlink('./assets/user_files/badges_files.pdf');
            }
            if($badgesdata[0]['code_type']=='1' && $registration_screen[0]['send_qr_code']=='1')
            {
                unlink('./assets/user_files/Barcode.png');
            }
        }
        if($registration_screen[0]['email_notify_admin']=='1')
        {
            $org_email=$this->Event_template_model->get_organizer_email_by_id($event_templates[0]['Organisor_id']);
            $adminmsg=ucfirst($user['Firstname']).' '.$user['Lastname']." has registered for ".ucfirst($event_templates[0]['Event_name']);
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = 'xHi$&h9M)x9m';
            $config['_encoding'] = 'base64';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('invite@allintheloop.com','All In The Loop');
            $this->email->to($org_email['Email']);
            $this->email->subject('New Registration');
            $this->email->message($adminmsg);
            $this->email->send();
            $this->email->clear(TRUE);
        }
        $this->load->view('events/thankyou_screen',$this->data);
    } 
    public function payment_failed_show_error_msg($acc_name,$Subdomain)
    { 
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->Add_attendee_model->delete_temp_multiuser($event_templates[0]['Id']);
        $this->session->set_flashdata('registration_screen_error',"Something Went Worng...");
        redirect(base_url()."App/".$acc_name."/".$Subdomain."/attendee_registration_screen");
    }
}