<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Docicon extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Doc Icon';
          $this->data['smalltitle'] = 'Doc Details';
          $this->data['page_edit_title'] = 'edit';
          $this->data['breadcrumb'] = 'Doc';
          parent::__construct($this->data);
          $this->load->model('Docicon_model');
          $this->load->model('Setting_model');
          $this->load->model('Event_model');
          $this->load->model('Profile_model');
          $this->load->library('session');
          $this->load->library('upload');
          
          $user = $this->session->userdata('current_user');
          if(!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
     }

     public function index()
     {
          $docicon = $this->Docicon_model->get_docicon_list();
          $this->data['docicon'] = $docicon;
            
          //print_r($users); exit();
          $this->template->write_view('css', 'docicon/css', $this->data, true);
          $this->template->write_view('content', 'docicon/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'docicon/js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = '0')
     {
          $this->data['docicon'] = $this->Docicon_model->get_docicon_list($id);
          
          if ($this->input->post())
          {
               if (!empty($_FILES['images']['name']))
               {
                   $tempname = explode('.', $_FILES['images']['name']);
                   $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                   $images_file = $tempname_imagename . "." . $tempname[1];
                   $_POST['image'] = $images_file;
                   $this->upload->initialize(array(
                           "file_name" => $images_file,
                           "upload_path" => "./assets/user_files",
                           "allowed_types" => 'gif|jpg|png',
                           "max_size" => '2048',
                           "max_width" => '2048',
                           "max_height" => '2048'
                   ));
                   
                   if (!$this->upload->do_multi_upload("images"))
                   {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error',$error['error']);
                   }
               }
               
               foreach ($this->input->post() as $k => $v)
               {
                    if ($k != "idval")
                    {
                         $k = strtolower($k);
                         $array_add[$k] = $v;
                    }
                    else
                    {
                         $idval=$v;
                    }
               }
               $user = $this->Docicon_model->edit_docicon($idval,$array_add);
               $this->session->set_flashdata('docicon_data', 'updated');
               redirect("Docicon");
          }
          
          $this->template->write_view('css', 'docicon/add_css', $this->data, true);
          $this->template->write_view('content', 'docicon/edit', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'docicon/add_js', $this->data, true);
          $this->template->render();
     }

     public function add()
     {
          if ($this->input->post())
          {
               if (!empty($_FILES['images']['name']))
               {
                   $tempname = explode('.', $_FILES['images']['name']);
                   $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                   $images_file = $tempname_imagename . "." . $tempname[1];
                   $_POST['image'] = $images_file;
                   $this->upload->initialize(array(
                           "file_name" => $images_file,
                           "upload_path" => "./assets/user_files",
                           "allowed_types" => 'gif|jpg|png',
                           "max_size" => '2048',
                           "max_width" => '2048',
                           "max_height" => '2048'
                   ));

                   if (!$this->upload->do_multi_upload("images"))
                   {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error',$error['error']);
                   }
               }
               
               foreach ($this->input->post() as $k => $v)
               {
                    if ($k != "password_again" && $k != "idval")
                    {
                         $k = ucfirst(strtolower($k));
                         $array_add[$k] = $v;
                    }
               }
               
               $user = $this->Docicon_model->add_docicon($array_add);
               $this->session->set_flashdata('docicon_data', 'Added');
               redirect("Docicon");
          }

          $this->template->write_view('css', 'docicon/add_css', $this->data, true);
          $this->template->write_view('content', 'docicon/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'docicon/add_js', $this->data, true);
          $this->template->render();
     }

     public function delete($id,$eventid)
     {
          $user = $this->Docicon_model->delete_docicon($id);
          $this->session->set_flashdata('docicon_data', 'Deleted');
          
          redirect("Docicon");
     }

}
