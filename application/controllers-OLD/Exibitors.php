<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Exibitors extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Exibitors';
          $this->data['smalltitle'] = 'Exibitors';
          $this->data['breadcrumb'] = 'Exibitors';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
           $this->load->library('formloader1');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Notes_admin_model');
          //$this->load->model('Exibitor_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $user = $this->session->userdata('current_user');
          $cnt = 0;
          $eventname=$this->Event_model->get_all_event_name();
          if(in_array($this->uri->segment(2),$eventname))
          {
               if ($user != '')
               {
                    $parameters = $this->uri->uri_to_assoc(1);
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($parameters[$this->data['pagetitle']]);
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    if(!empty($roledata))
                    {
                         $roleid = $roledata[0]->Role_id;
                         $eventid = $event_templates[0]['Id'];
                         $rolename = $roledata[0]->Name;
                         $cnt = 0;
                         $req_mod = ucfirst($this->router->fetch_class());

                         //$cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename, $eventid);
                         $cnt = 1;
                    }
                    else
                    {
                         $cnt=0 ;
                    }
                    
                    if ($cnt == 1)
                    {
                         $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
                         $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                         $this->data['pagetitle'] = "Cms";

                         $parameters = $this->uri->uri_to_assoc(1);
                         $parameters = $this->uri->uri_to_assoc(1);
                         redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
                    }
               }
          }
          else
          {
               $parameters = $this->uri->uri_to_assoc(1);
               redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);  
          }
     }

     public function index($Subdomain = NULL)
     {

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;     
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
          $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];    
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $exibitors_list = $this->Event_template_model->get_exibitors_list($Subdomain);
          $this->data['exibitors_list'] = $exibitors_list;
          
          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;

          $this->data['Subdomain'] = $Subdomain;
          $user = $this->session->userdata('current_user');
          if (empty($user))
          {
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('content', 'registration/index', $this->data, true);
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
          }
          else
          {
               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
               $this->template->write_view('content', 'Exibitors/index', $this->data, true);
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->write_view('footer', 'Exibitors/footer', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);
               $this->template->render();
          }
     }

     public function View($Subdomain = NULL, $id = null)
     {
          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $exibitors_data = $this->Event_template_model->get_exibitors_data($Subdomain, $id);
          $this->data['exibitors_data'] = $exibitors_data;

          $this->data['subdomain'] = $Subdomain;
          $this->data['sp_id'] = $id;

          $user = $this->session->userdata('current_user');
          if (empty($user))
          {
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                $this->template->write_view('content', 'registration/index', $this->data, true);
                $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                $this->template->render();
          }
          else
          {
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
			   
               $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
               $this->data['cms_menu'] = $cmsmenu;

               $this->template->write_view('css', 'frontend_files/css', $this->data, true);
               $this->template->write_view('header', 'frontend_files/header', $this->data, true);
               $this->template->write_view('content', 'Exibitors/user', $this->data, true);
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);
               $this->template->render();
          }

          
     }

}
