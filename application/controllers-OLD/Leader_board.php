<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class leader_board extends FrontendController
{
  function __construct()
  {
    
    $this->data['pagetitle'] = 'Analytics';
    $this->data['smalltitle'] = '';
    $this->data['breadcrumb'] = 'Analytics';
    $this->data['page_edit_title'] = 'edit';
    parent::__construct($this->data);
    $this->load->model('Agenda_model');
    $this->load->model('Attendee_model');
    $this->load->library('formloader');
    $this->load->library('formloader1');
    $this->load->model('Event_model');
    $user = $this->session->userdata('current_user');
    $eventid=$this->uri->segment(3);
    $this->load->database();
    $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
    $user_events =  array_filter(array_column($user_events,'Event_id'));
    if(!in_array($eventid,$user_events))
    {
         redirect('Forbidden');
    }
    $eventmodule=$this->Event_model->geteventmodulues($eventid);
    $module=json_decode($eventmodule[0]['module_list']);
    if(!in_array('8',$module))
    {
      redirect('Forbidden');
    }
    $event_templates = $this->Event_model->view_event_by_id($eventid);
    $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);
    $roledata = $this->Event_model->getUserRole($eventid);
    if(!empty($roledata))
    {
      $roleid = $roledata[0]->Role_id;
      $rolename = $roledata[0]->Name;
      $cnt = 0;
      $req_mod = ucfirst($this->router->fetch_class());
      if ($this->data['pagetitle'] == "Analytics")
      {
        $title = "Leader";
      }
      $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
    }
    else
    {
      $cnt=0;
    }
    if (!empty($user[1]['event_id_selected']))
    {
      $this->data['event_id_selected'] = $user[1]['event_id_selected'];
    }
    if ($cnt == 1 && in_array('8',$menu_list))
    {
      $this->load->model('Leader_board_model');
      $this->load->model('Agenda_model');
      $this->load->model('Event_model');
      $this->load->model('Setting_model');
      $this->load->model('Profile_model');
      $this->load->model('Event_template_model');
      $roles = $this->Event_model->get_menu_list($roleid,$eventid);
      $this->data['roles'] = $roles;
    }
    else
    {
      redirect('Forbidden');
    }
  }
  public function index_old($id)
  {
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $orid = $this->data['user']->Id;
    if ($this->data['user']->Role_name == 'User')
    {
      $total_permission = $this->Agenda_model->get_permission_list();
      $this->data['total_permission'] = $total_permission;
    }
    if ($this->data['user']->Role_name == 'Client')
    {
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
    }
    $leader_boards = $this->Leader_board_model->get_leader_board_list();
    $this->data['Leader_boards'] = $leader_boards;
    $module_click_data = $this->Leader_board_model->get_all_click_tracking($id);
    $this->data['module_click_data'] = $module_click_data;
    $user_survey_ans = $this->Leader_board_model->get_all_userwise_survey_ans($id);
    $this->data['user_survey_ans'] = $user_survey_ans;
    $metting_data=$this->Leader_board_model->get_all_metting_by_event_id($id);
    $this->data['metting_data']=$metting_data;
    $view_bids=$this->Leader_board_model->get_all_bids($id);
    $this->data['view_bids']=$view_bids;
    $agenda=$this->Leader_board_model->get_all_agenda_list($id);
    $this->data['agenda'] = $agenda;
    $currency=$this->Leader_board_model->get_current_byevent($id);
    $this->data['currency']=$currency;
    $user_notes = $this->Leader_board_model->get_all_userwise_notes($id);
    $this->data['user_notes'] = $user_notes;
    $advert_click_data = $this->Leader_board_model->get_advert_click_tracking($id);
    $this->data['advert_click_data'] = $advert_click_data;
    $msg_tracking = $this->Leader_board_model->get_all_msg_tracking($id);
    $this->data['msg_tracking'] = $msg_tracking;
    $comments_tracking = $this->Leader_board_model->get_all_comments_tracking($id);
    $this->data['comments_tracking'] = $comments_tracking;
    $click_track_chart = $this->Leader_board_model->get_click_tracking_chart($id);
    $this->data['click_track_chart'] = $click_track_chart;
    $message_track_chart = $this->Leader_board_model->get_message_tracking_chart($id);
    $this->data['message_track_chart'] = $message_track_chart;
    $comment_track_chart = $this->Leader_board_model->get_comment_tracking_chart($id);
    $this->data['comment_track_chart'] = $comment_track_chart;
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/index', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  /*public function index($id)
  {

    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $orid = $this->data['user']->Id;
    if ($this->data['user']->Role_name == 'User')
    {
      $total_permission = $this->Agenda_model->get_permission_list();
      $this->data['total_permission'] = $total_permission;
    }
    if ($this->data['user']->Role_name == 'Client')
    {
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
    }
    $currency=$this->Leader_board_model->get_current_byevent($id);
    $this->data['currency']=$currency;

    

    $this->data['total_register_user']=$this->Leader_board_model->get_total_register_user_by_event($id,$event[0]['Organisor_id'],NULL,NULL);
    $this->data['most_popular_area']=$this->Leader_board_model->get_most_popular_areas_by_event($id,NULL,NULL);

    

    $this->data['most_active_user']=$this->Leader_board_model->get_most_active_user_by_event($id,NULL,NULL);

    

    $this->data['most_login_os']=$this->Leader_board_model->get_most_login_operating_system_by_event($id,NULL,NULL);

    
   
    $this->data['agenda'] = $this->Leader_board_model->get_all_agenda_list($id,NULL,NULL);
    $this->data['top_rated_session']=$this->Leader_board_model->get_top_rated_session_by_event($id,NULL,NULL);

   


    $this->data['most_popular_session']=$this->Leader_board_model->get_most_popular_session_by_event($id);
    $this->data['most_checkin_session']=$this->Leader_board_model->get_most_check_in_session_by_event($id);
    $survey_question_pie_chart=$this->Leader_board_model->get_survey_question_by_pie_chart($id,NULL,NULL);

   

    $final_Array_chart = array();
    foreach ($survey_question_pie_chart as $key => $value) 
    {
      $final_Array_chart[$value['Question']]['Q_id']=$value['Id'];
      $final_Array_chart[$value['Question']][$value['panswer']][] = array(
        'id' => $value['puserid'],
        'Firstname' => $value['Firstname'],
        'Lastname' => $value['Lastname'],
        'Email' => $value['Email']
      );
    }
    $final_Array_chart1=array();
    foreach($final_Array_chart as $key => $value)
    {
      foreach ($value as $i => $j) 
      {
        if(is_array($j))
        {   
          $final_Array_chart1[$key]['chart'][$i] = count($j);
        }
        else
        {
          $final_Array_chart1[$key][$i]=$j;
        }
      }
    }


    

    $this->data['survey_question_pie_chart']=$final_Array_chart1;
    $this->data['forms_data']=$this->Leader_board_model->get_all_forms_by_event($id,NULL,NULL);


    
    $this->data['cms_click_data']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,Null,$from_datetime,$to_datetime);
    $this->data['most_popular_cms']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,5,$from_datetime,$to_datetime);
    $this->data['cms_click_user']=$this->Leader_board_model->get_cms_click_user_by_event($id,$from_datetime,$to_datetime);



    $this->data['advert_click_data']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,Null,NULL,NULL);
    $this->data['most_popular_advert']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,5,NULL,NULL);
    $this->data['advert_click_user']=$this->Leader_board_model->get_adverts_click_user_by_event($id,NULL,NULL);
    $this->data['top_profileviews_sponsors']=$this->Leader_board_model->get_top_views_profiles_sponsors($id,Null,NULL);
    $this->data['visit_soonsor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_sponsors_profiles($id,NULL,NULL);
    $this->data['most_user_messages']=$this->Leader_board_model->get_user_send_messages_by_event($id,NULL,NULL);
    $this->data['most_user_comment']=$this->Leader_board_model->get_user_comment_by_event($id,NULL,NULL);
    $this->data['most_user_shared_photos']=$this->Leader_board_model->get_most_shared_photos_user_by_event($id,Null,Null);
    $this->data['most_user_likes']=$this->Leader_board_model->get_user_likes_by_event($id,NULL,NULL);
    $this->data['swap_contact_user']=$this->Leader_board_model->get_swap_contact_user_by_event($id,Null,NULL);
    $this->data['top_profileviews_exibitor']=$this->Leader_board_model->get_top_views_profiles_exhibitor($id,Null,NULL);
    $this->data['visit_exhibitor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_exhibitor_profiles($id,NULL,NULL);
    $this->data['top_exibitor_have_request_meeting']=$this->Leader_board_model->get_top_exhibitor_have_requested_meeting($id,NULL,NULL);
    $this->data['top_requested_meeting_attendee']=$this->Leader_board_model->get_top_attendee_have_requested_meeting($id,NULL,NULL);
    $this->data['top_exibitor_by_favorited']=$this->Leader_board_model->get_top_exibitor_have_favorites($id,NULL,NULL);
    $this->data['top_sponsors_by_favorited']=$this->Leader_board_model->get_top_sponsors_have_favorites($id,NULL,NULL);
    $this->data['top_speakers_by_favorited']=$this->Leader_board_model->get_top_speakers_have_favorites($id,NULL,NULL);
    $this->data['top_attendee_by_favorited']=$this->Leader_board_model->get_top_attendees_have_favorites($id,NULL,NULL);
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/index_new', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/leader_js', $this->data, true);
    $this->template->render();
  }*/
  public function index_new_time($id)
  {
    $from_datetime=$this->input->post('from_datetime');
    $to_datetime=$this->input->post('to_datetime');
    $this->data['from_datetime']=$from_datetime;
    $this->data['to_datetime']=$to_datetime;
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $orid = $this->data['user']->Id;
    if ($this->data['user']->Role_name == 'User')
    {
      $total_permission = $this->Agenda_model->get_permission_list();
      $this->data['total_permission'] = $total_permission;
    }
    if ($this->data['user']->Role_name == 'Client')
    {
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
    }
    $currency=$this->Leader_board_model->get_current_byevent($id);
    $this->data['currency']=$currency;
    $this->data['total_register_user']=$this->Leader_board_model->get_total_register_user_by_event($id,$event[0]['Organisor_id'],$from_datetime,$to_datetime);
    $this->data['most_popular_area']=$this->Leader_board_model->get_most_popular_areas_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_active_user']=$this->Leader_board_model->get_most_active_user_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_login_os']=$this->Leader_board_model->get_most_login_operating_system_by_event($id,$from_datetime,$to_datetime);
    $this->data['agenda'] = $this->Leader_board_model->get_all_agenda_list($id,$from_datetime,$to_datetime);
    $this->data['top_rated_session']=$this->Leader_board_model->get_top_rated_session_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_popular_session']=$this->Leader_board_model->get_most_popular_session_by_event($id);
    $this->data['most_checkin_session']=$this->Leader_board_model->get_most_check_in_session_by_event($id);
    $survey_question_pie_chart=$this->Leader_board_model->get_survey_question_by_pie_chart($id,$from_datetime,$to_datetime);
    $final_Array_chart = array();
    foreach ($survey_question_pie_chart as $key => $value) 
    {
      $final_Array_chart[$value['Question']]['Q_id']=$value['Id'];
      $final_Array_chart[$value['Question']][$value['panswer']][] = array(
        'id' => $value['puserid'],
        'Firstname' => $value['Firstname'],
        'Lastname' => $value['Lastname'],
        'Email' => $value['Email']
      );
    }
    $final_Array_chart1=array();
    foreach($final_Array_chart as $key => $value)
    {
      foreach ($value as $i => $j) 
      {
        if(is_array($j))
        {   
          $final_Array_chart1[$key]['chart'][$i] = count($j);
        }
        else
        {
          $final_Array_chart1[$key][$i]=$j;
        }
      }
    }
    $this->data['survey_question_pie_chart']=$final_Array_chart1;
    $this->data['forms_data']=$this->Leader_board_model->get_all_forms_by_event($id,$from_datetime,$to_datetime);
    $this->data['cms_click_data']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,Null,$from_datetime,$to_datetime);
    $this->data['most_popular_cms']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,5,$from_datetime,$to_datetime);
    $this->data['cms_click_user']=$this->Leader_board_model->get_cms_click_user_by_event($id,$from_datetime,$to_datetime);

    $this->data['advert_click_data']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,Null,$from_datetime,$to_datetime);
    $this->data['most_popular_advert']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,5,$from_datetime,$to_datetime);
    $this->data['advert_click_user']=$this->Leader_board_model->get_adverts_click_user_by_event($id,$from_datetime,$to_datetime);
    $this->data['top_profileviews_sponsors']=$this->Leader_board_model->get_top_views_profiles_sponsors($id,$from_datetime,$to_datetime);
    $this->data['visit_soonsor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_sponsors_profiles($id,$from_datetime,$to_datetime);
    $this->data['most_user_messages']=$this->Leader_board_model->get_user_send_messages_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_user_comment']=$this->Leader_board_model->get_user_comment_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_user_shared_photos']=$this->Leader_board_model->get_most_shared_photos_user_by_event($id,$from_datetime,$to_datetime);
    $this->data['most_user_likes']=$this->Leader_board_model->get_user_likes_by_event($id,$from_datetime,$to_datetime);
    $this->data['swap_contact_user']=$this->Leader_board_model->get_swap_contact_user_by_event($id,$from_datetime,$to_datetime);
    $this->data['top_profileviews_exibitor']=$this->Leader_board_model->get_top_views_profiles_exhibitor($id,$from_datetime,$to_datetime);
    $this->data['visit_exhibitor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_exhibitor_profiles($id,$from_datetime,$to_datetime);
    $this->data['top_exibitor_have_request_meeting']=$this->Leader_board_model->get_top_exhibitor_have_requested_meeting($id,$from_datetime,$to_datetime);
    $this->data['top_requested_meeting_attendee']=$this->Leader_board_model->get_top_attendee_have_requested_meeting($id,$from_datetime,$to_datetime);
    $this->data['top_exibitor_by_favorited']=$this->Leader_board_model->get_top_exibitor_have_favorites($id,$from_datetime,$to_datetime);
    $this->data['top_sponsors_by_favorited']=$this->Leader_board_model->get_top_sponsors_have_favorites($id,$from_datetime,$to_datetime);
    $this->data['top_speakers_by_favorited']=$this->Leader_board_model->get_top_speakers_have_favorites($id,$from_datetime,$to_datetime);
    $this->data['top_attendee_by_favorited']=$this->Leader_board_model->get_top_attendees_have_favorites($id,$from_datetime,$to_datetime);
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/index_new', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/leader_js', $this->data, true);
    $this->template->render();
  }
  public function exportmsg($event_id,$uid)
  {
    $res=$this->Leader_board_model->getmsgcsvdata($event_id,$uid);
    $filename = "user_Message_details.csv";
    $fp = fopen('php://output', 'w');
    $header[] = "         ";
    $header[] = "User Name";
    $header[] = "Total Messages";
    $header[] = "Recipient";
    $header[] = "Messages Sent";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp,array());
    fputcsv($fp, $header);
    foreach ($res as $key => $value) 
    {
      $data['empty']="    ";
      $data['name']=ucfirst($value['Firstname']).' '.ucfirst($value['Lastname']);
      $data['total_hit']=$value['total_hit'];
      $cnt=count($value['Receive']);
      $i=0;
      foreach ($value['Receive'] as $key1 => $value1) 
      {
        $i=$i+1;
        if($key1==0)
        {
          if($value1['Firstname']=="" && $value1['Lastname']=="")
          {
            $data['receivename']="User";
          }
          else
          {
            $data['receivename']=ucfirst($value1['Firstname']).' '.ucfirst($value1['Lastname']);
          }
          $data['rtotal_hit']=$value1['total_hit'];
          fputcsv($fp, $data);
        }
        else
        {
          $data['name']="";
          $data['total_hit']="";
          if($value1['Firstname']=="" && $value1['Lastname']=="")
          {
            $data['receivename']="User";
          }
          else
          {
            $data['receivename']=ucfirst($value1['Firstname']).' '.ucfirst($value1['Lastname']);
          }
          $data['rtotal_hit']=$value1['total_hit'];
          fputcsv($fp, $data);
        }
        if($cnt==$i)
        {
          $data['name']="";
          $data['total_hit']="";
          $data['receivename']="Total";
          $data['rtotal_hit']=$value['total_hit'];
          fputcsv($fp, $data);
        }
      }
    }
    exit();
  }
  public function exportxlsxmsg($event_id,$uid)
  {
    $this->load->library('excel');
    $res=$this->Leader_board_model->getmsgcsvdata($event_id,$uid);
    $filename='user_Message_details.xls';
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setCellValue('A1',' ');
    $this->excel->getActiveSheet()->setCellValue('B1','User Name');
    $this->excel->getActiveSheet()->setCellValue('C1','Total Messages');
    $this->excel->getActiveSheet()->setCellValue('D1','Recipient');
    $this->excel->getActiveSheet()->setCellValue('E1','Messages Sent');
    $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
    $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
    $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
    $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
    foreach ($res as $key => $value) 
    {
      $this->excel->getActiveSheet()->setCellValue('A2',"    ");
      $this->excel->getActiveSheet()->setCellValue('B2',ucfirst($value['Firstname']).' '.ucfirst($value['Lastname']));
      $this->excel->getActiveSheet()->setCellValue('C2',$value['total_hit']);
      $cnt=count($value['Receive']);
      $i=0;
      $row=2;
      foreach ($value['Receive'] as $key1 => $value1) 
      {
        $i=$i+1;
        if($key1==0)
        {
          if($value1['Firstname']=="" && $value1['Lastname']=="")
          {
            $this->excel->getActiveSheet()->setCellValue('D'.$row,"User");
          }
          else
          {
            $this->excel->getActiveSheet()->setCellValue('D'.$row,ucfirst($value1['Firstname']).' '.ucfirst($value1['Lastname']));
          }
          $this->excel->getActiveSheet()->setCellValue('E'.$row,$value1['total_hit']);
          $row++;
        }
        else
        {
          $this->excel->getActiveSheet()->setCellValue('A'.$row," ");
          $this->excel->getActiveSheet()->setCellValue('B'.$row," ");
          $this->excel->getActiveSheet()->setCellValue('C'.$row," ");
          if($value1['Firstname']=="" && $value1['Lastname']=="")
          {
            $this->excel->getActiveSheet()->setCellValue('D'.$row,"User");
          }
          else
          {
            $this->excel->getActiveSheet()->setCellValue('D'.$row,ucfirst($value1['Firstname']).' '.ucfirst($value1['Lastname']));
          }
          $this->excel->getActiveSheet()->setCellValue('E'.$row,$value1['total_hit']);
          $row++;
        }
        if($cnt==$i)
        {
          $this->excel->getActiveSheet()->setCellValue('A'.$row," ");
          $this->excel->getActiveSheet()->setCellValue('B'.$row," ");
          $this->excel->getActiveSheet()->setCellValue('C'.$row," ");
          $this->excel->getActiveSheet()->setCellValue('D'.$row,"Total");
          $this->excel->getActiveSheet()->setCellValue('E'.$row,$value['total_hit']);
          $row++;
        }  
      }
    }
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
    $objWriter->save('php://output');
    exit();
  }
  public function export($event_id,$id)
  {
    if($id==1)
    {
      $module_click_data = $this->Leader_board_model->get_all_click_tracking($event_id);
      $filename = "user_click_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total_clicks";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top Clicker List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($module_click_data as $key => $value) 
      {
        unset($value['hit_date']);
        unset($value['menu_hit']);
        fputcsv($fp, $value);
      }
    }
    else if($id==2)
    {
      $advert_click_data = $this->Leader_board_model->get_advert_click_tracking($event_id);
      $filename = "advertise_user_click_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total_clicks";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top Adversite Clicker List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($advert_click_data as $key => $value) 
      {
        fputcsv($fp, $value);
      }
    }
    else if($id==3)
    {
      $user_survey_ans = $this->Leader_board_model->get_all_userwise_survey_ans($event_id);
      $filename = "advertise_user_click_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total Answer";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top  Answer User List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($user_survey_ans as $key => $value) 
      {
        fputcsv($fp, $value);
      }
    }
    else if($id==4)
    {
      $user_notes = $this->Leader_board_model->get_all_userwise_notes($event_id);
      $filename = "users_notes_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total Notes";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top  Notes User List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($user_notes as $key => $value) 
      {
        fputcsv($fp, $value);
      }
    }
    else if($id==5)
    {
      $msg_tracking = $this->Leader_board_model->get_all_msg_tracking($event_id);
      $filename = "users_messages_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total Msgs";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top Messages User List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($msg_tracking as $key => $value) 
      {
        fputcsv($fp, $value);
      }
    }
    else if($id==6)
    {
      $comments_tracking = $this->Leader_board_model->get_all_comments_tracking($event_id);
      $filename = "users_comments_details.csv";
      $fp = fopen('php://output', 'w');
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total Comments";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top Comments User List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($comments_tracking as $key => $value) 
      {
        fputcsv($fp, $value);
      }
    }
    else if($id==7)
    {
      $view_bids=$this->Leader_board_model->get_all_bids($event_id);
      $currency=$this->Leader_board_model->get_current_byevent($event_id);
      $filename = "users_bids_details.csv";
      $fp = fopen('php://output', 'w');
      $symbol=iconv("UTF-8", "cp1252", $amt);
      $header[] = "Name";
      $header[] = "Title";
      $header[] = "Company_name";
      $header[] = "Email";
      $header[] = "Street";
      $header[] = "Suburb";
      $header[] = "Postcode";
      $header[] = "Total Bids";
      $header[] = "Total Bids Amount(".ucfirst($currency[0]).")";
      $header[] = "Total Wins";
      $header[] = "Total Wins Amount(".ucfirst($currency[0]).")";
      header('Content-type: application/csv');
      header('Content-Disposition: attachment; filename='.$filename);
      fputcsv($fp,array("","","","","","Top Bidders User List","","","",""));
      fputcsv($fp,array());
      fputcsv($fp,array());
      fputcsv($fp, $header);
      foreach ($view_bids as $key => $value) 
      {
        fputcsv($fp, $value);
      } 
    }
    exit();
  }
  public function export_agenda_details($eid,$aid)
  {
    $keysdata=$this->Attendee_model->get_all_custom_modules($eid);
    $agenda_details=$this->Leader_board_model->get_agenda_details($eid,$aid);
    $filename = "users_agenda_details.csv";
    $fp = fopen('php://output', 'w');      
    $symbol=iconv("UTF-8", "cp1252", $amt);
    $header[] = "First Name";
    $header[] = "Last Name";
    $header[] = "Email";
    $header[] = "Job Title";
    $header[] = "Company Name";
    $header[] = "Country";
    foreach ($keysdata as $key => $value) {
      $header[] = ucfirst($value['column_name']);
    }
    $header[] = "Session Rating";
    $header[] = "Save Agenda";
    $header[] = "Check In";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp,array());
    fputcsv($fp,array());
    fputcsv($fp, $header);
    foreach ($agenda_details as $key => $value) {
      $data[]=ucfirst($value['Firstname']);
      $data[]=ucfirst($value['Lastname']);
      $data[]=$value['Email'];
      $data[]=ucfirst($value['Title']);
      $data[]=ucfirst($value['Company_name']);
      $data[]=ucfirst($value['Country']);
      for($j=0;$j<count($keysdata);$j++)
      {
        $custom=json_decode($value['extra_column'],TRUE);
        $keynm=$keysdata[$j]['column_name'];
        $data[]=$custom[$keynm];
      }
      $data[]=$value['rating'];
      $arr1=explode(",",$value['agenda_id']);
      if(in_array($agendaid,$arr1)){
        $data[]="Yes";
      }
      else
      {
        $data[]="No";
      }
      $arr=explode(",",$value['check_in_agenda_id']);
      if(in_array($agendaid,$arr)){
        $data[]="Yes";
      }
      else
      {
        $data[]="No";
      }
      fputcsv($fp, $data);
      $data=array();
    }
  }
  public function details($id,$userid)
  {
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $orid = $this->data['user']->Id;
    if ($this->data['user']->Role_name == 'User')
    {
      $total_permission = $this->Agenda_model->get_permission_list();
      $this->data['total_permission'] = $total_permission;
    }
    if ($this->data['user']->Role_name == 'Client')
    {
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
    }
    $leader_boards = $this->Leader_board_model->get_leader_board_list();
    $this->data['Leader_boards'] = $leader_boards;
    $module_click_data = $this->Leader_board_model->get_userwise_click_tracking($id,$userid);
    $this->data['module_click_data'] = $module_click_data;
    $msg_tracking = $this->Leader_board_model->get_all_msg_tracking($id);
    $this->data['msg_tracking'] = $msg_tracking;
    $comments_tracking = $this->Leader_board_model->get_all_comments_tracking($id);
    $this->data['comments_tracking'] = $comments_tracking;
    $click_track_chart = $this->Leader_board_model->get_click_tracking_chart($id);
    $this->data['click_track_chart'] = $click_track_chart;
    $message_track_chart = $this->Leader_board_model->get_message_tracking_chart($id);
    $this->data['message_track_chart'] = $message_track_chart;
    $comment_track_chart = $this->Leader_board_model->get_comment_tracking_chart($id);
    $this->data['comment_track_chart'] = $comment_track_chart;
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function agenda_details($eventid,$agendaid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($eventid);
    $this->data['agendaid']=$agendaid;
    $agenda_details=$this->Leader_board_model->get_agenda_details($eventid,$agendaid);
    $this->data['agenda_details']=$agenda_details;
    $event = $this->Event_model->get_admin_event($eventid);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$eventid);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/agenda_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/add_js', $this->data, true);
    $this->template->render();
  }
  public function agenda_comment_details($eventid,$agendaid)
  {
    $this->data['agendaid']=$agendaid;
    $agenda_comment=$this->Leader_board_model->get_all_comment_by_agenda_id($eventid,$agendaid);
    $this->data['agenda_comment']=$agenda_comment;
    $event = $this->Event_model->get_admin_event($eventid);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$eventid);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/agenda_comments', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_report_in_csv($eid)
  {
    $filename = "session_rating_details.csv";
    $fp = fopen('php://output', 'w');
    $header[] = "Session Name";
    $header[] = "First Name";
    $header[] = "Last Name";
    $header[] = "Title";
    $header[] = "Company";
    $header[] = "Email Address";
    $header[] = "Rating Given";
    $header[] = "comment";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp, $header);
    $ratingdetail=$this->Leader_board_model->get_all_agenda_session_rating_by_event($eid);
    foreach ($ratingdetail as $key => $value) {
      $data['Session Name']=$value['Heading'];
      $data['First Name']=$value['Firstname'];
      $data['last Name']=$value['Lastname'];
      $data['Title']=$value['Title'];
      $data['Company']=$value['Company_name'];
      $data['Email Address']=$value['Email'];
      $data['Rating']=$value['rating'];
      $data['comment']=$value['comments'];
      fputcsv($fp, $data);
    }
  }
  public function download_form_data_csv($eid,$fid)
  {
    $formsdata=$this->Leader_board_model->get_formsubmitdata_by_form_id($eid,$fid);
    $fdata=$this->Leader_board_model->get_form_data_by_form_id($eid,$fid);
    $f_data=json_decode($fdata[0]['json_data'],true);
    $filename = "form_data.csv";
    $fp = fopen('php://output', 'w');
    $symbol=iconv("UTF-8", "cp1252", $amt);
    $header[] = "User Name";
    $header[] = "User Email";
    $header[] = "Company Name";
    $header[] = "Title";
    foreach ($f_data['fields'] as $key => $value) {
      if($value['type']!="element-section-break")
      {
        $header[] = $value['title'];
      }
    }
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp,array());
    fputcsv($fp, $header);
    foreach ($formsdata as $key => $value) {
      $vdata['User Name']=ucfirst($value['Firstname']).' '.$value['Lastname'];
      $vdata['User Email']=$value['Email'];
      $vdata['Company Name']=$value['Company_name'];
      $vdata['Title']=$value['Title'];
      $fielddata=json_decode($value['json_submit_data'],true);
      foreach ($fielddata as $key1 => $value1) {
        if(is_array($value1))
        {
          $vdata[$key1]=implode(",",$value1);
        }
        else
        {
          $vdata[$key1]=$value1;
        }
      }
      fputcsv($fp, $vdata);
    }
  }
  public function get_all_register_user_by_event($id)
  {
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $register_user_list=$this->Leader_board_model->get_all_resgister_user_in_this_event($id,$event[0]['Organisor_id']);
    $this->data['register_user_list']=$register_user_list;
    $this->data['is_register_user_list']='1';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();  
  }
  public function get_most_popular_area_details($id,$mid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $modules_details=$this->Leader_board_model->get_user_have_click_on_modules_by_menu_id($id,$mid);
    $this->data['modules_details']=$modules_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='1';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_most_active_user_details($id,$uid)
  {
    $user_details=$this->Leader_board_model->get_user_have_click_on_modules_by_user_id($id,$uid);
    $this->data['user_details']=$user_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='1';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();    
  }
  public function login_os_details($id,$os=Null)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $os_details=$this->Leader_board_model->get_user_list_by_login_os($id,$os);
    $this->data['os_details']=$os_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='1';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_question_details($id,$qid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $question_details=$this->Leader_board_model->get_user_list_by_question_id($id,$qid);
    $this->data['question_details']=$question_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='1';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_advert_details($id,$aid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $adverts_details=$this->Leader_board_model->get_advert_details_by_adverts_id($id,$aid);
    $this->data['adverts_details']=$adverts_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='1';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_user_advert_details($id,$uid)
  {
    $user_adverts_details=$this->Leader_board_model->get_user_advert_details_by_user_id($id,$uid);
    $this->data['user_adverts_details']=$user_adverts_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='1';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_attendee_visited_sponsord_profiles_details($id,$sid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id); 
    $sponsor_visite_profile_details=$this->Leader_board_model->get_user_list_visited_sponsor_profile($id,$sid);
    $this->data['sponsor_visite_profile_details']=$sponsor_visite_profile_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='1';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_attendee_visited_sponsors_profiles_details($id,$uid)
  {
    $sponsors_list_visit_by_attendee=$this->Leader_board_model->get_sponsors_list_visite_by_attendee($id,$uid);
    $this->data['sponsors_list_visit_by_attendee']=$sponsors_list_visit_by_attendee;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='1';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_all_user_msg_by_event($id,$uid)
  {
    $user_messages_list=$this->Leader_board_model->get_all_user_messages_by_user_event($id,$uid);
    $this->data['user_messages_list']=$user_messages_list;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='1';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_attendee_visited_exibitor_profiles_details($id,$eid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $exibitor_visite_profile_details=$this->Leader_board_model->get_user_list_visited_exibitor_profile($id,$eid);
    $this->data['exibitor_visite_profile_details']=$exibitor_visite_profile_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='1';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_exhibitor_list_visited_attendee_details($id,$uid)
  {
    $exhibitor_list_visit_by_attendee=$this->Leader_board_model->get_exibitor_list_visite_by_attendee($id,$uid);
    $this->data['exhibitor_list_visit_by_attendee']=$exhibitor_list_visit_by_attendee;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='1';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_attendee_request_meeting_exibitor($id,$eid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $attendee_list_requested_meeting=$this->Leader_board_model->get_attendee_list_requested_meeting_by_exhibitor_id($id,$eid);
    $this->data['attendee_list_requested_meeting']=$attendee_list_requested_meeting;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='1';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_exhibitor_list_requested_meeting($id,$uid)
  {
    $exhibitor_list_requested_meeting = $this->Leader_board_model->get_exhibitor_list_requested_meeting_by_attendee_id($id,$uid);
    $this->data['exhibitor_list_requested_meeting']=$exhibitor_list_requested_meeting;

    /*echo "<pre>";
    print_r($this->data['exhibitor_list_requested_meeting']);
    exit();*/

    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='1';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;

    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');

    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function get_user_list_to_favorites($id,$eid,$mtype)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $user_list_to_exibitor=$this->Leader_board_model->get_user_list_favorites($id,$eid,$mtype);
    $this->data['user_list_to_exibitor']=$user_list_to_exibitor;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='0';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='1';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }
  public function export_message_data($eid)
  {
    $message_data=$this->Leader_board_model->get_all_messages_csv_data($eid);
    $filename = "users_messages_details.csv";
    $fp = fopen('php://output', 'w');
    $symbol=iconv("UTF-8", "cp1252", $amt);
    $header[] = "Sender Name";
    $header[] = "Message Content";
    $header[] = "Receive Name";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp, $header);
    foreach ($message_data as $key => $value) 
    {
      $csvdata['Sender Name']=$value['Sender_name'];
      $csvdata['Message Content']=strip_tags($value['Message']);
      $csvdata['Receive Name']=$value['Receiver_name'];
      fputcsv($fp, $csvdata);
      unset($csvdata);
    } 
  }
  public function get_cms_details($id,$aid)
  {
    $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($id);
    $adverts_details=$this->Leader_board_model->get_cms_details_by_adverts_id($id,$aid);
    $this->data['adverts_details']=$adverts_details;
    $this->data['is_register_user_list']='0';
    $this->data['is_modules_details']='0';
    $this->data['is_user_details']='0';
    $this->data['is_os_details']='0';
    $this->data['is_question_details']='0';
    $this->data['is_advert_details']='1';
    $this->data['is_user_advert_details']='0';
    $this->data['is_sponsor_visite_profile_details']='0';
    $this->data['is_attendee_visit_sponsor_profile_details']='0';
    $this->data['is_all_user_messages_details']='0';
    $this->data['is_exibitor_visite_profile_details']='0';
    $this->data['is_attendee_visit_exhibitor_profile_details']='0';
    $this->data['is_attendee_list_requested_meeting_details']='0';
    $this->data['is_exhibitor_list_requested_meeting_details']='0';
    $this->data['is_user_list_to_favorites_details']='0';
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $event_id = $id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/more_details', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/js', $this->data, true);
    $this->template->render();
  }   

  public function index_temp($id)
  {
      //error_reporting(E_ALL);
  		$limit = 25;
  		$event = $this->Event_model->get_admin_event($id);
	    $this->data['event'] = $event[0];
	    $user = $this->session->userdata('current_user');
	    $roleid = $user[0]->Role_id;
	    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
	    $this->data['users_role']=$user_role;
	    $orid = $this->data['user']->Id;
	    if ($this->data['user']->Role_name == 'User')
	    {
	      $total_permission = $this->Agenda_model->get_permission_list();
	      $this->data['total_permission'] = $total_permission;
	    }
	    if ($this->data['user']->Role_name == 'Client')
	    {
	      $event = $this->Event_model->get_admin_event($id);
	      $this->data['event'] = $event[0];
	    }
	    $currency=$this->Leader_board_model->get_current_byevent($id);
	    $this->data['currency']=$currency;

	    $this->data['total_register_user']=$this->Leader_board_model->get_total_register_user_by_event($id,$event[0]['Organisor_id'],NULL,NULL,$limit);
	    $this->data['most_popular_area']=$this->Leader_board_model->get_most_popular_areas_by_event($id,NULL,NULL,NULL);

	    $this->data['most_active_user']=$this->Leader_board_model->get_most_active_user_by_event($id,NULL,NULL,$limit);

	    $this->data['most_login_os']=$this->Leader_board_model->get_most_login_operating_system_by_event($id,NULL,NULL,$limit);

      //echo "<pre>";
      //print_r($this->data['most_login_os']); exit();
	    
	    $this->data['agenda'] = $this->Leader_board_model->get_all_agenda_list($id,$limit);
	    $this->data['top_rated_session']=$this->Leader_board_model->get_top_rated_session_by_event($id,NULL,NULL);

	    $this->data['most_popular_session']=$this->Leader_board_model->get_most_popular_session_by_event($id);
	    $this->data['most_checkin_session']=$this->Leader_board_model->get_most_check_in_session_by_event($id);
	    $survey_question_pie_chart=$this->Leader_board_model->get_survey_question_by_pie_chart($id,NULL,NULL);
	   
	    $final_Array_chart = array();
	    foreach ($survey_question_pie_chart as $key => $value) 
	    {
	      $final_Array_chart[$value['Question']]['Q_id']=$value['Id'];
	      $final_Array_chart[$value['Question']][$value['panswer']][] = array(
	        'id' => $value['puserid'],
	        'Firstname' => $value['Firstname'],
	        'Lastname' => $value['Lastname'],
	        'Email' => $value['Email']
	      );
	    }
	    $final_Array_chart1=array();
	    foreach($final_Array_chart as $key => $value)
	    {
	      foreach ($value as $i => $j) 
	      {
	        if(is_array($j))
	        {   
	          $final_Array_chart1[$key]['chart'][$i] = count($j);
	        }
	        else
	        {
	          $final_Array_chart1[$key][$i]=$j;
	        }
	      }
	    }

	    $this->data['survey_question_pie_chart']=$final_Array_chart1;
	    $this->data['forms_data']=$this->Leader_board_model->get_all_forms_by_event($id,NULL,NULL,$limit);

	    $this->data['cms_click_data']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,Null,$from_datetime,$to_datetime);
	    $this->data['most_popular_cms']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,5,$from_datetime,$to_datetime);
	    $this->data['cms_click_user']=$this->Leader_board_model->get_cms_click_user_by_event($id,$from_datetime,$to_datetime,$limit);
	    $this->data['advert_click_data']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,Null,NULL,NULL);
	    $this->data['most_popular_advert']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,5,NULL,NULL);
	    $this->data['advert_click_user']=$this->Leader_board_model->get_adverts_click_user_by_event($id,NULL,NULL,$limit);
	    $this->data['top_profileviews_sponsors']=$this->Leader_board_model->get_top_views_profiles_sponsors($id,Null,NULL,$limit);
	    $this->data['visit_soonsor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_sponsors_profiles($id,NULL,NULL,$limit);
	    $this->data['most_user_messages']=$this->Leader_board_model->get_user_send_messages_by_event($id,NULL,NULL,$limit);

      $this->data['most_user_photos_comment']=$this->Leader_board_model->get_user_comment_by_event_photos($id,NULL,NULL,$limit);
      $this->data['most_user_msg_comment']=$this->Leader_board_model->get_user_comment_by_event_msg($id,NULL,NULL,$limit);
	     $this->data['most_user_activity_comment']=$this->Leader_board_model->get_user_comment_by_event_activity($id,NULL,NULL,$limit);

	     $this->data['most_user_shared_photos']=$this->Leader_board_model->get_most_shared_photos_user_by_event($id,Null,Null,$limit);
	     $this->data['most_user_likes']=$this->Leader_board_model->get_user_likes_by_event($id,NULL,NULL,$limit);
	     $this->data['swap_contact_user']=$this->Leader_board_model->get_swap_contact_user_by_event($id,Null,NULL,$limit);

	    $this->data['top_profileviews_exibitor']=$this->Leader_board_model->get_top_views_profiles_exhibitor($id,Null,NULL,$limit);
	    $this->data['visit_exhibitor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_exhibitor_profiles($id,NULL,NULL,$limit);
	    $this->data['top_exibitor_have_request_meeting']=$this->Leader_board_model->get_top_exhibitor_have_requested_meeting($id,NULL,NULL,$limit);
	    $this->data['top_requested_meeting_attendee']=$this->Leader_board_model->get_top_attendee_have_requested_meeting($id,NULL,NULL,$limit);
	    $this->data['top_exibitor_by_favorited']=$this->Leader_board_model->get_top_exibitor_have_favorites($id,NULL,NULL,$limit);
	    $this->data['top_sponsors_by_favorited']=$this->Leader_board_model->get_top_sponsors_have_favorites($id,NULL,NULL,$limit);
	    $this->data['top_speakers_by_favorited']=$this->Leader_board_model->get_top_speakers_have_favorites($id,NULL,NULL,$limit);
	    $this->data['top_attendee_by_favorited']=$this->Leader_board_model->get_top_attendees_have_favorites($id,NULL,NULL,$limit);
	    $menudata = $this->Event_model->geteventmenu($id, 8);
	    $menu_toal_data = $this->Event_model->get_total_menu($id);
	    $this->data['menu_toal_data'] = $menu_toal_data;
	    $this->data['event_id'] = $id;
	    $this->data['menu_id'] = $menudata[0]->id;
	    $this->data['title'] = $menudata[0]->menuname;
	    $this->data['img'] = $menudata[0]->img;
	    $this->data['img_view'] = $menudata[0]->img_view;
	    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
	    $user = $this->session->userdata('current_user');
	    $this->template->write_view('css', 'admin/css', $this->data, true);
	    $this->template->write_view('header', 'common/header', $this->data, true);
	    $this->template->write_view('content', 'leader_board/index_temp', $this->data, true);
	    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
	    $this->template->write_view('js', 'leader_board/leader_js', $this->data, true);
	    $this->template->render();
  }

  public function index($id)
  {
      $limit = 25;
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
      $user = $this->session->userdata('current_user');
      $roleid = $user[0]->Role_id;
      $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
      $this->data['users_role']=$user_role;
      $orid = $this->data['user']->Id;
      if ($this->data['user']->Role_name == 'User')
      {
        $total_permission = $this->Agenda_model->get_permission_list();
        $this->data['total_permission'] = $total_permission;
      }
      if ($this->data['user']->Role_name == 'Client')
      {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
      }
      $currency=$this->Leader_board_model->get_current_byevent($id);
      $this->data['currency']=$currency;

      $this->data['total_register_user']=$this->Leader_board_model->get_total_register_user_by_event($id,$event[0]['Organisor_id'],NULL,NULL,$limit);
      $this->data['most_popular_area']=$this->Leader_board_model->get_most_popular_areas_by_event($id,NULL,NULL,NULL);

      $this->data['most_active_user']=$this->Leader_board_model->get_most_active_user_by_event($id,NULL,NULL,$limit);

      $this->data['most_login_os']=$this->Leader_board_model->get_most_login_operating_system_by_event($id,NULL,NULL,$limit);
      
      $this->data['agenda'] = $this->Leader_board_model->get_all_agenda_list($id,$limit);
      $this->data['top_rated_session']=$this->Leader_board_model->get_top_rated_session_by_event($id,NULL,NULL);

      $this->data['most_popular_session']=$this->Leader_board_model->get_most_popular_session_by_event($id);
      $this->data['most_checkin_session']=$this->Leader_board_model->get_most_check_in_session_by_event($id);
      $survey_question_pie_chart=$this->Leader_board_model->get_survey_question_by_pie_chart($id,NULL,NULL);
     
      $final_Array_chart = array();
      foreach ($survey_question_pie_chart as $key => $value) 
      {
        $final_Array_chart[$value['Question']]['Q_id']=$value['Id'];
        $final_Array_chart[$value['Question']][$value['panswer']][] = array(
          'id' => $value['puserid'],
          'Firstname' => $value['Firstname'],
          'Lastname' => $value['Lastname'],
          'Email' => $value['Email']
        );
      }
      $final_Array_chart1=array();
      foreach($final_Array_chart as $key => $value)
      {
        foreach ($value as $i => $j) 
        {
          if(is_array($j))
          {   
            $final_Array_chart1[$key]['chart'][$i] = count($j);
          }
          else
          {
            $final_Array_chart1[$key][$i]=$j;
          }
        }
      }

      $this->data['survey_question_pie_chart']=$final_Array_chart1;
      $this->data['forms_data']=$this->Leader_board_model->get_all_forms_by_event($id,NULL,NULL,$limit);

      $this->data['cms_click_data']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,Null,$from_datetime,$to_datetime);
      $this->data['most_popular_cms']=$this->Leader_board_model->get_all_and_most_popular_cms_by_event($id,5,$from_datetime,$to_datetime);
      $this->data['cms_click_user']=$this->Leader_board_model->get_cms_click_user_by_event($id,$from_datetime,$to_datetime,$limit);
      $this->data['advert_click_data']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,Null,NULL,NULL);
      $this->data['most_popular_advert']=$this->Leader_board_model->get_all_and_most_popular_advert_by_event($id,5,NULL,NULL);
      $this->data['advert_click_user']=$this->Leader_board_model->get_adverts_click_user_by_event($id,NULL,NULL,$limit);
      $this->data['top_profileviews_sponsors']=$this->Leader_board_model->get_top_views_profiles_sponsors($id,Null,NULL,$limit);
      $this->data['visit_soonsor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_sponsors_profiles($id,NULL,NULL,$limit);
      $this->data['most_user_messages']=$this->Leader_board_model->get_user_send_messages_by_event($id,NULL,NULL,$limit);

      $this->data['most_user_photos_comment']=$this->Leader_board_model->get_user_comment_by_event_photos($id,NULL,NULL,$limit);
      $this->data['most_user_msg_comment']=$this->Leader_board_model->get_user_comment_by_event_msg($id,NULL,NULL,$limit);
       $this->data['most_user_activity_comment']=$this->Leader_board_model->get_user_comment_by_event_activity($id,NULL,NULL,$limit);

       $this->data['most_user_shared_photos']=$this->Leader_board_model->get_most_shared_photos_user_by_event($id,Null,Null,$limit);
       $this->data['most_user_likes']=$this->Leader_board_model->get_user_likes_by_event($id,NULL,NULL,$limit);
       $this->data['swap_contact_user']=$this->Leader_board_model->get_swap_contact_user_by_event($id,Null,NULL,$limit);

      $this->data['top_profileviews_exibitor']=$this->Leader_board_model->get_top_views_profiles_exhibitor($id,Null,NULL,$limit);
      $this->data['visit_exhibitor_profiles_attendee']=$this->Leader_board_model->get_attendee_visited_exhibitor_profiles($id,NULL,NULL,$limit);
      $this->data['top_exibitor_have_request_meeting']=$this->Leader_board_model->get_top_exhibitor_have_requested_meeting($id,NULL,NULL,$limit);
      $this->data['top_requested_meeting_attendee']=$this->Leader_board_model->get_top_attendee_have_requested_meeting($id,NULL,NULL,$limit);
      $this->data['top_exibitor_by_favorited']=$this->Leader_board_model->get_top_exibitor_have_favorites($id,NULL,NULL,$limit);
      $this->data['top_sponsors_by_favorited']=$this->Leader_board_model->get_top_sponsors_have_favorites($id,NULL,NULL,$limit);
      $this->data['top_speakers_by_favorited']=$this->Leader_board_model->get_top_speakers_have_favorites($id,NULL,NULL,$limit);
      $this->data['top_attendee_by_favorited']=$this->Leader_board_model->get_top_attendees_have_favorites($id,NULL,NULL,$limit);
      $menudata = $this->Event_model->geteventmenu($id, 8);
      $menu_toal_data = $this->Event_model->get_total_menu($id);
      $this->data['menu_toal_data'] = $menu_toal_data;
      $this->data['event_id'] = $id;
      $this->data['menu_id'] = $menudata[0]->id;
      $this->data['title'] = $menudata[0]->menuname;
      $this->data['img'] = $menudata[0]->img;
      $this->data['img_view'] = $menudata[0]->img_view;
      $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
      $user = $this->session->userdata('current_user');
      $this->template->write_view('css', 'admin/css', $this->data, true);
      $this->template->write_view('header', 'common/header', $this->data, true);
      $this->template->write_view('content', 'leader_board/index_temp', $this->data, true);
      $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
      $this->template->write_view('js', 'leader_board/leader_js', $this->data, true);
      $this->template->render();
  }

  public function view_more($id,$type)
  {
    /*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/
    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];
    $akeyword = '';
    if($this->input->post('akeyword'))
    {
      $akeyword = $this->input->post('akeyword');
    }
    $this->data['akeyword'] = $akeyword;

    $ekeyword = '';
    if($this->input->post('ekeyword'))
    {
      $ekeyword = $this->input->post('ekeyword');
    }
    $this->data['ekeyword'] = $ekeyword;
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
    $this->data['users_role']=$user_role;
    $orid = $this->data['user']->Id;
    if ($this->data['user']->Role_name == 'User')
    {
      $total_permission = $this->Agenda_model->get_permission_list();
      $this->data['total_permission'] = $total_permission;
    }
    if ($this->data['user']->Role_name == 'Client')
    {
      $event = $this->Event_model->get_admin_event($id);
      $this->data['event'] = $event[0];
    }
    $currency=$this->Leader_board_model->get_current_byevent($id);
    $this->data['currency']=$currency;

    $this->load->library('pagination');
    $limit = 10;
    $start_index = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

    switch ($type) {
      case 'agenda':
        $this->data['agenda'] = $this->Leader_board_model->get_all_agenda_list($id,$limit,$start_index,$akeyword); 
        $total_records = $this->Leader_board_model->get_all_agenda_list_total($id,$akeyword);
        $config['base_url'] = base_url() . 'leader_board/view_more/'.$id.'/agenda';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit;
        $config["uri_segment"] = 5;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();      
        break;
      case 'exhibitors':
        $this->data['visit_exhibitor_profiles_attendee'] = $this->Leader_board_model->get_attendee_visited_exhibitor_profiles($id,null,null,$limit,$start_index,$ekeyword); 
        $total_records = $this->Leader_board_model->get_attendee_visited_exhibitor_profiles_total($id,null,null,null,null,$ekeyword);
        $config['base_url'] = base_url() . 'leader_board/view_more/'.$id.'/exhibitors';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit;
        $config["uri_segment"] = 5;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();       
        break;
      
      default:
        # code...
        break;
    }

    $menudata = $this->Event_model->geteventmenu($id, 8);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $this->data['menu_toal_data'] = $menu_toal_data;
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
    $this->data['type'] =$type;
    $user = $this->session->userdata('current_user');
    $this->template->write_view('css', 'admin/css', $this->data, true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'leader_board/index_view_more', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'leader_board/leader_js', $this->data, true);
    $this->template->render();
  }

}
