<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Photos_admin extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Photos';
          $this->data['smalltitle'] = 'Switch on Photos to allow Users to upload Photos to the Photos feed.';
          $this->data['breadcrumb'] = 'Photos';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('11',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
                    
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               //$eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());

               $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('11',$menu_list))
          {
               $this->load->model('Photos_model');
               $this->load->model('Setting_model');
               $this->load->model('Agenda_model');               
               $this->load->model('Profile_model');
               $this->load->model('Event_template_model');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $menudata = $this->Event_model->geteventmenu($id, 11);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;


          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;

               $this->template->write_view('css', 'admin/css', true);
               $this->template->write_view('content', 'agenda/index', true);
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               $this->template->write_view('js', 'admin/js', true);
               $this->template->render();
          }
          
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $event = $this->Photos_model->get_eventinfo($id);
          $this->data['Event'] = $event;

          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'photos_admin/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'photos_admin/js', $this->data, true);
          $this->template->render();
     }

}
