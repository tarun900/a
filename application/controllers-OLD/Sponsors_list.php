<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sponsors_list extends CI_Controller
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Sponsors';
          $this->data['smalltitle'] = 'Sponsors';
          $this->data['breadcrumb'] = 'Sponsors';
          parent::__construct($this->data);
          $this->load->library('formloader');
          $this->load->library('formloader1');
          $this->load->model('Agenda_model');
          $this->load->library('formloader1');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $user = $this->session->userdata('current_user');
          $cnt = 0;
          $eventname=$this->Event_model->get_all_event_name();
          if(in_array($this->uri->segment(3),$eventname))
          {
               if ($user != '')
               {
                    $parameters = $this->uri->uri_to_assoc(1);
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    if(!empty($roledata))
                    {
                         $roleid = $roledata[0]->Role_id;
                         $eventid = $event_templates[0]['Id'];
                         $rolename = $roledata[0]->Name;
                         $cnt = 0;
                         $req_mod = ucfirst($this->router->fetch_class());
                         $cnt = 1;
                    }
                    else
                    {
                         $cnt=0 ;
                    }
                    
                    if ($cnt == 1)
                    {
                         $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                         $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                         $this->data['pagetitle'] = "Cms";
                         $parameters = $this->uri->uri_to_assoc(1);
                         $parameters = $this->uri->uri_to_assoc(1);
                         redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
                    }
               }
          }
          else
          {
               $parameters = $this->uri->uri_to_assoc(1);
               redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);  
          }
     }

     public function index($acc_name,$Subdomain = NULL,$intFormId=NULL)
     {    
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;
          $user = $this->session->userdata('current_user');
          if(!empty($user[0]->Id))
          {
            $current_date=date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id,$current_date,'43',$event_templates[0]['Id']); 
          }
          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          for($i=0;$i<count($menu_list);$i++)
          {
            if('Sponsors_list'==$menu_list[$i]['pagetitle'])
            {
                $mid=$menu_list[$i]['id'];
            }
          }
          $this->data['menu_id']=$mid;
          $this->data['menu_list'] = $menu_list;

          $eid=$event_templates[0]['Id'];
          $res=$this->Agenda_model->getforms($eid,$mid);
          $this->data['form_data']=$res;
          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;
          $array_temp_past = $this->input->post();
          $user = $this->session->userdata('current_user');
          if(!empty($array_temp_past))
          {
            
            $aj=json_encode($this->input->post());
            $formdata=array('f_id' =>$intFormId,
                'm_id'=>$mid,
                'user_id'=>$user[0]->Id,
                'event_id'=>$eid,
                'json_submit_data'=>$aj
             );
           $this->Agenda_model->formsinsert($formdata);
           redirect(base_url().'Sponsors_list/'.$acc_name.'/'.$Subdomain);
           //echo '<script>window.location.href="'.base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'"</script>';
           exit;
          }
          if(!empty($user[0]->Id)){
            $my_favorites=$this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'],$user[0]->Id,'43');
            $this->data['my_favorites']=$my_favorites;
          }

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;
		  
          $sponsors_data=$this->Event_template_model->get_sponsors_list_by_type($Subdomain);
          $this->data['sponsors_data'] = $sponsors_data;
          $sponsors_list = $this->Event_template_model->get_sponsors_list($Subdomain);
          $this->data['sponsors_list'] = $sponsors_list;
          
          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;

          $this->data['Subdomain'] = $Subdomain;

          $user = $this->session->userdata('current_user');
          $this->data['Subdomain'] = $Subdomain;
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
           if ($event_templates[0]['Event_type'] == '1')
           {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'sponsors_list/newindex', $this->data, true);
                  $this->template->write_view('footer', 'sponsors_list/footer', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '2')
           {

              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'sponsors_list/newindex', $this->data, true);
                 $this->template->write_view('footer', 'sponsors_list/footer', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '3') 
           {
              $this->session->unset_userdata('acc_name');
              $acc['acc_name'] =  $acc_name;
              $this->session->set_userdata($acc);
              $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('43',$event_templates[0]['Id']);
              if($isforcelogin['is_force_login']=='1' && empty($user))
              {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
              }
              else
              {
                $this->template->write_view('content', 'sponsors_list/newindex', $this->data, true);
                $this->template->write_view('footer', 'sponsors_list/footer', $this->data, true);
              }
           }
           else
           {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'sponsors_list/newindex', $this->data, true);
                  $this->template->write_view('footer', 'sponsors_list/footer', $this->data, true);
              }
           }
           $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
           $this->template->render();  
         
     }

     public function View($acc_name,$Subdomain = NULL, $id = null)
     {
          $uid = $this->Event_template_model->get_sp_by_page($id);
          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;
           $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
               $this->data['fb_login_data'] = $fb_login_data;
               $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;
          $sponsors_data = $this->Event_template_model->get_sponsors_data($Subdomain, $id);
          $this->data['sponsors_data'] = $sponsors_data;
          $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);  
          $this->data['user_url'] = $user_url;
          $this->data['Subdomain'] = $Subdomain;
          $this->data['sp_id'] = $uid;
          $this->data['Sid'] = $uid;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          if($user[0]->Role_id==4)
          {
            $this->Event_model->add_visit_profile_hit($event_templates[0]['Id'],$id,$user[0]->Id);
            $custom=$this->Event_model->get_user_custom_clounm($user[0]->Id,$event_templates[0]['Id']);
            $this->data['custom']=$custom;
          }
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
           if ($event_templates[0]['Event_type'] == '1')
           {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'sponsors_list/user', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '2')
           {

              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                 $this->template->write_view('content', 'sponsors_list/user', $this->data, true);
              }
           }
           elseif ($event_templates[0]['Event_type'] == '3') 
           {
              $this->session->unset_userdata('acc_name');
              $acc['acc_name'] =  $acc_name;
              $this->session->set_userdata($acc);
              $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('43',$event_templates[0]['Id']);
              if($isforcelogin['is_force_login']=='1' && empty($user))
              {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
              }
              else
              {
                $this->template->write_view('content', 'sponsors_list/user', $this->data, true);
              }
           }
           else
           {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'sponsors_list/user', $this->data, true);
              }
           }
           $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
           $this->template->render();
     }

}
