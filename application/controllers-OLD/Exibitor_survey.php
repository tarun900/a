<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exibitor_survey extends FrontendController
{
	function __construct()
	{
		$this->data['pagetitle'] = 'Polls & Feedback';
		$this->data['smalltitle'] = 'Design multiple, flexible surveys for attendees to complete';
		$this->data['breadcrumb'] = 'Polls & Feedback';
		$this->data['page_edit_title'] = 'edit';
		parent::__construct($this->data);
		$this->load->model('Agenda_model');
		$this->load->model('Event_model');
		$this->load->model('Event_template_model');
		$this->load->model('Exibitor_model');
		$this->load->model('Exibitor_survey_model');
		$user = $this->session->userdata('current_user');
        $eventid=$this->uri->segment(3);

        $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

        $event = $this->Event_model->get_admin_event($eventid);
        $this->data['Subdomain'] = $event[0]['Subdomain'];
        $premission=$this->Exibitor_model->get_exibitor_user_premission($eventid,$user[0]->Id);
        /*if($premission[0]['add_surveys']!='1' && $event[0]['allow_custom_survey_lead']!='1')
        {
        	redirect('forbidden');
        }
        else
        {*/
	        $this->data['exibitor_premission']=$premission;
	        $event = $this->Event_model->get_admin_event($eventid);
			$this->data['event'] = $event[0];
			$user_role=$this->Event_template_model->get_menu_list($user[0]->Role_id,$eventid);
			$this->data['users_role']=$user_role;
			$this->data['event_id'] = $eventid;
        // }
	}
	public function index($id)
	{
		$user_id = $this->data['user']->Id;
		$questions=$this->Exibitor_survey_model->get_all_exibitor_user_questions($id,$user_id);
		$this->data['question_list']=$questions;
		$this->template->write_view('css', 'admin/css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'exibitor_survey/index', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'exibitor_survey/js', $this->data, true);
		$this->template->render();
	}
	public function add($id)
	{
		$user_id = $this->data['user']->Id;
		if($this->input->post())
		{
			$question_array['event_id']=$id;
			$question_array['exibitor_user_id']=$user_id;
			$question_array['Question']=$this->input->post('Question');
			$question_array['Question_type']=$this->input->post('Question_type');
			if($this->input->post('Question_type')=='1' || $this->input->post('Question_type')=='2')
			{
				$question_array['Option']=json_encode($this->input->post('Option'));
			}
			else
			{
				$question_array['Option']=NULL;	
			}
			$question_array['show_commentbox']=$this->input->post('show_commentbox')=='1' ? '1' : '0';
			$question_array['commentbox_display_style']=$this->input->post('commentbox_display_style')=='1' ? '1' : '0';
			$question_array['commentbox_label_text']= !empty($this->input->post('commentbox_label_text')) ? $this->input->post('commentbox_label_text') : NULL;
			$question_id=$this->Exibitor_survey_model->save_exibitor_user_question($question_array,NULL);
			$this->session->set_flashdata('survey_data', 'Question Added Successfully...');
			if($this->data['user']->role_type == '1' )		
					redirect("Event/eventhomepage/".$id);		
			if($this->data['user']->Role_id == '3')		
					redirect("Lead_retrieval_admin/index/".$id);
            redirect("Exibitor_survey/index/".$id);
		}
		$this->template->write_view('css', 'admin/add_css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'exibitor_survey/add', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'exibitor_survey/js', $this->data, true);
		$this->template->render();
	}
	public function edit($id,$qid)
	{
		$user_id = $this->data['user']->Id;
		$question=$this->Exibitor_survey_model->get_exibitor_user_edit_question($qid,$id,($this->data['user']->role_type == '1') ? $this->data['user']->Id : null);
		$this->data['question']=$question;
		$skiplogic=$this->Exibitor_survey_model->get_skip_logic_by_question_id($qid);
        $this->data['skiplogic']=$skiplogic;
        $Surveys = $this->Exibitor_survey_model->get_question_for_skiplogic_add($id,$user_id,$qid);
        $this->data['questions_list'] = $Surveys;
		if($this->input->post())
		{
			$question_array['Question']=$this->input->post('Question');
			$question_array['Question_type']=$this->input->post('Question_type');
			if($this->input->post('Question_type')=='1' || $this->input->post('Question_type')=='2')
			{
				$question_array['Option']=json_encode($this->input->post('Option'));
			}
			else
			{
				$question_array['Option']=NULL;	
			}
			$question_array['show_commentbox']=$this->input->post('show_commentbox')=='1' ? '1' : '0';
			$question_array['commentbox_display_style']=$this->input->post('commentbox_display_style')=='1' ? '1' : '0';
			$question_array['commentbox_label_text']= !empty($this->input->post('commentbox_label_text')) ? $this->input->post('commentbox_label_text') : NULL;
			$question_id=$this->Exibitor_survey_model->save_exibitor_user_question($question_array,$qid);
			$arroption=!empty($this->input->post('Option')) ? $this->input->post('Option') : array();
            $older_option=!empty($this->input->post('older_option')) ? $this->input->post('older_option') : array();
            foreach ($older_option as $key => $value) 
            {
                if($value != $arroption[$key])
                {
                    $this->Exibitor_survey_model->update_option_in_skiplogic($qid,str_ireplace(" ","_",$value),str_ireplace(" ","_",$arroption[$key]));
                }
            }
            $delete_option=array_diff(array_filter(json_decode($survey[0]['Option'],true)),$older_option);
            foreach ($delete_option as $key => $value) 
            {
                $this->Exibitor_survey_model->delete_skip_login_option($qid,$value);
            }
			$this->session->set_flashdata('survey_data', 'Question Updated Successfully...');
			if($this->data['user']->role_type == '1')		
					redirect("Event/eventhomepage/".$id);		
			if($this->data['user']->Role_id == '3')		
					redirect("Lead_retrieval_admin/index/".$id);
            redirect("Exibitor_survey/index/".$id);
		}
		$this->template->write_view('css', 'admin/add_css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'exibitor_survey/edit', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'exibitor_survey/js', $this->data, true);
		$this->template->render();
	}
	public function reorder_question($id)
	{
		$user_id = $this->data['user']->Id;
		$questions=$this->Exibitor_survey_model->get_all_exibitor_user_questions($id,$user_id);
		$this->data['questions']=$questions;
		if ($this->input->post())
		{
			foreach ($this->input->post('question_ids') as $key => $value) {
			    $this->Exibitor_survey_model->save_question_ordering($value,$key+1);
			}
			if($this->data['user']->role_type == '1')		
					redirect("Event/eventhomepage/".$id);		
			if($this->data['user']->Role_id == '3')		
					redirect("Lead_retrieval_admin/index/".$id);
			$this->session->set_flashdata('survey_data', 'Question Ordering Save Successfully...');
			redirect("Exibitor_survey/index/".$id);
		}
		$this->template->write_view('css', 'admin/add_css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'exibitor_survey/order_question_views', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'exibitor_survey/js', $this->data, true);
		$this->template->render();
	}
	public function save_skiplogic($eid,$qid)
	{
		$question=$this->Exibitor_survey_model->get_exibitor_user_edit_question($qid,$eid);
		$Option=array_filter(json_decode($question['Option'],true));
		foreach ($Option as $key => $value) {
		   $this->Exibitor_survey_model->save_question_skiplogin($qid,str_ireplace(" ","_",$value),$this->input->post('question_id__'.str_ireplace(" ","_",$value)));
		}
		$this->session->set_flashdata('survey_data', 'Question Skip Logic Save Successfully...');
		if($this->data['user']->role_type == '1')		
			redirect("Event/eventhomepage/".$eid);
		redirect("Exibitor_survey/index/".$eid);
	}
	public function delete_question($eid,$qid)
	{
		$this->Exibitor_survey_model->remove_exibitor_user_question($qid);
		$this->session->set_flashdata('survey_data', 'Question Deleted Successfully...');
		if($this->data['user']->role_type == '1')		
			redirect("Event/eventhomepage/".$eid);		
		if($this->data['user']->Role_id == '3')		
			redirect("Lead_retrieval_admin/index/".$eid);
		redirect("Exibitor_survey/index/".$eid);
	}
}