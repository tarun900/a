<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Lead_retrieval extends CI_Controller
{
  function __construct()
  {
    $this->data['pagetitle'] = 'Lead Retrival';
    $this->data['smalltitle'] = 'Lead Retrival';
    $this->data['breadcrumb'] = 'Lead Retrival';
    parent::__construct($this->data);
    $this->load->library('formloader');
    $this->load->library('formloader1');
    $this->template->set_template('front_template');
    $this->load->model('Event_template_model');
    $this->load->model('Lead_retrieval_model');
    $this->load->model('Cms_model');
    $this->load->model('Event_model');
    $this->load->model('Setting_model');
    $this->load->model('Speaker_model');
    $this->load->model('Message_model');
    $this->load->model('Notes_admin_model');
    $this->load->model('Profile_model');
    $this->load->model('Agenda_model');
    $user = $this->session->userdata('current_user');
    $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
    $eventid = $event_val[0]['Id'];
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);
    if(!in_array('53',$menu_list))
    {
      echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
    }
    $eventname=$this->Event_model->get_all_event_name();
    if(in_array($this->uri->segment(3),$eventname))
    {
      if ($user != '')
      {
        $parameters = $this->uri->uri_to_assoc(1);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
        $logged_in_user_id=$user[0]->Id;
        $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
        if ($cnt == 1)
        {
          $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
          $this->data['notes_list'] = $notes_list;
        }
        else
        {
          $event_type=$event_templates[0]['Event_type'];
          if($event_type==3)
          {
            $this->session->unset_userdata('current_user');
            $this->session->unset_userdata('invalid_cred');
            $this->session->sess_destroy();
          }
          else
          {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain=$this->uri->segment(3);
            $acc_name=$this->uri->segment(2);
            echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
          }
        }
      }
    }
    else
    {
      $parameters = $this->uri->uri_to_assoc(1);
      $Subdomain=$this->uri->segment(3);
      $flag=1;
      echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
    }
  }
  public function index($acc_name=NULL,$Subdomain = NULL,$intFormId=NULL)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $user = $this->session->userdata('current_user');
    $exibitor_user_id=$user[0]->Id;
    if($user[0]->Role_id=='4')
    {
      $reps_data=$this->Lead_retrieval_model->get_exibitor_by_representative_id_in_event($event_templates[0]['Id'],$exibitor_user_id);
      $exibitor_user_id=$reps_data['exibitor_user_id'];
    }
    $exibitor_premission=$this->Lead_retrieval_model->get_exibitor_user_premission($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['exibitor_premission']=$exibitor_premission;
    $total_reps_add=$this->Lead_retrieval_model->get_total_representative_by_exibitor_user($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['total_reps_add']=$total_reps_add;
    $my_lead=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['my_lead']=$my_lead;
    $my_representatives=$this->Lead_retrieval_model->get_all_my_representative_by_exibitor_user($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['my_representatives']=$my_representatives;
    $attendee_user=$this->Lead_retrieval_model->get_event_all_attendee_from_assing_resp($event_templates[0]['Id']);
    $this->data['attendee_user']=$attendee_user;
    if(!empty($user[0]->Id))
    {
      $req_mod = $this->router->fetch_class();
      if($req_mod=="lead_retrieval")
      {
        $req_mod="lead_retrieval";
      }
      $menu_id=$this->Event_model->get_menu_id($req_mod);
      $current_date=date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id,$current_date,$menu_id,$event_templates[0]['Id']); 
    }
    $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting']=$notificationsetting;
    $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
    $this->data['sign_form_data']=$res1;
    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $this->data['fb_login_data'] = $fb_login_data;
    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    for($i=0;$i<count($menu_list);$i++)
    {
      if('lead_retrieval'==$menu_list[$i]['pagetitle'])
      {
        $mid=$menu_list[$i]['id'];
      }
    }
    $this->data['menu_id']=$mid;
    $this->data['menu_list'] = $menu_list;
    $eid=$event_templates[0]['Id'];
    $res=$this->Agenda_model->getforms($eid,$mid);
    $this->data['form_data']=$res;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $user = $this->session->userdata('current_user');
    $this->data['Subdomain'] = $Subdomain;
    $intFormBuild = $this->input->post('hdnFormBuild');
    if($intFormBuild==1)
    {
      unset($_POST['hdnFormBuild']);
      $aj=json_encode($this->input->post());
      $formdata=array('f_id' =>$intFormId,
        'm_id'=>$mid,
        'user_id'=>$user[0]->Id,  
        'event_id'=>$eid,
        'json_submit_data'=>$aj
      );
      $this->Agenda_model->formsinsert($formdata);
      echo '<script>window.location.href="'.base_url().'lead_retrieval/'.$acc_name."/".$Subdomain.'"</script>';
      exit;
    }
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'lead_retrieval/index', $this->data, true);
        $this->template->write_view('footer', 'lead_retrieval/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'lead_retrieval/index', $this->data, true);
        $this->template->write_view('footer', 'lead_retrieval/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] =  $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('2',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {    
        $this->template->write_view('content', 'lead_retrieval/index', $this->data, true);
        $this->template->write_view('footer', 'lead_retrieval/footer', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'lead_retrieval/index', $this->data, true);
        $this->template->write_view('footer', 'lead_retrieval/footer', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }
  public function scan_lead_user($acc_name,$Subdomain,$uid)
  {
    $user = $this->session->userdata('current_user');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $exibitor_user_id=$user[0]->Id;
    if($user[0]->Role_id=='4')
    {
      $reps_data=$this->Lead_retrieval_model->get_exibitor_by_representative_id_in_event($event_templates[0]['Id'],$exibitor_user_id);
      $exibitor_user_id=$reps_data['exibitor_user_id'];
    }
    elseif($user[0]->role_type == '1')
    {
      $reps_data = $this->Lead_retrieval_model->get_lead_user_by_rep($event_templates[0]['Id'],$exibitor_user_id);
      $exibitor_user_id=$reps_data['exibitor_user_id'];
    }
    $custom_column=$this->Lead_retrieval_model->get_all_custom_column_data($event_templates[0]['Id']);
    $this->data['custom_column']=$custom_column;
    $this->data['acc_name']=$acc_name;
    $this->data['Subdomain']=$Subdomain;
    if($this->input->post())
    {
      $lead_user_id=$this->input->post('lead_user_id');
      $lead_data['firstname']=$this->input->post('firstname');
      $lead_data['lastname']=$this->input->post('lastname');
      $lead_data['email']=$this->input->post('email');
      $lead_data['title']=$this->input->post('title');
      $lead_data['company_name']=$this->input->post('company_name');
      if($reps_data['rep_id'])
        $lead_data['rep_id'] = $reps_data['rep_id'];
      $custom_column_data=array();
      foreach ($custom_column as $key => $value) {
        $custom_column_data[$value['column_name']]=$this->input->post($value['column_id']);
      }
      $lead_data['custom_column_data']=json_encode($custom_column_data);
      $this->Lead_retrieval_model->save_scan_lead_data($lead_data,$event_templates[0]['Id'],$exibitor_user_id,$lead_user_id);

      $question = $this->input->post('question');
      foreach ($question as $key => $val)
      {
        $question_data = array();
        $question_data['Answer'] = is_array($this->input->post('question_option'.$val)) ? implode(',',array_filter($this->input->post('question_option'.$val))) : $this->input->post('question_option'.$val);
        $question_data['answer_comment'] = empty($this->input->post('answer_comment'.$val)) ? NULL : $this->input->post('answer_comment'.$val);
        $this->Lead_retrieval_model->add_question_answer($question_data,$val,$lead_user_id,$event_templates[0]['Id']);
      }
      $this->data['save_success']=true;
      $this->load->view('lead_retrieval/scan_lead_user_views',$this->data);
    }
    $attendee=$this->Lead_retrieval_model->get_edit_attendee_informaion_event_by_user_id($event_templates[0]['Id'],$uid);
    $this->data['attendee_user']=$attendee;
    $questions=$this->Lead_retrieval_model->get_all_exibitor_user_questions($event_templates[0]['Id'],$exibitor_user_id,$uid);
    $this->data['questions']=$questions;
    $this->data['save_success']=false;
    $this->load->view('lead_retrieval/scan_lead_user_views',$this->data);
  }
  public function add_new_resp($acc_name,$Subdomain)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $exibitor_premission=$this->Lead_retrieval_model->get_exibitor_user_premission($event_templates[0]['Id'],$user[0]->Id);
    $limit_error=0;
    foreach ($this->input->post('resp_ids') as $key => $value) 
    {
      $total_reps_add=$this->Lead_retrieval_model->get_total_representative_by_exibitor_user($event_templates[0]['Id'],$user[0]->Id);
      if($total_reps_add <= $exibitor_premission['maximum_Reps'] || empty($exibitor_premission['maximum_Reps']))
      {
        $this->Lead_retrieval_model->add_new_representative($event_templates[0]['Id'],$user[0]->Id,$value); 
      }
      else{
        break;
        $limit_error=1;
      }
    }
    if($limit_error==1)
    {
      $this->session->set_flashdata('lead_error','Add Representative Limit exceeded...');
    }
    else
    {
      $this->session->set_flashdata('lead_success','Representative Added SuccessFully...');
    }
    redirect('Lead_retrieval/'.$acc_name.'/'.$Subdomain);
  }
  public function export_my_lead($acc_name,$Subdomain)
  {
    $user = $this->session->userdata('current_user');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $exibitor_user_id=$user[0]->Id;
    if($user[0]->Role_id=='4')
    {
      $reps_data=$this->Lead_retrieval_model->get_exibitor_by_representative_id_in_event($event_templates[0]['Id'],$exibitor_user_id);
      $exibitor_user_id=$reps_data['exibitor_user_id'];
    }
    $custom_column=$this->Lead_retrieval_model->get_all_custom_column_data($event_templates[0]['Id']);
    $this->data['custom_column']=$custom_column;
    $questions=$this->Lead_retrieval_model->get_all_exibitor_user_questions_for_export($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['questions']=$questions;
    $event_id = $event_templates[0]['Id'];
    $filename = "lead_user_detail.csv";
    $fp = fopen('php://output', 'w');
    $header[] = "FirstName";
    $header[] = "LastName";
    $header[] = "Email";
    $header[] = "Title";
    $header[] = "Company Name";
    $header[] = "Country";
    $header[] = "Phone";
    if($event_id == '634' || $event_id == '1012')
    {
      $header[] = "Salutation";
      $header[] = "Country";
      $header[] = "Suburb";
      $header[] = "State";
      $header[] = "Mobile";
      $header[] = "Telephone";
      $header[] = "Badge Number";
    }
    elseif($event_id == '1378')
    { 
      $header[] = "Address Line 1";
      $header[] = "Address Line 2";
      $header[] = "State";
      $header[] = "Zip";
      $header[] = "Badge Number";
    }
    elseif($event_id == '1512')
    {
        $header[] = 'Address Line 1,2';
        $header[] = 'City';
        $header[] = 'State';
        $header[] = 'Zip (Postal Code)';
        $header[] = "Badge Number";
    }

    $header[] = "Lead Rep";
    $header[] = "Lead Scan Date";

    if($event_id != '634')
    { 
      if($event_id != '1012'):
      foreach ($custom_column as $key => $value) {
        $header[]=ucfirst($value['column_name']);
      }
      endif;
    }
    foreach ($questions as $key => $value) {
      $header[]=$value['Question'];
    }
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp, $header);
    $my_lead=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user_test($event_templates[0]['Id'],$exibitor_user_id);
    //j($my_lead);
    foreach ($my_lead as $key => $value) 
    { 
      $data['FirstName']=$value['Firstname'];
      $data['LastName']=$value['Lastname'];
      $data['Email']=$value['Email'];
      $data['Title']=$value['Title'];
      $data['Company Name']=$value['Company_name'];
      $data['Country']=$value['country'];
      $data['Mobile']=$value['mobile'];

      if($event_id == '634' || $event_id == '1012')
      {
        $data['Salutation']=$value['salutation'];
        $data['Country']=$value['country'];
        $data['Suburb']=$value['Suburb'];
        $data['State']=$value['State'];
        $data['Mobile']=$value['mobile'];
        $data['Telephone']=$value['Phone_business'];
        $data['Badge Number']=$value['badgeNumber'];
      }
      elseif ($event_id == '1378')
      { 
        $data['Address Line 1'] = $value['a1'];
        $data['Address Line 2'] = $value['a2'];
        $data['State'] = $value['state'];
        $data['Zip'] = $value['zip'];
        $data['Badge Number']=$value['badgeNumber'];
      }
      elseif ($event_id == '1512')
      {
        $data['Address Line 1,2'] = $value['Street'];
        $data['City'] = $value['Suburb'];
        $data['State'] = $value['State'];
        $data['Zip (Postal Code)'] = $value['Postcode'];
        $data['Badge Number']=$value['badgeNumber'];
      }

      $data['Lead Rep']=$value['lead_rep'];
      $data['Lead Scan Date']=$value['created_date'];
      $custom_column_data=json_decode($value['custom_column_data'],true);

      if($event_id != '634')
      { 
        if($event_id != '1012'):
        foreach ($custom_column as $ckey => $cvalue) 
        {
          $data[ucfirst($cvalue['column_name'])]=$custom_column_data[$cvalue['column_name']];
        }
      endif;
      }
      // $ans=$this->Lead_retrieval_model->get_user_question_answer_all($event_templates[0]['Id'],$value['Id'],$user[0]->Id);
      
      $exibitor_ques = explode('~~',$value['exibitor_ques']);
      $exibitor_ans = explode('~~',$value['exibitor_ans']);
      foreach ($exibitor_ques as $keyqu => $valuequ)
      {
        $tmp_ans[$valuequ] = $exibitor_ans[$keyqu];
      }
      foreach ($questions as $qkey => $qvalue) 
      {
        // $data[]= !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment']; 
        $data[] = $tmp_ans[$qvalue['q_id']];
      }
      unset($tmp_ans);
      fputcsv($fp, $data);
      unset($data);
    }
  }
  public function email_my_lead($acc_name,$Subdomain)
  {
    $user = $this->session->userdata('current_user');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $exibitor_user_id=$user[0]->Id;
    if($user[0]->Role_id=='4')
    {
      $reps_data=$this->Lead_retrieval_model->get_exibitor_by_representative_id_in_event($event_templates[0]['Id'],$exibitor_user_id);
      $exibitor_user_id=$reps_data['exibitor_user_id'];
    }
    $custom_column=$this->Lead_retrieval_model->get_all_custom_column_data($event_templates[0]['Id']);
    $this->data['custom_column']=$custom_column;
    $questions=$this->Lead_retrieval_model->get_all_exibitor_user_questions_for_export($event_templates[0]['Id'],$exibitor_user_id);
    $this->data['questions']=$questions;
    $event_id = $event_templates[0]['Id'];
    $filename = "lead_user_detail.csv";
    $fp = fopen($filename, 'w');
    $header[] = "FirstName";
    $header[] = "LastName";
    $header[] = "Email";
    $header[] = "Title";
    $header[] = "Company Name";
    if($event_id == '634' || $event_id == '1012')
    {
      $header[] = "Salutation";
      $header[] = "Country";
      $header[] = "Mobile";
      $header[] = "Badge Number";
    }
    elseif($event_id == '1378')
    { 
      $header[] = "Address Line 1";
      $header[] = "Address Line 2";
      $header[] = "State";
      $header[] = "Zip";
      $header[] = "Badge Number";
    }
    elseif($event_id == '1512')
    {
        $header[] = 'Address Line 1,2';
        $header[] = 'City';
        $header[] = 'State';
        $header[] = 'Zip (Postal Code)';
        $header[] = "Badge Number";
    }
    $header[] = "Lead Rep";
    $header[] = "Lead Scan Date";
    if($event_id != '634')
    { 
      if($event_id != '1012'):
      foreach ($custom_column as $ckey => $cvalue) 
      {
        $data[ucfirst($cvalue['column_name'])]=$custom_column_data[$cvalue['column_name']];
      }
    endif;
    }
    foreach ($questions as $key => $value) {
      $header[]=$value['Question'];
    }
    fputcsv($fp, $header);
    //$my_lead=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($event_templates[0]['Id'],$exibitor_user_id);
    $my_lead=$this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user_test($event_templates[0]['Id'],$exibitor_user_id);

    foreach ($my_lead as $key => $value) 
    {
      $data['FirstName']=$value['Firstname'];
      $data['LastName']=$value['Lastname'];
      $data['Email']=$value['Email'];
      $data['Title']=$value['Title'];
      $data['Company Name']=$value['Company_name'];
      if($event_id == '634' || $event_id == '1012')
      {
        $data['Salutation']=$value['salutation'];
        $data['Country']=$value['country'];
        $data['Mobile']=$value['mobile'];
        $data['Badge Number']=$value['badgeNumber'];
      }
      elseif ($event_id == '1378')
        { 
            $data['Address Line 1'] = $value['a1'];
            $data['Address Line 2'] = $value['a2'];
            $data['State'] = $value['state'];
            $data['Zip'] = $value['zip'];
            $data['Badge Number']=$value['badgeNumber'];
        }
        elseif ($event_id == '1512')
        {
            $data['Address Line 1,2'] = $value['Street'];
            $data['City'] = $value['Suburb'];
            $data['State'] = $value['State'];
            $data['Zip (Postal Code)'] = $value['Postcode'];
            $data['Badge Number']=$value['badgeNumber'];
        }
      $data['Lead Rep']=$value['lead_rep'];
      $data['Lead Scan Date']=$value['created_date'];
      $custom_column_data=json_decode($value['custom_column_data'],true);
      if($event_id != '634')
      { 
        if($event_id != '1012'):
        foreach ($custom_column as $ckey => $cvalue) 
        {
          $data[ucfirst($cvalue['column_name'])]=$custom_column_data[$cvalue['column_name']];
        }
       endif;
      }

      $exibitor_ques = explode('~~',$value['exibitor_ques']);
      $exibitor_ans = explode('~~',$value['exibitor_ans']);
      foreach ($exibitor_ques as $keyqu => $valuequ)
      {
        $tmp_ans[$valuequ] = $exibitor_ans[$keyqu];
      }
      foreach ($questions as $qkey => $qvalue) 
      {
        // $data[]= !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment']; 
        $data[] = $tmp_ans[$qvalue['q_id']];
      }
      /*foreach ($questions as $qkey => $qvalue) 
      {
        $ans=$this->Lead_retrieval_model->get_user_question_answer($event_templates[0]['Id'],$value['Id'],$qvalue['q_id']);
        $data[]= !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment']; 
      }*/
      fputcsv($fp, $data);
      unset($data);
    }
    fclose($fp);
    $config['protocol']   = 'smtp';
    $config['smtp_host']  = 'localhost';
    $config['smtp_port']  = '25';
    $config['smtp_user']  = 'invite@allintheloop.com';
    $config['smtp_pass']  = 'xHi$&h9M)x9m';
    $config['_encoding']  = 'base64';
    $config['mailtype']   = 'html';
    $this->email->initialize($config);
    $this->email->from('invite@allintheloop.com','All In The Loop');
    $this->email->to($user[0]->Email);
    //$this->email->to('jagdish@xhtmljunkies.com');
    $this->email->subject("My Lead Csv File");
    $this->email->message("Please find below Attachment");
    $this->email->attach($filename);
    $this->email->send();
    $this->email->clear(TRUE);
    $this->session->set_flashdata('lead_success','My Lead Email Send SuccessFully...');
    $this->session->set_flashdata('survey_data', 'My Lead Email Send SuccessFully...');
    redirect($_SERVER['HTTP_REFERER']);
    //redirect('lead_retrieval/'.$acc_name.'/'.$Subdomain);
  }
  public function delete_my_representative($acc_name,$Subdomain,$resp_user_id)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->Lead_retrieval_model->remove_my_representative($event_templates[0]['Id'],$resp_user_id);
    $this->session->set_flashdata('lead_success','Representative Deleted SuccessFully...');
    redirect('Lead_retrieval/'.$acc_name.'/'.$Subdomain);
  }
}
