<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Agenda extends CI_Controller
{
    function __construct()
    {
        $this->data['pagetitle'] = 'Agenda';
        $this->data['smalltitle'] = 'Agenda';
        $this->data['breadcrumb'] = 'Agenda';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->library('formloader1');
        $this->load->model('Agenda_model');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Notes_admin_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Profile_model');
        $this->load->model('Message_model');
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->User_id;
        $eventname = $this->Event_model->get_all_event_name();
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('1', $module))
        {
            redirect(base_url() . 'Forbidden/');
            // echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
        }
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (in_array('1', $menu_list))
        {
            if (in_array($this->uri->segment(3) , $eventname))
            {
                if ($user != '')
                {
                    $parameters = $this->uri->uri_to_assoc(1);
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                    $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                    if ($cnt == 1)
                    {
                        $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                        $this->data['notes_list'] = $notes_list;
                    }
                    else
                    {
                        $event_type = $event_templates[0]['Event_type'];
                        if ($event_type == 3)
                        {
                            $this->session->unset_userdata('current_user');
                            $this->session->unset_userdata('invalid_cred');
                            $this->session->sess_destroy();
                        }
                        else
                        {
                            $parameters = $this->uri->uri_to_assoc(1);
                            $Subdomain = $this->uri->segment(3);
                            $acc_name = $this->uri->segment(2);
                            redirect(base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain);
                            // echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
                        }
                    }
                }
                else
                {
                    $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                    $Organisor_id = $event_templates[0]['Organisor_id'];
                    $acc_name = $this->Event_template_model->get_acc_name($Organisor_id);
                    // $this->data['acc_name']=$acc_name;
                }
            }
            else
            {
                $parameters = $this->uri->uri_to_assoc(1);
                $flag = 1;
                redirect(base_url() . 'Pageaccess/' . $parameters[$this->data['pagetitle']] . '/' . $flag);
            }
        }
        else
        {
            redirect(base_url() . 'Forbidden/');
        }
    }
    public function index($acc_name, $Subdomain = NULL, $intFormId = NULL)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $this->data['event_templates'] = $event_templates;
        $this->data['timezonetype'] = $this->Event_model->get_timezone_desc_and_type($event_templates[0]['Event_time_zone']);
        $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
        $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
        $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
        if ($event_templates[0]['allow_show_all_agenda'] != '1')
        {
            if (count($category_list) == 1)
            {
                $cid = array(
                    $category_list[0]['cid']
                );
            }
            else
            {
                if (!empty($agenda_category[0]['agenda_category_id']))
                {
                    $cid = array_column($agenda_category, 'agenda_category_id');
                }
                else
                {
                    $cid = array(
                        $primary_agenda[0]['Id']
                    );
                }
            }
        }
        else
        {
            $cid = array_column($category_list, 'cid');
        }
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $user_id = $user[0]->Id;
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $this->data['user_agenda_list'] = $user_agenda_list;
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Agenda' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $res1 = $this->Event_template_model->get_singup_forms($eid);
        $this->data['sign_form_data'] = $res1;
        if ($event_templates[0]['show_session_by_time'] == '1')
        {
            $agenda = $this->Event_template_model->get_agenda_list_by_time($Subdomain, null, $cid);
        }
        else
        {
            $agenda = $this->Event_template_model->get_agenda_list($Subdomain, null, $cid);
        }
        $this->data['agenda'] = $agenda;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $agenda_list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, null, $id, $cid);
        $this->data['agenda_list'] = $agenda_list;
        /*echo "<pre>";
        print_r($agenda_list);
        exit();*/
        $this->data['metting'] = $this->Agenda_model->check_metting_availability($event_templates[0]['Id']);
        $this->data['Subdomain'] = $Subdomain;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            redirect(base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain);
            // echo '<script>window.location.href="'.base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'"</script>';
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('1', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function view_category_session($acc_name, $Subdomain, $cid)
    {
        $cid = array(
            $cid
        );
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $this->data['event_templates'] = $event_templates;
        $this->data['timezonetype'] = $this->Event_model->get_timezone_desc_and_type($event_templates[0]['Event_time_zone']);
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $user_id = $user[0]->Id;
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $this->data['user_agenda_list'] = $user_agenda_list;
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Agenda' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $res1 = $this->Event_template_model->get_singup_forms($eid);
        $this->data['sign_form_data'] = $res1;
        if ($event_templates[0]['show_session_by_time'] == '1')
        {
            $agenda = $this->Event_template_model->get_agenda_list_by_time($Subdomain, null, $cid);
        }
        else
        {
            $agenda = $this->Event_template_model->get_agenda_list($Subdomain, null, $cid);
        }
        $this->data['agenda'] = $agenda;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $agenda_list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, null, $id, $cid);
        $this->data['agenda_list'] = $agenda_list;
        /*echo "<pre>";
        print_r($agenda_list);
        exit();*/
        $this->data['metting'] = $this->Agenda_model->check_metting_availability($event_templates[0]['Id']);
        $this->data['Subdomain'] = $Subdomain;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            redirect(base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain);
            // echo '<script>window.location.href="'.base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'"</script>';
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('1', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
                {
                    $this->template->write_view('content', 'Agenda/index_YouMatterDay', $this->data, true);
                }
                else
                {
                    $this->template->write_view('content', 'Agenda/index', $this->data, true);
                }
                $this->template->write_view('footer', 'Agenda/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function getpushnoti($Subdomain)
    {
        $this->load->model('Notifications_model');
        $Subdomain = $this->uri->segment(2);
        $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $notify_message = $this->Notifications_model->get_push_notification($dataevents[0]['Id'], $user[0]->User_id);
        if (empty($notify_message))
        {
            $notify_res['push_success'] = "0";
        }
        else
        {
            $notify_res['push_success'] = "1";
            $notify_res['push_notification'] = $notify_message;
        }
        $data1 = json_encode($notify_res);
        echo $data1;
        exit;
    }
    public function View($Subdomain = NULL, $id = NULL)
    {
        $Subdomain = $this->uri->segment(2);
        $id = $this->uri->segment(4);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $agenda = $this->Event_template_model->get_agenda_list($Subdomain);
        $this->data['agenda'] = $agenda;
        $this->data['Subdomain'] = $Subdomain;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, $id, NULL, NULL);
        if (!empty($list))
        {
            $agenda_list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, $id, null, 1);
            $this->data['agenda_list'] = $agenda_list;
        }
        $user = $this->session->userdata('current_user');
        if (empty($user))
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'Agenda/list', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
    }
    public function View_agenda($acc_name = NULL, $Subdomain = NULL, $id = NULL)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $this->data['timezonetype'] = $this->Event_model->get_timezone_desc_and_type($event_templates[0]['Event_time_zone']);
        $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
        $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
        $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
        if (count($category_list) == 1)
        {
            $cid = $category_list[0]['cid'];
        }
        else
        {
            if (!empty($agenda_category[0]['agenda_category_id']))
            {
                $cid = $agenda_category[0]['agenda_category_id'];
            }
            else
            {
                $cid = $primary_agenda[0]['Id'];
            }
        }
        date_default_timezone_set("UTC");
        $cdate = date('Y-m-d H:i:s');
        if (!empty($event_templates[0]['Event_show_time_zone']))
        {
            if (strpos($event_templates[0]['Event_show_time_zone'], "-") == true)
            {
                $arr = explode("-", $event_templates[0]['Event_show_time_zone']);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) - $intNew);
            }
            if (strpos($event_templates[0]['Event_show_time_zone'], "+") == true)
            {
                $arr = explode("+", $event_templates[0]['Event_show_time_zone']);
                $intoffset = $arr[1] * 3600;
                $intNew = abs($intoffset);
                $cdate = date('Y-m-d H:i:s', strtotime($cdate) + $intNew);
            }
        }
        $this->data['category_data'] = $this->Agenda_model->get_agenda_category_by_id($cid);
        $this->data['prv_rating'] = $this->Agenda_model->get_prev_session_rating($id, $cdate);
        $this->data['prev_agenda'] = $this->Agenda_model->get_prev_session($id, $cdate);
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $id);
        $this->data['agenda_value'] = $agenda_value;
        $user = $this->session->userdata('current_user');
        $agenda_comment = $this->Agenda_model->get_agenda_comment_by_agenda_id_and_user_id($event_templates[0]['Id'], $user[0]->Id, $id);
        $this->data['agenda_comment'] = $agenda_comment;
        $this->data['rating_data'] = $this->Agenda_model->get_save_rating($id);
        $totalsave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($id, null);
        $this->data['totalsave'] = $totalsave;
        $user_id = $user[0]->Id;
        if ($user[0]->Role_id == 4)
        {
            $custom = $this->Event_model->get_user_custom_clounm($user[0]->Id, $event_templates[0]['Id']);
            $this->data['custom'] = $custom;
        }
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $this->data['user_agenda_list'] = $user_agenda_list;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $user = $this->session->userdata('current_user');
        $strIp = $_SERVER['REMOTE_ADDR'];
        // $this->setCuTimezone($strIp);
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/view_new', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/view_new', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('1', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/view_new', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/view_new', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function setCuTimezone($ip = "")
    {
        /*$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        if($query && $query['status'] == 'success')
        {
        date_default_timezone_set($query['timezone']);
        }*/
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/php/' . $ip);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        print_r($output);
        exit;
    }
    public function view_msg($acc_name = NULL, $Subdomain = NULL, $id = NULL, $msgtype)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        if ($msgtype == '1')
        {
            $this->data['msg'] = "This session is fully booked â€“ Unfortunately you have not been booked onto this session";
        }
        else if ($msgtype == '2')
        {
            $this->data['msg'] = "This session clashes with another session you have saved â€“ Unfortunately you have not been booked onto this session";
        }
        else if ($msgtype == '3')
        {
            $this->data['msg'] = "You cannot check in for this session. It is either not the right time or you are not booked on this session";
        }
        $user = $this->session->userdata('current_user');
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/msg_agenda', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/msg_agenda', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('1', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/msg_agenda', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Agenda/msg_agenda', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function user_agenda_update($id = null)
    {
        $acc_name = $this->uri->segment(2);
        $Subdomain = $this->uri->segment(3);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $on_pending = $event_templates[0]['on_pending_agenda'];
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        if (empty($user_id))
        {
            echo "error###Something went wrong";
        }
        else
        {
            if ($this->input->post('process') == "SAVE")
            {
                $totalsave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($this->input->post('agenda_id') , $user_id);
                $cnt = $this->Agenda_model->exit_user_agenda($user_id);
                if (count($totalsave) < 1)
                {
                    $agenda_value = $this->Event_template_model->get_agenda_value_by_id($this->uri->segment(3) , $this->input->post('agenda_id'));
                    $overlapping = $this->Agenda_model->get_overlapping_agenda($agenda_value, $user_id, $on_pending);
                    if ($overlapping == false)
                    {
                        echo base_url() . 'Agenda/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/view_msg/' . $this->input->post('agenda_id') . '/2';
                        die;
                    }
                }
                if ($cnt > 0)
                {
                    $checkedval = explode(',', $this->input->post('checkbox_values'));
                    if ($on_pending == '1')
                    {
                        $data['pending_agenda_id'] = implode(',', $checkedval);
                    }
                    else
                    {
                        $data['agenda_id'] = implode(',', $checkedval);
                    }
                    $this->Agenda_model->update_users_agenda($user_id, $data);
                    // $checkedval= substr(implode(',', $checkedval),0);
                    echo $data['pending_agenda_id'];
                }
                else
                {
                    $checkedval = explode(',', $this->input->post('checkbox_values'));
                    $data['user_id'] = $user_id;
                    if ($on_pending == '1')
                    {
                        $data['pending_agenda_id'] = $checkedval[1];
                    }
                    else
                    {
                        $data['agenda_id'] = $checkedval[1];
                    }
                    $this->Agenda_model->add_users_agenda($data);
                    $checkedval = implode(',', $checkedval);
                    echo $checkedval;
                }
            }
            else
            {
                $checkedval = explode(',', $this->input->post('checkbox_values'));
                if ($on_pending == '1')
                {
                    $data['pending_agenda_id'] = implode(',', $checkedval);
                }
                else
                {
                    $data['agenda_id'] = implode(',', $checkedval);
                }
                $this->Agenda_model->update_users_agenda($user_id, $data);
                echo $data['pending_agenda_id'];
            }
        }
    }
    public function view_user_agenda($Subdomain, $acc_name)
    {
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        // if(!empty($user_id))
        // {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $agenda_id_array = explode(',', $user_agenda_list[0]['agenda_id']);
        $agenda_list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, $id, $agenda_id_array);
        $this->data['agenda_list'] = $agenda_list;
        if ($event_templates[0]['show_session_by_time'] == '1')
        {
            $agenda = $this->Event_template_model->get_agenda_list_by_time($Subdomain, $agenda_id_array);
        }
        else
        {
            $agenda = $this->Event_template_model->get_agenda_list($Subdomain, $agenda_id_array);
        }
        $this->data['agenda'] = $agenda;
        $this->data['metting'] = $this->Agenda_model->check_metting_availability($event_templates[0]['Id']);
        $this->data['metting_data_by_datetime'] = $this->Agenda_model->get_all_metting_in_attendee_id_by_datetime($event_templates[0]['Id']);
        $this->data['metting_data_by_type'] = $this->Agenda_model->get_all_metting_in_attendee_id_by_type($event_templates[0]['Id']);
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
        {
            $this->template->write_view('content', 'Agenda/YouMatterDay_user_agenda_view', $this->data, true);
        }
        else
        {
            $this->template->write_view('content', 'Agenda/user_agenda_view', $this->data, true);
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->render();
        // }
    }
    public function view_user_pending_agenda($Subdomain, $acc_name)
    {
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        // if(!empty($user_id))
        // {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $agenda_types = $this->Event_template_model->get_agenda_types($Subdomain);
        $this->data['agenda_types'] = $agenda_types;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $agenda_id_array = explode(',', $user_agenda_list[0]['pending_agenda_id']);
        $agenda_list = $this->Event_template_model->get_agenda_list_by_types($Subdomain, $id, $agenda_id_array);
        $this->data['agenda_list'] = $agenda_list;
        if ($event_templates[0]['show_session_by_time'] == '1')
        {
            $agenda = $this->Event_template_model->get_agenda_list_by_time($Subdomain, $agenda_id_array);
        }
        else
        {
            $agenda = $this->Event_template_model->get_agenda_list($Subdomain, $agenda_id_array);
        }
        $this->data['agenda'] = $agenda;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        if ($Subdomain == "ogilvy589dbab325232YouMatterDay2017")
        {
            $this->template->write_view('content', 'Agenda/YouMatterDay_user_pending_agenda_view', $this->data, true);
        }
        else
        {
            $this->template->write_view('content', 'Agenda/user_pending_agenda_view', $this->data, true);
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->render();
        // }
    }
    public function add_note($Subdomain = null)
    {
        $user = $this->session->userdata('current_user');
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        if ($user[0]->Role_name == 'Client')
        {
            $logged_in_user_id = $user[0]->Id;
        }
        else
        {
            $logged_in_user_id = $this->Event_model->get_org_id_by_event($event_templates[0]['Id']);
        }
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $this->data['Subdomain'] = $Subdomain;
        $acc_name = $this->session->userdata('acc_name');
        if ($this->input->post())
        {
            $data = array(
                'User_id' => $user[0]->Id,
                'Organisor_id' => $logged_in_user_id,
                'Event_id' => $event_templates[0]['Id'],
                'Heading' => $this->input->post('Heading') ,
                'Description' => $this->input->post('Description') ,
                'Created_at' => $this->input->post('Created_at')
            );
            $this->Event_template_model->add_notes($data);
            redirect("Notes/" . $acc_name . '/' . $Subdomain);
        }
        if (empty($user))
        {
            //                $this->load->view('registration/login',$this->data);
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->load->view('Notes/add', $this->data);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'Notes/js', $this->data, true);
            $this->template->render();
        }
    }
    public function user_check_in_update($acc_name, $Subdomain, $id)
    {
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $id);
        date_default_timezone_set("UTC");
        $cdate = date('Y-m-d H:i:s');
        if (!empty($agenda_value[0]['checking_datetime']))
        {
            if (!empty($event_templates[0]['Event_show_time_zone']))
            {
                if (strpos($event_templates[0]['Event_show_time_zone'], "-") == true)
                {
                    $arr = explode("-", $event_templates[0]['Event_show_time_zone']);
                    $intoffset = $arr[1] * 3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s', strtotime($cdate) - $intNew);
                }
                if (strpos($event_templates[0]['Event_show_time_zone'], "+") == true)
                {
                    $arr = explode("+", $event_templates[0]['Event_show_time_zone']);
                    $intoffset = $arr[1] * 3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s', strtotime($cdate) + $intNew);
                }
            }
        }
        else
        {
            $agenda_value[0]['checking_datetime'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - 3600);
        }
        if ($agenda_value[0]['checking_datetime'] <= $cdate)
        {
            $this->Agenda_model->user_agenda_check_in_process($id, $user_id);
        }
        else
        {
            echo base_url() . 'Agenda/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/view_msg/' . $id . '/3';
            die;
        }
        $user_agenda_list = $this->Agenda_model->get_agenda_list_user_wise($user_id);
        $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $id);
        $chekin = explode(",", $user_agenda_list[0]['check_in_agenda_id']);
        if (in_array($agenda_value[0]['Id'], $chekin))
        {
            $calss = "btn btn-success pull-center";
        }
        else
        {
            $calss = "btn btn-green pull-center";
        }
        echo $calss;
        die;
    }
    public function user_rating_save($acc_name, $event, $agenda_id)
    {
        // $agenda_value = $this->Event_template_model->get_agenda_value_by_id($event,$agenda_id);
        // $event_templates = $this->Event_template_model->get_event_template_by_id_list($event);
        // date_default_timezone_set("UTC");
        // $enddate=$agenda_value[0]['End_date'].' '.$agenda_value[0]['End_time'];
        // $cdate=date('Y-m-d H:i:s');
        // if(!empty($event_templates[0]['Event_show_time_zone']))
        // {
        //   if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
        //   {
        //     $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
        //     $intoffset=$arr[1]*3600;
        //     $intNew = abs($intoffset);
        //     $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
        //   }
        //   if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
        //   {
        //     $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
        //     $intoffset=$arr[1]*3600;
        //     $intNew = abs($intoffset);
        //     $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
        //   }
        // }
        // if($enddate<=$cdate)
        // {
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        $this->Agenda_model->save_user_rating($agenda_id, $user_id, $this->input->post('rating'));
        echo "success###";
        die;
        // }
        // else
        // {
        //   echo "error###".$agenda_value[0]['Heading']." can not be rated until it has ended";die;
        // }
    }
    public function remove_from_view_my_agenda($acc_name, $event, $agenda_id)
    {
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        $this->Agenda_model->remove_from_save_my_agenda($agenda_id, $user_id);
        echo "success";
        die;
    }
    public function save_all_pending_agenda($acc_name, $Subdomain)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
        $this->data['time_format'] = $time_format;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        $savemsg = $this->Agenda_model->book_all_pending_agenda($user_id, $time_format);
        $this->data['msg'] = $savemsg;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('content', 'Agenda/move_seesion_msg', $this->data, true);
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->render();
    }
    public function add_session_user_comment($acc_name, $Subdomain, $agenda_id)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $login_user = $this->session->userdata('current_user');
        $user_id = $login_user[0]->Id;
        if (!empty($login_user[0]->Id))
        {
            $commentsid = $this->Agenda_model->save_user_agenda_comment($event_templates[0]['Id'], $user_id, $agenda_id, $this->input->post('comments'));
            if (!empty($commentsid))
            {
                die("Success###Thank you for your feedback.");
            }
            else
            {
                die("error###Sothing Went wrong.");
            }
        }
        else
        {
            die("error###Please login first before comment on Session.");
        }
    }
    public function save_user_session($acc_name, $Subdomain, $aid)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $on_pending = $event_templates[0]['on_pending_agenda'];
        $user = $this->session->userdata('current_user');
        $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $aid);
        $classname = "btn btn-green";
        if (!empty($user[0]->Id))
        {
            $user_agenda = $this->Agenda_model->get_user_agenda($user[0]->Id);
            if ($on_pending == '1')
            {
                $pending_agenda = array_filter(explode(",", $user_agenda[0]['pending_agenda_id']));
                if (in_array($aid, $pending_agenda))
                {
                    unset($pending_agenda[array_search($aid, $pending_agenda) ]);
                    $classname = "btn btn-green";
                }
                else
                {
                    array_push($pending_agenda, $aid);
                    $classname = "btn btn-success";
                }
                $save_data['pending_agenda_id'] = implode(",", $pending_agenda);
                $this->Agenda_model->save_useragenda($save_data, $user[0]->Id);
                die("Success###" . $classname);
            }
            else
            {
                $pending_agenda = array_filter(explode(",", $user_agenda[0]['pending_agenda_id']));
                if (in_array($aid, $pending_agenda))
                {
                    unset($pending_agenda[array_search($aid, $pending_agenda) ]);
                    $save_data['pending_agenda_id'] = implode(",", $pending_agenda);
                }
                $totalsave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($aid, $user[0]->Id);
                if (count($totalsave) < 1)
                {
                    if ($agenda_value[0]['allow_clashing'] == '0')
                    {
                        $overlapping = $this->Agenda_model->get_overlapping_agenda($agenda_value, $user[0]->Id, $on_pending);
                        if ($overlapping == false)
                        {
                            die('error###' . base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_msg/' . $aid . '/2');
                        }
                    }
                    if (!empty($agenda_value[0]['Maximum_People']))
                    {
                        $totalagendasave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($aid, NULL);
                        $placesleft = $agenda_value[0]['Maximum_People'] - count($totalagendasave);
                        if ($placesleft <= 0)
                        {
                            die('error###' . base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_msg/' . $aid . '/1');
                        }
                    }
                }
                $agenda_id = array_filter(explode(",", $user_agenda[0]['agenda_id']));
                if (in_array($aid, $agenda_id))
                {
                    unset($agenda_id[array_search($aid, $agenda_id) ]);
                    $classname = "btn btn-green";
                }
                else
                {
                    array_push($agenda_id, $aid);
                    $classname = "btn btn-success";
                }
                $save_data['agenda_id'] = implode(",", $agenda_id);
                $this->Agenda_model->save_useragenda($save_data, $user[0]->Id);
            }
            $this->Agenda_model->save_sesssion_by_user($user[0]->Id, $aid);
            die("Success###" . $classname);
        }
        else
        {
            die("error###Something went wrong");
        }
    }
    public function direct_save_user_session($acc_name, $Subdomain, $aid)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $agenda_value = $this->Event_template_model->get_agenda_value_by_id($Subdomain, $aid, $event_templates[0]['default_lang']['lang_id']);
        $classname = "btn btn-green";
        if (!empty($user[0]->Id))
        {
            $user_agenda = $this->Agenda_model->get_user_agenda($user[0]->Id);
            $totalsave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($aid, $user[0]->Id);
            if (count($totalsave) < 1)
            {
                if ($agenda_value[0]['allow_clashing'] == '0')
                {
                    $overlapping = $this->Agenda_model->get_overlapping_agenda($agenda_value, $user[0]->Id, $on_pending);
                    if ($overlapping == false)
                    {
                        die('error###' . base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_msg/' . $aid . '/2');
                    }
                }
                if (!empty($agenda_value[0]['Maximum_People']))
                {
                    $totalagendasave = $this->Agenda_model->get_how_many_time_save_agenda_by_id($aid, NULL);
                    $placesleft = $agenda_value[0]['Maximum_People'] - count($totalagendasave);
                    if ($placesleft <= 0)
                    {
                        die('error###' . base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_msg/' . $aid . '/1');
                    }
                }
            }
            $agenda_id = array_filter(explode(",", $user_agenda[0]['agenda_id']));
            if (in_array($aid, $agenda_id))
            {
                unset($agenda_id[array_search($aid, $agenda_id) ]);
                $classname = "btn btn-green";
            }
            else
            {
                array_push($agenda_id, $aid);
                $classname = "btn btn-success";
            }
            $save_data['agenda_id'] = implode(",", $agenda_id);
            $this->Agenda_model->save_useragenda($save_data, $user[0]->Id);
            $this->Agenda_model->save_sesssion_by_user($user[0]->Id, $aid);
            die("Success###" . $classname);
        }
        else
        {
            die("error###Something went wrong");
        }
    }
}