<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Events extends CI_Controller
{

     function __construct()
     {

          $this->data['pagetitle'] = 'Events';
          $this->data['smalltitle'] = 'Events Details';
          $this->data['breadcrumb'] = 'Events Template';
          parent::__construct($this->data);
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Event_model');
          $this->load->model('Cms_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Profile_model');
          $this->load->model('User_model');
          $this->load->model('Login_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Message_model');
          $this->load->library('upload');
          $this->load->library('formloader1');
          $this->load->model('Agenda_model');
          $eventname=$this->Event_model->get_all_event_name();
          if(in_array($this->uri->segment(2),$eventname))
          {
            $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
            $this->data['notes_list'] = $notes_list;
            
            
            $user = $this->session->userdata('current_user');

            if ($user != '')
            {
                 
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($parameters[$this->data['pagetitle']]);
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);

                 if(!empty($roledata))
                 {
                      $roleid = $roledata[0]->Role_id;
                      $eventid = $event_templates[0]['Id'];
                      $rolename = $roledata[0]->Name;
                      $cnt = 0;
                      $req_mod = ucfirst($this->router->fetch_class());

                      //$cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename,$eventid);
                      $cnt = 1;
                 }
                 else
                 {
                      $cnt = 0;
                 }

                 if ($cnt == 1)
                 {
                      $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
                      $this->data['notes_list'] = $notes_list;
                 }
                 else
                 {
                      $parameters = $this->uri->uri_to_assoc(1);
                      redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
                 }
            }
          }
          else
          {
            $parameters = $this->uri->uri_to_assoc(1);
            redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
          }
     }

     public function index($Subdomain = NULL, $Email = NULL,$flag=NULL)
     {
         
          if (!$Subdomain)
          {

               redirect('Login');
          }
          else
          {  
              
               $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $this->data['event_templates'] = $event_templates;

              
               $event_id=$event_templates[0]['Id'];

               $fb_login_data = $this->Event_model->getraisedsetting($event_id);
               $this->data['fb_login_data'] = $fb_login_data;
              
             
               $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
               $this->data['menu'] = $menu;

               $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
               $this->data['notify_msg'] = $notifiy_msg;

               $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
               $this->data['notisetting']=$notificationsetting;


               $user = $this->session->userdata('current_user');
               $roleid = $user[0]->Role_id;

               $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
               $this->data['menu_list'] = $menu_list;
               
               $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
               $this->data['cms_feture_menu'] = $cms_feture_menu;

               $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
               $this->data['cms_menu'] = $cmsmenu;

               $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
               $this->data['speakers'] = $speakers;

               $notes_list = $this->Event_template_model->get_notes($Subdomain);
               $this->data['notes_list'] = $notes_list;

               $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
               $this->data['agendas'] = $agendas;

               $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
               $this->data['attendees'] = $attendees;

               $map = $this->Event_template_model->get_map($Subdomain);
               $this->data['map'] = $map;

               $this->data['Subdomain'] = $Subdomain;
               $this->data['Email'] = $Email;
               $this->data['speaker_flag']=$flag;

               $res=$this->Event_template_model->get_singup_forms($event_id);
      
               $this->data['form_data']=$res;

               $countrylist = $this->Profile_model->countrylist();
               $this->data['countrylist'] = $countrylist;
                
                $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                
               $user = $this->session->userdata('current_user');
               if ($event_templates[0]['Event_type'] == '1')
               {
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                      $this->template->write_view('content', 'events/index', $this->data, true);
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '2')
               {
  
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                      $this->template->write_view('content', 'events/index', $this->data, true);
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '3') 
               {
                  $this->template->write_view('content', 'events/index', $this->data, true);  
               }

               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->write_view('js', 'frontend_files/js', $this->data, true);
               $this->template->render();
          }
     }
     public function preview($acc_name,$Subdomain = NULL)
     {
         
          if (!$Subdomain)
          {

               redirect('Login');
          }
          else
          {
               $images_file="";
               if (!empty($_FILES['images']['name']))
               {

                        $imgname = explode('.', $_FILES['images']['name']);
                        $tempname = str_replace(" ","_",$imgname);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $images_file = $tempname_imagename . "." . $tempname[1];
                        $this->upload->initialize(array(
                                "file_name" => $images_file,
                                "upload_path" => "./assets/user_files",
                                "allowed_types" => 'gif|jpg|png|jpeg',
                                "max_size" => '100000',
                                "max_width" => '3000',
                                "max_height" => '3000'
                        ));

                        if (!$this->upload->do_multi_upload("images"))
                        {
                             $error = array('error' => $this->upload->display_errors());
                           
                        }
                       $images[] = $images_file;
                       $data['Images'] = json_encode($images);
                       $this->Event_template_model->update_banner_images($Subdomain,$data);
               }
              
               if (!empty($_FILES['logo_images']['name']))
               {

                        $imgname = explode('.', $_FILES['logo_images']['name']);
                        $tempname = str_replace(" ","_",$imgname);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $images_file = $tempname_imagename . "." . $tempname[1];
                        $this->upload->initialize(array(
                                "file_name" => $images_file,
                                "upload_path" => "./assets/user_files",
                                "allowed_types" => 'gif|jpg|png|jpeg',
                                "max_size" => '100000',
                                "max_width" => '3000',
                                "max_height" => '3000'
                        ));

                        if (!$this->upload->do_multi_upload("logo_images"))
                        {
                             $error = array('error' => $this->upload->display_errors());
                           
                        }
                       $images[] = $images_file;
                       $data['Logo_images'] = json_encode($images);
                       $this->Event_template_model->update_logo_images($Subdomain,$data);
               }

               $data['Background_color']=$this->input->post('Background_color');
               $data['Top_background_color']=$this->input->post('Top_background_color');
               $data['Top_text_color']=$this->input->post('Top_text_color');
               $data['Footer_background_color']=$this->input->post('Footer_background_color');
               $data['Description']=$this->input->post('Description');
               $data['Subdomain']=$Subdomain;
               $data['Start_date']=$this->input->post('Start_date');
               $data['End_date']=$this->input->post('End_date');
               $data['Event_name']=$this->input->post('Event_name');
               $data['Organisor_id']=$this->input->post('Organisor_id');
               $data['Id']=$this->input->post('Id');
               if(($_FILES['images']['name']=="" && $this->input->post('Images')=="") && $this->input->post('preimg')=="")
               {
                  $data['Images']="";
               }
               else
               {
                  if($this->input->post('Images')=="" && $_FILES['images']['name']=="")
                  {
                    $arr[]=$this->input->post('preimg');
                    $data['Images']=json_encode($arr);
                  }
               }
               $this->Event_template_model->update_temp_events($Subdomain,$data);
               $event_templates = $this->Event_template_model->get_event_template_by_id_list_preview($Subdomain,$data);
               $this->data['event_templates'] = $event_templates;
             
               $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
               $this->data['menu'] = $menu;

               $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
               $this->data['notify_msg'] = $notifiy_msg;

               $user = $this->session->userdata('current_user');
               $roleid = $user[0]->Role_id;

               $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
               $this->data['menu_list'] = $menu_list;
               
               $cms_feture_menu = $this->Event_model->geteventcmsmenu($event_templates[0]['Id'], null, 1);
               $this->data['cms_feture_menu'] = $cms_feture_menu;

               $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
               $this->data['cms_menu'] = $cmsmenu;

               $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
               $this->data['speakers'] = $speakers;

               $notes_list = $this->Event_template_model->get_notes($Subdomain);
               $this->data['notes_list'] = $notes_list;

               $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
               $this->data['agendas'] = $agendas;

               $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
               $this->data['attendees'] = $attendees;

               $map = $this->Event_template_model->get_map($Subdomain);
               $this->data['map'] = $map;

               $this->data['Subdomain'] = $Subdomain;
               $this->data['Email'] = $Email;
               $this->data['speaker_flag']=$flag;
              
               $user = $this->session->userdata('current_user');

                if (empty($user))
                {
                     $this->template->write_view('css', 'frontend_files/css', $this->data, true);
                     $this->template->write_view('header', 'frontend_files/header', $this->data, true);
                     $this->template->write_view('content', 'registration/index', $this->data, true);
                     $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
                     $this->template->write_view('js', 'frontend_files/js', $this->data, true);
                     $this->template->render();
                }
                else
                {
                     $html=$this->load->view('frontend_files/css', $this->data, true);
                     $html.=$this->load->view('frontend_files/header', $this->data, true);
                     //$html.=$this->load->view('frontend_files/sidebar', $this->data, true);
                     $html.=$this->load->view('events/index', $this->data, true);
                     $html.=$this->load->view('frontend_files/js', $this->data, true);             
                     echo $html;
                }
          }
           
          
        
     }


}
