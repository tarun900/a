<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Thankyou extends FrontendController {
	function __construct() {
        $this->data['pagetitle'] = 'Thank you';
		$this->data['smalltitle'] = 'Thank you for your business';
		$this->data['breadcrumb'] = 'Thank you';
		parent::__construct($this->data);
	}
	public function index()
	{
        $this->template->write_view('content', 'thankyou/index', $this->data , true);
		$this->template->render();
	}	
}
