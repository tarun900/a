<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/note_model');
        $this->load->model('native/event_template_model');
        $this->load->model('native/attendee_model');
        $this->events = $this->event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images'] = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        if(!empty($this->input->post()))
        {
            $this->cmsmenu = $this->cms_model->get_cms_page_new($this->input->post('event_id'),NULL,NULL,$this->input->post('user_id'),$this->input->post('lang_id'));
            $this->cmsmenu1 = $this->cms_model->get_cms_page_new1($this->input->post('event_id'));
        }
        
        foreach ($this->cmsmenu as $key => $values) 
        {
            if(!empty($values['Images']))
            {
                $cmsbannerimage_decode = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Images'] = "";
            }
            if(!empty($values['Logo_images']))
            {
                $cmslogoimage_decode = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->menu_list1 = $this->event_model->geteventmenu_list1($this->input->post('event_id'), null, null,$this->input->post('user_id'));
    }

    public function index()
    {

        $user = $this->app_login_model->check_token_with_event($this->input->post('_token'),$this->input->post('event_id'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $fetureproduct = $this->event_model->getFetureProduct($this->input->post('event_id'));

            $data = array(
                'events' => $this->events,
                'menu_list' => $this->menu_list,
                'cmsmenu' => $this->cmsmenu
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function event_id()
    {
        //$start_fun_time = microtime(true);
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
                $user = $this->app_login_model->getUserDetailsId($user_id);
                /*if($user['Id'] =! '' && $user['Rid'] == '4')
                {
                    $active_module1 = $this->app_login_model->check_custom_view($event_id,$user_id);
                    $this->cmsmenu = (in_array('21',$active_module1)) ? $this->cmsmenu : [];
                }*/


                if(!empty($event_list))
                {
                    $menus = explode(',', $event_list[0]['checkbox_values']);
                   
                    $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";
                   
                    if($event_list[0]['fun_background_color']==0)
                    {
                         $event_list[0]['fun_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_text_color']==0)
                    {
                         $event_list[0]['fun_top_text_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                    }
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_block_background_color']==0)
                    {
                         $event_list[0]['fun_block_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_background_color']==0)
                    {
                         $event_list[0]['fun_top_background_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                    }
                    if($event_list[0]['theme_color']==0)
                    {
                         $event_list[0]['theme_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                    }
                    if($event_list[0]['fun_block_text_color']==0)
                    {
                         $event_list[0]['fun_block_text_color']="#FFFFFF";
                    }
                    
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    $img=json_decode($event_list[0]['Images']);
                    $Logo_images=json_decode($event_list[0]['Logo_images']);
                    $Background_img=json_decode($event_list[0]['Background_img']);
                    /*if(empty($img[0]))
                    {
                        $img[0]="";
                    }*/
                    if(empty($Logo_images[0]))
                    {
                        $Logo_images[0]="";
                    }
                    if(empty($Background_img[0]))
                    {
                        $Background_img[0]="";
                    }
                    
                    $event_list[0]['Images']=$img[0];
                    // need to change
                    if($event_id == "259")
                    	$event_list[0]['banners']=["1482317026_event_crop_banner.png","new_space1490201207.jpg","smi_banner1489420689.jpg"];
                	else
                		$event_list[0]['banners'] = ($img) ? $img : [];
                    $event_list[0]['Logo_images1']=$Logo_images[0];
                    $event_list[0]['Background_img1']=$Background_img[0];
                    $event_list[0]['description1']=$event_list[0]['Description'];
                    $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];
                    $images[0] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BEVERAGES.JPG" ;
                    $images[1] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BURGER.JPG" ;
                    $images[2] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-DAIRY.JPG" ;
                    $images[3] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height--FAT&OIL.JPG" ;
                    $images[4] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-PULSES.JPG" ;
                    $checkbox_values = $event_list[0]['checkbox_values'];
                    $menu_array = explode(',', $checkbox_values);
                   
                    $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                    $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                    if($event_id == '447')
                    $event_list[0]['banner_images'] = $images;

                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                        if(count($note_data)>0)
                        {
                             $note_status=1;
                        }
                        else
                        {
                             $note_status=0;
                        }

                    }
                    if($event_id == '447')
                    {
                        $event_list[0]['Description'] = '';
                        $event_list[0]['description1'] = '';

                        $gulf_images[0]['image'] = "http://www.allintheloop.net/assets/images/gulfood/exhibutor-search.jpg";
                        $gulf_images[0]['id'] = "3";

                        $gulf_images[1]['image'] = "http://www.allintheloop.net/assets/images/gulfood/locate.jpg";
                        $gulf_images[1]['id'] = "253";

                        $gulf_images[2]['image'] = "http://www.allintheloop.net/assets/images/gulfood/my-meeting.jpg";
                        $gulf_images[2]['id'] = "249";

                        $gulf_images[3]['image'] = "http://www.allintheloop.net/assets/images/gulfood/virtual-supermarket.png";
                        $gulf_images[3]['id'] = "1500";

                        $gulf_images[4]['image'] = "http://www.allintheloop.net/assets/images/gulfood/salon-img.png";
                        $gulf_images[4]['id'] = "293";

                        $gulf_images[5]['image'] = "http://www.allintheloop.net/assets/images/gulfood/gulfood-sponsors.jpg";
                        $gulf_images[5]['id'] = "43";

                        $gulf_images[6]['image'] = "http://www.allintheloop.net/assets/images/gulfood/social-info.jpg";
                        $gulf_images[6]['id'] = "17";

                        $gulf_images[7]['image'] = "http://www.allintheloop.net/assets/images/gulfood/usefil-info.png";
                        $gulf_images[7]['id'] = "2500";

                        $gulf_images[8]['image'] = "http://www.allintheloop.net/assets/images/gulfood/book2.png";
                        $gulf_images[8]['id'] = "294";
                              
                       
                       $data1 = array(
                        'events' => $event_list,
                        'menu_list' => $this->menu_list,
                        'cmsmenu' => $this->cmsmenu,
                        'note_status' => $note_status,
                        'home_page_images' => $gulf_images,
                        'home_page_content' => "http://www.allintheloop.net/assets/images/gulfood/welcome-to-gulfood.png",
                        );
                    }
                    else
                    {
                        if($event_id == '455')
                        {
                            foreach ($this->menu_list as $key => $value) {
                                if($value['is_feture_products'] == '1')
                                {
                                    $value['is_cms'] = '0';
                                    $value['my_id'] = $value['id'];
                                    $menu[] = $value;
                                }
                            }
                            foreach ($this->cmsmenu as $key => $value) {
                                if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                                {
                                    $value['my_id'] = $value['Id'];
                                    $value['is_cms'] = '1';
                                    $menu[] = $value;
                                }
                            }
                            
                            $sort_key_val=array("","379","10","271","11","7","12","16","2","15");
                            foreach ($menu as $key => $value) {
                                 $kkey=array_search($value['my_id'], $sort_key_val);
                                
                                 if($kkey!='')
                                    $array[$kkey] = $value;
                                 else
                                    $array[$key] = $value;
                            }
                            for ($i = 0 ;$i<=(count($array)); $i++) {
                                if($array[$i]!=null)
                                    $array1[] = $array[$i];
                               }
                            foreach ($array1 as $key => $value) 
                            {
                              $array1[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                              $array1[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                              $array1[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                              $array1[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                             
                            }
                        }
                        
                        $active_module = array_column($this->menu_list, 'id');
                        $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;

                        $banner_images_coords = $this->app_login_model->banner_images_and_coords($event_id);

                        foreach ($banner_images_coords['images'] as $key => $value) 
                        {   
                            if(!empty($value['Image']))
                            {
                                $source_url = base_url()."assets/user_files/".$value['Image'];
                                $info = getimagesize($source_url);
                                $new_name = "new_".$value['Image'];
                                $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                                if ($info['mime'] == 'image/jpeg')
                                {   
                                    $quality = 50;
                                    $image = imagecreatefromjpeg($source_url);
                                    imagejpeg($image, $destination_url, $quality);
                                }
                                elseif ($info['mime'] == 'image/gif')
                                {   
                                    $quality = 5;
                                    $image = imagecreatefromgif($source_url);
                                    imagegif($image, $destination_url, $quality);

                                }
                                elseif ($info['mime'] == 'image/png')
                                {   
                                    $quality = 5;
                                    $image = imagecreatefrompng($source_url);

                                    $background = imagecolorallocatealpha($image,255,0,255,127);
                                    imagecolortransparent($image, $background);
                                    imagealphablending($image, false);
                                    imagesavealpha($image, true);
                                    imagepng($image, $destination_url, $quality);
                                }
                                $banner_images_coords['images'][$key]['Image'] = $new_name;
                            }
                        }

                        $data1 = array(
                            'events' => $event_list,
                            'banner_images' => $banner_images_coords,
                            'menu_list' => $this->menu_list,
                            /*'cmsmenu' => (in_array('21',$active_module1)) ? $this->cmsmenu : [],*/
                            'cmsmenu' => $this->cmsmenu,
                            'ersite_home_page_menu' =>($event_id == '455') ?  $array1 : [],
                            'note_status' => $note_status
                        );
                    }

                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        /*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/app/event_id function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
        echo json_encode($data);
    } 


    public function event_id1()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');




        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list = $this->app_login_model->check_event_with_id1($event_id,$user_id);
                if(!empty($event_list))
                {
                    $menus = explode(',', $event_list[0]['checkbox_values']);
                   
                    $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";
                   
                    if($event_list[0]['fun_background_color']==0)
                    {
                         $event_list[0]['fun_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_text_color']==0)
                    {
                         $event_list[0]['fun_top_text_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                    }
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_block_background_color']==0)
                    {
                         $event_list[0]['fun_block_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_background_color']==0)
                    {
                         $event_list[0]['fun_top_background_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                    }
                    if($event_list[0]['theme_color']==0)
                    {
                         $event_list[0]['theme_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                    }
                    if($event_list[0]['fun_block_text_color']==0)
                    {
                         $event_list[0]['fun_block_text_color']="#FFFFFF";
                    }
                    
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    $img=json_decode($event_list[0]['Images']);
                    $Logo_images=json_decode($event_list[0]['Logo_images']);
                    $Background_img=json_decode($event_list[0]['Background_img']);
                    /*if(empty($img[0]))
                    {
                        $img[0]="";
                    }*/
                    if(empty($Logo_images[0]))
                    {
                        $Logo_images[0]="";
                    }
                    if(empty($Background_img[0]))
                    {
                        $Background_img[0]="";
                    }
                    
                    $event_list[0]['Images']=$img[0];
                    // need to change
                    if($event_id == "259")
                        $event_list[0]['banners']=["1482317026_event_crop_banner.png","new_space1490201207.jpg","smi_banner1489420689.jpg"];
                    else
                        $event_list[0]['banners'] = ($img) ? $img : [];
                    $event_list[0]['Logo_images1']=$Logo_images[0];
                    $event_list[0]['Background_img1']=$Background_img[0];
                    $event_list[0]['description1']=$event_list[0]['Description'];
                    $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];
                    $images[0] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BEVERAGES.JPG" ;
                    $images[1] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BURGER.JPG" ;
                    $images[2] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-DAIRY.JPG" ;
                    $images[3] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height--FAT&OIL.JPG" ;
                    $images[4] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-PULSES.JPG" ;
                    $checkbox_values = $event_list[0]['checkbox_values'];
                    $menu_array = explode(',', $checkbox_values);
                   
                    $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                    $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                    if($event_id == '447')
                    $event_list[0]['banner_images'] = $images;

                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                        if(count($note_data)>0)
                        {
                             $note_status=1;
                        }
                        else
                        {
                             $note_status=0;
                        }

                    }
                    if($event_id == '447')
                    {
                        $event_list[0]['Description'] = '';
                        $event_list[0]['description1'] = '';

                        $gulf_images[0]['image'] = "http://www.allintheloop.net/assets/images/gulfood/exhibutor-search.jpg";
                        $gulf_images[0]['id'] = "3";

                        $gulf_images[1]['image'] = "http://www.allintheloop.net/assets/images/gulfood/locate.jpg";
                        $gulf_images[1]['id'] = "253";

                        $gulf_images[2]['image'] = "http://www.allintheloop.net/assets/images/gulfood/my-meeting.jpg";
                        $gulf_images[2]['id'] = "249";

                        $gulf_images[3]['image'] = "http://www.allintheloop.net/assets/images/gulfood/virtual-supermarket.png";
                        $gulf_images[3]['id'] = "1500";

                        $gulf_images[4]['image'] = "http://www.allintheloop.net/assets/images/gulfood/salon-img.png";
                        $gulf_images[4]['id'] = "293";

                        $gulf_images[5]['image'] = "http://www.allintheloop.net/assets/images/gulfood/gulfood-sponsors.jpg";
                        $gulf_images[5]['id'] = "43";

                        $gulf_images[6]['image'] = "http://www.allintheloop.net/assets/images/gulfood/social-info.jpg";
                        $gulf_images[6]['id'] = "17";

                        $gulf_images[7]['image'] = "http://www.allintheloop.net/assets/images/gulfood/usefil-info.png";
                        $gulf_images[7]['id'] = "2500";

                        $gulf_images[8]['image'] = "http://www.allintheloop.net/assets/images/gulfood/book2.png";
                        $gulf_images[8]['id'] = "294";
                              
                       
                       $data1 = array(
                        'events' => $event_list,
                        'menu_list' => $this->menu_list1,
                        'cmsmenu' => $this->cmsmenu1,
                        'note_status' => $note_status,
                        'home_page_images' => $gulf_images,
                        'home_page_content' => "http://www.allintheloop.net/assets/images/gulfood/welcome-to-gulfood.png",
                        );
                    }
                    else
                    {
                        if($event_id == '455')
                        {
                            foreach ($this->menu_list1 as $key => $value) {
                                if($value['is_feture_products'] == '1')
                                {
                                    $value['is_cms'] = '0';
                                    $value['my_id'] = $value['id'];
                                    $menu[] = $value;
                                }
                            }
                            foreach ($this->cmsmenu1 as $key => $value) {
                                if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                                {
                                    $value['my_id'] = $value['Id'];
                                    $value['is_cms'] = '1';
                                    $menu[] = $value;
                                }
                            }
                            
                            $sort_key_val=array("","379","10","271","11","7","12","16","2","15");
                            foreach ($menu as $key => $value) {
                                 $kkey=array_search($value['my_id'], $sort_key_val);
                                
                                 if($kkey!='')
                                    $array[$kkey] = $value;
                                 else
                                    $array[$key] = $value;
                            }
                            for ($i = 0 ;$i<=(count($array)); $i++) {
                                if($array[$i]!=null)
                                    $array1[] = $array[$i];
                               }
                            foreach ($array1 as $key => $value) 
                            {
                              $array1[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                              $array1[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                              $array1[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                              $array1[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                             
                            }
                        }
                        $data1 = array(
                            'events' => $event_list,
                            'menu_list' => $this->menu_list1,
                            'cmsmenu' => $this->cmsmenu1,
                            'ersite_home_page_menu' =>($event_id == '455') ?  $array1 : [],
                            'note_status' => $note_status
                        );
                    }
                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function event_id_advance_design()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
                $user = $this->app_login_model->getUserDetailsId($user_id);
                
                if(!empty($event_list))
                {
                    $menus = explode(',', $event_list[0]['checkbox_values']);
                    $tmp = preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_list[0]['Description'])));
                    $event_list[0]['Description'] = empty($tmp) ?  "" : $event_list[0]['Description'];
                    
                    $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";
                   
                    if($event_list[0]['fun_background_color']==0)
                    {
                         $event_list[0]['fun_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_text_color']==0)
                    {
                         $event_list[0]['fun_top_text_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                    }
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_block_background_color']==0)
                    {
                         $event_list[0]['fun_block_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_background_color']==0)
                    {
                         $event_list[0]['fun_top_background_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                    }
                    if($event_list[0]['theme_color']==0)
                    {
                         $event_list[0]['theme_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                    }
                    if($event_list[0]['fun_block_text_color']==0)
                    {
                         $event_list[0]['fun_block_text_color']="#FFFFFF";
                    }
                    
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    $img=json_decode($event_list[0]['Images']);
                    $Logo_images=json_decode($event_list[0]['Logo_images']);
                    $Background_img=json_decode($event_list[0]['Background_img']);
                    
                    if(empty($Logo_images[0]))
                    {
                        $Logo_images[0]="";
                    }
                    if(empty($Background_img[0]))
                    {
                        $Background_img[0]="";
                    }
                    
                    $event_list[0]['Images']=$img[0];
                    $event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                    $event_list[0]['Logo_images1']=$Logo_images[0];
                    $event_list[0]['Background_img1']=$Background_img[0];
                    $event_list[0]['description1']=$event_list[0]['Description'];
                    $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];
                    $images[0] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BEVERAGES.JPG" ;
                    $images[1] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-BURGER.JPG" ;
                    $images[2] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-DAIRY.JPG" ;
                    $images[3] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height--FAT&OIL.JPG" ;
                    $images[4] = "http://www.allintheloop.net/assets/images/GULFOOD--MOBILE-APP-BANNER-is-1500px-Width-and-800px-height-PULSES.JPG" ;
                    $checkbox_values = $event_list[0]['checkbox_values'];
                    $menu_array = explode(',', $checkbox_values);
                   
                    $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                    $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                    if($event_id == '447')
                    $event_list[0]['banner_images'] = $images;

                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                        if(count($note_data)>0)
                        {
                             $note_status=1;
                        }
                        else
                        {
                             $note_status=0;
                        }

                    }
                    if($event_id == '447')
                    {
                        $event_list[0]['Description'] = '';
                        $event_list[0]['description1'] = '';

                        $gulf_images[0]['image'] = "http://www.allintheloop.net/assets/images/gulfood/exhibutor-search.jpg";
                        $gulf_images[0]['id'] = "3";

                        $gulf_images[1]['image'] = "http://www.allintheloop.net/assets/images/gulfood/locate.jpg";
                        $gulf_images[1]['id'] = "253";

                        $gulf_images[2]['image'] = "http://www.allintheloop.net/assets/images/gulfood/my-meeting.jpg";
                        $gulf_images[2]['id'] = "249";

                        $gulf_images[3]['image'] = "http://www.allintheloop.net/assets/images/gulfood/virtual-supermarket.png";
                        $gulf_images[3]['id'] = "1500";

                        $gulf_images[4]['image'] = "http://www.allintheloop.net/assets/images/gulfood/salon-img.png";
                        $gulf_images[4]['id'] = "293";

                        $gulf_images[5]['image'] = "http://www.allintheloop.net/assets/images/gulfood/gulfood-sponsors.jpg";
                        $gulf_images[5]['id'] = "43";

                        $gulf_images[6]['image'] = "http://www.allintheloop.net/assets/images/gulfood/social-info.jpg";
                        $gulf_images[6]['id'] = "17";

                        $gulf_images[7]['image'] = "http://www.allintheloop.net/assets/images/gulfood/usefil-info.png";
                        $gulf_images[7]['id'] = "2500";

                        $gulf_images[8]['image'] = "http://www.allintheloop.net/assets/images/gulfood/book2.png";
                        $gulf_images[8]['id'] = "294";
                              
                       
                       $data1 = array(
                        'events' => $event_list,
                        'menu_list' => $this->menu_list,
                        'cmsmenu' => $this->cmsmenu,
                        'note_status' => $note_status,
                        'home_page_images' => $gulf_images,
                        'home_page_content' => "http://www.allintheloop.net/assets/images/gulfood/welcome-to-gulfood.png",
                        );
                    }
                    else
                    {
                        if($event_id == '455')
                        {
                            foreach ($this->menu_list as $key => $value) {
                                if($value['is_feture_products'] == '1')
                                {
                                    $value['is_cms'] = '0';
                                    $value['my_id'] = $value['id'];
                                    $menu[] = $value;
                                }
                            }
                            foreach ($this->cmsmenu as $key => $value) {
                                if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                                {
                                    $value['my_id'] = $value['Id'];
                                    $value['is_cms'] = '1';
                                    $menu[] = $value;
                                }
                            }
                            
                            $sort_key_val=array("","379","10","271","11","7","12","16","2","15");
                            foreach ($menu as $key => $value) {
                                 $kkey=array_search($value['my_id'], $sort_key_val);
                                
                                 if($kkey!='')
                                    $array[$kkey] = $value;
                                 else
                                    $array[$key] = $value;
                            }
                            for ($i = 0 ;$i<=(count($array)); $i++) {
                                if($array[$i]!=null)
                                    $array1[] = $array[$i];
                               }
                            foreach ($array1 as $key => $value) 
                            {
                              $array1[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                              $array1[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                              $array1[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                              $array1[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                             
                            }
                        }
                        
                        foreach ($this->menu_list as $key => $value)
                        {
                            if($value['is_feture_products'] == '1')
                            {
                                $value['is_cms'] = '0';
                                $value['my_id'] = $value['id'];
                                $menu[] = $value;
                            }
                            if($value['id'] == '1' && !empty($value['category_list']) && $value['is_feture_products'] == '1')
                            {
                                foreach ($value['category_list'] as $k => $v)
                                {   
                                    $v['is_cms'] = '0';
                                    $menu[] = $v;
                                }
                            }
                        }
                        foreach ($this->cmsmenu as $key => $value)
                        {
                            if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                            {
                                $value['my_id'] = $value['Id'];
                                $value['is_cms'] = '1';
                                $value['category_id'] = "";
                                $menu[] = $value;

                            }
                        }
                      

                        $sort_key_val=array("","379","10","271","11","7","12","16","2","15");
                        
                        /*foreach ($menu as $key => $value)
                        {
                             $kkey=array_search($value['my_id'], $sort_key_val);
                            
                             if($kkey!='')
                                $array[$kkey] = $value;
                             else
                                $array[$key] = $value;
                        }
                        for ($i = 0 ;$i<=(count($array)); $i++)
                        {
                            if($array[$i]!=null)
                                $array1[] = $array[$i];
                        }*/
                        foreach ($menu as $key => $value) 
                        {
                          $menu[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                          $menu[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                          $menu[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                          $menu[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                          if($value['is_cms'] == '1')
                            $menu[$key]['is_force_login'] = 0;



                        }
                        function sortByOrder($a, $b)
                        {
                        return $a['sort_order'] - $b['sort_order'];
                        }

                        usort($menu, 'sortByOrder');
                       
                        
                        $active_module = array_column($this->test, 'id');
                        $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;

                        $banner_images_coords = $this->app_login_model->banner_images_and_coords($event_id,$user_id);

                        foreach ($banner_images_coords['images'] as $key => $value) 
                        {   
                            if(!empty($value['Image']))
                            {
                                $source_url = base_url()."assets/user_files/".$value['Image'];
                                $info = getimagesize($source_url);
                                $new_name = "new_".$value['Image'];
                                $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                                if ($info['mime'] == 'image/jpeg')
                                {   
                                    $quality = 50;
                                    $image = imagecreatefromjpeg($source_url);
                                    imagejpeg($image, $destination_url, $quality);
                                }
                                elseif ($info['mime'] == 'image/gif')
                                {   
                                    $new_name = $value['Image'];
                                    /*$quality = 5;
                                    $image = imagecreatefromgif($source_url);
                                    imagegif($image, $destination_url, $quality);
                                    */
                                }
                                elseif ($info['mime'] == 'image/png')
                                {   
                                    $quality = 5;
                                    $image = imagecreatefrompng($source_url);

                                    $background = imagecolorallocatealpha($image,255,0,255,127);
                                    imagecolortransparent($image, $background);
                                    imagealphablending($image, false);
                                    imagesavealpha($image, true);
                                    imagepng($image, $destination_url, $quality);
                                }
                                $banner_images_coords['images'][$key]['Image'] = $new_name;
                            }
                        }
                        $lang_list = $this->app_login_model->get_event_lang_list($event_id);
                        $event_list[0]['photo_filter_enabled'] = (in_array('54',array_column($this->menu_list,'id'))) ? '1' : '0';
                       /* $event_list[0]['photo_filter_image'] = $this->compress_image($event_list[0]['photo_filter_image']);*/
                        $data1 = array(
                            'events' => $event_list,
                            'banner_images' => $banner_images_coords,
                            'menu_list' => $this->menu_list,
                            /*'cmsmenu' => (in_array('21',$active_module1)) ? $this->cmsmenu : [],*/
                            'cmsmenu' => $this->cmsmenu,
                            'ersite_home_page_menu' =>($event_id == '455') ?  $array1 : [],
                            'aitl_home_page_menu' => ($menu != '') ?  $menu : [],
                            'note_status' => $note_status,
                            'lang_list' => $lang_list
                        );
                    }

                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    } 
    public function getSystemMemInfo() 
    {       
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach ($data as $line)
        {   
            list($key, $val) = array_pad(explode(":", $line, 2), 2, null);
            $meminfo[$key] = trim($val);
        }
        return $meminfo;
    }
    public function get_geo_json()
    {
        //$data = file_get_contents('http://www.allintheloop.net/assets/geojson/wgs84text.geojson');
        $data = file_get_contents('http://www.allintheloop.net/assets/geojson/wgs84poly1.geojson');
        echo $data;
        //echo json_encode($data);exit;
    }
    public function get_server_load_time_50()
    {   
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        //$this->output->enable_profiler(TRUE);
        $starttime = $this->app_login_model->get_endtime_of_last_rec_50();
        $diff = microtime(true) - $starttime;

        $sec = intval($diff);
        $micro = $diff - $sec;

        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));

        $mem = $this->getSystemMemInfo();
        /*$time = microtime(true);
        $finish = $time;*/
        $i = 0;
        while ($i<=100)
        {
          echo "hello<br>";
          $i++;
        }
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;


        $total_time = round(($finish - $start), 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->app_login_model->add_server_data_50($insert);
        
    }
    public function get_server_load_time_100()
    {   
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        //$this->output->enable_profiler(TRUE);
        $starttime = $this->app_login_model->get_endtime_of_last_rec_100();
        $diff = microtime(true) - $starttime;

        $sec = intval($diff);
        $micro = $diff - $sec;

        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));

        $mem = $this->getSystemMemInfo();
        /*$time = microtime(true);
        $finish = $time;*/
        $i = 0;
        while ($i<=100)
        {
          echo "hello<br>";
          $i++;
        }
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;


        $total_time = round(($finish - $start), 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->app_login_model->add_server_data_100($insert);
        
    }
    public function get_server_load_time_200()
    {   
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        //$this->output->enable_profiler(TRUE);
        $starttime = $this->app_login_model->get_endtime_of_last_rec_200();
        $diff = microtime(true) - $starttime;

        $sec = intval($diff);
        $micro = $diff - $sec;

        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));

        $mem = $this->getSystemMemInfo();
        /*$time = microtime(true);
        $finish = $time;*/
        $i = 0;
        while ($i<=100)
        {
          echo "hello<br>";
          $i++;
        }
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;


        $total_time = round(($finish - $start), 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->app_login_model->add_server_data_200($insert);
        
    }
    public function get_server_load_time_500()
    {   
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        //$this->output->enable_profiler(TRUE);
        $starttime = $this->app_login_model->get_endtime_of_last_rec_500();
        $diff = microtime(true) - $starttime;

        $sec = intval($diff);
        $micro = $diff - $sec;

        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));

        $mem = $this->getSystemMemInfo();
        /*$time = microtime(true);
        $finish = $time;*/
        $i = 0;
        while ($i<=100)
        {
          echo "hello<br>";
          $i++;
        }
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;


        $total_time = round(($finish - $start), 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->app_login_model->add_server_data_500($insert);
        
    }
    public function get_exi_info()
    {	
        $file = './assets/geojson/cityspace/standNumber.geojson';
        $data = file_get_contents($file);
        $data = json_decode($data,true);

        foreach ($data['features'] as $key => $value)
        {   
            $tmp = $this->app_login_model->get_exhi_info($value['properties']['Text']);
            $data['features'][$key]['properties']['page_id'] = (!empty($tmp['Id'])) ? $tmp['Id'] : '';
            $data['features'][$key]['properties']['exi_id'] = (!empty($tmp['user_id'])) ? $tmp['user_id'] : '';
            $img = (!empty($tmp['company_logo']) && $tmp['company_logo'] != '[null]') ? json_decode($tmp['company_logo']) : [];
            $data['features'][$key]['properties']['company_logo'] = (!empty($img[0])) ? $img[0] : '';
            $data['features'][$key]['properties']['comapany_name'] = (!empty($tmp['Heading'])) ? $tmp['Heading'] : '';
            $data['features'][$key]['properties']['exhi_desc'] = (!empty($tmp['Description'])) ? substr($tmp['Description'],0,50) : '';
        }
        $data = json_encode($data);
        $result=file_put_contents($file,$data);

        /*if($result === false) {
            echo "false";
        } else {
            echo "true";
        }*/
        echo $data;exit;
    }
    public function get_default_lang()
    {
        $event_id=$this->input->post('event_id');
        $lang_id=$this->input->post('lang_id');
        if(!empty($event_id))
        {
            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
            $data = array(
                'success' => true,
                'default_lang' => $default_lang
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function compress_image($data)
    {   
        if(is_array($data))
        {
            foreach ($data as $key => $value) 
            {   
                if(!empty($value))
                {
                    $source_url = base_url()."assets/user_files/".$value;
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value;
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                    if ($info['mime'] == 'image/jpeg')
                    {   
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {   
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);

                    }
                    elseif ($info['mime'] == 'image/png')
                    {   
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);

                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $new_data[] = $new_name;
                }
            }
            return $new_data;
        }
        elseif(!empty($data))
        {
            $source_url = base_url()."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 50;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);

                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
            return $new_name;
        }
    }
    public function get_event($event_id,$user_id)
    {   
        if($event_id!='')
        {
            $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
            $user = $this->app_login_model->getUserDetailsId($user_id);

            if(!empty($event_list))
            {
                $menus = explode(',', $event_list[0]['checkbox_values']);
                $tmp = preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_list[0]['Description'])));
                $event_list[0]['Description'] = empty($tmp) ?  "" : $event_list[0]['Description'];

                $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";

                if($event_list[0]['fun_background_color']==0)
                {
                    $event_list[0]['fun_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_text_color']==0)
                {
                     $event_list[0]['fun_top_text_color1']="#FFFFFF";
                }
                else
                {
                     $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                }
                if($event_list[0]['fun_footer_background_color']==0)
                {
                     $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_block_background_color']==0)
                {
                     $event_list[0]['fun_block_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_background_color']==0)
                {
                     $event_list[0]['fun_top_background_color1']="#FFFFFF";
                }
                else
                {
                     $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                }
                if($event_list[0]['theme_color']==0)
                {
                     $event_list[0]['theme_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                }
                if($event_list[0]['fun_block_text_color']==0)
                {
                     $event_list[0]['fun_block_text_color']="#FFFFFF";
                }

                if($event_list[0]['fun_footer_background_color']==0)
                {
                     $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                $img=json_decode($event_list[0]['Images']);
                $Logo_images=json_decode($event_list[0]['Logo_images']);
                $Background_img=json_decode($event_list[0]['Background_img']);

                if(empty($Logo_images[0]))
                {
                    $Logo_images[0]="";
                }
                if(empty($Background_img[0]))
                {
                    $Background_img[0]="";
                }

                $event_list[0]['Images']=$img[0];

                $event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                $event_list[0]['Logo_images1']=$Logo_images[0];
                $event_list[0]['Background_img1']=$Background_img[0];
                $event_list[0]['description1']=$event_list[0]['Description'];
                $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];

                $checkbox_values = $event_list[0]['checkbox_values'];
                $menu_array = explode(',', $checkbox_values);

                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                
                $active_module = array_column($this->menu_list, 'id');
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;

                $event_list[0]['photo_filter_enabled'] = (in_array('54',array_column($this->menu_list,'id'))) ? '1' : '0';
            }
        }
        return $event_list;                   
    }
}