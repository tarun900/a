<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('native/event_model');
    }

    public function index()
    {
        $user = $this->login_model->check_token($this->input->post('_token'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Invalid token.'
                )
            );   
        } 
        else 
        {
            $res = $this->event_model->get_event_list($user[0]->Id);
                        
            $data = array(
              'success' => true,
              'data' => $res
            );
        }
        
        echo json_encode($data);
    }
}
