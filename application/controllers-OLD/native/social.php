<?php 

require_once( APPPATH . "third_party/TwitterAPIExchange.php" ); 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/social_model');
        include('application/libraries/nativeGcm.php');
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null);
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function social_list()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        if($event_id!=''  && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                  
                );   
            } 
            else 
            {
                $social_list = $this->social_model->getSocialListByEventId($event_id,$user_id);
                $data = array(
                    'social_list' => $social_list,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getTwitetrFeeds()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        if($event_id!=''  && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                  
                );   
            } 
            else 
            {
                
                $keywords = $this->social_model->getTwitterKeywords($event_id);
                if($keywords!=''){
                    $settings = array(
                        'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
                        'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
                        'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
                        'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
                    );
                    $url = 'https://api.twitter.com/1.1/search/tweets.json';
                    $requestMethod = 'GET';
                    $getfield = '?q=#'.$keywords.'&count=1000';
                    $twitter = new TwitterAPIExchange($settings);
                    $json =  $twitter->setGetfield($getfield)
                                     ->buildOauth($url, $requestMethod)
                                     ->performRequest();
                    $result = json_decode($json);
                    if(!empty($result))
                    {
                      foreach ($result->statuses as $key => $value) {
                          $img = $value->user->profile_image_url;
                          $result->statuses[$key]->user->profile_image_url = str_replace('_normal', '',$img);
                      }
                      $limit          = 20;
                      $page_no        = (!empty($page_no))?$page_no:1;
                      $start          = ($page_no-1)*$limit;

                      $total          = count($result->statuses);
                      $total_page     = ceil($total/$limit);
                      $data1           = array_slice($result->statuses,$start,$limit);

                      $data = array(
                          'hashtag' => $keywords,
                          'twitter_data' => $data1,
                          'total_page'  => $total_page,
                          /*'menu' => $this->menu_list,
                          'cmsmenu' => $this->cmsmenu,*/
                          'total_records'=>$total,
                      );
                  }
                  else
                  {
                     $data = array(
                      'success' => true,
                      'message' => "No feeds available."
                    );
                  }
                 }
                else {
                       $data = array(
                      'success' => true,
                      'message' => "No feeds available."
                    );
                }
                $data = array(
                  'success' => true,
                  'data' => $data
                );
           
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getInstagramFeeds()
    {
          $event_id=$this->input->post('event_id');
          $page_no =$this->input->post('page_no');

          if($event_id!='' && $page_no!='')
          {
              $access_token=$this->social_model->getInstaToken($event_id);
              /*if($access_token!='')
              {*/
                 
                  $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
                  $json_link.="access_token={$access_token}&count={1000}";
                  $json = file_get_contents($json_link);
                  $data = json_decode($json,TRUE);
                  $data = $data['data'];
                  $limit          = 12;
                  $page_no        = (!empty($page_no))?$page_no:1;
                  $start          = ($page_no-1)*$limit;

                  $total          = count($data);
                  $total_page     = ceil($total/$limit);
                  $data           = array_slice($data,$start,$limit);
                  /*if(!empty($data))
                  {*/
                  $data = array(
                      'success' => true,
                      'data' => ($data) ? $data : [],
                      'page_count'=>$total_page,
                      'total_records'=>$total,
                    );
                  /*}
                  else
                  {
                    $data = array(
                      'success' => false,
                      'message' => "No feeds available"
                    );
                  }*/
              /*}
              else
              {
                    $data = array(
                      'success' => false,
                      'message' => "No feeds available"
                    );
              }*/
          }
          else
          {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
          }
          echo json_encode($data);
            
    }   
    public function getFacebookFeed()
    {
        $event_id=$this->input->post('event_id');
        $page_no =(int)$this->input->post('page_no');
        if($event_id!='')
        {
          $page_no = ($page_no == 1) ? 0 : $page_no ;
          $offset = ($page_no == 1 || $page_no == 0) ? 0 : $page_no + 10;
          $profile_id = $this->social_model->getFacbookProfileId($event_id);
          $app_id = "206032402939985";
          $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
            
          $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
          $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?limit=10&offset=$offset&{$authToken}&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments");
          
          $facebook_feeds = json_decode($json_object,true);
          $fb_data        = $facebook_feeds['data'];
          /*$limit          = 9;
          $page_no        = (!empty($page_no))?$page_no:1;
          $start          = ($page_no-1)*$limit;

          $total          = count($fb_data);
          $total_page     = ceil($total/$limit);
          $fb_data        = array_slice($fb_data,$start,$limit);*/
         
          foreach ($fb_data as $key => $value) {
             $fb_data[$key]['logo'] = "https://graph.facebook.com/$profile_id/picture?type=large";
             $fb_data[$key]['created_time'] = date('d F Y',strtotime($value['created_time'])) ." at ".date('H:i A',strtotime($value['created_time']));
              if(array_key_exists('attachments', $value))
              {
                if(array_key_exists('subattachments',$value['attachments']['data'][0]))
                {
                    foreach ($value['attachments']['data'][0]['subattachments']['data'] as $key1 => $value1) 
                    {
                      foreach ($value1['media'] as $key11 => $value11)
                      {
                          $fb_data[$key]['attachments']['media'][] = $value11;
                      }
                      unset($fb_data[$key]['attachments']['data']);
                    }
                }
                else
                {
                  if(array_key_exists('image',$value['attachments']['data'][0]['media']))
                  {
                      $fb_data[$key]['attachments']['media'][] = $value['attachments']['data'][0]['media']['image'];
                      unset($fb_data[$key]['attachments']['data']);
                  }
                  else
                  {
                      $fb_data[$key]['attachments']['media'] = [];
                      unset($fb_data[$key]['attachments']['data']);
                  }
                }
              }
              else
              {
                $fb_data[$key]['attachments']['media'] = [];
              }
          }
          $data = array(
              'success' => true,
              'data' => ($fb_data) ? $fb_data : [],
              'total_page'=>(empty($fb_data)) ? -1 : 1,
              
              
          );
        }
        else
        {
          $data = array(
            'success' => false,
            'message' => "Invalid parameters"
          );
        }
         echo json_encode($data);
    } 
    public function notification(){
        //error_reporting(1);
        $temp = $this->input->post('gcm_id');
        $obj = new Gcm();
        $message_data['Message']='test';
        $result = $obj->send_notification($temp,$message_data);
        //print_r($result);exit;
    }
    public function getTwitetrFeedsNew()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $hashtag = $this->input->post('hashtag');

        if($event_id!=''  && $event_type!='' && $hashtag!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                  
                );   
            } 
            else 
            {
                  
                $keywords = $hashtag;
                if($keywords!=''){
                    $settings = array(
                        'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
                        'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
                        'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
                        'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
                    );
                    $url = 'https://api.twitter.com/1.1/search/tweets.json';
                    $requestMethod = 'GET';
                    $getfield = '?q=#'.$keywords.'&count=1000';
                    $twitter = new TwitterAPIExchange($settings);
                    $json =  $twitter->setGetfield($getfield)
                                     ->buildOauth($url, $requestMethod)
                                     ->performRequest();
                    $result = json_decode($json);
                    if(!empty($result))
                    {
                      foreach ($result->statuses as $key => $value) {
                          $img = $value->user->profile_image_url;
                          $result->statuses[$key]->user->profile_image_url = str_replace('_normal', '',$img);
                      }
                      $limit          = 20;
                      $page_no        = (!empty($page_no))?$page_no:1;
                      $start          = ($page_no-1)*$limit;

                      $total          = count($result->statuses);
                      $total_page     = ceil($total/$limit);
                      $data1           = array_slice($result->statuses,$start,$limit);

                      $data = array(
                          'hashtag' => $keywords,
                          'twitter_data' => $data1,
                          'total_page'  => $total_page,
                          /*'menu' => $this->menu_list,
                          'cmsmenu' => $this->cmsmenu,*/
                          'total_records'=>$total,
                      );
                  }
                  else
                  {
                     $data = array(
                      'success' => true,
                      'message' => "No feeds available."
                    );
                  }
                 }
                else {
                       $data = array(
                      'success' => true,
                      'message' => "No feeds available."
                    );
                }
                $data = array(
                  'success' => true,
                  'data' => $data
                );
           
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getTwitetrHashtags()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        if($event_id!=''  && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                  
                );   
            } 
            else 
            {
                  
                $keywords = $this->social_model->getTwitterKeywordsNew($event_id);
                if(!empty($keywords))
                {
                  $data = array(
                      'hashtag' => $keywords,
                  );

                  $data = array(
                    'success' => true,
                    'message' => 'Hashtags Found',
                    'data' => $data
                  );   
                }
                else
                {
                  $data = array(
                    'success' => true,
                    'message' => 'No Hashtags Found',
                    'data' => ''
                  ); 
                }
           
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
