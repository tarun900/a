<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fundraising extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/note_model');
        $this->load->model('native/event_template_model');

        $this->events = $this->event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images']      = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        $this->cmsmenu  = $this->cms_model->get_cms_page($this->input->post('event_id'));
       
        foreach ($this->cmsmenu as $key => $values) 
        {
            if(!empty($values['Images']))
            {
                $cmsbannerimage_decode              = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images']      = $cmsbannerimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Images']      = "";
            }
            if(!empty($values['Logo_images']))
            {
                $cmslogoimage_decode                = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
    }
    public function fundraising_home()
    {

        $event_id       = $this->input->post('event_id');
        $event_type     = $this->input->post('event_type');
        $token          = $this->input->post('token');
        $user_id        = $this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success'   => false,
                    'data'      => array(
                        'msg'   => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list         = $this->app_login_model->check_event_with_id($event_id,$user_id);

                $checkbox_values    = $event_list[0]['checkbox_values'];
                $menu_array         = explode(',', $checkbox_values);
               
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                 $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                $img                = json_decode($event_list[0]['Images']);
                $Logo_images        = json_decode($event_list[0]['Logo_images']);
                $Background_img     = json_decode($event_list[0]['Background_img']);
                if(empty($img[0]))
                {
                    $img[0]             ="";
                }
                if(empty($Logo_images[0]))
                {
                    $Logo_images[0]     ="";
                }
                if(empty($Background_img[0]))
                {
                    $Background_img[0]  ="";
                }
                $event_list[0]['Images']            = $img[0];
                $event_list[0]['Logo_images1']      = $Logo_images[0];
                $event_list[0]['Background_img1']   = $Background_img[0];
                $event_list[0]['description1']      = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1']  = $event_list[0]['Icon_text_color'];
                $note_status                        = 0;
                $cart_count                         = 0;
                if($user_id!='')
                {
                    $note_data  = $this->note_model->getNotesListByEventId($event_id,$user_id);
                    $cart_count = count($this->event_model->getcartdata($user_id));
                    if(count($note_data)>0)
                    {
                         $note_status   = 1;
                    }
                    else
                    {
                         $note_status   = 0;
                    }
                }
                $category       = $this->event_model->getCategoryEvent($event_id);
                $slides         = $this->event_model->getSlidesEvent($event_id);
                $fund_settings  = $this->event_model->getFundraisingSettings($event_id,$user_id);
                if(!empty($fund_settings))
                {
                    $url1=$fund_settings[0]['event_video_link'];
                    if(strstr($url1,'www.youtube.com'))
                    {
                        if(strstr($url1,'watch?v='))
                        {
                            $urlnew = str_replace('watch?v=', 'embed/', $url1);
                        }
                        else
                        {
                            $urlnew = $fund_settings[0]['event_video_link'];
                        }
                    }
                    else
                    { 
                        if(strstr($url1,'//player.vimeo.com/video/'))
                        {
                            $urlnew = $fund_settings[0]['event_video_link'];
                        }
                        else
                        {
                            $urlnew = str_replace("//vimeo.com/","//player.vimeo.com/video/", $url1);
                        }
                    }
                    $fund_settings[0]['event_video_link'] = $urlnew;
                }
                
                $currency               = $this->event_model->getCurrencyByEvent($event_id);
                $manullay_raise_flag    = $fund_settings[0]['raised_display'];
                if($manullay_raise_flag == '0')
                {
                   $raised_price        = $fund_settings[0]['raised_price'];
                   $order_tot_data      = $this->event_model->get_ordered_total($event_id,$fund_settings[0]['updatedate']);

                   foreach ($order_tot_data as $key => $value) 
                   {
                      $order_total      = $order_total+$value['price'];
                   }
                   $order_bid_data      = $this->event_model->get_bid_total($event_id,$fund_settings[0]['updatedate']);
                   $bid_tot             = $order_bid_data;
                   $pledge_tot          = $this->event_model->get_pledge_total($event_id,$fund_settings[0]['updatedate']);

                   $instant_donation_total  = $this->event_model->getinstantdonationtotal($event_id);  
                   $fundraising_donation    = $this->event_model->getfundraisingdonationtotal($event_id);
                   $raised_so_far           = $order_total+$bid_tot+$pledge_tot+$raised_price+$instant_donation_total+$fundraising_donation;
                }
                else
                {
                   $order_tot_data          = $this->event_model->get_ordered_total($event_id,null);
                   $order_total;
                   foreach ($order_tot_data as $key => $value) 
                   {
                      $order_total          = $order_total+$value['price'];
                   }
                    
                    $order_bid_data         = $this->event_model->get_bid_total($event_id,null);
                    $bid_tot                = $order_bid_data;
                    $pledge_tot             = $this->event_model->get_pledge_total($event_id,null);
                    $instant_donation_total = $this->event_model->getinstantdonationtotal($event_id);  
                    $fundraising_donation   = $this->event_model->getfundraisingdonationtotal($event_id);
                    $raised_so_far          = $order_total+$bid_tot+$pledge_tot+$instant_donation_total+$fundraising_donation;
                    if($raised_so_far=='')
                    {
                      $raised_so_far = 0;
                    }
                } 
               
                $latest_bids                = $this->event_model->get_latest_total_bids($event_id,3);
                $latest_pleadge             = $this->event_model->get_latest_pleadges($event_id,2);
                if(!empty($latest_bids) && !empty($latest_pleadge))
                {
                     $latest_pleadge_bids   = array_merge($latest_bids,$latest_pleadge);
                }
                else
                {
                    if(empty($latest_bids) && empty($latest_pleadge_bids))
                    {
                        $latest_pleadge_bids = array([0]=>"");
                    }
                    else
                    {
                        if(empty($latest_pleadge))
                        {
                            $latest_pleadge_bids = $latest_bids;
                        }
                        else
                        {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    } 
                }
                $menu_list_home = $this->event_model->getfrontfunmenu($event_id, null, 1);
                $arrAcc         = $this->event_model->getAccname($event_id); 
                $Subdomain      = $this->event_model->get_subdomain($event_id);

                $url            = base_url().'app/'.$arrAcc[0]['acc_name'].'/'.$Subdomain;
                $fb_url         = "https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=".$url."&picture=&display=popup&ref=plugin&src=share_button";
                /*$twitter_url    = "http://twitter.com/home?status=Support ".ucfirst($arrAcc[0]['acc_name']). " and checkout their fundraising app!  @allintheloop fundraising ".$url;*/
                $twitter_url    = "http://twitter.com/home?status=Support%20".ucfirst($arrAcc[0]['acc_name'])."%20and%20checkout%20their%20fundraising%20app!%20@allintheloop fundraising%20".$url;
                $donation_settings = $this->event_model->get_stripe_setting($event_id);

                $event_list = $this->get_event($event_id,$user_id);

                $data1 = array(
                    'events'                => $event_list,
                    'menu_list'             => $this->menu_list,
                    'home_menu'             => $menu_list_home,
                    'cmsmenu'               => $this->cmsmenu,
                    'fundraising_settings'  => $fund_settings,
                    'latest_pleadge_bids'   => $latest_pleadge_bids,
                    'facebook_url'          => $fb_url,
                    'twitter_url'           => $twitter_url,
                    'raised_so_far'         => $raised_so_far,
                    'category'              => $category,
                    'slides'                => $slides,
                    'currency'              => $currency,
                    'cart_count'            => $cart_count,
                    'note_status'           => $note_status,
                    'donation_settings'     => $donation_settings
                );

                $data       = array(
                  'success' => true,
                  'data'    => $data1
                );
           
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function fundraising_products()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $page_no    = $this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success'   => false,
                    'data'      => array(
                        'msg'   => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $products_data  = $this->event_model->products($event_id,$page_no);
               // print_r($products_data);
                $data1          = array(
                        'products'      => ($products_data['products']) ? $products_data['products'] : [],
                        'total_page'    =>$products_data['total_page']
                    );
                $data           = array(
                      'success'     => true,
                      'data'        => $data1
                    );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getProductsByCategory()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $page_no    = $this->input->post('page_no');
        $category   = $this->input->post('category');
        if($event_id!='' && $event_type!='' && $page_no!='' && $category!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success'   => false,
                    'data'      => array(
                        'msg'   => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $products_data=$this->event_model->productsByCategory($event_id,$page_no,$category);
                $data1 = array(
                    'products'      => $products_data['products'],
                    'total_page'    =>$products_data['total_page']

                );

                $data = array(
                  'success' => true,
                  'data'    => $data1
                );
           
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
   public function get_fundraising_donation_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');

        if($event_id!='' && $event_type!='' )
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success'   => false,
                    'data'      => array
                    (
                        'msg'   => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $fund_settings  = $this->event_model->getFundraisingSettings($event_id);
                $latest_bids    = $this->event_model->get_latest_total_bids($event_id,3);
                $latest_pleadge = $this->event_model->get_latest_pleadges($event_id,2);
                if(!empty($latest_bids) && !empty($latest_pleadge))
                {
                     $latest_pleadge_bids = array_merge($latest_bids,$latest_pleadge);
                }
                else
                {
                    if(empty($latest_bids) && empty($latest_pleadge_bids))
                    {
                        $latest_pleadge_bids = array([0]=>"");
                    }
                    else
                    {
                        if(empty($latest_pleadge))
                        {
                            $latest_pleadge_bids = $latest_bids;
                        }
                        else
                        {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    } 
                }
                $products_data  = $this->event_model->getDonationDetails($event_id);
                $supporter_data = $this->event_model->getSupporterData($event_id);
                $currency       = $this->event_model->getCurrencyByEvent($event_id);
                $note_status    = 0;
                $cart_count     = 0;
                if($user_id!='')
                {
                    $note_data  = $this->note_model->getNotesListByEventId($event_id,$user_id);
                    $cart_count = count($this->event_model->getcartdata($user_id));
                    if(count($note_data)>0)
                    {
                         $note_status = 1;
                    }
                    else
                    {
                         $note_status = 0;
                    }
                }
                 $manullay_raise_flag    = $fund_settings[0]['raised_display'];
                if($manullay_raise_flag == '0')
                {
                   $raised_price        = $fund_settings[0]['raised_price'];
                   $order_tot_data      = $this->event_model->get_ordered_total($event_id,$fund_settings[0]['updatedate']);

                   foreach ($order_tot_data as $key => $value) 
                   {
                      $order_total      = $order_total+$value['price'];
                   }
                   $order_bid_data      = $this->event_model->get_bid_total($event_id,$fund_settings[0]['updatedate']);
                   $bid_tot             = $order_bid_data;
                   $pledge_tot          = $this->event_model->get_pledge_total($event_id,$fund_settings[0]['updatedate']);

                   $instant_donation_total  = $this->event_model->getinstantdonationtotal($event_id);  
                   $fundraising_donation    = $this->event_model->getfundraisingdonationtotal($event_id);
                   $raised_so_far           = $order_total+$bid_tot+$pledge_tot+$raised_price+$instant_donation_total+$fundraising_donation;
                }
                else
                {
                   $order_tot_data          = $this->event_model->get_ordered_total($event_id,null);
                   $order_total;
                   foreach ($order_tot_data as $key => $value) 
                   {
                      $order_total          = $order_total+$value['price'];
                   }
                    
                    $order_bid_data         = $this->event_model->get_bid_total($event_id,null);
                    $bid_tot                = $order_bid_data;
                    $pledge_tot             = $this->event_model->get_pledge_total($event_id,null);
                    $instant_donation_total = $this->event_model->getinstantdonationtotal($event_id);  
                    $fundraising_donation   = $this->event_model->getfundraisingdonationtotal($event_id);
                    $raised_so_far          = $order_total+$bid_tot+$pledge_tot+$instant_donation_total+$fundraising_donation;
                    if($raised_so_far=='')
                    {
                      $raised_so_far = 0;
                    }
                } 
                array_walk($products_data,function(&$item){$item=strval($item);});
                $data = array(
                  'success'             => true,
                  'fundraising_data'    => (!empty($products_data)) ? $products_data : '',
                  'supporter_data'      =>(!empty($supporter_data)) ? $supporter_data : [],
                  'latest_pleadge_bids' => (!empty($latest_pleadge_bids)) ? $latest_pleadge_bids : '',
                  'currency'            => $currency,
                  'cart_count'          => $cart_count,
                  'note_status'         => $note_status,
                  'raised_amount'       => $raised_so_far,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function save_fundraising_donation_details()
    {
        $email      = $this->input->post('email');
        $name       = $this->input->post('name');
        $address    = $this->input->post('address');
        $city       = $this->input->post('city');
        $postcode   = $this->input->post('postcode');
        $country    = $this->input->post('country');
        $amount     = $this->input->post('amount');
        $comment    = $this->input->post('comment');
        $status     = $this->input->post('status');
        $event_id   = $this->input->post('event_id');

        if($event_id!='' && $name!='' && $address!='' && $city!='' && $postcode!='' && $country!='' && $amount!=''  && $status!='' && $email!='')  
        {
                $data['name']               = $name;
                $data['email']              = $email;
                $data['address']            = $address;
                $data['address_zip']        = $postcode;
                $data['address_city']       = $city;
                $data['address_state']      = '';
                $data['address_country']    = $country;
                $data['donation_amount']    = $amount;
                $data['donation_comment']   = $comment;
                $data['status']             = $status;
                $data['create_date']        = date('Y-m-d h:i:s');
                $data['event_id']           = $event_id;

                $result = $this->event_model->saveDonationDetails($data);
                if($result){
                    $data = array(
                      'success' => true,
                      'message' => 'Saved successfully.',
                    );    
                }else{
                    $data = array(
                      'success' => true,
                      'message' => 'Something went wrong. Please try again.',
                    );
                }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function save_instant_donation_details()
    {
        $email      = $this->input->post('email');
        $name       = $this->input->post('name');
        $address    = $this->input->post('address');
        $city       = $this->input->post('city');
        $postcode   = $this->input->post('postcode');
        $country    = $this->input->post('country');
        $amount     = $this->input->post('amount');
        $comment    = $this->input->post('comment');
        $status     = $this->input->post('status');
        $event_id   = $this->input->post('event_id');

        if($event_id!='' && $name!='' && $address!='' && $city!='' && $postcode!='' && $country!='' && $amount!=''  && $status!='' && $email!='')  
        {
                $data['name']               = $name;
                $data['email']              = $email;
                $data['address']            = $address;
                $data['address_zip']        = $postcode;
                $data['address_city']       = $city;
                $data['address_state']      = '';
                $data['address_country']    = $country;
                $data['donation_amount']    = $amount;
                $data['donation_comment']   = $comment;
                $data['status']             = $status;
                $data['create_date']        = date('Y-m-d h:i:s');
                $data['event_id']           = $event_id;

                $result = $this->event_model->saveInstantDonationDetails($data);
                if($result){
                    $data = array(
                      'success' => true,
                      'message' => 'Saved successfully.',
                    );    
                }else{
                    $data = array(
                      'success' => true,
                      'message' => 'Something went wrong. Please try again.',
                    );
                }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function orders()
    {
        $user_id    = $this->input->post('user_id');
        $event_id   = $this->input->post('event_id');

        if($user_id!='' && $event_id!='')
        {
            $latest_bids    = $this->event_model->get_latest_total_bids($event_id,3);
            $latest_pleadge = $this->event_model->get_latest_pleadges($event_id,2);
            if(!empty($latest_bids) && !empty($latest_pleadge))
            {
                 $latest_pleadge_bids = array_merge($latest_bids,$latest_pleadge);
            }
            else
            {
                if(empty($latest_bids) && empty($latest_pleadge_bids))
                {
                    $latest_pleadge_bids=array([0]=>"");
                }
                else
                {
                    if(empty($latest_pleadge))
                    {
                        $latest_pleadge_bids = $latest_bids;
                    }
                    else
                    {
                        $latest_pleadge_bids = $latest_pleadge;
                    }
                } 
            }
            $orders     = $this->event_model->get_order_list($user_id);
            $auction    = $this->event_model->get_auction_list($user_id,$event_id);
            $pledge     = $this->event_model->get_pledgelist($user_id,$event_id);
            $currency   = $this->event_model->getCurrencyByEvent($event_id);
            $note_status= 0;
            $cart_count = 0;
            if($user_id!='')
            {
                $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                $cart_count= count($this->event_model->getcartdata($user_id));
                if(count($note_data)>0)
                {
                     $note_status = 1;
                }
                else
                {
                     $note_status = 0;
                }
            }
            $data = array(
                'success'               => true,
                'orders'                => $orders ,
                'auctions'              => $auction ,
                'pledges'               => $pledge,
                'latest_pleadge_bids'   => (!empty($latest_pleadge_bids)) ? $latest_pleadge_bids : '',
                'currency'              => $currency,
                'cart_count'            => $cart_count,
                'note_status'           => $note_status,
            ); 
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_event($event_id,$user_id)
    {   
        if($event_id!='')
        {
            $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
            $user = $this->app_login_model->getUserDetailsId($user_id);

            if(!empty($event_list))
            {
                $menus = explode(',', $event_list[0]['checkbox_values']);
                $tmp = preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_list[0]['Description'])));
                $event_list[0]['Description'] = empty($tmp) ?  "" : $event_list[0]['Description'];

                $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";

                if($event_list[0]['fun_background_color']==0)
                {
                    $event_list[0]['fun_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_text_color']==0)
                {
                     $event_list[0]['fun_top_text_color1']="#FFFFFF";
                }
                else
                {
                     $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                }
                if($event_list[0]['fun_footer_background_color']==0)
                {
                     $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_block_background_color']==0)
                {
                     $event_list[0]['fun_block_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_background_color']==0)
                {
                     $event_list[0]['fun_top_background_color1']="#FFFFFF";
                }
                else
                {
                     $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                }
                if($event_list[0]['theme_color']==0)
                {
                     $event_list[0]['theme_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                }
                if($event_list[0]['fun_block_text_color']==0)
                {
                     $event_list[0]['fun_block_text_color']="#FFFFFF";
                }

                if($event_list[0]['fun_footer_background_color']==0)
                {
                     $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                $img=json_decode($event_list[0]['Images']);
                $Logo_images=json_decode($event_list[0]['Logo_images']);
                $Background_img=json_decode($event_list[0]['Background_img']);

                if(empty($Logo_images[0]))
                {
                    $Logo_images[0]="";
                }
                if(empty($Background_img[0]))
                {
                    $Background_img[0]="";
                }

                $event_list[0]['Images']=$img[0];

                //$event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                $event_list[0]['banners'] = $img;
                $event_list[0]['Logo_images1']=$Logo_images[0];
                $event_list[0]['Background_img1']=$Background_img[0];
                $event_list[0]['description1']=$event_list[0]['Description'];
                $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];

                $checkbox_values = $event_list[0]['checkbox_values'];
                $menu_array = explode(',', $checkbox_values);

                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                
                $active_module = array_column($this->menu_list, 'id');
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;

                $event_list[0]['photo_filter_enabled'] = (in_array('54',array_column($this->menu_list,'id'))) ? '1' : '0';
            }
        }
        return $event_list;                   
    }  
}
