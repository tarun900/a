<?php 
require_once( APPPATH . "third_party/TwitterAPIExchange.php" ); 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/activity_model');
        $this->load->model('native/social_model');
        $this->load->model('native/app_login_model');

        //date_default_timezone_set("UTC");
    }
    public function getFeeds()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id'); // optional
        $page_no =$this->input->post('page_no');
        
        if($event_id!= '' && $page_no!= '')
        {
            $public_message_feeds = $this->activity_model->getPublicMessageFeeds($event_id,$user_id);
            $photo_feed     = $this->activity_model->getPhotoFeeds($event_id,$user_id);
            $check_in_feed  = $this->activity_model->getCheckInFeeds($event_id,$user_id);
            $rating_feed    = $this->activity_model->getRatingFeeds($event_id,$user_id);
            $user_feed      = $this->activity_model->getUserFeeds($event_id,$user_id);
            $activity_feeds = $this->activity_model->getActivityFeeds($event_id,$user_id);
            
            $data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds);

            function sortByTime($a, $b)
            {
                $a = $a['Time'];
                $b = $b['Time'];

                if ($a == $b)
                {
                    return 0;
                }
                return ($a > $b) ? -1 : 1;
            } 
            if(!empty($data))
            {
                usort($data, 'sortByTime');
                foreach ($data as $key => $value)
                {
                    $data[$key]['Time'] = $this->activity_model->get_timeago(strtotime($value['Time']));
                }
            }  
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);
            foreach ($data as $key => $value)
            {
                $comments = $this->activity_model->getComments($value['id'],$value['type'],$user_id); 
                $data[$key]['total_comments'] = count($comments);
                $data[$key]['comments'] = $comments;
            }
            $data = array(
                  'success'     => true,
                  'data'        => $data,
                  'total_page'  => $total_page,
                  'total'       =>$total,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   
    public function getFeedDetails()
    {
        $type   = $this->input->post('type');
        $id     = $this->input->post('id');
        
        if($id!= '' && $type!= '')
        {
            if($type == "activity")
            {
                $feed_details           = $this->activity_model->getFeedDetails($id);
                $image_array            = $feed_details->image;
                $feed_details->image    = json_decode($image_array);
            }

            $data = array(
                  'success' => true,
                  'data'    => ($feed_details) ? $feed_details : "",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   
    public function postUpdate()
    {
        $event_id   = $this->input->post('event_id');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $message    = $this->input->post('message');

        if($event_id!='' && $message!='' && $token!='' && $user_id!='')
        {
            $this->load->model('DufourCoProductions/app_login_model');
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                        )
                    );   
            } 
            else 
            {
                $message_data['message']    = $message;
                $message_data['sender_id']  = $user_id;
                $message_data['event_id']   = $event_id;
                $message_data['image']      = "";
                $message_data['time']       = date("Y-m-d H:i:s");

                $message_id = $this->activity_model->savePublicPost($message_data);

                if($message_id!=0)
                {
                    $data = array(
                        'success'       => true,
                        'message'       => "Successfully Saved",
                        'message_id'    => $message_id,
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something went wrong. Please try again.",
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveActivityImages()
    {
        $activity_id    = $this->input->post('activity_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            //copy("./assets/user_files/".$new_image_name,"./assets/user_files/thumbnail/".$new_image_name);
            $images         = $new_image_name;
            $message_data   = $this->activity_model->getMessageDetails($activity_id);
            
            $update_data['image'] = $new_image_name;
            if($message_data->image!='')
            {
                $arr                    = json_decode($message_data->image);
                $arr[]                  = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->activity_model->updateMessageImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->activity_model->updateMessageImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $activity_id=$this->input->post('activity_id');
        if($event_id!='' && $activity_id!='')
        {
            $images = $this->activity_model->getImages($event_id,$activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function feedLike()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $where = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                );
            $like = $this->activity_model->getFeedLike($where);
            if($like!='')
            {
                $update_data = array(
                    'like' => ($like == '1') ? '0'  :  '1',
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->updateFeedLike($update_data,$where);
            }
            else{
                $data = array(
                    'module_type' => $module_type,
                    'module_primary_id' => $module_primary_id,
                    'user_id' => $user_id,
                    'like' => ($like) ? 0  :  1,
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->makeFeedLike($data);
            }    
            $data = array(
                'success' => true,
                'message' => 'Successfully done.',
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function feedComment()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='' && $comment!='')
        {
            $data = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                );
            $id = $this->activity_model->makeFeedComment($data);
              
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => $id,
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function saveCommentImages()
    {
        $activity_id    = $this->input->post('comment_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
           
            $images         = $target_path;
            $message_data   = $this->activity_model->getCommentDetails($activity_id);
            
            if($message_data->comment_image!='')
            {
                $arr                    = json_decode($message_data->comment_image);
                $arr[]                  = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->activity_model->updateCommentImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->activity_model->updateCommentImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getCommentImageList()
    {
        $activity_id=$this->input->post('comment_id');
        if($activity_id!='')
        {
            $images = $this->activity_model->getCommentImages($activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function deleteFeedComment()
    {
        $comment_id = $this->input->post('comment_id');

        if($comment_id!='')
        {
            $this->activity_model->deleteComment($comment_id);
            $data = array(
                'success' => true,
                'message' => 'Comment deleted successfully',
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
                );
        }
        echo json_encode($data);
    }
    public function get_internal_data($event_id,$user_id)
    {
        $per = $this->activity_model->getPermission($event_id);
        if(!empty($per))
        {
            $public_message_feeds = ($per['public']) ? $this->activity_model->getPublicMessageFeeds_new($event_id,$user_id) : [];

            $photo_feed = ($per['photo']) ? $this->activity_model->getPhotoFeeds_new($event_id,$user_id) : [];
            
            $check_in_feed = ($per['check_in']) ? $this->activity_model->getCheckInFeeds_new($event_id,$user_id) : [];
            
            $rating_feed = ($per['rating']) ? $this->activity_model->getRatingFeeds_new($event_id,$user_id) : [];
            
            $user_feed = ($per['update_profile']) ? $this->activity_model->getUserFeeds_new($event_id,$user_id) : [];
            
            $arrSaveSession = ($per['show_session']) ? $this->activity_model->getSaveSessionFeed($event_id,$user_id) : [];
           
            $arrauctionbid = ($per['auction_bids']) ? $this->activity_model->getauctionbidsFeed($event_id,$user_id) : [];
            
            $arrcomments = ($per['comments']) ? $this->activity_model->getcommentfeed($event_id,$user_id) : [];
            
            $activity_feeds = $this->activity_model->getActivityFeeds_new($event_id,$user_id);
            $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
            
            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($activity_data, 'sortByOrder');
            $acc = $this->activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];

            foreach ($activity_data as $key => $value)
            {
                $activity_data[$key]['comments']=$this->activity_model->getComments($value['id'],$value['type'],$user_id);
                $activity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $activity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $activity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $activity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $activity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $activity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $activity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $activity_data[$key]['timestamp'] = strtotime($value['Time']);

                $activity_data[$key]['f_share_url'] = '';
                $activity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;;
                $activity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $activity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($activity_data[$key]['image']))
                {
                    $activity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $activity_data[$key]['is_alert'] = 0;
            }
            return $activity_data;
        }
        else
        {
            $activity_data = [];
            return $activity_data;
        }
    }
    public function get_alert_data($event_id,$user_id)
    {
        $per = $this->activity_model->getPermission($event_id);
        if(!empty($per))
        {

            $orgpublic_message_feed = ($per['public']) ? $this->activity_model->getPublicMessageFeeds_by_org($event_id,$user_id) : [];

            $orgphoto_feed = ($per['photo']) ? $this->activity_model->getPhotoFeeds_by_org($event_id,$user_id) : [];
            
            $orgcheck_in_feed = ($per['check_in']) ? $this->activity_model->getCheckInFeeds_by_org($event_id,$user_id) : [];

            $orgrating_feed = ($per['rating']) ? $this->activity_model->getRatingFeeds_by_org($event_id,$user_id) : [];
            
            $orguser_feed = ($per['update_profile']) ? $this->activity_model->getUserFeeds_by_org($event_id,$user_id) : [];
            
            $orgarrSaveSession = ($per['show_session']) ? $this->activity_model->getSaveSessionFeed_by_org($event_id,$user_id) : [];
            
            $orgarrauctionbid = ($per['auction_bids']) ? $this->activity_model->getauctionbidsFeed_by_org($event_id,$user_id) : [];

            $orgarrcomments = ($per['comments']) ? $this->activity_model->getcommentfeed_by_org($event_id,$user_id) : [];
            
            $orgactivity_feeds = $this->activity_model->getActivityFeeds_by_org($event_id,$user_id);
            $notify=$this->activity_model->get_all_notification_by_event($event_id,$user_id);
            $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);

            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($orgactivity_data, 'sortByOrder');            

            $acc = $this->activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];

            foreach ($orgactivity_data as $key => $value) 
            {
                $orgactivity_data[$key]['comments']=$this->activity_model->getComments($value['id'],$value['type'],$user_id);
                $orgactivity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $orgactivity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $orgactivity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $orgactivity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $orgactivity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $orgactivity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $orgactivity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $orgactivity_data[$key]['timestamp'] = strtotime($value['Time']);
                $orgactivity_data[$key]['f_share_url'] = '';
                $orgactivity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;
                $orgactivity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $orgactivity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($orgactivity_data[$key]['image']))
                {
                    $orgactivity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $orgactivity_data[$key]['is_alert'] = 1;
            }
            return $orgactivity_data;
        }
        else
        {
            $orgactivity_data = [];
            return $orgactivity_data;
        }
    }
    public function get_twitter_data($event_id,$tw_next_url)
    {
        $per = $this->activity_model->getPermission($event_id);    
        if(!empty($per['hashtag']))
        {
            $settings = array(
            'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
            'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
            'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
            'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
            );
            $url = 'https://api.twitter.com/1.1/search/tweets.json';
            if(!empty($tw_next_url))
            {   
                $getfield = $tw_next_url;
            }
            else
            {
                $getfield = '?q=#'.$per['hashtag'].'&count=5';
            }

            if($tw_next_url == '1')
            {
                $tweets['statuses'] = [];
            }
            else
            {
                $requestMethod = 'GET';
                $twitter = new TwitterAPIExchange($settings);
                $tweest = $twitter->setGetfield($getfield)
                                 ->buildOauth($url, $requestMethod)
                                 ->performRequest();
                $tweets = json_decode($tweest,true);
            } 
            foreach ($tweets['statuses'] as $key => $value)
            {
                $tweets['statuses'][$key]['created_at'] = $this->get_timeago($value['created_at']);
                $tweets['statuses'][$key]['timestamp'] = strtotime($value['created_at']);
                $tweets['statuses'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['t_share_url'] = "http://twitter.com/home?status=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['url'] = "https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                if(array_key_exists('extended_entities',$value))
                {
                    foreach($value['extended_entities']['media'] as $k => $v)
                    {
                        $tweets['statuses'][$key]['media'][] = $v['media_url'];
                    }
                    unset($tweets['statuses'][$key]['extended_entities']);
                }
                else
                {
                    $tweets['statuses'][$key]['media'] = [];
                }
            }
        }
        if(!empty($tweets['statuses']))
        {
            $twitter = $tweets;
        }
        else
        {
            $twitter = new stdClass();
            $twitter->statuses = [];
            $twitter->search_metadata = new stdClass();
        }
        return $twitter;
    }
    public function get_facebook_data($event_id,$fb_next_url)
    {   
        $per = $this->activity_model->getPermission($event_id);
        if(!empty($per['fbpage_name']))
        {
            $profile_id=$per['fbpage_name'];
            $app_id = "206032402939985";
            $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
            $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
            if(!empty($fb_next_url))
            {
                if($fb_next_url == '1')
                {
                    $facebook_feeds['data'] = [];                    
                }
                else
                {
                    $json_object = file_get_contents($fb_next_url); 
                    $facebook_feeds=json_decode($json_object,true);
                }
            }
            else
            {
                $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=5");
                $facebook_feeds=json_decode($json_object,true);
            }
            foreach ($facebook_feeds['data'] as $key => $value) 
            {
                $facebook_feeds['data'][$key]['logo'] = "https://graph.facebook.com/$profile_id/picture?type=large";
                $facebook_feeds['data'][$key]['created_time'] = $this->get_timeago($value['created_time']);
                $facebook_feeds['data'][$key]['timestamp'] = strtotime($value['created_time']);
                $tmp = explode('_',$value['id']);
                
                $facebook_feeds['data'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                $facebook_feeds['data'][$key]['t_share_url'] = "http://twitter.com/home?status=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                $facebook_feeds['data'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                $facebook_feeds['data'][$key]['url'] = "https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                if(array_key_exists('attachments', $value))
                {
                    if(array_key_exists('subattachments',$value['attachments']['data'][0]))
                    {
                        foreach ($value['attachments']['data'][0]['subattachments']['data'] as $key1 => $value1) 
                        {
                            foreach ($value1['media'] as $key11 => $value11)
                            {
                                $facebook_feeds['data'][$key]['attachments']['media'][] = $value11;
                            }
                            unset($facebook_feeds['data'][$key]['attachments']['data']);
                        }
                    }
                    else
                    {
                        if(array_key_exists('image',$value['attachments']['data'][0]['media']))
                        {
                            $facebook_feeds['data'][$key]['attachments']['media'][] = $value['attachments']['data'][0]['media']['image'];
                            unset($facebook_feeds['data'][$key]['attachments']['data']);
                        }
                        else
                        {
                            $facebook_feeds['data'][$key]['attachments']['media'] = [];
                            unset($facebook_feeds['data'][$key]['attachments']['data']);
                        }
                    }
                }
                else
                {
                    $facebook_feeds['data'][$key]['attachments']['media'] = [];
                }
                if(array_key_exists('comments',$value))
                {
                    foreach ($value['comments']['data'] as $k => $v)
                    {
                        $facebook_feeds['data'][$key]['comments']['data'][$k]['from']['logo'] = "https://graph.facebook.com/".$v['from']['id']."/picture?type=large";
                    }
                }
                else
                {
                    $facebook_feeds['data'][$key]['comments'] = new stdClass();
                }
            }
        }

        
        if(!empty($facebook_feeds['data']))
        {
            $facebook = $facebook_feeds;
        }
        else
        {
            $facebook = new stdClass();
            /*$facebook->data = [];
            $facebook->paging = new stdClass();*/
        }
        return $facebook;
    }
    public function get_instagram_data($event_id,$insta_next_url)
    {
        $access_token=$this->activity_model->get_instagram_access_token($event_id);
        $photo_count=5;     
        if(!empty($insta_next_url))
        {
            if($insta_next_url == '1')
            {
                $instagram_feed['data'] = [];                    
            }
            else
            {
                $json_link=$insta_next_url;
            }
        }
        else
        {
            $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
            $json_link.="access_token={$access_token}&count={$photo_count}";
        }
        $json = file_get_contents($json_link);
        $instagram_feed = json_decode($json,TRUE);
        foreach ($instagram_feed['data'] as $key => $value) {
            $instagram_feed['data'][$key]['created_time'] = $this->get_timeago(date("Y-m-d H:i:s",$value['created_time']));
            $instagram_feed['data'][$key]['timestamp'] = strtotime(date("Y-m-d H:i:s",$value['created_time']));
            $instagram_feed['data'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=".$value['link'];
            $instagram_feed['data'][$key]['t_share_url'] = "http://twitter.com/home?status=".$value['link'];
            $instagram_feed['data'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=".$value['link'];
            $instagram_feed['pagination'] = ($instagram_feed['pagination'])?: new stdClass();
        }
        return ($instagram_feed)?: new stdClass();
    }
    public function get_all()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        $fb_next_url = $this->input->post('fb_next_url');
        $tw_next_url = $this->input->post('tw_next_url');
        $insta_next_url = $this->input->post('insta_next_url');

        if($event_id!='' && $user_id!='')
        {
            $per = $this->activity_model->getPermission($event_id);
           
            $activity_data = $this->get_internal_data($event_id,$user_id);
            $orgactivity_data = $this->get_alert_data($event_id,$user_id);
            
            $limit          = 5;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $activity_data  = array_slice($activity_data,$start,$limit);
            $orgactivity_data  = array_slice($orgactivity_data,$start,$limit);
            
            $twitter = $this->get_twitter_data($event_id,$tw_next_url);
            $facebook = $this->get_facebook_data($event_id,$fb_next_url);
            $instagram =$this->get_instagram_data($event_id,$insta_next_url);
            $data = array(
                    'all'      => $activity_data,
                    'alerts'   => $orgactivity_data,
                    'facebook' => $facebook,
                    'twitter'  => $twitter,
                    'instagram'=> $instagram
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_internal()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='' && $user_id!='')
        {
            $activity_data = $this->get_internal_data($event_id,$user_id);

            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($activity_data);
            $total_page     = ceil($total/$limit);
            $activity_data  = array_slice($activity_data,$start,$limit);

            
            $data = array(
                    'internal'      => $activity_data,
                    'total_page' => $total_page,
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_alert()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='' && $user_id!='')
        {   
            $orgactivity_data = $this->get_alert_data($event_id,$user_id);
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($orgactivity_data);
            $total_page     = ceil($total/$limit);
            $orgactivity_data  = array_slice($orgactivity_data,$start,$limit);
            $data = array(
                    'internal'   => $orgactivity_data,
                    'total_page' => $total_page,
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_social()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $fb_next_url = $this->input->post('fb_next_url');
        $tw_next_url = $this->input->post('tw_next_url');

        if($event_id!='' && $user_id!='')
        {
            $twitter = $this->get_twitter_data($event_id,$tw_next_url);
            $facebook = $this->get_facebook_data($event_id,$fb_next_url);
            $instagram =$this->get_instagram_data($event_id,$insta_next_url);
            $data = array(
                    'facebook' => $facebook,
                    'twitter'  => $twitter,
                    'instagram'=> $instagram
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_timeago($ptime)
    {    
        $estimate_time = time() - strtotime($ptime);
        if ($estimate_time < 1)
        {
           return '1 second ago';
        }
        $condition = array(
              12 * 30 * 24 * 60 * 60 => 'year',
              30 * 24 * 60 * 60 => 'month',
              24 * 60 * 60 => 'day',
              60 * 60 => 'hour',
              60 => 'minute',
              1 => 'second'
        );
        foreach ($condition as $secs => $str)
        {
            $d = $estimate_time / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
        $user_id=$this->input->post('user_id');
    }
    public function feedLikeNew()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $event_id=$this->input->post('event_id');
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $where = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'event_id' => $event_id,
                );
            $like = $this->activity_model->getFeedLike($where);
            if($like!='')
            {
                $update_data = array(
                    'like' => ($like == '1') ? '0'  :  '1',
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->updateFeedLike($update_data,$where);
            }
            else
            {
                $data = array(
                    'module_type' => $module_type,
                    'module_primary_id' => $module_primary_id,
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'like' => ($like) ? 0  :  1,
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->activity_model->makeFeedLike($data);
            }    
            $data = array(
                'success' => true,
                'message' => 'Successfully done.',
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function feedCommentNew()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $event_id = $this->input->post('event_id');
        
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $data = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                'event_id' => $event_id,
                );
            if(!empty($_FILES['image']))
            {
                $new_image_name = round(microtime(true) * 1000).".jpeg";
                $target_path    = "././assets/user_files/".$new_image_name;              
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                $images         = $target_path;
                $arr[0]                 = $new_image_name;
                $data['comment_image']    = json_encode($arr);
            }

            $id = $this->activity_model->makeFeedComment($data);
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => $id,
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function add_photo_filter_image()
    {
        $event_id = $this->input->post('event_id');
        $sender_id = $this->input->post('user_id');

        if($event_id!='' && $sender_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            //copy("./assets/user_files/".$new_image_name,"./assets/user_files/thumbnail/".$new_image_name);
            
            $arr[0]              = $new_image_name;
            $insert['image']     = json_encode($arr);
            $insert['message']   = "";
            $insert['event_id']  = $event_id;
            $insert['sender_id'] = $sender_id;
            $insert['time']      = date('Y-m-d H:i:s'); 
            $this->activity_model->savePublicPost($insert);
            
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_photofilter_url()
    {
        $event_id = $this->input->post('event_id');
        $sender_id = $this->input->post('user_id');

        if($event_id!='' && $sender_id!='')
        {
            $new_image_name = $event_id.'-'.$sender_id.'-'.round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/images/linkedin_share/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
                  'url'     => base_url().'assets/images/linkedin_share/'.$new_image_name 
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
?>