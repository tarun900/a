<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('Native/App_login_model');
        $this->load->model('Native/Event_template_model');
        $this->load->model('Native/Cms_model');
        $this->load->model('Native/Event_model');
        $this->load->model('Native/Settings_model');
        include('application/libraries/nativeGcm.php');
        include('application/libraries/FcmV2.php');
       

        /*$this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));*/
    }

    public function headerSettings()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('_token');
        $user = $this->app_login_model->check_token_with_event($token,$event_id);
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $menu = $this->settings_model->getNotesHeader($event_id);
            $menu_list = explode(',', $menu->checkbox_values);
            $menu_settings = (in_array(6, $menu_list)) ? 1 : 0;
            

            $data = array(
                'event' => $user[0],
                'event_feture_product' => $fetureproduct,
                /*'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu,*/
                'note_enable' => $menu_settings,
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }
    /*public function notification()
    {
        $event_id=$this->input->post('event_id');
        $menu_id=$this->input->post('menu_id');
        $user_id=$this->input->post('user_id');

        $result = array();    
            if($event_id=='' || $menu_id=='' || $user_id=='')
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Invalid parameter.'
                    )
                ); 
            }
            else
            {
                $gcm_id = $this->settings_model->getGcmId($user_id);
                $device = $this->settings_model->getDevice($user_id);
                if($gcm_id!='')
                {
                    $data = $this->settings_model->is_notification_read($event_id,$menu_id,$user_id);
                    $extra['message_type'] = '';
                    $extra['message_id'] = '';
                    $extra['event'] = $this->settings_model->event_name($event_id);
                    if(!empty($data))
                    {
                        $obj = new Gcm();
                        foreach ($data as $key => $value) 
                        {
                            if($device == "Iphone")
                            {
                                $msg =  $value['content'];
                                
                                $result = $obj->send_notification($gcm_id,$msg,$extra,$device);
                            }
                            else
                            {
                                $msg['title'] = "New Notification";
                                $msg['message'] = $value['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($gcm_id,$msg,$extra);
                            } 
                        }
                    }
                }   
                $data = array(
                    'success' => true,
                    'data' => $result,
                );
        }
        echo json_encode($data);    
    }*/
    public function notification()
    {   
       
        echo shell_exec('/usr/local/bin/php index.php Cronjob import_attendee');
        echo shell_exec('/usr/local/bin/php index.php Cronjob import_speaker');
        echo shell_exec('/usr/local/bin/php index.php Cronjob import_exhi');
        echo shell_exec('/usr/local/bin/php index.php Cronjob import_agenda');
        /*$url = "https://www.allintheloop.net/aitldemo/cronjob/test";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);*/
        /*error_reporting(E_ALL);
        echo shell_exec('/usr/local/bin/php aitldemo index.php cronjob import_attendee');
        echo shell_exec('/usr/local/bin/php aitldemo index.php cronjob import_attendee');*/
        
                
        $users = $this->Settings_model->getAllUsers();
               
         foreach ($users as $key => $value) 
        {
            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $notification = $this->Settings_model->getSceduledNotification($value['Id'],$value['gcm_id'],$event_id);
                $event_id_arr = array('619');
                if(!in_array($event_id,$event_id_arr))
                {
                    if(!empty($notification))
                    {
                        foreach ($notification as $key => $noti) 
                        {
                            if(!empty($noti['moduleslink']))
                            {
                                $extra['message_type'] = 'cms';
                                $extra['message_id'] = $noti['moduleslink'];
                            }
                            elseif(!empty($noti['custommoduleslink']))
                            {
                                $extra['message_type'] = 'custom_page';
                                $extra['message_id'] = $noti['custommoduleslink'];
                            }
                            $extra['event'] = $this->Settings_model->event_name($event_id);
                            if($value['device'] == "Iphone")
                            {   
                                $obj = new Fcm();
                                $extra['title'] = $noti['title'];
                                $msg =  $noti['content'];
                                $result[] = $obj->send(1,$value['gcm_id'],$msg,$extra,$value['device'],$event_id);  
                            }
                            else
                            {   
                                $obj = new Gcm($event_id);
                                $msg['title'] =   $noti['title'];
                                $msg['message'] = $noti['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                            $this->Event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                        }
                    }
                }
                else
                {
                    if(!empty($notification))
                    {
                        foreach ($notification as $key => $noti) 
                        {
                            $obj = new Gcm($event_id);
                            if(!empty($noti['moduleslink']))
                            {
                                $extra['message_type'] = 'cms';
                                $extra['message_id'] = $noti['moduleslink'];
                            }
                            elseif(!empty($noti['custommoduleslink']))
                            {
                                $extra['message_type'] = 'custom_page';
                                $extra['message_id'] = $noti['custommoduleslink'];
                            }
                            $extra['event'] = $this->Settings_model->event_name($event_id);
                            if($value['device'] == "Iphone")
                            {
                                $extra['title'] = $noti['title'];
                                $msg =  $noti['content'];
                                  
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            }
                            else
                            {
                                $msg['title'] =   $noti['title'];
                                $msg['message'] = $noti['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                            $this->Event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                        }
                    }
                    
                }
            }
        }
        print_r($result);  
    }

    public function notification_test()
    {   
        $users = $this->settings_model->getAllUsers();
        dd($users);
        foreach ($users as $key => $value) 
        {
            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $notification = $this->settings_model->getSceduledNotification($value['Id'],$value['gcm_id'],$event_id);
                $event_id_arr = array('619');

                if(!in_array($event_id,$event_id_arr))
                {
                    if(!empty($notification))
                    {
                        foreach ($notification as $key => $noti) 
                        {
                            
                            if(!empty($noti['moduleslink']))
                            {
                                $extra['message_type'] = 'cms';
                                $extra['message_id'] = $noti['moduleslink'];
                            }
                            elseif(!empty($noti['custommoduleslink']))
                            {
                                $extra['message_type'] = 'custom_page';
                                $extra['message_id'] = $noti['custommoduleslink'];
                            }
                            $extra['event'] = $this->settings_model->event_name($event_id);
                            if($value['device'] == "Iphone")
                            {   
                                $obj = new Fcm();
                                $extra['title'] = $noti['title'];
                                $msg =  $noti['content'];
                                $result[] = $obj->send(1,$value['gcm_id'],$msg,$extra,$value['device']);  
                            }
                            else
                            {   
                                $obj = new Gcm($event_id);
                                $msg['title'] =   $noti['title'];
                                $msg['message'] = $noti['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                            $this->event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                        }
                    }
                }
                else
                {
                    if(!empty($notification))
                    {
                        foreach ($notification as $key => $noti) 
                        {
                            $obj = new Gcm($event_id);
                            if(!empty($noti['moduleslink']))
                            {
                                $extra['message_type'] = 'cms';
                                $extra['message_id'] = $noti['moduleslink'];
                            }
                            elseif(!empty($noti['custommoduleslink']))
                            {
                                $extra['message_type'] = 'custom_page';
                                $extra['message_id'] = $noti['custommoduleslink'];
                            }
                            $extra['event'] = $this->settings_model->event_name($event_id);
                            if($value['device'] == "Iphone")
                            {
                                $extra['title'] = $noti['title'];
                                $msg =  $noti['content'];
                                  
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            }
                            else
                            {
                                $msg['title'] =   $noti['title'];
                                $msg['message'] = $noti['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                            $this->event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                        }
                    }
                    
                }
            }
        }
        print_r($result);  
    }
    public function session_notification()
    {
        /*$insert['email'] = 'Session Notification '.date('Y-m-d H:i:s');
        $this->db->insert('tmp_user_session',$insert);*/

        $users = $this->settings_model->get_session_save_Users();
        $result = [];
        foreach ($users as $key => $value) 
        {
            $date = $this->settings_model->getSessionScehduleTime($value['event_id']);
            if($date < $value['notify_datetime'])
                continue;

            $event_id = $value['event_id'];
            if($value['gcm_id']!='')
            {
                $this->settings_model->markAsSendSessionNotification($value['user_id'],$value['Id']);

                $obj = new Gcm($event_id);
                $extra['message_type'] = 'cms';
                $extra['message_id'] = '1';
                $extra['event'] = $this->settings_model->event_name($event_id);
                if($value['device'] == "Iphone")
                {
                    $extra['title'] = $value['title'];
                    $msg =  $value['messages'];
                      
                    $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                }
                else
                {
                    $msg['title'] =   $value['title'];
                    $msg['message'] = $value['messages'];
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                } 
                        //$this->event_model->sendEmailToAttendees($event_id,$noti['content'],$value['Email'],"Notification");
                }
        }
        print_r($result);  
    }

    public function testNotification()
    {

        error_reporting(1);
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $gcm_id = $this->settings_model->getGcmId($user_id);
        $device = $this->settings_model->getDevice($user_id);
        $event_name = $this->settings_model->event_name($event_id);
        $obj  = new Gcm($event_id);
        if($gcm_id == '')
        {
            $result = 'GCM id not available.';
        }
        else
        {
            if($device == "Iphone")
            {
                $msg="Test";
                $extra['I']  = 1; 
                $extra['message_type']  = 'test'; 
                //$extra['event']  = $event_name;
                $extra['title'] = "Test";
                $result      = $obj->send_notification($gcm_id,$msg,$extra,$device);
            }
            else
            {
                $msg['title']           = "VOTE IN THE PROJECT EXHIBITION";
                $msg['message']         = "Don't forget to.";
                $msg['vibrate']         = 1;
                $msg['sound']           = 1;
                $extra['message_id']    = 1; 
                $extra['message_type']  = 'test'; 
                $extra['event']         = $event_name;
                $result                 = $obj->send_notification($gcm_id,$msg,$extra);
            }
        }
        echo json_encode($result);
    }

    public function getAdvertising()
    {
        $event_id   = $this->input->post('event_id');
        $menu_id    = $this->input->post('menu_id');

        if($event_id!='' && $menu_id!='')
        {
            $advertise_data           = $this->settings_model->getAdvertisingData($event_id,$menu_id);
            $advertise_data->H_images = (json_decode($advertise_data->H_images)) ?json_decode($advertise_data->H_images) :[] ;
            $advertise_data->F_images = (json_decode($advertise_data->F_images)) ? json_decode($advertise_data->F_images) : [];
            if($advertise_data)
            {
            $data = array(
                    'success'   => true,
                    'data'      => $advertise_data,
                );
            }
            else
            {
                $data = array(
                    'success'   => true,
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => "Invalid Parameters",
                );
        }
        echo json_encode($data);
    }
     public function fundraisingDonationPayment()
    {
        //error_reporting(1);
        $total              = $this->input->post('total');
        $stripeToken        = $this->input->post('stripeToken');
        $currency           = $this->input->post('currency');
        $admin_secret_key   = $this->input->post('admin_secret_key'); // sk_test_MQdSDqW91LmspHHYRzZ5FNyk

        if( $total==''  || $stripeToken=='' || $currency=='' || $admin_secret_key=='')
        {
             $data = array(
              'success' => false,
              'message' => "Invalid parameters"
             );
             echo json_encode($data);
             exit;
        }
        else
        {
            require($_SERVER['DOCUMENT_ROOT']. '/stripe/init.php');
            try
            {
                \Stripe\Stripe::setApiKey($admin_secret_key); 
                //\Stripe\Stripe::$apiBase = "https://api-tls12.stripe.com";

                
                $payment =  \Stripe\Charge::create(
                      array(
                      "amount"      => $total * 100,
                      "currency"    => strtoupper($currency),
                      "source"      => $stripeToken,
                      "description" => "All In The Loop"
                    )
                );
                
                $data1['transaction_id']=$stripeToken;
                if($payment['status']=="succeeded")
                {
                    $data1['order_status']="completed";
                     $data = array(
                      'success'         => true,
                      'message'         => "completed",
                      'transaction_id'  => $data1['transaction_id'],
                    );
                }
                else
                {
                    $data1['order_status']=$payment['status'];  
                    $data = array(
                      'success'         => false,
                      'message'         => "failed",
                      'transaction_id'  => $data1['order_status'],
                    );
                }
                echo json_encode($data);
            }
            catch(Exception $e)
            {
               $data = array(
                      'success' => false,
                      'message' => "Something went wrong.Please try again.",
                      'error'   => $e,
                    );

                echo json_encode($data);
            }
        }
    }
    public function checkVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ALlInTheLoop';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function userClickBoard()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $exhibitor_page_id = $this->input->post('exhibitor_page_id');
        $sponser_id = $this->input->post('sponser_id');
        $advertise_id = $this->input->post('advertise_id');
        $menu_id = $this->input->post('menu_id');
        $click_type = $this->input->post('click_type');

        if($user_id!='' && $event_id!='' && $click_type!='')
        {
            switch ($click_type) {
                case 'AD':
                        $where['advert_id'] = $advertise_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);

                        $adwhere['event_id'] = $event_id;
                        $adwhere['user_id'] = $user_id;
                        $adwhere['menu_id'] = '5';
                        $adwhere['date'] = date('Y-m-d');
                        $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');

                        break;
                
                case 'SP':
                        $where['sponsor_id'] = $sponser_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);
                        break;
                
                case 'EX':
                        $where['exhibitor_id'] = $exhibitor_page_id;
                        $where['event_id'] = $event_id;
                        $where['user_id'] = $user_id;
                        $where['date'] = date('Y-m-d');
                        $this->settings_model->hitUserClickBoard($where);
                        break;

                case 'OT':
                        $adwhere['event_id'] = $event_id;
                        $adwhere['user_id'] = $user_id;
                        $adwhere['menu_id'] = $menu_id;
                        $adwhere['date'] = date('Y-m-d');
                        $this->settings_model->hitUserLeaderBoard($adwhere,'menu_hit');
                        break;
              
            }
        }

    }

    public function gulfoodVerisonCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Gulfood';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkCoburnsVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Coburns';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkAITLVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'ALlInTheLoop';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkActivateVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Activate';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkWHGVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'WHG';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkAsiabrakeVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Asiabrake';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkShowcaseVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Showcase';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkAndEventsVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = '&Events';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkRealtorsVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Realtors';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkFlemingVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Fleming';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkErsteVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Erste';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function checkSDLVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'SDL';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function getAllApps()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $apps = $this->settings_model->getAllApps($device);
            $data = array(
              'success'  => true,
              'data'  => ($apps) ? $apps : [],
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 

    public function updateVersionCode()
    {
        $device = $this->input->post('device');
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        if($device!='' && $id!='')
        {
            $device = strtolower($device);
            $where['Id'] = $id;
            $update_data[$device."_code"] =  $code;
            $apps = $this->settings_model->updateVersionCode($update_data,$where);
            $data = array(
              'success'  => true,
              'message'  => "Version updated successfully.",
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkEnactusVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Enactus';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function checkMainesVersionCode()
    {
        $device = $this->input->post('device');
        if($device!='')
        {
            $where['app'] = 'Maines';
            $device = strtolower($device);
            $code = $this->settings_model->getVersionCode($where,$device);
            
            $data = array(
              'success'  => true,
              'code'  => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    } 
    public function count_noti()
    {

        $str = "15132,15640,15710,15711,15767,15771,15772,15773,15783,15784,15786,15787,15788,15789,15790,15791,15792,15793,15794,15795,15796,15797,15798,15799,15800,15801,15803,15804,15805,15806,15808,15809,15810,15811,15812,15813,15814,15815,15816,15817,15818,15819,15820,15821,15822,15826,15827,15829,15830,15831,15832,15833,15834,15835,15836,15837,15839,15840,15842,15843,15844,15845,15846,15847,15849,15850,15852,15853,15854,15855,15857,15858,15860,15861,15862,15863,15864,15865,15869,15870,15874,15875,15876,15877,15878,15879,15880,15881,15882,15883,15885,15886,15887,15888,15889,15890,15891,15892,15893,15894,15895,15897,15898,15899,15900,15901,15902,15903,15904,15905,15906,15907,15909,15910,15911,15912,15913,15914,15915,15916,15917,15919,15920,15921,15922,15923,15924,15926,15927,15928,15929,15930,15931,15932,15933,15934,15935,15936,15937,15938,15939,15940,15941,15942,15943,15945,15946,15947,15948,15949,15950,15951,15952,15954,15955,15958,15959,15960,15961,15962,15964,15966,15967,15970,15973,15976,15977,15978,15989,15990,15991,15992,15993,15996,15997,15998,15999,16000,16003,16004,16005,16006,16007,16009,16012,16013,16014,16015,16017,16019,16036,16037,16039,16042,16043,16044,16045,16046,16047,16048,16049,16051,16052,16053,16055,16056,16061";
        $data = explode(',', $str);
        echo count($data);
    }
    public function get_o_screen()
    {
        $event_id = $this->input->post('event_id');
        if($event_id!='')
        {
            
            $data = $this->settings_model->get_onboarding_settings($event_id);
            $data = array(
              'success'  => true,
              'code'  => ($data) ? $data : '',
            );
        }
        else
        {
            $data = array(
              'success'  => false,
              'message'  => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
}