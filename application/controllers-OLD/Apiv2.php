<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('../application/libraries/Rssparser.php');

class Apiv2 extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    public function index( $offset = 0 )
    {
         die("Direct Access Denied..");
    }
    public function add_temp_attendees_ucas()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.eventsforce.net/ucas/api/v2/events/1817/attendees.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpDQjA2RTY3RjE4REE0MDYzQTUxMDVDNTA5MjIwNEIwOA==",
            "Cache-Control: no-cache",
            "Postman-Token: be8d4b3e-99c3-4be8-3ae4-20b9085fd558"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            //echo $response;exit;
            $users = json_decode($response,true);
            $users = $users['data'];
            foreach ($users as $key => $value)
            {    
                if($key < 209):
                    $insert['Firstname'] = $value['firstName'];
                    $insert['Lastname'] = $value['lastName'];
                    $insert['Email'] = $value['email'];
                    $insert['Title'] = $value['jobTitle'];
                    $insert['Salutation'] = $value['title'];
                    $insert['Company_name'] = $value['company'];
                    $insert['Mobile'] = $value['phoneNumber'];
                    $insert['Phone_business'] = $value['workPhoneNumber'];

                    $opts = array(
                      'http'=>array(
                        'method'=>"GET",
                        'header'=>"Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpDQjA2RTY3RjE4REE0MDYzQTUxMDVDNTA5MjIwNEIwOA==",
                            "Cache-Control: no-cache",
                            "Postman-Token: be8d4b3e-99c3-4be8-3ae4-20b9085fd558"
                        )
                    );
                    $context = stream_context_create($opts);
                    $extra_column = json_decode(file_get_contents($value['detailsURL'],false, $context),true)['data'];
                    
                    $insert_extra['Work Phone Number'] = $extra_column['workPhoneNumber'];
                    $insert_extra['Address Line 1'] = $extra_column['addressLine1'];
                    $insert_extra['Address Line 2'] = $extra_column['addressLine2'];
                    $insert_extra['Post Code'] = $extra_column['postCode'];
                    $insert_extra['Town'] = $extra_column['town'];
                    $insert_extra['County'] = $extra_column['county'];
                    $insert_extra['Country'] = $extra_column['country'];
                    
                    $key = array_search('PositionUSE', array_column($extra_column['customData'], 'key'));
                    $insert_extra['PositionUSE'] = $extra_column['customData'][$key]['value'];

                    $key = array_search('School Type UCAS', array_column($extra_column['customData'], 'key'));
                    $insert_extra['School Type UCAS'] = $extra_column['customData'][$key]['value'];

                    $key = array_search('Special Requirements span events', array_column($extra_column['customData'], 'key'));
                    $insert_extra['Special Requirements'] = $extra_column['customData'][$key]['value'];

                    $key = array_search('Mobile Phone Number', array_column($extra_column['customData'], 'key'));
                    $insert_extra['Mobile Phone Number'] = $extra_column['customData'][$key]['value'];

                    $insert['extra_column'] = json_encode($insert_extra);
                     
                    $check_tmp_user = $this->db->where('Email',$value['email'])->get('temp_ucas_users')->row_array();
                    if(empty($check_tmp_user))
                    {
                        $this->db->insert('temp_ucas_users',$insert);
                    }
                    else
                    {   
                        $this->db->where($check_tmp_user);
                        $this->db->update('temp_ucas_users',$insert);
                    }
               endif;
            }
            j('Temp Data Addedd Successfully');         
        }
    }
    public function add_attendees_ucas()
    {   
        $this->load->model('native_single_fcm/app_login_model');
        $temp_ucas_users = $this->db->limit('50')->get('temp_ucas_users')->result_array();
        
        foreach ($temp_ucas_users as $key => $value)
        {  
            $event_id = 996;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();

            $data = $value;
            unset($data['id']);
            unset($data['extra_column']);
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $user_id = $user['Id'];
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
            }
            $event_attendee = $this->db->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get('event_attendee')->row_array();
            if(!empty($event_attendee))
            {
                $update['extra_column'] = $value['extra_column'];
                $this->db->where($event_attendee);
                $this->db->update('event_attendee',$update);
            }
            
            $this->db->where($value);
            $this->db->delete('temp_ucas_users');
        }
        $temp_ucas_users = $this->db->get('temp_ucas_users')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $temp_ucas_users . " Refresh Page -> " . round($temp_ucas_users / 50 ) . " Time";
        j($msg);
    }
    public function add_ipexpo_attendee()
    {
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $event_id = 1511;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function add_ucexpo_attendee()
    {
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $event_id = 1107;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function Euromedicom_Speakers()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/speakers.php?id=244",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            $response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);
            $response = str_replace('&mdash;','<![CDATA[&mdash;]]>',$response);
            $response = str_replace('&nbsp;','<![CDATA[&nbsp;]]>',$response);
            $response = str_replace('&bull;','<![CDATA[&bull;]]>',$response);
            $response = str_replace('&hellip;','<![CDATA[&hellip;]]>',$response);
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xml = json_decode($xmlJson, 1);
            //echo $response;
            /*$response = utf8_encode(html_entity_decode($response));
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);*/
            // j($xml);
            foreach ($xml['Informa']['AMWC_2018']['Speakers']['Speaker']  as $key => $value)
            {   
                // j($value);
                $speaker['tmp_id'] = $value['ID'];
                $speaker['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $speaker['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $speaker['JobTitle'] = empty($value['JobTitle'])? '' :$value['JobTitle'];
                $speaker['Company'] = empty($value['Company'])? '' :$value['Company'];
                $speaker['Biography'] = empty($value['Biography'])? '' :$value['Biography'];
                $speaker['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $speaker['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $speaker['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $speaker['Image'] = empty($value['Image'])? '' :$value['Image'];

                $temp_speaker = $this->db->where('tmp_id',$value['ID'])->get('Euromedicom_Speakers')->row_array();
                if(empty($temp_speaker))
                {
                    $this->db->insert('Euromedicom_Speakers',$speaker);
                }
                else
                {
                    $this->db->where($temp_speaker);
                    $this->db->update('Euromedicom_Speakers',$speaker);
                }
            }
            j('Data inserted successfully');
        }
    }
    public function Euromedicom_Speakers_add()
    {
        $org_id = 32052;
        $eid = 799;
        $temp_speakers=$this->db->select('*')->from('Euromedicom_Speakers')->limit(50)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id'])))->row_array();
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="company_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
                //$user_data['Logo']=NULL;

            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $user_data['updated_date']=date('Y-m-d H:i:s');
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }

            unset($reldata);
            unset($rel_data);

            $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            $social_link_data['Linkedin_url']=$value['Linkedin'];
            $social_link_data['Twitter_url']=$value['Twitter'];
            $social_link_data['Facebook_url']=$value['Facebook'];
            if(empty($sociallinkdata))
            {
                $social_link_data['User_id']=$user_id;
                $social_link_data['Event_id']=$eid;
                $this->db->insert('user_social_links',$social_link_data);
            }
            else
            {
                $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
            }
            unset($sociallinkdata);
            unset($social_link_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Speakers');
            //lq();
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function Euromedicom_Agenda()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/agenda.php?id=244",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $response = utf8_encode(html_entity_decode($response));
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            // j($xml);
            j($xml['Informa']['AMWC_2018']['Sessions']['Session']);
            foreach ($xml['Informa']['AMWC_2018']['Sessions']['Session']  as $key => $value)
            {   
                // j($value);
                $agenda['tmp_id'] = $value['ID'];
                $agenda['StartDate'] = empty($value['StartDate'])? '' :$value['StartDate'];
                $agenda['EndDate'] = empty($value['EndDate'])? '' :$value['EndDate'];
                $agenda['StartTime'] = empty($value['StartTime'])? '' :$value['StartTime'];
                $agenda['EndTime'] = empty($value['EndTime'])? '' :$value['EndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Location'])? '' :$value['Location'];
                $agenda['Description'] = empty($value['Description'])? '' :$value['Description'];
                $agenda['Location'] = empty($value['Location'])? '' :$value['Location'];
                
                if(array_key_exists('Speaker',$value['Speakers']))
                {
                    $temp_speaker = array_filter($value['Speakers']['Speaker']);
                    $temp_speaker = array_filter(array_column($temp_speaker,'ID'));
                    $agenda['Speakers'] = empty($value['Speakers']['Speaker'])? '' :implode(',',$temp_speaker);
                }
                else
                {
                    $agenda['Speakers'] = '';
                }
                
                if(!empty($value['Description']['Topic']))
                {
                    $agenda['Description'] = implode('<br>',$value['Description']['Topic']);
                }

                $tmp_agenda = $this->db->where('tmp_id',$value['ID'])->get('Euromedicom_Agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('Euromedicom_Agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('Euromedicom_Agenda',$agenda);
                }
                $data[] = $agenda;
            }
            j($data);
            j('data Insert Successfully');
        }
    }
    public function Euromedicom_Agenda_add()
    {
        $org_id=32052;
        $Agenda_id=1078;
        $eid=799;
        $temp_session=$this->db->select('*')->from('Euromedicom_Agenda')->limit(50)->get()->result_array();
        // j($temp_session);
        foreach ($temp_session as $key => $value) 
        {
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->get_where('user',array('knect_api_user_id'=>$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=$value['StartDate'];
            $session_data['Start_time']=$value['StartTime'];
            $session_data['End_date']=$value['EndDate'];
            $session_data['End_time']=$value['EndTime'];
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['custom_location']=$value['Location'];
            // j($session_data);
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            elseif(trim($value['Type']) == '')
            {
                $type_id = ($eid == 936) ? 1780 : 1779;
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code','euromedicom-'.trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code','euromedicom-'.trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']='euromedicom-'.trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Agenda');
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function Euromedicom_Sponsors()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/sponsors.php?id=244",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            //echo $response;
            $response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);
            $response = str_replace('&mdash;','<![CDATA[&mdash;]]>',$response);
            $response = str_replace('&nbsp;','<![CDATA[&nbsp;]]>',$response);
            $response = str_replace('&bull;','<![CDATA[&bull;]]>',$response);
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xml = json_decode($xmlJson, 1);
            

            /*$response = utf8_encode(html_entity_decode($response));
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            j($xml);*/
            foreach ($xml['Informa']['AMWC_2018']['Sponsors']['Sponsor']  as $key => $value)
            {   
                $sponsor['tmp_id'] = $value['ID'];
                $sponsor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $sponsor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $sponsor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $sponsor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $sponsor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $sponsor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $sponsor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $sponsor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $tmp_sponsor = $this->db->where('tmp_id',$value['ID'])->get('Euromedicom_Sponsors')->row_array();
                if(empty($tmp_sponsor))
                {
                    $this->db->insert('Euromedicom_Sponsors',$sponsor);
                }
                else
                {   
                    $this->db->where($tmp_sponsor);
                    $this->db->update('Euromedicom_Sponsors',$sponsor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Sponsors_add()
    {
        $event_id = 799;
        $org_id   = 32052;
        $tmp_sponsor=$this->db->select('*')->where('id',218)->from('Euromedicom_Sponsors')->limit(50)->get()->result_array();
        foreach ($tmp_sponsor as $key => $value)
        {
            $check_sponsor = $this->db->where('api_id',$value['tmp_id'])->where('Event_id',$event_id)->get('sponsors')->row_array();
            $data['api_id'] = $value['tmp_id'];
            $data['Company_name'] = $value['Company'];
            $data['Description']  = $value['CompanyDescription'];
            $data['website_url']  = $value['Website'];
            $data['facebook_url'] = $value['Facebook'];
            $data['twitter_url  '] = $value['Twitter'];
            $data['linkedin_url'] = $value['Linkedin'];
            $data['instagram_url'] = $value['Instagram'];
            $data['Event_id'] = $event_id;
            $data['Organisor_id'] = $org_id;

            if (!empty($value['Image']))
            {
                $destination = "company_logo_" . uniqid();
                $source = $value['Image'];
                $info = getimagesize($source);
                if ($info['mime'] == 'image/jpeg')
                {
                    $logo = $destination . '.jpeg';
                    $image = imagecreatefromjpeg($source);
                    imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                }
                elseif ($info['mime'] == 'image/gif')
                {
                    $logo = $destination . '.gif';
                    $image = imagecreatefromgif($source);
                    imagegif($image, "./assets/user_files/" . $destination . '.gif');
                }
                elseif ($info['mime'] == 'image/png')
                {
                    $logo = $destination . '.png';
                    $image = imagecreatefrompng($source);
                    $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                    imagecolortransparent($image, $background);
                    imagealphablending($image, false);
                    imagesavealpha($image, true);
                    imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                }
                $data['company_logo'] = json_encode(array(
                    $logo
                ));
            }
            if(empty($check_sponsor))
            {
                $this->db->insert('sponsors',$data);
            }
            else
            {
                $this->db->where($check_sponsor);
                $this->db->update('sponsors',$data);
            }
            unset($data);
             $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Sponsors');
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Sponsors')->get()->num_rows();
        echo "Success -> Remaning Sponsor. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('Sponsor added  successfully');
    }
    public function Euromedicom_Exhi()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/exhibitors.php?id=244",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            
            $response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);
            $response = str_replace('&mdash;','<![CDATA[&mdash;]]>',$response);
            $response = str_replace('&nbsp;','<![CDATA[&nbsp;]]>',$response);
            $response = str_replace('&bull;','<![CDATA[&bull;]]>',$response);
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            // j($xmlArr);
            
            foreach ($xmlArr['Informa']['AMWC_2018']['Exhibitors']['Exhibitor']  as $key => $value)
            {   
                $Exhibitor['tmp_id'] = 'Euromedicom-'.$value['ID'];
                $Exhibitor['Email'] = empty($value['Email'])? 'Euromedicom-'.$value['ID'].'@venturiapps.com' :$value['Email'];
                $Exhibitor['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $Exhibitor['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $Exhibitor['StandNumber'] = empty($value['StandNumber'])? '' :$value['StandNumber'];
                $Exhibitor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $Exhibitor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $Exhibitor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $Exhibitor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $Exhibitor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $Exhibitor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $Exhibitor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $Exhibitor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $Tmp_Exhibitor = $this->db->where('tmp_id','Euromedicom-'.$value['ID'])->get('Euromedicom_Exhibitors')->row_array();
                $Exhibitor['CompanyDescription'] =$Exhibitor['CompanyDescription'];
                $data[] = $Exhibitor;
                if(empty($Tmp_Exhibitor))
                {
                    $this->db->insert('Euromedicom_Exhibitors',$Exhibitor);
                }
                else
                {   
                    $this->db->where($Tmp_Exhibitor);
                    $this->db->update('Euromedicom_Exhibitors',$Exhibitor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Exhi_add($value='')
    {   
        //error_reporting(E_ALL);
        // die("Direct Access Denied..");
        $oid = '32052';
        $eid = '799';
        $temp_exibitor = $this->db->select('*')->from('Euromedicom_Exhibitors')->limit('50')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['LastName'];
                $user_data['Company_name'] = $value['Company'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Company']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Company'];
                $ebi_data['Description'] = $value['CompanyDescription'];
                $ebi_data['stand_number'] = $value['StandNumber'];

                if (!empty($value['Image']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['Image'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['Website'];
                $ebi_data['facebook_url'] = $value['Facebook'];
                $ebi_data['linkedin_url'] = $value['Linkedin'];
                $ebi_data['instagram_url'] = $value['Instagram'];
                $ebi_data['twitter_url'] = $value['Twitter'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Company']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('Euromedicom_Exhibitors');
        }
        echo "Success";
        die;
    }
    public function add_medlab_attendee($page_no)
    {   

        $this->load->model('native_single_fcm/app_login_model');
        $event_id = 1012;
        $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
        //j($page_no);
        switch ($page_no)
        {
            case '1':
                $temp_attendees = $this->db->limit(50)->get('medlab_tmp_data')->result_array();
                break;
            case '2':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,150)->result_array();
                break;
            case '3':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,250)->result_array();
                break;
            case '4':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,350)->result_array();
                break;
            case '5':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,450)->result_array();
                break;
            case '6':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,550)->result_array();
                break;
            case '7':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,650)->result_array();
                break;
            case '8':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,750)->result_array();
                break;
            case '9':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,850)->result_array();
                break;
            case '10':
                $temp_attendees = $this->db->get('medlab_tmp_data',50,950)->result_array();
                break;
        }        
        foreach ($temp_attendees as $key => $value)
        {

            $check_unique = $this->db->where('Unique_no',$value['unique_no'])->where('Email',$value['Email'])->get('user')->row_array();
            
            if(!$check_unique)
            {   
                $check_unique = $this->db->where('Unique_no',$value['unique_no'])->get('user')->row_array();
                if($check_unique)
                {
                    $data1 = array(
                        'success' => false,
                        'message' => 'Unique no is already exists with another email',
                        'data' => $value
                    );
                    echo json_encode($data1);exit;
                }
            }
            
            $user = $check_unique;
            
            $data['Company_name'] = $value['CompanyName'];
            $data['Firstname'] = $value['FirstName'];
            $data['Lastname'] = $value['Surname'];
            $data['Email'] = $value['Email'];
            $data['Street'] = $value['Street'];
            $data['Suburb'] = $value['Suburb'];
            $data['State'] = $value['State'];
            $country_id = $this->db->where('country_name',$value['Country'])->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$value['Country'];
            $data['Postcode'] = $value['PostCode'];
            $data['Mobile'] = $value['MobileArea'].$value['MobileNumber'];
            $data['Phone_business'] = $value['TelephoneArea'].$value['Telephone'];
            $data['Salutation'] = $value['Position'];
            $data['Title'] = $value['Title'];
            $data['Unique_no'] = $value['unique_no'];
            $data['barcodes'] = $value['Barcode_list'];
            // j($data);
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$value['Email']);
                $this->db->where('Unique_no',$value['unique_no']);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->check_email_and_unique_no_by_event($value['Email'],$value['unique_no'],$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data1[] = array(
                    'success' => true,
                    'message' => 'User Info updated',
                    'data' => $user
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data1[] = array(
                    'success' => true,
                    'message' => 'User Info added',
                    'data' => $value
                );
            }
            $this->db->where($value);
            $this->db->delete('medlab_tmp_data');
            unset($user);
            unset($data);
        }
        j($data1);
    }
    public function assign_ucas_agenda()
    {   
        //error_reporting(E_ALL);
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('Ucas_user_session_temp uct','uct.email = u.Email');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id','996');
        $this->db->group_by('u.Id');
        $this->db->limit(1);
        $res = $this->db->get()->result_array();
        // j($res);
        foreach ($res as $key => $value)
        {
            $tmp_user_agenda = $this->db->where('user_id',$value['Id'])->get('users_agenda')->row_array();
            if($tmp_user_agenda)
            {
                $tmp_agenda = explode(',',$tmp_user_agenda['agenda_id']);
                $new_agenda  = explode(',',$value['agenda_id']);
                $fin_agenda = array_merge($tmp_agenda,$new_agenda);
                $this->db->where($tmp_user_agenda);
                $update['agenda_id'] = implode(',',array_unique($fin_agenda));
                $this->db->update('users_agenda',$update);
            }   
            else
            {
                $insert['user_id'] = $value['Id'];
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['updated_date'] = date('Y-m-d H:i:s');
                $this->db->insert('users_agenda',$insert);
            }
            $this->db->where('email',$value['email']);
            $this->db->delete('Ucas_user_session_temp');
        }
        j('data added successfully');
    }
    public function arab_fill_lead_firstname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','634');
        $this->db->where('(el.firstname = "" || el.firstname IS NULL || el.firstname = "(null)")');
        $this->db->where('(u.Firstname != "" && u.Firstname IS NOT NULL && u.Firstname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }

    public function arab_fill_lead_email()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','634');
        $this->db->where('(el.email = "" || el.email IS NULL || el.email = "(null)")');
        $this->db->where('(u.Email != "" && u.Email IS NOT NULL && u.Email != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->get('exibitor_lead el')->result_array();
        // j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
            unset($update);
        }
        j($data);
    }

    public function arab_fill_lead_lastname()
    {
        $this->db->select('u.*,el.*,el.exibitor_lead_id as update_lead_id');
        $this->db->where('el.event_id','634');
        $this->db->where('(el.lastname = "" || el.lastname IS NULL || el.lastname = "(null)")');
        $this->db->where('(u.Lastname != "" && u.Lastname IS NOT NULL && u.Lastname != "(null)")');
        $this->db->where('u.Unique_no IS NOT NULL');
        $this->db->join('user u','u.Id = el.lead_user_id');
        $res = $this->db->limit(350)->get('exibitor_lead el')->result_array();
        //lq();
        //j($res);
        foreach ($res as $key => $value)
        {   
            $update['firstname'] = !empty($value['firstname']) ? $value['firstname'] : $value['Firstname'];
            $update['lastname'] = !empty($value['lastname']) ? $value['lastname'] : $value['Lastname'];
            $update['email'] = !empty($value['email']) ? $value['email'] : $value['Email'];
            $update['title'] = !empty($value['title']) ? $value['title'] : $value['Title'];
            $update['company_name'] = !empty($value['company_name']) ? $value['company_name'] : $value['Company_name'];
            $update['salutation'] = !empty($value['salutation']) ? $value['salutation'] : $value['Salutation'];
            $tmp_country = $this->db->where('id',$value['Country'])->get('country')->row_array();
            $update['country'] = !empty($value['country']) ? $value['country'] : $tmp_country['country_name'];
            $update['mobile'] = !empty($value['mobile']) ? $value['mobile'] : $value['Mobile'];
            $update['badgeNumber'] = $value['Unique_no'];
            $this->db->where('exibitor_lead_id',$value['update_lead_id']);
            $this->db->update('exibitor_lead',$update);
            $data[] = $update;
        }
        j($data);
    }
    public function cityscape_temp_exhi()
    {   
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/catalogue/web/PUX18CS/a1VrMy9ZQzdkRHErb2xzNjhHV0NKTy9ZcGRhNG9ZcnZNdG4rTzNPZEdlaz01/.%2A/all/all/.%2A/all/false",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
                "Postman-Token: 789ab832-f5a2-122a-0d37-b5d9ae10edf8"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $data = json_decode($response,true);
            // j($data);
            foreach ($data as $key => $value)
            {
                $tmp_exhi['type'] = $value['entry'];
                $tmp_exhi['exhibitorguid'] = $value['exhibitorguid'].'-1112@venturiapps.com';
                $tmp_exhi['stand'] = $value['stand'];
                $tmp_exhi['logo'] = $value['logothumb'];
                $tmp_exhi['companyname'] = $value['companyname'];
                $coun_id = $this->db->select('*')->from('country')->where('country_name', trim($value['country']))->get()->row_array();
                $tmp_exhi['country'] = $coun_id['id'];
                $tmp_exhi['website'] = $value['website'];
                
                $tmp_exhi['products'] = implode(',',$value['products']);
                $tmp_exhi['email'] = $value['email'];
                $tmp_exhi['phone'] = $value['phone'];
                $tmp_exhi['contactperson'] = $value['contactperson'];
                $tmp_exhi['link_facebook'] = $value['link_facebook'];
                $tmp_exhi['link_twitter'] = $value['link_twitter'];
                $tmp_exhi['link_linkedin'] = $value['link_linkedin'];
                $tmp_exhi['link_youtube'] = $value['link_youtube'];
                $tmp_exhi['link_instagram'] = $value['0'];
                $products = implode(',',$value['products']);
                if(!empty($products))
                {
                    $products = implode(',',$value['products']).',';
                }
                else
                {
                    $products = implode(',',$value['products']);
                }
                $tmp_exhi['keywords'] = $products.$value['country'];
                $tmp_exhi['description'] = $value['description'].'<br><br>';
                if(!empty($value['description']))
                $tmp_exhi['description'] .= 'Email: '.str_replace(',','<br>',$value['email']).'<br>';
                else
                $tmp_exhi['description']  = 'Email: '.str_replace(',','<br>',$value['email']).'<br>';

                //j($tmp_exhi);
                $this->db->insert('cityscape_temp_exhi',$tmp_exhi);
                unset($tmp_exhi);
                unset($coun_id);
            }
            echo "tmp exhi added";
        }
    }
    public function add_cityscape_exhi()
    {
        // die("Direct Access Denied..");
        date_default_timezone_set('GMT');
        set_time_limit(0);
        $eid = '1112';
        $this->load->model('native_single_fcm/app_login_model');
        $oid = $this->app_login_model->getOrganizerByEvent($eid);
        $temp_exibitor = $this->db->limit('50')->get('cityscape_temp_exhi')->result_array();
        foreach($temp_exibitor as $key => $value)
        {   
            if (!empty(trim($value['exhibitorguid'])))
            {
                $user_data['Firstname'] = $value['contactperson'];
                $user_data['Company_name'] = $value['companyname'];
                $user_data['Mobile'] = $value['phone'];
                $user_data['Country'] = $value['country'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['exhibitorguid']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['exhibitorguid']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['exhibitorguid']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['exhibitorguid'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['companyname'];
                $ebi_data['Description'] = $value['description'];
                $ebi_data['stand_number'] = $value['stand'];
                if (!empty($value['logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['website'];
                $ebi_data['facebook_url'] = $value['link_facebook'];
                $ebi_data['youtube_url'] = $value['link_youtube'];
                $ebi_data['linkedin_url'] = $value['link_linkedin'];
                $ebi_data['instagram_url'] = $value['link_instagram'];
                $ebi_data['twitter_url'] = $value['link_twitter'];
                $ebi_data['Short_desc'] = $value['keywords'];
                $ebi_data['country_id'] = $value['country'];
                $ebi_data['email_address'] = $value['email'];
                $ebi_data['Description'] = $value['description'];
                if($value['type'] == 'Gold')
                {
                    $ebi_data['et_id'] = '179';
                }
                else if($value['type'] == 'Bronze')
                {
                    $ebi_data['et_id'] = '180';
                }
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['companyname']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('exhibitorguid', $value['exhibitorguid']);
            $this->db->delete('cityscape_temp_exhi');
            // lq();
        }
        $total_exhi = $this->db->select()->from('cityscape_temp_exhi')->get()->num_rows();
        echo "Success -> Remaning Exhi. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        /*if(!empty($total_exhi))
            return $this->add_arab_helth_exhi();
        else
            echo "all exhi added";*/
        die;
    }
    public function fha_exhi_upload($page_no)
    {   
        error_reporting(1);
        ini_set('display_errors','1');
        ini_set('memory_limit','-1');
        $event_id = '585';

        switch ($page_no)
        {
            case 1:
                $tmp_exhi = $this->db->limit(10)->get('fha_tmp_exhi')->result_array();
                break;            
            case 2:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,30)->result_array();
                break;
            case 3:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,45)->result_array();
                break;
            case 4:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,60)->result_array();
                break;
            case 5:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,75)->result_array();
                break;
            case 6:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,90)->result_array();
                break;            
            case 7:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,105)->result_array();
                break;
            case 8:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,120)->result_array();
                break;
            case 9:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,135)->result_array();
                break;
            case 10:
                $tmp_exhi = $this->db->get('fha_tmp_exhi',15,150)->result_array();
                break;
        }
        // print_r($tmp_exhi);exit;
        //$tmp_exhi = $this->db->limit(15)->get('fha_tmp_exhi')->result_array();
        $this->load->model('exibitor_model');
        $this->load->model('event_model');
        $event = $this->event_model->get_admin_event($event_id);
        $id = $event_id;
        foreach ($tmp_exhi as $key1=> $value1)
        {   
            $datacsv = array_values($value1);
            if (!empty($datacsv[2]))
            {
                $ex_user['Firstname'] = $datacsv[0];
                $ex_user['Lastname'] = $datacsv[1];
                $ex_user['Email'] = $datacsv[2];
                $ex_user['Password'] = md5($datacsv[3]);
                $ex_user['Company_name'] = $datacsv[4];
                $ex_user['Organisor_id'] = $event[0]['Organisor_id'];
                $ex_user['Created_date'] = date('Y-m-d H:i:s');
                $ex_user['Active'] = '1';
                $ex_user['updated_date'] = date('Y-m-d H:i:s');
                $user_id = $this->exibitor_model->add_user_from_csv($ex_user, $id);
                $ex_data['Organisor_id'] = $event[0]['Organisor_id'];
                $ex_data['Event_id'] = $id;
                $ex_data['Heading'] = $datacsv[4];
                if (!empty($datacsv[5]))
                {
                    $et_id = $this->exibitor_model->get_exibitor_type_id_by_type_code($datacsv[5], $id);
                    $ex_data['et_id'] = $et_id;
                }
                $ex_data['stand_number'] = $datacsv[6];
                $ex_data['Short_desc'] = $datacsv[7];
                $ex_data['Description'] = htmlentities(str_replace('Singapore Expo',' Singapore Expo',$datacsv[8]));
                $ex_data['website_url'] = $datacsv[9];
                $ex_data['facebook_url'] = $datacsv[10];
                $ex_data['twitter_url'] = $datacsv[11];
                $ex_data['linkedin_url'] = $datacsv[12];
                $ex_data['instagram_url'] = $datacsv[13];
                $ex_data['youtube_url'] = $datacsv[14];
                $ex_data['country_id'] = $this->exibitor_model->getCountryIdFromName($datacsv[17]);
                if (!empty($datacsv[15]))
                {
                    /*$logo="company_logo_".uniqid().'.png';
                    copy($datacsv[15],"./assets/user_files/".$logo);
                    $ex_data['company_logo']=json_encode(array($logo));*/
                    $destination = "new_company_logo_" . uniqid();
                    $source = str_replace(' ', '%20', $datacsv[15]);
                    $info = getimagesize($source);
                    $img_size = get_headers($source, '1');
                    $img_size = $img_size['Content-Length'] / 1000;
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $quality = ($img_size > '1000') ? '50' : '100';
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $quality = ($img_size > '1000') ? '5' : '9';
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', $quality);
                    }
                    $ex_data['company_logo'] = json_encode(array($logo));
                }
                if (!empty($datacsv[16]))
                {   
                    $img_arr = array_filter(explode(",", $datacsv[16]));
                    $img_nm = array();
                    foreach($img_arr as $key => $value)
                    {
                        $images = "banner_" . uniqid() . '.png';
                        $img_nm[$key] = $images;
                        copy($value, "./assets/user_files/" . $images);
                    }
                    $ex_data['Images'] = json_encode($img_nm);
                }
                $ex_data['user_id'] = $user_id;
                $ex_id = $this->exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
            }
            
            $this->db->where($value1)->delete('fha_tmp_exhi');
            unset($datacsv);
            $data[] =  $ex_data;
            unset($ex_data);
        }
        j($data);
    }
    public function connectech_asia_exhi_upload($page_no=1)
    {   
        error_reporting(1);
        ini_set('display_errors','1');
        ini_set('memory_limit','-1');
        $event_id = '870';
            $this->db->reconnect();

        switch ($page_no)
        {
            case 1:
                $tmp_exhi = $this->db->limit(10)->get('connectech_asia_exhi')->result_array();
                break;            
            case 2:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,20)->result_array();
                break;
            case 3:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,30)->result_array();
                break;
            case 4:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,40)->result_array();
                break;
            case 5:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,50)->result_array();
                break;
            case 6:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,90)->result_array();
                break;            
            case 7:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,105)->result_array();
                break;
            case 8:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,120)->result_array();
                break;
            case 9:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,135)->result_array();
                break;
            case 10:
                $tmp_exhi = $this->db->get('connectech_asia_exhi',10,150)->result_array();
                break;
        }
        // print_r($tmp_exhi);exit;
        //$tmp_exhi = $this->db->limit(15)->get('fha_tmp_exhi')->result_array();
        $this->load->model('exibitor_model');
        $this->load->model('event_model');
        $event = $this->event_model->get_admin_event($event_id);
        $id = $event_id;
        foreach ($tmp_exhi as $key1=> $value1)
        {   
            $datacsv = array_values($value1);
            if (!empty($datacsv[2]))
            {
                $ex_user['Firstname'] = $datacsv[0];
                $ex_user['Lastname'] = $datacsv[1];
                $ex_user['Email'] = $datacsv[2];
                $ex_user['Password'] = md5($datacsv[3]);
                $ex_user['Company_name'] = $datacsv[4];
                $ex_user['Organisor_id'] = $event[0]['Organisor_id'];
                $ex_user['Created_date'] = date('Y-m-d H:i:s');
                $ex_user['Active'] = '1';
                $ex_user['updated_date'] = date('Y-m-d H:i:s');
                $user_id = $this->exibitor_model->add_user_from_csv($ex_user, $id);
                $ex_data['Organisor_id'] = $event[0]['Organisor_id'];
                $ex_data['Event_id'] = $id;
                $ex_data['Heading'] = $datacsv[4];
                if (!empty($datacsv[5]))
                {
                    $et_id = $this->exibitor_model->get_exibitor_type_id_by_type_code($datacsv[5], $id);
                    $ex_data['et_id'] = $et_id;
                }
                $ex_data['stand_number'] = $datacsv[6];
                $ex_data['Short_desc'] = $datacsv[7];
                $ex_data['Description'] = htmlentities($datacsv[8]);
                $ex_data['website_url'] = $datacsv[9];
                $ex_data['facebook_url'] = $datacsv[10];
                $ex_data['twitter_url'] = $datacsv[11];
                $ex_data['linkedin_url'] = $datacsv[12];
                $ex_data['instagram_url'] = $datacsv[13];
                $ex_data['youtube_url'] = $datacsv[14];
                $ex_data['country_id'] = $this->exibitor_model->getCountryIdFromName($datacsv[17]);
                
                $destination = "new_company_logo_" . uniqid();
                $source = str_replace(' ', '%20', $datacsv[15]);
                $info = getimagesize($source);
                
                if (!empty($datacsv[15]) && !empty($info))
                {
                    /*$logo="company_logo_".uniqid().'.png';
                    copy($datacsv[15],"./assets/user_files/".$logo);
                    $ex_data['company_logo']=json_encode(array($logo));*/
                    $img_size = get_headers($source, '1');
                    $img_size = $img_size['Content-Length'] / 1000;
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $quality = ($img_size > '1000') ? '50' : '100';
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $quality = ($img_size > '1000') ? '5' : '9';
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', $quality);
                    }
                    $ex_data['company_logo'] = json_encode(array($logo));
                }
                if (!empty($datacsv[16]))
                {   
                    $img_arr = array_filter(explode(",", $datacsv[16]));
                    $img_nm = array();
                    foreach($img_arr as $key => $value)
                    {
                        $images = "banner_" . uniqid() . '.png';
                        $img_nm[$key] = $images;
                        copy($value, "./assets/user_files/" . $images);
                    }
                    $ex_data['Images'] = json_encode($img_nm);
                }
                $ex_data['user_id'] = $user_id;
                $ex_id = $this->exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
            }
            
            $this->db->where($value1)->delete('connectech_asia_exhi');
            unset($datacsv);
            $data[] =  $ex_data;
            unset($ex_data);
        }
        j($data);
    }
    public function ipexpo_speaker()
    {
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ipexpomanchester.com/feeds/MobileSpeakers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            

            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            //j($xmlArr);
            foreach ($xmlArr['Speaker']  as $key => $value)
            {   
                // j($value);
                $speaker['tmp_id'] = $value['SpeakerID'].'-ipexpo';
                $speaker['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $speaker['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $speaker['JobTitle'] = empty($value['JobTitle'])? '' :$value['JobTitle'];
                $speaker['Company'] = empty($value['Company'])? '' :$value['Company'];
                $speaker['Biography'] = (is_array($value['Description']))? '' :$value['Description'];
                $speaker['Website'] = empty($value['Website'])? '' :$value['Website'];
                $speaker['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $speaker['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $speaker['Image'] = empty($value['Photo'])? '' :$value['Photo'];

                $temp_speaker = $this->db->where('tmp_id',$value['SpeakerID'].'-ipexpo')->get('temp_ipexpo_Speakers')->row_array();
                if(empty($temp_speaker))
                {
                    $this->db->insert('temp_ipexpo_Speakers',$speaker);
                }
                else
                {
                    $this->db->where($temp_speaker);
                    $this->db->update('temp_ipexpo_Speakers',$speaker);
                }
            }
            j('data inserted successfully');
        }
    }
    public function ipexpo_speaker_add()
    {
        $org_id = 48217;
        $eid = 992;
        $temp_speakers=$this->db->select('*')->from('temp_ipexpo_Speakers')->limit(50)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id'])))->row_array();
            
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="company_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);


            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }

            unset($reldata);
            unset($rel_data);

            $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            $social_link_data['Linkedin_url']=$value['Linkedin'];
            $social_link_data['Twitter_url']=$value['Twitter'];
            $social_link_data['Website_url']=$value['Website'];
            if(empty($sociallinkdata))
            {
                $social_link_data['User_id']=$user_id;
                $social_link_data['Event_id']=$eid;
                $this->db->insert('user_social_links',$social_link_data);
            }
            else
            {
                $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
            }
            unset($sociallinkdata);
            unset($social_link_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ipexpo_Speakers');
            //lq();
        }
        $total_exhi = $this->db->select()->from('temp_ipexpo_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function ipexpo_agenda()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ipexpomanchester.com/feeds/MobileSeminars",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            // j($xmlArr);

            foreach ($xmlArr['Seminar'] as $key => $value)
            {   
                // j($value);
                $agenda['tmp_id'] = $value['SeminarID'].'-ipexpo';
                $agenda['StartDate'] = empty($value['StartDate'])? '' :$value['StartDate'];
                $agenda['EndDate'] = empty($value['EndDate'])? '' :$value['EndDate'];
                $agenda['StartTime'] = empty($value['StartTime'])? '' :$value['StartTime'];
                $agenda['EndTime'] = empty($value['EndTime'])? '' :$value['EndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Theatre'])? '' :$value['Theatre'];
                if(!is_array($value['Description']))
                $agenda['Description'] = empty($value['Description']) ? '' :$value['Description'];
                
                if(array_key_exists('SpeakerID',$value['Speakers']))
                {   
                    if(is_array($value['Speakers']['SpeakerID']))
                    {
                        $agenda['Speakers'] = empty($value['Speakers']['SpeakerID'])? '' :implode(',',array_filter($value['Speakers']['SpeakerID']));
                    }
                    else
                    {   
                        
                            $agenda['Speakers'] = $value['Speakers']['SpeakerID'];
                    }
                }
                else
                {
                    $agenda['Speakers'] = NULL;
                }

                $tmp_agenda = $this->db->where('tmp_id',$value['SeminarID'].'-ipexpo')->get('temp_ipexpo_agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('temp_ipexpo_agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('temp_ipexpo_agenda',$agenda);
                }
                $data[] = $agenda;
                unset($agenda);
            }
            j($data);
            j('data Insert Successfully');
        }
    }
    public function ipexpo_agenda_add()
    {
        $org_id=48217;
        $Agenda_id=1114;
        $eid=992;
        $temp_session=$this->db->select('*')->from('temp_ipexpo_agenda')->limit(50)->get()->result_array();
        // j($temp_session);
        foreach ($temp_session as $key => $value) 
        {
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->get_where('user',array('knect_api_user_id'=>$svalue.'-ipexpo'))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            
            $session_data['Start_date']=DateTime::createFromFormat('d/m/y',$value['StartDate'])->format('Y-m-d');
            $session_data['Start_time']=date('H:i:s',strtotime($value['StartTime']));
            $session_data['End_date']=DateTime::createFromFormat('d/m/y',$value['EndDate'])->format('Y-m-d');
            $session_data['End_time']=date('H:i:s',strtotime($value['EndTime']));
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['allow_clashing'] = '1';
            $session_data['show_checking_in'] = '1';
            $session_data['show_rating'] = '1';
            $session_data['allow_comments'] = '1';

            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            elseif(trim($value['Type']) == '')
            {
                $type_id = ($eid == 936) ? 1780 : 1779;
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            $data[] = $session_data;
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ipexpo_agenda');
        }
        j($data);
        $total_exhi = $this->db->select()->from('temp_ipexpo_agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function ucexpo_speaker()
    {
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ucexpo.co.uk/feeds/MobileSpeakers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            

            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            // j($xmlArr);
            foreach ($xmlArr['Speaker']  as $key => $value)
            {   
                //j($value);
                $speaker['tmp_id'] = $value['SpeakerID'].'-ucexpo';
                $speaker['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $speaker['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $speaker['JobTitle'] = empty($value['JobTitle'])? '' :$value['JobTitle'];
                $speaker['Company'] = empty($value['Company'])? '' :$value['Company'];
                $speaker['Biography'] = (is_array($value['Description']))? '' :$value['Description'];
                $speaker['Website'] = empty($value['Website'])? '' :$value['Website'];
                $speaker['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $speaker['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $speaker['Image'] = empty($value['Photo'])? '' :$value['Photo'];

                $temp_speaker = $this->db->where('tmp_id',$value['SpeakerID'].'-ucexpo')->get('temp_ucexpo_Speakers')->row_array();
                if(empty($temp_speaker))
                {
                    $this->db->insert('temp_ucexpo_Speakers',$speaker);
                }
                else
                {
                    $this->db->where($temp_speaker);
                    $this->db->update('temp_ucexpo_Speakers',$speaker);
                }
            }
            j('data inserted successfully');
        }
    }
    public function ucexpo_speaker_add()
    {
        $org_id = 48217;
        $eid = 1107;
        $temp_speakers=$this->db->select('*')->from('temp_ucexpo_Speakers')->limit(50)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id'])))->row_array();
            
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="company_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);


            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }

            unset($reldata);
            unset($rel_data);

            $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            $social_link_data['Linkedin_url']=$value['Linkedin'];
            $social_link_data['Twitter_url']=$value['Twitter'];
            $social_link_data['Website_url']=$value['Website'];
            if(empty($sociallinkdata))
            {
                $social_link_data['User_id']=$user_id;
                $social_link_data['Event_id']=$eid;
                $this->db->insert('user_social_links',$social_link_data);
            }
            else
            {
                $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
            }
            unset($sociallinkdata);
            unset($social_link_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ucexpo_Speakers');
            //lq();
        }
        $total_exhi = $this->db->select()->from('temp_ucexpo_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function ucexpo_agenda()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.ucexpo.co.uk/feeds/MobileSeminars",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            // j($xmlArr);

            foreach ($xmlArr['Seminar'] as $key => $value)
            {   
                // j($value);
                $agenda['tmp_id'] = $value['SeminarID'].'-ucexpo';
                $agenda['StartDate'] = empty($value['StartDate'])? '' :$value['StartDate'];
                $agenda['EndDate'] = empty($value['EndDate'])? '' :$value['EndDate'];
                $agenda['StartTime'] = empty($value['StartTime'])? '' :$value['StartTime'];
                $agenda['EndTime'] = empty($value['EndTime'])? '' :$value['EndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Theatre'])? '' :$value['Theatre'];
                if(!is_array($value['Description']))
                $agenda['Description'] = empty($value['Description']) ? '' :$value['Description'];
                
                if(array_key_exists('SpeakerID',$value['Speakers']))
                {   
                    if(is_array($value['Speakers']['SpeakerID']))
                    {
                        $agenda['Speakers'] = empty($value['Speakers']['SpeakerID'])? '' :implode(',',array_filter($value['Speakers']['SpeakerID']));
                    }
                    else
                    {   
                        
                            $agenda['Speakers'] = $value['Speakers']['SpeakerID'];
                    }
                }
                else
                {
                    $agenda['Speakers'] = NULL;
                }

                $tmp_agenda = $this->db->where('tmp_id',$value['SeminarID'].'-ucexpo')->get('temp_ucexpo_agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('temp_ucexpo_agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('temp_ucexpo_agenda',$agenda);
                }
                $data[] = $agenda;
                unset($agenda);
            }
            j($data);
            j('data Insert Successfully');
        }
    }
    public function ucexpo_agenda_add()
    {
        $org_id=48217;
        $Agenda_id=1116;
        $eid=1107;
        $temp_session=$this->db->select('*')->from('temp_ucexpo_agenda')->limit(50)->get()->result_array();
        // j($temp_session);
        foreach ($temp_session as $key => $value) 
        {
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->get_where('user',array('knect_api_user_id'=>$svalue.'-ucexpo'))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            
            $session_data['Start_date']=DateTime::createFromFormat('d/m/y',$value['StartDate'])->format('Y-m-d');
            $session_data['Start_time']=date('H:i:s',strtotime($value['StartTime']));
            $session_data['End_date']=DateTime::createFromFormat('d/m/y',$value['EndDate'])->format('Y-m-d');
            $session_data['End_time']=date('H:i:s',strtotime($value['EndTime']));
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['allow_clashing'] = '1';
            $session_data['show_checking_in'] = '1';
            $session_data['show_rating'] = '1';
            $session_data['allow_comments'] = '1';

            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            elseif(trim($value['Type']) == '')
            {
                $type_id = ($eid == 936) ? 1780 : 1779;
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            $data[] = $session_data;
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('temp_ucexpo_agenda');
        }
        j($data);
        $total_exhi = $this->db->select()->from('temp_ucexpo_agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function Euromedicom_Speakers_MCA()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/speakers.php?id=260",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            //echo $response;
            // $response = utf8_encode(html_entity_decode($response));
            $response = html_entity_decode($response);
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            //LIVE_PLASTIC_SURGERY_-_STATE_OF_THE_ART 
            //MCA_2018
            foreach ($xml['Informa']['MCA_2018']['Speakers']['Speaker']  as $key => $value)
            {   
                $speaker['tmp_id'] = 'euromedicom-mca-'.$value['ID'];
                $speaker['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $speaker['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $speaker['JobTitle'] = empty($value['JobTitle'])? '' :$value['JobTitle'];
                $speaker['Company'] = empty($value['Company'])? '' :$value['Company'];
                $speaker['Biography'] = empty($value['Biography'])? '' :$value['Biography'];
                $speaker['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $speaker['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $speaker['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $speaker['Image'] = empty($value['Image'])? '' :$value['Image'];
                
                
                $temp_speaker = $this->db->where('tmp_id','euromedicom-mca-'.$value['ID'])->get('Euromedicom_Speakers')->row_array();

                if(empty($temp_speaker))
                {
                    $this->db->insert('Euromedicom_Speakers',$speaker);
                }
                else
                {
                    $this->db->where($temp_speaker);
                    $this->db->update('Euromedicom_Speakers',$speaker);
                }
            }
            j('Data inserted successfully');
        }
    }
    public function Euromedicom_Speakers_add_MCA()
    {
        $org_id = 32052;
        $eid = 1045;
        $temp_speakers=$this->db->select('*')->from('Euromedicom_Speakers')->limit(50)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id'])))->row_array();
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Email'] = $value['FirstName'].'-'.$value['LastName'].'-MCA_2018-'.uniqid().'@venturiapps.com';
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            $user_data['is_from_api'] = '1';
            if(!empty($value['Image']))
            {   
                show_errors();
                $logoname="company_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
                // $user_data['Logo']=NULL;

            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $user_data['updated_date']=date('Y-m-d H:i:s');
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }

            unset($reldata);
            unset($rel_data);

            $sociallinkdata=$this->db->get_where('user_social_links',array('Event_id'=>$eid,'User_id'=>$user_id))->row_array();
            $social_link_data['Linkedin_url']=$value['Linkedin'];
            $social_link_data['Twitter_url']=$value['Twitter'];
            $social_link_data['Facebook_url']=$value['Facebook'];
            if(empty($sociallinkdata))
            {
                $social_link_data['User_id']=$user_id;
                $social_link_data['Event_id']=$eid;
                $this->db->insert('user_social_links',$social_link_data);
            }
            else
            {
                $this->db->where(array('Event_id'=>$eid,'User_id'=>$user_id))->update('user_social_links',$social_link_data);
            }
            unset($sociallinkdata);
            unset($social_link_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Speakers');
            //lq();
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function Euromedicom_Agenda_face()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/agenda.php?id=260",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $response = html_entity_decode($response);
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            // j($xml);
            //LIVE_PLASTIC_SURGERY_-_STATE_OF_THE_ART
            
            foreach ($xml['Informa']['MCA_2018']['Sessions']['Session']  as $key => $value)
            {   
                // j($value);
                $agenda['tmp_id'] = 'euromedicom-MCA_2018-'.$value['ID'];
                $agenda['StartDate'] = empty($value['StartDate'])? '' :$value['StartDate'];
                $agenda['EndDate'] = empty($value['EndDate'])? '' :$value['EndDate'];
                $agenda['StartTime'] = empty($value['StartTime'])? '' :$value['StartTime'];
                $agenda['EndTime'] = empty($value['EndTime'])? '' :$value['EndTime'];
                $agenda['Name'] = empty($value['Name'])? '' :$value['Name'];
                $agenda['Type'] = empty($value['Location'])? '' :$value['Location'];
                $agenda['Description'] = empty($value['Description'])? '' :$value['Description'];
                $agenda['Location'] = empty($value['Location'])? '' :$value['Location'];
                
                if(!empty($value['Description']['Topic']))
                {
                    $agenda['Description'] = implode('<br><br>',$value['Description']['Topic']);
                }
                if(array_key_exists('Speaker',$value['Speakers']))
                {
                    $temp_speaker = array_filter($value['Speakers']['Speaker']);
                    $temp_speaker = array_filter(array_column($temp_speaker,'ID'));
                    $agenda['Speakers'] = empty($value['Speakers']['Speaker'])? '' :implode(',',$temp_speaker);
                }
                else
                {
                    $agenda['Speakers'] = '';
                }
                

                $tmp_agenda = $this->db->where('tmp_id','euromedicom-MCA_2018-'.$value['ID'])->get('Euromedicom_Agenda')->row_array();
                if(empty($tmp_agenda))
                {
                    $this->db->insert('Euromedicom_Agenda',$agenda);
                }
                else
                {
                    $this->db->where($tmp_agenda);
                    $this->db->update('Euromedicom_Agenda',$agenda);
                }
            }
            j('data Insert Successfully');
        }
    }
    public function Euromedicom_Agenda_add_face()
    {
        $org_id=32052;
        $Agenda_id=1819;
        $eid=1045;
        $temp_session=$this->db->select('*')->from('Euromedicom_Agenda')->limit(50)->get()->result_array();
        // j($temp_session);
        foreach ($temp_session as $key => $value) 
        {
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->get_where('user',array('knect_api_user_id'=>'euromedicom-mca-'.$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=$value['StartDate'];
            $session_data['Start_time']=$value['StartTime'];
            $session_data['End_date']=$value['EndDate'];
            $session_data['End_time']=$value['EndTime'];
            $session_data['Heading']=html_entity_decode(trim($value['Name']));
            $session_data['description']=$value['Description'];
            $session_data['custom_location']=$value['Location'];
            // j($session_data);
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            elseif(trim($value['Type']) == '')
            {
                $type_id = ($eid == 936) ? 1780 : 1779;
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code','euromedicom-'.trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code','euromedicom-MCA_2018-'.trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']='euromedicom-MCA_2018-'.trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Agenda');
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function Euromedicom_Sponsors_face()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/sponsors.php?id=247",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            //echo $response;
            $response = utf8_encode(html_entity_decode($response));
            $response=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = json_decode(json_encode((array) simplexml_load_string($response)), 1);
            // j($xml);
            foreach ($xml['Informa']['AMWC_2018']['Sponsors']['Sponsor']  as $key => $value)
            {   
                $sponsor['tmp_id'] = 'euromedicom-face-'.$value['ID'];
                $sponsor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $sponsor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $sponsor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $sponsor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $sponsor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $sponsor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $sponsor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $sponsor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $tmp_sponsor = $this->db->where('tmp_id','euromedicom-face-'.$value['ID'])->get('Euromedicom_Sponsors')->row_array();
                if(empty($tmp_sponsor))
                {
                    $this->db->insert('Euromedicom_Sponsors',$sponsor);
                }
                else
                {   
                    $this->db->where($tmp_sponsor);
                    $this->db->update('Euromedicom_Sponsors',$sponsor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Sponsors_add_face()
    {
        $event_id = 1044;
        $org_id   = 32052;
        $tmp_sponsor=$this->db->select('*')->from('Euromedicom_Sponsors')->limit(50)->get()->result_array();
        foreach ($tmp_sponsor as $key => $value)
        {
            $check_sponsor = $this->db->where('api_id',$value['tmp_id'])->where('Event_id',$event_id)->get('sponsors')->row_array();
            $data['api_id'] = $value['tmp_id'];
            $data['Company_name'] = $value['Company'];
            $data['Description']  = $value['CompanyDescription'];
            $data['website_url']  = $value['Website'];
            $data['facebook_url'] = $value['Facebook'];
            $data['twitter_url  '] = $value['Twitter'];
            $data['linkedin_url'] = $value['Linkedin'];
            $data['instagram_url'] = $value['Instagram'];
            $data['Event_id'] = $event_id;
            $data['Organisor_id'] = $org_id;

            if (!empty($value['Image']))
            {
                $destination = "company_logo_" . uniqid();
                $source = $value['Image'];
                $info = getimagesize($source);
                if ($info['mime'] == 'image/jpeg')
                {
                    $logo = $destination . '.jpeg';
                    $image = imagecreatefromjpeg($source);
                    imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                }
                elseif ($info['mime'] == 'image/gif')
                {
                    $logo = $destination . '.gif';
                    $image = imagecreatefromgif($source);
                    imagegif($image, "./assets/user_files/" . $destination . '.gif');
                }
                elseif ($info['mime'] == 'image/png')
                {
                    $logo = $destination . '.png';
                    $image = imagecreatefrompng($source);
                    $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                    imagecolortransparent($image, $background);
                    imagealphablending($image, false);
                    imagesavealpha($image, true);
                    imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                }
                $data['company_logo'] = json_encode(array(
                    $logo
                ));
            }

            if(empty($check_sponsor))
            {
                $this->db->insert('sponsors',$data);
            }
            else
            {
                $this->db->where($check_sponsor);
                $this->db->update('sponsors',$data);
            }
            unset($data);
             $this->db->where('tmp_id',$value['tmp_id'])->delete('Euromedicom_Sponsors');
        }
        $total_exhi = $this->db->select()->from('Euromedicom_Sponsors')->get()->num_rows();
        echo "Success -> Remaning Sponsor. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('Sponsor added  successfully');
    }
    public function Euromedicom_Exhi_face()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://emc2.euromedicom.com/xmlMobile/exhibitors.php?id=247",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            $response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);
            $xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
            $xmlJson = json_encode($xml);
            $xmlArr = json_decode($xmlJson, 1);
            
            foreach ($xmlArr['Informa']['AMWC_2018']['Exhibitors']['Exhibitor']  as $key => $value)
            {   
                $Exhibitor['tmp_id'] = 'euromedicom-face-'.$value['ID'];
                $Exhibitor['Email'] = empty($value['Email'])? 'euromedicom-face-'.$value['ID'].'@venturiapps.com' :$value['Email'];
                $Exhibitor['FirstName'] = empty($value['FirstName'])? '' :$value['FirstName'];
                $Exhibitor['LastName'] = empty($value['LastName'])? '' :$value['LastName'];
                $Exhibitor['StandNumber'] = empty($value['StandNumber'])? '' :$value['StandNumber'];
                $Exhibitor['Company'] = empty($value['Company'])? '' :$value['Company'];
                $Exhibitor['CompanyDescription'] = empty($value['CompanyDescription'])? '' :$value['CompanyDescription'];
                $Exhibitor['Image'] = empty($value['Image'])? '' :$value['Image'];
                $Exhibitor['Website'] = empty($value['Website'])? '' :$value['Website'];
                $Exhibitor['Facebook'] = empty($value['Facebook'])? '' :$value['Facebook'];
                $Exhibitor['Twitter'] = empty($value['Twitter'])? '' :$value['Twitter'];
                $Exhibitor['Linkedin'] = empty($value['Linkedin'])? '' :$value['Linkedin'];
                $Exhibitor['Instagram'] = empty($value['Instagram'])? '' :$value['Instagram'];
                $Tmp_Exhibitor = $this->db->where('tmp_id','euromedicom-face-'.$value['ID'])->get('Euromedicom_Exhibitors')->row_array();
                $Exhibitor['CompanyDescription'] =$Exhibitor['CompanyDescription'];
                $data[] = $Exhibitor;
                if(empty($Tmp_Exhibitor))
                {
                    $this->db->insert('Euromedicom_Exhibitors',$Exhibitor);
                }
                else
                {   
                    $this->db->where($Tmp_Exhibitor);
                    $this->db->update('Euromedicom_Exhibitors',$Exhibitor);
                }
            }
            j('Data inserted Successfully');
        }
    }
    public function Euromedicom_Exhi_add_face($value='')
    {   
        //error_reporting(E_ALL);
        // die("Direct Access Denied..");
        $oid = '32052';
        $eid = '1044';
        $temp_exibitor = $this->db->select('*')->from('Euromedicom_Exhibitors')->limit('50')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['LastName'];
                $user_data['Company_name'] = $value['Company'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Company']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Company'];
                $ebi_data['Description'] = $value['CompanyDescription'];
                $ebi_data['stand_number'] = $value['StandNumber'];

                if (!empty($value['Image']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['Image'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['Website'];
                $ebi_data['facebook_url'] = $value['Facebook'];
                $ebi_data['linkedin_url'] = $value['Linkedin'];
                $ebi_data['instagram_url'] = $value['Instagram'];
                $ebi_data['twitter_url'] = $value['Twitter'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Company']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('Euromedicom_Exhibitors');
        }
        echo "Success";
        die;
    }
    public function mjbaker_meeting_upload()
    {
        $tmp_mettings = $this->db->where('user_id_2 IS NOT NULL')->where('user_id_1 IS NOT NULL')->get('mjbaker_meeting')->result_array();
        foreach($tmp_mettings as $key => $value)
        {
            $insert['attendee_id'] = $value['user_id_2'];
            $insert['recever_attendee_id'] = $value['user_id_1'];
            $insert['event_id'] = '1017';
            $insert['date'] = '2018-02-22';
            $insert['time'] = $value['Start_time'];
            $insert['status'] = '1';
            $insert['created_datetime'] = date('Y-m-d H:i:s');
            $this->db->insert('exhibitor_attendee_meeting',$insert);
            $data[] = $insert;
            // $this->db->where($value)->delete('mjbaker_meeting');
        }
        j($data);
    }
    public function napec_exhi()
    {    
        //error_reporting(E_ALL);
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://webapi.napec-dz.com/api/NapecExhibitor",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {   
            /*$response = str_replace('&reg;','<![CDATA[&reg;]]>',$response);
            $response = str_replace('&trade;','<![CDATA[&trade;]]>',$response);
            $response = str_replace('&ldquo;','<![CDATA[&ldquo;]]>',$response);
            $response = str_replace('&rdquo;','<![CDATA[&rdquo;]]>',$response);
            $response = str_replace('&acute;','<![CDATA[&acute;]]>',$response);*/
            // j(json_decode($response));
            //$xml = simplexml_load_string($response, 'SimpleXMLElement');
            // $xmlJson = json_encode($response);
            $xmlArr = json_decode($response, 1);
            // j($xmlArr);
            foreach ($xmlArr as $key => $value)
            {   
                $Exhibitor['tmp_id'] = 'napec-'.$value['IdOwner'];
                $Exhibitor['Email'] = empty($value['Exhibitor_Email_Address'])? 'napec-'.$value['IdOwner'].'@venturiapps.com' :$value['Exhibitor_Email_Address'];
                $Exhibitor['Company'] = empty($value['Exhibitor_Company_Name'])? '' :$value['Exhibitor_Company_Name'];
                $Exhibitor['Description'] = empty($value['Exhibitor_Description'])? '' :$value['Exhibitor_Description'];
                /*if(!empty($value['Tel']))
                {
                    $Exhibitor['Description'] .= "<br>Tel: ".$value['Tel'];
                }
                if(!empty($value['Exhibitor_Email_Address']))
                {
                    $Exhibitor['Description'] .= "<br>Email: ".$value['Exhibitor_Email_Address'];
                }*/                
                $Exhibitor['FirstName'] = empty($value['Exhibitor_Company_Name']) ? '' :$value['Exhibitor_Company_Name'];
                $Exhibitor['Website'] = empty($value['Exhibitor_Website'])? '' :$value['Exhibitor_Website'];
                $Exhibitor['StandNumber'] = empty($value['Stand_Number'])? '' :$value['Stand_Number'];

                $Tmp_Exhibitor = $this->db->where('tmp_id','napec-'.$value['IdOwner'])->get('Napec_Exhibitors')->row_array();
                $data[] = $Exhibitor;
                if(empty($Tmp_Exhibitor))
                {
                    $this->db->insert('Napec_Exhibitors',$Exhibitor);
                }
                else
                {   
                    $this->db->where($Tmp_Exhibitor);
                    $this->db->update('Napec_Exhibitors',$Exhibitor);
                }
            }
        }
    }
    public function napec_exhi_add($value='')
    {   
        //error_reporting(E_ALL);
        $oid = '31277';
        $eid = '772';
        $temp_exibitor = $this->db->select('*')->from('Napec_Exhibitors')->limit('80')->get()->result_array();
        // die("Direct Access Denied..");
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Company_name'] = $value['Company'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Company']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Company'];
                $ebi_data['Description'] = $value['Description'];
                $ebi_data['stand_number'] = $value['StandNumber'];
                $ebi_data['company_logo'] = NULL;
                $ebi_data['Images'] = NULL;
                

                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Company']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('Napec_Exhibitors');
        }
        echo "Success";
        die;
    }
    public function HFTP($value='')
    {
        $soapUrl = "partners.a2zinc.net/dataservices/public/ExhibitorProvider.asmx"; 
        $soapUser = "AllInTheLoop";  
        $soapPassword = "A12InThe3oop";
        $key = "MzI2EREtWRTC0ex3UGToDcelVmm0QzB7FGxgf+7g0so6soT9j3eqHz9l3nZZhHNysOJvFTb9KUkWyDwZjlDol8k1akP8gL7as+XlI3nx2P4=";
        $installnname = "HFTP";
    
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Header>
                            <a2zAuthenticationHeader xmlns="http://www.a2zshow.com/DataServices/v2/">
                              <Key>'.$key.'</Key>
                              <Source />
                              <UserName>'.$soapUser.'</UserName>
                              <Password>'.$soapPassword.'</Password>
                              <InstallName>'.$installnname.'</InstallName>
                            </a2zAuthenticationHeader>
                          </soap:Header>
                          <soap:Body>
                            <DataService xmlns="http://www.a2zshow.com/DataServices/ExhibitorListRequest/v1">
                              <Request>xml</Request>
                            </DataService>
                          </soap:Body>
                        </soap:Envelope>';  

        $headers = array(
                    "Content-type: text/xml;charset=\"utf-8\"",
                    "Accept: text/xml",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    "SOAPAction: http://www.a2zshow.com/DataServices/v2/getExhibitorList", 
                    "Content-length: ".strlen($xml_post_string),
                ); 

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        
        $response = curl_exec($ch); 
        curl_close($ch);

       
        $response1 = str_replace("<soap:Body>","",$response);
        $response2 = str_replace("</soap:Body>","",$response1);

        
        $parser = simplexml_load_string($response2);
        echo "<pre>";
        print_r($parser);exit;
    }
    public function HFTP_Exhi()
    {   
        /*//error_reporting(E_ALL);
        ini_set('display_errors','1');*/
        $soapUrl = "partners.a2zinc.net/dataservices/public/ExhibitorProvider.asmx"; 
        $soapUser = "AllInTheLoop";  
        $soapPassword = "A12InThe3oop";
        $key = "MzI2EREtWRTC0ex3UGToDcelVmm0QzB7FGxgf+7g0so6soT9j3eqHz9l3nZZhHNysOJvFTb9KUkWyDwZjlDol8k1akP8gL7as+XlI3nx2P4=";
        $installnname = "HFTP";
    
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                          <soap:Header>
                            <a2zAuthenticationHeader xmlns="http://www.a2zshow.com/DataServices/v2/">
                              <Key>'.$key.'</Key>
                              <Source />
                              <UserName>'.$soapUser.'</UserName>
                              <Password>'.$soapPassword.'</Password>
                              <InstallName>'.$installnname.'</InstallName>
                            </a2zAuthenticationHeader>
                          </soap:Header>
                          <soap:Body>
                            <DataService xmlns="http://www.a2zshow.com/DataServices/OnlineBoothInformationRequest/v1">
                              <Request>xml</Request>
                            </DataService>
                          </soap:Body>
                        </soap:Envelope>';  

        $headers = array(
                    "Content-type: text/xml;charset=\"utf-8\"",
                    "Accept: text/xml",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    "SOAPAction: http://www.a2zshow.com/DataServices/v2/getOnlineBoothInformation", 
                    "Content-length: ".strlen($xml_post_string),
                ); 

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch); 
        curl_close($ch);
        $response1 = str_replace("<soap:Body>","",$response);
        $response2 = str_replace("</soap:Body>","",$response1);

        $parser = simplexml_load_string($response2);
        $xml = json_decode(json_encode((array) simplexml_load_string($response2)), 1);
        $xmlArr = $xml['DataService']['Response']['a2zXMLRootNode']['OnlineBoothInformation'];
        foreach ($xmlArr as $key => $value)
        {   
            if($value['EventCode'] == 'HITECHouston2018')
            {   
                $Exhibitor['tmp_id'] = 'HFTP-'.$value['CompanyID'].'-'.$value['@attributes']['BoothID'];
                $Exhibitor['Email'] = is_array($value['ContactEmail']) || ($value['ContactEmail'] == 'none@none.com') ? 'HFTP-'.$value['CompanyID'].'-'.$value['@attributes']['BoothID'].'@venturiapps.com' :$value['ContactEmail'];
                $Exhibitor['Email'] .=  '__'.$Exhibitor['tmp_id'];
                $Exhibitor['FirstName'] = is_array($value['ContactFName']) ? '' :$value['ContactFName'];
                $Exhibitor['LastName'] = is_array($value['ContactLName'])? '' :$value['ContactLName'];
                $Exhibitor['Company'] = is_array($value['CompanyName'])? '' :$value['CompanyName'];
                $Exhibitor['StandNumber'] = is_array($value['BoothNumber'])? '' :$value['BoothNumber'];
                $Exhibitor['Title'] = is_array($value['ContactTitle'])? '' :$value['ContactTitle'];
                $Exhibitor['CompanyDescription'] = is_array($value['OnlineProfile'])? '' :$value['OnlineProfile'];
                $Exhibitor['Image'] = is_array($value['CompanyLogo'])? '' :$value['CompanyLogo'];
                $Exhibitor['City'] = is_array($value['ContactCity'])? '' :$value['ContactCity'];
                $Exhibitor['State'] = is_array($value['State'])? '' :$value['State'];
                $Exhibitor['Postcode'] = is_array($value['ContactZip'])? '' :$value['ContactZip'];
                $Exhibitor['Website'] = is_array($value['WebSiteURL'])? '' :$value['WebSiteURL'];

                if(!is_array($value['ContactCountry']))
                {
                    $country_id = $this->db->where('country_name',$value['ContactCountry'])->get('country')->row_array()['id'];
                    $Exhibitor['country'] = $country_id ?: $value['ContactCountry'];
                }
                
                foreach ($value['CustomFields']['Field'] as $key1 => $value1)
                {   
                    if(!empty($value1['@attributes']['Name']))
                    {
                        $Exhibitor[$value1['@attributes']['Name']] = $value1['CustomFieldResponses']['Value']['@attributes']['Text'];                
                    }
                }
                if(!empty($value['ProductCategories']['Category']['SubCategory']))
                {   
                    $tmp_keywords = array_column($value['ProductCategories']['Category']['SubCategory'],'Text');
                    if(!empty($value['ContactCountry']))
                    {
                        $tmp_keywords[] = $value['ContactCountry'];
                    }
                    $Exhibitor['Keywords'] = implode(',',array_reverse($tmp_keywords));
                    unset($tmp_keywords);
                }
                else
                {
                    $Exhibitor['Keywords'] = $value['ContactCountry'];
                }
                $Tmp_Exhibitor = $this->db->where('tmp_id',$Exhibitor['tmp_id'])->get('HFTP_Exhibitors')->row_array();
                $data[] = $Exhibitor;

                if(empty($Tmp_Exhibitor))
                {
                    $this->db->insert('HFTP_Exhibitors',$Exhibitor);
                }
                else
                {   
                    $this->db->where($Tmp_Exhibitor);
                    $this->db->update('HFTP_Exhibitors',$Exhibitor);
                }
                unset($Exhibitor);
            }
        }
        j($data); 

    }
    public function HFTP_Exhi_add()
    {   
        // //error_reporting(E_ALL);
        $oid = '55497';
        //$eid = '1087';
        $eid = '1404';
        $temp_exibitor = $this->db->select('*,GROUP_CONCAT(StandNumber) as StandNumber',false)
                                  ->from('HFTP_Exhibitors')
                                  ->group_by('Email')
                                  ->limit('25')->get()->result_array();

        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['LastName'];
                $user_data['Company_name'] = $value['Company'];
                $user_data['Title'] = $value['Title'];
                $user_data['State'] = $value['State'];
                $user_data['Postcode'] = $value['Postcode'];
                $user_data['Country'] = $value['country'];
                $user_data['is_from_api'] = '1';

                $ures = $this->db->select('*')->from('user')
                                 ->where('Email', trim($value['Email']))
                                 ->get()
                                 ->result_array();

                if(count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }

                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')
                                     ->from('exibitor')
                                     ->where('Event_id', $eid)
                                     ->where('Heading', trim($value['Company']))
                                     ->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')
                                     ->from('exibitor')
                                     ->where('Event_id', $eid)
                                     ->where('user_id IS NULL')
                                     ->where('Heading', trim($value['Company']))
                                     ->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Company'];
                $ebi_data['Description'] = $value['CompanyDescription'];
                $ebi_data['stand_number'] = $value['StandNumber'];
                $ebi_data['Short_desc'] = $value['Keywords'];

                if (!empty($value['Image']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['Image'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    if(!empty($logo))
                    {
                        $ebi_data['company_logo'] = json_encode(array($logo));
                    }
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                
                $ebi_data['Images'] = NULL;
                $ebi_data['website_url'] = $value['Website'];
                $ebi_data['facebook_url'] = $value['Facebook'];
                $ebi_data['linkedin_url'] = $value['Linkedin'];
                $ebi_data['instagram_url'] = $value['Instagram'];
                $ebi_data['twitter_url'] = $value['Twitter'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Company']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Company']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('HFTP_Exhibitors');
        }
        $HFTP_Exhibitors = $this->db->get('HFTP_Exhibitors')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $HFTP_Exhibitors . " Refresh Page -> " . round($HFTP_Exhibitors / 50 ) . " Time";
        updateModuleDate($eid,'exhibitor');
        j($msg);
    }

    public function modifyEmailsHFTP()
    {
       /* $temp_exibitor = $this->db->select('*,GROUP_CONCAT(StandNumber) as StandNumber',false)
                                  ->from('HFTP_Exhibitors')
                                  ->group_by('Email')
                                  ->limit('50')->get()->result_array();

        foreach ($temp_exibitor as $key => $value)
        {   
            $this->db->select('u.*');
            $this->db->where('u.Email',$value['Email']);
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('reu.Event_id','1087');
            $this->db->where('reu.Role_id',6);
            $user = $this->db->get('user u')->row_array();

            if(!empty($user))
            {
                $update['Email'] = $user['Email'].'__'.$value['tmp_id'];
                $this->db->where($user);
                $this->db->update('user',$update);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('HFTP_Exhibitors');
        }*/
    }
    public function check_unique_user($event_id,$email)
    {   
        //error_reporting(E_ALL);
        // ini_set('display_errors','1');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where("((reu.Event_id = '$event_id' AND u.Email = '$email') OR (u.Email = '$email' AND reu.Role_id = '3'))");
        $res = $this->db->get('user u')->row_array();
        return $res;
    }
    public function add_HITECAmsterdam_attendee()
    {
       
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Title = $this->input->post('Title');
        $Company_name = $this->input->post('Company_name');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $event_id = 1087;
            $check_unique = $this->check_unique_user($event_id,$Email);
            // $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            if(!empty($Password))
            {
                if(strlen($Password) < 6)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Password should be minimum 6 character'
                    );
                    echo json_encode($data);exit;
                }
                $data['Password'] = md5($Password);
            }
            $data['Title'] = $Title;
            $data['Company_name'] = $Company_name;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }

    public function add_HITECHouston_attendee()
    {
       
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Title = $this->input->post('Title');
        $Company_name = $this->input->post('Company_name');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $event_id = 1404;
            // $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            $check_unique = $this->check_unique_user($event_id,$Email);

            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Duplicate email. Please contact HFTP at 512-249-5333 for further assistance.'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            if(!empty($Password))
            {
                if(strlen($Password) < 6)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Password should be minimum 6 character'
                    );
                    echo json_encode($data);exit;
                }
                $data['Password'] = md5($Password);
            }
            $data['Title'] = $Title;
            $data['Company_name'] = $Company_name;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }

    public function add_HITEC3rdAnnualConvention_attendee()
    {
       
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Title = $this->input->post('Title');
        $Company_name = $this->input->post('Company_name');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $event_id = 1413;
            // $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            $check_unique = $this->check_unique_user($event_id,$Email);

            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            if(!empty($Password))
            {
                if(strlen($Password) < 6)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Password should be minimum 6 character'
                    );
                    echo json_encode($data);exit;
                }
                $data['Password'] = md5($Password);
            }
            $data['Title'] = $Title;
            $data['Company_name'] = $Company_name;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }

    public function add_Podcast_Movement_attendee()
    {
       
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Title = $this->input->post('Title');
        $Company_name = $this->input->post('Company_name');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists.'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $event_id = 1381;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            if(!empty($Password))
            {
                if(strlen($Password) < 6)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Password should be minimum 6 character'
                    );
                    echo json_encode($data);exit;
                }
                $data['Password'] = md5($Password);
            }
            $data['Title'] = $Title;
            $data['Company_name'] = $Company_name;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }

    public function add_temp_attendees_ucas_annual_admissions()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.eventsforce.net/ucas/api/v2/events/1824/attendees.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpDQjA2RTY3RjE4REE0MDYzQTUxMDVDNTA5MjIwNEIwOA==",
            "Cache-Control: no-cache",
            "Postman-Token: de9657bf-9c7d-473c-e7e5-2ead15bad577"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            //echo $response;exit;
            $users = json_decode($response,true);
            $users = $users['data'];
            foreach ($users as $key => $value)
            {    
                $insert['Firstname'] = $value['firstName'];
                $insert['Lastname'] = $value['lastName'];
                $insert['Email'] = $value['email'];
                $insert['Title'] = $value['jobTitle'];
                $insert['Salutation'] = $value['title'];
                $insert['Company_name'] = $value['company'];
                 
                $check_tmp_user = $this->db->where('Email',$value['email'])->get('temp_ucas_users')->row_array();
                if(empty($check_tmp_user))
                {
                    $this->db->insert('temp_ucas_users',$insert);
                }
                else
                {   
                    $this->db->where($check_tmp_user);
                    $this->db->update('temp_ucas_users',$insert);
                }
            }
            j('Temp Data Addedd Successfully');         
        }
    }

    public function add_temp_sessions_ucas_annual_admissions($session_id,$session_code)
    {
        $curl = curl_init();
          curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.eventsforce.net/ucas/api/v2/events/1824/sessions/$session_id/bookedAttendees.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpDQjA2RTY3RjE4REE0MDYzQTUxMDVDNTA5MjIwNEIwOA==",
            "Cache-Control: no-cache",
            "Postman-Token: b0d4cc43-8afb-15b0-e49d-4bff93f17df7"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            //echo $response;exit;
            $agenda = json_decode($response,true);
            $agenda = $agenda['data'];
            foreach ($agenda as $key => $value)
            {    
                $insert['session_code'] = $session_code;
                $insert['email'] = $value['email'];
                $this->db->insert('ucas_session_temp_j',$insert);
            }
            j('Temp Data Addedd Successfully');         
        }
    }

    public function add_attendees_ucas_annual_admission()
    {   
        $this->load->model('native_single_fcm/app_login_model');
        $temp_ucas_users = $this->db->limit('50')->get('temp_ucas_users')->result_array();
        
        foreach ($temp_ucas_users as $key => $value)
        {  
            $event_id = 1308;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$value['Email'])->get('user')->row_array();

            $data = $value;
            unset($data['id']);
            unset($data['extra_column']);
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $user_id = $user['Id'];
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
            }
            $event_attendee = $this->db->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get('event_attendee')->row_array();
            if(!empty($event_attendee))
            {
                $update['extra_column'] = $value['extra_column'];
                $this->db->where($event_attendee);
                $this->db->update('event_attendee',$update);
            }
            
            $this->db->where($value);
            $this->db->delete('temp_ucas_users');
        }
        $temp_ucas_users = $this->db->get('temp_ucas_users')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $temp_ucas_users . " Refresh Page -> " . round($temp_ucas_users / 50 ) . " Time";
        j($msg);
    }
    public function assign_ucas_agenda_admissions()
    {   
        //error_reporting(E_ALL);
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('ucas_session_temp_j uct','uct.email = u.Email');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id','1308');
        $this->db->group_by('u.Id');
        $this->db->limit(50);
        $res = $this->db->get()->result_array();
        
        foreach ($res as $key => $value)
        {
            $tmp_user_agenda = $this->db->where('user_id',$value['Id'])->get('users_agenda')->row_array();
            if($tmp_user_agenda)
            {
                $tmp_agenda = explode(',',$tmp_user_agenda['agenda_id']);
                $new_agenda  = explode(',',$value['agenda_id']);
                $fin_agenda = array_merge($tmp_agenda,$new_agenda);
                $this->db->where($tmp_user_agenda);
                $update['agenda_id'] = implode(',',array_unique($fin_agenda));
                $this->db->update('users_agenda',$update);
            }   
            else
            {
                $insert['user_id'] = $value['Id'];
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['updated_date'] = date('Y-m-d H:i:s');
                $this->db->insert('users_agenda',$insert);
            }
            $this->db->where('email',$value['email']);
            $this->db->delete('ucas_session_temp_j');
        }
        j('data added successfully');
    }

    public function delete_ucas_sessions_users()
    {
        //error_reporting(E_ALL);
       /* $agenda_ids1 = '21224,21225,21226,21227,21228,21229,21230,21231,21232,21233,21234,21235,21236,21237,21238,21239,21240,21241,21242,21243,21244,21245,21246,21247,21248,21249,21250,21251,21252,21253,21254,21256,21257,21258,21259,21260,21261,21262,21263,21264,21265,21266,21267,21268,21269,21270,21271,21272,21273,21274,21275,21276,21277,21278,21279,21280,21281,21282,21283,21284,21285,21286,21287,21288,21289,21291,21292,21293,21294,21295,21296,21297,21298,21299,21300,21301,21302,21303,21304,21305,21306,21307,21308,21675,21676,21677,21681,21682,21683,21684,21688';*/

        $agenda_ids = explode(',', $agenda_ids1);


        $uas = [];
       
        foreach ($agenda_ids as $key => $value) 
        {
            $data = $this->db->select('*')->from('users_agenda')->where('find_in_set("'.$value.'",agenda_id) > 0')->get()->result_array();
            if(!empty($data))
            {
                foreach ($data as $key => $agenda) {
                     $where['id'] = $agenda['id'];
                     $tmp = explode (',',$agenda['agenda_id']);
                     if (($key = array_search($value, $tmp)) !== false) {
                        unset($tmp[$key]);
                     }
                     $update['agenda_id'] = implode(',', $tmp);

                     $this->db->where($where);
                     $this->db->update('users_agenda',$update);
                }
               
            }
        }

        exit;
    }
    public function test()
    {   
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('ucas_session_temp_j uct','uct.email = u.Email');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id','1308');
        $this->db->group_by('u.Id');
        $res = $this->db->get()->row_array();
        j($res);
    }
    public function ipexpo_users_saved_agenda()
    {   
        $this->db->select('ua.*,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email');
        $this->db->join('user u','u.Id = ua.user_id');
        $this->db->join('relation_event_user reu','reu.User_id = ua.user_id');
        $this->db->where('reu.Event_id','1107');
        $this->db->where('reu.Role_id','4');
        $res = $this->db->get('users_agenda ua')->result_array();
        foreach($res as $key => $value)
        {   
            $agenda_ids = array_filter(explode(',',$value['agenda_id']));
            sort($agenda_ids);
            $this->db->select('Id,Heading,Start_date,Start_time');
            $this->db->where_in('Id',$agenda_ids);
            $this->db->order_by('Id');
            $agenda = $this->db->get('agenda')->result_array();
            $res[$key]['agenda'] = $agenda;
        }
        // j($res);
        $this->load->helper('download');
        $filename = "ucexpo_saved_agedna_by_user.csv";
        $fp = fopen('php://output', 'w');
        $headers[] = 'Session Name';
        $headers[] = 'Date/Time';
        $headers[] ='First Name';
        $headers[] ='Last Name';
        $headers[] ='Title';
        $headers[] ='Company Name';
        $headers[] ='Email';
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $headers);
        foreach ($res as $key => $value)
        {
            foreach ($value['agenda'] as $key1 => $value1)
            {
                $data['Session Name'] = $value1['Heading'];
                $data['Date/Time'] = $value1['Start_date'].' '.$value1['Start_time'];
                $data['First Name'] = $value['Firstname'];
                $data['Last Name'] = $value['Lastname'];
                $data['Title'] = $value['Title'];
                $data['Company Name'] = $value['Company_name'];
                $data['Email'] = $value['Email'];
                fputcsv($fp, $data);
                unset($data);
            }
        }        
    }
    public function ipexpo_users_checkin_agenda()
    {   
        $this->db->select('ua.*,u.Firstname,u.Lastname,u.Title,u.Company_name,u.Email');
        $this->db->join('user u','u.Id = ua.user_id');
        $this->db->join('relation_event_user reu','reu.User_id = ua.user_id');
        $this->db->where('reu.Event_id','1107');
        $this->db->where('reu.Role_id','4');
        $res = $this->db->get('users_agenda ua')->result_array();
        foreach($res as $key => $value)
        {   
            $agenda_ids = array_filter(explode(',',$value['check_in_agenda_id']));
            sort($agenda_ids);
            if(!empty($agenda_ids))
            {
                $this->db->select('Id,Heading,Start_date,Start_time');
                $this->db->where_in('Id',$agenda_ids);
                $this->db->order_by('Id');
                $agenda = $this->db->get('agenda')->result_array();
                $res[$key]['agenda'] = $agenda;
            }
        }
        // j($res);
        $this->load->helper('download');
        $filename = "ucexpo_users_checkin_agenda.csv";
        $fp = fopen('php://output', 'w');
        $headers[] = 'Session Name';
        $headers[] = 'Date/Time';
        $headers[] ='First Name';
        $headers[] ='Last Name';
        $headers[] ='Title';
        $headers[] ='Company Name';
        $headers[] ='Email';
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $headers);
        foreach ($res as $key => $value)
        {
            foreach ($value['agenda'] as $key1 => $value1)
            {
                $data['Session Name'] = $value1['Heading'];
                $data['Date/Time'] = $value1['Start_date'].' '.$value1['Start_time'];
                $data['First Name'] = $value['Firstname'];
                $data['Last Name'] = $value['Lastname'];
                $data['Title'] = $value['Title'];
                $data['Company Name'] = $value['Company_name'];
                $data['Email'] = $value['Email'];
                fputcsv($fp, $data);
                unset($data);
            }
        }
    }

    public function import_attendee($aid)
    {
        $aid =  $this->uri->segment(3);
        $this->load->model('native_single_fcm/app_login_model');
        $eid =  $this->app_login_model->get_event_id_byaccess_id($aid);
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Title = $this->input->post('Title');
        $Company_name = $this->input->post('Company_name');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(strpos($Email, '@') === false)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists.'
                );
                echo json_encode($data);exit;
            }
            $this->load->model('native_single_fcm/app_login_model');
            $org_id=$this->app_login_model->getOrganizerByEvent($eid);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            if(!empty($Password))
            {
                if(strlen($Password) < 6)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Password should be minimum 6 character'
                    );
                    echo json_encode($data);exit;
                }
                $data['Password'] = md5($Password);
            }
            $data['Title'] = $Title;
            $data['Company_name'] = $Company_name;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$eid);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$eid,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$eid,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function add_ipexpoeurope_attendee()
    {
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            /*$check_unique = $this->db->where('Email',$Email)->get('user')->row_array();
            if($check_unique)
            {   
                $data = array(
                    'success' => false,
                    'message' => 'Email is already exists'
                );
                echo json_encode($data);exit;
            }*/
            $this->load->model('native_single_fcm/app_login_model');
            $event_id = 1682;
            $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->app_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
}