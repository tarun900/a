<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sponsors extends FrontendController
{
    function __construct()
    {

        $this->data['pagetitle'] = 'Your Sponsors';
        $this->data['smalltitle'] = 'Organise your Sponsors lists and user portals.';
        $this->data['breadcrumb'] = 'Your Sponsors';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        ini_set('auto_detect_line_endings', true);
        $this->load->model('Event_model');
        $this->load->model('User_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
        $this->load->model('Speaker_model');
        $this->load->model('Exibitor_model');
        $this->load->model('Sponsors_model');
        $this->load->model('Agenda_model');
        $this->load->model('Map_model');
        $this->load->model('Event_template_model');
        $this->load->library('email');
        $this->load->library('upload');
        $this->load->library('session');
        $event_id = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        if($user[0]->Role_id == '4')
               redirect('Forbidden');
        $this->load->database();
        $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
        $user_events =  array_filter(array_column($user_events,'Event_id'));
        if(!in_array($event_id,$user_events))
        {
           redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($event_id);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $eventmodule = $this->Event_model->geteventmodulues($event_id);
        $event = $this->Event_model->get_module_event($event_id);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $module = json_decode($eventmodule[0]['module_list']);
        // echo "<pre>";print_r($module);exit;
        if (!in_array('43', $module) || !in_array('43', $menu_list))
        {
            redirect('Forbidden');
        }

    }
    public function index($id)
    {
        $user = $this->session->userdata('current_user');
        $this->data['event_id11'] = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['email_template'] = $this->Setting_model->email_template($id);
        $this->data['event'] = $event[0];
        $sponsors = $this->Sponsors_model->get_sponsors_list($id);
        $this->data['sponsors'] = $sponsors;
        $sponsors_type = $this->Sponsors_model->get_sponsors_type_list($id);
        $this->data['sponsors_type'] = $sponsors_type;
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 43);
        $this->data['module_group_list'] = $module_group_list;
        $event_id = $user[0]->Event_id;
        $org = $this->Event_model->get_org_id_by_event($id);
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $this->data['org'] = $org;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 43);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        // echo "<pre>";print_r($user_role); exit();
        $this->data['categorie_data']=$this->Sponsors_model->get_sponsors_categories_by_event($id,NULL);
        $this->data['sponsors_categories'] = $this->Sponsors_model->get_sponsors_categories_by_event($id);
        $this->data['users_role'] = $user_role;
        $this->data['event_id'] = $id;
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'sponsors/css', $this->data, true);
        $this->template->write_view('content', 'sponsors/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'sponsors/js', $this->data, true);
        $this->template->render();
    }
    public function add_page($id, $user_id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        }
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            if (empty($this->input->post('company_logo_crop_data_textbox')))
            {
                if (!empty($_FILES['company_logo']['name']))
                {
                    $tempname = explode('.', $_FILES['company_logo']['name']);
                    $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => '*',
                        "max_size" => '100000',
                        "max_width" => '5000',
                        "max_height" => '5000'
                    ));
                    if (!$this->upload->do_multi_upload("company_logo"))
                    {
                        $error = array(
                            'error' => $this->upload->display_errors()
                        );
                        $this->session->set_flashdata('error', $error['error']);
                    }
                }
            }
            else
            {
                $img = $_POST['company_logo_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_company_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
            }
            if (empty($this->input->post('company_banner_crop_data_textbox')))
            {
                if (!empty($_FILES['banner_Images']['name']))
                {
                    $tempname = explode('.', $_FILES['banner_Images']['name']);
                    $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file1 = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file1,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => '*',
                        "max_size" => '100000',
                        "max_width" => '5000',
                        "max_height" => '5000'
                    ));
                    if (!$this->upload->do_multi_upload("banner_Images"))
                    {
                        $error = array(
                            'error' => $this->upload->display_errors()
                        );
                        $this->session->set_flashdata('error', "" . $error['error']);
                    }
                    $Images = array(
                        $images_file1
                    );
                }
            }
            else
            {
                $img = $_POST['company_banner_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file1 = strtotime(date("Y-m-d H:i:s")) . "_company_crop_banner_image.png";
                $filepath = "./assets/user_files/" . $images_file1;
                file_put_contents($filepath, $unencodedData);
                $Images = array(
                    $images_file1
                );
            }
            $data['sponsors_array']['user_id'] = $user_id;
            $data['sponsors_array']['Organisor_id'] = $Organisor_id;
            $data['sponsors_array']['Event_id'] = $id;
            $data['sponsors_array']['Sponsors_name'] = $this->input->post('Sponsors_name');
            $data['sponsors_array']['st_id'] = $this->input->post('sponsors_type');
            $data['sponsors_array']['Company_name'] = $this->input->post('Heading');
            $data['sponsors_array']['Description'] = $this->input->post('Description');
            $data['sponsors_array']['Images'] = $Images;
            $data['sponsors_array']['website_url'] = $this->input->post('website_url');
            $data['sponsors_array']['facebook_url'] = $this->input->post('facebook_url');
            $data['sponsors_array']['twitter_url'] = $this->input->post('twitter_url');
            $data['sponsors_array']['linkedin_url'] = $this->input->post('linkedin_url');
            $data['sponsors_array']['youtube_url'] = $this->input->post('youtube_url');
            $data['sponsors_array']['instagram_url'] = $this->input->post('instagram_url');
            $data['sponsors_array']['company_logo'] = $images_file;
            $sponsors_id = $this->Sponsors_model->add_user_as_sponsors($data);
            $this->session->set_flashdata('sponsors_data', ' Added');
            redirect("Sponsors/index/" . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $sponsors_type = $this->Sponsors_model->get_sponsors_type_list($id);
        $this->data['sponsors_type'] = $sponsors_type;
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'sponsors/add_page', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function page_edit($id, $eid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $sponsors = $this->Sponsors_model->get_sponsors_list($id);
        $this->data['sponsors'] = $sponsors;
        $sponsors_type = $this->Sponsors_model->get_sponsors_type_list($id);
        $this->data['sponsors_type'] = $sponsors_type;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $sponsors_by_id = $this->Sponsors_model->get_sponsors_by_id($id, $eid);
        $this->data['sponsors_by_id'] = $sponsors_by_id;
        if ($this->data['page_edit_title'] = 'edit')
        {
            if ($id == NULL || $id == '')
            {
                redirect('Exibitor');
            }
            if ($this->input->post())
            {
                if (empty($this->input->post('company_logo_crop_data_textbox')))
                {
                    if (!empty($_FILES['company_logo']['name']))
                    {
                        $tempname = explode('.', $_FILES['company_logo']['name']);
                        $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $images_file = $tempname_imagename . "." . $tempname[1];
                        $this->upload->initialize(array(
                            "file_name" => $images_file,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                        ));
                        if (!$this->upload->do_multi_upload("company_logo"))
                        {
                            $error = array(
                                'error' => $this->upload->display_errors()
                            );
                            $this->session->set_flashdata('error', $error['error']);
                        }
                    }
                }
                else
                {
                    $img = $_POST['company_logo_crop_data_textbox'];
                    $filteredData = substr($img, strpos($img, ",") + 1);
                    $unencodedData = base64_decode($filteredData);
                    $images_file = strtotime(date("Y-m-d H:i:s")) . "_company_crop_logo_image.png";
                    $filepath = "./assets/user_files/" . $images_file;
                    file_put_contents($filepath, $unencodedData);
                }
                if (empty($this->input->post('company_banner_crop_data_textbox')))
                {
                    if (!empty($_FILES['banner_Images']['name']))
                    {
                        $tempname = explode('.', $_FILES['banner_Images']['name']);
                        $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                        $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                        $images_file1 = $tempname_imagename . "." . $tempname[1];
                        $this->upload->initialize(array(
                            "file_name" => $images_file1,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                        ));
                        if (!$this->upload->do_multi_upload("banner_Images"))
                        {
                            $error = array(
                                'error' => $this->upload->display_errors()
                            );
                            $this->session->set_flashdata('error', "" . $error['error']);
                        }
                        $Images = array(
                            $images_file1
                        );
                    }
                }
                else
                {
                    $img = $_POST['company_banner_crop_data_textbox'];
                    $filteredData = substr($img, strpos($img, ",") + 1);
                    $unencodedData = base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s")) . "_company_crop_banner_image.png";
                    $filepath = "./assets/user_files/" . $images_file1;
                    file_put_contents($filepath, $unencodedData);
                    $Images = array(
                        $images_file1
                    );
                }
                $data['sponsors_array']['Id'] = $this->uri->segment(4);
                $data['sponsors_array']['Organisor_id'] = $logged_in_user_id;
                $data['sponsors_array']['Event_id'] = $id;
                $data['sponsors_array']['user_id'] = $this->input->post('ex_user');
                $data['sponsors_array']['st_id'] = $this->input->post('sponsors_type');
                $data['sponsors_array']['Sponsors_name'] = $this->input->post('Sponsors_name');
                $data['sponsors_array']['Company_name'] = $this->input->post('Heading');
                $data['sponsors_array']['website_url'] = $this->input->post('website_url');
                $data['sponsors_array']['facebook_url'] = $this->input->post('facebook_url');
                $data['sponsors_array']['twitter_url'] = $this->input->post('twitter_url');
                $data['sponsors_array']['linkedin_url'] = $this->input->post('linkedin_url');
                $data['sponsors_array']['youtube_url'] = $this->input->post('youtube_url');
                $data['sponsors_array']['instagram_url'] = $this->input->post('instagram_url');
                $data['sponsors_array']['Description'] = $this->input->post('Description');
                $data['sponsors_array']['Images'] = $Images;
                $data['sponsors_array']['old_images'] = $this->input->post('old_images');
                $data['sponsors_array']['company_logo'] = $images_file;
                $data['sponsors_array']['old_company_logo'] = $this->input->post('old_company_logo');
                $this->Sponsors_model->update_sponsors($data);
                $this->session->set_flashdata('sponsors_data', ' Updated');
                redirect("Sponsors/index/" . $id);
            }
            $this->session->set_userdata($data);
            $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
            $this->template->write_view('content', 'sponsors/page_edit', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $total_permission = $this->Sponsors_model->get_permission_list();
                $this->data['total_permission'] = $total_permission;
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function delete_sponsors($Event_id, $id)
    {
        $sponsors = $this->Sponsors_model->delete_sponsors($id, $Event_id);
        $this->session->set_flashdata('sponsors_data', 'Deleted');
        redirect("Sponsors/index/" . $Event_id);
    }
    public function mass_upload_page($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        if ($user[0]->Role_name == 'Client')
        {
            $Organisor_id = $user[0]->Id;
        }
        else
        {
            $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        }
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'sponsors/mass_upload_page', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function download_template_csv($eid)
    {
        $this->load->helper('download');
        $filename = "sponsors_demo.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Sponsors Name";
        $header[] = "Sponsor Type";
        $header[] = "Company Name";
        $header[] = "Description";
        $header[] = "Website Url";
        $header[] = "Facebook Url";
        $header[] = "Twitter Url";
        $header[] = "Linkedin Url";
        $header[] = "Instagram Url";
        $header[] = "Youtube Url";
        $header[] = "Company Logo";
        $header[] = "Images";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $data['Sponsors_name'] = "test sponser";
        $data['Sponsors_type'] = "Sponsors Type Code";
        $data['Company_name'] = "test pvt ltd";
        $data['Description'] = "sponsors policy Description";
        $data['website_url'] = "https://www.website.com";
        $data['facebook_url'] = "https://www.facebook.com";
        $data['twitter_url'] = "https://www.twitter.com";
        $data['linkedin_url'] = "https://www.linkedin.com";
        $data['instagram_url'] = "https://www.instagram.com";
        $data['youtube_url'] = "https://www.youtube.com";
        $data['company_logo'] = "Company Logo Images Link";
        $data['Images'] = "Images Link for e.g(https://1.png,http://2.png)";
        fputcsv($fp, $data);
    }
    public function upload_csv_sponsors($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        if (pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION) == "csv")
        {
            $file = $_FILES['csv']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            while (($datacsv = fgetcsv($handle, 0, ",")) !== FALSE)
            {
                if ($find_header != '0' && count($datacsv) > 1)
                {
                    $sponsors_data['sponsors_array']['Organisor_id'] = $event[0]['Organisor_id'];
                    $sponsors_data['sponsors_array']['Event_id'] = $id;
                    $sponsors_data['sponsors_array']['Sponsors_name'] = $datacsv[0];
                    if (!empty($datacsv[1]))
                    {
                        $tid = $this->Sponsors_model->get_sponsors_type_id_by_type_code($datacsv[1], $id);
                        $sponsors_data['sponsors_array']['st_id'] = $tid;
                    }
                    $sponsors_data['sponsors_array']['Company_name'] = $datacsv[2];
                    $sponsors_data['sponsors_array']['Description'] = $datacsv[3];
                    $sponsors_data['sponsors_array']['website_url'] = $datacsv[4];
                    $sponsors_data['sponsors_array']['facebook_url'] = $datacsv[5];
                    $sponsors_data['sponsors_array']['twitter_url'] = $datacsv[6];
                    $sponsors_data['sponsors_array']['linkedin_url'] = $datacsv[7];
                    $sponsors_data['sponsors_array']['instagram_url'] = $datacsv[8];
                    $sponsors_data['sponsors_array']['youtube_url'] = $datacsv[9];
                    if (!empty($datacsv[10]))
                    {
                        $logo = "company_logo_" . uniqid() . '.png';
                        copy($datacsv[10], "./assets/user_files/" . $logo);
                        $sponsors_data['sponsors_array']['company_logo'] = $logo;
                    }
                    if (!empty($datacsv[11]))
                    {
                        $img_arr = explode(",", $datacsv[11]);
                        $img_nm = array();
                        foreach($img_arr as $key => $value)
                        {
                            $images = "banner_" . uniqid() . '.png';
                            $img_nm[$key] = $images;
                            copy($value, "./assets/user_files/" . $images);
                        }
                        $sponsors_data['sponsors_array']['Images'] = $img_nm;
                    }
                    $sponsors_id = $this->Sponsors_model->add_user_as_sponsors($sponsors_data);
                }
                $find_header++;
            }
            $this->session->set_flashdata('sponsors_data', ' Added');
            redirect(base_url() . 'Sponsors/index/' . $id);
        }
        else
        {
            $this->session->set_flashdata('sponsors_error_data', ' please Upload csv Files.');
            redirect(base_url() . 'Sponsors/mass_upload_page/' . $id);
        }
    }
    public function add_sponsor_type($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            $type_data['type_name'] = $this->input->post('sponsor_type_name');
            $type_data['type_position'] = $this->input->post('position_in_directory');
            $type_data['type_color'] = $this->input->post('custom_color_picker');
            $type_data['event_id'] = $id;
            $type_data['created_date'] = date('Y-m-d H:i:s');
            $type_id = $this->Sponsors_model->save_sponsor_type($type_data);
            $this->session->set_flashdata('ex_in_data', 'Type Added');
            redirect(base_url() . 'Sponsors/index/' . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'sponsors/add_sponsor_type', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Sponsors_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function sponsors_type_edit($id, $tid)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post())
        {
            $type_data['type_name'] = $this->input->post('sponsor_type_name');
            $type_data['type_position'] = $this->input->post('position_in_directory');
            $type_data['type_color'] = $this->input->post('custom_color_picker');
            $type_id = $this->Sponsors_model->update_sponsor_type($type_data, $id, $tid);
            $this->session->set_flashdata('ex_in_data', 'Type Updated');
            redirect(base_url() . 'Sponsors/index/' . $id);
        }
        $edit_type_data = $this->Sponsors_model->get_edit_sponsors_type($id, $tid);
        $this->data['type_data'] = $edit_type_data;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'sponsors/add_css', $this->data, true);
        $this->template->write_view('content', 'sponsors/edit_sponsor_type', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Sponsors_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'sponsors/add_js', $this->data, true);
        $this->template->render();
    }
    public function delete_sponsors_type($eid, $tid)
    {
        $this->Sponsors_model->remove_sponsors_type($eid, $tid);
        $this->session->set_flashdata('ex_in_data', 'Type Deleted');
        redirect(base_url() . 'Sponsors/index/' . $eid);
    }
    public function add_group($id)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $sponsors = $this->Sponsors_model->get_sponsors_list($id);
        $this->data['sponsors'] = $sponsors;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 43;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, NULL);
            redirect(base_url() . 'Sponsors/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'sponsors/add_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_group($id, $mgid)
    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_group_data($mgid,$id,43);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $sponsors = $this->Sponsors_model->get_sponsors_list($id);
        $this->data['sponsors'] = $sponsors;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 43;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, $mgid);
            redirect(base_url() . 'Sponsors/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'sponsors/edit_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_group($event_id, $group_id)
    {
        $this->Event_template_model->delete_group($group_id,$event_id);
        $this->session->set_flashdata('sponsors_data', 'Group Deleted');
        redirect(base_url() . 'Sponsors/index/' . $event_id);
    }
    public function add_sponsors_categorie($id) //7/26/2018 6:33:15 PM
    {
         $user = $this->session->userdata('current_user');
         $roleid = $user[0]->Role_id;
         $event_id = $id;
         $user_role = $this->event_template_model->get_menu_list($roleid, $id);
         $this->data['users_role'] = $user_role;
         $logged_in_user_id = $user[0]->Id;
         if ($this->input->post())
         {
              $categorie_data['category'] = $this->input->post('categorie_name');
              $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
              $categorie_data['event_id'] = $id;
              $categorie_data['menu_id'] = '7';
              $categorie_data['updated_date'] = date('Y-m-d H:i:s');
              $categorie_data['created_date'] = date('Y-m-d H:i:s');
              $this->Sponsors_model->add_sponsors_categorie($categorie_data);
              $this->session->set_flashdata('sponsor_data', 'Categorie Added');
              redirect(base_url() . 'sponsors/index/' . $id);
         }

         $event = $this->event_model->get_admin_event($id);
         $this->data['event'] = $event[0];
         $this->session->set_userdata($data);
         $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
         $this->template->write_view('content', 'sponsors/add_sponsor_categorie', $this->data, true);
         if ($this->data['user']->Role_name == 'User')
         {
              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
         }
         else
         {
              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
         }

         $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
         $this->template->render();
    }

    public function sponsors_categories_edit($id, $cid) //7/26/2018 6:33:15 PM
    {
         $user = $this->session->userdata('current_user');
         $roleid = $user[0]->Role_id;
         $event_id = $id;
         $user_role = $this->event_template_model->get_menu_list($roleid, $id);
         $this->data['users_role'] = $user_role;
         $logged_in_user_id = $user[0]->Id;
         if ($this->input->post())
         {
              $categorie_data['category'] = $this->input->post('categorie_name');
              $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
              $categorie_data['updated_date'] = date('Y-m-d H:i:s');
              $this->Sponsors_model->update_sponsors_categorie($categorie_data, $cid);
              $this->session->set_flashdata('sponsor_data', ' Categorie Updated');
              redirect(base_url() . 'sponsors/index/' . $id);
         }

         $this->data['categorie_data'] = $this->Sponsors_model->get_sponsors_categories_by_event($id, $cid);
         $event = $this->event_model->get_admin_event($id);
         $this->data['event'] = $event[0];
         $this->session->set_userdata($data);
         $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
         $this->template->write_view('content', 'sponsors/edit_sponsor_categorie', $this->data, true);
         if ($this->data['user']->Role_name == 'User')
         {
              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
         }
         else
         {
              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
         }

         $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
         $this->template->render();
    }

    public function delete_sponsors_categorie($id, $cid) //7/26/2018 6:33:15 PM
    {
         $this->Sponsors_model->delete_sponsors_categorie($cid, $id);
         $this->session->set_flashdata('sponsor_data', 'Categorie Deleted');
         redirect(base_url().'sponsors/index/'.$id);
    }

    public function assign_sponsors_category($eventid)
    {
        $sponsor_id = $this->input->post('sponsors_id');
        $sponsor_categories = $this->input->post('sponsors_categories');
        if (!empty($sponsor_categories))
        {
            foreach($sponsor_id as $key => $value)
            {
                $insert_data['user_id'] = $value;
                $insert_data['keyword'] = $sponsor_categories;
                $this->Sponsors_model->assign_sponsors_categories($insert_data, $eventid);
            }
        }
        echo "success###" . base_url() . 'sponsor/index/' . $eventid;
    }
}
