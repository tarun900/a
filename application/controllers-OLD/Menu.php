<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends FrontendController {

    function __construct() {
        $this->data['pagetitle'] = 'Menu';
        $this->data['smalltitle'] = 'Menu';
        $this->data['breadcrumb'] = 'Menu';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Menu_model');
        $this->load->model('Event_model');
        $this->load->library('upload');
    }

    public function index($data) 
    {

        if ($this->input->post()) 
        {   
            if (!empty($_FILES['Images']['name'][0])) 
            {
                foreach ($_FILES['Images']['name'] as $k => $v) 
                {
                    $v = str_replace(' ', '', $v);

                    if (file_exists("./assets/user_files/" . $v))
                        $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    else
                        $Images[] = $v;
                }

                $this->upload->initialize(array(
                    "file_name" => $Images,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => '*',
                    "max_size" => '10000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));

                if (!$this->upload->do_multi_upload("Images")) 
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', "For Menu Images, " . $error['error']);
                }
            }
            $menudata = $this->Event_model->geteventmenu($this->input->post('event_id'),$this->input->post('menu_id'));

            if(!empty($Images))
            {
                $menu_data['agenda_array']['img'] = $Images[0];
            }
            if($this->input->post('is_feture_product') != '')
            {
                $menu_data['agenda_array']['is_feture_product'] = '1';
            }
            else
            {
                $menu_data['agenda_array']['is_feture_product'] = '0';
            }
            
            $menu_data['agenda_array']['menu_id'] = $this->input->post('menu_id');
            $menu_data['agenda_array']['event_id'] = $this->input->post('event_id');
            $menu_data['agenda_array']['title'] = $this->input->post('title');
            $menu_data['agenda_array']['img_view'] = $this->input->post('img_view');
            $menu_data['agenda_array']['Redirect_url'] = $this->input->post('Redirect_url');
            $agenda_id = $this->Menu_model->add_menu($menu_data);
          
            redirect(ucfirst($menudata[0]->menuurl). $this->input->post('event_id'));
        }
    }

}