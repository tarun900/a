<?php 
/*error_reporting(E_ALL);
ini_set('display_errors','1');*/
include_once (dirname(__FILE__) . "/Apiv3.php");
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Apiv4 extends Apiv3 {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Speaker_model');
        $this->load->model('Agenda_model');
        $this->load->model('Sponsors_model');
        $this->ncrToken = file_get_contents('./assets/ncrToken/ncr.txt');
    }
    public function index( $offset = 0 )
    {   
        // $this->load->library('../controllers/apiv3');
        die("Direct Access Denied..");
    }
    public function VentureFestaddExhi()
    {   
        extract($this->input->post());
        $event_id = '1802';
        $org_id   = '383336';
        
        /*if($username != 'ppma' || $password != '^D6X/8wNn-`d:?g.' || empty($username)  || empty($password))
        {
            $data = array(
                    'success' => false,
                    'message' => 'API access is not allowed.'
                );
            echo json_encode($data);exit; 
        }*/


        if(!empty($Email) && !empty($Company_name) && !empty($Firstname) && !empty($Lastname))
        {
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $user['Firstname'] = $Firstname;
            $user['Lastname'] = $Lastname;
            $user['Company_name'] = $Company_name;
            $user['Email'] = $Email;
            $user['Password'] = $Password ? md5($Password) : md5('123456');
            $user['Organisor_id'] = $org_id;
            $user['Created_date'] = date('Y-m-d H:i:s');
            $user['Active'] = '1';
            $user['updated_date'] = date('Y-m-d H:i:s');
            $user['is_from_api'] = '1';
            $user['Country'] = $this->getCountryIdFromName($Country);
            $user_id = $this->addExhiUser($user,$event_id);

            $ex_data['Event_id'] = $event_id;
            $ex_data['Heading'] = $Company_name;
            $ex_data['Organisor_id'] = $org_id;
            if(!empty($Type))
            {
                $ex_data['et_id'] = $this->getExhiTypeId($event_id,$Type);
            }
            $ex_data['stand_number'] = $stand_number;
            $ex_data['Short_desc'] = $Short_desc;
            $ex_data['Description'] = htmlentities($Description);
            $ex_data['website_url'] = $website_url;
            $ex_data['facebook_url'] = $facebook_url;
            $ex_data['twitter_url'] = $twitter_url;
            $ex_data['linkedin_url'] = $linkedin_url;
            $ex_data['instagram_url'] = $instagram_url;
            $ex_data['youtube_url'] = $youtube_url;
            $ex_data['country_id'] = $this->getCountryIdFromName($Country);
            $this->load->library('upload');
            if (!empty($_FILES['company_logo']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['company_logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("company_logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['company_logo'] = json_encode(array($images_file));
            }

            if (!empty($_FILES['banner_image']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['banner_image']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("banner_image"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['Images'] = json_encode(array($images_file));
            }
            
            $ex_data['user_id'] = $user_id;
            $this->add_exhibitor($ex_data);
            updateModuleDate($event_id,'exhibitor');
            $data = array(
                    'success' => true,
                    'message' => 'Exhibitor Updated Successfully'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                    );
        }
        updateModuleDate($event_id,'exhibitor');   
        echo json_encode($data);exit;
    }
    public function VentureFestaddSpeaker($id)
    {   
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
            
        if(!empty($Firstname) && !empty($Lastname))
        {

            if(empty($Email))
            {
                $Email = $Firstname.'-'.$event_id.'-'.$Organisor_id.'@venturiapps.com';
            }
            else
            {
                if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Email should be valid'
                    );
                    echo json_encode($data);exit;   
                }
            }
            
            $user = $this->getUserByEvent($Email);

            $speaker_data['Salutation'] = $Salutation;
            $speaker_data['Firstname'] = $Firstname;
            $speaker_data['Lastname'] = $Lastname;
            $speaker_data['Email'] = $Email;
            $speaker_data['Password'] =$Password ? md5($Password) : '123456';
            $speaker_data['Title'] = $Title;
            $speaker_data['Company_name'] = $Company_name;
            $speaker_data['Mobile'] = $Mobile;
            $speaker_data['Speaker_desc'] = $Speaker_desc;
                
            $user_link['Website_url'] = $Website_url;
            $user_link['Facebook_url'] = $Facebook_url;
            $user_link['Twitter_url'] = $Twitter_url;
            $user_link['Linkedin_url'] = $Linkedin_url;
            
            if (!empty($_FILES['Logo']['name'][0]))
            {
                $this->load->library('upload');
                 $tempname = explode('.', $_FILES['Logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("Logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $speaker_data['Logo'] = $images_file;
            } 

            if(empty($user))
            {
                $speaker_data['Organisor_id'] = $Organisor_id;
                $speaker_data['Created_date'] = date('Y-m-d H:i:s');
                $speaker_data['Active'] = '1';
                $speaker_data['is_from_api'] = '1';
                $speaker_id = $this->Speaker_model->add_csv_speaker($event_id, $speaker_data, $user_link);
            }
            else
            {
                $speaker_data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Id',$user['Id']);
                $this->db->update('user',$speaker_data);
                $speaker_id =  $user['Id'];
            }
            $this->setUserWithEvent($event_id,$speaker_id,'7',$Organisor_id);
            if(!empty($Type))
            {
                $this->Speaker_model->assign_spear_type($id,$Type,$speaker_id);
            }
            $data = array(
                'success' => true,
                'message' => "Speaker Updated Successfully"
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Paramenters"
            );
        }
        updateModuleDate($event_id,'speaker');   
        echo json_encode($data);exit;
    }
    public function getUserByEvent($email)
    {
        $this->db->where('Email',$email);
        $user = $this->db->get('user')->row_array();
        return $user;
    }
    public function setUserWithEvent($event_id,$user_id,$role_id,$org_id)
    {   
        $where['Event_id'] = $event_id;
        $where['User_id'] = $user_id;
        $this->db->where($where);
        $res = $this->db->get('relation_event_user')->row_array();
        if(empty($res))
        {
            $where['Organisor_id'] = $org_id;
            $this->db->insert('relation_event_user',$where);
        }
    }
    public function VentureFestaddSession()
    {
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
        $cid = '2112';
        if(!empty($start_date) && !empty($start_time) && !empty($end_date) && !empty($end_time) && !empty($heading) && !empty($type))
        {
            if(!validate_date($start_date))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Start date is Invalid'
                );
            }

            if(!validate_time($start_time))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Start time is Invalid'
                );
            }

            if(!validate_date($end_date))
            {
                $data = array(
                    'success' => false,
                    'message' => 'End date is Invalid'
                );
            }

            if(!validate_time($end_time))
            {
                $data = array(
                    'success' => false,
                    'message' => 'End time is Invalid'
                );
            }

            if(!empty($data))
            {
                echo json_encode($data);exit;
            }


            $data['agenda_array']['Organisor_id'] = $Organisor_id;
            $data['agenda_array']['Event_id'] = $event_id;
            $data['agenda_array']['Start_date'] = $start_date;
            $data['agenda_array']['Start_time'] = $start_time;
            $data['agenda_array']['End_date'] = $end_date;
            $data['agenda_array']['End_time'] = $end_time;
            if (!empty($checking_datetime))
            {
                $data['agenda_array']['checking_datetime'] = $checking_datetime;
            }
            $data['agenda_array']['Heading'] = $heading;
            $check = $this->Agenda_model->check_exists_type_name($event_id,$type);

            if (count($check))
            {
                $type_id = $check[0]['type_id'];
            }
            else
            {
                $type_id = $this->Agenda_model->add_session_types($event_id, $type);
            }
            $data['agenda_array']['Types'] = $type_id;
            if (!empty($speaker_email))
            {
                $s_email = explode(",", $speaker_email);
                $s_ids = array();
                foreach($s_email as $key1 => $value1)
                {
                    $s_ids[$key1] = $this->Agenda_model->get_speaker_id_by_email($value1, $event_id);
                }
                $data['agenda_array']['Speaker_id'] = implode(",", $s_ids);
            }
            if (!empty($map_code))
            {
                $data['agenda_array']['Address_map'] = $this->Agenda_model->get_map_id_by_map_code($map_code, $event_id);
            }

            $data['agenda_array']['Agenda_status'] = '1';
            $data['agenda_array']['description'] = $description;
            $data['agenda_array']['Maximum_People'] = $maximum_people;
            $data['agenda_array']['show_places_remaining'] = $show_places_remaining?:0;
            $data['agenda_array']['show_checking_in'] = $show_checking_in?:0;
            $data['agenda_array']['show_rating'] = $show_rating?:0;
            $data['agenda_array']['custom_speaker_name'] = $custom_speaker_name;
            $data['agenda_array']['custom_location'] = $custom_location;
            $data['agenda_array']['allow_clashing'] = $allow_clashing?:0;
            $data['agenda_array']['allow_comments'] = $allow_comments?:0;

            $this->db->where('Heading',$heading);
            $this->db->where('Event_id',$event_id);
            $res = $this->db->get('agenda')->row_array();
            if(empty($res))
            {
                $this->Agenda_model->add_agenda($data, $event_id, $cid);
            }
            else
            {   
                $data['agenda_array']['Id'] = $res['Id'];
                $this->Agenda_model->update_agenda($data,$cid,$event_id);
            }
            $data = array(
                    'success' => true,
                    'message' => 'Agenda Updated Successfully'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                );
        }
        updateModuleDate($event_id,'agenda');   
        echo json_encode($data);
    }

    public function VentureFestaddSponsor()
    {
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
            
        if (!empty($Sponsors_name))
        {
            $sponsors_data['sponsors_array']['Organisor_id'] = $Organisor_id;
            $sponsors_data['sponsors_array']['Event_id'] = $event_id;
            $sponsors_data['sponsors_array']['Sponsors_name'] = $Sponsors_name;
            if (!empty($Sponsors_type))
            {
                $tid = $this->Sponsors_model->get_sponsors_type_id_by_type_code($Sponsors_type, $event_id);
                $sponsors_data['sponsors_array']['st_id'] = $tid;
            }
            $sponsors_data['sponsors_array']['Company_name'] = $Company_name;
            $sponsors_data['sponsors_array']['Description'] = $Description;
            $sponsors_data['sponsors_array']['website_url'] = $website_url;
            $sponsors_data['sponsors_array']['facebook_url'] = $facebook_url;
            $sponsors_data['sponsors_array']['twitter_url'] = $twitter_url;
            $sponsors_data['sponsors_array']['linkedin_url'] = $linkedin_url;
            $sponsors_data['sponsors_array']['instagram_url'] = $instagram_url;
            $sponsors_data['sponsors_array']['youtube_url'] = $youtube_url;


            if (!empty($_FILES['Logo']['name'][0]))
            {
                $this->load->library('upload');
                 $tempname = explode('.', $_FILES['Logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("Logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $sponsors_data['sponsors_array']['company_logo'] = $images_file;
            }

            $this->db->where('Sponsors_name',$Sponsors_name);
            $this->db->where('Event_id',$event_id);
            $res = $this->db->get('sponsors')->row_array();
            if(empty($res))
            {
                $sponsors_id = $this->Sponsors_model->add_user_as_sponsors($sponsors_data);
            }
            else
            {   
                $sponsors_data['sponsors_array']['Id'] = $res['Id'];
                $sponsors_id = $this->Sponsors_model->update_sponsors($sponsors_data);
            }
            $data = array(
                'success' => true,
                'message' => 'Sponsor Updated Successfully'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        updateModuleDate($event_id,'sponsor');   
        echo json_encode($data);
    }

    public function resetNCRToken()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/auth/token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials&scope=c85ea381-8202-4ca1-84b2-ce58e2f5ee74&client_id=E20F3E47-7D82-4C65-B8BB-6DA340FFED1B&client_secret=AITLlearning2019",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 4b813944-8ae5-4485-87c1-de5b19e4264a"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            show_errors();
            $response = json_decode($response,true);
            $file = fopen("./assets/ncrToken/ncr.txt","w");
            echo fwrite($file,$response['access_token']);
            fclose($file);
        }
    }

    public function NCRSessions()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/Sessions?\$expand=TimeSlot,Room,SessionType,Track,PropertyValues/PropertyMetadata",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            $response = filterArray($response,'Enabled','1');
            if($_GET['t'] == '1')
            {	
                j($response[0]);
                j(json_encode($response[0]));
            	echo json_encode($response);exit;
            }
            foreach ($response as $key => $value)
            {   
                if(!empty($value['Code']))
                {   
                    
                    $tmp_desc[] = substr($value['Room']['Name'], 0, strpos($value['Room']['Name'], "("))."<br>";

                    // $tmp_desc[] = $value['Description']."<br>";
                    
                    //Guide
                    $key = array_search('27891', array_column($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = $value['PropertyValues'][$key]['Value']."<br>";
                    
                    $tmp_desc[] = "<ul>";
                    //Take-Away 1
                    $key = array_search('27892', array_column($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";
                    
                    //Take-Away 2
                    $key = array_search('27893', array_column($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";

                    //Take-Away 3
                    $key = array_search('27894', array_column($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";

                    $tmp_desc[] = "</ul>";


                    $tmp_desc[] = "<p>Hot Topics</p>";
                    //hot topics
                    $tmp_desc[] = "<ul>";
                    $key = array_search('27890', array_column($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";
                    $tmp_desc[] = "</ul>";
                
                    $agenda['tmp_id'] = 'learn2018-'.$value['Id'];
                    $agenda['StartDate'] = date('Y-m-d',strtotime(date($value['TimeSlot']['StartTime']['EventTime'])));
                    $agenda['EndDate'] = date('Y-m-d',strtotime(date($value['TimeSlot']['EndTime']['EventTime'])));
                    $agenda['StartTime'] = date('H:i:s',strtotime(date($value['TimeSlot']['StartTime']['EventTime'])));
                    $agenda['EndTime'] = date('H:i:s',strtotime(date($value['TimeSlot']['EndTime']['EventTime'])));
                    $agenda['Name'] = $value['Code'].' : '.$value['Title'];
                    $agenda['Type'] = $value['Track']['Title'];
                    $agenda['Location'] = substr($value['Room']['Name'], 0, strpos($value['Room']['Name'], "("));
                    $agenda['Speakers'] = implode(',',$value['SpeakerOrder']);
                    $agenda['Description'] = implode('',$tmp_desc);
                    unset($tmp_desc);
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://ngapi.hubb.me/api/V1/1950/Attachments?sessionId=".$value['Id'],
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "GET",
                      CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$this->ncrToken,
                        "Cache-Control: no-cache",
                        "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
                      ),
                    ));

                    $doc = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                      echo "cURL Error #:" . $err;
                    } else
                    {   
                        $doc = json_decode($doc,true);
                        $allowed_ext = array('.ppt','.pptx','.doc','.docx','.pdf');
                        $doc = filterArray($doc,'Extension',$allowed_ext);
                        foreach ($doc as $dkey => $dvalue)
                        {
                            $insert_doc[$dkey]['Title'] = $dvalue['Title'];
                            $insert_doc[$dkey]['Link'] = $dvalue['Link'];
                            $insert_doc[$dkey]['SessionId'] = 'learn2018-'.$dvalue['SessionId'];
                            $insert_doc[$dkey]['tmp_id'] = $dvalue['Id'];
                            $insert_doc[$dkey]['type'] = $dvalue['Extension'];
                        }
                        if(!empty($insert_doc))
                        {
                            $this->db->insert_batch('Learn_documents',$insert_doc);
                            unset($insert_doc);
                        }
                    }

                    $fin[] = $agenda;
                    $tmp_agenda = $this->db->where('tmp_id','learn2018-'.$value['Id'])->get('Learn_Agenda')->row_array();
                    if(empty($tmp_agenda))
                    {
                        $this->db->insert('Learn_Agenda',$agenda);
                    }
                    else
                    {
                        $this->db->where($tmp_agenda);
                        $this->db->update('Learn_Agenda',$agenda);
                    }
                }
            }
            j('Data Imported');
        }
    }

    public function NCRDocuments()
    {   
        $event_id = '1756';
        $docs = $this->db->get('Learn_documents')->result_array();
        foreach ($docs as $key => $value)
        {   
            $file_name = clean($value['Title']).time().$value['Type'];
            $path = './assets/user_documents/'.$file_name;
            copy($value['Link'],$path);
            $insert['Event_id'] = $event_id;
            $insert['title'] = $value['Title'];
            $insert['doc_type'] = '0';
            $insert['parent'] = '0';
            $res = $this->db->where($insert)->get('documents')->row_array();
            if(empty($res))
            {
                $this->db->insert('documents',$insert);
                $insert_id = $this->db->insert_id();
                $insert_doc['document_id'] = $insert_id;
                $insert_doc['document_file'] = $file_name;
                $this->db->insert('document_files',$insert_doc);
                
                $where['agenda_code'] = $value['SessionId'];
                $where['Event_id'] = $event_id;
                $update['document_id'] = $insert_id;
                $this->db->where($where);
                $this->db->update('agenda',$update);
            }
            else
            {   
                $where['agenda_code'] = $value['SessionId'];
                $where['Event_id'] = $event_id;
                $update['document_id'] = $res['id'];
                $this->db->where($where);
                $this->db->update('agenda',$update);
            }
            $this->db->where($value)->delete('Learn_documents');
        }
        j('documents updated');

    }

    public function NCRAgenda_add()
    {   

        $org_id=33782;
        $Agenda_id=1997;
        $eid=1756;
        $temp_session=$this->db->select('*')->from('Learn_Agenda')->limit(50)->get()->result_array();

        foreach ($temp_session as $key => $value) 
        {   
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $speaker_name = array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->select('Id,CONCAT_WS(" ",Firstname,Lastname) as Name,Company_name',false)->get_where('user',array('knect_api_user_id'=>'learn2018-'.$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                        $tmp_name[] = $sidarr['Name'];
                        $tmp_name[] = $sidarr['Company_name'];
                        $spr_name = implode(', ',$tmp_name);
                        array_push($speaker_name,$spr_name);
                        unset($tmp_name);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=$value['StartDate'];
            $session_data['Start_time']=$value['StartTime'];
            $session_data['End_date']=$value['EndDate'];
            $session_data['End_time']=$value['EndTime'];
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['custom_location']=$value['Location'];
            $session_data['custom_speaker_name']=implode(',',$speaker_name);
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Learn_Agenda');
        }
        $total_exhi = $this->db->select()->from('Learn_Agenda')->get()->num_rows();
        updateModuleDate($eid,'agenda');
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function NCRSessionsTypes()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/SessionTypes",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            if($_GET['t'] == '1')
            {
                echo $response;exit;
            }
            else
            {
                $response = json_decode($response,true);
                foreach ($response as $key => $value)
                {
                    $insert['type_name'] = $value['Name'];
                    $insert['event_id'] = '1756';
                    $res = $this->db->where($insert)->get('session_types')->row_array();
                    if(!$res)
                    {
                        $this->db->insert('session_types',$insert);
                    }
                }
            }
            j('Agenda session added Successfully');
        }
    }
    public function NCRSpeakerSessions()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/SpeakerSessions",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            j($response);
            foreach ($response as $key => $value)
            {
                j($value);
            }
        }
    }

    public function NCRUsers()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/Users",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            if($_GET['t'] == '1')
            {
                j($response);
            }
            foreach ($response as $key => $value)
            {   
                if(in_array('Speaker', $value['Roles']))
                {   
                    $insert[$key]['tmp_id'] = 'learn2018-'.$value['Id'];
                    $insert[$key]['FirstName'] = $value['FirstName'];
                    $insert[$key]['LastName'] = $value['LastName'];
                    $insert[$key]['Email'] = $value['EmailAddress'].'learn2018-'.$value['Id'];
                    $insert[$key]['JobTitle'] = $value['Company'];
                    $insert[$key]['Company'] = '';
                    $insert[$key]['Biography'] = $value['Biography'];
                    $insert[$key]['Image'] = $value['PhotoLink'];
                }
            }
            $this->db->insert_batch('Learn_Speakers',$insert);
            j('Speaker data updated');
        }
    }
    public function NCRUsersAdd()
    {
        $org_id = 33782;
        $eid = 1756;
        $temp_speakers=$this->db->limit(100)->get('Learn_Speakers')->result_array();

        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id']),'Email' => $value['Email']))->row_array();
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Email'] =  $value['Email'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="learn2018_spr_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            $user_data['is_from_api'] = '1';
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id,'Role_id' => '7'))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Learn_Speakers');
        }
        $total_exhi = $this->db->select()->from('Learn_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        updateModuleDate($eid,'speaker');
        die;
    }
    public function assign_arkansas_agenda()
    {    
        // j('hi');
        //error_reporting(E_ALL);
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('temp_user_agenda_1 uct','uct.user_id = u.Id');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id','1856');
        // $this->db->where('uct.user_id','409870');
        $this->db->group_by('u.Id');
        $this->db->limit(100);
        $res = $this->db->get()->result_array();
        foreach ($res as $key => $value)
        {
            $tmp_user_agenda = $this->db->where('user_id',$value['Id'])->get('users_agenda')->row_array();
            if($tmp_user_agenda)
            {
                $tmp_agenda = explode(',',$tmp_user_agenda['agenda_id']);
                $new_agenda  = explode(',',$value['agenda_id']);
                $fin_agenda = array_merge($tmp_agenda,$new_agenda);
                $this->db->where($tmp_user_agenda);
                $update['agenda_id'] = implode(',',array_unique($fin_agenda));
                $this->db->update('users_agenda',$update);
            }   
            else
            {
                $insert['user_id'] = $value['Id'];
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['updated_date'] = date('Y-m-d H:i:s');
                $this->db->insert('users_agenda',$insert);
            }
            $this->db->where('email',$value['email']);
            $this->db->delete('temp_user_agenda_1');
        }
        j('data added successfully');
    }
    public function deeplink()
    {   
        extract($_GET);
        $deeplink = $h."://m.allintheloop.com?node_main=".$node_main."&node_sub=".$node_sub."&nevent=".$nevent;
        ?>
        <script type="text/javascript">
            (function() {
              var app = {
                launchApp: function() {
                  window.location.replace("<?php echo $deeplink; ?>");
                  this.timer = setTimeout(this.openWebApp, 1000);

                },

                openWebApp: function() {
                  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                  if (/android/i.test(userAgent)) {
                        window.location.replace("https://play.google.com/store/apps/details?id=com.allintheloop&hl=en_US");
                    }

                    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                        window.location.replace("https://itunes.apple.com/app/id1200784294");
                    }
                }
              };
              app.launchApp();
            })();
        </script>
        <?php
    }
    public function learn2018SessionSpeaker()
    {   
        $this->db->where('Event_id','1756');
        $this->db->where('Speaker_id IS NOT NULL');
        $res = $this->db->get('agenda')->result_array();
        $user_id =  array_unique(explode(',',implode(',',array_column($res,'Speaker_id'))));

        #123
        $tmp_users = $this->db->where_in('Id',$user_id)->get('user')->result_array();
        foreach ($tmp_users as $key => $value) {
            $users[$value['Id']] = $value;
        }

        foreach ($res as $key => $value)
        {   
            $speaker_id = explode(',',$value['Speaker_id']);
            foreach ($speaker_id as $key1 => $value1)
            {
                $tmp_name[] = $users[$value1]['Firstname'];
                $tmp_name[] = $users[$value1]['Lastname'];
                $spr_name[] = implode(' ',$tmp_name).' - '.$users[$value1]['Title'];
                unset($tmp_name);
            }
            $update['custom_speaker_name'] = implode(', ',$spr_name);
            $this->db->where('Id',$value['Id']);
            $this->db->update('agenda',$update);
            unset($spr_name);
        }
        j('Custom Spekaer name updated');
    }
    public function clarionAttendeeTmp()
    {   
        $url1 = "https://www.eventsforce.net/appointmentgroup/api/v2/events/225/attendees.json";
        $url2 = "https://www.eventsforce.net/appointmentgroup/api/v2/events/213/attendees.json";
        $header = array("Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ==");
        $data[] = json_decode(j_curl($url1,$header),true)['data']; 
        $data[] = json_decode(j_curl($url2,$header),true)['data']; 
        $data = array_merge($data[0],$data[1]);
        
        foreach($data as $key => $value)
        {   
            $insert['tmp_id'] = 'clarion2018-'.$value['personID'];
            $insert['Firstname'] = $value['firstName'];
            $insert['Lastname'] = $value['lastName'];
            $insert['Email'] = $value['email'];
            $insert['Title'] = $value['jobTitle'];
            $insert['Salutation'] = $value['title'];
            $insert['Company_name'] = $value['company'];
            $insert['detailsURL'] = $value['detailsURL'];
            $tmp[] = $insert;
            unset($insert);
        }
        $this->db->insert_batch('clarion_attendee',$tmp);
        j($tmp);
    }
    public function clarionAttendeeExtraColumn($page_no=1)
    {   

        ini_set('max_execution_time', 0);
        switch ($page_no)
        {
            case 1:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50')->get('clarion_attendee')->result_array();
                break;            
            case 2:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','100')->get('clarion_attendee')->result_array();
                break;
            case 3:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','200')->get('clarion_attendee')->result_array();
                break;
            case 4:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','300')->get('clarion_attendee')->result_array();
                break;
            case 5:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','400')->get('clarion_attendee')->result_array();
                break;
            case 6:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','500')->get('clarion_attendee')->result_array();
                break;
            case 7:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','600')->get('clarion_attendee')->result_array();
                break;
        }
        $opts = array(
                      'http'=>array(
                        'method'=>"GET",
                        'header'=>"Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ=="
                        )
                    );
        $context = stream_context_create($opts);
        foreach ($clarion_attendee as $key => $value)
        {
            $extra_column = json_decode(file_get_contents($value['detailsURL'],false, $context),true)['data'];
            $allKeys = array_column($extra_column['customRegistrationData'], 'key');
            $allValues = array_column($extra_column['customRegistrationData'], 'value');
            $customData = array_combine($allKeys,$allValues);
            $allKeys = array_column($extra_column['customData'], 'key');
            $allValues = array_column($extra_column['customData'], 'value');
            $customData2 = array_combine($allKeys,$allValues);
            $tmp['First Name'] = $extra_column['firstName'];
            $tmp['Clarion Barcode'] = $customData['Clarion barcode'];
            $tmp['Address Line 1'] = $extra_column['addressLine1'];
            $tmp['Address Line 2'] = $extra_column['addressLine2'];
            $tmp['Town'] = $extra_column['town'];
            $tmp['Postcode'] = $extra_column['postCode'];
            $tmp['Dietary Requirements'] = $customData2['Dietary Requirements'];
            $tmp['Dietary Requirements Other'] = $customData2['Dietary Requirements Other'];
            $tmp['Special Access Requirements'] = $customData['Special Access Requirements'];
            $tmp['Office location Clarion'] = $customData['Office location Clarion'];
            $tmp['Other office location'] = $customData['Other office location'];
            $tmp['Do you have a question that youd like to ask Ruth'] = $customData['Do you have a question that youd like to ask Ruth'];
            $tmp['What topics would you like to see covered at the Conference'] = $customData['What topics would you like to see covered at the Conference'];
            $tmp['Clarion event date'] = $customData['Clarion event date'];
            $tmp['Clarion workshop session time'] = $customData['Clarion workshop session time'];
            $tmp['Will you be using the coaches provided'] = $customData['Will you be using the coaches provided'];
            $tmp['PersonID'] = $extra_column['PersonID'];
            $tmp['Do you have an email address'] = $customData['Do you have a email address'];
            $tmp['Clarion Event Date'] = $customData['Clarion event date'];
            $tmp['Clarion Workshop Session Time'] = $customData['Clarion workshop session time'];
            $tmp['First Serve'] = $customData['First serve'];
            $tmp['Preferred Workshop'] = $customData['Please select your preferred workshop'];
            $clarion_attendee[$key]['extra'] = json_encode($tmp);
        }
        $this->db->update_batch('clarion_attendee',$clarion_attendee,'tmp_id');
        j($clarion_attendee);
    }
    public function add_attendees_clarion($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100')->get('clarion_attendee')->result_array();
                break;            
            case 2:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','100')->get('clarion_attendee')->result_array();
                break;
            case 3:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','200')->get('clarion_attendee')->result_array();
                break;
            case 4:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','300')->get('clarion_attendee')->result_array();
                break;
            case 5:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','400')->get('clarion_attendee')->result_array();
                break;
            case 6:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','500')->get('clarion_attendee')->result_array();
                break;
            case 7:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','600')->get('clarion_attendee')->result_array();
                break;
        }
        
        $event_id = 1825;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($clarion_attendee as $key => $value)
        {   
            $this->db->select('u.*,reu.Role_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('u.Email',$value['Email']);
            $role = $this->db->get('user u')->row_array();
            if(empty($role) || $role['Role_id'] != '3')
            {
                unset($role['Role_id']);
                $user = $role;
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                $data['knect_api_user_id'] = $value['tmp_id'];
                unset($data['detailsURL']);
                unset($data['tmp_id']);
                unset($data['extra']);
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                $event_attendee = $this->db->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get('event_attendee')->row_array();
                if(!empty($event_attendee))
                {
                    $update['extra_column'] = $value['extra'];
                    $this->db->where($event_attendee);
                    $this->db->update('event_attendee',$update);
                }
                $this->db->where($value);
                $this->db->delete('clarion_attendee');
            }
        }
        $clarion_attendee = $this->db->get('clarion_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $clarion_attendee . " Refresh Page -> " . round($clarion_attendee / 50 ) . " Time";
        j($msg);
    }

}
/* End of file apiv4.php */
/* Location: ./application/controllers/apiv4.php */