<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Exhibitor extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Your Exhibitors';
          $this->data['smalltitle'] = 'Organise your exhibitor lists and user portals.';
          $this->data['breadcrumb'] = 'Your Exhibitors';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          ini_set('auto_detect_line_endings', true);
          $this->load->model('Event_model');
          $this->load->model('User_model');
          $this->load->model('Setting_model');
          $this->load->model('Profile_model');
          $this->load->model('Speaker_model');
          $this->load->model('Exibitor_model');
          $this->load->model('Agenda_model');
          $this->load->model('Map_model');
          $this->load->model('Event_template_model');
          $this->load->library('email'); 
          $this->load->library('upload');
          $this->load->library('session');

          $event_id=$this->uri->segment(3);
          $user = $this->session->userdata('current_user');
          if($user[0]->Role_id == '4')
               redirect('Forbidden');
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($event_id,$user_events))
          {
               redirect('Forbidden');
          }
          $event_templates = $this->Event_model->view_event_by_id($event_id);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $eventmodule=$this->Event_model->geteventmodulues($event_id);
          $event = $this->Event_model->get_module_event($event_id);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('3',$module) || !in_array('3',$menu_list))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
     }

    /* public function index($id)
     {
          
          $user = $this->session->userdata('current_user');
          $this->data['event_id11'] = $id;
          
          $event = $this->Event_model->get_admin_event($id);
          $this->data['email_template'] = $this->Setting_model->email_template($id);

          $this->data['event'] = $event[0];
          $users = $this->User_model->get_exhib_list_byevent($id);
          $exhibitor_type = $this->Exibitor_model->get_exhibitor_type_list($id);
          $this->data['exhibitor_type'] = $exhibitor_type;
          $this->data['countries'] = $this->Exibitor_model->getAllCountries();
          $this->data['event_countries'] = $this->Exibitor_model->getAllEventCountries($id);

          $event_id = $id;
          $org = $user[0]->Organisor_id;
          $org_email=$this->Event_model->getOrgEmail($org);
          $this->data['org_email']=$org_email;
          $this->data['org'] = $org;
          $rolename = $user[0]->Role_name;
          $menudata = $this->Event_model->geteventmenu($id,3);
          $menu_toal_data = $this->Event_model->get_total_menu($id);

          $exibitor_page_list = $this->Exibitor_model->get_event_wise_exibitor_page_list($id);
          $this->data['exibitor_page_list'] = $exibitor_page_list;
          $this->data['categorie_data']=$this->Exibitor_model->get_exibitor_categories_by_event($id,NULL);
          $this->data['categorie_group']=$this->Exibitor_model->get_category_group($id);
          $exibitor_invited_list = $this->Exibitor_model->get_invited_exibitor($id);
          $this->data['exibitor_invited_list'] = $exibitor_invited_list;
          $this->data['menu_toal_data'] = $menu_toal_data;
          $authorized_user=$this->Exibitor_model->get_authorized_user_data($id);
          $this->data['auth_user_list']=$authorized_user;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
          $acc=$this->Event_template_model->getAccname($id);
          $acc_name=$acc[0]['acc_name'];
          if ($this->input->post())
          {
               foreach ($this->input->post('invites') as $key => $value)
               {
                    $agenda_invited_list = $this->Exibitor_model->get_invited_exibitor($id);
                    foreach ($agenda_invited_list as $key1 => $values1)
                    {
                         $arraydata = array();
                         if ($values1['Emailid'] == $value)
                         {
                              $arraydata = $values1;
                              break;
                         }
                    }
                    $exibitor_data = $this->Exibitor_model->get_invited_exibitor_data($id,$value);
                    $msg = html_entity_decode($this->input->post('msgcontent'));
                    $sent_from_name = $this->input->post('sent_from_name');
                    $tes = urlencode(base64_encode($value));
                    //$msg1  = ucfirst($arraydata['firstname']) . " " . ucfirst($arraydata['lastname']);
                    //$msg1 .= "<br/>".$msg;
                    $msg1 = $msg;
                    $eid=urlencode(base64_encode($id));
                    $stand_number = urlencode(base64_encode($exibitor_data[0]['link_code']));
                    $event_link = base_url().'login/exibitorsetpassword/'.$eid.'/'.$tes.'/'.$stand_number;
                    $msg1 .= '<br/>' . '<a href="'.$event_link.'">Click here</a> to register<br/><br/>';
                    $msg1 .= "<br/><br>".'Thanks'."<br>";
                    $msg1 .= "<br/>".$sent_from_name;
                    $sub = $this->input->post('subject');
                    $sent_from='invite@allintheloop.com';
                    $config['protocol']   = 'smtp';
                    $config['smtp_host']  = 'localhost';
                    $config['smtp_port']  = '25';
                    $config['smtp_user']  = 'invite@allintheloop.com';
                    $config['smtp_pass']  = 'xHi$&h9M)x9m';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from($sent_from, $sent_from_name);
                    $this->email->to($value);
                    $this->email->subject($sub);
                    $this->email->set_mailtype("html");
                    $this->email->message($msg1);
                    $this->email->send();
               }
          }
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          if ($event_id > 0)
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          else
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          $this->data['users'] = $users;
          $inv_status=array();
          $attendee_user_list=$this->User_model->get_attendee_user_list($id,$user[0]->Organisor_id);
          $this->data['attendee_user_list']=$attendee_user_list;
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('css', 'exibitor/css', $this->data, true);
          $this->template->write_view('content', 'exibitor/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'exibitor/js', $this->data, true);
          $this->template->render();
     }*/
     public function invite_individual_exhibitor($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['email_template'] = $this->Setting_model->email_template($id);
          $acc=$this->Event_template_model->getAccname($id);
          $acc_name=$acc[0]['acc_name'];
          if ($this->input->post())
          {
               $msg = html_entity_decode($this->input->post('msgcontent'));
               $sent_from_name = $this->input->post('sent_from_name');
               $tes = urlencode(base64_encode($this->input->post('email_address')));
               $eid=urlencode(base64_encode($id));
               $stand_number = urlencode(base64_encode($this->input->post('invite_stand_number')));
               //$msg1  = ucfirst($this->input->post('first_name'))." ".ucfirst($this->input->post('last_name'));
               //$msg1 .= '<br/>'.$msg;
               $msg1 = $msg;
               $event_link = base_url().'login/exibitorsetpassword/'.$eid.'/'.$tes.'/'.$stand_number;
               $msg1 .= '<br/>' . '<a href="'.$event_link.'">Click here</a> to register<br/><br/>';
               $msg1 .= "<br/><br>".'Thanks'."<br>";
               $msg1 .= "<br/>".$sent_from_name;
               $sub = $this->input->post('subject1');
               $sent_from='invite@allintheloop.com';
               $config['protocol']   = 'smtp';
               $config['smtp_host']  = 'localhost';
               $config['smtp_port']  = '25';
               $config['smtp_user']  = 'invite@allintheloop.com';
               $config['smtp_pass']  = 'xHi$&h9M)x9m';
               $config['mailtype'] = 'html';
               $this->email->initialize($config);
               $this->email->from($sent_from, $sent_from_name);
               $this->email->to($this->input->post('email_address'));
               $this->email->set_mailtype("html");
               $this->email->subject($sub);
               $this->email->message($msg1);
               $this->email->send();
               $this->Exibitor_model->addinvite_exibitor($this->input->post('email_address'), $id, $this->input->post('first_name'), $this->input->post('last_name'),$this->input->post('invite_stand_number'));
               $this->session->set_flashdata('ex_in_data', 'Send Invitation');
               redirect('Exhibitor/index/' . $id);
          }
     }
     public function invite_mulitple_exhibitor($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['email_template'] = $this->Setting_model->email_template($id);
          $acc=$this->Event_template_model->getAccname($id);
          $acc_name=$acc[0]['acc_name'];
          if ($this->input->post())
          {

               $string = " ";
               if ($_FILES['csv']['tmp_name'] != "")
               {
                    $file = $_FILES['csv']['tmp_name'];
                    $handle = fopen($file, "r");
                    $find_header = 0;

                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
                    {    
                        $find_header++;
                        if( $find_header > 1 ) 
                        {
                              
                              $attandedata = array();
                              $msg = $this->input->post('msg');
                              $sent_from_name = $this->input->post('sent_from_name');
                              $tes   = urlencode(base64_encode($data[0]));
                              //$msg1 = ucfirst($data[0])." ".ucfirst($data[1]);
                              $eid=urlencode(base64_encode($id));
                              $stand_number = urlencode(base64_encode($data[3]));
                              //$msg1 .= '<br/>'.$msg;
                              $msg1 = $msg;
                              $event_link = base_url().'login/exibitorsetpassword/'.$eid.'/'.$tes.'/'.$stand_number;
                              $msg1 .= '<br/>' . '<a href="'.$event_link . '/' . $tes.'">Click here</a> to register<br/><br/>';
                              $msg1 .= "<br/><br>".'Thanks'."<br>";
                              $msg1 .= "<br/>".$sent_from_name;
                              $sub = $this->input->post('subject');
                              if($sub=='')
                              {
                                $sub =  $email_template[0]['Subject'];
                              }
                              $sent_from='invite@allintheloop.com';
                              $config['protocol']   = 'smtp';
                              $config['smtp_host']  = 'localhost';
                              $config['smtp_port']  = '25';
                              $config['smtp_user']  = 'invite@allintheloop.com';
                              $config['smtp_pass']  = 'xHi$&h9M)x9m';
                              $config['mailtype'] = 'html';
                              $this->email->initialize($config);
                              $this->email->from($sent_from, $sent_from_name);
                              $this->email->to($data[0]);
                              $this->email->subject($sub);
                              $this->email->message($msg1);
                              $this->email->set_mailtype("html");
                              $this->email->send();
                              $this->Exibitor_model->addinvite_exibitor($data[0], $id, $data[1], $data[2],$data[3]);
                         }
                    }
                    $this->session->set_flashdata('ex_in_data', 'Send Invitation');
                    redirect('Exhibitor/index/' . $id);
               }
          }
     }
     public function add_page($id,$user_id)
     {
          $user = $this->session->userdata('current_user');
          $users = $this->User_model->get_exhib_list_byevent($id);
          $exist = $this->Exibitor_model->get_exibitor_by_id($id,$user_id);
          if($exist)
               return redirect('Forbidden');
          
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $this->data['user_list'] = $users;
          if ($user[0]->Role_name == 'Client')
          {

               $Organisor_id = $user[0]->Id;
          }
          else
          {
               $Organisor_id = $user[0]->Organisor_id;
          }
          $logged_in_user_id = $user[0]->Id;
          $exhibitor_category = $this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exhibitor_category'] = $exhibitor_category;
       
          if ($this->input->post())
          {
               if(empty($this->input->post('company_logo_crop_data_textbox')))
               {
                    if (!empty($_FILES['company_logo']['name'][0]))
                    {

                         $tempname = explode('.', $_FILES['company_logo']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                                 "file_name" => $images_file,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => '*',
                                 "max_size" => '100000',
                                 "max_width" => '5000',
                                 "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("company_logo"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', $error['error']);
                         }
                    }
               }
               else
               {
                    $img=$_POST['company_logo_crop_data_textbox'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file = strtotime(date("Y-m-d H:i:s"))."_company_crop_logo_image.png"; 
                    $filepath = "./assets/user_files/".$images_file; 
                    file_put_contents($filepath, $unencodedData); 
               }
               if(empty($this->input->post('company_banner_crop_data_textbox')))
               {
                   if (!empty($_FILES['banner_Images']['name']))
                    {
                         /*foreach ($_FILES['Images']['name'] as $k => $v)
                         {
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                             
                         }*/
                         $tempname = explode('.', $_FILES['banner_Images']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                                 "file_name" => $images_file1,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => '*',
                                 "max_size" => '100000',
                                 "max_width" => '5000',
                                 "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("banner_Images"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $Images=array($images_file1);
                    }
               }
               else
               {
                    $img=$_POST['company_banner_crop_data_textbox'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s"))."_company_crop_banner_image.png"; 
                    $filepath = "./assets/user_files/".$images_file1; 
                    file_put_contents($filepath, $unencodedData); 
                    $Images=array($images_file1);
               }

               $data['exibitor_array']['Organisor_id'] = $Organisor_id;
               $data['exibitor_array']['user_id']=$user_id;
               $data['exibitor_array']['Event_id'] = $id;
               if(!empty($this->input->post('exhibitor_type')))
               {
                    $data['exibitor_array']['et_id']=$this->input->post('exhibitor_type');
               }
               else
               {
                    $data['exibitor_array']['et_id']=NULL;    
               }
               if(!empty($this->input->post('exhibitor_group')))
               {
                    $data['exibitor_array']['eg_id']=$this->input->post('exhibitor_group');
               }
               else
               {
                    $data['exibitor_array']['eg_id']=NULL;    
               }
               $data['exibitor_array']['Heading'] = $this->input->post('Heading');
               $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
               $data['exibitor_array']['Description'] = $this->input->post('Description');
               $data['exibitor_array']['Images'] = $Images;
               $data['exibitor_array']['website_url'] = $this->input->post('website_url');
               $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['exibitor_array']['youtube_url'] = $this->input->post('youtube_url');
               $data['exibitor_array']['instagram_url'] = $this->input->post('instagram_url');
               $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
               $data['exibitor_array']['main_contact_name'] = $this->input->post('main_contact_name');
               $data['exibitor_array']['main_email_address'] = $this->input->post('main_email_address');
               $data['exibitor_array']['Address_map'] = $this->input->post('Address_map');
               $data['exibitor_array']['link_user']=implode(",",$this->input->post('attendee_id'));
               $data['exibitor_array']['stand_number'] = $this->input->post('stand_number');
               $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
               $data['exibitor_array']['HQ_phone_number'] = $this->input->post('HQ_phone_number');     
               $data['exibitor_array']['email_address'] = $this->input->post('email_address');
               $data['exibitor_array']['company_logo'] = $images_file;
               $data['exibitor_array']['country_id'] = $this->input->post('country_id');
               $data['exibitor_array']['sponsored_category'] = implode(",",$this->input->post('sponsored_category'));

               $exibitor_id = $this->Exibitor_model->add_user_as_exibitor($data);
               $this->session->set_flashdata('exibitor_data', ' Added');
               redirect("Exhibitor/page_edit/" . $id.'/'.$exibitor_id);
          }
          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;
          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;

          $countrylist = $this->Profile_model->countrylist();
          $this->data['countrylist'] = $countrylist;

          $exhibitor_type = $this->Exibitor_model->get_exhibitor_type_list($id);
          $this->data['exhibitor_type'] = $exhibitor_type;

          $this->data['attendee_user']=$this->Exibitor_model->get_all_event_attendee_user_list($id);

          $exhibitor_group = $this->Exibitor_model->get_exhibitor_group_list($id);
          $this->data['exhibitor_group'] = $exhibitor_group;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->data['event_countries_dropdown'] = $this->Exibitor_model->getEventCountriesDropDown($id);
          $agenda_list = $this->Agenda_model->get_agenda_list($id);
          $this->data['agenda_list'] = $agenda_list;
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_page', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();

     }
     public function exist($id)
     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Id;

          $user_list = $this->User_model->get_all_users($id);
          $this->data['user_list'] = $user_list;
          $this->data['Event_id'] = $Event_id;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          if ($this->input->post())
          {
               $data['user_array']['Event_id'] = $this->input->post('Event_id');
               $data['user_array']['User_id'] = $this->input->post('User_id');
               $data['user_array']['Role_id'] = $this->input->post('rolename');
               $user_id = $this->User_model->add_exist_user($data);
               $this->session->set_flashdata('user_data', 'Added');
               redirect("Exhibitor/index/" . $Event_id);
          }

          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/exist', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = '0')
     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          $this->data['users'] = $this->User_model->get_user_list($id);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function page_edit($id,$eid)
     {

          $user = $this->session->userdata('current_user');


          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          $users = $this->User_model->get_exhib_list_byevent($id);
         
          $this->data['user_list'] = $users;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $exhibitor_type = $this->Exibitor_model->get_exhibitor_type_list($id);
          $this->data['exhibitor_type'] = $exhibitor_type;

          $exhibitor_group = $this->Exibitor_model->get_exhibitor_group_list($id);
          $this->data['exhibitor_group'] = $exhibitor_group;

          $exhibitor_category = $this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exhibitor_category'] = $exhibitor_category;

          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;     
          $exibitor_list = $this->Exibitor_model->get_exibitor_list($id);
          $this->data['exibitor_list'] = $exibitor_list;
          $this->data['attendee_user']=$this->Exibitor_model->get_all_event_attendee_user_list($id);

          $exibitor_by_id = $this->Exibitor_model->get_exibitor_by_id($id,$eid);
          $this->data['exibitor_by_id'] = $exibitor_by_id;

          $mapid=$exibitor_by_id[0]['Address_map'];
          $this->data['mapdata']=$this->Map_model->getmapdata_by_mapid($mapid);
          
          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Exhibitor');
               }

               if ($this->input->post())
               {
                    if(empty($this->input->post('company_logo_crop_data_textbox')))
                    {
                         if (!empty($_FILES['company_logo']['name']))
                         {

                              $tempname = explode('.', $_FILES['company_logo']['name']);
                              $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $images_file = $tempname_imagename . "." . $tempname[1];

                              $this->upload->initialize(array(
                                      "file_name" => $images_file,
                                      "upload_path" => "./assets/user_files",
                                      "allowed_types" => '*',
                                      "max_size" => '100000',
                                      "max_width" => '5000',
                                      "max_height" => '5000'
                              ));

                              if (!$this->upload->do_multi_upload("company_logo"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', $error['error']);
                              }
                         }
                    }
                    else
                    {
                         $img=$_POST['company_logo_crop_data_textbox'];
                         $filteredData=substr($img, strpos($img, ",")+1); 
                         $unencodedData=base64_decode($filteredData);
                         $images_file = strtotime(date("Y-m-d H:i:s"))."_company_crop_logo_image.png"; 
                         $filepath = "./assets/user_files/".$images_file; 
                         file_put_contents($filepath, $unencodedData); 
                    }

                    if(empty($this->input->post('company_banner_crop_data_textbox')))
                    {
                        if (!empty($_FILES['banner_Images']['name']))
                         {
                              $tempname = explode('.', $_FILES['banner_Images']['name']);
                              $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $images_file1 = $tempname_imagename . "." . $tempname[1];
                              $this->upload->initialize(array(
                                      "file_name" => $images_file1,
                                      "upload_path" => "./assets/user_files",
                                      "allowed_types" => '*',
                                      "max_size" => '100000',
                                      "max_width" => '5000',
                                      "max_height" => '5000'
                              ));

                              if (!$this->upload->do_multi_upload("banner_Images"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', "" . $error['error']);
                              }
                              $Images=array($images_file1);
                         }
                    }
                    else
                    {
                         $img=$_POST['company_banner_crop_data_textbox'];
                         $filteredData=substr($img, strpos($img, ",")+1); 
                         $unencodedData=base64_decode($filteredData);
                         $images_file1 = strtotime(date("Y-m-d H:i:s"))."_company_crop_banner_image.png"; 
                         $filepath = "./assets/user_files/".$images_file1; 
                         file_put_contents($filepath, $unencodedData); 
                         $Images=array($images_file1);
                    }
                    $data['exibitor_array']['Id'] = $this->uri->segment(4);
                    $data['exibitor_array']['Organisor_id'] = $logged_in_user_id;
                    $data['exibitor_array']['Event_id'] = $id;
                    if(!empty($this->input->post('exhibitor_type')))
                    {
                         $data['exibitor_array']['et_id']=$this->input->post('exhibitor_type');
                    }
                    else
                    {
                         $data['exibitor_array']['et_id']=NULL;    
                    }
                    if(!empty($this->input->post('exhibitor_group')))
                    {
                         $data['exibitor_array']['eg_id']=$this->input->post('exhibitor_group');
                    }
                    else
                    {
                         $data['exibitor_array']['eg_id']=NULL;    
                    }
                    $data['exibitor_array']['user_id']=$this->input->post('ex_user');
                    $data['exibitor_array']['Heading'] = $this->input->post('Heading');
                    $data['exibitor_array']['website_url'] = $this->input->post('website_url');
                    $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
                    $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
                    $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
                    $data['exibitor_array']['youtube_url'] = $this->input->post('youtube_url');
                    $data['exibitor_array']['instagram_url'] = $this->input->post('instagram_url');
                    $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
                    $data['exibitor_array']['main_email_address'] = $this->input->post('main_email_address');
                    $data['exibitor_array']['stand_number'] = $this->input->post('stand_number');
                    $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
                    $data['exibitor_array']['HQ_phone_number'] = $this->input->post('HQ_phone_number');     
                    $data['exibitor_array']['email_address'] = $this->input->post('email_address');
                    $data['exibitor_array']['main_contact_name'] = $this->input->post('main_contact_name');
                    $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
                    $data['exibitor_array']['Description'] = $this->input->post('Description');
                    $data['exibitor_array']['Address_map'] = $this->input->post('Address_map');
                    $data['exibitor_array']['link_user']=implode(",",$this->input->post('attendee_id'));
                    $data['exibitor_array']['Images'] = $Images;
                    $data['exibitor_array']['old_images'] = $this->input->post('old_images');
                    $data['exibitor_array']['company_logo'] = $images_file;
                    $data['exibitor_array']['old_company_logo'] = $this->input->post('old_company_logo');
                    $data['exibitor_array']['country_id'] = $this->input->post('country_id');
                    $data['exibitor_array']['sponsored_category'] = implode(',',$this->input->post('sponsored_category'));
                    $data['exibitor_array']['updated_date'] = date('Y-m-d H:i:s');
                    
                    $this->Exibitor_model->update_exibitor($data);
                    $this->session->set_flashdata('exibitor_data', ' Updated');
                    redirect("Exhibitor/page_edit/" . $id.'/'.$eid);
               }
               $this->data['event_countries_dropdown'] = $this->Exibitor_model->getEventCountriesDropDown($id);

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
               $this->template->write_view('content', 'exibitor/page_edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Exibitor_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
               $this->template->render();
          }
     }
     public function add($eventid)
     {
          $event = $this->Event_model->get_admin_event($eventid);

          $this->data['event'] = $event[0];
          if ($this->input->post())
          {
               foreach ($this->input->post() as $k => $v)
               {
                    if ($k == "email")
                    {
                         $k = "Email";
                         $array_add[$k] = $v;
                    }
                    else if ($k == "password")
                    {
                         $k = "Password";
                         if($eventid=='447')
                         {
                              $this->load->library('RC4');
                              $rc4 = new RC4();
                              $v = $rc4->encrypt($v);
                         }
                         else
                         {
                              $v = md5($v);
                         }
                         $array_add[$k] = $v;
                    }
                    else if ($k == "password_again")
                    {
                         
                    }
                    else
                    {
                         $array_add[$k] = $v;
                    }
               }
               $send_mail=$array_add['send_mail'];
               unset($array_add['send_mail']);
               $user = $this->User_model->add_user($array_add, $eventid);
               if($send_mail=='1')
               {
     		     $this->load->library('email');
                    $this->email->initialize($config);
                    $slug="Add New Exhibitor";
                    $em_template = $this->Setting_model->front_email_template($slug,$eventid);
                    $em_template = $em_template[0];
                    $msg1 = html_entity_decode($em_template['Content']);
                    $patterns = array();
                    $patterns[0] = '/{{FirstNameLastName}}/';
                    $patterns[1] = '/{{AppName}}/';
                    $patterns[2] = '/{{App URL}}/';
                    $patterns[3] = '/{{EmailAddress}}/';
                    $patterns[4] = '/{{Password}}/';
                    $patterns[5] = '/{{Administrator}}/';
                    $replacements = array();
                    $replacements[0] = ucfirst($array_add['Firstname'])." ".ucfirst($array_add['Lastname']);
                    $replacements[1] = $event[0]['Subdomain'];
                    $url=base_url().'?email='.urlencode(base64_encode($array_add['Email']));
                    $replacements[2]="<a href='".$url."' target='_blank'>".$url."</a>";
                    $replacements[3] = $array_add['Email'];
                    $replacements[4] = $this->input->post('password');
                    $replacements[5] = 'Administrator';
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    $this->email->from($em_template['From'],'Event Management');
                    $this->email->to($array_add['Email']);
                    $this->email->set_mailtype("html");
                    $this->email->subject($em_template['Subject']);
                    $this->email->message($msg1);
                    $this->email->send();
               }
               $this->session->set_flashdata('exibitor_data', 'Added');
               redirect("Exhibitor/index/" . $eventid);
          }
          $role_id = $user[0]->Role_id;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$eventid);
          $this->data['users_role']=$user_role;
          $this->data['roles'] = $this->User_model->get_role_list_version2($eventid);
          
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }

     public function delete($Event_id, $id)
     {
          $user = $this->User_model->delete_exibitor_user($Event_id,$id);
          $this->session->set_flashdata('exibitor_data', 'Deleted');
          //redirect("exhibitor/index/" . $Event_id);
     }
     public function delete_exibitor($id, $Event_id)
     {
          $exibitor = $this->Exibitor_model->delete_exibitor($id,$Event_id);
          $this->session->set_flashdata('exibitor_data', 'Deleted');
          //redirect("exhibitor/index/" . $Event_id);
     }

     public function download_csv()
     {
        $this->load->helper('download');
        $filename = "Exhibitors.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "EmailAddress";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Stand Number";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);
        $data['EmailAddress']="test@test.com";
        $data['Firstname']="Test";
        $data['Lastname']="Test2";
        $data['stand_number']="65452315";
        fputcsv($fp, $data);
     }
     public function saveallow_setting_for_exhibitor($id)
     {
	    $ei['event_array']['Id'] = $id;
        if($this->input->post('hide_request_meeting')=='1')
        {
          $ei['event_array']['hide_request_meeting']=$this->input->post('hide_request_meeting');
        }
        else
        {
          $ei['event_array']['hide_request_meeting']='0'; 
        }
        if($this->input->post('allow_msg_user_to_exhibitor')=='1')
        {
          $ei['event_array']['allow_msg_user_to_exhibitor']=$this->input->post('allow_msg_user_to_exhibitor');
        }
        else
        {
          $ei['event_array']['allow_msg_user_to_exhibitor']='0'; 
        }
        if($this->input->post('allow_meeting_exibitor_to_attendee')=='1')
        {
          $ei['event_array']['allow_meeting_exibitor_to_attendee']=$this->input->post('allow_meeting_exibitor_to_attendee');
        }
        else
        {
          $ei['event_array']['allow_meeting_exibitor_to_attendee']='0'; 
        }
        
        $ei['event_array']['link_auto_attendee'] = ($this->input->post('link_auto_attendee')=='1') ?'1':'0';
        
        $this->Event_model->update_admin_event($ei);
        redirect(base_url().'Exhibitor/index/'.$id.'#allow_messaging');
     }
     public function save_exhi_front_settings($id)
     {
         $ei['event_array']['Id'] = $id;
        if($this->input->post('show_topbar_exhi')=='1')
        {
          $ei['event_array']['show_topbar_exhi']=$this->input->post('show_topbar_exhi');
        }
        else
        {
          $ei['event_array']['show_topbar_exhi']='0'; 
        }

        $this->Event_model->update_admin_event($ei);
        redirect(base_url().'Exhibitor/index/'.$id);
     }
     public function add_exhibitor_type($id)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
            $type_data['type_name']=$this->input->post('exhibitor_type_name');
            $type_data['type_position']=$this->input->post('position_in_directory');
            $type_data['type_color']=$this->input->post('custom_color_picker');
            $type_data['event_id']=$id;
            $type_data['created_date']=date('Y-m-d H:i:s');
            $type_id=$this->Exibitor_model->save_exhibitor_type($type_data);
            updateModuleDate($id,'exhibitor');
            $this->session->set_flashdata('ex_in_data', 'Type Added');
            redirect(base_url().'Exhibitor/index/'.$id);
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_exhibitor_type', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function exhibitor_type_edit($id,$tid)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
            $type_data['type_name']=$this->input->post('exhibitor_type_name');
            $type_data['type_position']=$this->input->post('position_in_directory');
            $type_data['type_color']=$this->input->post('custom_color_picker');
            $type_id=$this->Exibitor_model->update_exhibitor_type($type_data,$id,$tid);
            updateModuleDate($id,'exhibitor');
            $this->session->set_flashdata('ex_in_data', 'Type Updated');
            redirect(base_url().'Exhibitor/index/'.$id);
          }
          $edit_type_data=$this->Exibitor_model->get_edit_exhibitors_type($id,$tid);
          $this->data['type_data']=$edit_type_data;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/edit_exhibitor_type', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
            $total_permission = $this->Exibitor_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function delete_exhibitor_type($eid,$tid)
     {
          $this->Exibitor_model->remove_exhibitor_type($eid,$tid);
          $this->session->set_flashdata('ex_in_data', 'Type Deleted');
          redirect(base_url().'Exhibitor/index/'.$eid);
     }
     public function ajax_manage($id)
     {
          $aColumns = array('user.Firstname','ex.Heading','user.Email');
          $sIndexColumn = "Id";
          $sTable = "exibitor";
          $sLimit = "";
          if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
          {
               $sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
                    mysql_real_escape_string( $_GET['iDisplayLength'] );
          }
          if ( isset( $_GET['iSortCol_0'] ) )
          {
               $sOrder = "ORDER BY  ";
               for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
               {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                         $index = ($_GET['iSortCol_'.$i] != "0") ? (int)$_GET['iSortCol_'.$i] -1 : (int)$_GET['iSortCol_'.$i] ;
                         $sOrder .= $aColumns[ intval( $index ) ]."
                              ".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
                    }
               }
               
               $sOrder = substr_replace( $sOrder, "", -2 );
               if ( $sOrder == "ORDER BY" )
               {
                    $sOrder = "";
               }
          }
          $sWhere = "WHERE `relation_event_user`.`Event_id` = '".$id."' AND `r`.`Id` = 6";
          if ( $_GET['sSearch'] != "" )
          {
               $sWhere .= " AND (";
               for ( $i=0 ; $i<count($aColumns) ; $i++ )
               {
                    $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
               }
               $sWhere = substr_replace( $sWhere, "", -3 );
               $sWhere .= ')';
          }
          for ( $i=0 ; $i<count($aColumns) ; $i++ )
          {
               if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
               {
                    if ( $sWhere == "" )
                    {
                         $sWhere = "WHERE ";
                    }
                    else
                    {
                         $sWhere .= " AND ";
                    }
                    $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
               }
          }
          /*$sQuery = "
               SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
               FROM   $sTable
               $sWhere
               $sOrder
               $sLimit
          ";*/

          if($id == '694')
          {
               $sQuery = "SELECT user.Id, user.Firstname, user.Lastname, user.Email, user.Active, 
                         count(ex.user_id) as page_cnt, ex.id as eid, ex.Heading,ep.add_surveys,ep.maximum_Reps,count(er.reps_id) as total_resp
                         FROM (exibitor ex) 
                         JOIN user  ON ex.user_id = user.Id AND ex.Event_id=".$id."
                         JOIN relation_event_user ON user.Id = relation_event_user.User_id 
                         JOIN role r ON relation_event_user.Role_id = r.Id 
                         Left join exibitor_premission ep on ex.event_id=".$id." And ep.user_id=user.Id 
                         LEFT Join exhibitor_representatives er on er.exibitor_user_id=user.Id and er.event_id=".$id."
                         $sWhere
                         GROUP BY ex.Id 
                         ".$sOrder." ".$sLimit;
               $rResult = $this->db->query($sQuery)->result_array();

               $sQuery1 = "SELECT user.Id, user.Firstname, user.Lastname, user.Email, user.Active, 
                         count(ex.user_id) as page_cnt, ex.id as eid, ex.Heading,ep.add_surveys,ep.maximum_Reps,count(er.reps_id) as total_resp
                         FROM (exibitor ex) 
                         JOIN user  ON ex.user_id = user.Id AND ex.Event_id=".$id."
                         JOIN relation_event_user ON user.Id = relation_event_user.User_id 
                         JOIN role r ON relation_event_user.Role_id = r.Id 
                         Left join exibitor_premission ep on ex.event_id=".$id." And ep.user_id=user.Id 
                         LEFT Join exhibitor_representatives er on er.exibitor_user_id=user.Id and er.event_id=".$id."
                         $sWhere
                         GROUP BY ex.Id 
                         ".$sOrder;
          }
          else
          {
               $sQuery = "SELECT `user`.`Id`, `user`.`Firstname`, `user`.`Lastname`, `user`.`Email`, `user`.`Active`, count(ex.user_id) as page_cnt, `ex`.`id` as eid, `ex`.`Heading`,ep.add_surveys,ep.maximum_Reps,count(er.reps_id) as total_resp FROM (`user`) JOIN `relation_event_user` ON `user`.`Id` = `relation_event_user`.`User_id` JOIN `role` r ON `relation_event_user`.`Role_id` = `r`.`Id` LEFT JOIN `exibitor` ex ON `ex`.`user_id` = `user`.`Id` AND ex.Event_id=".$id." Left join `exibitor_premission` ep on ex.event_id=".$id." And ep.user_id=user.Id LEFT Join exhibitor_representatives er on er.exibitor_user_id=user.Id and er.event_id=".$id." $sWhere GROUP BY `user`.`Id` ".$sOrder." ".$sLimit;
               $rResult = $this->db->query($sQuery)->result_array();

               $sQuery1 = "SELECT `user`.`Id`, `user`.`Firstname`, `user`.`Lastname`, `user`.`Email`, `user`.`Active`, count(ex.user_id) as page_cnt, `ex`.`id` as eid, `ex`.`Heading` FROM (`user`) JOIN `relation_event_user` ON `user`.`Id` = `relation_event_user`.`User_id` JOIN `role` r ON `relation_event_user`.`Role_id` = `r`.`Id` LEFT JOIN `exibitor` ex ON `ex`.`user_id` = `user`.`Id` AND ex.Event_id=".$id." $sWhere GROUP BY `user`.`Id`";
          }
       
          
          
          $rResultTotal2 = $this->db->query($sQuery1)->result_array();
          $iTotal = count($rResultTotal2);
          $output = array(
               "sEcho" => intval($_GET['sEcho']),
               "iTotalRecords" => $iTotal,
               "iTotalDisplayRecords" => $iTotal,
               "aaData" => array()
          );
          if(empty($_GET['iDisplayStart']))
          {
               $intCnt = 1;
          }
          else
          {
               $intCnt = $_GET['iDisplayStart']+1;
          }
          if(!empty($rResult))
          {
               foreach($rResult as $intKey=>$strVal)
               {
                    $row = array();
                    $row[0]="<input type='checkbox' name='exiuser_ids[]' value='".$strVal['Id']."' id='exibitor_user_id_".$strVal['Id']."' class='foocheck'><label for='exibitor_user_id_".$strVal['Id']."'></label>";
                    $row[1]    = $strVal['Firstname'];
                    $linkurl=base_url().'profile/update/'.$strVal['Id'].'/'.$id;
                    $row[2] = "<a href='".$linkurl."'>".$strVal['Heading']."</a>";
                    $row[3] = $strVal['Email'];
                    //$row[4] = $strVal['add_surveys']=='1' ? 'Yes' : 'No';
                    //$row[5] = $strVal['total_resp'];
                    //$row[6] = '<input type="text" id="maximum_Reps__'.$strVal['Id'].'" class="maximum_Reps" name="maximum_Reps" value="'.$strVal['maximum_Reps'].'">';
                    $row[4] = '<a href="'.$linkurl.'" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Profile"><i class="fa fa-share"></i></a>
                         <a href="javascript:;" onclick="delete_user('.$strVal['Id'].','.$id.')" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>';
                    if($strVal['page_cnt']==0) {
                         $link=base_url().'exhibitor/add_page/'.$id.'/'.$strVal['Id'];
                         $row[5] = '<a style="" class="btn btn-primary"  href="'.$link.'"><i class="fa fa-plus"></i>Add Content</a>';
                    }
                    else{
                         $link=base_url().'exhibitor/page_edit/'.$id.'/'.$strVal['eid'];
                         $row[5] = '<a href="'.$link.'" class="btn btn-blue" data-placement="top" data-original-title="Edit"><i class="fa fa-edit">Edit Content</i></a>';
                    }     
                    $output['aaData'][] = $row;
                    $intCnt++;
               }
          }
          echo json_encode($output);
     }
     public function mass_upload_page($id)
     {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;
          $this->data['user_list'] = $users;
        if ($user[0]->Role_name == 'Client')
        {

          $Organisor_id = $user[0]->Id;
        }
        else
        {
          $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        }
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
        $this->template->write_view('content', 'exibitor/mass_upload_page', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
          $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
        $this->template->render();
     }
     public function download_template_csv($eid)
     {
        $this->load->helper('download');
        $filename = "exibitor_demo.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Password";
        $header[] = "Exhibitor Name";
        $header[] = "Exhibitor Type";
        $header[] = "Stand Number";
        $header[] = "Keywords";
        $header[] = "Description";
        $header[] = "Website Url";
        $header[] = "Facebook Url";
        $header[] = "Twitter Url";
        $header[] = "Linkedin Url";
        $header[] = "Instagram Url";
        $header[] = "Youtube Url";
        $header[] = "Company Logo";
        $header[] = "Images";
        $header[] = "Country name";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);
        $data['Firstname']="test";
        $data['Lastname']="demo";
        $data['Email']="test@gmail.com";
        $data['Password']="123456";
        $data['Sponsors_name']="test exhibitor";
        $data['Sponsors_type']="exhibitor Type Code";
        $data['Stand Number'] = "15486";
        $data['Keywords']="test,pvt,ltd";
        $data['Description']="sponsors policy Description";
        $data['website_url']="https://www.website.com";
        $data['facebook_url']="https://www.facebook.com";
        $data['twitter_url']="https://www.twitter.com";
        $data['linkedin_url']="https://www.linkedin.com";
        $data['instagram_url']="https://www.instagram.com";
        $data['youtube_url']="https://www.youtube.com";
        $data['company_logo']="Company Logo Images Link";
        $data['Images']="Images Link for e.g(https://1.png,http://2.png)";
        $data['Country name']="Argentina";
        fputcsv($fp, $data);
     }
     public function upload_csv_sponsors($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          if (pathinfo($_FILES['csv']['name'],PATHINFO_EXTENSION)=="csv")
          {
               $file = $_FILES['csv']['tmp_name'];
               $handle = fopen($file, "r");
               $find_header = 0;
               while (($datacsv = fgetcsv($handle,0,",")) !== FALSE)
               {
                    if($find_header!='0' && count($datacsv)>1) 
                    {
                         if(!empty($datacsv[2]))
                         {
                              $ex_user['Firstname']=$datacsv[0];
                              $ex_user['Lastname']=$datacsv[1];
                              $ex_user['Email']=$datacsv[2];
                              $ex_user['Password']=md5($datacsv[3]);
                              $ex_user['Company_name']=$datacsv[4];
                              $ex_user['Organisor_id']=$event[0]['Organisor_id'];
                              $ex_user['Created_date']=date('Y-m-d H:i:s');
                              $ex_user['Active']='1';
                              $ex_user['updated_date']=date('Y-m-d H:i:s');

                              $user_id=$this->Exibitor_model->add_user_from_csv($ex_user,$id);
                              $ex_data['Organisor_id']=$event[0]['Organisor_id'];
                              $ex_data['Event_id']=$id;
                              $ex_data['Heading']=$datacsv[4];
                              if(!empty($datacsv[5]))
                              {
                                   $et_id=$this->Exibitor_model->get_exibitor_type_id_by_type_code($datacsv[5],$id);
                                   $ex_data['et_id']=$et_id;
                              }
                              $ex_data['stand_number']=$datacsv[6];
                              $ex_data['Short_desc']=$datacsv[7];
                              $ex_data['Description']=$datacsv[8];
                              $ex_data['website_url']=$datacsv[9];
                              $ex_data['facebook_url']=$datacsv[10];
                              $ex_data['twitter_url']=$datacsv[11];
                              $ex_data['linkedin_url']=$datacsv[12];
                              $ex_data['instagram_url']=$datacsv[13];
                              $ex_data['youtube_url']=$datacsv[14];
                              $ex_data['country_id']=$this->Exibitor_model->getCountryIdFromName($datacsv[17]);
                              if(!empty($datacsv[15]))
                              {
                                   /*$logo="company_logo_".uniqid().'.png';
                                   copy($datacsv[15],"./assets/user_files/".$logo);
                                   $ex_data['company_logo']=json_encode(array($logo));*/

                                   $destination="new_company_logo_".uniqid();
                                   $source=str_replace(' ','%20',$datacsv[15]);
                                   $info = getimagesize($source);
                                   $img_size = get_headers($source,'1');
                                   $img_size = $img_size['Content-Length']/1000;

                                   if ($info['mime'] == 'image/jpeg')
                                   { 
                                        $quality = ($img_size > '1000') ? '50' : '100';
                                        $logo=$destination.'.jpeg';
                                        $image = imagecreatefromjpeg($source);
                                        imagejpeg($image, "./assets/user_files/".$destination.'.jpeg', $quality);
                                   }
                                   elseif ($info['mime'] == 'image/gif')
                                   { 
                                        $logo=$destination.'.gif';
                                        $image = imagecreatefromgif($source);
                                        imagegif($image, "./assets/user_files/".$destination.'.gif');
                                   }
                                   elseif ($info['mime'] == 'image/png')
                                   {    
                                        $quality = ($img_size > '1000') ? '5' : '9';
                                        $logo=$destination.'.png';
                                        $image = imagecreatefrompng($source);
                                        $background = imagecolorallocatealpha($image,255,0,255,127);
                                        imagecolortransparent($image, $background);
                                        imagealphablending($image, false);
                                        imagesavealpha($image, true);
                                        imagepng($image, "./assets/user_files/".$destination.'.png',$quality);
                                   }
                                   $ex_data['company_logo']=json_encode(array($logo));
                              }
                              if(!empty($datacsv[16]))
                              {
                                   $img_arr=array_filter(explode(",",$datacsv[16]));
                                   $img_nm=array();
                                   foreach ($img_arr as $key => $value) {
                                      $images="banner_".uniqid().'.png';
                                      $img_nm[$key]=$images;
                                      copy($value,"./assets/user_files/".$images);
                                    }
                                   $ex_data['Images']=json_encode($img_nm);
                              }
                              $ex_data['user_id']=$user_id;
                              $ex_id=$this->Exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
                         }
                    }
                    $find_header++;
               }
               $this->session->set_flashdata('exibitor_data', ' Added');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          else
          {
               $this->session->set_flashdata('exibitor_error_data', ' please Upload csv Files.');
               redirect(base_url().'Exhibitor/mass_upload_page/'.$id);
          }
     }
     public function add_authorized_email($id)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $this->data['user_list'] = $users;
          $Organisor_id = $this->Event_model->get_org_id_by_event($id);
          $logged_in_user_id = $user[0]->Id;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_authorized_email', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function download_template_csv_authorizeduser($eid)
     {
          $this->load->helper('download');
          $filename = "authorized_user.csv";
          $fp = fopen('php://output', 'w');
          $header[] = "Firstname";
          $header[] = "Lastname";
          $header[] = "Email";
          $header[] = "Title";
          $header[] = "Company Name";
          header('Content-type: application/csv');
          header('Content-Disposition: attachment; filename='.$filename);
          fputcsv($fp, $header);
          $data['Firstname']="test";
          $data['Lastname']="testing";
          $data['Email']="testing@gmail.com";
          $data['Title']="Developer";
          $data['Company Name']="test pvt ltd";
          fputcsv($fp, $data);
     }
     public function upload_authorized_user($eid)
     {
          $event = $this->Event_model->get_admin_event($eid);
          if (pathinfo($_FILES['csv_files']['name'],PATHINFO_EXTENSION)=="csv")
          {
               $file = $_FILES['csv_files']['tmp_name'];
               $handle = fopen($file, "r");
               $find_header = 0;
               while (($datacsv = fgetcsv($handle,0,",")) !== FALSE)
               {
                    if($find_header!='0' && !empty($datacsv[2]))
                    {
                         $data['firstname']=$datacsv[0];
                         $data['lastname']=$datacsv[1];
                         $data['Email']=$datacsv[2];
                         $data['title']=$datacsv[3];
                         $data['company_name']=$datacsv[4];
                         $data['Event_id']=$eid;
                         $data['email_type']='1';
                         $this->Exibitor_model->save_authorized_user_data($data);
                    } 
                    $find_header++;     
               }
               $this->session->set_flashdata('ex_in_data', 'Authorized Emails Added');
               redirect(base_url().'Exhibitor/index/'.$eid);
          }
          else
          {
               $this->session->set_flashdata('upload_error_data', ' please Upload csv Files.');
               redirect(base_url().'Exhibitor/add_authorized_email/'.$eid);
          }
     }
     public function edit_authorized_user($id,$aid)
     {
          $auth_user_data=$this->Exibitor_model->get_edit_authorized_user_data_by_id($id,$aid);
          $this->data['auth_user_data']=$auth_user_data;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $this->data['user_list'] = $users;
          $Organisor_id = $this->Event_model->get_org_id_by_event($id);
          $logged_in_user_id = $user[0]->Id;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->data['event_id']=$id;
          if($this->input->post())
          {
               $data['firstname']=$this->input->post('firstname');
               $data['lastname']=$this->input->post('lastname');
               $data['Email']=$this->input->post('Email');
               $data['title']=$this->input->post('title');
               $data['company_name']=$this->input->post('company_name');
               $update=$this->Exibitor_model->update_authorized_user_data_by_id($data,$id,$aid);
               if($update)
               {
                    $this->session->set_flashdata('ex_in_data', 'Authorized User Updated');
                    redirect(base_url().'Exhibitor/index/'.$id);
               }
               else
               {
                    $this->session->set_flashdata('authorized_error_data', 'Email Address Already exists Please Choose Another Email Address.');
                    redirect(base_url().'Exhibitor/edit_authorized_user/'.$id.'/'.$aid);
               }
          }
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/edit_authorized_email', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function check_exists_authuseremail($eid,$aid)
     {
          $check=$this->Exibitor_model->check_already_exists_email_in_authorized($this->input->post('Email'),$eid,$aid);
          if(count($check) > 0)
          {
               echo "error###Email Address Already exists Please Choose Another Email Address.";
          }
          else
          {
               echo "success###";
          }
          die;
     }
     public function delete_authorized_user($eid,$aid)
     {
          $this->Exibitor_model->delete_authorized_email($eid,$aid);
          $this->session->set_flashdata('ex_in_data', 'Authorized User Deleted');
          //redirect(base_url().'exhibitor/index/'.$eid);
     }
     public function add_exhibitor_categorie($id)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
               if(empty($this->input->post('categorie_icons_crop_data')))
               {
                   if (!empty($_FILES['categories_icons']['name']))
                    {
                         $tempname = explode('.', $_FILES['categories_icons']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                            "file_name" => $images_file1,
                            "upload_path" => "./assets/exhibtior_category_icon",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("categories_icons"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $categorie_data['categorie_icon']=$images_file1;
                    }
               }
               else
               {
                    $img=$_POST['categorie_icons_crop_data'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s"))."_categories_icon.png"; 
                    $filepath = "./assets/exhibtior_category_icon/".$images_file1; 
                    file_put_contents($filepath, $unencodedData); 
                    $categorie_data['categorie_icon']=$images_file1;
               }
               $categorie_data['category']=$this->input->post('categorie_name');
               $categorie_data['categorie_keywords']=$this->input->post('Short_desc');
               $categorie_data['event_id']=$id;
               $categorie_data['updated_date']=date('Y-m-d H:i:s');
               $this->Exibitor_model->add_exibitor_categorie($categorie_data);
               $this->session->set_flashdata('ex_in_data', 'Exhibitor Categorie Added');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_exhibitor_categorie', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function exhibitor_categories_edit($id,$cid)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
               if(empty($this->input->post('categorie_icons_crop_data')))
               {
                   if (!empty($_FILES['categories_icons']['name']))
                    {
                         $tempname = explode('.', $_FILES['categories_icons']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                            "file_name" => $images_file1,
                            "upload_path" => "./assets/exhibtior_category_icon",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("categories_icons"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $categorie_data['categorie_icon']=$images_file1;
                    }
               }
               else
               {
                    $img=$_POST['categorie_icons_crop_data'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s"))."_categories_icon.png"; 
                    $filepath = "./assets/exhibtior_category_icon/".$images_file1; 
                    file_put_contents($filepath, $unencodedData); 
                    $categorie_data['categorie_icon']=$images_file1;
               }
               $categorie_data['category']=$this->input->post('categorie_name');
               $categorie_data['categorie_keywords']=$this->input->post('Short_desc');
               $categorie_data['updated_date']=date('Y-m-d H:i:s');

               $this->Exibitor_model->update_exibitor_categorie($categorie_data,$cid);
               $this->session->set_flashdata('ex_in_data', 'Exhibitor Categorie Updated');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $this->data['categorie_data']=$this->Exibitor_model->get_exibitor_categories_by_event($id,$cid);
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/edit_exhibitor_categorie', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function delete_categorie_images($id,$cid)
     {
          $categorie_data=$this->Exibitor_model->get_exibitor_categories_by_event($id,$cid);
          unlink('./assets/exhibtior_category_icon/'.$categorie_data[0]['categorie_icon']);
          $categoriedata['categorie_icon']=NULL;
          $this->Exibitor_model->update_exibitor_categorie($categoriedata,$cid);
          redirect(base_url().'Exhibitor/exhibitor_categories_edit/'.$id.'/'.$cid);
     }
     public function delete_exhibitor_categorie($id,$cid)
     {
          $this->Exibitor_model->remove_exhibitor_categories($cid,$id);
          $this->session->set_flashdata('ex_in_data', 'Exhibitor Categorie Deleted');
          //redirect(base_url().'exhibitor/index/'.$id);
     }
     /*public function upload_csv_sponsors($id)
     {
          $event = $this->Event_model->get_admin_event($id);
          if (pathinfo($_FILES['csv']['name'],PATHINFO_EXTENSION)=="csv")
          {
               $file = $_FILES['csv']['tmp_name'];
               $handle = fopen($file, "r");
               $find_header = 0;
               while (($datacsv = fgetcsv($handle,0,",")) !== FALSE)
               {
                    if($find_header!='0' && count($datacsv)>1) 
                    {
                         if(!empty($datacsv[8]))
                         {
                              $ex_user['Firstname']=$datacsv[7];
                              $ex_user['Email']=$datacsv[8];
                              $ex_user['Password']=md5(trim($datacsv[10]));
                              $ex_user['Company_name']=$datacsv[7];
                              $ex_user['Organisor_id']=$event[0]['Organisor_id'];
                              $ex_user['Created_date']=date('Y-m-d H:i:s');
                              $ex_user['Active']='1';
                              $user_id=$this->Exibitor_model->add_user_from_csv($ex_user,$id);
                              $ex_data['Organisor_id']=$event[0]['Organisor_id'];
                              $ex_data['Event_id']=$id;
                              $ex_data['main_contact_name']=$datacsv[0];
                              $ex_data['main_email_address']=$datacsv[1];
                              $ex_data['linkedin_url']=$datacsv[2];
                              $ex_data['facebook_url']=$datacsv[3];
                              $ex_data['twitter_url']=$datacsv[4];
                              $ex_data['website_url']=$datacsv[5];
                              $ex_data['Description']=$datacsv[6];
                              $ex_data['Heading']=$datacsv[7];
                              $ex_data['stand_number']=$datacsv[9];
                              $ex_data['email_address']=NULL;
                              $ex_data['user_id']=$user_id;
                              $ex_id=$this->Exibitor_model->add_exhibitor_page_data_from_csv($ex_data);
                         }
                    }
                    $find_header++;
               }
               $this->session->set_flashdata('exibitor_data', ' Added');
               redirect(base_url().'exhibitor/index/'.$id);
          }
          else
          {
               $this->session->set_flashdata('exibitor_error_data', ' please Upload csv Files.');
               redirect(base_url().'exhibitor/mass_upload_page/'.$id);
          }
     }*/
     public function add_exibitor_premission($id)
     {
          foreach ($this->input->post('exiuser_ids') as $key => $value) 
          {
               $premission_arr['add_surveys']=$this->input->post('premission');
               $this->Exibitor_model->save_add_surveys_permission($value,$id,$premission_arr);
          }
          echo "Success";die;
     }
     public function save_maximum_Reps($id)
     {
          $maximum_Reps_arr['maximum_Reps']=!empty($this->input->post('maximum_Reps')) ? $this->input->post('maximum_Reps') : NULL;
          $user_id=$this->input->post('user_id');
          $this->Exibitor_model->save_add_surveys_permission($user_id,$id,$maximum_Reps_arr);
          echo "Success";die;
     }
     public function add_parent_categorie($id)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
               if(empty($this->input->post('categorie_icons_crop_data')))
               {
                    if (!empty($_FILES['categories_icons']['name']))
                    {
                         $tempname = explode('.', $_FILES['categories_icons']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                            "file_name" => $images_file1,
                            "upload_path" => "./assets/exhibtior_category_icon",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                         ));
                         if (!$this->upload->do_multi_upload("categories_icons"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $categorie_data['categorie_icon']=$images_file1;
                    }
               }
               else
               {
                    $img=$_POST['categorie_icons_crop_data'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s"))."_categories_icon.png"; 
                    $filepath = "./assets/exhibtior_category_icon/".$images_file1; 
                    file_put_contents($filepath, $unencodedData); 
                    $categorie_data['categorie_icon']=$images_file1;
               }
               $categorie_data['category']=$this->input->post('categorie_name');
               $categorie_data['categorie_keywords']=NULL;
               $categorie_data['event_id']=$id;
               $categorie_data['category_type']='1';
               $category_id=$this->Exibitor_model->add_exibitor_categorie($categorie_data);
               $rel_data=array();
               foreach ($this->input->post('category_ids') as $key => $value) {
                    $rel_data[$key]['parent_category_id']=$category_id;
                    $rel_data[$key]['exibitor_category_id']=$value;
               }
               $this->Exibitor_model->add_parent_categorie_ralation($rel_data);
               $this->session->set_flashdata('ex_in_data', 'Parent Categorie Added');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $exibitor_category=$this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exibitor_category']=$exibitor_category;
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_parent_categorie', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function parent_categories_edit($id,$pcid)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
               if(empty($this->input->post('categorie_icons_crop_data')))
               {    
                   if (!empty($_FILES['categories_icons']['name']))
                    {    
                         $tempname = explode('.', $_FILES['categories_icons']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                            "file_name" => $images_file1,
                            "upload_path" => "./assets/exhibtior_category_icon",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("categories_icons"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $categorie_data['categorie_icon']=$images_file1;
                    }
               }
               else
               {
                    $img=$_POST['categorie_icons_crop_data'];
                    $filteredData=substr($img, strpos($img, ",")+1); 
                    $unencodedData=base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s"))."_categories_icon.png"; 
                    $filepath = "./assets/exhibtior_category_icon/".$images_file1; 
                    file_put_contents($filepath, $unencodedData); 
                    $categorie_data['categorie_icon']=$images_file1;
               }
               $categorie_data['category']=$this->input->post('categorie_name');
               $categorie_data['categorie_keywords']=NULL;
               $categorie_data['category_type'] ='1';
               $this->Exibitor_model->update_exibitor_categorie($categorie_data,$pcid);
               $this->Exibitor_model->update_category_relation($pcid,$this->input->post('category_ids'));
               $this->session->set_flashdata('ex_in_data', 'Parent Categorie Updated');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $this->data['categorie_data']=$this->Exibitor_model->get_exibitor_categories_by_event($id,$pcid);
          $exibitor_category=$this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exibitor_category']=$exibitor_category;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/edit_parent_categorie', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }

     public function save_event_countries($event_id)
     {
          foreach ($this->input->post('countries') as $key => $value) {
               $data['country_id'] = $value;
               $data['event_id'] = $event_id;
               $this->Exibitor_model->saveEventCountries($data,$event_id);
          }
          
          $this->session->set_flashdata('ex_in_data', 'Country settings updated');
          redirect(base_url().'Exhibitor/index/'.$event_id);

     }

     public function save_slider_settings($event_id)
     {
          $data['show_slider'] = ($this->input->post('show_slider') == '1') ? $this->input->post('show_slider') : '0';
          $where['Id'] = $event_id;

          $this->Exibitor_model->updateEvent($data,$where);

          $this->session->set_flashdata('ex_in_data', 'slider settings updated');
          redirect(base_url().'Exhibitor/index/'.$event_id);
     }
     //Monday 30 April 2018 12:56:52 PM IST
     public function add_categorie_group($id) 
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {
               $insert['name']=$this->input->post('name');
               $insert['event_id']=$id;
               $category_id=$this->Exibitor_model->add_categorie_group($insert);
               $rel_data=array();
               foreach ($this->input->post('category_ids') as $key => $value) {
                    $rel_data[$key]['group_id']=$category_id;
                    $rel_data[$key]['c_c_id']=$value;
                    $rel_data[$key]['event_id']=$id;
               }
               $this->Exibitor_model->add_categorie_group_ralation($rel_data);
               $this->session->set_flashdata('ex_in_data', 'Categorie Group Added');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $exibitor_category=$this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exibitor_category']=$exibitor_category;
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/add_categorie_group', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function edit_categorie_group($id,$gid)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;
          if($this->input->post())
          {    
               $update['name']=$this->input->post('name');
               $this->Exibitor_model->update_category_group_relation($gid,$this->input->post('category_ids'));
               $this->Exibitor_model->update_exibitor_categorie_group($update,$gid);
               $this->session->set_flashdata('ex_in_data', 'Categorie Group Updated');
               redirect(base_url().'Exhibitor/index/'.$id);
          }
          $this->data['categorie_data']=$this->Exibitor_model->get_category_group($id,$gid);
          $exibitor_category=$this->Exibitor_model->get_all_exibitor_category_for_assign_parent_category($id);
          $this->data['exibitor_category']=$exibitor_category;
          
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->session->set_userdata($data);
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/edit_categorie_group', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Exibitor_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }
     public function delete_exhibitor_categorie_group($id,$cid)
     {
          $this->Exibitor_model->remove_exhibitor_categories_group($cid,$id);
          $this->session->set_flashdata('ex_in_data', 'Exhibitor Categorie Group Deleted');
          //redirect(base_url().'exhibitor/index/'.$id);
     }

     public function index($id,$limit = 10,$start = 0)
     {
         
          $user = $this->session->userdata('current_user');
          $this->data['event_id11'] = $id;
          $keyword = $sorder = '';
          if($this->input->post('keyword'))
          {
               $keyword = $this->input->post('keyword');
          }
          $this->data['keyword'] = $keyword;
          $this->load->library('pagination');
          $limit_per_page = $limit;
          $this->data['limit_per_page'] = $limit_per_page;
          /*if($limit == 10)
               $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
          else*/
               $start_index = $start;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['email_template'] = $this->Setting_model->email_template($id);

          $this->data['event'] = $event[0];
          $users = $this->User_model->get_exhib_list_byevent($id);
          $exhibitor_type = $this->Exibitor_model->get_exhibitor_type_list($id);
          $this->data['exhibitor_type'] = $exhibitor_type;
          $sWhere = "WHERE `relation_event_user`.`Event_id` = '".$id."' AND `r`.`Id` = 6";
          if($keyword!='')
          {
               $sWhere.=' AND (user.Firstname like "%'.$keyword.'%" OR ex.Heading like "%'.$keyword.'%" OR user.Email like "%'.$keyword.'%")';
          }
          $order_by       = ($_GET['order_by']) ? (($_GET['order_by'] == 'Heading') ? 'ex.'.$_GET['order_by'] : 'user.'.$_GET['order_by']) : '';
          $order          = ($_GET['order']) ? $_GET['order'] : 'asc';

          if($order_by!='')
          {
               $sorder = 'ORDER BY '.$order_by.' '.$order;
          }
          $this->data['exhibitors_list'] = $this->Exibitor_model->get_exhibitors_list($id,$sWhere,$limit_per_page,$start_index,$sorder);

          $total_records = count($this->Exibitor_model->get_exhibitors_list($id,$sWhere,0,0));

          $config['base_url'] = base_url() . 'exhibitor/index/'.$id.'/'.$limit;
          $config['total_rows'] = $total_records;
          $this->data['total_rows'] = $total_records;
          $this->data['start'] = $start;
          $config['per_page'] = $limit_per_page;
          $config["uri_segment"] =  5;
          $this->pagination->initialize($config);
          $this->data["links"] = $this->pagination->create_links();

          $this->data['countries'] = $this->Exibitor_model->getAllCountries();
          $this->data['event_countries'] = $this->Exibitor_model->getAllEventCountries($id);

          $event_id = $id;
          $org = $user[0]->Organisor_id;
          $org_email=$this->Event_model->getOrgEmail($org);
          $this->data['org_email']=$org_email;
          $this->data['org'] = $org;
          $rolename = $user[0]->Role_name;
          $menudata = $this->Event_model->geteventmenu($id,3);
          $menu_toal_data = $this->Event_model->get_total_menu($id);

          $exibitor_page_list = $this->Exibitor_model->get_event_wise_exibitor_page_list($id);
          $this->data['exibitor_page_list'] = $exibitor_page_list;
          $this->data['categorie_data']=$this->Exibitor_model->get_exibitor_categories_by_event($id,NULL);
          $this->data['categorie_group']=$this->Exibitor_model->get_category_group($id);
          $exibitor_invited_list = $this->Exibitor_model->get_invited_exibitor($id);
          $this->data['exibitor_invited_list'] = $exibitor_invited_list;
          $this->data['menu_toal_data'] = $menu_toal_data;
          $authorized_user=$this->Exibitor_model->get_authorized_user_data($id);
          $this->data['auth_user_list']=$authorized_user;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
          $acc=$this->Event_template_model->getAccname($id);
          $acc_name=$acc[0]['acc_name'];
          if ($this->input->post())
          {
               foreach ($this->input->post('invites') as $key => $value)
               {
                    $agenda_invited_list = $this->Exibitor_model->get_invited_exibitor($id);
                    foreach ($agenda_invited_list as $key1 => $values1)
                    {
                         $arraydata = array();
                         if ($values1['Emailid'] == $value)
                         {
                              $arraydata = $values1;
                              break;
                         }
                    }
                    $exibitor_data = $this->Exibitor_model->get_invited_exibitor_data($id,$value);
                    $msg = html_entity_decode($this->input->post('msgcontent'));
                    $sent_from_name = $this->input->post('sent_from_name');
                    $tes = urlencode(base64_encode($value));
                    //$msg1  = ucfirst($arraydata['firstname']) . " " . ucfirst($arraydata['lastname']);
                    //$msg1 .= "<br/>".$msg;
                    $msg1 = $msg;
                    $eid=urlencode(base64_encode($id));
                    $stand_number = urlencode(base64_encode($exibitor_data[0]['link_code']));
                    $event_link = base_url().'login/exibitorsetpassword/'.$eid.'/'.$tes.'/'.$stand_number;
                    $msg1 .= '<br/>' . '<a href="'.$event_link.'">Click here</a> to register<br/><br/>';
                    $msg1 .= "<br/><br>".'Thanks'."<br>";
                    $msg1 .= "<br/>".$sent_from_name;
                    $sub = $this->input->post('subject');
                    $sent_from='invite@allintheloop.com';
                    $config['protocol']   = 'smtp';
                    $config['smtp_host']  = 'localhost';
                    $config['smtp_port']  = '25';
                    $config['smtp_user']  = 'invite@allintheloop.com';
                    $config['smtp_pass']  = 'xHi$&h9M)x9m';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from($sent_from, $sent_from_name);
                    $this->email->to($value);
                    $this->email->subject($sub);
                    $this->email->set_mailtype("html");
                    $this->email->message($msg1);
                    $this->email->send();
               }
          }
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          if ($event_id > 0)
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          else
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          $this->data['users'] = $users;
          $inv_status=array();
          $attendee_user_list=$this->User_model->get_attendee_user_list($id,$user[0]->Organisor_id);
          $this->data['attendee_user_list']=$attendee_user_list;
          $this->data['event_settings'] = $this->Event_model->get_event_settings($id);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('css', 'exibitor/css', $this->data, true);
          $this->template->write_view('content', 'exibitor/index_temp', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'exibitor/js', $this->data, true);
          $this->template->render();
     }


       /****** Import Exhibitors ******/

     public function import_exhibitors($id)
     {
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $this->data['user_list'] = $users;
          if ($user[0]->Role_name == 'Client')
          {
               $Organisor_id = $user[0]->Id;
          }
          else
          {
               $Organisor_id = $this->Event_model->get_org_id_by_event($id);
          }
          $logged_in_user_id = $user[0]->Id;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
          $this->template->write_view('content', 'exibitor/import_exhibitors', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
          $this->template->render();
     }


     public function save_import_exhibitors($id)
     {
          $event_data = $this->Event_model->get_event_accesskey_id($id,$this->input->post('access_key'));
          /*if(empty($event_data))
          {
               $this->session->set_flashdata('csv_flash_message', 'Access key is wrong.');
               redirect(base_url().'exhibitor/import_exhibitors/'.$id);
          }*/


          if($this->input->post('file-type') == 'JSON')
          {
               $data['json_url'] = $this->input->post('json_url');
               $data['import_type'] =  'exhibitors';
               $data['event_id'] = $id;
               $data['file_type'] = '1';
               $this->Event_model->saveEventCSVImport($data);
               $event_data = $this->Event_model->get_event_accesskey_id($id,$this->input->post('access_key'));
               $saveCSVData = $this->Event_model->saveJSONDataExhi($data['json_url'],$event_data['Id'],$event_data['Organisor_id']);
               $this->session->set_flashdata('csv_flash_message', 'JSON Imported Successfully.');
               redirect(base_url().'Exhibitor/import_exhibitors/'.$id);
          }
          else
          {

               session_start();
               $filepath = $this->input->post('csv_url');
               $contents = file_get_contents($this->input->post('csv_url'));
               if (strlen($contents))
               {
                    $_SESSION['access_key'] = $this->input->post('access_key');
                    $_SESSION['csv_url'] = $this->input->post('csv_url');

                    $handle = fopen($filepath, "r");
                    $find_header = 0;
                    while (($datacsv[] = fgetcsv($handle,1000,",")) !== FALSE)
                    {}
                    $keys = array_shift($datacsv);
                    foreach ($datacsv as $key => $value) {
                         $csv[] = array_combine($keys,$value);
                    }
                    $nomatch = array();
                    $csvtest = array_filter($csv);
                    
                    $this->data['import_csv_exhi_data'] = $csvtest;
                    $_SESSION['save_import_exhibitors'] = $csvtest;
                    $this->data['keysdata'] = $keys;
                    foreach ($csv as $key => $value)
                    {
                         $keysarr = array_keys($value);
                         $fixedc = array('Firstname','Lastname','Email','Password','Exhibitor_Name','Exhibitor_Type','Stand_Number','Keywords','Description','Website_Url','Facebook_Url','Twitter_Url','Linkedin_Url','Instagram_Url','Youtube_Url','Company_Logo','Images','Country_name','Heading');
                         for ($i=0;$i<count($keysarr);$i++) 
                         {
                              if(!in_array($keysarr[$i],$fixedc))
                              {
                                   $kk = $keysarr[$i];
                                   if(!in_array($kk,$nomatch))
                                   {
                                        $nomatch[]=$kk;
                                   }
                              }
                         }
                    }
               }
               else
               {
                   $this->session->set_flashdata('csv_flash_message', 'CSV file not found.');
                   redirect(base_url().'Exhibitor/import_exhibitors/'.$id);
               }
               $user = $this->session->userdata('current_user');
               $roleid = $user[0]->Role_id;
               $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
               $this->data['users_role']=$user_role;
               $this->data['user_list'] = $users;
               if ($user[0]->Role_name == 'Client')
               {
                    $Organisor_id = $user[0]->Id;
               }
               else
               {
                    $Organisor_id = $this->Event_model->get_org_id_by_event($id);
               }

               $this->data['nomatch'] = count($nomatch);
               $this->data['Event_id'] = $id;
               $logged_in_user_id = $user[0]->Id;
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
               $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
               $this->template->write_view('content', 'exibitor/maping_import_exhibitor', $this->data, true);
               if ($this->data['user']->Role_name == 'User')
               {
                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }
               $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
               $this->template->render();
          }
     }


     public function upload_import_exhibitors($id)
     {
          //error_reporting(E_ALL);
          session_start();
          $key = array_keys($this->input->post());
          $postval = array_values($this->input->post());
          $postarr = array_combine($key,$postval);
          asort($postarr);
          
          $saveEventCSV['event_id'] = $id;
          $saveEventCSV['import_type'] = 'exhibitors';
          $saveEventCSV['csv_url'] = $_SESSION['csv_url'];
          $saveEventCSV['csv_mapping'] = json_encode($postarr);
          
          $csvSave = $this->Event_model->saveEventCSVImport($saveEventCSV);
          $csv = $_SESSION['save_import_exhibitors'];
          $access_key = $_SESSION['access_key'];

          $event_data = $this->Event_model->get_event_accesskey_id($id,$access_key);
          $saveCSVData = $this->Event_model->saveCSVDataExhi($saveEventCSV['csv_url'],$event_data['Id'],$event_data['Organisor_id']);
          
               
          unset($_SESSION['save_import_exhibitors']);
          unset($_SESSION['access_key']);
          unset($_SESSION['csv_url']);
          $this->session->set_flashdata('csv_flash_message', 'CSV Imported Successfully.');
          redirect(base_url().'Exhibitor/import_exhibitors/'.$id);
     }

     public function download_exhi_template_csv($eid)
     {
          $this->load->helper('download');
          $filename = "import_exibitor.csv";
          $fp = fopen('php://output', 'w');
          $header[] = "Firstname";
          $header[] = "Lastname";
          $header[] = "Email";
          $header[] = "Password";
          $header[] = "Exhibitor_Name";
          $header[] = "Exhibitor_Type";
          $header[] = "Stand_Number";
          $header[] = "Keywords";
          $header[] = "Description";
          $header[] = "Website_Url";
          $header[] = "Facebook_Url";
          $header[] = "Twitter_Url";
          $header[] = "Linkedin_Url";
          $header[] = "Instagram_Url";
          $header[] = "Youtube_Url";
          $header[] = "Company_Logo";
          $header[] = "Images";
          $header[] = "Country_name";
          $header[] = "Heading";
          header('Content-type: application/csv');
          header('Content-Disposition: attachment; filename='.$filename);
          fputcsv($fp, $header);

          for($i=0;$i<1;$i++)
          {
               $data['Firstname'] = "Exhi".$i;
               $data['Lastname'] = "Exhi".$i;
               $data['Email'] = "Exhi$i@gmail.com";
               $data['Password'] = "123456";
               $data['Exhibitor_Name'] = "exhibitor name";
               $data['Exhibitor_Type'] = "exhibitor Type Code";
               $data['Stand_Number'] = "15486";
               $data['Keywords'] = "test,pvt,ltd";
               $data['Description'] = "sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description";
               $data['Website_Url'] = "https://www.website.com";
               $data['Facebook_Url'] = "https://www.facebook.com";
               $data['Twitter_Url'] = "https://www.twitter.com";
               $data['Linkedin_Url'] = "https://www.linkedin.com";
               $data['Instagram_Url'] = "https://www.instagram.com";
               $data['Youtube_Url'] = "https://www.youtube.com";
               $data['Company_Logo'] = "https://www.allintheloop.net/aitldemo/assets/images/adminpanellogo.png";
               $data['Images'] = "";
               $data['Country_name'] = "Argentina";
               $data['Heading'] = "Your Heading";
               fputcsv($fp, $data);
          }
     }
     public function download_import_exhi_json()
     {    
          error_reporting(E_ALL);
          $this->load->helper('download');
          $filename = "import_exhi.json";
          $fp = fopen('php://output', 'w');

          header('Content-type: application/json');
          header('Content-Disposition: attachment; filename='.$filename);
          
          for($i=0;$i<1;$i++)
          {
               $data['Firstname'] = "Exhi".$i;
               $data['Lastname'] = "Exhi".$i;
               $data['Email'] = "Exhi$i@gmail.com";
               $data['Password'] = "123456";
               $data['Exhibitor_Name'] = "exhibitor name";
               $data['Exhibitor_Type'] = "exhibitor Type Code";
               $data['Stand_Number'] = "15486";
               $data['Keywords'] = "test,pvt,ltd";
               $data['Description'] = "sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Description sponsors policy Descriptionsponsors policy Description sponsors policy Description";
               $data['Website_Url'] = "https://www.website.com";
               $data['Facebook_Url'] = "https://www.facebook.com";
               $data['Twitter_Url'] = "https://www.twitter.com";
               $data['Linkedin_Url'] = "https://www.linkedin.com";
               $data['Instagram_Url'] = "https://www.instagram.com";
               $data['Youtube_Url'] = "https://www.youtube.com";
               $data['Company_Logo'] = "https://www.allintheloop.net/aitldemo/assets/images/adminpanellogo.png";
               $data['Images'] = "";
               $data['Country_name'] = "Argentina";
               $data['Heading'] = "Your Heading";
               $fin[$i] = $data;
          }
          $data = json_encode($fin);
          fputs($fp, $data);
     }
     public function show_import_exhi_json()
     {    
          echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
          echo 
          '[{
        "Firstname": "<font color=green><b>(String)</b></font> Firstname of Exhibitor",
        "Lastname": "<font color=green><b>(String)</b></font> Lastname of Exhibitor",
        "Email": "<font color=green><b>(String)</b></font> Email of Exhibitor <font color=red><b>**Required**</b></font>",
        "Password": "<font color=green><b>(String)</b></font> Password",
        "Exhibitor_Name": "<font color=green><b>(String)</b></font> Company Name",
        "Exhibitor_Type": "<font color=green><b>(String)</b></font> Type Code",
        "Stand_Number": "<font color=green><b>(String)</b></font> Stand Number",
        "Keywords": "<font color=green><b>(String)</b></font> Exhibitor Short Description <b>(coma separated)</b>",
        "Description": "<font color=green><b>(String)</b></font> Company Description of Exhibitors",
        "Website_Url": "<font color=green><b>(String)</b></font> Company Website URL",
        "Facebook_Url": "<font color=green><b>(String)</b></font> Company Facebook URL",
        "Twitter_Url": "<font color=green><b>(String)</b></font> Company Twitter URL",
        "Linkedin_Url": "<font color=green><b>(String)</b></font> Company Linkedin URL",
        "Instagram_Url": "<font color=green><b>(String)</b></font> Company Instagram URL",
        "Youtube_Url": "<font color=green><b>(String)</b></font> Company Youtube URL",
        "Company_Logo": "<font color=green><b>(String)</b></font> URL of Company Logo",
        "Images": "<font color=green><b>(Json String Array)</b></font> URL of Company Images",
        "Country_name": "<font color=green><b>(String)</b></font> Country Name",
        "Heading": "<font color=green><b>(String)</b></font> Heading"
    },
    {
        "Firstname": "Jhon",
        "Lastname": "deo",
        "Email": "Jhon@abc.com",
        "Password": "123456",
        "Exhibitor_Name": "ABC PVT. LTD.",
        "Exhibitor_Type": "GOLD",
        "Stand_Number": "A-34",
        "Keywords": "food,meet,veg",
        "Description": "ABC PVT. LTD. was establised in 1990. it have more then 1000 clients all over world",
        "Website_Url": "https:\/\/www.abc.com",
        "Facebook_Url": "https:\/\/www.facebook.com\/abc",
        "Twitter_Url": "https:\/\/www.twitter.com\/abc",
        "Linkedin_Url": "https:\/\/www.linkedin.com\/abc",
        "Instagram_Url": "https:\/\/www.instagram.com\/abc",
        "Youtube_Url": "https:\/\/www.youtube.com\/abc",
        "Company_Logo": "https:\/\/www.abc.com\/logo.png",
        "Images": "",
        "Country_name": "Argentina",
        "Heading": "ABC PVT. LTD Argentina"
    }]';exit;
          error_reporting(E_ALL);
          header('Content-type: application/json');
          
          for($i=0;$i<2;$i++)
          {    
               if($i ==  0)
               {
                    $data['Firstname'] = "(String) Firstname of Exhibitor";
                    $data['Lastname'] = "(String) Lastname of Exhibitor";
                    $data['Email'] = "(String) Email of Exhibitor <font color=red><b>**Required**</b></font>";
                    $data['Password'] = "(String) Password";
                    $data['Exhibitor_Name'] = "(String) Company Name";
                    $data['Exhibitor_Type'] = "(String) Type Code";
                    $data['Stand_Number'] = "(String) Stand Number";
                    $data['Keywords'] = "(String) Exhibitor Short Description (coma separated)";
                    $data['Description'] = "(String) Company Description of Exhibitors";
                    $data['Website_Url'] = "(String) Company Website URL";
                    $data['Facebook_Url'] = "(String) Company Facebook URL";
                    $data['Twitter_Url'] = "(String) Company Twitter URL";
                    $data['Linkedin_Url'] = "(String) Company Linkedin URL";
                    $data['Instagram_Url'] = "(String) Company Instagram URL";
                    $data['Youtube_Url'] = "(String) Company Youtube URL";
                    $data['Company_Logo'] = "(String) URL of Company Logo";
                    $data['Images'] = "(Json String Array) URL of Company Images";
                    $data['Country_name'] = "(String) Country Name";
                    $data['Heading'] = "(String) Heading";
               }
               else
               {
                    $data['Firstname'] = "Jhon";
                    $data['Lastname'] = "deo";
                    $data['Email'] = "Jhon@abc.com";
                    $data['Password'] = "123456";
                    $data['Exhibitor_Name'] = "ABC PVT. LTD.";
                    $data['Exhibitor_Type'] = "GOLD";
                    $data['Stand_Number'] = "A-34";
                    $data['Keywords'] = "food,meet,veg";
                    $data['Description'] = "ABC PVT. LTD. was establised in 1990. it have more then 1000 clients all over world";
                    $data['Website_Url'] = "https://www.abc.com";
                    $data['Facebook_Url'] = "https://www.facebook.com/abc";
                    $data['Twitter_Url'] = "https://www.twitter.com/abc";
                    $data['Linkedin_Url'] = "https://www.linkedin.com/abc";
                    $data['Instagram_Url'] = "https://www.instagram.com/abc";
                    $data['Youtube_Url'] = "https://www.youtube.com/abc";
                    $data['Company_Logo'] = "https://www.abc.com/logo.png";
                    $data['Images'] = "";
                    $data['Country_name'] = "Argentina";
                    $data['Heading'] = "ABC PVT. LTD Argentina";
               }
               $fin[$i] = $data;

          }
          // $data = json_encode($fin);
          echo json_encode($fin, JSON_PRETTY_PRINT);
     }
     public function save_settings($id)
     {    
          $data['hide_share_contcat_exhi'] = empty($this->input->post('hide_share_contcat_exhi')) ? '0' : '1';
          $data['show_stand_no'] = empty($this->input->post('show_stand_no')) ? '0' : '1';
          $data['stand_no_label'] = empty($this->input->post('show_stand_no')) ? '' : ($this->input->post('stand_no_label')?:'Stand');
          $this->Event_model->save_event_settings($id,$data);
          updateModuleDate($id,'exhibitor');
          $this->session->set_flashdata('exibitor_data', 'Settings Saved');
          redirect(base_url().'Exhibitor/index/'.$id.'#allow_messaging');
     }
}
