<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Photos_hub extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Photos';
          $this->data['smalltitle'] = 'Photos';
          $this->data['breadcrumb'] = 'Photos';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $this->load->model('Photos_model');
          $org=$this->Event_model->get_organizer_by_acc_name($this->uri->segment(2));
          $acc['acc_name'] =  $this->uri->segment(2);
          $this->session->set_userdata($acc);
          $oid=$org[0]['Id'];
          if(empty($oid))
          {
               $org=$this->Event_model->get_organizer_by_acc_name($this->uri->segment(3));
               $oid=$org[0]['Id'];
          }
          $hub_active=$this->Event_model->get_hub_active_by_organizer($oid); 
          if($hub_active!='1')
          {
               redirect(base_url().'Forbidden/');
          }
     }
     public function index($acc_name)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $hubevent=$this->Event_model->get_hub_event_menu_setting_organizerid($org_id);
          $view_chats1=$this->Photos_model->get_public_photomsg_by_org($org_id,0);
          $this->data['view_chats1']=$view_chats1;
          $hubdesing=$this->Event_model->get_hub_event($org_id);
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
          $this->data['hub_cms_menu'] = $cmsmenu;
          $this->data['hubdesing']=$hubdesing;
          $event_templates[0]['Logo_images']=$hubdesing[0]['hub_logo_images'];
          $event_templates[0]['attendees_active']=$hubdesing[0]['attendees_active'];
          $event_templates[0]['public_messages_active']=$hubdesing[0]['public_messages_active'];
          $event_templates[0]['Img_view']='0';
          $event_templates[0]['Event_name']=$hubdesing[0]['hub_name'];
          $event_templates[0]['Subdomain']=$hubevent[0]['Subdomain'];
          $event_templates[0]['menu_text_color']='#FFFFFF';
          $event_templates[0]['menu_background_color']='#6b6b6b';
          $event_templates[0]['menu_hover_background_color']='#b5b5b5';
          $event_templates[0]['Top_text_color']=$hubdesing[0]['hub_top_bar_text_color'];
          $event_templates[0]['Top_background_color']=$hubdesing[0]['hub_top_bar_backgroundcolor'];
          $event_templates[0]['huburl']=base_url().'hub/'.$acc_name;
          $this->data['event_templates']=$event_templates;
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          $this->template->write_view('content', 'Photos_hub/uploadphoto_hub', $this->data, true);
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->render();
     }
     public function loadmore($acc_name,$start)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
          $this->data['hub_cms_menu'] = $cmsmenu;
          $view_chats1=$this->Photos_model->get_public_photomsg_by_org($org_id,$start);
          $Sid = $this->uri->segment(4);
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          foreach ($view_chats1 as $key => $value)
          {
               $arr=explode(',',$value['user_id']);
               $like=0;
               $like=count(array_filter($arr));
               echo "<div class='message_container'>";
               if ($value['Sender_id'] == $user[0]->Id)
               {
                    echo "<div class='msg_edit-view-box'>";
                    echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                    echo "</div>";
                    echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                    echo "Delete";
                    echo "</div>";
                    echo "</div>";
               }

               echo "<div class='msg_main_body'>";
               echo "<div class='message_img'>";
               if ($value['Senderlogo'] != "")
               {
                    echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
               }
               else
               {
                    echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
               }
               echo "</div>";
               echo "<div class='msg_fromname'>";
               $t=time().$key;
               echo '<a href="#" class="tooltip_data">';
               echo ucfirst($value['Sendername']);
               echo '</a>';
               echo "</div>";
               if (!empty($value['Receiver_id']))
               {
                    echo "<div class='msg_with'>";
                    echo "with";
                    echo "</div>";
                    echo "<div class='msg_toname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Recivername']);
                    echo '</a>';                                               
                    echo "</div>";
               }
               echo "</div>";
               echo "<div class='msg_date'>";
               echo $this->timeAgo(strtotime($value['Time']));
               echo "</div>";
               $img_data = json_decode($value['image']);
               foreach ($img_data as $kimg => $valimg)
               {
                    echo "<div class='msg_photo photos_feed'>";
                    echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                    echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                    echo "</a>";
                    echo "</div>";
               }
                    echo "<div style='clear:both'>";
                    if(in_array($user[0]->Id, $arr))
                    {
                         $str="dislike_".$value['Id'];
                         $post_id=$value['Id'];
                         ?>
                         <div class='like blue_like' id="<?php echo 'like_'.$value['Id'];?>" ><a href='#'  class="like_btn blue_btn" onclick="unlike('<?php echo $user[0]->Id;?>','<?php echo $post_id;?>','<?php echo $str;?>')" ><i class='fa fa-thumbs-up'></i>Like</a></div>
                         <?php 
                    }
                    else
                    {      
                         $post_id=$value['Id'];
                         $str1="like_".$value['Id'];?>
                         <div class='like' id="<?php echo 'like_'.$value['Id'];?>"><a href='#' class='like_btn'  onclick="like('<?php echo $user[0]->Id;?>','<?php echo $post_id;?>','<?php echo $str1;?>')" ><i class='fa fa-thumbs-up'></i>Like</a></div>
                         <?php
                    }

                    echo "</div>";
                    $post_id=$value['Id'];
                    echo "<div id='".$post_id."'>";
                    echo "&nbsp;comments   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$like." people like this";
                    echo "<span style='float:left;padding:0px;'>".count($view_chats1[$key]['comment'])."  </span>";
                    echo "</div>";
                    echo "<div class='toggle_comment msg_toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
               <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Photos_hub/upload_commentimag/".$acc_name."/". $post_id."'>
                    <div class='comment_message_img'>";
                    
                    if ($user[0]->Logo!="")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    
                    echo "</div>
                    <textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>
                    <div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>
                    <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />
                         
               </form>
               <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";
               if (!empty($view_chats1[$key]['comment']))
               {
                    $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                    $i = 0;
                    $flag = false;
                    foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                         if ($cval['Logo'] != "")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div>
               <div class='comment_wrapper'>        
               <div class='comment_username'>
                    " . ucfirst($cval['user_name']) . "
               </div>
               <div class='comment_text'>
                    " . $cval['comment'] . "
               </div>
               <div class='comment_text' style='float:right;padding-left:13px;'>";
                         ?>
                         <?php $this->timeAgo(strtotime($cval['Time'])); ?>
                         <?php
                         echo"</div>
               </div>";

                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo photos_feed'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$post_id.",this)'>&nbsp;</button>";
                         }
                         echo "</div>";


                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                         }

                         $i++;
                    }
               }
               echo "</div>
               </div>";
               echo "</div>";
          }
          die;
     }
     public function timeAgo($time_ago)
     {
          $cur_time = time();
          $time_elapsed = $cur_time - $time_ago;

          $seconds = $time_elapsed;
          $minutes = round($time_elapsed / 60);
          $hours = round($time_elapsed / 3600);
          $days = round($time_elapsed / 86400);
          $weeks = round($time_elapsed / 604800);
          $months = round($time_elapsed / 2600640);
          $years = round($time_elapsed / 31207680);
          // Seconds
          if ($seconds <= 60)
          {
               echo "$seconds seconds ago";
          }
          //Minutes
          else if ($minutes <= 60)
          {
               if ($minutes == 1)
               {
                    echo "one minute ago";
               }
               else
               {
                    echo "$minutes minutes ago";
               }
          }
          //Hours
          else if ($hours <= 24)
          {
               if ($hours == 1)
               {
                    echo "an hour ago";
               }
               else
               {
                    echo "$hours hours ago";
               }
          }
          //Days
          else if ($days <= 7)
          {
               if ($days == 1)
               {
                    echo "yesterday";
               }
               else
               {
                    echo "$days days ago";
               }
          }
          //Weeks
          else if ($weeks <= 4.3)
          {
               if ($weeks == 1)
               {
                    echo "a week ago";
               }
               else
               {
                    echo "$weeks weeks ago";
               }
          }
          //Months
          else if ($months <= 12)
          {
               if ($months == 1)
               {
                    echo "a month ago";
               }
               else
               {
                    echo "$months months ago";
               }
          }
          //Years
          else
          {
               if ($years == 1)
               {
                    echo "one year ago";
               }
               else
               {
                    echo "$years years ago";
               }
          }
     }
     public function upload_imag($acc_name)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
           if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {
               define("MAX_SIZE", "200000");
               $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
               $uploaddir = "assets/user_files/";
               $thumbuploaddir = "assets/user_files/thumbnail/";
               foreach ($_FILES['photos']['name'] as $name => $value)
               {
                    $filename = stripslashes($_FILES['photos']['name'][$name]);
                    $size = filesize($_FILES['photos']['tmp_name'][$name]);
                    $ext = $this->getExtension($filename);
                    $ext = strtolower($ext);
                    if (in_array($ext, $valid_formats))
                    {
                         if ($size < (MAX_SIZE * 1024))
                         {
                            $fnm=explode(".",$filename);
                              $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $filename=$filename.".".$fnm[1];
                              $image_name = time() . $filename;
                              echo "<li>
                                        <div class='msg_photo'>
                                             <input type='hidden' name='unpublished_photo[]' value='" . $image_name . "'>
                                             <img src='" . base_url() . $thumbuploaddir . $image_name . "' class='imgList'>
                                             <button class='photo_remove_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button>
                                        </div>
                                   </li>";
                              $fnm=explode(".",$image_name);
                              $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $image_name=$image_name.".".$fnm[1];
                              $newname = $uploaddir . $image_name;
                              $thumbnewname = $thumbuploaddir . $image_name;
                              if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
                              {
                                   $time = time();
                                   $datafiles[] = $image_name;
                                   $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
                              }
                              else
                              {
                                   echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                              }
                         }
                         else
                         {
                              echo '<span class="imgList">You have exceeded the size limit!</span>';
                         }
                    }
                    else
                    {
                         echo '<span class="imgList">Unknown extension!</span>';
                    }
               }
               $string = "";
               $string = json_encode($datafiles);
          }
          exit;
     }
     public function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth, $quality)
     { 
          $details = getimagesize("$imageDirectory/$imageName") or die('Please only upload images.'); 
          $type = preg_replace('@^.+(?<=/)(.+)$@', '$1', $details['mime']); 
          eval('$srcImg = imagecreatefrom'.$type.'("$imageDirectory/$imageName");'); 
          $thumbHeight = $details[1] * ($thumbWidth / $details[0]); 
          $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight); 
          imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight,  
          $details[0], $details[1]); 
          eval('image'.$type.'($thumbImg, "$thumbDirectory/$imageName"'. 
          (($type=='jpeg')?', $quality':'').');'); 
          imagedestroy($srcImg); 
          imagedestroy($thumbImg); 
     } 
     public function getExtension($str)
     {
          $i = strrpos($str, ".");
          if (!$i)
          {
               return "";
          }
          $l = strlen($str) - $i;
          $ext = substr($str, $i + 1, $l);
          return $ext;
     }
     public function chatspublic($acc_name)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $this->load->model('User_model');
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          $userdata=$this->Event_model->get_user_by_hub_event_by_user_id($org_id,$lid);
          
          $view_chats1 = $this->Photos_model->view_hangouts_public_msg($userdata[0]['eventid']);
          $this->data['view_chats1'] = $view_chats1;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
               }
               else
               {
                    $resiver = NULL;
               }

               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $resiver,
                       'Parent' => '0',
                       'Event_id' => $userdata[0]['eventid'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic')
               );

               $current_date=date('Y/m/d');
               foreach ($resiver as $key => $value) {
                    $this->Photos_model->add_msg_hit($lid,$current_date,$value); 
               }
               $this->Photos_model->send_grp_speaker_message($data1);
               if ($this->input->post('ispublic') == '1')
               {
                    $view_chats1 = $this->Photos_model->view_hangouts_public_msg($userdata[0]['eventid']);
               }
               else
               {
                  if(!empty($resiver))
                  {
                      $strRec = @implode(',',$resiver);
                      if(!empty($strRec))
                      {
                          $arrUser = $this->User_model->getUserFromIds($strRec);
                          if(!empty($arrUser))
                          {
                              foreach($arrUser as $intKey=>$strValue)
                              {
                                  $strMsg = $this->input->post('Message');
                                  $strMsg .= "<br/>";
                                  $strMsg .= "Event Link : ".  base_url()."Events/".$userdata[0]['Subdomain'];
                                  $this->email->from($user[0]->Email, $user[0]->Firstname);
                                  $this->email->to($strValue['Email']);
                                  $this->email->subject("Private Message");
                                  $this->email->set_mailtype("html");
                                  $this->email->message($strMsg);    
                                  $this->email->send();
                              }
                          }
                      }
                  }
                  $view_chats1 = $this->Photos_model->view_hangouts_private_msg($userdata[0]['eventid']);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
                  <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Feeds <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";
                    echo "<div class='msg_date'>";
                    echo $this->timeAgo(strtotime($value['Time']));
                    echo "</div>";

                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo photos_feed'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }
                    
                    if($this->input->post('ispublic') == '1')
                    {    
                      $arr=explode(',',$value['user_id']);                  
                      $like=0;
                      $like=count(array_filter($arr));
                      $str="like_".$value['Id'];
                      echo "<div style='clear:both'>";
                      if(in_array($user[0]->Id, $arr))
                      {
                          echo "<div class='like blue_like' id='".$str."'><a href='#'  class='like_btn blue_btn' onclick='unlike(".$value['Sender_id'].",".$value['Id'].",".$str.")' ><i class='fa fa-thumbs-up'></i>Like</a></div>";
                      }
                      else
                      {
                          echo "<div class='like' id='".$str."'><a href='#' class='like_btn'  onclick='like(".$value['Sender_id'].",".$value['Id'].",".$str.")' ><i class='fa fa-thumbs-up'></i>Like</a></div>";
                      }
                      echo "</div>";
                         $post_id=$value['Id'];
                        echo "<div id='".$post_id."'>";
                        echo "&nbsp;comments   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$like." people like this";
                        echo "<span style='float:left;padding:0px;'>".count($view_chats1[$key]['comment'])."  </span>";
                        echo "</div>";
                        echo "<div class='toggle_comment msg_toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    }
                    else
                    {
                          echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                    }
                    
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                    <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Photos_hub/upload_commentimag/".$acc_name."/". $post_id."'>
                                        <div class='comment_message_img'>";
                    if ($user[0]->Logo!="")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>";
 
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                    }
                    else
                    {
                         echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                    }
                    
                    
                    
                    echo "<div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                    }
                    else
                    {
                          echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                    }
                                             
                    echo "</form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "")
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }
                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->timeAgo(strtotime($cval['Time'])) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo photos_feed'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/thumbnail/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$post_id.",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }
     public function upload_commentimag($acc_name,$msg_id)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {
               define("MAX_SIZE", "20000000");
               $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
               //$datafiles=array();
               $uploaddir = "assets/user_files/"; //a directory inside
               foreach ($_FILES['cmphto']['name'] as $name => $value)
               {

                    $filename = stripslashes($_FILES['cmphto']['name'][$name]);
                    $size = filesize($_FILES['cmphto']['tmp_name'][$name]);
                    $ext = $this->getExtension($filename);
                    $ext = strtolower($ext);
                    if (in_array($ext, $valid_formats))
                    {
                         if ($size < (MAX_SIZE * 1024))
                         {
                              $fnm=explode(".",$filename);
                              $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $filename=$filename.".".$fnm[1];
                              $image_name = time() . $filename;
                              echo "<div class='comment_container_afteruploadimag'><li>
                              <div class='msg_photo'>
                              <input type='hidden' name='unpublished_commentphoto[]' value='" . $image_name . "'>
                              <img src='" . base_url() . $uploaddir . $image_name . "' class='imgList'>
                              </div></li><button class='comment_section_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button></div>";
                               $fnm=explode(".",$image_name);
                              $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $image_name=$image_name.".".$fnm[1];
                              $newname = $uploaddir . $image_name;

                              if (move_uploaded_file($_FILES['cmphto']['tmp_name'][$name], $newname))
                              {
                                   $time = time();
                                   $datafiles[] = $image_name;
                                   $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
                              }
                              else
                              {
                                   echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                              }
                         }
                         else
                         {
                              echo '<span class="imgList">You have exceeded the size limit!</span>';
                         }
                    }
                    else
                    {
                         echo '<span class="imgList">Unknown extension!</span>';
                    }
               }
               $string = "";
               $string = json_encode($datafiles);
          }
          exit;
     }
     public function commentaddpublic($acc_name,$is_type)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
          $userdata=$this->Event_model->get_user_by_hub_event_by_user_id($org_id,$lid);
          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");
               $time=$array_add['Time'];
               $msg_id=$this->input->post('msg_id');
               $sender_id=$this->Photos_model->get_senderid($msg_id);
               $current_date=date('Y/m/d');
          
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($userdata[0]['Subdomain']);     
               $comment_id = $this->Photos_model->add_comment($userdata[0]['eventid'],$array_add);
               $view_chats1 = $this->Photos_model->view_hangouts_public_msg($userdata[0]['eventid'], 0, 10,$this->input->post('msg_id'));
               if(count($view_chats1) > 1)
               {
                  for ($j=0; $j <=count($view_chats1); $j++) { 
                      if($view_chats1[$j]['Id']==$this->input->post('msg_id'))
                      {
                        $view_comm[0]=$view_chats1[$j];  
                      }
                  }
               }

               if (!empty($view_comm[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_comm[0]['comment'] = array_reverse($view_comm[0]['comment']);
                    foreach ($view_comm[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }
                         echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                         if ($cval['Logo'] != "")
                         {
                              echo "<img src='" . base_url() . "assets/user_files/" . $cval['Logo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }
                         echo "</div><div class='comment_wrapper'><div class='comment_username'>".ucfirst($cval['user_name'])."</div><div class='comment_text'>".$cval['comment']."</div>".$this->timeago(strtotime($cval['Time'])) . "</div>";
                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo photos_feed'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$msg_id.",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;          
     }
     public function like($acc_name)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
        $user_id=$this->input->post('user_id');
        $post_id=$this->input->post('post_id');
        $str_user = $this->Photos_model->get_userid_by_post($post_id);
        if(empty($str_user))
        {
          $new_str=$user_id;
        }
        else
        {
          $new_str=$str_user.','.$user_id;
        }
        $this->Photos_model->update_feed_like($post_id,$new_str);
        $arr_user=explode(',', $new_str);
        $total_like=count($arr_user);
        $str='like_'.$post_id;
        $a='<a href="#"  class="like_btn blue_btn" onclick="unlike('.$user_id.','.$post_id.','.$str.')"><i class="fa fa-thumbs-up"></i>Like</a>';
        $j_data=json_encode(array('total_like'=>$total_like,'post_id'=>$post_id,'status'=>'dislike','html'=>$a));
        echo $j_data;
     }
     public function dislike($acc_name)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
        $user_id=$this->input->post('user_id');
        $post_id=$this->input->post('post_id');
        $str_user = $this->Photos_model->get_userid_by_post($post_id);
        $arr_user=explode(',', $str_user);
        $new_user_like_arr;
        foreach ($arr_user as $key => $value) 
        {
            if($value!=$user_id)
            {
                $new_user_like_arr[]=$value;
            }
        }
        $total_like=count($new_user_like_arr);
        $like_str=implode(",",$new_user_like_arr);
        $this->Photos_model->update_feed_like($post_id,$like_str);
        $a="<a href='#' class='like_btn'  onclick='like(".$user_id.",".$post_id.")'><i class='fa fa-thumbs-up'></i>Like</a>";
        $j_data=json_encode(array('total_like'=>$total_like,'post_id'=>$post_id,'status'=>'dislike','html'=>$a));
        echo $j_data; 
     }
     public function delete_message($acc_name,$msg_id)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          echo "333";
          $chats = $this->Photos_model->delete_message($msg_id);
          exit;
     }
     public function delete_comment($acc_name,$id)
     {
          $org=$this->Event_model->get_organizer_by_acc_name($acc_name);
          $org_id=$org[0]['Id'];
          echo "333";
          $chats = $this->Photos_model->delete_comment($id);
          exit;
     }
}
?>
