<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Mycontact extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Business Card Swap';
          $this->data['smalltitle'] = 'Allow users to swap contact details when granted permission';
          $this->data['breadcrumb'] = 'Business Card Swap';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);
          $eventmodule=$this->Event_model->geteventmodulues($eventid);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('14',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          $user = $this->session->userdata('current_user');
          
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
              // $eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Business Card Swap")
               {
                    $title = "MyContact";
               }

               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }

          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('14',$menu_list))
          {    
               $this->load->model('My_contact_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');               
               $this->load->model('Profile_model');
               $this->load->model('Event_template_model');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {    
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id)
     {    

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
          }

          $menudata = $this->Event_model->geteventmenu($id, 14);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;

          $rolename = $user[0]->Role_name;
          //$roles = $this->Agenda_model->get_login_role($roleid);
          $this->data['roles'] = $roles;
          $req_mod = ucfirst($this->router->fetch_class());


     
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          if ($req_mod == 'MyContact')
          {
               $req_mod = "MyContact";
          }
          $check_client = 0;

          if ($rolename == 'Client')
          {
               $this->template->write_view('css', 'mycontact/css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'mycontact/index', $this->data, true);
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               $this->template->write_view('js', 'mycontact/js', $this->data, true);
               $this->template->render();
          }
          else
          {
                         $this->template->write_view('css', 'mycontact/css', $this->data, true);
                         $this->template->write_view('header', 'common/header', $this->data, true);
                         $this->template->write_view('content', 'mycontact/index', $this->data, true);

                         if ($this->data['user']->Role_name == 'User')
                         {
                              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
                         }
                         else
                         {
                              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
                         }

                         $this->template->write_view('js', 'mycontact/js', $this->data, true);
                         $this->template->render();
          }
     }

}
