<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Analytics extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Analytics';
        $this->data['smalltitle'] = '';
        $this->data['breadcrumb'] = 'Analytics';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        ini_set('auto_detect_line_endings', true);
        $this->load->model('Event_model');
        $eventid = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        if ($user[0]->Role_id == '4') redirect('Forbidden');
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column_1($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
    }
    public function index($id)

    {
        $this->template->write_view('css', 'analytics/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'analytics/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/news_sidebar', $this->data, true);
        $this->template->write_view('js', 'analytics/js', true);
        $this->template->render();
    }
}