<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Exhibitor_portal extends FrontendController

{
     function __construct()
     {
          $this->data['pagetitle'] = 'My Exhibitor Profile';
          $this->data['smalltitle'] = 'Edit Your Exhibitor Profile Here.';
          $this->data['breadcrumb'] = 'My Exhibitor Profile';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Exibitor_model');
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Map_model');
          $this->load->model('Event_template_model');
          $user = $this->session->userdata('current_user');
          $eventid = $this->uri->segment(3);
          $user = $this->session->userdata('current_user');
          $eventid = $this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
          $user_events = array_filter(array_column_1($user_events, 'Event_id'));
          if (!in_array($eventid, $user_events))
          {
               redirect('Forbidden');
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $roledata = $this->Event_model->getUserRole($eventid);
          if (!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Your Exhibitors")
               {
                    $title = "Exhibitors";
               }
               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
               // $cnt = $this->Agenda_model->check_auth($req_mod, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt = 0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          if ($cnt == 1)
          {
               $this->load->model('Exibitor_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->model('Event_template_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid, $eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               $this->load->model('Exibitor_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->library('upload');
               // redirect('forbidden');
          }
     }
     public function index($id=NULL)

     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $orid = $this->data['user']->Id;
          $premission = $this->Exibitor_model->get_exibitor_user_premission($id, $orid);
          $this->data['exibitor_premission'] = $premission;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $users = $this->Exibitor_model->get_exhib_list_byevent($id, $orid);
          $this->data['users'] = $users;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
          $this->data['users_role'] = $user_role;
          $exibitor_list = $this->Exibitor_model->get_users_exibitor_list($orid, $id);
          $this->data['exibitor_list'] = $exibitor_list;
          $menudata = $this->Event_model->geteventmenu($id, 3);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;
          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
          $this->template->write_view('css', 'exhibitor_portal/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'exhibitor_portal/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exhibitor_portal/js', $this->data, true);
          $this->template->render();
     }
     public function add($id=NULL)

     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
          $this->data['users_role'] = $user_role;
          if ($user[0]->Role_name == 'Client')
          {
               $Organisor_id = $user[0]->Id;
          }
          else
          {
               $Organisor_id = $user[0]->Organisor_id;
          }
          $logged_in_user_id = $user[0]->Id;
          if ($this->input->post())
          {
               if (empty($this->input->post('company_logo_crop_data_textbox')))
               {
                    if (!empty($_FILES['company_logo']['name'][0]))
                    {
                         $tempname = explode('.', $_FILES['company_logo']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                              "file_name" => $images_file,
                              "upload_path" => "./assets/user_files",
                              "allowed_types" => '*',
                              "max_size" => '100000',
                              "max_width" => '5000',
                              "max_height" => '5000'
                         ));
                         if (!$this->upload->do_multi_upload("company_logo"))
                         {
                              $error = array(
                                   'error' => $this->upload->display_errors()
                              );
                              $this->session->set_flashdata('error', $error['error']);
                         }
                    }
               }
               else
               {
                    $img = $_POST['company_logo_crop_data_textbox'];
                    $filteredData = substr($img, strpos($img, ",") + 1);
                    $unencodedData = base64_decode($filteredData);
                    $images_file = strtotime(date("Y-m-d H:i:s")) . "_company_crop_logo_image.png";
                    $filepath = "./assets/user_files/" . $images_file;
                    file_put_contents($filepath, $unencodedData);
               }
               if (empty($this->input->post('company_banner_crop_data_textbox')))
               {
                    if (!empty($_FILES['banner_Images']['name']))
                    {
                         /*foreach ($_FILES['Images']['name'] as $k => $v)
                         {
                         $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                         $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }*/
                         $tempname = explode('.', $_FILES['banner_Images']['name']);
                         $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                         $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file1 = $tempname_imagename . "." . $tempname[1];
                         $this->upload->initialize(array(
                              "file_name" => $images_file1,
                              "upload_path" => "./assets/user_files",
                              "allowed_types" => '*',
                              "max_size" => '100000',
                              "max_width" => '5000',
                              "max_height" => '5000'
                         ));
                         if (!$this->upload->do_multi_upload("banner_Images"))
                         {
                              $error = array(
                                   'error' => $this->upload->display_errors()
                              );
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                         $Images = array(
                              $images_file1
                         );
                    }
               }
               else
               {
                    $img = $_POST['company_banner_crop_data_textbox'];
                    $filteredData = substr($img, strpos($img, ",") + 1);
                    $unencodedData = base64_decode($filteredData);
                    $images_file1 = strtotime(date("Y-m-d H:i:s")) . "_company_crop_banner_image.png";
                    $filepath = "./assets/user_files/" . $images_file1;
                    file_put_contents($filepath, $unencodedData);
                    $Images = array(
                         $images_file1
                    );
               }
               $data['exibitor_array']['Organisor_id'] = $Organisor_id;
               $data['exibitor_array']['user_id'] = $logged_in_user_id;
               $data['exibitor_array']['Event_id'] = $Event_id;
               $data['exibitor_array']['Heading'] = $this->input->post('Heading');
               $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
               $data['exibitor_array']['Description'] = $this->input->post('Description');
               $data['exibitor_array']['Images'] = $Images;
               $data['exibitor_array']['website_url'] = $this->input->post('website_url');
               $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['exibitor_array']['youtube_url'] = $this->input->post('youtube_url');
               $data['exibitor_array']['instagram_url'] = $this->input->post('instagram_url');
               // $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
               // $data['exibitor_array']['main_contact_name'] = $this->input->post('main_contact_name');
               // $data['exibitor_array']['Address_map'] = $this->input->post('Address_map');
               // $data['exibitor_array']['main_email_address'] = $this->input->post('main_email_address');
               // $data['exibitor_array']['stand_number'] = $this->input->post('stand_number');
               // $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
               // $data['exibitor_array']['HQ_phone_number'] = $this->input->post('HQ_phone_number');
               $data['exibitor_array']['email_address'] = $this->input->post('email_address');
               $data['exibitor_array']['company_logo'] = $images_file;
               $exibitor_id = $this->Exibitor_model->add_user_as_exibitor($data);
               $this->session->set_flashdata('exibitor_data', 'Added');
               redirect("Exhibitor_portal/edit/" . $id . "/" . $exibitor_id);
          }
          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;
          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;
          $countrylist = $this->Profile_model->countrylist();
          $this->data['countrylist'] = $countrylist;
          $premission = $this->Exibitor_model->get_exibitor_user_premission($id, $user[0]->Id);
          $this->data['exibitor_premission'] = $premission;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $agenda_list = $this->Agenda_model->get_agenda_list($id);
          $this->data['agenda_list'] = $agenda_list;
          $this->template->write_view('css', 'exhibitor_portal/add_css', $this->data, true);
          $this->template->write_view('content', 'exhibitor_portal/add', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'exhibitor_portal/add_js', $this->data, true);
          $this->template->render();
     }
     public function edit($id=NULL, $eid=NULL)

     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
          $this->data['users_role'] = $user_role;
          $logged_in_user_id = $user[0]->Id;
          $premission = $this->Exibitor_model->get_exibitor_user_premission($Event_id, $user[0]->Id);
          $this->data['exibitor_premission'] = $premission;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;
          $exibitor_list = $this->Exibitor_model->get_exibitor_list($id);
          $this->data['exibitor_list'] = $exibitor_list;
          $exibitor_by_id = $this->Exibitor_model->get_exibitor_by_id_exhi($id, $eid);
          $this->data['exibitor_by_id'] = $exibitor_by_id;
          $mapid = $exibitor_by_id[0]['Address_map'];
          $this->data['mapdata'] = $this->Map_model->getmapdata_by_mapid($mapid);
          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Exibitor');
               }
               if ($this->input->post())
               {
                    if (empty($this->input->post('company_logo_crop_data_textbox')))
                    {
                         if (!empty($_FILES['company_logo']['name']))
                         {
                              $tempname = explode('.', $_FILES['company_logo']['name']);
                              $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $images_file = $tempname_imagename . "." . $tempname[1];
                              $this->upload->initialize(array(
                                   "file_name" => $images_file,
                                   "upload_path" => "./assets/user_files",
                                   "allowed_types" => '*',
                                   "max_size" => '100000',
                                   "max_width" => '5000',
                                   "max_height" => '5000'
                              ));
                              if (!$this->upload->do_multi_upload("company_logo"))
                              {
                                   $error = array(
                                        'error' => $this->upload->display_errors()
                                   );
                                   $this->session->set_flashdata('error', $error['error']);
                              }
                         }
                    }
                    else
                    {
                         $img = $_POST['company_logo_crop_data_textbox'];
                         $filteredData = substr($img, strpos($img, ",") + 1);
                         $unencodedData = base64_decode($filteredData);
                         $images_file = strtotime(date("Y-m-d H:i:s")) . "_company_crop_logo_image.png";
                         $filepath = "./assets/user_files/" . $images_file;
                         file_put_contents($filepath, $unencodedData);
                    }
                    if (empty($this->input->post('company_banner_crop_data_textbox')))
                    {
                         if (!empty($_FILES['banner_Images']['name']))
                         {
                              $tempname = explode('.', $_FILES['banner_Images']['name']);
                              $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                              $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                              $images_file1 = $tempname_imagename . "." . $tempname[1];
                              $this->upload->initialize(array(
                                   "file_name" => $images_file1,
                                   "upload_path" => "./assets/user_files",
                                   "allowed_types" => '*',
                                   "max_size" => '100000',
                                   "max_width" => '5000',
                                   "max_height" => '5000'
                              ));
                              if (!$this->upload->do_multi_upload("banner_Images"))
                              {
                                   $error = array(
                                        'error' => $this->upload->display_errors()
                                   );
                                   $this->session->set_flashdata('error', "" . $error['error']);
                              }
                              $Images = array(
                                   $images_file1
                              );
                         }
                    }
                    else
                    {
                         $img = $_POST['company_banner_crop_data_textbox'];
                         $filteredData = substr($img, strpos($img, ",") + 1);
                         $unencodedData = base64_decode($filteredData);
                         $images_file1 = strtotime(date("Y-m-d H:i:s")) . "_company_crop_banner_image.png";
                         $filepath = "./assets/user_files/" . $images_file1;
                         file_put_contents($filepath, $unencodedData);
                         $Images = array(
                              $images_file1
                         );
                    }
                    $data['exibitor_array']['Id'] = $this->uri->segment(4);
                    $data['exibitor_array']['Organisor_id'] = $logged_in_user_id;
                    $data['exibitor_array']['Event_id'] = $Event_id;
                    $data['exibitor_array']['Heading'] = $this->input->post('Heading');
                    $data['exibitor_array']['Description'] = $this->input->post('Description');
                    $data['exibitor_array']['website_url'] = $this->input->post('website_url');
                    $data['exibitor_array']['facebook_url'] = $this->input->post('facebook_url');
                    $data['exibitor_array']['twitter_url'] = $this->input->post('twitter_url');
                    $data['exibitor_array']['linkedin_url'] = $this->input->post('linkedin_url');
                    $data['exibitor_array']['youtube_url'] = $this->input->post('youtube_url');
                    $data['exibitor_array']['instagram_url'] = $this->input->post('instagram_url');
                    // $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
                    // $data['exibitor_array']['main_email_address'] = $this->input->post('main_email_address');
                    // $data['exibitor_array']['stand_number'] = $this->input->post('stand_number');
                    // $data['exibitor_array']['phone_number1'] = $this->input->post('phone_number1');
                    // $data['exibitor_array']['HQ_phone_number'] = $this->input->post('HQ_phone_number');
                    $data['exibitor_array']['email_address'] = $this->input->post('email_address');
                    // $data['exibitor_array']['main_contact_name'] = $this->input->post('main_contact_name');
                    $data['exibitor_array']['Short_desc'] = $this->input->post('Short_desc');
                    $data['exibitor_array']['Images'] = $Images;
                    // $data['exibitor_array']['Address_map'] = $this->input->post('Address_map');
                    $data['exibitor_array']['old_images'] = $this->input->post('old_images');
                    $data['exibitor_array']['company_logo'] = $images_file;
                    $data['exibitor_array']['old_company_logo'] = $this->input->post('old_company_logo');
                    $this->Exibitor_model->update_exibitor($data);
                    $this->session->set_flashdata('exibitor_data', 'Updated');
                    redirect("Exhibitor_portal/edit/" . $id . "/" . $eid);
               }
               $this->session->set_userdata($data);
               $this->template->write_view('css', 'exhibitor_portal/add_css', $this->data, true);
               $this->template->write_view('content', 'exhibitor_portal/edit', $this->data, true);
               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Exibitor_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;
                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }
               $this->template->write_view('js', 'exhibitor_portal/add_js', $this->data, true);
               $this->template->render();
          }
     }
     public function delete($id=NULL, $Event_id=NULL)

     {
          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
               $exibitor_list = $this->Exibitor_model->get_exibitor_list($id);
               $this->data['exibitor_list'] = $exibitor_list[0];
          }
          $exibitor = $this->Exibitor_model->delete_exibitor($id, $Event_id);
          $this->session->set_flashdata('exibitor_data', 'Deleted');
          redirect("Exhibitor_portal/index/" . $Event_id);
     }
}