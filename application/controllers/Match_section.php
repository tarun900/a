<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends FrontendController

{
  function __construct()
  {
    $this->data['pagetitle'] = 'Dashboard';
    $this->data['smalltitle'] = 'overview &amp; stats';
    $this->data['breadcrumb'] = 'Dashboard';
    parent::__construct($this->data);
    $this->load->model('Event_model');
    $this->load->model('client_model');
    $this->load->model('Attendee_model');
  }
  public function index()

  {
    if ($this->data['user']->Role_name == 'Client')
    {
      $orid = $this->data['user']->Id;
      $res = $this->Event_model->get_latest_event_list();
      $this->data['Events'] = $res;
      $total_events = mysql_query("SELECT * FROM event WHERE organisor_id = $orid");
      $this->data['num_total_event'] = mysql_num_rows($total_events);
      $total_public = mysql_query("SELECT * FROM event WHERE event_type='1' AND organisor_id = $orid");
      $this->data['num_total_public_event'] = mysql_num_rows($total_public);
      $total_private = mysql_query("SELECT * FROM event WHERE event_type='0' AND organisor_id = $orid");
      $this->data['num_total_private_event'] = mysql_num_rows($total_private);
      $total_active = mysql_query("SELECT * FROM event WHERE status='1' AND organisor_id = $orid");
      $this->data['num_total_active_event'] = mysql_num_rows($total_active);
      $total_inactive = mysql_query("SELECT * FROM event WHERE status='0' AND organisor_id = $orid");
      $this->data['num_total_inactive_event'] = mysql_num_rows($total_inactive);
      $total_event = $this->Event_model->eventallocation_graph();
      $this->data['event_list'] = $total_event;
      $recentattendee = $this->Attendee_model->get_recent_attendee_list();
      $this->data['recentattendee'] = $recentattendee;
    }
    if ($this->data['user']->Role_name == 'Attendee' || $this->data['user']->Role_name == 'Speaker')
    {
      $Events = $this->Event_model->get_attendee_event_list();
      $this->data['Events'] = $Events;
      // echo'<pre>'; print_r($Events); exit;
    }
    if ($this->data['user']->Role_name == 'Administrator')
    {
      $res = $this->Event_model->get_latest_event_list();
      $this->data['Events'] = $res;
    }
    if ($this->data['user']->Role_name == 'User')
    {
      redirect('Profile');
    }
    $this->template->write_view('css', 'dashboard/css', $this->data, true);
    $this->template->write_view('content', 'dashboard/index', $this->data, true);
    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    $this->template->write_view('js', 'dashboard/js', $this->data, true);
    $this->template->render();
    return $res;
  }
}